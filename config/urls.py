import django
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.contrib.auth import views as auth_views
from django.apps import apps
from two_factor import urls as two_factor_url
# from blowhorn.app import application as api
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

# from blowhorn.legacy.app import application as legacy_api
from graphene_django.views import GraphQLView
from organizations.backends import invitation_backend
from django.views.i18n import JavaScriptCatalog
from django.contrib.flatpages import views
# from blowhorn.app import Blowhorn


# from blowhorn.oscar.core.loading import get_class

# bh_app = get_class('app', 'Blowhorn')

js_info_dict = {
    'packages': ('recurrence', ),
}

urlpatterns = [
    url(r'', include('website.urls')),
    url(r'', include('blowhorn.livetracking.urls')),
    url(r'', include('blowhorn.changelog.urls')),
    # Django Admin, use {% url 'admin:index' %}
    # url(r'^account/login/$', auth_views.login, {'template_name': 'registration/login.html'}),
    url(r'^account/login/$', auth_views.LoginView.as_view(template_name='registration/login.html')),
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    #    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    #    url(r'^admin', include(admin.site.urls)),
    url(settings.ADMIN_URL, admin.site.urls),
    url(r'^nested_admin/', include('nested_admin.urls')),

    # User management
    url(r'^users/', include(('blowhorn.users.urls', 'users'), namespace='users')),
    url(r'^accounts/', include('allauth.urls')),

    # JWT Authentication
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),

    # SQL Explorer
    #url(r'^explorer/', include('explorer.urls')),

    # Your stuff: custom urls includes go here
    url(r'^i18n/', include('django.conf.urls.i18n')),
    # url(r'', two_factor_url.url),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view, js_info_dict),
    # url(r'', include(apps.get_app_config('oscar').urls[0])),
    url(r'^api/', include('blowhorn.urls')),
    # url(r'', legacy_api.urls),
    url(r'^organization/', include('organizations.urls')),
    url(r'^graphql', GraphQLView.as_view(graphiql=True)),
    url(r'^invitations/', include(invitation_backend().get_urls())),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns

handler404 = 'website.views.custom_404'
