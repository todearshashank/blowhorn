import os
import sys

from django.core.wsgi import get_wsgi_application


app_path = os.path.dirname(os.path.abspath(__file__)).replace('/config', '')
sys.path.append(os.path.join(app_path, 'blowhorn'))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")

application = get_wsgi_application()

if os.environ.get('DJANGO_SETTINGS_MODULE') == 'config.settings.production':

    print("Launching MQTT Server")

    from blowhorn.activity import mqtt

    mqtt.client.loop_start()
