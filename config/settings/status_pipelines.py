from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from blowhorn.customer.constants import APPROVED, EXPIRED, PENDING_CUSTOMER_CONFIRMATION, CUSTOMER_CONFIRMED,\
    CUSTOMER_DISPUTED, UNAPPROVED, CREATED, REJECTED

# Parent Statuses, which will mainly be used to fetch the models based on
# the broad classification.
# This is a virtual entity and will not be used in any model.
# For model statuses, refer to the section below.
NEW = 0
IN_PROGRESS = 100
COMPLETED = 200
CANCELLED = 400
PARTIAL_COMPLETED = 700

# Shipment Alert Statuses
NORMAL = 'Normal'
ON_SCHEDULE = 'On Schedule'
LOW_CRITICAL = 'Low'
MEDIUM_CRITICAL = 'Medium'
HIGHLY_CRITICAL = 'High'
FAILURE = 'Failure'
DELAYED = 'Delayed Delivery'

# Define the status constants and their respective db values.
# Generally naming convention for variable is model name
# and the respective status
# Values can be anything we wish to store in the database.
ORDER_NEW = 'New-Order'
ORDER_ALLOCATED = 'Allocated'
ORDER_SENT_FOR_FULILMENT = 'SENT_FOR_FULFILMENT'
ORDER_PAYMENT_DONE = 'PAYMENT_DONE'
ORDER_PACKED = 'Packed'
ORDER_OUT_FOR_PICKUP = 'Out-For-Pickup'
ORDER_PICKED = 'Picked'
ORDER_PICKUP_FAILED = 'Pickup-Failed'
PICKUP_ATTEMPTED = 'Pickup Attempted'
WMS_ORDER_PICKED = 'WMS Picked'
WMS_ORDER_PARTIALLY_PICKED = 'WMS Partially Picked'
DRIVER_ACCEPTED = 'Accepted'
DRIVER_REJECTED = 'Rejected'
DRIVER_DISPATCHED = 'Dispatched'
DRIVER_NO_RESPONSE = 'No Response'
DRIVER_ASSIGNED = 'Assigned'
DRIVER_ARRIVED_AT_PICKUP = 'At-Pickup'
OUT_FOR_DELIVERY = 'Out-For-Delivery'
REACHED_AT_HUB = 'At-Hub'
IN_TRANSIT = 'In-Transit'
REACHED_AT_HUB_RTO = 'At-Hub-RTO'
ORDER_MOVING_TO_HUB = 'Moving-To-Hub'
ORDER_MOVING_TO_HUB_RTO = 'Moving-To-Hub-RTO'
DRIVER_ARRIVED_AT_DROPOFF = 'At-DropOff'
ORDER_RETURNED = 'Returned'
ORDER_RETURNED_RTO = 'Returned-To-Origin'
ORDER_RETURNED_RTO_FAILED = 'Returned-To-Origin-Failed'
ORDER_OUT_FOR_RTO = 'Out-For-RTO'
ORDER_DELIVERED = 'Delivered'
ORDER_RETURNED_TO_HUB = 'Returned-To-Hub'
ORDER_CANCELLED = 'Cancelled'
ORDER_EXPIRED = 'Expired'
ORDER_LOST = 'Lost'
UNABLE_TO_DELIVER = 'Unable-To-Deliver'
RESCHEDULE_DELIVERY = 'Rescheduled'
DELIVERY_ACK = 'Delivery-Acknowledged'
RETURNED_ACK = 'Returned-Acknowledged'
UNABLE_TO_DELIVER_ACK = 'Unable-To-Deliver-Acknowledged'
READY_FOR_DELIVERY = 'Ready-For-delivery'
ORDER_DELAYED = 'Delayed'
UNATTEMPTED = 'Un-Attempted'
INVALID_ORDER_TRANSITION = 'Invalid-Order-Transition'
INVALID_CUSTOMER = 'Invalid-Customer'
DUMMY_STATUS = 'Dummy'

#ECOMM order status
ORDER_PENDING = 'Order Pending'
ORDER_PLACED = 'Order Placed'

TRIP_NEW = 'New'
TRIP_REPORTED_AT_PICKUP = "Reported-at-Pickup"  # Not part of FSM Flow now.
TRIP_VEHICLE_LOAD = 'Vehicle Loading'
TRIP_STARTED = 'Started'
TRIP_COMPLETED = 'Completed'
TRIP_CANCELLED = 'Cancelled'
TRIP_IN_PROGRESS = 'In-Progress'
TRIP_INTERRUPT = 'Interrupted'
TRIP_SUSPENDED = 'Suspended'
TRIP_SCHEDULED_OFF = 'Scheduled-Off'
TRIP_ALL_STOPS_DONE = 'All-Stops-Done'
# used for subscription type of orders
TRIP_NO_SHOW = 'No-Show'
PAYMENT_ACKNOWLEDGE = 'Payment-Done'
DRIVER_RESTORED = 'Driver-Restored'
LOG_IN = 'Logged-In'
LOG_OUT = 'Logged-Out'
REORDERED_STOP_NEW = 'ReOrdered-Stop-To-New'
FCM_UPDATED = 'Fcm-Id-Updated'
REORDERED_STOP_IN_PROGRESS = 'ReOrdered-Stop-To-In-Progress'
APP_EVENT_TRIP_COMPLETED = 'Trip-Completed'
APP_EVENT_TRIP_END = 'Trip-End'
APP_EVENT_TRIP_START = 'Trip-Start'

TRIP_STATUS = [
    TRIP_NEW,
    TRIP_COMPLETED,
    TRIP_CANCELLED,
    TRIP_IN_PROGRESS,
    TRIP_SUSPENDED,
    TRIP_NO_SHOW,
    TRIP_SCHEDULED_OFF
]

TRIP_ALL_STATUSES = [
    TRIP_NEW,
    DRIVER_ACCEPTED,
    TRIP_IN_PROGRESS,
    TRIP_ALL_STOPS_DONE,
    TRIP_COMPLETED,
    TRIP_SUSPENDED,
    TRIP_CANCELLED,
    TRIP_NO_SHOW,
    TRIP_SCHEDULED_OFF,
    DUMMY_STATUS,
]

# Define the master statuses list and corresponding details for each status.
MASTER_STATII = {
    ORDER_NEW: {
        'description': 'New Order',
        'user_message': 'Your {} booking has been confirmed'.format(
            settings.BRAND_NAME),
        'color': '',
        'parent_status': NEW,
        'label': 'Booking Accepted',
        'value': 0
    },
    ORDER_PACKED: {
        'description': 'Order packed at the hub',
        'color': '339933',
        'parent_status': IN_PROGRESS,
        'label': 'Order Packed'
    },
    DRIVER_ASSIGNED: {
        'description': 'Order assigned to driver',
        'color': '339933',
        'parent_status': IN_PROGRESS,
        'label': 'Order Assigned'
    },
    DRIVER_ACCEPTED: {
        'description': 'Driver accepted the order',
        'user_message': 'Your {} driver is on its way to pickup'.format(
            settings.BRAND_NAME),
        'color': '339933',
        'parent_status': IN_PROGRESS,
        'label': 'Driver Accepted'
    },
    DRIVER_ARRIVED_AT_PICKUP: {
        'description': 'Vehicle is loading the order',
        'user_message': 'Your {} driver is loading the order'.format(
            settings.BRAND_NAME),
        'color': 'CE93D8',
        'parent_status': IN_PROGRESS,
        'label': 'Arrived At Pickup'
    },
    OUT_FOR_DELIVERY: {
        'description': 'Vehicle has been loaded.',
        'user_message': 'Your {} driver is on its way to drop off'.format(
            settings.BRAND_NAME),
        'color': '',
        'parent_status': IN_PROGRESS,
        'label': 'Finished Loading'
    },
    REACHED_AT_HUB: {
        'description': 'Vehicle reached at Hub.',
        'parent_status': IN_PROGRESS,
        'label': 'Reached At Hub'
    },
    DRIVER_ARRIVED_AT_DROPOFF: {
        'description': 'Vehicle reached the drop-off location.',
        'user_message': 'Your {} driver is at the drop off point'.format(
            settings.BRAND_NAME),
        'color': '81C784',
        'parent_status': IN_PROGRESS,
        'label': 'Arrived At Dropoff'
    },
    TRIP_NEW: {
        'description': 'Trip has been planned',
        'user_message': '',
        'color': '',
        'parent_status': IN_PROGRESS
    },
    TRIP_IN_PROGRESS: {
        'description': 'Trip is in progress.',
        'user_message': '',
        'color': '',
        'parent_status': IN_PROGRESS
    },
    ORDER_RETURNED: {
        'description': 'Order Returned',
        'user_message': '',
        'color': '',
        'parent_status': COMPLETED,
        'label': 'Return Order',
        'value': 4
    },
    ORDER_DELIVERED: {
        'description': 'Order Completed',
        'user_message': 'Your {} booking is completed'.format(
            settings.BRAND_NAME),
        'color': '',
        'parent_status': COMPLETED,
        'label': 'Deliver Order',
        'value': 2
    },
    UNABLE_TO_DELIVER: {
        'description': 'Unable to deliver the order',
        'user_message': '',
        'color': '',
        'parent_status': COMPLETED,
        'value': 3
    },
    TRIP_COMPLETED: {
        'description': 'Trip has been completed',
        'user_message': 'Your {} trip is complete!'.format(settings.BRAND_NAME),
        'color': '339933',
        'parent_status': COMPLETED
    },
    ORDER_CANCELLED: {
        'description': 'Order Cancelled',
        'user_message': '',
        'color': '',
        'parent_status': CANCELLED
    },
    TRIP_CANCELLED: {
        'description': 'Trip has been cancelled',
        'user_message': '',
        'color': '',
        'parent_status': CANCELLED
    },
    TRIP_INTERRUPT: {
        'description': 'Trip interrupted.',
        'user_message': '',
        'color': '',
        'parent_status': PARTIAL_COMPLETED
    },
    ORDER_RETURNED_TO_HUB: {
        'description': 'Order Returned to Hub',
        'user_message': '',
        'color': '',
        'parent_status': PARTIAL_COMPLETED
    }
}

SHIPMENT_ORDER_STATUSES = {
    DRIVER_ASSIGNED: 'Assigned',
    ORDER_PACKED: 'Packed',
    OUT_FOR_DELIVERY: 'Out-For-Delivery',
    ORDER_RETURNED: 'Returned',
    ORDER_DELIVERED: 'Delivered',
    ORDER_RETURNED_TO_HUB: 'Returned-To-Hub',
    ORDER_LOST: 'Lost',
    UNABLE_TO_DELIVER: 'Unable-To-Deliver',
    DELIVERY_ACK: 'Delivery-Acknowledged',
    ORDER_NEW: 'New-Order',
}

ORDER_STATUSES = {
    ORDER_NEW: 'Pending',
    DRIVER_ACCEPTED: 'Assigned',
    DRIVER_ASSIGNED: 'Assigned',
    DRIVER_ARRIVED_AT_PICKUP: 'In-Progress',
    OUT_FOR_DELIVERY: 'In-Progress',
    DRIVER_ARRIVED_AT_DROPOFF: 'In-Progress',
    ORDER_DELIVERED: 'Complete',
    ORDER_RETURNED: 'Returned',
    ORDER_RETURNED_TO_HUB: 'At-Hub',
    UNABLE_TO_DELIVER: 'Attempted',
    ORDER_CANCELLED: 'Cancelled',
    REACHED_AT_HUB: 'At-Hub',
    ORDER_OUT_FOR_PICKUP: 'Out-For-Pickup',
    ORDER_PICKED: 'Picked',
    ORDER_PICKUP_FAILED: 'Pickup-Failed',
    PICKUP_ATTEMPTED: 'Pickup Attempted',
    ORDER_MOVING_TO_HUB: 'Moving-To-Hub',
    DELIVERY_ACK: 'Delivery-Acknowledged',
    ORDER_RETURNED_RTO: 'Returned-To-Origin',
    ORDER_RETURNED_RTO_FAILED: 'Returned-To-Origin-Failed',
    ORDER_MOVING_TO_HUB_RTO: 'Moving-To-Origin',
    ORDER_OUT_FOR_RTO: 'Moving-To-Origin',
    REACHED_AT_HUB_RTO: 'At-Hub-RTO',
    ORDER_LOST: 'Shipment Lost',
    DUMMY_STATUS: 'Cancelled'
}

ORDER_STATUS_CHOICES = (
    ('Pending', 'Pending'),
    ('Assigned', 'Assigned'),
    ('In-Progress', 'In-Progress'),
    ('Complete', 'Complete'),
    ('Returned', 'Returned'),
    ('At-Hub', 'At-Hub'),
    ('Out-For-Pickup', 'Out-For-Pickup'),
    ('Picked', 'Picked'),
    ('Pickup-Failed', 'Pickup-Failed'),
    ('Pickup Attempted', 'Pickup Attempted'),
    ('Moving-To-Hub', 'Moving-To-Hub'),
    ('Delivery-Acknowledged', 'Delivery-Acknowledged'),
    ('Returned-To-Origin', 'Returned-To-Origin'),
    ('Returned-To-Origin-Failed', 'Returned-To-Origin-Failed'),
    ('Moving-To-Origin', 'Moving-To-Origin'),
    ('At-Hub-RTO', 'At-Hub-RTO'),
    ('Shipment Lost', 'Shipment Lost'),
    ('Cancelled', 'Cancelled')
)

SHIPMENT_STATUS_MAPPING_LEGEND = {
    ORDER_NEW: 'Pending',
    DRIVER_ACCEPTED: 'Pending',
    DRIVER_ASSIGNED: 'Pending',
    DRIVER_ARRIVED_AT_PICKUP: 'Pending',
    ORDER_OUT_FOR_PICKUP: 'Pending',
    OUT_FOR_DELIVERY: 'In-Progress',
    DRIVER_ARRIVED_AT_DROPOFF: 'In-Progress',
    REACHED_AT_HUB: 'In-Progress',
    ORDER_RETURNED_TO_HUB: 'In-Progress',
    ORDER_PICKED: 'In-Progress',
    ORDER_DELIVERED: 'Complete',
    ORDER_RETURNED: 'Returned',
    UNABLE_TO_DELIVER: 'Attempted',
    PICKUP_ATTEMPTED: 'Pickup Attempted',
    ORDER_MOVING_TO_HUB: 'Moving-To-Hub',
    DELIVERY_ACK: 'Delivery-Acknowledged',
    ORDER_RETURNED_RTO : 'Returned-To-Origin',
    ORDER_MOVING_TO_HUB_RTO: 'Moving-To-Origin',
    ORDER_OUT_FOR_RTO: 'Moving-To-Origin',
    ORDER_LOST: 'Shipment Lost',
    ORDER_CANCELLED: 'Cancelled',
    DUMMY_STATUS: 'Cancelled'
}


ORDER_B2C_STATUSES = {
    ORDER_NEW: 'Ordered',
    DRIVER_ACCEPTED: 'Packed',
    DRIVER_ASSIGNED: 'Packed',
    OUT_FOR_DELIVERY: 'Shipped',
    ORDER_DELIVERED: 'Delivered',
    ORDER_RETURNED: 'Returned',
    ORDER_RETURNED_TO_HUB: 'At Hub',
    UNABLE_TO_DELIVER: 'Attempted',
    ORDER_CANCELLED: 'Cancelled',
    ORDER_LOST: 'Shipment Lost',
    DUMMY_STATUS: 'Cancelled',
    RESCHEDULE_DELIVERY: "Attempted"
}

ORDER_B2C_DESC = {
    ORDER_NEW: 'Your Order has been placed',
    DRIVER_ACCEPTED: 'Your item has been picked by our delivery partner',
    DRIVER_ASSIGNED: 'Your item has been picked by our delivery partner',
    OUT_FOR_DELIVERY: 'Your order has been dispatched',
    ORDER_DELIVERED: 'Your item has been delivered',
    ORDER_RETURNED: 'Your shipment has been Retured',
    ORDER_RETURNED_TO_HUB: 'Your Order has arrived at our hub',
    UNABLE_TO_DELIVER: 'Your Order has been attempted',
    ORDER_CANCELLED: 'Your order has been Cancelled',
    ORDER_LOST: 'Your Shipment has been Lost',
    DUMMY_STATUS: 'Your order has been Cancelled',
    RESCHEDULE_DELIVERY: "Your order has been Attempted"
}

ORDER_B2C_FUTURE_DESC = {
    DRIVER_ACCEPTED: 'Delivery partner will be assigned soon',
    DRIVER_ASSIGNED: 'Delivery partner will be assigned soon',
    OUT_FOR_DELIVERY: 'Dispatching shortly',
    ORDER_DELIVERED: 'Your item will be delivered on or before %s',
    ORDER_CANCELLED: 'Your order has been Cancelled',
    ORDER_LOST: 'Your Shipment has been Lost',
    DUMMY_STATUS: 'Your order has been Cancelled',
    RESCHEDULE_DELIVERY: "Your order has been Attempted"
}

ORDER_DETAIL_STATUS = {
    ORDER_NEW: 'booking_accepted',
    DRIVER_ACCEPTED: 'driver_accepted',
    DRIVER_ARRIVED_AT_PICKUP: 'arrived_at_pickup',
    OUT_FOR_DELIVERY: 'finished_loading',
    DRIVER_ARRIVED_AT_DROPOFF: 'arrived_at_dropoff',
    ORDER_DELIVERED: 'completed'
}

TRIP_END_STATUSES = [TRIP_CANCELLED, TRIP_COMPLETED, TRIP_SUSPENDED, TRIP_SCHEDULED_OFF, TRIP_NO_SHOW,
                     DUMMY_STATUS]

INVOICE_EDIT_STATUSES = [CREATED, REJECTED]
INVOICE_END_STATUSES = [APPROVED, PENDING_CUSTOMER_CONFIRMATION, CUSTOMER_CONFIRMED, CUSTOMER_DISPUTED, UNAPPROVED]

ORDER_END_STATUSES = [ORDER_DELIVERED, ORDER_RETURNED,
                      ORDER_CANCELLED, ORDER_LOST,
                      DELIVERY_ACK, DUMMY_STATUS,
                      RETURNED_ACK, ORDER_RETURNED_RTO]

SHIPMENT_ORDER_END_STATUSES = [ORDER_DELIVERED,
                               ORDER_CANCELLED, ORDER_LOST,
                               DELIVERY_ACK, DUMMY_STATUS,
                               RETURNED_ACK, ORDER_RETURNED_RTO]

SHIPMENT_TRIP_ORDER_STATUSES = [ORDER_DELIVERED, ORDER_RETURNED_RTO, ORDER_PENDING,
                                ORDER_CANCELLED, ORDER_LOST,
                                DELIVERY_ACK, DUMMY_STATUS]

ORDER_CANCELLED_STATUS = [ORDER_RETURNED, ORDER_CANCELLED, DUMMY_STATUS ]

ORDER_RETURN_TO_HUB_TRANSITION_STATUSES_CHECK = [
    ORDER_RETURNED, UNABLE_TO_DELIVER, OUT_FOR_DELIVERY, ORDER_CANCELLED]
ORDER_SCANNING_STATUSES_CHECK = [
    ORDER_PACKED, ORDER_RETURNED_TO_HUB, REACHED_AT_HUB, OUT_FOR_DELIVERY]

RETRIP_ORDER_STATUSES = [ORDER_PICKED, REACHED_AT_HUB]

WEBHOOK_STATUS_MAP = {
    34182 : [DRIVER_ASSIGNED, OUT_FOR_DELIVERY, ORDER_DELIVERED]
}

DASHBOARD_ORDER_STATUSES = [
    ORDER_NEW,
    ORDER_PICKED,
    OUT_FOR_DELIVERY,
    ORDER_LOST,
    RESCHEDULE_DELIVERY,
    ORDER_DELAYED,
    ORDER_RETURNED,
    ORDER_DELIVERED,
    UNABLE_TO_DELIVER,
    ORDER_RETURNED_TO_HUB,
    ORDER_CANCELLED,
]

# Defines the status pipeline for orders.
ORDER_STATUS_PIPELINE = {
    settings.C2C_ORDER_TYPE: {
        ORDER_NEW: (DRIVER_ACCEPTED, ORDER_CANCELLED,),
        DRIVER_ACCEPTED: (DRIVER_ARRIVED_AT_PICKUP, ORDER_CANCELLED,),
        DRIVER_ARRIVED_AT_PICKUP: (OUT_FOR_DELIVERY, ORDER_CANCELLED,),
        OUT_FOR_DELIVERY: (DRIVER_ARRIVED_AT_DROPOFF,),
        DRIVER_ARRIVED_AT_DROPOFF: (ORDER_DELIVERED,),
        ORDER_DELIVERED: (),
        ORDER_CANCELLED: (),
    },
    settings.SHIPMENT_ORDER_TYPE: {
        ORDER_NEW: (ORDER_CANCELLED, DRIVER_ARRIVED_AT_PICKUP, ORDER_PACKED, OUT_FOR_DELIVERY),
        ORDER_PACKED: (ORDER_CANCELLED, DRIVER_ASSIGNED,),
        DRIVER_ASSIGNED: (OUT_FOR_DELIVERY, ORDER_CANCELLED,),
        DRIVER_ARRIVED_AT_PICKUP: (OUT_FOR_DELIVERY, ORDER_CANCELLED,),
        OUT_FOR_DELIVERY: (DRIVER_ASSIGNED, ORDER_RETURNED_TO_HUB, REACHED_AT_HUB, UNABLE_TO_DELIVER, ORDER_DELIVERED, ORDER_RETURNED, ORDER_CANCELLED,),
        REACHED_AT_HUB: (OUT_FOR_DELIVERY, ORDER_CANCELLED,),
        ORDER_DELIVERED: (),
        ORDER_CANCELLED: (ORDER_RETURNED_TO_HUB,),
        ORDER_RETURNED: (ORDER_RETURNED_TO_HUB,),
        UNABLE_TO_DELIVER: (ORDER_RETURNED_TO_HUB,),
        ORDER_RETURNED_TO_HUB: (OUT_FOR_DELIVERY,),
        DELIVERY_ACK: (),
        RETURNED_ACK: (),
        UNABLE_TO_DELIVER_ACK: (),
    }

}
# Define the current and the available new status for order line .
LINE_STATUS_PIPELINE = {
    'default': {

    }
}
# Define the current and the available new status for a trip .
TRIP_STATUS_PIPELINE = {
    'default': {
        TRIP_NEW: (TRIP_IN_PROGRESS, TRIP_CANCELLED),
        TRIP_IN_PROGRESS: (TRIP_COMPLETED, TRIP_CANCELLED,),
        TRIP_CANCELLED: (),
        TRIP_COMPLETED: (),
        TRIP_NO_SHOW: ()
    }
}

# Define the current and the available new status for a trip stops .
STOPS_STATUS_PIPELINE = {
    'default': {
        TRIP_NEW: (TRIP_IN_PROGRESS, TRIP_CANCELLED,),
        TRIP_IN_PROGRESS: (TRIP_COMPLETED, TRIP_CANCELLED, TRIP_NEW),
        TRIP_CANCELLED: (),
        TRIP_COMPLETED: ()
    }
}

# Define the trigger status of order line once the status of order has
# been updated.
ORDER_STATUS_CASCADE = {
    'default': {
        DRIVER_ACCEPTED: TRIP_NEW,
    }
}

# Shipment Alert Statuses
NORMAL = 'NORMAL'
ON_SCHEDULE = 'ON SCHEDULE'
LOW_CRITICAL = 'LOW'
MEDIUM_CRITICAL = 'MEDIUM'
HIGHLY_CRITICAL = 'HIGH'
FAILURE = 'FAILURE'
SUCCESS = 'SUCCESS'

ALERT_COLORS = {
    NORMAL: '#4A4A4A',
    ON_SCHEDULE: '#74ED63',
    LOW_CRITICAL: '#E39506',
    MEDIUM_CRITICAL: '#B52F18',
    HIGHLY_CRITICAL: '#FC2C07',
    FAILURE: '#FF0000',
    SUCCESS: '#27F10C',
}


COLOR_DEFAULT = '#4A4A4A'
COLOR_NORMAL = '#006F89'
COLOR_DANGER = '#B33131'
COLOR_WARNING = '#BC8A42'
COLOR_SUCCESS = '#007435'

ORDER_STATUS_COLOURS = {
    ORDER_NEW: COLOR_DEFAULT,
    ORDER_PICKED: COLOR_NORMAL,
    OUT_FOR_DELIVERY: COLOR_NORMAL,
    ORDER_DELIVERED: COLOR_SUCCESS,
    DELIVERY_ACK: COLOR_SUCCESS,
    RETURNED_ACK: COLOR_SUCCESS,
    ORDER_LOST: COLOR_WARNING,
    ORDER_DELAYED: COLOR_WARNING,
    UNABLE_TO_DELIVER_ACK: COLOR_WARNING,
    RESCHEDULE_DELIVERY: COLOR_WARNING,
    ORDER_RETURNED: COLOR_DANGER,
    ORDER_RETURNED_TO_HUB: COLOR_DANGER,
    UNABLE_TO_DELIVER: COLOR_DANGER,
    ORDER_CANCELLED: COLOR_DANGER,
}

SHIPMENT_ALERT_LEVELS = (
    (NORMAL, NORMAL),
    (ON_SCHEDULE, ON_SCHEDULE),
    (LOW_CRITICAL, LOW_CRITICAL),
    (MEDIUM_CRITICAL, MEDIUM_CRITICAL),
    (HIGHLY_CRITICAL, HIGHLY_CRITICAL),
    (FAILURE, FAILURE)
)

ALERT_LEVELS_COLOURS = (
    ('#66ff33', 'Green'),
    ('#ffff1a', 'Yellow'),
    ('#ff6600', 'Orange'),
    ('#ff0000', 'Red'),
    ('#800000', 'Brown')
)


# Returns the status pipelines for a given model and site_id combination.
# If current status is provided, it will return the available statii, it
# can move.
def get_status_pipeline(model, site_id=settings.SITE_ID,
                        current_status=None, order_type=None):

    model_class = model.__name__ if isinstance(model, type) else None
    if not model_class:
        print("Invalid model class provided.")
        return ()

    pipeline_constant = '%s_STATUS_PIPELINE' % model_class.upper()

    try:
        statii = eval(pipeline_constant)
    except AttributeError:
        print("Model status pipelines has not been defined yet.")
        return ()

    valid_site_statii = _filter_valid_statii(statii=statii,
                                             site_id=site_id, order_type=order_type)

    if current_status is not None:
        return valid_site_statii.get(current_status, ())
    return valid_site_statii

# Returns the status cascade for a given model and site_id combination.
# If current status is provided, it will return the next possible statii,
# it can move.


def get_status_cascade(model, site_id=settings.SITE_ID,
                       current_status=None, order_type=None):

    model_class = type(model).__name__ if isinstance(model, type) else None
    if not model_class:
        print("Invalid model class provided.")
        return ()

    status_cascade = '%s_STATUS_CASCADE' % model_class.upper()

    try:
        statii = eval(status_cascade)
    except AttributeError:
        print("Model status cascade has not been defined yet.")
        return ()

    valid_site_statii = _filter_valid_statii(statii=statii,
                                             site_id=site_id, order_type=order_type)
    if current_status is not None:
        return valid_site_statii.get(current_status, ())

    return valid_site_statii

# Reverts back the label for a particular status, if that is a valid status.


def get_status_info(status):
    if status in MASTER_STATII:
        return MASTER_STATII[status]

    print("Invalid status %s" % status)
    return None

# Reverts all the hypernym status for a given status from STATII_TREE


def get_child_statii(status):
    child_status_list = []
    for master_status in MASTER_STATII:
        if MASTER_STATII[master_status]['parent_status'] == status:
            child_status_list.append(master_status)
    return child_status_list

# Private function which filters out the statii based on site_id and from
# the MASTER_STATII list.


def _filter_valid_statii(statii, site_id, order_type):

    if not order_type:
        order_type = 'default'

    site_statii = statii.get(site_id, statii.get(order_type))
    for status in site_statii:
        if status not in MASTER_STATII:
            # Removing the invalid status which is not present in MASTER
            # STATII.
            del site_statii[status]
            print(
                'Invalid statii %s. Can\'t find the status in the MASTER_STATII list' % status)

    return site_statii


# Business Flow's
FEDEX_FLOW = 'FEDEX'
AMZ_AUTO_FLOW = 'AMZ-auto-now-flow'
FURLENCO_FLOW = 'Furlenco'
AMZ_PACKAGE_FLOW = 'AMZ-package-delivery-flow'
ZOPNOW_FLOW = 'Zopnow-flow'

BUSINESS_FLOWS = (
    (AMZ_AUTO_FLOW, AMZ_AUTO_FLOW),
    (AMZ_PACKAGE_FLOW, AMZ_PACKAGE_FLOW),
    (FEDEX_FLOW, FEDEX_FLOW),
    (FURLENCO_FLOW, FURLENCO_FLOW),
    (ZOPNOW_FLOW, ZOPNOW_FLOW)
)

# Driver statuses
REFERRED = 'referred'
SUPPLY = 'supply'
ONBOARDING = 'onboarding'
ACTIVE = 'active'
INACTIVE = 'inactive'
BLACKLISTED = 'blacklisted'
DRIVER_STATUSES = (
    (REFERRED, 'Referred'),
    (SUPPLY, 'Supply'),
    (ONBOARDING, 'Onboarding'),
    (ACTIVE, 'Active'),
    (INACTIVE, 'Inactive'),
    (BLACKLISTED, 'Blacklisted')
)

DRIVER_DISPLAY_STATUSES = {
    REFERRED: ((REFERRED, 'Referred'),),
    SUPPLY: ((SUPPLY, 'Supply'),),
    ONBOARDING: ((ONBOARDING, 'Onboarding'),),
    ACTIVE: ((ACTIVE, 'Active'),),
    INACTIVE: ((INACTIVE, 'Inactive'),),
    BLACKLISTED: ((BLACKLISTED, 'Blacklisted'),)
}

DRIVER_BANNED_STATUSES = [INACTIVE, BLACKLISTED]

DRIVER_DEFAULT_STATUS = ((SUPPLY, 'Supply'),)
# DRIVER_BLACKLISTED_STATUS = ((BLACKLISTED, 'Blacklisted'),)

DRIVER_ALLOWED_STATUSES = (
    (ONBOARDING, 'Onboarding'),
    (ACTIVE, 'Active'),
    (INACTIVE, 'Inactive'),
    (BLACKLISTED, 'Blacklisted')
)

DRIVER_END_STATUSES = (
    (ACTIVE, 'Active'),
    (INACTIVE, 'Inactive'),
    (BLACKLISTED, 'Blacklisted')
)

DEACTIVATION_REASONS = (
    ('Lost to competition', 'Lost to competition'),
    ('Contract issue - Timings', 'Contract issue - Timings'),
    ('Contract issue - Route/Kms', 'Contract issue - Route/Kms'),
    ('Low salary', 'Low salary'),
    ('Lack of Bookings', 'Lack of Bookings'),
    ('Delay in Salary', 'Delay in Salary'),
    ('Personal issues', 'Personal issues'),
    ('Disciplinary Isues', 'Disciplinary Isues'),
    ('Contract closed', 'Contract closed'),
    ('Sudden Discontinuation - without prior notice', 'Sudden Discontinuation - without prior notice'),
    ('High Absenteeism', 'High Absenteeism'),
    ('Theft and fraud', 'Theft and fraud'),
    ('Intoxicated on duty', 'Intoxicated on duty'),
    ('Rude and indecent behavior', 'Rude and indecent behavior'),
    ('Damaged goods or property', 'Damaged goods or property'),
    ('On duty violation', 'On duty violation'),
    ('Others', 'Others'),
)

TRIP_STATUSES = (
    (TRIP_NEW, TRIP_NEW),
    (TRIP_IN_PROGRESS, TRIP_IN_PROGRESS),
    (TRIP_CANCELLED, TRIP_CANCELLED),
    (TRIP_COMPLETED, TRIP_COMPLETED),
    (TRIP_NO_SHOW, TRIP_NO_SHOW)
)

# Better Place statuses for background verification
PENDING = 'pending'
VERIFIED = 'verified'
RED_CASE = 'red_case'
PARTIALLY_VERIFIED = 'partially_verified'
NOT_VERIFIABLE = 'not_verifiable'
CANCELLED = 'cancelled'
NO_RESPONSE = 'no_response'
DOCUMENT_SUBMITTED = 'document_submitted'
ACKNOWLEDGEMENT_RECEIVED = 'acknowledgement_received'

BACKGROUND_VERIFICATION_STATUSES = (
    (PENDING, "Pending"),
    (VERIFIED, "Verified"),
    (RED_CASE, "Red Case"),
    (PARTIALLY_VERIFIED, "Partially Verified"),
    (NOT_VERIFIABLE, "Not Verifiable"),
    (CANCELLED, "Cancelled"),
    (NO_RESPONSE, "No Response")
)

BACKGROUND_VERIFICATION_SUB_STATUSES = (
    (DOCUMENT_SUBMITTED, "Document Submitted"),
    (ACKNOWLEDGEMENT_RECEIVED, "Acknowledgement Received")
)


APP_ORDER_STATUS_NEW = 'upcoming'
APP_ORDER_STATUS_CURRENT = 'current'
APP_ORDER_STATUS_PAST = 'past'

MSG_EVENTS = (
    (ORDER_PICKED, _('Picked')),
    (OUT_FOR_DELIVERY, _('Out-For-Delivery')),
    (UNABLE_TO_DELIVER, _('Unable-To-Deliver')),
    (ORDER_DELIVERED, _('Delivered')),
    (ORDER_RETURNED, _('Returned')),
    (RESCHEDULE_DELIVERY, _('Rescheduled'))
)

STOP_MSG_STATUS = (
    (TRIP_NEW, _('New')),
    (TRIP_IN_PROGRESS, _('In-Progress'))
)

TRACKING_WITHOUT_MAP = [
    'booking_accepted',
    'driver_accepted',
    DRIVER_ACCEPTED,
    DRIVER_ASSIGNED,
    DRIVER_ARRIVED_AT_DROPOFF,
    REACHED_AT_HUB,
    REACHED_AT_HUB_RTO,
    ORDER_MOVING_TO_HUB,
    ORDER_NEW,
    ORDER_PENDING,
    ORDER_OUT_FOR_PICKUP,
    ORDER_PICKED,
    ORDER_PICKUP_FAILED,
    CANCELLED,
    DUMMY_STATUS,
    TRIP_SUSPENDED,
    EXPIRED,
    ORDER_LOST,
    ORDER_MOVING_TO_HUB_RTO,
    ORDER_RETURNED_TO_HUB,
    ORDER_RETURNED_RTO,
    ORDER_RETURNED_RTO_FAILED,
    ORDER_RETURNED,
    REJECTED,
    UNABLE_TO_DELIVER
]

PARCEL_TRACKING_STATUSES = {
    ORDER_NEW: 'Created',
    DRIVER_ACCEPTED: 'Assigned',
    DRIVER_ASSIGNED: 'Assigned',
    DRIVER_ARRIVED_AT_PICKUP: OUT_FOR_DELIVERY,
    OUT_FOR_DELIVERY: OUT_FOR_DELIVERY,
    DRIVER_ARRIVED_AT_DROPOFF: OUT_FOR_DELIVERY,
    ORDER_DELIVERED: 'Delivered',
    ORDER_RETURNED: 'Rejected',
    ORDER_RETURNED_TO_HUB: 'Returned',
    UNABLE_TO_DELIVER: 'Attempted',
    ORDER_CANCELLED: 'Cancelled',
    REACHED_AT_HUB: 'At-Hub',
    ORDER_OUT_FOR_PICKUP: 'Out-For-Pickup',
    ORDER_PICKED: 'Picked',
    ORDER_PICKUP_FAILED: 'Pickup-Failed',
    PICKUP_ATTEMPTED: 'Pickup Attempted',
    ORDER_MOVING_TO_HUB: 'Moving-To-Hub',
    DELIVERY_ACK: 'Delivery-Acknowledged',
    ORDER_RETURNED_RTO: 'Returned-To-Origin',
    ORDER_RETURNED_RTO_FAILED: 'Returned-To-Origin-Failed',
    ORDER_MOVING_TO_HUB_RTO: 'Moving-To-Origin',
    ORDER_OUT_FOR_RTO: 'Moving-To-Origin',
    REACHED_AT_HUB_RTO: 'At-Hub-RTO',
    ORDER_LOST: 'Lost',
    DUMMY_STATUS: 'Cancelled'
}

PARCEL_TRACKING_PIPELINE = [
    PARCEL_TRACKING_STATUSES[ORDER_NEW],
    PARCEL_TRACKING_STATUSES[ORDER_PICKED],
    PARCEL_TRACKING_STATUSES[OUT_FOR_DELIVERY],
]

PARCEL_TRACKING_END_STATUSES = [
    PARCEL_TRACKING_STATUSES[ORDER_DELIVERED],
    PARCEL_TRACKING_STATUSES[ORDER_CANCELLED],
    PARCEL_TRACKING_STATUSES[ORDER_RETURNED],
    PARCEL_TRACKING_STATUSES[ORDER_LOST],
    PARCEL_TRACKING_STATUSES[ORDER_RETURNED_TO_HUB],
]

PARCEL_TRACKING_PIPELINE_MAPPING = {
    PARCEL_TRACKING_STATUSES[ORDER_NEW]: [
        PARCEL_TRACKING_STATUSES[ORDER_NEW],
        PARCEL_TRACKING_STATUSES[DRIVER_ACCEPTED],
        PARCEL_TRACKING_STATUSES[DRIVER_ASSIGNED],
        PARCEL_TRACKING_STATUSES[DRIVER_ARRIVED_AT_PICKUP],
        PARCEL_TRACKING_STATUSES[ORDER_OUT_FOR_PICKUP],
    ],
    PARCEL_TRACKING_STATUSES[ORDER_PICKED]: [
        PARCEL_TRACKING_STATUSES[ORDER_PICKED],
    ],
    PARCEL_TRACKING_STATUSES[OUT_FOR_DELIVERY]: [
        PARCEL_TRACKING_STATUSES[OUT_FOR_DELIVERY],
        PARCEL_TRACKING_STATUSES[DRIVER_ARRIVED_AT_DROPOFF]
    ]
}

TRACKING_MESSAGES = [{
    'statuses': [ORDER_NEW, ORDER_PENDING,],
    'messages': {
        'present': 'Your order is awaited from #CUSTOMER_NAME.',
        'past': 'Your order was awaited from #CUSTOMER_NAME.',
    }
}, {
    'statuses': [DRIVER_ASSIGNED, DRIVER_ACCEPTED],
    'messages': {
        'present': 'Your order has been assigned for pick up.',
        'past': 'Your order was assigned for pick up.'
    }
}, {
    'statuses': [ORDER_OUT_FOR_PICKUP],
    'messages': {
        'present': 'Your order is out for pickup.',
        'past': 'Your order was out for pickup.'
    }
}, {
    'statuses': [ORDER_PICKED, DRIVER_ARRIVED_AT_PICKUP ],
    'messages': {
        'present': 'Your order is picked up and will soon be out for delivery.',
        'past': 'Your order was picked up.'
    }
}, {
    'statuses': [OUT_FOR_DELIVERY],
    'messages': {
        'present': 'Your order is dispatched and is out for delivery.',
        'past': 'Your order was dispatched (out for delivery).'
    }
}, {
    'statuses': [ORDER_DELIVERED],
    'messages': {
        'present': 'Your order has been delivered successfully.',
        'past': 'Your order has been delivered successfully.'
    }
}, {
    'statuses': [REJECTED, ORDER_RETURNED],
    'messages': {
        'present': 'Order has been rejected by #END_CUSTOMER_NAME on #EVENT_DATETIME.',
        'past': 'Order was rejected by #END_CUSTOMER_NAME on #EVENT_DATETIME.'
    }
}, {
    'statuses': [ORDER_RETURNED_TO_HUB, ORDER_RETURNED_RTO_FAILED],
    'messages': {
        'present': 'Your order has been returned to #CUSTOMER_NAME on #EVENT_DATETIME.',
        'past': 'Your order was returned to #CUSTOMER_NAME on #EVENT_DATETIME.'
    }
}, {
    'statuses': [UNABLE_TO_DELIVER],
    'messages': {
        'present': 'Delivery was attempted on #EVENT_DATETIME and we were unable to deliver due to #EVENT_REMARKS. We will attempt to deliver soon and you can track the order once it is out for delivery.',
        'past': 'Delivery was attempted on #EVENT_DATETIME and we were unable to deliver due to #EVENT_REMARKS.'
    }
}, {
    'statuses': [ORDER_CANCELLED],
    'messages': {
        'present': 'This order has been cancelled by #CUSTOMER_NAME. Request you to contact #CUSTOMER_NAME customer care.',
        'past': 'This order has been cancelled by #CUSTOMER_NAME. Request you to contact #CUSTOMER_NAME customer care.'
    }
},  {
    'statuses': [REACHED_AT_HUB],
    'messages': {
        'present': 'Your order has reached #HUB_NAME.',
        'past': 'Your order reached #HUB_NAME.'
    }
}, {
    'statuses': [ORDER_PICKUP_FAILED,],
    'messages': {
        'present': 'The order pick-up failed due to #EVENT_REMARKS. We will attempt to pick-up the order soon.',
        'past': 'The order pick-up failed due to #EVENT_REMARKS.'
    }
},
{
    'statuses': [PICKUP_ATTEMPTED],
    'messages': {
        'present': 'Pick-up was attempted on #EVENT_DATETIME and we were unable to pick-up due to #EVENT_REMARKS. We will attempt to pick-up soon.',
        'past': 'Pick-up was attempted on #EVENT_DATETIME and we were unable to pick-up due to #EVENT_REMARKS.'
    }
}, {
    'statuses': [ORDER_MOVING_TO_HUB],
    'messages': {
        'present': 'Package is moving to a nearby hub (#HUB_NAME)',
        'past': 'Package was moved to a nearby hub (#HUB_NAME)'
    }
}, {
    'statuses': [ORDER_RETURNED_RTO],
    'messages': {
        'present': 'Your order has been returned to #CUSTOMER_NAME on #EVENT_DATETIME.',
        'past': 'Your order has been returned to #CUSTOMER_NAME on #EVENT_DATETIME.'
    }
}, {
    'statuses': [ORDER_RETURNED_RTO_FAILED],
    'messages': {
        'present': 'Order returning to the customer was attempted on #EVENT_DATETIME and we were unable to return due to #EVENT_REMARKS. We will attempt to return soon.',
        'past': ' Order returning to the customer was attempted on #EVENT_DATETIME and we were unable to return due to #EVENT_REMARKS.'
    }
}, {
    'statuses': [ORDER_MOVING_TO_HUB_RTO],
    'messages': {
        'present': 'Package is moving to a nearby hub (#HUB_NAME)',
        'past': 'Package was moved to a nearby hub (#HUB_NAME)'
    }
}, {
    'statuses': [ORDER_OUT_FOR_RTO],
    'messages': {
        'present': 'Your order has been picked and the return is initiated.',
        'past': 'Your order was picked and the return was initiated.'
    }
}, {
    'statuses': [REACHED_AT_HUB_RTO],
    'messages': {
        'present': 'Your order has been picked from you and the return is initiated.',
        'past': 'Your order was picked and the return was initiated.'
    }
},
{
    'statuses': [ORDER_LOST,],
    'messages': {
        'present': 'This package is lost in transition. Please contact our customer care.',
        'past': 'This package is lost in transition. Please contact our customer care.'
    }
},
{
    'statuses': [DUMMY_STATUS, TRIP_SUSPENDED, EXPIRED],
    'messages': {
        'present': 'Order not found',
        'past': 'Order not found',
    }
},]

SHOPIFY_STATUS_MAPPING = {
    ORDER_PICKED: 'picked_up',
    WMS_ORDER_PICKED: 'picked_up',
    DRIVER_ASSIGNED: 'picked_up',
    OUT_FOR_DELIVERY: 'out_for_delivery',
    ORDER_DELIVERED: 'delivered',
    UNABLE_TO_DELIVER: 'attempted_delivery',
    REACHED_AT_HUB: 'in_transit',
    IN_TRANSIT: 'in_transit',
    ORDER_CANCELLED: 'failure'
}
# Shopify Status
SHOPIFY_LABEL_PRINTED_STATUS = 'label_printed'
SHOPIFY_LABEL_PURCHASED_STATUS = 'label_purchased'
SHOPIFY_ATTEMPTED_DELIVERY_STATUS = 'attempted_delivery'
SHOPIFY_READY_FOR_PICKUP_STATUS = 'ready_for_pickup'
SHOPIFY_PICKED_UP_STATUS = 'picked_up'
SHOPIFY_CONFIRMED_STATUS = 'confirmed'
SHOPIFY_IN_TRANSIT_STATUS = 'in_transit'
SHOPIFY_OUT_FOR_DELIVERY_STATUS = 'out_for_delivery'
SHOPIFY_DELIVERED_STATUS = 'delivered'
SHOPIFY_FAILURE_STATUS = 'failure'


SHOPIFY_FULFILLMENT_PENDING = 'pending'
SHOPIFY_FULFILLMENT_OPEN = 'open'
SHOPIFY_FULFILLMENT_SUCCESS = 'success'
SHOPIFY_FULFILLMENT_CANCELLLED = 'cancelled'
SHOPIFY_FULFILLMENT_ERROR = 'error'
SHOPIFY_FULFILLMENT_FAILURE = 'failure'

SHOPIFY_FULFILLMENT_MAPPING = {
    OUT_FOR_DELIVERY : SHOPIFY_FULFILLMENT_SUCCESS,
    ORDER_DELIVERED : SHOPIFY_FULFILLMENT_SUCCESS,
    ORDER_CANCELLED : SHOPIFY_FULFILLMENT_CANCELLLED,
    ORDER_RETURNED : SHOPIFY_FULFILLMENT_CANCELLLED
}

SHOPIFY_STATUS_MAPPING = {
    OUT_FOR_DELIVERY : SHOPIFY_OUT_FOR_DELIVERY_STATUS,
    ORDER_DELIVERED : SHOPIFY_DELIVERED_STATUS,
    UNABLE_TO_DELIVER : SHOPIFY_ATTEMPTED_DELIVERY_STATUS,
    ORDER_CANCELLED : SHOPIFY_FAILURE_STATUS,
    ORDER_RETURNED : SHOPIFY_FAILURE_STATUS
}
