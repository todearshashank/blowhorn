# -*- coding: utf-8 -*-
"""
Django settings for Blowhorn Logistics project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from .integrations import *
from .web import *
import environ
import os
from kombu import Exchange, Queue  # celery dependencies

"""
Import all the oscar items
"""
#from  oscar.defaults import *
from django.utils.translation import ugettext_lazy as _
import datetime
OSCAR_SLUG_ALLOW_UNICODE = False

# (blowhorn/config/settings/base.py - 3 = blowhorn/)
ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('blowhorn')
SITE_ROOT = os.path.dirname(os.path.realpath(__name__))
TEMPORARY_FILE_DIR = '/tmp'

# Load operating system environment variables and then prepare to use them
env = environ.Env()

# .env file, should load only in development environment
READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=False)
USE_READ_REPLICA = 'RDS_READ_REP_1' in os.environ
OSCAR_DELETE_IMAGE_FILES = True

# HOOK_EVENTS = {
#     # 'any.event.name': 'App.Model.Action' (created/updated/deleted)
#     'order.added': 'order.Order.created',
#     'order.changed': 'order.Order.updated+',
#     'order.removed': 'order.Order.deleted',
#     # and custom events, no extra meta data needed
#     'order.read': 'order.Order.read',
#     'event.added': 'order.Event.created',
#     'user.logged_in': None
# }
#
# HOOK_FINDER = 'blowhorn.company.models.find_and_fire_hook'

if READ_DOT_ENV_FILE:
    # Operating System Environment variables have precedence over variables defined in the .env file,
    # that is to say variables from the .env files will only be used if not defined
    # as environment variables.
    env_file = str(ROOT_DIR.path('.env'))
    print('Loading : {}'.format(env_file))
    env.read_env(env_file)
    print('The .env file has been loaded. See base.py for more information')

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.flatpages',
    'django.forms',

    # Useful template tags:
    # 'django.contrib.humanize',

    # Admin
    'jet',
    #    'suit',
    'django.contrib.admin',
    'nested_admin',
    'nested_inline',

    # OTP and two-factor authentication
    'django_otp',
    'django_otp.plugins.otp_hotp',
    'django_otp.plugins.otp_totp',
    'django_otp.plugins.otp_static',

    # DRF for APIs
    'rest_framework',
    'rest_framework_gis',
    'import_export',
    #'widget_tweaks',

    # Scheduler
    'recurrence',

    # Map widgets
    'mapwidgets',

    # Map filters for
    'mathfilters'
]
THIRD_PARTY_APPS = [
    # 'rest_hooks',
    'crispy_forms',  # Form layouts
    'allauth',  # registration
    'allauth.account',  # registration
    'allauth.socialaccount',  # registration
    'allauth.socialaccount.providers.facebook',  # facebook
    'allauth.socialaccount.providers.google',  # google
    'auditlog',  # audit log and history
    'audit_log',
    # 'django_auditlog',
    'rest_framework_swagger',
    'floppyforms',
    'fcm_django',
    'django_celery_results',  # celery backend results -- redis
    'array_tags',  # tag widgets for ArrayField
    'django_celery_beat',  # database backed periodic tasks
    'django_fsm',  # FSM
    'fsm_admin',  # FSM Admin
    'rangefilter',
    'sortedm2m',
    'django.contrib.humanize',
    'prettyjson',  # JSON Formatter Django Admin
    'rest_condition',
    'graphene_django',
    'django_filters',
    'organizations',
    'ckeditor',
    # Django Elasticsearch integration
    'django_elasticsearch_dsl',
    'django_elasticsearch_dsl_drf',
    'corsheaders',
    # 'architect',

    # All apps from  Oscar need to be added here
    #'oscar.config.Shop',
    #'oscar.apps.analytics.apps.AnalyticsConfig',
    #'oscar.apps.checkout.apps.CheckoutConfig',
    #'oscar.apps.shipping.apps.ShippingConfig',
    # 'blowhorn.oscar.apps.catalogue.apps.CatalogueConfig',
    #'oscar.apps.catalogue.reviews.apps.CatalogueReviewsConfig',
    #'oscar.apps.communication.apps.CommunicationConfig',
    #'oscar.apps.partner.apps.PartnerConfig',
    #'oscar.apps.basket.apps.BasketConfig',
    # 'oscar.apps.payment.apps.PaymentConfig',
    #'oscar.apps.offer.apps.OfferConfig',
    # 'oscar.apps.order.apps.OrderConfig',
    # 'oscar.apps.customer.apps.CustomerConfig',
    #'oscar.apps.search.apps.SearchConfig',
    #'oscar.apps.voucher.apps.VoucherConfig',
    #'oscar.apps.wishlists.apps.WishlistsConfig',
    #'oscar.apps.dashboard.apps.DashboardConfig',
     #'oscar.apps.dashboard.reports.apps.ReportsDashboardConfig',
     #'oscar.apps.dashboard.users.apps.UsersDashboardConfig',
     #'oscar.apps.dashboard.orders.apps.OrdersDashboardConfig',
     #'oscar.apps.dashboard.catalogue.apps.CatalogueDashboardConfig',
     #'oscar.apps.dashboard.offers.apps.OffersDashboardConfig',
     #'oscar.apps.dashboard.partners.apps.PartnersDashboardConfig',
     #'oscar.apps.dashboard.pages.apps.PagesDashboardConfig',
     #'oscar.apps.dashboard.ranges.apps.RangesDashboardConfig',
     #'oscar.apps.dashboard.reviews.apps.ReviewsDashboardConfig',
     #'oscar.apps.dashboard.vouchers.apps.VouchersDashboardConfig',
     #'oscar.apps.dashboard.communications.apps.CommunicationsDashboardConfig',
     #'oscar.apps.dashboard.shipping.apps.ShippingDashboardConfig',
    'blowhorn.address',
    'blowhorn.customer',
    'blowhorn.order',
    'blowhorn.payment',
    'blowhorn.oscar',
    #'oscarapi',
]

# Apps specific for this project go here.
LOCAL_APPS = [
    'website',
    # custom users app
    # 'blowhorn.users.app.UsersConfig',
    # Your stuff: custom apps go here
    'blowhorn.document',
    'blowhorn.vehicle',
    'blowhorn.driver',
    'blowhorn.trip',
    'blowhorn.contract',
    'blowhorn.livetracking',
    'blowhorn.users',
    'blowhorn.company',
    'blowhorn.gaemigrations',  # remove this later
    'blowhorn.apps',
    'blowhorn.apps.customer_app',
    'blowhorn.apps.driver_app',
    'blowhorn.apps.operation_app',
    'blowhorn.apps.integrations',
    'blowhorn.exotel',
    'blowhorn.activity',
    'blowhorn.coupon',
    'blowhorn.integration_apps',
    'blowhorn.changelog',
    'blowhorn.mitra',
    'blowhorn.logapp',
    'blowhorn.shopify',
    'blowhorn.route_optimiser',
    'blowhorn.freshchat',
    'blowhorn.report',
    'blowhorn.wms',
    'blowhorn.catalogue',
    'blowhorn.partner',
    'blowhorn.basket',
    'blowhorn.stock',
    # 'blowhorn.search_indexes'
]

LOCALE_PATHS = [os.path.join(SITE_ROOT, 'blowhorn/locale'), ]

LANGUAGES = (
    ('ar', _('Arabic')),
    ('kn', _('Kannada')),
    ('en_us', _('English')),
)
# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

HOST = env('HOST', default='https://blowhorn.com')
# activates this time zone on admin views . In future we will store
# time zone on user profile  and render them based on user.
ACT_TIME_ZONE = env('ACT_TIME_ZONE', default='Asia/Kolkata')
ACTIVE_COUNTRY_CODE = env('ACTIVE_COUNTRY_CODE', default='+91')
ALT_ACTIVE_COUNTRY_CODE = env('ALT_ACTIVE_COUNTRY_CODE', default='0')
COUNTRY_CODE_A2 = env('COUNTRY_CODE_A2', default='IN')
CEAT_ACCOUNT_ID = env('CEAT_ACCOUNT_ID', default="0012s000002WTYc")
CEAT_CONTACT_ACCOUNT_ID = env('CEAT_CONTACT_ACCOUNT_ID',
                              default="0016F00002GdNbK")
CEAT_RECORD_TYPE_ID = env('CEAT_RECORD_TYPE_ID', default="0122s000000bp7W")
CEAT_CONTACT_RECORD_TYPE_ID = env('CEAT_CONTACT_RECORD_TYPE_ID',
                                  default="0126F000001BWnD")
CEAT_CLIENT_ID = env('CEAT_CLIENT_ID',
                     default='3MVG9Po2PmyYruukkFeRDT_y_MJ_D85SoyIZHZ72OrqllP8S2.BuzovevGgjru4TkZNrEjytbb4XZIzcZLhNA')
CEAT_CLIENT_SECRET_KEY = env('CEAT_CLIENT_SECRET_KEY',
                             default='D5B2141B60532203F1BD697A75013B8625E402BD668D62099D99AA622FF38BCD')
CEAT_TOKEN_REFRESH_URL = env('CEAT_TOKEN_REFRESH_URL',
                             default='https://test.salesforce.com/services/oauth2/token?grant_type=refresh_token&client_id=')
CEAT_CREATE_CONTACT_URL = env('CEAT_CREATE_CONTACT_URL',
                              default='https://ceat--fullcopy.cs75.my.salesforce.com/services/apexrest/createCustomerContact')
CEAT_OREDER_RAML_URL = env('CEAT_OREDER_RAML_URL',
                           default='https://ceat--fullcopy.cs75.my.salesforce.com/services/data/v46.0/composite/tree/Order')
CEAT_ORDER_PUSH = env('CEAT_ORDER_PUSH',
                      default='https://ceat--fullcopy.cs75.my.salesforce.com/services/apexrest/submitSalesOrder')
INVOID_TOKEN = env('INVOID_TOKEN', default='')

BRAND_NAME = env('BRAND_NAME', default='Blowhorn')

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'oscar.apps.basket.middleware.BasketMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'auditlog.middleware.AuditlogMiddleware',
    'blowhorn.templates.admin.middleware.TimezoneMiddleware',
    'audit_log.middleware.JWTAuthMiddleware',
    'audit_log.middleware.UserLoggingMiddleware',
    'blowhorn.common.middleware.RequestMiddleware',
    'corsheaders.middleware.CorsMiddleware'
]

# LOGGING = {
#     'version': 1,
#     'loggers': {
#         'django.db.backends': {
#             'level': 'DEBUG',
#         },
#     },
# }

# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
    'sites': 'blowhorn.contrib.sites.migrations'
}

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)
ENABLE_SLACK_NOTIFICATIONS = env.bool('ENABLE_SLACK_NOTIFICATIONS', False)
ENABLE_TRIP_UPDATION_MESSAGE = env.bool('ENABLE_TRIP_UPDATION_MESSAGE', False)
PRODUCTION_DRIVER_APP = env.bool('PRODUCTION_DRIVER_APP', False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See:
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND',
                    default='django.core.mail.backends.smtp.EmailBackend')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [
    ("""Admin""", 'tech@blowhorn.com'),
]

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# Appending the slash to the end of the URL's
APPEND_SLASH = True

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

IST_TIME_ZONE = ACT_TIME_ZONE

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = env('LANGUAGE_CODE', default='en-us')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = False

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # See:
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See:
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
            str(ROOT_DIR.path('frontend')),
            str(ROOT_DIR.path('wms')),
            str(ROOT_DIR.path('track-parcel')),
            str(ROOT_DIR.path('myfleets'))
        ],
        'OPTIONS': {
            # See:
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See:
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                # Your stuff: custom template context processors go here
                #'oscar.apps.search.context_processors.search_form',
                #'oscar.apps.checkout.context_processors.checkout',
                #'oscar.apps.communication.notifications.context_processors.notifications',
                #'oscar.core.context_processors.metadata',
            ],
            'libraries': {
                'customer_filter': 'blowhorn.templatetags.customer_filter',

            }
        },
    },
]

FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

# See:
# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# Vue project location
FRONTEND_DIR = os.path.join(APPS_DIR, 'frontend')
WMS_DIR = os.path.join(APPS_DIR, 'wms')

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [
    str(APPS_DIR.path('static')),
]

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# PASSWORD STORAGE SETTINGS
# ------------------------------------------------------------------------------
# See
# https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

# PASSWORD VALIDATION
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
# ------------------------------------------------------------------------------

# AUTH_PASSWORD_VALIDATORS = [
#    {
#        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#    },
#    {
#        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
#    },
#    {
#        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
#    },
#    {
#        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
#    },
# ]

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = [
    #'oscar.apps.customer.auth_backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend'
]

# Some really nice defaults
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'optional'
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_USERNAME_REQUIRED = False
SOCIALACCOUNT_EMAIL_VERIFICATION = env.bool(
    'SOCIALACCOUNT_EMAIL_VERIFICATION', 'none')
ACCOUNT_DEFAULT_HTTP_PROTOCOL = env(
    'ACCOUNT_DEFAULT_HTTP_PROTOCOL', default='https')
# Following can be enabled to change limit
# ACCOUNT_LOGIN_ATTEMPTS_LIMIT (=5)
ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT = env(
    'ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT', default=120)

ACCOUNT_ALLOW_REGISTRATION = env.bool(
    'DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)
ACCOUNT_ADAPTER = 'blowhorn.users.adapters.AccountAdapter'
SOCIALACCOUNT_ADAPTER = 'blowhorn.users.adapters.SocialAccountAdapter'
ACCOUNT_SIGNUP_FORM_CLASS = 'blowhorn.users.adapters.SignupForm'
SIGNUP_PASSWORD_ENTER_TWICE = True
ACCOUNT_LOGOUT_ON_GET = True

# https://developers.facebook.com/docs/graph-api/changelog
SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'VERSION': env('FACEBOOK_OAUTH_VERSION', default='v5.0'),
        'APP_ID': env('FACEBOOK_OAUTH_APPID', default='400115703500135')
    }
}

# Custom user app defaults
# Select the correct user model
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = 'account_login'
# LOGIN_URL = 'two_factor:login'

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'

DOMAIN_WHITELIST = env('DOMAIN_WHITELIST',
                       default=['blowhorn.com', 'blowhorn.net'])

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = r'^admin/'

# Your common stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
}

# For Django REST Framework
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'blowhorn.legacy.authentication.LegacyJSONWebTokenAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
}

# Redis

REDIS_PORT = 6379
REDIS_DB = 0
REDIS_HOST = os.environ.get('REDIS_PORT_6379_TCP_ADDR', 'redis')

# Order Processing Constants
ORDER_TYPE_DICT = (
    ('booking', 'Booking'),
    ('subscription', 'Subscription'),
    ('spot', 'Spot'),
    ('shipment', 'Shipment'),
    ('ondemand', 'On-demand'),
    ('dummy', 'Dummy'),
    ('ecomm', 'Ecomm'),
    ('deafcom', 'Deafcom')
)
DEFAULT_ORDER_TYPE = 'booking'
ONDEMAND_ORDER_TYPE = 'ondemand'
SHIPMENT_ORDER_TYPE = 'shipment'
C2C_ORDER_TYPE = 'booking'
SUBSCRIPTION_ORDER_TYPE = 'subscription'
SPOT_ORDER_TYPE = 'spot'
ECOMM_ORDER_TYPE = 'ecomm'
DEAFCOM_ORDER_TYPE = 'deafcom'

DROPOFF_ACTION = 'dropoff'
DELIVERED_ADDRESS_GEOPOINT_FETCH_LIMIT = 10
MINIMUM_ADDRESS_STRING_MATCHING_PERCENTAGE = 75

# Celery
CELERY_ENABLE_UTC = True
timezone = "UTC"
# from celery import Celery
from config.settings.celery import *
# from blowhorn.celery import *
# celery_app = celery_app
# celery_app = Celery()
# from config.settings.celery_settings import *

# configure queues, currently we have only one
task_default_queue = 'high'
task_default_exchange = 'high'
task_default_routing_key = 'high'
CELERY_BROKER_TRANSPORT_OPTIONS = {
    'max_retries': 4,
    'interval_start': 0,
    'interval_step': 0.5,
    'interval_max': 3,
}
CELERY_TASK_ROUTES = {
    # -- HIGH PRIORITY QUEUE -- #
    'blowhorn.driver.tasks.push_notification_to_driver_app': {'queue': 'high'},
    'blowhorn.payment.tasks.send_slack_notification_for_payment': {
        'queue': 'high'},
    'blowhorn.payment.tasks.update_donation_payments': {'queue': 'high'},
    'blowhorn.customer.tasks.push_notification_to_customer': {'queue': 'high'},
    'blowhorn.customer.tasks.process_customer_data_delete_request': {'queue': 'high'},
    'blowhorn.customer.tasks.send_credit_limit_slack_notification': {
        'queue': 'high'},
    'blowhorn.driver.tasks.push_driver_notification': {'queue': 'high'},
    'blowhorn.customer.tasks.broadcast_order_details_to_dashboard': {
        'queue': 'high'},
    'blowhorn.customer.tasks.broadcast_orderline_details_to_dashboard': {
        'queue': 'high'},
    'blowhorn.apps.driver_app.tasks.send_unallocated_orders_slack_notification': {
        'queue': 'high'},
    'blowhorn.common.tasks.send_async_mail': {'queue': 'high'},
    'blowhorn.common.tasks.broadcast_to_firebase': {'queue': 'high'},
    'blowhorn.driver.tasks.generate_driver_payments': {'queue': 'high'},
    'blowhorn.driver.tasks.generate_driver_payment': {'queue': 'high'},
    'blowhorn.order.tasks.send_status_notification': {'queue': 'high'},
    'blowhorn.order.tasks.create_shipment_orders_in_batch': {'queue': 'high'},
    'blowhorn.order.tasks.alert_driver_prior_to_trip': {'queue': 'high'},
    'blowhorn.contract.tasks.generate_subscription_orders': {'queue': 'high'},
    'blowhorn.trip.tasks.calculate_margin': {'queue': 'high'},
    'blowhorn.customer.tasks.regenerate_customer_invoice': {'queue': 'high'},
    'blowhorn.customer.tasks.generate_customer_reports': {'queue': 'high'},
    'blowhorn.customer.tasks.mail_customer_reports': {'queue': 'high'},
    'blowhorn.contract.tasks.generate_expected_margin': {'queue': 'high'},
    'blowhorn.driver.tasks.deactivate_driver_payments': {'queue': 'high'},
    'blowhorn.customer.tasks.close_invoices': {'queue': 'high'},
    'blowhorn.drivers.tasks.close_unsettled_payment': {'queue': 'high'},
    'blowhorn.customer.tasks.upload_to_gst_portal_automation': {'queue': 'high'},
    'blowhorn.customer.tasks.send_invoices_to_customers': {'queue': 'high'},
    'blowhorn.customer.tasks.send_credit_breach_mails': {'queue': 'high'},
    'blowhorn.customer.tasks.update_invoices_status_with_credit_notes': {'queue': 'high'},
    'blowhorn.payment.tasks.send_cod_report': {'queue': 'high'},

    # -- NORMAL PRIORITY QUEUE -- #
    'blowhorn.driver.tasks.push_notification_to_driver': {'queue': 'high'},
    'blowhorn.common.tasks.send_sms': {'queue': 'high'},
    'blowhorn.common.tasks.update_cms': {'queue': 'high'},
    'blowhorn.common.tasks.send_zippped_consignment_notes': {'queue': 'high'},
    'blowhorn.driver.tasks.mailer': {'queue': 'high'},
    'blowhorn.order.tasks.broadcast_booking_updates_admin': {'queue': 'high'},
    'blowhorn.order.tasks.broadcast_spot_order_updates_admin': {'queue': 'high'},
    'blowhorn.order.tasks.broadcast_booking_updates_customer': {
        'queue': 'high'},
    'blowhorn.order.tasks.get_geopoint_from_delivered_addresses': {
        'queue': 'high'},
    'blowhorn.order.tasks.notify': {'queue': 'high'},
    'blowhorn.order.tasks.create_order_on_ceat_system': {'queue': 'high'},
    'blowhorn.order.tasks.initiate_paytm_refund': {'queue': 'high'},
    'blowhorn.order.tasks.send_kiosk_terms_of_service': {'queue': 'high'},
    'blowhorn.order.tasks.send_restore_notification_to_driver_app': {
        'queue': 'high'},
    'blowhorn.order.tasks.send_mail': {'queue': 'high'},
    'blowhorn.users.tasks.send_notification_mail': {'queue': 'high'},
    'blowhorn.order.tasks.send_c2c_slack_notification': {'queue': 'high'},
    'blowhorn.order.tasks.send_c2c_payment_notification': {'queue': 'high'},
    'blowhorn.order.tasks.push_customer_feedback_to_slack': {'queue': 'high'},
    'blowhorn.order.tasks.update_order_to_expired': {'queue': 'high'},
    'blowhorn.order.tasks.update_call_logs': {'queue': 'high'},
    'blowhorn.order.tasks.send_order_pod_webhooks': {'queue': 'high'},
    'blowhorn.order.tasks.withdraw_amount_from_paytm': {'queue': 'high'},
    'blowhorn.driver.tasks.delete_driver_activity_from_firebase': {
        'queue': 'high'},
    'blowhorn.trip.tasks.mark_inactive_trips': {'queue': 'high'},
    'blowhorn.trip.tasks.generate_consignments': {'queue': 'high'},
    'blowhorn.driver.tasks.clear_driver_activity': {'queue': 'high'},
    'blowhorn.customer.tasks.generate_customer_invoices': {'queue': 'high'},
    'blowhorn.customer.tasks.create_customer_invoice_v2': {'queue': 'high'},
    'blowhorn.driver.tasks.driver_gps_distance': {'queue': 'high'},
    'blowhorn.contract.tasks.deactivate_contract': {'queue': 'high'},
    'blowhorn.contract.tasks.make_reminder_calls': {'queue': 'high'},
    'blowhorn.customer.tasks.expire_invoices': {'queue': 'high'},
    'blowhorn.customer.tasks.assign_revenue_month_to_invoices': {
        'queue': 'high'},
    'blowhorn.driver.tasks.create_leads': {'queue': 'high'},

    # -- LOW PRIORITY QUEUE -- #
    'blowhorn.integration_apps.invoid.tasks.send_document_for_verification_to_invoid': {
        'queue': 'high'},
    'blowhorn.order.tasks.send_slack_notification': {'queue': 'high'},
    'blowhorn.payment.tasks.broadcast_payment': {'queue': 'high'},
    'blowhorn.payment.tasks.update_paytm_transaction_status': {'queue': 'high'},
    'blowhorn.customer.tasks.send_transaction_update_to_customer': {
        'queue': 'high'},
    'blowhorn.customer.tasks.send_donation_email': {'queue': 'high'},
    'blowhorn.common.tasks.delete_file': {'queue': 'high'},
    'blowhorn.apps.operation_app.v3.services.tasks.notify_user': {
        'queue': 'high'},
    'blowhorn.apps.operation_app.v5.tasks.push_notification_to_cosmos': {
        'queue': 'high'},
    'blowhorn.apps.operation_app.v5.tasks.expire_notifications': {
        'queue': 'high'},
    'blowhorn.apps.operation_app.v5.tasks.send_invoice_action_notification': {
        'queue': 'high'},
    'blowhorn.customer.tasks.send_invitation': {'queue': 'high'},
    'blowhorn.customer.tasks.booking_order_payment_due_notifications': {
        'queue': 'high'},
    'blowhorn.customer.tasks.publish_location_data': {'queue': 'activity'},
    'blowhorn.contract.tasks.notify_contract_status_change': {'queue': 'high'},
    'blowhorn.driver.tasks.deactivate_drivers': {'queue': 'high'},
    'blowhorn.document.tasks.send_expiry_alert': {'queue': 'high'},
    'blowhorn.contract.tasks.calculate_duty_hrs': {'queue': 'high'},
    'blowhorn.shopify.tasks.sync_customer_orders': {'queue': 'high'},
    'blowhorn.shopify.tasks.push_sync_notif_to_dashboard': {'queue': 'high'},
    'blowhorn.wms.tasks.create_asn_order': {'queue': 'high'},

}

# Sensible settings for celery
task_always_eager = False
task_acks_late = True
task_publish_retry = True
worker_disable_rate_limits = False

# By default we will ignore result
# If you want to see results and try out tasks interactively,
# change it to False
# Or change this setting on tasks level
task_ignore_result = False # change
CELERY_SEND_TASK_ERROR_EMAILS = False
result_expires = 600
CELERY_EVENT_MAX_RETRIES = 3

# Set redis as celery result backend
result_backend = 'redis://%s:%d/%d' % (REDIS_HOST, REDIS_PORT, REDIS_DB)
redis_max_connections = 1

# Don't use pickle as serializer, json is much safer
task_serializer = "json"
accept_content = ['application/json']

worker_hijack_root_logger = False
worker_prefetch_multiplier = 1
worker_max_tasks_per_child = 1000

# Celery Beat
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'

# RaabitMQ configs
RABBIT_HOSTNAME = os.environ.get('RABBIT_PORT_5672_TCP', 'rabbit')


if RABBIT_HOSTNAME.startswith('tcp://'):
    RABBIT_HOSTNAME = RABBIT_HOSTNAME.split('//')[1]

broker_url = os.environ.get('CELERY_BROKER_URL', '')
if not broker_url:
    broker_url = 'amqp://{user}:{password}@{hostname}/{vhost}/'.format(
        user=os.environ.get('RABBIT_ENV_USER', 'admin'),
        password=os.environ.get('RABBIT_ENV_RABBITMQ_PASS', 'mypass'),
        hostname=RABBIT_HOSTNAME,
        vhost=os.environ.get('RABBIT_ENV_VHOST', ''))

# We don't want to have dead connections stored on rabbitmq, so we have to
# negotiate using heartbeats
CELERY_BROKER_HEARTBEAT = 30
if not broker_url.endswith("?heartbeat=" + str(CELERY_BROKER_HEARTBEAT)):
    broker_url += "?heartbeat=" + str(CELERY_BROKER_HEARTBEAT)

CELERY_BROKER_POOL_LIMIT = 1
CELERY_BROKER_CONNECTION_TIMEOUT = 10
CELERY_RESULT_PERSISTENT=False

# Common shipment items
C2C_ITEMS = [
    'Sofas',
    'TV',
    'Refrigerator',
    'Chairs',
    'Washing Machine',
    'Dining Table',
    'Office Furniture'
]

ALLOWED_HOSTS = ['*']
"""
Django Suit configuration example
SUIT_CONFIG = {
   # header
    'ADMIN_NAME': 'Blowhorn Admin',
   # 'HEADER_DATE_FORMAT': 'l, j. F Y',
   # 'HEADER_TIME_FORMAT': 'H:i',

   # forms
   # 'SHOW_REQUIRED_ASTERISK': True,  # Default True
   # 'CONFIRM_UNSAVED_CHANGES': True, # Default True

   # menu
   # Refer about global search field:
   # #search-url
   # http://django-suit.readthedocs.io/en/develop/configuration.html
   'SEARCH_URL': '',
   # 'MENU_ICONS': {
   #    'sites': 'icon-leaf',
   #    'auth': 'icon-lock',
   # },
   # 'MENU_OPEN_FIRST_CHILD': True, # Default True
   # 'MENU_EXCLUDE': ('auth.group',),
   # 'MENU': (
   #     'sites',
   #     {'app': 'auth', 'icon':'icon-lock', 'models': ('user', 'group')},
   #     {'label': 'Settings', 'icon':'icon-cog',
              'models': ('auth.user', 'auth.group')},
   #     {'label': 'Support', 'icon':'icon-question-sign',
              'url': '/support/'},
   # ),

   # misc
   # 'LIST_PER_PAGE': 15
}
"""

GOOGLE_GEOCODING_DISTANCEMATRIX_API_KEY = env(
    'GOOGLE_GEOCODING_DISTANCEMATRIX_API_KEY',
    default='AIzaSyANXxk2T9dBAjlJnVkP-cu1-4Obr5HWVIM')

GEOCODE_BASE_URL = 'https://maps.google.com/maps/api/geocode/json'

MAP_CENTER_LOCATION_NAME = env('MAP_CENTER_LOCATION_NAME',
                               default='Bengaluru')
MAP_CENTER_LOCATION_POINTS = env('MAP_CENTER_LOCATION_POINTS',
                                 default=[12.9538477, 77.3507366])
GOOGLE_MAP_AUTOCOMPLETE_CONFIG = (
    "GooglePlaceAutocompleteOptions", {
        "componentRestrictions": {
            "country": COUNTRY_CODE_A2.lower()
        }
    }
)
MAP_WIDGETS = {
    "GooglePointFieldWidget": (
        ("zoom", 15),
        ("mapCenterLocationName", "Bengaluru"),
        ("GooglePlaceAutocompleteOptions", {
            'componentRestrictions': {'country': COUNTRY_CODE_A2.lower()}}),
        ("markerFitZoom", 12),
        ("mapCenterLocationName", MAP_CENTER_LOCATION_NAME),
        GOOGLE_MAP_AUTOCOMPLETE_CONFIG,
    ),
    "GooglePolygonFieldWidget": (
        ("zoom", 15),
        ("mapCenterLocationName", "Bengaluru"),
        ("mapCenterLocation", [12.9538477, 77.3507366]),
        ("GooglePlaceAutocompleteOptions", {
            'componentRestrictions': {'country': COUNTRY_CODE_A2.lower()}}),
        ("markerFitZoom", 12),
        ("mapCenterLocationName", MAP_CENTER_LOCATION_NAME),
        ("mapCenterLocation", MAP_CENTER_LOCATION_POINTS),
        GOOGLE_MAP_AUTOCOMPLETE_CONFIG,
        ("polyOptions", {
            "strokeWeight": 0,
            "fillOpacity": 0.45,
            "editable": True,
            "draggable": True
        })
    ),
    "GooglePolylineFieldWidget": (
        ("zoom", 15),
        ("markerFitZoom", 12),
        ("mapCenterLocationName", MAP_CENTER_LOCATION_NAME),
        ("mapCenterLocation", MAP_CENTER_LOCATION_POINTS),
        ("polyOptions", {
            "strokeWeight": 0,
            "fillOpacity": 0.45,
            "strokeColor": "#FF0000",
            "editable": False,
            "draggable": False
        })
    )
}

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=365)
}

C2C_CONSTANTS = {
    'PRICE_WEB_TEXT': '₹300 for first thirty mins, prorated after',
    'PRICE_MOBILE_TEXT': '₹300 for 30 Mins',
    'PRICE_MOBILE_DETAILS_TEXT': 'Prorated after',
    'APP_SHARE_BODY_TEXT': env('APP_SHARE_BODY_TEXT',
                               default="Hi! Now move goods hassle free with blowhorn. Download now : https://blowhorn.com/app"),
    'APP_SHARE_SUB_TEXT': env('APP_SHARE_SUB_TEXT',
                              default="Moving goods? Here's a tip, blowhorn it!"),
}

CUSTOMER_CANCELLATION_REASONS = {
    'Bigger vehicle': 'I need a bigger Vehicle',
    'Packing': 'I need packing',
    'Pricing': 'Service is Expensive',
    'Dismantling': 'I need dismantling',
    'Change in Plan': 'Change in Plan',
    'Checking website': 'Checking Website',
    'Others': 'Others'
}

DEFAULT_COUNTRY = {
    'isd_code': '91',
    'name': 'India',
    'country_code': 'IN'
}

# OTP will be valid until this time captured in seconds.
OTP_CONFIG = {
    'step': 5 * 60,  # Time in seconds
    'digits': 4,
}
RAZORPAY_PATH = 'https://api.razorpay.com/v1/payments'

# All providers are defined in common/messaging/providers.
DEFAULT_SMS_PROVIDER = env('DEFAULT_SMS_PROVIDER', default='exotel')
DEFAULT_PAYMENT_GATEWAY = env('DEFAULT_PAYMENT_GATEWAY', default='razorpay')
CASH_FREE_PAYMENT_GATEWAY = env('CASH_FREE_PAYMENT_GATEWAY', default='cashfree')
CEAT_API_KEY = env('CEAT_API_KEY', default='')
PAYFAST_PAYMENT_GATEWAY = 'payfast'

# For now,used to handle email ids of guest customers and drivers, the
# below will act as hostname.
DEFAULT_DOMAINS = {
    'customer': 'customer.{}.net'.format(BRAND_NAME.lower()),
    'driver': 'driver.{}.net'.format(BRAND_NAME.lower()),
    'vendor': 'vendor.{}.net'.format(BRAND_NAME.lower()),
}

SMS_KEYS = {
    'debug_true': 'jTf4/xSkWjN',
    'debug_false': 'gvk8Opckty2',
}

DRIVER_DOMAIN_REGEX = r'@driver.blowhorn.net$'
# OSCAR_DYNAMIC_CLASS_LOADER = 'blowhorn.common.oscar_loading.get_class'
OSCAR_DEFAULT_CURRENCY = env('OSCAR_DEFAULT_CURRENCY', default='INR')
CURRENCY_WITHOUT_SYMBOL = {
    'ZAR': 'R'
}
PAYMENT_METHODS = env('PAYMENT_METHODS', default=['paytm', 'razorpay'])

MAPS_OPERATION_RETRY_COUNT = 5
MAPS_OPERATION_BACKOFF_TIME_S = 0.3

EMAIL_REGEX = '^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'

FIREBASE_ACTIVE_DRIVERS_URL = 'active_drivers/'
FIREBASE_ADMIN_AUTH_EMAIL = 'admin@blowhorn.com'
FIREBASE_ADMIN_NEW_BOOKING_CHANNEL = "admin-bookings/new-booking/v1/"
FIREBASE_ADMIN_NEW_SPOT_ORDER_CHANNEL = "admin-spot/new-order/v1"
FIREBASE_ADMIN_NEW_PROMOTION_BOOKING_CHANNEL = 'admin-promotion-bookings/new-booking'
FIREBASE_BOOKING_TASK_STATUS_URL = 'admin-booking-task-status/task-status'
FIREBASE_VIEW_USER_PROFILE_CHANNEL = "view-user-profile/view-profile"
FIREBASE_AUTH_TOKEN_EXPIRY_MINUTES = 60

CUSTOMER_XIAOMI = 'Xiaomi'
ORDER_CREATION_PAST_TIME_TOLERANCE = 180

# dont change these, used in Stage Transportation app
DELIVERY_ASSOCIATE = "Delivery_Associate"
HUB_ASSOCIATE = "Hub_Associate"
WAREHOUSE_ASSOCIATE = "Warehouse_Associate"

DELIVERY_ASSOCIATE_DISPLAY = "Delivery Associate"
HUB_ASSOCIATE_DISPLAY = "Hub Associate"
WAREHOUSE_ASSOCIATE_DISPLAY = "Warehouse Associate"

USER_CLIENT_TYPE_IOS = 'iOS'
USER_CLIENT_TYPE_ANDROID = 'Android'
USER_CLIENT_TYPES = [
    USER_CLIENT_TYPE_IOS,
    USER_CLIENT_TYPE_ANDROID,
]

C2C_PRICE_WEB_TEXT = u'₹300 for first sixty mins, prorated after that'
C2C_PRICE_MOBILE_TEXT = u'₹300 for 60 Mins'
C2C_PRICE_MOBILE_DETAILS_TEXT = u'Prorated after'

VEHICLE_CLASSES_MAP = {
    'Mini-Truck': 'Mini-Truck',
    'Pickup': 'Truck',
    'Light-Truck': 'Cargo-Van'
}

DRIVER_STATUSES = [
    'idle',
    'occupied',
    'b2b'
]

DRIVER_TYPES = {
    'idle': '/static/website/images/truck_idle.png',
    'occupied': "/static/website/images/truck_busy.png",
    'b2b': "/static/website/images/mini_truck_b2b.png"
}
DEFAULT_DRIVER_TYPE = 'NA'
MAX_DRIVER_RESPONSE_TIME_SECONDS = 20

LABOUR_COST_LIGHT = 400
LABOUR_COST_MEDIUM = 400
LABOUR_COST_HEAVY = 400

NO_REPLY = 'no-reply@blowhorn.com'

# CACHING
# ------------------------------------------------------------------------------

REDIS_LOCATION = '{0}/{1}'.format(env('REDIS_URL',
                                      default='redis://redis:6379'), 0)

# Heroku URL does not pass the DB number, so we parse it in

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': REDIS_LOCATION,
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'IGNORE_EXCEPTIONS': True,  # mimics memcache behavior.
            # http://niwinz.github.io/django-redis/latest/#_memcached_exceptions_behavior
        }
    }
}

# ADMIN CUSTOMIZATION
DEFAULT_BUILD = 'blowhorn'
WEBSITE_BUILD = env('WEBSITE_BUILD',
                    default=DEFAULT_BUILD)  # it should be website folder name
BUILD_FLAVOUR = env('BUILD_FLAVOUR',
                    default='prod')  # it defines dev or prod build
SITE_HEADER = env('SITE_HEADER', default='Blowhorn Administration')
SITE_TITLE = env('SITE_TITLE', default='Blowhorn Admin Site')
INDEX_TITLE = env('INDEX_TITLE', default='Welcome to blowhorn admin site')
HOMEPAGE_URL = env('HOMEPAGE_URL', default='/')
WEBSITE_FONT = env('WEBSITE_FONT', default='Heebo')

JET_DEFAULT_THEME = env('JET_DEFAULT_THEME',
                        default='blowhorn')  # theme folder name
THEME_BUTTON_COLOR = env('THEME_BUTTON_COLOR',
                         default='87E9FF')  # color of the theme's button in user menu
JET_THEME_TITLE = env('JET_DEFAULT_THEME',
                      default=JET_DEFAULT_THEME)  # theme title
JET_THEMES = [
    {
        'theme': JET_DEFAULT_THEME,
        'color': '#%s' % THEME_BUTTON_COLOR,
        'title': JET_THEME_TITLE
    },
    {
        'theme': 'default',
        'color': '#47bac1',
        'title': 'Default'
    },
    {
        'theme': 'green',
        'color': '#44b78b',
        'title': 'Green'
    },
    {
        'theme': 'light-green',
        'color': '#2faa60',
        'title': 'Light Green'
    },
    {
        'theme': 'light-violet',
        'color': '#a464c4',
        'title': 'Light Violet'
    },
    {
        'theme': 'light-blue',
        'color': '#5EADDE',
        'title': 'Light Blue'
    },
    {
        'theme': 'light-gray',
        'color': '#222',
        'title': 'Light Gray'
    }
]

JET_CHANGE_FORM_SIBLING_LINKS = False
# JET_SIDE_MENU_COMPACT = True

JET_SIDE_MENU_ITEMS = [  # A list of application or custom item dicts
    {'label': _('Supply'), 'items': [
        {'name': 'driver.driver', 'label': 'Drivers'},
        #        {'name': 'pages.page', 'label': _('Static page')},
        {'name': 'vehicle.vehicle'},
        {'name': 'contract.buyrate'},
        {'name': 'document.documentevent'},
        #        {'label': _('Analytics'), 'url': 'http://example.com', 'url_blank': True},
    ]},
    {'label': _('Sales'), 'items': [
        {'name': 'customer.businesscustomer'},
        {'name': 'contract.contractsale'},
        {'name': 'contract.sellrate'},
        {'name': 'customer.customerinvoice'},
        {'name': 'customer.invoicesummary'},
        {'name': 'customer.customerledger'},
    ]},
    {'label': _('Operations'), 'items': [
        {'label': _('Live Tracking'), 'url': '/admin/livetracking'},
        {'name': 'contract.contractoperation'},
        {'name': 'contract.buyrate'},
        {'label': _('Resource Allocations'),
         'url': '/admin/resourceallocation'},
        {'name': 'driver.driverpayment'},
        {'name': 'driver.driverpaymentadjustment'},
        {'name': 'driver.drivercashpaid'},
        {'name': 'driver.nonoperationpayment'},
        {'name': 'driver.driverpaymentsettlementview'},
        {'name': 'driver.adjustmentapproval'},
    ]},
    {'label': _('Orders/Trips'), 'items': [
        {'name': 'order.bookingorder'},
        {'name': 'order.shipmentorder'},
        {'name': 'order.fixedorder'},
        {'name': 'order.spotorder'},
        {'name': 'trip.trip'},
        {'name': 'driver.driverratingdetails'},
    ]},
    {'label': _('Utilities'), 'items': [
        {'label': 'Departure Sheet', 'url': '/admin/departuresheet'},
        {'label': 'Heatmap', 'url': '/admin/heatmap'},
        {'label': 'Shipment', 'url': '/admin/shipment'},
        {'label': 'Kiosk', 'url': '/admin/kiosk'},
    ]},
    {'label': _('Finance'), 'items': [
        {'name': 'driver.driverbalance'},
        {'name': 'driver.driverledger'},
        {'name': 'driver.driverledgerbatch'},
        {'name': 'customer.customerinvoice'},
        {'name': 'customer.customerreceivable'},
        {'name': 'customer.openingbalance'},
        {'name': 'customer.collectionssummary'},
        {'name': 'order.FlashSaleSettlementView'},
    ]},
    {'label': _('Store Front Order'), 'items': [
        {'name': 'order.storefrontorder'},
        {'name': 'order.flashsalesettlementview'},
        {'name': 'catalogue.product'},
        {'name': 'order.flashsalebanner'},
    ]},
    # {'label': _('Change Logs'), 'items': [
    #     {'label': 'Admin Site', 'url':'/admin/changelog/admin'},
    #     {'label': 'Customer App', 'url':'/admin/changelog/customerapp'},
    #     {'label': 'Fleet Dashboard', 'url':'/admin/changelog/fleetdashboard'},
    #     {'label': 'MIS', 'url':'/admin/changelog/mis'},
    #     {'label': 'Partner App', 'url':'/admin/changelog/partnerapp'},
    #     {'label': 'Platform', 'url':'/admin/changelog/platform'},
    #     {'label': 'Web App', 'url':'/admin/changelog/webapp'},
    #
    # ]},
    #    {'app_label': 'banners', 'items': [
    #        {'name': 'banner'},
    #        {'name': 'bannertype'},
    #    ]},
]

IMPORT_EXPORT_USE_TRANSACTIONS = True

# Batch user email
CRON_JOB_DEFAULT_USER = env('CRON_JOB_DEFAULT_USER',
                            default='celery@blowhorn.com')

MEDIA_UPLOAD_STRUCTURE = "uploads/documents/{module_name}/{instance_handle}/{doc_code}/{file_name}"

DATETIME_FORMAT = 'd-m-Y H:i'

TIME_FORMAT = '%d %b, %Y %I:%M %p'
DATE_FORMATS = '%d %b, %Y'

DATE_FORMAT = 'j N Y'

DATE_INPUT_FORMATS = ('%d-%m-%Y', '%Y-%m-%d',)

ADMIN_DATETIME_FORMAT = "%d-%m-%Y %H:%M"
ADMIN_DATE_FORMAT = "%d-%m-%Y"
ADMIN_TIME_FORMAT = "%H:%M"

APP_DATETIME_FORMAT = "%b %d, %Y %H:%M"
APP_DATE_FORMAT = "%b %d, %Y"

COMPARE_DATETIME_FORMAT = "%Y-%m-%d %H:%M"
COMPARE_DATE_FORMAT = "%Y-%m-%d"
FILE_TIMESTAMP_FORMAT = "%Y-%m-%d_%H-%M-%s"

SHIPMENT_ORDER_SLACK_URL = env("DEFAULT_SLACK_URL",
                               default="https://hooks.slack.com/services/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")

SHIPMENT_ORDER_SLACK_CHANNEL = env("DEFAULT_SLACK_CHANNEL",
                                   default='#shipment-orders')

SHIPMENT_ORDER_TRACK_URL = 'http://enterprise.blowhorn.com/admin/order/shipmentorder'

C2C_BOOKING_ORDER_SLACK_URL = env("DEFAULT_SLACK_URL",
                                  default="https://hooks.slack.com/services/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")

C2C_ORDER_TRACK_URL = env('C2C_ORDER_TRACK_URL',
                          default='http://enterprise.blowhorn.com/admin/order/bookingorder')

C2C_PAYMENT_SLACK_URL = env("DEFAULT_SLACK_URL",
                            default="https://hooks.slack.com/services/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")

C2C_PAYMENT_SLACK_CHANNEL = env("DEFAULT_SLACK_CHANNEL", default='#payments')

C2C_CUSTOMER_FEEDBACK_SLACK_URL = env("DEFAULT_SLACK_URL",
                                      default="https://hooks.slack.com/services/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")

C2C_CUSTOMER_FEEDBACK_SLACK_CHANNEL = env("DEFAULT_SLACK_CHANNEL",
                                          default='#c2c_internal')

CREDIT_CROSS_CHANNEL = '#credit-cross-customer'

TRIP_REJECTION_SLACK_URL = env("DEFAULT_SLACK_URL",
                               default='https://hooks.slack.com/services/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')

TRIP_REJECTION_SLACK_CHANNEL = env("DEFAULT_SLACK_CHANNEL",
                                   default='#unallocated-orders')

TRIP_ACCEPTANCE_HOUR_BEFORE = 'TRIP_ACCEPTANCE_HOUR_BEFORE'

TRIP_ACCEPTANCE_HOUR_AFTER = 'TRIP_ACCEPTANCE_HOUR_AFTER'

HEADER = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'OPTIONS'
}

SENDGRID_API_KEY = env('SENDGRID_API_KEY', default=None)
SENDGRID_SANDBOX_MODE_IN_DEBUG = False

# Used in spot order past & future pickup datetime validation
ALLOWED_FUTURE_DAYS = 7
MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME = env(
    'MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME', default=7)

DEAFCOM_ALLOWED_FUTURE_DAYS = 7
DEAFCOM_MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME = env(
    'DEAFCOM_MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME', default=7)

# Freshdesk configs
FRESHDESK_API_KEY = env("FRESHDESK_API_KEY", default="4oRvTWDfxlDwF38UxWFp")
FRESHDESK_PASSWORD = env("FRESHDESK_PASSWORD", default="catbus123")
FRESHDESK_DOMAIN = env("FRESHDESK_DOMAIN", default="blowhorn")
FRESHDESK_EMAIL = "shoutout@blowhorn.com"
FRESHDESK_BLOWHORN_HOST = "https://%s.freshdesk.com" % FRESHDESK_DOMAIN
FRESHDESK_TICKET_CREATION_END_POINT = "/api/v2/tickets"
FRESHCHAT_WEB_CONFIG = {
    'api_key': env('FRESHCHAT_KEY_WEB',
                   default='e5f1447a-3fc3-49d1-a52c-2d19468f6c76'),
    'app_name': env('FRESHCHAT_APP_NAME', default='Blowhorn Support'),
    'app_logo': env('FRESHCHAT_APP_LOGO',
                    default='https://blowhorn.com/static/assets/Assets/Logo.png'),
    'background_color': env('FRESHCHAT_WIDGET_BACKGROUND', default='#00d2f6'),
    'foreground_color': env('FRESHCHAT_WIDGET_FOREGROUND', default='#FFFFFF')
}
CLEVERTAP_ACCOUNT_ID = env('CLEVERTAP_ACCOUNT_ID', default='TEST-W86-97R-884Z')

# Betterplace configuration
BETTERPLACE_API_KEY_TEST = "xWV/Wi53NiFe+TITwVMpKAhjDJ65Lz1I8OSbZ2MrMZx2HWoHLzaFChvFF8LAzSQECsF8bR3pXjJaCxntggH0Xg=="
BETTERPLACE_ENDPOINT_CREATE_PROFILE_TEST = "https://testportal.betterplace.co.in/VishwasAPI/api/public/v2/createProfile/employee"
BETTERPLACE_ENDPOINT_VERIFICATION_STATUS_TEST = "https://testportal.betterplace.co.in/VishwasAPI/api/public/v2/getVerificationStatusExt/employee"
BETTERPLACE_BLOWHORNS_ENDPOINT_API_KEY_TEST = "zb4DRMXJMojQfnEePkP8hz9B2e8jtA0smkWhpGqqeR0"
DVARA_LOAN = "https://test.dvaramoney.com/partner/v1/initiateSparkLoan"

FLASH_SALE_API_KEY = env("FLASH_SALE_API_KEY",
                         default="5z0Dar4yEVlGce8B23uwNb6QjUM1WC")
DVARA_API_KEY = env("DVARA_API_KEY",
                    default="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjQ3MzYzMDg2NTcsImRhdGEiOnsiY29tcGFueV9pZCI6ImIxZjI5YmQyLWI1YjktNTYwYS04ZDEwLTYzYTdmNGI5MDBiOCIsImNvbXBhbnlfY29kZSI6IkRDQkgwMDEifSwiaWF0IjoxNTgyNzA4NjU3fQ.f_v0JeleIAPpV7XxOCWtJG-lot1JSubAvMCroCk_Zx4")
DVARA_INITIATE_LOAN = env("DVARA_INITIATE_LOAN",
                          default=DVARA_LOAN)
BETTERPLACE_API_KEY = env("BETTERPLACE_API_KEY",
                          default=BETTERPLACE_API_KEY_TEST)
BETTERPLACE_ENDPOINT_CREATE_PROFILE = env("BETTERPLACE_ENDPOINT_CREATE_PROFILE",
                                          default=BETTERPLACE_ENDPOINT_CREATE_PROFILE_TEST)
BETTERPLACE_ENDPOINT_VERIFICATION_STATUS = env(
    "BETTERPLACE_ENDPOINT_VERIFICATION_STATUS",
    default=BETTERPLACE_ENDPOINT_VERIFICATION_STATUS_TEST)
BETTERPLACE_BLOWHORNS_ENDPOINT_API_KEY = env(
    "BETTERPLACE_BLOWHORNS_ENDPOINT_API_KEY",
    default=BETTERPLACE_BLOWHORNS_ENDPOINT_API_KEY_TEST)

PARTNER_APPLY_MAPPING = {
    "Dvara": "/api/integrations/v1/finance/driverinformation/",
    "Symbo": "/api/integrations/v1/eligibility/symbo/"
}

DVARA_WEB_VIEW = env('DVARA_WEB_VIEW',
                     default="https://partner.dvaramoney.com/universal?partner_user_id=%s&api_key=") + DVARA_API_KEY

PARTNER_WEB_VIEW_MAPPING = {
    "Dvara": DVARA_WEB_VIEW
}

PARTNER_ELIGILIBITY_URL = {
    "Dvara": "/api/drivers/drivereligibility",
    "PerkFinance": "/api/drivers/drivereligibility",
    "Symbo": "/api/integrations/v1/eligibility/symbo"
}
# DATA_UPLOAD_MAX_MEMORY_SIZE = 5242880

# Slack general settings
SLACK_URL = env(
    "DEFAULT_SLACK_URL",
    default="https://hooks.slack.com/services/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")

SLACK_CHANNELS = {
    'PAYMENTS': '#tech-testing',
    'EMAIL_FAILURE': '#email-failure'
}

# THUMBNAIL FOR SLACK
THUMBNAIL_LINK = 'https://blowhorn.com/static/website/images/blowhorn-logo.png'

BLOWHORN_SUPPORT_NUMBER = env('COMPANY_SUPPORT_NUMBER', default='+918880900300')
COMPANY_SUPPORT_EMAIL = env('COMPANY_SUPPORT_EMAIL',
                            default='shoutout@blowhorn.com')
TERMS_OF_SERVICE_URL = env('TERMS_OF_SERVICE_URL', default='/termsofservice')
SOCIAL_LINKS = {
    'facebookId': env('FACEBOOK_ID', default='Blowhornlog'),
    'twitterId': env('TWITTER_ID', default='blowhornlog'),
    'linkedinId': env('LINKEDIN_ID', default='company/blowhorn'),
}

DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000000
MAX_ALLOWED_WALLET_RECHARGE_AMOUNT = env('MAX_ALLOWED_WALLET_RECHARGE_AMOUNT',
                                         default=10000)

# App Download links
IOS_APP_LINK = env('IOS_APP_LINK',
                   default='https://itunes.apple.com/us/app/blowhorn-mini-trucks-on-hire/id1251408316?mt=8')
ANDROID_APP_LINK = env('ANDROID_APP_LINK',
                       default='https://play.google.com/store/apps/details?id=net.blowhorn.app')

# TODO need to be changed to playstore links
DRIVER_APP_ANDROID_LINK = 'https://play.google.com/store/apps/details?id=net.blowhorn.driverapp'
DRIVER_APP_IOS_LINK = 'https://play.google.com/apps/testing/net.blowhorn.driverapp'

DRIVER_APP_ANDROID_LINK_TEST = 'https://s3.us-east-2.amazonaws.com/test-enterprise-blowhorn/Apps/Driver/Blowhorn-Driver.apk'
DRIVER_APP_IOS_LINK_TEST = 'https://s3.us-east-2.amazonaws.com/test-enterprise-blowhorn/Apps/Driver/Blowhorn-Driver.apk'

# graphene-django schema mapping
GRAPHENE = {
    'SCHEMA': 'blowhorn.schema.schema'
}

# DEFAULT_INVOICE_MAIL = 'invoice@blowhorn.net'
DEFAULT_INVOICE_MAIL = env('DJANGO_DEFAULT_INVOICE_MAIL',
                           default='Blowhorn Invoice <invoice@blowhorn.com>')

# TODO add this in configuration
ALLOW_PAYMENT_SELECTION = False

# Django Organization
INVITATION_BACKEND = 'blowhorn.customer.backends.defaults.InvitationBackend'
REGISTRATION_BACKEND = 'blowhorn.customer.backends.defaults.RegistrationBackend'

# ALLOW_PAYMENT_SELECTION = True

# Interval at which activity logs will be synced to MongoDB.
DRIVER_ACTIVITY_LOG_SYNC_INTERVAL = env(
    'DRIVER_ACTIVITY_LOG_SYNC_INTERVAL', default=30)

BLOWHORN_API_KEY = env('BLOWHORN_API_KEY',
                       default='EMNr1oXZTF6a9PDel8vquH0xhIzcn7')

DRIVER_ACTIVITY_PURGE_INTERVAL = env(
    'DRIVER_ACTIVITY_PURGE_INTERVAL', default=86400
)

DATABASE_ROUTERS = ['blowhorn.routers.mongo.MongoRouter']

# GPS Event Ingestion parameters for Instakart
INSTAKART_DATA_INGESTION_SOURCE = env(
    'INSTAKART_DATA_INGESTION_SOURCE', default='testing')
INSTAKART_DATA_INGESTION_EVENT = env(
    'INSTAKART_DATA_INGESTION_EVENT', default='vehicle_event')

EXTENSION_MAX_UNIQUE_QUERY_ATTEMPTS = 1000

USE_THOUSAND_SEPARATOR = True
NUMBER_GROUPING = 3

# MQTT Broker config:
MQTT_BROKER_HOST = env('MQTT_BROKER_HOST', default='broker.hivemq.com')
MQTT_BROKER_PORT = env('MQTT_BROKER_PORT', default=1883)
MQTT_BROKER_USERNAME = env('MQTT_BROKER_USERNAME', default=None)
MQTT_BROKER_PASSWORD = env('MQTT_BROKER_PASSWORD', default=None)
MQTT_KEEP_ALIVE_INTERVAL = env.int('MQTT_KEEP_ALIVE_INTERVAL', default=60)

EMAIL_FAILURE_SLACK_CHANNEL = {
    'url': env('SLACK_CHANNEL_EMAIL_FAILURE',
               default='https://hooks.slack.com/services/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'),
    'name': env('SLACK_CHANNEL_EMAIL_FAILURE',
                default='#email-failure-alert'),
}

MQTT_RESPONSE_CONST = 'MQTT'
FCM_RESPONSE_CONST = 'FCM'

DEFAULT_SHIPMENT_ORDER_REPORT_RECIPIENTS = env(
    'DEFAULT_SHIPMENT_ORDER_REPORT_RECIPIENTS',
    default=['nikhil.suresh@blowhorn.com', 'sankalp@blowhorn.com'])

DEFAULT_CREDIT_LIMIT_NOTIFICATIONS = env('DEFAULT_CREDIT_LIMIT_NOTIFICATIONS', default=[])

PERKFINANCE_EXCLUDE_DRIVER = [30199, 17275]

ANDROID_TEMPLATE_OTP_KEY = env('ANDROID_TEMPLATE_OTP_KEY',
                               default='gvk8Opckty2')

MZANSIGO_URL_C2C = env('MZANSIGO_ORDER_URL',
                       default='https://booking.mzansigo.co.za/api/orders/partners/getordercount/')
MZANSIGO_URL_B2B = env('MZANSIGO_INVOICE_URL',
                       default='https://booking.mzansigo.co.za/api/customer/partners/getinvoicedetails/')
MZANSIGO_URL_INVOICE_FLAG = env('MZANSIGO_URL_INVOICE_FLAG',
                                default='https://booking.mzansigo.co.za/api/customer/partners/updateinvoiceflag/')

CURRENCY_API_KEY = '16a54a7a705976cfd06eb144d04cdc99'
CURRENCY_CONVERTER_URL = 'http://www.apilayer.net/api/live?access_key=%s' % (
    CURRENCY_API_KEY)

DEFAULT_CITY = env('DEFAULT_CITY', default='Bengaluru')

DEFAULT_COUNTRY_CODE = 'IN'

# Read default elastic config
ES_HOST = env('ES_HOST', default='18.221.137.53')
ES_PORT = env('ES_PORT', default='9200')

# Elasticsearch configuration
ELASTICSEARCH_DSL = {
    'default': {
        'hosts': '18.221.137.53:9200'
    },
}

# Name of the Elasticsearch index
ELASTICSEARCH_INDEX_NAMES = {
    'blowhorn.order.documents': 'order_test',
    'blowhorn.contract.documents': 'contract_test',
}

MZANSIGO = 'mzansigo'

ENABLE_REVISED_COST = env.bool('ENABLE_REVISED_COST', default=False)

ALLOW_PAYMENT_SELECTION = env('ALLOW_PAYMENT_SELECTION', default=False)

CASH_FREE_APPID = env('CASH_FREE_APPID',
                      default="15904885bd830f70dafa7647540951")

CASH_FREE_SECRET_KEY = env('CASH_FREE_SECRET_KEY',
                           default="21c7e1a8d9b70895c6c9e4510c450c14017fafb7")

CASH_FREE_CREATE_ORDER_URL = env('CASH_FREE_CREATE_ORDER_URL',
                                 default="https://test.cashfree.com/api/v1/order/create")
CASH_FREE_GENERATE_TOKEN_URL = env('CASH_FREE_GENERATE_TOKEN_URL',
                                   default="https://test.cashfree.com/api/v2/cftoken/order")

CASH_FREE_PAYOUTS_GENERATE_URL = env('CASH_FREE_PAYOUTS_GENERATE_URL',
                                     default="https://payout-gamma.cashfree.com/payout/v1/addBeneficiary")

CASH_FREE_PAYOUTS_GENERATE_TOKEN_ID = env('CASH_FREE_PAYOUT_TOKEN_ID',
                                          default="CF18792C2CHNEOEVK9HKISILJT0")

CASH_FREE_PAYOUTS_TRANSFER_GENERATE_URL = env('CASH_FREE_PAYOUTS_TRANSFER_GENERATE_URL',
                                              default="https://payout-gamma.cashfree.com/payout/v1/requestAsyncTransfer")
CASH_FREE_GENERATE_TOKEN_KEY = env('CASH_FREE_PAYOUT_TRANSFER_TOKEN',
                                   default="f43e19d882db6571908834526fe88bbaf0196a7a")
CASH_FREE_PAYOUTS_URL= env("CASH_FREE_PAYOUTS_URL",default="https://payout-gamma.cashfree.com/payout/v1/authorize")

CASH_FREE_EPOS_APPID = env('CASH_FREE_EPOS_APPID',
                           default="187929082cea02ba7d69f420d29781")

CASH_FREE_EPOS_SECRET_KEY = env('CASH_FREE_EPOS_SECRET_KEY',
                                default="bad3525fb4a61e6ddd4510c4645fab061d8f7c82")

CASH_FREE_EPOS_GENERATE_TOKEN_URL = env('CASH_FREE_EPOS_GENERATE_TOKEN_URL',
                                        default="https://test.cashfree.com/eposapi/v1/cftoken/order")
CASH_FREE_PAYMENT_STATUS_CHECK = env('CASH_FREE_PAYMENT_STATUS_CHECK',
                                     default="https://test.cashfree.com/api/v1/order/info/")

ENABLE_DRIVER_EVENT = env.bool('ENABLE_DRIVER_EVENT', default=True)

ELASTICSEARCH_INDEX_POSTFIX = env('ELASTICSEARCH_INDEX_POSTFIX', default='test')

DISTANCE_MATRIX_KEY = env('DISTANCE_MATRIX_KEY', default='')

GOOGLE_MAP_DISTANCE_MATRIX_API = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=%s&destinations=%s&mode=driving&departure_time=now&key=%s'

EXOTEL_MASKING = 'https://%s:%s@api.exotel.com/v1/Accounts/%s/Calls/connect.json'

EXOTEL_CALL_DETAILS = 'https://%s:%s@api.exotel.com/v1/Accounts/%s/Calls/%s.json'

EXOTEL_API_KEY = env('EXOTEL_API_KEY', default='')

EXOTEL_API_TOKEN = env('EXOTEL_API_TOKEN', default='')

EXOTEL_API_SID = env('EXOTEL_API_SID', default='')

KALEYRA_API_KEY = env('KALEYRA_API_KEY', default='Adbe4ef5f0c4cd48932a2eb894a23fdf2')

KALYERA_API_SID = env('KALYERA_API_SID', default='HXAP1690565030IN')

KALEYRA_HEADER_API_KEY = env('KALEYRA_HEADER_API_KEY', default='P1a03N19d91B')

SMS_CALLBACK_URL = env('SMS_CALLBACK_URL', default='%s/api/sms/status' % HOST)

CALLBACK_COUNT = env('CALLBACK_COUNT', default="3")

TIME_INTERVAL = env('TIME_INTERVAL', default="20")

CALLBACK_METHOD = env('CALLBACK_METHOD', default='POST')

TWILIO_TOKEN = env('TWILIO_TOKEN', default='e6264dfb2d7b820a3da44bf69a923593')

TWILIO_SID = env('TWILIO_SID', default='AC6470da85b51f7b393ad58578546e65de')

ENABLE_DRIVER_WELL_BEING = env('ENABLE_DRIVER_WELL_BEING', default=True)

WHATSAPP_OUTBOUND_MSG_URL = env('WHATSAPP_OUTBOUND_MSG_URL',
                                default="https://api.freshchat.com/v2/outbound-messages/whatsapp")

WHATSAPP_KEY = env('WHATSAPP_KEY',
                   default='Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJpS216TTVkenRIWmprdmdSY3VrVHgxTzJ2SFlTM0U5YmVJME9XbXRNR1ZzIn0.eyJqdGkiOiI4ZTc2YWNlYy00MGMzLTRmMDktOTYzNS04YzliMmVmNmRhODgiLCJleHAiOjE5MDYwOTI2MDUsIm5iZiI6MCwiaWF0IjoxNTkwNzMyNjA1LCJpc3MiOiJodHRwOi8vaW50ZXJuYWwtZmMtdXNlMS0wMC1rZXljbG9hay1vYXV0aC0xMzA3MzU3NDU5LnVzLWVhc3QtMS5lbGIuYW1hem9uYXdzLmNvbS9hdXRoL3JlYWxtcy9wcm9kdWN0aW9uIiwiYXVkIjoiYTBhZjY2NDMtYzgwZi00OWM0LTliMzEtNmFmNDg1NzgxMWUxIiwic3ViIjoiZjNmZmI1MzgtNGI2MC00MGRkLWEzOTktNzcwNjA3OTcxMWViIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYTBhZjY2NDMtYzgwZi00OWM0LTliMzEtNmFmNDg1NzgxMWUxIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYmRhZDJlMTMtY2YzMS00ODk4LTllMjctMTI4N2JlYWVmN2MxIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6W10sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJhZ2VudDp1cGRhdGUgbWVzc2FnZTpjcmVhdGUgYWdlbnQ6Y3JlYXRlIG1lc3NhZ2U6Z2V0IGRhc2hib2FyZDpyZWFkIHJlcG9ydHM6ZXh0cmFjdDpyZWFkIHJlcG9ydHM6cmVhZCBhZ2VudDpyZWFkIGNvbnZlcnNhdGlvbjp1cGRhdGUgdXNlcjpkZWxldGUgY29udmVyc2F0aW9uOmNyZWF0ZSBvdXRib3VuZG1lc3NhZ2U6Z2V0IG91dGJvdW5kbWVzc2FnZTpzZW5kIHVzZXI6Y3JlYXRlIHJlcG9ydHM6ZmV0Y2ggdXNlcjp1cGRhdGUgdXNlcjpyZWFkIGJpbGxpbmc6dXBkYXRlIHJlcG9ydHM6ZXh0cmFjdCBjb252ZXJzYXRpb246cmVhZCIsImNsaWVudElkIjoiYTBhZjY2NDMtYzgwZi00OWM0LTliMzEtNmFmNDg1NzgxMWUxIiwiY2xpZW50SG9zdCI6IjE5Mi4xNjguMTI5LjI0OSIsImNsaWVudEFkZHJlc3MiOiIxOTIuMTY4LjEyOS4yNDkifQ.JX6vcVN2zsbBYy0XXYhnBfh3DHRroqXCe44S9976saOK_g_Cg1OaswQyZFrrQm7m46m9rzWo4LJIotNFwqObIsDN1dOyu0gQFJ00piXF-MG_SGNNVuTvvCvoPwQPVpht1GRuCuKNBC19uTh7um3N6X3wYAnW3JnrXQOVlYmq4VSAUuzhlb9Pax5Z1yKlC8XmKor5dcze10D0lYX77Lf_k8ZX1WOVhnhj1zCQEGVUlq7ljpmqOEF_Lil7op5ZvsMq-BP-dUlflxAVtaOUxc6kf8hdwEeFxPLV9ZbJWgLub9Bw0y_lUx4z7ug_ed4BrZO4-InDLcx2NQmE4h4jDUUpMw')

APPLE_CLIENT_ID = env('APPLE_CLIENT_ID', default='net.blowhorn.app.dev')
SOCIAL_AUTH_APPLE_KEY = env('SOCIAL_AUTH_APPLE_KEY', default='Y8KKA47AY5')
SOCIAL_AUTH_APPLE_PRIVATE_KEY = env('SOCIAL_AUTH_APPLE_PRIVATE_KEY',
                                    default="-----BEGIN PRIVATE KEY-----\nMIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgCKUvprAaLsx03jXw\nJTWft6kBVcUl2kT5AF0mCmd++6agCgYIKoZIzj0DAQehRANCAASuMIm5UQTGwu4y\nswbExr5hIFNR+ySejLN4lj52l8zqAjJaklAJ4pcn8+NaLk7YNr9+wfqBIzmNm0gk\nF3YKokhM\n-----END PRIVATE KEY-----")
SOCIAL_AUTH_APPLE_TEAM_ID = env('SOCIAL_AUTH_APPLE_TEAM_ID',
                                default='JMZ4MVL4RJ')

CLOUDFRONT_KEY_ID = env('CLOUDFRONT_KEY_ID', default="APKAIPXF7DN7UR2FCFUQ")
CLOUDFRONT_BASE_URL = env('CLOUDFRONT_BASE_URL',
                          default="https://dhalf0kt5yvr3.cloudfront.net")
INVALID_OTP_ATTEMPTS_ALLOWED = env('INVALID_OTP_ATTEMPTS_ALLOWED', default=4)
DISPATCH_COUNT = env('DISPATCH_COUNT', default=4)
DISPATCH_INTERVAL = env('DISPATCH_INTERVAL_MIN', default=5)

CMS_LOGIN_URL = env('CMS_LOGIN_URL',
                    default="https://strapi.blowhorn.com/auth/local")
CMS_IDENTIFIER = env('CMS_USER_NAME', default="akshara@blowhorn.com")
CMS_PASSWORD = env('CMS_PASSWORD', default="12345678@Blowhorn")
CMS_CITY_URL = env('CMS_CITY_URL',
                   default='https://strapi.blowhorn.com/cities/')
CMS_RATE_CARD_URL = env('CMS_RATE_CARD_URL',
                        default='https://strapi.blowhorn.com/rate-cards/')
CMS_VEHICLE_CLASS_URL = env('CMS_VEHICLE_CLASS_URL',
                            default='https://strapi.blowhorn.com/vehicle-classes/')
CMS_BUCKET_NAME = env('CMS_BUCKET_NAME', default='blowhorncms')
DLT_ENTITY_ID = env('DLT_ENTITY_ID', default='')
DEFAULT_TESTMAIL = ['shalu@blowhorn.com', 'ramprakash@blowhorn.com']

CHANNEL_SALES_MAIL = env('CHANNEL_SALES_MAIL', default='channel-sales@blowhorn.com')
COVID19_SUPPORT_MAIL = env('COVID19_SUPPORT_MAIL', default='covid19support@blowhorn.com')

# Shopify Beta
# SHOPIFY_CLIENT_ID = env('SHOPIFY_CLIENT_ID', default='b90f3fbb7087f15112ea285a784575cd')
# SHOPIFY_SECRET = env('SHOPIFY_SECRET', default='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')
SHOPIFY_CLIENT_ID = env('SHOPIFY_CLIENT_ID', default='7265cf130e41bf6af037c3340afc4af4')
SHOPIFY_SECRET = env('SHOPIFY_SECRET', default='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')

AES_KEY_1 = env('AES_KEY_1', default='aes1password1234')
AES_KEY_2 = env('AES_KEY_2', default='aes2password4321')
BLOWHORN_CONCERN_MAIL = env('BLOWHORN_CONCERN_MAIL', default='account-info@blowhorn.com')


try:
    from config.settings.env import *
except ImportError:
    pass

CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOW_METHODS = [
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
]

E_INVOICING_CLIENT_ID = env('E_INVOICING_CLIENT_ID', default='CATBUSkjgf5ffd22dfg98dfg7')
E_INVOICING_CLIENT_SECRET = env('E_INVOICING_CLIENT_SECRET', default='CBf9gkf8dj48rdhf8hr48r42')
E_INVOICING_IPADDRESS = env('E_INVOICING_IPADDRESS', default='15.206.130.170')
E_INVOICING_USER_NAME = env('E_INVOICING_USER_NAME', default='CATBUS')
TESTING_NUMBERS = env('TESTING_NUMBERS', default=['2222211111', '333332222'])
GST_FILING_REQUIRED = env('GST_FILING_REQUIRED', default=True)
E_INVOICING_PASSWORD = env('E_INVOICING_PASSWORD', default='Catbus@21')
E_INVOICING_GSTIN = env('E_INVOICING_GSTIN', default='29AAFCC6784M1ZV')
E_INVOICING_AUTH_URL = env('E_INVOICING_AUTH_URL', default='http://182.76.79.236:35001/EinvoiceTPApi-QA/authenticate')
E_INVOICING_IRN_URL = env('E_INVOICING_IRN_URL', default='http://182.76.79.236:35001/EinvoiceTPApi-QA/einvoice')
E_INVOICING_GET_PDF = env('E_INVOICING_GET_PDF', default='http://182.76.79.236:35001/EinvoiceTPApi-QA/EInvoice?action=GETINVOICE&docdate=%s&doctype=%s&docnum=%s')

DECENTRO_BASE_URL = env('DECENTRO_BASE_URL', default='https://in.staging.decentro.tech')
DECENTRO_CLIENT_ID = env('DECENTRO_CLIENT_ID', default='blowhorn_staging')
DECENTRO_CLIENT_SECRET = env('DECENTRO_CLIENT_SECRET', default='K4K9j9MpxAC3jGo2le5MpJnl4kya4cWW')
DECENTRO_MODULE_SECRET = env('DECENTRO_MODULE_SECRET', default='XJgJnQtMarOIIrfsoFeiswZpeYJ8KFI5')
DECENTRO_ACCOUNT_MODULE_SECRET = env('DECENTRO_ACCOUNT_MODULE_SECRET', default='')
DECENTRO_BANK_PROVIDER_SECRET = env('DECENTRO_BANK_PROVIDER_SECRET', default='')
