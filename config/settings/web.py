import environ
env = environ.Env()

# Need to keep old keys
# @todo store keys in redis
GOOGLE_TAG_MANAGER_KEY_OLD = env('GOOGLE_TAG_MANAGER_KEY_OLD', default='')
GOOGLE_TAG_MANAGER_KEY = env('GOOGLE_TAG_MANAGER_KEY', default='')
FB_CONVERSION_KEY_OLD = env('FB_CONVERSION_KEY_OLD', default='')
FB_CONVERSION_KEY = env('FB_CONVERSION_KEY', default='')
