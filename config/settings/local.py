"""
Local settings

- Run in Debug mode

- Use console backend for emails

- Add Django Debug Toolbar
- Add django-extensions as app
"""
from .base import *  # noqa

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = env('DJANGO_SECRET_KEY',
                 default='2gmA=c.l,^O9;%9ea0&?F=z@cjq1eg$XPi+`0Gb*1P3xH6%}},')

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

WHITENOISE_MIDDLEWARE = ['whitenoise.middleware.WhiteNoiseMiddleware', ]
MIDDLEWARE = WHITENOISE_MIDDLEWARE + MIDDLEWARE
RAVEN_MIDDLEWARE = [
    'raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware']
MIDDLEWARE = RAVEN_MIDDLEWARE + MIDDLEWARE

# Mail settings
# ------------------------------------------------------------------------------

# Django mail will use default PORT: 25
#EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = env('DJANGO_DEFAULT_FROM_EMAIL',
                        default='mail@blowhorn.net')
EMAIL_HOST = env('DJANGO_HOST_EMAIL', default='smtp.gmail.com')
SERVER_EMAIL = env('DJANGO_HOST_EMAIL', default=DEFAULT_FROM_EMAIL)
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND',
                   default='django.core.mail.backends.console.EmailBackend')
EMAIL_HOST_PASSWORD = env('DJANGO_EMAIL_PASSWORD', default='')
EMAIL_HOST_USER = env('DJANGO_HOST_USER', default=DEFAULT_FROM_EMAIL)
EMAIL_USE_TLS = True

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db('DATABASE_URL', default='postgres:///blowhorn')
}
DATABASES['default']['ATOMIC_REQUESTS'] = True
DATABASES['default']['NAME'] = env('DB_NAME', default='blowhorn')
# specify Postgis backend
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'
DATABASES['default']['HOST'] = 'postgres'
DATABASES['default']['USER'] = 'blowhorn'

# CACHING
# ------------------------------------------------------------------------------


# django-debug-toolbar
# ------------------------------------------------------------------------------
MIDDLEWARE += ['corsheaders.middleware.CorsMiddleware',
                       'debug_toolbar.middleware.DebugToolbarMiddleware', ]
INSTALLED_APPS += ['debug_toolbar', ]

INTERNAL_IPS = ['127.0.0.1', '10.0.2.2', ]

import socket
import os
# tricks to have debug toolbar when developing with docker
if os.environ.get('USE_DOCKER') == 'yes':
    ip = socket.gethostbyname(socket.gethostname())
    INTERNAL_IPS += [ip[:-1] + '1']

DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}

# django-extensions
# ------------------------------------------------------------------------------
INSTALLED_APPS += ['django_extensions', ]

# Constants
GOOGLE_MAPS_API_KEY = env('GOOGLE_MAPS_API_KEY', default="AIzaSyDHnk-B5voE01PGVefEjnGvHohVA8QDY8M")
GOOGLE_MAPS_API_KEY_ADMIN = env('GOOGLE_MAPS_API_KEY', default=GOOGLE_MAPS_API_KEY)
GOOGLE_CLIENT_ID = env('GOOGLE_CLIENT_ID', default="309303081199-mfh2l774kgcfs49a37qne030nj7sf9iu.apps.googleusercontent.com")
IOS_GOOGLE_CLIENT_ID = env('IOS_GOOGLE_CLIENT_ID', default="322133066794-irbs24soh7ivlkh1emarae8jdhn02sg3.apps.googleusercontent.com")
GOOGLE_CLIENT_ID_COSMOS = env('GOOGLE_CLIENT_ID_COSMOS', default='322133066794-299ibl95vrcasmstl8ajpun0i03qq4tc.apps.googleusercontent.com')

# Razor pay path
RAZORPAY_KEY_ID = 'rzp_test_fTKyjp0r9a2HEx'
RAZORPAY_SECRET_KEY = 'M56r2o2tZoIQz7KFzOmE4UPu'
RAZORPAY_WEBHOOK_SECRET = 'qcpNWoV1LlfAe0H7urmU5nOdxtvGaj'

RAZORPAY_SETTINGS = {
    'path': RAZORPAY_PATH,
    'id': RAZORPAY_KEY_ID,
    'secret': RAZORPAY_SECRET_KEY,
    'webhook_secret': RAZORPAY_WEBHOOK_SECRET
}

RAZORPAY_KEY_ID_RELIEF = 'rzp_test_JWlgp3kqsx5ftn'
RAZORPAY_SECRET_KEY_RELIEF = 'mUGfiVI7CRiiC9td4QQU3rjQ'
RAZORPAY_WEBHOOK_SECRET_RELIEF = 'sk2PNTDAPPJwqr6'
RAZORPAY_SETTINGS_RELIEF = {
    'path': RAZORPAY_PATH,
    'id': RAZORPAY_KEY_ID_RELIEF,
    'secret': RAZORPAY_SECRET_KEY_RELIEF,
    'webhook_secret': RAZORPAY_WEBHOOK_SECRET_RELIEF
}

RAZORPAY_ACCOUNTS = {
    'default': RAZORPAY_SETTINGS,
    'donation': RAZORPAY_SETTINGS_RELIEF
}

#added to run server on local
AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
AWS_STORAGE_BUCKET_NAME = "test-enterprise-blowhorn"


FIREBASE_APIKEY = env('FIREBASE_APIKEY', default="AIzaSyDjWLvY90sbbZOOd8DQ36WZ89tnVfWIGu0")
FIREBASE_DATABASE_SECRET = env('FIREBASE_DATABASE_SECRET', default="TJpU4a0RuoaHT6YvQQVFtqm2lIBqkWaEQmBJkr86")
FIREBASE_AUTHDOMAIN = env('FIREBASE_AUTHDOMAIN', default="blowhorntest.firebaseapp.com")
FIREBASE_DATABASE_URL = env('FIREBASE_DATABASE_URL', default='https://blowhorntest.firebaseio.com')
FIREBASE_STORAGEBUCKET = env('FIREBASE_STORAGEBUCKET', default="blowhorntest.appspot.com")
FIREBASE_MESSAGINGSENDERID = env('FIREBASE_MESSAGINGSENDERID', default="322133066794")
FIREBASE_USER_UID = env('FIREBASE_USER_UID', default="L3f2FIYLMUcB0U6KFVBEBxIGJtx2")
FIREBASE_ADMIN_USER_UID = env('FIREBASE_ADMIN_USER_UID', default="I4e0UzuwNMNTFrhgNbbQYpZmNvJ2")
FIREBASE_ADMIN_UID = env('FIREBASE_ADMIN_UID', default="kgLWcMStevbxI9cQQpVmrjiXuG03")
FIREBASE_ADMIN_AUTH_EMAIL_UID = env('FIREBASE_ADMIN_AUTH_EMAIL_UID', default='kgLWcMStevbxI9cQQpVmrjiXuG03')

SERVICE_ACCOUNT_EMAIL = env('SERVICE_ACCOUNT_EMAIL', default="firebasetest@blowhorntest.iam.gserviceaccount.com")
SERVICE_ACCOUNT_AUD = "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit"
FIREBASE_PRIVATE_KEY = env('FIREBASE_PRIVATE_KEY', default="-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCicXK6B5LI9ihe\nKlfCLO/Kqi+k8MKDRnrhZXasXeJV7e+mzrsXRqjKmbaX+tXmC2ypxsF3VcY+89D5\nme8pWkHMWWPIAKg3jmA3FgGdvXhNR1ip75U0/PFxvrZ+07pBPSfCl98UsxsLvBqC\n69/rHwsjNwnFtTHzmKqxYTZNK/SvYfWcfG3jVW6L3dwUzXlFiRvNJDelGMIM4Chp\na1cQtSmVcdVfQFhF+vbsSar3ntkqIy5K/eFTd3Nvq+L35UYJmZVZohHeoseJl9Fd\nJnFFV3gTx3Cq97h+RioDA96E6U+RtwoPakwNhuAVyMyHX3g7UTwOsIRqgco+RMAh\na6wYkX1fAgMBAAECggEBAIUvaPYXoYS9NmnGY7+jolNS589fSsvhLHzC+OLn8tur\nQtNMKl8yItBB6VCAa4G6mjQ07oiOPgV9WDhdupX4F3yZSdNAnqs2Q7LGizqujwUl\nWw+xjG8ueDNfPzeLFunIX2IPrOrG0oTzi/UNucCH6lTMGmoMXtEvubvHTYNGZ86m\nvsn3/TeA86pFdV3HagTWG8OfVlRUl2u5NNUZvj+QDklyfFS687Wf5cSxJUweRoUR\nWcdGmVzNTy+ibnX8K2/c8FEhQMOGoFt+nQ+4DJGbHcw5m9Cn37o45HNxT4cCLFmc\nF6/6eBCWsUiE+1RkDcu3ivi3n9P3YbyYLTBPuI3oRZECgYEA1OOUDtABQoECtKA5\nwKAdCiJ/ItQnlTfkfjy5GGFGPsbhGVFL1z+yS97xRQAMpW9gBlkC0rEh3Y9qln1X\nIbA3VxmmW9kY/WPsJzHqz6JojdeM5FIqBTrqA0FqZWBqHHjb7n+6X6aJGuWqSo2z\nh+rxh7i2kEJB+oypeiBlmfgxAHkCgYEAw1aykToXHxxtHC/KiRU/xvkkFCLvn4ay\nqd0LQ5wwVsEDFCtOfDW5TcdW0Q2NA/YH4vNRUj4K62BYVDMdyc7sBWIJJZfPUgKS\ndrVHJmyArncs6GkxtebvNS+KpGCGEDCrpin0VDr5oD1lYJq3pMqZrFIw0RlE8aFG\nYl0Qy77KZpcCgYBtPwf4ffdXiFq6FErutXuRethhKBvT8CW6PjEB+NESX/6dfmF7\n6Pz3AEYxkW/r6XaOa1kOg8WMLA+XgGSZLhOIB+qd0Y+IYKGMD4qe2Ins2AM+G4fi\nvTPfTc9qjIqHZ+H8xuD+Ori668ZykbriccdWTYyqBMZU66AAwuVAmOOUiQKBgApY\neiak6/qS19iJU3HFNVaFdXq3ivdo++dxNJqFGC5QFKipzoNXmRPcfeWekRxnmG1V\ns7XmYU+sD6GN/4Ljua5jTda23p0aU8WDvlvznTEgULmScLaQn5SyiuY/0nnjCgRH\n7o/Te8sX1VmzeAx7Sn5M8Bby/qrJft/QzAXueevLAoGAZsyIEDanPqrkYL6Hjaai\ngAa9bdl5j6axs7bsWXyymIEUn/64UVxF09oeO9Nur1K0LHw6WcdAgMF8dHNk1D3O\nNgrpTx5EUYwmhlu69r+/+NewVXxlGdzHreJuE+sz1Oy1jMpFObYmOCFD5C7Sj28D\nXmsBjWNg98xCcHpKcxlUH8M=\n-----END PRIVATE KEY-----\n")

#FIREBASE_PRIVATE_KEY = env('FIREBASE_PRIVATE_KEY', default="-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDXkgV8agL4PRu1\nNGSG3uWKzZqNHpu6qXy4dpqBhEoOdWhQJAg6GvTn6+WExmhcD2F4mT7W3p432fqq\nrxxG6xovULI5qMB1wikleR8qVWDzJM31+9a2tiS3GvfS91RXMBFDKSTgwUXh0YuS\nNcODl/IT2yH2YGxaOvgoLLcIkuyLkRkuMrQrWWK11SjK8d7rQu5o72W7wOIz7dbd\nk0y4bZCU0rCddtVHmLxuAiVnsFtEB1omGLkiNip/YOeK6vz6VOTXwlKZbqy/BObA\nILD4szrrnCPZHT/+TLwKiJC/iVKX30x1yy3K0uTO4W0FUoh8HO+FUtOaNp6AVXXq\nT7DBH4APAgMBAAECggEAaifFPdQyrC3jhCNN0cDYBrdjAVVuWMh325rEUT1k/qPA\nf56O0pZNvhvDhdPytkr4s9u7PzKXdWeJch4TbbT+ywgRDCXWyOaZPpPbzgwHVxTr\nu9ONk6I0iFrmaWRUiy7ngUw0ld13XVHESnbhVd8wXajOm7Y2BWT+qUIDzFwV0k/1\nPOagJ4H3+FjLFma/bojoN8cPam4ZLppFOId0w0qdWdR3IlmA04cqOr8zBrP2dNfx\nOrjYG1UZRLqfI93WTDIJg4RN7rNQrY+rW4bnWemY8fFGMnDQ/uATL0WHIHEvO74c\npTU5yWMDZWHfsSAEe6lVnMDph0QgaAWC9jbKG3MlOQKBgQDySTjunFSRFgJGTvk5\nJJp0HHFmRtMTs3EKI+Q4J7PudrIcoJNPYyu+EnIraNZSoO2PXmQiY3SrMftprwTS\nm0kcCkKM0w5vq1zF/MD4YduSRgWDcy56uqjh7d2EYQSO3kYWBEMd8VN6PplFLjcj\nwd4zG/chVd7NXFNvSgg/83AC7QKBgQDjxa8Ia1B/VPPQ99gD6iUIeO9PRJfa4vrI\nRuHD/aILnTZoewH5TKofPOSDXkslwBMKhuXRd319GTft7EueYZ0LIOKbFUTWWqgA\nb/iqS/AXauEmUGFlrJeUiOg0Ehypy3efxjnZZbH8kBjm9C5pDZ2CXrmGjvUN3IfF\n4nXfuiuDawKBgCTX+a5fW4TMof/rFn8YD9zEToJNFuASE4iFOLlJYFVM+3za9kvG\nOuqmh3IOeTkLe9Snd/a6xQ0bhq3ljYgxUQbQkJo1piZZGryI2RdsWiV+PGxm4ZSM\nOg5RS7RLxJOtPV8vur0c66LVTh11D7GCU7XV9Ni+5Ci6d32e9m4zKL9pAoGBAMoj\n/KyqIKyAhklkS49d0zPrr+ZXZ7VYT1xCm8ZGZ/OKuGiNEjlfcnN6pQp5OpPsHHnx\nMjBtrS7CnMJIPOv4kVj4/GLJw2fA2OxacwMflZoSvnI0T9veCkBGKF1d3ZZK/oUL\ndPecqxMMBy2gczfEX4795gSnBpAgsVZLHnVJUai1AoGBAOzhe2P7TkLKK8WXy5qj\nxKK7/tE0U9QE906z4fC8xei/mfJOOfZYXOgQLaRETBCx5KWrHhzNcYGZU8V2C3rg\nQVbZzvjZOpS+iGXdPZ184koBbi2yJIfGftfWeg/TvklZ1uIa+OMezvvm/DNvuQL4\nJfFsL4T/f8JHdnxKLmQ8EjpM\n-----END PRIVATE KEY-----\n")

FIREBASE_KEYS = {
    'driver': env('FIREBASE_DRIVER_KEY', default='TZMyeYbWWDa79UvrJIJX1zb6qXA2'),
    'customer': FIREBASE_USER_UID
}


# EXOTEL Configurations
EXOTEL_BASEURL = None
EXOTEL_SID = None
EXOTEL_TOKEN = None

FIREBASE_SERVER_KEY = env('FIREBASE_SERVER_KEY', default='AIzaSyC94ek7NfMHOiS9eE_lQZNIZ8ODd_9SjaI')
# Push Notifications via Firebase using fcm-django package
FCM_DJANGO_SETTINGS = {
    # Default is set to the Driver APP SERVER Key
    "FCM_SERVER_KEY": FIREBASE_SERVER_KEY,
    # true if you want to have only one active device
    # per registered user at a time
    # default: False
    "ONE_DEVICE_PER_USER": True,
    # devices to which notifications cannot be sent,
    # are deleted upon receiving error response from FCM
    # default: False
    "DELETE_INACTIVE_DEVICES": True,
}

MAP_WIDGETS.update({
    "GOOGLE_MAP_API_KEY": env('SERVER_GOOGLE_MAP_KEY', default="AIzaSyDHnk-B5voE01PGVefEjnGvHohVA8QDY8M")
})

# TESTING
# ------------------------------------------------------------------------------
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# Your local stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

# XIAOMI Notification Url
XIAOMI_NOTIFICATION_URL = 'http://owms.api.test.mi.com/tms/WaybillSigned'
ECODE_FOR_XIAOMI = 'Ind.blowhorn'

# Email addresses for sending email
RECEIPT_SENDER_ID = 'nikhil@blowhorn.net'
ADMIN_SENDER_ID = RECEIPT_SENDER_ID

# Email addresses for sending email
# TEST
EMAIL_ID_CONTACT_US = 'admin@blowhorn.net'
ADMIN_SENDER_ID = 'admin@blowhorn.net'

# To allow access origin for ng server for mytrips
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    'http://localhost:4200',
)

# Any settings (or variable assignment) should be done on top of the above
# try except block.
TESTING_SLACK_URL = 'https://hooks.slack.com/services/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'
TESTING_SLACK_CHANNEL_DEFAULT = '#tech-testing'

# Paytm settings (Staging settings provided by PayTM)
PAYTM_MERCHANT_KEY = "_EpuVfgXTejTGRM9"
PAYTM_MERCHANT_ID = "CATBUS07328649070242"
PAYTM_WEBSITE = 'WEB_STAGING'
HOST_URL = 'http://localhost:80'
PAYTM_CALLBACK_URL = HOST_URL + "/api/paytm/"
PAYTM_ENDPOINT_TRANSN_REQUEST = "https://securegw-stage.paytm.in/theia/processTransaction"
PAYTM_ENDPOINT_TRANSN_STATUS = "https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus"
PAYTM_REFUND_REQUEST_URL = 'https://securegw-stage.paytm.in/refund/process'
PAYTM_ENDPOINT_TRANSN_WITHDRAWAL = "https://pguat.paytm.com/oltp/HANDLER_FF/withdrawScw"
# Load any server specific settings (if applicable)

# Payfast Payment Gateway
PAYFAST_MERCHANT_ID = '10013372'
PAYFAST_MERCHANT_KEY = 'i04zmqwfiyqpf'
PAYFAST_CANCEL_URL = '/payfast/cancel'
PAYFAST_SUCCESS_URL = '/payfast/success'
PAYFAST_HOOK_URL = '/api/payfast/notify-payment/'

# MongoDB configuration.
# Dev driver apps are connecting to HiveMQ broker and location data would be
# available on below mongodb.

# DATABASES.update(
#     {
#         "activity": {
#             'ENGINE': 'djongo',
#             'NAME': env('MONGO_DB_NAME', default='blowhorn'),
#             'HOST': env('MONGO_DB_HOST', default='localhost'),
#             'USER': env('MONGO_DB_USER', default='admin'),
#             'PASSWORD': env('MONGO_DB_PASSWORD', default='admin'),
#         }
#     }
# )


try:
       from .local_settings import *
except ImportError:
   pass

# DATABASES.update(
#     {
#         "activity": {
#             'ENGINE': 'djongo',
#             'CLIENT': {
#                 'name': 'test_suren',
#                 'host': 'mongodb://blowhorntestcluster-shard-00-00-jsdo3.mongodb.net:27017,blowhorntestcluster-shard-00-01-jsdo3.mongodb.net:27017,blowhorntestcluster-shard-00-02-jsdo3.mongodb.net:27017/test?ssl=true&replicaSet=BlowhornTestCluster-shard-0&authSource=admin&retryWrites=true',
#                 'username': 'bhornmongo',
#                 'password': 'admin123',
#                 'authMechanism': 'SCRAM-SHA-1'
#             }
#         }
#     }
# )
