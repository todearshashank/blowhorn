"""
Production Configurations

- Use Amazon's S3 for storing static files and uploaded media
- Use mailgun to send emails
- Use Redis for cache

- Use sentry for error logging


"""

# from boto.s3.connection import OrdinaryCallingFormat

import logging
import raven


from .base import *  # noqa
from celery.schedules import crontab

# HOST = 'https://blowhorn.com'
# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env('DJANGO_SECRET_KEY')

# This ensures that Django will be able to detect a secure connection
# properly on Heroku.
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# raven sentry client
# See https://docs.sentry.io/clients/python/integrations/django/
INSTALLED_APPS += ['raven.contrib.django.raven_compat', ]

# Use Whitenoise to serve static files
# See: https://whitenoise.readthedocs.io/
WHITENOISE_MIDDLEWARE = ['whitenoise.middleware.WhiteNoiseMiddleware', ]
MIDDLEWARE = WHITENOISE_MIDDLEWARE + MIDDLEWARE
RAVEN_MIDDLEWARE = ['raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware']
MIDDLEWARE = RAVEN_MIDDLEWARE + MIDDLEWARE


# EXOTEL Configurations
EXOTEL_BASEURL = ''
EXOTEL_SID = 'blowhorn5'
EXOTEL_TOKEN = 'bd8fa18e650fd7edec713b212c851522a940177f'


# SECURITY CONFIGURATION
# ------------------------------------------------------------------------------
# See https://docs.djangoproject.com/en/dev/ref/middleware/#module-django.middleware.security
# and https://docs.djangoproject.com/en/dev/howto/deployment/checklist/#run-manage-py-check-deploy

# set this to 60 seconds and then to 518400 when you can prove it works
SECURE_HSTS_SECONDS = 60
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
    'DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS', default=True)
SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
    'DJANGO_SECURE_CONTENT_TYPE_NOSNIFF', default=True)
SECURE_BROWSER_XSS_FILTER = True
# SESSION_COOKIE_SECURE = True
# SESSION_COOKIE_HTTPONLY = True
SECURE_SSL_REDIRECT = env.bool('DJANGO_SECURE_SSL_REDIRECT', default=True)
# CSRF_COOKIE_SECURE = True
# CSRF_COOKIE_HTTPONLY = True
X_FRAME_OPTIONS = 'SAMEORIGIN'

# SITE CONFIGURATION
# ------------------------------------------------------------------------------
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
# END SITE CONFIGURATION

INSTALLED_APPS += ['gunicorn', ]


# STORAGE CONFIGURATION
# ------------------------------------------------------------------------------
# Uploaded Media Files
# ------------------------
# See: http://django-storages.readthedocs.io/en/latest/index.html
INSTALLED_APPS += ['storages', ]

# Async Django email backend using celery
# See https://pypi.python.org/pypi/django-celery-email
INSTALLED_APPS += ['djcelery_email', ]

# For ELASTIC_APM:
INSTALLED_APPS += ['elasticapm.contrib.django', ]
ELASTIC_APM = {
    'SERVER_URL': 'http://apmserver.blowhorn.com:8200',
}
MIDDLEWARE += ['elasticapm.contrib.django.middleware.TracingMiddleware', ]
# EMAIL
# ------------------------------------------------------------------------------
DEFAULT_FROM_EMAIL = env('DJANGO_DEFAULT_FROM_EMAIL',
                         default='Blowhorn Logistics <noreply@blowhorn.com>')
EMAIL_SUBJECT_PREFIX = env('DJANGO_EMAIL_SUBJECT_PREFIX', default='[Blowhorn Logistics]')
SERVER_EMAIL = env('DJANGO_SERVER_EMAIL', default=DEFAULT_FROM_EMAIL)
EMAIL_HOST = env('DJANGO_EMAIL_HOST', default='smtp.gmail.com')
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND',
                    default='django.core.mail.backends.smtp.EmailBackend')
EMAIL_HOST_PASSWORD = env('DJANGO_EMAIL_PASSWORD', default='')
EMAIL_HOST_USER = env('DJANGO_HOST_USER', default=DEFAULT_FROM_EMAIL)
EMAIL_USE_TLS = True

# Anymail with Mailgun
INSTALLED_APPS += ['anymail', ]
ANYMAIL = {
    'MAILGUN_API_KEY': env('DJANGO_MAILGUN_API_KEY'),
    'MAILGUN_SENDER_DOMAIN': env('MAILGUN_SENDER_DOMAIN')
}

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See:
# https://docs.djangoproject.com/en/dev/ref/templates/api/#django.template.loaders.cached.Loader
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader', 'django.template.loaders.app_directories.Loader', ]),
]

# ------------------------------------------------------------------------------
# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db('DATABASE_URL', default='postgres:///blowhorn'),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True
DATABASES['default']['NAME'] = env('DB_NAME', default='blowhorn')
# specify Postgis backend
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'

# Use the Heroku-style specification
# Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
#DATABASES['default'] = env.db('DATABASE_URL')

# Read replica config

if 'RDS_READ_REP_1' in os.environ:
    DATABASES.update(
        {
            'readrep1': {
                'ENGINE': 'django.contrib.gis.db.backends.postgis',
                'NAME': env('DB_NAME', default='blowhorn'),
                'USER': env('POSTGRES_USER', default='blowhorn'),
                'PASSWORD': env('POSTGRES_PASSWORD', default='blowhorn'),
                'HOST': env('RDS_READ_REP_1', default=''),
                'PORT': '5432',
            }
        }
    )

# Sentry Configuration
SENTRY_DSN = env('DJANGO_SENTRY_DSN')
SENTRY_CLIENT = env('DJANGO_SENTRY_CLIENT', default='raven.contrib.django.raven_compat.DjangoClient')
SENTRY_CELERY_LOGLEVEL = env.int('DJANGO_SENTRY_LOG_LEVEL', logging.ERROR)
RAVEN_CONFIG = {
    'CELERY_LOGLEVEL': env.int('DJANGO_SENTRY_LOG_LEVEL', logging.ERROR),
    'dsn': SENTRY_DSN,
    #    'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),

}

# Custom Admin URL, use {% url 'admin:index' %}
ADMIN_URL = env('DJANGO_ADMIN_URL')

# Your production stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

# Constants
DEFAULT_GOOGLE_MAPS_API_KEY_ADMIN = "AIzaSyCx1SpS_1X6_gqwMaNxWiE7TPLhgamMp0Y"  # For admin
# GOOGLE_MAPS_API_KEY = "AIzaSyCEKVvLTOuQDg-v-iOZcG5xOKwnYrx4S9s"
DEFAULT_GOOGLE_MAPS_API_KEY = "AIzaSyCHAdcIxi3dcpzs1LQBdI6y7iNrMmgFisE" # Prem-Key

GOOGLE_MAPS_API_KEY = env('GOOGLE_MAPS_API_KEY', default=DEFAULT_GOOGLE_MAPS_API_KEY)
GOOGLE_MAPS_API_KEY_ADMIN = env('GOOGLE_MAPS_API_KEY', default=DEFAULT_GOOGLE_MAPS_API_KEY_ADMIN)
GOOGLE_CLIENT_ID = env('GOOGLE_CLIENT_ID', default="309303081199-mfh2l774kgcfs49a37qne030nj7sf9iu.apps.googleusercontent.com")
IOS_GOOGLE_CLIENT_ID = env('IOS_GOOGLE_CLIENT_ID', default="309303081199-4vjijvjeru5ldht30gse6to3p9j5ith4.apps.googleusercontent.com")

RAZORPAY_KEY_ID = 'rzp_live_Yi6jI06EH7Umgi'
RAZORPAY_SECRET_KEY = 'JDOJtLgxaL9HIW9o3L7I8gPk'
RAZORPAY_WEBHOOK_SECRET = 'WEBXV7aR4htZjMib1s0cJglyznfxTw'


RAZORPAY_SETTINGS = {
    'path': RAZORPAY_PATH,
    'id': RAZORPAY_KEY_ID,
    'secret': RAZORPAY_SECRET_KEY,
    'webhook_secret': RAZORPAY_WEBHOOK_SECRET
}

FIREBASE_DATABASE_URL = env('FIREBASE_DATABASE_URL', default='https://blowhorntemplate.firebaseio.com')
FIREBASE_APIKEY = env('FIREBASE_APIKEY', default="AIzaSyCoVOu3uXmeWPdb3JsHUxyPv9apKdw8DME")
FIREBASE_AUTHDOMAIN = env('FIREBASE_AUTHDOMAIN', default="blowhorntemplate.firebaseapp.com")
FIREBASE_STORAGEBUCKET = env('FIREBASE_STORAGEBUCKET', default="blowhorntemplate.appspot.com")
FIREBASE_MESSAGINGSENDERID = env('FIREBASE_MESSAGINGSENDERID', default="309303081199")
FIREBASE_USER_UID = env('FIREBASE_USER_UID',default="jl8Ra20DD8bst6mvC49GOG07Fzx1")
FIREBASE_ADMIN_USER_UID = env('FIREBASE_ADMIN_USER_UID', default="M3rfUvjKyxaBJ3l886h7ATfnOBE3")
FIREBASE_DATABASE_SECRET = env('FIREBASE_DATABASE_SECRET', default='SIQHKKmMRRYcGMrJ2k4ygp5nbwFzCLpngSEQUgHX')
FIREBASE_ADMIN_AUTH_EMAIL_UID = env('FIREBASE_ADMIN_AUTH_EMAIL_UID', default='YlZEUXBKpNgXE3BO6LBWQByjaf92')

FIREBASE_PRIVATE_KEY = env('FIREBASE_PRIVATE_KEY', default="-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDXkgV8agL4PRu1\nNGSG3uWKzZqNHpu6qXy4dpqBhEoOdWhQJAg6GvTn6+WExmhcD2F4mT7W3p432fqq\nrxxG6xovULI5qMB1wikleR8qVWDzJM31+9a2tiS3GvfS91RXMBFDKSTgwUXh0YuS\nNcODl/IT2yH2YGxaOvgoLLcIkuyLkRkuMrQrWWK11SjK8d7rQu5o72W7wOIz7dbd\nk0y4bZCU0rCddtVHmLxuAiVnsFtEB1omGLkiNip/YOeK6vz6VOTXwlKZbqy/BObA\nILD4szrrnCPZHT/+TLwKiJC/iVKX30x1yy3K0uTO4W0FUoh8HO+FUtOaNp6AVXXq\nT7DBH4APAgMBAAECggEAaifFPdQyrC3jhCNN0cDYBrdjAVVuWMh325rEUT1k/qPA\nf56O0pZNvhvDhdPytkr4s9u7PzKXdWeJch4TbbT+ywgRDCXWyOaZPpPbzgwHVxTr\nu9ONk6I0iFrmaWRUiy7ngUw0ld13XVHESnbhVd8wXajOm7Y2BWT+qUIDzFwV0k/1\nPOagJ4H3+FjLFma/bojoN8cPam4ZLppFOId0w0qdWdR3IlmA04cqOr8zBrP2dNfx\nOrjYG1UZRLqfI93WTDIJg4RN7rNQrY+rW4bnWemY8fFGMnDQ/uATL0WHIHEvO74c\npTU5yWMDZWHfsSAEe6lVnMDph0QgaAWC9jbKG3MlOQKBgQDySTjunFSRFgJGTvk5\nJJp0HHFmRtMTs3EKI+Q4J7PudrIcoJNPYyu+EnIraNZSoO2PXmQiY3SrMftprwTS\nm0kcCkKM0w5vq1zF/MD4YduSRgWDcy56uqjh7d2EYQSO3kYWBEMd8VN6PplFLjcj\nwd4zG/chVd7NXFNvSgg/83AC7QKBgQDjxa8Ia1B/VPPQ99gD6iUIeO9PRJfa4vrI\nRuHD/aILnTZoewH5TKofPOSDXkslwBMKhuXRd319GTft7EueYZ0LIOKbFUTWWqgA\nb/iqS/AXauEmUGFlrJeUiOg0Ehypy3efxjnZZbH8kBjm9C5pDZ2CXrmGjvUN3IfF\n4nXfuiuDawKBgCTX+a5fW4TMof/rFn8YD9zEToJNFuASE4iFOLlJYFVM+3za9kvG\nOuqmh3IOeTkLe9Snd/a6xQ0bhq3ljYgxUQbQkJo1piZZGryI2RdsWiV+PGxm4ZSM\nOg5RS7RLxJOtPV8vur0c66LVTh11D7GCU7XV9Ni+5Ci6d32e9m4zKL9pAoGBAMoj\n/KyqIKyAhklkS49d0zPrr+ZXZ7VYT1xCm8ZGZ/OKuGiNEjlfcnN6pQp5OpPsHHnx\nMjBtrS7CnMJIPOv4kVj4/GLJw2fA2OxacwMflZoSvnI0T9veCkBGKF1d3ZZK/oUL\ndPecqxMMBy2gczfEX4795gSnBpAgsVZLHnVJUai1AoGBAOzhe2P7TkLKK8WXy5qj\nxKK7/tE0U9QE906z4fC8xei/mfJOOfZYXOgQLaRETBCx5KWrHhzNcYGZU8V2C3rg\nQVbZzvjZOpS+iGXdPZ184koBbi2yJIfGftfWeg/TvklZ1uIa+OMezvvm/DNvuQL4\nJfFsL4T/f8JHdnxKLmQ8EjpM\n-----END PRIVATE KEY-----\n")
SERVICE_ACCOUNT_EMAIL = env('SERVICE_ACCOUNT_EMAIL', default="firebase@blowhorntemplate.iam.gserviceaccount.com")
SERVICE_ACCOUNT_AUD = "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit"

FIREBASE_KEYS = {
    'driver': env('FIREBASE_DRIVER_KEY', default='rdAsXhMqCKW8p87QiQ05C3eCYvW2'),
    'customer': FIREBASE_USER_UID
}

FIREBASE_SERVER_KEY = env('FIREBASE_SERVER_KEY', default='AIzaSyAgqqtZ_Nat1j3PrCX_Cm_uh0N6jtu91m0')
# Push Notifications via Firebase using fcm-django package
FCM_DJANGO_SETTINGS = {
    # Default is set to the Driver APP SERVER Key
    "FCM_SERVER_KEY": FIREBASE_SERVER_KEY,
    # true if you want to have only one active device
    # per registered user at a time
    # default: False
    "ONE_DEVICE_PER_USER": True,
    # devices to which notifications cannot be sent,
    # are deleted upon receiving error response from FCM
    # default: False
    "DELETE_INACTIVE_DEVICES": True,
}

# Email addresses for sending email
RECEIPT_SENDER_ID = 'receipts@blowhorn.net'
ADMIN_SENDER_ID = 'admin@blowhorn.net'

# Email addresses for sending email
EMAIL_ID_CONTACT_US = env('EMAIL_ID_CONTACT_US', default='shoutout@blowhorn.net')
ADMIN_SENDER_ID = 'admin@blowhorn.net'

# AWS - S3 Bucket Config
# ------------------------------------------------------------------------------
AWS_S3_REGION_NAME = env('AWS_S3_REGION_NAME')
AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_BUCKET_NAME')
AWS_S3_OBJECT_PARAMETERS = {
    'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
    'CacheControl': 'max-age=94608000',
}
AWS_AUTO_CREATE_BUCKET = False
AWS_QUERYSTRING_AUTH = False

# Uploaded Media Folder
DEFAULT_FILE_STORAGE = 's3_folder_storage.s3.DefaultStorage'
DEFAULT_S3_PATH = "media"
MEDIA_ROOT = '/%s/' % DEFAULT_S3_PATH
MEDIA_URL = '//s3.amazonaws.com/%s/media/' % AWS_STORAGE_BUCKET_NAME

# Static media folder
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
# STATICFILES_STORAGE = 's3_folder_storage.s3.StaticStorage'
# STATIC_S3_PATH = "static"
# STATIC_ROOT = "/%s/" % STATIC_S3_PATH
# # STATIC_URL = '//s3.amazonaws.com/%s/static/' % AWS_STORAGE_BUCKET_NAME
# STATIC_URL = '//s3.%s.amazonaws.com/%s/static/' % (AWS_S3_REGION_NAME, AWS_STORAGE_BUCKET_NAME)

MAP_WIDGETS.update({
    "GOOGLE_MAP_API_KEY": GOOGLE_MAPS_API_KEY
})

# Celery Queue configuration
CELERY_QUEUES = (
    Queue('high', Exchange('high'), routing_key='high'),
    Queue('normal', Exchange('normal'), routing_key='normal'),
    Queue('low', Exchange('low'), routing_key='low'),
)

# Logging related changes

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'INFO',
        'handlers': ['console', 'sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': True,
        },
        'raven': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry'],
            'propagate': True,
        },
        'sentry.errors': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry'],
            'propagate': True,
        },
        'django.security.DisallowedHost': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry'],
            'propagate': True,
        },
    },
}

# Middleware to log all request, response details.
MIDDLEWARE += ["blowhorn.common.logging.middleware.RequestLoggingMiddleware"]

# Paytm settings
PAYTM_MERCHANT_KEY = env('PAYTM_MERCHANT_KEY', default='_EpuVfgXTejTGRM9')
PAYTM_MERCHANT_ID = env('PAYTM_MERCHANT_ID', default='CATBUS07328649070242')
PAYTM_WEBSITE = env('PAYTM_WEBSITE', default='WEB_STAGING')
HOST_URL = env('HOST_URL', default='http://localhost:8000')
PAYTM_CALLBACK_URL = HOST_URL + env('PAYTM_CALLBACK_URL', default='/api/paytm/')
PAYTM_ENDPOINT_TRANSN_REQUEST = env(
    'PAYTM_ENDPOINT_TRANSN_REQUEST', default='https://pguat.paytm.com/oltp-web/processTransaction')

PAYTM_ENDPOINT_TRANSN_WITHDRAWAL = env(
    'PAYTM_ENDPOINT_TRANSN_WITHDRAWAL', default='https://pguat.paytm.com/oltp/HANDLER_FF/withdrawScw')

PAYTM_REFUND_REQUEST_URL = env(
    'PAYTM_REFUND_REQUEST_URL', default='https://securegw.paytm.in/refund/process')

PAYTM_ENDPOINT_TRANSN_STATUS = env(
    'PAYTM_ENDPOINT_TRANSN_STATUS', default="https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus")
PAYTM_INDUSTRY_TYPE_ID = env('PAYTM_INDUSTRY_TYPE_ID', default="Retail109")
PAYTM_CHANNEL_ID = env('PAYTM_CHANNEL_ID', default='WEB')

# Payfast Payment Gateway
PAYFAST_MERCHANT_KEY = env('PAYFAST_MERCHANT_KEY', default='')
PAYFAST_MERCHANT_ID = env('PAYFAST_MERCHANT_ID', default='')
PAYFAST_CANCEL_URL = '/api/payfast/cancel'
PAYFAST_SUCCESS_URL = '/api/payfast/success'
PAYFAST_HOOK_URL = '/api/payfast/notify-payment/'

# Slack Channels
SLACK_CHANNELS = {
    'PAYMENTS': env('SLACK_CHANNEL_PAYMENTS', default='#tech-testing')
}

# MongoDB configuration.
DATABASES.update(
    {
        "activity": {
            'ENGINE': 'djongo',
            'NAME': env('MONGO_DB_NAME', default='blowhorn'),
            'HOST': env('MONGO_DB_HOST', default='localhost'),
            'USER': env('MONGO_DB_USER', default='admin'),
            'PASSWORD': env('MONGO_DB_PASSWORD', default='admin'),
        }
    }
)


# Load any server specific settings (if applicable)
try:
    from .local_settings import *
except ImportError:
    pass
