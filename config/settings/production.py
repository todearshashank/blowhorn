"""
Production Configurations

- Use Amazon's S3 for storing static files and uploaded media
- Use mailgun to send emails
- Use Redis for cache

- Use sentry for error logging


"""

# from boto.s3.connection import OrdinaryCallingFormat

import logging
import raven


from .base import *  # noqa
from celery.schedules import crontab

# HOST = 'https://blowhorn.com'
# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env('DJANGO_SECRET_KEY')

# This ensures that Django will be able to detect a secure connection
# properly on Heroku.
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# raven sentry client
# See https://docs.sentry.io/clients/python/integrations/django/
INSTALLED_APPS += ['raven.contrib.django.raven_compat', ]

# Use Whitenoise to serve static files
# See: https://whitenoise.readthedocs.io/
WHITENOISE_MIDDLEWARE = ['whitenoise.middleware.WhiteNoiseMiddleware', ]
MIDDLEWARE = WHITENOISE_MIDDLEWARE + MIDDLEWARE
RAVEN_MIDDLEWARE = ['raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware']
MIDDLEWARE = RAVEN_MIDDLEWARE + MIDDLEWARE


# EXOTEL Configurations
EXOTEL_BASEURL = ''
EXOTEL_SID = 'blowhorn5'
EXOTEL_TOKEN = 'bd8fa18e650fd7edec713b212c851522a940177f'


# SECURITY CONFIGURATION
# ------------------------------------------------------------------------------
# See https://docs.djangoproject.com/en/dev/ref/middleware/#module-django.middleware.security
# and https://docs.djangoproject.com/en/dev/howto/deployment/checklist/#run-manage-py-check-deploy

# set this to 60 seconds and then to 518400 when you can prove it works
SECURE_HSTS_SECONDS = 60
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
    'DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS', default=True)
SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
    'DJANGO_SECURE_CONTENT_TYPE_NOSNIFF', default=True)
SECURE_BROWSER_XSS_FILTER = True
# SESSION_COOKIE_SECURE = True
# SESSION_COOKIE_HTTPONLY = True
SECURE_SSL_REDIRECT = env.bool('DJANGO_SECURE_SSL_REDIRECT', default=True)
# CSRF_COOKIE_SECURE = True
# CSRF_COOKIE_HTTPONLY = True
X_FRAME_OPTIONS = 'SAMEORIGIN'

# SITE CONFIGURATION
# ------------------------------------------------------------------------------
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
# END SITE CONFIGURATION

INSTALLED_APPS += ['gunicorn', ]


# STORAGE CONFIGURATION
# ------------------------------------------------------------------------------
# Uploaded Media Files
# ------------------------
# See: http://django-storages.readthedocs.io/en/latest/index.html
INSTALLED_APPS += ['storages', ]

# Async Django email backend using celery
# See https://pypi.python.org/pypi/django-celery-email
INSTALLED_APPS += ['djcelery_email', ]

# # For ELASTIC_APM:
INSTALLED_APPS += ['elasticapm.contrib.django', ]
ELASTIC_APM = {'SERVER_URL': env('ELASTIC_APM_SERVER_URL', default=''),
               'SECRET_TOKEN': env('ELASTIC_APM_TOKEN', default=''),
               'ENVIRONMENT': env('ELASTIC_APM_ENV', default='TEST'),
               'DEBUG': env('ELASTIC_APM_DEBUG', default=True),
               'ELASTIC_APM_TRANSACTION_SAMPLE_RATE': env('ELASTIC_APM_TRANSACTION_SAMPLE_RATE', default=0.5),
               'ELASTIC_APM_METRICS_INTERVAL': env('ELASTIC_APM_METRICS_INTERVAL', default='2m'),
               'ELASTIC_APM_DISABLE_SEND': env('ELASTIC_APM_DISABLE_SEND', default=False),
               'ELASTIC_APM_API_REQUEST_SIZE': env('ELASTIC_APM_API_REQUEST_SIZE', default='200kb'),
               'ELASTIC_APM_API_REQUEST_TIME': env('ELASTIC_APM_API_REQUEST_TIME', default='10s')
               }

MIDDLEWARE += ['elasticapm.contrib.django.middleware.TracingMiddleware', ]
# EMAIL
# ------------------------------------------------------------------------------
DEFAULT_FROM_EMAIL = env('DJANGO_DEFAULT_FROM_EMAIL',
                         default='Blowhorn Logistics <noreply@blowhorn.com>')

B2C_DEFAULT_FROM_EMAIL = env('DJANGO_B2C_DEFAULT_FROM_EMAIL',
                         default='Blowhorn Store Front <noreply@blowhorn.com>')
EMAIL_SUBJECT_PREFIX = env('DJANGO_EMAIL_SUBJECT_PREFIX', default='[Blowhorn Logistics]')
SERVER_EMAIL = env('DJANGO_SERVER_EMAIL', default=DEFAULT_FROM_EMAIL)
EMAIL_HOST = env('DJANGO_EMAIL_HOST', default='smtp.gmail.com')
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND',
                    default='django.core.mail.backends.smtp.EmailBackend')
EMAIL_HOST_PASSWORD = env('DJANGO_EMAIL_PASSWORD', default='')
EMAIL_HOST_USER = env('DJANGO_HOST_USER', default=DEFAULT_FROM_EMAIL)
EMAIL_USE_TLS = True

# Anymail with Mailgun
INSTALLED_APPS += ['anymail', ]
ANYMAIL = {
    'MAILGUN_API_KEY': env('DJANGO_MAILGUN_API_KEY'),
    'MAILGUN_SENDER_DOMAIN': env('MAILGUN_SENDER_DOMAIN')
}

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See:
# https://docs.djangoproject.com/en/dev/ref/templates/api/#django.template.loaders.cached.Loader
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader', 'django.template.loaders.app_directories.Loader', ]),
]

# ------------------------------------------------------------------------------
# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': env.db('DATABASE_URL', default='postgres:///blowhorn'),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True
DATABASES['default']['NAME'] = env('DB_NAME', default='blowhorn')
# specify Postgis backend
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'

# Use the Heroku-style specification
# Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
#DATABASES['default'] = env.db('DATABASE_URL')

# Read replica config

if 'RDS_READ_REP_1' in os.environ:
    DATABASES.update(
        {
            'readrep1': {
                'ENGINE': 'django.contrib.gis.db.backends.postgis',
                'NAME': env('DB_NAME', default='blowhorn'),
                'USER': env('POSTGRES_USER', default='blowhorn'),
                'PASSWORD': env('POSTGRES_PASSWORD', default='blowhorn'),
                'HOST': env('RDS_READ_REP_1', default=''),
                'PORT': '5432',
            }
        }
    )

# Sentry Configuration
SENTRY_DSN = env('DJANGO_SENTRY_DSN')
SENTRY_CLIENT = env('DJANGO_SENTRY_CLIENT', default='raven.contrib.django.raven_compat.DjangoClient')
SENTRY_CELERY_LOGLEVEL = env.int('DJANGO_SENTRY_LOG_LEVEL', logging.ERROR)
RAVEN_CONFIG = {
    'CELERY_LOGLEVEL': env.int('DJANGO_SENTRY_LOG_LEVEL', logging.ERROR),
    'dsn': SENTRY_DSN,
    #    'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),

}

# Custom Admin URL, use {% url 'admin:index' %}
ADMIN_URL = env('DJANGO_ADMIN_URL')

# Your production stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

# Constants
DEFAULT_GOOGLE_MAPS_API_KEY_ADMIN = "AIzaSyCx1SpS_1X6_gqwMaNxWiE7TPLhgamMp0Y"  # For admin
# GOOGLE_MAPS_API_KEY = "AIzaSyCEKVvLTOuQDg-v-iOZcG5xOKwnYrx4S9s"
DEFAULT_GOOGLE_MAPS_API_KEY = "AIzaSyCHAdcIxi3dcpzs1LQBdI6y7iNrMmgFisE" # Prem-Key

GOOGLE_MAPS_API_KEY = env('GOOGLE_MAPS_API_KEY', default=DEFAULT_GOOGLE_MAPS_API_KEY)
GOOGLE_MAPS_API_KEY_ADMIN = env('GOOGLE_MAPS_API_KEY', default=DEFAULT_GOOGLE_MAPS_API_KEY_ADMIN)
GOOGLE_CLIENT_ID = env('GOOGLE_CLIENT_ID', default="309303081199-mfh2l774kgcfs49a37qne030nj7sf9iu.apps.googleusercontent.com")
IOS_GOOGLE_CLIENT_ID = env('IOS_GOOGLE_CLIENT_ID', default="309303081199-4vjijvjeru5ldht30gse6to3p9j5ith4.apps.googleusercontent.com")
GOOGLE_CLIENT_ID_COSMOS = env('GOOGLE_CLIENT_ID_COSMOS', default='')


RAZORPAY_KEY_ID = env('RAZORPAY_KEY_ID', default='')
RAZORPAY_SECRET_KEY = env('RAZORPAY_SECRET_KEY', default='')
RAZORPAY_WEBHOOK_SECRET = env('RAZORPAY_WEBHOOK_SECRET', default='')
RAZORPAY_SETTINGS = {
    'path': RAZORPAY_PATH,
    'id': RAZORPAY_KEY_ID,
    'secret': RAZORPAY_SECRET_KEY,
    'webhook_secret': RAZORPAY_WEBHOOK_SECRET
}

RAZORPAY_KEY_ID_RELIEF = env('RAZORPAY_KEY_ID_RELIEF', default='')
RAZORPAY_SECRET_KEY_RELIEF = env('RAZORPAY_SECRET_KEY_RELIEF', default='')
RAZORPAY_WEBHOOK_SECRET_RELIEF = env('RAZORPAY_WEBHOOK_SECRET_RELIEF', default='')
RAZORPAY_SETTINGS_RELIEF = {
    'path': RAZORPAY_PATH,
    'id': RAZORPAY_KEY_ID_RELIEF,
    'secret': RAZORPAY_SECRET_KEY_RELIEF,
    'webhook_secret': RAZORPAY_WEBHOOK_SECRET_RELIEF
}

RAZORPAY_ACCOUNTS = {
    'default': RAZORPAY_SETTINGS,
    'donation': RAZORPAY_SETTINGS_RELIEF
}

FIREBASE_DATABASE_URL = env('FIREBASE_DATABASE_URL', default='https://blowhorntemplate.firebaseio.com')
FIREBASE_APIKEY = env('FIREBASE_APIKEY', default="AIzaSyCoVOu3uXmeWPdb3JsHUxyPv9apKdw8DME")
FIREBASE_AUTHDOMAIN = env('FIREBASE_AUTHDOMAIN', default="blowhorntemplate.firebaseapp.com")
FIREBASE_STORAGEBUCKET = env('FIREBASE_STORAGEBUCKET', default="blowhorntemplate.appspot.com")
FIREBASE_MESSAGINGSENDERID = env('FIREBASE_MESSAGINGSENDERID', default="309303081199")
FIREBASE_USER_UID = env('FIREBASE_USER_UID',default="jl8Ra20DD8bst6mvC49GOG07Fzx1")
FIREBASE_ADMIN_USER_UID = env('FIREBASE_ADMIN_USER_UID', default="M3rfUvjKyxaBJ3l886h7ATfnOBE3")
FIREBASE_DATABASE_SECRET = env('FIREBASE_DATABASE_SECRET', default='SIQHKKmMRRYcGMrJ2k4ygp5nbwFzCLpngSEQUgHX')
FIREBASE_ADMIN_AUTH_EMAIL_UID = env('FIREBASE_ADMIN_AUTH_EMAIL_UID', default='YlZEUXBKpNgXE3BO6LBWQByjaf92')

FIREBASE_PRIVATE_KEY = env('FIREBASE_PRIVATE_KEY', default="-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCxrvuSQi6/0TmV\nRf+98WZVZ5j1T9IoCq1pkeu7NuQW/yX0HQFGpnmzRx9lLEqrYuH9tHyJCLQrsMYb\nxjPnqJFunoDR7SNjUvEqhWss1eWRYm3Q5fL/0WysKyKm75sm/gu2wEVWHGNCqMM9\nFW6yKzCDyw+ZYP3+GvPGWElQrw0MGUchbbD2jrqJEtzmP9se3pXTSFjFobbetgAg\ny35bbPXhfLy7ofu09IXRw6XkFDFHF7AT6KQXdoa91lJXWRvJOU+Mgkhgfz0Gd3fH\n5gfCer5i2mEoWHIr8xCIQzE44CknPqEwW+Vj5thGKq7kwa/mIn0YYHU6nakvhypZ\nmb24jm3VAgMBAAECggEBAIL0wrmjSa1aWSRpuPmXZ6Lefd+fevbEF9dNEv6ikC1a\n0/A9SGl33Q6DBuKcaFQ0xvckBP1CivgAi6KAzFSa6sUq0SrnqkNUKCb8umcLJu2m\nK+Xg9N4SzhN+n0WGJ0Xvy1ZgEzpKJeZFV9A3caSjLFiWWuLil5Cm8LBo9XAf9G/L\nHv8Vt0fdeZR1NODPAY7Pk8JHUBTEdIdRyroggFm/hx+n3wA1OTYpZ/TdLZrttx6v\nVIjZJCciZ6lRBJM3/YkT62FqvcUeEW7E0aNEqLr0ADZ7LGoi1kioqiv+vjwytyaj\nWRsREiIUHGzkqyGmrNEVpW6bg9QSvoAIgOWaQWMH8WkCgYEA3dz5ESRbjsps/l7Q\nRSZ6yvD7Yr94FYX5pZHYsdEi4TmYOnAylZCKwv2aKNhElgT31thVEQvsAAge78eN\nKe+s2L7SH99shznX+DHgplcL5u1VA53Xmwe0Qa2sgzZUTAGWNettYTMMaJFA+TIy\nXqMyiPms9U+3d9FEuQ7pz4MdaS8CgYEAzQXOUcEmr+JlwObry10CZZzSnTz+Uo7m\nYeTdhbW31rwvsOxtJ6XmLtSZ639P2D+ZZxHuTgxi21zaaGwXy015EE4jUhaxTddG\nUZIxXtXXncAQMEL3z/JmNCeB/MNFIjhPqrKDaGmtrjsKCh/mTGBL+tbGe19Th08F\npZH8tiN/0DsCgYEAiNBhjx6aj11pBRu21025Xzhn7SchiDRMFLvxowXSuoznwPG7\nX9v4lfIJhLKI6PI7W3OgUZjGeeaIHVkzRe8U6bwSm7ENnf4fnmAzpp/7KPal/Erz\n0BhaIj5VKbzxEsXPuYRquQvf1QPtyz4XLaD54toEvGniO7MC7BO0P1Eda+UCgYAz\nDTIN23nFrCtIOHgYgIV7ReyP6MMvTZQCZlne1Jv9ZYi+lnCrwbyqhpf5Mw9vuo55\nQtbDKoRnORzNibT3E8iEjeBvrWTjDCrfXvbaNYdmKZAeZF6Osj5O2FOe3ATX+4ui\n5qagFkiALQVX9g+Djyr8QATWbas/8yZlzw9NT1OhRwKBgQCNxoNlBzcpoJfPsULI\n5pXsBlCx9sfow9mXqTfS01QQ/622b7bPCTg6yBkY7p6qip/SZ70VKT7awM1sySJC\nJnQdzHuMjTyV1MH2vcpVsw3yqghQnZu0dunlryRnVqK6rwheIiCE3fd9ldaia+Qj\nalANc5pJvv/mzv5LaEm+NAXfpQ==\n-----END PRIVATE KEY-----\n")
SERVICE_ACCOUNT_EMAIL = env('SERVICE_ACCOUNT_EMAIL', default="firebase@blowhorntemplate.iam.gserviceaccount.com")
SERVICE_ACCOUNT_AUD = "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit"

FIREBASE_KEYS = {
    'driver': env('FIREBASE_DRIVER_KEY', default='rdAsXhMqCKW8p87QiQ05C3eCYvW2'),
    'customer': FIREBASE_USER_UID
}

FIREBASE_SERVER_KEY = env('FIREBASE_SERVER_KEY', default='AIzaSyAgqqtZ_Nat1j3PrCX_Cm_uh0N6jtu91m0')
# Push Notifications via Firebase using fcm-django package
FCM_DJANGO_SETTINGS = {
    # Default is set to the Driver APP SERVER Key
    "FCM_SERVER_KEY": FIREBASE_SERVER_KEY,
    # true if you want to have only one active device
    # per registered user at a time
    # default: False
    "ONE_DEVICE_PER_USER": True,
    # devices to which notifications cannot be sent,
    # are deleted upon receiving error response from FCM
    # default: False
    "DELETE_INACTIVE_DEVICES": True,
}

# Email addresses for sending email
RECEIPT_SENDER_ID = 'receipts@blowhorn.net'
ADMIN_SENDER_ID = 'admin@blowhorn.net'

# Email addresses for sending email
EMAIL_ID_CONTACT_US = env('EMAIL_ID_CONTACT_US', default='shoutout@blowhorn.net')
ADMIN_SENDER_ID = 'admin@blowhorn.net'

# AWS - S3 Bucket Config
# ------------------------------------------------------------------------------
AWS_S3_REGION_NAME = env('AWS_S3_REGION_NAME')
AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_BUCKET_NAME')
AWS_S3_CUSTOM_DOMAIN=env('AWS_S3_CUSTOM_DOMAIN')
AWS_CLOUDFRONT_KEY = "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAotrbGVAXJgdYlV3GLukSp+mTz3D+shjRlzMCYKZNhkU4pLQk\njoRSiiEKIeA+wWu+jvM0l5q3/8uDDGRE9jFk66rpdWX7/xurLMY3oNZeM/VS0KAb\nvvPGJVSrClTUF9dLB+mPFVU3XXwV8xyIgPeGf50+31r2dUOUTrIgPW90PFifhiWr\n/pG5jzKwxVNcZxlZi4wkjvGsLnQFvt61U9B/gGn97ZLXECoyXO2K8Fkt6ChH/PAM\nRu9lJ8KvSnPLKWzc7JUX60IjMM/c+KWj8RfPktTmplhl5sSlIuWehpCHohYGNGGn\nzq/11h/mIY1S13gzMupzuXIQ+T46sZljHYjEFwIDAQABAoIBAH8uZb8d8gYL5J05\nj6Vg9eXWH034k5+ojD4FYwzxs0dx1aui143JPuCKruwHwVqydy4SNvdSp/GHEL/j\nqouNxZ/aaNn7HFJ9XC/qWNUttIHlS5iMAT8eB5dtyOmXl89Knlj+iHSDiWpjloy2\nss/s/se4mLo7wo+PiEnxCAaSfPcGjAO9uYdiXWPbLC9oR+4PVhbKpgCpVS2q70Ez\nvitarWqL8Pw8nBIw3fQHZgTQEiyVyFtjM4JIe52GRHrWvGJkAlPNwjgoVKLhAG2q\nhLsM9Dpjyio1UnVCyItlIPIw4wsav2tNXYDETEqGiJpSjv2YgCV4gu5GnOiRAWrt\nlB1ShLkCgYEA0Se64rmw22DA+1mgdCrNJkWVjFE05P4HC+JQdATXroZhKMkDHMgi\naBlNyLeFlTb3b/jgU4XkumoUud6S8Rfk1DkAhdMUV1hS9e4NgpnCw9OzxwplHD6w\nImKbGrofP3/GbOUq/RuCZisIu8+P6ajz90j90uQy3T4MI9mvg936Q+0CgYEAx1Rr\nczoBQERHivZIb5vr+9yyDN91892hIDHxP/9c+Dmi5ah2YHBkBgq3V5nyd8059mVz\nI5x+Ps+Tph3O60d8BBSpNAZjuOme/l3gHCbAMbqeHLFvleRWFgeneP408XY/hTEU\nFHaoEP4Ai9jhDwwoBWdXs3Nyp75Op6hQ4ATgb5MCgYA+qcuZMA8nuvY2mCS1iTlL\nlR94GcBiNyV/Tv/Rsr234TTs1kSjc+azD5m5YBqamQxLNOJow73CiBPLmD6oEwOQ\n/b8FWTdH8ANzAS51CYJLz3qWUKv4aWO1+ZsEx+yixrJ5X7Ogf4Ry/cOxGT/BvsfR\nN8IsVAhVNsjGWOr39CKOBQKBgHlJB7xsDfjR0wpeAwNOR4/P4NLuYXIKaBET8mxz\ncFllkJXXJHhXyrTWbmqyNJPq3xrq7d08SqDjYz7PomkM7Qv6ml4aiTEpjeHOsx/O\nAGIB4FUrLe9xqWPsMl3vp71IqyvbChHNhUQZmbF+PKsQRYXMNNPvKlOg+3EOfG9i\n6EFbAoGBAK/Zexl2uIuApWVqvZfWk9REn4111fGvxd9If8oyNiop47BCjzRX5aPi\nDupzRh7H+zICE+X5c56cGqCy/yCDIDSi04I8X5cjdvH02HbDJ/7qlsFmKeVneFrO\n8o63Z+0uhWqq2Z11hbGr9wW9TGI0OY7Tt33YnyUwaWj/W50M0OVh\n-----END RSA PRIVATE KEY-----".encode('ascii')
AWS_CLOUDFRONT_KEY_ID = env('AWS_CLOUDFRONT_KEY_ID', None)
AWS_QUERYSTRING_EXPIRE = env('AWS_QUERYSTRING_EXPIRE', default=172800)


# AWS_S3_OBJECT_PARAMETERS = {
#     'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
#     'CacheControl': 'max-age=94608000',
# }
AWS_AUTO_CREATE_BUCKET = False
AWS_QUERYSTRING_AUTH = True
AWS_S3_SECURE_URLS = True
CDN_ENABLE=True
AWS_DEFAULT_ACL=None

# Uploaded Media Folder
DEFAULT_FILE_STORAGE = 's3_folder_storage.s3.DefaultStorage'
#DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
DEFAULT_S3_PATH = "media"
MEDIA_ROOT = '/%s/' % DEFAULT_S3_PATH
MEDIA_URL = '//s3.amazonaws.com/%s/media/' % AWS_STORAGE_BUCKET_NAME

# Static media folder
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
# STATICFILES_STORAGE = 's3_folder_storage.s3.StaticStorage'
# STATIC_S3_PATH = "static"
# STATIC_ROOT = "/%s/" % STATIC_S3_PATH
# # STATIC_URL = '//s3.amazonaws.com/%s/static/' % AWS_STORAGE_BUCKET_NAME
# STATIC_URL = '//s3.%s.amazonaws.com/%s/static/' % (AWS_S3_REGION_NAME, AWS_STORAGE_BUCKET_NAME)

MAP_WIDGETS.update({
    "GOOGLE_MAP_API_KEY": GOOGLE_MAPS_API_KEY
})

# Celery Queue configuration
CELERY_QUEUES = (
    Queue('high', Exchange('high'), routing_key='high'),
    Queue('normal', Exchange('normal'), routing_key='normal'),
    Queue('low', Exchange('low'), routing_key='low'),
)

# Logging related changes

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'INFO',
        'handlers': ['console', 'sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['console', 'sentry'],
            'level': 'WARNING',
            'propagate': True,
        },
        'raven': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry'],
            'propagate': True,
        },
        'sentry.errors': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry'],
            'propagate': True,
        },
        'django.security.DisallowedHost': {
            'level': 'WARNING',
            'handlers': ['console', 'sentry'],
            'propagate': True,
        },
    },
}

# Middleware to log all request, response details.
MIDDLEWARE += ["blowhorn.common.logging.middleware.RequestLoggingMiddleware"]

# Paytm settings
PAYTM_MERCHANT_KEY = env('PAYTM_MERCHANT_KEY', default='_EpuVfgXTejTGRM9')
PAYTM_MERCHANT_ID = env('PAYTM_MERCHANT_ID', default='CATBUS07328649070242')
PAYTM_WEBSITE = env('PAYTM_WEBSITE', default='WEB_STAGING')
HOST_URL = env('HOST_URL', default='http://localhost:8000')
PAYTM_CALLBACK_URL = HOST_URL + env('PAYTM_CALLBACK_URL', default='/api/paytm/')
PAYTM_ENDPOINT_TRANSN_REQUEST = env(
    'PAYTM_ENDPOINT_TRANSN_REQUEST', default='https://pguat.paytm.com/oltp-web/processTransaction')

PAYTM_ENDPOINT_TRANSN_WITHDRAWAL = env(
    'PAYTM_ENDPOINT_TRANSN_WITHDRAWAL', default='https://pguat.paytm.com/oltp/HANDLER_FF/withdrawScw')

PAYTM_REFUND_REQUEST_URL = env(
    'PAYTM_REFUND_REQUEST_URL', default='https://securegw.paytm.in/refund/process')

PAYTM_ENDPOINT_TRANSN_STATUS = env(
    'PAYTM_ENDPOINT_TRANSN_STATUS', default="https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus")
PAYTM_INDUSTRY_TYPE_ID = env('PAYTM_INDUSTRY_TYPE_ID', default="Retail109")
PAYTM_CHANNEL_ID = env('PAYTM_CHANNEL_ID', default='WEB')

# Payfast Payment Gateway
PAYFAST_MERCHANT_KEY = env('PAYFAST_MERCHANT_KEY', default='')
PAYFAST_MERCHANT_ID = env('PAYFAST_MERCHANT_ID', default='')
PAYFAST_CANCEL_URL = '/api/payfast/cancel'
PAYFAST_SUCCESS_URL = '/api/payfast/success'
PAYFAST_HOOK_URL = '/api/payfast/notify-payment/'

# Slack Channels
SLACK_CHANNELS = {
    'PAYMENTS': env('SLACK_CHANNEL_PAYMENTS', default='#tech-testing')
}

# MongoDB configuration.
DATABASES.update(
    {
        "activity": {
            'ENGINE': 'djongo',
            'CLIENT': {
                'name': env('MONGO_DB_NAME', default='blowhorn'),
                'host': env('MONGO_DB_HOST', default='localhost'),
                'username': env('MONGO_DB_USER', default='admin'),
                'password': env('MONGO_DB_PASSWORD', default='admin'),
                'authMechanism': 'SCRAM-SHA-1'
            }
        }
    }
)

# Load any server specific settings (if applicable)
try:
    from .local_settings import *
except ImportError:
    pass
