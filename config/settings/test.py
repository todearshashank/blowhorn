'''
Test settings

- Used to run tests fast on the continuous integration server and locally
'''

from .base import *  # noqa


# DEBUG
# ------------------------------------------------------------------------------
# Turn debug off so tests run faster
DEBUG = False
TEMPLATES[0]['OPTIONS']['debug'] = False

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = env('DJANGO_SECRET_KEY', default='CHANGEME!!!')

# Mail settings
# ------------------------------------------------------------------------------
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# In-memory email backend stores messages in django.core.mail.outbox
# for unit testing purposes
EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

# CACHING
# ------------------------------------------------------------------------------
# Speed advantages of in-memory caching without having to run Memcached
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# TESTING
# ------------------------------------------------------------------------------
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# SQLLite DATABASE for testing ## Could not get it working with ArrayField
# Resorting back to postgres
#-------------------------------------------------------------------------------
DATABASES = {
    'default': env.db('DATABASE_URL', default='postgres:///blowhorn')
}
DATABASES['default']['ATOMIC_REQUESTS'] = True
DATABASES['default']['NAME'] = env('DB_NAME', default='blowhorn')
# specify Postgis backend
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'
DATABASES['default']['HOST'] = 'postgres'
DATABASES['default']['USER'] = 'blowhorn'

# Disable migrations
MIGRATION_MODULES = {
    'blowhorn.order': None,
    'blowhorn.route_optimiser': None,
    'blowhorn.basket': None,
    'blowhorn.contract': None,
    'blowhorn.freshchat': None,
    'blowhorn.wms': None,
    'blowhorn.vehicle': None,
    'blowhorn.changelog': None,
    'blowhorn.driver': None,
    'blowhorn.activity': None,
    'blowhorn.payment': None,
    'blowhorn.shopify': None,
    'blowhorn.document': None,
    'blowhorn.coupon': None,
    'blowhorn.exotel': None,
    'blowhorn.logapp': None,
    'blowhorn.contrib.sites.migrations': None,
    'blowhorn.trip': None,
    'blowhorn.users': None,
    'blowhorn.address': None,
    'blowhorn.stock': None,
    'blowhorn.integration_apps': None,
    'blowhorn.apps': None,
    'blowhorn.apps.customer_app.v1': None,
    'blowhorn.apps.operation_app.v1': None,
    'blowhorn.apps.operation_app.v2': None,
    'blowhorn.apps.operation_app.v3': None,
    'blowhorn.apps.kiosk.migrations': None,
    'blowhorn.apps.driver_app.v1': None,
    'blowhorn.customer': None,
    'blowhorn.company': None,
    'website': None
}
# PASSWORD HASHING
# ------------------------------------------------------------------------------
# Use fast password hasher so tests run faster
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]

# TEMPLATE LOADERS
# ------------------------------------------------------------------------------
# Keep templates in memory so tests run faster
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ['django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    ], ],
]

# REmove these from production.py and move to some vault / AWS secret keys
#------------------------------------------------------------------------------
GOOGLE_MAPS_API_KEY_ADMIN="nonsense"
GOOGLE_MAPS_API_KEY="nonsense"
AWS_ACCESS_KEY_ID="nonsense"
AWS_SECRET_ACCESS_KEY="nonsense"
SERVICE_ACCOUNT_EMAIL="nonsense"
FIREBASE_PRIVATE_KEY="nonsense"
