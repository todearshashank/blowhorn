from celery.schedules import crontab

beat_schedule = {
    "generate_expected_margin": {
        "task": "blowhorn.contract.tasks.generate_expected_margin",
        "schedule": crontab(hour="1", minute="30"),
    },
    "generate_subscription_orders": {
        "task": "blowhorn.contract.tasks.generate_subscription_orders",
        "schedule": crontab(hour="5, 11, 17, 23", minute="30"),
    },
    "deactivate_contract": {
        "task": "blowhorn.contract.tasks.deactivate_contract",
        "schedule": crontab(minute="29", hour="20"),
    },
    "make_reminder_calls": {
        "task": "blowhorn.contract.tasks.make_reminder_calls",
        "schedule": crontab(minute="*/60"),
    },
    "calculate_duty_hrs": {
        "task": "blowhorn.contract.tasks.calculate_duty_hrs",
        "schedule": crontab(minute=15, hour=23),
    },
    "mail_customer_reports": {
        "task": "blowhorn.customer.tasks.mail_customer_reports",
        "schedule": crontab(hour="02", minute="30"),
    },
    "generate_customer_reports": {
        "task": "blowhorn.customer.tasks.generate_customer_reports",
        "schedule": crontab(hour="19", minute="00"),
    },
    "generate_customer_invoices": {
        "task": "blowhorn.customer.tasks.generate_customer_invoices",
        "schedule": crontab(hour="23", minute="00"),
    },
    "push_notification_to_customer": {
        "task": "blowhorn.customer.tasks.push_notification_to_customer",
        "schedule": crontab(hour="03", minute="30", day_of_week="1,5"),
    },
    "expire_invoices": {
        "task": "blowhorn.customer.tasks.expire_invoices",
        "schedule": crontab(minute="59", hour="20"),
    },
    "assign_revenue_month_to_invoices": {
        "task": "blowhorn.customer.tasks.assign_revenue_month_to_invoices",
        "schedule": crontab(minute="25", hour="18"),
    },
    "booking_order_payment_due_notifications": {
        "task": "blowhorn.customer.tasks.booking_order_payment_due_notifications",
        "schedule": crontab(hour="10", minute="0", day_of_week='sunday'),
    },
    "close_invoices": {
        "task": "blowhorn.customer.tasks.close_invoices",
        "schedule": crontab(hour="23", minute="00"),
    },
    "add_cashfree_benificiary":{
        "task": "blowhorn.customer.tasks.add_cashfre_benificiary",
        "schedule": crontab(hour="22", minute="00"),
    },
    "cashfree_payout_transfer":{
        "task": "blowhorn.customer.tasks.cashfree_payout_transfer",
        "schedule": crontab(hour="22", minute="00")
    },
    "send_expiry_alert": {
        "task": "blowhorn.document.tasks.send_expiry_alert",
        "schedule": crontab(hour="1", minute="30"),
    },
    "deactivate_driver_payments": {
        "task": "blowhorn.driver.tasks.deactivate_driver_payments",
        "schedule": crontab(hour="23", minute="30"),
    },
    "generate_driver_payments": {
        "task": "blowhorn.driver.tasks.generate_driver_payments",
        "schedule": crontab(hour="1,4,7,9,14", minute="30"),
    },
    "close_unsettled_payment": {
        "task": "blowhorn.driver.tasks.close_unsettled_payment",
        "schedule": crontab(hour="20", minute="30"),
    },
    "driver_gps_distance": {
        "task": "blowhorn.driver.tasks.driver_gps_distance",
        "schedule": crontab(hour="2,6,10,14", minute="15"),
    },
    "clear_driver_activity": {
        "task": "blowhorn.driver.tasks.clear_driver_activity",
        "schedule": crontab(hour="19", minute="30"),
    },
    "clear_dashboard_data_from_firebase": {
        "task": "blowhorn.customer.tasks.clear_dashboard_data_from_firebase",
        "schedule": crontab(hour="22", minute="30"),
    },
    "deactivate_drivers": {
        "task": "blowhorn.driver.tasks.deactivate_drivers",
        "schedule": crontab(hour="0, 18, 21", minute="15"),
    },
    "create_leads": {
        "task": "blowhorn.driver.tasks.create_leads",
        "schedule": crontab(hour="20", minute="30"),
    },
    "expire_driver_event_status": {
        "task": "blowhorn.driver.tasks.expire_driver_event_status",
        "schedule": crontab(hour="19", minute="30"),
    },
    "push_notification_to_driver": {
        "task": "blowhorn.driver.tasks.push_notification_to_driver",
        "schedule": crontab(hour="05", minute="00"),
    },
    "update_order_to_expired": {
        "task": "blowhorn.order.tasks.update_order_to_expired",
        "schedule": crontab(hour="21", minute="30"),
    },
    "index_orders": {
        "task": "blowhorn.order.tasks.index_orders",
        "schedule": crontab(hour="1, 3, 5, 7, 9, 11, 13, 15 ,17, 19, 21, 23", minute="30"),
    },
    "update_call_logs": {
        "task": "blowhorn.order.tasks.update_call_logs",
        "schedule": crontab(hour="21", minute="30"),
    },
    "send_order_pod_webhooks": {
        "task": "blowhorn.order.tasks.send_order_pod_webhooks",
        "schedule": crontab(hour="01", minute="30")
    },
    "update_paytm_transaction_status": {
        "task": "blowhorn.payment.tasks.update_paytm_transaction_status",
        "schedule": crontab(minute='*/15'),
    },
    "update_donation_payments": {
        "task": "blowhorn.payment.tasks.update_donation_payments",
        "schedule": crontab(minute=0, hour=0),
    },
    "mark_inactive_trips": {
        "task": "blowhorn.trip.tasks.mark_inactive_trips",
        "schedule": crontab(minute="00", hour="01, 16"),
    },
    "generate_consignments": {
        "task": "blowhorn.trip.tasks.generate_consignments",
        "schedule": crontab(minute="29", hour="18"),
    },
    "calculate_margin": {
        "task": "blowhorn.trip.tasks.calculate_margin",
        "schedule": crontab(minute=0, hour=23),
    },
    "upload_to_gst_portal_automation": {
        "task": "blowhorn.customer.tasks.upload_to_gst_portal_automation",
        "schedule": crontab(minute="30", hour="11"),
    },
    "send_invoices_to_customers": {
        "task": "blowhorn.customer.tasks.send_invoices_to_customers",
        "schedule": crontab(minute="30", hour="6"),
    },
    "send_credit_breach_mails": {
        "task": "blowhorn.customer.tasks.send_credit_breach_mails",
        "schedule": crontab(minute="30", hour="03"),
    },
    "update_invoices_status_with_credit_notes": {
        "task": "blowhorn.customer.tasks.update_invoices_status_with_credit_notes",
        "schedule": crontab(minute="30", hour="02")
    },
    "shopify_reconciliation": {
        "task": "blowhorn.shopify.tasks.shopify_reconciliation",
        "schedule": crontab(hour="19", minute="00"),
    },
}
