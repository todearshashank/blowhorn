# System and Django libraries
import re
import json
import logging
import requests
import traceback
import itertools
from datetime import datetime
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q, Count, F
from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.response import TemplateResponse
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html


# 3rd Party libraries
import razorpay
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from blowhorn.oscar.core.loading import get_model
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.authentication import BasicAuthentication

# Blowhorn Modules
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.parsers import PreserveRawBodyJSONParser
from blowhorn.common.utils import get_repr, get_field
from blowhorn.common.sms_templates import APP_DOWNLOAD_LINK_TEMPLATE
from blowhorn.apps.driver_app.v2.helpers.trip import get_asset_for_trip
from blowhorn.apps.customer_app.v2.views import CustomerProfileInformation
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME
from blowhorn.address.models import get_closed_hour, State
from blowhorn.address.utils import get_city_details, get_cities_geojson, \
    get_city_coverage, get_city_contact_details, AddressUtil, \
    get_city_from_geopoint
from blowhorn.address.constants import country_calling_codes
from blowhorn.common.communication import Communication
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.customer.helpers.permission_helper import PermissionBackend
from blowhorn.customer.views import get_customer_from_api_key, CustomerProfile
from blowhorn.customer.utils import MyfleetHelper, create_customer
from blowhorn.order.utils import OrderUtil
from blowhorn.contract.utils import ContractUtils
from blowhorn.customer.constants import PAYMENT_URL_SEPARATOR
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.order.mixins import ItemCategoryMixin
from blowhorn.payment.mixins import PaymentMixin, DONATION
from blowhorn.utils.datetime_function import DateTime
from blowhorn.company.models import News
from blowhorn.trip.models import Trip, Stop
from blowhorn.order.models import OrderContainer, OrderLine
from website.constants import IOS_REGEX, ANDROID_REGEX, \
    BENGALURU, HYDERABAD, CHENNAI, DELHI, MUMBAI, \
    LANGUAGE_CODES_FOR_TEMPLATES, SEO_DATA, DASHBOARD_CONFIG, LANGUAGE_CODE_FIELD_MAPPING
from blowhorn.driver.constants import TEXT_CONTENT, IN_APP
from blowhorn.common.utils import get_repr, get_field
from blowhorn.utils.html_to_pdf import render_to_pdf

City = get_model('address', 'City')
Hub = get_model('address', 'Hub')
Customer = get_model('customer', 'Customer')
User = get_model('users', 'User')
Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Contract = get_model('contract', 'Contract')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def custom_404(request, exception):

    cities = list(City.objects.all())
    customer = CustomerMixin().get_customer(request.user)
    if customer and customer.is_SME():
        contract_types = [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]
    else:
        contract_types = [CONTRACT_TYPE_C2C]

    _dict = {
        'vehicle_classes': ContractUtils.get_sell_packages(
            cities=cities, contract_types=contract_types, customer=customer)
    }
    context = {
        'params': json.dumps(_dict)
    }
    return render(request, 'website/404_not_found.html', context, status=404)


def get_params():
    cities = get_city_coverage()
    _dict = {
        'cities': cities
    }
    return _dict


def dashboard_context_data(request, context, customer=None):
    user = request.user
    uastring = request.META.get('HTTP_USER_AGENT', '')

    viewport_user_scalable = ''
    if uastring and ('Mobile' in uastring and 'Safari' in uastring):
        viewport_user_scalable = 'user-scalable=no'
    context['viewport_user_scalable'] = viewport_user_scalable

    is_authenticated = user and user.is_authenticated
    is_staff = is_authenticated and user.is_staff
    is_non_individual, is_business_user = False, False
    is_org_admin, is_org_owner = False, False
    customer_name, customer_id = None, None
    logged_user = {}
    permissions = {}
    cancellation_reasons = []
    modules = []
    if is_authenticated:
        logged_user = {
            'id': user.pk,
            'name': user.name,
            'email': user.email
        }
        if not is_staff:
            customer = CustomerMixin().get_customer(user)
            if customer:
                is_non_individual = customer.is_non_individual()
                is_business_user = MyfleetHelper().is_business_user(customer)
                is_org_admin = user.is_customer() or customer.is_admin(user)
                is_org_owner = user.is_customer()
                customer_id = customer.id
                customer_name = user.name
                cancellation_reasons = OrderUtil().fetch_order_cancellation_reasons()
                modules = CustomerMixin().get_available_user_modules(
                    query_params={
                        'system__code': 'TMS'
                    },
                    fields=['id', 'code', 'display_order'],
                    ordering=['display_order']
                )
                if not is_org_admin:
                    permissions = CustomerProfile().get_user_permissions(user.pk)

    context['customer'] = customer_name
    context['customer_id'] = customer_id
    context['is_staff'] = is_staff
    context['is_org_admin'] = is_org_admin
    context['is_org_owner'] = is_org_owner
    context['is_authenticated'] = is_authenticated
    context['is_business_user'] = is_business_user
    context['is_non_individual'] = is_non_individual
    context['logged_user'] = logged_user
    context['google_map_api_key'] = settings.GOOGLE_MAPS_API_KEY
    context['params'] = json.dumps(get_params())
    context['homepage_url'] = settings.HOMEPAGE_URL
    context['default_currency'] = settings.OSCAR_DEFAULT_CURRENCY
    context['country_code'] = settings.COUNTRY_CODE_A2
    context['isd_dialing_code'] = settings.ACTIVE_COUNTRY_CODE
    context['current_build'] = settings.WEBSITE_BUILD
    context['build_flavour'] = settings.BUILD_FLAVOUR
    context['font_name'] = settings.WEBSITE_FONT
    context['support_email'] = settings.COMPANY_SUPPORT_EMAIL
    context['terms_of_service_url'] = settings.TERMS_OF_SERVICE_URL
    context['brand_name'] = settings.BRAND_NAME
    context['modules'] = list(modules)
    context['permissions'] = json.dumps(permissions)
    config = DASHBOARD_CONFIG.get(customer and customer.name) or {}
    context['dashboard_config'] = json.dumps(config)
    payment_methods = settings.PAYMENT_METHODS
    if isinstance(payment_methods, str):
        payment_methods = json.loads(payment_methods)
    context['payment_methods'] = payment_methods
    context['cancellation_reasons'] = cancellation_reasons
    return context


class ParametersView(APIView, ItemCategoryMixin):
    """
        API to get city coverage details
    """
    authentication_classes = ()
    permission_classes = (AllowAny,)

    def get(self, request):
        city_coverage, contact_details = get_city_details()
        items = self.get_item_categories()
        response = {
            'cities': get_cities_geojson(),
            'items': list(items) if items else [],
            'vehicle_classes': [],
            'city_coverage': city_coverage,
            'driver_rating_config': CustomerProfileInformation()._get_ratingwise_driver_remarks(order_type=settings.SHIPMENT_ORDER_TYPE)
        }
        return Response(response)


class Slots(APIView):
    """
        API to get timeslots for a given city
    """
    authentication_classes = ()
    permission_classes = ()

    def get(self, request):
        data = request.query_params
        time_func = DateTime()
        pickup_latitude = data.get('from-lat')
        pickup_longitude = data.get('from-lon')
        pickup_geopoint = GEOSGeometry(
            'POINT(%s %s)' % (pickup_longitude, pickup_latitude))
        current_city = get_city_from_geopoint(pickup_geopoint,
                                              field='coverage_pickup')
        num_days_future = current_city.future_booking_days
        start_hour = int(current_city.start_time.strftime('%H'))
        end_hour = int(current_city.end_time.strftime('%H'))

        start_minute = current_city.start_time.minute
        end_minute = current_city.end_time.minute
        today = time_func.get_locale_datetime(datetime.now())
        max_date_to_show = time_func.add_timedelta(
            today, {'days': num_days_future})
        closed_datetime_list = get_closed_hour(current_city,
                                               max_date_to_show.date())

        slots = {}
        for i in range(num_days_future):
            date = time_func.add_timedelta(today, {'days': i})
            this_day_slots = []
            this_day_closed_hours = {}
            if closed_datetime_list:
                for data in closed_datetime_list:
                    if date.date() == data[0]:
                        this_day_closed_hours = {
                            'from': int(data[1].strftime('%H')),
                            'to': int(data[2].strftime('%H')),
                        }

            for t in range(24):
                time_24hr = datetime.strptime("%s" % t, "%H")
                time_12hr = time_24hr.strftime("%I%p")
                slot_status = ''
                outside_city_service_hours = t < start_hour or t > end_hour
                inside_closed_hours = (
                    this_day_closed_hours and
                    (this_day_closed_hours.get('from') <= t <=
                        this_day_closed_hours.get('to'))
                )
                if outside_city_service_hours or inside_closed_hours:
                    slot_status = 'disabled'
                else:
                    slot_status = 'enabled'

                this_day_slots.append({
                    't': t,
                    'time': time_12hr,
                    'status': slot_status,
                })
                slots[i] = this_day_slots

        _dict = {
            'slots': slots,
            'start_minute': start_minute,
            'end_minute': end_minute,
            'max_advance_booking_days': num_days_future
        }
        return Response(status=status.HTTP_200_OK, data=_dict,
                        content_type="application/json")


class TrackOrder(TemplateView):
    template_name = "website/track.html"

    def get(self, request):
        user_details = {}
        user = self.request.user
        if user and user.is_authenticated:
            user_details = {
                'name': user.name,
                'email': user.email,
                'mobile': user.national_number
            }

        context = {
            'user_details_json': json.dumps(user_details),
            'user_id': user_details.get('email') if user_details else None,
            'key_id': settings.RAZORPAY_KEY_ID,
            'google_map_api_key': settings.GOOGLE_MAPS_API_KEY,
            'firebase_apikey': settings.FIREBASE_APIKEY,
            'firebase_authdomain': settings.FIREBASE_AUTHDOMAIN,
            'firebase_messagingsenderid': settings.FIREBASE_MESSAGINGSENDERID,
            'firebase_storagebucket': settings.FIREBASE_STORAGEBUCKET,
            'firebase_database_url': settings.FIREBASE_DATABASE_URL,
            'cancellation_reasons': OrderUtil().fetch_order_cancellation_reasons()
        }
        return TemplateResponse(request, self.template_name, context)


class MyTripsView(TemplateView):
    """
        Template renderer for loading dashboard for logged in user
    """
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(MyTripsView, self).get_context_data(**kwargs)
        user = self.request.user
        context['is_staff'] = user and user.is_authenticated and user.is_staff
        context['cancellation_reasons'] = OrderUtil().fetch_order_cancellation_reasons()
        return context


class BlowhornAuthView(TemplateView, CustomerMixin):

    """
        Angular5 Template renderer for authenticated users.
    """
    template_name = "home_blowhorn.html"

    def get_params(self):
        cities = get_city_coverage()
        _dict = {
            'cities': cities
        }
        return _dict

    def get_context_data(self, **kwargs):
        context = super(BlowhornAuthView, self).get_context_data(**kwargs)
        return dashboard_context_data(self.request, context)

    def get(self, request, *args, **kwargs):
        user = request.user
        if user:
            # @todo need to pass `next` as param and redirect after login.
            #  Need changes in home page controller
            if not user.is_authenticated:
                return redirect('/')

            if user.is_customer:
                return TemplateResponse(request, self.template_name,
                                        self.get_context_data())
            else:
                return TemplateResponse(request, '403_csrf.html',
                                        status=status.HTTP_401_UNAUTHORIZED)
        return redirect('/')

class BlowhornAdminView(TemplateView):

    """
        Angular5 Template renderer for admin users.
    """
    template_name = "home_blowhorn.html"

    def get_context_data(self, **kwargs):
        context = super(BlowhornAdminView, self).get_context_data(**kwargs)
        return dashboard_context_data(self.request, context)

    def get(self, request, *args, **kwargs):
        """
        A. if user obj present and
            1. not authenticated, redirect to /admin/login
            2. authenticated and
                a. is staff, load the page
                b. not staff, show forbidden page
        B. if no user obj (not even AnonymousUser), redirect to /admin/login
        """
        user = request.user
        if user:
            if not user.is_authenticated:
                return redirect('/admin/login/?next=%s' % request.path)

            if user.is_staff:
                return TemplateResponse(request, self.template_name,
                                        self.get_context_data())

            else:
                return TemplateResponse(request, '403_csrf.html',
                                        status=status.HTTP_401_UNAUTHORIZED)
        return redirect('/')


class BlowhornView(TemplateView, CustomerMixin):

    """
        Angular5 Template renderer for unauthenticated users.
    """
    template_name = "home_blowhorn.html"

    def get_context_data(self, **kwargs):
        context = super(BlowhornView, self).get_context_data(**kwargs)
        return dashboard_context_data(self.request, context)

    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, self.template_name,
                                self.get_context_data())


class WmsDashboardView(TemplateView, CustomerMixin):
    template_name = "wms/index.html"

    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, self.template_name)

@method_decorator(xframe_options_exempt, name='dispatch')
class BlowhornTokenAuthView(TemplateView, CustomerMixin):
    """
        Angular5 Template renderer for unauthenticated users using api-key.
    """
    template_name = "home_blowhorn.html"

    def get_context_data(self, **kwargs):
        context = super(BlowhornTokenAuthView, self).get_context_data(**kwargs)
        return dashboard_context_data(self.request, context, self.customer)

    def get(self, request, *args, **kwargs):
        api_key = request.META.get('HTTP_API_KEY', None)
        order_number = kwargs.get('order_id', None)
        print('BlowhornTokenAuthView --> ', request.__dict__, kwargs)
        order = Order.objects.filter(number=order_number).first()
        self.customer = None
        if order:
            self.customer = order.customer

        # customer = get_customer_from_api_key(api_key) or \
        #     self.get_customer(request.user)
        # if not customer:
        #     return TemplateResponse(request, '403_csrf.html',
        #         status=status.HTTP_401_UNAUTHORIZED)

        # order_qs = Order.objects.filter(number=order_number, customer=customer)
        # if not order_qs.exists():
        #     return TemplateResponse(request, '404.html',
        #         status=status.HTTP_400_BAD_REQUEST)
        context = self.get_context_data()
        return TemplateResponse(request, self.template_name, context)

class MyfleetView(TemplateView):

    """
        Template renderer for loading tracking page for logged in b2b user
    """
    template_name = "website/myfleet.html"

    def get(self, request):
        if request.user and request.user.is_authenticated:
            context = {
                'user': request.user.name,
                'firebase_apikey': settings.FIREBASE_APIKEY,
                'firebase_authdomain': settings.FIREBASE_AUTHDOMAIN,
                'firebase_messagingsenderid':
                    settings.FIREBASE_MESSAGINGSENDERID,
                'firebase_storagebucket': settings.FIREBASE_STORAGEBUCKET,
                'firebase_database_url': settings.FIREBASE_DATABASE_URL,
                'google_map_api_key': settings.GOOGLE_MAPS_API_KEY,
                'uid': request.user.email
            }
            return TemplateResponse(request, self.template_name, context)
        else:
            return TemplateResponse(request, '403_csrf.html',
                                    status=status.HTTP_401_UNAUTHORIZED)


@method_decorator(xframe_options_exempt, name='dispatch')
class HomeView(TemplateView, CustomerMixin, ItemCategoryMixin):
    template_name = "website/index.html"
    template_404_name = "website/404_not_found.html"

    def _get_contract_types(self, customer=None):
        # if customer and customer.is_SME():
        return [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]
        # else:
        #     return [CONTRACT_TYPE_C2C]

    def get_params(self, cities=None, is_authenticated=False, is_embed=False, load_home=True):
        _dict = {
            'offerbanner': {},
        }
        if is_embed and not load_home:
            from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE
            contract_cities = Contract.objects.filter(
                contract_type=CONTRACT_TYPE_C2C,
                status=CONTRACT_STATUS_ACTIVE
            ).select_related('city').values_list('city__name', flat=True)
            _dict['contract_cities'] = list(set(contract_cities))
            return _dict

        customer = self.get_customer(self.request.user)
        city_coverage, city_contacts = get_city_details(cities=cities)
        _dict['city_coverage'] = city_coverage
        _dict['city_contact_details'] =  city_contacts
        items = self.get_item_categories()
        vehicle_classes = ContractUtils.get_sell_packages(
            cities=cities,
            contract_types=self._get_contract_types(customer),
            customer=customer,
            is_authenticated=is_authenticated
        )

        if not isinstance(vehicle_classes, list):
            vehicle_classes = [vehicle_classes]
        _dict.update({
            'items': list(items) if items else [],
            'vehicle_classes': vehicle_classes
        })
        return _dict

    def rebook_json_formatter(self, address):
        """ Function to convert address, geopoint, contact to json """

        return {
            'address': {
                'full_address': address.line1,
                'area': address.line2,
                'postal_code': address.postcode,
                'city': address.line4,
                'landmark': '',
            },
            'contact': AddressUtil().get_contact_from_address(address),
            'geopoint':
                AddressUtil().get_latlng_from_geopoint(address.geopoint)
        }

    def _get_cities(self):
        """
        :return: queryset of City instances
        """
        cities = City.objects.get_super_queryset().select_related(
            'address', 'default_contract', 'default_vehicle_class')
        return cities.only('name', 'coverage_pickup', 'coverage_dropoff',
                           'future_booking_days', 'contact', 'default_contract',
                           'default_vehicle_class', 'address',
                           'loading_mins', 'unloading_mins')\
            .exclude(future_booking_days=0)

    def __get_rebook_data(self, order_number):
        order = OrderUtil().get_order_from_friendly_id(order_number)
        if order:
            contract = order.customer_contract
            order_city = '%s' % contract.city
            waypoints = sorted(
                order.waypoint_set.all(),
                key=lambda x: x.id)

            shipping_address_str = ','.join(
                order.shipping_address.active_address_fields())
            waypoints = [
                self.rebook_json_formatter(x.shipping_address)
                for x in waypoints if (','.join(
                    x.shipping_address.active_address_fields()) !=
                                       shipping_address_str)
            ]
            return {
                'pickup': self.rebook_json_formatter(order.pickup_address),
                'dropoff': self.rebook_json_formatter(order.shipping_address),
                'waypoints': waypoints,
                'items': [],
                'vehicle_class':
                    order.vehicle_class_preference.commercial_classification,
                'city': '%s' % order_city if order_city else '',
                'contract_id': contract.id,
            }
        return {}

    def __get_next_url(self, user, skip_dashboard_redirection):
        if user.is_staff:
            return '/admin'

        if skip_dashboard_redirection != 'yes':
            customer = self.get_customer(user)
            if customer:
                is_non_individual = customer.is_non_individual()
                return '/dashboard' if is_non_individual else '/dashboard/mytrips'

        return '/'

    def __process_embed_request(self, request, context, is_authenticated):
        context['host'] = request.META.get('HTTP_HOST') or settings.HOST
        embed_type = request.GET.get('type', None)
        skip_dashboard_redirection = request.GET.get('skip_dashboard_redirection', 'no')
        if embed_type == 'myaccount':
            if is_authenticated:
                next_url = self.__get_next_url(request.user, skip_dashboard_redirection)
                return redirect(next_url)

            return TemplateResponse(
                request, 'website/auth_index.html', context
            )
        elif embed_type in ['booking', 'bookingform']:
            return TemplateResponse(request, 'website/index.html', context)

    def get(self, request, city=''):
        rebook_data = None
        context = {}
        uastring = self.request.META.get('HTTP_USER_AGENT')
        request_from_embed = request.GET.get('embed', False)
        book_now = request.path == '/book'
        viewport_user_scalable = ''
        user = self.request.user
        is_authenticated = user and user.is_authenticated
        if uastring and ('Mobile' in uastring and 'Safari' in uastring):
            viewport_user_scalable = 'user-scalable=no'
        context['viewport_user_scalable'] = viewport_user_scalable
        if not request_from_embed and not book_now and settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
            return TemplateResponse(request, 'dist/index.html', context)

        if not city:
            city = request.GET.get('city', '')
        embed_type = request.GET.get('type', None)
        order_number = request.GET.get('rebook', '')
        if order_number:
            rebook_data = self.__get_rebook_data(order_number)

        cities = self._get_cities()
        context['country_code'] = settings.COUNTRY_CODE_A2
        context['isd_dialing_code'] = settings.ACTIVE_COUNTRY_CODE
        context['default_currency'] = settings.OSCAR_DEFAULT_CURRENCY
        context['active_timezone'] = settings.ACT_TIME_ZONE
        context['website_build'] = settings.WEBSITE_BUILD
        context['brand_name'] = settings.BRAND_NAME
        context['is_embed'] = request_from_embed
        context['load_home'] = not embed_type or embed_type == 'bookingform'
        context['homepage_url'] = settings.HOMEPAGE_URL
        context['city'] = city
        context['rebook'] = json.dumps(rebook_data)
        context['seo'] = SEO_DATA.get(settings.WEBSITE_BUILD, {})
        context['states'] = list(State.objects.values('name', 'id').order_by('name'))
        global_params = self.get_params(
            cities,
            is_authenticated=False,
            is_embed=request_from_embed,
            load_home=context.get('load_home', True)
        )
        context['params'] = json.dumps(global_params)
        context['user_logged_in'] = is_authenticated
        context['is_staff'] = is_authenticated and user.is_staff
        context['b_user_logged_in'] = is_authenticated
        context['google_client_id'] = settings.GOOGLE_CLIENT_ID
        context['google_map_api_key'] = settings.GOOGLE_MAPS_API_KEY
        context['fresh_chat_data'] = settings.FRESHCHAT_WEB_CONFIG

        # Analytics and Tag Manager
        context['google_tag_manager_key_old'] = settings.GOOGLE_TAG_MANAGER_KEY_OLD
        context['fb_conversion_key_old'] = settings.FB_CONVERSION_KEY_OLD
        context['google_tag_manager_key'] = settings.GOOGLE_TAG_MANAGER_KEY
        context['fb_conversion_key'] = settings.FB_CONVERSION_KEY
        if request_from_embed:
            context['bg_url'] = request.GET.get('bg_url', '')
            return self.__process_embed_request(
                request, context, is_authenticated)
        context['country_calling_codes'] = country_calling_codes
        context['clevertap_account_id'] = settings.CLEVERTAP_ACCOUNT_ID

        city = city.replace('-', ' ')
        if city and any(city == c.name for c in cities):
            context['city'] = city
        elif city != '':
            return TemplateResponse(request, self.template_404_name, context,
                                    status=404)

        return TemplateResponse(request, self.template_name, context)


class BookingView(TemplateView, CustomerMixin, ItemCategoryMixin):
    template_name = "website/index.html"
    template_404_name = "website/404_not_found.html"

    def _get_contract_types(self, customer=None):
        return [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]

    def get_params(self, cities=None, is_authenticated=False, is_embed=False, load_home=True):
        _dict = {
            'offerbanner': {},
        }
        customer = self.get_customer(self.request.user)
        city_coverage, city_contacts = get_city_details(cities=cities)
        _dict['city_coverage'] = city_coverage
        _dict['city_contact_details'] =  city_contacts
        items = self.get_item_categories()
        vehicle_classes = ContractUtils.get_sell_packages(
            cities=cities,
            contract_types=self._get_contract_types(customer),
            customer=customer,
            is_authenticated=is_authenticated
        )

        if not isinstance(vehicle_classes, list):
            vehicle_classes = [vehicle_classes]
        _dict.update({
            'items': list(items) if items else [],
            'vehicle_classes': vehicle_classes
        })
        return _dict

    def _get_cities(self):
        """
        :return: queryset of City instances
        """
        cities = City.objects.get_super_queryset().select_related(
            'address', 'default_contract', 'default_vehicle_class')
        return cities.only('name', 'coverage_pickup', 'coverage_dropoff',
                           'future_booking_days', 'contact', 'default_contract',
                           'default_vehicle_class', 'address',
                           'loading_mins', 'unloading_mins')\
            .exclude(future_booking_days=0)

    def get(self, request, city=''):
        rebook_data = None
        context = {}
        uastring = self.request.META.get('HTTP_USER_AGENT')
        viewport_user_scalable = ''
        user = self.request.user
        is_authenticated = user and user.is_authenticated
        if uastring and ('Mobile' in uastring and 'Safari' in uastring):
            viewport_user_scalable = 'user-scalable=no'
        context['viewport_user_scalable'] = viewport_user_scalable

        if not city:
            city = request.GET.get('city', '')
        embed_type = request.GET.get('type', None)
        order_number = request.GET.get('rebook', '')
        if order_number:
            rebook_data = self.__get_rebook_data(order_number)

        cities = self._get_cities()
        context['country_code'] = settings.COUNTRY_CODE_A2
        context['isd_dialing_code'] = settings.ACTIVE_COUNTRY_CODE
        context['default_currency'] = settings.OSCAR_DEFAULT_CURRENCY
        context['active_timezone'] = settings.ACT_TIME_ZONE
        context['website_build'] = settings.WEBSITE_BUILD
        context['brand_name'] = settings.BRAND_NAME
        context['load_home'] = not embed_type or embed_type == 'bookingform'
        context['homepage_url'] = settings.HOMEPAGE_URL
        context['city'] = city
        context['rebook'] = json.dumps(rebook_data)
        context['seo'] = SEO_DATA.get(settings.WEBSITE_BUILD, {})
        context['states'] = list(State.objects.values('name', 'id').order_by('name'))
        global_params = self.get_params(
            cities,
            is_authenticated=False,
        )
        context['params'] = json.dumps(global_params)
        context['user_logged_in'] = is_authenticated
        context['is_staff'] = is_authenticated and user.is_staff
        context['b_user_logged_in'] = is_authenticated
        context['google_client_id'] = settings.GOOGLE_CLIENT_ID
        context['google_map_api_key'] = settings.GOOGLE_MAPS_API_KEY
        context['fresh_chat_data'] = settings.FRESHCHAT_WEB_CONFIG

        # Analytics and Tag Manager
        context['google_tag_manager_key_old'] = settings.GOOGLE_TAG_MANAGER_KEY_OLD
        context['fb_conversion_key_old'] = settings.FB_CONVERSION_KEY_OLD
        context['google_tag_manager_key'] = settings.GOOGLE_TAG_MANAGER_KEY
        context['fb_conversion_key'] = settings.FB_CONVERSION_KEY
        context['country_calling_codes'] = country_calling_codes
        context['clevertap_account_id'] = settings.CLEVERTAP_ACCOUNT_ID

        city = city.replace('-', ' ')
        if city and any(city == c.name for c in cities):
            context['city'] = city
        elif city != '':
            return TemplateResponse(request, self.template_404_name, context,
                                    status=404)

        return TemplateResponse(request, self.template_name, context)


class ContactUs(APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        data = request.data
        name = data.get('name', '')
        email = data.get('email', '')
        phone = data.get('phone', '')
        message = data.get('message', '')

        subject = '%s | %s | %s' % (name, email, phone)
        api_key = settings.FRESHDESK_API_KEY
        domain = settings.FRESHDESK_DOMAIN
        password = settings.FRESHDESK_PASSWORD

        headers = {'Content-Type': 'application/json'}
        ticket = {
            'subject': subject,
            'description': message,
            'email': email if email else settings.FRESHDESK_EMAIL,
            'priority': 1,
            'status': 2
        }

        url = settings.FRESHDESK_BLOWHORN_HOST + settings.FRESHDESK_TICKET_CREATION_END_POINT
        req = requests.post(url,
                            auth=(api_key, password),
                            headers=headers,
                            data=json.dumps(ticket))

        return Response(status=status.HTTP_200_OK,
                        data=_('We will contact you soon'))

        # Communication.send_email(
        #     settings.EMAIL_ID_CONTACT_US, settings.ADMIN_SENDER_ID,
        #                          subject, message)


class AppDownload(generics.RetrieveAPIView):
    authentication_classes = ()
    permission_classes = ()

    def get(self, request):
        mobile = request.GET.get('mobile', '')
        if mobile:
            template_dict = APP_DOWNLOAD_LINK_TEMPLATE
            Communication.send_sms(
                sms_to=mobile,
                message=APP_DOWNLOAD_LINK_TEMPLATE['text'],
                priority='high',
                template_dict=template_dict)
            logging.info("Sms sent to %s" % mobile)
            response_message = 'Link has been sent to %s' % mobile
            return Response(status=status.HTTP_200_OK, data=_(response_message))
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Please Provide Mobile Number.'))


class QRCode(TemplateView):

    def get(self, request):
        ua = request.META.get('HTTP_USER_AGENT', '')
        logging.info(ua)
        iphone = re.match(IOS_REGEX, ua)
        android = re.match(ANDROID_REGEX, ua)
        if iphone:
            return redirect(settings.IOS_APP_LINK)
        elif android:
            return redirect(settings.ANDROID_APP_LINK)
        else:
            return redirect('/')


class DriverAppLink(TemplateView):

    def get(self, request):
        ua = request.META.get('HTTP_USER_AGENT', '')
        logger.info(ua)
        iphone = re.match(IOS_REGEX, ua)
        android = re.match(ANDROID_REGEX, ua)

        if settings.PRODUCTION_DRIVER_APP:
            ios_link = settings.DRIVER_APP_IOS_LINK
            android_link = settings.DRIVER_APP_ANDROID_LINK
        else:
            ios_link = settings.DRIVER_APP_IOS_LINK_TEST
            android_link = settings.DRIVER_APP_ANDROID_LINK_TEST
        if iphone:
            return redirect(ios_link)
        elif android:
            return redirect(android_link)
        else:
            return redirect(android_link)


class TermsOfService(TemplateView):

    def get(self, request):
        return TemplateResponse(request, "website/termsofservice.html")


class DriverHandSanitizer(TemplateView):
    template_name = "website/driver_notification/image.html"

    def get(self, request):
        data = request.GET.dict()
        language_code = data.get('ln', 'en') if data.get(
            'ln') in LANGUAGE_CODES_FOR_TEMPLATES else 'en'
        image_path = '/static/corona/driver_sanitizer_ack/%s.jpg'

        image_path = image_path % language_code
        context = {
            'img_path': image_path
        }
        return TemplateResponse(request, self.template_name, context, content_type='text/html')


class DriverMedicalInsurance(TemplateView):
    template_name = "website/driver_notification/image.html"

    def get(self, request):
        data = request.GET.dict()
        language_code = data.get('ln', 'en') if data.get(
            'ln') in LANGUAGE_CODES_FOR_TEMPLATES else 'en'
        image_path = '/static/driverinsurance/%s.jpg'

        image_path = image_path % language_code
        context = {
            'img_path': image_path
        }
        return TemplateResponse(request, self.template_name, context, content_type='text/html')


class DriverLoanDvara(TemplateView):
    template_name = "website/driver_notification/image.html"

    def get(self, request):
        data = request.GET.dict()
        language_code = data.get('ln', 'en') if data.get('ln') in LANGUAGE_CODES_FOR_TEMPLATES else 'en'
        image_path = '/static/dvara/%s.jpg'

        image_path = image_path % language_code
        context = {
            'img_path': image_path
        }
        return TemplateResponse(request, self.template_name, context, content_type='text/html')


class DriverFunds(TemplateView):
    template_name = "website/driverfunds.html"

    def get(self, request):
        data = request.GET.dict()
        return TemplateResponse(request, self.template_name, content_type='text/html')


class DriverTermsOfService(TemplateView):
    template_name = "website/termsofservice/%s/driverapp.html" % settings.WEBSITE_BUILD

    def get(self, request):
        context = {
            'homepage_url': settings.HOMEPAGE_URL
        }
        return TemplateResponse(request, self.template_name, context)


class DriverReferralTermsOfService(TemplateView):

    def get(self, request):
        context = {}
        contacts = []
        cities = City.objects.filter(name__in=[BENGALURU, CHENNAI, HYDERABAD, DELHI, MUMBAI]).values('name', 'supply_contact_number')
        for i in cities:
            contacts.append({
                'city': i.get('name'),
                'support_number': i.get('supply_contact_number'),
            })

        return TemplateResponse(request, "website/driverreferraltermsofservice.html", context={
            'contacts': contacts
        })


class PrivacyPolicy(TemplateView):
    template_name = "website/privacy_policy.html"

    def get_context_data(self, **kwargs):
        context = super(PrivacyPolicy, self).get_context_data(**kwargs)
        contacts_dict = get_city_contact_details()
        contact_list = [contacts_dict[key] for key in
                        sorted(contacts_dict.keys())]
        context['support_number'] = contact_list[0] if len(contact_list) else ''
        return context

    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, self.template_name,
                                self.get_context_data())

class StockCheckPdfView(TemplateView):
    template_name = 'pdf/stock_check/main.html'

    def get(self, request):
        from blowhorn.common.utils import generate_pdf
        static_base = settings.ROOT_DIR.path('website/static')
        logo_path = '%s/wms/logos/bottom_text.png' % static_base
        data = {
            'logo': logo_path,
            'warehouse': {
                'name': 'BSK Warehouse',
                'address': 'Address Line 1, Address Line 2, Bengaluru 75'
            },
            'performed_by': {
                'name': 'Nagesh K',
                'date': '14 Sept 2021, 11:02 AM'
            },
            'client': 'Adukale',
            'sku_details': [{
                'id': 1,
                'name': 'Gojjavalakki / Gojju avalakki | Our Best Selling Breakfast and Evening Snack | Adukale',
                'description': 'Some nice SKU description',
                'results': [{
                    'tag': 'Nippattu 16 Sep 21',
                    'prev_qty': 23,
                    'curr_qty': 25,
                    'diff': 2,
                    'reason': 'something went wrong something went wrong something went wrong something went wrong'
                }, {
                    'tag': 'Nippattu 16 Sep 21',
                    'prev_qty': 23,
                    'curr_qty': 25,
                    'diff': 2,
                    'reason': 'something went wrong something went wrong something went wrong something went wrong'
                }, {
                    'tag': 'Nippattu 16 Sep 21',
                    'prev_qty': 23,
                    'curr_qty': 25,
                    'diff': 2,
                    'reason': 'something went wrong something went wrong something went wrong something went wrong'
                }]
            }, {
                'id': 2,
                'name': 'Gojjavalakki / Gojju avalakki | Our Best Selling Breakfast and Evening Snack | Adukale',
                'description': 'Some nice SKU description',
                'results': [{
                    'tag': 'Nippattu 16 Sep 21',
                    'prev_qty': 23,
                    'curr_qty': 25,
                    'diff': 2,
                    'reason': 'something went wrong something went wrong something went wrong something went wrong'
                }, {
                    'tag': 'Nippattu 16 Sep 21',
                    'prev_qty': 23,
                    'curr_qty': 25,
                    'diff': 2,
                    'reason': 'something went wrong something went wrong something went wrong something went wrong'
                }, {
                    'tag': 'Nippattu 16 Sep 21',
                    'prev_qty': 23,
                    'curr_qty': 25,
                    'diff': 2,
                    'reason': 'something went wrong something went wrong something went wrong something went wrong'
                }]
            }],
            'remarks': [
                'something went wrong something went wrong something went wrong something went wrong',
                'something went wrong something went wrong something went wrong something went wrong',
                'something went wrong something went wrong something went wrong something went wrong',
                'something went wrong something went wrong something went wrong something went wrong',
                'something went wrong something went wrong something went wrong something went wrong something went wrong something went wrong'
            ],
            'key_notes': [
                # 'SKU 1 has been lying around in all places',
                # 'SKU 1 has been lying around in all places',
                # 'SKU 1 has been lying around in all places',
                # 'SKU 1 has been lying around in all places',
                # 'SKU 1 has been lying around in all places'
            ],
            'footer': {
                'link': settings.HOST,
                'brand_name': settings.BRAND_NAME,
                'email': settings.COMPANY_SUPPORT_EMAIL,
                'contact': '+880177112277'
            }
        }
        if request.GET.get('pdf', None):
            footer = {"template": "pdf/stock_check/footer.html", "context": data}
            pdf = generate_pdf(self.template_name, data, footer=footer)
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "id_card.pdf"
            content = "attachment; filename=%s" % filename
            response['Content-Disposition'] = content
            return response
        return TemplateResponse(request, self.template_name, data)


class HealthCheck(APIView):
    permission_classes = (AllowAny, )

    def get(self, request):
        return Response(status=status.HTTP_200_OK)


class ParcelTracking(TemplateView):
    template_name = "track-parcel/index.html"

    def get(self, request, ref_number=None):
        return TemplateResponse(request, self.template_name)


class TrackRedirectView(APIView):
    permission_classes = (AllowAny, )

    def get(self, request, ref_number=None):
        query = Q(reference_number=ref_number) | Q(number=ref_number)
        order = Order.objects.filter(query).first()
        if order and order.order_type == settings.SHIPMENT_ORDER_TYPE:
            return redirect('/track-parcel/%s' % ref_number)

        return redirect('/track/%s' % ref_number)


class RazorpayHook(APIView, PaymentMixin):
    permission_classes = (AllowAny, )
    parser_classes = (PreserveRawBodyJSONParser,)

    def __get_razorpay_cred(self, _type=None):
        if _type == DONATION:
            return settings.RAZORPAY_KEY_ID_RELIEF, settings.RAZORPAY_SECRET_KEY_RELIEF, \
                settings.RAZORPAY_WEBHOOK_SECRET_RELIEF

        return settings.RAZORPAY_KEY_ID, settings.RAZORPAY_SECRET_KEY, \
            settings.RAZORPAY_WEBHOOK_SECRET

    def post(self, request):
        logger.info('API HOOK call from razorpay: %s', request.data)
        received_signature = request.META.get('HTTP_X_RAZORPAY_SIGNATURE', '')
        data = request.data
        payload = data.get('payload')
        payment = payload.get('payment')
        entity = payment.get('entity')
        amount = entity.get('amount')
        txn_id = entity.get('id')
        notes = entity.get('notes')
        type_ = notes.get('type')
        if not notes:
            message = 'Notes is empty.'
            logger.info(message)
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        _id, _key, _hook_secret = self.__get_razorpay_cred(_type=type_)
        client = razorpay.Client(auth=(_id, _key))
        try:
            client.utility.verify_webhook_signature(
                request.raw_body,
                received_signature,
                _hook_secret
            )
        except Exception:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Signature Verification Failed')
        event = data.get('event')
        if event not in ['payment.authorized', 'payment.captured']:
            message = 'Event is not authorized'
            logger.info(message)
            return Response(status=status.HTTP_304_NOT_MODIFIED, data=message)

        count = int(notes.get('count', 1))
        i = 0
        order_numbers = []
        while i <= count:
            key = 'booking_key'
            if i > 0:
                key = 'booking_key%s' % i

            more_order_numbers = notes.get(key, '').split(',')
            if more_order_numbers:
                order_numbers = list(set(more_order_numbers + order_numbers))
            i += 1
        if '' in order_numbers:
            order_numbers.remove('')

        user = None
        if type_ == DONATION:
            if not request.user.is_anonymous:
                user = request.user
            else:
                email = entity.get('email')
                user, customer = create_customer(email)

        status_code, data = self.capture_online_payment(order_numbers, txn_id,
                                                        type_, amount, user=user)
        logger.info('Razorpay Hook: %s, %s' % (status_code, data))
        return Response(status=status_code, data=data)


class PaymentWebView(TemplateView):
    template_name = "website/payment.html"

    def get(self, request, pid=None):
        customer_pk = pid.split(PAYMENT_URL_SEPARATOR)[0]
        logger.info('customer_pk: %s' % customer_pk)
        try:
            customer = Customer.objects.get(pk=customer_pk)
        except Customer.DoesNotExist:
            return TemplateResponse(request, 'website/404_not_found.html')

        user = customer.user
        profile = {
            'name': customer.name,
            'email': user.email,
            'mobile': user.national_number
        }
        payment_info = customer.get_unpaid_orders()

        if payment_info and not payment_info.get('unpaid_orders'):
            return TemplateResponse(request, 'website/404_not_found.html')

        payment_info.pop('can_place_order', '')
        context = {
            'rzpid': settings.RAZORPAY_SETTINGS.get('id', ''),
            'profile': json.dumps(profile),
            'payment_info': payment_info,
            'website_build': settings.WEBSITE_BUILD,
            'seo': SEO_DATA.get(settings.WEBSITE_BUILD, {})
        }
        return TemplateResponse(request, self.template_name, context)


class ApiDocumentationView(TemplateView):
    template_name = "website/api_documentation.html"

    def get(self, request):
        return TemplateResponse(request, self.template_name)

class ErrorReportView(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication,)
    permission_classes = (AllowAny, )

    def post(self, request):
        data = request.data
        slack_channel = settings.EMAIL_FAILURE_SLACK_CHANNEL.get('name', '')
        slack_channel_url = settings.EMAIL_FAILURE_SLACK_CHANNEL.get('url', '')
        if settings.DEBUG:
            slack_channel = settings.TESTING_SLACK_CHANNEL_DEFAULT
            slack_channel_url = settings.TESTING_SLACK_URL

        attachments = [{
            'color': '#FF0000',
            'author_name': request.META.get(
                'HTTP_REFERER', request.META.get('HTTP_HOST',
                                 'Unknown Host')) or 'Unknown Host',
            'title': '%s' % data.get('message'),
            'text': '%s' % data.get('stack')
        }]

        slack_message = {
            'channel': slack_channel,
            'attachments': attachments
        }
        try:
            requests.post(url=slack_channel_url, json=slack_message)
            return Response(
                status=status.HTTP_200_OK,
                data='Error report submitted successfully')
        except ConnectionResetError:
            logging.error(traceback.format_exc())
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data='Failed to submit error report')


class NewsView(APIView):
    authentication_classes = ()
    permission_classes = (AllowAny,)
    records_per_page = 25

    def __get_news_dict(self, obj):
        publishers_logo = obj.publishers_logo.url if obj.publishers_logo else ''
        return {
            'thumbnail': obj.thumbnail.url if obj.thumbnail else '',
            'heading': obj.heading,
            'body': obj.body,
            'published_date': obj.published_date.isoformat(),
            'publishers_logo': publishers_logo,
            'article_link': obj.article_link
        }

    def __get_paginated_data(self, queryset, page):
        paginator = Paginator(queryset, self.records_per_page)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else
            objects.previous_page_number()
        )
        next_page = 0 if not objects.has_next() else objects.next_page_number()
        data = {
            "count": count,
            "previous": previous,
            "next_page": next_page,
            "results": objects.object_list,
        }
        return data

    def get(self, request):
        current_page = int(request.GET.get('page', 1))
        qs = News.objects.all().order_by('-published_date')
        data = self.__get_paginated_data(qs, current_page)
        data = {
            'results': [self.__get_news_dict(obj) for obj in
                        data.get('results')],
            'next_page': data.get('next_page', None)
        }
        return Response(status=status.HTTP_200_OK, data=data)


class TrackAssetView(TemplateView):
    template_name = "website/track_assets.html"

    def __get_assets(self, trip):
        asset_qs = OrderContainer.objects.filter(trip=trip)\
            .select_related('container', 'container__container_type', 'order')\
            .values('container__container_type__name', 'order__reference_number')\
            .annotate(cont_type_count=Count('container'))
        return asset_qs

    def __get_returned_items(self, trip):
        order_pks = Stop.objects.filter(trip=trip)\
            .values_list('order_id', flat=True)
        orderline_qs = OrderLine.objects.filter(order_id__in=order_pks)\
            .select_related('sku', 'order')\
            .annotate(qty_returned=F('quantity')- F('qty_removed') - F('qty_delivered'))\
            .filter(qty_returned__gt=0)\
            .order_by('order__reference_number')\
            .values('order__reference_number', 'qty_returned', 'sku__name')
        grouped_data = {}
        for ref_number, items in itertools.groupby(
                orderline_qs, key=lambda x: x['order__reference_number']):
            grouped_data[ref_number] = list(items)
        return grouped_data

    def get(self, request, trip_number=None):
        trip_qs = Trip.objects\
        .select_related('driver', 'driver__user')\
        .filter(
            trip_number='TRIP-%s' % trip_number.upper()
        ).exclude(
            status=StatusPipeline.TRIP_COMPLETED
        )
        context = {}
        if not trip_qs.exists():
            context['error'] = _('Link has been expired')
        else:
            trip = trip_qs.first()
            context['trip_creation_time'] = trip.trip_creation_time
            context['total_cash_collected'] = trip.total_cash_collected
            context['otp'] = trip.otp
            context['assets'] = list(self.__get_assets(trip))
            context['returned_items'] = self.__get_returned_items(trip)
            driver = trip.driver
            context['driver'] = {
                'name': driver.name,
                'mobile': driver.user.national_number
            }
        return TemplateResponse(request, self.template_name, context)


@xframe_options_exempt
def trackButton(request):
    template_name = "website/embeds/track_button.html"
    context = {
        'host': settings.HOST,
        'homepage_url': settings.HOMEPAGE_URL
    }
    response = render(request, template_name, content_type='text/html', context=context)
    return response


@xframe_options_exempt
def myAccountButton(request):
    template_name = "website/embeds/myaccount.html"
    context = {
        'host': settings.HOST,
        'homepage_url': settings.HOMEPAGE_URL
    }
    response = render(request, template_name, content_type='text/html', context=context)
    return response


@xframe_options_exempt
def bookingFormView(request):
    template_name = "website/index.html"
    context = {
        'host': settings.HOST,
        'homepage_url': settings.HOMEPAGE_URL
    }
    response = render(request, template_name, content_type='text/html', context=context)
    return response


@method_decorator(xframe_options_exempt, name='dispatch')
class EmbedTemplateView(TemplateView):

    def get(self, request, pathname=None):
        template_name = "website/embeds/%s.html" % pathname
        context = {
            'host': settings.HOST,
            'homepage_url': settings.HOMEPAGE_URL
        }
        return TemplateResponse(request, template_name, context)


class MyAccountView(TemplateView, CustomerMixin):
    template_name = "website/myaccount.html"

    def get(self, request, *args, **kwargs):
        user = request.user
        if user:
            # @todo need to pass `next` as param and redirect after login.
            #  Need changes in home page controller
            if not user.is_authenticated:
                return redirect('/')

            if user.is_customer:
                return TemplateResponse(request, self.template_name,
                                        self.get_context_data())
            else:
                return TemplateResponse(request, '403_csrf.html',
                                        status=status.HTTP_401_UNAUTHORIZED)
        return redirect('/')

    def get_context_data(self, **kwargs):
        context = super(MyAccountView, self).get_context_data(**kwargs)
        user = self.request.user
        customer = CustomerMixin().get_customer(user)
        is_authenticated = user and user.is_authenticated
        context['is_authenticated'] = is_authenticated
        context['rzpid'] = settings.RAZORPAY_SETTINGS.get('id', '')
        context['google_map_api_key'] = settings.GOOGLE_MAPS_API_KEY
        context['website_build'] = settings.WEBSITE_BUILD
        context['brand_name'] = settings.BRAND_NAME
        context['homepage_url'] = settings.HOMEPAGE_URL
        context['country_code'] = settings.COUNTRY_CODE_A2
        context['default_currency'] = settings.OSCAR_DEFAULT_CURRENCY
        context['active_timezone'] = settings.ACT_TIME_ZONE
        context['host'] = self.request.META.get('HTTP_HOST') or settings.HOST
        context['font_name'] = settings.WEBSITE_FONT
        context['seo'] = SEO_DATA.get(settings.WEBSITE_BUILD, {})
        context['is_business_user'] = customer and customer.is_non_individual()
        context['max_allowed_recharge_amount'] = settings.MAX_ALLOWED_WALLET_RECHARGE_AMOUNT
        payment_methods = settings.PAYMENT_METHODS
        if isinstance(payment_methods, str):
            payment_methods = json.loads(payment_methods)
        context['payment_methods'] = payment_methods
        return context


class PartnerAdvisoryView(TemplateView):

    def get(self, request):
        return TemplateResponse(request, 'corona/driver_advisory.html')


class CustomerAdvisoryView(TemplateView):

    def get(self, request):
        return TemplateResponse(request, 'corona/customer_advisory.html')


class DonationView(TemplateView, CustomerMixin):
    template_name = "website/donation.html"

    def get_context_data(self, **kwargs):
        request_from_app = self.request.GET.get('app', False)
        context = super(DonationView, self).get_context_data(**kwargs)
        user = self.request.user
        is_authenticated = user and user.is_authenticated
        context = {
            'rzpid': settings.RAZORPAY_SETTINGS_RELIEF.get('id', ''),
            'is_authenticated': is_authenticated,
            'donation': DONATION,
            'request_from_app': request_from_app
        }
        context['clevertap_account_id'] = settings.CLEVERTAP_ACCOUNT_ID
        context['google_tag_manager_key_old'] = settings.GOOGLE_TAG_MANAGER_KEY_OLD
        context['fb_conversion_key_old'] = settings.FB_CONVERSION_KEY_OLD
        context['google_tag_manager_key'] = settings.GOOGLE_TAG_MANAGER_KEY
        context['fb_conversion_key'] = settings.FB_CONVERSION_KEY
        return context

    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, self.template_name,
                                self.get_context_data())


class RazorpayWebRedirectionView(APIView, PaymentMixin):

    permission_classes = (AllowAny,)
    cancel_template = 'website/donation/cancel.html'
    checkout_template = 'payments/razorpay/checkout.html'

    def post(self, request):
        data = request.data.dict()
        payment_status = None
        razorpay_payment_id = data.get('razorpay_payment_id')
        _dict = {
            'razorpay_payment_id': razorpay_payment_id
        }
        transaction_details = self.fetch_transaction_details(razorpay_payment_id, DONATION)
        if not transaction_details:
            return render(
                request,
                self.cancel_template
            )

        return render(
            request,
            self.checkout_template,
            dict(data=transaction_details, end_point='/webview/donation/capture')
        )

    def get(self, request):
        return Response(status=200)


class CaptureDonationView(APIView, PaymentMixin):

    permission_classes = (AllowAny,)
    cancel_template = 'website/donation/cancel.html'
    success_template = 'website/donation/success.html'

    def post(self, request):
        data = request.data
        txn_id = data.get('id', None)
        amount = data.get('amount', 0.0)
        email = data.get('email', '')
        _type = DONATION
        user = None
        if not request.user.is_anonymous:
            user = request.user
        else:
            email = data.get('email', '')
            user, customer = create_customer(email)

        status_code, data = self.capture_online_payment(
            [], txn_id, _type, amount, user=user)
        logger.info('Razorpay: %s, %s' % (status_code, data))

        if status_code == status.HTTP_200_OK:
            return render(
                request,
                self.success_template
            )

        return render(
            request,
            self.cancel_template
        )

    def get(self, request):
        return Response(status=200)

class ServiceAvailability(generics.ListAPIView):
    """
    Description     : API to get the list of pincodes serviced by Blowhorn.
                      If customer_hubs_only
                        returns list of hubs for the customer
                      Else
                        returns all the blowhorn hubs (microwarehouses) where customer is null
    Request type    : GET
    Params          : city(string), customer_hubs_only(boolean), pincode
    Response        : Available serviceable pincodes for the city
    Authentication  : Basic session Authentication with CSRF exemption
    Permission      : AllowAny with api_key passed in header
    """
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    permission_classes = (AllowAny, )
    renderer_classes = [JSONRenderer]

    def get(self, request, format=None):
        params = self.request.GET
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                data ='Unauthorized, pass correct api_key in request header')

        # Only one of pincode or city is mandatory

        pincode = params.get('pincode', None)

        city = params.get('city', None)

        if not any([city, pincode]):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                    data=_('either city or pincode is to be supplied. e.g. ?city=Bengaluru or ?pincode=560076'))
        if city and pincode:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                    data=_('supply either city or pincode (not both). e.g. ?city=Bengaluru or ?pincode=560076'))

        customer_hubs_only = params.get('customer_hubs_only', None)
        try:
            if not city:
                if pincode and customer_hubs_only:
                # Pincode in customer hub
                    hub_exists = Hub.objects.filter(customer=customer, pincodes__contains=[pincode]).exists()
                    return Response(status=status.HTTP_200_OK, data={"status": status.HTTP_200_OK, "serviceable": hub_exists})
                else:
                # pincode in blowhorn hub
                    hub_exists = Hub.objects.filter(customer__isnull=True, pincodes__contains=[pincode]).exists()
                    return Response(status=status.HTTP_200_OK, data={"status": status.HTTP_200_OK, "serviceable": hub_exists})


            city = City.objects.get(name=city)
            if customer and customer_hubs_only:
            # If request contains to display customer hubs only
                if not Hub.objects.filter(customer=customer, city=city, pincodes__isnull=True).exists():
                # If no pincodes have been defined for the customer
                    return Response(status=status.HTTP_200_OK,
                        data=_('no pincodes defined for customer hubs. Reach out to blowhorn operations'))
                pincodes = Hub.objects.filter(customer=customer, city=city). \
                            annotate(customer_hub=F('name')).values('customer_hub', 'pincodes')
            else:
            # Show all blowhorn hubs with pincodes for the city
                pincodes = Hub.objects.filter(customer__isnull=True, city=city). \
                            annotate(blowhorn_hub=F('name')).values('blowhorn_hub', 'pincodes')
            return Response(status=status.HTTP_200_OK, data=pincodes)
        except City.DoesNotExist:
            response_data = {}
            response_data['error'] = _('blowhorn does not provide service in this city')
            response_data['operating_cities'] =  Hub.objects.filter(pincodes__len__gt=1).values_list('city__name', flat=True).distinct()
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

