/* Javascript user constants can be stored here */

// Live tracking constants used in user myfleet, branding
const MYFLEET_TRACKING = 'myfleet';
const BRANDING_LIVE_TRACKING = 'branding';
const GOOGLE_LOGIN = 'Google';
const FACEBOOK_LOGIN = 'Facebook';
