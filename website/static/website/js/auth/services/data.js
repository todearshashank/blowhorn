angular.module('authApp').factory('dataService', function ($compile, $http, $q) {
    var factory = {};
    factory.getDataFromJson = function(build, filepath) {
        var defer = $q.defer();
        $http.get(`/static/website/js/content/${build}/${filepath}.json`)
            .success(function(data) {
                defer.resolve(data);
            }).error(function() {
                defer.reject('could not find json');
            });
        return defer.promise;
    }

    factory.getParameterByName = function(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
            results = regex.exec(location.search);
        return results == null ?
          false :
          decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    factory.signupDetails = {};
    factory.setSignupDetails = function (data) {
        factory.signupDetails = {
            email: data.email,
            mobile: data.mobile
        };
    }

    factory.nextPageUrl = function (is_staff, is_business_user) {
        if (is_staff) {
            return '/admin';
        } else if (is_business_user) {
            return '/dashboard';
        } else {
            return '/dashboard/mytrips';
        }
    }
    
    return factory;
});
