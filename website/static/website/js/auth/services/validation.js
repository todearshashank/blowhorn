angular.module('authApp').factory('validationService', function ($compile, $http, $q) {
    var factory = {};
    factory.validGstin = function(gstin) {
        return $scope.regex.gstin.test(gstin)
    }

    factory.getErrorMessage = function (data) {
        // Using default error messages from django response.
        var _form = data.form;
        if (!_form) {
            let responseJSON = data.responseJSON;
            if (!responseJSON) {
                return 'Something went wrong. Try again later.'
            }
            _form = responseJSON.form;
        }
        var nonFieldErrors = _form.errors;
        if (nonFieldErrors.length > 0) {
            return nonFieldErrors[0];
        } else {
            var fields = _form.fields;
            for (var field in fields) {
            var fieldErrors = fields[field].errors;
            if (fieldErrors.length > 0) {
                return fieldErrors[0];
            }
            }
        }
    }

    return factory;
});
