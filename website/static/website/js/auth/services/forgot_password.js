angular.module('authApp').factory('recoverPasswordService', function ($http, $q) {
    var factory = {};

    function getCookieValue(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        var csrf = $("input[name='csrfmiddlewaretoken']").val();
        return cookieValue || csrf;
    }

    factory.sendPasswordResetLink = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "/accounts/password/reset/",
            type: 'POST',
            data: data,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };
    return factory;
});
