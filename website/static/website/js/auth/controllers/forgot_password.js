angular
  .module('authApp')
  .controller('passwordRecoveryController',
  function ($scope, recoverPasswordService, validationService) {
    const PASSWORD_RESET_LINK_SUCCESS = 'We have sent a password reset link to your email. Link expires in 6 hrs.';

    $scope.isInEmbed = isInEmbed;
    $scope.urlParams = window.location.search;
    $scope.loginUrl = `/login/${$scope.urlParams}`;
    $scope.homepageUrl = homepageUrl;
    $scope.recoveryEmail = null;
    $scope.success = false;
    $modal = $('#password-recover');
    $modal.modal({backdrop: 'static', keyboard: false});
    $modal.modal('show');

    $scope.resetPassword = function (e) {
        if (!$scope.recoveryEmail) {
            return;
        }
        recoverPasswordService
            .sendPasswordResetLink({email:$scope.recoveryEmail})
            .then(handleSuccess)
            .catch(handleError);
    };

    function handleSuccess() {
        $scope.success = true;
        $scope.passwordRecoveryMessage = PASSWORD_RESET_LINK_SUCCESS;
        $scope.$applyAsync();
    }

    function handleError(data) {
        $scope.success = false;
        $scope.passwordRecoveryMessage = validationService.getErrorMessage(data);
        $scope.$applyAsync();
    }

    $scope.goToHome = function () {
      window.open($scope.homepageUrl, '_self');
    }
});
