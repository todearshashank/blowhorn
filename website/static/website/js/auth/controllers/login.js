angular
  .module('authApp')
  .controller('loginController',
  function ($scope, $location, loginData, loginService, validationService, dataService) {

    $scope.data = loginData;
    $scope.isInEmbed = isInEmbed;
    $scope.homepageUrl = homepageUrl;
    $scope.urlParams = window.location.search;
    $scope.country_code = country_code;
    $scope.isd_dialing_code = isd_dialing_code;
    $scope.signupUrl = `/register/${$scope.urlParams}`;
    $scope.recoverPasswordUrl = `/forgot-password/${$scope.urlParams}`;
    $loginModal = $('#login-modal');
    $loginModal.modal({backdrop: 'static', keyboard: false});
    $loginModal.modal('show');
    $scope.submitting = false;
    $scope.errorMessage = null;
    $scope.primaryLoginField = null;
    $scope.password = null;
    $scope.submitBtnText = 'LOGIN';

    let mobileNumberRegex = /^\d{9,10}$/;
    let digitRegex = /^\d+$/;

    function signIn() {
      $scope.submitting = true;
      if ($scope.primaryLoginFieldType === 'email') {
        var data = {
          login: $scope.primaryLoginField,
          password: $("#password").val(),
          remember: false
        };
        loginService.signIn(data)
          .then(handleSuccess)
          .catch(handleFailure);
      } else {
        let data = {
          mobile: $scope.primaryLoginField
        };
        loginService.sendOtp(data)
          .then(handleOtpSendSuccess)
          .catch(handleFailure);
      }
    }

    function handleOtpSendSuccess(data) {
      dataService.setSignupDetails({
        mobile: $scope.primaryLoginField
      });
      $location.path(`/otp`);
    }

    function handleSuccess(data) {
      $scope.submitting = false;
      loginService.getUserProfile(data)
        .then(
          function (data) {
            let profile = data.message;
            let nextUrl = dataService.nextPageUrl(profile.is_staff, profile.b_myfleet_required);
            $scope.nextPage(nextUrl);
          }
        ).catch(handleFailure);
    }

    function handleFailure(data) {
      try {
        var response = jQuery.parseJSON(data.responseText);
        message = response.message.text;
      } catch (err) {
        message = validationService.getErrorMessage(data);
      }
      $scope.errorMessage = message;
      $scope.submitting = false;
      $scope.$applyAsync();
    }

    $scope.signIn = function (e) {
      e.preventDefault();
      signIn();
    };

    $scope.resetLoginErrorMessage = function () {
      $scope.errorMessage = null;
    };

    $scope.nextPage = function (url) {
      console.log('URL', url);
      window.location.replace(url);
    }

    $scope.goToHome = function () {
      window.open($scope.homepageUrl, '_self');
    }

    $scope.telephoneOnKeydown = function (event) {
      if (event.which === 187) {
        event.preventDefault();
        return false;
      }
    }

    $scope.primaryLoginFieldType = 'email';
    $scope.checkInputType = function () {
      $('#login-message').hide();
      $scope.primaryLoginFieldType = digitRegex.test($scope.primaryLoginField) ? 'phone' : 'email';
      $scope.submitBtnText = $scope.primaryLoginFieldType === 'email' ? 'LOGIN' : 'SEND OTP';
    }

});
