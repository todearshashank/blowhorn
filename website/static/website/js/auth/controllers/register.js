angular
  .module('authApp')
  .controller('registerController',
  function ($scope, $location, registerData, registerService, validationService, dataService) {

    $scope.data = registerData;
    $scope.current_build = current_build
    $scope.isInEmbed = isInEmbed;
    $scope.homepageUrl = homepageUrl;
    $scope.urlParams = window.location.search;
    $scope.loginUrl = `/login/${$scope.urlParams}`;
    $scope.country_code = country_code;
    $scope.isd_dialing_code = isd_dialing_code;
    $scope.signupDetails = {
      state: ''
    };
    $scope.states = states;
    $registerModal = $('#register-modal');
    $registerModal.modal({backdrop: 'static', keyboard: false});
    $registerModal.modal('show');
    $scope.emailRegex = /[a-zA-Z0-9_\.]+@[a-zA-Z0-9_\.]+\.[a-zA-Z0-9]+/;
    $scope.submitting = false;
    $scope.primaryLoginField = null;
    $scope.password = null;

    let mobileNumberRegex = /^\d{9,10}$/;
    let digitRegex = /^\d+$/;

    function initFormValidation($signup_form) {
      $signup_form.validate({
        errorPlacement: function (error, element) {},
        rules: {
          name: {
            required: false
          },
          email: {
            required: false,
            email: true
          },
          password: {
            required: false
          },
          repassword: {
            required: false,
            equalTo: '#signup-password'
          },
          mobile: {
            required: false,
            digits: true,
            // minlength: 10,
            // maxlength: 10
          },
          checkbox: {
            required: true
          }
        }
      });
    }

    function validateForm() {
      if (
        $scope.signupDetails.is_business_user &&
        !$scope.signupDetails.legal_name
      ) {
        $scope.signupDetails.error = 'Company name is required';
        return false;
      }
      if (
        $scope.signupDetails.gstin &&
        !$scope.signupDetails.state
      ) {
        $scope.signupDetails.error = 'State/Province is required';
        return false;
      }
      if ($scope.primaryLoginFieldType === 'email'&& !$scope.signupDetails.password){
        $scope.signupDetails.error = 'Please enter password';
        return;
      }

      if (!$scope.primaryLoginFieldType){
        $scope.signupDetails.error = 'Please enter Email / Phone number';
      return;
      }
      $scope.$applyAsync();
      return true;
    }

    $scope.proceedRegistration = function (e, form) {
      e.preventDefault();
      $scope.submitting = true;
      if (!validateForm()) {
        $scope.submitting = false;
        return;
      }
      // initFormValidation($signup_form);
      console.log('form-->', form);
      if (form.$valid) {
        $scope.submitting = true;
        signupUser();
      }
    };

    function signupUser() {
      var data = {
        name: $scope.signupDetails.name,
        password1: $scope.signupDetails.password,
        password2: $scope.signupDetails.password,
        is_business_user: $scope.signupDetails.is_business_user,
        gstin: $scope.signupDetails.gstin,
        state: $scope.signupDetails.state,
        legal_name: $scope.signupDetails.legal_name
      };

      if ($scope.primaryLoginFieldType === 'email'){
        data.email = $('#primary-signup-field').val();
      }
      else {
        data.phone_number = $('#primary-signup-field').val();
      }

      if (!data.name){
        data.name = 'Guest';
      }

      if (!data.phone_number){
        data.phone_number = null;
      }

      if (!data.email){
        data.email = data.phone_number + '@customer.' + $scope.current_build + '.net';
      }

      if (!data.password1){
        data.password1 = '1234';
        data.password2 = '1234';
      }

      registerService.signupUser(data)
        .then(handleSuccess)
        .catch(handleFailure);
    }

    function handleSuccess(data) {
      if ($scope.primaryLoginFieldType === 'email'){
        $('#register-modal').modal('hide');
        window.open($scope.homepageUrl, "_self")
      }
      else {
        dataService.setSignupDetails({
            mobile: $('#primary-signup-field').val()
        });
        $location.path(`/otp`);
      }
    }

    function handleFailure(data) {
      $scope.submitting = false;
      if (!data) {
        $scope.signupDetails.error = 'Something went wrong. Try again later.';
        return;
      }
      $scope.signupDetails.error = validationService.getErrorMessage(data);
    }

    $scope.goToHome = function () {
      window.open($scope.homepageUrl, '_self');
    }

    $scope.telephoneOnKeydown = function (event) {
      if (event.which === 187) {
        event.preventDefault();
        return false;
      }
    }

    $scope.primaryLoginFieldType = 'email';
    $scope.checkInputType = function () {
    $('#login-message').hide();
    $scope.primaryLoginFieldType = digitRegex.test($scope.primaryLoginField) ? 'phone' : 'email';
    }
});
