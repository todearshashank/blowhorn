angular
  .module('authApp')
  .controller('otpController',
  function ($scope, otpService, validationService, dataService) {

    $scope.isInEmbed = isInEmbed;
    $scope.homepageUrl = homepageUrl;
    $scope.urlParams = window.location.search;
    $scope.next = `${window.location.host}/dashboard`;
    $scope.otp = dataService.signupDetails;
    $scope.country_code = country_code;
    $scope.isd_dialing_code = isd_dialing_code;
    $scope.success = false;
    $modal = $('#otp-modal');
    $modal.modal({backdrop: 'static', keyboard: false});
    $modal.modal('show');

    $scope.goToHome = function () {
      window.open($scope.homepageUrl, '_self');
    }

    $scope.message = null;
    $scope.verifySignupOtp = function (e) {
      e.preventDefault();
      var $otp_form = $('#otp-form');
      $otp_form.validate({
        errorPlacement: function (error, element) {},
        rules: {
          // email: {
          //   required: true
          // },
          mobile: {
            required: true,
            digits: true,
          },
          otp: {
            required: true,
            digits: true
          }
        }
      });
      if ($otp_form.valid()) {
        var data = {
            // email: $scope.otp.email,
            mobile: $scope.otp.mobile,
            otp: $scope.otp.otp
        };
        if (!data.mobile || !data.otp) {
          return;
        }
        otpService
          .verifySignupOtp(data)
          .then(handleSuccess)
          .catch(handleError);
      }
    };

    function handleSuccess(data) {
      $scope.success = true;
      $scope.message = data.message;
      $scope.next = dataService.nextPageUrl(data.is_staff, data.is_business_user);
      $scope.$applyAsync();
      $scope.nextPage();
    }

    $scope.nextPage = function () {
      window.location.replace($scope.next);
    }

    function handleError(data) {
      $scope.success = false;
      console.log('data: ', data);
      $scope.message = (
        data.responseJSON && data.responseJSON.message ?
          data.responseJSON.message :
            data.responseText ||
            'Something went wrong. Try again later.'
      );
      $scope.$applyAsync();
    }
});
