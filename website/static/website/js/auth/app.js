angular.module('authApp', ['ngRoute', 'ngAnimate', 'ngMessages', 'ui.bootstrap', 'oc.lazyLoad', 'ngIntlTelInput']);

// NOTE: This version number has to be changed if any changes been done in this app
let version = 2.6;
angular.module('authApp').config(appConfig).run(['$route', '$rootScope', '$location', appRun])

function appConfig($routeProvider, $locationProvider, $httpProvider, ngIntlTelInputProvider) {

    ngIntlTelInputProvider.set({
        allowDropdown: false,
        separateDialCode: true,
    });

    $routeProvider.
    when('/login', {
        templateUrl: '/static/website/html/auth/login.html',
        controller: 'loginController as LC',
        resolve: {
            loginData: function (dataService) {
                return dataService.getDataFromJson(current_build, 'login');
            },
            lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([{
                    name: 'authApp',
                    files: [
                        `/static/website/js/auth/controllers/login.js?v=${version}`,
                        `/static/website/js/auth/services/login.js?v=${version}`,
                        `/static/website/js/auth/directives/auth.js?v=${version}`,
                    ]
                }]);
            }]
        }
    }).
    when('/register', {
        templateUrl: '/static/website/html/auth/register.html',
        controller: 'registerController as RC',
        resolve: {
            registerData: function (dataService) {
                return dataService.getDataFromJson(current_build, 'register');
            },
            lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([{
                    name: 'authApp',
                    files: [
                        `/static/website/js/auth/controllers/register.js?v=${version}`,
                        `/static/website/js/auth/services/register.js?v=${version}`,
                        `/static/website/js/auth/directives/auth.js?v=${version}`,
                    ]
                }]);
            }]
        }
    }).when('/forgot-password', {
        templateUrl: '/static/website/html/auth/forgot_password.html',
        controller: 'passwordRecoveryController as PC',
        resolve: {
            lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([{
                    name: 'authApp',
                    files: [
                        `/static/website/js/auth/controllers/forgot_password.js?v=${version}`,
                        `/static/website/js/auth/services/forgot_password.js?v=${version}`,
                    ]
                }]);
            }]
        }
    }).when('/otp', {
        templateUrl: '/static/website/html/auth/otp.html',
        controller: 'otpController as OC',
        resolve: {
            lazy: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([{
                    name: 'authApp',
                    files: [
                        `/static/website/js/auth/controllers/otp.js?v=${version}`,
                        `/static/website/js/auth/services/otp.js?v=${version}`,
                    ]
                }]);
            }]
        }
    });

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.headers.common.AccessControlAllowOrigin = '*';
    // Use the HTML5 History API
    $locationProvider.html5Mode({
        enabled: true,
        // rewriteLinks: false
        requireBase: false
    });
};


function appRun($route, $rootScope, $location) {
    var original = $location.path;
    $location.path = function (path, no_reload) {
        if (no_reload === true) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
}
