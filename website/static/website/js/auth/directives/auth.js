angular.module("authApp").directive('bindHtmlCompile', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.bindHtmlCompile);
            }, function (value) {
                element.html(value && value.toString());
                var compileScope = scope;
                if (attrs.bindHtmlScope) {
                    compileScope = scope.$eval(attrs.bindHtmlScope);
                }
                $compile(element.contents())(compileScope);
            });
        }
    };
}]);

angular.module("authApp").directive('countryCode', countryCode);
function countryCode() {
    return {
        restrict: 'AE',
        scope: {
          countryCodeA2: '=',
          isdDialingCode: '='
        },
        link: function (scope) {
        },
        templateUrl: '/static/website/sections/components/country_code.html'
    };
}
