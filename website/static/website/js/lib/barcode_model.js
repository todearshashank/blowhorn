(function($) {
    $(document).ready(function() {
        $("body").append('<div id="id-card-wrap"></div>');
        $("#id-card-wrap").load("/static/website/html/barcode.html");
    });
})(django.jQuery);

function showBarCode(value, orderNumber) {
    // For documentation, visit: https://lindell.me/JsBarcode/
    let options = {
        margin: 30
    };
    JsBarcode("#barcode", value || 'No barcode data found', options);
    $('#barcode-model-header').html('Order Number: ' + orderNumber);
    $("#modal-barcode").modal({ backdrop: "static", keyboard: false });
    $("#modal-barcode").modal("show");
}
