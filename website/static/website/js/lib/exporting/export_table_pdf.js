/*  Function for exporting a table as PDF 
    After populating the table data it uses jsPdf.js library for pdf conversion */
function exportPdf( docHeader, filename, tColumns, tData ){
    var pdf = new jsPDF('l', 'pt');
    var header = function(data) {
        pdf.setFontSize(18);
        pdf.setTextColor(40);
        pdf.setFontStyle('normal');
        pdf.text(docHeader, data.settings.margin.left, 50);
    };
    var options = {
        theme: 'grid',
        beforePageContent: header,
        margin: { top: 80 },
        tableWidth: 'auto',
        columnStyles: {
            halign: 'center',
            0: { fillColor: [42, 193, 173],
                textColor: 255
            }
        }
    };

    // Refer: https://github.com/simonbengtsson/jsPDF-AutoTable
    pdf.autoTable(tColumns, tData, options);
    pdf.save(filename + '.pdf');
}