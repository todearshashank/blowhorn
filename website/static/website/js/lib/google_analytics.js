/*******************************************************************************
                                Google Analytics
*******************************************************************************/

function sendOrderPlacedEvent(friendly_id, estd_cost) {
   try {
    gtag('event', 'conversion', {
        'send_to': 'AW-678033239/6IUYCOCOl7kBENfup8MC',
        'transaction_id': friendly_id
    });

    gtag('event', 'booking_placed', {
        'event_category': 'BookingPlaced',
        'event_label': 'BookingPlaced',
        'value': estd_cost
    });
   } catch {
        console.log('Failed to send booking event.')
   }
}

function sendEvent(event, method) {
    try {
        gtag('event', event, {'method': method});
    } catch {
        console.log('Failed to send event.')
    }
}

function sendCustomEvent(event, data) {
    try {
        gtag('event', event, data);
    } catch {
        console.log('Failed to send event.')
    }
}
