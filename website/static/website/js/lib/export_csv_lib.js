/* 
STEPS TO USE -

1) Add the export_csv_lib.js file after including Angular but before module's app.js (file that defines the module & its dependencies)
2) Inject "export_functions" as dependency in the Angular module in question.
3) Whichever table you want the widget to appear in, just write export-functions as an attribute in the table tag.

OPTIONS -

1) export-ignore-headers: Add this attribute to specify headers you want to ignore. Pass a scope variable inside it. eg:- export-ignore-headers="{{headersToIgnore}}". Here, headersToIgnore is array of header names to ignore.
2) export-additional-data: Add this attribute to pass extra data. eg:- export-additional-data="{{additionalData}}". Here, additionalData is an array of objects.
3) export-filename-prefix:
    Default filename: 'Export-data-<today's_date>.csv'
    - Add this attribute to specify the prefix string for filename. eg: export-filename-prefix="{{filenamePrefix}}
    - Today's date will be concatenated to prefix. eg:- '<prefix_string>-<today's_date>'
    - It will override default config.
4) export-container-id:
    Default: export-menu will be rendered at first TH of table-header
    - Add this attribute to specify the container div to which the export-menu you want to append.
    - If container id is passed then button will be rendered at desired position. It will override default config.

NOTES - 

1) Depends on Papaparse to find correct delimiter for the exported CSV.
2) The table needs to have proper thead and tbody for it to work.
3) Last row that contains total might be offset. Should be removed but not applicable everywhere.
4) On scrolling, the widget goes behind the table since it's not static (unlike the headers).

DECISIONS - 

1) Attaching template (using directive.templateUrl) to the directive doesn't work since we want to place the widget html inside the first th (not anywhere else).
2) To get all rows of the table, table.rows is not used because in case of multiline headers, table.rows was repeating headers. Instead we've looped over table contents.

*/

angular.module("export_functions", [])
.controller('exportFunctionsController', function ($scope, $window) {

    const FORMAT_XLS = "xls";
    const FORMAT_CSV = "csv";
    const DATATYPE_CSV = "data:text/csv;charset=UTF-8";
    const DATATYPE_XLS = "data:application/vnd.ms-excel";
    const ELEMEMT_LI = "LI";

    try {
        $scope.headers_to_ignore_arr = JSON.parse($scope.headers_to_ignore);
        $scope.headers_to_ignore_arr.push("");
    } catch(err) {
        $scope.headers_to_ignore_arr = [""];
        console.log("Export CSV library : Incorrect headers-to-ignore field. It should be a scope array");
    }
    try {
        $scope.additional_data_arr = JSON.parse($scope.additional_data);
    } catch(err){
        $scope.additional_data_arr = [];
        console.log("Export CSV library : Additional data should be array of objects");
    }

    function downloadFile(dataType, data, fileFormat, event) {
        var a = document.createElement('a');
        a.href = dataType + ', ' + encodeURIComponent(data);
        document.body.appendChild(a);
        a.download = getFilename(fileFormat);
        a.click();
        event.preventDefault();
    }

    function hideDropdown(event) {
        setStyle(event, "#fff", "#23527c");
        var elem = event.target.parentElement;
        // Prevent hiding LI item because on clicking image.
        var parentElem = (elem.tagName === ELEMEMT_LI) ? elem.parentElement : elem;
        parentElem.style.display = "none";
    }

    $scope.exportAsExcel = function (event) {
        event.stopPropagation();
        hideDropdown(event, "#fff", "#23527c");
        var tab_text="<table border='2px'><tr>";
        tab_text = getTableData(FORMAT_XLS, $scope.table_element);
        if (tab_text) {
            var ua = $window.navigator.userAgent;
            downloadFile(DATATYPE_XLS, tab_text, FORMAT_XLS, event);
        }
    };

    $scope.exportAsCSV = function (event) {
        event.stopPropagation();
        hideDropdown(event, "#fff", "#23527c");
        var table_data = getTableData(FORMAT_CSV, $scope.table_element);
        var elmnt = event.currentTarget;
        if (table_data) {
            var csv = Papa.unparse({data: table_data});
            downloadFile(DATATYPE_CSV, csv, FORMAT_CSV, event);
        }
    };

    function getFilename(fileFormat) {
        return (($scope.exportFilenamePrefix || 'Export-data') + '__' + getFormattedDate() + '.' + fileFormat);
    }

    function getFormattedDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var formattingOptions = {
            hour: "2-digit",
            minute: "2-digit",
            hour12: false
        };
        var _time = today.toLocaleTimeString('en-IN', formattingOptions);
        _time = _time.replace(':', '');
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '-' + mm + '-' + yyyy + '_' + _time;
        return today;
    }

    // function that loops over table, ignoring pseudo elements
    // returns array containing row objects for csv export, returns table string for Excel export
    function getTableData(export_mode, element){
        var children = element.children();
        var children_arr = Array.prototype.slice.call(children);
        var headers = [];
        var table_data = [];
        var additional_headers = [];
        if($scope.additional_data_arr.length > 0){
            for(var i = 0;i < $scope.additional_data_arr.length;i++){
                $scope.additional_data_arr[i] = flattenObject($scope.additional_data_arr[i]);
            }
            additional_headers = getAllKeys($scope.additional_data_arr);
        }

        var headers_str, table_data_str = "<table border='2px'><tr>";
        for(var i = 0;i < children_arr.length;i++){
            var child = children_arr[i];
            var tagName = child.tagName;
            if(tagName == "THEAD"){
                var header_rows = child.childNodes;
                header_rows = Array.prototype.slice.call(header_rows);
                for(var j = 0;j < header_rows.length;j++){
                    if(header_rows[j].tagName == 'TR'){
                        header_cells = header_rows[j].childNodes;
                        header_cells = Array.prototype.slice.call(header_cells);
                        for(var k = 0;k < header_cells.length;k++){
                            if(header_cells[k].tagName == 'TH'){
                                /*  Only take headers from the lowermost header row, the one that's most specific
                                    applicable when multirow headers are present */
                                var hasFilter = false;
                                /* To prevent adding texts present in filter dropdown options to header list of excel file in firefox */
                                try {
                                    var cNodes = header_cells[k].childNodes;
                                    for (var m = 0; m < cNodes.length; m++) {
                                        var cNodeTag = cNodes[m].tagName;
                                        if (cNodeTag === 'SELECT' || cNodeTag === 'INPUT') {
                                            hasFilter = true;
                                            break;
                                        }
                                    }
                                } catch (e) {
                                    hasFilter = false;
                                }
                                if (header_cells[k].colSpan == 1 && !hasFilter &&
                                    headers.indexOf(header_cells[k].innerText) === -1) {
                                    if ($scope.exportContainerId) {
                                        headers.push(header_cells[k].innerText);
                                    } else {
                                        /* To prevent adding button text to first column header in exported file
                                        when default button is used */
                                        var childNodesTh = header_cells[k].childNodes;
                                        var innerText = childNodesTh[0].innerText;
                                        var headerText = (innerText && innerText.trim() === "EXPORT") ?
                                                         childNodesTh[1].textContent : header_cells[k].innerText;
                                        headers.push(headerText);
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
        
        if(export_mode == FORMAT_XLS){
            headers_filtered = [];
            for(var k = 0;k < headers.length;k++){
                if($scope.headers_to_ignore_arr.indexOf(headers[k]) == -1){
                    headers_filtered.push(headers[k]);
                }
            }
            for(var i = 0;i<additional_headers.length;i++){
                headers_filtered.push(additional_headers[i]);
            }
            headers_str = "<th>" + headers_filtered.join("</th><th>") + "</th>";
            table_data_str += headers_str + "</tr>";
        }
        
        for(var i = 0;i < children_arr.length;i++){
            var child = children_arr[i];
            var tagName = child.tagName;
            if(tagName == "TBODY"){
                var body_rows = child.childNodes;
                body_rows = Array.prototype.slice.call(body_rows);
                var body_row_elements = [];
                for(var j = 0;j < body_rows.length;j++){
                    //Ignoring all pseudo elements like text etc
                    if(body_rows[j].nodeType == Node.ELEMENT_NODE){
                        body_row_elements.push(body_rows[j]);
                    }
                }
                // If there is no data, alert the user.
                if (body_row_elements.length === 0) {
                    $window.alert("Get some data in -__-");
                    return;
                }
                // required since all elements inside tbody might not be tr. might have some pseudo elements
                row_offset = 0;
                for(j = 0;j < body_row_elements.length;j++){
                    if(body_row_elements[j].tagName == 'TR'){
                        body_row_cells = body_row_elements[j].childNodes;
                        body_row_cells = Array.prototype.slice.call(body_row_cells);
                        row_obj = {};
                        column_offset = 0;
                        if (export_mode == FORMAT_XLS) {
                            table_data_str += "<tr>";
                        }

                        for(var k = 0;k < body_row_cells.length;k++){
                            if(body_row_cells[k].tagName == 'TD'){
                                if($scope.headers_to_ignore_arr.indexOf(headers[column_offset]) == -1){
                                    row_obj[headers[column_offset]] = body_row_cells[k].innerText;    
                                    if(export_mode == FORMAT_XLS){ table_data_str += "<td>" + body_row_cells[k].innerText + "</td>" };
                                }
                                column_offset++;
                            }
                        }

                        for(var a = 0;a < additional_headers.length;a++){
                            var header = additional_headers[a];
                            try{
                                if($scope.additional_data_arr[row_offset].hasOwnProperty(header)){
                                    row_obj[header] = $scope.additional_data_arr[row_offset][header];
                                    table_data_str += "<td>" + $scope.additional_data_arr[row_offset][header] + "</td>";
                                } else{
                                    row_obj[header] = "";
                                    table_data_str += "<td></td>";
                                } 
                            }
                            catch(err){
                                row_obj[header] = "";
                                table_data_str += "<td></td>";
                            }
                        }
                        if (export_mode == FORMAT_XLS) {
                            table_data_str += "</tr>";
                        }
                        table_data.push(row_obj);
                        row_offset++;
                    }
                }
                break;
            }

        }
        if (export_mode == FORMAT_XLS) {
            table_data_str += "</table>";
            return table_data_str;
        } else {
            return table_data;
        }   
    }

    //take nested objects and return obj with no nesting
    function flattenObject(data) {
        var result = {};
        function recurse (cur, prop) {
            if (Object(cur) !== cur) {
                result[prop] = cur;
            }else if(Array.isArray(cur)) {
                for(var i=0, l=cur.length; i<l; i++){
                    recurse(cur[i], prop ? prop+"."+i : ""+i);
                }
                if (l == 0){
                    result[prop] = [];
                }
            }else{
                var isEmpty = true;
                for (var p in cur) {
                    isEmpty = false;
                    recurse(cur[p], prop ? prop+"."+p : p);
                }
                if (isEmpty){
                    result[prop] = {};
                }
            }
        }
        recurse(data, "");
        return result;
    }

    function getAllKeys(arr){
        var keys = [];
        for(var i = 0;i<arr.length;i++){
            obj = arr[i];
            for(key in obj){
                if(obj.hasOwnProperty(key)){
                    if(keys.indexOf(key) == -1){
                        keys.push(key);
                    }
                }
            }
        }
        console.log(keys);
        return keys;
    }

    function setStyle(event, backgroundColor, textColor) {
        var elmnt = event.currentTarget;
        elmnt.style.backgroundColor = backgroundColor;
        elmnt.style.textDecoration = "none";
        elmnt.style.color = textColor;
        event.preventDefault();
    }

    /* Add style for dropdown menu on mouseover; background-color: theme-color, text-color: white */
    $scope.addStyle = function (event) {
        setStyle(event, "#0193ac", "#fff");
    };

    /* Remove style for dropdown menu on mouseleave; background-color: white, text-color: blue */
    $scope.removeStyle = function (event) {
        setStyle(event, "#fff", "#23527c");
    };
})
.directive('exportFunctions', function($compile) {
    var directive = {};
    //only allowed to be used as attribute
    directive.restrict = 'A';
    //directive scope variables attached to variables from parent scope
    directive.scope = {exportContainerId: '@exportContainerId', headers_to_ignore:'@exportIgnoreHeaders', additional_data:'@exportAdditionalData', exportFilenamePrefix: '@exportFilenamePrefix'};
    directive.controller = 'exportFunctionsController';
    // gets attached to every instance of the directive
    directive.link = function (scope, element, attributes) {

        var export_container;
        var export_btn;
        if (scope.exportContainerId) {
            export_container = $("#" + scope.exportContainerId);
            export_btn = angular.element('<button class="btn btn-success" style="font-size: 12px;height:34px;">Export <span class="caret"></span></button></button>');
        } else {
            export_container = angular.element('<div class="btn-group" style="font-size: 12px !important;position: absolute;left: 0;top: -24px;"></div>');
            export_btn = angular.element('<button id="export-menu" class="btn btn-success btn-sm" style="border-radius:3px;padding:2px;padding-right:5px;z-index:1000;"><i id="export-menu-icon" class="glyphicon glyphicon-menu-hamburger"></i> EXPORT</button>');
        }
        var dropdownTemplate = angular.element('<ul class="export-dropdown" role="menu" ' +
            'style="list-style: none;position: absolute;top: 100%;left: 0;display: none;float: left;min-width: 160px;padding: 5px 0;z-index:1000;' +
            'margin: 2px 0 0;background-color: #ffffff;border: 1px solid #ccc;border: 1px solid rgba(0, 0, 0, 0.2); -webkit-border-radius: 6px; -moz-border-radius: 6px;' +
            'border-radius: 6px; -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2); -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2); ' +
            '-webkit-background-clip: padding-box; -moz-background-clip: padding;background-clip: padding-box;">' +
            '<li style="color:#23527c;cursor: pointer;font-size: 12px;padding-left: 10px;padding-bottom:3px;padding-top:3px;text-align: left;" ng-mouseover="addStyle($event)" ng-mouseleave="removeStyle($event)" ng-click="exportAsCSV($event)"><img src="/static/website/images/export_csv.png" width="18px"> CSV</li>' +
            '<li style="color:#23527c;cursor: pointer;font-size: 12px;padding-left: 10px;text-align: left;padding-bottom:3px;padding-top:3px;" ng-mouseover="addStyle($event)" ng-mouseleave="removeStyle($event)" ng-click="exportAsExcel($event)"><img src="/static/website/images/export_xls.png" width="18px"> EXCEL</li>' +
            '</ul>');

        export_btn.bind("click", function (e) {
            e.stopPropagation();
            e.preventDefault();
            var clickedElem = e.currentTarget;
            var elem = (clickedElem.id === "export-menu-icon") ? clickedElem.offsetParent.nextElementSibling : clickedElem.nextElementSibling;
            if (elem) {
                elem.style.display = (elem.style.display === "none") ? "block" : "none";
            }
        });

        /* Hide the export dropdown menu on clicking anywhere else on window */
        angular.element(window.document).bind("click", function (e) {
            e.stopPropagation();
            var exportMenus = document.getElementsByClassName("export-dropdown");
            for (var i = 0; i < exportMenus.length; i++) {
                exportMenus[i].style.display = "none";
            }
        });

        scope.table_element = element;
        export_container.append(export_btn);
        export_container.append(dropdownTemplate);
        /*  Insert the widget HTML inside the first TH inside THEAD if no container id found.
            Breaking as soon as we find it. */
        if (!scope.exportContainerId) {
            var children = element.children();
            // Children is of the type NodeList. We need to convert it to array so that we can loop over it.
            var children_arr = Array.prototype.slice.call(children);
            var inserted = false;
            for (var i = 0; i < children_arr.length && !inserted; i++) {
                var child = children_arr[i];
                var tagName = child.tagName;
                if (tagName == "THEAD") {
                    var header_rows = child.childNodes;
                    header_rows = Array.prototype.slice.call(header_rows);
                    for (var j = 0; j < header_rows.length && !inserted; j++) {
                        if (header_rows[j].tagName == 'TR') {
                            header_cells = header_rows[j].childNodes;
                            header_cells = Array.prototype.slice.call(header_cells);
                            for (var k = 0; k < header_cells.length && !inserted; k++) {
                                if (header_cells[k].tagName == 'TH') {
                                    element.parent().css('position', 'relative');
                                    var first_th = angular.element(header_cells[k]);
                                    first_th.css('position', 'relative');
                                    first_th.prepend(export_container);
                                    inserted = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        // Attaches the directive DOM with the associated scope functions as defined in the controller.
        $compile(export_container)(scope);
    };
    return directive;
});
