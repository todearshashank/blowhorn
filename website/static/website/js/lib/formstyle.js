/* form - style.js */

(function() {
	var val = $.fn.val;

	$.fn.val = function() {
		var args = Array.prototype.slice.call(arguments, 0);
		var ret = val.apply(this, args);

		if (args.length > 0)
			$(this).trigger('formstyle-refresh');

		return ret;
	}
})();

jQuery(function($){

	$.fn.formstyle = function() {

		this.each(function() {
			var self = $(this);

			if (self.data('formstyle')) return;

			// DROP DOWN STYLING
			if (self.is('select') || self.find('select').length) {
				self = self.is('select') ? self : self.find('select');
				self.each(function() {
					var $this = $(this);
					var wrap = $('<span class="select-holder" />');
					$this.wrap(wrap).data('formstyle', wrap);

					var title = $('option:selected',this).text();

					$this
						.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
						.after('<span class="select">' + title + '</span>')
						.bind('change formstyle-refresh', function(){
							val = $this.find('option:selected').text();
							$this.next().text(val);

							if($this.hasClass('error')) {
								$this.siblings('span.select').addClass('error-select');
							}
						});
				});
			}

		})
	}

});
