angular.module('blowhornApp', [
    'ngRoute',
    'ngAnimate',
    'ng.deviceDetector',
    'ui.bootstrap',
    'angular-sortable-view',
    'ja.qr',
    'ngIntlTelInput'
]);

// NOTE: This version number has to be changed if any changes been done in this app
let version = 1.1;
angular.module('blowhornApp')
    .config(appConfig)
    .run(['$route', '$rootScope', '$location', runApp]);

function appConfig($routeProvider, $locationProvider, $httpProvider, ngIntlTelInputProvider) {

    ngIntlTelInputProvider.set({
        allowDropdown: false,
        separateDialCode: true,
        nationalMode: true,
    });

    $routeProvider.
    when('/booking/', {
        templateUrl: '/static/website/html/home/booking_basic.html',
        controller: 'bookingBaiscFormController',
        resolve: {
            ctrlOptions: function () {
                return {
                    page: 'booking',
                };
            },
            masterData: function (dataService) {
                return dataService.getDataFromJson(current_build, 'home');
            },
        }
    }).
    when('/book', {
        templateUrl: '/static/website/html/home/main.html',
        controller: 'blowhornController',
        resolve: {
            ctrlOptions: function () {
                return {
                    page: 'home',
                };
            },
            masterData: function (dataService) {
                return dataService.getDataFromJson(current_build, 'home');
            },
        }
    }).
    when('/', {
        templateUrl: '/static/website/html/home/main.html',
        controller: 'blowhornController',
        resolve: {
            ctrlOptions: function () {
                return {
                    page: 'home',
                };
            },
            masterData: function (dataService) {
                return dataService.getDataFromJson(current_build, 'home');
            },
        }
    }).
    when('/_=_', {
        // This is required for facebook login, more info: https://developers.facebook.com/x/bugs/318390728250352/
        templateUrl: '/static/website/html/home/main.html',
        controller: 'blowhornController',
        resolve: {
            ctrlOptions: function () {
                return {
                    page: 'home',
                };
            },
            masterData: function (dataService) {
                return dataService.getDataFromJson(current_build, 'home');
            },
        }
    });
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.headers.common.AccessControlAllowOrigin = '*';
    // Use the HTML5 History API
    $locationProvider.html5Mode({
        enabled: true,
        // rewriteLinks: false
        requireBase: false
    });
}

function runApp($route, $rootScope, $location) {
    var original = $location.path;
    $location.path = function (path, no_reload) {
        if (no_reload === true) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
};
