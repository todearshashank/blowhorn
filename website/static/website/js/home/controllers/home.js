var s;
var logging = false;
if (!logging) {
  console.log = function () {};
}
const DEFAULT_BUILD = 'blowhorn';

angular
  .module('blowhornApp')
  .controller('blowhornController', function (
    $scope,
    $log,
    $timeout,
    $interval,
    $http,
    $filter,
    $location,
    $anchorScroll,
    blowhornService,
    generalService,
    mapHelpers,
    deviceDetector,
    ctrlOptions,
    masterData,
    $window
  ) {
    // Scroll to top of page first
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    $scope.page = ctrlOptions.page;
    if (logging) {
      s = $scope;
    }
    $scope.deviceDetector = deviceDetector;
    var coach_marks_shown_count = 0;
    $scope.isDefaultBuild = current_build === DEFAULT_BUILD;

    const ICON_WAYPOINT_MARKER = `/static/website/images/${current_build}/markers/stop-marker.svg`;
    const ICON_PICKUP_MARKER = `/static/website/images/${current_build}/markers/Blue-Marker@1.5x.png`;
    const ICON_DROPOFF_MARKER = `/static/website/images/${current_build}/markers/Purple-Marker@1.5x.png`;
    const ICON_GHOST_MARKER = `/static/website/images/${current_build}/markers/ghost-marker@2x.png`;

    const SAVED_ADDRESS_HELPER_FLAG = 'hideSavedAddressHelperText';
    const REORDER_WAYPOINT_HELPER_FLAG = 'hideWaypointReorderHelperText';
    const GENERAL_COACH_MARKS_SHOWN_COUNT = 'general_coach_marks';
    const GENERAL_COACH_MARKS_SHOW_LIMIT = 3;
    const CHAT_HELP_SHOWN_FLAG = 'chatHelpDisplayed';

    const DISTANCE_MULTIPLIER = 1;
    const DURATION_RANGE_MULTIPLIER = 1.5;
    const MAX_ADVANCE_BOOKING_DAYS = 14;
    const MAX_CALENDER_DAYS_TO_SHOW = 21;
    const SPLIT_CHAR = ' ';
    const FIXED_DISCOUNT = 'fixed';
    const PERCENTAGE_DISCOUNT = 'percentage';
    const PAYMENT_MODE_CASH = 'Cash';
    const PAYMENT_MODE_ONLINE = 'Online';
    const MESSAGE_OUT_OF_COVERAGE = 'Out of Coverage';
    const MESSAGE_REBOOK_OUT_OF_COVERAGE = 'We have updated our service area. Service is not available in requested location.';
    const DEFAULT_CURRENCY = default_currency;

    $scope.more = false;

    /* MESSAGES */
    const WAYPOINT_OPTIMIZE_CONFIRMATION =
      'Optimizing path will change the order of waypoints. Do you want to continue?';
    const PASSWORD_RESET_LINK_SUCCESS =
      'We have sent a password reset link to your email. Link expires in 6 hrs.';
    const GET_THE_APP_SUCCESS =
      'We have sent an SMS with a link to our mobile app!';

    $scope.termsAndConditionsUrl = $scope.isDefaultBuild ? '/termsofservice' : '/static/website/images/terms_and_conditions.pdf';
    $scope.privacyPolicyUrl = $scope.isDefaultBuild ? '/privacypolicy': '/static/website/images/terms_and_conditions.pdf';
    $scope.agreement_content = "<a href='/termsofservice' class='link-red' title='Terms of Service' target='_blank'>Terms of Service</a> and <a href='/privacypolicy' class='link-red' title='Privacy Policy' target='_blank'>Privacy Policy</a>";
    if (!$scope.isDefaultBuild) {
      $scope.agreement_content = "I agree to <a href=/static/website/images/terms_and_conditions.pdf class='link-red' title='Combined Website Policy' target='_blank'>Combined Website Policy</a>";
    }
    $scope.view_mode = 'ondemand';
    $scope.faq_tab = 'c2c';
    $scope.faq = [];
    $scope.enterprise = [];
    $scope.getAppContent = {};
    $scope.currency = DEFAULT_CURRENCY;
    $scope.country_code = country_code;
    $scope.isd_dialing_code = isd_dialing_code;
    $scope.enterprise_send = false;
    $scope.solutions_send = false;
    $scope.homePageIntroData = masterData.intro;
    $scope.homePageOfferingData = masterData.offerings;
    $scope.current_build = current_build;
    $scope.homepageUrl = homepage_url;
    $scope.userAvatarUrl = `/static/website/images/${current_build}/avatar.png`;
    $scope.logoUrl = `/static/website/images/${current_build}/logo/logo.png`;
    $scope.showHomeLoader = false;
    $scope.couponData;
    $scope.moreData = [];
    var currentCityIndex = 0;
    var currentWeDoItAllIndex = 0;
    var weDoItAllList = [
      'Line haul delivery',
      'Spot delivery',
      'Fixed',
      'Milk run',
      'Enterprise'
    ];
    $scope.weDoItAll = weDoItAllList[0];
    if (!angular.isDefined($scope.spinner)) {
      $scope.spinner = $interval(() => {
        var cityElement = angular.element(document.querySelector('#cityName'));
        var weDoItAllElement = angular.element(
          document.querySelector('#weDoItAll')
        );
        if (cityElement && $scope.city_list) {
          currentCityIndex++;
          if (currentCityIndex >= $scope.city_list.length) {
            currentCityIndex = 0;
          }
          $scope.cityName = $scope.city_list[currentCityIndex];
          if ($scope.city_list.length > 1) {
            cityElement.removeClass('reset');
            cityElement.addClass('spin');
          }
        }
        if (weDoItAllElement) {
          currentWeDoItAllIndex++;
          if (currentWeDoItAllIndex >= weDoItAllList.length) {
            currentWeDoItAllIndex = 0;
          }
          $scope.weDoItAll = weDoItAllList[currentWeDoItAllIndex];
          weDoItAllElement.removeClass('reset');
          weDoItAllElement.addClass('spin');
        }
        var timeout = $timeout(() => {
          if (cityElement) {
            cityElement.removeClass('spin');
            cityElement.addClass('reset');
          }
          if (weDoItAllElement) {
            weDoItAllElement.addClass('reset');
            weDoItAllElement.removeClass('spin');
          }
        }, 1200);
      }, 2000);
    }
    var real_coupon_code = '';
    var bbox_bottom_left = null;
    var bbox_top_right = null;
    var placesservice = null;
    var current_center = null;
    var device_subtype = '';
    var googleLoaded = false;
    var mapBooking = null;
    var previously_geocoded_pickup_address = '';
    var previously_geocoded_dropoff_address = '';
    var pickup_city = null;
    var dropoff_city = null;
    var pickupLocation = null;
    var pickupLocationPoint = null;
    var dropoffLocation = null;
    var dropoffLocationPoint = null;
    var latlngBounds;
    var default_latlng;
    var autocomplete;
    var pos;
    var i;
    var display_chat_help = true;
    var data;
    $scope.max_advance_booking_days = MAX_ADVANCE_BOOKING_DAYS;
    var gmap_addr = {
      street_number: 'short_name',
      premise: 'short_name',
      subpremise: 'short_name',
      route: 'long_name',
      neighborhood: 'short_name', //Area1.1.1.1
      sublocality_level_3: 'long_name', // Area1.1.1
      sublocality_level_2: 'long_name', // Area1.1
      sublocality_level_1: 'long_name', // Area1
      sublocality: 'long_name', // General Area
      locality: 'long_name', // City
      postal_code: 'short_name', // Pin code
      administrative_area_level_1: 'long_name', // State
      country: 'long_name' // Country
    };

    $scope.no_scroll = false;
    $scope.loaded = false;
    $scope.regex = {
      name: /^\[a-zA-Z .]+$/,
      email: /[a-zA-Z0-9_\.]+@[a-zA-Z0-9_\.]+\.[a-zA-Z0-9]+/,
      integer: /^\d+$/,
      mobileNumber: /^\d{9,10}$/,
      altPhone: /[0-9]{8,10}/,
      otp: /^\d{4,}$/,
      coupon: /^[a-zA-Z0-9]{2,}$/,
      latlng: /([+-]?\d+\.\d+)(\s*,\s*|\s+)([+-]?\d+\.\d+)/,
      w3wPattern: /^[a-zA-Z]*\.[a-zA-Z]*\.[a-zA-Z]*$/,
      gstin: /^\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}$/
    };

    $scope.modal = {
      login: 'login-modal',
      registration: 'register-modal',
      passwordRecovery: 'password-recover',
      changePassword: 'modal-password',
      getApp: 'getApp-modal',
      profile: 'profile-modal'
    };

    $scope.jobs = [];
    $scope.coverage_areas = {};
    var coverage_area_coordinates = null;
    var dropoff = {};
    var wordPattern = /^[a-zA-Z]*\.[a-zA-Z]*\.[a-zA-Z]*$/; // Pattern for what3words.
    var w3wPickupLoc;
    var w3wDropoffLoc;
    var polygon;
    var infowindowOptions = {
      maxWidth: 200
    };

    $scope.polygons = {};
    $scope.bounds = {};
    $scope.user_logged_in = user_logged_in == 'True';
    $scope.b_user_logged_in = b_user_logged_in == 'True';
    $scope.b_myfleet_required = b_myfleet_required == 'True';
    $scope.is_staff_user = false;
    $scope.is_login_screen = false;
    $scope.c2c_price_web_text = '';
    $scope.branding_url = '';
    $scope.authorized_branding_user = false;

    $scope.b_myfleet_required =
      b_myfleet_required == 'True' ||
      (blowhornService.userProfile &&
        blowhornService.userProfile.b_myfleet_required);

    $scope.intRegex = /^\d+$/;
    $scope.phoneregEx = /^\d{10}$/;
    $scope.mobileNumberRegex = /^[789]\d{9}$/;
    $scope.otpregEx = /^\d{4,}$/;
    $scope.couponregEx = /^[a-zA-Z0-9]{2,}$/;
    $scope.alertMessage = '';
    $scope.isDisabled = false;
    $scope.outOfCoverage = false;
    $scope.formsubmitted = false;
    $scope.selected_vehicle_class = null;
    $scope.selected_time = null;
    $scope.selected_day = null;
    $scope.selected_rate_text = '';
    $scope.podCost = 0;
    $scope.isPodApplicable = false;
    $scope.timeslots = [];
    $scope.moreItems = [];
    $scope.time = {};

    $scope.gstinPlaceholder = $scope.isDefaultBuild ? 'GSTIN (Optional)' : 'Company VAT number (Optional)';
    $scope.signupDetails = {};
    $scope.timeFrame = null;
    $scope.disableCoupon = false;
    $scope.disableBooking = true;
    $scope.discount_message = '';
    $scope.discount_value = 0;
    $scope.invalidCoupon = '';
    $scope.saved_locations = [];
    $scope.userProfile = null;
    $scope.items = [];
    $scope.bookingComplete = false;
    $scope.saveBtnText = 'Save';
    $scope.mdate = '';
    $scope.track = {
      booking_id: null
    };
    $scope.parcelTrack = {
      ref_number: null
    };
    $scope.otp = {
      email: '',
      mobile: '',
      mobileReadonly: true,
      enableResend: false,
      activationNeeded: false,
      activationEmailSent: false,
      activationOtpSent: false
    };
    $scope.get_the_app = {
      phone_number: ''
    };
    $scope.contact_us = {
      disabled: true,
      message_sent: false,
      name: '',
      email: '',
      phone: '',
      message: ''
    };
    $scope.solutionContact = {
      fullname: '',
      contactNumber: '',
      error: ''
    };
    $scope.availableLabourOptions = [];
    $scope.tipOptions = [20, 50, 100];

    function getErrorMessage(data) {
      // Using default error messages from django response.
      var _form = data.form;
      if (!_form) {
        _form = data.responseJSON.form;
      }
      var nonFieldErrors = _form.errors;
      if (nonFieldErrors.length > 0) {
        return nonFieldErrors[0];
      } else {
        var fields = _form.fields;
        for (var field in fields) {
          var fieldErrors = fields[field].errors;
          if (fieldErrors.length > 0) {
            return fieldErrors[0];
          }
        }
      }
    }

    $scope.enableMore = function (bool){
      $scope.more = bool;
    }

    $scope.contact_us_send = function (contact, flag) {
      blowhornService
        .contact_us(contact)
        .then(function () {
          $scope.contact_us = {
            disabled: true,
            message_sent: true,
            name: '',
            email: '',
            phone: '',
            message: ''
          };
          if (flag) {
            $scope[flag] = true;
            setTimeout(() => $scope[flag] = false, 3000);
          }
        })
        .catch(function (data) {
          console.log('Failed to contact', data);
        });
    };

    $scope.$watchGroup(
      [
        'contact_us.name',
        'contact_us.email',
        'contact_us.phone',
        'contact_us.message'
      ],
      function (data) {
        console.log(data);
        var name, email, phone, message;
        name = data[0];
        email = data[1];
        phone = data[2];
        message = data[3];
        var email_valid = email && $scope.regex.email.test(email);
        var phone_valid = phone ? $scope.regex.altPhone.test(phone) : true;
        $scope.contact_us.disabled = !name || !email_valid || !message || !phone_valid;
      }
    );

    $scope.get_the_app = {
      disabled: true,
      mobile: '',
      message_sent: false
    };

    $scope.$watch('get_the_app.mobile', function (value) {
      $scope.get_the_app.disabled = !$scope.regex.mobileNumber.test(value);
    });

    $scope.get_the_app_get = function () {
      $scope.get_the_app.disabled = true;
      var data = {
        mobile: $scope.get_the_app.mobile
      };
      blowhornService
        .get_the_app(data)
        .then(function () {
          $scope.get_the_app.message_sent = true;
          showMessagePopup(GET_THE_APP_SUCCESS, $scope.modal.getApp);
        })
        .catch(function (data) {
          console.log('Failed to contact', data);
        });
    };

    $scope.infobox_open = 'Pickup';
    $scope.show = {
      booking_form: false,
      Mobile: true,

      download_app: true,
      save_location_option: false,
      save_address: false,
      otp_invalid: false,
      add_edit_contact: false,
      ghost: true,
      help: true,
      save_route: false,
      login_tooltip: true,
      where_section: false,
      when_section: false,
      what_section: false,
      vehicle_section: false,
      labour_coupon_section: false,
      booking_summary: false,
      coupon_success: false,
      exit_screen: false,
      vehicle_info: false,
      booking_otp: false,
      CouponInvalid: false,
      fareEstimationMessage: false,
      coupon_terms_and_conditions: false,
      loadingFare: false,
      no_news: false,
      loading_news: true,
      fare_info: false,
      tip_info: false
    };
    $scope.more_of_vehicle = {};
    $scope.booking_error_msg = '';
    $scope.mobileVerificationRequired = false;

    $scope.section = {
      where_done: false,
      when_done: false,
      what_done: false,
      vehicle_done: false,
      confirm_done: false
    };

    $scope.current_section = '';
    $scope.continue_is_enabled = false;
    $scope.next_section = 'where';
    $scope.prev_section = 'home';

    $scope.directionsService = initDirectionsService();
    var rendererOptions = {
      draggable: false,
      suppressMarkers: true,
      preserveViewport: true
    };
    $scope.tracking_url = '';
    $scope.friendly_id = '';
    $scope.selectedCity = null;
    $scope.selectedCityBounds = {
      pickup: initLatLngBounds(),
      dropoff: initLatLngBounds(),
    }
    $scope.selectedCityPolygon = null;
    $scope.directionsDisplay = initDirectionsRenderer(rendererOptions);
    $scope.map = {};
    $scope.ghost_markers = [];

    $scope.bookingObject = generalService.getBookingObject(
      ICON_PICKUP_MARKER,
      ICON_DROPOFF_MARKER
    );
    $scope.saveRouteObject = {};

    $scope.total_distance_meters = null;
    $scope.total_time_seconds = null;

    if (generalService.storageAvailable('localStorage')) {
      $scope.hideSavedAddressHelperText = generalService.getValueFromLocalStorage(
        SAVED_ADDRESS_HELPER_FLAG
      );
      $scope.show.showWaypointDragHelp = Boolean(
        generalService.getValueFromLocalStorage(REORDER_WAYPOINT_HELPER_FLAG)
      );
      coach_marks_shown_count = parseInt(
        generalService.getValueFromLocalStorage(
          GENERAL_COACH_MARKS_SHOWN_COUNT
        ) || 0
      );
      console.log(
        'Coach marks count',
        typeof coach_marks_shown_count,
        coach_marks_shown_count
      );
      $scope.show.help =
        coach_marks_shown_count <= GENERAL_COACH_MARKS_SHOW_LIMIT;
      console.log('Coach show', $scope.show.help);

      display_chat_help = !generalService.getValueFromLocalStorage(CHAT_HELP_SHOWN_FLAG);
      $scope.enterprise = generalService.getEnterpriseContent();
      $scope.getAppContent = generalService.getAppContent();
    } else {
      console.log('Local storage not available...!');
    }


    $scope.show.download_app =
      generalService.showAppDownload === false ?
      generalService.showAppDownload :
      true;
    $scope.closeDownloadLink = function () {
      $scope.show.download_app = false;
      generalService.showAppDownload = false;
    };

    if ($scope.user_logged_in) {
      disableOtp();
    }

    $scope.$watch('map.mapBooking', function (value) {
      $scope.directionsDisplay.setMap(value);
    });

    $scope.isInEmbed = isEmbed;
    $scope.brandName = brandName;
    $scope.showHomePage = loadHome && !isEmbed;
    $scope.bookingConfirmedNextLinkText = `Go back to ${ $scope.user_logged_in ? 'dashboard' : 'home' }`;
    $scope.vehicle_dict = {};
    coverage_area_coordinates = global_params.city_coverage;
    updateVehicleDict(global_params.vehicle_classes);
    var contract_cities = [];
    if (isEmbed && !loadHome) {
      contract_cities = global_params.contract_cities;
    } else {
      contract_cities = Object.keys($scope.vehicle_dict);
    }
    $scope.items = global_params.items;
    $scope.offerBanner = angular.copy(global_params.offerbanner);
    $scope.city_coverage = global_params.city_coverage;
    $scope.city_list = contract_cities.sort();
    $scope.cityName = $scope.city_list[0];
    $scope.cityContactDetails = global_params.city_contact_details;
    $scope.defaultCity = $scope.city_list ? $scope.city_list[0] : '';
    $scope.defaultContact = $scope.cityContactDetails[$scope.defaultCity];
    $scope.states = states;
    $('#alert_message').hide();
    googleLoaded = true;

    for (var city in coverage_area_coordinates) {
      var coverages = coverage_area_coordinates[city];
      $scope.coverage_areas[city] = mapHelpers.convertCoverageToGoogleObjects(coverages['coverage']);
    }

    for (var city in $scope.coverage_areas) {
      pickup = mapHelpers.createPolygon({
        paths: $scope.coverage_areas[city]['pickup'],
        visible: false
      });
      dropoff = mapHelpers.createPolygon({
        paths: $scope.coverage_areas[city]['dropoff'],
        visible: false
      });
      $scope.polygons[city] = {
        pickup: pickup,
        dropoff: dropoff
      };
      $scope.bounds[city] = {
        'pickup': initLatLngBounds(),
        'dropoff': initLatLngBounds(),
      }
      setBoundForCoverage('pickup');
      setBoundForCoverage('dropoff');
    }

    function setBoundForCoverage(key) {
      $scope.coverage_areas[city][key].map(function (_location) {
        $scope.bounds[city][key].extend(_location);
      });
    }

    function updateVehicleDict(vehicles) {
      vehicles
        .filter(item => {
          return item.vehicles.length > 0;
        })
        .forEach(item => {
          item.vehicles.forEach(vc => {
            vc.selected_contract_id = vc.packages[0].contract_id;
          });
          $scope.vehicle_dict[item.city] = item.vehicles;
        });

      if ($scope.selectedCity) {
        $scope.vehicle_classes = $scope.vehicle_dict[$scope.selectedCity];
        if (!$scope.bookingObject.selected_vehicle_class) {
          $scope.bookingObject.selected_vehicle_class =
            $scope.vehicle_classes[0].classification;
          $scope.bookingObject.selected_contract_id =
            $scope.vehicle_classes[0].packages[0].contract_id;
          $scope.selected_rate_text =
            $scope.vehicle_classes[0].packages[0].description.base.text;
          $scope.more_of_vehicle = $scope.vehicle_classes[0];
          $scope.currency = $scope.vehicle_classes[0].packages[0].currency;
          $scope.setPodCost($scope.vehicle_classes[0].packages[0]);
        }
      }
      $scope.$applyAsync();
    }

    function getUserProfile() {
      blowhornService
        .getUserProfile()
        .then(function (data) {
          $scope.userProfile = data.message;
          $scope.show.save_location_option = true;
          $scope.b_myfleet_required = data.message.b_myfleet_required;
          blowhornService.userProfile = $scope.userProfile;
          $scope.disable_category = $scope.canEditCustomerCategory();
          $scope.is_staff_user = $scope.userProfile.is_staff;
          $scope.$applyAsync();
        }).catch(function (data) {
          console.log('Failed to get user profile');
        });
    }

    function getVehicleClassesAndRatePackages() {
      blowhornService
        .getVehicleClassesAndRatePackages()
        .then(function (result) {
          global_params.vehicle_classes = result.data;
          updateVehicleDict(result.data);
          $scope.$applyAsync();
        })
        .catch(function (data) {
          console.log('Failed to get vehicle classes');
        });
    }

    $scope.canEditCustomerCategory = function () {
      return (
        $scope.userProfile.is_business_user || !$scope.userProfile.is_org_admin
      );
    };

    if (blowhornService.userProfile == null) {
      if ($scope.user_logged_in) {
        getUserProfile();
        getVehicleClassesAndRatePackages();
      }
    } else {
      $scope.userProfile = blowhornService.userProfile;
      //$scope.show.ghost = (rebook_data == null);
      $scope.disable_category = $scope.canEditCustomerCategory();
      $scope.show.save_location_option = true;
      $scope.$applyAsync();
    }

    $scope.$watch('userProfile.saved_locations', function (saved_locations) {
      console.log(saved_locations);
      // reset existing markers
      $scope.ghost_markers.map(function (marker) {
        marker.setMap(null);
      });
      var saved_location;
      var ghost_icon = {
        url: ICON_GHOST_MARKER
      };

      $scope.ghost_markers = [];
      if (!saved_locations) {
        return;
      }

      saved_locations.map(function (saved_location, i) {
        var markerOptions = {
          draggable: false,
          optimized: false,
          zIndex: 1000,
          position: saved_location.geopoint,
          icon: ghost_icon
        };
        var map = null;
        if ($scope.show.ghost) {
          map = $scope.map.mapBooking;
        }
        var marker = initMarker(map, markerOptions);
        $scope.ghost_markers.push(marker);
        infobox = mapHelpers.createGhostInfobox(
          $scope,
          $scope.map.mapBooking,
          marker,
          saved_location.name,
          'userProfile.saved_locations[' + i + ']'
        );
        hover_infobox = mapHelpers.createGhostHoverInfobox(
          $scope,
          $scope.map.mapBooking,
          marker,
          saved_location.name,
          'userProfile.saved_locations[' + i + ']'
        );
        marker.addListener('click', function () {
          // On click open the main infobox and close the hover infobox
          this.infobox.openIfClosed();
          this.hover_infobox.close();
        });
        marker.addListener('mouseover', function () {
          // On mouseover show the hover infobox iff the infobox is not open
          if (!this.infobox.getVisible()) {
            this.hover_infobox.openIfClosed();
          }
        });
        marker.addListener('mouseout', function () {
          this.hover_infobox.close();
        });
        saved_location.marker = marker;
      });
    });

    $scope.$watch('show.ghost', function (value) {
      console.log('Show Ghost', value);
      if (value == true) {
        $scope.ghost_markers.map(function (marker) {
          marker.setMap($scope.map.mapBooking);
        });
      } else {
        $scope.ghost_markers.map(function (marker) {
          marker.setMap(null);
        });
      }
    });

    function calculateFinalFare(applicableTime, stepSize, rate) {
      return Math.ceil(applicableTime / stepSize) * parseFloat(rate);
    }

    function doEstimation(vehicle_slabs, base_pay, timeVal, distanceVal) {
      // Assuming that sell-rate slabs are sorted based on respective attributes.
      var estimatedFare = angular.copy(base_pay);
      if ($scope.bookingObject.selectedLabour) {
        estimatedFare += $scope.bookingObject.selectedLabour.total_labour_cost;
      }

      if ($scope.bookingObject.podRequired) {
        estimatedFare += parseInt($scope.podCost);
      }

      if (
        $scope.bookingObject.couponCode &&
        $scope.bookingObject.discount_value
      ) {
        if ($scope.bookingObject.discount_type === FIXED_DISCOUNT) {
          $scope.discount_value = $scope.bookingObject.discount_value;
          estimatedFare = Math.max(estimatedFare - $scope.discount_value, 0);
        } else if ($scope.bookingObject.discount_type === PERCENTAGE_DISCOUNT) {
          var discount_value = $scope.bookingObject.discount_value / 100;
          if (!$scope.bookingObject.max_allowed_discount) {
            discount_value = estimatedFare * discount_value;
          } else {
            discount_value = Math.min(
              $scope.bookingObject.max_allowed_discount,
              estimatedFare * discount_value
            );
          }
          $scope.discount_value = discount_value;
          estimatedFare -= discount_value;
        }
      }

      for (var i = 0; i < vehicle_slabs.length; i++) {
        var applicableVal = 0;
        switch (vehicle_slabs[i].attribute) {
          case 'Minutes':
            applicableVal = angular.copy(timeVal);
            break;

          case 'Hours':
            applicableVal = angular.copy(timeVal) / 60;
            break;

          case 'Kilometres':
            applicableVal = angular.copy(distanceVal);
            break;

          default:
            console.log('Slab attribute is from out of the box');
        }

        var slab_start = parseFloat(vehicle_slabs[i].start);
        var slab_end = parseFloat(vehicle_slabs[i].end);
        if (applicableVal <= slab_start) {
          return estimatedFare;
        } else if (applicableVal > slab_start && applicableVal > slab_end) {
          valForCalculation = slab_end - slab_start;
        } else if (applicableVal > slab_start && applicableVal < slab_end) {
          valForCalculation = applicableVal - slab_start;
        }
        estimatedFare += calculateFinalFare(
          valForCalculation,
          vehicle_slabs[i].step_size,
          vehicle_slabs[i].rate
        );
      }
      return estimatedFare;
    }

    $scope.estimateFare = function () {
      /*  PARAMS: base_pay, date (not used as of now), distance, time
          NOTE: Assuming sell-rate for future also same as current */
      $scope.bookingObject.estimatedInitFare = 0;
      $scope.bookingObject.estimatedExtendedFare = 0;
      $scope.show.fareEstimationMessage =
        new Date($scope.selected_day) > new Date();
      if (!$scope.more_of_vehicle || !$scope.more_of_vehicle.packages) {
        return;
      }
      var rate = $scope.more_of_vehicle.packages.find(
        vc => vc.contract_id === $scope.bookingObject.selected_contract_id
      );
      var loading_unloading_time = $scope.more_of_vehicle.time;
      var sell_rate = rate ? rate.sell_rate : {};
      var vehicle_slabs = sell_rate.sellvehicleslab_set || [];
      var base_pay = parseFloat(sell_rate.base_pay);
      $scope.timeInMinutes = $scope.total_time_seconds ?
        $scope.total_time_seconds / 60 :
        0;
      $scope.distanceInKms = $scope.total_distance_meters ?
        $scope.total_distance_meters / 1000 :
        0;

      $scope.bookingObject.estimatedInitFare = doEstimation(
        vehicle_slabs,
        base_pay,
        $scope.timeInMinutes +
        (loading_unloading_time.loading_mins ?
          loading_unloading_time.loading_mins :
          0) +
        (loading_unloading_time.unloading_mins ?
          loading_unloading_time.unloading_mins :
          0),
        $scope.distanceInKms
      );
      $scope.bookingObject.estimatedExtendedFare = doEstimation(
        vehicle_slabs,
        base_pay,
        $scope.timeInMinutes * 1.2 +
        (loading_unloading_time.loading_mins ?
          loading_unloading_time.loading_mins :
          0) +
        (loading_unloading_time.unloading_mins ?
          loading_unloading_time.unloading_mins :
          0),
        $scope.distanceInKms * 1.2
      );
      $scope.$applyAsync();
    };

    $scope.setThisLocation = function (saved_location, which) {
      console.log(saved_location);
      var target;
      switch (which) {
        case 'pickup':
          target = $scope.bookingObject.pickup;
          break;
        case 'drop':
          target = $scope.bookingObject.dropoff;
          break;
        case 'stop':
          $scope.addWaypoint(saved_location.geopoint);
          target =
            $scope.bookingObject.waypointMarkers[
              $scope.bookingObject.waypointMarkers.length - 1
            ];
          break;
      }
      console.log(target);
      target.saved = true;
      saved_location.marker.infobox.close();
      target.marker.setPosition(saved_location.geopoint);
      target.address = angular.copy(saved_location.address);
      target.contact = angular.copy(saved_location.contact);
    };

    function toggleAddressPanel() {
      if (!$('#collapseWhere').hasClass('in')) {
        $('.collapse.in').collapse('hide');
        $('#collapseWhere').collapse('show');
      }
    }

    $scope.deletePickup = function () {
      // Remove first waypoint location as pickup
      var wPoint = $scope.bookingObject.waypointMarkers.shift();
      console.log(wPoint);
      wPoint.marker.setMap(null);
      $scope.renameWaypoints(0);
      $scope.bookingObject.pickup.address = wPoint.address;
      $scope.bookingObject.pickup.marker.setPosition(
        wPoint.marker.getPosition()
      );
      //$scope.calcRoute();
      $scope.zoomOutMarkers();
    };

    $scope.deleteDropoff = function () {
      // Remove last waypoint location and set as dropoff
      var wPoint = $scope.bookingObject.waypointMarkers.pop();
      console.log(wPoint);
      wPoint.marker.setMap(null);
      $scope.bookingObject.dropoff.address = wPoint.address;
      $scope.bookingObject.dropoff.marker.setPosition(
        wPoint.marker.getPosition()
      );
      //$scope.calcRoute();
      $scope.zoomOutMarkers();
    };

    $scope.deleteWaypoint = function (index) {
      // Delete waypoint and rename them
      var wPoint = $scope.bookingObject.waypointMarkers.splice(index, 1)[0];
      $scope.firstTimeAddingWaypoints = false;
      wPoint.marker.setMap(null);
      $scope.renameWaypoints(index);
    };

    $scope.closeHelp = function () {
      generalService.setValueToLocalStorage(
        GENERAL_COACH_MARKS_SHOWN_COUNT,
        coach_marks_shown_count + 1
      );
      $scope.show.help = false;
    };

    $scope.hideHelperText = function () {
      $scope.displayWaypointHelp = false;
      $scope.show.showWaypointDragHelp = false;
      generalService.setValueToLocalStorage(REORDER_WAYPOINT_HELPER_FLAG, true);
    };

    $scope.renameWaypoints = function (start_index) {
      for (
        var i = start_index; i < $scope.bookingObject.waypointMarkers.length; i++
      ) {
        var waypoint_index = i + 1;
        $scope.bookingObject.waypointMarkers[i].title =
          'Stop ' + waypoint_index;
        $scope.bookingObject.waypointMarkers[
          i
        ].icon.url = mapHelpers.getWaypointIcon(waypoint_index);
      }
      $scope.$applyAsync();
      $scope.calcRoute();
    };

    function showConfirmation(message, cancelBtnText) {
      $scope.message = message;
      $scope.cancelBtnText = cancelBtnText;
      $scope.showConfirmation = true;
      $scope.$applyAsync();
    }

    $scope.askConfirmation = function () {
      showConfirmation(WAYPOINT_OPTIMIZE_CONFIRMATION, 'Cancel');
    };

    $scope.closeConfirmationDialog = function () {
      $scope.showConfirmation = false;
      $scope.$applyAsync();
    };

    $scope.addWaypoint = function (position) {
      var totalWaypoints = $scope.bookingObject.waypointMarkers.length;
      $scope.displayWaypointHelp =
        totalWaypoints === 1 && !$scope.show.showWaypointDragHelp;

      var stop_number = totalWaypoints + 1;
      var title = 'Stop ' + stop_number;
      var initial_position = null;
      if (position == null) {
        var legs = $scope.directionsDisplay.getDirections().routes[0].legs;
        var legs_count = $scope.directionsDisplay.getDirections().routes[0].legs
          .length;
        var last_leg = $scope.directionsDisplay.getDirections().routes[0].legs[
          legs_count - 1
        ];
        var midpoint_index = parseInt(last_leg.steps.length / 2);
        var initial_position = last_leg.steps[midpoint_index].start_location;
      } else {
        initial_position = position;
      }
      var wp = generalService.getLocationObject(
        title,
        mapHelpers.getWaypointIcon(stop_number),
        initial_position
      );
      $scope.bookingObject.waypointMarkers.push(wp);
      setTimeout($scope.calcRoute, 200);
    };

    function getUnpackedWaypoint(waypoint) {
      return {
        address: {
          line: waypoint.address.street_address,
          area: waypoint.address.area,
          postal_code: waypoint.address.postal_code
        },
        lat: waypoint.marker.getPosition().lat(),
        lon: waypoint.marker.getPosition().lng()
      };
    }

    function unpackWaypoints() {
      var waypointsData = [];
      for (var i = 0; i < $scope.bookingObject.waypointMarkers.length; i++) {
        waypointsData.push(
          getUnpackedWaypoint($scope.bookingObject.waypointMarkers[i])
        );
      }
      return waypointsData;
    }

    $scope.setMarkerPosition = function (marker, location_) {
      if (generalService.withinCityLimits(location_, $scope.polygons)) {
        marker.setPosition(location_);
        return true;
      }
      return false;
    };

    $scope.calcRoute = function () {
      if (
        !(
          $scope.bookingObject.pickup.marker &&
          $scope.bookingObject.dropoff.marker
        )
      ) {
        return;
      }
      var waypoints = $scope.bookingObject.waypointMarkers.map(function (val) {
        return {
          location: val.marker.getPosition()
        };
      });
      var departureTime = null;
      if ($scope.bookingObject.selected_datetime) {
        departureTime = new Date($scope.bookingObject.selected_datetime);
      }
      // refer: https://developers.google.com/maps/documentation/directions/intro#DirectionsRequests
      var request = {
        origin: $scope.bookingObject.pickup.marker.getPosition(),
        waypoints: waypoints,
        destination: $scope.bookingObject.dropoff.marker.getPosition(),
        travelMode: google.maps.TravelMode.DRIVING,
      };
      if (departureTime) {
        request['drivingOptions'] = {
          departureTime: departureTime,
        };
      }

      $scope.directionsService.route(request, function (result, status) {
        // console.error(result, status);
        if (status == google.maps.DirectionsStatus.OK) {
          $scope.total_distance_meters = result.routes[0].legs.reduce(function (
              a,
              b
            ) {
              return a + b.distance.value;
            },
            0);
          $scope.total_time_seconds = result.routes[0].legs.reduce(function (
              a,
              b
            ) {
              return a + b.duration.value + (b.duration_in_traffic ? b.duration_in_traffic.value : 0);
            },
            0);

          // $scope.estimateFare();
          $scope.directionsDisplay.setDirections(result);
        }
      });
    };

    function checkIfLatLng(address) {
      trimmed = address.trim();
      match = $scope.regex.latlng.exec(trimmed);
      if (match) {
        var lat = match[1],
          lon = match[3];
        latlng_object = new google.maps.LatLng(lat, lon);
        return latlng_object;
      }
      return null;
    }

    $scope.openSaveRoute = function () {
      $scope.saveRouteObject = jQuery.extend(true, {}, $scope.bookingObject);
      $scope.show.save_route = true;
      $scope.show.save_route_success = false;
      $scope.show.save_route_msg = false;
    };

    $scope.zoomOutMarkers = function () {
      $scope.outOfCoverage = false;
      $scope.isDisabled = false;
      $scope.alertMessage = '';
      latlngBounds = initLatLngBounds();
      $scope.bookingObject.waypointMarkers
        .concat([$scope.bookingObject.pickup, $scope.bookingObject.dropoff])
        .map(function (wp) {
          latlngBounds.extend(wp.marker.getPosition());
        });
      $scope.map.mapBooking.fitBounds(latlngBounds);
    };

    function zoomToMarker(marker, markerType) {
      $scope.map.mapBooking.panTo(marker.getPosition());
      $scope.map.mapBooking.setZoom(16);
    }

    var pageLoaded = false;
    var loadingMsg = 'Signing up. Please wait for a while...';
    var pollingId = {};

    $scope.getFareEstimation = function () {
      $scope.show.loadingFare = true;
      var loading_unloading_time = $scope.more_of_vehicle.time;
      var timeInMinutes = $scope.total_time_seconds ? $scope.total_time_seconds / 60 : 0;
      var data = {
        contract_id: $scope.bookingObject.selected_contract_id,
        item_category: $scope.bookingObject.selectedItems,
        distanceInKM: $scope.total_distance_meters ? $scope.total_distance_meters / 1000 : 0,
        timeInMins: timeInMinutes +
          (loading_unloading_time.loading_mins ?
            loading_unloading_time.loading_mins : 0) +
          (loading_unloading_time.unloading_mins ?
            loading_unloading_time.unloading_mins : 0),
        timeframe:{
          now_or_later: $scope.bookingObject.selectedTimeFrame,
          later_value: $scope.bookingObject.selected_datetime != null ?
            $scope.bookingObject.selected_datetime.format(
              'DD-MMM-YYYY HH:mm'
            ) : ''
        },
        coupon_code: $scope.show.coupon_success ?
          $scope.bookingObject.couponCode : '',
        is_pod_required: $scope.bookingObject.podRequired,
        labour_option: $scope.bookingObject.selectedLabour,
     };

      $scope.bookingObject.estimatedInitFare = 0;
      $scope.bookingObject.disableSubmit = true;
      blowhornService.getFareEstimation(data).then(function (data) {
        $scope.bookingObject.estimatedInitFare = data.total_payable;
        $scope.discount_value = data.discount;
        $scope.show.loadingFare = false;
        if ($scope.verifyingCoupon) {
            $scope.applyCoupon();
        }
        $scope.bookingObject.disableSubmit = false;
      }).catch(function (data) {
        $scope.show.loadingFare = false;
        console.error('Failed to calculate estimation.', data);
      });
    }

    $scope.placeBooking = function () {
      var later_value = null;
      if ($scope.bookingObject.selected_datetime) {
        later_value = $scope.bookingObject.selected_datetime.toISOString();
      }
      var data = {
        pickup: {
          name: $scope.bookingObject.pickup.name,
          address: $scope.bookingObject.pickup.address,
          contact: $scope.bookingObject.pickup.contact,
          geopoint: $scope.bookingObject.pickup.marker.getPosition().toJSON()
        },
        dropoff: {
          name: $scope.bookingObject.dropoff.name,
          address: $scope.bookingObject.dropoff.address,
          contact: $scope.bookingObject.dropoff.contact,
          geopoint: $scope.bookingObject.dropoff.marker.getPosition().toJSON()
        },
        waypoints: $scope.bookingObject.waypointMarkers.map(function (val) {
          return {
            sequence_id: val.title.split(SPLIT_CHAR)[1],
            name: val.name,
            address: val.address,
            contact: val.contact,
            geopoint: val.marker.getPosition().toJSON()
          };
        }),
        timeframe: {
          now_or_later: $scope.bookingObject.selectedTimeFrame,
          later_value: later_value
        },
        items: $scope.bookingObject.selectedItems,
        labour_option: $scope.bookingObject.selectedLabour,
        is_pod_required: $scope.bookingObject.podRequired,
        coupon_code: $scope.show.coupon_success ?
          $scope.bookingObject.couponCode : '',
        vehicle_class: $scope.bookingObject.selected_vehicle_class,
        customer_contract: $scope.bookingObject.selected_contract_id,
        estimated_distance: $scope.total_distance_meters,
        estimated_time: $scope.total_time_seconds / 60,
        estimated_cost_floor: $scope.bookingObject.estimatedInitFare + (parseInt($scope.bookingObject.tipForPartner) || 0.0),
        estimated_cost_ceil: $scope.bookingObject.estimatedExtendedFare,
        booking_type: $scope.bookingObject.selectedTimeFrame,
        device_type: deviceDetector.isMobile() ? 'Mob-Website' : 'Website',
        customer_name: $scope.bookingObject.guestName,
        payment_mode: $scope.bookingObject.payment_mode,
        tip_for_partner: $scope.bookingObject.tipForPartner && $scope.bookingObject.tipForPartner > 0 ? parseInt($scope.bookingObject.tipForPartner) : 0.0
      };

      if ($scope.isDefaultBuild && location.search) {
        var utm_data = convertUrlParametersToJson();
        data['utm_data'] = {
          utm_source: utm_data.utm_source,
          utm_medium: utm_data.utm_medium,
          utm_campaign: utm_data.utm_campaign,
          extra_data: utm_data
        };
      }
      console.log(data);

      $scope.showConfirmationScreen();
      var orderData = {
        action: 'create',
        data: JSON.stringify(data)
      };
      $scope.booking_error_msg = '';
      blowhornService
        .placeBooking(
          orderData,
          $scope.bookingObject.mobileNum,
          $scope.bookingObject.otpNum
        )
        .then(function (data) {
          console.log(data);
          switch (data.status) {
            case 'FAIL':
              switch (data.message.why) {
                case 'out-of-coverage-area':
                  break;
                case 'outside-working-hours':
                  break;
                default:
                  break;
              }
              break;
            case 'PASS':
              var booking_id = data.message.booking_id;
              $scope.tracking_url = data.message.tracking_url;
              $scope.friendly_id = data.message.friendly_id;
              if (data.message.booking_type === 'advanced') {
                $scope.exit_screen = 'done';
              } else {
                pollingId[booking_id] = setInterval(function () {
                  checkBookingStatus(booking_id, mobile, otp);
                }, 3000);
              }
              if ($scope.isDefaultBuild && google_tag_manager_key) {
                sendOrderPlacedEvent(
                  $scope.friendly_id,
                  $scope.bookingObject.estimatedInitFare
                );
              }
              break;
          }
        })
        .catch(function (data) {
          console.log(data);
          //registerOnClickTryAgain();
          $scope.exit_screen = 'fail';

          if (data.responseJSON) {
            data = data.responseJSON;
          } else {
            data = JSON.parse(data);
          }
          if (data.status === 'UNAUTHORIZED') {
            //console.log('unauthorized');
            $('#completed').modal('hide');
            $scope.openBookingForm();
            $scope.show.otp_invalid = true;
            if (data.message) {
              $scope.booking_error_msg =
                data.message && data.message.replace(/"/g, '');
            }
          } else {
            //console.log('authorized');
            $scope.exit_screen = 'fail';
            $scope.exitBottomScreen = 'exit-fail-msg';
            if (data.message) {
              $scope.booking_error_msg =
                data.message && data.message.replace(/"/g, '');
            }
          }
        });
    };

    function disableOtp() {
      $scope.show.Mobile = false;
      $scope.disableCoupon = false;
      $scope.disableBooking = false;
    }

    $scope.showConfirmationScreen = function () {
      $scope.bookingComplete = true;
      $scope.show.exit_screen = true;
      //$scope.closeBookingForm();
      $('#completed').modal('show');
      $scope.exit_screen = 'wait';
      $scope.exitBottomScreen = $scope.user_logged_in ?
        'exitmsg-thanks' :
        'exitmsg-signup';
      //$scope.$applyAsync();
    };

    $scope.instant_init = true;
    $scope.refreshBookingObject = function () {
      generalService.resetLocationObject($scope.bookingObject.pickup);
      generalService.resetLocationObject($scope.bookingObject.dropoff);
      $scope.bookingObject.waypointMarkers.map(function (wp) {
        wp.marker.setMap(null);
      });
      $scope.bookingObject.waypointMarkers = [];
      $scope.bookingObject.selectedTimeFrame = null;
      $scope.bookingObject.selectedLabour = null;
      $scope.bookingObject.selectedVehicleClass = null;
      $scope.bookingObject.selectedItems = [];
      $scope.bookingObject.payment_mode = PAYMENT_MODE_CASH;
      $scope.selected_time = null;
      $scope.instant_init = false;
      $scope.instant_init = true;

      window.location.reload();
    };

    var otpsendcount = 0;
    $scope.sendOtp = function () {
      $scope.show.Mobile = false;
      $scope.showOtp = true;
      var data = {
        mobile: $scope.bookingObject.mobileNum,
        name: $scope.bookingObject.guestName,
        alt: otpresendcount
      };
      blowhornService
        .sendOtp(data)
        .then(function (data) {
          otpsendcount = 0;
        })
        .catch(function (data) {
          if (otpsendcount <= 2) {
            $scope.sendOtp();
          }
          otpsendcount++;
        });
    };

    function checkBookingStatus(booking_id, mobile, otp) {
      var data = {
        booking_id: booking_id,
        mobile: mobile,
        otp: otp
      };
      blowhornService
        .checkBookingStatus(data)
        .then(function () {
          current_status = data.message.current_status;
          booking_type = data.message.booking_type;
          confirmed = data.message.confirmed;
          if (data.status === 'PASS') {
            if (
              current_status == 'driver_accepted' ||
              current_status == 'arrived_at_pickup' ||
              current_status == 'finished_loading' ||
              current_status == 'arrived_at_dropoff' ||
              current_status == 'completed' ||
              booking_type == 'advanced' ||
              confirmed == true
            ) {
              pageLoaded = true;
              clearInterval(pollingId[booking_id]);
              $scope.exit_screen = 'done';
            } else if (
              current_status == 'cancelled' ||
              current_status == 'other'
            ) {
              pageLoaded = true;
              clearInterval(pollingId[booking_id]);
              $scope.exit_screen = 'fail';
              $scope.exitBottomScreen = 'exit-fail-msg';
              //registerOnClickTryAgain();
            }
          } else {
            pageLoaded = true;
            clearInterval(pollingId[booking_id]);
            $scope.exit_screen = 'fail';
            $scope.exitBottomScreen = 'exit-fail-msg';
            //registerOnClickTryAgain();
          }
        })
        .catch(function (data) {
          pageLoaded = true;
          clearInterval(pollingId[booking_id]);
          $scope.exit_screen = 'fail';
          $scope.exitBottomScreen = 'exit-fail-msg';
          //registerOnClickTryAgain();
        });
    }

    var zebra_obj;
    var mtime;
    var otpresendcount = 0;
    var timeslot_json = {};
    var start_hours = 0;
    var end_hours = 23;
    var debounceflag = false;
    var debounceresend = false;
    var debouncetryagain = false;

    $scope.proceedToBookingScreen = function () {
      // if ($scope.userProfile && !$scope.userProfile.mobile) {
      //   $scope.userProfile.error = 'Please update your mobile number';
      //   $scope.showProfile();
      //   return;
      // }
      // if (
      //   $scope.userProfile &&
      //   !$scope.userProfile.mobile_status &&
      //   !$scope.userProfile.email_status
      // ) {
      //   $scope.resendActivationOtp();
      //   return;
      // }
      if (
        $scope.bookingObject.pickup.address.full_address != '' &&
        $scope.bookingObject.dropoff.address.full_address != '' &&
        $scope.bookingObject.pickup.address.is_from_google_place &&
        $scope.bookingObject.dropoff.address.is_from_google_place
      ) {
        $scope.openBookingForm();
      }
    };

    $scope.openLoginModal = function (e) {
      e.preventDefault();
      $('#login-message').html('');
      $('#login-modal').modal('show');
    };

    $scope.openRegistrationModal = function (e) {
      changeRegistrationPanelFlag('signup');
      $scope.otpSignupMessage = '';
      $scope.signupDetails.error = '';
      $('#register-modal').modal({
        backdrop: true,
        keyboard: false
      });
      $('#register-modal').modal('show');
      $('#login-modal').modal('hide');
    };

    // OFFER BAND
    $scope.bandId = 'discount-off';
    $scope.toggleBand = function (e, flag) {
      e.preventDefault();
      e.stopPropagation();
      $scope.bandId = flag === $scope.bandId ? 'discount-off' : flag;
      $scope.$applyAsync();
    };
    $(document).ready(function () {
      var keyDown = false,
        ctrl = 17,
        vKey = 86,
        Vkey = 118;
      $(document)
        .keydown(function (e) {
          if (e.keyCode == ctrl) {
            keyDown = true;
          }
        })
        .keyup(function (e) {
          if (e.keyCode == ctrl) {
            keyDown = false;
          }
        });

      $('#mobile_number')
        .on('keypress', function (e) {
          if (!e) {
            var e = window.event;
          }
          if (e.keyCode > 0 && e.which == 0) {
            return true;
          }
          if (e.keyCode) {
            code = e.keyCode;
          } else if (e.which) {
            code = e.which;
          }
          var character = String.fromCharCode(code);
          if (character == '\b' || character == ' ' || character == '\t') {
            return true;
          }
          if (keyDown && (code == vKey || code == Vkey)) {
            return character;
          } else {
            return /[0-9]$/.test(character);
          }
        })
        .on('focusout', function (e) {
          var $this = $(this);
          $this.val($this.val().replace(/[^0-9]/g, ''));
        })
        .on('paste', function (e) {
          var $this = $(this);
          setTimeout(function () {
            $this.val($this.val().replace(/[^0-9]/g, ''));
          }, 5);
        });
    });
    toastr.options.closeButton = true;
    toastr.options.preventDuplicates = true;
    toastr.options.positionClass = 'toast-top-right';
    $scope.sendAppLink = function (e) {
      e.preventDefault();
      var mobNumber = $scope.offerBanner.mobile;
      if (!mobNumber || mobNumber.length !== 10) {
        toastr.warning('Please, enter 10 digit number.', 'Warning');
        return;
      }
      var data = {
        mobile: mobNumber,
        off_val: $scope.offerBanner.offer_text,
        coupon_code: $scope.offerBanner.coupon_code
      };
      blowhornService
        .sendAppLink(data)
        .then(function (data) {
          toastr.success(
            'Blowhon App link sent to ' + mobNumber + '.',
            'SUCCESS'
          );
        })
        .catch(function (data) {
          toastr.error('Please try agian.', 'FAILED');
        });
    };

    /*Signup Validation*/
    $('.btn-signup').click(function () {
      $('.exit-bottom-wrap form').validate({
        errorPlacement: function (error, element) {},
        highlight: function (element) {
          $(element)
            .addClass('error-placeholder')
            .attr('placeholder', 'Enter valid email');
        },
        unhighlight: function (element) {
          $(element)
            .removeClass('error-placeholder')
            .removeAttr('placeholder');
        },
        rules: {
          signup: {
            required: true,
            email: true
          }
        }
      });
      if ($('.exit-bottom-wrap form').valid()) {
        $('#register-modal').modal('show');
        $('#register-signup')
          .show()
          .siblings('#register-home')
          .hide();
        $('#signup-email').val($('#signup').val());
        $('#signup-mobile').val($('#mobile').val());
      }
    });

    $scope.getTimeBox = function (day) {
      delta = day.diff($scope.today, 'days');
      console.log(day, $scope.today, delta);
      var now = new Date();
      var this_hour = now.getHours();
      if (now.getMinutes() > 59) {
        this_hour = this_hour + 1;
      }
      var coming_hour = this_hour + 1;
      var this_slots = timeslot_json[delta];
      var atleast_one = false;
      console.log(this_slots);
      $scope.timeslots = [];

      if (this_slots.length) {
        $.each(this_slots, function (index, value) {
          if (delta === 0 && value.t <= this_hour) {
            value.status = 'disabled';
            // $scope.timeslots.push(value);
          } else {
            if (value.status !== 'disabled' && value.status !== '') {
              atleast_one = true;
              $scope.timeslots.push(value.t);
            }
          }
          $scope.$applyAsync();
        });
      }
      if (atleast_one) {
        $scope.noSlots = false;
        $scope.showSlots = true;
        $scope.disabled = false;
      } else {
        $scope.noSlots = true;
        $scope.showSlots = false;
        $scope.disabled = true;
      }
    };

    // Calendar Functions
    function getAvailableEnd(today) {
      return today.clone().add($scope.max_advance_booking_days - 1, 'days');
    }

    function getToday() {
      return moment().hour(0).minute(0).second(0).millisecond(0);
    }

    function getShowStart(today) {
      return today.clone().subtract(1, 'days').startOf('week').add(1, 'days');
    }

    function getShowEnd(today) {
      return today.clone().add($scope.max_advance_booking_days, 'days').endOf('week');
    }

    $scope.$watch('bookingObject.selectedTimeFrame', function (timeframe) {
      console.log(timeframe);
      var now = new Date();
      $scope.isDisabled = false;
      $scope.selected_time = null;

      if (timeframe === 'now') {
        $scope.show.fareEstimationMessage = false;
        $scope.section.when_done = false;
        $scope.nowNotAvailable = true;
        $scope.noSlots = false;
        $scope.isDisabled = true;
        $scope.showSlots = false;
        var this_hour_orig = now.getHours();
        var this_hour = this_hour_orig;
        if (now.getMinutes() > 59) {
          this_hour = (this_hour + 1) % 24;
        }
        var coming_hour = (this_hour + 1) % 24;
        var today_slots = timeslot_json[0];
        if (today_slots) {
          if (this_hour === 23 || this_hour_orig === 23) {
            // For post 10:30 PM load tomorrow's slots
            today_slots = timeslot_json[1];
          }
          console.log(coming_hour, today_slots);
          today_slots.map(function (value) {
            if (value.t === coming_hour && value.status === 'enabled') {
              $scope.nowNotAvailable = false;
              $scope.section.when_done = true;
              $scope.isDisabled = false;
              return false;
            }
          });
        }
      } else if (timeframe === 'later') {
        $scope.nowNotAvailable = false;
        $scope.showSlots = true;
        $scope.selected_time = null;
        $scope.today = getToday();
        $scope.availability_end = getAvailableEnd($scope.today);
        var show_start = getShowStart($scope.today);
        var show_end = getShowEnd($scope.today);
        $scope.days_array = {};
        console.log('Dates', $scope.today, $scope.availability_end, show_start, show_end);
        for (var i = show_start; i < show_end; i = i.clone().add(1, 'days')) {
          var month = i.format('MMMM');
          if ($scope.days_array.hasOwnProperty(month)) {
            $scope.days_array[month].push(i);
          } else {
            if (show_start.format('MMMM') === month) {
              $scope.days_array[month] = [i];
            } else {
              var prevDays = [];
              var prevDay = i.clone().subtract(1, 'days')
              var prevDayStart = prevDay.startOf('week');
              for (var j = prevDayStart; j < i; j = j.clone().add(1, 'days')) {
                prevDays.push(-1);
              }
              prevDays.shift();
              prevDays.push(i);
              $scope.days_array[month] = prevDays;
            }
          }
        }
        if ($scope.selected_day && !$scope.timeslots.length) {
          $scope.noSlots = true;
          $scope.showSlots = false;
          $scope.disabled = true;
        } else {
          $scope.noSlots = false;
          $scope.showSlots = true;
          $scope.disabled = false;
        }
      }
    });

    $scope.changeCouponFlags = function (show_coupon_success, message) {
      console.log('coupon resp', message);
      $scope.discount_message = show_coupon_success ? message : {};
      $scope.show.coupon_success = show_coupon_success;
      $scope.disableCoupon = false;
      $scope.show.CouponInvalid = !show_coupon_success;
      $scope.invalidCoupon = !show_coupon_success ? message : '';
      if (!$scope.show.coupon_success) {
        $scope.bookingObject.couponCode = '';
      }
      //$scope.confirmBookingForm.coupon.$setValidity('valid', show_coupon_success);
      $scope.verifyingCoupon = false;
      $scope.$applyAsync();
    };

    function changeOtpFlags(valid) {
      $scope.disableBooking = valid;
      $scope.showOtpSuccess = !valid;
      $scope.showOtp = valid;
      $scope.show.Mobile = valid;
      $scope.show.otp_invalid = valid;
      $scope.disableCoupon = valid;
      $scope.verifyingOtp = false;
      $scope.confirmBookingForm.otp.$setValidity('valid', valid);
      if (!valid) {
        $scope.showOtp = '';
      }
      $scope.$applyAsync();
    }

    function verifyOtpBooking() {
      var data = {
        mobile: $scope.bookingObject.mobileNum,
        otp: $scope.bookingObject.otpNum
      };
      $scope.verifyingOtp = true;
      blowhornService
        .verifyOtpBooking(data)
        .then(function (data) {
          changeOtpFlags(false);
        })
        .catch(function (data) {
          changeOtpFlags(true);
        });
    }

    /*Time Picker*/
    function timePicker() {
      $('#table-time td:not(.disabled)').on('click', function () {
        $('#table-time td').removeClass('selected');
        $(this).addClass('selected');
        $('#table-day').removeClass('error-placeholder');
      });
    }

    /*Modal Exit Screen*/
    $scope.tryAgain = function (e) {
      event.preventDefault();
      if (!debouncetryagain) {
        debouncetryagain = true;
        $scope.placeBooking();
        $scope.exit_screen = 'wait';
        setTimeout(function () {
          debouncetryagain = false;
        }, 3000);
      }
    };

    $scope.changeBookingDateTime = function (e) {
      e.preventDefault();
      $('#completed').modal('hide');
      $timeout(function () {
        $scope.openBookingForm();
        $('#collapseWhen').collapse('toggle');
      }, 200);
    };

    /* Registration block */
    $scope.openAuthPage = function (url, operation, provider) {
      setTimeout(openPage, 1000);
      var isPageOpened = false;
      function openPage() {
        if (!isPageOpened) {
          isPageOpened = true;
          window.open(url, '_self');
        }
      }
      gtag('event', operation, {
        'method': provider,
        'event_callback': openPage
      });
    }
    function validGstin(gstin, obj) {
      if (!$scope.regex.gstin.test(gstin)) {
        obj.error = 'Invalid GSTIN';
        $scope.$applyAsync();
        return false;
      }
      return true;
    }

    function changeRegistrationPanelFlag(flag) {
      $scope.enableResend = false;
      $scope.registerPanelName = flag;
      $scope.$applyAsync();
    }

    $scope.showRegistrationForm = function () {
      changeRegistrationPanelFlag('signup');
    };

    $scope.proceedRegistration = function (e) {
      e.preventDefault();
      var $signup_form = $('#signup-form');
      if (
        $scope.signupDetails.is_business_user &&
        !$scope.signupDetails.legal_name
      ) {
        $scope.signupDetails.error = 'Company name is required';
        return;
      }
      if (!$scope.isDefaultBuild && $scope.signupDetails.gstin && !$scope.signupDetails.state) {
        $scope.signupDetails.error = 'State/Province is required';
        return;
      }

      if ($scope.primaryLoginFieldType === 'email'){
          $signup_form.validate({
            errorPlacement: function (error, element) {},
            rules: {
              name: {
                required: false
              },
              password: {
                required: true
              },
              repassword: {
                required: true,
                equalTo: '#signup-password'
              },
              mobile: {
                required: false,
                digits: true,
              },
              checkbox: {
                required: true
              }
            }
          });
      }
      else {
          $signup_form.validate({
            errorPlacement: function (error, element) {},
            rules: {
              name: {
                required: false
              },
              password: {
                required: false
              },
              repassword: {
                required: false,
                equalTo: '#signup-password'
              },
              checkbox: {
                required: true
              }
            }
          });

      }

      if ($signup_form.valid()) {
        if ($scope.primaryLoginField === 'email' && !$scope.regex.email.test($('#primary-signup-field').val())) {
          $scope.signupDetails.error = 'Invalid email id';
          return;
        }
        if ($scope.primaryLoginFieldType === 'email'&& !$scope.signupDetails.password){
          $scope.signupDetails.error = 'Please enter password';
          return;
        }

        if (!$scope.primaryLoginFieldType){
          $scope.signupDetails.error = 'Please enter Email / Phone number';
          return;
        }
        signupUser();
      }
    };

    function signupUser() {
      var data = {
        name: $scope.signupDetails.name,
        password1: $scope.signupDetails.password,
        password2: $scope.signupDetails.password,
        is_business_user: $scope.signupDetails.is_business_user,
        gstin: $scope.signupDetails.gstin,
        state: $scope.signupDetails.state,
        legal_name: $scope.signupDetails.legal_name
      };

      if ($scope.primaryLoginFieldType === 'email'){
        data.email = $('#primary-signup-field').val();
      }
      else {
        data.phone_number = $('#primary-signup-field').val();
      }

      if (!data.name){
        data.name = 'Guest';
      }

      if (!data.phone_number){
        data.phone_number = null;
      }

      if (!data.email){
        data.email = data.phone_number + '@customer.' + $scope.current_build + '.net';
      }

      if (!data.password1){
        data.password1 = '1234';
        data.password2 = '1234';
      }

      blowhornService
        .signupUser(data)
        .then(function (data) {
          // Automatically user will be logged-in on signup (all-auth)
          $scope.b_user_logged_in = true;
          if ($scope.primaryLoginFieldType === 'email'){
            $('#register-modal').modal('hide');
          }
          else {
            $scope.otp = {
                mobile: $('#primary-signup-field').val()
            };
          }
          // USING ALL_AUTH SO NOT SUPPORTED, NEED TO CALL EXPLICITLY
          getUserProfile();
          getVehicleClassesAndRatePackages();
          $timeout(function () {
            $scope.enableResend = true;
          }, 60000);
          changeRegistrationPanelFlag('otp');
          sendEvent('sign_up', 'custom');
        })
        .catch(function (data) {
          $scope.signupDetails.error = getErrorMessage(data);
        });
    }

    $scope.otpErrors = {};
    $scope.verifySignupOtp = function (e) {
      e.preventDefault();
      var $otp_form = $('#otp-form');
      $otp_form.validate({
        errorPlacement: function (error, element) {},
        rules: {
          email: {
            required: true
          },
          mobile: {
            required: true,
            digits: true,
            minlength: 10
          },
          otp: {
            required: true,
            digits: true
          }
        }
      });
      if ($otp_form.valid()) {
        var data = $('#otp-form').serialize();
        blowhornService
          .verifySignupOtp(data)
          .then(function (data) {
            console.log(data);
            if (!$scope.mobileVerificationRequired) {
              $scope.user_logged_in = true;
              $scope.b_user_logged_in = true;
              $scope.show.save_location_option = true;
              getUserProfile();
              getVehicleClassesAndRatePackages();
              if (!$scope.is_login_screen){
                showMessagePopup(otp_success_message, $scope.modal.registration);
              }
              $('#register-modal').modal('hide');
            } else {
              $('#otp-modal').modal('hide');
              $scope.placeBooking();
            }
          })
          .catch(function (data) {
            console.error(data)
            var response = data.responseJSON;
            if (response) {
              $scope.otpErrors.error = typeof(response) === 'string' ? response : response.message;
            }
          });
      }
    };

    /* Open login modal from registration */
    $scope.redirectToSignin = function (e) {
      e.preventDefault();
      $('#register-modal').modal('hide');
      $('#login-modal').modal('show');
    };

    /*Arrow Scroll*/
    $(document).on('click', '.anchor-scroll', function (event) {
      event.preventDefault();
      var target = $(this).attr('href');
      $('html, body').animate({
          scrollTop: $(target).offset().top
        },
        1000
      );
    });

    /* Equal height columns */
    function equalHeight(group) {
      var tallest = 0;
      group.each(function () {
        thisHeight = $(this).height();
        if (thisHeight > tallest) {
          tallest = thisHeight;
        }
      });
      group.height(tallest);
    }
    equalHeight($('.feedback-wrap'));

    /* Section subscribe */
    var $mailinglist = $('#mailing-list');
    $mailinglist.validate({
      errorPlacement: function (error, element) {},
      highlight: function (element) {
        $('#subscribe-btn').addClass('disabled');
      },
      unhighlight: function (element) {
        $('#subscribe-btn').removeClass('disabled');
      },
      rules: {
        email: {
          required: true
        }
      }
    });

    var $subscribeemail = $('#subscribe');
    $subscribeemail.on('keyup', function (e) {
      if ($mailinglist.valid()) {
        $('#subscribe-btn').removeClass('disabled');
      } else {
        $('#subscribe-btn').addClass('disabled');
      }
    });

    $('#subscribe-btn').click(function (e) {
      e.preventDefault();

      function toggleMessage() {
        $('#subscribe').val('');
        $('#subscribe-done').fadeIn(function () {
          setTimeout(function () {
            $('#subscribe-done').fadeOut();
          }, 3000);
        });
      }
      if (!$('#subscribe-btn').hasClass('disabled') && $mailinglist.valid()) {
        //initiate AJAX
        window.location = 'track/' + $('#subscribe').val();
      }
    });

    /* Section Track */
    $scope.trackBooking = function (e) {
      console.log('tracking booking');
      e.preventDefault();
      console.log($scope.track.booking_id);
      if ($scope.track.booking_id) {
        window.location = '/track/' + $scope.track.booking_id;
      }
    };

    $scope.trackParcel = function (e) {
      console.log('tracking parcel');
      e.preventDefault();
      if ($scope.parcelTrack.ref_number) {
        window.location =
          '/track-parcel/' + $scope.parcelTrack.ref_number + '/shipment';
      }
    };

    $scope.checkKey = function (e) {
      if (e.which === 13) {
        $scope.trackBooking(e);
      }
    };

    $scope.bookingOtpKeypress = function (e) {
      if (e.which === 13) {
        e.preventDefault();
        $scope.show.booking_otp = false;
        $scope.verifyOtpAndBook();
      }
    };

    var registration_success_message =
      'An account activation link has been sent to your email. Please check your inbox ';
    var otp_success_message =
      'You have successfully verified your mobile number';

    function showMessagePopup(message, id) {
      $('#' + id).modal('hide');
      $('#message-modal').modal('show');
      $('#message')
        .show()
        .html(message);
    }

    $('#btn-social-login').on('click', function (e) {
      e.preventDefault();
      var $social_form = $('#social-form');

      $social_form.validate({
        errorPlacement: function (error, element) {},
        rules: {
          name: {
            required: true
          },
          email: {
            required: true
          },
          mobile: {
            required: true,
            digits: true,
            minlength: 10
          }
        }
      });
      if ($social_form.valid()) {
        $.ajax({
          url: 'signup',
          type: 'POST',
          data: $social_form.serialize(),
          dataType: 'json',
          success: function (data, textStatus, jqXHR) {
            $('#social-modal').modal('hide');
            $('#otp-email').val($('#social-email').val());
            $('#otp-mobile').val($('#social-mobile').val());
            $('#register-modal').modal('show');
            $('#register-otp')
              .show()
              .siblings('#register-home, #register-signup')
              .hide();
          },
          error: function (jqXHR, textStatus, exception) {
            $('#social-mobile').addClass('ipt-error');
            $('.error-box')
              .slideDown('slow')
              .removeClass('green')
              .addClass('red');
            $('.error-message').text('Email already exists!');
            console.log(exception);
          }
        });
      }
    });

    $scope.resetPassword = function (e) {
      //e.preventDefault();
      var $password_recover_form = $('#password-recover-form');
      $password_recover_form.validate({
        errorPlacement: function (error, element) {},
        rules: {
          email: {
            required: true
          }
        }
      });
      if ($password_recover_form.valid()) {
        var data = {
          email: $('#recovery-email').val()
        };
        blowhornService
          .sendPasswordResetLink(data)
          .then(function (data) {
            showMessagePopup(
              PASSWORD_RESET_LINK_SUCCESS,
              $scope.modal.passwordRecovery
            );
          })
          .catch(function (data) {
            $scope.passwordRecoveryMessage = getErrorMessage(data);
          });
      }
    };

    $scope.resendActivationOtp = function () {
      $scope.otp.enableResend = false;
      $scope.otp.activationNeeded = false;
      $scope.otp.activation = true;
      if ($scope.b_user_logged_in) {
        $scope.otp = {
          email: $scope.userProfile.email,
          mobile: $scope.userProfile.mobile.toString()
        };
      }
      $scope.$applyAsync();
      changeRegistrationPanelFlag('otp');
      $('#otp-modal').modal({
        backdrop: 'static',
        keyboard: false
      });
      // $('#otp-modal').modal('show');
      blowhornService
        .sendOtp($scope.otp)
        .then(function () {
          console.log('Message has been sent');
        })
        .catch(function (data) {
          console.log(data);
        });
    };

    $scope.resendActivationEmail = function () {
      // @todo: need to write new api
      return;
      $scope.otp.activationNeeded = false;
      blowhornService
        .sendActivationLink($scope.otp)
        .then(function () {
          $scope.otp.activationOtpSent = true;
        })
        .catch(function (data) {
          console.log(data);
        });
    };

    function onLoginSuccess(data) {
      console.log(data);
      $('#login-modal').modal('hide');
      $scope.user_logged_in = true;
      $scope.b_user_logged_in = true;
      $scope.show.save_location_option = true;
      // USING ALL_AUTH SO NOT SUPPORTED, NEED TO CALL EXPLICITLY
      if (data.location !== '/') {
        window.location.href = 'admin';
        return;
      }
      getUserProfile();
      getVehicleClassesAndRatePackages();
      disableOtp();
      sendEvent('login', 'custom');
    }

    function onLoginFailure(data) {
      try {
        var response = jQuery.parseJSON(data.responseText);
        message = response.message.text;
      } catch (err) {
        message = getErrorMessage(data);
      }
      $('#login-message').html(message).show();
    }

    function showLoginVerifyOtp(data) {
       $scope.otp = {
            mobile: $('#primary-login-field').val()
          };
       $scope.is_login_screen = true;
       $('#login-modal').modal('hide');
       $('#register-modal').modal('show');
       changeRegistrationPanelFlag('otp');
    }

    function signIn() {

      if ($scope.primaryLoginFieldType === 'email') {
          var data = {
            login: $('#primary-login-field').val(),
            password: $('#password').val(),
            remember: false
            };
        blowhornService
          .signIn(data)
          .then(onLoginSuccess)
          .catch(onLoginFailure);
      } else {
        var data = {
            mobile: $('#primary-login-field').val(),
            };
        blowhornService
          .sendOtpForLogin(data)
          .then(showLoginVerifyOtp)
          .catch(onLoginFailure);
      }
    }

    $scope.signIn = function (e) {
      e.preventDefault();
      var $login_form = $('#login-form');
      $login_form.validate({
        errorPlacement: function (error, element) {},
        rules: {
          email: {
            required: true
          },
          password: {
            required: true
          }
        }
      });
      if ($login_form.valid()) {
        signIn();
      }
    };

    $scope.resetLoginErrorMessage = function () {
      $('#login-message').hide();
    };

    $scope.primaryLoginFieldType = 'email';
    $scope.primaryLoginField = null;
    $scope.checkInputType = function () {
      $('#login-message').hide();
      $scope.primaryLoginFieldType = $scope.regex.integer.test($('#primary-login-field').val()) ? 'phone' : 'email';
      $scope.submitBtnText = $scope.primaryLoginFieldType === 'email' ? 'LOGIN' : 'SEND OTP';
      $scope.$applyAsync()
    }

    $scope.isBusinessUser = function (obj) {
      if (!obj.is_business_user && !obj.gstin) {
        obj.error = '';
        obj.gstin = '';
        obj.legal_name = '';
      }
    };

    /* Changing password */
    $scope.setPasswordErrorMessage = function (message) {
      $scope.pwdErrorMessage = message;
      $scope.$applyAsync();
    };

    $scope.changePassword = function (e) {
      $scope.passwordDetails = {
        oldPassword: '',
        password: '',
        repassword: ''
      };
      $scope.pwdErrorMessage = '';
      e.preventDefault();
      $('#profile-modal').modal('hide');
      $('#modal-password').modal({
        backdrop: 'static',
        keyboard: false
      });
      $('#modal-password').modal('show');
      $scope.setPasswordErrorMessage('');
    };

    $scope.saveNewPassword = function (e) {
      e.preventDefault();
      var data = {
        oldpassword: $scope.passwordDetails.oldPassword,
        password1: $scope.passwordDetails.password,
        password2: $scope.passwordDetails.repassword
      };
      if (!data.oldpassword) {
        $scope.setPasswordErrorMessage('Please, fill provide the old password');
      }
      if (!data.password1) {
        $scope.setPasswordErrorMessage('Please, fill the password');
        return;
      }
      if (data.password1 !== data.password2) {
        $scope.setPasswordErrorMessage('Passwords do not match');
        return;
      }
      blowhornService
        .saveNewPassword(data)
        .then(function (data) {
          $('#modal-password').modal('hide');
          showMessagePopup(
            'Your password has been changed',
            $scope.modal.changePassword
          );
        })
        .catch(function (data) {
          $scope.setPasswordErrorMessage(getErrorMessage(data));
        });
    };

    function openMyFleet() {
      setTimeout(function () {
        window.location.href = 'myfleet';
      }, 100);
    }

    function showSignUpModal() {
      $('#modal-signup').modal({
        backdrop: 'static',
        keyboard: false
      });
      $('#modal-signup').modal('show');
      $('#modal-login').modal('hide');
      $('.error-box')
        .removeClass('red')
        .css('display', 'none');
      return false;
    }

    function showSignUpModalPostBooking() {
      $('#ipt-email').val($('#signup').val());
      $('#ipt-mobile').val($('#mobile').val());
      $('#modal-signup').modal({
        backdrop: 'static',
        keyboard: false
      });
      $('#modal-signup').modal('show');
      $('.error-box')
        .removeClass('red')
        .css('display', 'none');
      return false;
    }

    $scope.showProfile = function () {
      $('#profile-modal').modal('show');
      $scope.profile = angular.copy($scope.userProfile);
      $scope.$applyAsync();
    };

    // Update user profile
    $scope.disable_category = false;
    $scope.updateProfile = function (e) {
      e.preventDefault();
      var $profile_form = $('#profile-form');
      if ($scope.profile.is_business_user && !$scope.profile.legal_name) {
        $scope.profile.error = 'Company name is required';
        return;
      }
      if (!$scope.isDefaultBuild && $scope.profile.gstin && !$scope.profile.state) {
        $scope.profile.error = 'State/Province is required';
        return;
      }
      $profile_form.validate({
        errorPlacement: function (error, element) {},
        rules: {
          name: {
            required: true
          },
          email: {
            required: true,
            email: true
          },
          mobile: {
            required: true,
            digits: true,
            minlength: 10
          }
        }
      });
      if ($profile_form.valid()) {
        var data = {
          email: $scope.profile.email,
          name: $scope.profile.name,
          mobile: $scope.profile.mobile,
          is_business_user: $scope.profile.is_business_user,
          gstin: $scope.profile.gstin,
          state: $scope.profile.state,
          legal_name: $scope.profile.legal_name
        };

        blowhornService
          .updateProfile(data)
          .then(function (data) {
            if (data.status === 'PASS') {
              $scope.userProfile.mobile = $scope.profile.mobile;
              $scope.userProfile.name = $scope.profile.name;
              $scope.userProfile.gstin = $scope.profile.gstin;
              $scope.userProfile.legal_name = $scope.profile.legal_name;
              $scope.userProfile.state = $scope.profile.state;
              $scope.userProfile.is_business_user =
                $scope.profile.is_business_user;
              $scope.disable_category = $scope.canEditCustomerCategory();
              if (data.message === 'verify-mobile') {
                $('#otp-email').val($scope.profile.email);
                $('#otp-mobile').val($scope.profile.mobile);
                $scope.enableResend = false;
                $timeout(function () {
                  $scope.enableResend = true;
                }, 60000);
                changeRegistrationPanelFlag('otp');
                $('#register-modal').modal('show');
                $('#profile-modal').modal('hide');
                $('#register-otp')
                  .show()
                  .siblings('#register-home, #register-signup')
                  .hide();
              } else {
                showMessagePopup(
                  'Your profile has been updated.',
                  $scope.modal.profile
                );
              }
            }
          }).catch(function (data) {
            console.log('Failed', data);
            $scope.profile.error = data.message;
          });
      }
    };

    function convertUrlParametersToJson() {
      return JSON.parse('{"' + decodeURI(location.search.split('?')[1].replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}');
    }

    function getParameterByName(name) {
      name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
      var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
      return results == null ?
        false :
        decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    function resendActivationEmail(email) {
      message = 'Your activation email has been re-sent.';
      showMessagePopup(message);
      $.ajax({
        url: 'emailrecaptcha',
        type: 'POST',
        data: {
          email: email
        },
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {},
        error: function (jqXHR, textStatus, exception) {}
      });
    }

    $('#form-captcha-email').submit(function () {
      var data = $('#form-captcha-email').serializeArray();
      var email = $('#email').val();
      var emailObject = {
        name: 'email',
        value: email
      };

      data.push(emailObject);
      loadingMsg = 'Sending activation link to your email... Please wait!';

      $.ajax({
        url: 'emailrecaptcha',
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
          if (data.status == 'PASS') {
            $('.error-box')
              .slideDown('slow')
              .removeClass('red')
              .addClass('green')
              .find('.error-message')
              .html(data.message);
          } else {
            $('.error-box')
              .slideDown('slow')
              .removeClass('green')
              .addClass('red')
              .find('.error-message')
              .html(data.message);
          }
        },
        error: function (jqXHR, textStatus, exception) {
          $('.error-box')
            .slideDown('slow')
            .removeClass('green')
            .addClass('add')
            .find('.error-message')
            .html('Something went wrong!... Please try after sometime');
        }
      });

      Recaptcha.reload();

      return false;
    });

    function onSignIn(googleUser) {
      // Handle successful sign-in
      googleLoginCallback(googleUser.getAuthResponse().id_token);
    }

    function onSignInFailure() {
      // Handle sign-in errors
      console.log('login error');
    }

    $scope.googleLogin = function () {
      console.log('googlelogin');
      gapi.load('auth2', function () {
        gapi.auth2.init();
        var GoogleAuth = gapi.auth2.getAuthInstance();
        if (GoogleAuth.isSignedIn.get()) {
          console.log('User already Signed in');
        } else {
          GoogleAuth.signIn().then(function () {
            console.log('Signed in');
            onSignIn(GoogleAuth.currentUser.get());
          }, onSignInFailure);
        }
      });
    };

    function getVerificationMessage(email) {
      var message =
        "Please activate your Blowhorn account.<br> <a href='#' style='font-weight: 700' onclick='return resendActivationEmail(\"" +
        email +
        '");\'><u>Click here </u></a> to resend Activation email.';
      return message;
    }

    // executes after successful login
    function googleLoginCallback(accessToken) {
      loadingMsg = 'Getting your info...please wait!';
      var data = {
        access_token: accessToken
      };
      blowhornService
        .googleLoginAuthenticate(data)
        .then(function (data) {
          processLoginResponse(data, GOOGLE_LOGIN);
        })
        .catch(function (data) {
          console.log('Failed to authenticate.');
        });
    }

    function authenticateSocialProfile(name, email, mobile, social_type) {
      loadingMsg = 'Signing Up...please wait!';
    }

    function processLoginResponse(data, socialType, fb_id) {
      switch (data.status) {
        case 'FAIL':
          {
            switch (data.message.why) {
              case 'not_registered':
                {
                  //console.log('not registered');
                  showSocialLoginPopup(
                    data.message.name,
                    data.message.email,
                    socialType
                  );
                  break;
                }
              case 'verification_pending':
                {
                  //console.log('verification pending');
                  var message = getVerificationMessage(email);
                  showMessagePopup(message);
                  break;
                }
              case 'account_unavailable':
                {
                  //console.log('verification pending');
                  var message =
                    'Unable to fetch your Account details. Check privacy settings!';
                  showMessagePopup(message);
                  break;
                }
            }
            break;
          }
        default:
          if (data.message.mobile_status === 'verification_pending') {
            $scope.userProfile = data.message;
            $scope.showProfile();
          }
          $scope.userProfile = data.message;
          $('#login-modal').modal('hide');
          $('#register-modal').modal('hide');
          $scope.user_logged_in = true;
          $scope.b_user_logged_in = true;
          $scope.show.save_location_option = true;
          $scope.$applyAsync();
          disableOtp();
          if (socialType == FACEBOOK_LOGIN) {
            var fb_pic =
              'http://graph.facebook.com/' + fb_id + '/picture?type=normal';
            $('#user-pic').attr('src', fb_pic);
          }
          if (data.message.b_myfleet_required) {
            $scope.b_myfleet_required = data.message.b_myfleet_required;
            openMyFleet();
          }
          /*$('#messenger-row').show();
                if (data.message.b_messenger_subscribed) {
                    $('#messenger').hide();
                }
                else {
                    $('#messenger').show();
                }*/
          break;
      }
    }

    function continueFBLoginProcess(token, fb_id) {
      var data = {
        access_token: token,
        email: '',
        name: ''
      };
      blowhornService
        .fbLoginAuthenticate(data)
        .then(function (data) {
          processLoginResponse(data, FACEBOOK_LOGIN, fb_id);
        })
        .catch(function (data) {
          console.log(data.responseText);
        });
    }

    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
      //console.log('statusChangeCallback');
      console.log(response);
      // The response object is returned with a status field that lets the
      // app know the current login status of the person.
      // Full docs on the response object can be found in the documentation
      // for FB.getLoginStatus().
      if (response.status === 'connected') {
        // Logged into your app and Facebook.
        continueFBLoginProcess(
          response.authResponse.accessToken,
          response.authResponse.userID
        );
        $('#register-modal').hide();
        $('#login-modal').hide();
      } else if (response.status === 'not_authorized') {
        message = 'Your Facebook Account is not authorized for this!';
        showMessagePopup(message);
      }
    }

    $scope.clear_contact = function () {
      contact_us = {
        disabled: true,
        message_sent: true,
        name: '',
        email: '',
        phone: '',
        message: ''
      };
    };

    $scope.facebookLogin = function () {
      var facebook_app_id = $("meta[name='facebook-app-id']").attr('content');
      FB.init({
        appId: facebook_app_id,
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true, // parse social plugins on this page
        version: 'v2.6'
      });
      FB.login(
        function (response) {
          statusChangeCallback(response);
        }, {
          scope: 'public_profile,email'
        }
      );
    };

    function getTimeSlots() {
      console.log($scope.bookingObject.dropoff);
      var data = {
        'from-lat': $scope.bookingObject.pickup.position.lat(),
        'from-lon': $scope.bookingObject.pickup.position.lng()
      };
      $scope.minutes = {
        start: 0,
        end: 59
      };
      blowhornService
        .getTimeSlots(data)
        .then(function (data) {
          timeslot_json = data.slots;
          $scope.max_advance_booking_days = data.max_advance_booking_days;
          console.log(timeslot_json);
          $scope.minutes.start = data.start_minute;
          $scope.minutes.end = data.end_minute;
        })
        .catch(function (data) {
          console.log('Failed to load slots');
        });
    }

    $scope.getRateText = function (vc) {
      var selected_rate_package = vc.packages.filter(
        item => item.contract_id === vc.selected_contract_id
      );
      if (selected_rate_package.length) {
        return selected_rate_package[0].description.base.text;
      } else {
        return vc.packages[0].description.base.text;
      }
    };

    $scope.showSection = function (section) {
      $scope.current_section = section;
      $scope.show.where_section = false;
      $scope.show.when_section = false;
      $scope.show.what_section = false;
      $scope.show.vehicle_section = false;
      $scope.show.labour_coupon_section = false;
      $scope.zoomOutMarkers();
      switch (section) {
        case 'vehicle':
          $scope.show.vehicle_section = true;
          if ($scope.bookingObject.selected_vehicle_class == null) {
            $scope.bookingObject.selected_vehicle_class =
              $scope.vehicle_classes[0].classification;
            $scope.bookingObject.selected_contract_id =
              $scope.vehicle_classes[0].packages[0].contract_id;
            $scope.selected_rate_text =
              $scope.vehicle_classes[0].packages[0].description.base.text;
            $scope.more_of_vehicle = $scope.vehicle_classes[0];
            //$scope.show.vehicle_info = true;
          }
          $scope.next_section = 'where';
          $scope.prev_section = 'home';
          break;
        case 'where':
          $scope.show.where_section = true;
          $scope.next_section = 'when';
          $scope.prev_section = 'vehicle';
          break;
        case 'when':
          $scope.show.when_section = true;
          $scope.next_section = 'what';
          $scope.prev_section = 'where';
          if ($scope.timeFrame === null) {
            $scope.timeFrame = 'now';
          }
          break;
        case 'what':
          if ($scope.b_user_logged_in) {
            $scope.getCouponData();
          }
          $scope.show.what_section = true;
          // $scope.getFareEstimation();
          //$scope.next_section = 'vehicle';
          $scope.next_section = 'preview';
          $scope.prev_section = 'when';
          break;
          // case "labour/coupon":
          //     $scope.show.labour_coupon_section = true;
          //     $scope.next_section = 'preview';
          //     break;
        case 'preview':
          // $scope.show.labour_coupon_section = false;
          $scope.getFareEstimation();
          $scope.show.what_section = true;
          $scope.show.booking_summary = true;
          $scope.isMobile = $(window).width() <= 375;
          if ($scope.bookingObject && !$scope.bookingObject.payment_mode) {
            $scope.bookingObject.payment_mode = PAYMENT_MODE_CASH;
          }
          //$scope.next_section = null;
          break;
      }
    };

    $scope.goBackInBookingFormMobile = function (section) {
      $scope.current_section = section;
      $scope.show.where_section = false;
      $scope.show.when_section = false;
      $scope.show.what_section = false;
      $scope.show.vehicle_section = false;
      $scope.show.labour_coupon_section = false;
      switch (section) {
        case 'home':
          $scope.bookingFormLogoClicked();
          break;

        case 'vehicle':
          $scope.show.vehicle_section = true;
          if ($scope.bookingObject.selected_vehicle_class == null) {
            $scope.bookingObject.selected_vehicle_class =
              $scope.vehicle_classes[0].classification;
            $scope.bookingObject.selected_contract_id =
              $scope.vehicle_classes[0].packages[0].contract_id;
            $scope.selected_rate_text =
              $scope.vehicle_classes[0].packages[0].description.base.text;
            $scope.more_of_vehicle = $scope.vehicle_classes[0];
          }
          $scope.next_section = 'where';
          $scope.prev_section = 'home';
          break;

        case 'where':
          $scope.show.where_section = true;
          $scope.next_section = 'when';
          $scope.prev_section = 'vehicle';
          break;

        case 'when':
          $scope.show.when_section = true;
          $scope.next_section = 'what';
          $scope.prev_section = 'where';
          if ($scope.timeFrame === null) {
            $scope.timeFrame = 'now';
          }
          break;

        case 'what':
          $scope.show.what_section = true;
          $scope.next_section = 'preview';
          $scope.prev_section = 'when';
          break;
      }
    }

    $scope.resetRecoveryForm = function () {
      $scope.passwordRecoveryMessage = '';
    };

    $scope.$watch('current_section', function (section) {
      console.log('current_section', section);
      $scope.watcher && $scope.watcher();
      switch (section) {
        case 'where':
          console.log('registering where');
          $scope.watcher = $scope.$watch('section.where_done', function (value) {
            console.log('section.where_done', value);
            $scope.continue_is_enabled = value;
          });
          break;
        case 'when':
          console.log('registering when');
          $scope.watcher = $scope.$watch('section.when_done', function (value) {
            $scope.continue_is_enabled = value;
          });
          break;
        case 'vehicle':
          console.log('registering vehicle');
          $scope.watcher = $scope.$watch('section.vehicle_done', function (
            value
          ) {
            console.log('section.vehicle_done', value);
            $scope.continue_is_enabled = value;
          });
          break;
        case 'what':
          $scope.watcher = $scope.$watch('section.what_done', function (value) {
            console.log('section.what_done', value);
            $scope.continue_is_enabled = (value && $scope.section.when_done
              && $scope.section.where_done);
          });
          break;
          // case "labour/coupon":
          //     $scope.continue_is_enabled = ($scope.section.where_done && $scope.section.when_done && $scope.section.vehicle_done);
          //     break;
      }
    });

    $scope.validate_where = function () {
      console.log('validate where');
      $scope.section.where_done =
        $scope.bookingObject.pickup.address.full_address != '';
      $scope.section.where_done =
        $scope.section.where_done &&
        $scope.bookingObject.dropoff.address.full_address != '';
      for (var i = 0; i < $scope.bookingObject.waypointMarkers.length; i++) {
        var wPoint = $scope.bookingObject.waypointMarkers[i];
        console.log(wPoint, wPoint.address.full_address);
        $scope.section.where_done =
          $scope.section.where_done && wPoint.address.full_address != '';
      }
      console.log('$scope.section.where_done', $scope.section.where_done);
    };

    $scope.$watchCollection('bookingObject.waypointMarkers', function (n_, o_) {
      $scope.validate_where();
    });

    $scope.is_continue_enabled = function () {
      console.log('Running is continue enabled');
      switch ($scope.current_section) {
        case 'where':
          return $scope.section.where_done;
        case 'when':
          return $scope.section.when_done;
        case 'vehicle':
          return $scope.section.vehicle_done;
          // case "labour/coupon":
          //     return ($scope.section.where_done && $scope.section.when_done && $scope.section.vehicle_done);
      }
      console.log('here also');
      return true;
    };

    $scope.openBookingForm = function () {
      if (deviceDetector.isMobile()) {
        $('.chat-intro-container, .chat-icon-container').hide();
      }
      if (
        google.maps.geometry.poly.containsLocation(
          $scope.bookingObject.pickup.position,
          $scope.selectedCityPolygon.pickup
        )
      ) {
        $scope.bookingObject.pickup.marker.setPosition(
          $scope.bookingObject.pickup.position
        );
      } else {
        $scope.bookingObject.pickup.marker.setPosition(
          $scope.selectedCityBounds.pickup.getCenter()
        );
      }
      if (
        google.maps.geometry.poly.containsLocation(
          $scope.bookingObject.dropoff.position,
          $scope.selectedCityPolygon.dropoff
        )
      ) {
        $scope.bookingObject.dropoff.marker.setPosition(
          $scope.bookingObject.dropoff.position
        );
      } else {
        $scope.bookingObject.dropoff.marker.setPosition(
          $scope.selectedCityBounds.dropoff.getCenter()
        );
      }
      $scope.showSection('vehicle');
      $scope.zoomToMarker($scope.bookingObject.pickup.marker);
      $timeout(function () {
        triggerMapResize($scope.map.mapBooking);
        $scope.zoomOutMarkers(); // zoomToMarker($scope.bookingObject.pickup.marker);
      }, 200);
      getTimeSlots();
    };

    $scope.bookingFormLogoClicked = function () {
      window.location.href = homepage_url || `/embed/home`;
    }

    $scope.selectCity = function (city, noreload) {
      if ($scope.selectedCity != city) {
        $scope.bookingObject.pickup.address.full_address = '';
        $scope.bookingObject.dropoff.address.full_address = '';
        $scope.resetPickupDropoffWaypoints();
      }
      $scope.b_user_logged_in =
        b_user_logged_in == 'True' || Boolean(blowhornService.userProfile);
      $scope.b_myfleet_required =
        b_myfleet_required == 'True' ||
        (blowhornService.userProfile &&
          blowhornService.userProfile.b_myfleet_required);

      $scope.selectedCity = city;
      selected_city = city;
      /* Display only selected city vehicles mapped in c2c contract */
      $scope.vehicle_classes = $scope.vehicle_dict[$scope.selectedCity];
      $scope.$applyAsync();
      // Adding a delay of 100 ms since 'instant-from' is disabled initially and
      // and enabled only after city is selected
      $timeout(function () {
        let ref = document.getElementById('instant-from');
        if (ref) {
          ref.focus();
        }
      }, 100);
    };

    $scope.$watch('selectedCity', function (value) {
      console.log('selectedCity changed. Setting City Bounds', value);
      if (value == null) {
        return;
      }

      $scope.selectedCityPolygon = {
        pickup: $scope.polygons[value]['pickup'],
        dropoff: $scope.polygons[value]['dropoff'],
      }
      $scope.selectedCityBounds.pickup = $scope.bounds[value]['pickup'];
      $scope.selectedCityBounds.dropoff = $scope.bounds[value]['dropoff'];
    });

    $scope.isCitySelected = function (city) {
      return $scope.selectedCity === city;
    };

    $scope.closeTooltip = function () {
      $scope.hideSavedAddressHelperText = true;
      generalService.setValueToLocalStorage(SAVED_ADDRESS_HELPER_FLAG, true);
    };

    $scope.openSaveAddress = function (data) {
      console.log('open save address', data);
      $scope.$$childHead.saveAddressForm.$setPristine();
      $scope.$$childHead.saveAddressForm.$setUntouched();
      $scope.show.save_address = true;
      $scope.address_under_edit = data;
      $scope.savedAddress = angular.copy({
        name: data.name,
        address: data.address,
        contact: data.contact,
        position: {
          lat: data.marker.getPosition().lat(),
          lon: data.marker.getPosition().lng()
        },
        errors: {
          addressName: false,
          addressMobile: false
        }
      });
      $scope.$applyAsync();
    };

    $scope.openAddEditContact = function (data) {
      console.log('open add/edit contact', data);
      $scope.$$childHead.saveContactForm.$setPristine();
      $scope.$$childHead.saveContactForm.$setUntouched();
      $scope.show.add_edit_contact = true;
      $scope.address_under_edit = data;
      $scope.newContact = angular.copy(data.contact);
      $scope.$applyAsync();
    };

    $scope.dismissEditContactModal = function () {
      $scope.show.add_edit_contact = false;
      $scope.newContact = {
        mobile: '',
        name: ''
      };
    };

    $scope.AddEditContact = function (contact) {
      console.log('add/edit contact', contact);
      $scope.show.add_edit_contact = false;
      $scope.address_under_edit.contact = contact;
      $scope.$applyAsync();
    };

    $scope.$watch('bookingObject.selected_vehicle_class', function (value) {
      if (value != null) {
        $scope.section.vehicle_done = true;
        $scope.updateAvailableLabourOptions();
        // $scope.estimateFare();
      }
    });

    // $scope.$watch('bookingObject.selectedLabour', function (val) {
    //   $scope.estimateFare();
    // });

    $scope.$watch('bookingObject.podRequired', function (val) {
      // $scope.estimateFare();
      if ($scope.show.booking_summary) {
        $scope.getFareEstimation();
      }
    });

    // Refer: https://developers.google.com/web/fundamentals/native-hardware/user-location/
    var locationErrors = {
      0: 'Unknown error',
      1: 'Permission denied',
      2: 'Position unavailable (error response from location provider)',
      3: 'Timed out'
    };

    $scope.useCurrentLocation = function (marker) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          function (position) {
            var geolocation = getGoogleLatLngObject(
              position.coords.latitude,
              position.coords.longitude
            );
            console.log(geolocation);
            var city = generalService.getCity(geolocation, $scope.polygons);
            if (city != null) {
              marker.setPosition(geolocation);
              new google.maps.event.trigger(marker, 'dblclick');
              $scope.map.mapBooking.panTo(geolocation);
            } else {
              //address.full_address = '';
              $scope.alertMessage = 'Area not covered';
              $scope.isDisabled = true;
              $scope.outOfCoverage = true;
              $scope.$applyAsync();
            }
          },
          function (error) {
            console.log('Error occurred. Error code: ' + error.code);
            //alert(locationErrors[1]);
          }
        );
      } else {
        alert('Geolocation is not supported for this Browser/OS.');
      }
    };

    $scope.selectSavedLocation = function (target, saved_location) {
      console.log(saved_location);
      target.name = saved_location.name;
      target.saved = true;
      target.address = angular.copy(saved_location.address);
      target.contact = {
        name: saved_location.contact.name,
        mobile: saved_location.contact.mobile ?
          '' + saved_location.contact.mobile : ''
      };
      target.marker.setPosition(saved_location.geopoint);
      //$scope.calcRoute();
      $scope.zoomToMarker(target.marker);
    };

    $scope.swapPickupAndDropoff = function () {
      // Copy address and marker position of pickup
      var p_address = angular.copy($scope.bookingObject.pickup.address);
      var p_position = $scope.bookingObject.pickup.marker.getPosition();

      // Replace pickup address and marker position with dropoff data
      $scope.bookingObject.pickup.marker.setPosition(
        $scope.bookingObject.dropoff.marker.getPosition()
      );
      $scope.bookingObject.pickup.address =
        $scope.bookingObject.dropoff.address;

      // Replace dropoff data with the copied data
      $scope.bookingObject.dropoff.marker.setPosition(p_position);
      $scope.bookingObject.dropoff.address = p_address;
      $scope.$applyAsync();
      $scope.zoomOutMarkers();
    };

    function invalidateInputs(marker) {
      marker.setVisible(false);
      //infowindow.close();
      $scope.alertMessage = 'Pickup and Dropoff should be in the same city';
      $scope.isDisabled = true;
      $scope.outOfCoverage = true;
      $scope.$applyAsync();
    }

    function validateAddressForm() {
      var isAddressFormValid = true;
      var pickupCity = $scope.pickupAddress.city;
      if (pickupCity !== $scope.bookingObject.dropoffAddress.city) {
        $scope.pickupAddress = {};
        $scope.bookingObject.dropoffAddress = {};
        invalidateInputs($scope.bookingObject.pickup.marker);
        invalidateInputs($scope.bookingObject.dropoff.marker);
        isAddressFormValid = false;
      } else {
        // Check city of waypoints and highlight invalid fields
        for (var i = 0; i < $scope.bookingObject.waypointMarkers.length; i++) {
          var wPoint = $scope.bookingObject.waypointMarkers[i];
          if (wPoint.city !== pickupCity) {
            wPoint.address = '';
            wPoint.setVisible(false);
            isAddressFormValid = false;
          }
        }
        $scope.alertMessage = !isAddressFormValid ?
          'Waypoint should be in same city as pickup and dropoff' :
          '';
      }
      $scope.$applyAsync();
      return isAddressFormValid;
    }

    $scope.formsubmitted = false;
    $scope.submitForm = function (event, section) {
      $scope.formsubmitted = true;
      $scope.isDisabled = true;
      if (section === 'when') {
        if (
          $scope.addressForm.$valid &&
          !$scope.outOfCoverage &&
          validateAddressForm()
        ) {
          $scope.alertMessage = '';
          $scope.isDisabled = false;
          sectionWhen(); //next section
        }
      } else if (section === 'what') {
        if (
          $scope.addressForm.$valid &&
          validateTimeForm() &&
          !$scope.outOfCoverage
        ) {
          $scope.isDisabled = false;
        }
      } else if (section === 'confirmBooking') {
        if (
          $scope.addressForm.$valid &&
          validateTimeForm() &&
          !$scope.outOfCoverage
        ) {
          $scope.isDisabled = false;
        }
      }
    };

    function validateTimeForm() {
      return (
        ($scope.bookingObject.selectedTimeFrame === 'now' &&
          !$scope.nowNotAvailable) ||
        ($scope.bookingObject.selectedTimeFrame === 'later' &&
          $scope.selected_time) ||
        ($scope.bookingObject.selectedTimeFrame === 'tomorrow' &&
          $scope.selected_time) ||
        ($scope.bookingObject.selectedTimeFrame === 'custom' &&
          $scope.mdate &&
          $scope.selected_time)
      );
    }

    $scope.isTimeFrameSelected = function (timeframe) {
      return $scope.bookingObject.selectedTimeFrame === timeframe;
    };

    $scope.dayIsAvailable = function (day) {
      if (day === -1) {
        return;
      }
      return day.isBetween($scope.today, $scope.availability_end, 'day', '[]');
    };

    $scope.selectDay = function (day) {
      if ($scope.dayIsAvailable(day)) {
        $scope.selected_day = day;
        $scope.getTimeBox(day);
        // $scope.estimateFare();
      }
    };

    $scope.$watch('selected_day', function (value) {
      $scope.bookingObject.selected_datetime = null;
      $scope.selected_time = null;
      $scope.section.when_done = false;
    });

    $scope.selectTimeSlot = function (newTime) {
      $scope.selected_time = newTime.format('hh:mmA');
      var hour = newTime.format('HH');
      var minute = newTime.format('mm');
      $scope.bookingObject.selected_datetime = $scope.selected_day
        .clone()
        .hour(hour)
        .minute(minute);
      $scope.section.when_done = true;
      $scope.$applyAsync();
    };

    $scope.isSlotActive = function (time) {
      return $scope.selected_time === time;
    };

    $scope.isPanelSelected = function (panel) {
      return $scope.selectedPanel === panel;
    };

    $scope.selectPanel = function (panel) {
      $scope.selectedPanel = panel;
    };

    $scope.selectItems = function (item) {
      var found = $scope.bookingObject.selectedItems.indexOf(item);
      if (found === -1) {
        $scope.bookingObject.selectedItems.push(item);
      } else {
        $scope.bookingObject.selectedItems.splice(found, 1);
      }
      $scope.section.what_done = $scope.bookingObject.selectedItems &&
        $scope.bookingObject.selectedItems.length;
    };

    $scope.addMoreItems = function (e) {
      e.preventDefault();
      var moreItems = $scope.moreItems;
      var itemsCount = moreItems.length;
      if (itemsCount > 0 && !moreItems[itemsCount - 1]) {
        return;
      }
      $scope.moreItems.push('');
    };

    $scope.removeItem = function (index) {
      $scope.moreItems.splice(index, 1);
    };

    $scope.verifyMobile = function () {
      console.log($scope.bookingObject.mobileNum);
      $scope.sendOtp();
    };

    $scope.verifyOtp = function () {
      console.log($scope.bookingObject.otpNum);
      verifyOtpBooking();
    };

    $scope.verifyOtpAndBook = function () {
      console.log($scope.bookingObject.otpNum);
      var data = {
        mobile: $scope.bookingObject.mobileNum,
        otp: $scope.bookingObject.otpNum
      };
      $scope.verifyingOtp = true;
      blowhornService
        .verifyOtpBooking(data)
        .then(function (data) {
          $scope.placeBooking();
        }).catch(function (data) {
          $scope.show.otp_invalid = true;
          $scope.show.booking_otp = true;
        });
    };

    $scope.resendOtp = function () {
      $scope.bookingObject.otpNum = '';
      $scope.show.otp_invalid = false;
      $scope.disableBooking = true;
      $scope.disableCoupon = true;
      if (!debounceresend) {
        debounceresend = true;
        //timerOn();
        otpresendcount++;
        $scope.sendOtp();
        setTimeout(function () {
          debounceresend = false;
        }, 3000);
      }
    };

    $scope.getCouponData = function () {
      var data = {
        mobile: $scope.bookingObject.mobileNum,
        otp: $scope.bookingObject.otpNum,
        time_frame: $scope.bookingObject.selectedTimeFrame,
        date: $scope.bookingObject.selected_datetime != null ?
          $scope.bookingObject.selected_datetime.format('DD MMM YYYY HH:mm') : '',
        latitude: $scope.bookingObject.pickup.marker.getPosition().lat(),
        longitude: $scope.bookingObject.pickup.marker.getPosition().lng(),
        city: $scope.selectedCity,
        device_type: 'web'
      };
      blowhornService.getCoupons(data)
      .then(function (response) {
        $scope.couponData = response;
      }).catch(function (data) {
        console.error(data)
      });
    };

    $scope.selectCoupon = function (val) {
      $scope.bookingObject.couponCode = val;
    }

    $scope.showMore = function (val){
      $scope.moreData = val;
      $scope.show.coupon_terms_and_conditions=true
    }

    $scope.applyCoupon = function () {
      $scope.verifyingCoupon = true;
       if ($scope.show.loadingFare) {
        return;
       }
      var data = {
        coupon_code: $scope.bookingObject.couponCode,
        mobile: $scope.bookingObject.mobileNum,
        otp: $scope.bookingObject.otpNum,
        time_frame: $scope.bookingObject.selectedTimeFrame,
        date: $scope.bookingObject.selected_datetime != null ?
          $scope.bookingObject.selected_datetime.format('DD MMM YYYY HH:mm') : '',
        'pickup-lat': $scope.bookingObject.pickup.marker.getPosition().lat(),
        'pickup-lng': $scope.bookingObject.pickup.marker.getPosition().lng(),
        city: $scope.selectedCity,
        device_type: 'web'
      };
      console.log('coupon', data);
      blowhornService
        .applyCouponCode(data)
        .then(function (data) {
          console.log('coupon data', data);
          $scope.couponTermsAndConditions = data.terms_and_conditions;
          $scope.bookingObject.discount_value = data.discount_value;
          $scope.bookingObject.discount_type = data.discount_type;
          $scope.bookingObject.max_allowed_discount = data.max_allowed_discount;
          // $scope.estimateFare();
          $scope.changeCouponFlags(true, data.message);
        })
        .catch(function (data) {
          $scope.verifyingCoupon = false;
          $scope.changeCouponFlags(false, data.responseJSON.message);
        });
    };

    $scope.removeCoupon = function () {
      $scope.bookingObject.couponCode = '';
      $scope.show.coupon_success = false;
    };

    $scope.bookMyTruck = function () {
      if (!$scope.b_user_logged_in) {
        alert('Please login to book the truck.')
        return
      }
      $scope.mobileVerificationRequired = false;
      if (!$scope.userProfile.mobile) {
        $scope.userProfile.error = !$scope.userProfile.name ? 'Please update your contact details' : 'Please update your mobile number';
        $scope.mobileReadonly = false;
        $scope.mobileVerificationRequired = true;
        $scope.showProfile();
        return;
      }
      if (!$scope.userProfile.name) {
        $scope.userProfile.error = 'Please update your name';
        $scope.showProfile();
        return;
      }
      if (!$scope.userProfile.mobile_status && !$scope.userProfile.email_status) {
        $scope.resendActivationOtp();
        $scope.mobileVerificationRequired = true;
        return;
      }
      $scope.placeBooking();
    };

    $scope.sendOtpAndShowOtp = function () {
      $scope.show.booking_otp = true;
    };

    $scope.scrollTo = function (event, id) {
      event.preventDefault();
      $location.hash(id);
      $anchorScroll();
    };

    $scope.saveAddress = function (_form) {
      if (
        !$scope.savedAddress.name ||
        _form.saveAddressForm.addressContactMobile.$error.pattern
      ) {
        return;
      }
      var data = {
        name: $scope.savedAddress.name,
        line: $scope.savedAddress.address.street_address,
        area: $scope.savedAddress.address.area,
        city: $scope.savedAddress.address.city,
        full_address: $scope.savedAddress.address.full_address,
        postal_code: $scope.savedAddress.address.postal_code,
        lat: $scope.savedAddress.position.lat,
        lon: $scope.savedAddress.position.lon,
        contact_name: $scope.savedAddress.contact && $scope.savedAddress.contact.name,
        contact_mobile: $scope.savedAddress.contact && $scope.savedAddress.contact.mobile
      };
      blowhornService
        .saveUserAddress(data)
        .then(function (data) {
          console.log(data);
          $scope.address_under_edit.saved = true;
          $scope.address_under_edit.contact = $scope.savedAddress.contact;
          $scope.address_under_edit.name = $scope.savedAddress.name;
          $scope.savedAddress.saved = true;
          $scope.savedAddress.error = false;
          $scope.show.save_address = false;
          $scope.saveBtnText = 'Saved';
          $scope.userProfile.saved_locations = data;
          $scope.$applyAsync();
        })
        .catch(function (data) {
          console.log(data);
          $scope.savedAddress.error = data.responseJSON;
        });
    };

    $scope.dismissSaveAddressModal = function () {
      $scope.show.save_address = false;
      $scope.savedAddress = {
        name: '',
        contact: {
          name: '',
          mobile: ''
        }
      };
    };

    /*
    $scope.getUserAddress = function () {
        if ($scope.user_logged_in) {
            blowhornService.getUserAddress().then(function (data) {
                $scope.saved_locations = data.result;
                $scope.$applyAsync();
            }).catch(function (data) {
                console.log(data);
            });
        }
    };
    */

    $scope.zoomToMarker = function (marker) {
      $scope.map.mapBooking.panTo(marker.getPosition());
      $scope.map.mapBooking.setZoom(16);
      marker.infobox.openIfClosed();
    };

    $scope.optimizePath = function () {
      $scope.showConfirmation = false;
      var waypoints = $scope.bookingObject.waypointMarkers.map(function (val) {
        return {
          location: val.position
        };
      });
      $scope.directionsService.route({
          origin: $scope.bookingObject.pickup.marker.position,
          destination: $scope.bookingObject.dropoff.marker.position,
          waypoints: waypoints,
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING
        },
        function (response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
            console.log(response);
            var routes = response.routes;
            if (routes[0]) {
              var waypointOrders = routes[0].waypoint_order;
              var optimizedOrderWaypoints = [];
              for (var i = 0; i < waypointOrders.length; i++) {
                var wPoint =
                  $scope.bookingObject.waypointMarkers[waypointOrders[i]];
                optimizedOrderWaypoints.push(wPoint);
              }
              $scope.bookingObject.waypointMarkers = optimizedOrderWaypoints;
              //directionsDisplay.setDirections(response);
              showConfirmation(
                'Waypoints have been optimized successfully.',
                'OK'
              );
            } else {
              //showConfirmation("No need to optimize.", "OK");
            }
          } else {
            //window.alert('Directions request failed due to ' + status);
          }
        }
      );
    };

    $scope.toggleMap = function () {
      $('#sideBar, .mapBar').toggle();
    };

    $scope.telephoneOnKeydown = function (event) {
      if (event.which === 187) {
        event.preventDefault();
        return false;
      }
    }

    $scope.jQueryInit = function () {
      console.log('Initializing jQuery');
      $('.btn-map, .btnClose, .btnOk').on('click', function () {
        $('#sideBar, .mapBar').toggle();
      });
      $('.toggle-menu, #intro nav .translucent, #intro nav a').on(
        'click',
        function () {
          $('nav').toggleClass('on');
        }
      );
      var bodyWidth = $(window).width();
      if (bodyWidth > 768) {
        $('.hasStops').css('max-height', $('#sideBar').height() - 200);
      }

      $('.chat-avatar-container').toggle('scale');
      if (window.location.pathname === '/' && display_chat_help) {
        // Chat help with animation
        setTimeout(function () {
          $('.chat-intro-container').addClass('grow');
          $('.chat-avatar-container').toggle('scale');
          setTimeout(function () {
            $('.chat-intro-container').removeClass('grow');
            $('.chat-avatar-container').toggle('scale');
          }, 5000);
        }, 4000);
        generalService.setValueToLocalStorage(CHAT_HELP_SHOWN_FLAG, true);
      }
    };

    $scope.documentClickHandler = function (e) {
      if ($scope.bandId === 'track') {
        $scope.bandId = 'discount-off';
      }
      var classList = e.target.classList;
      let parentClassList = e.target.parentElement.classList;
      if (
        classList.value === 'visible-xs toggle-menu header-burger' ||
        parentClassList.value === 'visible-xs toggle-menu header-burger'
      ) {
        $('nav').toggleClass('on');
        $scope.no_scroll = true;
      } else {
        $('nav').removeClass('on');
        $scope.no_scroll = false;
      }
    };

    // $(document).on('click', function (e) {
    //     if ($scope.bandId === "track") {
    //         $scope.bandId = "discount-off";
    //     }
    //     var classList = e.target.classList;
    //     let parentClassList = e.target.parentElement.classList;
    //     if (classList.value === 'visible-xs toggle-menu header-burger' ||
    //         parentClassList.value === 'visible-xs toggle-menu header-burger') {
    //         $("nav").toggleClass("on");
    //     } else {
    //         $("nav").removeClass("on");
    //     }
    // });

    function sanitizeContact(contact) {
      if (contact == null) {
        return null;
      } else if (contact.name && contact.mobile) {
        return contact;
      } else {
        return null;
      }
    }

    $scope.getRoutePickup = function (stops) {
      var pickup = stops.filter(function (stop) {
        return stop.seq_no === 0;
      });

      return pickup[0].line1;
    };

    $scope.getRouteDropoff = function (stops) {
      var len = stops.length;

      var dropoff = stops.filter(function (stop) {
        return stop.seq_no === len - 1;
      });

      return dropoff[0].line1;
    };

    $scope.getSavedRoutes = function (req_data) {
      blowhornService
        .getSavedRoutes(req_data, $scope.selectedCity)
        .then(function (res_data) {
          $scope.show.load_route = true;
          $scope.routes = res_data;
        })
        .catch(function (res_data) {
          alert(res_data);
        });
    };

    $scope.resetPickupDropoffWaypoints = function () {
      generalService.resetLocationObject($scope.bookingObject.pickup);
      generalService.resetLocationObject($scope.bookingObject.dropoff);
      $scope.bookingObject.waypointMarkers.map(function (wp) {
        wp.marker.setMap(null);
      });
      $scope.bookingObject.waypointMarkers = [];
    };

    $scope.modifyPickupDropoffWaypoints = function (setTo, setFrom) {
      setTo.address = {
        full_address: setFrom.line1,
        area: setFrom.line3,
        city: setFrom.line4,
        state: setFrom.state,
        postal_code: setFrom.postcode,
        country: setFrom.country
      };
      setTo.contact = {
        name: setFrom.contact_name,
        mobile: setFrom.contact_mobile && setFrom.contact_mobile.toString().startsWith(isd_dialing_code) ?
          setFrom.contact_mobile.toString().substring(3) : setFrom.contact_mobile
      };
      if (setTo.marker) {
        setTo.marker.setPosition(
          new google.maps.LatLng(
            setFrom.geopoint.coordinates[1],
            setFrom.geopoint.coordinates[0]
          )
        );
      }
    };

    $scope.getSavedRoute = function (req_data) {
      blowhornService
        .getSavedRoutes(req_data)
        .then(function (res_data) {
          $scope.resetPickupDropoffWaypoints();

          let stops = res_data[0].stops;
          stops = stops.sort((a, b) => {
            return parseInt(a.seq_no - b.seq_no);
          });
          let dropoff = stops.pop();
          let pickup = stops.shift();

          // setting pickup location
          $scope.modifyPickupDropoffWaypoints(
            $scope.bookingObject.pickup,
            pickup
          );

          // setting dropoff location
          $scope.modifyPickupDropoffWaypoints(
            $scope.bookingObject.dropoff,
            dropoff
          );

          // zoomout has to be called here since waypoints will be rendered from the init watch
          // and calling it after waypoints throw null pointer for getposition
          $scope.zoomOutMarkers();

          //setting waypoints
          for (let stop of stops) {
            $scope.addWaypoint(
              new google.maps.LatLng(
                stop.geopoint.coordinates[1],
                stop.geopoint.coordinates[0]
              )
            );
            let waypoint =
              $scope.bookingObject.waypointMarkers[
                $scope.bookingObject.waypointMarkers.length - 1
              ];
            $scope.modifyPickupDropoffWaypoints(waypoint, stop);
          }
        })
        .catch(function (res_data) {
          alert(res_data);
        });
    };
    $scope.test = function (vc) {
      console.log(vc);
    };
    $scope.saveRoute = function () {
      $scope.show.save_route_msg = false;
      var name = $('#route_name')[0].value.trim();
      if (!name) {
        return;
      }

      var data = {
        route: {
          name: name,
          city: $scope.selectedCity,
          distance: $scope.total_distance_meters
        }
      };

      var stops = [];
      var bookingObject = $scope.saveRouteObject;

      stops.push({
        seq_no: 0,
        line1: bookingObject.pickup.address.full_address,
        line2: bookingObject.pickup.address.street_address,
        line3: bookingObject.pickup.address.area,
        line4: bookingObject.pickup.address.city,
        postcode: bookingObject.pickup.address.postal_code,
        state: bookingObject.pickup.address.state,
        country: country_code,
        contact_name: bookingObject.pickup.contact && bookingObject.pickup.contact.name,
        contact_mobile: bookingObject.pickup.contact && bookingObject.pickup.contact.mobile,
        lat: bookingObject.pickup.marker.position.lat(),
        lng: bookingObject.pickup.marker.position.lng()
      });

      var i = 1;
      for (var waypoint of bookingObject.waypointMarkers) {
        stops.push({
          seq_no: i++,
          line1: waypoint.address.full_address,
          line2: waypoint.address.street_address,
          line3: waypoint.address.area,
          line4: waypoint.address.city,
          postcode: waypoint.address.postal_code,
          state: waypoint.address.state,
          country: country_code,
          contact_name: waypoint.contact && waypoint.contact.name,
          contact_mobile: waypoint.contact && waypoint.contact.mobile,
          lat: waypoint.marker.position.lat(),
          lng: waypoint.marker.position.lng()
        });
      }

      stops.push({
        seq_no: bookingObject.waypointMarkers.length + 1,
        line1: bookingObject.dropoff.address.full_address,
        line2: bookingObject.dropoff.address.street_address,
        line3: bookingObject.dropoff.address.area,
        line4: bookingObject.dropoff.address.city,
        postcode: bookingObject.dropoff.address.postal_code,
        state: bookingObject.dropoff.address.state,
        country: country_code,
        contact_name: bookingObject.dropoff.contact && bookingObject.dropoff.contact.name,
        contact_mobile: bookingObject.dropoff.contact && bookingObject.dropoff.contact.mobile,
        lat: bookingObject.dropoff.marker.position.lat(),
        lng: bookingObject.dropoff.marker.position.lng()
      });

      data.route.stops = stops;
      blowhornService
        .saveRoute(data)
        .then(function (data) {
          $('#save-route-msg').text('Route is saved');
          $('#save-route-msg').css('color', 'green');
          $scope.show.save_route_msg = true;
          $scope.show.save_route_success = true;
          $scope.bookingObject = $scope.saveRouteObject;
          //$scope.closeSaveRoute();
        })
        .catch(function (data) {
          $('#save-route-msg').text(data);
          $('#save-route-msg').css('color', 'red');
          $scope.show.save_route_msg = true;
          $scope.show.save_route_success = false;
        });
    };

    $scope.closeSaveRoute = function () {
      $scope.show.save_route = false;
      $scope.show.save_route_msg = false;
      var name_field = $('#route_name')[0];
      name_field.value = '';
      $scope.saveRouteObject = {};
    };

    $scope.selectRateCard = function (vc, rate) {
      // $scope.bookingObject.selected_contract_id = rate.contract_id;
      //$scope.bookingObject.selected_vehicle_class = vc.classification;
      vc.selected_contract_id = rate.contract_id;
      //$scope.selected_rate_text = rate.description.base.text;
      $scope.show.vehicle_info = false;
      // $scope.estimateFare();
      $scope.selectVehicleClass(vc);
    };

    $scope.selectVehicleClass = function (vc) {
      $scope.bookingObject.selected_vehicle_class = vc.classification;
      $scope.bookingObject.selected_contract_id = vc.selected_contract_id;
      $scope.selected_rate_text = $scope.getRateText(vc);
      $scope.more_of_vehicle = vc;
      $scope.currency = vc.packages.length ? vc.packages[0].currency : DEFAULT_CURRENCY;
      $scope.updateAvailableLabourOptions();
    };

    $scope.selectDefaultLabour = function () {
      $scope.bookingObject.selectedLabour = !$scope.bookingObject.labourRequired ?
        null : $scope.availableLabourOptions[0];
    };

    $scope.openAvailableLabourOptions = function () {
      $scope.bookingObject.labourRequired = !$scope.bookingObject
        .labourRequired;
      $scope.selectDefaultLabour();
    };

    $scope.updateAvailableLabourOptions = function () {
      $scope.availableLabourOptions = [];
      $scope.bookingObject.selectedLabour = null;
      var rate_package = $scope.more_of_vehicle.packages.find(
        vc =>
        vc.labour_configuration.contract ===
        $scope.bookingObject.selected_contract_id
      );

      if (!rate_package) {
        $scope.isPodApplicable = false;
        return;
      }
      var labour = angular.copy(rate_package.labour_configuration);
      for (var j = 0; j < labour.max_labours; j++) {
        if (j == 0) {
          display_text = ['Driver', 'Only', '@'];
        } else {
          display_text = ['Driver', '+', j, '@'];
        }
        var num_of_labours = j + 1;
        display_text.push($filter('formatCurrency')($scope.currency) + labour.cost_per_labour * num_of_labours);
        $scope.availableLabourOptions.push({
          num_of_labours: num_of_labours,
          cost_per_labour: labour.cost_per_labour,
          total_labour_cost: num_of_labours * labour.cost_per_labour,
          display_text: display_text.join(' ')
        });
        $scope.selectDefaultLabour();
      }
      $scope.setPodCost(rate_package);
    };

    $scope.setPodCost = function (rate_package) {
      $scope.isPodApplicable = rate_package && rate_package.is_pod_applicable;
      $scope.podCost = rate_package.pod_cost;
    }

    $scope.selectLabour = function (e, labour) {
      e.preventDefault();
      $scope.bookingObject.selectedLabour = labour;
    };

    $scope.setPaymentMode = function (e, mode) {
      e.preventDefault();
      if (mode === 'Wallet' &&
        $scope.userProfile.wallet_available_balance < $scope.bookingObject.estimatedInitFare) {
        return;
      }
      $scope.bookingObject.payment_mode = mode;
    }

    if (selected_city && $location.path() === '/') {
      $scope.selectCity(selected_city, true);
      if ($scope.isDefaultBuild) {
        var title = `Blowhorn! Mini-Trucks on hire in ${selected_city}`;
        var meta_title = `blowhorn! Mini-Trucks on hire in ${selected_city}`;
        var description = `We are offering the most convenient way to move goods in ${selected_city} with real time tracking and fixed pricing. Fast and zippy mini-trucks, pickup trucks, light trucks. Give us a try! Visit our website to make a booking.`;
        if (selected_city.toLowerCase() === 'bengaluru') {
          title =
            'Hire Shifting Services Bangalore | Book Tempo, Mini Truck & Tata Ace Online';
          meta_title = title;
          description =
            'If you are looking for any shifting services on hire, then Bornhorn is the perfect match for you. We deliver wide transportation services across Bangalore, mini trucks, tempo, Tata Ace and other vehicles available. Get the app and book online!';
        } else if (selected_city.toLowerCase() === 'chennai') {
          title =
            'Booking Mini Truck, Tempo & Tata Ace on Rent | Goods Transport Services Chennai';
          meta_title = title;
          description =
            'Blowhorn offers a quick and reliable way for booking mini trucks, tempo and Tata Ace on rent across Chennai. We provide affordable goods transportation services in Chennai. Download the app now!';
        } else if (selected_city.toLowerCase() === 'hyderabad') {
          title =
            'Hire Mini Trucks Hyderabad | Online Tempo Transport Service | Tata Ace Booking';
          meta_title = title;
          description =
            'Hire mini trucks, tempo, Tata ace and other moving van with Blowhorn. We offer the most convenient way for moving goods in Hyderabad with real time tracking and fixed pricing. For bookings visit our website now!';
        } else if (selected_city.toLowerCase() === 'mumbai') {
          title =
            'Hire Mini Trucks Mumbai | Book Tempo Transport Service Online | Tata Ace Rental';
          meta_title = title;
          description =
            'Book affordable transportation services across Mumbai with Blowhorn. We have various transportation vehicles on rentals including large to mini trucks, tempo, Tata Ace and more. Hire online!';
        }
        document.title = title;
        $("meta[name='title']").attr('content', meta_title);
        $("meta[name='description']").attr('content', description);
        $("meta[property='og:title']").attr('content', meta_title);
        $("meta[property='og:description']").attr('content', description);
      }
    }

    function initRebook() {
      console.log($scope.bookingObject.pickup);

      $scope.selectCity(rebook_data.city, true);
      var vehicle_class = $scope.vehicle_classes.find(
        item => item.classification === rebook_data.vehicle_class
      );
      vehicle_class.selected_contract_id = rebook_data.contract_id;
      $scope.selectVehicleClass(vehicle_class);

      rebook_data.items.map(function (item) {
        console.log('Rebook item', item);
        if ($scope.items.indexOf(item) == -1) {
          $scope.items.push(item);
        }
        $scope.selectItems(item);
      });
      $scope.$watch('bookingObject.pickup.marker', function (marker) {
        if (marker) {
          marker.setPosition(rebook_data.pickup.geopoint);
        }
      });
      $scope.bookingObject.pickup.address = rebook_data.pickup.address;
      $scope.bookingObject.pickup.contact = sanitizeContact(
        rebook_data.pickup.contact
      );
      $scope.bookingObject.dropoff.address = rebook_data.dropoff.address;
      $scope.bookingObject.dropoff.contact = sanitizeContact(
        rebook_data.dropoff.contact
      );

      rebook_data.waypoints.map(function (wp, i) {
        var stop_number = i + 1;
        var title = 'Stop ' + stop_number;
        var wp_obj = generalService.getLocationObject(
          title,
          mapHelpers.getWaypointIcon(stop_number),
          wp.geopoint
        );
        wp_obj.address = wp.address;
        wp_obj.contact = sanitizeContact(wp.contact);
        $scope.bookingObject.waypointMarkers.push(wp_obj);
      });

      $scope.$watch('bookingObject.dropoff.marker', function (marker) {
        if (marker) {
          marker.setPosition(rebook_data.dropoff.geopoint);
          if ($scope.bookingObject.pickup.address.formatted_address !== MESSAGE_OUT_OF_COVERAGE &&
            $scope.bookingObject.dropoff.address.formatted_address !== MESSAGE_OUT_OF_COVERAGE) {
            $scope.showSection('when');
            $scope.openBookingForm();
            $scope.calcRoute();
            if ($scope.show.what_section) {
                $scope.getFareEstimation();
            }
          } else {
            $('#completed').modal('show');
            $scope.exit_screen = 'fail';
            $scope.booking_error_msg = MESSAGE_REBOOK_OUT_OF_COVERAGE;
          }
        }
        $scope.$applyAsync();
      });
      $scope.show.ghost = false;
      setTimeout(function () {
        $scope.calcRoute();
      }, 5000);
      $scope.$applyAsync();
    }

    function updateAddress(obj, results) {
      result = results.length ? results[0] : {};
      decodedAddress = mapHelpers.getAddressComponentsObject(result.address_components)
      obj.position = result.geometry.location;
      obj.address = decodedAddress;
      obj.address.formatted_address = result.address_components.formatted_address;
      obj.address.is_from_google_place = true;
      $scope.$applyAsync();
      return true;
    }

    function initBookingForm(fromLoc, toLoc, vclass) {
      $scope.selectCity(selected_city, true);
      if (fromLoc) {
        fromLoc = fromLoc.split(',');
      }
      if (toLoc) {
        toLoc = toLoc.split(',');
      }

      console.log('vclass', vclass)
      if (vclass) {
        var vehicleClass = $scope.vehicle_classes.find(
          item => item.id === vclass
        );

        if (!vehicleClass) {
          showMessagePopup(`Selected vehicle class not available for ${selected_city}. Please select an alternative vehicle`)
        } else {
          $scope.selectVehicleClass(vehicleClass)
        }
      }

      var pickupLocation = null;
      var dropLocation = null;
      $scope.$watch('bookingObject.pickup.marker', function (marker) {
        if (marker) {
          pickupLocation = mapHelpers.getGoogleLatLngObject(
            parseFloat(fromLoc[0]),
            parseFloat(fromLoc[1])
          );
          $scope.bookingObject.pickup.position = pickupLocation;
          marker.setPosition(pickupLocation);
          $scope.$applyAsync();

          mapHelpers.geocode(pickupLocation).then((data) => {
            updateAddress($scope.bookingObject.pickup, data);
            $scope.$applyAsync();
          });
        }
      });
      $scope.$watch('bookingObject.dropoff.marker', function (marker) {
        if (marker) {
          dropLocation = mapHelpers.getGoogleLatLngObject(
            parseFloat(toLoc[0]),
            parseFloat(toLoc[1])
          );

          $scope.bookingObject.dropoff.position = dropLocation;
          marker.setPosition(dropLocation);
          $scope.showHomeLoader = false;
          $scope.$applyAsync();
          $scope.openBookingForm();
          mapHelpers.geocode(dropLocation).then((data) => {
            updateAddress($scope.bookingObject.dropoff, data);
          });
        }
      });
      $scope.show.ghost = false;
      setTimeout(function () {
        $scope.calcRoute();
      }, 5000);
      $scope.$applyAsync();
    }

    angular.element(document).ready(function () {
      $scope.jQueryInit();
      if (rebook_data) {
        initRebook();
      } else {
        $scope.showHomeLoader = true;
        var vclass = getParameterByName('vclass');
        var fromLoc = getParameterByName('fromLoc');
        var toLoc = getParameterByName('toLoc');
        initBookingForm(fromLoc, toLoc, vclass);
      }
      loadScript("https://wchat.freshchat.com/js/widget.js", initChatWidget);
    });

    $scope.scrollTop = function () {
      $('html,body').animate({
          scrollTop: 0
        },
        'slow'
      );
    };

    $scope.set_view_mode = mode => {
      $scope.view_mode = mode;
    };

    $scope.solutionsCards = generalService.getSolutionPageContent();
    $scope.teamValues = generalService.getTeamValuesPageContent();
    $scope.faqBase = generalService.getFaqPageContent();
    $scope.faq = $scope.faqBase.c2c;

    $scope.setFaqTab = mode => {
      $scope.faq_tab = mode;
      $scope.faq = $scope.faqBase[mode];
      $('.faq-page .panel-collapse').removeClass('in');
      $('.faq-page .panel-heading').find(".more-less")
        .removeClass('glyphicon-minus')
        .addClass('glyphicon-plus');
    };

    function toggleIcon(e) {
      $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.faq-page .panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.faq-page .panel-group').on('shown.bs.collapse', toggleIcon);

    $scope.$on('$destroy', function (event) {
      $interval.cancel($scope.spinner);
    });

    function loadScript(url, callback) {
      var head = document.getElementsByTagName('head')[0];
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = url;
      if (callback) {
        script.onreadystatechange = callback;
        script.onload = callback;
      }
      head.appendChild(script);
    }

    function initChatWidget() {
      if ($scope.isDefaultBuild) {
        document.getElementById('chat-icon-container').style.display = 'block';
        document.getElementById('chat-intro-container').style.display = 'block';
        window.fcWidget.on("widget:opened", function (resp) {
          $('.chat-icon').toggle("scale");
          $('.close-chat').toggle("scale");
        });

        window.fcWidget.on("widget:closed", function (resp) {
          $('.chat-icon').toggle("scale");
          $('.close-chat').toggle("scale");
        });
      }
      window.fcWidget.init({
        token: freshChatData.api_key,
        host: "https://wchat.freshchat.com",
        config: {
          headerProperty: {
            appName: freshChatData.app_name,
            appLogo: freshChatData.app_logo,
            hideChatButton: $scope.isDefaultBuild,
            backgroundColor: freshChatData.background_color,
            foregroundColor: freshChatData.foreground_color,
          },
          cssNames: {
            widget: 'fc_frame',
          }
        },
      });
    }

    $scope.openPaymentPage = function () {
      if (!$scope.userProfile) {
        return;
      }
      if ($scope.userProfile.pending_amount <= 0) {
        window.location.href = '/dashboard';
        return;
      }
      let d = new Date();
      let rString = d.getSeconds().toString() + d.getMilliseconds().toString();
      window.location.href = "/payment/" + $scope.userProfile.cid + 'PB' + rString + '/web';
    };

    $scope.openDocsPage = function () {
      window.open('http://api.blowhorn.com', '_self');
    };

    $scope.openDashboard = function (screen) {
      if(!$scope.userProfile) {
        window.open('/', '_self');
        return;
      }
      let nextPage = blowhornService.nextPageUrl(
        $scope.userProfile.is_staff,
        $scope.userProfile.is_business_user
      );
      window.open(nextPage, '_self');
    }

    $scope.openMyAccount = function () {
      window.open('/myaccount', '_self');
    }

    $scope.openOriginalNews = function (link) {
      window.open(link, '_blank');
    };

    $scope.shareNews = function (platform, item) {
      generalService.shareOnMedia(
        platform, item.heading, item.article_link
      );
    };

    if ($scope.city_list && $scope.city_list.length === 1) {
      $scope.selectCity($scope.defaultCity, true);
    }

    $scope.showFareInfo = function () {
      $scope.selectedRateCard = $scope.more_of_vehicle.packages
        .find(mv => mv.contract_id === $scope.more_of_vehicle.selected_contract_id)
      $scope.selectedRateCardInfo = $scope.selectedRateCard.description;
      $scope.show.fare_info = !$scope.show.fare_info;
    };

    $scope.showTipInfo = function () {
      $scope.show.tip_info = !$scope.show.tip_info;
    }

    $scope.selectTip = function (val) {
      $scope.bookingObject.tipForPartner = val;
    }

    $scope.logout = function () {
      window.open('/accounts/logout/', '_self');
    }

    $scope.openDonationPage = function () {
      window.open('/relief', '_self');
    }
});
