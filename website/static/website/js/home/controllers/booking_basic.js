var s;
var logging = false;
if (!logging) {
  console.log = function () {};
}

angular
  .module('blowhornApp')
  .controller('bookingBaiscFormController', function (
    $scope,
    $timeout,
    $interval,
    $location,
    blowhornService,
    generalService,
    mapHelpers,
    deviceDetector,
    ctrlOptions,
  ) {
    // Scroll to top of page first
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    $scope.page = ctrlOptions.page;
    if (logging) {
      s = $scope;
    }
    $scope.deviceDetector = deviceDetector;
    const CHAT_HELP_SHOWN_FLAG = 'chatHelpDisplayed';
    const MESSAGE_OUT_OF_COVERAGE = 'Out of Coverage';
    const MESSAGE_REBOOK_OUT_OF_COVERAGE = 'We have updated our service area. Service is not available in requested location.';
    $scope.isInEmbed = isEmbed;
    $scope.bg_url = bg_url;
    $scope.introStyles = {};
    if (bg_url) {
      $scope.introStyles = {
        background: `url(${bg_url}) no-repeat center center fixed`,
        'background-size': 'cover'
      }
    }

    $scope.pickupOvalIcon = `/static/website/images/${current_build}/homepage/Pickup_oval.png`;
    $scope.dropoffOvalIcon = `/static/website/images/${current_build}/homepage/Dropoff_oval.png`;
    $scope.view_mode = 'ondemand';
    $scope.enterprise = [];
    $scope.getAppContent = {};
    $scope.enterprise_send = false;
    $scope.solutions_send = false;
    var currentCityIndex = 0;
    var currentWeDoItAllIndex = 0;
    var weDoItAllList = [
      'Line haul delivery',
      'Spot delivery',
      'Fixed',
      'Milk run',
      'Enterprise'
    ];
    $scope.weDoItAll = weDoItAllList[0];
    if (!angular.isDefined($scope.spinner)) {
      $scope.spinner = $interval(() => {
        var cityElement = angular.element(document.querySelector('#cityName'));
        var weDoItAllElement = angular.element(
          document.querySelector('#weDoItAll')
        );
        if (cityElement) {
          currentCityIndex++;
          if (currentCityIndex >= $scope.city_list.length) {
            currentCityIndex = 0;
          }
          $scope.cityName = $scope.city_list[currentCityIndex];
          // cityElement.removeClass('reset');
          // cityElement.addClass('spin');
        }
        if (weDoItAllElement) {
          currentWeDoItAllIndex++;
          if (currentWeDoItAllIndex >= weDoItAllList.length) {
            currentWeDoItAllIndex = 0;
          }
          $scope.weDoItAll = weDoItAllList[currentWeDoItAllIndex];
          weDoItAllElement.removeClass('reset');
          weDoItAllElement.addClass('spin');
        }
        var timeout = $timeout(() => {
          if (cityElement) {
            cityElement.removeClass('spin');
            cityElement.addClass('reset');
          }
          if (weDoItAllElement) {
            weDoItAllElement.addClass('reset');
            weDoItAllElement.removeClass('spin');
          }
        }, 1200);
      }, 2000);
    }
    var display_chat_help = true;
    $scope.no_scroll = false;
    $scope.loaded = false;
    $scope.regex = {
      name: /^\[a-zA-Z .]+$/,
      email: /[a-zA-Z0-9_\.]+@[a-zA-Z0-9_\.]+\.[a-zA-Z0-9]+/,
      integer: /^\d+$/,
      mobileNumber: /^[789]\d{9}$/,
      altPhone: /[0-9]{8,10}/,
      otp: /^\d{4,}$/,
      coupon: /^[a-zA-Z0-9]{2,}$/,
      latlng: /([+-]?\d+\.\d+)(\s*,\s*|\s+)([+-]?\d+\.\d+)/,
      w3wPattern: /^[a-zA-Z]*\.[a-zA-Z]*\.[a-zA-Z]*$/,
      gstin: /^\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}$/
    };
    $scope.jobs = [];
    $scope.coverage_areas = {};
    var coverage_area_coordinates = null;
    var dropoff = {};
    $scope.polygons = {};
    $scope.bounds = {};

    $scope.intRegex = /^\d+$/;
    $scope.phoneregEx = /^\d{10}$/;
    $scope.mobileNumberRegex = /^[789]\d{9}$/;
    $scope.otpregEx = /^\d{4,}$/;
    $scope.couponregEx = /^[a-zA-Z0-9]{2,}$/;
    $scope.alertMessage = '';
    $scope.isDisabled = false;
    $scope.outOfCoverage = false;
    $scope.formsubmitted = false;
    $scope.selected_vehicle_class = null;
    $scope.selected_time = null;
    $scope.selected_day = null;
    $scope.selected_rate_text = '';
    $scope.podCost = 0;
    $scope.isPodApplicable = false;
    $scope.timeslots = [];
    $scope.moreItems = [];
    $scope.time = {};
    $scope.instant_init = true;

    $scope.signupDetails = {};
    $scope.timeFrame = null;
    $scope.disableCoupon = false;
    $scope.disableBooking = true;
    $scope.discount_message = '';
    $scope.discount_value = 0;
    $scope.invalidCoupon = '';
    $scope.saved_locations = [];
    $scope.userProfile = null;
    $scope.items = [];
    $scope.bookingComplete = false;
    $scope.saveBtnText = 'Save';
    $scope.mdate = '';
    $scope.otp = {
      email: '',
      mobile: '',
      enableResend: false,
      activationNeeded: false,
      activationEmailSent: false,
      activationOtpSent: false
    };

    $scope.infobox_open = 'Pickup';
    $scope.show = {
      booking_form: false,
      Mobile: true,
      download_app: true,
      save_location_option: false,
      save_address: false,
      otp_invalid: false,
      add_edit_contact: false,
      ghost: true,
      help: true,
      save_route: false,
      login_tooltip: true,
      where_section: false,
      when_section: false,
      what_section: false,
      vehicle_section: false,
      labour_coupon_section: false,
      booking_summary: false,
      coupon_success: false,
      exit_screen: false,
      vehicle_info: false,
      booking_otp: false,
      CouponInvalid: false,
      fareEstimationMessage: false,
      coupon_terms_and_conditions: false,
      loadingFare: false,
      no_news: false,
      loading_news: true,
    };
    $scope.more_of_vehicle = {};
    $scope.booking_error_msg = '';
    $scope.section = {
      where_done: false,
      when_done: false,
      what_done: false,
      vehicle_done: false,
      confirm_done: false
    };

    $scope.current_section = '';
    $scope.continue_is_enabled = false;
    $scope.next_section = 'where';
    $scope.selectedCity = null;
    $scope.selectedCityBounds = {
      pickup: initLatLngBounds(),
      dropoff: initLatLngBounds(),
    }
    $scope.selectedCityPolygon = null;
    const ICON_PICKUP_MARKER = '/static/website/images/Blue-Marker@2x.png';
    const ICON_DROPOFF_MARKER = '/static/website/images/Purple-Marker@2x.png';
    $scope.bookingObject = generalService.getBookingObject(
      ICON_PICKUP_MARKER,
      ICON_DROPOFF_MARKER
    );
    $scope.saveRouteObject = {};
    $scope.total_distance_meters = null;
    $scope.total_time_seconds = null;

    if (generalService.storageAvailable('localStorage')) {
      display_chat_help = !generalService.getValueFromLocalStorage(CHAT_HELP_SHOWN_FLAG);
    //   $scope.enterprise = generalService.getEnterpriseContent();
    //   $scope.getAppContent = generalService.getAppContent();
    } else {
      console.log('Local storage not available...!');
    }

    // coverage_area_coordinates = global_params.city_coverage;
    var contract_cities = global_params.contract_cities;
    $scope.vehicle_dict = {};
    $scope.offerBanner = angular.copy(global_params.offerbanner);
    // $scope.city_coverage = {}//global_params.city_coverage;
    $scope.city_list = contract_cities.sort();
    $scope.cityName = $scope.city_list[0];
    $scope.defaultCity = $scope.city_list ? $scope.city_list[0] : '';
    $('#alert_message').hide();
    googleLoaded = true;

    // for (var city in coverage_area_coordinates) {
    //   var coverages = coverage_area_coordinates[city];
      // $scope.coverage_areas[city] = mapHelpers.convertCoverageToGoogleObjects(coverages);
    // }

    $scope.fetchCoverageArea = (city) => {
      $scope.coverage_areas = {
        pickup: [],
        dropoff: []
      };
      blowhornService.getCoverageArea(city).then((data) => {
        $scope.coverage_areas = mapHelpers.convertCoverageToGoogleObjects(data);
        drawPolygon();
        $scope.$applyAsync();
      }).catch(function (data) {
        $scope.coverage_areas = {};
      });
    }

    function drawPolygon() {
      pickup = mapHelpers.createPolygon({
        paths: $scope.coverage_areas['pickup'],
        visible: false
      });
      dropoff = mapHelpers.createPolygon({
        paths: $scope.coverage_areas['dropoff'],
        visible: false
      });
      $scope.polygons = {
        pickup: pickup,
        dropoff: dropoff
      };
      $scope.bounds = {
        pickup: initLatLngBounds(),
        dropoff: initLatLngBounds(),
      }
      setBoundForCoverage('pickup');
      setBoundForCoverage('dropoff');
      $scope.selectedCityPolygon = {
        pickup: $scope.polygons['pickup'],
        dropoff: $scope.polygons['dropoff'],
      }
    }

    function setBoundForCoverage(key) {
      $scope.coverage_areas[key].map((_location) => {
        $scope.bounds[key].extend(_location);
      });
      $scope.selectedCityBounds[key] = $scope.bounds[key];
      $scope.$applyAsync();
    }

    $scope.proceedToBookingScreen = function () {
      if (
        $scope.bookingObject.pickup.address.full_address != '' &&
        $scope.bookingObject.dropoff.address.full_address != '' &&
        $scope.bookingObject.pickup.address.is_from_google_place &&
        $scope.bookingObject.dropoff.address.is_from_google_place
      ) {
        // redirect to main booking form
        var pickupLoction = $scope.bookingObject.pickup.position;
        var dropoffLoction = $scope.bookingObject.dropoff.position;
        var fromLoc = `${pickupLoction.lat()},${pickupLoction.lng()}`;
        var toLoc = `${dropoffLoction.lat()},${dropoffLoction.lng()}`;
        top.window.location.href = `//${host}?embed=true&type=bookingform&city=${$scope.selectedCity}&fromLoc=${fromLoc}&toLoc=${toLoc}`;
      }
    };

    $scope.selectCity = function (city, noreload) {
      let url = '/';
      if (city) {
        url = `${'/city/'}${city.replace(' ', '-')}`;
      }
      $location.path(url, noreload);
      if ($scope.selectedCity != city) {
        $scope.bookingObject.pickup.address.full_address = '';
        $scope.bookingObject.dropoff.address.full_address = '';
        // $scope.resetPickupDropoffWaypoints();
      }
      $scope.selectedCity = city;
      selected_city = city;
      /* Display only selected city vehicles mapped in c2c contract */
      $scope.vehicle_classes = $scope.vehicle_dict[$scope.selectedCity];
      $scope.$applyAsync();
      // Adding a delay of 100 ms since 'instant-from' is disabled initially and
      // and enabled only after city is selected
      $timeout(function () {
        let ref = document.getElementById('instant-from');
        if (ref) {
          ref.focus();
        }
      }, 100);
    };

    $scope.$watch('selectedCity', function (value) {
      console.log('selectedCity changed. Setting City Bounds', value);
      if (value == null) {
        return;
      }
      $scope.fetchCoverageArea(value);
    });

    $scope.isCitySelected = function (city) {
      return $scope.selectedCity === city;
    };

    if (selected_city && $location.path() === '/') {
      $scope.selectCity(selected_city, true);
    }

    $scope.scrollTop = function () {
      $('html,body').animate({
          scrollTop: 0
        },
        'slow'
      );
    };

    $scope.set_view_mode = mode => {
      $scope.view_mode = mode;
    };

    $scope.$on('$destroy', function (event) {
      $interval.cancel($scope.spinner);
    });
});
