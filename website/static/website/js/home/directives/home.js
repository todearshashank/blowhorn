angular.module("blowhornApp").directive('googleMap', function () {
    return {
        restrict: 'A',
        scope: {
            map: '=map',
        },
        link: function (scope, element, attrs, model) {
            console.log('Initializing Google map', scope.map);
            //console.log(scope);
            var options = {
                zoom: 11,
                clickableIcons: false,
                gestureHandling: "greedy",
                center: getGoogleLatLngObject(0, 0),
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_CENTER
                },
                mapTypeControl: false,
                fullscreenControl: false,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                }
            };
            scope.map = initMap(element[0], options);
            //console.log('Google map', scope.map);
            scope.$applyAsync();
        }
    };
});

angular.module("blowhornApp").directive('infobox', function () {
    return {
        restrict: 'E',
        scope: {
            data: '=data',
            showSaveLocationOption: '=showSaveLocationOption',
            //openSaveAddress: '=openSaveAddress',
            //openAddEditContact: '=openAddEditContact',
        },
        link: function (scope) {
            //console.log(scope);
            scope.openSaveAddress = scope.$parent.$parent.openSaveAddress;
            scope.openAddEditContact = scope.$parent.$parent.openAddEditContact;
            /*
            scope.openSaveAddress1 = function(data) {
                console.log(data);
                scope.openSaveAddress()(data);
            };
            scope.openAddEditContact1 = function(data) {
                console.log(data);
                scope.openAddEditContact()(data);
            };
            console.log(scope);
            */
        },
        templateUrl: 'static/website/html/home/infobox.html'
    };
});

angular.module("blowhornApp").directive('ghostInfobox', function () {
    return {
        restrict: 'E',
        scope: {
            data: '=data',
            setThisLocation: '=setThisLocation',
        },
        link: function (scope) {
            //console.log(scope);
        },
        templateUrl: 'static/website/html/home/ghost_infobox.html'
    };
});

angular.module("blowhornApp").directive('ghostHoverInfobox', function () {
    return {
        restrict: 'E',
        scope: {
            data: '=data',
        },
        link: function (scope) {
            //console.log(scope);
        },
        templateUrl: 'static/website/html/home/ghost_hover_infobox.html'
    };
});

angular.module("blowhornApp").directive('availableCoupons', function () {
    return {
        restrict: 'E',
        scope: {
            coupons: '=coupons',
            appliedCoupon: '=appliedCoupon',
            selectCoupon: '&',
            showMore: '&'
        },
        link: function (scope) {},
        templateUrl: 'static/website/html/home/available_coupons.html'
    };
});

angular.module("blowhornApp").directive("googlePlaces", ['mapHelpers', function (helpers) {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            data: '=data',
            showSaveLocationOption: '=showSaveLocationOption',
            openSaveAddress: '&openSaveAddress',
            openAddEditContact: '&openAddEditContact',
            calcRoute: '&calcRoute',
            address: '=address',
            marker: '=marker',
            position: '=position',
            selectedCity: '=selectedCity',
            selectedCityBounds: '=selectedCityBounds',
            selectedCityPolygon: '=selectedCityPolygon',
            map: '=map',
            icon: '=icon',
            title: '=title',
            validate_where: '=validateWhere',
        },
        link: function (scope, element, attrs, model) {
            var parentScope = scope.$parent;
            var placeautocomplete = null;
            scope.$watch('map', function (value) {
                console.log('map', value);
                if (value != null) {
                    init();
                }
            });
            scope.$watch('position', function (value) {
                console.log('position', value);
                return;
                if (value != null && scope.marker != null) {
                    scope.marker.setPosition(value);
                }
            });
            scope.$watch('title', function (value) {
                console.log('title changed', scope.title);
                if (scope.marker != null) {
                    scope.marker.infobox.setContent(helpers.getInfoboxContent(scope));
                }
            });
            scope.$watch('icon.url', function (value) {
                console.log('icon changed', scope.icon, scope.marker);
                if (scope.marker != null) {
                    scope.marker.setIcon(scope.icon);
                }
            });

            scope.$watch('address.full_address', function (value) {
                console.log('scope.adderss.full_address', value);
                scope.validate_where();
            });

            function init() {
                //console.log(parentScope);
                //console.log(attrs);
                //console.log(scope);

                var address = scope.data.address;
                var position = scope.position;
                var icon = scope.data.icon;
                var markerOptions = {
                    draggable: true,
                    optimized: false,
                    zIndex: 1001,
                    position: position || getGoogleLatLngObject(0, 0),
                    icon: icon
                };
                var marker = initMarker(scope.map, markerOptions);
                scope.data.marker = marker;
                //console.log(scope);
                infobox = helpers.createInfobox(scope, scope.map, marker, scope.title, scope.data);
                marker.addListener('click', function () {
                    this.infobox.openIfClosed();
                });

                var position_at_dragstart;
                var position_changed_listener;

                function add_position_changed_listener() {
                    position_changed_listener = marker.addListener('position_changed', function () {
                        console.log('Marker Event Triggered : position_changed');
                        if (google.maps.geometry.poly.containsLocation(this.getPosition(), scope.selectedCityPolygon)) {
                            scope.calcRoute();
                            //new google.maps.event.trigger(this, 'dblclick' );
                        } else {
                            console.log('Out of Coverage');
                            scope.address.formatted_address = 'Out of Coverage';
                            var marker = this;
                            if (!position_at_dragstart) {
                                return;
                            }
                            setTimeout(function () {
                                marker.setPosition(position_at_dragstart);
                            }, 1000);
                        }
                    });
                    console.log('Added position changed listener', position_changed_listener);
                }

                add_position_changed_listener();
                marker.addListener('dragstart', function () {
                    console.log('Marker Event Triggered : dragstart');
                    // Have to remove it as position changed listener gets fired repeatedly
                    // all along as the marker is being dragged
                    position_at_dragstart = this.getPosition();
                    console.log(position_at_dragstart.toString());
                    google.maps.event.removeListener(position_changed_listener);
                });
                marker.addListener('dragend', function () {
                    console.log('Marker Event Triggered : dragend');
                    if (google.maps.geometry.poly.containsLocation(this.getPosition(), scope.selectedCityPolygon)) {
                        scope.calcRoute();
                        new google.maps.event.trigger(this, 'dblclick');
                        add_position_changed_listener();
                    } else {
                        console.log('Out of Coverage');
                        scope.address.formatted_address = 'Out of Coverage';
                        if (!position_at_dragstart) {
                            return;
                        }
                        var marker = this;
                        setTimeout(function () {
                            marker.setPosition(position_at_dragstart);
                            add_position_changed_listener();
                        }, 1000);
                    }
                    // Add the removed marker back and trigger position changed when we actually
                    // want it to get fired
                    //google.maps.event.trigger(this, 'position_changed');
                });
                marker.addListener('dblclick', function () {
                    console.log('Marker Event Triggered : dblclick');
                    helpers.geocode(this.getPosition()).then(function (results) {
                        console.log(results);
                        var result = results[0];
                        var components = helpers.getAddressComponentsObject(result.address_components);
                        scope.address = components;
                        scope.address.formatted_address = result.formatted_address;
                        marker.infobox.setContent(helpers.getInfoboxContent(scope));
                        marker.infobox.openIfClosed();
                    }).catch(function (status) {
                        console.log('Geocoding Failed', status);
                    });
                });

                var ac_options = {
                    componentRestrictions: {
                        country: country_code
                    }
                };
                placeautocomplete = new google.maps.places.Autocomplete(element[0], ac_options);
                placeautocomplete.addListener("place_changed", function () {
                    var place = this.getPlace();
                    console.log(place);
                    if (!place.geometry) {
                        return;
                    }

                    if (!google.maps.geometry.poly.containsLocation(place.geometry.location, scope.selectedCityPolygon)) {
                        //console.log('Out of coverage');
                        element[0].placeholder = 'Out of Coverage';
                        scope.address.full_address = "";
                        scope.$applyAsync();
                        return;
                    }

                    var location_ = place.geometry.location;
                    scope.address = helpers.getAddressComponentsObject(place.address_components);
                    scope.address.formatted_address = place.formatted_address;
                    marker.infobox.setContent(helpers.getInfoboxContent(scope));
                    marker.setPosition(location_);
                    scope.map.panTo(location_);
                    scope.map.setZoom(16);
                });

                scope.$watch('selectedCity', function (value) {
                    console.log('selectedCity changed. googlePlaces', value, scope.selectedCityBounds.toString());
                    if (value == null) {
                        return;
                    }
                    placeautocomplete.setBounds(scope.selectedCityBounds);
                    /*
                    scope.position = scope.selectedCityBounds.getCenter();
                    scope.address = helpers.getAddressComponentsObject([]);
                    scope.address.formatted_address = '';
                    */
                }, true);

                element.on('keydown', function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    //marker.address_manual = true;
                });
                element.on('blur', function (event) {
                    event.stopPropagation();
                    //marker.content = address.full_address;
                });

                google.maps.event.trigger(this, 'position_changed');
            }
        }
    }
}]);

angular.module("blowhornApp").directive("googlePlacesSimple", ['mapHelpers', function (helpers) {
    return {
        require: 'ngModel',
        restrict: 'A',
        scope: {
            address: '=address',
            position: '=position',
            selectedCity: '=selectedCity',
            selectedCityBounds: '=selectedCityBounds',
            selectedCityPolygon: '=selectedCityPolygon',
            marker: '=marker',
            next: '@next',
        },
        link: function (scope, element, attrs, model) {
            var parentScope = scope.$parent;
            var address = scope.address;
            var position = scope.position;
            var ac_options = {
                geocode: true,
                componentRestrictions: {
                    country: country_code
                }
            };
            var placeautocomplete = new google.maps.places.Autocomplete(element[0], ac_options);
            placeautocomplete.addListener("place_changed", function () {
                var place = this.getPlace();
                //console.log(place);
                if (!place.geometry) {
                    return;
                }
                console.log(google.maps.geometry.poly.containsLocation, place.geometry, scope.selectedCityPolygon);
                if (!google.maps.geometry.poly.containsLocation(place.geometry.location, scope.selectedCityPolygon)) {
                    console.log('Out of coverage');
                    element[0].placeholder = 'Out of Coverage';
                    scope.address.full_address = '';
                    scope.$applyAsync();
                    return;
                }

                scope.position = place.geometry.location;
                scope.address = helpers.getAddressComponentsObject(place.address_components);
                scope.address.formatted_address = place.formatted_address;
                scope.address.country = helpers.getPlaceComponents(place.address_components).country;
                scope.address.is_from_google_place = true;
                scope.address.country = place.address_components[place.address_components.length - 1].short_name;
                scope.$apply();
                var n = document.getElementById(attrs.next);
                n.focus();
            });

            scope.$watch('selectedCity', function (value) {
                console.log('selectedCity changed. googlePlacesSimple', value, scope.selectedCityBounds.toString());
                scope.address.is_from_google_place = false;
                scope.address.error = "";
                element.removeClass('input-zero-margin');
                if (value == null) {
                    return;
                }
                placeautocomplete.setBounds(scope.selectedCityBounds);
                scope.position = scope.selectedCityBounds.getCenter();
                /*
                scope.address = helpers.getAddressComponentsObject([]);
                scope.address.formatted_address = '';
                */
            }, true);

            element.on('keydown', function (event) {
                // console.log('Keydown', event);
                scope.address.error = "";
                element.removeClass('input-zero-margin');
                if (event.keyCode === 13) {
                    event.preventDefault();
                    event.stopPropagation();
                } else if (event.keyCode !== 9 && // TAB
                    event.keyCode !== 16 && // SHIFT
                    event.keyCode !== 17 && // CTRL
                    event.keyCode !== 18 // ALT
                ) {
                    scope.address.is_from_google_place = false;
                }
                scope.$applyAsync();
            });
            element.on('blur', function (event) {
                //console.log('blur', event.keyCode);
                event.stopPropagation();
            });

            $("#book").on('click', function () {
                if (scope.selectedCity && !scope.address.is_from_google_place) {
                    element.addClass('input-zero-margin');
                    scope.address.error = "Please choose from autocomplete dropdown";
                    scope.$applyAsync();
                }
            });
        }
    }
}]);

angular.module("blowhornApp").directive("phoneNumber", [function () {
    return {
        restrict: "EA",
        link: function (scope, elem, attrs) {
            var keyDown = false,
                ctrl = 17,
                vKey = 86,
                Vkey = 118,
                vPlus = 187;
            $(document).keydown(function (e) {
                if (e.keyCode === ctrl || e.keyCode === vPlus) {
                    keyDown = true;
                }
            }).keyup(function (e) {
                if (e.keyCode === ctrl) {
                    keyDown = false;
                }
            });

            angular.element(elem).on("keypress", function (e) {
                if (!e) {
                    var e = window.event;
                }
                if (e.keyCode > 0 && e.which === 0) {
                    return true;
                }
                if (e.keyCode) {
                    code = e.keyCode;
                } else if (e.which) {
                    code = e.which;
                }
                var character = String.fromCharCode(code);
                if (character === '\b' || character === ' ' || character === '\t') {
                    return true;
                }
                if (keyDown) {
                    if (code === vKey || code === Vkey) {
                        return character;
                    } else if (code === vPlus) {
                        return false;
                    }
                } else {
                    return (/[0-9]$/.test(character));
                }
            }).on('focusout', function (e) {
                var $this = $(this);
                $this.val($this.val().replace(/[^0-9]/g, ''));
            }).on('paste', function (e) {
                var $this = $(this);
                setTimeout(function () {
                    $this.val($this.val().replace(/[^0-9]/g, ''));
                }, 5);
            });
        }
    }
}]);

angular.module("blowhornApp").directive("timePicker", function () {
    return {
        restrict: "A",
        scope: {
            selectTimeSlot: '=selectTimeSlot',
            timeslots: '=timeslots',
            minuteInfo: '=minuteInfo'
        },
        link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datetimepicker({
                useCurrent: false,
                format: 'HH:mm',
                keepOpen: false,
                ignoreReadonly: true,
                showClose: true,
                stepping: 5,
                icons: {
                    close: 'glyphicon glyphicon-ok-circle'
                },
                tooltips: {
                    close: 'Ok'
                }
            }).on('dp.change', function (e) {
                scope.selectTimeSlot(e.date);
            }).on('dp.show', function (e) {
                if (scope.timeslots.length) {
                    $(element).data('DateTimePicker').defaultDate(scope.timeslots[0]);
                }
            });
            scope.$watch('timeslots', function (newVal) {
                $(element).data('DateTimePicker').enabledHours(newVal);
                if (newVal && newVal.length) {
                    var disabledTimeIntervals = [
                        [moment().hours(newVal[0]).minutes(0),
                            moment().hours(newVal[0]).minutes(scope.minuteInfo.start)
                        ],
                        [moment().hours(newVal[newVal.length - 1]).minutes(scope.minuteInfo.end),
                            moment().hours(newVal[newVal.length - 1]).minutes(59)
                        ]
                    ];
                    $(element).data('DateTimePicker').disabledTimeIntervals(disabledTimeIntervals);
                }
            });
            angular.element(document).click(function (e) {
                var elem = $(e.target);
                if (!elem.hasClass('btn-time') && !elem.hasClass('glyphicon-time') && $(element).data("DateTimePicker")) {
                    $(element).data("DateTimePicker").hide();
                }
            });
        }
    }
});

angular.module("blowhornApp").directive('bindHtmlCompile', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.bindHtmlCompile);
            }, function (value) {
                element.html(value && value.toString());
                var compileScope = scope;
                if (attrs.bindHtmlScope) {
                    compileScope = scope.$eval(attrs.bindHtmlScope);
                }
                $compile(element.contents())(compileScope);
            });
        }
    };
}]);

angular
    .module('blowhornApp')
    .directive('lazyLoad', lazyLoad)

function lazyLoad() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            const observer = new IntersectionObserver(loadImg)
            const img = angular.element(element)[0];
            observer.observe(img);
            function loadImg(changes) {
                changes.forEach(change => {
                    if(change.intersectionRatio > 0){
                        change.target.src = attrs.lazySrc;
                        change.target.classList.remove('img-blur');
                    }
                });
            }
        }
    }
}

angular.module("blowhornApp").directive('countryCode', countryCode);
function countryCode() {
    return {
        restrict: 'AE',
        scope: {
          countryCodeA2: '=',
          isdDialingCode: '='
        },
        link: function (scope) {
            console.log('scope.isdDialingCode', scope);
        },
        templateUrl: '/static/website/sections/components/country_code.html'
    };
}

angular
    .module('blowhornApp')
    .directive('lazyBackground', ['$document', lazyBackground]);

function lazyBackground($document) {
    return {
        restrict: 'A',
        link: function(scope, iElement, iAttrs) {
            var loader = angular.element('<div>...</div>');
            if (angular.isDefined(iAttrs.lazyLoader)) {
                loader = angular.element($document[0].querySelector(iAttrs.lazyLoader)).clone();
            }
            iElement.append(loader);
            var src = `${iAttrs.lazyBackgroun}?r=${Math.random()}`;
            var img = document.createElement('img');
            img.onload = function() {
                loader.remove();
                if (angular.isDefined(iAttrs.lazyClass)) {
                    iElement.addClass(iAttrs.lazyClass);
                }
                iElement.css({
                    'background-image': `url(${this.src})`
                });
                delete this;
            };
            img.onerror= function() {
                console.log('error');
            };
            img.src = src;
        }
    }
}

angular.module("blowhornApp").directive("positiveNumberOnly", function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text && text.length <= 3) {
                    var transformedInput = text.replace(/[^0-9]/g, '');
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return parseInt(transformedInput);
                }
                return null;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});


angular.module("blowhornApp").directive("bookingForm", function () {
    return {
        restrict: "E",
        templateUrl: "/static/website/html/home/booking.html"
    };
});

angular.module("blowhornApp").directive("blowhornHeader", function () {
    return {
        restrict: "E",
        templateUrl: "/static/website/html/home/header.html"
    };
});

angular.module("blowhornApp").directive("blowhornFooter", function () {
    return {
        restrict: "E",
        templateUrl: "/static/website/html/home/footer.html",
    };
});

angular.module("blowhornApp").directive("offeringOndemand", function () {
    return {
        restrict: "E",
        templateUrl: "/static/website/sections/offerings/ondemand.html"
    };
});

angular.module("blowhornApp").directive("offeringEnterprise", function () {
    return {
        restrict: "E",
        templateUrl: "/static/website/sections/offerings/enterprise.html"
    };
});

angular.module("blowhornApp").directive("blowhornBanner", function () {
    return {
        restrict: "E",
        templateUrl: "/static/website/html/home/banner.html"
    };
});
