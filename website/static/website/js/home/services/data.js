angular.module('blowhornApp').factory('dataService', function ($compile, $http, $q) {
    var factory = {};
    factory.getDataFromJson = function(build, filepath) {
        var defer = $q.defer();
        $http.get(`/static/website/js/content/${build}/${filepath}.json`)
            .success(function(data) {
                defer.resolve(data);
            }).error(function() {
                defer.reject('could not find json');
            });
        return defer.promise;
    }
    
    return factory;
});
