angular.module('blowhornApp').factory('mapHelpers', function ($compile, $http, $q) {
    var factory = {};
    var gmap_addr = {
        street_number: 'short_name',
        premise: 'short_name',
        subpremise: 'short_name',
        route: 'short_name',
        neighborhood: 'short_name', //Area1.1.1.1
        sublocality_level_3: 'long_name', // Area1.1.1
        sublocality_level_2: 'long_name', // Area1.1
        sublocality_level_1: 'long_name', // Area1
        sublocality: 'long_name', // General Area
        locality: 'long_name', // City
        postal_code: 'short_name', // Pin code
        administrative_area_level_1: 'long_name', // State
        country: 'short_name',// Country
    };

    var geocoder = initGeocoder();

    factory.geocode = function (position) {
        var defer = $q.defer();
        geocoder.geocode({'latLng': position}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                defer.resolve(results);
            } else {
                defer.reject(status);
            }
        });
        return defer.promise;
    };

    function getComponents(address_components) {
        //console.log(address_components);
        return_data = {};

        //Initialize the components to empty string
        for (var k in gmap_addr) {
            return_data[k] = "";
        }
        for (var i = 0; i < address_components.length; i++) {
            var addressType;
            // If types[] array in the address_components[] has "political" as its first element then assign
            // "sublocality_level"  Reference: https://developers.google.com/maps/documentation/geocoding/intro#Types
            if (address_components[i].types[0] == 'political') {
                addressType = address_components[i].types[2];
            } else {
                // Assign other component types like locality, country, postal_code
                addressType = address_components[i].types[0];
            }
            if (gmap_addr[addressType]) {
                var val = address_components[i][gmap_addr[addressType]];
                return_data[addressType] = val;
            }
        }
        //console.log(return_data);
        return return_data;
    }

    function getSubLocalityFromAddressComponents(components) {
        //console.log(components);
        if (components.sublocality_level_1 !== "") {
            return components.sublocality_level_1;
        } else if (components.sublocality_level_2 !== "") {
            return components.sublocality_level_2;
        } else if (components.sublocality_level_3 !== "") {
            return components.sublocality_level_3;
        }
        return components.sublocality;
    }

    function getCityFromAddressComponents(components) {
        return components.locality;
    }

    function getStateFromComponents(components) {
        return components.administrative_area_level_1;
    }

    function getPostalCodeFromAddressComponents(components) {
        return components.postal_code;
    }

    function getStreetAddressFromComponents(components) {
        var a = [
            components.street_number,
            components.premise,
            components.subpremise,
            components.route,
            components.neighborhood,
            components.sublocality_level_3,
            components.sublocality_level_2,
            //components.sublocality_level_1,
        ];
        var b = [];
        var sublocality = getSubLocalityFromAddressComponents(components);
        var s_min = sublocality.replace(/\s/g, '').toLowerCase();

        for (var i = 0; i < a.length; i++) {
            a_min = a[i].replace(/\s/g, '').toLowerCase();
            if (a_min === s_min) {
                continue;
            }
            if (b.join(' ').replace(/\s/g, '').toLowerCase().search(a_min) >= 0) {
                continue;
            }
            b.push(a[i]);
        }
        a = b.join(" ");
        a = a.replace(getSubLocalityFromAddressComponents(components), "");
        a = a.replace(/\s+/g, " ");
        a = a.trim();
        return a;
    }

    factory.getPlaceComponents = function (addressComponents) {
        return getComponents(addressComponents);
    }

    factory.getAddressComponentsObject = function (addressComponents) {
        var components = getComponents(addressComponents);
        var addressDetails = [
            getStreetAddressFromComponents(components),
            getSubLocalityFromAddressComponents(components),
            getCityFromAddressComponents(components),
            getPostalCodeFromAddressComponents(components)
        ];
        return {
            "street_address": addressDetails[0] || '',
            "area": addressDetails[1] || '',
            "city": addressDetails[2] || '',
            "postal_code": addressDetails[3] || '',
            "full_address": addressDetails.join(" ").replace(/\s+/g, " ").trim(),
            "google_places": true
        }
    };

    factory.getInfoboxContent = function (scope) {
        var content = '<infobox data="data" open-save-address="openSaveAddress" show-save-location-option="showSaveLocationOption" id="infowindow_content"></infobox>';
        var compiled = $compile(content)(scope);
        //console.log(compiled);
        //console.log(compiled[0]);
        return compiled[0];
    };

    factory.createInfobox = function (scope, map, marker, title, data) {
        //console.log(map, marker, data);
        var boxText = factory.getInfoboxContent(scope);
        var myOptions = {
            content: boxText,
            alignBottom: true,
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-116, -42),
            zIndex: null,
            boxStyle: {
                //background: "url('tipbox.gif') no-repeat",
                //padding: "10px 10px 10px 10px",
                //opacity: 0.75,
                width: "224px",
                height: "136px",
                "background-color": "#FFFFFF",
                "box-shadow" : "0 2px 4px 0 rgba(0,0,0,0.5)"
            },
            closeBoxMargin: "0px",
            closeBoxURL: "/static/website/images/Cancel.png",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false,
        };

        var ib = new InfoBox(myOptions);
        ib.openIfClosed = function() {
            if (this.getMap() == null) {
                this.open(map, marker);
            }
        };
        marker.infobox = ib;
        //ib.openIfClosed();
    };

    factory.getGhostInfoboxContent = function (scope, data) {
        var content = '<ghost-infobox data="' + data + '" set-this-location="setThisLocation"></ghost-infobox>';
        var compiled = $compile(content)(scope);
        //console.log(compiled);
        //console.log(compiled[0]);
        return compiled[0];
    };

    factory.createGhostInfobox = function (scope, map, marker, title, data) {
        //console.log(map, marker, data);
        var boxText = factory.getGhostInfoboxContent(scope, data);
        var myOptions = {
            content: boxText,
            alignBottom: true,
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-116, -42),
            zIndex: null,
            boxStyle: {
                //background: "url('tipbox.gif') no-repeat",
                //padding: "10px 10px 10px 10px",
                //opacity: 0.75,
                width: "224px",
                height: "136px",
                "background-color": "#FFFFFF",
                "box-shadow" : "0 2px 4px 0 rgba(0,0,0,0.5)"
            },
            //closeBoxMargin: "10px 2px 2px 2px",
            closeBoxMargin: "0px",
            closeBoxURL: "/static/website/images/Cancel.png",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false,
        };

        var ib = new InfoBox(myOptions);
        ib.openIfClosed = function() {
            if (this.getMap() == null) {
                this.open(map, marker);
            }
        };
        marker.infobox = ib;
        //ib.openIfClosed();
    };

    factory.getGhostHoverInfoboxContent = function (scope, data) {
        var content = '<ghost-hover-infobox data="' + data + '" ></ghost-hover-infobox>';
        var compiled = $compile(content)(scope);
        //console.log(compiled);
        //console.log(compiled[0]);
        return compiled[0];
    };

    factory.createGhostHoverInfobox = function (scope, map, marker, title, data) {
        //console.log(map, marker, data);
        var boxText = factory.getGhostHoverInfoboxContent(scope, data);
        var myOptions = {
            content: boxText,
            alignBottom: true,
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-116, -42),
            zIndex: null,
            boxStyle: {
                //background: "url('tipbox.gif') no-repeat",
                //padding: "10px 10px 10px 10px",
                //opacity: 0.75,
                width: "250px",
                height: "50px",
                "background-color": "#101010",
                "box-shadow" : "0 2px 4px 0 rgba(0,0,0,0.5)"
            },
            //closeBoxMargin: "10px 2px 2px 2px",
            closeBoxMargin: "0px",
            closeBoxURL: "/static/website/images/Cancel.png",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false,
        };

        var ib = new InfoBox(myOptions);
        ib.openIfClosed = function() {
            if (this.getMap() == null) {
                this.open(map, marker);
            }
        };
        marker.hover_infobox = ib;
        //ib.openIfClosed();
    };

    factory.getWaypointIcon = function(title) {
        return 'data:image/svg+xml;utf-8,' +
            encodeURIComponent(' \
<svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> \
    <title>RedMarkerWithNumber@1x</title> \
    <desc>Created with Sketch.</desc> \
    <defs></defs> \
    <g id="Assets" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> \
        <g id="Assets-Common" transform="translate(-336.000000, -417.000000)"> \
            <g id="RedMarkerWithNumber" transform="translate(336.000000, 417.000000)"> \
                <g id="Group" transform="translate(4.000000, 1.000000)"> \
                    <g id="Shadow" transform="translate(6.600000, 28.826087)" fill="#C9D2D6"> \
                        <path d="M19.8,4.844634 C19.8,7.51914323 15.3682617,9.68926799 9.90141408,9.68926799 C4.43173832,9.68926799 0,7.51914323 0,4.844634 C0,2.1680055 4.43173832,0 9.90141408,0 C15.3682617,0 19.8,2.1680055 19.8,4.844634" id="Fill-1"></path> \
                    </g> \
                    <path d="M10.7047569,32.8376585 L16.5,37.3043478 L22.2952431,32.8376585 C28.5491089,30.4263894 33,24.2256175 33,16.9565217 C33,7.59169337 25.6126984,0 16.5,0 C7.38730163,0 0,7.59169337 0,16.9565217 C0,24.2256175 4.4508911,30.4263894 10.7047569,32.8376585 Z" id="Combined-Shape" fill="#141515"></path> \
                    <rect id="Rectangle" fill="#D8D8D8" x="6.6" y="6.7826087" width="6.6" height="6.7826087"></rect> \
                    <ellipse id="Oval-2" fill="#FC3446" cx="16.5" cy="16.9565217" rx="14.85" ry="15.2608696"></ellipse> \
                </g> \
                <text id="2" font-family="Montserrat" font-size="24" font-weight="bold" letter-spacing="-0.300000012" fill="#FFFFFF"> \
                    <tspan x="13.2476563" y="26">' + title + '</tspan> \
                </text> \
                <rect id="bounds" stroke="#979797" stroke-width="0.01" x="0.005" y="0.005" width="39.99" height="39.99"></rect> \
            </g> \
        </g> \
    </g> \
    </svg>');
    };

    function getGoogleCoordinateObjects(coordinate_list) {
        let lat_lon = [];
        for (i = 0; i < coordinate_list.length; i++) {
            let lng = coordinate_list[i].lng || coordinate_list[i].lon;
            lat_lon.push(
                getGoogleLatLngObject(
                    coordinate_list[i].lat,
                    lng
                )
            );
        }
        return lat_lon;
    }

    factory.convertCoverageToGoogleObjects = function (coverages) {
        let coverage = {
            pickup: getGoogleCoordinateObjects(coverages['pickup']),
            dropoff: getGoogleCoordinateObjects(coverages['dropoff'])
        };
        return coverage;
    };

    factory.createPolygon = function (options) {
        if (!options.hasOwnProperty('paths')) {
            console.error('paths is mandatory');
            return;
        }
        return new google.maps.Polygon(options);
    }

    factory.getGoogleLatLngObject = function (lat, lng) {
        return getGoogleLatLngObject(lat, lng);
    }

    return factory;
});
