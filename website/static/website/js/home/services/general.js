angular.module('blowhornApp').factory('generalService', function ($compile, $http, $q) {
    var factory = {};

    factory.storageAvailable = function(type) {
        // Check if local-storage is available.
        // Sometimes user might have disabled browser storage.
        try {
            var storage = window[type];
            var x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        } catch (e) {
            return e instanceof DOMException && (
                // everything except Firefox
                e.code === 22 ||
                // Firefox
                e.code === 1014 ||
                // test name field too, because code might not be present
                // everything except Firefox
                e.name === 'QuotaExceededError' ||
                // Firefox
                e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
                // acknowledge QuotaExceededError only if there's something already stored
                storage.length !== 0;
        }
    };

    factory.getErrorMessage = function (data) {
        // Using default error messages from django response.
        var _form = data.form;
        if (!_form) {
            _form = data.responseJSON.form;
        }
        var nonFieldErrors = _form.errors;
        if (nonFieldErrors.length > 0) {
            return nonFieldErrors[0];
        } else {
            var fields = _form.fields;
            for (var field in fields) {
            var fieldErrors = fields[field].errors;
            if (fieldErrors.length > 0) {
                return fieldErrors[0];
            }
            }
        }
    }

    factory.setValueToLocalStorage = function(key, val) {
        localStorage.setItem(key, val);
    }

    factory.getValueFromLocalStorage = function(key) {
        return localStorage.getItem(key);
    }

    factory.isWithinCityLimits = function (location_, polygons) {
        console.log(location_.lat(), location_.lng());
        for (var city in polygons) {
            if (google.maps.geometry.poly.containsLocation(
                location_, polygons[city][location_.name])
            ) {
                return true;
            }
        }
        console.log('Out of Cities');
        return false;
    };

    factory.getCity = function (location_, polygons) {
        for (var city in polygons) {
            if (google.maps.geometry.poly.containsLocation(
                location_, polygons[city][location_.name])
            ) {
                return city;
            }
        }
        return null;
    };

  factory.getLocationObject = function (title, icon_url, position) {
    return {
      title: title,
      marker : null,
      icon : {
        url: icon_url,
        anchor: initAnchorPoint(20, 36),
        //origin: new google.maps.Point(0, 0)
      },
      color : null,
      name : '',
      saved : false,
      address : {
          full_address : '',
          country: ''
      },
      contact : null,
      initial_position: position,
    };
  }

  factory.resetLocationObject = function (obj) {
	  obj.address = {};
	  obj.contact = null;
	  obj.name = '';
	  obj.saved = false;
  };

  factory.getBookingObject = function (ICON_PICKUP_MARKER, ICON_DROPOFF_MARKER) {
    var data = {
        'pickup': factory.getLocationObject('Pickup', ICON_PICKUP_MARKER),
        'dropoff': factory.getLocationObject('Drop', ICON_DROPOFF_MARKER),
        'selectedTimeFrame': null,
        'selected_datetime': null,
        'selectedItems': [],
        'selectedLabour': null,
        'selectedVehicleClass': null,
        'selectedContractId': null,
        'mobileNum': null,
        'otpNum': null,
        'couponCode': null,
        'waypointMarkers': [],
    };
    return data;
  }

  factory.getSolutionPageContent = function () {
    return [{
        src: '/static/website/images/homepage/dash2.png',
        header: 'MY FLEET DASHBOARD',
        content: 'Tired of calling up your driver to keep his whereabouts? It’s no more a problem with the MyFleet dashboard where you can track the movement of every vehicle, trips taken and so much more. '
    }, {
        src: '/static/website/images/homepage/pickup.png',
        header: 'Speed of Delivery',
        content: 'We believe in a simple philosophy - customer happiness. That’s exactly why we ensure all our deliveries reach you fast at reasonable prices.'
    }, {
        src: '/static/website/images/homepage/dedicated.png',
        header: 'DEDICATED PERSONNEL',
        content: 'We don’t just get you on board, but we are always around to help you even if it’s in the wee hours of the day!'
    }, {
        src: '/static/website/images/homepage/expert.png',
        header: 'SUBJECT MATTER EXPERTS',
        content: 'Worried if your business is in the right hands? Well, don’t worry, we are the experts in the field and know how to run the show for you from start to finish.'
    }];
  }

  factory.getTeamValuesPageContent = function () {
    return [{
        header: 'Let Data be your Guide',
        content: 'Use data driven decisions. Augmenting Intuition with data validation works well'
    }, {
        header: 'Enjoy your freedom to take decisions, learn from your mistakes and move on',
        content: 'You have the freedom and space to take decisions. It is ok to make mistakes, as long as we learn and move on'
    }, {
        header: 'Think big. Be fearless. Challenge the status quo',
        content: 'Don’t be scared to try new things. Think big. You are actively encouraged to challenge convention and what ‘has been’'
    }, {
        header: 'Put yourself in the other person\'s shoes. Empathise',
        content: 'Learn to see the challenges from the other end. Use empathy to understand and solve conflicts'
    }, {
        header: 'Delight our customers by delivering on our promises',
        content: 'Customer delight for Blowhorn is defined by delivering on the promises we have made to our customer. We are happy to redirect customers to other service providers who are better suited for jobs, where we do not foresee fulfilling the customer promise'
    }, {
        header: 'Don’t wait. Act',
        content: 'Don\'t wait for someone to tell you what needs to be done. Be proactive, take ownership and act towards it.'
    }, {
        header: 'Do the right thing, even when no one is looking',
        content: 'Ethics are the foundations of a lasting business. There will be situations where ambiguity will creep in and we will not be watching you all the time. Just do the right thing'
    }, {
        header: 'Take control, see it to the end',
        content: 'Treat the company’s challenges as your own. Ensure that you finish what you start'
    }, {
        header: 'Stay Grounded',
        content: 'Being receptive to views and ideas of others. Good and bad things happen, but that should not change the way we treat our internal and external customers'
    }, {
        header: 'Pursue creative ideas that have the potential to change the world of logistics',
        content: 'Experiment. Devise new solutions to existing problems. Explore new markets. Blowhorn is your lab and your toolbox'
    }, {
        header: 'Collaborate, Ideate and Execute',
        content: 'Startups are a team sport. Execution of ideas matters. You cannot do it alone. Collaboration is the way ahead'
    }, {
        header: 'Be Tenacious',
        content: 'Don\'t give up in the face of challenges, keep going on ... Onward!'
    }, {
        header: 'Do more with less',
        content: 'Frugality is what has helped us survive against superior funded competitors and large incumbents. Frugal is not cheap. Frugal is doing more with less'
    }, {
        header: 'Have fun!',
        content: 'Building a business is hard. However, do not forget to have fun. Blow the horn!'
    }];
  }

  factory.getFaqPageContent = function () {
    return {
        c2c: [{
            header: 'What are the different vehicle types and their sizes?',
            content: 'The vehicles available on our platform are as follows: <br>'
            + '<div class="faq-vehicle-load-details">'
            +   '<div class="faq-vd-head">'
            +       '<div>Vehicle Type</div>'
            +       '<div>Size (LxWxH in ft.)</div>'
            +       '<div>Load</div>'
            +   '</div>'
            +   '<div class="faq-vd-row">'
            +       '<div>Tata Ace</div>'
            +       '<div>6 x 4.6 x 6</div>'
            +       '<div>750 K</div>'
            +   '</div>'
            +   '<div class="faq-vd-row">'
            +       '<div>Pickup Truck</div>'
            +       '<div>7 x 5 x 6</div>'
            +       '<div>1.5 Ton</div>'
            +   '</div>'
            +   '<div class="faq-vd-row">'
            +       '<div>Tata 407</div>'
            +       '<div>10 x 5.6 x 6</div>'
            +       '<div>2.2 Ton</div>'
            +   '</div>'
            + '</div>'
        }, {
            header: 'What are the prices for booking a vehicle?',
            content: 'The price primarily depends on the time of utilisation and vehicle type. '
            + 'We charge a flat fee for the first 60 minutes and pro-rate the price after that. We also have an ‘all day pricing’ for customers capped at 10 hours and 80 km.'
            + '<br><br> For NGOs and special causes, please contact <a href="mailto:shoutout@blowhorn.com">shoutout@blowhorn.com</a> for pricing.'
        }, {
            header: 'What are the goods not allowed to move?',
            content: 'We don’t move any construction material, dangerous goods, chemicals, unpacked fragile items, etc.'
        }, {
            header: 'Do you provide any extra services or accessories for moving?',
            content: 'We do not provide any additional services besides transportation.'
            + '<br>We are always open for service suggestions at <a href="mailto:shoutout@blowhorn.com">shoutout@blowhorn.com</a>'
        }, {
            header: 'What are the modes of payment?',
            content: '<ul>'
            + '<li>Credit or debit card</li>'
            + '<li>Paytm</li>'
            + '<li>Cash</li>'
            + '<li>Net Banking</li>'
            + '<li>UPI</li>'
            + '</ul>'
        }, {
            header: 'How quickly can I get a vehicle?',
            content: 'You will receive a confirmation call from us within 5 minutes from the time of booking. Once that is done, the vehicle availability and its details will be shared with you in 10 minutes. It should take no longer than 30 minutes for the driver to reach your pickup location once the driver details are assigned.'
            + '<br>In case of any delay or other clarifications, you can reach out to our Customer Delight team via chat, email or phone call. They would be delighted to delight you (We will be replacing our copywriter soon).'
        }, {
            header: 'How quickly can my shipment be delivered?',
            content: 'We assure you that we try our best to get shipments delivered as quickly as possible. It completely depends on the loading/unloading time` and traffic. Bangalore has ‘Silk Board’ junction, Mumbai has Andheri… you get the idea!'
        }, {
            header: 'Can I change my drop location after the booking is confirmed?',
            content: 'The app does not accept changes in current configuration. However,  you can call our Customer Delight team to make changes.'
        }, {
            header: 'How reliable are Blowhorn driver-partners?',
            content: 'We have a checklist of documents which every driver-partner needs to submit before he comes onboard.'
        }, {
            header: 'My driver/vehicle profile does not match. What do I do?',
            content: 'As a customer, you will be able to see the driver’s profile on the app along with his vehicle details. When you notice it’s not matching kindly reach out to our Customer Delight and don\'t let driver to load the vehicle.'
        }, {
            header: 'How do I report an incident?',
            content: 'Kindly reach out to our Customer Delight team at the earliest and we will ensure that the replacement vehicle reaches you in 1 hour.'
        }],
        enterprise: [{
            header: 'How do I report an incident?',
            content: 'If there’s any severe incident, we request you to report to your SPOC within 24 hours from the time of the incident. Our team will look into the matter and they will revert within 5 working days.'
        }, {
            header: 'Why choose Blowhorn over others?',
            content: 'At Blowhorn, we provide clean & well maintained moving rented trucks of all sizes on the fixed & on-demand basis for all your intra-city goods transport. Customer satisfaction is one value that we strive for and hence we ensure seamless operations in line with all customer requirements.'
        }, {
            header: 'What are the type of operations?',
            content: 'We offer the following operations'
            + '<ul>'
            + '<li>First mile delivery</li>'
            + '<li>Line Haul</li>'
            + '<li>Last mile delivery</li>'
            + '<li>Hyper local delivery</li>'
            + '</ul>'
        }, {
            header: 'How do I track my driver’s whereabouts?',
            content: 'All our drivers use the Blowhorn-Partner app. So you can track your vehicles on real time basis on the app as well as on the Enterprise dashboard.'
        }, {
            header: 'Do you integrate APIs?',
            content: 'Yes, we do depending on the customer requirement.'
        }, {
            header: 'Are there any specific locations where my vehicle cannot go?',
            content: 'We have geo-fenced the city limits and we urge you to pick your drop location within the city limits.'
        }, {
            header: 'What is the vehicle replacement time?',
            content: 'In case of vehicle breakdown, absence of vehicle, accident and the likes, the vehicle will be replaced within 1 hour from the time of reporting the incident to us.'
        }, {
            header: 'Do you offer credit period?',
            content: 'Yes, we do and it is 30 days for all the Enterprise customers.'
        }]
    };
  }


  factory.shareOnMedia = function (platform, heading, url_to_share) {
    let socialMedia = {
        facebook: `//facebook.com/sharer/sharer.php?title=${heading}&u=${url_to_share}`,
        twitter: `//twitter.com/intent/tweet?text=${heading}&url=${url_to_share}`,
        whatsapp: `//api.whatsapp.com/send?text=${heading}`,
        linkedin: `https://www.linkedin.com/shareArticle?mini=true&url=${url_to_share}&title=${heading}`,

    };
    console.log('socialMedia[platform]', socialMedia[platform]);
    window.open(socialMedia[platform], '_blank');
    return true;
  };

  factory.getAppContent = function () {
    return {
        model_header: 'Get the blowhorn App',
        h6: 'Book your truck on the move',
        mb30p: 'Download the blowhorn app on your smartphone',
        appstore: 'https://itunes.apple.com/in/app/blowhorn-mini-trucks-on-hire/id1251408316?mt=8',
        playstore: 'https://play.google.com/store/apps/details?id=net.blowhorn.app&amp;hl=en&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1',
    }
  };

  factory.getEnterpriseContent = function () {
    return {
        offers: [
            {
                title: 'Regular deliveries',
                img: '/static/website/images/homepage/revised/home_page_enterprise/asset_1.png',
                points: [
                    'Hourly/Daily package deliveries',
                    'Fixed vehicles',
                    'Line haul',
                ]
            }, {title: false}, {
                title: 'On-demand logistics',
                img: '/static/website/images/homepage/revised/home_page_enterprise/asset_2.png',
                points: [
                    'First mile/Last mile',
                    'Reverse logistics',
                    '4-hour rapid deliveries',
                ]
            }, {title: false}, {
                title: 'Storage and micro-warehouse',
                img: '/static/website/images/homepage/revised/home_page_enterprise/asset_3.png',
                points: [
                    'Micro-warehousing',
                    'Milk run',
                    'Multi-pick up and drop off points',
                ]
            }, {title: false}, {
                title: 'Special services',
                img: '/static/website/images/homepage/revised/home_page_enterprise/asset_4.png',
                points: [
                    'API integration',
                    'Driver + driver help',
                    'Access to dashboard',
                ]
            },
        ],
        brandDetails: {
            path_prefix: `/static/website/images/${current_build}/homepage/partners/image_`,
            total: 13,
            rows: [{
                total: 5
            }, {
                total: 4
            },{
                total: 5
            }]
        }
    }
  };

  return factory;
});
