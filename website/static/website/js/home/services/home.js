angular.module('blowhornApp').factory('blowhornService', function ($http, $q) {
    const API_VERSION_PREFIX = 'api';
    var factory = {};

    function getCookieValue(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        var csrf = $("input[name='csrfmiddlewaretoken']").val();
        return cookieValue || csrf;
    }

    /* GET requests */
    factory.getParams = function () {
        var defer = $q.defer();
        $http({
            url: "/parameters",
            method: "GET",
            dataType: "json"
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.getTimeSlots = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "/slots/",
            type: 'GET',
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.getUserProfile = function () {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/profile/`,
            method: "GET",
            dataType: "json",
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.getCoupons = function (data) {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/customers/v4/availablecoupons/`,
            method: "GET",
            params: data,
            dataType: "json",
        }).success(function (response) {
            defer.resolve(response);
        }).error(function (response) {
            defer.reject(response);
        });

        return defer.promise;
    };

    factory.getCoverageArea = function (city_id) {
        var defer = $q.defer();
        console.log('city_id: ', city_id);
        $http({
            url: `${API_VERSION_PREFIX}/city/coverage?city_name=${city_id}`,
            method: "GET",
            dataType: "json",
        }).success(function (response) {
            defer.resolve(response);
        }).error(function (response) {
            defer.reject(response);
        });

        return defer.promise;
    };

    factory.getVehicleClassesAndRatePackages = function () {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/customers/v2/rate-packages/`,
            method: "GET",
            dataType: "json",
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.checkBookingStatus = function (data) {
        var defer = $q.defer();
        $http({
            url: "bookingstatus",
            method: "GET",
            dataType: "json"
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    /* POST requests */
    factory.signIn = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "/accounts/login/",
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.sendOtpForLogin = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: `${API_VERSION_PREFIX}/customers/v6/validate/customer/`,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.googleLoginAuthenticate = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "/accounts/google/login/",
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.fbLoginAuthenticate = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "/accounts/facebook/login/",
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.signupUser = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: '/accounts/signup/',
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.verifySignupOtp = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: `${API_VERSION_PREFIX}/customers/v6/validate/otp/`,
            type: "POST",
            data: data,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.updateProfile = function (data) {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/profile/`,
            method: "PUT",
            data: data,
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.saveNewPassword = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "/accounts/password/change/",
            type: 'POST',
            data: data,
            // dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.sendPasswordResetLink = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "/accounts/password/reset/",
            type: 'POST',
            data: data,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.sendOtp = function (data) {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/otp/`,
            method: "POST",
            data: data,
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.sendAppLink = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "sendapplink",
            type: "POST",
            data: data,
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.saveUserAddress = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: `${API_VERSION_PREFIX}/location`,
            type: "POST",
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.applyCouponCode = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: `${API_VERSION_PREFIX}/coupon/apply`,
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.verifyOtpBooking = function (data) {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/verifyotpbooking/`,
            method: "POST",
            data: data,
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.saveRoute = function (data) {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/customer/route`,
            method: "POST",
            data: data,
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.getSavedRoutes = function (data, city) {
        var defer = $q.defer();
        $http({
            url: `${API_VERSION_PREFIX}/customer/route?city=${city}`,
            method: "GET",
            params: data,
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.placeBooking = function (data, mobile, otp) {
        var defer = $q.defer();
        $.ajax({
            url: `${API_VERSION_PREFIX}/orders`,
            type: "POST",
            headers: {
                mobile: mobile,
                otp: otp
            },
            data: data,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.contact_us = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "contactus",
            type: "POST",
            data: data,
            success: function (data, textStatus, jqXHR) {
                defer.resolve();
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.get_the_app = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: "applink",
            type: "GET",
            data: data,
            success: function (data, textStatus, jqXHR) {
                defer.resolve();
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.getFareEstimation = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: `${API_VERSION_PREFIX}/customers/v4/getfare`,
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    }

    factory.job_openings = function () {
        var defer = $q.defer();
        $.ajax({
            url: 'https://jsapi.recruiterbox.com/v1/openings?client_name=blowhorn',
            contentType: 'application/json',
            type: "GET",
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data.objects);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.fetch_news = function (page) {
        var defer = $q.defer();
        $.ajax({
            url: '/news',
            contentType: 'application/json',
            type: "GET",
            data: {page: page},
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.nextPageUrl = function (is_staff, is_business_user) {
        if (is_staff) {
            return '/admin';
        } else if (is_business_user) {
            return '/dashboard';
        } else {
            return '/dashboard/mytrips';
        }
    }

    return factory;
});
