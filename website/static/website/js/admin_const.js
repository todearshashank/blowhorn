/* Javascript admin constants can be stored here */

// Live tracking constants used in admin driver, bookings
const ADMIN_DRIVER_TRACKING = 'admin-driver-tracking';
const ADMIN_BOOKINGS_TRACKING = 'admin-bookings-tracking';
