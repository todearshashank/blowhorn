let seo = {
    "theme_color": "#00d2f6",
    "title": "blowhorn! Mini-Trucks on hire in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi.",
    "keywords": "blowhorn! Simpler, Swifter, Smarter; Tempo service, Book my Mini Truck, Transport within Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Transporter for Local Shifting, Tata Ace on Hire in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Hire Mini Truck in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Logistics Services in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Local Shifting in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Pick Up Vehicles, Door to Door Delivery, Online Pickup Booking, Schedule a Pickup, Book a Pickup, Pick Up and Drop Service, Shifting within Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Movers service",
    "description": "We are offering the most convenient way to move goods in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi with real time tracking and fixed pricing. Fast and zippy mini-trucks, pickup trucks, light trucks. Give us a try! Visit our website to make a booking.",
    "og_title": "blowhorn! Mini-Trucks on hire in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi",
    "og_url": "https://blowhorn.com/",
    "og_img": "https://storage.googleapis.com/blowhorn/logo.png",
    "og_description": "We are offering the most convenient way to move goods in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi with real time tracking and fixed pricing. Fast and zippy mini-trucks, pickup trucks, light trucks. Give us a try! Visit our website to make a booking.",
    "og_locale": "en_IN",
    "og_locale_alt": "en_US",
    "og_sitename": "blowhorn",
    "favicon_icon": "/static/website/images/blowhorn/favicon-32x32.png",
    "favicon_short_icon": "/static/website/images/blowhorn/favicon-32x32.png",
    "apple_touch_icon": "/static/website/images/apple-touch-icon-57x57.png"
}
