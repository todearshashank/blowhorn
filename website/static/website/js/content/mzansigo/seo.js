let seo = {
    "theme_color": "#0068C9",
    "title": "MzansiGo | On-Demand Moving and Furniture Delivery",
    "keywords": "MzansiGo is a top moving company based in Cape Town, South Africa. We provide various services such as house moving service, furniture moving service, bakkie rental service etc.",
    "description": "MzansiGo is a top moving company based in Cape Town, South Africa. We provide various services such as house moving service, furniture moving service, bakkie rental service etc.",
    "og_title": "MzansiGo | On-Demand Moving and Furniture Delivery",
    "og_url": "https://booking.mzansigo.co.za/",
    "og_img": "https://mzansigo.co.za/homee.jpg",
    "og_description": "MzansiGo is a top moving company based in Cape Town, South Africa. We provide various services such as house moving service, furniture moving service, bakkie rental service etc.",
    "og_locale": "en_SA",
    "og_locale_alt": "en_US",
    "og_sitename": "mzansigo",
    "favicon_icon": "",
    "favicon_short_icon": "",
    "apple_touch_icon": ""
}
