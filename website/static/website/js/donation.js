const TRANSACTION_TYPE = donation;
let tmpAmount = 0.0;
function initRazorpayTransaction() {
    let isApp = request_from_app == "true";
    var amount = $('#amount').val();
    let amountInPaisa = amount * 100;
    let email = $('#email').val();
    var options = {
        "key": rzpid,
        "amount": amountInPaisa,
        "name": "blowhorn",
        "description": "Logistic Services",
        "image": "/static/img/logo.png",
        "prefill": {
            "name": '',
            "email": email || '',
            "contact": ''
        },
        "notes": {
            "type": TRANSACTION_TYPE
        },
        "handler": function (response) {
            try {
                $('.loader-overlay').show();
                $.ajax({
                    type: 'POST',
                    url: 'api/razorpay',
                    data: {
                        'txn_id': response.razorpay_payment_id,
                        'amount': amountInPaisa,
                        'type': TRANSACTION_TYPE,
                        'email': email
                    },
                    success: function (data) {
                        displayMessage(data.merchantTxnId);
                    },
                    error: function (data) {
                        displayMessage(false);
                    }
                });
            } catch (e2) {
                console.log(e2);
            }
        },
    };
    if (isApp) {
        options["callback_url"] = `${window.location.origin}/webview/rzrpay`;
        options["redirect"] = true;
    }
    var rzp1 = new Razorpay(options);
    try {
        sendCustomEvent('donation', {
            'event_category': 'covid_19',
            'event_label': 'payment',
            'value': parseInt(amount)
        });
    } catch {
        console.error('Blocked...');
    }
    rzp1.open();
}

function displayMessage(merchantTxnId) {
    $('.loader-overlay').hide();
    $('.payment-block').hide();
    if (merchantTxnId) {
        $('#thanyouModal').modal('show');
        $('form').trigger('reset');
        $('#thanyouModal').on('shown.bs.modal', function () {
            $('#msg-transaction-id').text(merchantTxnId)
        })
    } else {
        $('#thanyouModal').modal('show');
    }
}

function addAmount(val) {
    tmpAmount += val;
    $('#amount').val(tmpAmount);
}

$('#amount').on('change', function () {
    let val = $('#amount').val();
    tmpAmount = val ? parseInt(val) : 0.0;
});

$('form').on('submit', function (e) {
    e.preventDefault();
    initRazorpayTransaction();
});

$(document).ready(function () {
    $('.loader-overlay').hide();
    if (is_authenticated == 'true') {
        $('#email').hide();
        $('#email').removeAttr('required');
    }
});
