function getOrderNumbers() {
  return payment_data.unpaid_orders.map(element => {
    return element.number;
  });
}

function getOrderNumbersNote(booking_keys) {
  var booking_key_str = 'booking_key';
  var booking_keys_rzrpay = '';
  var bookingDict = {};
  var keyIndex = 0;
  for (var i = 0; i < booking_keys.length; i++) {
    var currentBookingId = booking_keys[i];
    if ((booking_keys_rzrpay.length + currentBookingId.length) < 255) {
      booking_keys_rzrpay += ',' + currentBookingId;
      bookingDict[booking_key_str] = booking_keys_rzrpay;
    } else {
      keyIndex++;
      booking_key_str = 'booking_key' + keyIndex;
      booking_keys_rzrpay = currentBookingId;
      bookingDict[booking_key_str] = booking_keys_rzrpay;
    }
  }
  return bookingDict;
}

function redirectToPaytm() {
  var orders = getOrderIds();
  window.location.href = 'api/paytm/?entity_type=order&entity_id=' + orders + '&next=/';
}

function initRazorpayTransaction() {
  var booking_keys = getOrderNumbers();
  var pending_amount = payment_data.pending_amount * 100;

  var options = {
    "key": rzpid,
    "amount": pending_amount,
    "name": "blowhorn",
    "description": "Logistic Services",
    "image": "/static/img/logo.png",
    "prefill": {
      "name": profile.name,
      "email": profile.email,
      "contact": profile.mobile
    },
    "notes": {
      'type': 'booking',
      'count': payment_data.unpaid_orders.length
    },
    "handler": function (response) {
      try {
        $('.loader-overlay').show();
        $.ajax({
          type: 'POST',
          url: 'api/razorpay',
          data: {
            'booking_key': booking_keys.join(','),
            'txn_id': response.razorpay_payment_id,
            'amount': pending_amount,
            'type': 'booking'
          },
          success: function (data) {
            displayMessage(data['status'] == 'True');
          },
          error: function (data) {
            displayMessage(false);
          }
        });
      } catch (e2) {
        console.log(e2);
      }
    },
  };
  options['notes'] = Object.assign(
    options['notes'], getOrderNumbersNote(booking_keys)
  );
  var rzp1 = new Razorpay(options);
  rzp1.open();
}

function getOrderIds() {
  return payment_data.unpaid_orders.map(element => {
    return element.id;
  }).join(',');
}

function displayMessage (is_transacton_success) {
  $('.loader-overlay').hide();
  $('.payment-block').hide();
  if (is_transacton_success) {
    $('#page-reload-message').hide();
    $('#message-status h2').text("Payment Successful");
    $(".message").css("background-color", "#059005");
    $('#msg-icon').addClass('fa fa-check-circle');
  } else {
    $('#redirect-message, .link-home').hide();
    $('#message-status h2').text("Transaction Failed");
    $(".message").css("background-color", "#ff5555");
    $('#msg-icon').addClass('fa fa-exclamation-circle');
  }
  $('#message-status').show();

  // auto-redirect to homepage after 5 seconds
  var timeleft = 5;
  var rTimer = setInterval(function () {
    timeleft--;
    document.getElementById("msg-time").textContent = timeleft;
    if (timeleft <= 0) {
      clearInterval(rTimer);
      window.location.href = '/';
    }
  },1000);
}

$(document).ready(function () {
  $('#message-status').hide();
  $('.loader-overlay').hide();
});
