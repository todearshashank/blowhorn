/**
 * Created by Mohan Kumar on 12/5/15.
 */
var bookingArray = [];
var bookingIndex = -1;
var count = 0;
var bookings_table;
$(document).ready(function() {
    $(".dropdown-toggle").dropdown();
});
$(function () {
    //    // init bootpag
    //    $('#page-selection').bootpag({
    //        total: 10
    //    }).on("page", function (event, /* page number here */ num) {
    //        //$("#content").html("Insert content"); // some ajax content loading...
    //    });
    pathname = window.location.pathname;
    var regex = /\/track\/(\S+)/g
    var match_list = regex.exec(pathname);
    if (match_list === null) {
        //loadUserBookingHistory();
        showBookingList(null);
        loadUserPromotionBookingHistory();
    } else {
        booking_id = match_list[1];
        loadThisBooking(booking_id);
    }

    function renderMaptest(pickup_lat, pickup_lon, dropoff_lat, dropoff_lon) {
        var pickup_latlng = new google.maps.LatLng(pickup_lat, pickup_lon);
        var dropoff_latlng = new google.maps.LatLng(dropoff_lat, dropoff_lon);
        var myOptions = {
            zoom: 12,
            center: pickup_latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        mapBooking = new google.maps.Map(document.getElementById("mapzoom"), myOptions);
        latlngBounds = new google.maps.LatLngBounds();
        latlngBounds.extend(pickup_latlng);
        latlngBounds.extend(dropoff_latlng);
        mapBooking.fitBounds(latlngBounds);
        pickupMarker = new google.maps.Marker({
            map: mapBooking,
            position: pickup_latlng,
            icon : {
                url: '/static/website/images/map_marker_pickup.png',
                anchor: new google.maps.Point(36, 48)
            }
        });

        dropoffMarker = new google.maps.Marker({
            map: mapBooking,
            position: dropoff_latlng,
            icon: {
                url: '/static/website/images/map_marker_dropoff.png',
                anchor: new google.maps.Point(36, 48)
            }
        });
        google.maps.event.trigger(mapBooking, 'resize');
    }

    function loadThisBooking(booking_id) {
        $.ajax({
            url: "api/orders/" + booking_id + "/track",
            type: "get",
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                // console.log(response);
                if (!response) {
                    $('.no-booking').removeClass("hide");
                    $('.no-booking').show();
                    $('.bookings').hide();
                } else {
                    // bookingArray = response,
                    bookingIndex = 0;
                    showBookingList(bookingArray);
                    showBookingDialog(response);
                }
            },
            error: function (jqXHR, textStatus, exception) {
                $('.no-booking').removeClass("hide");
                $('.no-booking').show();
                $('.bookings').hide();
                console.log(exception);
            }
        });
    }

    function loadUserBookingHistory() {
        $.ajax({
            url: "/bookingsweb",
            type: "get",
            dataType: 'json',
            data: {page: 0},
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                var trHTML = [];
                bookingArray = response.bookings;
                count = response.count;
                console.log(bookingArray);
                if (bookingArray !== null && bookingArray.length == 0) {
                    $('.no-booking').removeClass("hide");
                    $('.no-booking').show();
                    $('.bookings').hide();
                } else {
                    showBookingList(bookingArray);
                }

            },
            error: function (jqXHR, textStatus, exception) {
                console.log(exception);
            }
        });
    }

    function loadUserPromotionBookingHistory() {
        $('.promotion_bookings').hide();
        $.ajax({
            url: "promotionbookings",
            type: "get",
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                //console.log(response);
                var trHTML = [];
                prbookingArray = response.data;
                if (prbookingArray !== null && prbookingArray.length == 0) {
                    $('.promotion_bookings').hide();
                } else {
                    $.each(prbookingArray, function (i, prbookingDetail) {
                        trHTML.push('<tr class="rows"  data-toggle="modal" data-index="' + i + '">');
                        trHTML.push('<td><span>' + prbookingDetail.booking_time + '<span></td>');
                        trHTML.push('<td>' + prbookingDetail.promotion_code + '</td>');
                        trHTML.push('<td>' + prbookingDetail.dropoff_address + '</td>');
                        if (prbookingDetail.current_status === 'completed') {
                            trHTML.push('<td>Completed</td>');
                        } else if (prbookingDetail.current_status === 'Cancelled') {
                            trHTML.push('<td class="text-danger">Cancelled</td>');
                        } else if (prbookingDetail.current_status === 'Accepted' ||
                            prbookingDetail.current_status === 'Accepted') {
                            trHTML.push('<td class="text-primary">Confirmed</td>');
                        } else if (prbookingDetail.current_status === 'On-Loading' ||
                            prbookingDetail.current_status === 'Unloading' ||
                            prbookingDetail.current_status === 'Loaded') {
                            trHTML.push('<td class="text-progress"><span>In Progress<span></td>');
                        } else {
                            trHTML.push('<td>--</td>');
                        }
                        if (prbookingDetail.total === '' || prbookingDetail.total_cost === null) {
                            trHTML.push('<td>--</td>');
                        } else {
                            trHTML.push('<td>&#x20B9; ' + prbookingDetail.total_cost + '</td>');
                        }
                    });
                    $('.promotion_bookings').append(trHTML.join(""));
                    $('.promotion_bookings').show();
                    $('.promotion_bookings').dataTable({
                        "paging": true,
                        "ordering": false,
                        "info": false,
                        "searching": false,
                        "bFilter": false,
                        "bLengthChange": false,
                    });
                }
            },
            error: function (jqXHR, textStatus, exception) {
                console.log(exception);
            }
        });
    }

    function rowToHTML(i, bookingDetail) {
        var trHTML = [];
        //trHTML.push('<div class="rows" data-toggle="modal" data-index="' + i + '">');
        trHTML.push('<td><span>' + bookingDetail.pickup_date_time + '<span></td>');
        trHTML.push('<td>' + bookingDetail.pickup_address + '</td>');
        trHTML.push('<td>' + bookingDetail.dropoff_address + '</td>');

        if (bookingDetail.total === '' || bookingDetail.total === null) {
            trHTML.push('<td>--</td>');
        } else {
            trHTML.push('<td>&#x20B9; ' + bookingDetail.total + '</td>');
        }

        if (bookingDetail.current_status === 'completed') {
            trHTML.push('<td>Completed</td>');
        } else if (bookingDetail.current_status === 'Cancelled') {
            trHTML.push('<td class="text-danger">Cancelled</td>');
        } else if (bookingDetail.current_status === 'Accepted' ||
        bookingDetail.current_status === 'Accepted') {
        trHTML.push('<td class="text-primary">Confirmed</td>');
        } else if (bookingDetail.current_status === 'On-Loading' ||
        bookingDetail.current_status === 'Unloading' ||
        bookingDetail.current_status === 'Loaded') {
        trHTML.push('<td class="text-progress"><span>In Progress<span></td>');
        } else {
            trHTML.push('<td>--</td>');
        }
        //trHTML.push('</div>');
        return trHTML.join("");
    }
    function showBookingList(response) {
      //console.log(response);
      if (response == null) {
        var trHTML = [];
        //$.each(response, function (i, bookingDetail) {
        //  trHTML.push(rowToHTML(i, bookingDetail))
        //});
        //$('.bookings').append(trHTML.join(""));
        $('.no-booking').addClass("hide");
        $('.no-booking').hide();
        $('.bookings').show();
        $('.modal-booking-status').removeClass('hide');
        bookings_table = $('.bookings').dataTable({
            "createdRow": function(row, data, index ) {
              $(row).html(rowToHTML(index, data));
              $(row).attr({'class':"rows", 'data-index':index});
            },
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            "paging": true,
            "ordering": false,
            "info": false,
            "searching": false,
            "bFilter": false,
            "bLengthChange": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/api/orders",
                "dataSrc": function (json) {
                    // data = JSON.parse(json);
                    return json.data;
                }
            }
            // "ajax": "/api/orders/",
            //"deferLoading": count,
        });
      } else {        
        $('.no-booking').hide();
        $('.bookings').show();
        $('.modal-booking-status').removeClass("hide");
        bookings_table = $('.bookings').dataTable({
            "createdRow": function(row, data, index ) {
              $(row).html(rowToHTML(index, data));
              $(row).attr({'class':"rows", 'data-index':index});
            },
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            "ordering": false,
            "info": false,
            "searching": false,
            "bFilter": false,
            "bLengthChange": false,
            "data": response,
            //"deferLoading": count,
        });
      }
    }
    $('.bookings').delegate('tr.rows', 'click', function (e) {
        //console.log($(this));
        //console.log(e);
        //console.log(bookings_table.fnGetData());
        $(".map-full").css("display", "none");
        var indexAttr = $(this).data('index');
        //console.log(indexAttr);
        bookingIndex = indexAttr;
        //var bookingdata = bookingArray[indexAttr];
        var bookingdata = bookings_table.fnGetData(indexAttr);
        window.location.href = '/track/' + bookingdata.friendly_id;
    });

    function getStaticMapUrl(pickup, dropoff, bookingType) {
        if (bookingType === 'shipment') {
            end_pt = dropoff.lat + ',' + dropoff.lon;
            base_url = "https://maps.googleapis.com/maps/api/staticmap?";
            params = [
                { name: 'key'     , value: google_map_api_key },
                { name: 'sensor'  , value: 'false' },
                { name: 'size'    , value: '275x196' },
                { name: 'maptype' , value: 'roadmap' },
                { name: 'markers' , value: 'color:green|label:D|' + end_pt },
            ]
            url = base_url + $.param(params);
            return url
        }
        else {
            begin_pt = pickup.lat + ',' + pickup.lon;
            end_pt = dropoff.lat + ',' + dropoff.lon;
            base_url = "https://maps.googleapis.com/maps/api/staticmap?";
            params = [
                { name: 'key'     , value: google_map_api_key },
                { name: 'sensor'  , value: 'false' },
                { name: 'size'    , value: '275x196' },
                { name: 'maptype' , value: 'roadmap' },
                { name: 'markers' , value: 'color:red|label:P|' + begin_pt },
                { name: 'markers' , value: 'color:green|label:D|' + end_pt },
            ]
            url = base_url + $.param(params);
            return url
        }
    }

    function showBookingDialog(bookingDetail) {
        var url = getStaticMapUrl(bookingDetail.pickup_geopt, bookingDetail.dropoff_geopt, bookingDetail.type);

        $('#preview-map').attr('src', url);
        $("#driver-img").attr("src", "/static/website/images/dummy_profile.png");

        $(".toggle-map-on").off();
        $(".toggle-map-on").on('click', function () {
            $(".col-left, .col-right, footer").css("display", "none");
            $(".map-full").css("display", "block");
            renderMap(bookingDetail);
        });

        $('#trip-status').removeClass('confirmed cancelled');
        $('#trip-status').show();
        $('#trip-cost').hide();
        $('#total-amt').text('');
        $('#trip-time').text('--');
        $('#distance').text('--');
        $('#assigned-driver').text('');
        $('#pickup-date').text(bookingDetail.pickup_date_or_rpt);
        

        console.log(bookingDetail)
        if (bookingDetail.current_status === 'completed') {
            $('#trip-status').hide();
            $('#trip-status').text('Completed');
            $('#trip-status').addClass('confirmed');
            $('#trip-cost').show();
            $('#min-fare').text(convertToDecimalValue(bookingDetail.min_fare));
            $('#additional-fare').text(convertToDecimalValue(bookingDetail.additional_fare));
            $('#labour-charges').text(convertToDecimalValue(bookingDetail.other_charges));
            $('#other-charges').text(convertToDecimalValue(bookingDetail.extra_charges));
            $('#sub-total').text(convertToDecimalValue(bookingDetail.sub_total));
            $('#discount').text(convertToDecimalValue(bookingDetail.discount));
            $('#total').text(convertToDecimalValue(bookingDetail.total));
            
            if(bookingDetail.payment_status_remote.toLowerCase() != 'paid' && bookingDetail.payment_mode.toLowerCase() != 'cash'){
                $('#payment-mode').hide();
                $('#payment-online').show();   
                $('#status-placeholder').hide()
            } else {
                $('#payment-mode').text(bookingDetail.payment_mode).show();
                $('#payment-online').hide();
                $('#status-placeholder').hide()
            }

            $('#total-amt').text(bookingDetail.total);
            $('#trip-time').text(bookingDetail.trip_duration);
            $('#distance').text(bookingDetail.distance + " km");
            $('.completed-status-details').show().siblings().hide();
        } else if (bookingDetail.current_status === 'Cancelled') {
            $('#trip-status').text('Cancelled');
            $('#trip-status').addClass('cancelled');
            $('.cancelled-status-details').show().siblings().hide();
        } else if (bookingDetail.current_status === 'Accepted') {
            $('#trip-status').text('Confirmed');
            $('#trip-status').addClass('confirmed');

            // Show the cancel option only if, it is yours order
            logged_in_user_email = $("#logged-in-user-email").text();
            if( bookingDetail.customer_email === logged_in_user_email) {
                $('#js-cancel-booking').removeClass("hide");
            }

            $('.confirmed-status-details').show().siblings().hide();
            $("#confirmed-status-message").html('<h4>Your truck will be assigned soon </h4><p><small>We will send you details 30 mins before pickup time</small></p>');
        } else if (bookingDetail.current_status === 'Accepted' ||
            bookingDetail.current_status === 'On-Loading' ||
            bookingDetail.current_status === 'Unloading' ||
            bookingDetail.current_status === 'Loaded') {
            $('#trip-status').addClass('confirmed');
            $('#trip-status').text('In Progress');
            $('.inprogress-status-details').show().siblings().hide();
            if (bookingDetail.current_status === 'Accepted') {
                $("#inprogress-status-message").html('<h4>Your truck is on its way...</h4><p><small>It should reach on time</small></p>');
                $('#trip-status').text('Confirmed');
            } else {
                $("#inprogress-status-message").html('<h4>Trip in Progress</h4>');
            }
        }

        if(bookingDetail.pickup_time_or_rpt !== undefined && bookingDetail.pickup_time_or_rpt  !== '' && bookingDetail.pickup_time_or_rpt !== null){
             $('#start-time').text(bookingDetail.pickup_time_or_rpt);
              $('#map-start-time').text(bookingDetail.pickup_time_or_rpt);
        } else {
            $('#start-time').text('--');
            $('#map-start-time').text('--')
        }
        
        if (bookingDetail.dropoff_time !== undefined && bookingDetail.dropoff_time  !== '' && bookingDetail.dropoff_time !== null) {
             $('#end-time').text(bookingDetail.dropoff_time);
             $('#map-end-time').text(bookingDetail.dropoff_time);
        } else {
            $('#end-time').text('--');
            $('#map-end-time').text('--'); 
        }

        $('#start-address').text(bookingDetail.pickup_address);
        $('#map-start-address').text(bookingDetail.pickup_address);
        $('#end-address').text(bookingDetail.dropoff_address);
        $('#map-end-address').text(bookingDetail.dropoff_address);
        $('#friendly-id').text(bookingDetail.friendly_id);
        $('#booking-id').text(bookingDetail.booking_id);
        $('#booking-key').text(bookingDetail.booking_key);
        $('#booking-date').text(bookingDetail.booking_date);
        $('#booking-time').text(bookingDetail.booking_time);

        //$('#booked-number').text(bookingDetail.contact_details);
        $('#labour-request').text(bookingDetail.labour_requested);

        if (bookingDetail.items_moved !== undefined && bookingDetail.items_moved.length !== 0) {
            var itemsList = '';
            $.each(bookingDetail.items_moved, function (i, items) {
                if (i !== (bookingDetail.items_moved.length - 1)) {
                    itemsList = itemsList + items + ', ';
                } else {
                    itemsList = itemsList + items;
                }

            });
            $('#item-moved').text(itemsList);
        } else {
            $('#item-moved').text('--');
        }

        $('#assigned-driver').text(bookingDetail.driver_name);
        if (bookingDetail.driver_picture !== null && bookingDetail.driver_picture !== '') {
            $("#driver-img").attr("src", bookingDetail.driver_picture);
        }
        $('#modal-booking-status-completed').modal('show');

        $('#js-cancel-booking').on('click', function (e) {
            e.stopImmediatePropagation()
            e.preventDefault();
            $('#reason-error').hide();
            $('#cancel-booking-title').html(bookingDetail.friendly_id);
            $('#js-confirm-cancellation-modal').modal('show');
        });
    }


    $(".toggle-map-off").click(function () {
        $(".map-full").css("display", "none");
        $(".col-left, .col-right, footer").css("display", "block");
    });

    
    $("#book-my-truck").click(function () {
        window.location.href ="/";
    });
    
    $('#modal-booking-status-completed').on('shown.bs.modal', function () {
       var exclude_state=['booking_accepted',"completed","cancelled"];
       if(bookingArray.length > 0){
          if(exclude_state.indexOf(bookingArray[bookingIndex].current_status) == -1)
            $(".map-full").css("display", "block");
            renderMap(bookingArray[bookingIndex]);
       }
    })

    function convertToDecimalValue(val) {
        var newVal = '--';
        if (val !== '' && val !== null) {
            newVal = val.toFixed(2);
        }
        return newVal;
    }

    function convertTimeFormat(timeVal) {
        if (timeVal !== '' && timeVal !== null) {
            var time_24 = timeVal.split(":");
            var hours = time_24[0];
            var minutes = time_24[1];
            var suffix = "AM";
            if (hours >= 12) {
                suffix = "PM";
                hours = hours - 12;
            }
            if (hours == 0) {
                hours = 12;
            }

            var current_time = hours + ":" + minutes + " " + suffix;

            timeVal = current_time;
        }
        return timeVal;
    }
});

function checkValue() {
    var reason = $('#js-cancel-booking-cancellation-reason').val();
    if (reason) {
        $('#reason-error').hide();
    }
    if ('Other' === reason) {
        $('#other-reason').show();
    } else {
        $('#js-cancel-booking-other-cancellation-reason').val('');
        $('#other-reason').hide();
    }
}

$(document).ready(function () {
    $('li').click(function () {
        $(this).addClass('active open').siblings().removeClass('active');
    });
    const BOOKING_CANCELLATION_URL = '/api/booking/update';
    $('#js-confirm-cancellation-modal-accept').on('click', function (e) {
        e.stopImmediatePropagation()
        e.preventDefault();
        var cancellation_reason = $("#js-cancel-booking-cancellation-reason").find(":selected").val();
        var other_cancellation_reason = $("#js-cancel-booking-other-cancellation-reason").val();
        if (!cancellation_reason || ('Other' === cancellation_reason && !other_cancellation_reason)) {
            $('#reason-error').show();
            return;
        }
        var data = {
            "booking_id": $("#booking-id").text(),
            "other_cancellation_reason": other_cancellation_reason,
            "cancellation_reason": cancellation_reason,
            "booking_action": "cancel_booking"
        };
        $.post(BOOKING_CANCELLATION_URL, data).success(function (response) {
            var response = JSON.parse(response);
            if (200 === response.status) {
                $("#trip-status").text("Cancelled").addClass("danger");
                $("#trip-status").removeClass("confirmed");
                $("#js-cancel-booking").hide();
                $("#js-confirm-cancellation-modal").modal("hide");
                $(".confirmed-status-details").hide();
                $(".cancelled-status-details").show();
            } else {
                alert(response.message);
            }
        });
    });
});
