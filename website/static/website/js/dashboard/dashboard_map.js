
/**********************************************
 * Parameters to process location updates and
 * filter out noise */
const MIN_DISTANCE_DELTA_METERS = 10;
const MIN_TIMEDELTA_MS = 1000;
const MAX_SPEED_KMPH = 100;
/*********************************************/

var logging = false;
if (logging == false) {
    console.log = function () {}
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?key=' + google_map_api_key + '&libraries=places&callback=_do';
  document.body.appendChild(script);
}

function _do() {
    //initializeMap();
    //getBookings();
}

window.onload = loadScript;

function renderMap(booking_data) {
    var fit_bounds_driver_location = null;
    var bounds = null;
    var socket = null;
    var marker_driver = null;
    var polyline_coordinates = [];
    var polyline = null;
    var latest_location = null;

    fit_bounds_driver_location = false;
    bounds = new google.maps.LatLngBounds();
    if (booking_data == null) {
        return false;
    }
    var driver_geopt_latlng_obj = null;
        bounds = new google.maps.LatLngBounds();
    // For advanced bookings the driver geopt can be empty. handling this case
    if (booking_data.driver_geopt != "" && booking_data.driver_geopt != null) {
        driver_geopt_latlng_obj = new google.maps.LatLng(booking_data.driver_geopt.lat, booking_data.driver_geopt.lon);
    }
    if (booking_data.pickup_geopt && booking_data.pickup_geopt.lat && booking_data.pickup_geopt.lon) {
        var pickup_latlng = new google.maps.LatLng(booking_data.pickup_geopt.lat, booking_data.pickup_geopt.lon);
        bounds.extend(pickup_latlng);
    }
    var dropoff_latlng = new google.maps.LatLng(booking_data.dropoff_geopt.lat, booking_data.dropoff_geopt.lon);
        bounds.extend(dropoff_latlng);

    var mapOptions = {
        zoom: 12,
        center: dropoff_latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("mapzoom"), mapOptions);

    //pickup marker
    var pimage = {
        url: '/static/website/images/map_marker_pickup.png',
        anchor: new google.maps.Point(36, 48)
    }

    if (booking_data.pickup_geopt && booking_data.pickup_geopt.lat && booking_data.pickup_geopt.lon) {
        marker_src = new google.maps.Marker({
            position: pickup_latlng,
            map: map,
            icon: pimage,
        });
    }
    //dropoff marker
    var dimage = {
        url: '/static/website/images/map_marker_dropoff.png',
        anchor: new google.maps.Point(36, 48)
    }
    marker_dst = new google.maps.Marker({
        position: dropoff_latlng,
        map: map,
        icon: dimage,
    });

    marker_driver = new google.maps.Marker({
        // position: driver_geopt_latlng_obj,
        map: map,
        title: 'Click to zoom'
    });
    marker_driver.setIcon("/static/website/images/pickup.png");

    cs = booking_data.current_status;
    polyline_coordinates = [];
    if (cs == "Complete") {
        //Booking is already completed. Draw the complete route on the map
        polyline_coordinates = booking_data.route_info.map( function(point) {
            var current_geopt_latlng_obj = new google.maps.LatLng(point.lat, point.lon); 
            bounds.extend(current_geopt_latlng_obj);
            return current_geopt_latlng_obj;
        });
    } else if (driver_geopt_latlng_obj != null) {
        // in case this was an advanced booking, we should not come to this section
        //polyline_coordinates.push(driver_geopt_latlng_obj);
        bounds.extend(driver_geopt_latlng_obj);
        marker_driver.setPosition(driver_geopt_latlng_obj);
    }

    map.fitBounds(bounds);

    polyline = new google.maps.Polyline({
        path: polyline_coordinates,
        geodesic: true,
        strokeColor: '#f74757',
        strokeOpacity: 1.0,
        strokeWeight: 4
    });
    polyline.setMap(map);

    // IF the booking is still on-going, then open a channel to get the location update from the driver
    if (cs != 'Complete') {
        var driver_id = (booking_data.driver_phone).toString();
        $.ajax({
            url: "api/firebasetoken/tracking",
            type: "GET",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                var firebase_auth_token = data.firebase_auth_token;
                var isSuccessful = true;
                firebase.auth().signInWithCustomToken(firebase_auth_token).then(function() {
                    isSuccessful = false;
                    firebase.database().ref("active_drivers/" + driver_id).on('value', function (snapshot) {
                        var message = snapshot.val();
                        if (message != null) {
                            var data = JSON.parse(message);
                            updateLocation(data);
                        }
                    }, function (err) {
                        console.log(err)
                    });
                }).catch(function(error) {
                    var errorMessage = error.message;
                    console.log(errorMessage);
                });
            },
            error: function (jqXHR, textStatus, exception) {
            console.log(exception);
            }
        });

        if (booking_data.type === 'shipment' && booking_data.driver_id) {
            $.ajax({
                url: "/api/orders/shipment/"+booking_data.order_id + "/track/"+ booking_data.driver_id,
                type: "GET",
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    var positon = data.geopoint.coordinates;
                    var latest_latlng = new google.maps.LatLng(positon[1], positon[0]);
                    console.log(latest_latlng);
                    marker_driver.setPosition(latest_latlng);                
                },
                error: function (jqXHR, textStatus, exception) {
                    console.log(exception);
                }
            });
        } else {
            // Now let's get the prior route to complete the polyline
            $.ajax({
                url: "/priorroute?booking_id=" + booking_data.booking_id,
                type: "GET",
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    var path = polyline.getPath();
                    var prior_path = data.route.reverse().map(function(point) {
                        var latlng = new google.maps.LatLng(point.lat, point.lon); 
                        bounds.extend(latlng);
                        path.insertAt(0, latlng);
                        return latlng;
                    });
                    if (data.route.length > 0) {
                        var latest = data.route[0];
                        console.log(latest);
                        var latest_latlng = new google.maps.LatLng(latest.lat, latest.lon);
                        marker_driver.setPosition(latest_latlng);
                    }
                    //marker_src.setPosition(path.getAt(0));
                    //marker_dst.setPosition(path.getAt(path.length-1));
                    map.fitBounds(bounds);
                },
                error: function (jqXHR, textStatus, exception) {
                    console.log(exception);
                }
            });
        }

        function isNewLocationNewer(old_location, new_location){
            if (old_location == null) {
                return true;
            } else if (new_location.accuracy_meters > 30) {
                return false;
            }
            var old_ = new Date(old_location.gpstimestamp);
            var new_ = new Date(new_location.gpstimestamp);
            var distance_meters =  google.maps.geometry.spherical.computeDistanceBetween(
                old_location.latlng,
                new_location.latlng
            );
            //console.log(old_, new_);
            var timedelta_ms = new_ - old_;
            var speed_kmph = distance_meters * 60 * 60 /timedelta_ms;
            console.log(timedelta_ms, distance_meters, speed_kmph);
            var newer = (timedelta_ms > MIN_TIMEDELTA_MS && 
                distance_meters > MIN_DISTANCE_DELTA_METERS &&
                speed_kmph < MAX_SPEED_KMPH
            );
            console.log(newer);
            //console.log('isNewLocationNewer', flag);
            return newer;
        }

        function updateLocation(data) {
            console.log(data);
            location_ = data.location;
            var latlng = new google.maps.LatLng(location_.lat, location_.lon);
            location_.latlng = latlng;
            if (!isNewLocationNewer(latest_location, location_)) {
                console.log('Latest location is stale/noise');
                return;
            }
            console.log('Updating newer location');
            latest_location = location_;
            marker_driver.setPosition(latlng);

            if (booking_data.type !== 'shipment') {
                var path = polyline.getPath();
                path.push(latlng);
            }
            bounds.extend(latlng);
            if (fit_bounds_driver_location == false) {
                //If fit bounds has been done once, not required to do it again.
                //Unlikely the driver will stray away and also not to disturb
                //users custom zoom changes in GUI
                map.fitBounds(bounds);
                fit_bounds_driver_location = true;
            }
        }
    }
}
