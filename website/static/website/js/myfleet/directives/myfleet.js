angular.module('myfleetApp').directive('myfleetDriverLog', function () {
    return {
        restrict: 'E',
        templateUrl: '/static/website/html/myfleet/myfleet_driver_chart.html'
    };
});

angular.module('myfleetApp').directive('myfleetDriverChart', function () {
    return {
        restrict: 'E',
        templateUrl: '/static/website/html/myfleet/myfleet_driver_log.html'
    };
});

angular.module('myfleetApp').directive('myfleetPastDriver', function () {
    return {
        restrict: 'E',
        templateUrl: '/static/website/html/myfleet/myfleet_past_driver.html'
    };
});

angular.module("myfleetApp").directive('customSelect2', function () {
    return {
        restrict: 'EA',
        require: '?ngModel',
        link: function (scope, element, attrs, ctrl) {
            element.select2({
                placeholder: "Select Driver",
                allowClear: true,
                minimumResultsForSearch: 10
            });
        }
    };
});
