angular.module('myfleetApp').factory('myfleetService', function ($q, $rootScope, $window) {

    const MYFLEET_TRACKING = 'myfleet';
    var factory = {};

    function getCookieValue(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        var csrf = $("input[name='csrfmiddlewaretoken']").val();
        return cookieValue || csrf;
    }

    factory.fetchParamsAndDrivers = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: 'api/b2cdata',
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.fetchTripsheets = function (params) {
        var defer = $q.defer();
        $.ajax({
            url: 'api/myfleet/tripsheets',
            type: 'POST',
            data: params,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.getPriorRoute = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: '/priorroutebusiness?&driver_id=' + data,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.getDriverProfile = function (data) {
        var defer = $q.defer();
        $.ajax({
					  url: 'api/profile/' + data,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.getRealTimeUpdates = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: 'api/firebasetoken/' + MYFLEET_TRACKING,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.getDriverDistanceData = function (id, mapped_to) {
        var defer = $q.defer();
        $.ajax({
            url: '/driverlog/' + id + '?method=distance&mapped_to=' + mapped_to,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.getDriverMeterData = function (id, mapped_to) {
        var defer = $q.defer();
        $.ajax({
            url: '/driverlog/' + id + '?method=meter&mapped_to=' + mapped_to,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.getPastDriverData = function (start_date, end_date) {
        var defer = $q.defer();
        $.ajax({
            url: '/driverlog/null?startdate=' + start_date + '&enddate=' + end_date,
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.openDriverTrace = function (id, date) {
        $window.open('myfleet/trace?driver_id=' + id + '&date=' + date);
    };

    return factory;
});
