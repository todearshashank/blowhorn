angular.module('myfleetApp', ['ngRoute', 'smart-table', 'ui.bootstrap', 'ngAnimate', 'isteven-multi-select', 'export_functions']);

angular.module('myfleetApp').config(function ($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: '/static/website/html/myfleet/main.html',
        controller: 'myfleetController'
    }).
    otherwise({
        redirectTo: '/'
    });
});
