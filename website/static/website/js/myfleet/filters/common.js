angular.module('myfleetApp').filter('grandTotal', function () {
    return function (data, key) {
        if (typeof (data) === 'undefined' || typeof (key) === 'undefined') {
            return 0;
        }
        var sum = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i][key]) {
                if (typeof (data[i][key]) === "number") {
                    sum += data[i][key];
                } else {
                    // Return null instead of zero for NaNs, helpful for show/hide table cell
                    sum = "";
                    break;
                }
            }
        }
        return sum;
    };
});
