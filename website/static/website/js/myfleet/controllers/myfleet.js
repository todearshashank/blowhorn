angular.module('myfleetApp').controller('myfleetController', function ($scope, $log, $interval, $http, $filter, myfleetService) {

    var driver_data = null;
    var map = null;
    var driver_dict = {};
    var markers_drivers = [];
    var markers_places = [];
    var driver_bounds = null;
    var places_bounds = null;
    var socket = null;
    var timeLastUpdate;
    var icons = {};
    var vclasses = {};
    var info_window_open = true;
    var polylinePath = [];
    var driverArray = [];
    var data_array = [];
    var mapped_drivers = [];
    const MIN_DISTANCE_DELTA_METERS = 10;

    $scope.tab = 1;
    $scope.driver_names = {};
    $scope.driver_details = {};
    $scope.toggleSidebar = false;
    $scope.showSidebar = false;
    $scope.isLoading = true;
    $scope.distanceCollection = [];
    $scope.distanceRowCollection = [];
    $scope.meterCollection = [];
    $scope.meterRowCollection = [];
    $scope.pastDriverCollection = [];
    $scope.pastDriverRowCollection = [];
    $scope.driver_id_name_list = [];
    $scope.selected_driver_id = [];
    // Filename for export
    $scope.exportFilenamePrefix = "DriverDistance";

    function getDataAndShowMap() {
        myfleetService.fetchParamsAndDrivers().then(function (data) {
            console.log(data);
            renderMap();
            initParams(data);
            processDriverData();
            $scope.fetchTripsheets();
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
    }
    getDataAndShowMap();

    function clearPathAndPickUpDropOffMarkers() {
        for (var i = 0; i < polylinePath.length; i++) {
            polylinePath[i].setMap(null);
        }
    }

    function clearMarkerInfoWindow() {
        for (var i = 0; i < markers_drivers.length; i++) {
            markers_drivers[i].infowindow.close();
        }
    }

    $scope.selectTab = function (setTab) {
        $scope.tab = setTab;
    };

    $scope.isSelected = function (checkTab) {
        return $scope.tab === checkTab;
    };

    $scope.refreshMap = function () {
        setTimeout(function () {
            triggerMapResize(map);
            $scope.fitZoom();
        }, 200);
    };

    $scope.fitZoom = function () {
        // $scope.driver_searchText = '';
        var newBounds = initLatLngBounds();
        for (var i = 0, marker; marker = markers_drivers[i]; i++) {
            if (marker.getMap() && marker.getVisible()) {
                newBounds.extend(marker.getPosition());
            }
        }
        map.fitBounds(newBounds);
    };

    function showHideDriver(driver, show_or_hide) {
        if (show_or_hide === 'show') {
            if (driver['marker'].getMap() === null) {
                driver['marker'].setMap(map);
                driver['circle'].setCenter(driver.latlng);
                driver['circle'].setRadius(driver.location['accuracy_meters']);
            }
        } else {
            driver['marker'].setMap(null);
            driver['circle'].setCenter(null);
            driver['circle'].setRadius(null);
        }
    }

    $scope.toggleInfowindow = function () {
        for (var i = 0, marker; marker = markers_drivers[i]; i++) {
            if (marker.getMap() !== null) {
                if (info_window_open) {
                    marker.infowindow.close();
                } else {
                    marker.infowindow.open(map, marker);
                }
            }
        }
        info_window_open = !info_window_open;
    };

    function renderMap() {
        map = initMap(document.getElementById("google-map"));
        var fit = document.getElementById('fit-zoom');
        var iw = document.getElementById('toggle-infowindow');
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(fit);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(iw);
        google.maps.event.addListener(map, 'drag', function() {
            triggerMapResize(map);
        });
        google.maps.event.addListener(map, 'zoom_changed', function() {
            triggerMapResize(map);
        });
    }

    function initParams(data) {
        icons = data.icons;
        driver_dict = data.driver_locations;
        cities = data.cities;
        mapped_drivers = data.mapped_drivers;
        driverArray = data.driver_details;
        $scope.driver_id_name_list = data.driver_id_name_regno;
    }

    function processDriverData() {
        for (var driver_id in driver_dict) {
            var driver = driver_dict[driver_id];
            var latlng = getGoogleLatLngObject(driver.location.lat, driver.location.lon);
            createMarker(driver);
            driver.marker.setPosition(latlng);
            driver.circle.setCenter(latlng);
            driver.latlng = latlng;
            setInfoWindowContent(driver);
            addDriversToSidebar(driver);
            showHideDriver(driver, 'show');
        }
        getRealTimeUpdates();
        $scope.fitZoom();
    }

    function processParams(data) {
        var _dict = {};
        for (var k = 0; k < data.length; k++) {
            display = data[k][0];
            key = data[k][1];
            _dict[key] = display;
        }
        return _dict;
    }

    function convertToArray(arrayOfObj) {
        return arrayOfObj.map(function (item) {
            return item.value;
        });
    }

    $scope.fetchTripsheets = function () {
        var selectedKeys = convertToArray($scope.selected_driver_id);
        console.log(selectedKeys);
        $("#info-text").hide();
        $("#loaderTripsheets").show();
        $scope.showSearchBox = false;
        $scope.isLoading = true;
        $scope.searchText = "";

        var startdate = $('#tripsheet_from').val();
        var enddate = $('#tripsheet_to').val();

        // Reset the table collections
        $scope.tripsheetCollection = [];
        // $scope.businessFlows = [];
        $scope.rowTripsheetCollection = [];

        var more = "";
        var params = {
            "driver_ids": JSON.stringify(selectedKeys),
            "startdate": startdate,
            "enddate": enddate,
            // "cursor": ""
        };

        function getTableData() {
            myfleetService.fetchTripsheets(params).then(function (data) {
                console.log(data);
                // if (!params.cursor) {
                if (data.result.length > 0) {
                    $scope.showSearchBox = true;
                    $scope.params = data.params;
                    $scope.dateParams = data.date_params;
                    // $scope.businessFlows = data.business_flows;
                    // $scope.dateParamsDict = processParams($scope.dateParams);
                    $scope.columns = [].concat(data.columns);
                } else {
                    $scope.infoText = "No tripsheets found";
                    $("#info-text").show();
                }
                // }
                params.cursor = data.cursor;
                more = data.more;

                $scope.rowTripsheetCollection = $scope.rowTripsheetCollection.concat(data.result);
                $scope.tripsheetCollection = [].concat($scope.rowTripsheetCollection);

                /*if (params.cursor) {
                    var flowKeys = [];
                    angular.forEach($scope.businessFlows, function (flow) {
                        flowKeys.push(flow.key);
                    });

                    for (var i = 0; i < data.business_flows.length; i++) {
                        if (flowKeys.indexOf(data.business_flows[i].key) === -1) {
                            $scope.businessFlows.push(data.business_flows[i]);
                        }
                    }
                }*/

                // if (more) {
                //     getTableData();
                // } else {
                    // Enable the buttons if no more data to fetch
                $scope.isLoading = false;
                $("#loaderTripsheets").hide();
                // }
            }).catch(function (data) {
                $scope.isLoading = false;
                $("#loaderTripsheets").hide();
                toastr.error(data.responseText, 'Failed');
            });
        }
        getTableData();
    }

    $scope.viewDriverTrace = function (row, event) {
        event.stopPropagation();
        myfleetService.openDriverTrace(row.driver_id, row.creation_date);
    }

    function getCircle(map, accuracy) {
        if (accuracy == null) {
            console.log('Accuracy is null. App needs to be updated');
            accuracy = 0;
        }
        var circleOptions = {
            radius: accuracy
        };
        return initCircle(map, circleOptions);
    }

    function createMarker(driver) {
        console.log('Creating marker for driver', driver.driver_id);
        var icon = icons[driver.status];
        var markerOptions = {
            draggable: false
        };
        var marker = initMarker(map, markerOptions);
        markers_drivers.push(marker);
        driver.marker = marker;
        marker.setIcon(icon);

        var infowindowOptions = {
            disableAutoPan: true,
        };
        var infowindow = initInfoWindow(infowindowOptions);
        marker.infowindow = infowindow;
        marker.driver = driver;
        google.maps.event.addListener(marker, 'click', (function(marker, info) {
            return function() {
                info.open(map, marker);
            }
        })(marker, infowindow));

        google.maps.event.addListener(marker, 'dblclick', (function(marker, info) {
            return function() {
                info.open(map, marker);
                drawPolyLinePath(map, driver.driver_id);
            }
        })(marker, infowindow));

        //Right click event
        google.maps.event.addListener(marker, 'rightclick', (function(marker, info) {
            return function() {
                info.close();
                clearPathAndPickUpDropOffMarkers();
            }
        })(marker, infowindow));

        driver.infowindow = infowindow;
        driver.last_update = driver.timestamp;
        driver.circle = getCircle(map, 0);
    }

    window.setInterval(function () {
        for (var driver_id in driver_dict) {
            driver = driver_dict[driver_id]
            setInfoWindowContent(driver);
        }
     }, 60000);

    function setInfoWindowContent(driver) {
        console.log('Update infowindow of driver', driver.driver_id);
        console.log(driver);
        infowindow      = driver.infowindow;
        name            = driver.profile.name;
        mobile          = driver.profile.mobile;
        timestamp       = new Date(driver.timestamp);
        gps_lag         = driver.location.gps_lag;
        battery_percent = driver.location.battery_percent;
        distance_km     = driver.distance_km;
        start_time_disp = driver.location.start_time_disp;
        //console.log(distance_km);
        //console.log(start_time_disp);
        // var distance_line = "";
        // try {
        //     distance_line = ('Since ' + start_time_disp + ' : ' + distance_km.toFixed(1) + ' km' + '<br>');
        // } catch(err) {
        // }
        var speed_kmph = Math.round(driver.location.speed_kmph * 100) / 100;
        var speed_line = speed_kmph != null? ('Speed: ' + speed_kmph +' km/hr<br>') : '';
        var updateTime = timestamp.toLocaleTimeString();
        var now = new Date();
        var time_delta_mins = (now - timestamp)/(1000*60);
        var showRed = 0;
        if (time_delta_mins > 4) {
            showRed = 1;
        }

        if (showRed == 1) {
            var marker_text =
            [
                'Name: ' + name + '<br>',
                'Mobile: ' + mobile + '<br>',
                'Last Update: <span style="color:red">' + updateTime + '</span><br>',
                gps_lag.trim() && ('Gps lag: ' + '<a style="color: red">' + gps_lag.trim() +'</a><br>' ),
                'Battery level: ' + battery_percent + '%' + '<br>',
                speed_line,
            ].join('').bold();
        } else {
            var marker_text =
            [
                'Name: ' + name + '<br>',
                'Mobile: ' + mobile + '<br>',
                'Last Update:<span style="color:black">' + updateTime + '</span><br>',
                gps_lag.trim() && ('Gps lag: ' + '<a style="color: red">' + gps_lag.trim() +'</a><br>' ),
                'Battery level: ' + battery_percent + '%' + '<br>',
                speed_line,
            ].join('').bold();
        }
        //console.log(marker_text);
        infowindow.setContent(marker_text);
    }

    function drawPolyLinePath(map, driver_id) {
        clearPathAndPickUpDropOffMarkers();
        var polyline = null;
        var polyline_coordinates = [];

        var polylineOptions = {
            path: polyline_coordinates
        };
        polyline = drawPolyline(map, polylineOptions);
        polylinePath.push(polyline);

        myfleetService.getPriorRoute(driver_id).then(function (data) {
            if (data) {
                var path = polyline.getPath();
                var prior_path = data.route.reverse().map( function(point) {
                    var latlng = new google.maps.LatLng(point.lat, point.lon);
                    path.insertAt(0, latlng);
                    return latlng;
                });
            }
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
    }

    function showDriverIfRequired (driver_) {
        showHideDriver(driver_, 'show');
    }

    // Function to check if the new location has a newer gps timestamp
    function isNewLocationNewer(old_location, new_location){
        var old_ = new Date(old_location.gpstimestamp);
        var new_ = new Date(new_location.gpstimestamp);
        return (new_ > old_);
    }

    function Distance(loc1, loc2) {
        return google.maps.geometry.spherical.computeDistanceBetween(loc1, loc2);
    }

    function updateDriverData(driver, data, new_driver, newer_location) {
        var distance_meters = 0;
        var latlng = getGoogleLatLngObject(data.location.lat, data.location.lon);
        if (new_driver) {
            driver.latlng = latlng;
            driver.distance_km = 0;
        }
        if (newer_location) {
            distance_meters = Distance(latlng, driver.latlng);
            driver.status = data.status;
            driver.timestamp = data.timestamp;
            driver.marker.setIcon(icons[data.status]);
            // If the distance between previous location is < 10m we might as well ignore it
            // as it can be GPS noise
            if (distance_meters > MIN_DISTANCE_DELTA_METERS || new_driver) {
                driver.distance_km += distance_meters/1000;
                driver.location = data.location;
                driver.marker.setPosition(latlng);
                driver.circle.setCenter(latlng);
                driver.latlng = latlng;
            }
            driver_dict[data.driver_id] = driver;
            setInfoWindowContent(driver);
            showDriverIfRequired(driver);
        }
    }

    function updateLocation(data) {
        console.log(data);
        var driver_id = data.driver_id;
        var status = data.status;
        var region = data.region;
        var driver = {};
        var newer_location = true;
        var new_driver = false;

        if (driver_dict.hasOwnProperty(driver_id)) {
            driver = driver_dict[driver_id];
            newer_location = isNewLocationNewer(driver.location, data.location);
            updateDriverData(driver, data, new_driver, newer_location);
        } else {
            new_driver = true;
            console.log(driver_id, 'Driver Profile not present. Fetching!!');
            myfleetService.getDriverProfile(driver_id).then(function (profile_data) {
                console.log(profile_data);
                driver.driver_id = driver_id;
                driver.status = status;
                driver.region = region;
                driver.profile = profile_data.message;
                driver_dict[driver_id] = driver;
                createMarker(driver);
                addDriversToSidebar(driver);
                updateDriverData(driver, data, new_driver, newer_location);
            }).catch(function (data) {
                console.log('Unknown Driver', driver_id);
            });
        }
    }

    function getRealTimeUpdates() {
        //toastr.success('Registering for realtime updates');
        console.log('Getting real-time updates');
        myfleetService.getRealTimeUpdates().then(function (data) {
            var firebase_auth_token = data.firebase_auth_token;
            var isSuccessful = true;
            firebase.auth().signInWithCustomToken(firebase_auth_token).catch(function(error) {
                var errorMessage = error.message;
                isSuccessful = false;
                console.log(errorMessage);
            });
            if (isSuccessful) {
              if (!mapped_drivers) {
                return;
              }
                for (var i = 0; i < mapped_drivers.length; i++) {
                    firebase.database().ref("active_drivers/" + mapped_drivers[i]).on('value', function (snapshot) {
                        var message = snapshot.val();
                        if (message != null) {
                            var data = JSON.parse(message);
                            updateLocation(data);
                        }
                    }, function (err) {
                        console.log(err)
                    });
                }
            }
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
    }

    function addDriversToSidebar(driver) {
        var driver_name_mobile = driver.profile.name + " - " + driver.profile.mobile;
        // var found = $scope.driver_names.indexOf(driver_name_mobile);
        // if (found === -1) {
        //     $scope.driver_names.push(driver_name_mobile);
        // }
        $scope.driver_names[driver.driver_id] = driver_name_mobile;
        $scope.driver_details[driver.driver_id] = {'name': driver.profile.name, 'status': driver.status};
    }

    $scope.getDriverFromSidebar = function (id) {
        clearMarkerInfoWindow();
        clearPathAndPickUpDropOffMarkers();
        var driver = driver_dict[id];
        if (driver) {
            map.setCenter(driver.marker.getPosition());
            new google.maps.event.trigger(driver.marker, 'click');
        }
    };

    $scope.openDriverListSidebar = function () {
        $scope.toggleSidebar = !$scope.toggleSidebar;
        if ($scope.toggleSidebar) {
            $scope.showSidebar = true;
        } else {
            $scope.showSidebar = false;
        }
    };

    $scope.selectDriver = function () {
        // $scope.driver_searchText = driver_name_number;
        clearMarkerInfoWindow();
        clearPathAndPickUpDropOffMarkers();
        for (var driver in $scope.driver_names) {
            if (driver === $scope.driver) {
                var driver = driver_dict[$scope.driver];
                if (driver) {
                    map.setCenter(driver.marker.getPosition());
                    new google.maps.event.trigger(driver.marker, 'click');
                }
                break;
            }
        }
    };

    function displayDriverDetails(driverArray) {
        $('.no-driver').hide();
        $('.drivers').hide();
        $('#meter_table').hide();
        $('#displayoption').hide();
        if (!driverArray || driverArray.length === 0) {
            $('.no-driver').show();
            $('.drivers').hide();
        } else {
            displayDriverList(driverArray);
        }
    }

    function displayDriverList(data) {
        var trHTML = [];
        $.each(data, function (i, driverDetail) {
            trHTML.push('<tr class="rows" data-toggle="modal" data-index="' + i + '">');
            trHTML.push('<td>' + driverDetail.name + '</td>');
            trHTML.push('<td>' + driverDetail.id + '</td>');
            //trHTML.push('<td>' + driverDetail.mobile + '</td>');
            trHTML.push('<td>' + driverDetail.mapped_to + '</td>');
            trHTML.push('<td>' + driverDetail.registration_number + '</td>');
            trHTML.push('<td>' + driverDetail.vehicle_model + ' </td>');
        });
        $('.drivers').append(trHTML.join(""));
        $('.no-driver').hide();
        $('.drivers').show();
        $('.drivers').dataTable({
            "paging": true,
            "ordering": false,
            "info": false,
            "searching": false,
            "bFilter": true,
            "bLengthChange": false
        });
    }

    $scope.openDriverTrace = function (driver_id, date) {
        myfleetService.openDriverTrace(driver_id, date);
    };

    $('.drivers').off();
    $('.drivers').on('click','tr.rows',function (e) {
        var indexAttr = $(this).data('index');
        driverIndex = indexAttr;
        var id = driverArray[indexAttr].id;
        var mapped = driverArray[indexAttr].mapped_to;
        myfleetService.getDriverDistanceData(id, mapped).then(function (data) {
            $scope.distanceCollection = data.result;
            $scope.distanceRowCollection = data.result;
            data_array = $scope.distanceCollection;
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
        $('#driver_name').val(driverArray[indexAttr].name);
        $('#driver_id').val(driverArray[indexAttr].id);
        $('#modal-driver-details').modal('show');
    });

    $('.drivers').on('click','tr.rows', function (e) {
        var indexAttr = $(this).data('index');
        driverIndex = indexAttr;
        var id = driverArray[indexAttr].id
        var mapped = driverArray[indexAttr].mapped_to;
        myfleetService.getDriverMeterData(id, mapped).then(function (data) {
            $scope.meterCollection = data.result;
            $scope.meterRowCollection = data.result;
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
        $('#driver_name').val(driverArray[indexAttr].name);
        $('#driver_id').val(driverArray[indexAttr].id);
        $('#startdate').val('');
        $('#enddate').val('');
        $('#modal-driver-details').modal('show');
    });

    //draw column chart
    function showChart() {
        console.log("Chart init");
        var newColumn = document.getElementById('distance_chart');
        newColumn.innerHTML="<button type='button' class='btn btn-info pull-right' id='exportDateDist'><span class='glyphicon glyphicon-export'></span> Export PDF</button><div id='column_chart'></div>";
        startdate= document.getElementById("startdate").value;
        enddate= document.getElementById("enddate").value;
        var sdate = new Date(startdate);
        var edate = new Date(enddate);
        var chart_data = [];
        var convDate = [];
        for (i=0;i<data_array.length;i++) {
            convDate[i] = new Date(data_array[i].date);
            if (sdate <= convDate[i] && edate >= convDate[i]){
                chart_data.push(data_array[i]);
            }
        }
        drawColumnChart(chart_data);
    }

    function calcChartData(data_array) {
        var work_date  = [];
        var distance   = [];
        var date_store = [];
        var convDate   = [];
        var finalColumnData = [];
        var monthName = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

        for (var i = 0; i < data_array.length; i++) {
            convDate[i] = new Date(data_array[i].date);
            work_date.push(monthName[convDate[i].getMonth()] +' '+ convDate[i].getDate());

            if (work_date[i] != work_date[i-1]) {
                date_store.push(work_date[i]);
            }
        }

        for (var i = 0; i < date_store.length; i++) {
            distance[i] =0;
            for (var j=0;j<work_date.length;j++) {
                if (date_store[i] == work_date[j]) {
                    if (data_array[j].distance_km != null && data_array[j].distance_km != '') {
                        distance[i] += parseFloat(data_array[j].distance_km);
                    }
                }
            }
        }

        for (var i = date_store.length-1; i >= 0; i--) {
            finalColumnData[date_store[i]] = distance[i];
        }
        return finalColumnData;
    }

    function exportCharts(chart, chartName) {
        var d = new Date();
        var date = d.getDate();
        var month = d.getMonth()+1;
        var year = d.getFullYear();
        var fileName = date + "-" + month+ "-" + year;
        chart.exportChart({
            type: 'application/pdf',
            filename: chartName + fileName,
            sourceWidth: 1000,
            sourceHeight:500
        });
    }

    function drawColumnChart(data_array) {
        var datesData = [];
        var distance  = [];
        var values = calcChartData(data_array);
        for (var key in values) {
            datesData.push(key);
            distance.push(values[key]);
        }
        var total = 0;
        $.each(distance, function(){total+=parseFloat(this.toFixed(1)) || 0;});

        $('#column_chart').html('');
        $('#column_chart').highcharts({
            chart:{
                marginTop: 80,
                marginLeft: 50,
                marginBottom:50,
                zoomType: 'xy',
                resetZoomButton: {
                    theme: {
                        fill: 'orange',
                        stroke: '#00cc00',
                        r: 16,
                        states: {
                            hover: {
                                fill: '#0099cc',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    }
                }
            },
            title: {
                text: 'Date vs Distance',
                x: -20, //center
                margin: 50,
                style: {
                    fontWeight: 'bold',
                    fontSize : 30
                }
            },
            subtitle: {
                text: 'Total Distance: '+total+' kms',
                floating: true,
                x: -20,
                y: 60,
                style: {
                    fontWeight: 'bold',
                    fontSize : 20
                }
            },
            xAxis: {
                categories: datesData,
            },
            yAxis: {
                min: 0,
                gridLineDashStyle: 'dot',
                title: {
                    text: 'Distance (kms)'
                }
            },
            plotOptions: {
                column: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<span style="font-size:1.2em;">{point.y:.1f} kms</span>'
                    },
                },
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                type : 'column',
                name: "Distance",
                data: distance,
                tooltip: {
                    pointFormat: 'Distance: {point.y:.2f} kms',
                    shared: false
                }
            }],
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            }
        });

        $('#exportDateDist').click( function () {
            var chart = $('#column_chart').highcharts();
            exportCharts(chart, 'dateDist_');
        });
    }

    $scope.showReport = function () {
        showChart();
        $('#column_chart').highcharts().setSize(1000,500);
        $('#modal-driver-chart').modal('show');
    };

    //past drivers modal
    $scope.showLog = function (row) {
        showDriverLog(row);
    };

    $('#pastdrivers').off();
    $('#pastdrivers').on('click',function () {
        if ($("#datevalid").valid()) {
            start_date= document.getElementById("start").value;
            end_date= document.getElementById("end").value;
            myfleetService.getPastDriverData(start_date, end_date).then(function (data) {
                $scope.pastDriverCollection = data.result;
                $scope.pastDriverRowCollection = data.result;
            }).catch(function (data) {
                toastr.error(data.responseText, "Failed");
            });
            $('#modal-past-driver-details').modal('show');
        }
    });

    function showDriverLog(row) {
        var driver_id = row.driver_id;
        var mapped = row.mapped_to;
        $('#driver_name').val(row.name);
        $('#driver_id').val(driver_id);
        var date = new Date(row.date);
        var distancedata = [];
        var meterdata = [];
        for (var i = 0; i < $scope.pastDriverCollection.length; i++) {
            distdate = new Date($scope.pastDriverCollection[i].date);
            if (date.getTime() == distdate.getTime() && driver_id==$scope.pastDriverCollection[i].driver_id) {
                distancedata.push($scope.pastDriverCollection[i]);
            }
        }
        $scope.distanceCollection = distancedata;

        myfleetService.getDriverMeterData(driver_id, mapped).then(function (data) {
            meterlogdisplay(data.result);
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });

        function meterlogdisplay(meterlogdata) {
            for (var i = 0; i < meterlogdata.length; i++) {
                meterdate = new Date(meterlogdata[i].mr_begin_time.split(" ")[0]);
                if (date.getTime() == meterdate.getTime() && driver_id==meterlogdata[i].driver_id) {
                    meterdata.push(meterlogdata[i]);
                }
            }
            $scope.meterCollection = meterdata;
        }
        $('#modal-driver-details').modal('show');
    }

    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

});
