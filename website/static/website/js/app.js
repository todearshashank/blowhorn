/*
	Variables for countdown
	If you have the countdown active on the site, change the following variables.

 */

/* Year */	var cdYear = 2013;
/* Month */	var cdMonth = 12;
/* Day */	var cdDay = 25;

/*===========================================================================*/
/* Start of js for site */

/* Viewport plugin */
(function($){$.belowthefold=function(element,settings){var fold=$(window).height()+$(window).scrollTop();return fold<=$(element).offset().top-settings.threshold;};$.abovethetop=function(element,settings){var top=$(window).scrollTop();return top>=$(element).offset().top+$(element).height()-settings.threshold;};$.rightofscreen=function(element,settings){var fold=$(window).width()+$(window).scrollLeft();return fold<=$(element).offset().left-settings.threshold;};$.leftofscreen=function(element,settings){var left=$(window).scrollLeft();return left>=$(element).offset().left+$(element).width()-settings.threshold;};$.inviewport=function(element,settings){return!$.rightofscreen(element,settings)&&!$.leftofscreen(element,settings)&&!$.belowthefold(element,settings)&&!$.abovethetop(element,settings);};$.extend($.expr[':'],{"below-the-fold":function(a,i,m){return $.belowthefold(a,{threshold:0});},"above-the-top":function(a,i,m){return $.abovethetop(a,{threshold:0});},"left-of-screen":function(a,i,m){return $.leftofscreen(a,{threshold:0});},"right-of-screen":function(a,i,m){return $.rightofscreen(a,{threshold:0});},"in-viewport":function(a,i,m){return $.inviewport(a,{threshold:0});}});})(jQuery);


$(document).ready(function(){

	//if($('#google-map').size() > 0){ setupMap(); }

	/*================ Stretch Armstrong ================*/

	var backgrounds = $('div.backgrounds');
	backgrounds.stretcharmstrong({
		'rotate'     : true,
		'rotate_interval'   : 5000,
		'transition' : {
		    'type'        : 'fade',
		    'duration'    : 1500,
		    'orientation' : 'horizontal'
		},
		'element'    : 'img',
		'resize'     : true,
		'background' : true
	});

	/*================ Header scrolling ================*/

	$(window).bind('scroll', function(){
		if($(window).width() < 768){ return; }
		$('.slider.large').css({ 'margin-top' : $(document).scrollTop() * 0.9 });
		$('.slider.small').css({ 'margin-top' : $(document).scrollTop() * 0.6 });
	});

	/*================ Countdown Timer ================*/

	$('.countdown').countdown({
		until: new Date(cdYear, cdMonth-1, cdDay),
		compact: true,
		layout: '<div class="cd-no"><span class="time d">{dn}</span> <span class="desc">Days</span></div> <div class="cd-no"><span class="time h">{hnn}</span> <span class="desc">Hours</span></div> <div class="cd-no"><span class="time m">{mnn}</span> <span class="desc">Minutes</span></div> <div class="cd-no"><span class="time s">{snn}</span><span class="desc">Days</span></div>',
	});

	/*================ Accordian ================*/

	$('.acc-content').live('click', function(e){
		e.preventDefault();
		if ($(this).hasClass('is-active')) { return; };
		$('.acc-content.is-active').removeClass('is-active');
		$(this).addClass('is-active');
	});

	/*================ Select Styling ================*/

	$('select').formstyle();

	/*================ Header Slider Animations ================*/

	var slideCount = $('.small .slides').find('img').length;
	var imgIndex = 1;

	var slideWidthSml = 216;
	var slideWidthLrg = 498;

	/* Set the width of the slides container */
	var sliderWidthSml = slideWidthSml * slideCount + 10;
	$('.slider.small .slides').width(sliderWidthSml);

	var sliderWidthLrg = slideWidthLrg * slideCount + 12;
	$('.slider.large .slides').width(sliderWidthLrg);

	/* Function for switching out the image */
	function switchImages(direction){
		if($('.slider').is(':animated')){ return; }
		$('.slider-control a.disabled').removeClass('disabled');

		if (direction == 'next') {
			$('.small .slides').animate({ "left": '-='+slideWidthSml });
			$('.large .slides').animate({ "left": '-='+slideWidthLrg });
			if (imgIndex == slideCount) { $('.slider-control a.next').addClass('disabled'); }
			imgIndex ++;
		} else { //previous
			$('.small .slides').animate({ "left": '+='+slideWidthSml });
			$('.large .slides').animate({ "left": '+='+slideWidthLrg });
			imgIndex --;
			if (imgIndex == 1) { $('.slider-control a.prev').addClass('disabled'); }
		}
	}

	/* Event capture on the slider control links */
	$('.slider-control a').live('click', function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled')) { return; }
		switchImages($(this).attr('id'));
	});

	/*================ Lazy Loading of elements ================*/

	$(window).bind('scroll', function(){ $('.fadein:in-viewport').css({ 'opacity': 1, top : 0 }); });
	$('.fadein:in-viewport').css({ 'opacity': 1, top : 0 });

	/*================ Sliders in content ================*/

	var slideBlock;
	var sliderWidth;
	var slideCount;
	var slideIndex;

	function getSlideHeight(slide) {
		var slideHeight = 0;
		if ($(window).width() > 768) {
			slide.find('.one-half').each(function(){
				if($(this).outerHeight() > slideHeight){ slideHeight = $(this).outerHeight(); }
			});
		} else {
			slide.find('.one-half').each(function(){
				slideHeight += $(this).outerHeight();
			});
		}
		return slideHeight + parseInt(slide.css("padding-top")) + parseInt(slide.css("padding-bottom"));
	}

	function slideHeight(slide){
		var slideHeight = getSlideHeight(slide);
		slide.animate({height : slideHeight});
		$('.slider-block, .slide-mask').animate({height : slideHeight});
	}

	function sliderInit(){
		slideBlock = $('.slider-block');
		sliderWidth = $('.slide-mask').width();
		slideCount = slideBlock.find('.slide').length;
		slideIndex = 1;

		/* Setup slider widths */
		$('.slides').width(slideCount * sliderWidth);
		$('.slider-block .slide').width(sliderWidth);

		slideHeight($('.slide').first());

		/* Resets */
		$('.slides').css({ "left" : 0 });
		$('.slider-block a.slide-control.next.disabled').removeClass('disabled');
		$('.slider-block a.slide-control.prev').addClass('disabled');
	}

	function changeSlider(direction){
		$('.slider-block a.slide-control.disabled').removeClass('disabled');


		if (direction == 'next') {
			$('.slider-block .slides').animate({ "left": '-='+sliderWidth });
			slideIndex ++;
			if (slideIndex == slideCount) { $('.slider-block a.slide-control.next').addClass('disabled'); }

			slideHeight($('.slide').eq(slideIndex - 1));

		} else { //previous
			$('.slider-block .slides').animate({ "left": '+='+sliderWidth });
			slideIndex --;
			if (slideIndex == 1) { $('.slider-block a.slide-control.prev').addClass('disabled'); }

			slideHeight($('.slide').eq(slideIndex - 1));
		}
	}

	/* Initialize slider */
	$(window).load(function(){ if($('.slider-block').size() > 0) { sliderInit(); } });
	$(window).resize(function(){ if($('.slider-block').size() > 0) { sliderInit(); } });

	/* Event capture */

	$('.slider-block a.slide-control').live('click', function(e){
		e.preventDefault();
		if ($(this).hasClass('disabled')) { return; }
		changeSlider($(this).attr('id'));
	});

	/*================ Skills sliders ================*/

	$('.skills li').each(function(){
		$(this).before('<div class="skill-title">'+$(this).html()+'</div>');
		$('<span class="percent-bar" />').appendTo($(this));
		$(this).find('.percent-bar').html($(this).data('skills-percent')+'%');
	});

	function showSkills(){
		$('.skills li:in-viewport').each(function(){
			if (!$(this).find('.percent-bar').hasClass('animatedIn')) {
				$(this).find('.percent-bar').animate({ 'width' : $(this).data('skills-percent')+'%' }, 2500).addClass('animatedIn');
			}
		});
	}
	showSkills();

	/* Animate the bars in when they come into view. */
	$(window).bind('scroll', function() { showSkills(); });

});

function showAnimation(id, msg) {
    if (document.getElementById(id) == null) {
        $("body").append('<div class="modal fade" id="'
        + id
        + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
        + '<div class="modal-dialog">'
        + '<div class="modal-content">'
        + '<div class="modal-body">'
        + '<div id="wait">'
        + '<img src="/static/img/loading.gif" width="50" height="50" /> <span id="waitText">'
        + msg + '</span>' + '</div>' + '</div>'
        + '</div><!-- /.modal-content -->'
        + '</div><!-- /.modal-dialog -->'
        + '</div><!-- /.modal -->');
    }
    else {
        $('#waitText').text(msg);
    }
    $("#" + id).modal({ backdrop: 'static', keyboard: false });
    $("#" + id).modal('show');
}

function hideAnimation(id) {
    $("#" + id).modal('hide');
}
