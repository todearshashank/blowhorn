angular.module("parcelTrackingApp").directive("blowhornFooter", function () {
    return {
        scope: {},
        restrict: "E",
        templateUrl: "/static/website/html/home/footer.html",
        link: function(scope) {
            scope.currentYear = new Date().getFullYear();
        }
    };
});
