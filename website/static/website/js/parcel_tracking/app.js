angular.module('parcelTrackingApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate']);

angular.module('parcelTrackingApp').config(function ($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: '/static/website/html/parcel_tracking/main.html',
        controller: 'parcelTrackingController'
    }).
    otherwise({
        redirectTo: '/'
    });
});
