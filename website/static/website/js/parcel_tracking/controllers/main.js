angular
    .module('parcelTrackingApp')
    .controller('parcelTrackingController', parcelTrackingController);

function parcelTrackingController($scope) {
    $scope.ref_number = refNumber;
    $scope.rowCollection = orderDetails;
    console.log('orderDetails: ', orderDetails);
    $scope.hasData = Object.keys(orderDetails).length;
    $scope.displayedCollection = $scope.rowCollection;
    $scope.showContent = currentBuild === 'blowhorn';
    $scope.logoWidth = $scope.showContent ? '120px': '44px';
    $scope.logoUrl = `/static/website/images/${currentBuild}/logo/logo.png`;

    const CONTENT = {
        blowhorn: {
            title: 'Linking the World.',
            subtitle: 'One City at a time!',
            body: {
                p1: `We are focussed on building a system to make
                    all things logistics efficient.
                    To achieve this, we have built the best intra city transportation
                    network, which
                    will be augmented by a class apart leading technology to deliver
                    goods economically.`,
                p2: `We want to build Earth’s best intra-city logistics company, by
                    solving the
                    toughest problems for our customers. In 2014, when we started
                    exploring the intra-city
                    logistics market in India, it was fragmented and inefficient. There
                    was no standardised pricing,
                    or processes. Technology could be used to create an asset light,
                    efficient, logistics
                    marketplace. We saw a need for a different kind of a company to
                    handle large logistics problems
                    of traditional and new age businesses. We believed that solving for
                    India, is solving for the world. That is why we founded Blowhorn.`
            }
        },
        mzansigo: {
            title: 'Moving Mzansi forward',
            subtitle: 'One city at a time!',
            body: {
                p1: `We are set-out to build and implement Africa’s best intra-city logistics company by providing unique solutions to tough problems. We leverage technology and value-adding partnerships to provide cost-effective solutions. Technology is used by MzansiGo to create an asset-light, logistics and distribution marketplace. Standardising a very informal and disorganised market is no easy feat, but we at MzansiGo believe this is possible and would inevitably lead to the most cost efficient solutions for the end-client. MzansiGo is already making waves in the moving of household and furniture items within Cape Town and beyond, but our horizons have widened to include solutions within the broader logistics and distribution space for a multitude of clients seeking to move away from primitive technology.`,
                p2: ''
            }
        }
    }
    $scope.content = CONTENT[currentBuild];
    $scope.$applyAsync();
}
