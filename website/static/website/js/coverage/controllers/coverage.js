angular.module("coverageApp").controller("coverageController", function ($scope, $http, $filter, coverageService) {

    $scope.city = {};
    $scope.rowCollection = [];
    $scope.displayedCollection = [];
    var map;
    var bounds;
    var polygon;
    var polygon_path;
    var drawingManager;
    var autocomplete;
    var selectedShape;
    var mapOptions = {
        zoom: 10
    };
    var polyOptions = {
        strokeWeight: 0,
        fillOpacity: 0.45,
        editable: true,
        draggable: true
    };
    $scope.phoneregEx = /^\+\d{8,12}$/;

    coverageService.fetchCoverageCities().then(function (data) {
        var data = data.features;
        $scope.rowCollection = data;
        $scope.displayedCollection = data;
        $scope.$applyAsync();
    }).catch(function (data) {
        toastr.error(data.responseText, "Failed");
    });

    function clearSelection() {
        if (selectedShape) {
            selectedShape.setEditable(false);
            selectedShape = null;
        }
    }

    function deleteSelectedShape() {
        if (selectedShape) {
            selectedShape.setMap(null);
        }
    }

    function setSelection(shape) {
        if (selectedShape) {
            selectedShape.setMap(null);
        }
        selectedShape = shape;
        shape.setEditable(true);
    }

    function displayCoordinates(shape) {
        $scope.city.coordinates = shape.getArray().toString();
        $scope.$applyAsync();
    }

    function changedAutoCompletePlace(place) {
        $scope.city.coordinates = '';
        $scope.$applyAsync();
        deleteSelectedShape();
        drawingManager.setDrawingMode('polygon');
        polygon_path = null;
    }

    function setupDrawingManager() {
        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon']
            },
            polygonOptions: polyOptions,
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
            if (e.type == google.maps.drawing.OverlayType.POLYGON) {
                // Switch back to non-drawing mode after drawing a shape.
                drawingManager.setDrawingMode(null);
                setSelection(e.overlay);
                polygon_path = e.overlay.getPath();
                displayCoordinates(polygon_path);
                var newShape = e.overlay;
                newShape.type = e.type;
                draggedCoordinates(newShape);
                map.data.add(new google.maps.Data.Feature({
                    geometry: new google.maps.Data.Polygon([e.overlay.getPath().getArray()])
                }));
            }
        });
        google.maps.event.addListener(drawingManager, 'drawingmode_changed', function() {
            deleteSelectedShape();
            $scope.city.coordinates = '';
            $scope.$applyAsync();
            polygon_path = null;
        });
    }

    function draggedCoordinates(polygon) {
        google.maps.event.addListener(polygon.getPath(), 'set_at', function(s) {
            polygon_path = polygon.getPath();
            displayCoordinates(polygon_path);
        });
        google.maps.event.addListener(polygon.getPath(), 'insert_at', function(i) {
            polygon_path = polygon.getPath();
            displayCoordinates(polygon_path);
        });
    }

    $scope.addCoverageCity = function () {
        $scope.city_form.$setPristine();
        $scope.city_form.$setUntouched();
        $scope.city = {};
        $scope.city.name = '';
        $scope.city.coordinates = [];
        $scope.city.contact = '';
        $scope.city.start_time = '';
        $scope.city.end_time = '';
        $scope.city.method = 'POST';
        $scope.updateCity = false;
        $('#pac-input').val('');
        $("#save_city").prop("disabled", false).html("Save");
        map = initMap(document.getElementById("google-map"), mapOptions);
        setupDrawingManager();
        autocomplete = initAutoComplete(map, document.getElementById('pac-input').cloneNode());
        addAutoCompleteListener(map, autocomplete, changedAutoCompletePlace);
        $("#coverage_modal").on("shown.bs.modal", function () {
            google.maps.event.trigger(map, "resize");
            map.setCenter(getDefaultLatLng());
        });
        $('#coverage_modal').modal({ backdrop: 'static', keyboard: true });
        $('#coverage_modal').modal('show');
    };

    function getFormattedCoordinates() {
        var coordinates_list = [];
        if (polygon_path) {
            polygon_path.forEach(function (loc) {
                coordinates_list.push([parseFloat(loc.lng().toFixed(5)), parseFloat(loc.lat().toFixed(5))]);
            });
            coordinates_list.push(coordinates_list[0]);
        }
        return [coordinates_list];
    }

    function getFormattedGeometry() {
        return {
            "type": "Polygon",
            "coordinates": getFormattedCoordinates()
        }
    }

    $scope.updateCoverageCity = function (row) {
        console.log(row);
        $scope.city_form.$setPristine();
        $scope.city_form.$setUntouched();
        $('#pac-input').val('');
        $('#save_city').prop('disabled', false).html('Save');
        $scope.updateCity = true;
        $scope.city.method = 'PUT';
        map = initMap(document.getElementById("google-map"), mapOptions);
        $scope.city.id = angular.copy(row.id);

        var city = angular.copy(row);
        var properties = city.properties;
        $scope.city.name = properties.name;
        $scope.city.contact = properties.contact;
        $scope.city.address = properties.address;
        $('#booking_start_time').val(properties.start_time);
        $('#booking_end_time').val(properties.end_time);
        $scope.city.geometry = row.geometry;

        setupDrawingManager();
        drawingManager.setDrawingMode(null);
        autocomplete = initAutoComplete(map, document.getElementById('pac-input').cloneNode());
        addAutoCompleteListener(map, autocomplete, changedAutoCompletePlace);
        var coordinate_list = [];
        bounds = new google.maps.LatLngBounds();
        var coordinates = $scope.city.geometry.coordinates[0];
        for (i = 0; i < coordinates.length; i++) {
            coordinate_list.push(new google.maps.LatLng(coordinates[i][1], coordinates[i][0]));
            bounds.extend(new google.maps.LatLng(coordinates[i][1], coordinates[i][0]));
        }
        polygon = new google.maps.Polygon({
            paths: coordinate_list,
        });
        polygon.setOptions(polyOptions);
        polygon.setMap(map);
        setSelection(polygon);
        polygon_path = polygon.getPath();
        if (polygon_path) {
            displayCoordinates(polygon_path);
        }
        if (polygon) {
            draggedCoordinates(polygon);
        }

        $('#coverage_modal').on('shown.bs.modal', function () {
            google.maps.event.trigger(map, 'resize');
            map.fitBounds(bounds);
        });
        $('#coverage_modal').modal({ backdrop: 'static', keyboard: true });
        $('#coverage_modal').modal('show');
    };

    function getCoordinateDict(loc) {
        return {'latitude': loc.lat().toFixed(5), 'longitude': loc.lng().toFixed(5)};
    }

    $scope.submitForm = function (isValid) {
        if (isValid) {
            if ($scope.city.name === '' || $scope.city.coordinates.length === 0) {
                toastr.warning("City name and Coordinates cannot be blank", "Failed");
                return;
            }
            $("#save_city").prop("disabled", true).html("<i class='fa fa-spinner fa-spin'></i> Saving");
            $scope.city.address.country = "IN"
            $scope.city.address.phone_number = $scope.city.contact;
            var params = {};
            var data = {
                'name': $scope.city.name,
                'coverage': getFormattedGeometry(),
                'contact': $scope.city.contact || '',
                'address': $scope.city.address,
                'start_time': $('#booking_start_time').val(),
                'end_time': $('#booking_end_time').val()
            };
            params['data'] = {'data': JSON.stringify(data)};
            params['url'] = "coverage/city/";
            params['method'] = "POST";
            if ($scope.updateCity) {
                 params['url'] += $scope.city.id + "/";
                 params['method'] = "PUT";
            }
            coverageService.addUpdateCoverageCity(params).then(function (data) {
                if ($scope.updateCity) {
                    for (i = 0; i < $scope.rowCollection.length; i++) {
                        if ($scope.rowCollection[i].id === data.id) {
                            angular.copy(data, $scope.rowCollection[i]);
                            break;
                        }
                    }
                } else {
                    $scope.rowCollection.unshift(data);
                }
                $scope.$applyAsync();
                toastr.success('City has been saved', 'Success');
                $('#coverage_modal').modal('hide');
            }).catch(function (data) {
                $("#save_city").prop("disabled", false).html("Save");
                toastr.error(data.responseText, "Failed");
            });
        }
    };
});
