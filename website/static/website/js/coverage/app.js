angular.module("coverageApp", ["ngRoute", "smart-table", "ui.bootstrap", "ngAnimate"]);

angular.module("coverageApp").config(function ($routeProvider) {
    $routeProvider.
    when("/", {
        templateUrl: "/static/blowhorn/html/coverage/coverage.html",
        controller: "coverageController"
    }).
    otherwise({
        redirectTo: "/"
    });
});
