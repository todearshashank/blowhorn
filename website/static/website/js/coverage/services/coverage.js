angular.module("coverageApp").factory("coverageService", function ($q, $rootScope, $http, $location) {

	var factory = {};

	factory.fetchCoverageCities = function () {
        var defer = $q.defer();
        $http({
            url: "coverage/city",
            method: "GET",
            dataType: "json"
        }).success(function (data) {
            defer.resolve(data);
        }).error(function (data) {
            defer.reject(data);
        });
        return defer.promise;
    };

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    factory.addUpdateCoverageCity = function (params) {
        var defer = $q.defer();
        $.ajax({
            url: params.url,
            type: params.method,
            data: params.data,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            },
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };
    return factory;
});
