angular.module("coverageApp").directive("addUpdateCoverage", function () {
    return {
        restrict:"E",
        templateUrl:"/static/blowhorn/html/coverage/add_update_coverage.html"
    };
});

angular.module("coverageApp").directive("coverageTable", function () {
    return {
        link: function (scope, elem, attrs, ctrl) {
            $(elem).stickyTableHeaders({fixedOffset: 108});
        }
    }
});

// if you are using bootstrap time-picker uncomment bellow lines
/* --------------------- Start
angular.module("coverageApp").directive("timePicker", function() {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModelCtrl) {
            $(element).timepicker({
                maxHours: 24,
                showMeridian: false
            });
        }
    }
});
----------------------- end */
