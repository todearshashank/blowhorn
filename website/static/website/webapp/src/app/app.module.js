
var appPath = '/static/website/webapp/src/app';
(function() {
    'use strict';

    angular.module('app', [
        'app.core',
        'app.widgets',
        'app.layout',
        'app.myaccount',
    ]);

})();