(function() {
    'use strict';

    var pathPrefix = '/static/website/webapp/src/app';
    angular
        .module('app.myaccount')
        .run(appRun);

    // appRun.$inject = ['routehelper']

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [{
            url: '/',
            config: {
                templateUrl: `${pathPrefix}/myaccount/myaccount.html`,
                controller: 'MyAccountController',
                controllerAs: 'vm',
                title: 'My Account',
                settings: {
                    nav: 1,
                    content: '<i class="fa fa-lock"></i> My Account'
                }
            }
        }];
    }
})();
