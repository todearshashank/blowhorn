(function() {
    'use strict';

    const REQUIRED_MINIMUM_BALANCE = 300;

    angular
        .module('app.myaccount')
        .controller('MyAccountController', MyAccountController);

    /* @ngInject */
    function MyAccountController(dataservice, logger, $uibModal, $filter) {
        /*jshint validthis: true */
        var vm = this;
        vm.transactions = [];
        vm.amount = 0.00;
        vm.payment_method = [];
        vm.title = 'My Account Payment';
        vm.default_currency = default_currency;
        vm.currencySymbol = $filter('formatCurrency')(default_currency);
        vm.showRupeeIcon = current_build === 'blowhorn';
        vm.show = {
            low_balance: false,
            loading_profile: false,
            loading_transactions: true,
        }
        vm.profile = {};
        getProfile();
        getTransactions();

        function getProfile() {
            vm.show.loading_profile = true;
            return dataservice
                .getProfile()
                .then(handleProfileSuccess);

            function handleProfileSuccess(data) {
                vm.profile = data;
                vm.show.loading_profile = false;
            }
        }

        function getTransactions() {
            vm.show.loading_transactions = true;
            return dataservice
                .getTransactions()
                .then(handleSuccess);

            function handleSuccess(data) {
                vm.available_balance = data.available_balance;
                vm.show.low_balance = vm.available_balance < REQUIRED_MINIMUM_BALANCE;
                vm.transactions = data.transaction_info;
                vm.show.loading_transactions = false;
                return data;
            }
        }

        vm.openRachargeModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: `${appPath}/myaccount/recharge.modal.html?v=1.0.1`,
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl',
                size: 'md',
                resolve: {
                    profile: function () {
                        return vm.profile;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                vm.available_balance = data.available_balance;
                vm.transactions = data.transaction_info;
                vm.show.low_balance = vm.available_balance < REQUIRED_MINIMUM_BALANCE;
                swal('Success!',
                    `${vm.currencySymbol}${data.amount_added} has been credited successfully`,
                    'success'
                );
            }, function () {
                // logger.info('Modal dismissed at: ' + new Date());
            });
        };
    }
})();

(function() {
    'use strict';
    angular
        .module('app.myaccount')
        .controller('ModalInstanceCtrl', ModalInstanceCtrl);

    function ModalInstanceCtrl($uibModalInstance, profile, logger, dataservice, helperService) {
        var $ctrl = this;
        $ctrl.name = 'Recharge Wallet';
        $ctrl.profile = profile;
        $ctrl.amountToAdd = '';
        $ctrl.amountOptions = [300, 500, 1000];
        $ctrl.availablePaymentMethods = payment_methods;
        $ctrl.default_currency = default_currency;
        $ctrl.mxAllowedRechargeAmt = mxAllowedRechargeAmt;
        $ctrl.selectedMethod = null;
        $ctrl.updatingRazorpayStatus = false;
        $ctrl.show = {
            paytm: payment_methods.indexOf('paytm') != -1,
            razorpay: payment_methods.indexOf('razorpay') != -1,
            payfast: payment_methods.indexOf('payfast') != -1
        }

        $ctrl.selectAmountOption = function (amount) {
            $ctrl.amountToAdd = $ctrl.amountToAdd ? $ctrl.amountToAdd + amount : amount;
        }

        $ctrl.initRazorpayTransaction = function () {
            let orderNumber = helperService.generateOrderNumber(profile.cid);
            var amount = $ctrl.amountToAdd;
            if (!$ctrl.amountToAdd || $ctrl.amountToAdd <= 0 || $ctrl.updatingRazorpayStatus) {
                return;
            }
            if (amount > $ctrl.mxAllowedRechargeAmt) {
                console.log('Amount exceeds allowed limit');
                return;
            }
            var options = {
                "key": rzpid,
                "amount": amount * 100,
                "name": "blowhorn",
                "description": "Logistic Services",
                "image": "/static/img/logo.png",
                "prefill": {
                  "name": profile.name,
                  "email": profile.email,
                  "contact": profile.mobile
                },
                "notes": {
                    'booking_key': orderNumber,
                    'type': 'booking',
                },
                "handler": function (response) {
                    let data = {
                        booking_key: orderNumber,
                        txn_id: response.razorpay_payment_id,
                        amount: amount * 100,
                        type: 'booking',
                    };
                    $ctrl.updatingRazorpayStatus = true;
                    dataservice
                        .updateRazarpayResponse(data, true)
                        .then(handleSuccess)
                        .catch(handleFailure)

                    function handleSuccess(data) {
                        $ctrl.updatingRazorpayStatus = false;
                        data['amount_added'] = amount;
                        $uibModalInstance.close(data);
                    }

                    function handleFailure(data) {
                        $ctrl.updatingRazorpayStatus = false;
                        swal('Failed!',
                            `Payment failed. Try again or use different payment method.`,
                            'error'
                        );
                    }
                },
            };
            var rzp1 = new Razorpay(options);
            rzp1.open();
        };

        $ctrl.initPaytmTransaction = function () {
            if (!$ctrl.amountToAdd || $ctrl.amountToAdd <= 0 || $ctrl.updatingRazorpayStatus) {
                return;
            }
            if ($ctrl.amountToAdd > $ctrl.mxAllowedRechargeAmt) {
                console.log('Amount exceeds allowed limit');
                return;
            }
            let orderNumber = helperService.generateOrderNumber(profile.cid);
            window.location.href = `api/paytm/?entity_type=order&order_number=${orderNumber}&amount=${$ctrl.amountToAdd}&add_balance=true&next=/myaccount?add_balance=true`;
        };

        $ctrl.initPayfastTransaction = function () {
            let orderNumber = helperService.generateOrderNumber(profile.cid);
            window.location.href = `api/payfast/checkout/?entity_type=order&order_number=${orderNumber}&amount=${$ctrl.amountToAdd}&add_balance=true`;
        };

        $ctrl.selectPaymentMethod = function (selectedMethod) {
            logger.info(`selectedMethod: ${selectedMethod}`);
            $ctrl.selectedMethod = selectedMethod;
        };

        $ctrl.suppress_event = function (event) {
            event.stopPropagation();
            return false;
        }

        $ctrl.ok = function () {
            $uibModalInstance.close($ctrl.selectedMethod);
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
