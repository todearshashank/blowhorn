angular.module('app').filter('formatCurrency', formatCurrency);

function formatCurrency() {
    const getNavigatorLanguage = () => (navigator.languages && navigator.languages.length) ? navigator.languages[0] : navigator.userLanguage || navigator.language || navigator.browserLanguage || 'en-IN';
    function getCurrencySymbol (locale, currency) {
        locale = locale || 'en-IN';
        currency = currency || 'INR';
        return (0).toLocaleString(
            locale,
            {
                style: 'currency',
                currency: currency,
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            }
        ).replace(/\d/g, '').trim()
    }
    return function (currency) {
        if (currency === 'ZAR') {
            // locale-string not converting to symbol for all currency
            return 'R';
        }
        return getCurrencySymbol(getNavigatorLanguage(), currency);
    }
}

