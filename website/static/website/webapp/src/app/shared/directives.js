angular
    .module('app')
    .directive('numbersOnly', numbersOnly);

function numbersOnly() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}


angular
    .module('app')
    .directive('miniLoader', miniLoader);

function miniLoader() {
    // slSize: font-size
    return {
        restrict: 'EA',
        template: '<div class="loader"></div>',
        scope: {
            // slVisible: '=',
            slSize: '='
        },
        link: function (scope, element, attrs) {
            /* Will handle the Size */
            scope.$watch(scope.slSize, function () {
                if (attrs.slsize) {
                    element[0].style.fontSize = attrs.slsize;
                }
            });
        }
    };
}