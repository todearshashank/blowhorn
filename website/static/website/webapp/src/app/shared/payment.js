angular
    .module('app')
    .factory('paymentService', paymentService);

/* @ngInject */
function paymentService(dataservice) {
    var service = {
        initRazorpayTransaction: initRazorpayTransaction
    };
    return service;

    function initRazorpayTransaction(profile, amount, booking_key, addingMoney) {
        console.log(profile, amount, booking_key, addingMoney);
        var options = {
            "key": rzpid,
            "amount": amount,
            "name": "blowhorn",
            "description": "Logistic Services",
            "image": "/static/img/logo.png",
            "prefill": {
              "name": profile.name,
              "email": profile.email,
              "contact": profile.mobile
            },
            "notes": {
              'type': 'booking',
            },
            "handler": addingMoney ? addMoneyHandler : defaultHandler,
        };
        var rzp1 = new Razorpay(options);
        console.log(rzp1);
        rzp1.open();

        function addMoneyHandler(response) {
            let data = {
                booking_key: booking_key,
                txn_id: response.razorpay_payment_id,
                amount: amount,
                type: 'booking',
            };
            dataservice
                .updateRazarpayResponse(data, true)
                .then(handleSuccess)
                .catch(handleFailure);

            function handleSuccess(data) {
                return data;
            }

            function handleFailure(data) {
                return data;
            }
        }

        function defaultHandler(response) {
            let data = {
                booking_key: booking_key,
                txn_id: response.razorpay_payment_id,
                amount: amount,
                type: 'booking',
            };
            dataservice
                .updateRazarpayResponse(data)
                .then(handleSuccess);
        }
    }
    
}
