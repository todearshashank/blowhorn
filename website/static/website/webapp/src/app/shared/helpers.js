angular
    .module('app')
    .factory('helperService', helperService);

/* @ngInject */
function helperService() {
    const ORDER_NUMBER_LENGTH = 10;
    var service = {
        generateOrderNumber: generateOrderNumber,
        storageAvailable: storageAvailable,
        getCookie: getCookie,
        removeItem: removeItem,
        eraseCookie: eraseCookie,
        setValueToLocalStorage: setValueToLocalStorage,
        getValueFromLocalStorage: getValueFromLocalStorage
    };
    return service;

    function generateOrderNumber(customerId) {
        var ceilLength = ORDER_NUMBER_LENGTH - customerId.toString().length - 1;
        var randomStr = Math.random().toString(36).substring(2, ceilLength) + Math.random().toString(36).substring(2, ceilLength);
        return `${customerId}${randomStr}`.toUpperCase();
    }

    function storageAvailable(type) {
        // Check if local-storage is available.
        // Sometimes user might have disabled browser storage.
        try {
            var storage = window[type];
            var x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        } catch (e) {
            return e instanceof DOMException && (
                // everything except Firefox
                e.code === 22 ||
                // Firefox
                e.code === 1014 ||
                // test name field too, because code might not be present
                // everything except Firefox
                e.name === 'QuotaExceededError' ||
                // Firefox
                e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
                // acknowledge QuotaExceededError only if there's something already stored
                storage.length !== 0;
        }
    }

    function setValueToLocalStorage (key, val) {
        localStorage.setItem(key, val);
    }

    function getValueFromLocalStorage (key) {
        return localStorage.getItem(key);
    }

    function removeItem(key) {
        localStorage.removeItem(key);
    }

    function eraseCookie(name) {
        document.cookie = name+'=; Max-Age=-99999999;';
    }

    function getCookie(name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        }
        else
        {
            begin += 2;
            var end = document.cookie.indexOf(";", begin);
            if (end == -1) {
            end = dc.length;
            }
        }
        // because unescape has been deprecated, replaced with decodeURI
        //return unescape(dc.substring(begin + prefix.length, end));
        return decodeURI(dc.substring(begin + prefix.length, end));
    }

}
