
'use strict';
angular.module('app.core')
    .config(toastrConfig)
    .value('config', config)
    .config(configure);

/* @ngInject */
function toastrConfig(toastr) {
    toastr.options.timeOut = 4000;
    toastr.options.positionClass = 'toast-bottom-right';
}

var config = {
    appErrorPrefix: '[blowhorn webapp Error] ', //Configure the exceptionHandler decorator
    appTitle: 'blowhorn webapp',
    version: '1.0.0'
};

/* @ngInject */
function configure ($logProvider, $routeProvider, $mdThemingProvider, 
    routehelperConfigProvider, exceptionHandlerProvider) {
        
    // Theme
    $mdThemingProvider.definePalette('blowhorn', {
        '50': '0088a8',
        '100': '0088a8',
        '200': '0088a8',
        '300': '0088a8',
        '400': '0088a8',
        '500': '0088a8',
        '600': '0088a8',
        '700': '0088a8',
        '800': '0088a8',
        '900': '0088a8',
        'A100': '0088a8',
        'A200': '0088a8',
        'A400': '0088a8',
        'A700': '0088a8',
        'contrastDefaultColor': 'light',   
        'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
        'contrastLightColors': undefined
    });
    $mdThemingProvider
        .theme('default')
        .primaryPalette('blowhorn')
        // .accentPalette('orange');

    // turn debugging off/on (no info or warn)
    if ($logProvider.debugEnabled) {
        $logProvider.debugEnabled(true);
    }

    // Configure the common route provider
    routehelperConfigProvider.config.$routeProvider = $routeProvider;
    routehelperConfigProvider.config.docTitle = 'blowhorn webapp: ';
    var resolveAlways = { /* @ngInject */
        ready: function(dataservice) {
            return dataservice.ready();
        }
    };
    routehelperConfigProvider.config.resolveAlways = resolveAlways;

    // Configure the common exception handler
    exceptionHandlerProvider.configure(config.appErrorPrefix);
}
