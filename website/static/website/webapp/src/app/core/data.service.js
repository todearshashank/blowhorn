'use strict';
angular
    .module('app.core')
    .factory('dataservice', dataservice);

/* @ngInject */
function dataservice($http, $location, $q, exception, logger) {
    var isPrimed = false;
    var primePromise;
    const API_URL_PREFIX = 'api';
    const API_VERSION = 'v4';
    var profile = {};
    var service = {
        getTransactions: getTransactions,
        updateRazarpayResponse: updateRazarpayResponse,
        getProfile: getProfile,
        ready: ready,
        profile: profile
    };
    return service;

    function getProfile() {
        let options = {
            url: `${API_URL_PREFIX}/profile/`,
            method: "GET",
            dataType: "json",
        }
        return $http(options)
            .then(getProfileComplete)
            .catch(getProfileFailed);

        function getProfileComplete(data, status, headers, config) {
            profile = data.data.message;
            return data.data.message;
        }

        function getProfileFailed(message) {
            exception.catcher('XHR Failed for getProfileFailed')(message);
            $location.url('/');
        }
    }

    function getTransactions() {
        let options = {
            url: `${API_URL_PREFIX}/customers/${API_VERSION}/account/transaction`,
            method: "GET",
            dataType: "json",
        }
        return $http(options)
            .then(getTransactionsComplete)
            .catch(getTransactionsFailed);

        function getTransactionsComplete(data, status, headers, config) {
            return data.data;
        }

        function getTransactionsFailed(message) {
            exception.catcher('XHR Failed')(message);
            $location.url('/');
        }
    }

    function updateRazarpayResponse(data, isAddMoney) {
        var url = isAddMoney ?
            `${API_URL_PREFIX}/customers/${API_VERSION}/account/razorpay/status-check` :
            `${API_URL_PREFIX}/razorpay`;
        let options = {
            url: url,
            method: "POST",
            headers: {
                'X-CSRFToken': getCookieValue('csrftoken'),
            },
            dataType: "json",
            data: data,
        }
        return $http(options)
            .then(updateRazarpayResponseComplete)
            .catch(updateRazarpayResponseFailed);

        function updateRazarpayResponseComplete(data) {
            return data.data;
        }

        function updateRazarpayResponseFailed(data) {
            exception.catcher('XHR Failed')(data);
            $location.url('/');
        }
    }

    function prime() {
        // This function can only be called once.
        if (primePromise) {
            return primePromise;
        }

        primePromise = $q.when(true).then(success);
        return primePromise;

        function success() {
            isPrimed = true;
        }
    }

    function ready(nextPromises) {
        var readyPromise = primePromise || prime();
        return readyPromise
            .then(function() { return $q.all(nextPromises); })
            .catch(exception.catcher('"ready" function failed'));
    }

    function getCookieValue(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        var csrf = $("input[name='csrfmiddlewaretoken']").val();
        return cookieValue || csrf;
    }
}
