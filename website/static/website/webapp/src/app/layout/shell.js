(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('Shell', Shell);

    Shell.$inject = ['$timeout', '$filter', 'config', 'logger', 'dataservice', 'helperService'];

    function Shell($timeout, $filter, config, logger, dataservice, helperService) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = brandName;
        vm.logoUrl = `/static/website/images/${current_build}/logo/logo.png`;
        vm.dashboardUrl = isBusinessUser === 'True' ? '/dashboard' : '/dashboard/mytrips';
        vm.bookingUrl = current_build === 'blowhorn' ? '/solutions/on-demand-transportation' : '/'
        vm.busyMessage = 'Please wait ...';
        vm.isBusy = true;
        vm.showSplash = true;
        vm.profile = {};
        vm.showDropdown = false;
        getProfile();

        function getProfile() {
            return dataservice
                .getProfile()
                .then(handleProfileSuccess);

            function handleProfileSuccess(data) {
                vm.profile = data;
            }
        }

        activate();

        function activate() {
            hideSplash();
        }

        function hideSplash() {
            //Force a 1 second delay so we can see the splash.
            $timeout(function() {
                vm.showSplash = false;
            }, 1000);
        }

        vm.logoutUser = function () {
            const tokenKey = 'auth._token.local';
            if (helperService.storageAvailable('localStorage')) {
                helperService.removeItem(tokenKey)
            }
            const cookieExists = helperService.getCookie(tokenKey);
            if (cookieExists) {
                helperService.eraseCookie(tokenKey);
            }
            setTimeout(() => window.open('/accounts/logout', '_self'), 200)
        }

        vm.toggleDropdown = function () {
            vm.showDropdown = !vm.showDropdown;
        }

    }
})();
