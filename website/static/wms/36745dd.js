(window.webpackJsonp=window.webpackJsonp||[]).push([[118,62],{665:function(e,t,o){"use strict";o.r(t);var n=o(5),component=Object(n.a)({},(function(){var e=this,t=e.$createElement,o=e._self._c||t;return o("svg",{staticClass:"animate-spin",attrs:{xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24"}},[o("circle",{staticClass:"opacity-25",attrs:{cx:"12",cy:"12",r:"10",stroke:"currentColor","stroke-width":"4"}}),e._v(" "),o("path",{staticClass:"opacity-75",attrs:{fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"}})])}),[],!1,null,null,null);t.default=component.exports},706:function(e,t,o){var content=o(716);content.__esModule&&(content=content.default),"string"==typeof content&&(content=[[e.i,content,""]]),content.locals&&(e.exports=content.locals);(0,o(22).default)("c806b6fc",content,!0,{sourceMap:!1})},715:function(e,t,o){"use strict";o(706)},716:function(e,t,o){var n=o(21)((function(i){return i[1]}));n.push([e.i,".input-box[data-v-23ecbdd8]{margin-top:0.25rem;display:block;width:100%;border-radius:0.375rem;--tw-border-opacity:1;border-color:rgba(209, 213, 219, var(--tw-border-opacity));--tw-shadow:0 1px 2px 0 rgba(0, 0, 0, 0.05);box-shadow:var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow)}.input-box[data-v-23ecbdd8]:focus{--tw-border-opacity:1;border-color:rgba(165, 180, 252, var(--tw-border-opacity));--tw-ring-offset-shadow:var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);--tw-ring-shadow:var(--tw-ring-inset) 0 0 0 calc(3px + var(--tw-ring-offset-width)) var(--tw-ring-color);box-shadow:var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);--tw-ring-opacity:1;--tw-ring-color:rgba(199, 210, 254, var(--tw-ring-opacity));--tw-ring-opacity:0.5}button[data-v-23ecbdd8] > :not([hidden]) ~ :not([hidden]){--tw-space-x-reverse:0;margin-right:calc(0.5rem * var(--tw-space-x-reverse));margin-left:calc(0.5rem * calc(1 - var(--tw-space-x-reverse)))}button[data-v-23ecbdd8]{border-radius:0.375rem;background-image:linear-gradient(to bottom right, var(--tw-gradient-stops));font-weight:600;--tw-text-opacity:1;color:rgba(255, 255, 255, var(--tw-text-opacity))}",""]),n.locals={},e.exports=n},725:function(e,t,o){"use strict";o.r(t);o(47),o(30),o(28),o(15),o(63),o(29),o(64);var n=o(2),r=o(27);o(66),o(42);function l(object,e){var t=Object.keys(object);if(Object.getOwnPropertySymbols){var o=Object.getOwnPropertySymbols(object);e&&(o=o.filter((function(e){return Object.getOwnPropertyDescriptor(object,e).enumerable}))),t.push.apply(t,o)}return t}var c={props:{data:{type:Object},editMode:{type:Boolean}},data:function(){return{loading:!1,config:{name:null,level_one_name:null,level_one_value:1,level_three_name:null,level_two_name:null,ratio_level_two_one:null,ratio_level_three_two:null,id:null}}},methods:{closeModal:function(e){e.refresh?this.$emit("closeModal",{refresh:!0}):this.$emit("closeModal")},editAssignData:function(){this.data&&(this.config=function(e){for(var i=1;i<arguments.length;i++){var source=null!=arguments[i]?arguments[i]:{};i%2?l(Object(source),!0).forEach((function(t){Object(r.a)(e,t,source[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(source)):l(Object(source)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(source,t))}))}return e}({},this.data),this.config.level_one_value=1)},submitConfig:function(){var e=this;return Object(n.a)(regeneratorRuntime.mark((function t(){var o;return regeneratorRuntime.wrap((function(t){for(;;)switch(t.prev=t.next){case 0:return t.prev=0,e.loading=!0,e.config.name=e.config.level_one_name,o={method:"".concat(e.editMode?"put":"post"),url:"api/wms/v1/trackinglevel",data:e.config},t.next=6,e.$axios(o);case 6:e.loading=!1,e.$toast.success("".concat(e.editMode?"Updated Succesfully":"Added Succesfully")),e.$store.dispatch("getLookupData"),e.config={name:null,level_one_name:null,level_one_value:1,level_three_name:null,level_two_name:null,ratio_level_two_one:null,ratio_level_three_two:null},e.closeModal({refresh:!0}),t.next=18;break;case 13:t.prev=13,t.t0=t.catch(0),e.loading=!1,t.t0.response.data.message&&e.$toast.error(t.t0.response.data.message),t.t0.response.data?e.$toast.error(t.t0.response.data):e.$toast.error(t.t0);case 18:case"end":return t.stop()}}),t,null,[[0,13]])})))()}}},v=c,d=(o(715),o(5)),component=Object(d.a)(v,(function(){var e=this,t=e.$createElement,o=e._self._c||t;return o("modal",{attrs:{name:"new-tracking-level",height:"auto",adaptive:""},on:{opened:e.editAssignData}},[o("div",{staticClass:"px-4 py-3 flex items-center justify-between border-b"},[o("p",[e._v(e._s(e.editMode?"Edit":"Add New")+" Tracking Level")]),e._v(" "),o("button",{staticClass:"rounded-md bg-gray-100 btn-focus-ring h-8 w-8",on:{click:function(t){return e.$modal.hide("new-tracking-level")}}},[o("svg",{staticClass:"w-4 h-4 mx-auto text-gray-700",attrs:{fill:"none",stroke:"currentColor",viewBox:"0 0 24 24",xmlns:"http://www.w3.org/2000/svg"}},[o("path",{attrs:{"stroke-linecap":"round","stroke-linejoin":"round","stroke-width":"2",d:"M6 18L18 6M6 6l12 12"}})])])]),e._v(" "),o("form",{staticClass:"p-4 space-y-4",on:{submit:function(t){return t.preventDefault(),e.submitConfig.apply(null,arguments)}}},[o("div",{staticClass:"grid grid-cols-3 gap-4 text-sm font-semibold"},[o("p",[e._v("Level Name")]),e._v(" "),o("p",[e._v("Level Value")]),e._v(" "),o("p",[e._v("Level Result")])]),e._v(" "),o("div",{staticClass:"border-b"}),e._v(" "),o("div",{staticClass:"grid grid-cols-3 gap-4 content-center"},[o("input",{directives:[{name:"model",rawName:"v-model",value:e.config.level_one_name,expression:"config.level_one_name"}],staticClass:"form-input input-box",attrs:{type:"text",required:"",placeholder:"Level 1 Name"},domProps:{value:e.config.level_one_name},on:{input:function(t){t.target.composing||e.$set(e.config,"level_one_name",t.target.value)}}}),e._v(" "),o("input",{directives:[{name:"model",rawName:"v-model",value:e.config.level_one_value,expression:"config.level_one_value"}],staticClass:"form-input input-box",attrs:{type:"text",required:"",readonly:"",placeholder:"Level Value"},domProps:{value:e.config.level_one_value},on:{input:function(t){t.target.composing||e.$set(e.config,"level_one_value",t.target.value)}}}),e._v(" "),o("div",{staticClass:"flex items-center"},[e.config.level_one_name?o("p",{staticClass:"text-sm text-gray-600"},[e._v("\n          "+e._s(e.config.level_one_name)+" = "+e._s(e.config.level_one_value)+"\n        ")]):e._e()])]),e._v(" "),o("div",{staticClass:"grid grid-cols-3 gap-4 content-center"},[o("input",{directives:[{name:"model",rawName:"v-model",value:e.config.level_two_name,expression:"config.level_two_name"}],staticClass:"form-input input-box",attrs:{type:"text",required:"",placeholder:"Level 2 Name"},domProps:{value:e.config.level_two_name},on:{input:function(t){t.target.composing||e.$set(e.config,"level_two_name",t.target.value)}}}),e._v(" "),o("input",{directives:[{name:"model",rawName:"v-model",value:e.config.ratio_level_two_one,expression:"config.ratio_level_two_one"}],staticClass:"form-input input-box",attrs:{type:"number",min:"0",required:"",placeholder:"Level 2 Value"},domProps:{value:e.config.ratio_level_two_one},on:{input:function(t){t.target.composing||e.$set(e.config,"ratio_level_two_one",t.target.value)}}}),e._v(" "),o("div",{staticClass:"flex items-center"},[e.config.level_one_name&&e.config.level_two_name&&e.config.ratio_level_two_one?o("p",{staticClass:"text-sm text-gray-600"},[e._v("\n          1 "+e._s(e.config.level_two_name)+" = "+e._s(e.config.ratio_level_two_one)+"\n          "+e._s(e.config.level_one_name)+"\n        ")]):e._e()])]),e._v(" "),o("div",{staticClass:"grid grid-cols-3 gap-4 content-center"},[o("input",{directives:[{name:"model",rawName:"v-model",value:e.config.level_three_name,expression:"config.level_three_name"}],staticClass:"form-input input-box",attrs:{type:"text",required:"",placeholder:"Level 3 Name"},domProps:{value:e.config.level_three_name},on:{input:function(t){t.target.composing||e.$set(e.config,"level_three_name",t.target.value)}}}),e._v(" "),o("input",{directives:[{name:"model",rawName:"v-model",value:e.config.ratio_level_three_two,expression:"config.ratio_level_three_two"}],staticClass:"form-input input-box",attrs:{type:"number",min:"0",required:"",placeholder:"Level 2 Value"},domProps:{value:e.config.ratio_level_three_two},on:{input:function(t){t.target.composing||e.$set(e.config,"ratio_level_three_two",t.target.value)}}}),e._v(" "),o("div",{staticClass:"flex items-center"},[e.config.level_two_name&&e.config.ratio_level_two_one&&e.config.level_three_name&&e.config.ratio_level_three_two?o("p",{staticClass:"text-sm text-gray-600"},[e._v("\n          1 "+e._s(e.config.level_three_name)+" = "+e._s(e.config.ratio_level_three_two)+"\n          "+e._s(e.config.level_two_name)+"\n        ")]):e._e()])]),e._v(" "),o("button",{staticClass:"\n        rounded-md\n        py-3\n        bg-gradient-to-br\n        from-bh-primary\n        to-bh-primary-dark\n        shadow\n        flex-centered\n        space-x-2\n        text-white\n        font-semibold\n        w-full\n        btn-focus\n      "},[e.loading?o("CommonButtonSpinner",{staticClass:"h-5 w-5"}):e._e(),e._v(" "),o("p",[e._v(e._s(e.loading?"Saving...":"Save"))])],1),e._v(" "),o("div",{staticClass:"flex space-x-6 bg-gray-100 rounded-lg p-4 text-sm"},[o("img",{directives:[{name:"lazy-load",rawName:"v-lazy-load"}],staticClass:"h-6 w-6",attrs:{"data-src":"https://api.iconify.design/heroicons-outline:information-circle.svg?color=%238995a7",alt:"info"}}),e._v(" "),o("p",{staticClass:"text-gray-600 w-full"},[e._v("\n        You can have a hierarchical list of configs for e.g "),o("br"),e._v("\n        1 PALLET = 10 BOXES "),o("br"),e._v("\n        1 BOX = 10 EACHES\n      ")])])])])}),[],!1,null,"23ecbdd8",null);t.default=component.exports;installComponents(component,{CommonButtonSpinner:o(665).default})}}]);