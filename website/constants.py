from django.conf import settings


IOS_REGEX = r'^\S+\s+\((iPhone|iPad)'
ANDROID_REGEX = r'^\S+\s+\(Linux; Android'
GOOGLE_MAPS_API_KEY = {'google_map_api_key': settings.GOOGLE_MAPS_API_KEY}

AVAILABLE_PAYMENT_MODES = [{
    'mode': 'razorpay',
    'img': '',
    'label': 'Credit Card / Debit Card',
    'title': 'Credit Card / Debit Card / Net Banking / Others'
}, {
    'mode': 'paytm',
    'img': 'static/assets/Assets/Paytm.png',
    'label': ''
}]

BENGALURU = 'Bengaluru'
CHENNAI = 'Chennai'
DELHI = 'Delhi NCR'
MUMBAI = 'Mumbai'
HYDERABAD = 'Hyderabad'

LANGUAGE_CODES_FOR_TEMPLATES = ['en', 'kn', 'ta', 'te', 'hi', 'mr']

LANGUAGE_CODE_FIELD_MAPPING = {
    'en': 'img_english',
    'kn': 'img_kannada',
    'ta': 'img_tamil',
    'te': 'img_telugu',
    'hi': 'img_hindi',
    'mr': 'img_marathi'
}

SEO_DATA = {
    'mzansigo': {
        "theme_color": "#0068C9",
        "title": "MzansiGo | On-Demand Moving and Furniture Delivery",
        "keywords": "MzansiGo is a top moving company based in Cape Town, South Africa. We provide various services such as house moving service, furniture moving service, bakkie rental service etc.",
        "description": "MzansiGo is a top moving company based in Cape Town, South Africa. We provide various services such as house moving service, furniture moving service, bakkie rental service etc.",
        "og_title": "MzansiGo | On-Demand Moving and Furniture Delivery",
        "og_url": "https://booking.mzansigo.co.za/",
        "og_img": "https://mzansigo.co.za/homee.jpg",
        "og_description": "MzansiGo is a top moving company based in Cape Town, South Africa. We provide various services such as house moving service, furniture moving service, bakkie rental service etc.",
        "og_locale": "en_SA",
        "og_locale_alt": "en_US",
        "og_sitename": "mzansigo",
        "favicon_icon": "/static/website/images/mzansigo/favicon-32x32.png",
        "favicon_short_icon": "/static/website/images/mzansigo/favicon-32x32.png",
        "apple_touch_icon": "/static/website/images/mzansigo/apple-touch-icon-57x57.png"
    },
    'blowhorn': {
        "theme_color": "#00d2f6",
        "title": "blowhorn! Mini-Trucks on hire in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi.",
        "keywords": "blowhorn! Simpler, Swifter, Smarter; Tempo service, Book my Mini Truck, Transport within Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Transporter for Local Shifting, Tata Ace on Hire in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Hire Mini Truck in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Logistics Services in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Local Shifting in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Pick Up Vehicles, Door to Door Delivery, Online Pickup Booking, Schedule a Pickup, Book a Pickup, Pick Up and Drop Service, Shifting within Bengaluru/Hyderabad/Chennai/Mumbai/Delhi, Movers service",
        "description": "We are offering the most convenient way to move goods in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi with real time tracking and fixed pricing. Fast and zippy mini-trucks, pickup trucks, light trucks. Give us a try! Visit our website to make a booking.",
        "og_title": "blowhorn! Mini-Trucks on hire in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi",
        "og_url": "https://blowhorn.com/",
        "og_img": "https://storage.googleapis.com/blowhorn/logo.png",
        "og_description": "We are offering the most convenient way to move goods in Bengaluru/Hyderabad/Chennai/Mumbai/Delhi with real time tracking and fixed pricing. Fast and zippy mini-trucks, pickup trucks, light trucks. Give us a try! Visit our website to make a booking.",
        "og_locale": "en_IN",
        "og_locale_alt": "en_US",
        "og_sitename": "blowhorn",
        "favicon_icon": "/static/website/images/blowhorn/favicon-32x32.png",
        "favicon_short_icon": "/static/website/images/blowhorn/favicon-32x32.png",
        "apple_touch_icon": "/static/website/images/blowhorn/apple-touch-icon-57x57.png"
    }
}

# It should be configurable
DASHBOARD_CONFIG = {
    'IHCL': {
        'markers': {
            'pickup': {
                'show': False,
                'icon': ''
            },
            'dropoff': {
                'show': True,
                'icon': 'static/blowhorn/assets/markers/external/stop.png'
            },
            'stop': {
                'show': False,
                'icon': 'static/blowhorn/assets/markers/external/stop.png'
            },
            'truck_marker': 'static/blowhorn/assets/markers/external/bike@2x.png'
        },
        'show_additional_info': False,
        'show_only_map': True,
        'show_call_button': True
    }
}
