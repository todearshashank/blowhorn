from django.conf import settings
from django import template

register = template.Library()
assignment_tag = register.assignment_tag if hasattr(register, 'assignment_tag') else register.simple_tag


@assignment_tag(takes_context=True)
def website_build(context):
    return settings.CURRENT_WEBSITE_BUILD
