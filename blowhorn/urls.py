from django.conf.urls import url
from blowhorn.common.firebase_token import FirebaseToken
from django.urls import include, path

urlpatterns = [
        url(r'^firebasetoken/(?P<token_id>[\w-]+)$', FirebaseToken.as_view(), name='firebase-token'),
        url(r'^', include('blowhorn.address.urls')),
        url(r'^', include('blowhorn.apps.urls')),
        url(r'^', include('blowhorn.changelog.app')),
        url(r'^', include('blowhorn.company.urls')),
        url(r'^', include('blowhorn.contract.urls')),
        url(r'^', include('blowhorn.coupon.urls')),
        url(r'^', include('blowhorn.customer.urls')),
        url(r'^', include('blowhorn.driver.urls')),
        url(r'^', include('blowhorn.exotel.urls')),
        url(r'^', include('blowhorn.livetracking.urls')),
        url(r'^', include('blowhorn.mitra.urls')),
        url(r'^', include('blowhorn.order.urls')),
        url(r'^', include('blowhorn.payment.urls')),
        url(r'^', include('blowhorn.report.urls')),
        url(r'^', include('blowhorn.route_optimiser.urls')),
        url(r'^', include('blowhorn.trip.urls')),
        url(r'^', include('blowhorn.mitra.urls')),
        url(r'^', include('blowhorn.wms.urls')),
        url(r'^', include('blowhorn.shopify.urls')),
        url(r'^', include('blowhorn.integration_apps.urls')),
        url(r'^', include('blowhorn.vehicle.urls')),
        ]
