from django.db import connection
from django.conf import settings

from import_export.widgets import ForeignKeyWidget
from blowhorn.oscar.core.loading import get_model
from import_export import resources, fields
from blowhorn.utils.functions import utc_to_ist


OrderCODPayment = get_model('payment', 'OrderCODPayment')
Order = get_model('order', 'Order')
Trip = get_model('trip', 'Trip')
Customer = get_model('customer', 'Customer')


class OrderCODPaymentResource(resources.ModelResource):

    payment_id = fields.Field(
        attribute="id",
        column_name="COD Payment ID"
    )

    payment_date = fields.Field(
        column_name="Payment Date"
    )

    deposited_to = fields.Field(
        column_name="Deposited to"
    )

    customer = fields.Field(
        column_name="Customer"
    )

    city = fields.Field(
        column_name="City"
    )

    order_status = fields.Field(
        column_name="Status"
    )

    order = fields.Field(
        attribute="order",
        column_name="Order Number",
        widget=ForeignKeyWidget(Order, 'number')
    )

    customer_reference_number = fields.Field(
        column_name="Customer Reference Number"
    )

    reference_number = fields.Field(
        column_name="Reference Number"
    )

    date_placed = fields.Field(
        column_name="Date Placed"
    )

    delivery_hub = fields.Field(
        column_name="Delivery Hub"
    )

    cash_on_delivery = fields.Field(
        column_name="Cash On Delivery"
    )

    cash_collected = fields.Field(
        column_name="Cash Collected"
    )

    customer_name = fields.Field(
        column_name="Customer Name"
    )

    driver_vehicle = fields.Field(
        column_name="Driver Vehicle"
    )

    delivered_date = fields.Field(
        column_name="Delivered On"
    )

    trip = fields.Field(
        attribute="trip",
        column_name="Trip ID",
        widget=ForeignKeyWidget(Trip, 'trip_number')
    )

    is_settled_to_customer = fields.Field(
        attribute="is_settled_to_customer",
        column_name="Payment Settled to Customer"
    )


    def dehydrate_payment_date(self, cod_obj):
        if cod_obj and cod_obj.created_date:
            date_placed = utc_to_ist(cod_obj.created_date).strftime(
                settings.ADMIN_DATETIME_FORMAT)
            return date_placed
        return ''

    def dehydrate_deposited_to(self, cod_obj):
        if cod_obj.cash_handover:
            return str(cod_obj.cash_handover.collected_by)
        else:
            return str(cod_obj.payment_bank_txn.entity)

    def dehydrate_customer(self, cod_obj):
        if cod_obj.order:
            return str(cod_obj.order.customer.name)
        return '-'

    def dehydrate_city(self, cod_obj):
        if cod_obj.order:
            return str(cod_obj.order.city)
        return '-'

    def dehydrate_order_status(self, cod_obj):
        if cod_obj.order:
            return str(cod_obj.order.status)
        return '-'

    def dehydrate_customer_reference_number(self, cod_obj):
        if cod_obj.order:
            return str(cod_obj.order.customer_reference_number)
        return '-'

    def dehydrate_reference_number(self, cod_obj):
        if cod_obj.order:
            return str(cod_obj.order.reference_number)
        return '-'

    def dehydrate_date_placed(self, cod_obj):
        if cod_obj.order and cod_obj.order.date_placed:
            date_placed = utc_to_ist(cod_obj.order.date_placed).strftime(
                settings.ADMIN_DATETIME_FORMAT)
            return date_placed
        return ''

    def dehydrate_delivery_hub(self, cod_obj):
        if cod_obj.order:
            return str(cod_obj.order.hub)

    def dehydrate_cash_on_delivery(self, cod_obj):
        if cod_obj:
            return cod_obj.amount
        return '-'

    def dehydrate_cash_collected(self, cod_obj):
        if cod_obj.order:
            return cod_obj.order.cash_collected
        return '-'

    def dehydrate_customer_name(self, cod_obj):
        if cod_obj.order and cod_obj.order.shipping_address:
            return cod_obj.order.shipping_address.first_name
        return ''

    def dehydrate_driver_vehicle(self, cod_obj):
        if cod_obj.trip:
            return str(cod_obj.trip.driver)
        return '-'

    def dehydrate_delivered_date(self, cod_obj):
        if cod_obj.order and cod_obj.order.updated_time:
            delivered_date = utc_to_ist(cod_obj.order.updated_time).strftime(
                settings.ADMIN_DATETIME_FORMAT)
            return delivered_date
        return ''

    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    class Meta:
        model = OrderCODPayment
        fields = ('payment_id', 'payment_date', 'deposited_to', 'customer',
                    'city', 'order_status', 'order', 'customer_reference_number',
                    'reference_number', 'date_placed', 'delivery_hub', 'cash_on_delivery',
                    'cash_collected', 'customer_name', 'driver_vehicle', 'delivered_date',
                    'trip', 'is_settled_to_customer',)
        export_order = ('payment_id', 'payment_date', 'deposited_to', 'customer',
                        'city', 'order_status', 'order', 'customer_reference_number',
                        'reference_number', 'date_placed', 'delivery_hub', 'cash_on_delivery',
                        'cash_collected', 'customer_name', 'driver_vehicle', 'delivered_date',
                        'trip', 'is_settled_to_customer',)



