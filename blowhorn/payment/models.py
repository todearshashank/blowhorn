from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings
from django.utils import timezone
from postgres_copy import CopyManager
from django.contrib.postgres.fields import JSONField

from blowhorn.common.models import BaseModel
from blowhorn.common.helper import CommonHelper
from blowhorn.oscar.core.loading import get_model
from blowhorn.payment.constants import PAYMENT_STATUS_OPTIONS, PAYTM, PENDING, SUCCESS, FAILURE
from blowhorn.customer.models import Customer, CustomerBankDetails
from blowhorn.trip.models import Trip
from blowhorn.driver.models import Driver, CashHandover
from blowhorn.document.utils import generate_file_path

Order = get_model('order', 'Order')
Hub = get_model('address', 'Hub')


class TransactionOrder(BaseModel):
    SERVICE_TYPES = (
        (PAYTM, 'PAYTM'),
    )

    STATUSES = (
        (SUCCESS, 'SUCCESS'),
        (FAILURE, 'FAILURE'),
        (PENDING, 'PENDING'),
    )

    service = models.CharField(_("Service"), max_length=10,
                               choices=SERVICE_TYPES, default=PAYTM)
    order_number = models.CharField(_("Transaction Order Number"),
                                    max_length=10)
    status = models.CharField(_("Status"), max_length=10, choices=STATUSES,
                              default=PENDING)
    amount = models.DecimalField(
        _('Transaction Amount'),
        max_digits=20, decimal_places=2,
        default=0
    )

    def save(self, *args, **kwargs):
        if not self.order_number:
            self.order_number = self._generate_order_number()
        super(TransactionOrder, self).save(*args, **kwargs)

    def _generate_order_number(self):
        """
        Generate a unique transaction order number using appropriate prefix.
        """

        prefix = 'OR-'
        while True:
            order_no = CommonHelper.generate_random_string(6, prefix=prefix)
            if not TransactionOrder.objects.filter(
                order_number=order_no).exists():
                return order_no

    def __str__(self):
        return str(self.order_number)

    class Meta:
        app_label = "payment"
        verbose_name = _("Transaction Order")
        verbose_name_plural = _("Transaction Orders")
        unique_together = ('service', 'order_number')


class OrderEntity(models.Model):
    transaction_order = models.ForeignKey(TransactionOrder,
                                          related_name='trans_entities',
                                          on_delete=models.CASCADE)
    order = models.ForeignKey('order.Order', null=True,
                              related_name='order_entities',
                              on_delete=models.CASCADE)
    invoice = models.ForeignKey('customer.CustomerInvoice', null=True,
                                related_name='invoice_entities',
                                on_delete=models.CASCADE)

    class Meta:
        app_label = "payment"
        verbose_name = _("Order Entity")
        verbose_name_plural = _("Order Entities")


class PayTmHistory(models.Model):
    """
    Any transaction history as a result of response from PayTM.
    Documentation here : http://paywithpaytm.com/developer/paytm_api_doc/
    """

    user = models.ForeignKey('users.User',
                             related_name='paytm_transactions',
                             on_delete=models.CASCADE)
    order = models.ForeignKey(TransactionOrder, related_name='transactions',
                              on_delete=models.CASCADE)
    TXNDATE = models.DateTimeField('TXN DATE', default=timezone.now)
    TXNID = models.CharField('TXN ID', max_length=50)
    BANKTXNID = models.CharField('BANK TXN ID', max_length=40, null=True,
                                 blank=True)
    BANKNAME = models.CharField('BANK NAME', max_length=50, null=True,
                                blank=True)
    RESPCODE = models.CharField('RESP CODE', max_length=20)
    PAYMENTMODE = models.CharField('PAYMENT MODE', max_length=10, null=True,
                                   blank=True)
    CURRENCY = models.CharField('CURRENCY', max_length=4, null=True, blank=True)
    GATEWAYNAME = models.CharField("GATEWAY NAME", max_length=30, null=True,
                                   blank=True)
    MID = models.CharField(max_length=40)
    RESPMSG = models.TextField('RESP MSG', max_length=250)
    TXNAMOUNT = models.FloatField('TXN AMOUNT')
    STATUS = models.CharField('STATUS', max_length=12)
    CUSTID = models.CharField('CUST ID', max_length=50, blank=True, null=True)
    is_refund_request = models.BooleanField(_("Is Refund request"),
                                            default=False)
    Response = models.TextField('Paytm response', null=True)

    class Meta:
        app_label = 'payment'
        verbose_name = _("Transaction History")
        verbose_name_plural = _("Transaction History")

    def __unicode__(self):
        return self.STATUS


class PayTmUserDetails(BaseModel):
    """
    Stores Paytm User details sent by PayTm when a user tries to connect
    """
    access_token = models.CharField(
        _("Customer Access Token"), max_length=255)

    expires = models.CharField(
        _("Expired Epoch Time"), max_length=255)

    scope = models.CharField(
        _("Customer Scope (eg. wallet)"), max_length=255)

    resource_owner_id = models.CharField(
        _("Customer Owner Id"), max_length=255)

    customer_mobile = models.CharField(
        _("Customer Mobile"), max_length=255)

    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT)


class PaymentBankRef(BaseModel):
    entity = models.CharField(_("Entity"), null=False, blank=False,
                              max_length=64)

    trip = models.ForeignKey(Trip, null=True, blank=True,
                             on_delete=models.PROTECT)

    order = models.ForeignKey(Order, null=True, blank=True,
                              on_delete=models.PROTECT)

    driver = models.ForeignKey(Driver, null=True, blank=True,
                               on_delete=models.PROTECT)

    amount = models.IntegerField(_('Amount'), default=0)

    class Meta:
        verbose_name = _("Payment Bank Reference")
        verbose_name_plural = _("Payment Bank Reference")

    def __str__(self):
        return "%s | %s" % (self.entity, self.driver)


class PaymentBankTxn(BaseModel):
    entity = models.CharField(_("Entity"), null=False, blank=False,
                              max_length=64)

    trip = models.ForeignKey(Trip, null=True, blank=True,
                             on_delete=models.PROTECT)

    order = models.ForeignKey(Order, null=True, blank=True,
                              on_delete=models.PROTECT)

    driver = models.ForeignKey(Driver, null=True, blank=True,
                               on_delete=models.PROTECT)

    payment_reference = models.ForeignKey(PaymentBankRef, null=True, blank=True,
                                          on_delete=models.PROTECT)

    amount_deposited = models.IntegerField(_('Amount Deposited at APB'),
                                           default=0)

    status = models.CharField(_('Txn Status'), max_length=64, null=True,
                              blank=True)

    txn_id = models.CharField(_('Txn Id'), max_length=128, null=True,
                              blank=True)

    txn_date = models.DateTimeField(_('Txn Date'))

    class Meta:
        unique_together = ("entity", "txn_id")
        verbose_name = _("Payment Bank Transactions")
        verbose_name_plural = _("Payment Bank Transactions")

    def __str__(self):
        return "%s | %s" % (self.entity, self.txn_id)


class OrderCODPayment(BaseModel):
    order = models.ForeignKey(Order, on_delete=models.PROTECT)

    trip = models.ForeignKey(Trip, on_delete=models.PROTECT)

    amount = models.DecimalField(_("COD amount"),
                                 max_digits=10, decimal_places=2,
                                 default=0)

    cash_handover = models.ForeignKey(CashHandover, on_delete=models.PROTECT,
                                      null=True, blank=True)

    payment_bank_txn = models.ForeignKey(PaymentBankTxn,
                                         on_delete=models.PROTECT,
                                         null=True, blank=True)

    is_settled_to_customer = models.NullBooleanField(
        _("Is Payment settled to Customer"), default=False, null=True,
        blank=True)
    
    status = models.CharField(
        _("Status"), choices=PAYMENT_STATUS_OPTIONS,
        max_length=250, blank=True, null=True, db_index=True)
    
    file = models.FileField(upload_to=generate_file_path, null=True, blank=True, max_length=256)

    ack_ref = models.CharField(_("CMS Acknowledgement Reference"), max_length=64, null=True, blank=True)

    utr = models.CharField(_("Unique Transaction Reference"), max_length=64, null=True, blank=True)

    copy_data = CopyManager()
    objects = models.Manager()

    def __str__(self):
        return "%s | %s" % (str(self.order), str(self.amount))

    class Meta:
        verbose_name = _('Order COD Payment')
        verbose_name_plural = _('Order COD Payments')


class EzetapPayment(BaseModel):

    order = models.ForeignKey(Order, on_delete=models.PROTECT)

    status = models.CharField(_("Transaction Status"), max_length=64, null=True, blank=True)

    amount = models.DecimalField(_("Amount"), max_digits=10, decimal_places=2, default=0, blank=True, null=True)

    response_data = JSONField(null=True, blank=True)

    bank_details = models.ForeignKey(CustomerBankDetails, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.order.number


from blowhorn.oscar.apps.payment.models import *

