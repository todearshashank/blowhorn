import os
import csv
import json
from datetime import timedelta
from django.conf import settings

from django.utils.html import mark_safe
from django.db import transaction
from django.contrib import admin
from django.contrib import messages
from django.db.models.aggregates import Count, Sum
from django.utils.translation import ugettext_lazy as _
from django.db.models import Case, When, Value, CharField, F, Func, Q
from django.http import HttpResponse
from django.utils import timezone
from blowhorn.address.models import Hub

from blowhorn.contract.permissions import is_finance_user
from blowhorn.order.models import OrderPaymentHistory
from blowhorn.oscar.apps.payment.models import Transaction

from blowhorn.common.admin import NonEditableAdmin, NonEditableTabularInlineAdmin, BaseAdmin
from blowhorn.payment.constants import PAYMENT_HANDEDOVER_TO_CMS, PAYMENT_RECEIVED_BY_ASSOCIATE, PAYMENT_SETTLED, PAYMENT_SETTLEMENT_INITIATED
from blowhorn.payment.models import TransactionOrder, PayTmHistory, OrderEntity, \
    PaymentBankRef, PaymentBankTxn, OrderCODPayment, EzetapPayment
from blowhorn.payment.tasks import send_cod_report
from blowhorn.utils.datetimerangefilter import DateFilter
from blowhorn.utils.functions import utc_to_ist
from blowhorn.utils.datetime_function import DateTime

tz_diff = DateTime.get_timezone_offset()


class PaytmHistoryInline(NonEditableTabularInlineAdmin):
    model = PayTmHistory
    extra = 0


class OrderEntityInline(NonEditableTabularInlineAdmin):
    model = OrderEntity
    fields = ('transaction_order', 'order', 'invoice')
    extra = 0


@admin.register(TransactionOrder)
class TransactionOrderAdmin(NonEditableAdmin):

    list_display = ('order_number', 'service', 'status')
    search_fields = ('trans_entities__order__number',)
    inlines = [OrderEntityInline, PaytmHistoryInline]


# class SourceAdmin(admin.ModelAdmin):
#     actions = None
#     search_fields = ('order__number', 'reference',)
#     list_filter = (('source_type', admin.RelatedOnlyFieldListFilter),)
#     list_display = ('id','order', 'source_type', 'amount_allocated',
#                     'amount_debited', 'balance', 'reference')
#
#     list_select_related = ('order', 'source_type',)
#     readonly_fields = ('order', 'source_type', 'amount_allocated',
#                        'amount_debited', 'amount_refunded', 'balance', 'currency',
#                        'reference', 'label', 'submission_data',
#                        'deferred_txns', 'wallet_reference',)
#
#     def has_add_permission(self, request):
#         return False
#
#     def has_delete_permission(self, request, obj=None):
#         return False
#
#     def change_view(self, request, object_id, extra_context=None):
#         extra_context = extra_context or {}
#         extra_context['show_save'] = False
#         extra_context['show_save_and_continue'] = False
#         extra_context['show_save_and_add_another'] = False
#         return super().change_view(
#             request, object_id,
#             extra_context=extra_context
#         )


class TransactionAdmin(admin.ModelAdmin):
    actions = None
    search_fields = ('source__order__number',)
    list_display = ('order_number', 'source', 'txn_type', 'amount',
                    'reference', 'status', 'date_created')
    readonly_fields = list_display

    def order_number(self, obj):
        source = obj.source
        if source.order:
            return source.order.number
        return '--NA--'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_save_and_add_another'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('source', 'source__order', 'source__source_type')
        return qs


class PaymentBankRefAdmin(admin.ModelAdmin):
    list_display = ('id', 'entity', 'driver', 'amount')

    def has_change_permission(self, request, object=None):
        return False

    def has_add_permission(self, request):
        return False


class PaymentBankTxnAdmin(admin.ModelAdmin):
    list_display = (
    'entity', 'driver', 'payment_reference', 'amount_deposited', 'txn_id',
    'txn_date')

    def has_change_permission(self, request, object=None):
        return False

    def has_add_permission(self, request):
        return False


def mark_as_settled(modeladmin, request, queryset):
    data = request.POST

    try:

        if queryset.count() == 0:
            messages.error(
                request,
                "Please select payments to settle."
            )
            return False

        _ack = data.get('ack_number', None)
        if not _ack:
            messages.error(
                request,
                "Please provide the Acknowledgement Number."
            )
            return False

        queryset = queryset.select_related('order', 'order__customer')
        records_already_settled = []
        records_not_settlement_initiated_status = []
        customer = set()
        _status_history = []

        for _record in queryset:
            _status_history.append(OrderPaymentHistory(
                order=_record.order, old_payment_status=_record.status,
                new_payment_status=PAYMENT_SETTLED,
                modified_by=request.user
            ))

            _number = _record.order.number
            if _record.is_settled_to_customer:
                records_already_settled.append(_number)
            if _record.status != PAYMENT_SETTLEMENT_INITIATED:
                records_not_settlement_initiated_status.append(_number)
            customer.add(_record.order.customer)

        if records_already_settled:
            messages.error(
                request,
                "Please exclude already settled payments"
                "%s" % records_already_settled
            )
            return False

        if records_not_settlement_initiated_status:
            messages.error(
                request,
                "Please exclude records with status other than settlement inititated"
                "%s" % records_not_settlement_initiated_status
            )
            return False

        if len(customer) > 1:
            messages.error(
                request,
                "COD Settlement should only be done for single customer at a time."
            )
            return False

        with transaction.atomic():
            OrderPaymentHistory.objects.bulk_create(_status_history)
            queryset.update(utr=_ack, status=PAYMENT_SETTLED, is_settled_to_customer=True)

        messages.success(
            request,
            "Marked the selected payments as Settled."
        )

    except:
        messages.error(
            request,
            "Something went wrong while marking the payments as settled "
        )
        return False

def handed_over_to_cms(modeladmin, request, queryset):
    data = request.POST
    if queryset.count() == 0:
        messages.error(
            request,
            "Please select payments to update."
        )
        return False

    _ack = data.get('ack_number', None)
    _proof = request.FILES.get('ack_proof_document', None)

    if not (_ack and _proof):
        messages.error(
            request,
            "Please upload the acknowledgement file and number."
        )
        return False

    records_in_uneligible_stage = queryset.exclude(status=PAYMENT_RECEIVED_BY_ASSOCIATE).values_list('order__number', flat=True)
    if records_in_uneligible_stage:
        messages.error(
            request,
            "Please exclude the payments in status other than Payment Received by Associate."
            "%s" % records_in_uneligible_stage
        )
        return False

    _status_history = []
    cod_list = []
    _fileobj = None
    with transaction.atomic():
        for _record in queryset:
            _status_history.append(OrderPaymentHistory(
                order=_record.order, old_payment_status=_record.status,
                new_payment_status=PAYMENT_HANDEDOVER_TO_CMS,
                modified_by=request.user
            ))
            cod_list.append(_record.id)
            if not _fileobj:
                _record.file.save('reference.png', _proof)
                _record.save()
                _fileobj = _record.file

        queryset.update(ack_ref=_ack, file=_fileobj, status=PAYMENT_HANDEDOVER_TO_CMS)
        if _status_history:
            OrderPaymentHistory.objects.bulk_create(_status_history)
        if cod_list:
            send_cod_report.apply_async(args=[request.user.email, json.dumps(cod_list)])

    messages.success(
        request,
        "Marked the selected payments as Handed over to CMS."
    )


class OrderCODPaymentAdmin(BaseAdmin):
    list_display = ['order', 'order_customer', 'status', 'amount', 'order_hub', 'is_settled_to_customer',
                    'trip', 'order_city', 'deposited_to', 'created_date', 'order_driver']

    list_filter = (
        ('order__customer', admin.RelatedOnlyFieldListFilter),
        ('order__city', admin.RelatedOnlyFieldListFilter),
        ('order__hub', admin.RelatedOnlyFieldListFilter),
        'is_settled_to_customer',
        'status',
        'cash_handover__collected_by',
        'payment_bank_txn__entity',
        ('order__driver',admin.RelatedOnlyFieldListFilter),
        ('created_date', DateFilter),
    )

    readonly_fiels = ['amount', 'order', 'get_log']
    actions = ['export_order_cod_data', handed_over_to_cms, 'preview_settlement', 'settlement_initiated', mark_as_settled,]
    search_fields = ('order__number', 'trip__trip_number', 'ack_ref', 'utr')
    multi_search_fields = ('order__number', 'trip__trip_number',)

    def preview_settlement(self, request, queryset):
        _aggregate = queryset.aggregate(total=Sum('amount'), count=Count('id'))
        amount = _aggregate.get('total')
        count = _aggregate.get('count')
        preview_message = 'Total Cash Preview: Sum=%.2f, Count=%d' % (amount, count)
        self.message_user(request, preview_message, level=messages.INFO)

    def settlement_initiated(self, request, queryset):
        amount, count = 0, 0
        if queryset.count() == 0:
            messages.error(
                request,
                "Please select payments to initiate settlement."
            )
            return False

        records_in_uneligible_stage = queryset.exclude(status=PAYMENT_HANDEDOVER_TO_CMS).values_list('order__number', flat=True)
        if records_in_uneligible_stage:
            messages.error(
                request,
                "Only payments that are Handed over to CMS can be can be settled"
                "%s" % records_in_uneligible_stage
            )
            return False

        with transaction.atomic():
            _status_history = []
            for _record in queryset:
                _status_history.append(OrderPaymentHistory(
                    order=_record.order, old_payment_status=_record.status,
                    new_payment_status=PAYMENT_SETTLEMENT_INITIATED,
                    modified_by=request.user
                ))
                amount += _record.amount
                count += 1

            OrderPaymentHistory.objects.bulk_create(_status_history)
            queryset.update(status=PAYMENT_SETTLEMENT_INITIATED)

        preview_message = 'Marked the selected payments as Settlement Initiated. Total Cash Settled: Sum=%.2f, Count=%d' % (amount, count)
        self.message_user(request, preview_message, level=messages.INFO)

    def export_order_cod_data(self, request, queryset):
        ids = queryset.values_list('id', flat=True)
        queryset = OrderCODPayment.copy_data.filter(id__in=ids)

        file_path = 'Order_cod_payments'

        queryset.annotate(
            deposited=Case(
                When(cash_handover__isnull=False, then=F('cash_handover__collected_by__email')),
                default=F('payment_bank_txn__entity'),
                output_field=CharField()
            ),
            creation_datetime=Func(
                F('created_date') + timedelta(hours=tz_diff),
                Value("DD/MM/YYYY HH24: MI"),
                function='to_char',
                output_field=CharField()
                )
        ).to_csv(
            file_path,
            'order__number', 'order__customer__name', 'amount', 'order__hub__name', 'is_settled_to_customer',
            'trip__trip_number', 'order__city__name', 'deposited', 'creation_datetime'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow([
                'Order Number', 'Customer', 'COD Amount', 'Delivery Hub',
                'Is Payment Settled To Customer',
                'Trip', 'City', 'Deposited To', 'Created Date'
            ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response

    export_order_cod_data.short_description = 'Export Order Cod details in excel'

    def deposited_to(self, obj):
        if obj.cash_handover is not None:
            return str(obj.cash_handover.collected_by)
        elif obj.payment_bank_txn:
            return str(obj.payment_bank_txn.entity)
        else:
            return ''

    deposited_to.short_description = 'Deposited To'
    deposited_to.admin_order_field = 'deposited_to'

    def order_city(self, obj):
        if obj.order:
            return str(obj.order.city)
        return '-'

    order_city.short_description = "City"
    order_city.admin_order_field = "order_city"

    def  order_hub(self, obj):
        if obj.order:
            return str(obj.order.hub)
        return '-'

    order_hub.short_description = "Delivery Hub"
    order_hub.admin_order_field = "order_hub"

    def order_customer(self, obj):
        if obj.order:
            return str(obj.order.customer)
        return '-'

    order_customer.short_description = "Customer"
    order_customer.admin_order_field = "order_customer"

    def order_driver(self, obj):
        if obj.order:
            return str(obj.order.driver)
        return '-'

    order_driver.short_description = "Driver"
    order_driver.admin_order_field = "order_driver"

    def get_log(self, obj):
        if not obj:
            return ''

        _list = OrderPaymentHistory.objects.filter(order=obj.order).order_by('modified_date')
        if not _list:
            return ''

        headers = ['Modified Date', 'Old Status', 'New Status', 'Modified By']
        template = """
            <table class='history'><thead>
        """
        for h in headers:
            template += """<th style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px; font-weight:600; font-size:10px;">{header}</th>""".format(header=h)
        template += """</thead><tbody>"""

        for row in _list:
            template += """
                            <tr>
                                <td style="border: 1px solid #ddd; padding: 13px;">{date_of_change}</td>
                                <td style="border: 1px solid #ddd; padding: 13px;">{old_val}</td>
                                <td style="border: 1px solid #ddd; padding: 13px;">{new_val}</td>
                                <td style="border: 1px solid #ddd; padding: 13px;">{username}</td>
                            </tr>
                        """.format(
                            date_of_change=utc_to_ist(row.modified_date).strftime(settings.ADMIN_DATETIME_FORMAT) if row.modified_date else '',
                            old_val=row.old_payment_status,
                            new_val=row.new_payment_status,
                            username=row.modified_by if row.modified_by else ''
                        )
        template += "</tbody></table>"
        return mark_safe(template)

    get_log.allow_tags = True
    get_log.short_description = _('Log')

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields':
                    ('order', 'status', 'amount', 'trip',
                     'deposited_to', 'order_customer',
                     'order_hub', 'order_city', 'order_driver')
            }),
            ('Bank Details', {
                'fields': ('is_settled_to_customer', 'file', 'ack_ref', 'utr')
            }),
            ('Change Log', {
                'fields': ('get_log',)
            }),
            ('Audit', {
                'fields': ('created_date', 'created_by', 'modified_date', 'modified_by')
            }),
        )
        return fieldsets

    def get_queryset(self, request):
        return super().get_queryset(request).select_related(
            'order', 'cash_handover', 'payment_bank_txn', 'trip')

    def get_actions(self, request):

        actions = super().get_actions(request)

        if 'delete_selected' in actions:
            del actions['delete_selected']
        
        if not is_finance_user(None, request.user):
            del actions['settlement_initiated']
            del actions['mark_as_settled']

        is_associate = Hub.objects.filter(associates=request.user).exists()
        if not is_associate:
            del actions['handed_over_to_cms']

        return actions

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_save_and_add_another'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
    
    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            'website/js/lib/jquery-ui.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            'admin/js/payment/payment_cod.js',
        )


class EzetapPaymentAdmin(BaseAdmin):
    list_display = ['order', 'city_name', 'status', 'get_customer', 'get_driver', 'get_trip_number','amount', 'created_date']

    list_filter = (
        ('order__customer', admin.RelatedOnlyFieldListFilter), ('order__hub', admin.RelatedOnlyFieldListFilter),
        ('created_date', DateFilter), ('order__city', admin.RelatedOnlyFieldListFilter), ('order__driver', admin.RelatedOnlyFieldListFilter)
        )

    readonly_fiels = ['amount', 'order', 'response_data']

    fields = ['amount', 'order', 'response_data']

    search_fields = ('order__number', 'order__reference_number',  'order__driver__name', 'order__stops__trip__trip_number')

    def get_queryset(self, request):
        qs = super().get_queryset(request).select_related('order')
        qs = qs.annotate(city_name=F('order__city__name'))
        return qs

    def city_name(self, obj):
        if obj.city_name:
            return obj.city_name
        return "--NA--"

    def get_driver(self, obj):
        if obj.order.driver:
            return obj.order.driver
        return '--NA--'

    get_driver.short_description = "Driver Name"
    get_driver.admin_order_field = "driver_name"

    def get_customer(self, obj):
        if obj.order:
            return obj.order.customer
        return '--NA--'

    get_customer.short_description = "Customer Name"
    get_customer.admin_order_field = "customer_name"

    def get_trip_number(self, obj):
        if obj.order and obj.order.stops.exists():
            return obj.order.stops.last().trip.trip_number
        return '--NA--'

    get_trip_number.short_description = "Trip Number"
    get_trip_number.admin_order_field = "trip_number"

    def get_actions(self, request):

        actions = super().get_actions(request)

        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(OrderCODPayment, OrderCODPaymentAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(PaymentBankRef, PaymentBankRefAdmin)
admin.site.register(PaymentBankTxn, PaymentBankTxnAdmin)
admin.site.register(EzetapPayment, EzetapPaymentAdmin)
