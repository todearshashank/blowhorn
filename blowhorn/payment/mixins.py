# System and Django libraries
import math
import time
import logging
import traceback
from datetime import timedelta, datetime
from django.conf import settings
from django.utils import timezone
from django.db.models import Sum, Q

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model
from rest_framework import status

# Blowhorn imports
from blowhorn.common.helper import CommonHelper
from blowhorn.customer.submodels.sub_models import CustomerLedgerStatement
from blowhorn.customer.utils import create_customer
from blowhorn.users.models import Donation, User
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.payment import Payment
from blowhorn.order.tasks import send_c2c_payment_notification
from blowhorn.order.const import PAYMENT_MODE_RAZORPAY, PAYMENT_STATUS_PAID
from blowhorn.vehicle.utils import get_vehicle_info
from blowhorn.oscar.core.loading import get_model
from blowhorn.order.signals import new_order_notification


Transaction = get_model('payment', 'Transaction')
Source = get_model('payment', 'Source')
SourceType = get_model('payment', 'SourceType')


DONATION = 'donation'
DEFAULT_PAYMENT_TYPE = 'cash'
PAYMENT_CREATED = 'created'
PAYMENT_INITIATED = 'initiated'
PAYMENT_DEBIT = 'debit'
PAYMENT_REFUND = 'refund'
PAYMENT_COMPLETED = 'completed'
PAYMENT_FAILED = 'failed'
PAYMENT_CAPTURED = 'captured'
PAYMENT_AUTHORISED = 'authorized'

STATUS_PAID = 'paid'
STATUS_UNPAID = 'unpaid'

Order = get_model('order', 'Order')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Cost:
    prorate_step_size_minutes = 10

    def __init__(self, rate_dict):
        if not isinstance(rate_dict, dict):
            logging.info('Rate Dict: %s, %s' % (type(rate_dict), rate_dict))
            rate_dict = {}
        self.base_price = rate_dict.get('base_price', 0)
        self.base_minutes = rate_dict.get('base_minutes', 0)
        self.prorate_10m = rate_dict.get('prorate_10m', 0)

    def compute(self, distance_km=0, duration_seconds=0, wait_seconds=0):
        delta_min = 0
        if duration_seconds is not None:
            delta_min = duration_seconds / 60.0
        if delta_min < self.base_minutes:
            total_cost = self.base_price
        else:
            total_cost = self.base_price + \
                         math.ceil((delta_min - self.base_minutes) / self.prorate_step_size_minutes) * self.prorate_10m

        components = {
            'fixed': self.base_price,
            'time': total_cost - self.base_price,
            'distance': 0,
        }
        return total_cost, components, None


class PaymentMixin(object):

    def create_source(self, payment_type, order=None, wallet_reference=None, amount=0):
        """
        Creates Source for Order
        :param order_number: used for adding balance
        :param order: instance of Order
        :param payment_type: online, cash (str)
        :return: instance of Source
        """
        logger.info('Creating payment source for order: %s, %s' % (
            order, payment_type))
        if not payment_type:
            payment_type = DEFAULT_PAYMENT_TYPE
        source_type = SourceType.objects.filter(code=payment_type).first()
        payment_source = Source(
            order=order,
            wallet_reference=wallet_reference,
            source_type=source_type,
            amount_allocated=order.total_payable if order else amount,
            amount_debited=0,
            amount_refunded=0)
        payment_source.save()
        return payment_source

    def allocate_amount(self, order):
        """
        Stores amount in Source
        :param order: instance of order
        :return:
        """
        payment_source = Source.objects.filter(order=order)
        return payment_source.allocate(order.total_incl_tax,
                                       status=PAYMENT_CREATED)

    def update_payment_type(self, order, payment_type, amount=0.0, reference=None):
        """
        :param order: instance of Order
        :param payment_type: online, cash (str)
        :param amount: float
        :return:
        """
        source_type = SourceType.objects.filter(code=payment_type).first()
        if source_type:
            if order:
                query=Q(order=order)
            else:
                query=Q(wallet_reference__in=reference)
            Source.objects.filter(query).update(source_type=source_type,
                                                      amount_allocated=amount)


    def update_source(self, payment_type, order=None, order_number=None, **kwargs):
        """
        :param order: instance of Order
        :param payment_type: online, cash (str)
        :param kwargs: extra params
        :return:
        """
        source_type = SourceType.objects.filter(code=payment_type).first()
        payment_source = Source.objects.filter(order=order, wallet_reference=order_number,
                                               source_type=source_type).first()
        amount_debited = int(kwargs.pop('amount_debited', 0))
        amount_refunded = int(kwargs.pop('amount_refunded', 0))
        payment_status = kwargs.pop('status', None)
        reference = kwargs.pop('reference', '')

        if not payment_source:
            payment_source = self.create_source(
                settings.DEFAULT_PAYMENT_GATEWAY,
                order=order,
                wallet_reference=order_number,
                amount=amount_debited
            )

        logger.info('payment_source: %s' % payment_source)
        if amount_debited > 0:
            payment_source.debit(amount=amount_debited, reference=reference,
                                 status=PAYMENT_DEBIT)

        elif amount_refunded > 0:
            payment_source.refund(amount=amount_refunded, reference=reference,
                                  status=PAYMENT_REFUND)

        elif payment_status == PAYMENT_FAILED:
            payment_source.transactions.create(
                txn_type=PAYMENT_DEBIT, amount=order.total_incl_tax,
                reference=reference, status=payment_status)

        return payment_source

    def get_payment_status(self, order):
        source = Source.objects.filter(order=order).first()
        if source:
            transaction = Transaction.objects.filter(source=source).first()
            if transaction and transaction.status == PAYMENT_COMPLETED:
                return STATUS_PAID

        return STATUS_UNPAID

    def get_cost_function(order):
        c2c_vehicle_info = get_vehicle_info(
            order.vehicle_class_preference.commercial_classification)
        rate_dict = c2c_vehicle_info.get('rate', {})
        rate_obj = Cost(rate_dict)
        return rate_obj.compute

    def get_booking_cost_estimate(order, distance_km, trip_time_seconds):
        cost_function = PaymentMixin.get_cost_function(order)
        cost, _, _ = cost_function(distance_km, trip_time_seconds, 0)
        return cost

    def fetch_transaction_details(self, txn_id, txn_type):
        try:
            return Payment().fetch(txn_id, account=txn_type)
        except BaseException:
            logging.error(traceback.format_exc())
            return None

    def capture_online_payment(self, order_numbers, txn_id, type_, amount,
                               from_mitra=False, user=None):
        """
        Common function to be used for capturing online payments done by Razorpay.

        :param order_numbers: number of Orders (list)
        :param txn_id: transaction id from Razorpay (str)
        :param type_: mode of payment (str)
        :param amount: amount in paisa (int)
        :return: status (HTTP status code), data (dict)
        """
        data = 'Something went wrong'
        amount_real = float(amount) / 100
        payment_gateway = settings.DEFAULT_PAYMENT_GATEWAY
        capture_status = 'False'
        add_balance = False
        ledger = None
        wallet_reference = None

        orders = Order.objects.filter(number__in=order_numbers)
        orders = orders.select_related('customer')
        if from_mitra:
            customer = orders[0].customer
            total_incl_tax = orders.aggregate(amount=Sum('total_incl_tax'))[
                'amount']
            # TODO : check this error
            total_incl_tax = float(total_incl_tax) if total_incl_tax else 0
            payable_amount = total_incl_tax
        elif orders:
            customer = orders[0].customer
            payable_amount = orders.aggregate(amount=Sum('total_payable'))['amount']
            payable_amount = float(payable_amount) if payable_amount else 0

        else:
            if not type_ == DONATION:
                add_balance = True
                ledger = CustomerLedgerStatement.objects.filter(source__wallet_reference__in=order_numbers)
                ledger = ledger.select_related('source').first()
                if add_balance and not ledger:
                    return status.HTTP_400_BAD_REQUEST, data
                customer = ledger.customer if ledger else None
                payable_amount = float(ledger.amount) if ledger else None
                wallet_reference = ledger.source.wallet_reference if ledger and ledger.source else None

            else:
                customer = None

        if not type_ == DONATION and payable_amount < amount_real:
            logging.info('Paid amount by customer is less than total payable.'
                         'Not capturing payment.')

            return status.HTTP_400_BAD_REQUEST, data


        if not type_ == DONATION and not add_balance:
            if len(order_numbers) != len(orders):
                logging.info('Some Invalid orders are not in Delivered status.'
                             'Not capturing payment.')

                return status.HTTP_400_BAD_REQUEST, data

            if not orders:
                return status.HTTP_400_BAD_REQUEST, 'Incorrect booking key'

        try:
            transaction = Payment().fetch(txn_id, account=type_)
            payment_status = transaction.get('status')
        except BaseException:
            payment_status = None
            logging.error(traceback.format_exc())

        if payment_status == PAYMENT_CAPTURED:
            logging.info(
                'Txn has already been captured. Not capturing again')
            capture_status = 'True'
            data = {
                'status': capture_status,
                'merchantTxnId': txn_id,
                'amount': amount,
                'payment_details': customer.get_payment_details() if customer else None
            }
            return status.HTTP_200_OK, data

        elif payment_status != PAYMENT_AUTHORISED:
            logging.info('Txn has status something other than authorized. '
                         'Not proceeding.')
            return status.HTTP_400_BAD_REQUEST, data

        if not type_ == DONATION:
            if not add_balance:
                for order in orders:
                    self.update_payment_type(order=order, payment_type=payment_gateway,
                                             amount=float(order.total_payable))
            else:
                self.update_payment_type(order=None, payment_type=payment_gateway,
                                         amount=float(ledger.amount), reference=order_numbers)

        payment_capture_response = Payment().capture(
            payment_id=txn_id,
            amount=amount,
            account=type_
        )

        if not payment_capture_response:
            return status.HTTP_400_BAD_REQUEST, data

        elif payment_capture_response.get('already_captured', False):
            capture_status = 'True'
            data = {
                'status': capture_status,
                'merchantTxnId': txn_id,
                'amount': amount_real,
                'payment_details': customer.get_payment_details() if customer else None
            }
            logging.info('Txn has already been captured. '
                         'Not capturing again.')
            return status.HTTP_200_OK, data
        elif payment_capture_response.get('captured', False):
            if type_ == DONATION:
                wallet_reference = CommonHelper.generate_random_string(6, prefix='DN-')
                payment_source = PaymentMixin().update_source(
                    payment_gateway,
                    order_number=wallet_reference,
                    amount_debited=amount_real
                )
                Donation(user=user, amount=amount_real,
                         source=payment_source, description=type_,
                         response=transaction).save()
                from blowhorn.customer.tasks import send_donation_email # Circular Dependency
                send_donation_email.apply_async((user.email, txn_id),)
            else:
                for order in orders:
                    extra_args = {
                        'amount_debited': float(order.total_payable),
                        'reference': txn_id
                    }
                    PaymentMixin().update_source(
                        order=order,
                        payment_type=payment_gateway,
                        order_number=wallet_reference,
                        **extra_args
                    )
                    if not add_balance:
                        order.payment_status = PAYMENT_STATUS_PAID
                        order.payment_mode = PAYMENT_MODE_RAZORPAY
                        if order.status == StatusPipeline.ORDER_PENDING:
                            order.status = StatusPipeline.ORDER_NEW
                        # partial paid is not allowed so amount paid is
                        # total payable
                        order.amount_paid_online = order.total_payable
                        order.save()
                capture_status = 'True'

                if settings.ENABLE_SLACK_NOTIFICATIONS and not from_mitra:
                    send_c2c_payment_notification.apply_async(
                        (order.id, float(order.total_payable), txn_id),
                        eta=timezone.now() + timedelta(
                            minutes=0.3
                        ))
                logging.info('Payment Done for order = %s, of Amount = %s, '
                             'Transaction = %s, Type = %s' %
                             (order, float(order.total_payable), txn_id, type_))

            data = {
                'status': capture_status,
                'merchantTxnId': txn_id,
                'amount': amount_real,
                'payment_details': customer.get_payment_details() if customer else None
            }
            return status.HTTP_200_OK, data
        else:
            extra_args = {
                'status': PAYMENT_FAILED,
                'reference': txn_id
            }
            if not add_balance:
                for order in orders:
                    PaymentMixin().update_source(
                        order=order,
                        payment_type=payment_gateway,
                        **extra_args
                    )
            else:
                PaymentMixin().update_source(
                    order=order,
                    payment_type=payment_gateway,
                    order_number=wallet_reference,
                    **extra_args
                )

            return status.HTTP_400_BAD_REQUEST, data

    def fetch_and_create_transactions(self, data, account=None):
        logger.info('Fetching transactions for %s' % data)
        payment_obj = Payment().all(data, account=account)
        transactions = payment_obj.get('items', [])
        if not transactions:
            logger.info('No transaction has been found for %s' % data)
            return

        for transaction in transactions:
            txn_id = transaction.get('id', None)
            amount = transaction.get('amount', 0.0)
            email = transaction.get('email', '')
            _type = DONATION
            try:
                user = User.objects.filter(email=email).first()
                if not user:
                    user = User.objects.create_user(email, '1234')

                status_code, data = self.capture_online_payment(
                    [], txn_id, _type, amount, user=user)
                logger.info('Razorpay: %s, %s' % (status_code, data))
            except BaseException as be:
                logger.info('Failed to create transaction for %s, %s' % (transaction, be))

            time.sleep(2)
