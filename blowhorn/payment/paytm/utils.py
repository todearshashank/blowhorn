import requests
import logging
from django.conf import settings
from urllib import parse


from blowhorn.payment.paytm import Checksum
from blowhorn.payment.models import TransactionOrder, PayTmHistory


def get_transaction_status(transaction_order):
    """
    Returns the status of a paytm transaction (from API response).
    Run this task on a `success` or `pending` response.
    """

    transaction_status_url = getattr(settings, 'PAYTM_ENDPOINT_TRANSN_STATUS', None)
    merchant_id = getattr(settings, 'PAYTM_MERCHANT_ID', None)
    merchant_key = getattr(settings, 'PAYTM_MERCHANT_KEY', None)

    input_data = {"MID": merchant_id, "ORDERID": transaction_order.order_number}
    checksum = Checksum.generate_checksum(input_data, merchant_key)
    input_data['CHECKSUMHASH'] = checksum.decode() if type(
        checksum) == bytes else checksum
    input_data_url_encoded = parse.quote_plus(str(input_data))
    url = transaction_status_url + "?JsonData=" + input_data_url_encoded

    try:
        response = requests.get(url)
    except:
        response = None

    logging.info('Checking transaction status for order %s' % (transaction_order.id, ))
    if response and response.status_code == 200:
        logging.info('Transaction status response for order %s : %s' % (
            transaction_order.order_number, response.text))
        if response.json().get('STATUS') == "TXN_SUCCESS":
            return "TXN_SUCCESS"
        elif response.json().get('STATUS') == "TXN_FAILURE":
            return "TXN_FAILURE"

    return None


def get_withdraw_response(data):
    """
    Returns the status of a paytm transaction (from API response).
    Run this task on a `success` or `pending` response.
    """
    user = data.pop('user', None)
    transaction_status_url = getattr(settings, 'PAYTM_ENDPOINT_TRANSN_WITHDRAWAL', None)
    input_data_url_encoded = parse.quote_plus(str(data))
    url = transaction_status_url + "?JsonData=" + input_data_url_encoded
    try:
        response = requests.get(url)
    except:
        response = None

    if response and response.status_code == 200:
        response_data = response.json()
        transaction_order = TransactionOrder.objects.filter(order_number=response_data.get('OrderId')).first()
        if user and transaction_order:
            data_dict = {}
            data_dict['TXNID'] = response_data.get('TxnId')
            data_dict['MID'] = response_data.get('MID')
            data_dict['TXNAMOUNT'] = response_data.get('TxnAmount')
            data_dict['BANKTXNID'] = response_data.get('BankTxnId')
            data_dict['RESPCODE'] = response_data.get('ResponseCode')
            data_dict['RESPMSG'] = response_data.get('ResponseMessage')
            data_dict['STATUS'] = response_data.get('Status')
            data_dict['PAYMENTMODE'] = response_data.get('PaymentMode')
            data_dict['BANKNAME'] = response_data.get('BankName')
            data_dict['CUSTID'] = response_data.get('CustId')
            data_dict['Response'] = response_data

            PayTmHistory.objects.create(user=user, order=transaction_order, **data_dict)

        if response_data.get('Status') == "TXN_SUCCESS":
            return {'status': True, 'response': response.json().get('TxnId')}
        elif response_data.get('Status') == "TXN_FAILURE":
            return {'status': False, 'response': response.json().get('ResponseMessage')}

    return {'status': False, 'response': 'Payment could not be processed'}
