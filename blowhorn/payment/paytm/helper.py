import os


def get_razorpay_slack_message(order, amount, txn_id):
    # Broadcasting the payent to Slack / Firebase.

    customer = order.customer

    message = '%s paid %s for %s'
    title = message % (
        customer.user.name,
        amount,
        order.number
    )
    track_url = 'http://%s/track/%s' % (os.environ.get(
        'HTTP_HOST', 'blowhorn.net'), order.number)

    slack_message = {
        #            "text": title,
        "attachments": [
            {
                #                    "fallback": title,
                "author_name": '%s | %s | %s' % (
                    customer.customer_category, customer.user.name, customer.user.phone_number),
                "title": title,
                "title_link": track_url,
                # "thumb_url" : thumb_url,
                "text": 'Txn ID : %s' % (txn_id),
                "color": "good"
            }
        ]
    }

    return slack_message
