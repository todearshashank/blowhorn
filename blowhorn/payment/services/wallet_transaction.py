import os
from django.conf import settings
from django.template import loader

from blowhorn.common.sms_templates import wallet_credit, wallet_debit
from blowhorn.contract.models import Contract
from blowhorn.payment.constants import TRANSACTION_TEMPLATE, \
    TRANSACTION_EMAIL_SUBJECT
from blowhorn.customer.models import CustomerLedgerStatement
from blowhorn.order.const import PAYMENT_MODE_PAYTM
from blowhorn.utils.mail import Email
from blowhorn.utils.datetime_function import DateTime


class TransactionNotification(object):

    def __init__(self):
        self.is_default_build = settings.WEBSITE_BUILD == settings.DEFAULT_BUILD
        self.currency_symbol = Contract().get_currency_symbol(
            currency_code=settings.OSCAR_DEFAULT_CURRENCY)

    def send_wallet_notification(self, data):
        ledger = CustomerLedgerStatement.objects.select_related(
            'customer', 'customer__user', 'source', 'order', 'wallet_source'
        ).get(
            pk=data.get('ledger_id')
        )

        logo_url = '%s/static/website/images/%s/logo/logo.png' % (
            settings.HOST, settings.WEBSITE_BUILD
        )
        transaction_type = 'credit' if ledger.amount > 0 else 'debit'
        transaction_detail_template = TRANSACTION_TEMPLATE.get(transaction_type, {})

        customer = ledger.customer
        customer_name = customer.name
        user = customer.user
        customer_email = user.email
        amount = float(ledger.amount)
        formatted_amount = '%s %s' % (settings.OSCAR_DEFAULT_CURRENCY, amount)
        subject = TRANSACTION_EMAIL_SUBJECT.get(transaction_type)
        print(subject)
        payment_mode = data.get('payment_mode', '')
        order_number = str(ledger.order or
                           (ledger.source.wallet_reference if ledger.source
                            else ledger.wallet_source))
        transaction_time = DateTime().get_locale_datetime(
            ledger.transaction_time).strftime(settings.APP_DATETIME_FORMAT)
        print('transaction_time: %s' % transaction_time)

        sms_content = None
        if transaction_type == 'credit':
            subject = subject.format(
                currency_symbol=self.currency_symbol,
                amount=amount,
                brand_name=settings.BRAND_NAME
            )
            template_dict = wallet_credit
            sms_content = template_dict['text'] % (
                customer_name,
                '%s %s' % (settings.OSCAR_DEFAULT_CURRENCY, amount),
                PAYMENT_MODE_PAYTM if payment_mode == PAYMENT_MODE_PAYTM
                    else 'UPI/Card/Net banking',
                order_number
            )
        elif transaction_type == 'debit':
            subject = subject.format(
                currency_symbol=self.currency_symbol,
                amount=('%s' % amount).replace('-', ''),
                brand_name=settings.BRAND_NAME,
                order_number=order_number
            )
            template_dict = wallet_debit
            sms_content = template_dict['text'] % (
                '%s %s' % (settings.OSCAR_DEFAULT_CURRENCY, ('%s' % amount).replace('-', '')),
                order_number,
                transaction_time,
                '%s %s' % (settings.OSCAR_DEFAULT_CURRENCY, float(ledger.balance)),
                '%s/track/%s' % (settings.HOST, order_number)
            )

        content = {
            'homepage_url': settings.HOMEPAGE_URL,
            'logo': {
                'url': logo_url,
                'styles': {
                    'width': '120px' if self.is_default_build else '60px',
                    'height': '32px' if self.is_default_build else '60px',
                }
            },
            'feedback': {
                'contact_mail': settings.COMPANY_SUPPORT_EMAIL,
                'subject': 'My%20{brand_name}%20experience'.format(
                    brand_name=settings.BRAND_NAME)
            },
            'app_links': {
                'ios': {
                    'url': settings.IOS_APP_LINK,
                    'img': 'https://enterprise-blowhorn.s3.us-east-2.amazonaws.com/resource/applestore-badge.png'
                },
                'android': {
                    'url': settings.ANDROID_APP_LINK,
                    'img': 'https://enterprise-blowhorn.s3.us-east-2.amazonaws.com/resource/google-play-badge.png'
                },
            },
            'brand_name': settings.BRAND_NAME,
            'social_links': {
                'facebookId': settings.SOCIAL_LINKS.get('facebookId'),
                'twitterId': settings.SOCIAL_LINKS.get('twitterId'),
                'linkedinId': settings.SOCIAL_LINKS.get('linkedinId'),
            },
            'current_build': settings.WEBSITE_BUILD,
            'link_to_wallet': '%s/myaccount' % settings.HOST,
            'currency': {
                'symbol': self.currency_symbol,
            },
            'transaction_details': {
                'heading': {
                    'text': transaction_detail_template.get('heading', {}).get('text', ''),
                    'color': transaction_detail_template.get('heading', {}).get('color', 'green'),
                }
            },
            'account_info': {
                'label': transaction_detail_template.get('account_info', {}).get('label'),
                'sub_label': '%s Wallet linked to' % settings.BRAND_NAME,
                'value': customer_email
            },
            'amount': ('%s' % amount).replace('-', ''),
            'payment_mode': payment_mode,
            'transaction_time': transaction_time,
            'transaction_id': order_number,
            'available_balance': float(ledger.balance),
            'customer_name': customer_name
        }
        html_content = loader.render_to_string(
            'website/emails/wallet_transaction_notification.html',
            content
        )

        if sms_content is not None:
            user.send_sms(sms_content, template_dict)

        from_email = 'Blowhorn <%s>' % settings.DEFAULT_FROM_EMAIL
        Email().send_email_message([customer_email], subject, html_content,
            from_email=from_email)
