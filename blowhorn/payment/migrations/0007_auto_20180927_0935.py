# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-27 09:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0075_auto_20180926_0840'),
        ('order', '0065_auto_20180925_1041'),
        ('payment', '0006_auto_20180615_0602'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderEntity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('invoice', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='invoice_entities', to='customer.CustomerInvoice')),
                ('order', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='order_entities', to='order.Order')),
            ],
            options={
                'verbose_name': 'Order Entity',
                'verbose_name_plural': 'Order Entities',
            },
        ),
        migrations.RemoveField(
            model_name='transactionorder',
            name='invoice',
        ),
        migrations.RemoveField(
            model_name='transactionorder',
            name='order',
        ),
        migrations.AddField(
            model_name='paytmhistory',
            name='CUSTID',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='CUST ID'),
        ),
        migrations.AddField(
            model_name='transactionorder',
            name='amount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=20, verbose_name='Transaction Amount'),
        ),
        migrations.AddField(
            model_name='orderentity',
            name='transaction_order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='trans_entities', to='payment.TransactionOrder'),
        ),
    ]
