# Generated by Django 2.2.17 on 2021-01-18 07:10

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0014_auto_20210108_0736'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='ordercodpayment',
            managers=[
                ('copy_data', django.db.models.manager.Manager()),
            ],
        ),
    ]
