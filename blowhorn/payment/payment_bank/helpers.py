from rest_framework import status

from django.utils import timezone
from datetime import timedelta

from blowhorn.customer.models import Partner
from blowhorn.driver.models import Driver
from blowhorn.trip.models import Trip
from blowhorn.payment.models import PaymentBankRef, PaymentBankTxn, OrderCODPayment
from config.settings import status_pipelines as StatusPipeline
from blowhorn.order.models import OrderPaymentHistory
from blowhorn.payment.constants import PAYMENT_COLLECTED_FROM_CUSTOMER, PAYMENT_RECEIVED_BY_ASSOCIATE



def get_driver_data(driver_id=None, trip_number=None):
    _status = True
    _ret = {}
    if driver_id:
        try:
            driver = Driver.objects.get(pk=driver_id)
            _ret['driver_id'] = driver.id
            _ret['amount'] = int(driver.cod_balance)
            _ret['name'] = driver.name
            _ret['msg'] = 'success'
            if trip_number:
                trip = Trip.objects.filter(trip_number=trip_number).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).first()
                _ret['trip'] = trip.id
                if not trip or trip.driver_id != driver.id:
                    _status = False
                    _ret['msg'] = 'Invalid trip'
            else:
                trip = Trip.objects.filter(driver_id=driver.id,
                                           status__in=[
                                            StatusPipeline.TRIP_IN_PROGRESS,
                                            StatusPipeline.TRIP_ALL_STOPS_DONE]
                                           ).first()
                _ret['trip'] = trip.id
        except:
            _status = False
            _ret['msg'] = 'Invalid driverId'
    elif trip_number:
        try:
            trip = Trip.obejcts.get(trip_number=trip_number)
            _ret['trip'] = trip.id
            _ret['amount'] = int(trip.total_cash_collected)
            _ret['name'] = trip.driver.name
        except:
            _status = False
            _ret['msg'] = 'Invalid trip'

    _ret['status'] = _status
    return _ret


def get_payment_bank_reference(entity, driver_data):
    _payment_ref_data = {
        'entity': entity,
        'driver_id': driver_data.get('driver_id'),
        'trip_id': driver_data.get('trip'),
        'amount': driver_data.get('amount', 0),
    }
    payment_ref = PaymentBankRef.objects.create(**_payment_ref_data)

    return payment_ref.id


def process_payment_bank_txn(txn_data):
    referenceId = txn_data.get('referenceId')
    entity = txn_data.get('entity')
    amount = txn_data.get('amount')

    payment_ref = PaymentBankRef.objects.filter(id=referenceId,
                                  entity=entity,
                                  amount=amount).first()

    if not payment_ref:
        msg = {'message': 'failure',
               'content': {'error': 'Invalid data for given referenceId'}
               }
        return status.HTTP_400_BAD_REQUEST, msg

    if amount != payment_ref.amount:
        msg = {'message': 'failure',
               'content': {'error': 'Invalid amount'}
               }
        return status.HTTP_400_BAD_REQUEST, msg

    transactionId = txn_data.get('transactionId')
    txn_date = txn_data.get('txn_date')

    payment_bank_txn_data = {
        'entity': entity,
        'driver': payment_ref.driver,
        'trip': payment_ref.trip,
        'payment_reference_id': payment_ref.id,
        'amount_deposited': amount,
        'txn_id': transactionId,
        'txn_date': txn_date,
        'status': txn_data.get('status', 'success')
    }

    # check if its a duplicate request to record deposit transaction
    payment_txn = PaymentBankTxn.objects.filter(**payment_bank_txn_data).first()
    if payment_txn:
        msg = {"message": "Duplicate Transaction",
               "content": {"referenceId": referenceId,
                           "status": payment_txn.status}
               }
        return status.HTTP_200_OK, msg

    payment_txn = PaymentBankTxn.objects.filter(entity=entity,
                                                payment_reference_id=payment_ref.id).first()

    if payment_txn:
        msg = {"message": "failure",
               "content": {"referenceId": referenceId,
                           "error": "Tranasction with same referenceId already exists"}
               }
        return status.HTTP_400_BAD_REQUEST, msg

    if txn_date < timezone.now() - timedelta(hours=24):
        msg = {"message": "failure",
               "content": {"referenceId": referenceId,
                           "error": "txn_date cannot be older than 24 hours"}
               }
        return status.HTTP_400_BAD_REQUEST, msg
    elif txn_date > timezone.now() + timedelta(minutes=3):
        msg = {"message": "failure",
               "content": {"referenceId": referenceId,
                           "error": "txn_date cannot be a future date time"}
               }
        return status.HTTP_400_BAD_REQUEST, msg

    try:
        payment_txn = PaymentBankTxn.objects.create(**payment_bank_txn_data)
        cod_orderlevel_save(
                        driver=payment_ref.driver,
                        trip=payment_ref.trip,
                        payment_txn=payment_txn
                    )
        # subtract amount from driver cod balance
        cod_balance = max(float(payment_ref.driver.cod_balance) - amount, 0)
        Driver.objects.filter(id=payment_ref.driver.id).update(
            cod_balance=cod_balance)

        msg = {"message": "success",
               "content": {"referenceId": referenceId,
                           "status": payment_txn.status}
               }
        return status.HTTP_201_CREATED, msg
    except:
        msg = {"message": "failure",
               "content": {"referenceId": referenceId,
                           "status": 'failure'}
               }
        return status.HTTP_400_BAD_REQUEST, msg


def is_auth_payment_bank(entity, api_key):
    """
        API key verification for the payment bank
    """

    if not api_key:
        return False

    partner = Partner.objects.filter(legalname=entity, api_key=api_key)
    if not partner:
        return False
    return True

def cod_orderlevel_save(driver, trip, cash_handover=None, payment_txn=None):
    from blowhorn.order.models import Event
    from blowhorn.trip.models import Stop

    # stop_objects = Stop.objects.filter(trip=trip).select_related('order')
    # trip_order_list = [ stop.order for stop in stop_objects ]
    # cod_complete_orders = OrderCODPayment.objects.filter(order__in=trip_order_list, trip=trip).select_related('order')
    # cod_complete_orders_list = [ item.order for item in cod_complete_orders ]
    # trip_order_list = list(set(trip_order_list) - set(cod_complete_orders_list))
    # event_filtered = Event.objects.filter(
    #                                     order__in=trip_order_list, 
    #                                     status=StatusPipeline.ORDER_DELIVERED,
    #                                     driver=driver,
    #                                     order__cash_on_delivery__gt=0
    #                                 )
    trip_order_set = set(Stop.objects.filter(trip=trip).values_list('order_id',flat=True))
    # trip_order_list = [ stop.order for stop in stop_objects ]
    cod_complete_orders_set = set(OrderCODPayment.objects.filter(order_id__in=trip_order_set, trip=trip, status=PAYMENT_RECEIVED_BY_ASSOCIATE).values_list('order_id',flat=True))
    # cod_complete_orders_list = [ item.order for item in cod_complete_orders ]
    trip_order_list = list(trip_order_set - cod_complete_orders_set)
    event_filtered = Event.objects.select_related('order').filter(
                        order_id__in=trip_order_list, 
                        status=StatusPipeline.ORDER_DELIVERED,
                        driver=driver,
                        order__cash_on_delivery__gt=0
                    )

    if not event_filtered.exists():
        return

    cod_list = []
    if cash_handover is not None:
        #Add with Cash handover
        _history = []
        for _event in event_filtered:
            cod_list.append(_event.order)
            _history.append(
                OrderPaymentHistory(**{
                    'order' : _event.order,
                    'old_payment_status' : PAYMENT_COLLECTED_FROM_CUSTOMER,
                    'new_payment_status' : PAYMENT_RECEIVED_BY_ASSOCIATE,
                    'modified_by' : cash_handover.collected_by
                })
            )

        OrderCODPayment.objects.filter(order__in=cod_list,
                trip=trip, status=PAYMENT_COLLECTED_FROM_CUSTOMER
                ).update(cash_handover=cash_handover, status=PAYMENT_RECEIVED_BY_ASSOCIATE)
        OrderPaymentHistory.objects.bulk_create(_history)

    else:
        #If cash handover is none then payment_txn is not none
        for event in event_filtered:
            cod_list.append(OrderCODPayment(**{
                "order": event.order,
                "trip": trip,
                "amount": event.order.cash_on_delivery,
                "payment_bank_txn": payment_txn
            }))
        OrderCODPayment.objects.bulk_create(cod_list)

    return