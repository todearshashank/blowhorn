from django.utils.translation import ugettext_lazy as _

# BLOW-1166 COD Stage
PAYMENT_COLLECTED_FROM_CUSTOMER = 'Collected from Customer'
PAYMENT_RECEIVED_BY_ASSOCIATE = 'Received by MWH Associate'
PAYMENT_HANDEDOVER_TO_CMS = 'Handed Over to CMS'
PAYMENT_SETTLEMENT_INITIATED = 'Settlement Initiated'
PAYMENT_SETTLED = 'Settled'

PAYMENT_STATUS_OPTIONS = (
    (PAYMENT_COLLECTED_FROM_CUSTOMER, _('Collected from Customer')),
    (PAYMENT_RECEIVED_BY_ASSOCIATE, _('Received by MWH Associate')),
    (PAYMENT_HANDEDOVER_TO_CMS, _('Handed Over to CMS')),
    (PAYMENT_SETTLEMENT_INITIATED, _('Settlement Initiated')),
    (PAYMENT_SETTLED, _('Settled')),
)

# Payment services
PAYTM = "PAYTM"

#AIrtel Payment Bank
APB='APB'

# Paytm Payment Statuses
PENDING = "PENDING"
FAILURE = "FAILURE"
SUCCESS = "SUCCESS"

# Transaction Email Content
TRANSACTION_TEMPLATE = {
    'credit': {
        'heading': {
            'text': 'Money Added Successfully',
            'color': 'green',
        },
        'account_info': {
            'label': 'To',
        },
    },
    'debit': {
        'heading': {
            'text': 'Money Paid Successfully',
            'color': 'red',
        },
        'account_info': {
            'label': 'From',
        },
    }
}

TRANSACTION_EMAIL_SUBJECT = {
    'credit': '{currency_symbol}{amount} added successfully to {brand_name} wallet',
    'debit': 'You paid {currency_symbol}{amount} to {brand_name} booking '
             '{order_number}',
}
