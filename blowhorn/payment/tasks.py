from email import encoders
from email.mime.base import MIMEBase
from io import StringIO
import os
import json
from django.core.mail.message import EmailMultiAlternatives
from django.template import loader
import requests
import traceback
import logging
from datetime import timedelta, datetime
from django.conf import settings
from django.utils import timezone
from django.db.models import F

from blowhorn.common.decorators import redis_batch_lock
from blowhorn.payment.models import OrderCODPayment, TransactionOrder
from blowhorn.payment.constants import PENDING
from blowhorn.payment.paytm.utils import get_transaction_status
from blowhorn.payment.mixins import  DONATION, PaymentMixin
from blowhorn.order.const import PAYMENT_STATUS_PAID, PAYMENT_STATUS_UNPAID, PAYMENT_MODE_PAYTM
from blowhorn.payment.constants import SUCCESS, FAILURE
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.order.models import Order
from blowhorn import celery_app

DEBUG = getattr(settings, 'DEBUG', None)


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def broadcast_payment(self, transaction_order_id):
    """
    Broadcast payment to slack.
    """

    transaction_order = TransactionOrder.objects.get(id=transaction_order_id)

    slack_url = getattr(settings, 'SLACK_URL', None)
    slack_channel_payments = getattr(settings, 'SLACK_CHANNELS', {}).get('PAYMENTS', None)

    if slack_url and slack_channel_payments:
        logging.info('Online payment broadcasting for order %s.' % (transaction_order.order_number, ))

        order_numbers = list(transaction_order.trans_entities.all().values_list('order__number', flat=True))

        if order_numbers:
            customer = Order.objects.filter(number=order_numbers[0]).first().customer

            message_title = "%s paid %s for Orders %s (%s)" % (
                customer.user.name,
                transaction_order.amount,
                order_numbers,
                transaction_order.service
            )
            message_url = None
            if len(order_numbers) == 1:
                message_url = 'http://%s/track/%s' % (os.environ.get('HTTP_HOST', 'blowhorn.net'), order_numbers[0])

            message_content = {
                "attachments": [
                    {
                        "author_name": '%s | %s | %s' % (
                            customer.customer_category, customer.user.name, customer.user.phone_number),
                        "title": message_title,
                        "title_link": message_url,
                        "text": 'Txn ID : %s' % (transaction_order.order_number, ),
                        "color": "good"
                    }
                ],
                "channel": slack_channel_payments
            }
            requests.post(slack_url, json.dumps(message_content))


@celery_app.task
# @periodic_task(run_every=timedelta(minutes=10))
@redis_batch_lock(period=600, expire_on_completion=True)
def update_paytm_transaction_status():
    """
    Periodic task to update status of `PENDING` paytm transaction requests.
    """
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    orders_pending = TransactionOrder.objects.filter(
        status=PENDING, created_date__lt=timezone.now() - timedelta(minutes=10))
    for transaction_order in orders_pending:
        status = get_transaction_status(transaction_order)
        if status == "TXN_SUCCESS":
            transaction_order.status = SUCCESS
            transaction_order.save()
            order_entities = transaction_order.trans_entities.all()
            for entity in order_entities:
                order = entity.order
                if order.payment_status == PAYMENT_STATUS_UNPAID:
                    order.payment_status = PAYMENT_STATUS_PAID
                    order.payment_mode = PAYMENT_MODE_PAYTM
                    order.save()
        elif status == "TXN_FAILURE":
            transaction_order.status = FAILURE
            transaction_order.save()


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def send_slack_notification_for_payment(self, order_numbers, amount, txn_id, type_, reason, auto_debit=False):
    if settings.ENABLE_SLACK_NOTIFICATIONS:
        _slack_url = settings.C2C_PAYMENT_SLACK_URL
        _slack_channel = settings.C2C_PAYMENT_SLACK_CHANNEL
    else:
        _slack_url = getattr(settings, 'SLACK_URL', None)
        _slack_channel = getattr(settings, 'SLACK_CHANNELS', {}).get('PAYMENTS', None)

    orders = Order.objects.filter(number__in=order_numbers)
    customer = orders[0].customer
    if txn_id:
        if auto_debit:
            message = 'Auto Debit via %s of amount %s for order %s successful'
        else:
            message = 'Payment withdrawal via %s of amount %s for  order %s successful'
        message = message % (
            type_,
            amount,
            order_numbers
        )
    else:
        txn_id = '-'
        if auto_debit:
            message = 'Auto Debit via %s of amount %s for order %s failed because %s.'
        else:
            message = 'Payment withdrawal via %s of amount %s for  order %s failed because %s.'

        message = message % (
            type_,
            amount,
            order_numbers,
            reason
        )
    track_url = None
    if len(order_numbers) == 1:
        track_url = 'http://%s/track/%s' % (os.environ.get('HTTP_HOST', 'blowhorn.net'), order_numbers[0])

    slack_message = {
        "attachments": [
            {
                "author_name": '%s | %s | %s' % (
                    customer.customer_category, customer.user.name, customer.user.phone_number),
                "title": message,
                "title_link": track_url,
                "text": 'Txn ID : %s' % (txn_id),
                "color": "good"
            }
        ]
    }
    slack_channel = _slack_channel
    slack_message.update({'channel': slack_channel})
    try:
        requests.post(url=_slack_url, json=slack_message)
    except:
        logging.error(traceback.format_exc())


@celery_app.task
# @periodic_task(run_every=timedelta(minutes=60))
@redis_batch_lock(period=3300, expire_on_completion=True)
def update_donation_payments():
    """
    Fetch & create missing transactions in donation
    """
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    start_time = datetime.now() - timedelta(minutes=60)
    data = {
        'from': int(start_time.timestamp())
    }
    PaymentMixin().fetch_and_create_transactions(data, account=DONATION)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def send_cod_report(self, user_email, cod_list):
    cod_id_list = json.loads(cod_list)
    payments = OrderCODPayment.copy_data.select_related(
                    'order', 'order__hub', 'order__city', 'order__customer', 'order__driver'
                ).filter(id__in=cod_id_list)
    email_to_list = [user_email, 'ramprakash@blowhorn.com', 'ashish.chauhan@blowhorn.com', 'harisha.g@blowhorn.com',
                     'paresh.gs@blowhorn.com']

    _total_cash = 0
    _hub_names = set()
    for _p in payments:
        _total_cash += _p.amount
        _hub_names.add(_p.order.hub.name)
        email_to_list.extend(list(_p.order.hub.supervisors.all().values_list('email', flat=True)))
        email_to_list.extend(list(_p.order.city.managers.all().values_list('email', flat=True)))

    email_to_list = list(set(email_to_list)) if settings.ENABLE_SLACK_NOTIFICATIONS else settings.DEFAULT_TESTMAIL
    context =  {
        "user" : user_email,
        "total_orders" : payments.count(),
        "hub_name" : ','.join(_hub_names),
        "brand_name" : settings.BRAND_NAME,
        "total_cash" : _total_cash
    }
    email_template_name = 'emailtemplates_v2/handed_over_to_cms.html'
    html_content = loader.render_to_string(email_template_name, context)
    mail = EmailMultiAlternatives(
        'Cash Handed Over to CMS Summary',
        html_content,
        to=email_to_list,
        cc=['finance@blowhorn.com',] if settings.ENABLE_SLACK_NOTIFICATIONS else [],
        from_email=settings.DEFAULT_FROM_EMAIL)
    field_list = [
        'order_number', 'customer_name', 'hub',
        'city', 'amount', 'trip_number', 'driver',
        'vehicle'
    ]
    mail.attach_alternative(html_content, "text/html")
    filepath = StringIO()
    payments.annotate(
        order_number=F('order__number'),
        customer_name=F('order__customer__name'),
        hub=F('order__hub__name'),
        city=F('order__city__name'),
        trip_number=F('trip__trip_number'),
        driver=F('order__driver__name'),
        vehicle=F('order__driver__driver_vehicle'),

    ).to_csv(filepath, *field_list)
    modified_file = 'CODReport%s.csv' % datetime.now().strftime('%d%b%y%H%M%S%f')

    part = MIMEBase('application', 'octet-stream')
    part.set_payload(filepath.getvalue().encode('utf-8'))
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment', filename=modified_file)
    mail.attach(part)
    mail.send()
