"""
{'m_payment_id': '1BMWLKT',
 'pf_payment_id': '889837',
 'payment_status': 'COMPLETE',
 'item_name': '1BMWLKT',
 'item_description': '',
 'amount_gross': '650.00',
 'amount_fee': '-14.95',
 'amount_net': '635.05',
 'custom_str1': '',
 'custom_str2': '',
 'custom_str3': '',
 'custom_str4': '',
 'custom_str5': '',
 'custom_int1': '',
 'custom_int2': '',
 'custom_int3': '',
 'custom_int4': '',
 'custom_int5': '',
 'name_first': 'Ragupathy',
 'name_last': '',
 'email_address': 'ragummrsa@gmail.com',
 'merchant_id': '10013372',
 'signature': '0bc84f2ef3a0ac4a1fd0d1437fb66092'}
"""

import logging
from django.conf import settings
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from rest_framework import status
from rest_framework.response import Response
from rest_framework import views
from rest_framework.permissions import AllowAny

from blowhorn.customer.mixins import CustomerMixin
from .constants import PAYFAST_RETURN_URL, PAYFAST_CANCEL_URL, \
    PAYFAST_PAYMENT_COMPLETE
from .mixin import SecurityMixin, PayfastMixin
from blowhorn.order.const import PAYMENT_STATUS_PAID, PAYMENT_MODE_ONLINE
from blowhorn.order.models import Order
from blowhorn.payment.mixins import PaymentMixin
from blowhorn.utils.functions import get_base_url
from blowhorn.customer.models import Customer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class PayFastNotifyHookView(views.APIView, SecurityMixin, PayfastMixin):
    permission_classes = (AllowAny,)

    # parser_classes = (PreserveRawBodyJSONParser,)

    def post(self, request):
        logger.info('API HOOK call from payfast: %s\n %s\n %s' %
                    (request.data, request.META, request.__dict__))

        data = request.data.dict()
        # data_flat = {k: v for (k, [v]) in data.items()}
        print('data_flat:', data)

        m_payment_id = data.get('m_payment_id', None)
        email_address = data.get('email_address')
        pf_payment_id = data.get('pf_payment_id', None)
        payment_status = data.get('payment_status', None)
        booking_number = data.get('item_name', '')
        item_description = data.get('item_description', '')
        amount_gross = data.get('amount_gross', 0.0)
        amount_deducted = data.get('amount_fee', 0.0)
        amount_received = data.get('amount_net', 0.0)
        is_add_balance = True if data.get('custom_str1', "") else False

        """
        Do recommended security checks
            1. Verify the security signature is valid
            2. Verify the source IP address belongs to PayFast
            3. Verify the payment amount matches your order amount
            4. Verify the data received is valid
        """
        if payment_status != PAYFAST_PAYMENT_COMPLETE:
            logger.info('Payment is not complete. Not proceeding.')
            return Response(status=400, data=_('Payment not complete'))

        remote_addr = request.META.get('REMOTE_ADDR', None)
        signature = data.get('signature', None)
        is_valid_signature = self.verify_signature(signature, data)
        if not is_valid_signature:
            logger.info('invalid_signature: %s' % signature)
            return Response(status=403, data=_('Invalid signature. Forbidden.'))

        # is_valid_ip = self.verify_ip(remote_addr)
        # if not is_valid_ip:
        #     return Response(
        #         status=403,
        #         data=_('Not a whitelisted IP. %s' % remote_addr)
        #     )

        # is_valid = self.data_is_valid(request.POST)
        # if not is_valid:
        #     logger.info('data is invalid: %s' % request.POST)
        #     return Response(status=400, data=_('Data has been modified'))

        if not is_add_balance:
            # @todo Multiple booking management
            if m_payment_id:
                try:
                    order = Order.objects.get(pk=int(m_payment_id))
                except Order.DoesNotExist as dne:
                    logger.warning('Order %s doesn\'t exists. %s' % (m_payment_id, dne))
                    return Response(status=400, data=_('Order not found'))
            else:
                try:
                    order = Order.objects.get(number=booking_number)
                except Order.DoesNotExist as dne:
                    logger.warning('Order %s doesn\'t exists. %s' % (m_payment_id, dne))
                    return Response(status=400, data=_('Order not found'))

            if order.payment_status == PAYMENT_STATUS_PAID:
                return Response(status=200, data=_('Order is already paid'))

            if not self.verify_payment_amount(order, amount_gross):
                message = 'Amount mismatch..! payable-amount: %s, amount-paid: %s' \
                          % (order.total_payable, amount_gross)
                logger.info(message)
                return Response(status=400, data=_(message))

            payment_mixin = PaymentMixin()
            payment_gateway = settings.PAYFAST_PAYMENT_GATEWAY
            payment_mixin.update_payment_type(
                order=order,
                payment_type=payment_gateway,
                amount=float(order.total_payable)
            )
            extra_args = {
                'amount_debited': float(order.total_payable) if not is_add_balance else amount_gross,
                'reference': pf_payment_id
            }
            payment_mixin.update_source(
                order=order,
                payment_type=payment_gateway,
                **extra_args
            )
            order.payment_status = PAYMENT_STATUS_PAID
            order.payment_mode = PAYMENT_MODE_ONLINE
            order.amount_paid_online = order.total_payable
            order.save()
            return Response(status=200, data=_('Payment received successfully'))
        else:
            print('m_payment_id ', m_payment_id)
            extra_args = {
                'amount_debited': float(amount_gross),
                'reference': m_payment_id
            }
            payment_source = PaymentMixin().update_source(
                order=None,
                order_number=m_payment_id,
                payment_type='payfast',  # settings.DEFAULT_PAYMENT_GATEWAY,
                **extra_args
            )

            # is_payment_captured = payment_response.get('captured', False) or payment_response.get('already_captured',
            #                                                                                       False)

            # if is_payment_captured:
            customer = Customer.objects.get(user__email=email_address)
            customer.add_balance_to_wallet(payment_source=payment_source, payment_response=data,
                                           payment_type='payfast')

            res = {
                'status': True,
                'merchantTxnId': m_payment_id,
                'amount': amount_gross,
                "available_balance": customer.get_customer_balance(),
                "transaction_info": customer.get_last_transactions()
            }

            return Response(status=status.HTTP_200_OK, data=res)


class CapturePayment(views.APIView, CustomerMixin):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        print('request data: ', data)
        order_pk = data.get('m_payment_id', None)
        booking_number = data.get('item_name', '')
        amount = data.get('amount', '')
        is_add_balance = True if data.get('add_balance', '') else False
        customer = CustomerMixin().get_customer(request.user)

        if not is_add_balance:
            # either order id or number should be sent
            if order_pk:
                try:
                    order = Order.objects.get(pk=order_pk)
                except Order.DoesNotExist as dne:
                    logger.warning('Order %s doesn\'t exists. %s' % (order_pk, dne))
                    return Response(status=400, data=_('Order not found'))
            else:
                try:
                    order = Order.objects.get(number=booking_number)
                except Order.DoesNotExist as dne:
                    logger.warning('Order %s doesn\'t exists. %s' % (booking_number, dne))
                    return Response(status=400, data=_('Order not found'))

            customer = order.customer
            data = {
                'status': 'True' if order.payment_status == PAYMENT_STATUS_PAID else 'False',
                'amount': order.total_payable,
                'payment_details': customer.get_payment_details()
            }
            if order.payment_status == PAYMENT_STATUS_PAID:
                return Response(status=200, data=data)
        else:
            data = {
                'status': True,
                "available_balance": customer.get_customer_balance() if customer else 0,
                "transaction_info": customer.get_last_transactions() if customer else []
            }
            return Response(status=200, data=data)

        return Response(status=400, data=_('Payment capture pending.'))


class CheckoutView(views.View, PayfastMixin, SecurityMixin):
    checkout_template = 'payments/payfast/checkout.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CheckoutView, self).dispatch(*args, **kwargs)

    def _get_user(self, **kwargs):
        """
        Returns the user performing the transaction.
        """

        user = None
        if self.request.user and self.request.user.is_authenticated:
            user = self.request.user
        elif 'transaction' in kwargs:
            transaction = kwargs.pop('transaction', None)
            if transaction:
                user = transaction.created_by
        else:
            entity_type = kwargs.pop("entity_type", None)
            entity_id = kwargs.pop("entity_id", None)
            if entity_type and entity_id:
                if entity_type == "order":
                    try:
                        order = Order.objects.select_related(
                            'customer', 'customer__user'
                        ).get(id=entity_id)
                        user = order.customer.user
                    except Order.DoesNotExist:
                        pass
        return user

    def __next_url(self, is_add_balance, order_number):
        """
        Returns redirection URL used after successful/failed transaction
        """
        if is_add_balance:
            return '/myaccount'
        else:
            return '/track/%s' % order_number

    def get(self, request):
        entity_type = request.GET.get('entity_type', None)
        entity_id = request.GET.get('entity_id', None)
        is_add_balance_val = request.GET.get('add_balance', '')
        is_add_balance = True if is_add_balance_val else False
        order_number = request.GET.get('order_number', None)
        amount = request.GET.get('amount', 0.0)
        if (not entity_type or not entity_id) and not is_add_balance:
            return HttpResponse(status.HTTP_400_BAD_REQUEST)

        if isinstance(entity_id, list):
            entity_id = entity_id[0]

        user = self._get_user(entity_type=entity_type, entity_id=entity_id)
        order = None
        order_pk = None
        total_payable = 0.0
        customer = None
        if not is_add_balance:
            order = Order.objects.select_related(
                'customer', 'customer__user'
            ).get(pk=entity_id)
            total_payable = order.total_payable
            customer = order.customer
            order_number = order.number
            order_pk = order.pk
        else:
            customer = CustomerMixin().get_customer(user)
            total_payable = amount

        if total_payable:
            base_url = get_base_url(request)
            logger.info('base_url: %s', base_url)
            next_url = self.__next_url(is_add_balance, order_number)
            return_url = PAYFAST_RETURN_URL.format(
                base_url=base_url, next_url=next_url)
            cancel_url = PAYFAST_CANCEL_URL.format(
                base_url=base_url, next_url=next_url)

            _dict = {
                'merchant_id': settings.PAYFAST_MERCHANT_ID,
                'merchant_key': settings.PAYFAST_MERCHANT_KEY,
                'return_url': return_url,
                'cancel_url': cancel_url,
                'notify_url': '%s%s' % (base_url, settings.PAYFAST_HOOK_URL),
                'name_first': customer.name,
                'name_last': '',
                'email_address': user.email,
                'cell_number': '%s'.zfill(3) % user.national_number,
                'm_payment_id': '%s' % order_pk or self.generate_m_payment_id(user),
                'amount': '%s' % float(total_payable),
                'item_name': order_number,
                'custom_str1': is_add_balance_val,
                'item_description': '',
                'email_confirmation': '0',
                'confirmation_address': user.email,
            }
            # _dict['signature'] = self.checkout_signature(_dict)
            print('checkout_dict: ', _dict)
            return render(
                request,
                self.checkout_template,
                dict(data=_dict, end_point=self.get_transaction_url())
            )
        return TemplateResponse(
            request,
            'website/404_not_found.html',
            status=status.HTTP_400_BAD_REQUEST
        )


class PayfastSuccessView(TemplateView):
    template_name = "payments/payfast/success.html"

    def get_context_data(self, **kwargs):
        return {'default_redirect_url': settings.HOST_URL}


class PayfastCancelView(TemplateView):
    template_name = "payments/payfast/cancel.html"

    def get_context_data(self, **kwargs):
        return {'default_redirect_url': settings.HOST_URL}
