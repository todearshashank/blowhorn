from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'checkout/$', views.CheckoutView.as_view(),
        name='payfast-checkout'),
    url(r'notify-payment/$', views.PayFastNotifyHookView.as_view(),
        name='payfast-notify-payment'),
    url(r'check-status/$', views.CapturePayment.as_view(),
        name='payfast-check-status'),
]
