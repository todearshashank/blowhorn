import logging
from hashlib import md5
from datetime import datetime
from django.conf import settings
from django.utils import timezone

from six.moves.urllib.error import HTTPError
from six.moves.urllib.parse import urlencode
from six.moves.urllib.request import urlopen
from blowhorn.oscar.core.loading import get_model

from blowhorn.oscar.apps.payment.models import Source

from blowhorn.common.ip_address import IpAddress
from .constants import ITN_SIGNATURE_FIELD_ORDER, PAYFAST_SANDBOX_URL,\
    PAYFAST_SERVER_IP_SUBNET, PAYFAST_DOMAINS, PAYFAST_PROCESS_URL,\
    PAYFAST_SANDBOX_SERVER, PAYFAST_LIVE_SERVER, CHECKOUT_SIGNATURE_FIELD_ORDER,\
    CHECKOUT_SIGNATURE_IGNORED_WHITESPACE, PAYFAST_DATA_VALID, PAYFAST_LIVE_URL

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Transaction = get_model('payment', 'Transaction')
Source = get_model('payment', 'Source')
SourceType = get_model('payment', 'SourceType')


class SecurityMixin(object):

    def _sign_fields(self, signable_fields):
        """
        Common signing code.
        """
        text = ''
        for (k, v) in signable_fields:
            assert isinstance(k, str), repr(k)
            assert isinstance(v, str), repr(v)
            text = urlencode(signable_fields, encoding='utf-8', errors='strict')
        return md5(text.encode('ascii')).hexdigest()

    def _prepare_signable_fields(self, valid_field_order, data_fields):
        """
        Prepare PayFast submission variables for signing, using the given field
        order.
        :raise ValueError:
            If `data_fields` contains any unexpected field names not in
            `valid_field_order`.
        """
        # WARNING..! This check should be present

        # present_fields = data_fields.keys()
        # extra_fields = present_fields - set(valid_field_order)
        # if extra_fields:
        #     raise ValueError(
        #         'Data contains unexpected fields: {!r}'.format(extra_fields))

        return [
            (name, data_fields[name]) for name in valid_field_order
            if name in data_fields
        ]

    def _drop_non_signature_fields(self, data_fields, include_empty):
        """
        Drop fields that should not be included in signatures.
        These are:
            * `signature`, as a convenience for verifying already-signed data.
            * Fields with empty values, if `include_empty` is false.
        """
        return {
            k: v for (k, v) in data_fields.items()
            if k != 'signature'
            if include_empty or v
        }

    def checkout_signature(self, checkout_data):
        """
        Calculate the signature of a checkout process submission.
        """
        # Omits fields with empty values.
        included_fields = self._drop_non_signature_fields(
            checkout_data, include_empty=False)

        # Strip ignored whitespace from values.
        stripped_fields = {
            name: value.strip(CHECKOUT_SIGNATURE_IGNORED_WHITESPACE)
            for (name, value) in included_fields.items()
        }

        signable_fields = self._prepare_signable_fields(
            CHECKOUT_SIGNATURE_FIELD_ORDER, stripped_fields)
        return self._sign_fields(signable_fields)

    def itn_signature(self, itn_data):
        """
        Calculate the signature of an ITN submission.
        """
        # ITN signatures include fields with empty values.
        included_fields = self._drop_non_signature_fields(
            itn_data, include_empty=True)
        signable_fields = self._prepare_signable_fields(
            ITN_SIGNATURE_FIELD_ORDER, included_fields)
        return self._sign_fields(signable_fields)

    def verify_signature(self, signature, data):
        """
        :param signature:
        :param data:
        :return:
        """
        itn_signature = self.itn_signature(data)
        logger.info('signature: %s, itn_signature: %s' %
                    (signature, itn_signature))
        return signature == itn_signature

    def verify_ip(self, remote_address):
        """
        :param remote_address:
        :return:
        """
        ip_address = IpAddress()
        subnet = PAYFAST_SERVER_IP_SUBNET
        for name in PAYFAST_DOMAINS:
            resolved_ip = ip_address.get_host_by_name(name)
            if resolved_ip is not None:
                subnet.append(resolved_ip)

        logger.info('subnet: %s, remote_address: %s' % (subnet, remote_address))
        for ip_subnet in subnet:
            return ip_address.ip_in_subnetwork(remote_address, ip_subnet)
        else:
            logger.info('Remote address not found in subnet')
            return False

    def verify_payment_amount(self, order, amount_gross):
        """
        :param order:
        :param amount_gross:
        :return:
        """
        logger.info('Verifying payment amount:\n total_payable: %s, '
                    'amount_paid: %s' % (order.total_payable, amount_gross))
        return float(order.total_payable) == float(amount_gross)

    def _values_to_encode(self, data):
        """
        :param data:
        :return:
        """
        return [
            (k, str(value).strip().encode('utf8'))
            for (k, value) in data.items() if k != 'signature'
        ]

    def data_is_valid(self, post_data, postback_server=PAYFAST_SANDBOX_URL):
        """
        Validates data via the postback. Returns True if data is valid,
        False if data is invalid and None if the request failed.
        """
        postback_url = PAYFAST_LIVE_URL
        post_str = urlencode(self._values_to_encode(post_data))
        post_bytes = post_str.encode(settings.DEFAULT_CHARSET)
        postback_url = postback_server.rstrip('/')
        try:
            response = urlopen(postback_url, data=post_bytes)
            result = response.read().decode('utf-8')
            return result == PAYFAST_DATA_VALID
        except HTTPError:
            return False


class PayfastMixin(object):

    def get_transaction_url(self):
        """
        :return:
        """
        host_url = PAYFAST_SANDBOX_SERVER if settings.DEBUG else \
            PAYFAST_LIVE_SERVER
        return PAYFAST_PROCESS_URL.format(host_url=PAYFAST_LIVE_SERVER)

    def generate_m_payment_id(self, user):
        _now = timezone.now().timestamp()
        _formatted_id = '%s%s' % (user.pk, _now)
        return int(_formatted_id[:8])
