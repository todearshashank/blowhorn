PAYFAST_LIVE_SERVER = 'www.payfast.co.za'
PAYFAST_SANDBOX_SERVER = 'sandbox.payfast.co.za'
PAYFAST_DOMAINS = [
    'www.payfast.co.za',
    'w1w.payfast.co.za',
    'w2w.payfast.co.za',
    'sandbox.payfast.co.za'
]
PAYFAST_SERVER_IP_SUBNET = [
    '197.97.145.145/24',
    '41.74.179.192/27'
]
PAYFAST_SANDBOX_URL = 'https://sandbox.payfast.co.za/eng/query/validate'
PAYFAST_LIVE_URL = 'https://www.payfast.co.za/eng/query/validate'
PAYFAST_PROCESS_URL = 'https://{host_url}/eng/process'
PAYFAST_RETURN_URL = '{base_url}/payfast/success?next={next_url}'
PAYFAST_CANCEL_URL = '{base_url}/payfast/cancel?next={next_url}'
PAYFAST_PAYMENT_COMPLETE = 'COMPLETE'
PAYFAST_DATA_VALID = 'VALID'

CHECKOUT_SIGNATURE_FIELD_ORDER = [
    # Merchant Details
    'merchant_id',
    'merchant_key',
    'return_url',
    'cancel_url',
    'notify_url',

    # Buyer Detail
    'name_first',
    'name_last',
    'email_address',
    'cell_number',

    # Transaction Details
    'm_payment_id',
    'amount',
    'item_name',
    'item_description',
    'custom_int1',
    'custom_int2',
    'custom_int3',
    'custom_int4',
    'custom_int5',
    'custom_str1',
    'custom_str2',
    'custom_str3',
    'custom_str4',
    'custom_str5',

    # Transaction Options
    'email_confirmation',
    'confirmation_address',
]

#: Field order for ITN submission signatures.
#: https://developers.payfast.co.za/documentation/#notify-page-itn
ITN_SIGNATURE_FIELD_ORDER = [
    # Transaction details
    'm_payment_id',
    'pf_payment_id',
    'payment_status',
    'item_name',
    'item_description',
    'amount_gross',
    'amount_fee',
    'amount_net',
    'custom_str1',
    'custom_str2',
    'custom_str3',
    'custom_str4',
    'custom_str5',
    'custom_int1',
    'custom_int2',
    'custom_int3',
    'custom_int4',
    'custom_int5',

    # Buyer details
    'name_first',
    'name_last',
    'email_address',

    # Merchant details
    'merchant_id',
]

CHECKOUT_SIGNATURE_IGNORED_WHITESPACE = ''.join([' ', '\t', '\n', '\r', '\x0b'])
