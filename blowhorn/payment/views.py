import json
import logging
import requests
import urllib.parse as urlparse
from urllib import parse
from urllib.parse import parse_qs
from django.conf import settings
from django import views
from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.views.generic import TemplateView
from django.db.models import Sum, F
from django.db import transaction
from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_aware, make_aware

from datetime import datetime

from rest_framework import generics, status, views as api_view
from rest_framework.response import Response
from blowhorn.oscar.core.loading import get_model
from rest_framework.permissions import AllowAny

from blowhorn.apps.integrations.v1.tyre.ceat.ceat import create_ceat_order
from blowhorn.customer.models import Customer
from blowhorn.order.models import Order
from blowhorn.mitra.helper.order import EcommOrderCreator
from blowhorn.customer.models import PartnerAccessKey
from blowhorn.payment.mixins import PaymentMixin, DONATION
from blowhorn.payment.paytm import Checksum
from blowhorn.payment.models import TransactionOrder, PayTmHistory, \
    PayTmUserDetails, OrderEntity
from blowhorn.payment.constants import PAYTM, SUCCESS, FAILURE, APB
from blowhorn.users.models import UserManager
from config.settings import status_pipelines as StatusPipeline
from blowhorn.payment.payment_bank.helpers import get_driver_data, \
    get_payment_bank_reference, process_payment_bank_txn, is_auth_payment_bank
from blowhorn.payment.paytm.utils import get_transaction_status, \
    get_withdraw_response
from blowhorn.payment.tasks import update_paytm_transaction_status, \
    broadcast_payment, send_slack_notification_for_payment
from blowhorn.customer.permissions import IsCustomer
from blowhorn.order.utils import OrderNumberGenerator
from blowhorn.common import sms_templates
from blowhorn.utils.functions import get_tracking_url
from blowhorn.order.const import (
    PAYMENT_STATUS_PAID,
    PAYMENT_MODE_PAYTM,
    PAYMENT_STATUS_UNPAID,
    PAYMENT_MODE_EZETAP
)
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.order.tasks import send_mail, create_order_on_ceat_system
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.customer.utils import create_customer

User = get_model('users', 'User')
EzetapPayment = get_model('payment', 'EzetapPayment')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Razorpay(generics.CreateAPIView, PaymentMixin):
    authentication_classes = (CsrfExemptSessionAuthentication,)
    permission_classes = (AllowAny, )

    def post(self, request, data=None):
        if not data:
            data = request.data

        order_numbers = data.get('booking_key', '').split(',')
        # Transaction id as txnid
        txn_id = data.get('txn_id') or data.get('txnid')
        amount = data.get('amount')
        type_ = data.get('type')
        from_mitra = data.get('from_mitra', 'false')
        from_mitra = True if from_mitra=='true' else False
        logging.info('Txn ID = %s, Amount = %s, Type = %s' %
                     (txn_id, amount, type_))

        user = None
        if type_ == DONATION:
            if not request.user.is_anonymous:
                user = request.user
            else:
                email = data.get('email', '')
                user, customer = create_customer(email)
        status_code, data = self.capture_online_payment(order_numbers, txn_id, type_, amount, from_mitra,
                                                        user=user)

        if from_mitra and order_numbers:
            EcommOrderCreator().update_order_payment(order_numbers[0])
            create_order_on_ceat_system.apply_async(args=[order_numbers[0]])
        return Response(status=status_code, data=data)


class PayTm(views.View):

    MERCHANT_KEY = getattr(settings, 'PAYTM_MERCHANT_KEY', None)
    MERCHANT_ID = getattr(settings, 'PAYTM_MERCHANT_ID', None)
    CALLBACK_URL = getattr(settings, 'PAYTM_CALLBACK_URL', None)
    WEBSITE = getattr(settings, 'PAYTM_WEBSITE', 'APPSTAGING')
    INDUSTRY_TYPE_ID = getattr(settings, 'PAYTM_INDUSTRY_TYPE_ID', 'Retail')
    CHANNEL_ID = getattr(settings, 'PAYTM_CHANNEL_ID', 'WEB')
    ENDPOINT_TRANSN_REQUEST = getattr(
        settings, 'PAYTM_ENDPOINT_TRANSN_REQUEST', None)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PayTm, self).dispatch(*args, **kwargs)

    def get(self, request):
        is_add_balance = True if request.GET.get('add_balance', '') else False
        entity_type = request.GET.get("entity_type", None)
        entity_id = request.GET.get("entity_id", None)
        entity_ids = entity_id.split(',') if entity_id else None
        order_number = request.GET.get('order_number', None)
        bill_amount = request.GET.get('amount', 0.0)
        balance_payable = request.GET.get('balance_payable', 0.0)
        _next = request.GET.get("next", None)
        user = request.user

        if not entity_type or not entity_id and not is_add_balance:
            return HttpResponse(status.HTTP_400_BAD_REQUEST)

        if not is_add_balance:
            user = self._get_user(entity_type=entity_type, entity_id=entity_id)
            if balance_payable and float(balance_payable) > 0:
                bill_amount = balance_payable
            else:
                bill_amount = self._get_bill_amount(entity_type, entity_ids)

        order = self._get_order(
            entity_type, entity_ids, user, bill_amount,
            order_number=order_number, is_add_balance=is_add_balance)
        if bill_amount:
            data_dict = {
                'MID': self.MERCHANT_ID,
                'ORDER_ID': order.order_number,
                'CUST_ID': user.id,
                'TXN_AMOUNT': bill_amount,
                'CHANNEL_ID': self.CHANNEL_ID,
                'WEBSITE': 'APPSTAGING',#self.WEBSITE,
                'INDUSTRY_TYPE_ID': self.INDUSTRY_TYPE_ID,
                'CALLBACK_URL': '%s?next=%s' % (self.CALLBACK_URL, _next) if _next else self.CALLBACK_URL,
            }
            param_dict = data_dict
            param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(
                data_dict, self.MERCHANT_KEY)

            data = {
                'paytmdict': param_dict,
                'api': self.ENDPOINT_TRANSN_REQUEST
            }
            return render(request, 'payments/paytm/payment.html', data)
        return TemplateResponse(request, 'website/404_not_found.html',
                                status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        _next = request.GET.get('next', '')
        data_dict = {}
        for key in request.POST:
            data_dict[key] = request.POST[key]

        CHECKSUM = data_dict.pop('CHECKSUMHASH', None)
        verify = Checksum.verify_checksum(
            data_dict, self.MERCHANT_KEY, CHECKSUM)

        if verify:
            transaction_order = TransactionOrder.objects.get(
                order_number=data_dict.pop('ORDERID'))
            logging.info('PayTM payment proceeding for order %s. '
                         'Gateway Response %s' % (
                            transaction_order.id, str(data_dict)))
            data_dict['order'] = transaction_order
            user = self._get_user(transaction=transaction_order)
            customer = CustomerMixin().get_customer(user)
            transaction = PayTmHistory.objects.create(user=user, **data_dict)
            transaction_status = data_dict.get("STATUS", None)
            parsed_next = urlparse.urlparse(_next)
            parsed_next = parse_qs(parsed_next.query)
            is_add_balance = parsed_next.get('add_balance', False)
            if is_add_balance:
                is_add_balance = is_add_balance[0] == 'true'
            if transaction_status == "TXN_SUCCESS":
                # Cross checking once again using transaction status API (As
                # per paytm security policy)
                if get_transaction_status(transaction_order) == "TXN_SUCCESS":
                    logging.info('PayTM payment status success for order %s' % (
                        transaction_order.order_number,))
                    self.on_success(
                        transaction_order,
                        amount=float(transaction_order.amount),
                        is_add_balance=is_add_balance,
                        customer=customer)
                elif get_transaction_status(transaction_order) == "TXN_FAILURE":
                    logging.info('PayTM payment status failed for order %s' % (
                        transaction_order.order_number,))
                    self.on_failure(transaction_order)
            elif transaction_status == "TXN_FAILURE":
                logging.info('PayTM payment failed for order %s' %
                             (transaction_order.order_number,))
                self.on_failure(transaction_order)
            elif transaction_status == "PENDING":
                update_paytm_transaction_status(transaction_order.id)
            if _next:
                return HttpResponseRedirect(self._get_redirect_url(transaction, _next))
            return render(request, "payments/paytm/response.html", {"paytm": data_dict})
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    def _get_redirect_url(self, transaction, _next):
        status = "success" if transaction.STATUS == "TXN_SUCCESS" else "failed"
        transaction_complete_url = "/api/transaction/complete/"
        return transaction_complete_url + \
            """?transaction_date=%s&transaction_id=%s&amount=%s&currency=%s&payment_mode=%s&bank_transaction_id=%s&bank_name=%s&message=%s&transaction_status=%s&next=%s""" % (
                transaction.TXNDATE, transaction.TXNID, transaction.TXNAMOUNT,
                transaction.CURRENCY, transaction.PAYMENTMODE, transaction.BANKTXNID, transaction.BANKNAME,
                transaction.RESPMSG, status, _next
            )

    def _get_user(self, **kwargs):
        """
        Returns the user performing the transaction.
        """

        user = None
        if self.request.user and self.request.user.is_authenticated:
            user = self.request.user
        elif 'transaction' in kwargs:
            transaction = kwargs.pop('transaction', None)
            if transaction:
                user = transaction.created_by
        else:
            entity_type = kwargs.pop("entity_type", None)
            entity_id = kwargs.pop("entity_id", None)
            if entity_type and entity_id:
                if entity_type == "order":
                    try:
                        user = Order.objects.get(id=entity_id).customer.user
                    except Order.DoesNotExist:
                        pass
                # TODO : To be done for invoice (if required).
        return user

    def _get_order(self, entity_type, entity_ids, user, amount=0, order_number=None, service=PAYTM, is_add_balance=False):
        """
        Returns transaction order for given `entity_type` and `entity_id`.
        """

        _dict = {}
        if entity_type == "order":
            if not entity_ids and not is_add_balance:
                return False
            _dict = {
                'service': PAYTM,
                'amount': amount,
                'order_number': order_number
            }

        if _dict:
            trans_order = TransactionOrder.objects.create(**_dict)
            trans_order.created_by = user
            trans_order.save()
            if not is_add_balance:
                for entity_id in entity_ids:
                    _dict = {
                        'order_id': entity_id,
                        'transaction_order_id': trans_order.id
                    }
                    order_entity = OrderEntity.objects.create(**_dict)
                    order_entity.save()
            return trans_order

    def _get_bill_amount(self, entity_type, entity_ids):
        """
        Returns the bill amount that customer will be charged.
        """

        if entity_type == "order":
            if not entity_ids:
                return 0
            total_amount = Order.objects.filter(pk__in=entity_ids).aggregate(amount=Sum('total_payable'))['amount']
            logging.info('PayTM payment proceeding for orders with ids %s with amount %s' % (entity_ids, total_amount))
            return float(total_amount) if total_amount else 0.0

    def on_success(self, transaction_order, amount=0, is_add_balance=False, customer=None):
        """
        Any logic on successful completion of an order.
        Try to keep whole (or most) of logic asynchronous.
        """

        transaction_order.status = SUCCESS
        transaction_order.save()
        if not is_add_balance:
            broadcast_payment.apply_async((transaction_order.id,), )

        order_entities = transaction_order.trans_entities.all()
        for entity in order_entities:
            order = entity.order
            if order.payment_status == PAYMENT_STATUS_UNPAID:
                order.payment_status = PAYMENT_STATUS_PAID
                order.payment_mode = PAYMENT_MODE_PAYTM
                if order.status == StatusPipeline.ORDER_PENDING:
                    order.status = StatusPipeline.ORDER_NEW
                order.save()

        if customer:
            customer.add_balance_to_wallet(
                amount=amount,
                payment_type='paytm',
                transaction_order=transaction_order
            )

    def on_failure(self, transaction_order):
        """
        Any logic on failed completion of an order.
        Try to keep whole (or most) of logic asynchronous.
        """

        transaction_order.status = FAILURE
        transaction_order.save()


class TransactionOrderComplete(TemplateView):
    template_name = "payments/paytm/order-complete.html"

    def get_context_data(self, **kwargs):
        return {'default_redirect_url': settings.HOST_URL}


class PayTmCheckSum(generics.CreateAPIView):
    """
    PayTm CheckSum generation and validation.
    """

    MERCHANT_KEY = getattr(settings, 'PAYTM_MERCHANT_KEY', None)

    # @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PayTmCheckSum, self).dispatch(*args, **kwargs)

    def _get_user(self, **kwargs):
        """
        Returns the user performing the transaction.
        """
        user = None
        if self.request.user and self.request.user.is_authenticated:
            user = self.request.user
        else:
            order_instance = kwargs.pop("order_instance", None)
            if order_instance:
                user = order_instance.customer.user
        return user

    def _get_order(self, entity_type, entity_ids, user, amount=0, order_number=None, is_add_balance=False):
        """
        Returns transaction order for given `entity_type` and `entity_id`.
        """

        _dict = {}
        if entity_type == "order":
            if not entity_ids and not is_add_balance:
                return False
            _dict = {'service': PAYTM, 'amount': amount, "order_number":order_number}
        if _dict:
            trans_order, is_created = TransactionOrder.objects.get_or_create(**_dict)
            trans_order.created_by = user
            trans_order.save()
            if not is_add_balance:
                for entity_id in entity_ids:
                    _dict = {'order_id': entity_id, 'transaction_order_id': trans_order.id}
                    order_entity = OrderEntity.objects.create(**_dict)
                    order_entity.save()
            return trans_order

    def generate_checksum(self, payload):
        if payload:
            checksum = Checksum.generate_checksum(
                payload.get("data", {}), self.MERCHANT_KEY)

            if isinstance(checksum, bytes):
                checksum = checksum.decode("utf-8")
            data = {
                'checksum': checksum
            }
            http_status = status.HTTP_200_OK
        else:
            data = {'errors': "Insufficient arguments."}
            http_status = status.HTTP_400_BAD_REQUEST
        return data, http_status

    def validate_checksum(self, payload):
        param_dict = payload.get("data", {})
        check_sum = payload.get("checksum", {})
        if param_dict and check_sum:
            data = {'status': Checksum.verify_checksum(
                param_dict, self.MERCHANT_KEY, check_sum)}
        else:
            data = {'errors': "Insufficient arguments."}
        return data, status.HTTP_200_OK

    def post(self, request, payload=None):
        action = request.GET.get('action', None)
        if not payload and self.request.body:
            try:
                payload = json.loads(self.request.body)
            except:
                pass
        result = {'data': None, 'errors': ''}
        is_add_balance = True if payload['data'].get('ADD_BALANCE', '') == 'true' else False
        payload['data'].pop('ADD_BALANCE', '')
        if not payload:
            result['errors'] = "Insufficient arguments."
            status_http = status.HTTP_400_BAD_REQUEST
            return HttpResponse(json.dumps(result), status_http)
        if action == "validate":
            data, status_http = self.validate_checksum(payload)
            result['data'] = data
        else:
            trans_order = None
            amount_payable = payload['data'].get('TxnAmount', 0)
            if not amount_payable:
                amount_payable = payload['data'].get('TXN_AMOUNT', 0)

            curr_order_numbers = payload['data'].pop('ORDER_ID', '')
            if curr_order_numbers and not is_add_balance:
                curr_order_numbers = curr_order_numbers.split(',')
            callback_url = payload['data'].get('CALLBACK_URL', '')

            if curr_order_numbers and not is_add_balance:
                orders = Order.objects.filter(number__in=curr_order_numbers)
                if orders:
                    user = self._get_user(order_instance=orders[0])
                    order = self._get_order(
                        entity_type="order",
                        entity_ids=orders.values_list('id', flat=True),
                        user=user,
                        amount=amount_payable
                    )
                else:
                    result['errors'] = "No such order exists"
                    status_http = status.HTTP_400_BAD_REQUEST
            elif not is_add_balance:
                generator = OrderNumberGenerator()
                order = generator.order_number(settings.C2C_ORDER_TYPE)
            else:
                order = curr_order_numbers
                user = request.user
                trans_order = self._get_order(
                    entity_type="order",
                    order_number=order,
                    entity_ids=[],
                    user=user,
                    amount=amount_payable,
                    is_add_balance=is_add_balance
                )

            if trans_order is not None:
                payload['data']['ORDER_ID'] = str(trans_order.order_number)
            else:
                payload['data']['ORDER_ID'] = str(order)
            if callback_url:
                if "ORDER_ID=" in callback_url:
                    payload['data']['CALLBACK_URL'] = callback_url.split("ORDER_ID=")[0] + "ORDER_ID=" + str(order)
                else:
                    payload['data']['CALLBACK_URL'] = callback_url
            data, status_http = self.generate_checksum(payload)
            result['data'] = data
            result['data']['paytm_order_id'] = str(order)
            print(result)
        return HttpResponse(json.dumps(result), status_http)


class PayTmTransactionOrderUpdate(generics.CreateAPIView, CustomerMixin):

    MERCHANT_KEY = getattr(settings, 'PAYTM_MERCHANT_KEY', None)
    MERCHANT_ID = getattr(settings, 'PAYTM_MERCHANT_ID', None)
    CALLBACK_URL = getattr(settings, 'PAYTM_CALLBACK_URL', None)
    WEBSITE = getattr(settings, 'PAYTM_WEBSITE', None)
    INDUSTRY_TYPE_ID = getattr(settings, 'PAYTM_INDUSTRY_TYPE_ID', 'Retail')
    CHANNEL_ID = getattr(settings, 'PAYTM_CHANNEL_ID', 'WEB')
    ENDPOINT_TRANSN_REQUEST = getattr(
        settings, 'PAYTM_ENDPOINT_TRANSN_REQUEST', None)

    # @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PayTmTransactionOrderUpdate, self).dispatch(*args, **kwargs)

    def post(self, request):
        data_dict = json.loads(self.request.body)
        CHECKSUM = data_dict.pop('CHECKSUMHASH')
        customer = self.get_customer(request.user)
        is_add_balance = True if data_dict.pop('ADD_BALANCE', '') else False
        from_mitra = data_dict.pop('from_mitra', 'false')
        mitra_order_number = data_dict.pop('booking_key', None)
        verify = Checksum.verify_checksum(
            data_dict, self.MERCHANT_KEY, CHECKSUM)
        http_status, result = status.HTTP_400_BAD_REQUEST, {}

        from_mitra = True if from_mitra == 'true' else False
        if verify:
            transaction_order = TransactionOrder.objects.get(
                order_number=data_dict.pop('ORDERID'))
            logging.info('PayTM payment proceeding for order %s. '
                         'Gateway Response %s' % (
                            transaction_order.id, str(data_dict)))
            data_dict['order'] = transaction_order
            user = self._get_user(transaction=transaction_order)
            transaction = PayTmHistory.objects.create(user=user, **data_dict)
            transaction_status = data_dict.get("STATUS", None)
            http_status, result = status.HTTP_200_OK, {}
            if transaction_status == "TXN_SUCCESS":
                # Cross checking once again using transaction status API (As
                # per paytm security policy)
                if get_transaction_status(transaction_order) == "TXN_SUCCESS":
                    logging.info('PayTM payment status success for order %s' % (
                        transaction_order.order_number,))
                    result['status'] = True
                    self.on_success(transaction_order, amount=float(transaction_order.amount),
                                    is_add_balance=is_add_balance, customer=customer)
                    # confirm ceat order
                    if from_mitra and mitra_order_number:
                        EcommOrderCreator().update_order_payment(
                            mitra_order_number)
                        create_order_on_ceat_system.apply_async(args=[mitra_order_number])
                        #create_ceat_order(mitra_order_number)
                    else:
                        result['payment_details'] = customer.get_payment_details() if not is_add_balance else {}
                        result.update({
                            'status': True,
                            'amount': float(transaction_order.amount),
                            "available_balance": customer.get_customer_balance(),
                            "transaction_info": customer.get_last_transactions()
                        })

                elif get_transaction_status(transaction_order) == "TXN_FAILURE":
                    logging.info('PayTM payment status failed for order %s' % (
                        transaction_order.order_number,))
                    result['status'] = False
                    self.on_failure(transaction_order)
            elif transaction_status == "TXN_FAILURE":
                logging.info('PayTM payment failed for order %s' %
                             (transaction_order.order_number,))
                result['status'] = False
                self.on_failure(transaction_order)
            elif transaction_status == "PENDING":
                update_paytm_transaction_status(transaction_order.id)
                result['status'] = False
        return HttpResponse(json.dumps(result), http_status)

    def _get_user(self, **kwargs):
        """
        Returns the user performing the transaction.
        """

        user = None
        if self.request.user and self.request.user.is_authenticated:
            user = self.request.user
        elif 'transaction' in kwargs:
            transaction = kwargs.pop('transaction', None)
            if transaction:
                user = transaction.created_by
        return user

    def on_success(self, transaction_order, amount=0, is_add_balance=False, customer=None):
        """
        Any logic on successful completion of an order.
        Try to keep whole (or most) of logic asynchronous.
        """
        transaction_order.status = SUCCESS
        transaction_order.save()
        if customer:
            broadcast_payment.apply_async((transaction_order.id,), )
        order_entities = transaction_order.trans_entities.all()
        for entity in order_entities:
            order = entity.order
            self.customer = order.customer
            if order.payment_status == PAYMENT_STATUS_UNPAID:
                order.payment_status = PAYMENT_STATUS_PAID
                order.payment_mode = PAYMENT_MODE_PAYTM
                order.save()

        if customer and is_add_balance:
            customer.add_balance_to_wallet(amount=amount, payment_type='paytm', transaction_order=transaction_order)

    def on_failure(self, transaction_order):
        """
        Any logic on failed completion of an order.
        Try to keep whole (or most) of logic asynchronous.
        """

        transaction_order.status = FAILURE
        transaction_order.save()


class PayTmRefund(views.View):
    MERCHANT_ID = getattr(settings, 'PAYTM_MERCHANT_ID', None)
    INDUSTRY_TYPE_ID = getattr(settings, 'PAYTM_INDUSTRY_TYPE_ID', 'Retail')
    MERCHANT_KEY = getattr(settings, 'PAYTM_MERCHANT_KEY', None)
    CALLBACK_URL = getattr(settings, 'PAYTM_CALLBACK_URL', None)
    WEBSITE = getattr(settings, 'PAYTM_WEBSITE', 'APPSTAGING')
    CHANNEL_ID = getattr(settings, 'PAYTM_CHANNEL_ID', 'WEB')
    ENDPOINT_TRANSN_REQUEST = getattr(
        settings, 'PAYTM_ENDPOINT_TRANSN_REQUEST', None)

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PayTmRefund, self).dispatch(*args, **kwargs)

    def __generate_payload_data(self):
        self.checksum_data = {

        }

        self.checksum_payload = {
            "data": {
                "MID": self.MERCHANT_ID,
                "ORDERID": self.paytm_trans_order_number,
                "REFUNDAMOUNT": self.order_amount,
                "TXNID": self.transaction_number,
                "REFID": self.paytm_trans_order_number,
                "TXNTYPE": "REFUND",
            }
        }
        self.checksum_data['payload'] = self.checksum_payload

    def generate_checksum(self, payload):
        if payload:
            checksum = Checksum.generate_refund_checksum(
                payload.get("data", {}), self.MERCHANT_KEY)

            if isinstance(checksum, bytes):
                checksum = checksum.decode("utf-8")
            data = {
                'checksum': checksum
            }
            http_status = status.HTTP_200_OK
        else:
            data = {'errors': "Insufficient arguments."}
            http_status = status.HTTP_400_BAD_REQUEST
        return data, http_status

    def __generate_checksum(self):
        data, status_code = self.generate_checksum(payload=self.checksum_payload)

        if status_code != 200:
            raise BaseException('Error while generating CheckSum')
        self.checksum_response = data

    def get_refund_response(self, data):
        transaction_status_url = getattr(settings, 'PAYTM_REFUND_REQUEST_URL', None)
        input_data_url_encoded = parse.quote_plus(str(data))
        url = transaction_status_url + "?JsonData=" + input_data_url_encoded
        response = requests.post(url)
        return response

    def __refund(self):
        data = {
            "MID": self.MERCHANT_ID,
            "ORDERID": self.paytm_trans_order_number,
            "TXNTYPE": "REFUND",
            "REFUNDAMOUNT": self.order_amount,
            "TXNID": self.transaction_number,
            "REFID": self.paytm_trans_order_number,
            'CHECKSUM': self.checksum_response.get('checksum')}

        paytm_response = self.get_refund_response(data)

        return paytm_response

    def __initialize_params(self, data):
        self.error_message = None

        self.id = data.get('id')
        self.paytm_trans_order_number = None
        if not self.id:
            self.error_message = 'Order id is missing'
            return
        self.order = Order.objects.filter(id=self.id).first()
        self.number = self.order.number
        if not self.order:
            self.error_message = 'Invalid Order'
            return
        self.order_amount = str(self.order.amount_paid_online)
        self.customer = self.order.customer
        self.user = self.customer.user
        self.transaction_number = self.order.order_entities.exclude(
            transaction_order__transactions__TXNID=None
            ).values_list('transaction_order__transactions__TXNID', flat=True).first()

        self.trans_order = TransactionOrder.objects.filter(trans_entities__order__number=self.number).first()
        self.paytm_trans_order_number = self.trans_order.order_number if self.trans_order else None

        if not self.transaction_number:
            self.error_message = 'Transaction number is missing'
            return

        if not self.paytm_trans_order_number:
            self.error_message = 'Paytm Order number is missing'
            return

    def post(self, request):
        try:
            data = json.loads(request.data)
        except:
            data = request.POST.dict()

        self.__initialize_params(data)
        if self.error_message:
            return HttpResponse(self.error_message, status.HTTP_400_BAD_REQUEST)

        try:
            self.__generate_payload_data()
            self.__generate_checksum()
            paytm_response = self.__refund()
        except BaseException as e:
            print(e)
            return HttpResponse(e, status.HTTP_400_BAD_REQUEST)

        if paytm_response:
            json_response = paytm_response.json()
            paytm_history = {}
            for field in PayTmHistory._meta.get_fields():
                if json_response.get(field.name, ''):
                    paytm_history[field.name] = json_response.get(field.name, '')
            paytm_history['Response'] = json_response
            paytm_history['is_refund_request'] = True
            PayTmHistory.objects.create(user=self.user, order=self.trans_order, **paytm_history)
            logging.info(json_response)
            if json_response and json_response.get('STATUS') == 'TXN_SUCCESS':
                self.order.is_refunded = True
                self.order.save()
                return HttpResponse(paytm_response, status=status.HTTP_200_OK)
            return HttpResponse(paytm_response, status=status.HTTP_400_BAD_REQUEST)
        return HttpResponse('Bad Request', status=status.HTTP_400_BAD_REQUEST)


class PayTmWithdrawal(generics.CreateAPIView, CustomerMixin):

    MERCHANT_ID = getattr(settings, 'PAYTM_MERCHANT_ID', None)
    INDUSTRY_TYPE_ID = getattr(settings, 'PAYTM_INDUSTRY_TYPE_ID', 'Retail')

    #@method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PayTmWithdrawal, self).dispatch(*args, **kwargs)

    def __generate_payload_data(self):
        self.checksum_data = {
            'action': ''
        }
        self.checksum_payload = {
            "data": {
                "MID": self.MERCHANT_ID,
                "ReqType": "WITHDRAW",
                "ORDER_ID": ','.join(self.order_numbers),
                "TxnAmount": self.order_amount,
                "AppIP": "127.0.0.1",
                "Currency": "INR",
                "DeviceId": self.paytmuser.customer_mobile,
                "SSOToken": self.paytmuser.access_token,
                "PaymentMode": "PPI",
                "CustId": self.paytmuser.resource_owner_id,
                "IndustryType": self.INDUSTRY_TYPE_ID,
                "Channel": "WAP",
                "AuthMode": "USRPWD",
                "MOBILE_NO": self.mobile,
                "EMAIL": self.email,
                "ADD_BALANCE": self.request_is_add_balance,
            }
        }
        self.checksum_data['payload'] = self.checksum_payload

    def __generate_checksum(self):
        request = HttpRequest()
        request.method = 'POST'
        request.user = self.customer.user
        request.META = self.request.META
        request.data = json.dumps(self.checksum_data)
        response = PayTmCheckSum.as_view()(request, payload=self.checksum_payload)

        if response.status_code != 200:
            raise BaseException('Error while generating CheckSum')
        self.checksum_response = json.loads(response.content)['data']

    def __withdraw_from_paytm(self):
        request_data = {}
        request_data['MID'] = self.MERCHANT_ID
        request_data['ReqType'] = "WITHDRAW"
        request_data['TxnAmount'] = self.order_amount
        request_data['AppIP'] = "127.0.0.1"
        request_data['OrderId'] = self.checksum_response.get('paytm_order_id')
        request_data['Currency'] = "INR"
        request_data['DeviceId'] = self.mobile
        request_data['SSOToken'] = self.paytmuser.access_token
        request_data['PaymentMode'] = "PPI"
        request_data['CustId'] = self.paytmuser.resource_owner_id
        request_data['IndustryType'] = self.INDUSTRY_TYPE_ID
        request_data['Channel'] = "WAP"
        request_data['AuthMode'] = "USRPWD"
        request_data['CheckSum'] = self.checksum_response.get('checksum')
        request_data['MOBILE_NO'] = self.mobile
        request_data['EMAIL'] = self.email
        request_data['user'] = self.customer.user

        paytm_response = get_withdraw_response(request_data)

        if not paytm_response.get('status'):
            self.result['status'] = False
            self.http_status = status.HTTP_400_BAD_REQUEST
            self.reason = paytm_response.get('response')
            if not settings.ENABLE_SLACK_NOTIFICATIONS:
                return

            if len(self.order_numbers) == 1:
                template_dict = sms_templates.paytm_debit_failure
                message = template_dict['text'] % (
                    self.order_numbers[0],
                    self.reason,
                    self.order_amount,
                    get_tracking_url(self.request, self.order_numbers[0]))
            else:
                template_dict = sms_templates.paytm_debit_bulk_failure
                message = template_dict['text'] % (
                    self.order_numbers,
                    self.reason,
                    self.order_amount)

            self.customer.user.send_sms(message, template_dict)
        else:
            self.txn_id = paytm_response.get('response')

            if not settings.ENABLE_SLACK_NOTIFICATIONS:
                return

            template_dict = sms_templates.paytm_debit_success
            message = template_dict['text'] % (
                self.order_amount,
                self.order_numbers,
            )
            self.customer.user.send_sms(message, template_dict)

    def __initialize_params(self, data, request=None):
        self.error_message = None
        self.reason = None
        self.order_numbers = data.get('number').split(",")
        self.order_amount = data.get('amount')
        self.request_is_add_balance = data.get('add_balance', 'false')
        self.is_add_balance = True if data.get('add_balance', '') else False
        if not self.is_add_balance:
            self.orders = Order.objects.filter(number__in=self.order_numbers)
            if not self.orders:
                self.error_message = 'Invalid Order'
                return
            self.customer = self.orders[0].customer
        elif request:
            self.customer = self.get_customer(request.user)
        self.email = self.customer.user.email
        self.paytmuser = PayTmUserDetails.objects.filter(customer=self.customer).order_by('-created_date').first()
        self.mobile = self.paytmuser.customer_mobile
        self.http_status, self.result = status.HTTP_200_OK, {'status': True}
        self.txn_id = False
        self.auto_debit = data.get('auto_debit', False)

    def post(self, request):
        try:
            data = json.loads(request.data)
        except:
            data = request.POST.dict()
        self.__initialize_params(data, request)
        if self.error_message:
            return HttpResponse(self.error_message, status.HTTP_400_BAD_REQUEST)

        try:
            self.__generate_payload_data()
            self.__generate_checksum()
            self.__withdraw_from_paytm()
        except BaseException as e:
            print(e)
            return HttpResponse(e, status.HTTP_400_BAD_REQUEST)

        if self.http_status == status.HTTP_200_OK:
            if not self.is_add_balance:
                Order.objects.filter(number__in=self.order_numbers).update(
                    payment_status=PAYMENT_STATUS_PAID,
                    payment_mode=PAYMENT_MODE_PAYTM, amount_paid_online=F('total_payable'))
                if settings.ENABLE_SLACK_NOTIFICATIONS:
                    for order in self.orders:
                        send_mail.apply_async((order.id,), )
                self.result['payment_details'] = self.customer.get_payment_details()
            else:
                user = request.user
                PayTmCheckSum()._get_order(
                    entity_type="order",
                    order_number=self.order_numbers[0],
                    entity_ids=[],
                    user=user,
                    amount=self.order_amount,
                    is_add_balance=self.is_add_balance
                )
                transaction_order = TransactionOrder.objects.get(
                    order_number=self.order_numbers[0])
                PayTm().on_success(transaction_order, amount=float(transaction_order.amount),
                                is_add_balance=self.is_add_balance, customer=self.customer)

        if not self.is_add_balance:
            send_slack_notification_for_payment.delay(
                self.order_numbers, self.order_amount,
                self.txn_id, PAYMENT_MODE_PAYTM,
                self.reason, self.auto_debit)
        if self.is_add_balance:
            data = {
                'status': True,
                'amount': float(transaction_order.amount),
                "available_balance": self.customer.get_customer_balance(),
                "transaction_info": self.customer.get_last_transactions()
            }
            return Response(data=data, status=self.http_status)

        return HttpResponse(json.dumps(self.result), self.http_status)


class PayTmUserDetailsView(generics.CreateAPIView):
    permission_classes = (IsCustomer, )

    def post(self, request):
        data = request.body
        try:
            data = json.loads(data)
        except:
            data = request.data.dict()

        if type(data) != dict:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        customer = CustomerMixin().get_customer(request.user)
        access_token = data.get('access_token')
        expires = data.get('expires')
        scope = data.get('scope')
        customer_mobile = data.get('customer_mobile')
        resource_owner_id = data.get('resourceOwnerId')

        _dict = {
            'access_token': access_token,
            'expires': expires,
            'scope': scope,
            'customer_mobile': customer_mobile,
            'resource_owner_id': resource_owner_id,
            'customer': customer
        }
        PayTmUserDetails.objects.create(**_dict)

        return Response(status=status.HTTP_200_OK)


class PaymentBankDriverDetailsView(api_view.APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        data = request.body
        try:
            data = json.loads(data)
        except:
            data = request.data.dict()
        entity = data.get('entity')
        driver_id = data.get('driverId')
        trip_number = data.get('trip')

        if not entity or not driver_id:
            _out = {
                "message": "failure",
                "content": {
                    "error": "entity and driverId is mandatory field"
                }
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        api_key = request.META.get('HTTP_API_KEY', None)
        _verified = is_auth_payment_bank(entity, api_key)

        if not _verified:
            _out = {'message': 'failure',
                    'content': {'error': 'Invalid authentication credential'}
                    }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        driver_data = get_driver_data(driver_id, trip_number)
        reference_id = None
        if driver_data.get('status'):
            reference_id = get_payment_bank_reference('APB', driver_data)

        if reference_id:
            _out = {
                "message": "success",
                "content": {
                    "Name": driver_data.get('name'),
                    "driverId": driver_id,
                    "trip": trip_number,
                    "amount": driver_data.get('amount'),
                    "referenceId": reference_id
                }
            }
            return Response(status=status.HTTP_200_OK, data=_out)
        else:
            _out = {
                "message": "failure",
                "content": {
                    "error": driver_data.get('msg'),
                    "driverId": driver_id,
                    "trip": trip_number,
                }
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)


class PaymentBankTxnView(api_view.APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        data = request.body
        try:
            data = json.loads(data)
        except:
            data = request.data.dict()

        entity = data.get('entity')
        reference_id = data.get('referenceId')
        amount = data.get('amount')
        transaction_id = data.get('transactionId')
        txn_date = data.get('txn_date')

        if not entity or not reference_id or not amount or not transaction_id:
            _out = {'message': 'failure',
                    'content': {'error': 'Request parameter is missing'}
                    }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        api_key = request.META.get('HTTP_API_KEY', None)
        _verified = is_auth_payment_bank(entity, api_key)

        if not _verified:
            _out = {'message': 'failure',
                    'content': {'error': 'Invalid authentication credential'}
                    }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            data['amount'] = int(amount)
        except:
            _out = {'message': 'failure',
                    'content': {'error': 'Invalid amount value'}
                    }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            data['txn_date'] = parse_datetime(txn_date)
            if not is_aware(data['txn_date']):
                data['txn_date'] = make_aware(data['txn_date'])
        except:
            _out = {'message': 'failure',
                    'content': {
                        'error': 'Date format should in format like -  2020-01-07T19:35:06.959084'}
                    }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        _status, msg = process_payment_bank_txn(data)

        return Response(status=_status, data=msg)


class EzetapPaymentViews(api_view.APIView):

    permission_classes = (AllowAny, )

    def post(self, request, order_number):
        data = request.data

        _status = data.get('status')

        if data.get('result').get('references'):
            order_ref = data.get('result').get('references').get('reference1')
            amount = data.get('result').get('txn').get('amount')
        else:
            order_ref = data.get('result').get('data')[0].get('references').get('reference1')
            amount = data.get('result').get('data')[0].get('txn').get('amount')
        print('order_ref - ', order_ref)
        if not order_ref:
            _msg = {
                "message": "Field reference1 not found"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_msg)

        if not order_number == order_ref:
            _msg = {
                "message": "Order number doesn't not match payment reference"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_msg)

        order = Order.objects.filter(number=order_ref).first()
        print('order - ', order)
        if not order:
            _msg = {
                "message": "No order found"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_msg)

        payment_data = {
            'order_id': order.id,
            'status': _status,
            'amount': amount,
            'response_data': data
        }
        EzetapPayment.objects.create(**payment_data)

        Order.objects.filter(id=order.id).update(
            payment_status=PAYMENT_STATUS_PAID,
            payment_mode=PAYMENT_MODE_EZETAP,
            amount_paid_online=amount)

        _msg = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_msg)

