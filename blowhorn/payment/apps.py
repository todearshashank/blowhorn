# from django.conf.urls import url, include
#
# from django.apps import AppConfig
#
#
# class PaymentConfig(AppConfig):
#
#     label = 'payment'
#     name = 'blowhorn.payment'
#     verbose_name = 'Payment'
#
#     namespace = 'payment'
#
#     def get_urls(self):
#         from blowhorn.payment import views
#
#         urlpatterns = [
#             url(r'^razorpay', views.Razorpay.as_view(), name='razorpay'),
#             # Paytm - Payment page generation and Call back handling
#             url(r'^paytm/$', views.PayTm.as_view(), name='paytm'),
#             # Paytm - Generate checksum
#             url(r'^paytm/checksum/$', views.PayTmCheckSum.as_view(),
#                 name='paytm-check-sum'),
#             # Intermediate transaction page (success / failure)
#             url(r'^transaction/complete/',
#                 views.TransactionOrderComplete.as_view(),
#                 name='order-complete'),
#             # Paytm - verification of the order's status update
#             url(r'^paytm/order-status-update/$',
#                 views.PayTmTransactionOrderUpdate.as_view(),
#                 name='order-status-update'),
#             # Paytm - creation/updation of Paytm users details
#             url(r'^paytm/users/details$', views.PayTmUserDetailsView.as_view(),
#                 name='paytm-users-details'),
#             # Paytm - withdrawal of money from paytm user account
#             url(r'^paytm/users/withdraw$', views.PayTmWithdrawal.as_view(),
#                 name='paytm-user-withdrawal'),
#             url(r'^paytm/refund$', views.PayTmRefund.as_view(),
#                 name='paytm-refund'),
#
#             # Payfast Gateway
#             url(r'^payfast', include('blowhorn.payment.payfast.urls')),
#             # Payment Bank
#             url(r'^payment-bank/driver/details',
#                 views.PaymentBankDriverDetailsView.as_view(),
#                 name='PB-driver-amount'),
#             url(r'^payment-bank/create-transaction',
#                 views.PaymentBankTxnView.as_view(),
#                 name='PB-Cash-Deposit-Txn'),
#         ]
#         return self.post_process_urls(urlpatterns)
