import requests
import re

from django.conf import settings
from blowhorn.common import sms_templates
from blowhorn.common.sms_templates import *

URL = settings.HOST + '/api/customer/sendsms'
HEADER = { "api-key" : 'P1a03N19d91B', "Content-type": "application/json" }

class TestSMSSending():

    def send_sms(self, provider, phone_number, templates):
        i = 0;
        for tem in templates:
            input_data = {}
            template = eval(tem)
            print(' %s Sending message: %s' % (i, tem))
            input_data['template_id'] = template['template_id'] 
            input_data['message'] = template['text'] 
            input_data['phone_number'] = phone_number
            input_data['service'] = provider
            i +=1
            if settings.ENABLE_SLACK_NOTIFICATIONS:
                response = requests.post(URL, json=input_data, headers=HEADER)
                print(response)

    def test_sms_kaleyra(self, phone_number):
        print('Testing Kaleyra Service')
        _vars = dir(sms_templates)
        _valid_templates = []

        for var in _vars:
            temp = re.match('^_|settings', var)
            if not temp:
                _valid_templates.append(var)

        self.send_sms('kaleyra', phone_number, _valid_templates)

    def test_sms_exotel(self, phone_number):
        print('Testing Exotel Service')
        _vars = dir(sms_templates)
        _valid_templates = []

        for var in _vars:
            temp = re.match('^_|settings', var)
            if not temp:
                _valid_templates.append(var)

        self.send_sms('exotel', phone_number, _valid_templates)
