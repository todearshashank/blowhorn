from blowhorn.logapp.constants import TEMPLATE_TYPE_CHOICES
from django.db import models
from django.utils.translation import ugettext_lazy as _
from blowhorn.common.models import BaseModel

class SmsLog(BaseModel):
    message = models.TextField(_('Message'), null=False, blank=False)
    template_id = models.CharField(_('DLT Template ID'), max_length=50, blank=True, null=True)
    to_number = models.CharField(_('To'), max_length=20)
    sms_status = models.CharField(_('SMS Status'), max_length=50, blank=True, null=True)
    detailed_status = models.TextField(_('Detailed Status'), blank=True, null=True)
    ssid = models.CharField(_('SMS SID'), max_length=200, blank=True, null=True)
    status = models.BooleanField(_("Status"), default=False)
    provider = models.CharField(_('SMS Provider'), max_length=10, blank=True, null=True)
    errors = models.TextField(_('Errors'), null=True, blank=True)
    cost = models.DecimalField(_('Cost'), decimal_places=2, max_digits=5,null=True)
    units = models.DecimalField(_('Units'), decimal_places=2, max_digits=5,null=True)
    customer = models.ForeignKey("customer.Customer", on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.template_id

    class Meta:
        verbose_name = _('SMS Log')
        verbose_name_plural = _('SMS Logs')

class SmsTemplates(BaseModel):
    message = models.TextField(_('Message'))
    template_id = models.CharField(_('DLT Template ID'), max_length=50)
    template_type = models.CharField(_("Type"), max_length=50, choices=TEMPLATE_TYPE_CHOICES)
    template_name = models.CharField(_('Template Name'), max_length=200)
    is_active = models.BooleanField(_("Is Active"), default=False)

    def __str__(self):
        return self.template_id

    class Meta:
        verbose_name = _('SMS Template')
        verbose_name_plural = _('SMS Templates')