OTP = 'OTP'
MKT = 'MKT'
TXN = 'TXN'

TEMPLATE_TYPE_CHOICES = (
    (OTP, "OTP"),
    (MKT, "Marketing"),
    (TXN, "Transactional")
)