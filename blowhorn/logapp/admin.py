from django.contrib import admin
from blowhorn.logapp.models import SmsLog, SmsTemplates

# Register your models here.
class SmsLogAdmin(admin.ModelAdmin):
    list_display = ('template_id', 'customer', 'to_number', 'message',
                        'sms_status', 'created_date')
    list_filter = ('provider', 'sms_status',
                    ('customer', admin.RelatedOnlyFieldListFilter))

    search_fields = ('template_id', 'to_number', 'message')
    exclude = ('status',)

    def get_readonly_fields(self, request, obj=None):
        return ['template_id', 'customer', 'ssid', 'to_number', 'cost', 
                'units', 'provider', 'sms_status',
                'detailed_status', 'message', 'errors', 'created_date', 'modified_date']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class SmsTemplatesAdmin(admin.ModelAdmin):
    list_display = ('template_id', 'message', 'template_type',
                        'template_name', 'is_active')
    list_filter = ('template_type', 'is_active')

    search_fields = ('template_id', 'template_name')

    def has_add_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return True

admin.site.register(SmsLog, SmsLogAdmin)
admin.site.register(SmsTemplates, SmsTemplatesAdmin)