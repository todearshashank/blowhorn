# Django and System libraries
import logging
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.conf import settings as setting
from django.core.exceptions import ValidationError

from blowhorn.oscar.core.loading import get_model
from phonenumber_field.phonenumber import PhoneNumber

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.apps.operation_app.v2.serializers import vehicle as vehicle_serializer
from blowhorn.utils import functions as utils_func
from blowhorn.common.helper import CommonHelper
from blowhorn.apps.mixins import *


User = get_user_model()
Country = get_model('address', 'Country')
Driver = get_model('driver', 'Driver')
DriverPreferredLocation = get_model('driver', 'DriverPreferredLocation')
Vehicle = get_model('vehicle', 'Vehicle')
VehicleClass = get_model('vehicle', 'VehicleClass')
VehicleModel = get_model('vehicle', 'VehicleModel')
BodyType = get_model('vehicle', 'BodyType')
Vendor = get_model('vehicle', 'Vendor')

VEHICLE_EXIST = 'Vehicle with corresponding registration number already exists'
PHONE_NUMBER_EXIST = "Phone Number already exists"


def get_detailed_data(params):
    """
        Fragmentize the requested parameters into various dictionary of parameters
        needed for creating the records of a specific relation
    """
    country_code = setting.COUNTRY_CODE_A2
    country = Country.objects.get(pk=country_code)
    vehicle_parameters = params['vehicles'][
        0] if params.get('vehicles', []) else {}

    driver_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'can_drive_and_deliver': params.get('can_drive_and_deliver', ''),
        'can_source_additional_labour': params.get('can_source_additional_labour', ''),
        'driving_license_number': params.get('driving_license_number', params.get('mobile', '')),
        'work_status': params.get('work_status', ''),
        'working_preferences': params.get('working_preferences', []),
        'own_vehicle': params.get('driver_is_owner', False),
        'operating_city': params.get('operating_city', ''),
        'pan': params.get('pan_number', '')
    }

    vehicle_data = {
        'vehicle_model': vehicle_parameters.get('vehicle_model', ''),
        'vehicle_class': vehicle_parameters.get('vehicleClass', ''),
        'body_type': vehicle_parameters.get('body_type_name', ''),
        'registration_certificate_number': params.get('driver_vehicle', ''),
        'model_year': vehicle_parameters.get('vehicle_model_year', '')
    }

    user_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'password': params.get('password', None),
        'email': params.get('email', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'user_id': None,
        'is_active': True if params.get('status') == StatusPipeline.ACTIVE else False
    }

    vendor_data = {
        'phone_number': params.get('owner_contact_no', ''),
        'name': params.get('owner_name', ''),
        'pan': params.get('owner_pan_number', '')
    }

    detailed_data = {
        'driver_data': driver_data,
        'vehicle_data': vehicle_data,
        'user_data': user_data,
        'vendor_data': vendor_data
    }

    return detailed_data


def validate_operation_data(params):
    """
        Validating the requested data and raising error(if any).
    """
    response = {'error': '', 'success': False}

    # To check if the requested dictionary is not empty
    if not params:
        logging.error("Driver details not found")
        response["error"] = "Driver details not found"
        return response

    # To validate the name of the driver
    if not utils_func.validate_name(params.get('name', '')):
        logging.error("Please Enter a valid name")
        response['error'] = 'Please Enter a valid name'
        return response

    # To validate the phone number of the driver
    try:
        utils_func.validate_phone_number(params.get('mobile', ''))
    except ValidationError as e:
        logging.error(
            "Error while validating the driver phone number : %s " % e)
        response['error'] = str(e.args[0])
        return response

    if params.get('vehicles', []):
        if not params['vehicles'][0].get('vehicle_model_year', None):
            logging.error(
                "Error while validating the vehicle model year")
            response['error'] = "Model year is needed for the vehicle"
            return response

    # To validate the driving licence and owner details(*in case if driver is
    # not the owner) of the driver
    if params.get('driver_activation', False):
        if params.get('driving_license_number', '') == params.get('mobile', ''):
            logging.error('Driving License Number field is empty')
            response['error'] = 'Enter a valid driving license number'
            return response

        if not params.get('driver_is_owner', False):
            if not params.get('owner_name', ''):
                logging.error("Owner name not provided")
                response['error'] = "Owner's name is mandatory"
                return response

            if not utils_func.validate_name(params['owner_name']):
                logging.error("Please Enter a valid name")
                response['error'] = "Please Enter a valid Owner name"
                return response

            try:
                utils_func.validate_phone_number(
                    params.get('owner_contact_no', ''))
            except ValidationError as e:
                logging.error(
                    "Error while validating the owner phone number : %s " % e)
                response['error'] = str(e.args[0])
                return response
    response['success'] = True
    return response


def get_user_for_driver(user_data):
    """
        Creating/Updating the user details for the driver
    """
    user = Driver.objects.upsert_user(
        email=user_data.get('email', None),
        name=user_data.get('name', ''),
        phone_number=user_data.get('phone', None),
        password=user_data.get('password', None),
        user_id=user_data.get('user_id', None),
        is_active=True if user_data[
            'status'] == StatusPipeline.ACTIVE else False
    )

    return user


def get_vehicle_models():
    """
        Fetch all the vehicle models available
    """
    vehicle_model = VehicleModel.objects.all()
    body_types = BodyType.objects.values_list('body_type', flat=True)
    serialized_vehicle_data = vehicle_serializer.VehicleModelSerializer(
        vehicle_model, many=True, context={'body_types': body_types})

    return serialized_vehicle_data.data


def add_vehicle(vehicle_params, is_update):
    """
        Creating/Updating vehicle details for the driver
    """

    vehicle_model = VehicleModel.objects.filter(
        model_name=vehicle_params.get('vehicle_model')).first()
    body_type = BodyType.objects.filter(
        body_type=vehicle_params.get('body_type')).first()

    vehicle_data = {
        'vehicle_model': vehicle_model.id if vehicle_model else None,
        'body_type': body_type.id if body_type else None,
        'registration_certificate_number': vehicle_params.get('registration_certificate_number'),
        'model_year': vehicle_params.get('model_year')

    }

    if is_update:
        vehicle_instance = Vehicle.objects.filter(
            registration_certificate_number=vehicle_params['registration_certificate_number']).first()
        vehicle = UpdateModelMixin().update(data=vehicle_data, instance=vehicle_instance,
                                            serializer_class=vehicle_serializer.VehicleAddSerializer)
    else:
        vehicle = CreateModelMixin().create(data=vehicle_data,
                                            serializer_class=vehicle_serializer.VehicleAddSerializer)

    logging.info(
        "This is the vehicle created for the driver : %s" % vehicle)

    return vehicle


def add_vendor(vendor_details):
    """
        Creatingthe owner for the driver
    """

    phone_number = PhoneNumber(setting.ACTIVE_COUNTRY_CODE, vendor_details.get('phone_number'))

    vendor = Vendor.objects.filter(phone_number=phone_number,
                                   pan=vendor_details.get('pan', '')).first()

    if not vendor:
        vendor = CreateModelMixin().create(
            data=vendor_details, serializer_class=vehicle_serializer.VendorSerializer)

    logging.info("This is the vendor created for the driver : %s" % vendor)

    return vendor


def add_preferred_location(loction_data, driver):
    """
        Add the preferred locations for the Driver
    """
    logging.info(
        "This is the preferred location data for the driver : %s " % loction_data)
    preferred_location_data = []
    DriverPreferredLocation.objects.filter(driver=driver).delete()
    for cord_data in loction_data:
        if isinstance(cord_data, dict) and cord_data.get('coordinates', {}):
            latitude = cord_data.get(
                'coordinates').get('latitude', None)
            longitude = cord_data.get(
                'coordinates').get('longitude', None)

            logging.info('Latitude:- %s longitude: %s',
                         latitude, longitude)

            if latitude and longitude:
                geopoint = CommonHelper.get_geopoint_from_latlong(
                    latitude, longitude)

                preferred_location_data.append(DriverPreferredLocation(
                    geopoint=geopoint, driver_id=driver.id))

    if not preferred_location_data:
        logging.info("No Preferred location was provided for the driver")
        raise ValidationError("Preferred Location is mandatory")

    DriverPreferredLocation.objects.bulk_create(preferred_location_data)
