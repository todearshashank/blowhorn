# 3rd party libraries
from rest_framework import serializers

# blowhorn imports
from blowhorn.driver.models import *
from blowhorn.document.serializers import DocumentSerializer
from blowhorn.apps.operation_app.v1.serializers.driver import DriverListSerializer as DriverListSerializerOne
from blowhorn.apps.operation_app.v2.serializers.vehicle import VehicleDocumentListSerializer


class DriverListSerializer(DriverListSerializerOne):

    working_preferences = serializers.SerializerMethodField(
        source='get_working_preferences')

    driver_document_submitted = serializers.SerializerMethodField(
        source='get_driver_document_submitted')

    vehicle_document_submitted = serializers.SerializerMethodField(
        source='get_vehicle_document_submitted')

    owner_name = serializers.SerializerMethodField(
        source='get_owner_name')

    owner_pan_number = serializers.SerializerMethodField(
        source='get_owner_pan_number')

    class Meta:
        model = Driver
        fields = DriverListSerializerOne.Meta.fields + \
            ('owner_pan_number', 'owner_name',
             'driver_document_submitted', 'vehicle_document_submitted')

    def get_working_preferences(self, driver):
        if driver.working_preferences:
            return driver.working_preferences
        return []

    def get_driver_document_submitted(self, driver):
        driver_document_list = []
        driver_documents = driver.driver_document.all()
        if driver_documents:
            driver_document_list = DriverDocumentListSerializer(
                driver_documents, many=True).data
        return driver_document_list

    def get_vehicle_document_submitted(self, driver):
        vehicle_document_list = []
        vehicles = driver.vehicles.all()
        for vehicle in vehicles:
            vehicle_documents = vehicle.vehicle_document.all()
            if vehicle_documents:
                document_list = VehicleDocumentListSerializer(
                    vehicle_documents, many=True).data
                vehicle_document_list.extend(document_list)

        return vehicle_document_list

    def get_owner_name(self, driver):
        if driver.owner_details:
            return driver.owner_details.name
        return ''

    def get_owner_pan_number(self, driver):
        if driver.owner_details:
            return driver.owner_details.pan
        return ''


class DriverDocumentSerializer(DocumentSerializer):

    def create(self, validated_data):
        driver_doc = DriverDocument.objects.create(**validated_data)
        driver_doc.created_by = validated_data.get('created_by')
        driver_doc.modified_by = validated_data.get('modified_by')
        driver_doc.save()

        if not driver_doc:
            return False

        return driver_doc

    class Meta:
        model = DriverDocument
        fields = DocumentSerializer.Meta.fields + ('driver',)


class DriverDocumentListSerializer(DocumentSerializer):
    document_type = serializers.CharField(
        source='document_type.name')

    class Meta:
        model = DriverDocument
        fields = ('document_type', 'status')


class DriverSerializer(serializers.ModelSerializer):

    class Meta:
        model = Driver
        fields = '__all__'
