from blowhorn.apps.operation_app.v1.serializers.vehicle import *
from blowhorn.vehicle.models import VehicleDocument
from blowhorn.document.serializers import DocumentSerializer


class VehicleAddSerializer(BaseSerializer):

    class Meta:
        model = Vehicle
        fields = '__all__'


class VehicleDocumentListSerializer(DocumentSerializer):
    document_type = serializers.CharField(
        source='document_type.name')

    class Meta:
        model = VehicleDocument
        fields = ('document_type', 'status')
