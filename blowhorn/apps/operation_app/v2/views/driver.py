# Django and System libraries
import json
import logging
import datetime

from django import db

from rest_framework import views, status
from rest_framework.response import Response
from rest_condition import Or
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from blowhorn.oscar.core.loading import get_model
from pytz import timezone

# blowhorn imports
from blowhorn.apps.operation_app.v2.serializers import driver as driver_serializer
from blowhorn.common import serializers as common_serializer
from blowhorn.address.models import City
from blowhorn.document.models import DocumentType
from blowhorn.utils.functions import ist_to_utc
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.apps.operation_app.v1.constants import *
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.apps.operation_app.v2.services.driver import DriverAddUpdateMixin
from blowhorn.apps.operation_app.v2.helper import validate_operation_data
from blowhorn.driver.permissions import IsActiveDriver
from blowhorn.common.helper import CommonHelper


Driver = get_model('driver', 'Driver')
DriverVehicleMap = get_model('driver', 'DriverVehicleMap')
DriverDocument = get_model('driver', 'DriverDocument')
DriverPreferredLocation = get_model('driver', 'DriverPreferredLocation')

VEHICLE_EXIST = 'Vehicle with corresponding registration number already exists'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverDetails(views.APIView):
    """
        Retrieve or update the driver instance
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )

    def get(self, request, pk, format=None):
        """
            Returns the driver record data corresponding to the driver id
        """

        driver = Driver.objects.filter(id=pk).first()

        if not driver:
            logging.info("Invalid driver ID")
            return Response(data="Driver does not exist",
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        serializer = driver_serializer.DriverSerializer(driver)
        return Response(status=status.HTTP_200_OK,
                        data=serializer.data)

    def put(self, request, pk, format=None):
        """
            For updating the record of the driver corresponding to the id
        """
        driver = Driver.objects.filter(id=pk).first()

        if not driver:
            logging.info("Invalid driver ID")
            return Response(data="Driver does not exist",
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')
        logging.info(
            "Driver for which the updation is needed : %s" % str(driver))

        try:
            data = json.loads(request.body)
            driver_details = data.get('driver_details', {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get('driver_details', '{}'))
            logging.info(
                "Request from the app dict : " + str(request.data.dict()))

        response_dict = {'status': False, "message": ''}

        validated_data = validate_operation_data(driver_details)

        if not validated_data.get('success'):
            response_dict["message"] = validated_data.get('error')
            return Response(data=response_dict,
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        with db.transaction.atomic():
            try:
                updated_driver = DriverAddUpdateMixin()._update_driver(
                    driver_details, driver, request)
            except Exception as e:
                response_dict["message"] = e.args[0]
                return Response(data=response_dict,
                                status=status.HTTP_400_BAD_REQUEST,
                                content_type='text/html; charset=utf-8')

            return Response(status=status.HTTP_200_OK,
                            data=updated_driver)


class DriversList(views.APIView):
    """
        List all the filtered driver records or create a new record
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )

    def get(self, request):
        """
            For fetching all the drivers for the given city
        """

        logging.info(
            "Following is the request data for fetching driver list : %s"
            % str(request.GET))

        query_params = request.GET.dict()
        filter_data = {}

        logging.info(
            "Following is the query params for the driver list :"
            " " + str(query_params))

        city = City.objects.filter(
            name=query_params.get('operating_city')).first()

        if not city:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data='No driver details found for this city')

        filter_data['operating_city'] = city

        if query_params.get('timestamp', ''):
            try:
                timestamp = datetime.datetime.strptime(
                    query_params.get('timestamp'), '%Y-%m-%d %H:%M:%S')
                last_modified = ist_to_utc(timestamp)
                last_modified_utc = last_modified.replace(
                    tzinfo=timezone('UTC'))
            except Exception as e:
                return Response(
                    status=status.HTTP_404_NOT_FOUND,
                    data='Provide timestamp in correct format')

            filter_data['modified_date__gte'] = last_modified

        driver_list = Driver.objects.filter(
            **filter_data).prefetch_related('driverpreferredlocation_set').order_by('status')

        if driver_list:
            page_to_be_displayed = query_params.get('next') if query_params.get(
                'next', '') else PAGE_TO_BE_DISPLAYED
            number_of_entry = NUMBER_OF_ENTRY_IN_PAGE

            logging.info(
                'Page to be displayed for the pagination purpose'
                ': %s' % page_to_be_displayed)
            logging.info(
                'Number of entry per page for pagination purpose'
                ': %s' % number_of_entry)

            serialized_data = common_serializer.PaginatedSerializer(
                queryset=driver_list, num=number_of_entry,
                page=page_to_be_displayed,
                serializer_method=driver_serializer.DriverListSerializer
            )

            return Response(status=status.HTTP_200_OK,
                            data=serialized_data.data)

        else:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data='No driver found')

    def post(self, request):
        """
            For creating a new driver
        """

        try:
            data = json.loads(request.body)
            driver_details = data.get('driver_details', {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get('driver_details', '{}'))
            logging.info(
                "Request from the app dict : " + str(request.data.dict()))

        response_dict = {'status': False, "message": ''}

        validated_data = validate_operation_data(driver_details)

        if not validated_data.get('success'):
            response_dict["message"] = validated_data.get('error')
            return Response(data=response_dict,
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        sid = db.transaction.savepoint()

        try:
            created_driver = DriverAddUpdateMixin()._create_driver(driver_details, request)
        except Exception as e:
            logging.error("This is the exception : %s " % e)
            db.transaction.savepoint_rollback(sid)
            response_dict["message"] = e.args[0]
            return Response(data=response_dict,
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        return Response(status=status.HTTP_200_OK,
                        data=created_driver)


class DriverDocumentUpload(views.APIView):
    """
        To upload the driver document
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in driver document add : " + str(request.data.dict()))

        documents = request.data.dict()
        driver_id = ''
        doc_status = {}

        if not documents:
            logging.info(
                "No documents where provided for the verification of driver")
            return Response(data="Please upload the documents",
                            status=status.HTTP_400_BAD_REQUEST)

        if documents.get('document_status', []):
            document_status = json.loads(documents.pop('document_status'))

            for statuses in document_status:
                driver_id = driver_id if driver_id else statuses.get(
                    'driver_id', '')

                if statuses.get('document_type', ''):
                    doc_status[statuses['document_type']] = statuses.get(
                        'document_status', '')

        if not driver_id:
            logging.info(
                "Documents not added for the activatioon of driver")
            return Response(data="Documents for activation of Driver is needed",
                            status=status.HTTP_400_BAD_REQUEST)

        if not spoc_id:
            logging.info(
                "No process associates found to assign the action owner.")
            return Response(data="No process associates found to assign the action owner.",
                            status=status.HTTP_400_BAD_REQUEST)

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                logging.info(
                    "Invalid document type is provided")
                return Response(data="Provide the valid document type",
                                status=status.HTTP_400_BAD_REQUEST)

            document_data = {
                'file': documents[key],
                'document_type': doc_type.id,
                'driver': driver_id,
                'status': doc_status[key] if doc_status.get(key, '') else VERIFICATION_PENDING
            }

            if request.user:
                document_data['created_by'] = request.user
                document_data['modified_by'] = request.user

            document_serializer = driver_serializer.DriverDocumentSerializer(
                data=document_data)
            if document_serializer.is_valid():
                document_serializer.save(created_by=document_data.get(
                    'created_by'), modified_by=document_data.get('modified_by'))
            else:
                logging.info(document_serializer.errors)
                return Response(document_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='Driver Details Added')


class DriverPhoto(views.APIView):
    permission_classes = (IsAuthenticated, Or(IsBlowhornStaff, IsActiveDriver),)

    def post(self, request, pk):
        """
        For updating an existing driver
        """

        logging.info("Request from the app" + str(request.data.dict()))
        data = request.data.dict()
        response = {}
        driver = Driver.objects.filter(pk=pk).first()
        if not driver:
            return Response(data='Driver does not exist',
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')
        try:
            photo = data.get('photo')
            driver.photo = photo
            driver.save()
        except BaseException:
            return Response(data='Something went wrong !!!', status=status.HTTP_400_BAD_REQUEST)

        if driver.photo:
            media_url = CommonHelper().get_media_url_prefix()
            response['photo'] = media_url + str(driver.photo)
        return Response(status=status.HTTP_200_OK, data=response)
