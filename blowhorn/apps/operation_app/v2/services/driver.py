# Django and System libraries
import logging
import phonenumbers
from django.core.exceptions import ValidationError
from django import db
from django.conf import settings as setting

# blowhorn imports
from blowhorn.apps.operation_app.v2.helper import *
from blowhorn.driver import messages
from config.settings import status_pipelines as StatusPipeline
from blowhorn.address.models import City
from blowhorn.apps.operation_app.v2.serializers.driver import *
from blowhorn.driver.models import DriverVehicleMap, DriverHistory
from blowhorn.apps.operation_app.v1.constants import *

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverAddUpdateMixin(object):

    def __create_data_for_driver(self, params, **kwargs):
        """
            Create/Update the related records of the driver.
        """

        driver = kwargs.get('driver', None)
        is_update = kwargs.get('is_update', False)

        data = {}
        detailed_data = get_detailed_data(params)
        logging.info(
            "This is the detailed data for adding the driver : %s " % detailed_data)

        driver_data = detailed_data.get('driver_data', {})
        vehicle_data = detailed_data.get('vehicle_data', {})
        user_data = detailed_data.get('user_data', {})
        vendor_data = detailed_data.get('vendor_data', {})

        city = City.objects.filter(
            name=driver_data.get('operating_city')).first()

        if city:
            driver_data['operating_city'] = city.id

        if driver:
            user_data['user_id'] = driver.user_id

        try:
            user = get_user_for_driver(user_data)
        except Exception as e:
            logging.info("This is the error while creating the user : %s " % e)
            raise db.IntegrityError('Driver Phone number already exist')
        driver_data['user'] = user.id

        if params.get('driver_activation', False):
            if not driver_data.get('own_vehicle', False):
                try:
                    vendor = add_vendor(vendor_data)
                except Exception as e:
                    logging.info(
                        "Following error occured while creating vendor : %s " % str(e))
                    raise ValidationError('Please Enter valid vendor details')
                driver_data['owner_details'] = vendor.id
                data['vendor'] = vendor

        try:
            vehicle = add_vehicle(
                vehicle_params=vehicle_data, is_update=is_update)
        except Exception as e:
            logging.info(
                "Following error occured while creating vehicle : %s" % str(e))
            raise db.IntegrityError(VEHICLE_EXIST)

        data['vehicle'] = vehicle
        data['driver_data'] = driver_data

        return data

    def __update_status(self, driver, status):

        if driver.status != status:
            # If driver is set as active
            # set active date to current day
            if status == StatusPipeline.ACTIVE:
                driver.active_from = timezone.now()
                # In case of reactive of driver set leaving date as none
                if driver.date_of_leaving:
                    driver.date_of_leaving = None
                    driver.reason_for_inactive = None
                    driver.description = None

            elif status == StatusPipeline.INACTIVE:
                if not inactive_reason:
                    raise ValidationError(messages.INACTIVE_REASON_MANDATORY)

                if not description:
                    raise ValidationError(
                        messages.INACTIVE_DESCRIPTION_MANDATORY)

                driver.date_of_leaving = timezone.now()

            driver.save()

        return driver

    def __auto_update_driver_status(self, driver):
        """
            Update the driver status if all the documents of the driver are Active
            and if driver is not in Active or Inactive state
        """
        if driver.status not in [StatusPipeline.ACTIVE, StatusPipeline.INACTIVE]:
            driver_documents = DriverDocument.objects.filter(driver=driver.pk)
            active_doc = 0
            # If all the driver documents are verified(active)
            # then update the driver status to 'Onboarding'
            if driver_documents:
                for document in driver_documents:
                    if document.status == StatusPipeline.ACTIVE:
                        active_doc += 1

                if len(driver_documents) == active_doc:
                    driver.status = StatusPipeline.ONBOARDING
                    driver.save()

        return driver

    def __post_creation_action(self, params, driver, vehicle):
        """
                Tasks which are needed to be performed once the driver is created/updated successfully
        """

        # Update the vehicle details(* if any changes are made) in the driver
        # vehicle map
        if not driver.driver_vehicle or (driver.driver_vehicle != vehicle.registration_certificate_number):
            DriverVehicleMap.objects.create(driver=driver, vehicle=vehicle)

        # Update the preferred location if needed
        if not params.get('address_model', []):
            logging.info("No Preferred location was provided for the driver")
            raise ValidationError("Preferred Location is mandatory")

        add_preferred_location(params['address_model'], driver)

        # Update the driver referral if needed
        if driver.referred_by and driver.status == StatusPipeline.ACTIVE:
            DriverReferrals.objects.filter(referred_driver=driver).update(
                activation_timestamp=datetime.datetime.now())

        response_dict = {'driver_id': driver.id,
                         'vehicle_id': vehicle.id,
                         'status': driver.status}

        return response_dict

    def _generate_driver_history(self, driver, driver_new_data, vendor, user):
        """
            Make entries in the driver history table for the modified data
        """

        driver_list = []

        for field in driver._meta.fields:
            field_name = field.__dict__.get('name')
            if field_name in DRIVER_FIELD_ALLOWED_FOR_UPDATE:
                old_value = str(getattr(driver, field_name))
                current_value = str(driver_new_data.get(field_name, None))
                if field_name == 'owner_details':
                    old_value = field.value_from_object(driver)
                    current_value = vendor

                if old_value != current_value:
                    if field_name == 'owner_details':
                        old_value = "%s | %s" % (
                            driver.owner_details.name,
                            driver.owner_details.phone_number) if driver.owner_details else None
                        current_value = "%s | %s" % (
                            vendor.name, vendor.phone_number) if vendor else None

                    driver_list.append(DriverHistory(field=field_name, old_value=old_value,
                                                     new_value=current_value, user=user, driver=driver))

        return driver_list

    def _update_driver(self, params, driver, request):
        """
            For updating the already existing driver
        """

        detailed_data = self.__create_data_for_driver(
            params=params, driver=driver, is_update=True)
        driver_data = detailed_data.get('driver_data', {})
        vendor = detailed_data.get('vendor', {})
        status = driver_data.pop('status')

        driver = self.__update_status(driver, status)

        driver_history_list = self._generate_driver_history(
            driver, driver_data, vendor, request.user)

        logging.info(
            "This is the driver data for the serialization : %s " % driver_data)

        updated_driver = UpdateModelMixin().update(data=driver_data, instance=driver,
                                                   serializer_class=DriverSerializer)

        vehicle = detailed_data.get('vehicle')

        updated_driver = self.__auto_update_driver_status(updated_driver)
        response = self.__post_creation_action(params, updated_driver, vehicle)

        Driver.objects.filter(id=driver.id).update(modified_by=request.user)

        if driver_history_list:
            DriverHistory.objects.bulk_create(driver_history_list)

        logging.info('Driver Updated')
        return response

    def _create_driver(self, params, request):
        """
                For creating a new driver
        """

        try:
            phone = phonenumbers.parse(params.get('mobile', ''),
                                       setting.DEFAULT_COUNTRY.get('country_code'))
        except:
            phone = ''

        filter_data = {
            'name': params.get('name', ''),
            'driver_vehicle': params.get('driver_vehicle', ''),
            'user__phone_number': phone
        }

        driver = Driver.objects.filter(**filter_data).first()

        if not driver or driver.status == StatusPipeline.REFERRED:

            detailed_data = self.__create_data_for_driver(params)
            logging.info(
                "This is the detailed data for adding the driver : %s " % detailed_data)
            driver_data = detailed_data.get('driver_data', {})

            if request.META.get('HTTP_DEVICE_TYPE', '') in setting.USER_CLIENT_TYPES:
                driver_data['source'] = USER_ACCESS_APP

            if driver and driver.referred_by:
                try:
                    referral_configuration = DriverReferralConfiguration.objects.filter(
                        city=city).last()
                    DriverReferrals.objects.create(
                        referred_driver=driver, referred_by=driver.referred_by,
                        referral_bonus=referral_configuration)
                except Exception as e:
                    logging.info(
                        "Following error occurred while adding driver referral : %s" % e.args[0])
                    raise ValidationError(e.args[0])

            created_driver = CreateModelMixin().create(data=driver_data, instance=driver,
                                                       serializer_class=DriverSerializer)
            created_driver.created_by = request.user
            created_driver.modified_by = request.user
            created_driver.save()

            vehicle = detailed_data.get('vehicle')

            response = self.__post_creation_action(
                params, created_driver, vehicle)
            logging.info('Driver Created')

            return response
        else:
            logging.info(
                "Driver activation details needed")
            raise ValidationError(
                "Driver already exists. Needs to be activated")
