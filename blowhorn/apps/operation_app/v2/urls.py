from django.conf.urls import url
from blowhorn.apps.operation_app.v2.views import user as user_views
from blowhorn.apps.operation_app.v2.views import driver as driver_views
from blowhorn.apps.operation_app.v2.views import vehicle as vehicle_views


urlpatterns = [
    url(r'^login$', user_views.Login.as_view(),
        name='operation-user-login'),
    url(r'^drivers/$', driver_views.DriversList.as_view(),
        name='driver-list'),
    url(r'^drivers/(?P<pk>[0-9]+)/$', driver_views.DriverDetails.as_view(),
        name='driver-details'),
    url(r'^drivers/documents/upload$', driver_views.DriverDocumentUpload.as_view(),
        name='driver-document-upload'),
    url(r'^vehicles/vehiclemodels$',
        vehicle_views.VehicleModelList.as_view(), name='vehicle-model-list'),
    url(r'^vehicles/documents/upload$', vehicle_views.VehicleDocumentUpload.as_view(),
        name='vehicle-document-upload'),

]
