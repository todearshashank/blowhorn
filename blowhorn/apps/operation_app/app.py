from django.apps import AppConfig
from django.conf.urls import include, url

from blowhorn.apps.operation_app.v1.app import application as v1_operation_app
from blowhorn.apps.operation_app.v2.app import application as v2_operation_app
from blowhorn.apps.operation_app.v3.app import application as v3_operation_app
from blowhorn.apps.operation_app.v4.app import application as v4_operation_app
from blowhorn.apps.operation_app.v5.app import application as v5_operation_app
from blowhorn.apps.operation_app.v6.app import application as v6_operation_app


class OperationVersionedApplication(AppConfig):
    def get_urls(self):
        urlpatterns = [
            url(r'^v1/', v1_operation_app.urls),
            url(r'^v2/', v2_operation_app.urls),
            url(r'^v3/', v3_operation_app.urls),
            url(r'^v4/', v4_operation_app.urls),
            url(r'^v5/', v5_operation_app.urls),
            url(r'^v6/', v6_operation_app.urls),
        ]
        return self.post_process_urls(urlpatterns)
