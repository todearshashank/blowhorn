from django.conf.urls import include, url

urlpatterns = [
    url(r'^v1/', include('blowhorn.apps.operation_app.v1.urls')),
    url(r'^v2/', include('blowhorn.apps.operation_app.v2.urls')),
    url(r'^v3/', include('blowhorn.apps.operation_app.v3.urls')),
    url(r'^v4/', include('blowhorn.apps.operation_app.v4.urls')),
    url(r'^v5/', include('blowhorn.apps.operation_app.v5.urls')),
    url(r'^v6/', include('blowhorn.apps.operation_app.v6.urls')),
]
