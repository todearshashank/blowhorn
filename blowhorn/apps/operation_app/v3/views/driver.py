import json
import re
import logging
import datetime

from django import db
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.parsers import MultiPartParser, FormParser
from blowhorn.oscar.core.loading import get_model

from config.settings import status_pipelines as StatusPipeline
from blowhorn.common import sms_templates
from blowhorn.common.helper import CommonHelper
from blowhorn.apps.operation_app.v3.serializers import driver as driver_serializer
from blowhorn.document.models import DocumentType
from blowhorn.utils.functions import ist_to_utc
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.apps.operation_app.v3.constants import *
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.apps.operation_app.v3.services.driver import (
    DriverAddUpdateMixin,
    DriverListMixin,
    DriverBackGroundVerificationMixin,
)
from blowhorn.address import utils as address_utils
from blowhorn.apps.operation_app.v3.helper import validate_operation_data
from blowhorn.apps.operation_app.v3.services.tasks import *
from blowhorn.apps.operation_app.v3.services.driver import *
from blowhorn.document.constants import DRIVER_DOCUMENT, DOC_VALIDATION_REGEX_MAPPING
from blowhorn.document.models import DocumentEvent
from blowhorn.driver.constants import READY_FOR_VERIFICATION
from blowhorn.vehicle.models import VendorDocument
from blowhorn.vehicle.serializers import VendorDocumentSerializer
from blowhorn.driver.serializers import DriverDocumentPageSerializer
from blowhorn.driver.util import get_driver_missing_doc


Driver = get_model("driver", "Driver")
DriverVehicleMap = get_model("driver", "DriverVehicleMap")
DriverDocument = get_model("driver", "DriverDocument")
DriverPreferredLocation = get_model("driver", "DriverPreferredLocation")
ResourceAllocation = get_model("contract", "ResourceAllocation")
State = get_model("address", "State")

VEHICLE_EXIST = "Vehicle with corresponding registration number already exists"

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverDetails(views.APIView):
    """
        Retrieve or update the driver instance
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request, pk, format=None):
        """
            Returns the driver record data corresponding to the driver id
        """
        data = {"result": [], "next": 0, "previous": 0, "count": 0}

        driver_qs = Driver.objects.filter(id=pk)

        driver_qs = driver_qs.select_related(
            "owner_details",
            "operating_city",
            "current_address",
            "permanent_address",
            "user",
        )

        driver_qs = driver_qs.values(*DRIVER_LIST_FIELDS)

        if not driver_qs:
            logging.info("Invalid driver ID")
            return Response(
                data="Driver does not exist",
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )

        data["result"] = DriverListMixin().get_driver_detailed_list(driver_qs)
        return Response(status=status.HTTP_200_OK, data=data)

    def put(self, request, pk, format=None):
        """
            For updating the record of the driver corresponding to the id
        """
        driver = Driver.objects.filter(id=pk).first()
        old_status = driver.status

        if not driver:
            logging.info("Invalid driver ID")
            return Response(
                data="Driver does not exist",
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )
        logging.info("Driver for which the updation is needed : %s" %
                     str(driver))

        try:
            data = json.loads(request.body)
            driver_details = data.get("driver_details", {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get("driver_details", "{}"))
            logging.info("Request from the app dict : " +
                         str(request.data.dict()))

        response_dict = {"status": False, "message": ""}

        # if driver.owner_details and driver_details.get('driver_is_owner'):
        #     message = _("Cannot mark driver is owner true as the driver is mapped to a vendor")
        #     response_dict["message"] = message
        #     return Response(status=status.HTTP_400_BAD_REQUEST, data=response_dict)

        if driver_details.get("status") not in StatusPipeline.DRIVER_BANNED_STATUSES:
            validated_data = validate_operation_data(
                params=driver_details, is_update=True
            )

            if not validated_data.get("success"):
                response_dict["message"] = validated_data.get("error")
                return Response(
                    data=response_dict,
                    status=status.HTTP_400_BAD_REQUEST,
                    content_type="text/html; charset=utf-8",
                )
        else:
            try:
                changed_status = DriverAddUpdateMixin()._deactivate_driver(
                    driver_details, driver, request)

            except Exception as e:
                response_dict["message"] = e.args[0]
                return Response(
                    data=response_dict,
                    status=status.HTTP_400_BAD_REQUEST,
                    content_type="text/html; charset=utf-8",
                )

        if driver_details.get("status") == StatusPipeline.ACTIVE:
            missing_docs = get_driver_missing_doc(driver,
                                                  status=StatusPipeline.ACTIVE)
            if missing_docs:
                message = "Can't activate driver, docs missing - "
                message += ', '.join(missing_docs)
                response_dict["message"] = message
                return Response(status=status.HTTP_400_BAD_REQUEST, data=response_dict)

        with db.transaction.atomic():
            try:
                updated_driver = DriverAddUpdateMixin()._update_driver(
                    driver_details, driver, request
                )
                new_status = updated_driver['status']

                if (new_status != old_status) and new_status == StatusPipeline.ACTIVE:
                    message = sms_templates.template_driver_active
                    driver.user.send_sms(message['text'], message)

            except Exception as e:
                response_dict["message"] = e.args[0]
                return Response(
                    data=response_dict,
                    status=status.HTTP_400_BAD_REQUEST,
                    content_type="text/html; charset=utf-8",
                )

            return Response(status=status.HTTP_200_OK, data=updated_driver)


class DriversList(views.APIView):
    """
        List all the filtered driver records or create a new record
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def __get_pagination_data(self, queryset, page, num):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else objects.previous_page_number()
        )
        next = 0 if not objects.has_next() else objects.next_page_number()

        # pages_count = paginator.num_pages
        data = {
            "count": count,
            "previous": previous,
            "next": next,
            "objects": objects.object_list,
            "last_loaded_on": timezone.now().isoformat()
        }

        return data

    def get_queryset(self, query_params):
        filter_data = {}
        logging.info(
            "Following is the query params for the driver list :"
            " " + str(query_params)
        )

        page_to_be_displayed = (
            query_params.get("next")
            if query_params.get("next", "") and int(query_params["next"]) > 0
            else PAGE_TO_BE_DISPLAYED
        )
        logging.info(
            "Page to be displayed for the pagination "
            "purpose: % s" % page_to_be_displayed
        )

        number_of_entry = NUMBER_OF_ENTRY_IN_PAGE
        logging.info(
            "Number of entry per page for pagination " "purpose: %s" % number_of_entry
        )

        filter_data["operating_city__name"] = query_params.get(
            "operating_city")

        if query_params.get("timestamp", "") and query_params.get("timestamp") != 'null':
            try:
                timestamp = query_params.get('timestamp')
                # last_modified = ist_to_utc(timestamp)
            except Exception as e:
                logging.info("This is the error for the timestamp : %s " % e)
                return Response(
                    status=status.HTTP_404_NOT_FOUND,
                    data="Provide timestamp in correct format",
                )
            filter_data["modified_date__gte"] = timestamp

        driver_qs = Driver.objects.filter(
            **filter_data).order_by("status", "id")

        pagination_data = self.__get_pagination_data(
            driver_qs, page_to_be_displayed, number_of_entry
        )

        driver_qs = pagination_data.pop("objects")

        driver_qs = driver_qs.select_related("owner_details", "user")

        driver_qs = driver_qs.values(*DRIVER_LIST_FIELDS)

        return {"driver_list": driver_qs, "pagination_data": pagination_data}

    def get(self, request):
        """
            For fetching all the drivers for the given city
        """

        logging.info(
            "Following is the request data for fetching driver list : %s"
            % str(request.GET)
        )

        query_params = request.GET.dict()

        queryset_data = self.get_queryset(query_params)
        driver_list = queryset_data.get("driver_list")

        data = queryset_data.get("pagination_data")

        if driver_list:
            data["result"] = DriverListMixin().get_driver_list(driver_list)
            return Response(status=status.HTTP_200_OK, data=data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND, data="No driver found")

    def post(self, request):
        """
            For creating a new driver
        """

        try:
            data = json.loads(request.body)
            driver_details = data.get("driver_details", {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get("driver_details", "{}"))
            logging.info("Request from the app dict : " +
                         str(request.data.dict()))

        response_dict = {"status": False, "message": ""}

        validated_data = validate_operation_data(
            params=driver_details, is_update=False)

        if not validated_data.get("success"):
            response_dict["message"] = validated_data.get("error")
            return Response(
                data=response_dict,
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )

        try:
            created_driver = DriverAddUpdateMixin()._create_driver(
                driver_details, request
            )
        except Exception as e:
            logging.info("This is the exception : %s " % e)
            response_dict["message"] = e.args[0]
            return Response(
                data=response_dict,
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )

        return Response(status=status.HTTP_200_OK, data=created_driver)


class DriverDocumentUpload(views.APIView):
    """
        To upload the driver document
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in driver document add : " +
            str(request.data.dict())
        )

        documents = request.data.dict()
        driver_id = ""
        doc_status = {}
        doc_number = {}
        doc_state = {}
        expiry_date = {}
        document_page = {}
        doc_ids = {}

        if not documents:
            logging.info(
                "No documents where provided for the verification of driver")
            return Response(
                data="Please upload the documents", status=status.HTTP_400_BAD_REQUEST
            )

        documents_uploaded = json.loads(documents.pop('uploaded_docs')) if documents.get(
            'uploaded_docs', None) else None

        if documents.get("document_status", []):
            document_status = json.loads(documents.pop("document_status"))

            for statuses in document_status:
                driver_id = driver_id if driver_id else statuses.get(
                    "driver_id", "")

                if statuses.get("document_type", ""):
                    doc_status[statuses["document_type"]] = statuses.get(
                        "document_status", ""
                    )

                if not statuses.get("document_number"):
                    logging.info(
                        "No document number was provided for %s"
                        % statuses["document_type"]
                    )
                    return Response(
                        data="Please Enter the document number for "
                        + str(statuses["document_type"]),
                        status=status.HTTP_400_BAD_REQUEST,
                    )

                if statuses.get("document_pages", None):
                    document_page[statuses["document_type"]] = statuses.get("document_pages", None)

                if statuses.get("id", None):
                    doc_ids[statuses["document_type"]] = statuses.get("id", None)

                doc_number[statuses["document_type"]
                           ] = statuses["document_number"]

                doc_type = DocumentType.objects.get(code=statuses["document_type"])

                if not re.match(doc_type.regex, statuses["document_number"]):
                    return Response(
                        data=doc_type.error_message,
                        status=status.HTTP_400_BAD_REQUEST,
                    )

                if not statuses.get("document_state"):
                    logging.info(
                        "No state was provided for %s" % statuses["document_type"]
                    )
                    return Response(
                        data="Please Enter the issuing state for "
                        + str(statuses["document_type"]),
                        status=status.HTTP_400_BAD_REQUEST,
                    )

                doc_state[statuses["document_type"]] = State.objects.filter(
                    name=statuses["document_state"]
                ).first()

                expiry_date[statuses["document_type"]
                            ] = statuses["expiry_date"]

        if not driver_id:
            logging.info("Documents not added for the activation of driver")
            return Response(
                data="Documents for activation of Driver is needed",
                status=status.HTTP_400_BAD_REQUEST,
            )

        bgv_verified_docs = list(
            DriverDocument.objects.filter(driver_id=driver_id, status=StatusPipeline.ACTIVE,
                                          bgv_status=StatusPipeline.VERIFIED).values_list('document_type__code',
                                                                                          flat=True))

        for key, value in doc_number.items():
            if key in bgv_verified_docs:
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_('BGV verified docs cannot be edited'))

            doc_exists = DriverDocument.objects.filter(
                document_type__code=key, number=value,
                status=StatusPipeline.ACTIVE).exclude(
                driver_id=driver_id
            ).first()

            if doc_exists:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_('Document number %s already exists with %s') % (value, doc_exists.driver))

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                if documents_uploaded:
                    continue
                logging.info("Invalid document type is provided")
                return Response(
                    data="Provide the valid document type",
                    status=status.HTTP_400_BAD_REQUEST)
            file = documents[key]
            if doc_ids.get(key, None):
                file = DriverDocument.objects.get(id=doc_ids[key]).file
            document_data = {
                "file": file,
                "document_type": doc_type.id,
                "driver": driver_id,
                "status": doc_status[key]
                if doc_status.get(key, "")
                else VERIFICATION_PENDING,
                "number": doc_number[key],
                "state": doc_state[key].pk,
                "expiry_date": expiry_date[key],
            }

            if request.user:
                document_data["created_by"] = request.user
                document_data["modified_by"] = request.user

            document_serializer = driver_serializer.DriverDocumentSerializer(
                data=document_data
            )
            if document_serializer.is_valid():
                doc = document_serializer.save(
                    created_by=document_data.get("created_by"),
                    modified_by=document_data.get("modified_by"),
                )
            else:
                logging.info(document_serializer.errors)
                return Response(
                    document_serializer.errors, status=status.HTTP_400_BAD_REQUEST
                )

            if document_page.get(key, None):
                document_page[key].append({
                    'file': doc.file,
                    'page_number': 1
                })
                for page in sorted(document_page[key], key=lambda x: x.get('page_number')):
                    page_key = page.pop('page_key', None)
                    if page_key:
                        page['file'] = documents[page_key]
                    page['driver_document'] = doc.id
                    document_page_serializer = DriverDocumentPageSerializer(data=page)
                    if document_page_serializer.is_valid():
                        doc_page = document_page_serializer.save(
                            created_by=document_data.get("created_by"),
                            modified_by=document_data.get("modified_by"),
                        )
                    else:
                        logging.info(document_page_serializer.errors)
                        return Response(document_page_serializer.errors,
                                        status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK, data="Driver Details Added")


class DriverDocumentList(views.APIView):
    """
        driver document list
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):

        resource = request.GET.get("resource", None)
        key = request.GET.get("key", None)

        driver_document_type = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT)
        doc_list = []

        for doc_type in driver_document_type:
            doc_data = {
                "code": doc_type.code,
                "name": doc_type.name,
                "identifier": doc_type.identifier,
                'is_expiry_required': doc_type.is_expire_needed,
            }
            if resource == "driver":
                doc_info = DriverDocument.objects.filter(
                    driver_id=key, document_type=doc_type, status__in=[ACTIVE, VERIFICATION_PENDING]
                ).order_by('-created_date')
                doc_serializer = DriverDocumentSerializer
            else:
                doc_info = VendorDocument.objects.filter(
                    vendor_id=key, document_type=doc_type
                )
                doc_serializer = VendorDocumentSerializer
            doc_data.update(
                {"document": doc_serializer(doc_info, many=True).data})
            doc_list.append(doc_data)

        return Response(
            status=status.HTTP_200_OK,
            data=doc_list,
            content_type="text/html; charset=utf-8",
        )


class DriverImage(views.APIView):
    """
        driver image document
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):

        resource = request.GET.get("resource", None)
        key = request.GET.get("key", None)

        doc_type = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT, code="Photo"
        ).first()

        rating = 0.0
        vehicle_icon = None
        partner = None

        if resource == "driver":
            partner = Driver.objects.get(pk=key)
            if partner.vehicles.all().exists():
                vehicle_icon = partner.vehicles.all()[
                                   0].vehicle_model.vehicle_class.image_detail.url or ''
            rating = partner.driver_rating.cumulative_rating if partner.driver_rating else 0.0
            doc = DriverDocument.objects.filter(
                driver_id=key, document_type=doc_type, status=StatusPipeline.ACTIVE
            ).last()
        else:
            partner = Vendor.objects.get(pk=key)
            doc = VendorDocument.objects.filter(
                vendor_id=key, document_type=doc_type, status=StatusPipeline.ACTIVE
            ).last()

        if doc:
            # Get photo uploaded in the driver/vendor docs if exists
            photo = doc.file.url
        else:
            # Get photo from the `Driver/Vendor` model if exists
            photo = partner.photo.url if partner.photo else ''

        profile_data = {
            'photo': photo,
            'vehicle_icon': str(vehicle_icon),
            'rating': float(rating)
        }

        return Response(
            status=status.HTTP_200_OK,
            data=profile_data,
            content_type="text/html; charset=utf-8",
        )


class BackGroundVerificationView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            data = json.loads(request.body)
            driver_details = data.get("driver_details", {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get("driver_details", "{}"))
            logging.info("Request from the app dict : " +
                         str(request.data.dict()))
        user = request.user
        response_dict = {"status": False, "message": ""}

        logging.info("This is the data for verification of driver : %s" % data)

        if not driver_details.get("id", None):
            response_dict["message"] = "No Driver was provided"
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_dict)

        driver_obj = Driver.objects.filter(id=driver_details["id"])

        if not driver_obj.first():
            response_dict["message"] = "No Driver Found"
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_dict)

        city = driver_obj.first().operating_city

        try:
            response = DriverBackGroundVerificationMixin().background_verification_processing(
                user=user,
                city=city,
                driver_obj=driver_obj,
                driver_details=driver_details,
                request=request,
            )

            return Response(status=status.HTTP_200_OK, data=response)
        except ValidationError as e:
            response_dict["message"] = e
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_dict)


class BackgroundVerificationEligibility(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk):
        driver_qs = Driver.objects.filter(id=pk)

        if not driver_qs:
            logging.info("Invalid driver ID")
            return Response(
                data="Driver does not exist",
                status=status.HTTP_400_BAD_REQUEST
            )

        driver = driver_qs.first()
        logging.info("Driver for which the updation is needed : %s" %
                     str(driver))

        if not driver.bgv_status:
            try:
                DriverBackGroundVerificationMixin(
                ).get_data_for_background_verification(driver)
                logging.info(
                    "Driver has complete data for verification")
                driver_qs.update(
                    modified_by=request.user,
                    modified_date=timezone.now(),
                    data_ready_for_bgv=True,
                    bgv_status=READY_FOR_VERIFICATION)
                return Response(status=status.HTTP_200_OK,
                                data="Driver is eligible for background verification")
            except Exception as e:
                logging.info(
                    "This is the issue with data validation : %s " % e)
                return Response(status=status.HTTP_200_OK, data=e.args[0])

        else:
            logging.info("Driver  bgv status is %s", driver.bgv_status)
            if driver.bgv_status == READY_FOR_VERIFICATION:
                message = "Driver is eligible for background verification"
            else:
                message = "Driver verification is in progress"

            return Response(
                data=message,
                status=status.HTTP_200_OK
            )
