from fcm_django.models import FCMDevice
from django.conf import settings
from blowhorn.common.notification import Notification


# Django and System libraries
import json
import logging
from django.utils.translation import ugettext_lazy as _
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.utils import timezone

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import BasicAuthentication

# blowhorn imports
from blowhorn.address import utils as address_utils
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.document.models import DocumentType
from blowhorn.users.models import UserLoginDevice, User
from blowhorn.users.views import google_login
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.document.serializers import DocumentTypeSerializer
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT, \
    DOCUMENT_STATUSES
from blowhorn.apps.operation_app.v3.helper import get_vehicle_models, save_device_data
from blowhorn.apps.operation_app.v3.constants import WORKING_PREFERENCES_MAP, \
    STATUS_FOR_NOT_SHOWING_NOTIFICATION, STATUS_CHANGE_MAPPING, \
    REASON_FOR_INACTIVATION, REASON_FOR_BLACKLISTING, PAGE_TO_BE_DISPLAYED, REGEX_MATCH_ANYTHING
from blowhorn.apps.operation_app.v3.helper import is_superuser_or_city_manager
from blowhorn.driver.models import DriverInactiveReason, BankAccount, DriverSourcingOption
from blowhorn.driver.serializers import BankAccountSerializer
from blowhorn.vehicle.models import VendorInactiveReason
from blowhorn.vehicle.constants import VEHICLE_NUMBER_REGEX
from blowhorn.apps.operation_app.v5.constants import VENDOR_MANDATORY_DOCUMENTS

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([])
class Login(views.APIView):
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def fetch_inactive_reason(self):
        return list(DriverInactiveReason.objects.filter(is_active=True).
                    values_list('reason', flat=True))

    def fetch_vendor_inactive_reason(self):
        return list(
            VendorInactiveReason.objects.filter(is_active=True).values_list('reason', flat=True))

    def get_driver_sourcing_options(self):
        return list(
            DriverSourcingOption.objects.filter(is_active=True, display_in_cosmos=True).values_list(
                'source', flat=True))

    def get_regex_values(self):
        country = settings.COUNTRY_CODE_A2
        pan_doc = DocumentType.objects.filter(code='PAN')
        pan_regex = pan_doc.first().regex if pan_doc.exists() else REGEX_MATCH_ANYTHING
        _dict = {
            'country_code': settings.ACTIVE_COUNTRY_CODE,
            'country': country,
            'vehicle_regex': VEHICLE_NUMBER_REGEX[country],
            'pan_regex': pan_regex
        }
        return _dict

    def post(self, request):
        """
        :param request: Have the google authentication token
        :return: If token is valid and the user is present with the
                staff permission then return email, name, vehicle_data
        """
        result = {}
        logging.info('User is authenticating Google token from supply app')

        body = request.data
        location_details = json.loads(body.get('location_details'))

        logging.info('App Version: %s' % body.get('app_version', ''))
        logged_in_user = google_login(request)

        if logged_in_user.get('status_code', '') != status.HTTP_200_OK:
            return Response(status=logged_in_user['status_code'],
                            data=logged_in_user.get('error_msg'))

        user = logged_in_user.get('user_object')

        device_data = {
            'imei_number': body.get('imei_number'),
            'device_id': body.get('device_id'),
            'uuid': body.get('uuid'),
        }
        device = save_device_data(user, device_data)

        vehicle_model_list = get_vehicle_models()

        operating_cities = address_utils.get_operating_cities(location_details)

        states = address_utils.get_state_list()

        driver_doc_types = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT)

        driver_document_type = driver_doc_types
        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)
        vendor_document_type = driver_document_type.filter(name__in=VENDOR_MANDATORY_DOCUMENTS)

        serialized_driver_doc_type_data = DocumentTypeSerializer(
            driver_document_type, many=True)
        serialized_vehicle_doc_type_data = DocumentTypeSerializer(
            vehicle_document_type, many=True)
        serialized_vendor_document_type = DocumentTypeSerializer(
            vendor_document_type, many=True
        )
        driver_document_type = serialized_driver_doc_type_data.data \
            if serialized_driver_doc_type_data.data else []
        vehicle_document_type = serialized_vehicle_doc_type_data.data \
            if serialized_vehicle_doc_type_data.data else []
        vendor_document_type = serialized_vendor_document_type.data \
            if serialized_vendor_document_type.data else []
        superuser_access = is_superuser_or_city_manager(
            logged_in_user.get('user_object'), location_details)

        result = {
            'driver_document_list': driver_document_type,
            'vehicle_document_list': vehicle_document_type,
            'vendor_document_list': vendor_document_type,
            'document_statuses': DOCUMENT_STATUSES,
            'auth_token': logged_in_user.get('token'),
            'name': logged_in_user.get('user_name'),
            'email': logged_in_user.get('user_email'),
            'vehicle_model': vehicle_model_list if vehicle_model_list else [],
            'working_preferences': WORKING_PREFERENCES_MAP,
            'operating_city_name': operating_cities
            if operating_cities else [],
            'is_superuser': superuser_access,
            'state': states,
            'status_for_not_showing_notification':
            STATUS_FOR_NOT_SHOWING_NOTIFICATION,
            'driver_status_list': STATUS_CHANGE_MAPPING,
            'reason_for_inactive': self.fetch_inactive_reason(),
            'reason_for_blacklisting': REASON_FOR_BLACKLISTING,
            'regex_values': self.get_regex_values(),
            'sourcing_options': self.get_driver_sourcing_options()
        }
        return Response(status=status.HTTP_200_OK, data=result,
                        content_type='text/html; charset=utf-8')


class Profile(views.APIView):

    def get(self, request):
        """
        :param request: Have the google authentication token
        :return: If token is valid and the user is present with the
                staff permission then return email, name, vehicle_data
        """
        result = {}
        logging.info('User is authenticating Google token from supply app')

        body = request.GET
        location_details = {
            'mLatitude': body.get('mLatitude'),
            'mLongitude': body.get('mLongitude')
        }

        device_data = {
            'imei_number': body.get('imei_number'),
            'device_id': body.get('device_id'),
            'uuid': body.get('uuid'),
        }
        device = save_device_data(request.user, device_data)

        vehicle_model_list = get_vehicle_models()

        operating_cities = address_utils.get_operating_cities(location_details)

        states = address_utils.get_state_list()

        driver_doc_types = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT)

        driver_document_type = driver_doc_types
        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)
        vendor_document_type = driver_document_type.filter(name__in=VENDOR_MANDATORY_DOCUMENTS)

        serialized_driver_doc_type_data = DocumentTypeSerializer(
            driver_document_type, many=True)
        serialized_vehicle_doc_type_data = DocumentTypeSerializer(
            vehicle_document_type, many=True)
        serialized_vendor_document_type = DocumentTypeSerializer(
            vendor_document_type, many=True
        )
        driver_document_type = serialized_driver_doc_type_data.data \
            if serialized_driver_doc_type_data.data else []
        vehicle_document_type = serialized_vehicle_doc_type_data.data \
            if serialized_vehicle_doc_type_data.data else []
        vendor_document_type = serialized_vendor_document_type.data \
            if serialized_vendor_document_type.data else []
        user = request.user

        superuser_access = is_superuser_or_city_manager(
            user, location_details)
        result = {
            'driver_document_list': driver_document_type,
            'vehicle_document_list': vehicle_document_type,
            'vendor_document_types': vendor_document_type,
            'document_statuses': DOCUMENT_STATUSES,
            'name': user.name,
            'email': user.email,
            'vehicle_model': vehicle_model_list if vehicle_model_list else [],
            'working_preferences': WORKING_PREFERENCES_MAP,
            'operating_city_name': operating_cities
            if operating_cities else [],
            'is_superuser': superuser_access,
            'state': states,
            'status_for_not_showing_notification':
                STATUS_FOR_NOT_SHOWING_NOTIFICATION,
            'driver_status_list': STATUS_CHANGE_MAPPING,
            'reason_for_inactive': Login().fetch_inactive_reason(),
            'reason_for_blacklisting': REASON_FOR_BLACKLISTING,
            'regex_values': Login().get_regex_values(),
            'sourcing_options': Login().get_driver_sourcing_options()
        }
        logging.info("This is the response being sent : %s " % result)

        return Response(status=status.HTTP_200_OK, data=result,
                        content_type='text/html; charset=utf-8')


class UserFCM(views.APIView):

    def post(self, request):
        data = request.data.dict()
        user = request.user
        # device_type = 'android'
        registration_id = data.get('registration-id')
        device_type = data.get('client', 'android')

        user_device = FCMDevice.objects.filter(user=user, active=True)
        if user_device.exists():
            if user_device.count() == 1:
                Notification.update_fcm_key_for_user(user_device.first(),
                                                     registration_id, '',
                                                     device_type)
        else:
            Notification.add_fcm_key_for_user(request.user, registration_id,
                                              '', device_type.lower())

        response = {
            'status': 'PASS',
            'message': 'Registration id successfully saved'
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


class BankAccountList(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )
    serializer_class = BankAccountSerializer

    def __get_pagination_data(self, queryset, page, num):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else objects.previous_page_number()
        )
        next = 0 if not objects.has_next() else objects.next_page_number()

        # pages_count = paginator.num_pages
        data = {
            "count": count,
            "previous": previous,
            "next": next,
            "objects": objects.object_list,
        }

        return data

    def get(self, request):
        try:
            accounts = BankAccount.objects.all().order_by('created_date')
            serializer = self.serializer_class(accounts, many=True)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Failed to fetch bank accounts')
        page_to_be_displayed = (
            request.GET.get("next")
            if request.GET.get("next", "") and int(request.GET["next"]) > 0
            else PAGE_TO_BE_DISPLAYED
        )
        number_of_entry = 1500

        pagination_data = self.__get_pagination_data(
            serializer.data, page_to_be_displayed, number_of_entry
        )
        data = {
            'account': pagination_data.pop('objects'),
            'pagination_data': pagination_data
        }
        return Response(status=status.HTTP_200_OK, data=data)
