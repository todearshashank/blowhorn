
from django import db
from django.db.models import Q, Prefetch

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated

from blowhorn.common.helper import CommonHelper
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.document.constants import VERIFICATION_PENDING, ACTIVE, REJECT
from blowhorn.document.models import DocumentType, DocumentArchive, DocumentEvent, mark_other_doc_inactive
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT
from config.settings import status_pipelines as StatusPipeline
from blowhorn.document.models import save_archive
from blowhorn.driver.models import DriverDocumentPage
from blowhorn.vehicle.models import VehicleDocumentPage, VendorDocumentPage


class DocumentVerification(views.APIView):
    """
        list document for verification
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )

    def get(self, request):
        if request.user.is_superuser:
            query = Q(status=VERIFICATION_PENDING)
        else:
            query = Q(action_owner=request.user, status=VERIFICATION_PENDING)

        querySet = DocumentEvent.copy_data.filter(query)
        querySet = querySet.prefetch_related('driver_document', 'vehicle_document',
                                             'vendor_document', 'driver_document__driver')
        querySet = querySet.prefetch_related('driver_document__document_type', 'vehicle_document__document_type')
        querySet = querySet.prefetch_related('vendor_document__document_type', 'vehicle_document__vehicle')
        querySet = querySet.prefetch_related('vehicle_document__vehicle__driver_set', 'vendor_document__vendor')
        querySet = querySet.select_related('action_owner')

        documents = []

        for document in querySet:
            doc = {'id': document.id}
            if document.driver_document:
                doc['driverDocument'] = document.driver_document.file.url if document.driver_document else ''
                doc['documentType'] = str(document.driver_document.document_type)
                doc['driver'] = '%s' % document.driver_document.number
                doc['operatingCity'] = document.driver_document.driver.name
                # First page image will be same as the document image
                document_pages = DriverDocumentPage.objects.filter(
                    driver_document=document.driver_document, page_number__gt=1)
                doc['document_pages'] = [page.file.url for page in
                                         document_pages] if document_pages else None

            if document.vehicle_document:
                doc['vehicleDocument'] = document.vehicle_document.file.url if document.vehicle_document else ''
                doc['documentType'] = document.vehicle_document.document_type.name
                doc['vehicle'] = '%s' % document.vehicle_document.number
                doc['operatingCity'] = document.vehicle_document.vehicle.registration_certificate_number
                document_pages = VehicleDocumentPage.objects.filter(
                    vehicle_document=document.vehicle_document, page_number__gt=1)
                doc['document_pages'] = [page.file.url for page in
                                         document_pages] if document_pages else None
            if document.vendor_document:
                doc['vendorDocument'] = document.vendor_document.file.url if document.vendor_document else ''
                doc['documentType'] = document.vendor_document.document_type.name
                doc['vendor'] = '%s' % document.vendor_document.number
                doc['operatingCity'] = document.vendor_document.vendor.name
                document_pages = VendorDocumentPage.objects.filter(
                    vendor_document=document.vendor_document, page_number__gt=1)
                doc['document_pages'] = [page.file.url for page in
                                         document_pages] if document_pages else None
            doc['uploadedBy'] = str(document.created_by) if document.created_by else ''
            documents.append(doc)

        document_map = {
            'documents': documents,
        }
        return Response(status=status.HTTP_200_OK, data=document_map,
                        content_type='application/json; charset=utf-8')


class DocumentPendingVerification(views.APIView):
    """
        list document pending verification uploaded by a user
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )

    def get(self, request):
        query = Q(status=VERIFICATION_PENDING, created_by=request.user)

        querySet = DocumentEvent.copy_data.filter(query)
        querySet = querySet.prefetch_related('driver_document', 'vehicle_document',
                                             'vendor_document', 'driver_document__driver')
        querySet = querySet.prefetch_related('driver_document__document_type', 'vehicle_document__document_type')
        querySet = querySet.prefetch_related('vendor_document__document_type', 'vehicle_document__vehicle')
        querySet = querySet.prefetch_related('vehicle_document__vehicle__driver_set', 'vendor_document__vendor')
        querySet = querySet.select_related('action_owner')

        documents = []

        for document in querySet:
            doc = {'id': document.id}
            if document.driver_document:
                doc['driverDocument'] = document.driver_document.file.url if document.driver_document else ''
                doc['documentType'] = str(document.driver_document.document_type)
                doc['driver'] = str(document.driver_document.driver)
                doc['operatingCity'] = document.driver_document.driver.operating_city.name
                # First page image will be same as the document image
                document_pages = DriverDocumentPage.objects.filter(
                    driver_document=document.driver_document, page_number__gt=1)
                doc['document_pages'] = [page.file.url for page in
                                         document_pages] if document_pages else None
            if document.vehicle_document:
                doc['vehicleDocument'] = document.vehicle_document.file.url if document.vehicle_document else ''
                doc['documentType'] = document.vehicle_document.document_type.name
                doc['vehicle'] = document.vehicle_document.vehicle.registration_certificate_number
                document_pages = VehicleDocumentPage.objects.filter(
                    vehicle_document=document.vehicle_document, page_number__gt=1)
                doc['document_pages'] = [page.file.url for page in
                                         document_pages] if document_pages else None
            if document.vendor_document:
                doc['vendorDocument'] = document.vendor_document.file.url if document.vendor_document else ''
                doc['documentType'] = document.vendor_document.document_type.name
                doc['vendor'] = document.vendor_document.vendor.name
                document_pages = VendorDocumentPage.objects.filter(
                    vendor_document=document.vendor_document, page_number__gt=1)
                doc['document_pages'] = [page.file.url for page in
                                         document_pages] if document_pages else None
            doc['uploadedBy'] = str(document.created_by)
            doc['actionOwner'] = str(document.action_owner)
            doc['actionOwnerNumber'] = document.action_owner.national_number
            documents.append(doc)

        document_map = {
            'documents': documents,
        }
        return Response(status=status.HTTP_200_OK, data=document_map,
                        content_type='application/json; charset=utf-8')


class ActivateDocument(views.APIView):
    """
        activate document
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )

    def put(self, request, pk=None):
        query = Q(pk=pk)
        qs = DocumentEvent.copy_data.filter(query)
        qs = qs.prefetch_related('driver_document', 'vehicle_document',
                                 'vendor_document')
        qs = qs.select_related('action_owner')
        doc = qs.first()

        if doc and doc.status == VERIFICATION_PENDING:
            from blowhorn.document.models import is_doc_number_exist
            record = is_doc_number_exist(doc)
            if record and record.exists():
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        'status': False,
                        'message': 'Document Number exists check %s' % record},
                    content_type='application/json; charset=utf-8')
            archive = DocumentArchive(new_status=ACTIVE,
                                      action_owner_id=doc.action_owner_id,
                                      old_status=VERIFICATION_PENDING,
                                      document_event=doc)
            save_archive(doc, archive, ACTIVE)
            doc.status = ACTIVE
            doc.save()
            mark_other_doc_inactive(doc)
            return Response(status=status.HTTP_200_OK,
                            data={
                                'status': True,
                                'message': 'Document activated.'
                            },
                            content_type='application/json; charset=utf-8')
        else:
            return Response(status=status.HTTP_200_OK,
                            data={
                                'status': False,
                                'message': 'Document status changed.'
                                           'Please reload.'
                            },
                            content_type='application/json; charset=utf-8')


class RejectDocument(views.APIView):
    """
        reject document
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff, )

    def put(self, request, pk=None):
        if not request.data.get('reason'):
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'status': False,
                    'message': 'Rejection reason is mandatory.'
                },
                content_type='application/json; charset=utf-8')

        query = Q(pk=pk)
        qs = DocumentEvent.copy_data.filter(query)
        qs = qs.prefetch_related('driver_document', 'vehicle_document',
                                 'vendor_document')
        qs = qs.select_related('action_owner')
        doc = qs.first()
        driver = None
        if doc and doc.status == VERIFICATION_PENDING:
            archive = DocumentArchive(new_status=REJECT,
                                      action_owner_id=doc.action_owner_id,
                                      old_status=VERIFICATION_PENDING,
                                      document_event=doc)

            if doc.driver_document:
                driver = doc.driver_document.driver
                archive.driver_document_id = doc.driver_document_id
                doc.driver_document.status = REJECT
                doc.driver_document.save()

            elif doc.vehicle_document:
                archive.vehicle_document_id = doc.vehicle_document_id
                doc.vehicle_document.status = REJECT
                driver = doc.vehicle_document.vehicle.driver_set.first()
                doc.vehicle_document.save()

            elif doc.vendor_document_id:
                driver = None
                archive.vendor_document_id = doc.vendor_document_id
                doc.vendor_document.status = REJECT
                doc.vendor_document.save()

            if not doc.vendor_document_id and driver:
                from blowhorn.driver.util import get_driver_missing_doc
                mandatory_driver_doc_type = DocumentType.objects.filter(
                    is_document_mandatory=True,
                    identifier=DRIVER_DOCUMENT)
                mandatory_vehicle_doc_type = DocumentType.objects.filter(
                    is_document_mandatory=True,
                    identifier=VEHICLE_DOCUMENT)

                missing_doc = get_driver_missing_doc(
                    driver, mandatory_driver_doc_type,
                    mandatory_vehicle_doc_type
                )
                if missing_doc or driver.status == StatusPipeline.SUPPLY:
                    driver.status = StatusPipeline.ONBOARDING
                    driver.save()

            archive.reason = request.data.get('reason')
            archive.save()
            doc.status = REJECT
            doc.save()

            return Response(
                status=status.HTTP_200_OK,
                data={
                    'status': True,
                    'message': 'Document rejected.'
                },
                content_type='application/json; charset=utf-8')
        else:
            return Response(status=status.HTTP_200_OK,
                            data={
                                'status': False,
                                'message': 'Document status changed.'
                                           'Please reload.'
                            },
                            content_type='application/json; charset=utf-8')
