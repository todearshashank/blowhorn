# System and Django libraries
import json
import pytz
import logging
from datetime import datetime, timedelta
from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import Case, When, F, DateTimeField
from django.db import transaction

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# Blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_SPOT
from blowhorn.contract.utils import SubscriptionService
from blowhorn.trip.utils import TripService
from blowhorn.trip.mixins import TripMixin
from config.settings.status_pipelines import TRIP_END_STATUSES
from blowhorn.customer.models import CustomerInvoice
from blowhorn.vehicle.models import VehicleClass
from blowhorn.order.models import OrderHistory, OrderCommunication
from blowhorn.customer.constants import INVOICE_EDITABLE_STATUS
from blowhorn.apps.operation_app.v5.services.notifications import \
    SendNotification
from blowhorn.apps.operation_app.v5.constants import CREATED, ASSIGN
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.communication import Communication
from blowhorn.common.sms_templates import cosmos_assign_driver_sms

Contract = get_model('contract', 'Contract')
Driver = get_model('driver', 'Driver')
Hub = get_model('address', 'Hub')
OrderBatch = get_model('order', 'OrderBatch')
Order = get_model('order', 'Order')
Trip = get_model('trip', 'Trip')
OrderRequest = get_model('order', 'OrderRequest')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CreatSpotOrder(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)

    def create(self, request):
        try:
            orders_data = json.loads(request.body)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Data is tampered or invalid.')

        desc_batch = "Spot Order Creation(App) %s" % (timezone.now().date().strftime('%d-%m-%Y'))
        batch = OrderBatch.objects.create(description=desc_batch, number_of_entries=0)
        orders_created = 0
        response_data = []
        order_request_list = []

        for order_data in orders_data:
            error_message = ''
            contract_id = order_data.get('contract_id', None)
            driver_id = order_data.get('driver_id', None)
            hub_id = order_data.get('hub_id', None)
            pickup_time_str = order_data.get('pickup_datetime', None)
            contact_name = order_data.get('customer_name', None)
            contact_phone_number = order_data.get('customer_phone_number', None)
            order_communication = None

            try:
                contract = Contract.objects.get(
                    pk=contract_id,
                    status=CONTRACT_STATUS_ACTIVE,
                    contract_type=CONTRACT_TYPE_SPOT)
                driver = Driver.objects.get(pk=driver_id) if driver_id else None
                hub = Hub.objects.get(pk=hub_id)
                pickup_datetime = pytz.timezone(settings.IST_TIME_ZONE).localize(
                    datetime.strptime(pickup_time_str, '%d-%m-%Y %H:%M')
                ).astimezone(pytz.timezone(settings.TIME_ZONE))
                vehicle_class = VehicleClass.objects.get(commercial_classification=order_data.get('vehicle_class'))

                # is_valid, message = Order().is_pickup_time_in_valid_date_range(pickup_datetime)
                # if not is_valid:
                #     raise Exception(message)

                is_invoice_approved = CustomerInvoice.objects.filter(service_period_start__gte=pickup_datetime,
                                               service_period_end__lte=pickup_datetime, contract_invoices__contract_id=contract_id,
                                               ).exclude(status__in=INVOICE_EDITABLE_STATUS).exists()
                if is_invoice_approved:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data='Cannot create order when Invoice is Approved.')

                with transaction.atomic():
                    is_order_created, created_order = SubscriptionService().save_enterprise_order(
                        user=contract.customer.user,
                        pickup_datetime=pickup_datetime,
                        customer=contract.customer,
                        city=contract.city,
                        hub=hub,
                        driver=driver,
                        order_type=settings.SPOT_ORDER_TYPE,
                        created_by=request.user,
                        contract=contract,
                        invoice_driver=driver,
                        blowhorn_contract=driver.contract if driver else None,
                        device_type='App',
                        batch=batch,
                        vehicle_class=vehicle_class
                    )
                    orders_created += is_order_created
                    logger.info('Send notification for order %s' % created_order.number)
                    if contact_phone_number:
                        order_communication = OrderCommunication.objects.create(
                            order=created_order,
                            contact_name=contact_name,
                            contact_phone_number=contact_phone_number)
                SendNotification(CREATED, request.user,
                                 created_order, ).notify_users()
            except BaseException as e:
                is_order_created = False
                error_message = str(e)
                order_data['error_message'] = error_message
                response_data.append(order_data)

            order_request_list.append(
                OrderRequest(**{
                    'contract_id': contract_id,
                    'driver_id': driver_id,
                    'hub_id': hub_id,
                    'order_communication_id': order_communication.id if order_communication else None,
                    'pickup_datetime': pickup_time_str,
                    'user': self.request.user,
                    'is_created': True if is_order_created else False,
                    'error_message': error_message
                })
            )

        if orders_created:
            batch.end_time = timezone.now()
            batch.number_of_entries = orders_created
            batch.save()
        else:
            batch.delete()

        OrderRequest.objects.bulk_create(order_request_list) if order_request_list else None

        return Response(status=status.HTTP_200_OK, data=response_data)

    def put(self, request):
        """
        Assign/reassign driver for a order
        """
        try:
            data = json.loads(request.body)
            order = Order.objects.filter(pk=data.get('order_id'))
            order = order.select_related('vehicle_class_preference')
            order = order.prefetch_related('trip_set')
            order = order.prefetch_related('driver')
            order = order.prefetch_related('driver__vehicles')[0]

            trip_base_pay = data.get('trip_buy_rate', None)
            contract_base_pay = order.customer_contract.contractterm_set.all().latest(
                'wef').buy_rate.driver_base_payment_amount
            if order.status == StatusPipeline.ORDER_CANCELLED:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_("This order has been cancelled!"))

            new_driver = Driver.objects.get(pk=data.get('driver_id')) if data.get('driver_id',
                                                                                  None) else None
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Data is tampered or invalid.')
        # old values in order
        old_driver = order.driver
        # update new values in order
        if new_driver:
            if new_driver == old_driver:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_(
                                    'Cannot assign the same driver whose trip was marked contingency,'
                                    ' assign a new driver'))
            else:
                order.driver = new_driver

        vehicle_class_limit = order.driver.vehicles.first().vehicle_model.vehicle_class.per_day_limit if order.driver\
            else None
        if vehicle_class_limit and float(trip_base_pay) > float(vehicle_class_limit):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Cannot enter supply cost greater than the daily approval limit')

        order.marked_for_contingency = False
        order.modified_by = request.user

        order_communication = OrderCommunication.objects.filter(order=order).first()

        with transaction.atomic():
            try:
                if (old_driver and old_driver != new_driver) or (
                    not old_driver and new_driver and order.trip_set.filter(
                     driver__isnull=False).exists()):
                    trip_deduction = float(contract_base_pay) - float(trip_base_pay)
                    if trip_deduction > 0:
                        trip_deduction = 0.0
                    TripService().create_contingency_trip(old_driver, order,
                                                          trip_base_pay=trip_base_pay,
                                                          trip_deduction_amount=trip_deduction)
                    OrderHistory.objects.create(order_id=order.id,
                                                field='driver',
                                                new_value=new_driver,
                                                user=request.user,
                                                modified_time=timezone.now())
                elif new_driver != old_driver:
                    TripMixin().update_driver_for_trip(new_driver, order, new_driver.contract,
                                                       trip_base_pay=trip_base_pay)
                    OrderHistory.objects.create(order_id=order.id,
                                                field='driver',
                                                new_value=new_driver,
                                                user=request.user,
                                                modified_time=timezone.now())
                order.save()
                if order_communication and settings.ENABLE_SLACK_NOTIFICATIONS:
                    Communication.send_sms(
                        sms_to=order_communication.contact_phone_number.national_number,
                        message=cosmos_assign_driver_sms['text'],
                        priority='high',
                        template_dict=cosmos_assign_driver_sms)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='Driver assign/reassign failed.')
        SendNotification(ASSIGN, request.user, order).notify_users()
        return Response(status=status.HTTP_200_OK, data=[])
