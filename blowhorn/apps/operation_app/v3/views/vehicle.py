# Django and System libraries
import json
import re
import logging
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from django.db.models import Q
from django import db

# blowhorn imports
from config.settings import status_pipelines
from blowhorn.address.models import State
from blowhorn.apps.operation_app.v1.constants import *
from blowhorn.apps.operation_app.v3.helper import (
    get_vehicle_models, add_vehicle, add_vendor)
from blowhorn.apps.operation_app.v3.serializers import vehicle \
    as vehicle_serializer
from blowhorn.document.models import DocumentType
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.document.constants import VEHICLE_DOCUMENT, DOC_VALIDATION_REGEX_MAPPING
from blowhorn.driver.models import DriverVehicleMap, Driver
from blowhorn.vehicle.models import VehicleDocument, Vehicle
from blowhorn.vehicle.serializers import VehicleDocumentSerializer, VehicleDocumentPageSerializer
from blowhorn.users.permission import IsBlowhornStaff

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([])
class VehicleModelList(views.APIView):

    def get(self, request):
        """
            Fetch all the vehicle models along with their vehicle
            class and body type
        """
        try:
            vehicle_model_list = get_vehicle_models()
            logging.info('Vehicle model list fetched: ' +
                         str(vehicle_model_list))

            return Response(status=status.HTTP_200_OK, data=vehicle_model_list)
        except Exception as e:
            logging.info('Get vehicle model list output error: %s' % e)
            return Response(status=status.HTTP_400_BAD_REQUEST)


class VehicleDocumentUpload(views.APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in vehicle document add : " + str(
                request.data.dict()))

        documents = request.data.dict()
        vehicle_id = None
        doc_status = {}
        doc_number = {}
        doc_state = {}
        expiry_date = {}
        document_page = {}
        doc_ids = {}

        if not documents:
            logging.info(
                "No documents where provided for the verification of vehicle")
            return Response(data="Please upload the documents",
                            status=status.HTTP_400_BAD_REQUEST)

        documents_uploaded = json.loads(documents.pop('uploaded_docs')) if documents.get(
            'uploaded_docs', None) else None

        if documents.get('document_status', []):
            document_status = json.loads(documents.pop('document_status'))

            for statuses in document_status:
                vehicle_id = vehicle_id if vehicle_id else statuses.get(
                    'vehicle_id', '')

                if statuses.get('document_type', ''):
                    doc_status[statuses['document_type']] = statuses.get(
                        'document_status', '')

                if not statuses.get('document_number'):
                    logging.info("No document number was provided for %s" %
                                 statuses['document_type'])
                    return Response(
                        data="Please Enter the document number for " +
                        str(statuses['document_type']),
                        status=status.HTTP_400_BAD_REQUEST)

                if statuses.get("document_pages", None):
                    document_page[statuses["document_type"]] = statuses.get("document_pages", None)

                if statuses.get("id", None):
                    document_page[statuses["document_type"]] = statuses.get("id", None)

                doc_number[statuses['document_type']] = statuses[
                    'document_number']

                doc_type = DocumentType.objects.get(code=statuses["document_type"])

                if not re.match(doc_type.regex, statuses["document_number"]):
                    return Response(
                        data=doc_type.error_message,
                        status=status.HTTP_400_BAD_REQUEST,
                    )

                if not statuses.get('document_state'):
                    logging.info("No state was provided for %s" %
                                 statuses['document_type'])
                    return Response(
                        data="Please Enter the issuing state for " +
                        str(statuses['document_type']),
                        status=status.HTTP_400_BAD_REQUEST)

                doc_state[statuses['document_type']] = State.objects.filter(
                    name=statuses['document_state']).first()

                expiry_date[statuses['document_type']] = statuses[
                    'expiry_date']

        if not vehicle_id:
            logging.info(
                "Documents not added for the activation of vehicle")
            return Response(
                data="Documents for activation of Vehicle is needed",
                status=status.HTTP_400_BAD_REQUEST)

        bgv_verified_docs = list(
            VehicleDocument.objects.filter(vehicle_id=vehicle_id, status=VehicleDocument.ACTIVE,
                                           bgv_status=status_pipelines.VERIFIED).values_list('document_type__code',
                                                                                             flat=True))

        for key, value in doc_number.items():
            if key in bgv_verified_docs:
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_('BGV verified docs cannot be edited'))

            doc_exists = VehicleDocument.objects.filter(
                document_type__code=key, number=value,
                status=VehicleDocument.ACTIVE).exclude(
              vehicle_id=vehicle_id
            ).first()

            if doc_exists:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_('Document number %s already exists with vehicle %s') % (
                                    value, doc_exists.vehicle))

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                if documents_uploaded:
                    continue
                logging.info("Invalid document type is provided")
                return Response(
                    data="Provide the valid document type",
                    status=status.HTTP_400_BAD_REQUEST)
            file = documents[key]
            if doc_ids.get(key, None):
                file = VehicleDocument.objects.get(id=doc_ids[key]).file
            document_data = {
                'file': file,
                'document_type': doc_type.id,
                'vehicle': vehicle_id,
                'status': doc_status[key] if doc_status.get(
                    key, '') else VERIFICATION_PENDING,
                'number': doc_number[key],
                'state': doc_state[key].pk,
                'expiry_date': expiry_date[key]
            }

            if request.user:
                document_data['created_by'] = request.user
                document_data['modified_by'] = request.user

            document_serializer = vehicle_serializer.VehicleDocumentSerializer(
                data=document_data)
            if document_serializer.is_valid():
                doc = document_serializer.save(
                    created_by=document_data.get(
                        'created_by'),
                    modified_by=document_data.get('modified_by'))
            else:
                logging.info(document_serializer.errors)
                return Response(document_serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)

            if document_page.get(key, None):
                document_page[key].append({
                    'file': doc.file,
                    'page_number': 1
                })
                for page in sorted(document_page[key], key=lambda x: x.get('page_number')):
                    page_key = page.pop('page_key', None)
                    if page_key:
                        page['file'] = documents[page_key]
                    page['vehicle_document'] = doc.id
                    document_page_serializer = VehicleDocumentPageSerializer(data=page)
                    if document_page_serializer.is_valid():
                        doc_page = document_page_serializer.save(
                            created_by=document_data.get("created_by"),
                            modified_by=document_data.get("modified_by"),
                        )
                    else:
                        logging.info(document_page_serializer.errors)
                        return Response(document_page_serializer.errors,
                                        status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='Vehicle Details Added')


class VehicleDocumentList(views.APIView):
    """
        vehicle document list
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):

        resource = request.GET.get('resource', None)
        key = request.GET.get('key', None)

        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)
        veh_doc_list = []

        for doc_type in vehicle_document_type:
            doc_data = {
                'code': doc_type.code,
                'name': doc_type.name,
                'identifier': doc_type.identifier,
                'is_expiry_required': doc_type.is_expire_needed
            }

            query = Q(vehicle__driver__id=key) if resource == 'driver' else Q(
                vehicle__vendor__id=key)
            query = query & Q(document_type=doc_type)
            doc_info = VehicleDocument.objects.filter(query)
            doc_data.update({
                'document': VehicleDocumentSerializer(doc_info, many=True).data
            })
            veh_doc_list.append(doc_data)

        return Response(status=status.HTTP_200_OK, data=veh_doc_list,
                        content_type='text/html; charset=utf-8')


class AvailableVehicleList(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    serializer_class = vehicle_serializer.VehicleSerializer

    def get(self, request):
        data = request.data
        if data.get('fleetowner', False):
            vehicles = Vehicle.objects.filter(vendor__isnull=True)
        else:
            mapped_vehicles = DriverVehicleMap.objects.\
                values_list('vehicle_id', flat=True)
            vehicles = Vehicle.objects.exclude(id__in=mapped_vehicles).\
                select_related(
                    'body_type', 'vehicle_model', 'vehicle_model__vehicle_class')
        available_vehicles = self.serializer_class(vehicles, many=True)
        return Response(
            status=status.HTTP_200_OK,
            data=available_vehicles.data,
            content_type='text/html; charset=utf-8')


class RemoveVehicleMapping(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request, driver_id=None):
        vehicle_map = DriverVehicleMap.objects.filter(driver_id=driver_id)
        logger.info('Removing vehicle mapping for driver %s: %s'
                    % (driver_id, vehicle_map))
        try:
            vehicle_map.delete()

            driver_data = {
                'owner_details': None,
                'driver_vehicle': None,
                'modified_date': timezone.now()
            }
            updated_driver = Driver.objects.filter(pk=driver_id) \
                .update(**driver_data)
            logger.info('Vehicle removed from driver: %s', updated_driver)
            return Response(
                status=status.HTTP_200_OK,
                data=_('Vehicle has been unmapped from driver'))
        except BaseException as be:
            logger.info('Error: %s' % be)
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_('Failed to unmap vehicle from driver'))


class AddVehicleMap(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request, driver_id=None):
        data = request.data
        vehicle_number = data.get('vehicle_number')
        if Driver.objects.filter(driver_vehicle=vehicle_number).exists():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_('Vehicle is already mapped to a driver'))

        vehicles = Vehicle.objects.filter(
            registration_certificate_number=vehicle_number)
        with transaction.atomic():
            if vehicles.exists():
                vehicle = vehicles.first()
            else:
                vehicle_data = {
                    'registration_certificate_number': vehicle_number,
                    'vehicle_model': data.get('vehicle_model'),
                    'body_type': data.get('body_type'),
                    'model_year': data.get('vehicle_model_year')
                }
                try:
                    vehicle = add_vehicle(
                        vehicle_params=vehicle_data,
                        is_update=False
                    )
                except BaseException as be:
                    logger.info('Failed to create vehicle: %s' % be)
                    return Response(
                        status=status.HTTP_400_BAD_REQUEST,
                        data=_('Failed to map vehicle to driver'))

            try:
                vehicle_map = DriverVehicleMap.objects.create(**{
                    'driver_id': driver_id,
                    'vehicle': vehicle
                })

            except BaseException as be:
                logger.info('Error: %s' % be)
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=_('Failed to map vehicle to driver'))

            is_owner = data.get('is_owner')
            driver_data = {
                'driver_vehicle': vehicle_number,
                'modified_date': timezone.now(),
                'own_vehicle': is_owner
            }

            try:
                if not is_owner:
                    vendor_details = {
                        'name': data.get('owner_name'),
                        'phone_number': data.get('owner_phonenumber'),
                        'pan': data.get('owner_pan'),
                    }
                    vendor = add_vendor(vendor_details)
                    driver_data['owner_details'] = vendor

                updated_driver = Driver.objects.filter(pk=driver_id) \
                    .update(**driver_data)

                logger.info('Vehicle has been mapped to driver: %s', updated_driver)
                return Response(
                    status=status.HTTP_200_OK,
                    data=_('Vehicle has been mapped to driver'))

            except BaseException as be:
                logger.info('Error: %s' % be)
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=_('Failed to create vendor details'))
