from django.conf.urls import url
from blowhorn.apps.operation_app.v3.views import user as user_views
from blowhorn.apps.operation_app.v3.views import driver as driver_views
from blowhorn.apps.operation_app.v3.views import vehicle as vehicle_views
from blowhorn.apps.operation_app.v3.views import order as order_views
from blowhorn.apps.operation_app.v3.views import document as document_views

urlpatterns = [
    url(r"^login$", user_views.Login.as_view(), name="operation-user-login"),
    url(r"^fcmid/$", user_views.UserFCM.as_view(), name="operation-user-login"),

    url(
        r"^profile$",
        user_views.Profile.as_view(),
        name="operation-user-profile"
    ),
    url(
        r"^drivers/verification$",
        driver_views.BackGroundVerificationView.as_view(),
        name="background-verification",
    ),
    url(
        r"^drivers/verification/eligibility/(?P<pk>[0-9]+)/$",
        driver_views.BackgroundVerificationEligibility.as_view(),
        name="background-verification-eligibility",
    ),
    url(r"^drivers/$", driver_views.DriversList.as_view(), name="driver-list"),
    url(
        r"^drivers/(?P<pk>[0-9]+)/$",
        driver_views.DriverDetails.as_view(),
        name="driver-details",
    ),
    url(
        r"^drivers/documents/upload$",
        driver_views.DriverDocumentUpload.as_view(),
        name="driver-document-upload",
    ),
    url(
        r"^drivers/documents/$",
        driver_views.DriverDocumentList.as_view(),
        name="driver-document-list",
    ),
    url(
        r"^driver/photo/$",
        driver_views.DriverImage.as_view(),
        name="driver-document-list",
    ),
    url(
        r"^documents/verification$",
        document_views.DocumentVerification.as_view(),
        name="pending-document-list",
    ),
    url(
        r"^documents/pending-verification$",
        document_views.DocumentPendingVerification.as_view(),
        name="pending-document-list-uploaded-by-a-user",
    ),
    url(
        r"^documents/activate/(?P<pk>[0-9]+)/$",
        document_views.ActivateDocument.as_view(),
        name="document activation",
    ),
    url(
        r"^documents/reject/(?P<pk>[0-9]+)/$",
        document_views.RejectDocument.as_view(),
        name="document activation",
    ),
    url(
        r"^vehicles/vehiclemodels$",
        vehicle_views.VehicleModelList.as_view(),
        name="vehicle-model-list",
    ),
    url(
        r"^vehicles/available-vehicles$",
        vehicle_views.AvailableVehicleList.as_view(),
        name="available-vehicles",
    ),
    url(
        r"^vehicles/documents/upload$",
        vehicle_views.VehicleDocumentUpload.as_view(),
        name="vehicle-document-upload",
    ),
    url(
        r"^vehicles/documents/$",
        vehicle_views.VehicleDocumentList.as_view(),
        name="vehicle-document-list",
    ),
    url(
        r"^orders/spot$",
        order_views.CreatSpotOrder.as_view(),
        name="order-spot-create",
    ),
    url(
        r"^driver/vehicle/remove/(?P<driver_id>[0-9]+)/$",
        vehicle_views.RemoveVehicleMapping.as_view(),
        name="remove-driver-vehicle-map",
    ),
    url(
        r"^driver/vehicle/add/(?P<driver_id>[0-9]+)/$",
        vehicle_views.AddVehicleMap.as_view(),
        name="add-driver-vehicle-map",
    )
]
