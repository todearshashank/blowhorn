# Django and System libraries
import logging

from django.conf import settings

# 3rd party libraries
from rest_framework import serializers
from phonenumber_field.phonenumber import PhoneNumber

# blowhorn imports
from blowhorn.common.serializers import BaseSerializer
from blowhorn.vehicle.models import VehicleModel, Vehicle, Vendor, \
    VehicleDocument
from blowhorn.utils import functions as utils_func
from blowhorn.document.serializers import ExtendedDocumentSerializer


class VehicleModelSerializer(serializers.ModelSerializer):
    vehicle_class = serializers.CharField(
        source='vehicle_class.commercial_classification')
    body_type = serializers.SerializerMethodField(source='get_body_type')

    class Meta:
        model = VehicleModel
        fields = ('model_name', 'vehicle_class', 'body_type')

    def get_body_type(self, vehicle_model):
        return self.context.get('body_types', [])


class VehicleSerializer(BaseSerializer):
    body_type_name = serializers.CharField(source='body_type.body_type')
    vehicle_model = serializers.CharField(source='vehicle_model.model_name')
    vehicle_class_name = serializers.CharField(
        source='vehicle_model.vehicle_class.commercial_classification')
    vehicle_model_year = serializers.CharField(source='model_year')

    class Meta:
        model = Vehicle
        fields = (
            'id', 'vehicle_model', 'body_type_name',
            'registration_certificate_number', 'vehicle_class_name',
            'vehicle_model_year')


class VehicleClassSerializer(serializers.ModelSerializer):
    vehicle_class_name = serializers.CharField(
        source='vehicle_model.vehicle_class.commercial_classification')

    class Meta:
        model = Vehicle
        fields = ['vehicle_class_name']


class VendorSerializer(serializers.Serializer):

    def validate(self, attrs):
        data = self.initial_data

        if not utils_func.validate_phone_number(data.get('phone_number', '')):
            logging.info("Please Enter a valid phone number")
            raise serializers.ValidationError(
                "Please Enter a valid phone number")
        data['phone_number'] = PhoneNumber(
            settings.ACTIVE_COUNTRY_CODE, data.get('phone_number'))

        return data

    def create(self, validated_data):
        # validated_data.pop('phone')
        data = Vendor.objects.create(**validated_data)
        if not data:
            return False

        return data

    class Meta:
        model = Vendor
        fields = '__all__'


class VehicleDocumentSerializer(ExtendedDocumentSerializer):

    def create(self, validated_data):
        vehicle_doc = VehicleDocument.objects.create(**validated_data)
        vehicle_doc.created_by = validated_data.get('created_by')
        vehicle_doc.modified_by = validated_data.get('modified_by')
        vehicle_doc.save()

        if not vehicle_doc:
            return False

        return vehicle_doc

    class Meta:
        model = VehicleDocument
        fields = ExtendedDocumentSerializer.Meta.fields + ('vehicle',)


class VehicleAddSerializer(BaseSerializer):

    class Meta:
        model = Vehicle
        fields = '__all__'


class VehicleDocumentListSerializer(ExtendedDocumentSerializer):
    document_type = serializers.CharField(
        source='document_type.name')

    class Meta:
        model = VehicleDocument
        fields = ('document_type', 'status')


class VehicleDocumentFilePathSerializer(ExtendedDocumentSerializer):

    file_path = serializers.SerializerMethodField(source='get_file_path')
    document_number = serializers.SerializerMethodField(
        source='get_document_number')

    class Meta:
        model = VehicleDocument
        fields = ('file_path', 'document_number',)

    def get_file_path(self, document):
        if document.file:
            return document.file.url
        return ''

    def get_document_number(self, document):
        return document.number
