# Django and System libraries

# 3rd party libraries
from rest_framework import serializers

# blowhorn imports
from blowhorn.driver.models import *
from blowhorn.document.serializers import ExtendedDocumentSerializer
from blowhorn.apps.operation_app.v3.serializers.vehicle import *
from blowhorn.apps.operation_app.v3.constants import *


class BackGroundVerificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = BackGroundVerification
        fields = '__all__'


class DriverDocumentSerializer(ExtendedDocumentSerializer):

    def create(self, validated_data):
        driver_doc = DriverDocument.objects.create(**validated_data)
        driver_doc.created_by = validated_data.get('created_by')
        driver_doc.modified_by = validated_data.get('modified_by')
        driver_doc.save()

        if not driver_doc:
            return False

        return driver_doc

    class Meta:
        model = DriverDocument
        fields = ExtendedDocumentSerializer.Meta.fields + ('driver',)


class DriverDocumentListSerializer(ExtendedDocumentSerializer):
    document_type = serializers.CharField(
        source='document_type.name')

    class Meta:
        model = DriverDocument
        fields = ('document_type', 'status')


class DriverSerializer(serializers.ModelSerializer):

    class Meta:
        model = Driver
        fields = '__all__'


class DriverAddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = DriverAddress
        fields = '__all__'
