# System and Django imports
from __future__ import absolute_import, unicode_literals
from django.conf import settings

# 3rd party libraries
from celery import shared_task

# blowhorn imports
from blowhorn import celery_app
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.common.notification import Notification
from blowhorn.driver.models import Driver
from blowhorn.users.models import User


MESSAGE_APPROVED_FOR_BGV = '{action_owner} has approved your request for Background Verification of {driver_name}'
MESSAGE_REJECTED_FOR_BGV = '{action_owner} has rejected your request for Background Verification of {driver_name} because {reason}'
MESSAGE_APPROVAL_NEEDED_FOR_BGV = '{action_owner} has requested Background Verification approval for {driver_name}'


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES,
             ignore_result=False)
def notify_user(self, user_list, sent_by, driver_pk, is_superuser, approval):

    driver = Driver.objects.get(id=driver_pk)
    driver_name = '%s' % driver
    sent_by_name = User.objects.get(id=sent_by).name
    message = ''
    if is_superuser:
        if approval:
            message = MESSAGE_APPROVED_FOR_BGV.format(
                action_owner=sent_by_name,
                driver_name=driver_name
            )
        else:
            message = MESSAGE_REJECTED_FOR_BGV.format(
                action_owner=sent_by_name,
                driver_name=driver_name,
                reason=driver.bgv_unapproval_reason
            )
    else:
        message = MESSAGE_APPROVAL_NEEDED_FOR_BGV.format(
            action_owner=sent_by_name,
            driver_name=driver_name
        )

    users = User.objects.filter(id__in=user_list)
    for user in users:
        if is_superuser:
            Notification.send_approval_unapproval_notification(
                user, driver, message)
        else:
            Notification.send_driver_data_to_manager(
                user, driver, message)
