from config.settings.status_pipelines import SUPPLY, \
    ONBOARDING, INACTIVE, ACTIVE


WORKING_PREFERENCES_MAP = [{'name': 'ADHOC',
                            'display_name': 'Adhoc',
                            'display_extra_options': False
                            },
                           {'name': 'MONTHLY',
                            'display_name': 'Monthly',
                            'display_extra_options': True
                            },
                           {'name': 'SME',
                            'display_name': 'SME',
                            'display_extra_options': False
                            }
                           ]


NUMBER_OF_ENTRY_IN_PAGE = 1000

NUMBER_OF_ORDERS_IN_PAGE = 100

PAGE_TO_BE_DISPLAYED = 1


DISPLAY_EXTRA_OPTIONS_FOR_PREFERENCES = ['MONTHLY']

SEARCH_RADIUS_IN_KM = 2


# USER_ACCESS_WEBSITE = u'Website'
# USER_ACCESS_APP = u'App'


# USER_ACCESS_MAP = [USER_ACCESS_APP, USER_ACCESS_WEBSITE]

DRIVER_FIELD_ALLOWED_FOR_UPDATE = [
    'pan', 'driver_vehicle']


BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING = {
    'DRIVING_LICENCE': 'DL',
    'VOTER_ID': 'Voter_ID',
    'AADHAR': 'Aadhaar',
    'RC': 'RC',
    # 'BANK_ACCOUNT': 'Bank_Passbook',
    'PAN': 'PAN'
}

BETTERPLACE_REVERSE_DOCUMENT_TYPE_CODE_MAPPING = {
    'DL': 'DRIVING_LICENCE',
    'Voter_ID': 'VOTER_ID',
    'Aadhaar': 'AADHAR',
    'RC': 'RC',
    # 'Bank_Passbook': 'BANK_ACCOUNT',
    'PAN': 'PAN'
}

BETTERPLACE_DRIVER_DOCUMENT_CODE = [
    'DRIVING_LICENCE', 'VOTER_ID', 'AADHAR', 'PAN']
BETTERPLACE_VEHICLE_DOCUMENT_CODE = ['RC']

STATE_REQUIRED_FOR_THE_DOCUMENTS = ['DL', 'Voter_ID']

PENDING = 'pending'
SENT_FOR_APPROVAL = 'sent_for_approval'
APPROVED = 'approved'
DISAPPROVED = 'disapproved'
SENT_FOR_VERIFICATION = 'sent_for_verification'
VERIFICATION_FAILED = 'verification_failed'
VERIFICATION_PASSED = 'verification_passed'

STATUS_FOR_NOT_SHOWING_NOTIFICATION = [APPROVED, DISAPPROVED,
                                       SENT_FOR_VERIFICATION]

STATUS_FOR_SENDING_VERIFICATION = [PENDING, DISAPPROVED]

DRIVER_LIST_FIELDS = ('id', 'gender', 'is_address_same',
                      'operating_city__name', 'own_vehicle',
                      'owner_details__phone_number',
                      'owner_details__name',
                      'owner_details__pan',
                      'user', 'user__phone_number', 'status',
                      'driver_vehicle', 'name', 'reason_for_inactive',
                      'can_read_english',
                      'can_drive_and_deliver',
                      'working_preferences',
                      'can_source_additional_labour', 'work_status',
                      'approved_for_bgv', 'bgv_unapproval_reason',
                      'bgv_status', 'bgv_requested_by', 'date_of_birth',
                      'father_name', 'data_ready_for_bgv',
                      'driving_license_number',
                      'operating_city__name', 'pan', 'prior_monthly_income',
                      'current_address', 'current_address__title',
                      'current_address__first_name',
                      'current_address__last_name',
                      'current_address__search_text',
                      'current_address__line4', 'current_address__line1', 'current_address__state',
                      'current_address__postcode',
                      'current_address__geopoint',
                      'current_address__country__name',
                      'permanent_address', 'permanent_address__title',
                      'permanent_address__first_name',
                      'permanent_address__last_name',
                      'permanent_address__search_text',
                      'permanent_address__line4', 'permanent_address__line1', 'permanent_address__state',
                      'permanent_address__postcode',
                      'permanent_address__geopoint',
                      'permanent_address__country__name',
                      'languages_known', 'previous_company', 'duty_timings', 'educational_qualifications',
                      )


STATUS_CHANGE_MAPPING = [
    {
        'status': ONBOARDING,
        'status_list': [ONBOARDING, ACTIVE, INACTIVE]},
    {
        'status': ACTIVE,
        'status_list': [ACTIVE, INACTIVE]},
    {
        'status': INACTIVE,
        'status_list': [ACTIVE, INACTIVE]},
    {
        'status': SUPPLY,
        'status_list': []}
]

REASON_FOR_INACTIVATION = ['Lost to competition', 'Contract issue - Timings',
                           'Contract issue - Route/Kms', 'Low salary',
                           'Lack of Bookings', 'Delay in Salary',
                           'Personal issues', 'Disciplinary Isues',
                           'Contract closed']


REASON_FOR_BLACKLISTING = ['Sudden Discontinuation - without prior notice',
                           'High Absenteeism', 'Theft and fraud', 'Intoxicated on duty',
                           'Rude and indecent behavior', 'Damaged goods or property',
                           'On duty violation', 'Others']

REGEX_MATCH_ANYTHING = r'^.*$'

DRIVER_MANDATORY_FIELDS = ['date_of_birth', 'current_address', 'father_name']
