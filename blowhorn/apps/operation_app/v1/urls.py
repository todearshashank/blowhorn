from django.conf.urls import url
from blowhorn.apps.operation_app.v1.views import user as user_views
from blowhorn.apps.operation_app.v1.views import driver as driver_views
from blowhorn.apps.operation_app.v1.views import vehicle as vehicle_views


urlpatterns = [
    url(r'^login$', user_views.Login.as_view(),
        name='operation-user-login'),
    url(r'^drivers/add$', driver_views.DriverAdd.as_view(),
        name='driver-add'),
    url(r'^drivers/filter$', driver_views.DriversList.as_view(),
        name='driver-filter'),
    url(r'^drivers/documents/upload$', driver_views.DriverDocumentUpload.as_view(),
        name='driver-document-upload'),
    url(r'^vehicles/vehiclemodels$',
        vehicle_views.VehicleModelList.as_view(), name='vehicle-model-list'),
    url(r'^vehicles/documents/upload$', vehicle_views.VehicleDocumentUpload.as_view(),
        name='vehicle-document-upload'),

]

