# Django and System libraries
import pytz
import json
import logging

from django.conf import settings

# 3rd party libraries
from rest_framework import serializers
from phonenumber_field.phonenumber import PhoneNumber

# blowhorn imports
from blowhorn.common.serializers import BaseSerializer
from blowhorn.driver.models import Driver, DriverDocument, DriverPreferredLocation
from blowhorn.utils import functions as utils_func
from blowhorn.address.utils import AddressUtil
from blowhorn.document.serializers import DocumentSerializer
from blowhorn.users.models import User
from blowhorn.apps.operation_app.v1.serializers.vehicle import VehicleSerializer


class DriverPreferredLocationSerializer(serializers.ModelSerializer):

    coordinates = serializers.SerializerMethodField(
        source='get_coordinates')

    def get_coordinates(self, driverpreferredlocation):
        coordinates = {}
        if driverpreferredlocation.geopoint:
            latlng_dict = AddressUtil().get_latlng_from_geopoint(
                driverpreferredlocation.geopoint)
            coordinates['latitude'] = latlng_dict.get('lat')
            coordinates['longitude'] = latlng_dict.get('lng')
        return coordinates

    class Meta:
        model = DriverPreferredLocation
        fields = ('id', 'driver', 'geopoint', 'coordinates',)


class DriverListSerializer(serializers.ModelSerializer):
    mobile = serializers.CharField(source='user.phone_number')

    address_model = serializers.SerializerMethodField(
        source='get_address_model')

    driver_is_owner = serializers.BooleanField(source='own_vehicle')

    vehicles = serializers.SerializerMethodField(source='get_vehicles')

    operating_city = serializers.SerializerMethodField(
        source='get_operating_city')

    owner_contact_no = serializers.SerializerMethodField(
        source='get_owner_contact_number')

    class Meta:
        model = Driver
        fields = ('id', 'name', 'driver_vehicle', 'mobile', 'driver_is_owner', 'owner_contact_no',
                  'working_preferences', 'can_drive_and_deliver', 'can_read_english', 'driving_license_number',
                  'can_source_additional_labour', 'work_status', 'address_model', 'status', 'vehicles', 'operating_city')

    def get_address_model(self, driver):
        address_dict = []

        driver_preferred_location = driver.driverpreferredlocation_set.all()
        if driver_preferred_location:
            address_dict = DriverPreferredLocationSerializer(
                driver_preferred_location, many=True).data

        return address_dict

    def get_vehicles(self, driver):
        vehicle_queryset = driver.vehicles.all()
        if vehicle_queryset:
            return VehicleSerializer(vehicle_queryset, many=True).data
        return []

    def get_operating_city(self, driver):
        return driver.operating_city.name

    def get_owner_contact_no(self, driver):
        if driver.owner_details and driver.owner_details.phone_number:
            return str(driver.owner_details.phone_number)
        return ""


class DriverAppSerializer(serializers.ModelSerializer):

    def validate_name(self, name):
        if not utils_func.validate_name(name):
            logging.error("Please Enter a valid name")
            raise serializers.ValidationError("Please Enter a valid name")
        return name

    def validate(self, data):
        data = self.initial_data
        if not utils_func.validate_phone_number(data.get('phone')):
            logging.error("Please Enter a valid phone number")
            raise serializers.ValidationError(
                "Please Enter a valid phone number")
        return data

    def create(self, validated_data):
        validated_data.pop('phone')
        driver = Driver.objects.create(**validated_data)
        driver.created_by = validated_data.get('created_by')
        driver.modified_by = validated_data.get('created_by')
        driver.save()

        if not driver:
            return False

        return driver

    class Meta:
        model = Driver
        fields = '__all__'


class DriverActivateSerializer(serializers.ModelSerializer):

    # modified_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Driver
        fields = '__all__'


class DriverDocumentSerializer(DocumentSerializer):

    def create(self, validated_data):
        driver_doc = DriverDocument.objects.create(**validated_data)
        driver_doc.created_by = validated_data.get('created_by')
        driver_doc.modified_by = validated_data.get('modified_by')
        driver_doc.save()

        if not driver_doc:
            return False

        return driver_doc

    class Meta:
        model = DriverDocument
        fields = DocumentSerializer.Meta.fields + ('driver',)
