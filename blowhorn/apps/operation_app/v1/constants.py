WORKING_PREFERENCES_MAP = [{'name': 'ADHOC', 'display_name': 'Adhoc', 'display_extra_options': False},
                           {'name': 'MONTHLY', 'display_name': 'Monthly',
                               'display_extra_options': True},
                           {'name': 'SME', 'display_name': 'SME', 'display_extra_options': False}]


NUMBER_OF_ENTRY_IN_PAGE = 100

PAGE_TO_BE_DISPLAYED = 1


DISPLAY_EXTRA_OPTIONS_FOR_PREFERENCES = ['MONTHLY']

SEARCH_RADIUS_IN_KM = 2


USER_ACCESS_WEBSITE = u'Website'
USER_ACCESS_APP = u'App'


USER_ACCESS_MAP = [USER_ACCESS_APP, USER_ACCESS_WEBSITE]

DRIVER_FIELD_ALLOWED_FOR_UPDATE = [
    'pan', 'own_vehicle', 'owner_details']
