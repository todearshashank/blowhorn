# Django and System libraries
import logging
from django.core.exceptions import ValidationError
from django import db
from django.utils.translation import ugettext as _
from django.contrib.auth import get_user_model
from django.conf import settings as setting

from blowhorn.oscar.core.loading import get_model
from phonenumber_field.phonenumber import PhoneNumber

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.apps.operation_app.v1.serializers import vehicle as vehicle_serializer
from blowhorn.utils import functions as utils_func


User = get_user_model()
# Country = get_model('address', 'Country')
Driver = get_model('driver', 'Driver')
Vehicle = get_model('vehicle', 'Vehicle')
VehicleClass = get_model('vehicle', 'VehicleClass')
VehicleModel = get_model('vehicle', 'VehicleModel')
BodyType = get_model('vehicle', 'BodyType')
Vendor = get_model('vehicle', 'Vendor')


def get_detailed_data(params):
    country_code = setting.COUNTRY_CODE_A2
    country = Country.objects.get(pk=country_code)
    vehicle_parameters = params['vehicles'][
        0] if params.get('vehicles', []) else {}
    driver_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'can_drive_and_deliver': params.get('can_drive_and_deliver', ''),
        'can_source_additional_labour': params.get('can_source_additional_labour', ''),
        'driving_license_number': params.get('driving_license_number', params.get('mobile', '')),
        # 'owner_contact_no': params.get('owner_contact_no', ''),
        # 'owner_name': params.get('owner_name', ''),
        # 'owner_pan': params.get('owner_pan', ''),
        'work_status': params.get('work_status', ''),
        'working_preferences': params.get('working_preferences', []),
        'own_vehicle': params.get('driver_is_owner', False),
        'operating_city': params.get('operating_city', ''),
        'pan': params.get('pan_number', '')
    }

    vehicle_data = {
        'vehicle_model': vehicle_parameters.get('vehicle_model', ''),
        'vehicle_class': vehicle_parameters.get('vehicleClass', ''),
        'body_type': vehicle_parameters.get('body_type_name', ''),
        'registration_certificate_number': params.get('driver_vehicle', ''),
        'model_year': vehicle_parameters.get('vehicle_model_year', '')
    }

    user_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'password': params.get('password', None),
        'email': params.get('email', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'user_id': None
    }

    vendor_data = {
        'phone_number': params.get('owner_contact_no', ''),
        'name': params.get('owner_name', ''),
        'pan': params.get('owner_pan_number', '')
    }

    detailed_data = {
        'driver_data': driver_data,
        'vehicle_data': vehicle_data,
        'user_data': user_data,
        'vendor_data': vendor_data
    }

    return detailed_data


def get_user_for_driver(user_data):
    user = Driver.objects.upsert_user(
        email=user_data['email'],
        name=user_data['name'],
        phone_number=user_data['phone'],
        password=user_data['password'],
        user_id=user_data.get('user_id', None),
        is_active=True if user_data[
            'status'] == StatusPipeline.ACTIVE else False
    )
    return user


def get_vehicle_models():
    vehicle_model = VehicleModel.objects.all()
    body_types = BodyType.objects.values_list('body_type', flat=True)
    serialized_vehicle_data = vehicle_serializer.VehicleModelSerializer(
        vehicle_model, many=True, context={'body_types': body_types})

    return serialized_vehicle_data.data


def add_vehicle(vehicle_params, is_update):
    if vehicle_params.get('registration_certificate_number'):
        vehicle = Vehicle.objects.filter(
            registration_certificate_number=vehicle_params['registration_certificate_number']).first()

        if vehicle:
            if not is_update:
                return None
            return vehicle

        vehicle_model = VehicleModel.objects.filter(
            model_name=vehicle_params.get('vehicle_model')).first()

        body_type = BodyType.objects.filter(
            body_type=vehicle_params.get('body_type')).first()

        vehicle = Vehicle.objects.create(
            vehicle_model=vehicle_model,
            body_type=body_type,
            registration_certificate_number=vehicle_params.get(
                'registration_certificate_number'),
            model_year=vehicle_params.get('model_year'))

        return vehicle


def add_vendor(vendor_details):
    if vendor_details.get('name', ''):
        if not utils_func.validate_name(vendor_details.get('name', '')):
            logging.error("Please Enter a valid name")
            raise ValidationError("Please Enter a valid Owner name")

    if not utils_func.validate_phone_number(vendor_details.get('phone_number', '')):
        logging.error('Owner phone number is in incorrect format')
        raise ValidationError('Owner phone number is in incorrect format')

    phone_number = PhoneNumber(setting.ACTIVE_COUNTRY_CODE, vendor_details.get('phone_number'))

    vendor = Vendor.objects.filter(phone_number=phone_number,
                                   pan=vendor_details.get('pan', '')).first()

    if not vendor:
        vendor_serializer = vehicle_serializer.VendorSerializer(
            data=vendor_details)

        if vendor_serializer.is_valid():
            logging.info(
                "Going to the app serializer with data: %s", vendor_details)
            try:
                vendor = vendor_serializer.save()
            except db.IntegrityError as e:
                raise ValidationError(e)

        else:
            raise ValidationError(vendor_serializer.errors)

    return vendor
