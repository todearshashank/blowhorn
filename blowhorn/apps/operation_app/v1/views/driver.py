# Django and System libraries
import json
import logging
import datetime
import phonenumbers

from django.utils.translation import ugettext_lazy as _
from django import db
from django.conf import settings as setting
from django.core.exceptions import ValidationError

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.authentication import BasicAuthentication
from blowhorn.oscar.core.loading import get_model
from pytz import timezone
from phonenumber_field.phonenumber import PhoneNumber

# blowhorn imports
from blowhorn.apps.operation_app.v1.serializers import driver as driver_serializer
from blowhorn.common.helper import CommonHelper
from blowhorn.common import serializers as common_serializer
from blowhorn.address.models import City
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.document.models import DocumentType
from blowhorn.utils.functions import ist_to_utc
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.users.views import google_login
from blowhorn.document.serializers import DocumentTypeSerializer
from blowhorn.apps.operation_app.v1.constants import *
from blowhorn.apps.operation_app.v1.mixins import get_detailed_data, get_user_for_driver, add_vendor, add_vehicle


Driver = get_model('driver', 'Driver')
DriverVehicleMap = get_model('driver', 'DriverVehicleMap')
DriverDocument = get_model('driver', 'DriverDocument')
DriverPreferredLocation = get_model('driver', 'DriverPreferredLocation')

VEHICLE_EXIST = 'Vehicle with corresponding registration number already exists'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriversList(views.APIView):
    permission_classes = (IsAuthenticated,)
    # authentication_classes = (CsrfExemptSessionAuthentication,
    #                           BasicAuthentication)

    def post(self, request):
        """
        For displaying the filtered list of the drivers
        in a paginated manner
        """

        logging.info(
            "Following is the request data for driver filter :"
            " " + str(request.body))

        query_params = json.loads(request.body)
        filter_data = {}

        # TODO: To be uncommented while working with postman
        # query_params = request.data

        logging.info(
            "Following is the query params for driver filter :"
            " " + str(query_params))

        city = City.objects.filter(
            name=query_params.get('operating_city')).first()

        if not city:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data='No driver details found for this city')

        filter_data['operating_city'] = city

        if query_params.get('timestamp', ''):
            try:
                timestamp = datetime.datetime.strptime(
                    query_params.get('timestamp'), '%Y-%m-%d %H:%M:%S')
                last_modified = ist_to_utc(timestamp)
                last_modified_utc = last_modified.replace(
                    tzinfo=timezone('UTC'))
            except Exception as e:
                return Response(
                    status=status.HTTP_404_NOT_FOUND,
                    data='Provide timestamp in correct format')

            filter_data['modified_date__gte'] = last_modified

        driver_list = Driver.objects.filter(
            **filter_data).prefetch_related('driverpreferredlocation_set').order_by('status')

        if driver_list:
            page_to_be_displayed = query_params.get('next') if query_params.get(
                'next', '') else PAGE_TO_BE_DISPLAYED
            number_of_entry = NUMBER_OF_ENTRY_IN_PAGE

            logging.info(
                'Page to be displayed for the pagination purpose'
                ': %s' % page_to_be_displayed)
            logging.info(
                'Number of entry per page for pagination purpose'
                ': %s' % number_of_entry)

            serialized_data = common_serializer.PaginatedSerializer(
                queryset=driver_list, num=number_of_entry,
                page=page_to_be_displayed,
                serializer_method=driver_serializer.DriverListSerializer
            )

            return Response(status=status.HTTP_200_OK,
                            data=serialized_data.data)

        else:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data='No driver found')


class DriverAdd(views.APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    # authentication_classes = (CsrfExemptSessionAuthentication,
    #                           BasicAuthentication)

    def post(self, request):
        """
        For adding a new Driver
        """

        # logging.info("Request from the app" + str(request.data.dict()))
        # data = request.data.dict()

        try:
            data = json.loads(request.body)
            driver_details = data.get('driver_details', {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get('driver_details', '{}'))
            logging.info(
                "Request from the app dict : " + str(request.data.dict()))

        # TODO: To be uncommented while working with postman
        # data = request.data

        logging.info("Following are the params for driver : %s",
                     driver_details)

        if not driver_details:
            logging.info("Driver details not found")
            response_dict = {
                "status": False,
                "message": "Driver details not found"
            }
            return Response(data=response_dict,
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        detailed_data = get_detailed_data(params=driver_details)

        driver_data = detailed_data.get('driver_data', {})
        vehicle_data = detailed_data.get('vehicle_data', {})
        user_data = detailed_data.get('user_data', {})
        vendor_data = detailed_data.get('vendor_data', {})

        sid = db.transaction.savepoint()

        logger.info('city: %s', driver_data['operating_city'])

        city = City.objects.filter(
            name=driver_data.get('operating_city')).first()

        if city:
            driver_data['operating_city'] = city.id

        try:
            phone = phonenumbers.parse(driver_data.get('phone', ''),
                                       setting.DEFAULT_COUNTRY.get(
                'country_code'))
        except:
            phone = ''

        filter_data = {
            'name': driver_data.get('name', ''),
            'driver_vehicle': vehicle_data.get('registration_certificate_number', ''),
            'user__phone_number': phone
        }

        driver = Driver.objects.filter(**filter_data).first()

        if not driver or driver_details.get('isupdating'):
            if request.META.get('HTTP_DEVICE_TYPE', '') in setting.USER_CLIENT_TYPES:
                driver_data['source'] = USER_ACCESS_APP

            if request.user:
                driver_data['created_by'] = request.user
                driver_data['modified_by'] = request.user

            if driver_details.get('isupdating'):
                user_data['user_id'] = driver.user_id

            try:
                user = get_user_for_driver(user_data)
            except db.IntegrityError:
                logging.info("User with the Phone number already exist")

                return Response(data="Driver Phone number already exist",
                                status=status.HTTP_400_BAD_REQUEST,
                                content_type='text/html; charset=utf-8')

            driver_data['user'] = user.id

            try:
                vehicle = add_vehicle(
                    vehicle_params=vehicle_data, is_update=driver_details.get('isupdating'))
                if not vehicle:
                    logging.info(VEHICLE_EXIST)
                    db.transaction.savepoint_rollback(sid)

                    return Response(data=VEHICLE_EXIST,
                                    status=status.HTTP_400_BAD_REQUEST)
            except db.IntegrityError as e:
                logging.info(
                    "Following error occured while creating vehicle : " + str(e))
                db.transaction.savepoint_rollback(sid)

                return Response(data="Please Enter valid vehicle details",
                                status=status.HTTP_400_BAD_REQUEST)

            if driver_details.get('isupdating'):
                serializer = driver_serializer.DriverActivateSerializer(
                    instance=driver, data=driver_data)
            else:
                serializer = driver_serializer.DriverAppSerializer(
                    data=driver_data)

            if serializer.is_valid():
                logging.info(
                    "Going to the app serializer with data: %s", driver_data)
                try:
                    driver = serializer.save(operating_city=city, user=user)

                except db.IntegrityError as e:
                    logging.info(
                        "Following error occured while creating vehicle : %s", e)
                    response_dict = {
                        "status": False,
                        "message": e.args[0]
                    }
                    return Response(data=response_dict,
                                    status=status.HTTP_400_BAD_REQUEST)

                if not driver.driver_vehicle or (driver.driver_vehicle != vehicle.registration_certificate_number):
                    DriverVehicleMap.objects.create(
                        driver=driver, vehicle=vehicle)

                if driver_details.get('address_model', []):
                    preferred_location_data = []
                    DriverPreferredLocation.objects.filter(
                        driver=driver).delete()
                    for cord_data in driver_details['address_model']:
                        if isinstance(cord_data, dict) and cord_data.get('coordinates', {}):
                            latitude = cord_data.get(
                                'coordinates').get('latitude', None)
                            longitude = cord_data.get(
                                'coordinates').get('longitude', None)

                            logger.info('Latitude:- %s longitude: %s',
                                        latitude, longitude)

                            if latitude and longitude:
                                geopoint = CommonHelper.get_geopoint_from_latlong(
                                    latitude, longitude)

                                preferred_location_data.append(DriverPreferredLocation(
                                    geopoint=geopoint, driver_id=driver.id))

                    if preferred_location_data:
                        DriverPreferredLocation.objects.bulk_create(
                            preferred_location_data)

                if not driver_details.get('driver_activation', False):
                    response_dict = {'driver_id': driver.id,
                                     'vehicle_id': vehicle.id}

                    return Response(status=status.HTTP_200_OK,
                                    data=response_dict)
            else:
                db.transaction.savepoint_rollback(sid)
                logging.info(
                    "This is the serializer error : %s", serializer.errors)
                return Response(data=serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            if not driver_details.get('driver_activation', False):
                logging.info(
                    "Driver activation details needed")
                db.transaction.savepoint_rollback(sid)
                return Response(data="Driver already exists. Needs to be activated",
                                status=status.HTTP_400_BAD_REQUEST)

        if driver_details.get('driver_activation', False):
            if request.user:
                driver_data['modified_by'] = request.user

            if not driver_data.get('own_vehicle', False):
                try:
                    vendor = add_vendor(vendor_data)
                except (db.IntegrityError, ValidationError) as e:
                    logging.info(
                        "Following error occured while creating vendor : " + str(e))
                    db.transaction.savepoint_rollback(sid)

                    return Response(data="Please Enter valid vendor details",
                                    status=status.HTTP_400_BAD_REQUEST)

                driver_data['owner_details'] = vendor.id

            if driver_data.get('driving_license_number', '') == driver_data.get('phone', ''):
                logging.error('Driving License Number field is empty')
                db.transaction.savepoint_rollback(sid)
                return Response(data="Enter a valid driving license number",
                                status=status.HTTP_400_BAD_REQUEST)

            if city:
                driver_data['operating_city'] = city.id

            driver_data['user'] = driver.user.id
            # driver_data.pop('phone')

            serializer = driver_serializer.DriverActivateSerializer(
                instance=driver, data=driver_data)

            if serializer.is_valid():
                logging.info(
                    "Going to the activate serializer with data: %s", driver_data)
                try:
                    driver = serializer.save()

                except db.IntegrityError as e:
                    logging.info(
                        "Following error occured while activating driver : %s", e)
                    response_dict = {
                        "status": False,
                        "message": e.args[0]
                    }

                    return Response(data=e.args[0],
                                    status=status.HTTP_400_BAD_REQUEST)

            else:
                logging.info(
                    "Error occur in serializer : %s ", serializer.errors)
                db.transaction.savepoint_rollback(sid)
                return Response(data=serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)

            return Response(status=status.HTTP_200_OK,
                            data='Driver Details Added')


class DriverDocumentUpload(views.APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in driver document add : " + str(request.data.dict()))

        documents = request.data.dict()
        driver_id = ''
        doc_status = {}

        if not documents:
            logging.info(
                "No documents where provided for the verification of driver")
            return Response(data="Please upload the documents",
                            status=status.HTTP_400_BAD_REQUEST)

        if documents.get('document_status', []):
            document_status = json.loads(documents.pop('document_status'))

            for statuses in document_status:
                driver_id = driver_id if driver_id else statuses.get(
                    'driver_id', '')

                if statuses.get('document_type', ''):
                    doc_status[statuses['document_type']] = statuses.get(
                        'document_status', '')

        if not driver_id:
            logging.info(
                "Documents not added for the activatioon of driver")
            return Response(data="Documents for activation of Driver is needed",
                            status=status.HTTP_400_BAD_REQUEST)

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                logging.info(
                    "Invalid document type is provided")
                return Response(data="Provide the valid document type",
                                status=status.HTTP_400_BAD_REQUEST)

            document_data = {
                'file': documents[key],
                'document_type': doc_type.id,
                'driver': driver_id,
                'status': doc_status[key] if doc_status.get(key, '') else VERIFICATION_PENDING
            }

            if request.user:
                document_data['created_by'] = request.user
                document_data['modified_by'] = request.user

            document_serializer = driver_serializer.DriverDocumentSerializer(
                data=document_data)
            if document_serializer.is_valid():
                document_serializer.save(created_by=document_data.get(
                    'created_by'), modified_by=document_data.get('modified_by'))
            else:
                logging.info(document_serializer.errors)
                return Response(document_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='Driver Details Added')
