# Django and System libraries
import json
import logging

from django.utils.translation import ugettext_lazy as _

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.authentication import BasicAuthentication
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.address import utils as address_utils
from blowhorn.common.helper import CommonHelper
from blowhorn.common import serializers as common_serializer
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.document.models import DocumentType
from blowhorn.users.views import google_login
from blowhorn.document.serializers import DocumentTypeSerializer
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT, DOCUMENT_STATUSES
from blowhorn.apps.operation_app.v1.mixins import get_vehicle_models
from blowhorn.apps.operation_app.v1.constants import WORKING_PREFERENCES_MAP

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([])
class Login(views.APIView):
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def post(self, request):
        """
        :param request: Have the google authentication token
        :return: If token is valid and the user is present with the
                staff permission then return email, name, vehicle_data
        """
        result = {}
        logging.info('User is authenticating Google token from supply app')

        body = request.data
        location_details = json.loads(body.get('location_details'))

        logging.info('App Version: %s' % body.get('app_version', ''))

        logged_in_user = google_login(request)

        if logged_in_user.get('status_code', '') != status.HTTP_200_OK:
            return Response(status=logged_in_user['status_code'],
                            data=logged_in_user.get('error_msg'))

        vehicle_model_list = get_vehicle_models()

        operating_cities = address_utils.get_operating_cities(location_details)

        driver_document_type = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT)
        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)

        serialized_driver_doc_type_data = DocumentTypeSerializer(
            driver_document_type, many=True)
        serialized_vehicle_doc_type_data = DocumentTypeSerializer(
            vehicle_document_type, many=True)
        driver_document_type = serialized_driver_doc_type_data.data if serialized_driver_doc_type_data.data else []
        vehicle_document_type = serialized_vehicle_doc_type_data.data if serialized_vehicle_doc_type_data.data else []

        result = {
            'driver_document_list': driver_document_type,
            'vehicle_document_list': vehicle_document_type,
            'document_statuses': DOCUMENT_STATUSES,
            'auth_token': logged_in_user.get('token'),
            'name': logged_in_user.get('user_name'),
            'email': logged_in_user.get('user_email'),
            'vehicle_model': vehicle_model_list if vehicle_model_list else [],
            'working_preferences': WORKING_PREFERENCES_MAP,
            'operating_city_name': operating_cities if operating_cities else []
        }

        return Response(status=status.HTTP_200_OK, data=result, content_type='text/html; charset=utf-8')
