# Django and System libraries
import json
import logging

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.parsers import MultiPartParser, FormParser
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.operation_app.v1.serializers import vehicle as vehicle_serializer
from blowhorn.document.models import DocumentType
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.apps.operation_app.v1.constants import *
from blowhorn.apps.operation_app.v1.mixins import get_vehicle_models

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([])
class VehicleModelList(views.APIView):

    def get(self, request):
        """
        Fetch all the vehicle models along with their vehicle class and body type
        """
        try:
            vehicle_model_list = get_vehicle_models()
            logging.info('Vehicle model list fetched: ' +
                         str(vehicle_model_list))

            return Response(status=status.HTTP_200_OK, data=vehicle_model_list)
        except Exception as e:
            logging.error('Get vehicle model list output error: %s' % e)
            return Response(status=status.HTTP_400_BAD_REQUEST)


class VehicleDocumentUpload(views.APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in vehicle document add : " + str(request.data.dict()))

        documents = request.data.dict()
        vehicle_id = None
        doc_status = {}

        if not documents:
            logging.info(
                "No documents where provided for the verification of vehicle")
            return Response(data="Please upload the documents",
                            status=status.HTTP_400_BAD_REQUEST)

        if documents.get('document_status', []):
            document_status = json.loads(documents.pop('document_status'))

            for statuses in document_status:
                vehicle_id = vehicle_id if vehicle_id else statuses.get(
                    'vehicle_id', '')

                if statuses.get('document_type', ''):
                    doc_status[statuses['document_type']] = statuses.get(
                        'document_status', '')

        if not vehicle_id:
            logging.info(
                "Documents not added for the activation of vehicle")
            return Response(data="Documents for activation of Vehicle is needed",
                            status=status.HTTP_400_BAD_REQUEST)

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                logging.info(
                    "Invalid document type is provided")
                return Response(data="Provide the valid document type",
                                status=status.HTTP_400_BAD_REQUEST)

            document_data = {
                'file': documents[key],
                'document_type': doc_type.id,
                'vehicle': vehicle_id,
                'status': doc_status[key] if doc_status.get(key, '') else VERIFICATION_PENDING
            }

            if request.user:
                document_data['created_by'] = request.user
                document_data['modified_by'] = request.user

            document_serializer = vehicle_serializer.VehicleDocumentSerializer(
                data=document_data)
            if document_serializer.is_valid():
                document_serializer.save(created_by=document_data.get(
                    'created_by'), modified_by=document_data.get('modified_by'))
            else:
                logging.info(document_serializer.errors)
                return Response(document_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='Vehicle Details Added')
