import logging
from django.utils.translation import ugettext_lazy as _

from rest_framework import views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.apps.operation_app.v5.serializers.vendor import \
    VendorVehicleSerializer
from blowhorn.apps.operation_app.v5.services.vendor_vehicle_mapping import \
    VendorVehicleMixin
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.vehicle.models import Vendor
from blowhorn.vehicle.constants import VENDOR_STATUS_INACTIVE

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class VendorVehicleListView(views.APIView, VendorVehicleMixin):
    """
    To fetch list of vehicles owned by a vendor
    GET <base>/fleetowner/<vendor_pk>/vehicles
    """
    serializer_class = VendorVehicleSerializer
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request, vendor_pk=None):
        qs = self.serializer_class.get_queryset(vendor_pk=vendor_pk)
        serializer = self.serializer_class(qs, many=True)
        return Response(status=200, data=serializer.data)


class VendorVehicleMapView(views.APIView, VendorVehicleMixin):
    """
    To map/unmap vehicle(s) to a vendor
    POST <base>/fleetowner/<vendor_pk>/map/vehicle
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request, vendor_pk=None):
        data = request.data
        vendor = Vendor.objects.get(pk=vendor_pk)

        if vendor.status == VENDOR_STATUS_INACTIVE:
            return Response(status=400, data=_("Please activate the vendor before mapping vehicles"))

        vehicles_list = data
        added_vehicle_pks = []
        deleted_vehicle_pks = []

        for i in vehicles_list:
            if i.get('flag') == 'added':
                ids = i.get('id', None)
                added_vehicle_pks.append(ids)
            elif i.get('flag') == 'deleted':
                ids = i.get('id', None)
                deleted_vehicle_pks.append(ids)

        mapped_count = 0
        unmapped_count = 0

        if len(added_vehicle_pks):
            mapped_count = self.map_existing_vehicle_to_vendor(added_vehicle_pks, vendor, request.user)

        if len(deleted_vehicle_pks):
            unmapped_count = self.unmap(deleted_vehicle_pks, vendor, request.user)

        if mapped_count == 0 and unmapped_count == 0:
            return Response(status=200)

        return Response(
            status=200, data=_('%s Vehicle(s) mapped. %s Vehicle(s) unmapped' % (mapped_count, unmapped_count)))
