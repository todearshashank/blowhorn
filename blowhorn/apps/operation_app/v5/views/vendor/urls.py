from django.conf.urls import url

from .list import VendorListView
from .add_update import *
from .vehicle_mapping import *
from .driver_mapping import *
from .detail import VendorDetailView
from .status import VendorActivationView

urlpatterns = [
    url(r"^list$", VendorListView.as_view(), name="vendor-list"),
    url(r"^add/$", VendorAdditionView.as_view(), name="vendor-add"),
    url(r"^personal_details/(?P<vendor_pk>[0-9]+)$",
        VendorUpdatePersonalDetailView.as_view(), name="vendor-personal"),
    url(r"^preferred_location/(?P<vendor_pk>[0-9]+)$",
        VendorUpdateLocationPreferenceView.as_view(),
        name="vendor-preferred-location"),
    url(r"^address/(?P<vendor_pk>[0-9]+)$",
        VendorUpdateAddressView.as_view(), name="vendor-address"),
    url(r"^bank/(?P<vendor_pk>[0-9]+)$", VendorUpdateBankDetailsView.as_view(),
        name="vendor-bank"),
    url(r"^work_preference/(?P<vendor_pk>[0-9]+)$",
        VendorUpdateWorkingPreferenceDetailsView.as_view(),
        name="vendor-work-preference"),
    url(r"^(?P<vendor_pk>[0-9]+)/vehicles$",
        VendorVehicleListView.as_view(),
        name="vendor-vehicles"),
    url(r"^(?P<vendor_pk>[0-9]+)/activate",
        VendorActivationView.as_view(),
        name="vendor-activate"),

    url(r"^(?P<vendor_pk>[0-9]+)/map/vehicle/$",
        VendorVehicleMapView.as_view()),
    url(r"^(?P<vendor_pk>[0-9]+)/map/driver/$",
        VendorDriverMapView.as_view()),

    url(r"^documents/upload/$", VendorDocumentUpload.as_view()),
    url(r"^documents/update/$", VendorDocumentUpdate.as_view()),

    url(r"^(?P<vendor_pk>[0-9]+)/drivers$",
        VendorDriverListView.as_view(),
        name="vendor-drivers"),
    url(r"^(?P<vendor_pk>[0-9]+)/details$",
        VendorDetailView.as_view(),
        name="vendor-detail"),
]
