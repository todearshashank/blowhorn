import re
import json
import logging
from django.db import transaction
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from blowhorn.address.models import Country
from django.conf import settings


from rest_framework import views
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response

from blowhorn.address.models import State
from blowhorn.document.constants import DOC_VALIDATION_REGEX_MAPPING
from blowhorn.apps.operation_app.v5.services.vendor import VendorMixin, \
    VendorValidationMixin
from blowhorn.document.models import DocumentType, Document
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.vehicle.models import Vendor, VendorDocument, VendorHistory
from blowhorn.vehicle.serializers import VendorDocumentSerializer, VendorDocumentPageSerializer
from blowhorn.vehicle.constants import VENDOR_STATUS_ACTIVE, VENDOR_STATUS_INACTIVE, \
    VENDOR_STATUS_BLACKLISTED

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class VendorAdditionView(views.APIView, VendorValidationMixin, VendorMixin):
    """
    Creates new vendor in system
    POST <base>/fleetowner/add
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def post(self, request):
        data = request.data
        print('DATA: ', data)
        personal_details = data.get('personal_details', {})
        location_details = personal_details.pop('locations', [])

        cleaned_personal_details = self.clean_personal_details(
            personal_details,
            mandatory_fields=['name', 'phone_number'],
            validate_fields=['phone_number'],
            vendor_pk=None
        )

        cleaned_personal_details.pop('status', None)
        cleaned_personal_details.pop('reason_for_inactive', None)

        vendor = self.update_personal_details(
            cleaned_personal_details, action_owner=request.user)
        cleaned_location_data = self.clean_location_details(
            location_details, vendor)
        locations = self.update_location_preference(
            cleaned_location_data, vendor, request.user)
        return Response(status=200, data=dict(id=vendor.pk))


class VendorUpdatePersonalDetailView(views.APIView, VendorValidationMixin,
                                     VendorMixin):
    """
    Updates personal details of a vendor
    PUT <base>/fleetowner/personal_details/<vendor_pk>
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request,  vendor_pk=None):
        data = request.data
        logger.info('data: %s' % data)
        data.pop('locations', None)
        data.pop('segment_save', None)

        vendor = Vendor.objects.select_related('user').get(pk=vendor_pk)
        vendor_status = data.get('status')

        if not vendor.status == vendor_status:
            if vendor_status == VENDOR_STATUS_ACTIVE:

                is_ready, message = self.is_ready_for_activation(vendor)
                if not is_ready:
                    return Response(status=400, data=message)

                vendor = self.update_vendor_status(
                    vendor, VENDOR_STATUS_ACTIVE, request.user, reason_for_inactive=None)
                return Response(status=200, data=_("Fleet owner activated successfully"))

            elif vendor_status == VENDOR_STATUS_INACTIVE:

                is_ready, message = self.is_ready_for_deactivation(vendor)
                if not is_ready:
                    return Response(status=400, data=[message])

                vendor = self.update_vendor_status(
                    vendor, VENDOR_STATUS_INACTIVE, request.user,
                    reason_for_inactive=data.get('reason_for_inactive', None))
                return Response(status=200, data=_("Fleet owner deactivated successfully"))

            elif vendor_status == VENDOR_STATUS_BLACKLISTED:
                is_ready, message = self.is_ready_for_deactivation(vendor)
                if not is_ready:
                    return Response(status=400, data=[message])

                vendor = self.update_vendor_status(
                    vendor, VENDOR_STATUS_BLACKLISTED, request.user,
                    reason_for_inactive=data.get('reason_for_inactive', None),
                    remarks=data.get('remarks', None)
                )

        data.pop('status', None)
        data.pop('reason_for_inactive', None)
        data.pop('remarks', None)

        cleaned_personal_details = self.clean_personal_details(
            data,
            mandatory_fields=['name', 'phone_number'],
            validate_fields=['phone_number', 'date_of_birth'],
            vendor_pk=vendor_pk
        )

        history_fields = ['pan', 'phone_number']
        vendors_history_list = self._generate_vendor_history(vendor, cleaned_personal_details, history_fields,
                                                             request.user)

        vendor = self.update_personal_details(cleaned_personal_details, vendor, vendors_history_list, request.user)

        return Response(
            status=200,
            data=_('Personal details updated successfully')
        )


class VendorUpdateLocationPreferenceView(views.APIView, VendorValidationMixin,
                                         VendorMixin):
    """
    Updates preferred locations of a vendor
    PUT <base>/fleetowner/preferred_location/<vendor_pk>
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request,  vendor_pk=None):
        data = request.data
        location_details = data.get('location_details', {})
        vendor = Vendor.objects.get(pk=vendor_pk)
        cleaned_location_data = self.clean_location_details(
            location_details, vendor)
        locations = self.update_location_preference(
            cleaned_location_data, vendor, request.user)
        return Response(
            status=200, data=_('Location preferences saved successfully'))


class VendorUpdateAddressView(views.APIView, VendorValidationMixin,
                              VendorMixin):
    """
    Updates address details (current/permanent) of a vendor
    PUT <base>/fleetowner/address/<vendor_pk>
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request,  vendor_pk=None):
        data = request.data
        vendor = Vendor.objects.get(pk=vendor_pk)

        current_address = data.get('current_address')

        country = Country.objects.filter(iso_3166_1_a2__iexact=settings.COUNTRY_CODE_A2).first()

        current_address = {
            'line1': current_address.get('addressLine1'),
            'line4': current_address.get('city'),
            'state': current_address.get('state'),
            'postcode': current_address.get('postalCode'),
            'country': country
        }

        permanent_address = data.get('permanent_address')
        permanent_address = {
            'line1': permanent_address.get('addressLine1'),
            'line4': permanent_address.get('city'),
            'state': permanent_address.get('state'),
            'postcode': permanent_address.get('postalCode'),
            'country': country
        }

        created, curr_address_data = self.clean_address_details(
            current_address, vendor, key='current_address')
        address_instance = self.update_address(
            created, curr_address_data, vendor, 'current_address', request.user)

        if data['is_address_same']:
            setattr(vendor, 'permanent_address', vendor.current_address)
            setattr(vendor, 'is_address_same', True)
            self.update_auditlog(vendor, request.user)

            return Response(
                status=200,
                data=_('%s updated successfully')
            )
        else:
            created, perm_address_data = self.clean_address_details(
            permanent_address, vendor, key='permanent_address')

            address_instance = self.update_address(
                created, perm_address_data, vendor, 'permanent_address', request.user)

        return Response(
            status=200,
            data=_('%s updated successfully')
        )


class VendorUpdateBankDetailsView(views.APIView, VendorValidationMixin,
                                  VendorMixin):
    """
    Updates bank details of a vendor
    PUT <base>/fleetowner/bank/<vendor_pk>
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request, vendor_pk=None):
        data = request.data
        cleaned_data = self.clean_bank_details(data)
        vendor = Vendor.objects.get(pk=vendor_pk)

        history_fields = ['bank_account']
        from blowhorn.driver.models import DriverPayment
        from blowhorn.driver.constants import APPROVED
        if DriverPayment.objects.filter(owner_details_id=vendor_pk, status=APPROVED, is_settled=False).exists():
            return Response(status=400, data=_(
                'Fleet owner has unsettled payments, settle the payments to change the bank account'))
        vendors_history_list = self._generate_vendor_history(vendor, cleaned_data, history_fields,
                                                             request.user)

        account = self.update_bank_details(cleaned_data, vendor, vendors_history_list, request.user)
        return Response(
            status=200,
            data=_('Bank account details updated successfully')
        )


class VendorUpdateWorkingPreferenceDetailsView(
        views.APIView, VendorValidationMixin, VendorMixin):
    """
    Updates working preference details of a vendor
    PUT <base>/fleetowner/work_preference/<vendor_pk>
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request, vendor_pk=None):
        data = request.data
        cleaned_data = self.clean_work_preferences(data)
        updated_vendor = self.update_work_preferences(cleaned_data, vendor_pk)
        return Response(
            status=200,
            data=_('Work preferences updated successfully')
        )


class VendorDocumentUpload(views.APIView):
    """
        To upload the fleetowner document
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in driver document add : " +
            str(request.data.dict())
        )

        documents = request.data.dict()
        fleet_id = ""
        doc_status = {}
        doc_number = {}
        doc_state = {}
        expiry_date = {}
        document_page = {}
        doc_ids = {}

        if not documents:
            logger.info(
                "No documents where provided for the verification of fleetowner")
            return Response(status=400, data=_('Please upload the documents'))

        documents_uploaded = json.loads(documents.pop('uploaded_docs')) if documents.get(
            'uploaded_docs', None) else None

        if documents.get("document_status", []):
            document_data = json.loads(documents.pop("document_status"))

            for doc in document_data:
                fleet_id = fleet_id if fleet_id else doc.get(
                    "fleet_id", "")

                if doc.get("document_type", ""):
                    doc_status[doc["document_type"]] = doc.get(
                        "document_status", ""
                    )

                if not doc.get("document_number"):
                    logger.info(
                        "No document number was provided for %s"
                        % doc["document_type"]
                    )
                    return Response(
                        data="Please Enter the document number for "
                        + str(doc["document_type"]),
                        status=400,
                    )

                if doc.get("document_pages", None):
                    document_page[doc["document_type"]] = doc.get("document_pages", None)

                if doc.get("id", None):
                    doc_ids[doc["document_type"]] = doc.get("id", None)

                doc_number[doc["document_type"]] = doc["document_number"]
                regex_pattern = DOC_VALIDATION_REGEX_MAPPING.get(
                    doc["document_type"], '')
                if regex_pattern and \
                        not re.match(regex_pattern, doc["document_number"]):
                    return Response(
                        data="Invalid document number for "
                             + str(doc["document_type"]),
                        status=400,
                    )

                if not doc.get("document_state"):
                    logger.info(
                        "No state was provided for %s" % doc["document_type"]
                    )
                    return Response(
                        data="Please Enter the issuing state for "
                        + str(doc["document_type"]),
                        status=400,
                    )

                doc_state[doc["document_type"]] = State.objects.filter(
                    name=doc["document_state"]
                ).first()
                expiry_date[doc["document_type"]] = doc["expiry_date"]

        if not fleet_id:
            logger.info("Documents not added for the activation of fleetowner")
            return Response(
                data="Documents for activation of fleetowner is needed",
                status=400,
            )

        for key, value in doc_number.items():
            doc_exists = VendorDocument.objects.filter(
                document_type__code=key, number=value,
                status=VendorDocument.ACTIVE).exclude(
                vendor_id=fleet_id
            ).first()
            if doc_exists:
                return Response(status=400, data=_('Document number %s already exists with fleet owner %s') % (
                    value, doc_exists.vendor))

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                if documents_uploaded:
                    continue
                logger.info("Invalid document type is provided")
                return Response(
                    data="Provide the valid document type",
                    status=400)

            file = documents[key]
            if doc_ids.get(key, None):
                file = VendorDocument.objects.get(id=doc_ids[key]).file
            document_data = {
                "file": file,
                "document_type": doc_type.id,
                "vendor": fleet_id,
                "status": doc_status[key]
                if doc_status.get(key, "")
                else Document.VERIFICATION_PENDING,
                "number": doc_number[key],
                "state": doc_state[key].pk,
                "expiry_date": expiry_date[key],
            }

            if request.user:
                document_data["created_by"] = request.user
                document_data["modified_by"] = request.user

            document_serializer = VendorDocumentSerializer(data=document_data)
            if document_serializer.is_valid():
                doc = document_serializer.save(
                    created_by=document_data.get("created_by"),
                    modified_by=document_data.get("modified_by"),
                )
            else:
                logger.info(document_serializer.errors)
                return Response(document_serializer.errors, status=400)

            if document_page.get(key, None):
                document_page[key].append({
                    'file': doc.file,
                    'page_number': 1
                })
                for page in sorted(document_page[key], key=lambda x: x.get('page_number')):
                    page_key = page.pop('page_key', None)
                    if page_key:
                        page['file'] = documents[page_key]
                    page['vendor_document'] = doc.id
                    document_page_serializer = VendorDocumentPageSerializer(data=page)
                    if document_page_serializer.is_valid():
                        doc_page = document_page_serializer.save(
                            created_by=document_data.get("created_by"),
                            modified_by=document_data.get("modified_by"),
                        )

                    else:
                        logger.info(document_page_serializer.errors)
                        return Response(document_page_serializer.errors,
                                        status=400)

        return Response(status=200, data=_('Documents uploaded successfully'))


class VendorDocumentUpdate(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request):
        documents = request.data
        with transaction.atomic():
            for doc in documents:
                doc_id = doc.pop('id')
                if doc['status'] != Document.VERIFICATION_PENDING:
                    return Response(
                        status=400,
                        data=_('Only Pending Document can be updated'))

                state_name = doc.pop('state', None)
                if state_name:
                    doc['state'] = State.objects.get(name=state_name)

                doc_type = doc.get('document_type', None)
                if doc_type:
                    doc['document_type'] = DocumentType.objects.get(
                        code=doc_type)

                if request.user and request.user.is_authenticated:
                    doc['modified_by'] = request.user

                doc['vendor_id'] = doc.pop('fleet_id')
                doc['modified_date'] = timezone.now()
                VendorDocument.objects.filter(id=doc_id).update(**doc)
        return Response(status=200, data=_('Updated Successfully'))
