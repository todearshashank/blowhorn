import logging
import json
from django.utils.translation import ugettext_lazy as _

from rest_framework import views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.apps.operation_app.v5.serializers.vendor import \
    VendorDetailSerializer
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.vehicle.models import Vendor
from blowhorn.apps.operation_app.v3.helper import is_superuser_or_city_manager


class VendorDetailView(views.APIView):
    """
       To fetch details of a vendor
       GET <base>/vendor/<vendor_pk>/details
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    serializer_class = VendorDetailSerializer

    def get(self, request, vendor_pk=None):
        data = request.data
        vendor = Vendor.objects.get(pk=vendor_pk)
        serializer = self.serializer_class(vendor)

        location_details = {
            'mLatitude': request.GET.get('mLatitude'),
            'mLongitude': request.GET.get('mLongitude')
        }
        superuser_or_city_manager = is_superuser_or_city_manager(
            request.user, location_details
        )
        _dict = {
            'superuser_or_city_manager': superuser_or_city_manager,
            'fleet': serializer.data
        }
        return Response(status=200, data=_dict)
