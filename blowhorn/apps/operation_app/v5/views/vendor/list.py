import logging
from django.db.models import Count, Q, Subquery, OuterRef
from django.db.models.functions import Coalesce
from django.utils import timezone

from rest_framework import views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.apps.operation_app.v5.serializers.vendor import \
    VendorListSerializer
from blowhorn.common.pagination import PaginationMixin
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.address.models import City
from blowhorn.driver.models import Driver
from blowhorn.vehicle.models import Vehicle

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class VendorListView(views.APIView, PaginationMixin):
    """
    To list all vendors in system
    GET <base>/vendors
    Filters: City(str), Status(str), last loaded timestamp(iso-string)
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    serializer_class = VendorListSerializer
    records_per_page = 1000

    def get(self, request):
        params = request.GET
        logger.info('DATA: %s' % params)
        current_page = int(request.GET.get('page', 1))
        qs = self.get_queryset(params)
        data = self.get_pagination_data(qs, current_page)
        results = data.get('results')

        data = {
            'results': self.serializer_class(results, many=True).data,
            'next': data.get('next'),
            'count': data.get('count', 0),
            'last_loaded_on': timezone.now().isoformat()
        }
        return Response(status=200, data=data)

    def get_queryset(self, params):
        if params:
            # city = City.objects.get(name=params.get('operating_city'))
            _params = (Q(preferred_locations__city__name=params.get('operating_city'))
                       | Q(preferred_locations__isnull=True))
            last_loaded_on = params.get('timestamp', None)
            if last_loaded_on:
                _params &= Q(modified_date__gte=last_loaded_on)
            qs = self.serializer_class.Meta.model.objects.filter(_params)
        else:
            qs = self.serializer_class.Meta.model.objects.all()
        qs = qs.prefetch_related('preferred_locations')
        qs = qs.annotate(
            total_drivers_mapped=Coalesce(
                Subquery(
                    Driver.objects.filter(
                        owner_details_id=OuterRef('pk')
                    ).values(
                        'owner_details_id'
                    ).annotate(
                        count=Count('pk')
                    ).values(
                        'count'
                    )
                ), 0),
            total_vehicles_owned=Coalesce(
                Subquery(
                    Vehicle.objects.filter(
                        vendor_id=OuterRef('pk')
                    ).values(
                        'vendor_id'
                    ).annotate(
                        count=Count('pk')
                    ).values(
                        'count'
                    )
                ), 0)
        )
        return qs.order_by('status')
