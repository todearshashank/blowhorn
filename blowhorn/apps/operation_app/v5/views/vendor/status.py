import logging
from django.utils.translation import ugettext_lazy as _

from rest_framework import views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.apps.operation_app.v5.services.vendor import VendorMixin, \
    VendorValidationMixin
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.vehicle.constants import VENDOR_STATUS_ACTIVE
from blowhorn.vehicle.models import Vendor

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class VendorActivationView(views.APIView, VendorValidationMixin, VendorMixin):
    """
    Activate the vendor
    POST <base>/fleetowner/<fleetowner_pk>/activate
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request, vendor_pk=None):
        data = request.data
        vendor = Vendor.objects.get(pk=vendor_pk)
        is_ready, message = self.is_ready_for_activation(vendor)
        if not is_ready:
            return Response(status=400, data=message)

        vendor = self.update_vendor_status(
            vendor, VENDOR_STATUS_ACTIVE, request.user)
        return Response(status=200, data=_('Vendor activated successfully'))
