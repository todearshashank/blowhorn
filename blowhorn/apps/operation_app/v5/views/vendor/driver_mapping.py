import logging
from django.utils.translation import ugettext_lazy as _

from rest_framework import views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.apps.operation_app.v5.constants import DOESNT_HAVE_PERMISSION, UNSETTLED_PAYMENT_EXISTS
from blowhorn.apps.operation_app.v5.serializers.vendor import \
    VendorDriverSerializer
from blowhorn.apps.operation_app.v5.services.driver_mapping import \
    VendorDriverMappingMixin
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.vehicle.models import Vendor
from blowhorn.vehicle.constants import VENDOR_STATUS_INACTIVE
from config.settings import status_pipelines

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class VendorDriverListView(views.APIView):
    """
    To fetch list of drivers mapped to a vendor
    GET <base>/fleetowner/<vendor_pk>/drivers
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    serializer_class = VendorDriverSerializer

    def get(self, request, vendor_pk=None):
        qs = self.serializer_class.get_queryset(vendor_pk=vendor_pk)
        serializer = self.serializer_class(qs, many=True)
        return Response(status=200, data=serializer.data)


class VendorDriverMapView(views.APIView, VendorDriverMappingMixin):
    """
    To map/unmap driver(s) to a vendor
    POST <base>/fleetowner/<vendor_pk>/map/driver
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request, vendor_pk=None):
        data = request.data
        vendor = Vendor.objects.get(pk=vendor_pk)

        if vendor.status == VENDOR_STATUS_INACTIVE:
            return Response(status=400, data=_("Please activate the vendor before mapping drivers"))

        drivers_list = data
        added_driver_pks = []
        deleted_driver_pks = []
        unsettled_payments = []
        city_permission_error = []

        for i in drivers_list:
            if i.get('flag') in ['added', 'deleted']:
                ids = i.get('id', None)
                status_check, validated_data = self.validate_data(ids, request.user)
                if status_check and validated_data:
                    if status_check == DOESNT_HAVE_PERMISSION:
                        city_permission_error.append(validated_data)
                    if status_check == UNSETTLED_PAYMENT_EXISTS:
                        unsettled_payments.append(validated_data)

            if i.get('flag') == 'added':
                ids = i.get('id', None)
                added_driver_pks.append(ids)
            elif i.get('flag') == 'deleted':
                ids = i.get('id', None)
                deleted_driver_pks.append(ids)

        if city_permission_error:
            return Response(status=400, data=_(
                "Only operation manager or superuser can map/unmap {}".format(
                    ' ,'.join([x for x in city_permission_error]))))

        if unsettled_payments:
            return Response(status=400, data=_("Driver {} has unsettled payments, settle the payments "
                                               "to map/unmap fleet owner".format(' ,'.join([x for x in unsettled_payments]))))

        mapped_count = 0
        unmapped_count = 0

        if len(added_driver_pks):
            mapped_count = self.map_existing_driver_to_vendor(
                added_driver_pks, vendor, request.user)

        if len(deleted_driver_pks):
            unmapped_count = self.unmap(deleted_driver_pks, vendor, request.user)
        # else:
        #     driver = self.(data, vendor, request.user)
        if mapped_count == 0 and unmapped_count == 0:
            return Response(status=200)
        return Response(
            status=200, data=_('%s Driver(s) mapped %s Driver(s) Unmapped' % (mapped_count, unmapped_count)))
