import json
import logging
from django.db.models import Q
from django.utils import timezone
from blowhorn.oscar.core.loading import get_model

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from blowhorn.oscar.core.loading import get_model

from blowhorn.users.constants import NOTIFICATION_STATUS_ACTIVE, FYI
from blowhorn.users.submodels.notification import AppNotification
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.apps.operation_app.v5.services.order import OrderDetails
from blowhorn.apps.operation_app.v5.services.user import HandoverItemsMixin
from blowhorn.address.utils import get_hubs_for_user


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Trip = get_model('trip', 'Trip')
Hub = get_model('address', 'Hub')
Driver = get_model('driver', 'Driver')
City = get_model("address", 'City')


class NotificationView(views.APIView, NotificationMixin):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        user = request.user
        results = self.get_queryset()
        response_data = []
        for result in results:
            category = self.get_notification_category(result, user)
            extra_context = json.loads(result.extra_context)

            if category == FYI:
                # Remove redirect url for users for whom FYI
                extra_context.pop('redirect_url', None)

            _dict = {
                'id': result.pk,
                'message': result.message,
                'category': category,
                'notification_group': result.notification_group,
                'created_date': result.created_date.isoformat(),
                'extra_context': extra_context,
            }
            response_data.append(_dict)
        return Response(status=status.HTTP_200_OK, data=response_data)

    def get_queryset(self):
        user = self.request.user
        params = Q(status=NOTIFICATION_STATUS_ACTIVE) & (
                Q(action_owners=user) | Q(broadcast_list=user))
        qs = AppNotification.objects.filter(params)\
            .order_by('-created_date').distinct()
        return qs.prefetch_related('action_owners', 'broadcast_list')


class NotificationOrderDetails(views.APIView):
    """
    Display details for order sent in app notification
    """
    def get(self, request, order_id):

        try:
            data = request.query_params
            operating_city = data.get('operating_city')
            notification_id = data.get('notification_id')
            city = City.objects.filter(name=operating_city,
                                       supply_users=request.user).first()
            """
            for supply users, only show orders marked as contingency or if order has no driver assigned
            """
            params = Q(pk=order_id)
            if city:
                params = params & (
                        Q(marked_for_contingency=True) | Q(driver__isnull=True))
            order = Order.objects.filter(params).first()

        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order id')

        if not order:
            #Expire notification as it is no more valid
            if notification_id:
                NotificationMixin().deactivate_notification(notification_id)
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Driver is already assigned to order')

        order_data = {}
        order_data['orders'] = OrderDetails(user=request.user).get_order_details(order)
        order_data[
            'contingency_reasons'] = OrderDetails().get_contingency_reasons()

        return Response(status=status.HTTP_200_OK, data=order_data)


class ListHub(views.APIView):
    """
    returns list of hubs for a given user
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):

        user = request.user
        data = request.query_params
        city_name = data.get('city_name')

        hubs = get_hubs_for_user(user)
        if not hubs:
            hubs = self.get_hubs_for_city(city_name)

        return Response(status=status.HTTP_200_OK, data=hubs)

    def get_hubs_for_city(self, city_name):
        """
        fetch all hubs for the given city
        """
        return Hub.objects.filter(city__name=city_name).values('id', 'name')


class DriverHandoverItems(views.APIView, HandoverItemsMixin):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    """
    Get list of items to be collected at a given hub for a driver. It can be
    packages that were undelivered/returned, or orders moving from one hub to
    another.It also returns cod amount if any with driver.
    """
    def get(self, request):

        orders, amount = None, None
        try:
            data = request.query_params
            self._initialize_attributes(data)
            if self.error_msg:
                return Response(status=self.status_code, data=self.error_msg)

            orders, amount = self.get_order_for_hub()
            self.response_data = {
                'orders': orders,
                'amount': amount,
                'driver_id': self.driver.id,
                'driver_name': self.driver.name
            }
        except Exception as e:
            self.status_code = status.HTTP_400_BAD_REQUEST
            self.error_msg = e.args[0]

        if not orders and not amount:
            self.status_code = status.HTTP_400_BAD_REQUEST
            self.error_msg = 'No orders or cash to be collected from driver'

        return Response(status=self.status_code,
                        data=self.error_msg or self.response_data)

    """
    Receive orders to hub. Clear driver cod balance if there is cash
    to be collected.
    """
    def post(self, request):
        try:
            data = request.data
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid request')

        user = request.user
        ret_status, message = self.receive_items(data, user)

        return Response(status=ret_status, data=message)
