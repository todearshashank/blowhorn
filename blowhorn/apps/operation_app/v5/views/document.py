from django import db
from django.db.models import Q
from django.utils import timezone

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.document.constants import VERIFICATION_PENDING, ACTIVE, REJECT, DOCUMENT_RC
from blowhorn.document.models import DocumentType, DocumentArchive, \
    DocumentEvent, mark_other_doc_inactive
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT
from config.settings import status_pipelines as StatusPipeline
from blowhorn.document.models import save_archive
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.apps.operation_app.v5.constants import COSMOS_SCREEN_URLS, \
    APPROVED, DISAPPROVED, NOTIFICATION_GROUP_INTERNAL_VERIFICATION, \
    NOTIFICATION_TEMPLATES


class ActivateDocument(views.APIView):
    """
        activate document
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request, pk=None):
        data = request.data
        query = Q(pk=pk)
        qs = DocumentEvent.copy_data.filter(query)
        qs = qs.prefetch_related('driver_document', 'vehicle_document',
                                 'vendor_document')
        qs = qs.select_related('action_owner')
        doc = qs.first()

        if doc and doc.status == VERIFICATION_PENDING:
            from blowhorn.document.models import is_doc_number_exist
            record = is_doc_number_exist(doc)
            if record and record.exists():
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        'status': False,
                        'message': 'Document Number exists check %s' % record},
                    content_type='application/json; charset=utf-8')
            archive = DocumentArchive(new_status=ACTIVE,
                                      action_owner_id=doc.action_owner_id,
                                      old_status=VERIFICATION_PENDING,
                                      document_event=doc)
            save_archive(doc, archive, ACTIVE)
            doc.status = ACTIVE
            doc.save()
            mark_other_doc_inactive(doc)
            subject = None
            doc_type = None

            if doc.driver_document_id:
                subject = doc.driver_document.driver
                doc_type = doc.driver_document.document_type
            elif doc.vehicle_document_id:
                subject = doc.vehicle_document.vehicle
                doc_type = doc.vehicle_document.document_type
            elif doc.vendor_document_id:
                subject = doc.vendor_document
                doc_type = doc.vendor_document.document_type

            extra_context = {
                'subject': '%s' % subject,
                'document_type': '%s' % doc_type,
                'action_by': request.user.name or request.user.email,
                'redirect_url': COSMOS_SCREEN_URLS.get(
                    NOTIFICATION_GROUP_INTERNAL_VERIFICATION),
            }
            message_template = NOTIFICATION_TEMPLATES.get(
                NOTIFICATION_GROUP_INTERNAL_VERIFICATION, {}).get(APPROVED)
            NotificationMixin().save_and_broadcast_notification(
                action_owners=[],
                broadcast_list=[doc.created_by.pk],
                action_by=request.user,
                action=APPROVED,
                extra_context=extra_context,
                notification_group=NOTIFICATION_GROUP_INTERNAL_VERIFICATION,
                message=message_template.format(
                    subject=extra_context.get('subject'),
                    action_owner=extra_context.get('action_by'),
                    action=APPROVED,
                    document_type=doc_type,
                ),
                old_notification_pk=data.get('notification_pk', None)
            )
            return Response(status=status.HTTP_200_OK,
                            data={
                                'status': True,
                                'message': 'Document activated.'
                            },
                            content_type='application/json; charset=utf-8')
        else:
            return Response(status=status.HTTP_200_OK,
                            data={
                                'status': False,
                                'message': 'Document status changed.'
                                           'Please reload.'
                            },
                            content_type='application/json; charset=utf-8')


class RejectDocument(views.APIView):
    """
        reject document
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def put(self, request, pk=None):
        if not request.data.get('reason'):
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'status': False,
                    'message': 'Rejection reason is mandatory.'
                },
                content_type='application/json; charset=utf-8')

        query = Q(pk=pk)
        qs = DocumentEvent.copy_data.filter(query)
        qs = qs.prefetch_related('driver_document', 'vehicle_document',
                                 'vendor_document')
        qs = qs.select_related('action_owner')
        doc = qs.first()
        driver = None
        if doc and doc.status == VERIFICATION_PENDING:
            archive = DocumentArchive(new_status=REJECT,
                                      action_owner_id=doc.action_owner_id,
                                      old_status=VERIFICATION_PENDING,
                                      document_event=doc)

            if doc.driver_document:
                driver = doc.driver_document.driver
                archive.driver_document_id = doc.driver_document_id
                doc.driver_document.status = REJECT
                doc.driver_document.save()

            elif doc.vehicle_document:
                archive.vehicle_document_id = doc.vehicle_document_id
                doc.vehicle_document.status = REJECT

                # Remove the driver vehicle mapping is rc is rejected
                if doc.vehicle_document and doc.vehicle_document.document_type.code == DOCUMENT_RC:
                    from blowhorn.integration_apps.invoid.driver_mixin import DriverAddUpdateMixin
                    DriverAddUpdateMixin().delete_drivervehicle_mapping(doc.vehicle_document.vehicle,
                                                doc.action_owner)

                doc.vehicle_document.save()

            elif doc.vendor_document_id:
                driver = None
                archive.vendor_document_id = doc.vendor_document_id
                doc.vendor_document.status = REJECT
                doc.vendor_document.save()

            if not doc.vendor_document_id and driver:
                from blowhorn.driver.util import get_driver_missing_doc
                mandatory_driver_doc_type = DocumentType.objects.filter(
                    is_document_mandatory=True,
                    identifier=DRIVER_DOCUMENT)
                mandatory_vehicle_doc_type = DocumentType.objects.filter(
                    is_document_mandatory=True,
                    identifier=VEHICLE_DOCUMENT)

                missing_doc = get_driver_missing_doc(
                    driver, mandatory_driver_doc_type,
                    mandatory_vehicle_doc_type
                )
                if missing_doc or driver.status == StatusPipeline.SUPPLY:
                    driver.status = StatusPipeline.ONBOARDING
                    driver.save()

            archive.reason = request.data.get('reason')
            archive.save()
            doc.status = REJECT
            doc.save()

            subject, doc_type, driver_id = None, None, None
            if doc.driver_document_id:
                driver_document = doc.driver_document
                subject = driver_document.driver
                doc_type = driver_document.document_type
            elif doc.vehicle_document_id:
                subject = doc.vehicle_document.vehicle
                doc_type = doc.vehicle_document.document_type
            elif doc.vendor_document_id:
                subject = doc.vendor_document
                doc_type = doc.vendor_document.document_type

            extra_context = {
                'subject': '%s' % subject,
                'document_type': '%s' % doc_type,
                'action_by': request.user.name or request.user.email,
                'redirect_url': COSMOS_SCREEN_URLS.get(
                    NOTIFICATION_GROUP_INTERNAL_VERIFICATION),
                'remarks': request.data.get('reason', None),
            }
            message_template = NOTIFICATION_TEMPLATES.get(
                NOTIFICATION_GROUP_INTERNAL_VERIFICATION, {}).get(DISAPPROVED)
            NotificationMixin().save_and_broadcast_notification(
                action_owners=[],
                broadcast_list=[doc.created_by.pk],
                action_by=request.user,
                action=DISAPPROVED,
                extra_context=extra_context,
                notification_group=NOTIFICATION_GROUP_INTERNAL_VERIFICATION,
                message=message_template.format(
                    subject=extra_context.get('subject'),
                    action_owner=extra_context.get('action_by'),
                    action=DISAPPROVED,
                    reason=extra_context.get('remarks', ''),
                    document_type=doc_type,
                ),
                old_notification_pk=request.data.get('notification_pk', None)
            )

            return Response(
                status=status.HTTP_200_OK,
                data={
                    'status': True,
                    'message': 'Document rejected.'
                },
                content_type='application/json; charset=utf-8')
        else:
            return Response(status=status.HTTP_200_OK,
                            data={
                                'status': False,
                                'message': 'Document status changed.'
                                           'Please reload.'
                            },
                            content_type='application/json; charset=utf-8')
