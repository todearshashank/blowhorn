import logging
import json
from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

from django.db.models import Q, Case, When, Value, CharField
from django.conf import settings
from blowhorn.oscar.core.loading import get_model
from django.utils import timezone
from django.db import transaction

from config.settings import status_pipelines as StatusPipeline
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.utils.functions import utc_to_ist
from blowhorn.address.models import City
from blowhorn.trip.utils import TripService
from blowhorn.order.models import OrderHistory
from blowhorn.trip.models import Trip, TRIP_SUSPENSION_REASONS_VEHICLE_BREAKDOWN, \
    TRIP_SUSPENSION_REASONS_DRIVER_DENIED_DUTY, TRIP_SUSPENSION_REASONS_SICK, \
    TRIP_SUSPENSION_REASONS_FAMILY_ISSUE,\
    TRIP_SUSPENSION_REASONS_TRAFFIC_VIOLATION, \
    TRIP_SUSPENSION_REASONS_NO_FUEL, TRIP_SUSPENSION_REASONS_ACCIDENT, \
    TRIP_SUSPENSION_REASONS_CUSTOMER_CANCELLED,\
    TRIP_SUSPENSION_REASONS_TRIP_INTERRUPTED
from blowhorn.apps.operation_app.v5.constants import REQUESTED, MY_ORDERS, \
    FIXED_ORDERS, TEAM_ORDERS
from blowhorn.apps.operation_app.v5.services.notifications import \
    SendNotification
from blowhorn.apps.operation_app.v5.services.order import OrderDetails

Driver = get_model("driver", "Driver")
Order = get_model("order", "Order")

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ListOrderForUser(views.APIView):
    """
    Fetch order details for for a user in categories :-
    My Orders, Team Orders, and Fixed Orders
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        user = request.user
        if not user:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Could not find user')

        try:
            response_data = OrderDetails(user=user).get_user_orders_data(request)
        except Exception as e:
            message = e.args[0]
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        return Response(status=status.HTTP_200_OK, data=response_data)


class MarkContingency(views.APIView):
    """
    View to mark an order contingency,
    this will suspend trip
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request):
        try:
            data = json.loads(request.body)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Could not parse request')

        order_id = data.get('order_id')
        user = request.user
        contingency_reason = data.get('contingency_reason')
        if not order_id or not contingency_reason:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='invalid data')

        order = Order.objects.filter(id=order_id).first()

        if order:
            with transaction.atomic():
                Order.objects.filter(id=order_id).update(marked_for_contingency=True,
                                                         modified_date=timezone.now(),
                                                         modified_by=user)
                OrderHistory.objects.create(order_id=order.id,
                                            field='driver',
                                            old_value=order.driver,
                                            user=user,
                                            modified_time=timezone.now())
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Order does not exist')

        TripService().create_contingency_trip(order.driver, order,
                                              suspension_trip_reason=contingency_reason,
                                              mark_contingency=True)

        SendNotification(REQUESTED, request.user, order).notify_users()

        return Response(status=status.HTTP_200_OK)


class CancelOrder(views.APIView):
    """
    view to cancel spot orders
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request):
        print(request.body)
        try:
            data = json.loads(request.body)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='invalid data')

        order_id = data.get('order_id')
        user = request.user
        cancellation_reason = data.get('cancellation_reason')
        if not order_id or not cancellation_reason:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='invalid data')

        order = Order.objects.filter(id=order_id)

        with transaction.atomic():
            if order.exists():
                Order.objects.filter(id=order_id).update(status=StatusPipeline.ORDER_CANCELLED,
                                                         reason_for_cancellation=cancellation_reason,
                                                         modified_date=timezone.now(),
                                                         modified_by=user)

            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Order does not exist')

            Trip.objects.filter(order_id=order_id).update(status=StatusPipeline.TRIP_CANCELLED,
                                                          modified_date=timezone.now(),
                                                          modified_by=user)

        return Response(status=status.HTTP_200_OK)
