# Django and system libraries
import json
import logging
from datetime import date, timedelta, datetime
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


# 3rd party libraries
from rest_framework import status, exceptions, generics, views
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import permission_classes
from blowhorn.oscar.core.loading import get_model

# Blowhorn Imports
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.driver.models import DriverPayment, DriverPaymentAdjustment, DriverCashPaid,\
    WITHHELD, UNAPPROVED, APPROVED, SUSPENDED, CLOSED
from blowhorn.contract.models import Contract
from blowhorn.address.models import City, State
from blowhorn.driver.constants import UNAPPROVED_DEACTIVATION_PERIOD, PAYMENT_TYPE_FIXED, \
    PAYMENT_TYPE_BONUS, PAGE_TO_BE_DISPLAYED, NUMBER_OF_ENTRY_IN_PAGE
from blowhorn.apps.operation_app.v5.services.driver_payment import DriverPaymentUtils, \
    adjustment_date_check
from blowhorn.apps.operation_app.v5.constants import DRIVER_PAYMENT_STATUS_MAPPING, \
    ADJUSTMENT_REASONS, ADJUSTMENT_CASHPAID_STATUS_MAPPING, COSMOS_SCREEN_URLS, \
    DRIVER_PAYMENT_ADJUSTMENT_URL, DRIVER_PAYMENT_CASH_PAID_URL, NOTIFICATION_TEMPLATES
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.users.constants import NOTIFICATION_GROUP_DRIVER_PAYMENT


DRIVER_PAYMENT_STATUSES = [UNAPPROVED, APPROVED, CLOSED, SUSPENDED, WITHHELD]
ADDED = _('Added')


class DriverPaymentFilterData(views.APIView):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        payment_type = [PAYMENT_TYPE_FIXED, PAYMENT_TYPE_BONUS]
        divisions = list(Contract.objects.select_related('division').values_list('division__name',
                                                                                 flat=True).exclude(
            division__isnull=True).distinct())
        cities = list(City.objects.values_list('name', flat=True))
        states = list(State.objects.values_list('name', flat=True))

        expiry_days = [
            ('0', 'expired'),
            ('1&5', '1 to 5'),
            ('6&10', '6 to 10'),
            ('11&15', '11 to 15'),
            ('16', 'greater than 15')
        ]

        response_data = {
            'payment_type': payment_type,
            'divisions': divisions,
            'cities': cities,
            'states': states,
            'expiry_days': expiry_days,
            'payment_statuses': DRIVER_PAYMENT_STATUSES,
            'payment_status_mapping': DRIVER_PAYMENT_STATUS_MAPPING,
            'adjustment_categories': ADJUSTMENT_REASONS,
            'adjustment_cashpaid_status_mapping': ADJUSTMENT_CASHPAID_STATUS_MAPPING
        }
        return Response(status=status.HTTP_200_OK, data=response_data)


class DriverPaymentListView(views.APIView):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get_paginated_data(self, queryset, page, num):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else objects.previous_page_number()
        )
        next = 0 if not objects.has_next() else objects.next_page_number()

        # pages_count = paginator.num_pages
        data = {
            "count": count,
            "previous": previous,
            "next": next,
            "objects": objects.object_list,
            "last_loaded_on": timezone.now().isoformat()
        }

        return data

    def get_queryset(self, query, query_params):
        qs = DriverPayment.objects.filter(query).distinct()
        qs = qs.select_related('driver', 'contract', 'contract__customer')
        qs = qs.order_by('-id')
        page_to_be_displayed = (
            query_params.get("next")
            if query_params.get("next", "") and int(query_params["next"]) > 0
            else PAGE_TO_BE_DISPLAYED
        )

        number_of_entry = NUMBER_OF_ENTRY_IN_PAGE
        pagination_data = self.get_paginated_data(
            qs, page_to_be_displayed, number_of_entry
        )

        qs = pagination_data.pop("objects")
        qs = qs.values('id', 'driver__name', 'status', 'contract__customer__name', 'net_pay',
                       'contract__contract_type', 'payment_type', 'is_settled', 'is_stale',
                       'driver__driver_vehicle', 'start_datetime', 'end_datetime')
        return {'payments_list': qs, 'pagination_data': pagination_data}

    def get_query(self, params):
        query = Q()
        from_start_date = params.get('from_start_date', None)
        from_end_date = params.get('from_end_date', None)
        until_start_date = params.get('until_start_date', None)
        until_end_date = params.get('until_end_date', None)
        payment_type = params.get('payment_type', None)
        expiry_days = params.get('expiry_days', None)
        has_contingency = params.get('has_contingency', None)
        division = params.get('params', None)
        city = params.get('params', None)
        state = params.get('state', None)
        is_settled = params.get('params', None)
        payment_status = params.get('status', None)

        if from_start_date:
            start_date = datetime.strptime(from_start_date, '%d-%m-%Y').date()
            end_date = datetime.strptime(
                from_end_date, '%d-%m-%Y').date() if from_end_date else timezone.now().date()
            query &= (Q(start_datetime__gte=start_date) & Q(start_datetime__lt=end_date))
        if until_start_date:
            start_date = datetime.strptime(until_start_date, '%d-%m-%Y').date()
            end_date = datetime.strptime(
                until_end_date, '%d-%m-%Y').date() if until_end_date else timezone.now().date()
            query &= (Q(end_datetime__gte=start_date) & Q(end_datetime__lt=end_date))
        if payment_type:
            query &= Q(payment_type=payment_type)
        if has_contingency:
            query &= Q(paymenttrip__trip__is_contingency=has_contingency)
        if division:
            query &= Q(contract__division__name=division)
        if city:
            query &= Q(contract__city__name=city)
        if is_settled:
            query &= Q(is_settled=is_settled)
        if expiry_days:
            _statuses = [UNAPPROVED, SUSPENDED]
            query &= Q(status__in=_statuses)
            val = [int(x) for x in expiry_days.split('&')]

            if len(val) == 1:
                limit = date.today() + timedelta(days=val[0]) - timedelta(
                    days=UNAPPROVED_DEACTIVATION_PERIOD)

                if val[0] <= 0:
                    query &= Q(end_datetime__lte=limit)

                elif len(val) == 1 and val[0] > 0:
                    query &= (Q(end_datetime__gte=limit) & Q(end_datetime__lte=date.today()))
            else:
                start = date.today() + timedelta(days=val[0]) - timedelta(
                    days=UNAPPROVED_DEACTIVATION_PERIOD)
                end = date.today() + timedelta(days=val[1]) - timedelta(
                    days=UNAPPROVED_DEACTIVATION_PERIOD)
                query &= (Q(end_datetime__gte=start) & Q(end_datetime__lte=end))
        if payment_status:
            query &= Q(status=payment_status)
        else:
            query &= Q(status__in=DRIVER_PAYMENT_STATUSES)

        if not (from_start_date or until_start_date):
            today = timezone.now().date()
            start_date = date(year=today.year, month=today.month, day=1)

            query &= Q(start_datetime__gte=start_date)

        if state:
            query &= Q(contract__city__state__name=state)

        return query

    def get(self, request):
        user = request.user
        query_params = request.query_params

        query = self.get_query(query_params)

        if not (user.is_superuser or user.is_city_manager):
            query &= (Q(contract__spocs=user) | Q(contract__supervisors=user))

        data = self.get_queryset(query, query_params)

        payments = list(data.get('payments_list'))

        for driver_payment in payments:
            if driver_payment['status'] in [APPROVED, WITHHELD, CLOSED]:
                driver_payment['will_expire_in'] = None
            else:
                difference = (driver_payment['end_datetime'] + timedelta(
                    days=UNAPPROVED_DEACTIVATION_PERIOD)) - date.today()
                driver_payment['will_expire_in'] = difference.days

        response_data = {
            'payments_list': payments,
            'pagination_data': data.get('pagination_data')
        }

        return Response(status=status.HTTP_200_OK, data=response_data)


class DriverPaymentDetailView(views.APIView, DriverPaymentUtils):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request, payment_id):
        driver_payment = DriverPayment.objects.filter(id=payment_id).first()
        data = request.GET.dict()

        if not driver_payment:
            return Response(status=status.HTTP_200_OK, data=_(
                'Driver Payment id %s does not exist. It might have been deleted!') % payment_id)

        tab = data.get('tab', None)
        data = self.payment_detail_info_mapping[tab](self, tab, payment_id, request.user)
        return Response(status=status.HTTP_200_OK, data=data)


class UpdateDriverPayment(views.APIView, DriverPaymentUtils):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request, payment_id):
        try:
            data = request.data.dict()
        except:
            data = request.data

        user = request.user
        action = data.get('action', None)
        success, message = self.status_update_action_mapping[action](self, payment_id, user)
        if not success:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=str(message))
        return Response(status=status.HTTP_200_OK, data=message)


class AddDriverPaymentAdjustment(views.APIView):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request):
        data = request.data
        user = request.user

        payment_id = data.get('payment_id', None)
        category = data.get('category', None)
        description = data.get('description', None)
        adjustment_amount = data.get('adjustment_amount', None)
        adjustment_date = data.get('adjustment_date', None)

        payment_obj = DriverPayment.objects.get(pk=payment_id)

        adjustment_datetime = datetime.strptime(
            adjustment_date, "%d-%m-%Y").date()
        error, _message = adjustment_date_check(payment_obj, adjustment_datetime)
        if error:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_message)

        user_to_be_notified = []

        extra_context = {
            'payment_id': str(payment_id),
            'action_by': user.email,
            'redirect_url': COSMOS_SCREEN_URLS.get(DRIVER_PAYMENT_CASH_PAID_URL) % payment_id,
            'remarks': ''
        }

        adjustment_obj = DriverPaymentAdjustment(payment_id=payment_id,
                                                 driver=payment_obj.driver,
                                                 contract=payment_obj.contract,
                                                 category=category,
                                                 description=description,
                                                 adjustment_amount=adjustment_amount,
                                                 adjustment_datetime=adjustment_datetime)
        adjustment_obj.save()

        # Saving created by separately as it is not getting saved in the above query
        adjustment_obj.created_by = user
        adjustment_obj.save()

        user_to_be_notified.append(adjustment_obj.action_owner.id)
        message = NOTIFICATION_TEMPLATES.get(NOTIFICATION_GROUP_DRIVER_PAYMENT).get(ADDED).format(
            user=user.name,
            type='adjustment',
            instance_id=adjustment_obj.pk,
            payment_id=payment_id,
            driver=payment_obj.driver,
            contract=payment_obj.contract.name
        )

        NotificationMixin().save_and_broadcast_notification(
            action_owners=user_to_be_notified,
            action_by=user,
            extra_context=extra_context,
            notification_group=NOTIFICATION_GROUP_DRIVER_PAYMENT,
            message=message
        )
        return Response(status=status.HTTP_200_OK,
                        data=_('Adjustment for payment %s added successfully') % payment_id)


class AddDriverCashPaid(views.APIView):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request):
        data = request.data.dict()
        user = request.user

        payment_id = data.get('payment_id', None)
        cash_paid_datetime = data.get('cash_paid_date', None)
        description = data.get('description', None)
        cash_paid = data.get('cash_paid', None)
        proof = data.get('proof', None)

        payment_obj = DriverPayment.objects.get(pk=payment_id)

        cash_paid_datetime = datetime.strptime(
            cash_paid_datetime, "%d-%m-%Y").date()
        error, _message = adjustment_date_check(payment_obj, cash_paid_datetime)
        if error:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_message)

        broadcast_list = [x.id for x in payment_obj.contract.supervisors.all()]

        extra_context = {
            'payment_id': str(payment_id),
            'action_by': user.name,
            'redirect_url': COSMOS_SCREEN_URLS.get(DRIVER_PAYMENT_CASH_PAID_URL) % payment_id,
            'remarks': ''
        }

        cash_paid_obj = DriverCashPaid(payment_id=payment_id,
                                       driver=payment_obj.driver,
                                       contract=payment_obj.contract,
                                       cash_paid_datetime=cash_paid_datetime,
                                       description=description, cash_paid=float(cash_paid),
                                       proof=proof)
        cash_paid_obj.save()

        # Saving created by separately as it is not getting saved in the above query
        cash_paid_obj.created_by = user
        cash_paid_obj.save()

        message = NOTIFICATION_TEMPLATES.get(NOTIFICATION_GROUP_DRIVER_PAYMENT).get(ADDED).format(
            user=user.name,
            type='cash paid',
            instance_id=cash_paid_obj.pk,
            payment_id=payment_id,
            driver=payment_obj.driver,
            contract=payment_obj.contract.name
        )

        NotificationMixin().save_and_broadcast_notification(
            action_owners=broadcast_list,
            action_by=user,
            extra_context=extra_context,
            notification_group=NOTIFICATION_GROUP_DRIVER_PAYMENT,
            message=message
        )

        return Response(status=status.HTTP_200_OK,
                        data=_('Added Cash Paid for payment %s') % payment_id)


class UpdateDriverPaymentAdjustment(views.APIView, DriverPaymentUtils):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request, adjustment_id):
        data = request.data
        user = request.user
        action = data.get('action')
        remarks = data.get('remarks', None)

        success, message, adjustment_obj = self._adjustments_action_mapping[action](self,
                                                                                    adjustment_id,
                                                                                    user,
                                                                                    remarks)
        if not success:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        users_to_be_notified = [x.id for x in adjustment_obj.payment.contract.supervisors.all()]
        payment_id = adjustment_obj.payment.id
        extra_context = {
            'payment_id': str(payment_id),
            'action_by': user.name,
            'redirect_url': COSMOS_SCREEN_URLS.get(DRIVER_PAYMENT_ADJUSTMENT_URL) % payment_id,
            'remarks': ''
        }

        message_ = NOTIFICATION_TEMPLATES.get(NOTIFICATION_GROUP_DRIVER_PAYMENT).get(
            action).format(
            user=user.name,
            type='adjustment',
            instance_id=adjustment_obj.pk,
            payment_id=payment_id,
            driver=adjustment_obj.driver,
            contract=adjustment_obj.contract.name
        )

        NotificationMixin().save_and_broadcast_notification(
            action_owners=[adjustment_obj.created_by.id],
            broadcast_list=users_to_be_notified,
            action_by=user,
            extra_context=extra_context,
            notification_group=NOTIFICATION_GROUP_DRIVER_PAYMENT,
            message=message_
        )
        return Response(status=status.HTTP_200_OK, data=message)


class UpdateDriverCashPaid(views.APIView, DriverPaymentUtils):

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request, cash_paid_id):
        data = request.data
        user = request.user
        action = data.get('action')

        success, message, cash_paid_obj = self._cash_paid_action_mapping[action](self, cash_paid_id,
                                                                                 user)
        if not success:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        users_to_be_notified = [x.id for x in cash_paid_obj.payment.contract.supervisors.all()]
        payment_id = cash_paid_obj.payment.id
        extra_context = {
            'payment_id': str(payment_id),
            'action_by': user.name,
            'redirect_url': COSMOS_SCREEN_URLS.get(DRIVER_PAYMENT_CASH_PAID_URL) % payment_id,
            'remarks': ''
        }

        message_ = NOTIFICATION_TEMPLATES.get(NOTIFICATION_GROUP_DRIVER_PAYMENT).get(
            action).format(
            user=user.name,
            type='cash paid',
            instance_id=cash_paid_obj.pk,
            payment_id=payment_id,
            driver=cash_paid_obj.driver,
            contract=cash_paid_obj.contract.name
        )

        NotificationMixin().save_and_broadcast_notification(
            action_owners=[cash_paid_obj.created_by.id],
            broadcast_list=users_to_be_notified,
            action_by=user,
            extra_context=extra_context,
            notification_group=NOTIFICATION_GROUP_DRIVER_PAYMENT,
            message=message_
        )
        return Response(status=status.HTTP_200_OK, data=message)
