# Django and system libraries
import json
import logging
from datetime import datetime
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from django.conf import settings


# 3rd party libraries
from rest_framework import status, exceptions, generics
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import permission_classes
from blowhorn.oscar.core.loading import get_model

# Blowhorn Imports
# from blowhorn.customer.submodels.invoice_models import CustomerInvoice
from blowhorn.customer import constants as customer_constants
from blowhorn.customer.utils import CustomerUtils
from blowhorn.customer.submodels.invoice_models import generate_invoice
from blowhorn.customer.utils import abbr_to_num
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.apps.operation_app.v5.services.customer import CustomerUtils
from blowhorn.apps.operation_app.v5.constants import INVOICE_STATUS_CHANGE_MAPPING

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

CustomerInvoice = get_model('customer', 'CustomerInvoice')
Company = get_model('company', 'Company')
City = get_model('address', 'City')
State = get_model('address', 'State')
ServiceHSNCodeMap = get_model('contract', 'ServiceHSNCodeMap')

INVOICE_STATUSES = [customer_constants.CREATED, customer_constants.REJECTED,
                    customer_constants.SENT_FOR_APPROVAL,
                    customer_constants.APPROVED, customer_constants.SENT_TO_CUSTOMER,
                    customer_constants.CUSTOMER_ACKNOWLEDGED,
                    customer_constants.CUSTOMER_DISPUTED, customer_constants.CUSTOMER_CONFIRMED,
                    customer_constants.PARTIALLY_PAID, customer_constants.PAYMENT_DONE
                    ]


class InvoiceFilterList(APIView, CustomerUtils):
    """
    API to send data used for filtering invoices on the client side
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        if not request.user.is_sales_representative(city=None):
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Unauthorized user!'))

        sales_rep = list(
            City.objects.values_list('sales_representatives__email', flat=True).distinct().exclude(
                sales_representatives=None))
        executives = list(Company.objects.exclude(executives__email__isnull=True
                                                  ).values_list('executives__email', flat=True))

        states = list(State.objects.values_list('name', flat=True))

        divisions = list(CustomerInvoice.objects.prefetch_related(
            'contract_invoices__contract__division').values_list(
            'contract_invoices__contract__division__name', flat=True).distinct().exclude(
            contract_invoices__contract__division=None))
        service_period = self._get_field_lookups('service_period_end')[0]
        month, year = service_period.split('-')
        query = Q(service_period_end__month=abbr_to_num.get(
            month, None), service_period_end__year=year)

        customers = list(CustomerInvoice.objects.filter(query).values_list('customer__name',
                                                                           flat=True).distinct())

        hsn_codes_list = []
        for i in ServiceHSNCodeMap.objects.iterator():
            hsn_codes_list.append(
                {
                    'id': i.id,
                    'hsn_code': '%s - %s' % (i.service_tax_category, i.hsn_code)
                }
            )

        response_data = {
            'sales_reps': sales_rep,
            'executives': executives,
            'status_mapping': INVOICE_STATUS_CHANGE_MAPPING,
            'invoice_statuses': INVOICE_STATUSES,
            'invoice_types': [customer_constants.DEBIT_NOTE, customer_constants.CREDIT_NOTE,
                              customer_constants.TAX_INVOICE],
            'states': states,
            'divisions': divisions,
            'customers': customers,
            'hsn_codes': hsn_codes_list,
            'revenue_booked_on': self._get_field_lookups('revenue_booked_on'),
            'service_period_end': self._get_field_lookups('service_period_end')
        }

        return Response(status=status.HTTP_200_OK, data=response_data)


class CustomerList(APIView, CustomerUtils):
    """
    API to retrieve list of customers
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        params = request.query_params

        if not request.user.is_sales_representative(city=None):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('You do not have permission to view this'))

        query = self._get_query(params, request)
        response_data = self._get_customers(query)

        return Response(status=status.HTTP_200_OK, data=response_data)


class CustomerInvoiceList(APIView, CustomerUtils):
    """
    API to retrieve customer wise invoices
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        query_params = request.query_params
        customer_id = query_params.get('customer_id', None)

        if not customer_id:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Please select a customer'))

        if not request.user.is_sales_representative(city=None):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('You do not have permission to view this'))

        query = self._get_query(query_params, request)
        if query:
            response_data = self._get_customerwise_invoices(query)
            return Response(status=status.HTTP_200_OK, data=response_data)

        return Response(status=status.HTTP_400_BAD_REQUEST, data=_('No invoices to show'))


class UpdateInvoice(APIView, CustomerUtils):
    """
    Update invoice based on the action selected on cosmos
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request):
        try:
            data = request.data.dict()
        except:
            data = request.data
        logger.info('Request from the app is : %s' % data)
        action = data.get('action', None)
        if action:
            success, message = self.invoice_action_mapping[action](self, data, request)
            if success:
                return Response(status=status.HTTP_200_OK, data=message)
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Requested action Failed!'))


class CreditNoteView(APIView, CustomerUtils):
    """
    Create Credit Note
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request):
        data = request.data
        invoice_type = customer_constants.CREDIT_NOTE
        related_invoice = data.get('related_invoice', None)
        invoice_date = data.get('invoice_date', None)
        base_pay = data.get('base_pay', None)
        hsn_code = data.get('hsn_code', None)
        invoice_status = customer_constants.CREATED
        comments = data.get('comments', None)
        remarks = data.get('remarks', None)

        if not related_invoice:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Related invoice is mandatory for creating credit note'))

        if base_pay >= 0:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Base pay should be negative for credit note'))

        invoice_date = datetime.strptime(invoice_date, '%d-%m-%Y').date()
        hsn_code = ServiceHSNCodeMap.objects.filter(id=hsn_code).first()
        related_invoice = CustomerInvoice.objects.filter(invoice_number=related_invoice).first()
        invoice = CustomerInvoice(
            invoice_type=invoice_type,
            related_invoice=related_invoice,
            invoice_date=invoice_date,
            charges_basic=base_pay,
            hs_code=hsn_code,
            status=invoice_status,
            comments=comments,
            remarks=remarks,
            is_credit_debit_note=True
        )
        invoice.created_by = request.user
        if invoice:
            generate_invoice(invoice)
            return Response(status=status.HTTP_200_OK, data=_('Credit Note raised successfully'))

        return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Credit Note request failed'))


class RelatedInvoiceList(APIView):
    """
        List of CUSTOMER CONFIRMED, PARTIALLY PAID invoices for adding credit note
    """

    permission_classes = (IsBlowhornStaff, IsAuthenticated)

    def get(self, request):
        invoices = list(CustomerInvoice.objects.filter(is_credit_debit_note=False,
                                                       status__in=[
                                                           customer_constants.CUSTOMER_CONFIRMED,
                                                           customer_constants.PARTIALLY_PAID
                                                       ]).values_list('invoice_number', flat=True))
        return Response(status=status.HTTP_200_OK, data=invoices)


class InvoiceSpocList(APIView):
    """
        Retrieve list of spocs and supervisors for an invoice
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        data = request.data
        invoice_number = data.get('invoice_number', None)
        invoice = CustomerInvoice.objects.filter(invoice_number__in=invoice_number).first()

        if not invoice:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Invoice not found'))

        if invoice.is_credit_debit_note:
            users = CustomerUtils().get_company_executives()
        else:
            users = CustomerUtils().get_sales_representatives(invoice)

        return Response(status=status.HTTP_200_OK, data=list(users))
