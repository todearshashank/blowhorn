import json
import logging
from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from blowhorn.apps.operation_app.v5.services.driver import \
    DriverBackGroundVerificationMixin as BGVMixin
from blowhorn.apps.operation_app.v3.services.driver import *
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.apps.operation_app.v5.constants import COSMOS_SCREEN_URLS, \
    REQUESTED, NOTIFICATION_GROUP_EXTERNAL_VERIFICATION, NOTIFICATION_TEMPLATES
from blowhorn.oscar.core.loading import get_model

Driver = get_model("driver", "Driver")
DriverVehicleMap = get_model("driver", "DriverVehicleMap")
DriverDocument = get_model("driver", "DriverDocument")
DriverPreferredLocation = get_model("driver", "DriverPreferredLocation")
ResourceAllocation = get_model("contract", "ResourceAllocation")
State = get_model("address", "State")

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class BackgroundVerificationView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            data = json.loads(request.body)
            driver_details = data.get("driver_details", {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get("driver_details", "{}"))
            logging.info("Request from the app dict : " +
                         str(request.data.dict()))
        user = request.user
        response_dict = {"status": False, "message": ""}

        logging.info("This is the data for verification of driver : %s" % data)
        if not driver_details.get("id", None):
            response_dict["message"] = "No Driver was provided"
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response_dict)

        driver_obj = Driver.objects.filter(id=driver_details["id"])
        if not driver_obj.first():
            response_dict["message"] = "No Driver Found"
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response_dict)

        city = driver_obj.first().operating_city
        try:
            response = BGVMixin().background_verification_processing(
                user=user,
                city=city,
                driver_obj=driver_obj,
                driver_details=driver_details,
                request=request,
            )
            return Response(status=status.HTTP_200_OK, data=response)
        except ValidationError as e:
            response_dict["message"] = e
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response_dict)
