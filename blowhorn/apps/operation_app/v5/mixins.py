import json
import logging

from blowhorn.apps.operation_app.v5.constants import NOTIFICATION_TEMPLATES, \
    REQUESTED
from blowhorn.apps.operation_app.v5.tasks import push_notification_to_cosmos
from blowhorn.users.constants import FYI, FYA, NOTIFICATION_STATUS_EXPIRED
from blowhorn.users.submodels.notification import AppNotification

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class NotificationMixin(object):

    def save_notification(self, data):
        action_owners = data.pop('action_owners', None)
        broadcast_list = data.pop('broadcast_list', None)
        instance = AppNotification(**data)
        try:
            instance.save()
            if action_owners:
                instance.action_owners.set(action_owners)
            if broadcast_list:
                instance.broadcast_list.set(broadcast_list)
        except BaseException as be:
            logger.info('Failed to create notification: %s' % data)
            logger.info('Error: %s' % be.args[0])

        return instance

    def get_notification_category(self, obj, user):
        action_owners = obj.action_owners.all()
        if user in action_owners:
            return FYA
        else:
            return FYI

    def get_notification_template(self, action, notification_group):
        return NOTIFICATION_TEMPLATES.get(notification_group, {}).get(
            action, '')

    def get_notification_message(self, action, notification_group,
                                 extra_context):
        template = self.get_notification_template(action, notification_group)
        if action == 'disapproved':
            return template.format(
                action=action,
                action_owner=extra_context.get('action_by', None),
                subject=extra_context.get('subject', None),
                reason=extra_context.get('remarks', None)
            )
        else:
            return template.format(
                action=action,
                action_owner=extra_context.get('action_by', None),
                subject=extra_context.get('subject', None),
            )

    def save_and_broadcast_notification(self, action_owners=None,
                                        broadcast_list=None, action_by=None,
                                        extra_context=None, action=None,
                                        notification_group=None, message=None,
                                        old_notification_pk=None):
        extra_context['action'] = action
        if message is None:
            message = self.get_notification_message(action, notification_group,
                                                    extra_context)

        data = {
            'message': message,
            'notification_group': notification_group,
            'source': action_by,
            'action_owners': action_owners,
            'broadcast_list': broadcast_list,
            'extra_context': extra_context,
        }
        data_for_action = {}
        data_for_broadcast = {}
        instance = self.save_notification(data)
        if old_notification_pk and action not in [REQUESTED]:
            NotificationMixin().deactivate_notification(old_notification_pk)

        if action_owners is not None:
            data_for_action = {
                'message': message,
                'click_action': 'FLUTTER_NOTIFICATION_CLICK'
            }

        if broadcast_list is not None:
            data_for_broadcast = {
                'message': message,
                'click_action': 'FLUTTER_NOTIFICATION_CLICK'
            }

        push_notification_to_cosmos.apply_async((
            json.dumps(data_for_action),
            action_owners,
            json.dumps(data_for_broadcast),
            broadcast_list)
        )

    def deactivate_notification(self, pk):
        if not pk:
            return False
        return AppNotification.objects.filter(pk=pk).update(
            status=NOTIFICATION_STATUS_EXPIRED)
