from django.conf.urls import url, include
from blowhorn.apps.operation_app.v3.views import user as user_views
from blowhorn.apps.operation_app.v3.views import driver as driver_views
from blowhorn.apps.operation_app.v3.views import vehicle as vehicle_views
from blowhorn.apps.operation_app.v3.views import order as order_views
from blowhorn.apps.operation_app.v3.views import document as document_views

from blowhorn.apps.operation_app.v4.views.driver import DriverDocumentList, \
    DriverDocumentUpdate
from blowhorn.apps.operation_app.v4.views.vehicle import VehicleDocumentList, \
    VehicleDocumentUpdate, AddVehicleMap, RemoveVehicleMapping, AddVehicle
from blowhorn.apps.operation_app.v5.views.driver import \
    BackgroundVerificationView
from blowhorn.apps.operation_app.v5.views.user import NotificationView, \
    DriverHandoverItems, ListHub, NotificationOrderDetails
from blowhorn.apps.operation_app.v5.views.document import ActivateDocument, \
    RejectDocument
from blowhorn.apps.operation_app.v5.views.order import ListOrderForUser, \
    MarkContingency, CancelOrder
from blowhorn.apps.operation_app.v5.views.customer import InvoiceFilterList, CustomerInvoiceList, UpdateInvoice, \
    CustomerList, CreditNoteView, RelatedInvoiceList
from blowhorn.apps.operation_app.v5.views.driver_payment import DriverPaymentListView, \
    DriverPaymentDetailView, DriverPaymentFilterData, UpdateDriverPayment, \
    AddDriverPaymentAdjustment, AddDriverCashPaid, UpdateDriverPaymentAdjustment, \
    UpdateDriverCashPaid


urlpatterns = [
            url(r"^login$",
                user_views.Login.as_view(),
                name="operation-user-login"),
            url(r"^profile",
                user_views.Profile.as_view(),
                name='operation-profile'),
            url(r"^fcmid/$",
                user_views.UserFCM.as_view(),
                name="operation-user-login"),
            url(
                r"^drivers/verification$",
                BackgroundVerificationView.as_view(),
                name="background-verification",
            ),
            url(
                r"^drivers/verification/eligibility/(?P<pk>[0-9]+)/$",
                driver_views.BackgroundVerificationEligibility.as_view(),
                name="background-verification-eligibility",
            ),
            url(r"^drivers/$",
                driver_views.DriversList.as_view(),
                name="driver-list"),
            url(
                r"^drivers/(?P<pk>[0-9]+)/$",
                driver_views.DriverDetails.as_view(),
                name="driver-details",
            ),
            url(
                r"^drivers/documents/upload$",
                driver_views.DriverDocumentUpload.as_view(),
                name="driver-document-upload",
            ),
            url(
                r"^drivers/documents/update$",
                DriverDocumentUpdate.as_view(),
                name="driver-document-update",
            ),
            url(
                r"^drivers/documents/$",
                DriverDocumentList.as_view(),
                name="driver-document-list",
            ),
            url(
                r"^bank_account$",
                user_views.BankAccountList.as_view(),
                name="Bank-Account"
            ),
            url(
                r"^driver/photo/$",
                driver_views.DriverImage.as_view(),
                name="driver-document-list",
            ),
            url(
                r"^documents/verification$",
                document_views.DocumentVerification.as_view(),
                name="pending-document-list",
            ),
            url(
                r"^documents/pending-verification$",
                document_views.DocumentPendingVerification.as_view(),
                name="pending-document-list-uploaded-by-a-user",
            ),
            url(
                r"^documents/activate/(?P<pk>[0-9]+)/$",
                ActivateDocument.as_view(),
                name="activate-document-v5",
            ),
            url(
                r"^documents/reject/(?P<pk>[0-9]+)/$",
                RejectDocument.as_view(),
                name="reject-document-v5",
            ),
            url(
                r"^vehicles/vehiclemodels$",
                vehicle_views.VehicleModelList.as_view(),
                name="vehicle-model-list",
            ),
            url(
                r"^vehicles/available-vehicles$",
                vehicle_views.AvailableVehicleList.as_view(),
                name="available-vehicles",
            ),
            url(
                r"^vehicles/documents/upload$",
                vehicle_views.VehicleDocumentUpload.as_view(),
                name="vehicle-document-upload",
            ),
            url(
                r"^vehicles/documents/update$",
                VehicleDocumentUpdate.as_view(),
                name="vehicle-document-update",
            ),
            url(
                r"^vehicles/documents/$",
                VehicleDocumentList.as_view(),
                name="vehicle-document-list",
            ),
            url(r"^vehicles/add-vehicle/$",
                AddVehicle.as_view(),
                name="add-vehicle"),
            url(
                r"^orders/spot$",
                order_views.CreatSpotOrder.as_view(),
                name="order-spot-create",
            ),
            url(
                r"^driver/vehicle/remove/(?P<driver_id>[0-9]+)/$",
                RemoveVehicleMapping.as_view(),
                name="remove-driver-vehicle-map",
            ),
            url(
                r"^driver/vehicle/add/(?P<driver_id>[0-9]+)/$",
                AddVehicleMap.as_view(),
                name="add-driver-vehicle-map",
            ),
            url(
                r"^notifications$",
                NotificationView.as_view(),
                name="fetch-notifications",
            ),
            url(
                r"^user_order/listorders",
                ListOrderForUser.as_view(),
                name="List-user_orders",
            ),
            # api to fetch order details when user clicks on order notification
            url(
                r"^user_order/order/(?P<order_id>[0-9]+)",
                NotificationOrderDetails.as_view(),
                name="Notification_orders",
            ),
            url(
                r"^user_order/contingency",
                MarkContingency.as_view(),
                name="List-user_orders",
            ),
            url(
                r"^order/cancel",
                CancelOrder.as_view(),
                name="Cancel-order",
            ),
            url(
                r"^user/hub/list$",
                ListHub.as_view(),
                name="list-hub",
            ),
            url(
                r"^user/driver/packages",
                DriverHandoverItems.as_view(),
                name="packages-handover-list",
            ),

            url(r'^fleetowner/',
                include('blowhorn.apps.operation_app.v5.views.vendor.urls')),

            url(
                r'^invoice/filterdata',
                InvoiceFilterList.as_view(),
                name="invoice-filter-data"
            ),

            url(
                r'^customers/list',
                CustomerList.as_view(),
                name='customers-list'
            ),

            url(
                r'^customer/invoices',
                CustomerInvoiceList.as_view(),
                name="customer-invoice-list"
            ),

            url(
                r'^invoice/update',
                UpdateInvoice.as_view(),
                name="update-invoice"
            ),

            url(
                r'^invoice/credit_note/create',
                CreditNoteView.as_view(),
                name="create-credit-note"
            ),

            url(
                r'^credit_note/related_invoices',
                RelatedInvoiceList.as_view(),
                name="related-invoices-list"
            ),

            url(
                r'^payments/filterdata',
                DriverPaymentFilterData.as_view(),
                name="driver-payment-filter-data"
            ),

            url(
                r'^payments/list',
                DriverPaymentListView.as_view(),
                name="payments-list"
            ),

            url(
                r'^payment/details/(?P<payment_id>[0-9]+)',
                DriverPaymentDetailView.as_view(),
                name="payment-details"
            ),

            url(
                r'^payment/update/(?P<payment_id>[0-9]+)',
                UpdateDriverPayment.as_view(),
                name="payment-update"
            ),

            url(
                r'^payment/addadjustment',
                AddDriverPaymentAdjustment.as_view(),
                name="add-adjustments"
            ),

            url(
                r'^payment/addcashpaid',
                AddDriverCashPaid.as_view(),
                name="add-cash-paid"
            ),

            url(
                r'^adjustment/update/(?P<adjustment_id>[0-9]+)',
                UpdateDriverPaymentAdjustment.as_view(),
                name="update-adjustments"
            ),

            url(
                r'^cashpaid/update/(?P<cash_paid_id>[0-9]+)',
                UpdateDriverCashPaid.as_view(),
                name="update-cash-paid"
            ),
        ]
