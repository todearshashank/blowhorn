from blowhorn.apps.operation_app.v3.constants import *
from blowhorn.users.constants import NOTIFICATION_GROUP_EXTERNAL_VERIFICATION, \
    NOTIFICATION_GROUP_INTERNAL_VERIFICATION, NOTIFICATION_GROUP_ORDER, \
    NOTIFICATION_GROUP_DRIVER_PAYMENT, NOTIFICATION_GROUP_CUSTOMER_INVOICE
from blowhorn.customer import constants as customer_constants
from blowhorn.driver import constants as driver_constants


MARKED_FOR_CONTINGENCY = 'Contingency'

# Actions
REQUESTED = 'requested'
VERIFICATION_STATUS = 'verification_status'
CREATED = 'created'
ASSIGN = 'assign'

# order categories
MY_ORDERS = 'My Orders'
FIXED_ORDERS = 'Fixed Orders'
TEAM_ORDERS = 'Spot Orders'

# Adjustment and cash paid actions
ADDED_ = 'Added'
APPROVED_ = 'Approved'
REJECTED_ = 'Rejected'
UNAPPROVED_ = 'Unapproved'

# Customer invoice actions
INVOICE_SENT_FOR_APPROVAL = customer_constants.SENT_FOR_APPROVAL
INVOICE_APPROVED = customer_constants.APPROVED
INVOICE_REJECTED = customer_constants.REJECTED
INVOICE_SENT_TO_CUSTOMER = customer_constants.SENT_TO_CUSTOMER
INVOICE_CUSTOMER_ACKNOWLEDGED = customer_constants.CUSTOMER_ACKNOWLEDGED
INVOICE_CUSTOMER_DISPUTED = customer_constants.CUSTOMER_DISPUTED
INVOICE_CUSTOMER_CONFIRMED = customer_constants.CUSTOMER_CONFIRMED

BGV_BASE_NOTIFICATION_TEMPLATE = '{action_owner} has {action} your request for Background Verification of {subject}'
MESSAGE_APPROVED_FOR_BGV = BGV_BASE_NOTIFICATION_TEMPLATE
MESSAGE_REJECTED_FOR_BGV = '%s because {reason}' % BGV_BASE_NOTIFICATION_TEMPLATE
MESSAGE_APPROVAL_NEEDED_FOR_BGV = '{action_owner} has {action} Background Verification approval for {subject}'

MESSAGE_APPROVED_FOR_INTERNAL_VERIFICATION = '{action_owner} has {action} {document_type} of {subject} uploaded by you'
MESSAGE_REJECTED_FOR_INTERNAL_VERIFICATION = '%s because {reason}' % MESSAGE_APPROVED_FOR_INTERNAL_VERIFICATION
MESSAGE_REQUESTED_FOR_INTERNAL_VERIFICATION = '{action_owner} has requested verification of {document_type} of {subject}'

TEMPLATE_BGV_VERIFICATION_STATUS = '{entity} of {driver} has been verified by {action_by}'

TEMPLATE_ORDER_MARKED_CONTINGENCY = '{order} has been marked as contingency by {action_by}, please assign a new driver'
TEMPLATE_SPOT_ORDER_CREATED = 'New order {order} has been created by {action_by}'
TEMPLATE_ORDER_DRIVER_ASSIGNED = 'Driver {driver} has been assigned to order {order}'

"""Driver Payment Templates"""
TEMPLATE_ADDED = '{user} has added {type}({instance_id}) for the payment ID {payment_id} | {driver} ({contract}). Please take action'
TEMPLATE_APPROVED = '{user} has approved the {type}({instance_id}) added by you for the payment ID {payment_id} | {driver}'
TEMPLATE_REJECTED = '{user} has rejected the {type}({instance_id}) added by you for the payment ID {payment_id} | {driver}'
TEMPLATE_UNAPPROVED = '{user} has unapproved the {type}({instance_id}) added by you for the payment ID {payment_id} | {driver}'


"""Customer Invoice Templates"""
TEMPLATE_SENT_FOR_APPROVAL = '{user} has sent invoice {invoice_number} for approval'
TEMPLATE_INVOICE_APPROVED = '{user} has approved invoice {invoice_number}'
TEMPLATE_INVOICE_REJECTED = '{user} has rejected invoice {invoice_number}'
TEMPLATE_SEND_INVOICE = '{user} has sent invoices {invoice_number} to the customer'
TEMPLATE_CUSTOMER_ACKNOWLEDGED = '{user} has marked invoices {invoice_number} as CUSTOMER ACKNOWLEDGED'
TEMPLATE_CUSTOMER_CONFIRMED = '{user} has marked invoices {invoice_number} as CUSTOMER CONFIRMED'
TEMPLATE_CUSTOMER_DISPUTED = '{user} has marked invoices {invoice_number} as CUSTOMER DISPUTED'


NOTIFICATION_TEMPLATES = {
    NOTIFICATION_GROUP_EXTERNAL_VERIFICATION: {
        APPROVED: MESSAGE_APPROVED_FOR_BGV,
        DISAPPROVED: MESSAGE_REJECTED_FOR_BGV,
        REQUESTED: MESSAGE_APPROVAL_NEEDED_FOR_BGV,
        VERIFICATION_STATUS: TEMPLATE_BGV_VERIFICATION_STATUS
    },
    NOTIFICATION_GROUP_INTERNAL_VERIFICATION: {
        APPROVED: MESSAGE_APPROVED_FOR_INTERNAL_VERIFICATION,
        DISAPPROVED: MESSAGE_REJECTED_FOR_INTERNAL_VERIFICATION,
        REQUESTED: MESSAGE_REQUESTED_FOR_INTERNAL_VERIFICATION,
    },
    NOTIFICATION_GROUP_ORDER: {
        REQUESTED: TEMPLATE_ORDER_MARKED_CONTINGENCY,
        CREATED: TEMPLATE_SPOT_ORDER_CREATED,
        ASSIGN: TEMPLATE_ORDER_DRIVER_ASSIGNED
    },
    NOTIFICATION_GROUP_DRIVER_PAYMENT: {
        ADDED_: TEMPLATE_ADDED,
        APPROVED_: TEMPLATE_APPROVED,
        REJECTED_: TEMPLATE_REJECTED,
        UNAPPROVED_: TEMPLATE_UNAPPROVED
    },
    NOTIFICATION_GROUP_CUSTOMER_INVOICE: {
        INVOICE_SENT_FOR_APPROVAL: TEMPLATE_SENT_FOR_APPROVAL,
        INVOICE_APPROVED: TEMPLATE_INVOICE_APPROVED,
        INVOICE_REJECTED: TEMPLATE_INVOICE_REJECTED,
        INVOICE_SENT_TO_CUSTOMER: TEMPLATE_SEND_INVOICE,
        INVOICE_CUSTOMER_ACKNOWLEDGED: TEMPLATE_CUSTOMER_ACKNOWLEDGED,
        INVOICE_CUSTOMER_DISPUTED: TEMPLATE_CUSTOMER_DISPUTED,
        INVOICE_CUSTOMER_CONFIRMED: TEMPLATE_CUSTOMER_CONFIRMED
    }
}


SEARCH_DRIVER_URL = 'search_driver'
DRIVER_PAYMENT_ADJUSTMENT_URL = 'search-driver-payment-adjustment'
DRIVER_PAYMENT_CASH_PAID_URL = 'search-driver-payment-cash-paid'

COSMOS_SCREEN_URLS = {
    NOTIFICATION_GROUP_EXTERNAL_VERIFICATION: '/notification-details',
    NOTIFICATION_GROUP_INTERNAL_VERIFICATION: '/document-verifications',
    NOTIFICATION_GROUP_ORDER: '/order-screen',
    SEARCH_DRIVER_URL: '/driver-search',
    DRIVER_PAYMENT_ADJUSTMENT_URL: 'payment/details/%s?tab=adjustment',
    DRIVER_PAYMENT_CASH_PAID_URL: 'payment/details/%s?tab=cash_paid',
}

REASSIGN = 'reassign'
MARK_AS_CONTINGENCY = 'mark_as_contingency'
SPOT_ORDER_ACTIONS = {
    REASSIGN: {
        'text': REASSIGN,
        'text_color': '0xFF3E6B0F',
        'background_color': '0xFFDCDCDC',
        'screen_url': COSMOS_SCREEN_URLS[SEARCH_DRIVER_URL]
    },
    MARK_AS_CONTINGENCY: {
        'text': MARK_AS_CONTINGENCY.replace('_', ' '),
        'text_color': '0xFF434343',
        'background_color': '0xFFDCDCDC',
        'screen_url': None
    },
    ASSIGN: {
        'text': ASSIGN,
        'text_color': '0xFF00B1DA',
        'background_color': '0xFFDCDCDC',
        'screen_url': COSMOS_SCREEN_URLS[SEARCH_DRIVER_URL]
    }
}

VENDOR_MANDATORY_DOCUMENTS = ['PAN Card', 'Bank account statement/passbook',
                              'Aadhaar Card']

INVOICE_STATUS_CHANGE_MAPPING = [
    {
        'status': customer_constants.CREATED,
        'status_list': [customer_constants.SENT_FOR_APPROVAL]
    },
    {
        'status': customer_constants.REJECTED,
        'status_list': [customer_constants.SENT_FOR_APPROVAL]
    },
    {
        'status': customer_constants.SENT_FOR_APPROVAL,
        'status_list': [customer_constants.APPROVED, customer_constants.REJECTED]
    },
    {
        'status': customer_constants.APPROVED,
        'status_list': [customer_constants.SENT_TO_CUSTOMER, customer_constants.REJECTED]
    },
    {
        'status': customer_constants.SENT_TO_CUSTOMER,
        'status_list': [customer_constants.SENT_TO_CUSTOMER,
                        customer_constants.CUSTOMER_ACKNOWLEDGED]
    },
    {
        'status': customer_constants.CUSTOMER_ACKNOWLEDGED,
        'status_list': [customer_constants.CUSTOMER_CONFIRMED, customer_constants.CUSTOMER_DISPUTED]
    },
]

UNAPPROVED_ACTION_STRING_MAPPING = {
    driver_constants.WITHHELD: 'Released',
    driver_constants.SUSPENDED: 'Unsuspended',
    driver_constants.CLOSED: 'Reopened',
    APPROVED_: 'Unapproved'
}

PAYMENT_DETAIL_FIELDS_MAPPING = {
    'general': (
        'id', 'contract__name', 'contract__contract_type', 'status', 'payment_type', 'driver__name',
        'start_datetime', 'end_datetime', 'is_settled', 'stale_reason', 'batch'),
    'payment_details': ('status', 'total_distance', 'base_payment', 'distance_payment',
                        'time_payment', 'total_shipment_amount', 'total_earnings',
                        'absent_days', 'leave_deduction', 'total_deductions', 'excess_deduction',
                        'adjustment', 'labour_cost', 'toll_charges', 'parking_charges',
                        'gross_pay', 'tip_amount', 'net_pay'),
    'calculation': ('calculation',),
    'bank_details': (),
    'adjustments': (
        'id', 'category', 'description', 'adjustment_amount', 'created_by', 'created_by__email',
        'adjustment_datetime', 'status', 'action_owner', 'action_owner__email',),
    'cash_paid': (
        'id', 'created_by__email', 'cash_paid_datetime', 'description', 'cash_paid', 'status',
        'proof')
}


DRIVER_PAYMENT_STATUS_MAPPING = [
    {
        'status': driver_constants.UNAPPROVED,
        'status_list': [APPROVED_, driver_constants.WITHHELD,
                        driver_constants.SUSPENDED, driver_constants.CLOSED]
    },
    {
        'status': driver_constants.WITHHELD,
        'status_list': [driver_constants.UNAPPROVED, driver_constants.SUSPENDED,
                        driver_constants.CLOSED]
    },
    {
        'status': driver_constants.CLOSED,
        'status_list': [driver_constants.UNAPPROVED]
    },
    {
        'status': driver_constants.SUSPENDED,
        'status_list': [driver_constants.UNAPPROVED]
    },
    {
        'status': APPROVED_,
        'status_list': [driver_constants.UNAPPROVED]
    }
]

ADJUSTMENT_REASONS = [
        'Disciplinary Issue',
        'Buy Rate Difference',
        'Toll Charges',
        'Labour Charges',
        'Previous Balance',
        'Miscellaneous',
        'Parking Fees',
    ]

ADJUSTMENT_CASHPAID_STATUS_MAPPING = [
    {
        'status': driver_constants.NEW,
        'status_list': [APPROVED_, driver_constants.REJECTED]
    },
    {
        'status': APPROVED_,
        'status_list': [driver_constants.UNAPPROVED]
    }
]


DOESNT_HAVE_PERMISSION = 'NO PERMISSION'
UNSETTLED_PAYMENT_EXISTS = 'UNSETTLED PAYMENT'
