from django.utils.translation import gettext as _

from rest_framework import serializers, exceptions

from blowhorn.address.serializer import AddressSerializer
from blowhorn.address.utils import AddressUtil
from blowhorn.driver.models import Driver
from blowhorn.driver.serializers import BankAccountSerializer
from blowhorn.vehicle.models import Vendor, Vehicle, VendorPreferredLocation, VendorAddress


class VendorListSerializer(serializers.ModelSerializer):
    total_drivers_mapped = serializers.IntegerField(read_only=True)
    total_vehicles_owned = serializers.IntegerField(read_only=True)
    cities = serializers.SerializerMethodField()

    class Meta:
        model = Vendor
        fields = (
         'id', 'name', 'phone_number', 'status', 'pan', 'cities', 'remarks', 'reason_for_inactive',
         'total_drivers_mapped', 'total_vehicles_owned')

    def get_cities(self, obj):
        preferred_locations = obj.preferred_locations.all()
        p = []
        for pl in preferred_locations:
            p.append('%s' % pl.city.name)
        return ', '.join(p)


class LocationInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = VendorPreferredLocation
        fields = '__all__'


class VendorAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendorAddress
        fields = '__all__'


class VendorDetailSerializer(serializers.ModelSerializer):
    location_info = LocationInfoSerializer(many=True, read_only=True)
    current_address = VendorAddressSerializer(read_only=True)
    permanent_address = VendorAddressSerializer(read_only=True)
    bank_account = BankAccountSerializer(read_only=True)
    dob = serializers.SerializerMethodField()
    locations = serializers.SerializerMethodField()

    class Meta:
        model = Vendor
        fields = ('id', 'name', 'phone_number', 'gender', 'dob',
                  'father_name', 'photo', 'location_info', 'locations', 'current_address',
                  'permanent_address', 'bank_account', 'pan', 'working_preferences',
                  'status', 'is_address_same')

    def get_dob(self, obj):
        dob = obj.date_of_birth.strftime("%d/%m/%Y") if obj.date_of_birth else None
        return dob

    def get_locations(self, obj):
        preferred_locations = obj.preferred_locations.all()
        coord_list = []
        _dict = {}
        for pl in preferred_locations:
            locations = pl.locations
            coordinates = []
            if locations:
                coordinates = [{
                    "coordinates": {'latitude': i.x, 'longitude': i.y}
                } for i in pl.locations]

            _dict = {
                'operating_city': pl.city.name,
                'addresses': coordinates
            }
            coord_list.append(_dict)

        return coord_list


class VendorVehicleSerializer(serializers.ModelSerializer):
    body_type = serializers.SerializerMethodField()
    vehicle_model = serializers.SerializerMethodField()
    vehicle_class = serializers.SerializerMethodField()

    class Meta:
        model = Vehicle
        fields = ('id', 'registration_certificate_number', 'body_type',
                  'vehicle_model', 'vehicle_class', 'model_year')

    def get_body_type(self, obj):
        return obj.body_type.body_type

    def get_vehicle_model(self, obj):
        return obj.vehicle_model.model_name

    def get_vehicle_class(self, obj):
        return obj.vehicle_model.vehicle_class.commercial_classification

    @staticmethod
    def get_queryset(vendor_pk):
        return Vehicle.objects.filter(
            vendor_id=vendor_pk
        ).select_related(
            'body_type', 'vehicle_model', 'vehicle_model',
            'vehicle_model__vehicle_class'
        )


class VendorDriverSerializer(serializers.ModelSerializer):
    body_type = serializers.SerializerMethodField()
    vehicle_model = serializers.SerializerMethodField()
    vehicle_class = serializers.SerializerMethodField()
    phone_number = serializers.SerializerMethodField()

    class Meta:
        model = Driver
        fields = ('id', 'name', 'phone_number', 'driver_vehicle', 'body_type',
                  'vehicle_class', 'vehicle_model', 'status')

    def get_phone_number(self, obj):
        phone_number = obj.user.phone_number
        if phone_number:
            return phone_number.national_number
        return ''

    def get_body_type(self, obj):
        vehicles = obj.vehicles.all()
        if len(vehicles) > 0:
            return vehicles[0].body_type.body_type
        return ''

    def get_vehicle_model(self, obj):
        vehicles = obj.vehicles.all()
        if len(vehicles) > 0:
            return vehicles[0].vehicle_model.model_name
        return ''

    def get_vehicle_class(self, obj):
        vehicles = obj.vehicles.all()
        if len(vehicles) > 0:
            return vehicles[0].vehicle_model.vehicle_class.commercial_classification
        return ''

    @staticmethod
    def get_queryset(vendor_pk):
        return Driver.objects.filter(
            owner_details_id=vendor_pk
        ).prefetch_related(
            'vehicles', 'vehicles__body_type', 'vehicles__vehicle_model',
            'vehicles__vehicle_model__vehicle_class'
        ).select_related('user')
