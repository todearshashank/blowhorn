import logging
from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q, F
from django.template.defaultfilters import pluralize
from django.utils import timezone
from datetime import timedelta
from blowhorn.order.utils import OrderUtil

from blowhorn.oscar.core.loading import get_model

from config.settings import status_pipelines as StatusPipeline
from blowhorn.utils.functions import utc_to_ist
from blowhorn.address.models import City
from blowhorn.contract.models import Contract
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_SPOT
from blowhorn.trip.models import TRIP_SUSPENSION_REASONS_VEHICLE_BREAKDOWN, \
    TRIP_SUSPENSION_REASONS_DRIVER_DENIED_DUTY, TRIP_SUSPENSION_REASONS_SICK, \
    TRIP_SUSPENSION_REASONS_FAMILY_ISSUE, \
    TRIP_SUSPENSION_REASONS_TRAFFIC_VIOLATION, \
    TRIP_SUSPENSION_REASONS_NO_FUEL, TRIP_SUSPENSION_REASONS_ACCIDENT, \
    TRIP_SUSPENSION_REASONS_CUSTOMER_CANCELLED, \
    TRIP_SUSPENSION_REASONS_TRIP_INTERRUPTED
from blowhorn.apps.operation_app.v5.constants import MY_ORDERS, \
    FIXED_ORDERS, TEAM_ORDERS, MARKED_FOR_CONTINGENCY, SPOT_ORDER_ACTIONS, \
    REASSIGN, MARK_AS_CONTINGENCY, ASSIGN, PAGE_TO_BE_DISPLAYED, NUMBER_OF_ORDERS_IN_PAGE

Order = get_model('order', 'Order')
Contract = get_model('contract', 'Contract')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class OrderDetails(object):

    def __init__(self, **kwargs):
        """
        :param kwargs:
        """
        self.user = kwargs.get('user')
        self.city = None
        self.supply_user = None
        self.contracts = None
        self.params = None
        self.is_spoc = False
        self.response_data = dict(filterList=[])
        if self.user:
            self.city = City.objects.prefetch_related('supply_users') \
                .filter(supply_users=self.user)
            self.supply_user = True if self.city.exists() else False
            self.params = (
                Q(status=CONTRACT_STATUS_ACTIVE) &
                (Q(created_by=self.user) |
                 Q(spocs=self.user) |
                 Q(supervisors=self.user))
            )
            self.contracts = Contract.objects \
                .select_related('created_by') \
                .prefetch_related('spocs', 'supervisors') \
                .filter(self.params)
            self.is_spoc = self.contracts.exists()

    def __get_pagination_data(self, queryset, page, num):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else objects.previous_page_number()
        )
        next = 0 if not objects.has_next() else objects.next_page_number()

        # pages_count = paginator.num_pages
        data = {
            "count": count,
            "previous": previous,
            "next": next,
            "objects": objects.object_list,
        }

        return data

    def get_user_orders_data(self, request):
        """
        :return:
        """
        """
        Supply user can only see orders which are marked for contingency,
        or order which doesn't have driver assigned
        """
        if self.is_spoc and self.supply_user:
            params = Q(customer_contract__in=self.contracts) | (
                Q(city__in=self.city) & (Q(marked_for_contingency=True)
                                         | Q(driver__isnull=True)))
        elif self.supply_user:
            params = (Q(city__in=self.city) & (Q(marked_for_contingency=True)
                                               | Q(driver__isnull=True)))

        elif self.is_spoc:
            params = Q(customer_contract__in=self.contracts) | (
                Q(customer_contract__in=self.contracts) & Q(marked_for_contingency=True))
        else:
            return None

        page_to_be_displayed = (
            request.GET.get("next")
            if request.GET.get("next", "") and int(request.GET["next"]) > 0
            else PAGE_TO_BE_DISPLAYED
        )

        number_of_entry = NUMBER_OF_ORDERS_IN_PAGE

        qs = Order.objects.filter(
            status__in=[StatusPipeline.ORDER_NEW,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.DRIVER_ASSIGNED,
                        StatusPipeline.OUT_FOR_DELIVERY],
            order_type__in=[settings.SUBSCRIPTION_ORDER_TYPE,
                            settings.SPOT_ORDER_TYPE,
                            settings.ONDEMAND_ORDER_TYPE],
            pickup_datetime__gte=timezone.now() - timedelta(days=2)
        ).select_related(
            'created_by', 'customer', 'driver', 'hub', 'customer_contract',
            'vehicle_class_preference', 'driver__user'
        ).prefetch_related(
            'driver__vehicles',
            'driver__vehicles__body_type',
            'driver__vehicles__vehicle_model',
            'driver__vehicles__vehicle_model__vehicle_class'
        )
        qs = qs.filter(params)
        order_grouping = request.GET.get('order_grouping')

        if order_grouping == MY_ORDERS:
            orders = qs.filter(created_by=self.user)
        elif order_grouping == FIXED_ORDERS:
            orders = qs.filter(order_type=settings.SUBSCRIPTION_ORDER_TYPE)
        elif order_grouping == TEAM_ORDERS:
            # team_contracts = Contract.objects.filter(supervisors=self.user,
            #                                          contract_type=CONTRACT_TYPE_SPOT).values_list('id').union(
            #     Contract.objects.filter(spocs=self.user, contract_type=CONTRACT_TYPE_SPOT).values_list('id'))
            params = params | Q(customer_contract__in=self.contracts)
            orders = qs.filter(
                params,
                order_type__in=[settings.SPOT_ORDER_TYPE, settings.ONDEMAND_ORDER_TYPE]
            ).exclude(created_by=self.user)

        orders = orders.order_by(F('marked_for_contingency').desc(nulls_last=True), '-pickup_datetime')
        pagination_data = self.__get_pagination_data(
            orders,
            page_to_be_displayed,
            number_of_entry
        )

        paginated_orders = pagination_data.pop('objects')
        self.response_data['pagination_data'] = pagination_data

        self.response_data['orders'] = []
        vehicle_classes = Contract.objects.select_related('city') \
            .prefetch_related('vehicle_classes') \
            .filter(
            city__name=request.GET.get('operating_city'),
            vehicle_classes__commercial_classification__isnull=False
        ).values_list(
            'vehicle_classes__commercial_classification', flat=True
        ).distinct()

        for order in paginated_orders:
            data = self.get_order_details(order)
            self.response_data['orders'].append(data)

        if MY_ORDERS in self.response_data['filterList']:
            self.response_data['filterList'].remove(MY_ORDERS)
            self.response_data['filterList'].insert(0, MY_ORDERS)

        self.response_data['contingency_reasons'] = \
            self.get_contingency_reasons()

        self.response_data['vehicle_classes'] = []
        for vc in vehicle_classes:
            self.response_data['vehicle_classes'].append(vc)

        self.response_data['roles'] = dict(
            is_spoc=self.is_spoc,
            is_supply=self.supply_user,
            is_superuser=self.user.is_superuser)

        self.response_data['cancellation_reasons'] = OrderUtil().fetch_order_cancellation_reasons()
        return self.response_data

    def __get_vehicle_details_for_display(self, customer_contract):
        """
        :param customer_contract:
        :return:
        """
        vehicle_classes = ', '.join(
            ['%s' % c for c in customer_contract.vehicle_classes.all()[:1]]
        )
        body_types = ', '.join(
            ['%s' % b for b in customer_contract.body_types.all()[:1]]
        )
        return vehicle_classes, body_types

    def get_order_details(self, order):
        """
        :param order:
        :return:
        """

        data = dict(
            order_id=order.id,
            order_number=order.number,
            customer_name=order.customer.name if order.customer else None,
            order_status={}
        )
        data['order_status']['text'] = order.status if not \
            order.marked_for_contingency else MARKED_FOR_CONTINGENCY

        if order.marked_for_contingency:
            data['order_status']['background_color'] = '0x80A8A8A8'
            data['order_status']['text_color'] = '0xFF434343'
        else:
            data['order_status']['background_color'] = '0x8000BBD3'
            data['order_status']['text_color'] = '0xFF0F526B'

        customer_contract = order.customer_contract
        data['hub_name'] = order.hub.name if order.hub else None
        '''
            WARNING..! App is converting to UTC automatically while formatting
            when timezone info present. So removing timezone info.
            @todo revert this change once app side fix released.
        '''
        data['pickup_datetime'] = utc_to_ist(order.pickup_datetime).replace(tzinfo=None)
        data['vehicle_number'] = order.driver.driver_vehicle if order.driver else None
        data['phone_number'] = str(order.driver.user.national_number) if order.driver else None
        data['driver_name'] = order.driver.name if order.driver else None
        data['contract_id'] = order.customer_contract_id if order else None
        data['contract_name'] = customer_contract.name
        data['contract_description'] = customer_contract.description
        data['base_pay'] = float(customer_contract.contractterm_set.all().latest(
                'wef').buy_rate.driver_base_payment_amount)
        data['hub_id'] = order.hub_id if order else None

        vehicle_class = order.vehicle_class_preference
        if order.driver:
            if order.driver.vehicles:
                data['vehicle_class'] = ', '.join(
                    [str(vc.vehicle_model.vehicle_class.commercial_classification)
                     for vc in order.driver.vehicles.all()])
                data['body_type'] = ', '.join([
                    str(vc.body_type.body_type) for vc in order.driver.vehicles.all()])
        else:
            if not vehicle_class:
                data['vehicle_class'], data['body_type'] = \
                    self.__get_vehicle_details_for_display(customer_contract)
            else:
                data['vehicle_class'] = \
                    vehicle_class.commercial_classification
                data['body_type'] = \
                    ', '.join(
                        ['%s' % b for b in customer_contract.body_types.all()]
                    )

        data['created_by'] = order.created_by.name or order.created_by.email
        display_text_1 = None
        display_text_2 = None
        driver = order.driver

        if order.marked_for_contingency:
            if self.supply_user:
                if not vehicle_class:
                    display_text_1, display_text_2 = \
                        self.__get_vehicle_details_for_display(customer_contract)
                else:
                    display_text_1 = \
                        vehicle_class.commercial_classification
                    display_text_2 = \
                        ', '.join(
                            ['%s' % b for b in customer_contract.body_types.all()]
                        )
            else:
                display_text_1 = None
                display_text_2 = None
            data['driver_name'] = None
            data['vehicle_number'] = None
            data['phone_number'] = None
        elif self.supply_user and self.is_spoc:
            if driver:
                display_text_1 = driver.name
                display_text_2 = driver.driver_vehicle
            else:
                if not vehicle_class:
                    display_text_1, display_text_2 = \
                        self.__get_vehicle_details_for_display(customer_contract)
                else:

                    display_text_1 = \
                        vehicle_class.commercial_classification
                    display_text_2 = \
                        ', '.join(
                            ['%s' % b for b in customer_contract.body_types.all()]
                        )
        elif self.supply_user:
            if not driver:
                if not vehicle_class:
                    display_text_1, display_text_2 = \
                        self.__get_vehicle_details_for_display(customer_contract)
                else:

                    display_text_1 = \
                        vehicle_class.commercial_classification
                    display_text_2 = ', '.join(
                        ['%s' % b for b in customer_contract.body_types.all()]
                    )

        elif self.is_spoc:
            if driver:
                display_text_1 = driver.name
                display_text_2 = driver.driver_vehicle

        data['display_text_1'] = display_text_1
        data['display_text_2'] = display_text_2

        duty_type = None
        if customer_contract and customer_contract.avg_duty_hrs:
            duty_type = '{} Hr{}'.format(
                customer_contract.avg_duty_hrs,
                pluralize(customer_contract.avg_duty_hrs))

        data['duty_type'] = duty_type
        data['action'] = {}
        if order.marked_for_contingency and self.supply_user:
            data['action'] = SPOT_ORDER_ACTIONS[REASSIGN]
        elif order.driver and not order.marked_for_contingency:
            data['action'] = SPOT_ORDER_ACTIONS[MARK_AS_CONTINGENCY]
        elif self.supply_user:
            data['action'] = SPOT_ORDER_ACTIONS[ASSIGN]

        if hasattr(order, 'filter_type'):
            data['filter_key'] = order.filter_type
            if order.filter_type not in self.response_data['filterList']:
                self.response_data['filterList'].extend([order.filter_type])

        return data

    def get_contingency_reasons(self):
        """
        :return:
        """
        return [
            TRIP_SUSPENSION_REASONS_VEHICLE_BREAKDOWN,
            TRIP_SUSPENSION_REASONS_DRIVER_DENIED_DUTY,
            TRIP_SUSPENSION_REASONS_SICK,
            TRIP_SUSPENSION_REASONS_FAMILY_ISSUE,
            TRIP_SUSPENSION_REASONS_TRAFFIC_VIOLATION,
            TRIP_SUSPENSION_REASONS_NO_FUEL,
            TRIP_SUSPENSION_REASONS_ACCIDENT,
            TRIP_SUSPENSION_REASONS_CUSTOMER_CANCELLED,
            TRIP_SUSPENSION_REASONS_TRIP_INTERRUPTED
        ]