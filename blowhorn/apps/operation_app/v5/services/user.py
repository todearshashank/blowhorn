import json
import logging
from django.db import transaction

from rest_framework import status
from blowhorn.oscar.core.loading import get_model
from django.db.models import Q, F, Case, When
from django.conf import settings

from config.settings import status_pipelines as StatusPipeline
from blowhorn.apps.driver_app.utils import LegacyCipher
from blowhorn.common.helper import CommonHelper
from blowhorn.payment.payment_bank.helpers import cod_orderlevel_save
from django.db.models import Q, Count

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Trip = get_model('trip', 'Trip')
Hub = get_model('address', 'Hub')
Driver = get_model('driver', 'Driver')
CashHandover = get_model('driver', 'CashHandover')


class HandoverItemsMixin(object):

    def _initialize_attributes(self, data):
        self.response_data = None
        self.error_msg = None
        self.status_code = status.HTTP_200_OK
        driver_mobile = settings.ACTIVE_COUNTRY_CODE + data.get('driver_mobile') if data.get('driver_mobile') else None
        driver_scan_code = data.get('driver_scan_code')
        self.hub_id = data.get('hub_id')
        self.hub = Hub.objects.filter(id=self.hub_id).first()
        self.driver = None
        location = data.get('location_details', None)
        self.geopoint = None
        if location:
            self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        if driver_scan_code:
            driver_mobile = settings.ACTIVE_COUNTRY_CODE + self._get_driver_mobile_using_qr_code(driver_scan_code)

        if driver_mobile:
            self.driver = self.get_driver(driver_mobile)

        if not self.driver:
            self.error_msg = 'Invalid driver mobile/reference number'
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        self.trip = Trip.objects.filter(driver=self.driver, status__in=[
            StatusPipeline.TRIP_IN_PROGRESS,
            StatusPipeline.TRIP_ALL_STOPS_DONE]
                                        ).first()

        if not self.trip:
            self.error_msg = 'No ongoing trip for driver exists'
            self.status_code = status.HTTP_400_BAD_REQUEST

    def get_order_for_hub(self):
        amount = None
        # Fetch orders moving to hub, undelivered/returned orders
        orders = Order.objects.filter(Q(driver=self.driver,
                                        status__in=[StatusPipeline.ORDER_PICKED,
                                                    StatusPipeline.OUT_FOR_DELIVERY,
                                                    StatusPipeline.ORDER_RETURNED,
                                                    StatusPipeline.ORDER_MOVING_TO_HUB,
                                                    StatusPipeline.ORDER_MOVING_TO_HUB_RTO,
                                                    StatusPipeline.UNABLE_TO_DELIVER,
                                                    StatusPipeline.ORDER_OUT_FOR_RTO],
                                        reference_number__isnull=False) & (
            Q(stop_orders__trip=self.trip) | Q(stops__trip=self.trip))).annotate(
            stop_type=Case(When(stop_orders__stop__isnull=False,
                                then=F('stop_orders__stop__stop_type')),
                           default=F('stops__stop_type')), order_lines_count=Count('order_lines'),
            ignore_order_lines=F('customer_contract__trip_workflow__ignore_order_lines')).values('id',
                                                                                                 'reference_number',
                                                                                                 'number',
                                                                                                 'stop_type',
                                                                                                 'order_lines_count',
                                                                                                 'ignore_order_lines')
        # amount = Order.objects.filter(status=StatusPipeline.ORDER_DELIVERED, stops__trip=self.trip,
        #                               cash_on_delivery__gt=0).aggregate(amount=Sum('cash_on_delivery'))
        if self.driver:
            amount = self.driver.cod_balance
        return orders, amount

    def get_driver(self, driver_mobile=None, driver_id=None):
        if driver_mobile:
            return Driver.objects.filter(user__phone_number=driver_mobile).first()
        else:
            return Driver.objects.filter(id=driver_id).first()

    def _get_driver_mobile_using_qr_code(self, driver_scan_code):

        try:
            information = LegacyCipher.decode(driver_scan_code)
            information = json.loads(information)
            driver_mobile = information.get('id')
            print('qr driver_mobile - ', driver_mobile)
            if not driver_mobile:
                return None
            return str(driver_mobile)
        except BaseException:
            pass

    def receive_items(self, data, user):

        ret_status = status.HTTP_200_OK
        message = 'Items collected successfully'

        driver_id = data.get('driver_id')
        hub_id = data.get('hub_id')
        hub = Hub.objects.filter(id=hub_id).first()
        amount = data.get('amount')
        order_ids = data.get('order_ids')

        location = data.get('location_details', None)
        geopoint = None
        address = None

        if location:
            geopoint = CommonHelper.get_geopoint_from_location_json(
                location)
            address = CommonHelper.get_address_from_location_json(location)

        driver = self.get_driver(driver_id=driver_id)

        trip = Trip.objects.filter(driver=driver, status__in=[
            StatusPipeline.TRIP_IN_PROGRESS,
            StatusPipeline.TRIP_ALL_STOPS_DONE]
                                        ).first()
        if not trip:
            trip = None

        try:
            with transaction.atomic():
                """
                add record to CashHandover table for handing over cash to hub associate by driver
                """
                # need this logic at driver app
                if amount:
                    cash_handover = CashHandover.objects.create(driver=driver,
                                                            collected_by=user,
                                                            amount=amount,
                                                            hub=hub,
                                                            trip=trip
                                                        )

                    cod_orderlevel_save(
                                    driver=driver,
                                    trip=trip,
                                    cash_handover=cash_handover
                                )
                    
                    # subtract amount from driver cod balance
                    cod_balance = float(driver.cod_balance) - amount
                    Driver.objects.filter(id=driver.id).update(cod_balance=cod_balance)

                if not order_ids:
                    return status.HTTP_200_OK, 'Cash collected successfully'

                orders = Order.objects.filter(id__in=order_ids)

                order_event_list = []
                for order in orders:
                    event_status = StatusPipeline.REACHED_AT_HUB_RTO if \
                        order.return_to_origin else \
                        StatusPipeline.REACHED_AT_HUB
                    order_event_list.append(Event(
                        status=event_status,
                        address=hub.shipping_address or None,
                        hub_associate=user,
                        driver_id=driver_id,
                        order=order,
                        current_location=geopoint,
                        current_address=address,
                        remarks=hub.name
                    ))
                Event.objects.bulk_create(order_event_list)

                orders.exclude(return_to_origin=True).update(
                    status=StatusPipeline.REACHED_AT_HUB,
                    latest_hub=hub)

                orders.filter(return_to_origin=True).update(
                    status=StatusPipeline.REACHED_AT_HUB_RTO,
                    latest_hub=hub)

        except Exception as e:
            ret_status = status.HTTP_400_BAD_REQUEST
            message = e.args[0]

        return ret_status, message
