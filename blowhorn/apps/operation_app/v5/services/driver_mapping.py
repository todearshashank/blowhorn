from django.utils import timezone

from blowhorn.address.models import City
from .vendor import VendorMixin
from blowhorn.driver.models import Driver, DriverHistory, APPROVED, DriverPayment
from blowhorn.apps.operation_app.v5.constants import DOESNT_HAVE_PERMISSION, UNSETTLED_PAYMENT_EXISTS
from blowhorn.users.constants import OPS_MANAGERS


class VendorDriverMappingMixin(object):
    vendor_mixin = VendorMixin()

    def map_existing_driver_to_vendor(self, driver_pks, vendor, action_by):
        """

        :param driver_pk:
        :param vendor:
        :param action_by:
        :return:
        """
        count = Driver.objects.filter(pk__in=driver_pks).update(
            owner_details=vendor,
            own_vehicle=False,
            modified_date=timezone.now(),
            modified_by=action_by
        )

        if count:
            drivers_list = []
            for i in driver_pks:
                driver = Driver.objects.get(pk=i)
                drivers_list.append(
                    DriverHistory(field='owner_details', old_value=None, new_value=vendor, user=action_by,
                                  driver=driver))
            DriverHistory.objects.bulk_create(drivers_list)
        self.vendor_mixin.update_auditlog(vendor, action_by)
        return count

    def unmap(self, driver_pks, vendor, action_by):
        """

        :param driver_pk:
        :param vendor:
        :param action_by:
        :return:
        """
        count = Driver.objects.filter(pk__in=driver_pks).update(
            owner_details=None,
            own_vehicle=True,
            modified_date=timezone.now(),
            modified_by=action_by
        )
        if count:
            drivers_list = []
            for i in driver_pks:
                driver = Driver.objects.get(pk=i)
                drivers_list.append(
                    DriverHistory(field='owner_details', old_value=vendor, new_value=None, user=action_by,
                                  driver=driver))
            DriverHistory.objects.bulk_create(drivers_list)
        self.vendor_mixin.update_auditlog(vendor, action_by)
        return count


    def validate_data(self, driver_pk, user):
        """
        :param driver_pk:
        :param vendor_obj:
        :return:
        """
        driver = Driver.objects.filter(pk=driver_pk).first()
        if driver:
            check_user_permission = True if OPS_MANAGERS in user.groups.values_list('name', flat=True) or user.is_superuser else False
            if not check_user_permission:
                return DOESNT_HAVE_PERMISSION, driver.name

            if DriverPayment.objects.filter(driver=driver, status=APPROVED, is_settled=False).exists():
                return UNSETTLED_PAYMENT_EXISTS, driver.name
        return None, None





