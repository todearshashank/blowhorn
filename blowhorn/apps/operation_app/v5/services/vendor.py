import re
import logging
from datetime import datetime
from django.conf import settings
from django.db import transaction
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import LineString
from django.contrib.gis.geos import Point, MultiPoint

import phonenumbers
from phonenumber_field.phonenumber import PhoneNumber
from rest_framework import exceptions

from config.settings.status_pipelines import TRIP_END_STATUSES, TRIP_NEW
from blowhorn.address.models import City, State
from blowhorn.driver.models import Driver
from blowhorn.trip.models import Trip
from blowhorn.apps.operation_app.v5.constants import VENDOR_MANDATORY_DOCUMENTS,\
    ACTIVE, REGEX_MATCH_ANYTHING
from blowhorn.common.helper import CommonHelper
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.document.models import DocumentType
from blowhorn.driver import constants as driver_const
from blowhorn.driver.models import BankAccount
from blowhorn.users.models import User
from blowhorn.customer.models import CustomerManager
from blowhorn.vehicle.models import VendorPreferredLocation, Vendor,\
    VendorAddress, VendorDocument, VendorHistory
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

REGEX_IFSC_CODE = r'^[A-Za-z]{4}[0]\w{6}$'
MANDATORY_VENDOR_FIELDS = ['name', 'phone_number', 'pan', 'current_address', 'permanent_address', 'date_of_birth',
                           'father_name'] \
    if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE else ['name', 'phone_number']


class VendorValidationMixin(object):

    def validate_age(self, dob):
        born = datetime.strptime(dob, "%d/%m/%Y").date()
        now = datetime.now().date()
        calc_age = ((now - born).days / driver_const.NUMBER_OF_DAYS_IN_YEAR)
        if driver_const.DRIVER_MIN_AGE <= calc_age:
            return born
        else:
            raise exceptions.ValidationError('Driver should be above %s ' \
                'years of age' % driver_const.DRIVER_MIN_AGE)

    def validate(self, data, mandatory_fields):
        """
        :param data:
        :param mandatory_fields:
        :return:
        """
        message = None
        missing_fields = []
        for f in mandatory_fields:
            if not data.get(f, None):
                missing_fields.append(f)

        if missing_fields:
            message = 'Fill the following fields:\n %s' % \
                      ', '.join(missing_fields)
        return message

    def clean_personal_details(self, data, mandatory_fields=None,
                               validate_fields=None, vendor_pk=None):
        """
        :param data:
        :param mandatory_fields:
        :param validate_fields:
        :return:
        """
        message = self.validate(data, mandatory_fields)
        if message:
            raise exceptions.ValidationError(_(message))

        phone_number = data.get('phone_number', None)
        email = '%s@%s' % (phone_number,
                           settings.DEFAULT_DOMAINS.get('vendor'))

        vendor = Vendor.objects.filter(pk=vendor_pk).first()
        phone_number = phonenumbers.parse(phone_number,
                                          settings.COUNTRY_CODE_A2)
        user = User.objects.filter(phone_number=phone_number)
        for i in user:
            if hasattr(i, 'vendor'):
                if i.vendor != vendor:
                    raise exceptions.ValidationError(
                        _("Fleetowner with same phone number already present")
                    )
        # if not vendor_pk or vendor.phone_number.national_number != int(phone_number):
        #     if User.objects.filter(email=email).exists():
        #         raise exceptions.ValidationError(
        #             _('Fleetowner with same phone number already present'))

        data['email'] = email
        if 'phone_number' in validate_fields:
            try:
                if not phonenumbers.is_valid_number(phone_number):
                    message = _('Invalid mobile number')
                    raise exceptions.ValidationError(message)
            except phonenumbers.NumberParseException:
                message = _('Invalid mobile number')
                raise exceptions.ValidationError(message)

            data['phone_number'] = phone_number

        pan = data.pop('pan', None)
        # pan_doc = DocumentType.objects.filter(
        #     code='PAN')
        # pan_regex = pan_doc.first().regex if pan_doc.exists() else REGEX_MATCH_ANYTHING
        # re.compile(pan_regex)

        # if pan is not None and not len(pan):
        #     data['pan'] = None
        # if pan:
        #     data['pan'] = pan.upper()
        #     vendor_qs = Vendor.objects.filter(pan=data['pan'])
        #     vendor_qs = vendor_qs.exclude(pk=vendor_pk)
        #
        #     if vendor_qs.exists():
        #         raise exceptions.ValidationError(
        #             _('Fleetowner with same PAN number already present'))
        #     if not re.match(pan_regex,
        #                     pan) and settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
        #         raise exceptions.ValidationError(
        #             _('Please enter valide PAN number')
        #         )

        if 'date_of_birth' in validate_fields and data.get('dob', None):
            data['date_of_birth'] = self.validate_age(data['dob'])
        elif not data.get('dob'):
            data.pop('dob', None)

        data['gender'] = data.get('gender') or 'male'
        return data

    def clean_location_details(self, data, vendor):
        """
        :param data:
        :param vendor:
        :return:
        """
        cleaned_data = []
        for loc in data:
            city = City.objects.get(name=loc.get('operating_city'))
            locations = loc.get('addresses', [])
            coordinates = []
            flag = True
            _dict = {}
            if len(locations) == 0:
                _dict = {
                    'vendor': vendor,
                    'city': city,
                    'locations': None
                }
                flag = False
                cleaned_data.append(_dict)
            for coord in locations:
                coord = coord.get('coordinates', None)

                if coord.get('latitude') is None and coord.get('longitude') is None:
                    _dict = {
                        'vendor': vendor,
                        'city': city,
                        'locations': None
                    }
                    flag = False
                    cleaned_data.append(_dict)

                latitude = coord.get('latitude', 0.0) or 0.0
                longitude = coord.get('longitude', 0.0) or 0.0
                if latitude and longitude:
                    geopoint = Point(latitude, longitude)
                    coordinates.append(geopoint)
            if flag:
                _dict = {
                    'vendor': vendor,
                    'city': city,
                    'locations': MultiPoint(coordinates)
                }
                cleaned_data.append(_dict)
        return cleaned_data

    def clean_address_details(self, data, vendor, key):
        """
        :param data:
        :param vendor:
        :param key:
        :return:
        """
        address_pk = getattr(vendor, key, None)
        geopoint = None
        if data.get('coordinates', {}):
            latitude = data['coordinates'].get('latitude', None)
            longitude = data['coordinates'].get('longitude', None)
            if latitude and longitude:
                geopoint = CommonHelper.get_geopoint_from_latlong(
                    latitude, longitude)
            data['geopoint'] = geopoint
            data.pop('coordinates', None)

        if address_pk is None:
            return False, data
        else:
            # data['vendor'] = vendor
            return True, data

    def clean_bank_details(self, data):
        """
        :param data:
        :return:
        """
        message = self.validate(
            data, ['account_name', 'account_number', 'ifsc_code'])
        if message:
            raise exceptions.ValidationError(_(message))

        ifsc_code = data.get('ifsc_code').upper()

        if not re.search(REGEX_IFSC_CODE,
                         ifsc_code) and settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            raise exceptions.ValidationError(_('Invalid IFSC Code'))
        data['ifsc_code'] = ifsc_code

        account_number = data.get('account_number')
        account = BankAccount.objects.filter(account_number=account_number)
        if account.exists() and data.get('id') == '0':
            raise exceptions.ValidationError(_("Account number already registered"))
        return data

    def clean_work_preferences(self, data):
        if not data:
            raise exceptions.ValidationError(_('No work preferences selected'))
        return data

    def is_mandatory_fields_present(self, vendor):
        return [field for field in MANDATORY_VENDOR_FIELDS
                if not getattr(vendor, field)]

    def get_vendor_document_types(self):
        return DocumentType.objects.filter(
            name__in=VENDOR_MANDATORY_DOCUMENTS).values_list('name', flat=True)

    def verify_documents(self, vendor):
        missing_documents = []
        pending_verification = []
        doc_types = self.get_vendor_document_types()
        for doc_type in doc_types:
            for doc in vendor.vendor_document.all():
                if str(doc.document_type) == doc_type:
                    if doc.status == ACTIVE:
                        break

                    if doc.status == VERIFICATION_PENDING:
                        pending_verification.append(str(doc_type))
                        break
            else:
                missing_documents.append(str(doc_type))

        if missing_documents or pending_verification:
            return {
                'pending': pending_verification,
                'missing': missing_documents
            }
        return False

    def is_ready_for_activation(self, vendor):
        missing_mandatory_fields = self.is_mandatory_fields_present(vendor)
        if missing_mandatory_fields:
            return False, [_('Add missing details\n %s' % ', '.join(missing_mandatory_fields))]

        validated_data = self.verify_documents(vendor)
        if validated_data:
            message = []
            missing_documents = validated_data.get('missing', {})
            if missing_documents:
                message.append('Missing Documents: \n %s' %
                               ', '.join(missing_documents))
                # message = missing_documents
            pending_documents = validated_data.get('pending', {})
            if pending_documents:
                message.append('Verification Pending: \n %s' %
                               ', '.join(pending_documents))

            # return False, '\n'.join(message)
            return False, message
        return True, None

    def is_ready_for_deactivation(self, vendor):
        drivers_list = Driver.objects.filter(owner_details=vendor)
        for d in drivers_list:
            for r in d.resourceallocation_set.all():
                if r.contract.status == CONTRACT_STATUS_ACTIVE:
                    return False, _(
                        "Fleet owner status cannot be changed as the drivers"
                        " associated have active contracts")
        trips = Trip.objects.filter(driver__owner_details=vendor).exclude(
            status__in=TRIP_END_STATUSES + [TRIP_NEW])
        if trips.exists():
            return False, _(
                "Fleet owner status cannot be changed as the drivers associated have ongoing trips")
        return True, None


class VendorMixin(object):

    def update_auditlog(self, vendor, user):
        """
        :param vendor:
        :param user:
        :return:
        """
        vendor.modified_date = timezone.now()
        vendor.modified_by = user
        vendor.save()
        return vendor

    def __create_or_update_user(self, data):
        _data = dict(
            name=data.get('name'),
            email=data.get('email'),
            phone_number=data.get('phone_number'),
            is_active=False)
        email_id = _data.get('email')
        user_obj = User.objects.filter(email=email_id).first()
        if user_obj:
            User.objects.filter(pk=user_obj.pk).update(**_data)
            return user_obj
        else:
            user = User(**_data)
            user.set_password('1234')
            user.save()
            return user

    def update_personal_details(self, data, instance=None, vendors_history_list=None, action_owner=None):
        """
        :param data:
        :param instance:
        :param action_owner:
        :return:
        """
        if instance:
            data['modified_date'] = timezone.now()
            data['modified_by'] = action_owner
            try:
                if data['dob'] is not None:
                    data['date_of_birth'] = datetime.strptime(data['dob'], "%d/%m/%Y")
                    data.pop('dob', None)
            except KeyError as ke:
                pass
            vendor_qs = Vendor.objects.filter(pk=instance.pk)
            if instance.user_id is None:
                user = User.objects.filter(phone_number=instance.phone_number, driver__isnull=True,
                                           customer__isnull=True, is_staff=False).first()
                if user:
                    instance.user = user
            self.__create_or_update_user(data)
            data.pop('email', None)
            vendor_qs.update(**data)
            if vendors_history_list:
                VendorHistory.objects.bulk_create(vendors_history_list)
            return instance
        else:
            with transaction.atomic():
                user = self.__create_or_update_user(data)
                data.pop('email', None)
                data.pop('id', None)
                data['user'] = user
                data['source'] = 'App'
                if action_owner:
                    data['created_by'] = action_owner
                vendor = Vendor.objects.create(**data)
                # Created by is not being saved in the above create query
                vendor.created_by = action_owner
                vendor.save()
                if vendors_history_list:
                    VendorHistory.objects.bulk_create(vendors_history_list)
                return vendor

    def update_location_preference(self, data, vendor, action_owner):
        """
        :param data:
        :param vendor:
        :param action_owner:
        :return:
        """
        try:
            vendor.preferred_locations.clear()
            for d in data:
                vendor.preferred_locations.add(VendorPreferredLocation.objects.create(**d))
            return self.update_auditlog(vendor, action_owner)
        except BaseException as e:
            logger.info('Exception Occurred: %s' % e)
            raise Exception(_('Failed to save location details'))

    def update_address(self, created, data, vendor, key, user):
        """
        :param created:
        :param data:
        :param vendor:
        :param key:
        :param user:
        :return:
        """
        if created:
            address = VendorAddress.objects.filter(
                pk=getattr(vendor, '%s_id' % key))
            address.update(**data)
            setattr(vendor, key, address.first())
            self.update_auditlog(vendor, user)
        else:
            try:
                address = VendorAddress.objects.create(**data)
                setattr(vendor, key, address)
                self.update_auditlog(vendor, user)
                return address
            except exceptions.APIException as e:
                logger.info('APIException: %s' % e)
                raise exceptions.APIException(
                    _('Failed to create %s' % key.replace('_', ' '))
                )

    def update_bank_details(self, data, vendor, vendors_history_list, user):
        """
        :param data:
        :param vendor:
        :param user:
        :return:
        """
        account = BankAccount.objects.filter(pk=data.get('id')).first()
        if account and int(data.get('id')) > 0:
            if vendor.bank_account != account and vendor.bank_account:
                # Assuming that transaction block won't fail
                vendor.bank_account.add_bank_account_history(vendor.bank_account, user, 'Unmapped-FO', driver=None,
                                                             vendor=vendor)
            account.add_bank_account_history(account, user, 'Mapped-FO', driver=None,
                                             vendor=vendor)
            account.account_name = data.get('account_name')
            account.ifsc_code = data.get('ifsc_code')
            vendor.bank_account = account
            with transaction.atomic():
                account.save()
                if vendors_history_list:
                    VendorHistory.objects.bulk_create(vendors_history_list)
                self.update_auditlog(vendor, user)
            return account
        else:
            vendor_cities = vendor.preferred_locations.values('city_id')
            if not (user.is_superuser or user.is_city_manager or user.is_ops_manager(city_qs=vendor_cities)):
                raise exceptions.ValidationError(
                    _('You do not have permission to add bank account, you can only choose from existing accounts')
                )
            try:
                data['created_by'] = user
                id = data.pop('id')
                account = BankAccount.objects.create(**data)
                VendorHistory.objects.create(field='bank_account', vendor=vendor, old_value=None, new_value=account,
                                             user=user, reason='App')
                vendor.bank_account = account
                action = 'Mapped-FO'
                account.add_bank_account_history(account, user, action, driver=None, vendor=vendor)
                self.update_auditlog(vendor, user)
                return account
            except exceptions.APIException as e:
                logger.info('APIException: %s' % e)
                raise exceptions.APIException(
                    _('Failed to create bank account')
                )

    def update_work_preferences(self, data, vendor_pk):
        """
        :param data:
        :param vendor_pk:
        :return:
        """
        vendor_qs = Vendor.objects.filter(pk=vendor_pk)
        try:
            vendor_qs.update(working_preferences=data.get('working_preferences'), modified_date=timezone.now())
        except exceptions.APIException as e:
            logger.info('APIException: %s' % e)
            raise exceptions.APIException(
                _('Failed to work preferences of vendor')
            )

    def update_vendor_status(self, vendor, status, action_by, reason_for_inactive=None,
                             remarks=None):
        """
        :param vendor:
        :param status:
        :param action_by:
        :return:
        """
        with transaction.atomic():
            user = vendor.user
            if not user:
                phone_number = vendor.phone_number
                email = '%s@%s' % (phone_number.national_number,
                                   settings.DEFAULT_DOMAINS.get('vendor'))
                user = User.objects.filter(email=email).first()
                if not user:
                    user = CustomerManager().upsert_user(
                        email=email,
                        name=vendor.name,
                        phone_number=str(phone_number.national_number),
                        password='1234',
                        is_mobile_verified=False
                    )
                vendor.user = user
            user.is_active = True if status == ACTIVE else False
            vendor.status = status
            vendor.modified_date = timezone.now()
            vendor.modified_by = action_by
            if reason_for_inactive is not None:
                vendor.reason_for_inactive = reason_for_inactive
            if remarks is not None:
                vendor.remarks = remarks
            try:
                user.save()
                vendor.save()
            except exceptions.APIException as e:
                logger.info('APIException: %s' % e)
                raise exceptions.APIException(
                    _('Failed to update vendor status')
                )

        return vendor

    def _generate_vendor_history(self, vendor, vendor_new_data, history_fields, user):
        """
            Make entries in the vendor history table for the modified data
        """

        vendor_list = []

        for field in vendor._meta.fields:
            field_name = field.__dict__.get('name')
            if field_name in history_fields:
                old_value = str(getattr(vendor, field_name))
                current_value = str(vendor_new_data.get(field_name, None))

                if old_value != current_value:
                    if field_name == 'bank_account':
                        current_value = "%s-%s-%s" % (
                            vendor_new_data.get('account_name'), vendor_new_data.get('account_number'),
                            vendor_new_data.get('ifsc_code'))
                    vendor_list.append(VendorHistory(field=field_name,
                                                     old_value=old_value,
                                                     new_value=current_value,
                                                     user=user, vendor=vendor, reason='App'))

        return vendor_list
