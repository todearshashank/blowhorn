# Django and System libraries
import logging
import requests
from dateutil import parser
import phonenumbers
import re
from django.core.exceptions import ValidationError
from django import db
from django.conf import settings as setting

# blowhorn imports
from blowhorn.apps.operation_app.v3.helper import *
from blowhorn.driver import messages
from config.settings import status_pipelines as StatusPipeline
from blowhorn.address.models import City
from blowhorn.apps.operation_app.v3.serializers.driver import *
from blowhorn.driver.models import DriverVehicleMap, DriverHistory, \
    DriverReferralConfiguration, BackGroundVerification, \
    DriverPreferredLocation
from blowhorn.apps.operation_app.v3.constants import *
from blowhorn.apps.operation_app.v3.services.tasks import *
from blowhorn.vehicle.models import VehicleDocument
from blowhorn.driver.models import DriverDocument
from blowhorn.driver.constants import SENT_FOR_APPROVAL, \
    DISAPPROVED, SENT_FOR_VERIFICATION, USER_ACCESS_APP
from blowhorn.address.permissions import is_city_manager
from blowhorn.apps.operation_app.v3.serializers.vehicle import *
from blowhorn.address.utils import AddressUtil
from blowhorn.contract.models import ResourceAllocation
from blowhorn.common.helper import CommonHelper
from blowhorn.driver import constants as DrivConst
from blowhorn.document.constants import ACTIVE
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.apps.operation_app.v5.constants import COSMOS_SCREEN_URLS, \
    NOTIFICATION_GROUP_EXTERNAL_VERIFICATION, REQUESTED, APPROVED
from blowhorn.address.models import State

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverAddUpdateMixin(object):

    def __create_data_for_driver(self, params, **kwargs):
        """
            Create/Update the related records of the driver.
        """

        driver = kwargs.get('driver', None)
        is_update = kwargs.get('is_update', False)

        data = {}
        vehicle = None
        detailed_data = get_detailed_data(params)
        logging.info(
            "This is the detailed data for adding the driver : %s "
            % detailed_data)

        driver_data = detailed_data.get('driver_data', {})
        vehicle_data = detailed_data.get('vehicle_data', {})
        user_data = detailed_data.get('user_data', {})
        vendor_data = detailed_data.get('vendor_data', {})
        current_address_data = detailed_data.get('current_address', {})
        permanent_address_data = detailed_data.get('permanent_address', {})

        city = City.objects.filter(
            name=driver_data.get('operating_city')).first()

        if city:
            driver_data['operating_city'] = city.id

        if driver:
            user_data['user_id'] = driver.user_id

        with db.transaction.atomic():

            try:
                user = get_user_for_driver(user_data)
            except Exception as e:
                logging.info(
                    "This is the error while creating the user : %s " % e)
                raise db.IntegrityError('Driver Phone number already exist')
            driver_data['user'] = user.id

            # if params.get('driver_activation', False):
            if not driver_data.get('own_vehicle', False) and is_update:
                try:
                    vendor = add_vendor(vendor_data)
                except Exception as e:
                    logging.info(
                        "Following error occured while creating vendor : %s "
                        % str(e))
                    raise ValidationError('Please Enter valid vendor details')
                driver_data['owner_details'] = vendor.id
                data['vendor'] = vendor

            if vehicle_data.get('registration_certificate_number'):
                try:
                    vehicle = add_vehicle(
                        vehicle_params=vehicle_data, is_update=is_update)
                except Exception as e:
                    logging.info(
                        "Following error occured while creating "
                        "vehicle : %s" % str(e))
                    raise db.IntegrityError(VEHICLE_EXIST)

            if params.get('is_vehicle_doc_available', False) and not vehicle:
                raise ValidationError(
                    "Vehicle document cannot be uploaded. Enter vehicle "
                    "details first")

            if current_address_data:
                try:
                    current_address = add_address_details_for_driver(
                        current_address_data)
                except Exception as e:
                    logging.info(
                        "Following error occurred while creating "
                        "current_address : %s " % str(e))
                    raise ValidationError('Please Enter valid current address')
                driver_data['current_address'] = current_address.id

            if permanent_address_data:
                try:
                    permanent_address = add_address_details_for_driver(
                        permanent_address_data)
                except Exception as e:
                    logging.info(
                        "Following error occurred while creating "
                        "permanent_address : %s " % str(e))
                    raise ValidationError(
                        'Please Enter valid permanent address')
                driver_data['permanent_address'] = permanent_address.id

            data['vehicle'] = vehicle
            data['driver_data'] = driver_data

        return data

    def __update_status(self, driver_data, driver, request):

        status = driver_data.get('status')

        if driver.status != status:
            # If driver is set as active
            # set active date to current day
            if status == StatusPipeline.ACTIVE:
                if not request.user.is_supply_user(driver.operating_city):
                    raise ValidationError(messages.RESTRICTED_ACCESS_ON_ACTIVATE)
                missing_fields = []
                for i in DRIVER_MANDATORY_FIELDS:
                    if not driver_data.get(i):
                        missing_fields.append(i)
                if not driver_data.get('is_address_same') and not driver_data.get('permanent_address'):
                    missing_fields.append('permanent_address')
                if missing_fields:
                    raise ValidationError(messages.MANDATORY_FIELD_FOR_ACTIVATION % ', '.join(missing_fields))
                driver.active_from = timezone.now()
                # In case of reactive of driver set leaving date as none
                if driver.date_of_leaving:
                    driver_data["date_of_leaving"] = None
                    driver_data["reason_for_inactive"] = None
                    driver_data["description"] = None

            elif status == StatusPipeline.INACTIVE:
                if not driver_data.get('reason_for_inactive', None):
                    raise ValidationError(messages.INACTIVE_REASON_MANDATORY)

                # if not description:
                #     raise ValidationError(
                #         messages.INACTIVE_DESCRIPTION_MANDATORY)

                driver_data["date_of_leaving"] = timezone.now()

        return driver

    def __auto_update_driver_status(self, driver):
        """
            Update the driver status if all the documents of the driver are
            Active and if driver is not in Active or Inactive state
        """
        if driver.status not in [StatusPipeline.ACTIVE,
                                 StatusPipeline.INACTIVE, StatusPipeline.BLACKLISTED]:
            driver_documents = DriverDocument.objects.filter(driver=driver.pk)
            active_doc = 0
            # If all the driver documents are verified(active)
            # then update the driver status to 'Onboarding'
            if driver_documents:
                for document in driver_documents:
                    if document.status == StatusPipeline.ACTIVE:
                        active_doc += 1

                if len(driver_documents) == active_doc:
                    driver.status = StatusPipeline.ONBOARDING
                    driver.save()

        return driver

    def __post_creation_action(self, params, driver, vehicle):
        """
            Tasks which are needed to be performed once the driver is
            created/updated successfully
        """

        # Update the vehicle details(* if any changes are made) in the driver
        # vehicle map
        if vehicle:
            if not driver.driver_vehicle or (
                    driver.driver_vehicle != vehicle.registration_certificate_number):
                DriverVehicleMap.objects.create(driver=driver, vehicle=vehicle)

        # Update the preferred location if needed
        if params.get('address_model', []):
            add_preferred_location(params['address_model'], driver)

        # Update the driver referral if needed
        if driver.referred_by and driver.status == StatusPipeline.ACTIVE:
            DriverReferrals.objects.filter(driver=driver).update(
                activation_timestamp=datetime.datetime.now())

        response_dict = {'driver_id': driver.id,
                         'vehicle_id': vehicle.id if vehicle else '-1',
                         'status': driver.status}

        return response_dict

    def _generate_driver_history(self, driver, driver_new_data, vendor, user):
        """
            Make entries in the driver history table for the modified data
        """

        driver_list = []

        for field in driver._meta.fields:
            field_name = field.__dict__.get('name')
            if field_name in DRIVER_FIELD_ALLOWED_FOR_UPDATE:
                old_value = str(getattr(driver, field_name))
                current_value = str(driver_new_data.get(field_name, None))
                if field_name == 'owner_details':
                    old_value = field.value_from_object(driver)
                    current_value = vendor

                if old_value != current_value:
                    if field_name == 'owner_details':
                        old_value = "%s | %s" % (
                            driver.owner_details.name,
                            driver.owner_details.phone_number) if \
                            driver.owner_details else None
                        current_value = "%s | %s" % (
                            vendor.name, vendor.phone_number) if \
                            vendor else None

                    driver_list.append(DriverHistory(field=field_name,
                                                     old_value=old_value,
                                                     new_value=current_value,
                                                     user=user, driver=driver))

        return driver_list

    def _update_driver(self, params, driver, request):
        """
            For updating the already existing driver
        """

        detailed_data = self.__create_data_for_driver(
            params=params, driver=driver, is_update=True)
        driver_data = detailed_data.get('driver_data', {})
        vendor = detailed_data.get('vendor', {})
        # status = driver_data.pop('status')
        city = driver_data.get('operating_city', None)
        is_referred = False

        if driver.status == StatusPipeline.REFERRED and driver.referred_by:
            is_referred = True
            referral_configuration = DriverReferralConfiguration.objects.filter(
                city_id=city).last()

            if not referral_configuration:
                logging.info("No referral configuration defined for this city")
                raise ValidationError(
                    "No Referral Configuration defined for this city")

            driver_data['referral_config'] = referral_configuration.id
            driver_data['status'] = StatusPipeline.SUPPLY

        driver = self.__update_status(driver_data, driver, request)

        driver_history_list = self._generate_driver_history(
            driver, driver_data, vendor, request.user)

        logging.info(
            "This is the driver data for the serialization : %s "
            % driver_data)

        updated_driver = UpdateModelMixin().update(
            data=driver_data, instance=driver,
            serializer_class=DriverSerializer)

        vehicle = detailed_data.get('vehicle')

        updated_driver = self.__auto_update_driver_status(updated_driver)
        response = self.__post_creation_action(params, updated_driver, vehicle)

        try:
            DriverBackGroundVerificationMixin(
            ).get_data_for_background_verification(driver)
            data_verification_ready = True
            bgv_status = DrivConst.READY_FOR_VERIFICATION
        except Exception as e:
            logging.info(
                "This is the issue with data validation : %s " % e)
            data_verification_ready = False
            bgv_status = None

        if is_referred:
            Driver.objects.filter(id=driver.id).update(
                created_by=request.user, modified_by=request.user,
                data_ready_for_bgv=data_verification_ready,
                bgv_status=bgv_status)
        else:
            Driver.objects.filter(id=driver.id).update(
                modified_by=request.user,
                data_ready_for_bgv=data_verification_ready,
                bgv_status=bgv_status)

        if driver_history_list:
            DriverHistory.objects.bulk_create(driver_history_list)

        logging.info('Driver Updated')
        return response

    def _create_driver(self, params, request):
        """
                For creating a new driver
        """

        try:
            phone = phonenumbers.parse(params.get('mobile', ''),
                                       setting.COUNTRY_CODE_A2)
        except:
            phone = ''

        filter_data = {
            'name': params.get('name', ''),
            # 'driver_vehicle': params.get('driver_vehicle', ''),
            'user__phone_number': phone
        }

        driver = Driver.objects.filter(**filter_data).first()

        if driver:
            logging.info(
                "Driver activation details needed")
            raise ValidationError(
                "Driver already exists. Needs to be activated")

        detailed_data = self.__create_data_for_driver(params)
        logging.info(
            "This is the detailed data for adding the driver : %s "
            % detailed_data)
        driver_data = detailed_data.get('driver_data', {})

        if request.META.get(
                'HTTP_DEVICE_TYPE', '') in setting.USER_CLIENT_TYPES:
            driver_data['source'] = USER_ACCESS_APP

        created_driver = CreateModelMixin().create(
            data=driver_data, serializer_class=DriverSerializer)
        created_driver.created_by = request.user
        created_driver.modified_by = request.user
        created_driver.save()

        vehicle = detailed_data.get('vehicle')

        response = self.__post_creation_action(
            params, created_driver, vehicle)
        logging.info('Driver Created')

        return response

    def _deactivate_driver(self, params, driver, request):
        """
            For changing the status of a driver to Inactive/Blacklisted
        """
        response_dict = {}

        detailed_data = get_detailed_data(params)
        logging.info(
            "This is the detailed data for adding the driver : %s "
            % detailed_data)

        driver_data = detailed_data.get('driver_data', {})

        driver = self.__update_status(driver_data, driver, request)

        driver_history_list = self._generate_driver_history(
            driver=driver,
            driver_new_data=driver_data,
            vendor=None,
            user=request.user)

        driver_status_data = {
            'status': driver_data['status'],
            'reason_for_inactive': driver_data['reason_for_inactive'],
            'description': driver_data['description']
        }

        logging.info(
            "This is the driver data for the serialization : %s "
            % driver_status_data)

        with transaction.atomic():

            updated_driver = UpdateModelMixin().update(
                data=driver_status_data, instance=driver,
                serializer_class=DriverSerializer, partial=True)

            if driver_history_list:
                DriverHistory.objects.bulk_create(driver_history_list)

            logging.info('Updated the status of the driver')

            response_dict = {'driver_id': updated_driver.id,
                             'status': driver.status}

        return response_dict


class DriverBackGroundVerificationMixin(object):

    def get_verification_resultant_data(self, data):
        verification_common_data = {
            'client_status': getattr(StatusPipeline, data['clientStatus'],
                                     None) if data.get(
                'clientStatus') else None,
            'bpss_status': getattr(StatusPipeline, data['bpssStatus'],
                                   None) if data.get('bpssStatus') else None,
            'reference_id': data.get('referenceId', None)
        }

        return verification_common_data

    def get_verified_address_details(self, address_data):
        data = address_data[0]
        address_details = {
            'bgv_status': getattr(StatusPipeline, data['status'],
                                  StatusPipeline.PENDING) if data.get(
                'status') else StatusPipeline.PENDING,

            'bgv_sub_status': getattr(
                StatusPipeline, data['subStatus'], None)
            if data.get('subStatus') else None,

            'remark': data.get('remark', ''),

            'verification_requested_on': parser.parse(data['requestedOn'])
            if data.get('requestedOn') else None,

            'verification_completed_on': parser.parse(data['completedOn'])
            if data.get('completedOn') else None,

            'verification_extra_details': data.get('moreDetails', None)
        }
        return address_details

    def get_verified_document_details(self, data):
        document_details = {
            'number': data.get('detail'),
            'bgv_status': getattr(StatusPipeline, data['status'],
                                  StatusPipeline.PENDING)
            if data.get('status') else StatusPipeline.PENDING,
            'bgv_remark': data.get('remark', ''),
            'verification_requested_on': parser.parse(data['requestedOn'])
            if data.get('requestedOn') else None,
            'verification_completed_on': parser.parse(data['completedOn'])
            if data.get('completedOn') else None,
            'verification_extra_details': data.get('moreDetails')
        }

        return document_details

    def get_verified_court_record(self, court_data, bgv_obj):
        data = court_data[0]
        verification_requested_on = parser.parse(
            data['requestedOn']) if data.get('requestedOn') else None
        verification_completed_on = parser.parse(
            data['completedOn']) if data.get('completedOn') else None
        verified_court_record_data = []

        for val in data.get('moreDetails', {}).get('result', {}):
            court_records = data['moreDetails']['result'][val]
            court_type = getattr(DrivConst, val, None)

            for record in court_records:
                try:
                    state = State.objects.get(name=record.get('state', None))
                except:
                    state = None

                court_data = {
                    'background_verification': bgv_obj,
                    'court_type': court_type,
                    'state': state,
                    'name': record.get('name'),
                    'case_type': record.get('caseType'),
                    'case_number': record.get('caseNo'),
                    'case_date': record.get('caseDate'),
                    'remark': record.get('remark'),
                    'district': record.get('district'),
                    'criminal_status': record.get('isCriminal'),
                    'address_type': getattr(DrivConst, record['addressType'])
                    if record.get('addressType') else None,
                    'court_verification_requested_on':
                    verification_requested_on,
                    'court_verification_completed_on':
                    verification_completed_on
                }

                verified_court_record_data.append(CourtRecord(**court_data))

        return verified_court_record_data

    def driver_data_validation_for_bgv(self, driver):

        errors = []

        if not driver.name:
            errors.append(
                "Name of the driver")

        if not driver.father_name:
            errors.append(
                "Father's name of driver")

        if not driver.gender:
            errors.append(
                "Gender of the driver")

        if not driver.operating_city:
            errors.append(
                "Operating City of the driver")

        if not driver.date_of_birth:
            errors.append(
                "Date of Birth of the driver")

        if not driver.current_address:
            errors.append(
                "Current address of the driver")

        if not driver.is_address_same and not driver.permanent_address:
            errors.append("Need Permanent address as "
                          "it is not same as current address")

        if errors:
            raise ValidationError(errors)

        return True

    def get_validated_document_data_for_bgv(self, driver):
        document_data_list = []
        documents = []

        # to get the latest active or pending document we are orderinng by created date desc
        driver_documents = DriverDocument.objects.filter(driver=driver, status=ACTIVE,
                        document_type__code__in=list(BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING.values())
                            ).order_by('-document_type', '-created_date').distinct('document_type')

        vehicle = driver.vehicles.all().first()
        vehicle_documents = VehicleDocument.objects.filter(
            vehicle=vehicle,  status=ACTIVE,
            document_type__code__in=list(
                BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING.values())
            ).order_by('-document_type', '-created_date').distinct('document_type')

        documents.extend(list(driver_documents))
        documents.extend(list(vehicle_documents))

        if not documents:
            raise ValidationError("No document applicable for verification "
                                  "is uploaded for the driver")

        mark_document_id_inactive = []
        inactive_marked_document_name = []
        for document in documents:
            if not document.number or document.number.startswith('-') or \
                    document.number.count(" "):
                raise ValidationError(
                    "Enter valid document number without space"
                    "for " + document.document_type.name)

            if document.document_type.code in BETTERPLACE_DRIVER_DOCUMENT_CODE and \
                not re.match(document.document_type.regex, str(document.number)):
                mark_document_id_inactive.append(document.id)
                inactive_marked_document_name.append(document.document_type.name)

            if document.document_type.code in \
                    STATE_REQUIRED_FOR_THE_DOCUMENTS and not document.state:
                raise ValidationError(
                    "Name of the state is needed "
                    "for " + str(document) + " for verification")

            document_data = {
                "action": "",
                "name": "",
                "docType": BETTERPLACE_REVERSE_DOCUMENT_TYPE_CODE_MAPPING[
                    document.document_type.code],
                "docNo": document.number,
                "state": document.state.name if document.state else "",
                "clientRefId": ""
            }

            document_data_list.append(document_data)

        if mark_document_id_inactive and driver_documents.exists():
            driver_documents.filter(id__in=mark_document_id_inactive).update(status=INACTIVE)
            raise ValidationError(
                    "Document number is invalid for " + " ".join(inactive_marked_document_name))

        return document_data_list

    def get_driver_address(self, address, address_type):

        if address and not address.city:
            raise ValidationError("City is needed in address for verification")

        if address and not address.state:
            raise ValidationError(
                "State is needed in address for verification")

        address_data = {
            "addressType": address_type,
            "line1": address.line1 if address else "",
            "line2": address.line2 if address else "",
            "city": address.city if address else "",
            "state": address.state if address else "",
            "pincode": address.postcode if address else "",
            "same": ""
        }

        if address_type == 'PERMANENT' and not address:
            address_data["same"] = "true"

        return address_data

    def get_data_for_background_verification(self, driver_obj):
        bgv_data_format = {}
        address_list = []

        self.driver_data_validation_for_bgv(driver_obj)
        document_data = self.get_validated_document_data_for_bgv(
            driver_obj)

        bgv_data_format["fName"] = driver_obj.name
        bgv_data_format["fatherName"] = driver_obj.father_name
        bgv_data_format["gender"] = driver_obj.gender.upper()
        bgv_data_format["location"] = driver_obj.operating_city.name
        bgv_data_format["mobile"] = str(
            driver_obj.user.phone_number.national_number)
        bgv_data_format["dob"] = driver_obj.date_of_birth.strftime("%d-%m-%Y")

        address_list.append(self.get_driver_address(
            driver_obj.current_address, 'PRESENT'))
        address_list.append(self.get_driver_address(
            driver_obj.permanent_address, 'PERMANENT'))

        bgv_data_format["addresses"] = address_list
        bgv_data_format["documents"] = document_data

        return bgv_data_format

    def create_bgv_profile(self, bgv_data):
        create_profile_url = getattr(
            settings, 'BETTERPLACE_ENDPOINT_CREATE_PROFILE', None)
        api_key = getattr(settings, 'BETTERPLACE_API_KEY', None)

        headers = {"Content-Type": "application/json", "ApiKey": api_key}

        logging.info(
            'This is the input data for the create profile '
            'of the betterplace : %s' % (json.dumps(bgv_data), ))

        try:
            response = requests.post(
                create_profile_url, data=json.dumps(bgv_data), headers=headers)
        except Exception as e:
            logging.info("This is the error while creating BGV Profile : ", e)
            response = None

        logging.info('Below is the response %s' % (response, ))

        return response

    def get_verified_data(self, reference_id):
        response_data = {}
        verification_status_url = getattr(
            settings, 'BETTERPLACE_ENDPOINT_VERIFICATION_STATUS', None)

        api_key = getattr(settings, 'BETTERPLACE_API_KEY', None)
        input_data = [{
            'id': reference_id
        }]
        headers = {"Content-Type": "application/json", "ApiKey": api_key}
        logging.info(
            'This is the url fetching the verification status '
            'of the betterplace : %s' % (verification_status_url, ))

        try:
            response = requests.post(
                verification_status_url, data=json.dumps(input_data),
                headers=headers)
        except:
            response = None

        logging.info('Below is the response %s' % (response, ))

        if response and response.status_code == 202:
            logging.info('This is the response content : %s' % response.json())
            response_data = response.json()
        else:
            raise Exception("No/Bad response obtained")

        return response_data

    def get_background_verification_status(self, bgv_object, reference_id):
        verified_data = self.get_verified_data(reference_id)
        data = verified_data.get('data', [])
        if not data:
            raise Exception("No data found for the verified driver")
        data = data[0]

        with transaction.atomic():
            try:
                bgv_data = self.get_verification_resultant_data(data)
                bgv_object.update(**bgv_data)
                bgv_obj = bgv_object.first()

                driver = bgv_obj.driver
                data_report = data.get('report', {})

                if data.get('client_status') in \
                        DrivConst.BACKGROUND_VERIFICATION_STATUS_FOR_FAIL:
                    Driver.objects.filter(id=driver.id).update(
                        bgv_status=DrivConst.VERIFICATION_FAILED)
                elif data.get('client_status') == 'VERIFIED':
                    Driver.objects.filter(id=driver.id).update(
                        bgv_status=DrivConst.VERIFICATION_PASSED)

                # To check if the address details are there in the
                # verification result or not and if it's there then
                # update the address record
                if "ADDRESS" in data_report:
                    if "CURRENT_ADDRESS" in data_report.get(
                            "ADDRESS", {}).get("result", {}):
                        current_address_data = self.get_verified_address_details(
                            data_report["ADDRESS"]["result"].get(
                                "CURRENT_ADDRESS", []))

                        if driver.current_address:
                            DriverAddress.objects.filter(
                                id=driver.current_address_id).update(
                                **current_address_data)

                    if "PERMANENT_ADDRESS" in data_report.get(
                            "ADDRESS", {}).get("result", {}):
                        permanent_address_data = self.get_verified_address_details(
                            data_report["ADDRESS"]["result"].get(
                                "PERMANENT_ADDRESS", []))
                        if driver.current_address:
                            DriverAddress.objects.filter(
                                id=driver.current_address_id).update(
                                **permanent_address_data)

                # To check if the document details are there in the
                # verification result or not and if it's there then update the
                # various document record submitted for the driver verification

                if "DOCUMENT" in data_report:
                    for val in BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING.keys():
                        if val in data_report.get("DOCUMENT", {}).get(
                                "result", {}):
                            for doc_data in data_report["DOCUMENT"][
                                    "result"].get(val):
                                if doc_data.get('detail', ''):
                                    document_data = \
                                        self.get_verified_document_details(doc_data)
                                    if val in BETTERPLACE_DRIVER_DOCUMENT_CODE:
                                        DriverDocument.objects.filter(
                                            number=doc_data['detail']).update(
                                            **document_data)
                                    else:
                                        VehicleDocument.objects.filter(
                                            number=doc_data['detail']).update(
                                            **document_data)

                # To check if the criminal record are there in the verification
                # result or not and if it's there then update the
                # various court records provided after the verification
                if "CRIMINAL" in data_report:
                    if "COURT_RECORD" in data_report.get("CRIMINAL", []).get(
                            "result", {}):
                        court_data = self.get_verified_court_record(
                            data_report["CRIMINAL"]["result"]["COURT_RECORD"],
                            bgv_obj)

                        if court_data:
                            CourtRecord.objects.bulk_create(court_data)
            except:
                raise Exception(
                    'Something went wrong while creating the record')

        return reference_id

    def background_verification_processing(self, user, city, driver_obj,
                                           driver_details, request):
        response_dict = {'status': False, "message": ''}
        user_to_be_notified = []
        driver = driver_obj.first()

        if driver.bgv_status == SENT_FOR_VERIFICATION:
            raise ValidationError("Driver already sent for verification")

        if driver.status == StatusPipeline.INACTIVE:
            raise ValidationError(
                "Inactive Driver cannot be sent for verification")

        if not driver_details.get('for_verification', False):
            if user.is_active and (
                    is_city_manager(city, user) or user.is_superuser):
                user_to_be_notified.append(driver.bgv_requested_by.id)

                if driver.bgv_status == DISAPPROVED:
                    raise ValidationError(
                        "Driver has already been disapproved for verification")

                if not driver_details.get('is_approved', False):
                    if not driver_details.get('reason', ''):
                        raise ValidationError(
                            "Reason for Unapproval is needed")

                    driver_obj.update(
                        approved_for_bgv=False,
                        bgv_unapproval_reason=driver_details['reason'],
                        bgv_reviewer=user,
                        bgv_status=DISAPPROVED,
                        data_ready_for_bgv=True,
                        modified_by=request.user,
                        modified_date=timezone.now()
                    )

                    extra_context = {
                        'driver_id': driver.id,
                        'subject': '%s' % driver,
                        'action_by': user.name,
                        'redirect_url': COSMOS_SCREEN_URLS.get(
                            NOTIFICATION_GROUP_EXTERNAL_VERIFICATION),
                        'remarks': driver_details.get('reason', '')
                    }
                    NotificationMixin().save_and_broadcast_notification(
                        action_owners=[],
                        broadcast_list=user_to_be_notified,
                        action_by=user,
                        action=DISAPPROVED,
                        extra_context=extra_context,
                        notification_group=NOTIFICATION_GROUP_EXTERNAL_VERIFICATION,
                        old_notification_pk=request.data.get('notification_id',
                                                             None)
                    )
                    response_dict['message'] = 'Approval rejected for ' \
                                               'verification'
                    response_dict['status'] = True
                    return response_dict

                bgv_data = self.get_data_for_background_verification(driver)
                response = self.create_bgv_profile(bgv_data)
                response_data = response.json()

                logging.info("This is the response from betterplace : %s" %
                             response.json())

                if not (response and response.status_code == 202):
                    errors = []
                    messages = response_data.get('messages')

                    for msg in messages:
                        if msg.get('message', ''):
                            errors.append(msg['message'])

                    if errors:
                        raise ValidationError(errors)
                    else:
                        raise ValidationError(
                            "Failed to create driver's profile")

                reference_id = response_data.get(
                    'data', {}).get('referenceId', None)

                if not reference_id:
                    raise ValidationError("No Reference id received")

                document_type = []
                for doc in bgv_data.get('documents'):
                    if doc.get('docType'):
                        document_type.append(doc['docType'])

                bgv_response_data = {
                    'driver': driver.id,
                    'reference_id': reference_id,
                    'source': USER_ACCESS_APP,
                    'document_type': document_type
                }

                created_bgv = CreateModelMixin().create(
                    data=bgv_response_data,
                    serializer_class=BackGroundVerificationSerializer)

                BackGroundVerification.objects.filter(
                    id=created_bgv.id).update(
                    created_by=request.user, modified_by=request.user)

                driver_obj.update(approved_for_bgv=True,
                                  bgv_unapproval_reason=None,
                                  bgv_reviewer=user,
                                  bgv_status=SENT_FOR_VERIFICATION,
                                  data_ready_for_bgv=False,
                                  modified_by=request.user,
                                  modified_date=timezone.now())

                extra_context = {
                    'driver_id': driver.id,
                    'subject': '%s' % driver,
                    'action_by': user.name,
                    'redirect_url': COSMOS_SCREEN_URLS.get(
                        NOTIFICATION_GROUP_EXTERNAL_VERIFICATION),
                }
                NotificationMixin().save_and_broadcast_notification(
                    action_owners=[],
                    broadcast_list=user_to_be_notified,
                    action_by=user,
                    action=APPROVED,
                    extra_context=extra_context,
                    notification_group=NOTIFICATION_GROUP_EXTERNAL_VERIFICATION,
                    old_notification_pk=request.data.get('notification_id', None)
                )

                response_dict['message'] = \
                    "Driver's Profile Created for Background Verification"
                response_dict['status'] = True
                return response_dict
            else:
                raise ValidationError("You don't have the permissions to "
                                      "approve/disapprove the verification")

        else:
            if user.is_active and user.is_staff:
                if driver.bgv_status == SENT_FOR_APPROVAL:
                    raise ValidationError(
                        "Driver's already sent for Background "
                        "Verification approval")

                city_managers = list(
                    city.managers.all().values_list('id', flat=True))
                users_to_be_notified = list(User.objects.filter(
                    is_superuser=True).exclude(
                    id__in=city_managers).values_list('id', flat=True))
                users_to_be_notified.extend(city_managers)

                driver_obj.update(bgv_status=SENT_FOR_APPROVAL,
                                  bgv_requested_by=user,
                                  data_ready_for_bgv=False,
                                  modified_by=request.user,
                                  modified_date=timezone.now())

                extra_context = {
                    'driver_id': driver.id,
                    'subject': '%s' % driver,
                    'action_by': user.name,
                    'redirect_url': COSMOS_SCREEN_URLS.get(
                        NOTIFICATION_GROUP_EXTERNAL_VERIFICATION),
                }
                NotificationMixin().save_and_broadcast_notification(
                    action_owners=users_to_be_notified,
                    broadcast_list=[],
                    action_by=user,
                    action=REQUESTED,
                    extra_context=extra_context,
                    notification_group=NOTIFICATION_GROUP_EXTERNAL_VERIFICATION
                )

                response_dict['message'] = \
                    'Notification sent to the SuperUsers and City Managers'
                response_dict['status'] = True
                return response_dict
            else:
                raise ValidationError("You don't have the permissions to "
                                      "send the driver for verification")


class DriverListMixin(object):

    def create_driver_detail_dict(self, driver):

        dict_ = {
            'id': driver.get('id', ''),
            'name': driver.get('name', ''),
            'driver_vehicle': driver.get('driver_vehicle', ''),
            'mobile': str(driver['user__phone_number']) if driver.get(
                'user__phone_number') else '',
            'driver_is_owner': driver.get('own_vehicle', False),
            'owner_contact_no': driver['owner_details__phone_number']
            if driver.get('owner_details__phone_number', '') else '',
            'working_preferences': driver['working_preferences']
            if driver.get('working_preferences', []) else [],
            'can_drive_and_deliver': driver.get(
                'can_drive_and_deliver', False),
            'can_read_english': driver.get('can_read_english', False),
            'driving_license_number': driver['driving_license_number']
            if driver.get('driving_license_number') else '',
            'can_source_additional_labour': driver.get(
                'can_source_additional_labour', False),
            'work_status': driver['work_status'] if driver.get(
                'work_status', '') else '',
            'address_model': [],
            'status': driver.get('status'),
            'vehicles': [],
            'operating_city': driver.get('operating_city__name', ''),
            'owner_pan_number': driver['owner_details__pan'] if driver.get(
                'owner_details__pan') else '',
            'owner_name': driver['owner_details__name'] if driver.get(
                'owner_details__name') else '',
            'verification_ready': driver.get('data_ready_for_bgv', False),
            'bgv_status': driver.get('bgv_status'),
            'date_of_birth': driver['date_of_birth'].strftime("%d/%m/%Y")
            if driver.get('date_of_birth') else '',
            'father_name': driver['father_name'] if driver.get(
                'father_name') else '',
            'allocated_to': [],
            'current_address': self.get_current_address(driver)
            if driver.get('current_address') else {},
            'permanent_address': self.get_permanent_address(driver)
            if driver.get('permanent_address') else {},
            'pan_number': driver.get('pan'),
            'prior_income': driver['prior_monthly_income']
            if driver.get('prior_monthly_income') else '',
            'gender': driver.get('gender', ''),
            'is_address_same': driver['is_address_same']
            if driver.get('is_address_same') else True,
            'document_file_path': [],
            'reason_for_inactive': driver['reason_for_inactive']
            if driver.get('reason_for_inactive') else ''

        }

        return dict_

    def get_pref_loc(self, driver_pref_loc):

        coordinates = {}
        if driver_pref_loc.get('geopoint'):
            latlng_dict = AddressUtil().get_latlng_from_geopoint(
                driver_pref_loc['geopoint'])
            coordinates['latitude'] = latlng_dict.get('lat')
            coordinates['longitude'] = latlng_dict.get('lng')

        dict_ = {
            'coordinates': coordinates
        }

        return dict_

    def get_current_address(self, driver):
        current_address = {}
        coordinates = {}

        if driver.get('current_address__geopoint'):
            latlng_dict = AddressUtil().get_latlng_from_geopoint(
                driver.get('current_address__geopoint'))
            coordinates['latitude'] = latlng_dict.get('lat')
            coordinates['longitude'] = latlng_dict.get('lng')

        current_address = {
            'title': driver.get('current_address__title'),
            'first_name': driver.get('current_address__first_name'),
            'last_name': driver.get('current_address__last_name'),
            'fullAddress': driver.get('current_address__search_text'),
            'addressLine1': driver.get('current_address__line1'),
            'city': driver.get('current_address__line4'),
            'postalCode': driver.get('current_address__postcode'),
            'coordinates': coordinates,
            'state': driver.get('current_address__state'),
            'country': driver.get('current_address__country__name')
        }

        return current_address

    def get_permanent_address(self, driver):
        permanent_address = {}
        coordinates = {}

        if driver.get('permanent_address__geopoint'):
            latlng_dict = AddressUtil().get_latlng_from_geopoint(
                driver.get('permanent_address__geopoint'))
            coordinates['latitude'] = latlng_dict.get('lat')
            coordinates['longitude'] = latlng_dict.get('lng')

        permanent_address = {
            'title': driver.get('permanent_address__title'),
            'first_name': driver.get('permanent_address__first_name'),
            'last_name': driver.get('permanent_address__last_name'),
            'fullAddress': driver.get('permanent_address__search_text'),
            'addressLine1': driver.get('permanent_address__line1'),
            'city': driver.get('permanent_address__line4'),
            'postalCode': driver.get('permanent_address__postcode'),
            'coordinates': coordinates,
            'state': driver.get('permanent_address__state'),
            'country': driver.get('permanent_address__country__name')
        }

        return permanent_address

    def get_vehicle_details(self, vehicle_detail):
        dict_ = {
            'id': vehicle_detail.get('vehicle'),
            'body_type_name': vehicle_detail.get(
                'vehicle__body_type__body_type'),
            'vehicle_model': vehicle_detail.get(
                'vehicle__vehicle_model__model_name'),
            'vehicle_class_name': vehicle_detail.get(
                'vehicle__vehicle_model__vehicle_class__commercial_classification'),
            'vehicle_model_year': vehicle_detail.get(
                'vehicle__model_year'),
            'registration_certificate_number': vehicle_detail.get(
                'vehicle__registration_certificate_number')
        }
        return dict_

    def get_driver_related_details(self, driver_queryset):
        driver_id_list = []
        driver_dict = {}

        for driver in driver_queryset:
            driver_id_list.append(driver.get('id'))
            driver_dict[driver['id']] = self.create_driver_detail_dict(
                driver)

        driver_preferred_location = DriverPreferredLocation.objects.filter(
            driver_id__in=driver_id_list).values('driver_id', 'geopoint')

        for pref_loc in driver_preferred_location:
            driver_dict[pref_loc['driver_id']][
                'address_model'].append(self.get_pref_loc(pref_loc))

        driver_vehicle_map = DriverVehicleMap.objects.filter(
            driver_id__in=driver_id_list).select_related('vehicle').values(
            'driver', 'vehicle', 'vehicle__body_type__body_type',
            'vehicle__vehicle_model__model_name',
            'vehicle__vehicle_model__vehicle_class__commercial_classification',
            'vehicle__model_year',
            'vehicle__registration_certificate_number',)

        for driv_vehi in driver_vehicle_map:
            driver_dict[driv_vehi['driver']]['vehicles'].append(
                self.get_vehicle_details(driv_vehi))

        driver_resource_allocation = ResourceAllocation.objects.filter(
            drivers__in=driver_id_list).values(
            'drivers', 'contract__customer__name')

        for allocated in driver_resource_allocation:
            driver_dict[allocated['drivers']]['allocated_to'].append(
                allocated.get('contract__customer__name'))

        return {'driver_id_list': driver_id_list, 'driver_dict': driver_dict}

    def get_document_details(self, document):

        dict_ = {}

        if document.get('document_type__code') in list(
                BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING.values()):
            dict_ = {
                'file_path': "{0}{1}".format(
                    CommonHelper().get_media_url_prefix(), document['file'])
                if document.get('file', None) else '',
                'document_number': document['number'] if document.get(
                    'number', None) else ''
            }

        return dict_

    def get_driver_list(self, driver_queryset):
        driver_list = []
        driver_data = self.get_driver_related_details(driver_queryset)
        driver_id_list = driver_data.get('driver_id_list', [])
        driver_dict = driver_data.get('driver_dict', {})
        driver_list = [driver_dict[key] for key in driver_id_list]
        return driver_list

    def get_driver_detailed_list(self, driver_queryset):
        driver_list = []
        vehicle_dict = {}
        driver_data = self.get_driver_related_details(driver_queryset)
        driver_id_list = driver_data.get('driver_id_list', [])
        driver_dict = driver_data.get('driver_dict', {})

        vehicle_number_list = []
        vehicle_documents = []
        for key in driver_dict:
            vehicle_dict[driver_dict[key]['driver_vehicle']] = key
            vehicle_number_list = driver_dict[key]['driver_vehicle']

        driver_documents = DriverDocument.objects.filter(
            driver_id__in=driver_id_list).values(
            'driver', 'document_type__code', 'file', 'number')

        if vehicle_number_list:
            vehicle_documents = VehicleDocument.objects.filter(
                vehicle__registration_certificate_number__in=vehicle_number_list
            ).values(
                'vehicle__registration_certificate_number',
                'document_type__code', 'file', 'number'
            )

        for doc in driver_documents:
            path_dict = self.get_document_details(doc)
            if path_dict:
                driver_dict[doc['driver']][
                    'document_file_path'].append(path_dict)

        for doc in vehicle_documents:
            path_dict = self.get_document_details(doc)
            if path_dict:
                driver_id = vehicle_dict[
                    doc['vehicle__registration_certificate_number']]
                driver_dict[driver_id][
                    'document_file_path'].append(path_dict)

        driver_list = [driver_dict[key] for key in driver_id_list]

        return driver_list
