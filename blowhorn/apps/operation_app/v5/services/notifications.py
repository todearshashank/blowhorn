import logging
import json
from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

from django.db.models import Q, Case, When, Value, CharField
from django.conf import settings
from blowhorn.oscar.core.loading import get_model
from django.utils import timezone

from config.settings import status_pipelines as StatusPipeline
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.users.constants import NOTIFICATION_GROUP_ORDER
from blowhorn.utils.functions import utc_to_ist
from blowhorn.address.models import City
from blowhorn.trip.utils import TripService
from blowhorn.trip.models import TRIP_SUSPENSION_REASONS_VEHICLE_BREAKDOWN, \
    TRIP_SUSPENSION_REASONS_DRIVER_DENIED_DUTY, TRIP_SUSPENSION_REASONS_SICK, \
    TRIP_SUSPENSION_REASONS_FAMILY_ISSUE,\
    TRIP_SUSPENSION_REASONS_TRAFFIC_VIOLATION, \
    TRIP_SUSPENSION_REASONS_NO_FUEL, TRIP_SUSPENSION_REASONS_ACCIDENT, \
    TRIP_SUSPENSION_REASONS_CUSTOMER_CANCELLED,\
    TRIP_SUSPENSION_REASONS_TRIP_INTERRUPTED
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.apps.operation_app.v5.constants import NOTIFICATION_TEMPLATES, \
    REQUESTED, COSMOS_SCREEN_URLS, ASSIGN, CREATED

Driver = get_model("driver", "Driver")
Order = get_model("order", "Order")

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class SendNotification(object):

    def __init__(self, template_type, user, order):
        self.template_type = template_type
        self.order = order
        self.user = user
        self.action_owners = None
        self.action = None
        self.message_template = NOTIFICATION_TEMPLATES.get(
            NOTIFICATION_GROUP_ORDER, {}).get(self.template_type)
        if template_type in (REQUESTED, CREATED):
            if template_type == REQUESTED:
                self.action = 'Marked for contingency'
            else:
                self.action = 'Order created'
            self.action_owners = City.objects.filter(
                id=order.city.id,
                supply_users__isnull=False).values_list(
                'supply_users__id', flat=True)
            self.message = self.message_template.format(
                order=self.order.number,
                action_by=self.user.name
            )
        elif self.template_type == ASSIGN:
            self.action = 'Driver assigned'
            self.action_owners = [order.created_by.id]
            self.message = self.message_template.format(
                order=self.order.number,
                driver=order.driver.user.name
            )

    def notify_users(self):
        # try:
        screen_url = COSMOS_SCREEN_URLS.get(NOTIFICATION_GROUP_ORDER)
        extra_context = {
            'order_id': self.order.id,
            'action_by': self.user.name,
            'redirect_url': screen_url,
            'remarks': ''
        }

        if not self.action_owners:
            return Response(status=status.HTTP_200_OK)

        try:
            NotificationMixin().save_and_broadcast_notification(
                action_owners=list(self.action_owners),
                action_by=self.user,
                action=self.action,
                extra_context=extra_context,
                notification_group=NOTIFICATION_GROUP_ORDER,
                message=self.message
            )
        except:
            raise Exception('Something went wrong while sending notification')
