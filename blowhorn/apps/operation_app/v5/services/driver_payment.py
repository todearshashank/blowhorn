# Django and system libraries
import json
from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Sum, Min, Max
from django.db import transaction
from django.utils.html import format_html

# Blowhorn Imports
from blowhorn.driver.models import DriverPayment, DriverPaymentHistory, PaymentTrip, DriverLedger, \
    DriverPaymentAdjustment, DriverCashPaid, DriverPaymentAdjustmentHistory, WITHHELD, UNAPPROVED,\
    APPROVED, SUSPENDED, CLOSED, DATE_FORMAT
from blowhorn.apps.operation_app.v5.constants import PAYMENT_DETAIL_FIELDS_MAPPING, \
    UNAPPROVED_ACTION_STRING_MAPPING
from blowhorn.utils.functions import get_in_ist_timezone, minutes_to_dhms
from blowhorn.driver.constants import REJECTED, NEW, TRIP_PACKAGES_LIST
from blowhorn.apps.operation_app.v5.services.payment_transition_conditions import \
    is_payment_supervisor, is_payment_spoc_or_supervisor, is_super_user, can_unapprove_payment, \
    can_approve_payment, is_current_user_can_approve, can_approve_adjustment, \
    can_unapprove_adjustment, is_adjustment_spoc_or_supervisor, is_adjustment_supervisor
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, BASE_PAYMENT_CYCLE_PER_TRIP
from blowhorn.driver import messages


t_base = """Base Amount:
  %2d days x %-9.2f\t\t\t\t%.2f
"""

t_trip_basepay = """   %s:\t\t\t\t%.2f
"""

trip_wise_calculation = """ \n%s \n  Base amount  \t\t\t\t\t%.2f\n"""

t_deduction = """Deductions:
  %2d days x %-9.2f\t\t\t%.2f
\n"""

t_variable = """  %s: %d %s [Slab: %2d - %2d] @ %-9.2f %.2f
"""
tw_variable = """  %s: %d %s [Slab: %2d - %2d] @ %-9.2f %.2f
"""

ruler = """---------------------------------------------------
"""
gross_pay = """%-20s\t\t\t%.2f
"""

trip_amount = """Trip Payment \t\t\t\t%.2f
"""

labour_and_toll = """  %-20s\t\t\t%.2f
"""

order_cash_collected = """Cash Collected \t\t\t\t%.2f\n"""

total_cash_collected = """Total Cash Collected \t\t\t%.2f\n"""

earnings = """ Earnings \t\t\t%.2f\n"""


def construct_trip_data(driver_payment):
    paymenttrip = driver_payment.paymenttrip_set.all()
    paymenttrip = paymenttrip.select_related('trip')

    try:
        data = json.loads(driver_payment.calculation)
    except:
        return ''

    time = data.get('time')
    variable = (
        ('distance', 'Km', data.get('distance')),
    )

    for t in time:
        time_attribute = t.get('attribute')[:3]
        variable += ('time', time_attribute, data.get('time')),
        break

    r = ''
    total_trip_amount = 0
    cash_collected = 0

    for pt in paymenttrip:
        trip_data = {}
        trip_total = 0
        for title, unit, components in variable:  # runs 2 times time and distance loop
            fixed_amount_title = 'Fixed Amount' + '(' + title + '):'
            fixed_amount = components[-1].get('fixed_amount', 0.0) if components else None
            if fixed_amount:
                r += gross_pay % (fixed_amount_title, components[-1].get('fixed_amount'))
            for d in data.get(title, []):
                if pt.trip.trip_number == d['trip']:
                    cash_collected = d.get('cash_collected', 0)
                    slab = d.get('slab', {})
                    var_data = t_variable % (
                    d.get('date', slab.get('interval')), d.get('value'), unit,
                    slab.get('start'), slab.get('end'), slab.get('rate'), d.get('amount'))

                    if "base_amount" not in trip_data:
                        r += trip_wise_calculation % (d.get('trip'), d.get('base_amount'))
                        if d.get('labour_cost'):
                            r += labour_and_toll % ('Labour:', d.get('labour_cost'))
                        if d.get('toll_charges'):
                            r += labour_and_toll % ('Toll Charges:', d.get('toll_charges'))
                        trip_total += d.get('base_amount') + d.get('labour_cost') + d.get(
                            'toll_charges')
                    if not var_data in trip_data:
                        r += var_data
                        trip_total += d.get('amount')
                    trip_data.update({
                        var_data: d.get('amount'),
                        "base_amount": d.get('base_amount'),
                        "labour_cost": d.get('labour_cost'),
                        "toll_charges": d.get('toll_charges'),
                        "parking_charges": d.get('parking_charges'),
                        "cash_collected": d.get('cash_collected'),
                    })
        r += ruler
        r += trip_amount % (trip_total)
        total_trip_amount += trip_total
        r += order_cash_collected % (-cash_collected)
        r += ruler
        r += gross_pay % ('Remaining Payable:', trip_total - cash_collected)
        r += ruler
    r += gross_pay % ('Total Trip Amount:', total_trip_amount)
    if driver_payment.adjustment:
        r += gross_pay % ('Adjustments:', driver_payment.adjustment)
    if driver_payment.tip_amount:
        r += gross_pay % ('Tips', driver_payment.tip_amount)
    if driver_payment.gross_pay:
        r += gross_pay % ('Gross Pay:', driver_payment.gross_pay)
    if driver_payment.cash_paid:
        r += gross_pay % ('Cash Paid:', -driver_payment.cash_paid)
    if driver_payment.cash_collected:
        r += gross_pay % ('Total Cash Collected:', -driver_payment.cash_collected)
    r += ruler
    r += gross_pay % ('Net Payable:', driver_payment.net_pay)
    return r


def driver_payment_calculation_formatter(driver_payment):
    if driver_payment.buy_rate.base_payment_interval == BASE_PAYMENT_CYCLE_PER_TRIP \
            and driver_payment.contract.contract_type == CONTRACT_TYPE_C2C:
        return construct_trip_data(driver_payment)
    r = ''
    data = json.loads(driver_payment.calculation)
    base = data.get('base')
    if base and base.get('amount') != 0:
        r += t_base % (base.get('days'), base.get('rate'), base.get('amount'))
    if base and base.get('trip_basepay_calculation'):
        trip_basepay = base.get('trip_basepay_calculation')
        if trip_basepay:
            r += '\nSupply Cost(Base Amount):\n'
            for i in trip_basepay:
                if i.get('trip_number') and i.get('base_pay'):
                    r += t_trip_basepay % (i.get('trip_number'), i.get('base_pay'))

    variable_components = (
        ('Extra Kms', 'Km', data.get('distance')),
        ('Packages', '', data.get('shipment')),
    )
    time = data.get('time')
    attribute = None
    for t in time:
        attribute = t.get('attribute')[:3]
        break

    if attribute:
        variable_components += ('Overtime', attribute, data.get('time')),
    all_vc = ''
    for title, unit, components in variable_components:
        vc = ''
        fixed_amount_title = 'Fixed Amount' + '(' + title + '):'
        fixed_amount = components[-1].get('fixed_amount', 0.0) if components else None
        if fixed_amount:
            r += gross_pay % (fixed_amount_title, components[-1].get('fixed_amount'))
        for t in components:
            slab = t.get('slab', {})
            amount = t.get('amount')
            if not amount or amount == 0:
                continue

            vc += t_variable % (t.get('date', slab.get('interval')), t.get('value'), unit,
                                slab.get('start'), slab.get('end'), slab.get('rate'),
                                t.get('amount'))
        all_vc += '%s\n' % title if vc else ''
        all_vc += vc

    if all_vc:
        r += '\nVariable Components:\n'
        r += all_vc

    r += ruler
    r += gross_pay % ('Total Earnings:', driver_payment.total_earnings)
    r += ruler

    deduction = data.get('deduction')
    if deduction:
        if deduction.get('amount'):
            r += t_deduction % (deduction.get('days'), deduction.get('rate'),
                                -deduction.get('amount') if deduction.get('amount') > 0 else 0.00)
        if deduction.get('excess_deduction'):
            r += gross_pay % ('Excess Deductions:', deduction.get('excess_deduction'))
    r += ruler
    if driver_payment.adjustment:
        r += gross_pay % ('Adjustments:', driver_payment.adjustment)
    if driver_payment.toll_charges:
        r += gross_pay % ('Toll Charges:', (float(driver_payment.toll_charges)))
    if driver_payment.parking_charges:
        r += gross_pay % ('Parking Charges:', (float(driver_payment.parking_charges)))
    if driver_payment.labour_cost:
        r += gross_pay % ('Labour:', (float(driver_payment.labour_cost)))
    r += ruler
    if driver_payment.tip_amount:
        r += gross_pay % ('Tips:', driver_payment.tip_amount)
    if driver_payment.gross_pay:
        r += gross_pay % ('Gross Pay:', driver_payment.gross_pay)
    if driver_payment.cash_paid:
        r += gross_pay % ('Cash Paid:', -driver_payment.cash_paid)
    if driver_payment.cash_collected:
        r += gross_pay % ('Cash Collected:', -driver_payment.cash_collected)
    r += ruler
    r += gross_pay % ('Net Payable:', driver_payment.net_pay)

    return r


def adjustment_date_check(payment, date):
    error = False
    message = ''
    trip_window = payment.paymenttrip_set.aggregate(
        min_date=Min('trip__planned_start_time'),
        max_date=Max('trip__planned_start_time'))

    if trip_window and trip_window.get('min_date', None) and trip_window.get('max_date', None):
        trip_window_start = get_in_ist_timezone(
            trip_window.get('min_date'))
        trip_window_end = get_in_ist_timezone(trip_window.get('max_date'))
        if date:
            if date < trip_window_start.date() or \
              date > trip_window_end.date():
                error = True
                message = messages.ADJUSTMENT_DATE_SHOULD_BE_FALL_UNDER_TRIP_WINDOW
                return error, message
    return error, message


def get_packages_info(trip):
    _dict = {}
    for i in TRIP_PACKAGES_LIST:
        count = getattr(trip, i, 0)
        if count:
            _dict[i] = count
    if _dict:
        return _dict
    else:
        return None


class DriverPaymentUtils(object):

    def _get_general_details(self, tab,  payment_id, user):
        driver_payment = DriverPayment.objects.filter(id=payment_id)
        response_data = driver_payment.values(
            *PAYMENT_DETAIL_FIELDS_MAPPING[tab]).first()

        payment_obj = driver_payment.first()

        trip_times = [
            x.trip.planned_start_time for x in payment_obj.paymenttrip_set.all()]
        if not trip_times:
            trip_window = ''
        else:
            min_ = get_in_ist_timezone(min(trip_times))
            max_ = get_in_ist_timezone(max(trip_times))
            trip_window = '%s - %s %s' % (min_.day, max_.day, max_.strftime('%b'))

        num_modified_trips = 0
        for t in payment_obj.paymenttrip_set.all():
            if t.trip.modified_date >= payment_obj.modified_date:
                num_modified_trips += 1
        is_stale = payment_obj.is_stale or num_modified_trips > 0

        vehicles = set([str(x.trip.vehicle)
                        for x in payment_obj.paymenttrip_set.all() if x.trip.vehicle])
        vehicles = ', '.join(vehicles)

        vehicle_classes = set([str(x.trip.vehicle.vehicle_model.vehicle_class)
                               for x in payment_obj.paymenttrip_set.all()
                               if x.trip.vehicle])
        vehicle_classes = ', '.join(vehicle_classes)

        has_contingency = bool([x for x in payment_obj.paymenttrip_set.all()
                                if x.trip.is_contingency])

        response_data['start_datetime'] = payment_obj.start_datetime.strftime('%d %b %Y')
        response_data['end_datetime'] = payment_obj.end_datetime.strftime('%d %b %Y')
        response_data['city'] = payment_obj.contract.city.name
        response_data['trip_window'] = trip_window
        response_data['is_stale'] = is_stale
        response_data['vehicles'] = vehicles
        response_data['vehicle_classes'] = vehicle_classes
        response_data['has_contingency'] = has_contingency
        return response_data

    def _get_payment_details(self, tab, payment_id, user):
        driver_payment = DriverPayment.objects.filter(id=payment_id)
        response_data = driver_payment.values(
            *PAYMENT_DETAIL_FIELDS_MAPPING[tab]).first()

        payment_obj = driver_payment.first()

        total_time = 0.0
        trip_time = sum([pt.trip.total_time
                         for pt in payment_obj.paymenttrip_set.all() if pt.trip.total_time])
        if trip_time:
            total_time = minutes_to_dhms(trip_time)

        response_data['total_time'] = str(total_time)
        response_data['cash_paid'] = -payment_obj.cash_paid
        response_data['cash_collected'] = -payment_obj.cash_collected
        return response_data

    def _get_calculation(self, tab, payment_id, user):
        driver_payment = DriverPayment.objects.filter(id=payment_id).first()
        slip = driver_payment_calculation_formatter(driver_payment)

        return format_html('<pre>%s</pre>' % slip)

    def _get_driver_bank_details(self, tab, payment_id, user):
        driver_payment = DriverPayment.objects.filter(id=payment_id).first()
        bank_account = driver_payment.driver.get_bank_account_details()
        note = None
        response_data = {}
        if bank_account:
            note = _('This is the Current Account On Driver Profile as of Now')
            response_data = {
                'beneficiary_name': bank_account.account_name,
                'ifsc_code': bank_account.ifsc_code,
                'account_number': bank_account.account_number,
                'note': note
            }
        return response_data

    def _get_trip_details(self, tab, payment_id, user):
        trips = PaymentTrip.objects.filter(payment_id=payment_id)
        qs = trips.select_related('trip')
        qs = qs.select_related('trip__order')
        qs = qs.select_related('trip__order__customer')
        qs = qs.order_by('trip__planned_start_time')
        response_data = []
        _dict = {}
        # response_data = list(
        #     qs.values('trip__trip_number', 'trip__order__number', 'trip__order__customer__name',
        #               'trip__status', 'trip__trip_base_pay', 'trip__trip_deduction_amount',
        #               'trip__total_distance', 'trip__total_time', 'trip__parking_charges',
        #               'trip__planned_start_time', 'trip__actual_start_time',
        #               'trip__actual_end_time', 'trip__meter_reading_start',
        #               'trip__meter_reading_end'
        #               ).annotate(
        #         payment_status=Case(
        #             When(trip__order__order_type='booking', then=F('trip__order__payment_status')),
        #             default=None)).annotate(
        #         toll_charges=Case(
        #             When(Q(trip__toll_charges__gt=0.0), then=F('trip__toll_charges')),
        #             default=F('trip__order__toll_charges')
        #         )))

        for payment_trip in qs:
            _dict = {}
            toll_charges = 0
            if payment_trip.trip:
                toll_charges = payment_trip.trip.toll_charges
            if not toll_charges:
                toll_charges = payment_trip.trip.order.toll_charges if payment_trip.trip.order else 0

            payment_status = None
            if payment_trip.trip and payment_trip.trip.order:
                order = payment_trip.trip.order
                if order.order_type == settings.DEFAULT_ORDER_TYPE:
                    payment_status = order.payment_status

            _dict = {
                'trip__trip_number': payment_trip.trip.trip_number,
                'trip__status': payment_trip.trip.status,
                'trip__trip_base_pay': payment_trip.trip.trip_base_pay,
                'trip__trip_deduction_amount': payment_trip.trip.trip_deduction_amount,
                'trip__total_distance': payment_trip.trip.total_distance,
                'trip__total_time': payment_trip.trip.total_time,
                'trip__parking_charges': payment_trip.trip.parking_charges,
                'trip__planned_start_time': payment_trip.trip.planned_start_time,
                'trip__actual_start_time': payment_trip.trip.actual_start_time,
                'trip__actual_end_time': payment_trip.trip.actual_end_time,
                'trip__meter_reading_start': payment_trip.trip.meter_reading_start,
                'trip__meter_reading_end': payment_trip.trip.meter_reading_end,
                'toll_charges': toll_charges,
                'payment_status': payment_status,
                'trip__order__number': payment_trip.trip.order.number
                if payment_trip.trip and payment_trip.trip.order else None,

                'trip__order__customer__name': payment_trip.trip.order.customer.name
                if payment_trip.trip and payment_trip.trip.order else None,

                'packages_info': get_packages_info(payment_trip.trip)
            }
            response_data.append(_dict)

        return response_data

    def _get_adjustments(self, tab, payment_id, user):
        adjustments = DriverPaymentAdjustment.objects.filter(payment_id=payment_id)
        data = []

        for i in adjustments.iterator():
            is_adjustment_spoc = is_adjustment_spoc_or_supervisor(i, user)
            can_approve = False
            can_unapprove = False
            can_reject = False
            if i.status == NEW:
                can_reject = is_adjustment_spoc
                can_approve, hint = can_approve_adjustment(i, user)
            if i.status == APPROVED:
                can_unapprove, hint = can_unapprove_adjustment(i)

            if can_unapprove:
                can_unapprove = is_adjustment_spoc

            _dict = {
                'id': i.id,
                'status': i.status,
                'category': i.category,
                'description': i.description,
                'adjustment_amount': i.adjustment_amount,
                'created_by__email': i.created_by.email,
                'adjustment_datetime': i.adjustment_datetime.strftime('%d-%m-%Y'),
                'action_owner__email': i.action_owner.email,
                'can_reject': can_reject,
                'can_approve': can_approve,
                'can_unapprove': can_unapprove
            }
            data.append(_dict)

        return data

    def _get_driver_cash_paid(self, tab, payment_id, user):
        cash_paid = DriverCashPaid.objects.filter(payment_id=payment_id)
        data = []

        for i in cash_paid.iterator():
            adjustment_spoc_or_supervisor = is_adjustment_spoc_or_supervisor(i, user)
            can_approve = False
            can_unapprove = False
            can_reject = False
            if i.status == NEW:
                can_reject = adjustment_spoc_or_supervisor
                can_approve, hint = can_approve_adjustment(i, user)
            if i.status == APPROVED:
                can_unapprove, hint = can_unapprove_adjustment(i)

            if can_approve:
                can_approve = is_adjustment_supervisor(i, user)
            if can_unapprove:
                can_unapprove = adjustment_spoc_or_supervisor
            _dict = {
                'id': i.id,
                'status': i.status,
                'created_by__email': i.created_by.email,
                'cash_paid_datetime': i.cash_paid_datetime.strftime('%d-%m-%Y'),
                'description': i.description,
                'cash_paid': i.cash_paid,
                'proof': i.proof.url,
                'can_reject': can_reject,
                'can_approve': can_approve,
                'can_unapprove': can_unapprove
            }
            data.append(_dict)

        return data

    # get methods mapping for getting driver payment details
    payment_detail_info_mapping = {
        'general': _get_general_details,
        'payment_details': _get_payment_details,
        'calculation': _get_calculation,
        'bank_details': _get_driver_bank_details,
        'trips': _get_trip_details,
        'adjustments': _get_adjustments,
        'cash_paid': _get_driver_cash_paid
    }

    """
        -------Driver payment status update methods--------
    """
    def _unapprove_payment(self, payment_id, action_owner):
        payment = DriverPayment.objects.filter(pk=payment_id)
        success = True
        message = ''
        payment_obj = payment.first()

        if not payment.exists():
            message = _('Payment ID does not exist. It might have been deleted!')
            success = False
        if success and payment_obj.status in [WITHHELD] and not is_payment_supervisor(payment_obj,
                                                                                      action_owner):
            message = _('You do not have permission to unapprove this payment')
            success = False

        if success and payment_obj.status in [SUSPENDED, CLOSED] and\
                not is_super_user(payment_obj, action_owner):
            message = _('You do not have permission to unapprove this payment')
            success = False

        if success and payment_obj.status in [APPROVED] and \
                not is_payment_supervisor(payment_obj, action_owner):
            success, message = can_unapprove_payment(payment_obj, action_owner)

        if not success:
            return success, message

        driver_ledger = []

        if payment_obj.status == APPROVED:
            driver_ledger = DriverLedger(
                driver=payment_obj.driver,
                amount=-payment_obj.net_pay,
                description='Approval-Reversal %s, %s - %s' % (
                    payment_obj.contract,
                    payment_obj.start_datetime.strftime(DATE_FORMAT),
                    payment_obj.end_datetime.strftime(DATE_FORMAT)
                ),
                ops_reference=payment_obj,
            )

        payment_history = DriverPaymentHistory(field='status',
                                               old_value=str(payment_obj.status),
                                               new_value=UNAPPROVED,
                                               user=action_owner,
                                               payment=payment_obj)
        payment_obj.status = UNAPPROVED
        with transaction.atomic():
            payment_obj.save()
            # payment_history.save()
            if driver_ledger:
                driver_ledger.save()

        message = _('%s the payment %s') % (
            UNAPPROVED_ACTION_STRING_MAPPING.get(payment_obj.status, UNAPPROVED), payment_id)

        return success, message

    def _approve_payment(self, payment_id, action_owner):
        payment = DriverPayment.objects.filter(pk=payment_id)
        success = True
        message = ''
        payment_obj = payment.first()

        if not payment.exists():
            message = _('Payment ID does not exist. It might have been deleted!')
            success = False
        if success and not is_payment_spoc_or_supervisor(payment_obj, action_owner):
            success = False
            message = _('You do not have permission to approve this payment')
        if success:
            success, message = can_approve_payment(payment_obj, action_owner)
            if success:
                success, message = is_current_user_can_approve(payment_obj, action_owner)

        if not success:
            return success, message

        payment_history = DriverPaymentHistory(field='status',
                                               old_value=str(payment_obj.status),
                                               new_value=APPROVED,
                                               user=action_owner,
                                               payment=payment_obj)

        driver_ledger = DriverLedger(
            driver=payment_obj.driver,
            owner_details=payment_obj.owner_details,
            amount=payment_obj.gross_pay,
            description='%s, %s - %s' % (
                payment_obj.contract,
                payment_obj.start_datetime.strftime(DATE_FORMAT),
                payment_obj.end_datetime.strftime(DATE_FORMAT)
            ),
            ops_reference=payment_obj,
        )

        total_amount_collected = payment_obj.cash_paid + payment_obj.cash_collected
        driver_ledger_cash_collected_comp = None
        if total_amount_collected > 0.0:
            driver_ledger_cash_collected_comp = DriverLedger(
                    driver=payment_obj.driver,
                    amount=-total_amount_collected,
                    description='Advance Cash Paid + Cash Collected-%s, %s - %s' % (
                        payment_obj.contract,
                        payment_obj.start_datetime.strftime(DATE_FORMAT),
                        payment_obj.end_datetime.strftime(DATE_FORMAT)
                    ),
                    ops_reference=payment_obj,
                )

        if payment_obj.net_pay <= 0.0:
            payment_obj.is_settled = True
        payment_obj.status = APPROVED
        with transaction.atomic():
            payment_obj.save()
            driver_ledger.save()
            # payment_history.save()
            if driver_ledger_cash_collected_comp:
                driver_ledger_cash_collected_comp.save()
        message = _('%s the payment %s') % (APPROVED, payment_id)
        return success, message

    def _withheld_payment(self, payment_id, action_owner):
        payment = DriverPayment.objects.filter(pk=payment_id)
        success = True
        message = ''
        payment_obj = payment.first()

        if not payment.exists():
            message = _('Payment ID does not exist. It might have been deleted!')
            success = False

        if success and not is_payment_spoc_or_supervisor(payment_obj, action_owner):
            message = _('You do not have permission to withheld this payment ID')
            success = False

        if not success:
            return success, message

        payment_history = DriverPaymentHistory(field='status',
                                               old_value=str(payment_obj.status),
                                               new_value=WITHHELD,
                                               user=action_owner,
                                               payment=payment_obj)

        with transaction.atomic():
            DriverPayment.objects.filter(pk=payment_id).update(status=WITHHELD)
            payment_history.save()

        message = _('%s the payment %s') % (WITHHELD, payment_id)
        return success, message

    def _suspend_payment(self, payment_id, action_owner):
        payment = DriverPayment.objects.filter(pk=payment_id)
        success = True
        message = ''
        payment_obj = payment.first()

        if not payment.exists():
            message = _('Payment ID does not exist. It might have been deleted!')
            success = False

        if success and not is_super_user(payment_obj, action_owner):
            message = _('You do not have permission to suspend this payment ID')
            success = False

        if not success:
            return success, message

        payment_history = DriverPaymentHistory(field='status',
                                               old_value=str(payment_obj.status),
                                               new_value=SUSPENDED,
                                               user=action_owner,
                                               payment=payment_obj)

        with transaction.atomic():
            DriverPayment.objects.filter(pk=payment_id).update(status=SUSPENDED)
            payment_history.save()

        message = _('%s the payment %s') % (SUSPENDED, payment_id)
        return success, message

    def _close_payment(self, payment_id, action_owner):
        payment = DriverPayment.objects.filter(pk=payment_id)
        success = True
        message = ''
        payment_obj = payment.first()

        if not payment.exists():
            message = _('Payment ID does not exist. It might have been deleted!')
            success = False

        if success and not is_payment_spoc_or_supervisor(payment_obj, action_owner):
            message = _('You do not have permission to close this payment ID')
            success = False

        if not success:
            return success, message

        payment_history = DriverPaymentHistory(field='status',
                                               old_value=str(payment_obj.status),
                                               new_value=CLOSED,
                                               user=action_owner,
                                               payment=payment_obj)

        with transaction.atomic():
            DriverPayment.objects.filter(pk=payment_id).update(status=CLOSED)
            payment_history.save()

        message = _('%s the payment %s') % (WITHHELD, payment_id)
        return success, message

    status_update_action_mapping = {
        UNAPPROVED: _unapprove_payment,
        APPROVED: _approve_payment,
        WITHHELD: _withheld_payment,
        SUSPENDED: _suspend_payment,
        CLOSED: _close_payment
    }

    """
        -----Driver Payment Adjustments status update methods--------
    """

    def _approve_adjustment(self, adjustment_id, action_owner, remarks=None):
        adjustment = DriverPaymentAdjustment.objects.filter(id=adjustment_id)
        success = True
        message = ''
        adjustment_obj = adjustment.first()

        if not adjustment.exists():
            message = _('Adjustment ID does not exist. It might have been deleted!')
            success = False

        if success:
            success, message = can_approve_adjustment(adjustment_obj, action_owner)

        if not success:
            return success, message, adjustment_obj

        if adjustment_obj.payment:
            sum_ = DriverPaymentAdjustment.objects.filter(payment=adjustment_obj.payment)\
                .filter(Q(status=APPROVED) | Q(pk=adjustment_obj.pk)) \
                .aggregate(Sum('adjustment_amount'))

            total_adjustment = sum_.get('adjustment_amount__sum') or 0
            adjustment_obj.payment.adjustment = total_adjustment
            adjustment_obj.status = APPROVED

            with transaction.atomic():
                adjustment_obj.payment.save()
                adjustment_obj.save()
            message = _('Approved the adjustment %s for payment ID %s') % (
                adjustment_id, adjustment_obj.payment_id)
        return success, message, adjustment_obj

    def _reject_adjustment(self, adjustment_id, action_owner, remarks):
        adjustment = DriverPaymentAdjustment.objects.filter(id=adjustment_id)
        success = True
        message = ''
        adjustment_obj = adjustment.first()

        if not adjustment.exists():
            message = _('Adjustment ID does not exist. It might have been deleted!')
            success = False

        if success and not is_adjustment_spoc_or_supervisor(adjustment_obj, action_owner):
            message = _('You do not have permission to reject this adjustment')
            success = False

        if not success:
            return success, message, adjustment_obj

        adjustment_obj.status = REJECTED

        with transaction.atomic():
            adjustment_obj.save()
            DriverPaymentAdjustmentHistory.objects.create(
                adjustment=adjustment_obj,
                field='status',
                old_value=NEW,
                new_value=REJECTED,
                remarks=remarks,
                created_by=action_owner,
                created_date=timezone.now()
            )
        message = _('Rejected the adjustment % for payment ID %s') % (
            adjustment_id, adjustment_obj.payment_id)

        return success, message, adjustment_obj

    def _unapprove_adjustment(self, adjustment_id, action_owner, remarks=None):
        adjustment = DriverPaymentAdjustment.objects.filter(id=adjustment_id)
        success = True
        message = ''
        adjustment_obj = adjustment.first()

        if not adjustment.exists():
            message = _('Adjustment ID does not exist. It might have been deleted!')
            success = False

        if success and not is_adjustment_spoc_or_supervisor(adjustment_obj, action_owner):
            message = _('You do not have permission to unapprove this adjustment')

        if success:
            success, message = can_unapprove_adjustment(adjustment_obj)

        if not success:
            return success, message, adjustment_obj

        if adjustment_obj.payment:
            sum_ = DriverPaymentAdjustment.objects.filter(payment=adjustment_obj.payment) \
                .exclude(pk=adjustment_obj.pk) \
                .filter(status=APPROVED) \
                .aggregate(Sum('adjustment_amount'))

            total_adjustment = sum_.get('adjustment_amount__sum') or 0
            adjustment_obj.payment.adjustment = total_adjustment
            adjustment_obj.status = NEW

            with transaction.atomic():
                adjustment_obj.payment.save()
                adjustment_obj.save()
            message = _('Unapproved the adjustment %s for payment ID %s') % (
                adjustment_id, adjustment_obj.payment_id)
        return success, message, adjustment_obj

    _adjustments_action_mapping = {
        APPROVED: _approve_adjustment,
        REJECTED: _reject_adjustment,
        UNAPPROVED: _unapprove_adjustment
    }

    """
        ------Driver Cash Paid status update methods.------
    """

    def _approve_cash_paid(self, cash_paid_id, action_owner):
        cash_paid = DriverCashPaid.objects.filter(id=cash_paid_id)
        message = ''
        success = True

        cash_paid_obj = cash_paid.first()

        if not cash_paid.exists():
            message = _('Cash Paid ID does not exist. It might have been deleted!')
            success = False

        if success and not is_adjustment_supervisor(cash_paid_obj, action_owner):
            message = _('You do not have permission to approve this')
            success = False

        if success:
            success, message = can_approve_adjustment(cash_paid_obj, action_owner)

        if not success:
            return success, message, cash_paid_obj

        if cash_paid_obj.payment:
            # Save the total cash paid onto the master payment
            # ORing self.pk since that will not be in approved state yet
            sum_ = DriverCashPaid.objects.filter(payment=cash_paid_obj.payment) \
                .filter(Q(status=APPROVED) | Q(pk=cash_paid_obj.pk)) \
                .aggregate(Sum('cash_paid'))

            total_cash_paid = sum_.get('cash_paid__sum') or 0
            cash_paid_obj.payment.cash_paid = total_cash_paid
            cash_paid_obj.status = APPROVED

            with transaction.atomic():
                cash_paid_obj.payment.save()
                cash_paid_obj.save()
            message = _('Approved the Cash Paid %s for payment ID %s') % (
                cash_paid_id, cash_paid_obj.payment_id)
        return success, message, cash_paid_obj

    def _reject_cash_paid(self, cash_paid_id, action_owner):
        cash_paid = DriverCashPaid.objects.filter(id=cash_paid_id)
        message = ''
        success = True

        cash_paid_obj = cash_paid.first()

        if not cash_paid.exists():
            message = _('Cash Paid ID does not exist. It might have been deleted!')
            success = False

        if success and not is_adjustment_spoc_or_supervisor(cash_paid_obj, action_owner):
            message = _('You do not have permission to reject this')
            success = False

        if not success:
            return success, message, cash_paid_obj

        cash_paid_obj.status = REJECTED
        cash_paid_obj.save()

        message = _('Rejected Cash Paid %s for payment ID %s') % (
            cash_paid_id, cash_paid_obj.payment_id)

        return success, message, cash_paid_obj

    def _unapprove_cash_paid(self, cash_paid_id, action_owner):
        cash_paid = DriverCashPaid.objects.filter(id=cash_paid_id)
        message = ''
        success = True

        cash_paid_obj = cash_paid.first()

        if not cash_paid.exists():
            message = _('Cash Paid ID does not exist. It might have been deleted!')
            success = False

        if success and not is_adjustment_spoc_or_supervisor(cash_paid_obj, action_owner):
            message = _('You do not have permission to reject this')
            success = False

        if success:
            success, message = can_unapprove_adjustment(cash_paid_obj)

        if not success:
            return success, message, cash_paid_obj

        if cash_paid_obj.payment:
            # Excluding self.pk since that will still be in approved state
            sum_ = DriverCashPaid.objects.filter(payment=cash_paid_obj.payment) \
                .exclude(pk=cash_paid_obj.pk) \
                .filter(status=APPROVED) \
                .aggregate(Sum('cash_paid'))

            total_cash_paid = sum_.get('cash_paid__sum') or 0
            cash_paid_obj.payment.cash_paid = total_cash_paid
            cash_paid_obj.status = NEW

            with transaction.atomic():
                cash_paid_obj.payment.save()
                cash_paid_obj.save()
            message = _('Unapproved the Cash Paid %s for payment ID %s') % (
                cash_paid_id, cash_paid_obj.payment_id)

        return success, message, cash_paid_obj

    _cash_paid_action_mapping = {
        APPROVED: _approve_cash_paid,
        REJECTED: _reject_cash_paid,
        UNAPPROVED: _unapprove_cash_paid
    }
