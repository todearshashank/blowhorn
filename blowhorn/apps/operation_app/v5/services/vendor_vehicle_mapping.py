from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework import exceptions

from blowhorn.vehicle.models import Vehicle, VehicleModel, BodyType
from .vendor import VendorMixin


class VendorVehicleValidationMixin(object):

    def validate(self, data, mandatory_fields):
        """
        :param data:
        :param mandatory_fields:
        :return:
        """
        message = None
        missing_fields = []
        for f in mandatory_fields:
            if not data.get(f, None):
                missing_fields.append(f)

        if missing_fields:
            message = 'Fill the following fields:\n %s' % \
                      ', '.join(missing_fields)
        return message

    def clean_vehicle_data(self, data, vendor):
        """

        :param data:
        :param vendor:
        :return:
        """
        message = self.validate(data, ['vehicle_number', 'vehicle_model',
                                       'body_type', 'vehicle_model_year'])
        if message:
            raise exceptions.ValidationError(_(message))

        vehicle_number = data.get('vehicle_number').upper()
        vehicle_model = data.get('vehicle_model')
        body_type = data.get('body_type')
        model_year = data.get('vehicle_model_year')

        if self.check_for_vehicle(vehicle_number):
            raise exceptions.ValidationError(
                _('Vehicle number already present in the system'))

        if model_year > timezone.now().year:
            raise exceptions.ValidationError(
                _('Model year cannot be future'))

        try:
            vehicle_model = VehicleModel.objects.get(model_name=vehicle_model)
        except exceptions.APIException:
            raise exceptions.APIException(
                _('Selected vehicle model not exists'))

        try:
            body_type = BodyType.objects.get(body_type=body_type)
        except exceptions.APIException:
            raise exceptions.APIException(
                _('Selected vehicle body type not exists'))
        return {
            'registration_certificate_number': vehicle_number,
            'vehicle_model': vehicle_model,
            'body_type': body_type,
            'model_year': model_year,
            'vendor': vendor
        }

    def check_for_vehicle(self, vehicle_number):
        """

        :param vehicle_number:
        :return:
        """
        return Vehicle.objects.filter(
            registration_certificate_number=vehicle_number
        ).exists()


class VendorVehicleMixin(object):
    vendor_mixin = VendorMixin()
    validation_mixin = VendorVehicleValidationMixin()

    def map_existing_vehicle_to_vendor(self, vehicle_pks, vendor, action_by):
        """

        :param vehicle_pk:
        :param vendor:
        :param action_by
        :return:
        """
        count = Vehicle.objects.filter(pk__in=vehicle_pks)\
            .update(vendor=vendor)
        self.vendor_mixin.update_auditlog(vendor, action_by)
        return count

    def unmap(self, vehicle_pks, vendor, action_by):
        """

        :param vehicle_pk:
        :param vendor:
        :param action_by:
        :return:
        """
        count = Vehicle.objects.filter(pk__in=vehicle_pks).update(vendor=None)
        self.vendor_mixin.update_auditlog(vendor, action_by)
        return count
