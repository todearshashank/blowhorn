# Django and system libraries
import pytz
import json
import calendar
from itertools import groupby
from datetime import date, datetime, timedelta
from django.conf import settings
from django.utils import timezone
from django.core.files.base import ContentFile
from django.db.models import Sum, Case, When, FloatField, IntegerField, F, Count, Min, Max, Q
from django.utils.translation import ugettext_lazy as _
from django.db import transaction

# 3rd party libraries
from blowhorn.oscar.core.loading import get_model
from rest_framework.exceptions import ValidationError

# Blowhorn Imports
from blowhorn.utils.mail import Email
from blowhorn.customer import constants as customer_constants
from blowhorn.contract.permissions import is_invoice_spoc, is_invoice_supervisor, \
    has_approval_permission
from blowhorn.customer.invoice_transition_conditions import within_dispute_threshold_date, \
    can_approve
from blowhorn.customer.submodels.invoice_models import create_invoice_history, \
    create_invoice_history_bulk, CreditDebitNote
from blowhorn.customer.invoice_transition_conditions import \
    is_city_sales_manager, is_collection_representative
from blowhorn.customer.utils import abbr_to_num
from blowhorn.apps.operation_app.v5.tasks import send_invoice_action_notification

# from blowhorn.customer.submodels.invoice_models import CustomerInvoice

Customer = get_model('customer', 'Customer')
InvoiceHistory = get_model('customer', 'InvoiceHistory')
InvoiceSupportingDocument = get_model('customer', 'InvoiceSupportingDocument')
MISDocument = get_model('customer', 'MISDocument')
User = get_model('users', 'User')


CustomerInvoice = get_model('customer', 'CustomerInvoice')

INVOICE_STATUSES = [
    customer_constants.APPROVED, customer_constants.SENT_TO_CUSTOMER,
    customer_constants.CUSTOMER_ACKNOWLEDGED,
    customer_constants.CUSTOMER_DISPUTED, customer_constants.CUSTOMER_CONFIRMED
]


def set_invoice_status(request, processed_invoices, due_date, history_data, updated_status,
                       supporting_doc):
    if updated_status not in [customer_constants.CUSTOMER_CONFIRMED,
                              customer_constants.CUSTOMER_ACKNOWLEDGED]:
        return False, 0

    with transaction.atomic():
        if updated_status == customer_constants.CUSTOMER_ACKNOWLEDGED:
            updated_count = CustomerInvoice.objects.filter(
                invoice_number__in=processed_invoices).update(
                status=updated_status, due_date=due_date,
                assigned_date=date.today(),
                modified_by=request.user,
                modified_date=timezone.now())
        else:
            updated_count = CustomerInvoice.objects.filter(
                invoice_number__in=processed_invoices).update(
                status=updated_status, due_date=due_date,
                modified_by=request.user,
                modified_date=timezone.now())

        invoice_history_instances = create_invoice_history_bulk(history_data)
        document = supporting_doc
        if document:
            first_record = None
            for history in invoice_history_instances:
                _dict = {
                    'invoice': history.invoice,
                    'invoice_history': history,
                    'supporting_doc': first_record.supporting_doc if first_record else document,
                    'created_by': request.user
                }
                instance = InvoiceSupportingDocument.objects.create(**_dict)
                if not first_record:
                    first_record = instance

        return True, updated_count
    return False, 0


class CustomerUtils(object):

    def _get_query(self, params, request):
        from django.db.models import Q

        current_owner__email = params.get('current_owner__email', None)
        invoice_state__name = params.get('invoice_state__name', None)
        blanket_invoice = params.get('blanket_invoice', False)
        revenue_booked_on = params.get('revenue_booked_on', None)
        service_period_end = params.get('service_period_end', None)
        customer_id = params.get('customer_id', None)
        customer__name = params.get('customer__name', None)
        status = params.get('status', None)
        division = params.get('division', None)
        assigned_to_me = params.get('assigned_to_me', False)
        contracts_list = params.get('contracts', None)

        query = Q()
        if assigned_to_me:
            query &= Q(current_owner=request.user)
        if division:
            query &= Q(contract_invoices__contract__division__name=division)
        if current_owner__email:
            query &= Q(current_owner__email=current_owner__email)
        if invoice_state__name:
            query &= Q(bh_source_state__name=invoice_state__name)
        if blanket_invoice:
            blanket_invoice = True if params.get('blanket_invoice') == 'True' else False
            query &= Q(invoice_type=customer_constants.TAX_INVOICE) & Q(
                is_credit_debit_note=blanket_invoice)
        if revenue_booked_on:
            month, year = revenue_booked_on.split('-')
            query &= Q(revenue_booked_on__month=abbr_to_num.get(
                month, None), revenue_booked_on__year=year)
        if service_period_end:
            month, year = service_period_end.split('-')
            query &= Q(service_period_end__month=abbr_to_num.get(
                month, None), service_period_end__year=year)
        if contracts_list:
            contracts_list = contracts_list.split(',')
            query &= Q(invoice_configs__contracts__in=contracts_list)

        invoice_type = params.get('invoice_type')

        if invoice_type:
            query &= Q(invoice_type=invoice_type)
        if customer_id:
            query &= Q(customer_id=customer_id)
        if customer__name:
            query &= Q(customer__name=customer__name)
        if status:
            query &= Q(status=status)
        if not status:
            query &= ~Q(status=customer_constants.EXPIRED)

        return query

    def _get_field_lookups(self, field):
        cal = []
        invoices = CustomerInvoice.objects.extra(
            select={"year": "EXTRACT(YEAR  FROM " + field + ")",
                    "month": "EXTRACT(MONTH  FROM " + field + ")"}
            ).distinct().values_list("year", "month").order_by("-year", "-month")

        for i in invoices:
            if all(i):
                # i[1] is month and i[0] is year
                cal.append((calendar.month_abbr[int(i[1])] + '-' + str(int(i[0]))))
        return cal

    def _get_customers(self, query):
        customers = list(
            CustomerInvoice.objects.select_related('current_owner', 'customer', 'bh_source_state',
                                                   'invoice_state', 'invoice_configs',
                                                   'related_invoice').prefetch_related(
                'contract_invoices', 'contract_invoices__contract',
                'contract_invoices__contract__division').filter(
                query).values(
                'customer_id', 'customer__name').annotate(
                due_amount=Sum(Case(
                    When(status__in=[customer_constants.APPROVED,
                                     customer_constants.SENT_TO_CUSTOMER,
                                     customer_constants.CUSTOMER_ACKNOWLEDGED,
                                     customer_constants.CUSTOMER_DISPUTED,
                                     customer_constants.CUSTOMER_CONFIRMED],
                         then=F('total_amount')), output_field=FloatField()
                )),
                due_invoices=Count(Case(
                    When(status__in=[customer_constants.APPROVED,
                                     customer_constants.SENT_TO_CUSTOMER,
                                     customer_constants.CUSTOMER_ACKNOWLEDGED,
                                     customer_constants.CUSTOMER_DISPUTED,
                                     customer_constants.CUSTOMER_CONFIRMED],
                         then=F('total_amount')), output_field=IntegerField())
                ),
                total_amount=Sum('total_amount')
            )
        )
        return sorted(customers, key=lambda i: i['customer__name'])

    def _get_customerwise_invoices(self, query):
        customers_list = []
        invoices_list = []
        customer_name = ''
        invoices = CustomerInvoice.objects.select_related('current_owner', 'customer',
                                                          'bh_source_state',
                                                          'invoice_state', 'invoice_configs',
                                                          'related_invoice').filter(query).order_by(
            'modified_date')

        for invoice in invoices.iterator():
            customer_name = invoice.customer.name
            invoices_list.append(
                {
                    'id': invoice.id,
                    'customer_id': invoice.customer.id,
                    'customer__name': customer_name,
                    'status': invoice.status,
                    'due_date': invoice.due_date.strftime('%d-%b-%Y') if invoice.due_date else None,
                    'current_owner__email': invoice.current_owner.email
                    if invoice.current_owner else None,
                    'invoice_number': invoice.invoice_number,
                    'bh_source_state__name': invoice.bh_source_state.name,
                    'invoice_state__name': invoice.invoice_state.name,
                    'invoice_date': invoice.invoice_date.strftime('%d-%b-%Y')
                    if invoice.invoice_date else None,
                    'invoice_type': invoice.invoice_type,
                    'total_amount': invoice.total_amount,
                    'file': invoice.file.url if invoice.file else None,
                    'related_invoice__invoice_number': invoice.related_invoice.invoice_number
                    if invoice.related_invoice else None,
                    'related_invoice__file': invoice.related_invoice.file.url
                    if invoice.related_invoice and invoice.related_invoice.file else None
                }
            )
        customers_list.append(
            {
                'customer': customer_name,
                'invoices': invoices_list
            }
        )
        return customers_list

    """ Status transition methods"""

    def _send_for_approval(self, invoice_data, request):
        """
        Send invoice for approval
        """
        invoice_number = invoice_data.get('invoice_number', [])
        action_owner_email = invoice_data.get('action_owner', None)
        due_date = invoice_data.get('due_date', None)
        supporting_doc = invoice_data.get('supporting_doc', None)

        success = True
        message = _('Sent invoice for approval')
        requested_user = request.user

        if not (invoice_number or due_date):
            success = False
            message = _('Please enter all the details')

        action_owner = User.objects.filter(email=action_owner_email).first()
        invoice = CustomerInvoice.objects.filter(invoice_number__in=invoice_number).first()

        if not can_approve(invoice):
            success = False
            message = _('Cannot send this invoice for approval')

        if not is_invoice_spoc(invoice, requested_user):
            success = False
            message = _('You do not have permission to send this invoice for approval')

        if success:
            notification_details = {
                'assignor_email': requested_user,
                'invoice': invoice,
                'invoice_id': invoice.invoice_number,
                'assignor_name': requested_user.name,
                'assignee_email': [action_owner_email],
                'status': customer_constants.SENT_FOR_APPROVAL,
                'due_date': due_date
            }
            history = create_invoice_history(invoice.status, customer_constants.SENT_FOR_APPROVAL,
                                             invoice, action_owner, due_date)
            invoice.current_owner = action_owner
            invoice.assigned_date = date.today()

            if supporting_doc:
                data = {
                    'invoice': invoice,
                    'invoice_history': history
                }
                mis_data = {
                    'invoice': invoice,
                    'mis_doc': supporting_doc
                }
                mis = MISDocument(**mis_data)
                mis.save()
                document = InvoiceSupportingDocument(**data)
                document.save()
                notification_details['supporting_doc'] = mis.mis_doc
            Email().email_invoice_assignment_notification(notification_details)
            send_invoice_action_notification.apply_async(
                (customer_constants.SENT_FOR_APPROVAL, invoice_number,
                 [action_owner.email],
                 requested_user.email,), )

        return success, message

    def _approve_invoice(self, invoice_data, request):
        """
        Approve invoice
        """
        invoice_number = invoice_data.get('invoice_number', [])
        action_owner_email = invoice_data.get('action_owner', None)
        due_date = invoice_data.get('due_date', None)

        success = True
        message = _('Approved invoice successfully')
        requested_user = request.user

        if not (invoice_number or due_date):
            success = False
            message = _('Please enter all the details')

        action_owner = User.objects.filter(email=action_owner_email).first()
        invoice = CustomerInvoice.objects.filter(invoice_number__in=invoice_number).first()

        if not can_approve(invoice):
            success = False
            message = _('Cannot approve this invoice')

        if not has_approval_permission(invoice, requested_user):
            success = False
            message = _('You do not have permission to approve this invoice')

        if success:
            notification_details = {
                'assignor_email': requested_user,
                'invoice': invoice,
                'invoice_id': invoice.invoice_number,
                'assignor_name': requested_user.name,
                'assignee_email': [action_owner_email],
                'status': customer_constants.APPROVED,
                'due_date': due_date
            }
            create_invoice_history(invoice.status, customer_constants.APPROVED, invoice,
                                   action_owner, due_date)
            invoice.current_owner = action_owner
            invoice.assigned_date = date.today()
            tz = pytz.timezone(settings.ACT_TIME_ZONE)
            invoice.invoice_date = timezone.now().astimezone(tz).date()
            invoice.timestamp_approved = invoice.invoice_date
            Email().email_invoice_assignment_notification(notification_details)

            invoice.save()

            if invoice.is_invoice_no_update_required():
                invoice.invoice_number = invoice.generate_invoice_number(
                    invoice.timestamp_approved, invoice.bh_source_state.code
                )

            if invoice.is_credit_debit_note:
                CreditDebitNote.generate_credit_debit_notes(invoice)
            else:
                # Regenerate PDF invoice
                from blowhorn.customer.helpers.invoice_helper import InvoiceHelper
                total_by_service_type = json.loads(invoice.total_by_service_type)
                fragment = invoice.contract_invoices.first()
                entity = fragment.entities.first()
                sell_rate = entity.sell_rate if entity else None

                bytes = InvoiceHelper().generate_invoice_as_pdf(
                    invoice, invoice.bh_source_state, invoice.invoice_state,
                    total_by_service_type=total_by_service_type, fragment=fragment,
                    sell_rate=sell_rate)

                if bytes:
                    content = ContentFile(bytes)
                    invoice.file.save(str(invoice.invoice_number) + ".pdf", content, save=True)
            invoice.save()

        return success, message

    def _send_invoice(self, invoice_data, request):
        """
        Email invoice(s) to customer
        """
        invoice_number = invoice_data.get('invoice_number', [])
        email = invoice_data.get('email', None)
        cc_email = invoice_data.get('cc_email', None)

        requesting_user = request.user

        error_list = []
        invoice_list = []
        zip_invoice = False
        message = _('Invoice(s) sent successfully')
        success = True

        if not invoice_number:
            success = False
            message = _('No invoice selected')

        qs = CustomerInvoice.objects.filter(invoice_number__in=invoice_number)

        if CustomerInvoice.objects.filter(id__in=qs).distinct('customer').count() > 1:
            success = False
            message = _('Cannot zip invoices of different customers')

        for invoice in qs:
            if invoice.status not in [customer_constants.APPROVED,
                                      customer_constants.SENT_TO_CUSTOMER]:
                error_list.append(invoice.id)
            else:
                invoice_list.append(invoice)

        if error_list:
            success = False
            message = _("Cannot Send Invoices in state other than APPROVED/"
                        "SENT TO CUSTOMER state. Check id %s") % error_list

        if not email:
            success = False
            message = _('No email selected from customer contacts')

        if success:
            if qs.count() > 1:
                zip_invoice = True
                _date = qs.aggregate(Min('service_period_start'),
                                     Max('service_period_end'))
                start_date = _date.get('service_period_start__min')
                end_date = _date.get('service_period_end__max')
                Email().send_invoice(qs, email, start_date, end_date,
                                     zip_invoice=zip_invoice, requesting_user=requesting_user,
                                     cc=cc_email)
            else:
                # If only one invoice is selected send it as a CustomerInvoice object
                customer_invoice = qs.first()

                start_date = customer_invoice.service_period_start
                end_date = customer_invoice.service_period_end

                due_date = date.today() + timedelta(days=10)
                due_date = due_date if customer_invoice.status == customer_constants.APPROVED else\
                    customer_invoice.due_date

                Email().send_invoice(customer_invoice, email, start_date, end_date,
                                     zip_invoice=zip_invoice, requesting_user=requesting_user,
                                     cc=cc_email)

                history = create_invoice_history(customer_invoice.status,
                                                 customer_constants.SENT_TO_CUSTOMER,
                                                 customer_invoice,
                                                 customer_invoice.current_owner, due_date,
                                                 created_by=requesting_user)

                customer_invoice.status = customer_constants.SENT_TO_CUSTOMER
                customer_invoice.modified_by = requesting_user
                customer_invoice.modified_date = timezone.now()
                customer_invoice.due_date = due_date

                if hasattr(customer_invoice, 'document'):
                    customer_invoice.document.history = history
                    customer_invoice.document.save()

                customer_invoice.assigned_date = date.today()
                customer_invoice.save()

        return success, message

    def _mark_as_customer_acknowledged(self, invoice_data, request):
        """
        Mark invoices as CUSTOMER ACKNOWLEDGED if valid.
        """

        invoice_number = json.loads(invoice_data.get('invoice_number', []))
        supporting_doc = invoice_data.get('supporting_doc', None)

        invalid_invoices = []
        processed_invoices = []
        history_data = []
        action_owners = []
        customer = None
        message = None
        success = True
        due_date = date.today() + timedelta(days=10)

        if not invoice_number:
            success = False
            message = _('No invoice selected')

        qs = CustomerInvoice.objects.filter(invoice_number__in=invoice_number)

        for invoice in qs:
            if supporting_doc and customer and customer != invoice.customer:
                success = False
                message = _(
                    'Cannot upload document for different customers.'
                    ' Choose invoices of same customer.')

            customer = invoice.customer

            if invoice.status == customer_constants.SENT_TO_CUSTOMER and is_city_sales_manager(
                invoice, request.user):

                processed_invoices.append(invoice.invoice_number)
                action_owners.append(invoice.current_owner.email if invoice.current_owner else None)
                invoice_history = InvoiceHistory(
                    old_status=customer_constants.SENT_TO_CUSTOMER,
                    new_status=customer_constants.CUSTOMER_ACKNOWLEDGED,
                    invoice=invoice,
                    action_owner=invoice.current_owner,
                    due_date=due_date,
                    created_by=request.user
                )
                history_data.append(invoice_history)

            else:
                invalid_invoices.append('%s' % invoice.pk)

        if invalid_invoices:
            success = False
            message = _(
                    "Failed to mark invoices as customer acknowledged: %s."
                    " Either status is not SENT TO CUSTOMER  or you don't have permission"
                    " to perform this action." % ', '.join(
                        invalid_invoices))
            return success, message

        if processed_invoices:
            processed, updated_count = set_invoice_status(
                request, processed_invoices, due_date, history_data,
                updated_status=customer_constants.CUSTOMER_ACKNOWLEDGED,
                supporting_doc=supporting_doc)
            if processed:
                action_owners = list(set(action_owners))
                send_invoice_action_notification.apply_async(
                    (customer_constants.CUSTOMER_ACKNOWLEDGED, processed_invoices,
                     action_owners,
                     request.user.email,), )
                message = _("{updated_count} invoices are marked as CUSTOMER ACKNOWLEDGED."
                            " {processed_invoices}".format(updated_count=updated_count,
                                                           processed_invoices=', '.join(
                                                               processed_invoices)))

        return success, message

    def _mark_as_customer_confirmed(self, invoice_data, request):
        """
        Mark invoices as CUSTOMER CONFIRMED if valid
        """

        invoice_number = json.loads(invoice_data.get('invoice_number', []))
        supporting_doc = invoice_data.get('supporting_doc', None)

        invalid_invoices = []
        processed_invoices = []
        history_data = []
        action_owners = []
        customer = None
        due_date = None
        message = None
        success = True

        if not invoice_number:
            success = False
            _('No invoice selected')

        qs = CustomerInvoice.objects.filter(invoice_number__in=invoice_number)

        for invoice in qs:
            if supporting_doc and customer and customer != invoice.customer:
                success = False
                _('Cannot upload document for different customers.'
                  ' Choose invoices of same customer.')

            customer = invoice.customer
            if invoice.status == customer_constants.CUSTOMER_ACKNOWLEDGED and \
                    is_city_sales_manager(invoice, request.user):

                processed_invoices.append(invoice.invoice_number)
                action_owners.append(invoice.current_owner.email if invoice.current_owner else None)
                invoice_history = InvoiceHistory(
                    old_status=customer_constants.CUSTOMER_ACKNOWLEDGED,
                    new_status=customer_constants.CUSTOMER_CONFIRMED,
                    invoice=invoice,
                    action_owner=None,
                    due_date=due_date,
                    created_by=request.user
                )
                history_data.append(invoice_history)

            else:
                invalid_invoices.append('%s' % invoice.pk)

        if invalid_invoices:
            success = False
            message = _("Failed to mark invoices as customer acknowledged: %s. \
                    Either status is not CUSTOMER ACKNOWLEDGED or "
                        "you don't have permission to perform this action." %
                        ', '.join(invalid_invoices))
            return success, message

        if processed_invoices:
            processed, updated_count = set_invoice_status(
                request, processed_invoices, due_date, history_data,
                updated_status=customer_constants.CUSTOMER_CONFIRMED, supporting_doc=supporting_doc)
            if processed:
                action_owners = list(set(action_owners))
                send_invoice_action_notification.apply_async(
                    (customer_constants.CUSTOMER_CONFIRMED, processed_invoices,
                     action_owners,
                     request.user.email,), )
                message = _("{updated_count} invoices are marked as CUSTOMER CONFIRMED. "
                            "{processed_invoices}".format(updated_count=updated_count,
                                                          processed_invoices=', '.join(
                                                              processed_invoices))
                            )

        return success, message

    def _mark_as_disputed(self, invoice_data, request):
        """
        Mark an invoice as CUSTOMER DISPUTED if valid
        """
        invoice_number = invoice_data.get('invoice_number', [])
        reason = invoice_data.get('reason', None)
        due_date = invoice_data.get('due_date', None)
        spoc = None
        message = _('Invoice status updated successfully')
        success = True

        if not (invoice_number and reason and due_date):
            success = False
            message = _('Please enter all the details')
        invoice = CustomerInvoice.objects.filter(invoice_number=invoice_number)

        if invoice.count() > 1:
            success = False
            message = _('Cannot mark multiple invoices as CUSTOMER DISPUTED')

        invoice = invoice.first()
        if invoice.status != customer_constants.CUSTOMER_ACKNOWLEDGED:
            success = False
            message = _('Only CUSTOMER ACKNOWLEDGED invoices can be marked as DISPUTED')

        if not within_dispute_threshold_date(invoice):
            success = False
            message = _(
                "Invoice cannot be marked DISPUTED as"
                " it's service period exceeded the Revenue threshold date")

        if success:
            invoice_history = InvoiceHistory.objects.filter(
                invoice=invoice, old_status=customer_constants.CREATED).latest('created_time')
            if invoice_history:
                spoc = invoice_history.created_by

            notification_details = {
                'assignor_email': request.user,
                'invoice': invoice,
                'assignor_name': request.user.name,
                'assignee_email': [str(spoc)] if spoc else None,
                'status': customer_constants.CUSTOMER_DISPUTED,
                'due_date': due_date,
                'reason': reason
            }

            history = create_invoice_history(invoice.status, customer_constants.CUSTOMER_DISPUTED,
                                             invoice, spoc, due_date,
                                             reason=reason)
            if hasattr(invoice, 'document'):
                invoice.document.invoice_history = history
                invoice.document.save()

            invoice.assigned_date = date.today()
            invoice.status = customer_constants.CUSTOMER_DISPUTED
            invoice.save()

            Email().email_invoice_assignment_notification(notification_details)
            send_invoice_action_notification.apply_async(
                (customer_constants.CUSTOMER_DISPUTED, invoice_number,
                 [spoc.email],
                 request.user.email,), )

        return success, message

    def _reject_invoice(self, invoice_data, request):
        """
        Mark an invoice as REJECTED if valid
        """
        invoice_number = invoice_data.get('invoice_number', [])
        reason = invoice_data.get('reason', None)
        due_date = invoice_data.get('due_date', None)
        spoc = None
        success = True
        message = _('Invoice status updated successfully')

        if not (invoice_number and reason and due_date):
            success = False
            message = _('Please enter all the details')
        invoice = CustomerInvoice.objects.filter(invoice_number=invoice_number)

        if invoice.count() > 1:
            success = False
            message = _('Cannot mark multiple invoices as REJECTED')

        invoice = invoice.first()
        if invoice.status != customer_constants.APPROVED:
            success = False
            message = _('Only APPROVED invoices can be REJECTED')

        if success:
            invoice_history = InvoiceHistory.objects.filter(
                invoice=invoice, old_status=customer_constants.CREATED).latest('created_time')
            if invoice_history:
                spoc = invoice_history.created_by

            notification_details = {
                'assignor_email': request.user,
                'invoice': invoice,
                'assignor_name': request.user.name,
                'assignee_email': [str(spoc)] if spoc else None,
                'status': customer_constants.REJECTED,
                'due_date': due_date,
                'reason': reason
            }
            history = create_invoice_history(invoice.status, customer_constants.REJECTED, invoice,
                                             spoc,
                                             due_date, reason=reason)
            invoice.current_owner = spoc
            invoice.assigned_date = date.today()
            invoice.status = customer_constants.REJECTED
            invoice.save()

            Email().email_invoice_assignment_notification(notification_details)
            send_invoice_action_notification.apply_async(
                (customer_constants.REJECTED, invoice_number,
                 [spoc.email],
                 request.user.email,), )
        return success, message

    invoice_action_mapping = {
        customer_constants.SENT_TO_CUSTOMER: _send_invoice,
        customer_constants.CUSTOMER_ACKNOWLEDGED: _mark_as_customer_acknowledged,
        customer_constants.CUSTOMER_CONFIRMED: _mark_as_customer_confirmed,
        customer_constants.CUSTOMER_DISPUTED: _mark_as_disputed,
        customer_constants.REJECTED: _reject_invoice
    }
