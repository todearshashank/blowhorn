# System and Django imports
from __future__ import absolute_import, unicode_literals
import json
from django.conf import settings
from django.db.models import Q

# 3rd party libraries
from celery import shared_task
# from celery.decorators import periodic_task
from celery.schedules import crontab

# blowhorn imports
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.common.decorators import redis_batch_lock
from blowhorn.common.notification import Notification
from blowhorn.users.mixins import NotificationMixin
from blowhorn.users.models import User
from blowhorn.apps.operation_app.v5.constants import NOTIFICATION_TEMPLATES
from blowhorn.users.constants import NOTIFICATION_GROUP_CUSTOMER_INVOICE
from blowhorn import celery_app
# from config.settings.base import celery_app


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES,
             ignore_result=False)
def push_notification_to_cosmos(self, data_for_action, action_owners=None,
                                data_for_broadcast=None, broadcast_list=None):
    data_for_action = json.loads(data_for_action)
    if action_owners:
        Notification.push_to_fcm_devices(
            None,
            data_for_action.get('message', ''),
            data_for_action,
            user_pks=action_owners
        )

    data_for_broadcast = json.loads(data_for_broadcast)
    if broadcast_list:
        Notification.push_to_fcm_devices(
            None,
            data_for_broadcast.get('message', ''),
            data_for_broadcast,
            user_pks=broadcast_list
        )

@celery_app.task
# @periodic_task(run_every=(crontab(hour="00", minute="00")))
@redis_batch_lock(period=84600, expire_on_completion=True)
def expire_notifications():
    """
    Expires the app notifications
    SCHEDULED AT 05:30AM
    """
    NotificationMixin().expire_notifications()


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES,
             ignore_result=False)
def send_invoice_action_notification(self, action, invoice_number, action_owner, requested_user):
    from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
    users = []

    requested_user = User.objects.filter(email=requested_user).first()

    if action_owner:
        current_owner = list(
            User.objects.filter(email__in=action_owner).values_list('id', flat=True))
        users.extend(current_owner) if current_owner else None

    extra_context = {
        'invoice_number': invoice_number,
        'action_by': requested_user.email,
        'remarks': ''
    }

    message = NOTIFICATION_TEMPLATES.get(NOTIFICATION_GROUP_CUSTOMER_INVOICE).get(action).format(
        user=requested_user, invoice_number=invoice_number
    )

    NotificationMixin().save_and_broadcast_notification(
        broadcast_list=users,
        action_by=requested_user,
        extra_context=extra_context,
        notification_group=NOTIFICATION_GROUP_CUSTOMER_INVOICE,
        message=message
    )
