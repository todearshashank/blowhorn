# Django and System libraries
import json
import logging
from django import db
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.apps.operation_app.v3.helper import add_vehicle, add_vendor
from blowhorn.vehicle.serializers import VehicleDocumentPageSerializer
from blowhorn.address.models import State
from blowhorn.driver.models import DriverVehicleMap, Driver, DriverHistory
from blowhorn.document.models import DocumentType
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.document.constants import VEHICLE_DOCUMENT, REJECT
from blowhorn.vehicle.models import VehicleDocument, Vehicle, VehicleDocumentPage
from blowhorn.vehicle.serializers import VehicleDocumentSerializer
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.apps.operation_app.v3.services.driver import DriverAddUpdateMixin
from blowhorn.contract.models import Resource

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class AddVehicle(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    """Adds a vehicle"""

    def post(self, request):
        logging.info(
            "Request from the app for vehicle addition : " + str(json.loads(request.body)))
        try:
            data = json.loads(request.body)
            vehicle_number = data.get('vehicle_number')

            vehicle_data = {
                'registration_certificate_number': vehicle_number,
                'vehicle_model': data.get('vehicle_model'),
                'body_type': data.get('body_type'),
                'model_year': data.get('vehicle_model_year')
            }
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data="Invalid data sent")
        vehicles = Vehicle.objects.filter(
            registration_certificate_number=vehicle_number)
        is_vehicle_update = False

        if vehicles.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_("A vehicle with a given number already exists!"))
        else:
            try:
                vehicle = add_vehicle(
                    vehicle_params=vehicle_data,
                    is_update=is_vehicle_update
                )
                logging.info("Vehicle created with following details :" + str(vehicle))

            except BaseException as e:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=_("Failed to create the vehicle")
                )
        return Response(status=status.HTTP_200_OK, data=_("Vehicle added successfully!"))


class VehicleDocumentUpdate(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request):
        documents = request.data
        with db.transaction.atomic():
            for doc in documents:
                doc_id = doc.get('id', None)

                if not doc_id:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=_("Invalid request missing document ID"))

                driver_doc = VehicleDocument.objects.get(id=doc_id)
                if driver_doc.status != VERIFICATION_PENDING:
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data="Only Pending Document can be updated")

                state_name = doc.pop('state', None)
                if state_name:
                    doc['state'] = State.objects.get(name=state_name)

                doc_type = doc.get('document_type', None)
                if doc_type:
                    doc['document_type'] = DocumentType.objects.get(
                        code=doc_type)

                if request.user and request.user.is_authenticated:
                    doc['modified_by'] = request.user

                doc['modified_date'] = timezone.now()
                VehicleDocument.objects.filter(id=doc_id).update(**doc)
        return Response(status=status.HTTP_200_OK, data='Updated Successfully')


class VehicleDocumentList(views.APIView):
    """
        vehicle document list
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):

        resource = request.GET.get('resource', None)
        key = request.GET.get('key', None)

        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)
        veh_doc_list = []

        for doc_type in vehicle_document_type:
            document_pages = []
            doc_data = {
                'code': doc_type.code,
                'name': doc_type.name,
                'identifier': doc_type.identifier,
                'is_expiry_required': doc_type.is_expire_needed
            }

            query = Q(vehicle__driver__id=key) if resource == 'driver' else Q(
                vehicle__vendor__id=key)
            query = query & Q(document_type=doc_type)
            doc_info = VehicleDocument.objects.filter(query)\
                .exclude(status=REJECT).order_by('-created_date').first()
            doc_pages = VehicleDocumentPage.objects.filter(vehicle_document=doc_info,
                                                           page_number__gt=1)
            for page in doc_pages:
                document_pages.append(VehicleDocumentPageSerializer(page).data)

            document_data = VehicleDocumentSerializer(doc_info).data
            document_data.update(
                {"document_pages": document_pages}
            )
            doc_data.update({
                'document': document_data
            })
            veh_doc_list.append(doc_data)

        return Response(status=status.HTTP_200_OK, data=veh_doc_list,
                        content_type='text/html; charset=utf-8')


class AddVehicleMap(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request, driver_id=None):
        data = request.data
        vehicle_number = data.get('vehicle_number')
        if Driver.objects.filter(driver_vehicle=vehicle_number)\
                .exclude(pk=driver_id).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Vehicle is already mapped to a driver'))

        stored_vehicle_map = DriverVehicleMap.objects.filter(
            driver_id=driver_id).select_related('vehicle')
        with transaction.atomic():
            if stored_vehicle_map.exists():
                stored_vehicle = stored_vehicle_map.first().vehicle.registration_certificate_number
                if stored_vehicle != vehicle_number:
                    stored_vehicle_map.delete()

            vehicles = Vehicle.objects.filter(
                registration_certificate_number=vehicle_number)
            vehicle_data = {
                'registration_certificate_number': vehicle_number,
                'vehicle_model': data.get('vehicle_model'),
                'body_type': data.get('body_type'),
                'model_year': data.get('vehicle_model_year')
            }
            is_vehicle_update = False
            if vehicles.exists():
                is_vehicle_update = True
                vehicle = add_vehicle(
                    vehicle_params=vehicle_data,
                    is_update=is_vehicle_update
                )
            else:
                try:
                    vehicle = add_vehicle(
                        vehicle_params=vehicle_data,
                        is_update=is_vehicle_update
                    )
                except BaseException as be:
                    logger.info('Failed to create vehicle: %s' % be)
                    return Response(
                        status=status.HTTP_400_BAD_REQUEST,
                        data=_('Failed to update vehicle'))

            vehicle_map_details = {
                'driver_id': driver_id,
                'vehicle': vehicle
            }
            if not DriverVehicleMap.objects.filter(
                    **vehicle_map_details).exists():
                try:
                    vehicle_map = DriverVehicleMap.objects.create(
                        **vehicle_map_details)
                except BaseException as be:
                    logger.info('Error: %s' % be)
                    return Response(
                        status=status.HTTP_400_BAD_REQUEST,
                        data=_('Failed to map vehicle to driver'))

            # is_owner = data.get('is_owner')
            # vendor = None
            driver_data = {
                'driver_vehicle': vehicle_number,
                'modified_date': timezone.now(),
                # 'own_vehicle': is_owner
            }
            # print("is_owner :",is_owner)
            # if not is_owner:
            #     try:
            #         vendor_details = {
            #             'name': data.get('owner_name'),
            #             'phone_number': data.get('owner_phonenumber'),
            #             'pan': data.get('owner_pan'),
            #         }
            #         vendor = add_vendor(vendor_details)
            #
            #     except BaseException as be:
            #         logger.info('Error: %s' % be)
            #         return Response(
            #             status=status.HTTP_400_BAD_REQUEST,
            #             data=_('Failed to update vendor details'))

            if request.user:
                driver_data['modified_by'] = request.user

            # driver_data['owner_details'] = vendor
            driver_qs = Driver.objects.filter(pk=driver_id).select_related('owner_details')

            driver = driver_qs.first()
            driver_history_list = DriverAddUpdateMixin()._generate_driver_history(
                driver, driver_data, driver.owner_details, request.user)
            if driver_history_list:
                DriverHistory.objects.bulk_create(driver_history_list)

            updated_driver = driver_qs.update(**driver_data)
            logger.info('Vehicle mapping updated for driver: %s', updated_driver)
            return Response(status=status.HTTP_200_OK,
                            data={
                                'id': vehicle.id
                            })


class RemoveVehicleMapping(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request, driver_id=None):
        vehicle_map = DriverVehicleMap.objects.filter(driver_id=driver_id)
        logger.info('Removing vehicle mapping for driver %s: %s'
                    % (driver_id, vehicle_map))
        try:
            vehicle_map.delete()
            driver_data = {
                'owner_details': None,
                'driver_vehicle': None,
                'modified_date': timezone.now(),
            }
            driver_qs = Driver.objects.filter(pk=driver_id).select_related('owner_details')
            driver = driver_qs.first()

            if driver.status == StatusPipeline.ACTIVE:
                driver_data['status'] = StatusPipeline.ONBOARDING

            if request.user:
                driver_data['modified_by'] = request.user

            driver_history_list = DriverAddUpdateMixin()._generate_driver_history(
                driver, driver_data, driver.owner_details, request.user)
            updated_driver = driver_qs.update(**driver_data)

            if driver_history_list:
                DriverHistory.objects.bulk_create(driver_history_list)

            logger.info('Vehicle removed from driver: %s', updated_driver)
            return Response(status=status.HTTP_200_OK,
                            data=_('Vehicle has been unmapped from driver'))

        except BaseException as be:
            logger.info('Error: %s' % be)
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_('Failed to unmap vehicle from driver'))
