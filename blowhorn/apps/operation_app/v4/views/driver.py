from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from blowhorn.apps.operation_app.v3.services.driver import *
from blowhorn.driver.serializers import DriverDocumentPageSerializer
from blowhorn.document.models import DocumentType
from blowhorn.document.constants import VERIFICATION_PENDING
from blowhorn.document.constants import DRIVER_DOCUMENT, REJECT
from blowhorn.apps.operation_app.v5.constants import VENDOR_MANDATORY_DOCUMENTS
from blowhorn.vehicle.models import VendorDocument, VendorDocumentPage
from blowhorn.vehicle.serializers import VendorDocumentSerializer, VendorDocumentPageSerializer
from blowhorn.users.permission import IsBlowhornStaff

Driver = get_model("driver", "Driver")
DriverVehicleMap = get_model("driver", "DriverVehicleMap")
DriverDocument = get_model("driver", "DriverDocument")
DriverPreferredLocation = get_model("driver", "DriverPreferredLocation")
ResourceAllocation = get_model("contract", "ResourceAllocation")
State = get_model("address", "State")

VEHICLE_EXIST = "Vehicle with corresponding registration number already exists"

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverDocumentUpdate(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def put(self, request):
        documents = request.data
        with db.transaction.atomic():
            for doc in documents:
                doc_id = doc.pop('id')
                if doc['status'] != VERIFICATION_PENDING:
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data="Only Pending Document can be updated")

                state_name = doc.pop('state', None)
                if state_name:
                    doc['state'] = State.objects.get(name=state_name)

                doc_type = doc.get('document_type', None)
                if doc_type:
                    doc['document_type'] = DocumentType.objects.get(
                        code=doc_type)

                if request.user and request.user.is_authenticated:
                    doc['modified_by'] = request.user

                doc['modified_date'] = timezone.now()
                DriverDocument.objects.filter(id=doc_id).update(**doc)
        return Response(status=status.HTTP_200_OK, data='Updated Successfully')


class DriverDocumentList(views.APIView):
    """
        driver document list
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):

        resource = request.GET.get("resource", None)
        key = request.GET.get("key", None)

        if resource == "driver":
            document_type = DocumentType.objects.filter(
                identifier=DRIVER_DOCUMENT)
        else:
            document_type = DocumentType.objects.filter(
                name__in=VENDOR_MANDATORY_DOCUMENTS)

        doc_list = []

        for doc_type in document_type:
            document_pages = []
            doc_data = {
                "code": doc_type.code,
                "name": doc_type.name,
                "identifier": doc_type.identifier,
                'is_expiry_required': doc_type.is_expire_needed,
            }
            if resource == "driver":
                doc_info = DriverDocument.objects.filter(
                    driver_id=key, document_type=doc_type
                ).exclude(status=REJECT).order_by('-created_date').first()
                doc_serializer = DriverDocumentSerializer

                doc_pages = DriverDocumentPage.objects.filter(driver_document=doc_info,
                                                              page_number__gt=1)
                for page in doc_pages:
                    document_pages.append(DriverDocumentPageSerializer(page).data)
            else:
                doc_info = VendorDocument.objects.filter(
                    vendor_id=key, document_type=doc_type
                ).exclude(status=REJECT).order_by('-created_date').first()
                doc_serializer = VendorDocumentSerializer

                doc_pages = VendorDocumentPage.objects.filter(vendor_document=doc_info,
                                                              page_number__gt=1)
                for page in doc_pages:
                    document_pages.append(VendorDocumentPageSerializer(page).data)

            document_data = doc_serializer(doc_info).data
            document_data.update(
                {"document_pages": document_pages}
            )
            doc_data.update(
                {"document": document_data})
            doc_list.append(doc_data)

        return Response(
            status=status.HTTP_200_OK,
            data=doc_list,
            content_type="text/html; charset=utf-8",
        )
