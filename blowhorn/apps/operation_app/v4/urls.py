from django.conf.urls import url
from blowhorn.apps.operation_app.v3.views import user as user_views
from blowhorn.apps.operation_app.v3.views import driver as driver_views
from blowhorn.apps.operation_app.v3.views import vehicle as vehicle_views
from blowhorn.apps.operation_app.v3.views import order as order_views
from blowhorn.apps.operation_app.v3.views import document as document_views

from blowhorn.apps.operation_app.v4.views.driver import DriverDocumentList, \
    DriverDocumentUpdate
from blowhorn.apps.operation_app.v4.views.vehicle import VehicleDocumentList, \
    VehicleDocumentUpdate, AddVehicleMap, RemoveVehicleMapping

urlpatterns = [
            url(r"^login$",
                user_views.Login.as_view(),
                name="operation-user-login"),
            url(r"^fcmid/$",
                user_views.UserFCM.as_view(),
                name="operation-user-login"),
            url(
                r"^drivers/verification$",
                driver_views.BackGroundVerificationView.as_view(),
                name="background-verification",
            ),
            url(
                r"^drivers/verification/eligibility/(?P<pk>[0-9]+)/$",
                driver_views.BackgroundVerificationEligibility.as_view(),
                name="background-verification-eligibility",
            ),
            url(r"^drivers/$",
                driver_views.DriversList.as_view(),
                name="driver-list"),
            url(
                r"^drivers/(?P<pk>[0-9]+)/$",
                driver_views.DriverDetails.as_view(),
                name="driver-details",
            ),
            url(
                r"^drivers/documents/upload$",
                driver_views.DriverDocumentUpload.as_view(),
                name="driver-document-upload",
            ),
            url(
                r"^drivers/documents/update$",
                DriverDocumentUpdate.as_view(),
                name="driver-document-update",
            ),
            url(
                r"^drivers/documents/$",
                DriverDocumentList.as_view(),
                name="driver-document-list",
            ),
            url(
                r"^driver/photo/$",
                driver_views.DriverImage.as_view(),
                name="driver-document-list",
            ),
            url(
                r"^documents/verification$",
                document_views.DocumentVerification.as_view(),
                name="pending-document-list",
            ),
            url(
                r"^documents/pending-verification$",
                document_views.DocumentPendingVerification.as_view(),
                name="pending-document-list-uploaded-by-a-user",
            ),
            url(
                r"^documents/activate/(?P<pk>[0-9]+)/$",
                document_views.ActivateDocument.as_view(),
                name="activate-document-v4",
            ),
            url(
                r"^documents/reject/(?P<pk>[0-9]+)/$",
                document_views.RejectDocument.as_view(),
                name="reject-document-v4",
            ),
            url(
                r"^vehicles/vehiclemodels$",
                vehicle_views.VehicleModelList.as_view(),
                name="vehicle-model-list",
            ),
            url(
                r"^vehicles/available-vehicles$",
                vehicle_views.AvailableVehicleList.as_view(),
                name="available-vehicles",
            ),
            url(
                r"^vehicles/documents/upload$",
                vehicle_views.VehicleDocumentUpload.as_view(),
                name="vehicle-document-upload",
            ),
            url(
                r"^vehicles/documents/update$",
                VehicleDocumentUpdate.as_view(),
                name="vehicle-document-update",
            ),
            url(
                r"^vehicles/documents/$",
                VehicleDocumentList.as_view(),
                name="vehicle-document-list",
            ),
            url(
                r"^orders/spot$",
                order_views.CreatSpotOrder.as_view(),
                name="order-spot-create",
            ),
            url(
                r"^driver/vehicle/remove/(?P<driver_id>[0-9]+)/$",
                RemoveVehicleMapping.as_view(),
                name="remove-driver-vehicle-map",
            ),
            url(
                r"^driver/vehicle/add/(?P<driver_id>[0-9]+)/$",
                AddVehicleMap.as_view(),
                name="add-driver-vehicle-map",
            ),
        ]

