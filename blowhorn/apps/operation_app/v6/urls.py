from django.conf.urls import url, include
from blowhorn.apps.operation_app.v6.views import driver as driver_views
from blowhorn.apps.operation_app.v6.views import order as order_views
from blowhorn.apps.operation_app.v6.views.user import NotificationView

urlpatterns = [
            url(
                r"^drivers/$",
                driver_views.DriversList.as_view(),
                name="driver-list"
            ),
            url(
                r"^drivers/(?P<pk>[0-9]+)/$",
                driver_views.DriverDetails.as_view(),
                name="driver-details",
            ),
            url(
                r"^shipments/filterdata",
                order_views.ShipmentOrderFilterData.as_view(),
                name="shipments-filter-data"
            ),
            url(
                r"^shipments/list",
                order_views.ShipmentOrderListView.as_view(),
                name="shipment-orders-list"
            ),
            url(
                r"^shipments/create_trip",
                order_views.ShipmentOrderCreateTrip.as_view(),
                name="shipment-orders-create-trip"
            ),
            url(
                r"^shipments/assign_orders",
                order_views.ShipmentOrderAssignDriver.as_view(),
                name="shipment-orders-assign"
            ),
            url(
                r"^drivers/link$",
                driver_views.DriverAppLink.as_view(),
                name="driver-share-app-link",
            ),
            url(
                r"^notifications$",
                NotificationView.as_view(),
                name="fetch-notifications",
            ),

        ]
