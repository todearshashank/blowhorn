# Django and System libraries
import datetime
import logging
from django.contrib.auth import get_user_model
from django.conf import settings as setting
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from blowhorn.address.models import City
from blowhorn.oscar.core.loading import get_model
from phonenumber_field.phonenumber import PhoneNumber

# blowhorn imports
from blowhorn.users.constants import OPS_MANAGERS
from config.settings import status_pipelines as StatusPipeline
from blowhorn.apps.operation_app.v2.serializers import vehicle as \
    vehicle_serializer
from blowhorn.apps.operation_app.v3.serializers import driver \
    as driver_serializer
from blowhorn.utils import functions as utils_func
from blowhorn.common.helper import CommonHelper
from blowhorn.apps.mixins import *
from blowhorn.driver.constants import GENDER_MALE
from blowhorn.address.permissions import is_city_manager
from blowhorn.address.utils import get_city_from_latlong
from blowhorn.driver.models import DriverVehicleMap, DriverPayment, APPROVED
from blowhorn.users.models import UserLoginDevice, LoginDeviceHistory


User = get_user_model()
Country = get_model('address', 'Country')
Driver = get_model('driver', 'Driver')
DriverPreferredLocation = get_model('driver', 'DriverPreferredLocation')
Vehicle = get_model('vehicle', 'Vehicle')
VehicleClass = get_model('vehicle', 'VehicleClass')
VehicleModel = get_model('vehicle', 'VehicleModel')
BodyType = get_model('vehicle', 'BodyType')
Vendor = get_model('vehicle', 'Vendor')

VEHICLE_EXIST = 'Vehicle with corresponding registration number already exists'
PHONE_NUMBER_EXIST = "Phone Number already exists"


def get_address_data(address, country):
    dict_ = {
        'coordinates': address.get('coordinates'),
        'line1': address.get('fullAddress'),
        'line4': address.get('city'),
        'state': address.get('state'),
        'postcode': address.get('postalCode'),
        'country': country.pk,
        'search_text': address.get('fullAddress')
    }

    return dict_


def get_detailed_data(params):
    """
        Fragmentize the requested parameters into various dictionary of
        parameters needed for creating the records of a specific relation
    """
    country_code = setting.COUNTRY_CODE_A2
    country = Country.objects.get(pk=country_code)
    vehicle_parameters = params['vehicles'][0] \
        if params.get('vehicles', []) else {}

    current_address = {}
    permanent_address = {}

    driver_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'can_drive_and_deliver': params.get('can_drive_and_deliver', ''),
        'can_source_additional_labour': params.get(
            'can_source_additional_labour', ''),
        # 'driving_license_number': params.get('driving_license_number', ''),
        'work_status': params.get('work_status', ''),
        'working_preferences': params.get('working_preferences', []),
        'own_vehicle': params.get('driver_is_owner', False),
        'operating_city': params.get('operating_city', ''),
        # 'pan': params.get('pan_number', ''),
        'prior_monthly_income': params['prior_income'] if params.get(
            'prior_income', None) else None,
        'gender': params['gender'].lower() if params.get(
            'gender') else GENDER_MALE,
        'reason_for_inactive': params['reason_for_inactive']
        if params.get('reason_for_inactive') else None,
        'description': params['remark']
        if params.get('remark') else None
    }

    vehicle_data = {
        'vehicle_model': vehicle_parameters.get('vehicle_model', ''),
        'vehicle_class': vehicle_parameters.get('vehicleClass', ''),
        'body_type': vehicle_parameters.get('body_type_name', ''),
        'registration_certificate_number': params['driver_vehicle'].upper()
        if params.get('driver_vehicle', '') else '',
        'model_year': vehicle_parameters.get('vehicle_model_year', '')
    }

    user_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'password': params.get('password', None),
        'email': params.get('email', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'user_id': None,
        'is_active': True if params.get('status') == StatusPipeline.ACTIVE
        else False
    }

    vendor_data = {
        'phone_number': params.get('owner_contact_no', ''),
        'name': params.get('owner_name', ''),
        'pan': params.get('owner_pan_number', '')
    }

    if params.get('date_of_birth'):
        driver_data['date_of_birth'] = datetime.datetime.strptime(
            params['date_of_birth'], "%d/%m/%Y").date()
    if params.get('father_name'):
        driver_data['father_name'] = params['father_name']

    if params.get('current_address') and params.get(
            'current_address', {}).get('fullAddress'):
        current_address = get_address_data(params['current_address'], country)
        driver_data['is_address_same'] = params.get('is_address_same', False)

        if params.get('is_address_same', False):
            permanent_address = get_address_data(
                params['current_address'], country)
        else:
            permanent_address = get_address_data(
                params['permanent_address'], country)

    detailed_data = {
        'driver_data': driver_data,
        'vehicle_data': vehicle_data,
        'user_data': user_data,
        'vendor_data': vendor_data,
        'current_address': current_address,
        'permanent_address': permanent_address
    }

    return detailed_data


def validate_operation_data(params, is_update=False, user=None):
    """
        Validating the requested data and raising error(if any).
    """
    response = {'error': '', 'success': False}

    # To check if the requested dictionary is not empty
    if not params:
        logging.info("Driver details not found")
        response["error"] = "Driver details not found"
        return response

    # To validate the name of the driver
    if params.get('name') and not utils_func.validate_name(params.get('name', '')):
        logging.info("Please Enter a valid name")
        response['error'] = 'Please Enter a valid name'
        return response

    # To validate the date of birth
    if params.get('date_of_birth'):
        try:
            utils_func.validate_age(params['date_of_birth'], "%d/%m/%Y")
        except ValidationError as e:
            logging.info(
                "Error while validating the driver age : %s " % e)
            response['error'] = str(e.args[0])
            return response

    # To validate the phone number of the driver
    try:
        utils_func.validate_phone_number(params.get('mobile', ''))
    except:
        logging.info(
            "Invalid phone number")
        response['error'] = _('Please enter a valid mobile number')
        return response

    if params.get('driving_license_number') and is_update:
        driver = Driver.objects.filter(driving_license_number=params.get('driving_license_number')). \
            exclude(id=params.get('id'))
        if driver.exists():
            response["error"] = "DL already exists! Check %s" % str(driver.first())
            return response


    if params.get('driver_vehicle'):
        if not params.get('vehicles', []):
            logging.info(
                "No Vehicle Details Provided")
            response['error'] = "Please provide vehicle details"
            return response

        if not params['vehicles'][0].get('body_type_name', None):
            logging.info(
                "Error while validating the vehicle body type")
            response['error'] = "Body Type of the vehicle is needed"
            return response

        if not params['vehicles'][0].get('vehicle_model', None):
            logging.info(
                "Error while validating the vehicle model")
            response['error'] = "Model of vehicle is needed"
            return response

        if not params['vehicles'][0].get('vehicle_model_year', None):
            logging.info(
                "Error while validating the vehicle model year")
            response['error'] = "Model year is needed for the vehicle"
            return response

    if params.get('current_address') and params.get(
            'current_address', {}).get('fullAddress') and is_update:
        if not params['current_address'].get('postalCode'):
            logging.info("Postal code not provided for the current address")
            response['error'] = "Postal Code is needed for the current address"
            return response

        if not params['current_address'].get('city'):
            logging.info("City not provided for the current address")
            response['error'] = "City is needed for current address"
            return response

        if not params['current_address'].get('state'):
            logging.info("State not provided for the current address")
            response['error'] = "State is needed for the current address"
            return response

    if params.get('current_address') and params.get(
            'current_address', {}).get('postalCode') and is_update:
        if not params['current_address'].get('fullAddress'):
            logging.info("Full Address not provided for the current address")
            response['error'] = "Full address is needed for the current "
            "address"
            return response

        if not params['current_address'].get('city'):
            logging.info("City not provided for the current address")
            response['error'] = "City is needed for current address"
            return response

        if not params['current_address'].get('state'):
            logging.info("State not provided for the current address")
            response['error'] = "State is needed for the current address"
            return response

    if params.get('current_address', {}).get('fullAddress') and not params.get(
            'is_address_same', False) and not (params.get(
                'permanent_address', {}).get('postalCode', '') or params.get(
                'permanent_address', {}).get('fullAddress', '')) and is_update:
        logging.info(
            "Permanent Address is not provided as current address is not "
            "same as permanent address")
        response['error'] = "Permanent Address is needed"
        return response

    if is_update and not params.get('is_address_same', False):

        if params.get('permanent_address') and params.get(
                'permanent_address', {}).get('fullAddress'):
            if not params['permanent_address'].get('postalCode'):
                logging.info(
                    "Postal code not provided for the permanent address")
                response['error'] = "Postal Code is needed for the permanent "
                "address"
                return response

            if not params['permanent_address'].get('city'):
                logging.info("City not provided for the permanent address")
                response['error'] = "City is needed for the permanent address"
                return response

            if not params['permanent_address'].get('state'):
                logging.info("State not provided for the permanent address")
                response['error'] = "State is needed for the permanent address"
                return response

        if params.get('permanent_address') and params.get(
                'permanent_address', {}).get('postalCode'):
            if not params['permanent_address'].get('fullAddress'):
                logging.info(
                    "Full Address not provided for the permanent address")
                response['error'] = "Full address is needed for the permanent "
                "address"
                return response

            if not params['permanent_address'].get('city'):
                logging.info("City not provided for the permanent address")
                response['error'] = "City is needed for the permanent address"
                return response

            if not params['permanent_address'].get('state'):
                logging.info("State not provided for the permanent address")
                response['error'] = "State is needed for the permanent address"
                return response

        # To validate the driving licence and owner details(
        # *in case if driver is not the owner) of the driver
        # if params.get('driver_activation', False):
        # if params.get('driving_license_number', '') == params.get(
        # 'mobile', ''):
        #     logging.info('Driving License Number field is empty')
        #     response['error'] = 'Enter a valid driving license number'
        #     return response

    # if not params.get('driver_is_owner', False) and is_update:
    #     if not params.get('owner_name', ''):
    #         logging.info("Owner name not provided")
    #         response['error'] = "Owner's name is mandatory"
    #         return response
    #
    #     if not utils_func.validate_name(params['owner_name']):
    #         logging.info("Please Enter a valid name")
    #         response['error'] = "Please Enter a valid Owner name"
    #         return response
    #
    #     try:
    #         utils_func.validate_phone_number(
    #             params.get('owner_contact_no', ''))
    #     except ValidationError as e:
    #         logging.info(
    #             "Error while validating the owner phone number : %s " % e)
    #         response['error'] = str(e.args[0]) + ' for Fleet Owner'
    #         return response

    response['success'] = True
    return response


def get_user_for_driver(user_data):
    """
        Creating/Updating the user details for the driver
    """
    user = Driver.objects.upsert_user(
        email=user_data.get('email', None),
        name=user_data.get('name', ''),
        phone_number=user_data.get('phone', None),
        password=user_data.get('password', None),
        user_id=user_data.get('user_id', None),
        is_active=True if user_data[
            'status'] == StatusPipeline.ACTIVE else False
    )

    return user


def get_vehicle_models():
    """
        Fetch all the vehicle models available
    """
    vehicle_model = VehicleModel.objects.all()
    body_types = BodyType.objects.values_list('body_type', flat=True)
    serialized_vehicle_data = vehicle_serializer.VehicleModelSerializer(
        vehicle_model, many=True, context={'body_types': body_types})

    return serialized_vehicle_data.data


def add_vehicle(vehicle_params, is_update):
    """
        Creating/Updating vehicle details for the driver
    """

    vehicle_model = VehicleModel.objects.filter(
        model_name=vehicle_params.get('vehicle_model')).first()
    body_type = BodyType.objects.filter(
        body_type=vehicle_params.get('body_type')).first()

    vehicle_data = {
        'vehicle_model': vehicle_model.id if vehicle_model else None,
        'body_type': body_type.id if body_type else None,
        'registration_certificate_number': vehicle_params.get(
            'registration_certificate_number'),
        'model_year': vehicle_params.get('model_year')

    }

    vehicle_number = vehicle_params.get('registration_certificate_number', None)
    vehicle_instance = None
    mapping_exists = False
    if vehicle_number:
        vehicle_instance = Vehicle.objects.filter(
                registration_certificate_number=vehicle_number).first()
        mapping_exists = DriverVehicleMap.objects.filter(
            vehicle=vehicle_instance).exists()

    if is_update or (vehicle_instance and not mapping_exists):
        vehicle = UpdateModelMixin().update(
            data=vehicle_data, instance=vehicle_instance,
            serializer_class=vehicle_serializer.VehicleAddSerializer)
    else:
        vehicle = CreateModelMixin().create(
            data=vehicle_data,
            serializer_class=vehicle_serializer.VehicleAddSerializer)

    logging.info(
        "This is the vehicle created for the driver : %s" % vehicle)

    return vehicle


def add_vendor(vendor_details):
    """
        Creatingthe owner for the driver
    """

    phone_number = PhoneNumber(setting.ACTIVE_COUNTRY_CODE, vendor_details.get('phone_number'))

    vendor = Vendor.objects.filter(phone_number=phone_number).first()

    if not vendor:
        vendor_details['source'] = 'App'
        vendor = CreateModelMixin().create(
            data=vendor_details,
            serializer_class=vehicle_serializer.VendorSerializer)

    logging.info("This is the vendor created for the driver : %s" % vendor)

    return vendor


def add_preferred_location(location_data, driver):
    """
        Add the preferred locations for the Driver
    """
    logging.info(
        "This is the preferred location data for the "
        "driver : %s " % location_data)
    preferred_location_data = []
    DriverPreferredLocation.objects.filter(driver=driver).delete()
    for cord_data in location_data:
        if isinstance(cord_data, dict) and cord_data.get('coordinates', {}):
            latitude = cord_data.get(
                'coordinates').get('latitude', None)
            longitude = cord_data.get(
                'coordinates').get('longitude', None)

            logging.info('Latitude:- %s longitude: %s',
                         latitude, longitude)

            if latitude and longitude:
                geopoint = CommonHelper.get_geopoint_from_latlong(
                    latitude, longitude)

                preferred_location_data.append(DriverPreferredLocation(
                    geopoint=geopoint, driver_id=driver.id))

    if not preferred_location_data:
        logging.info("No Preferred location was provided for the driver")
        raise ValidationError("Preferred Location is mandatory")

    DriverPreferredLocation.objects.bulk_create(preferred_location_data)


def add_address_details_for_driver(address_data):
    geopoint = None
    if address_data.get('coordinates', {}):
        latitude = address_data['coordinates'].get('latitude', None)
        longitude = address_data['coordinates'].get('longitude', None)

        logging.info('Latitude:- %s longitude:- %s', latitude, longitude)

        if latitude and longitude:
            geopoint = CommonHelper.get_geopoint_from_latlong(
                latitude, longitude)
        address_data['geopoint'] = geopoint
        address_data.pop('coordinates')
    address_data['country'] = setting.COUNTRY_CODE_A2
    address = CreateModelMixin().create(
        data=address_data,
        serializer_class=driver_serializer.DriverAddressSerializer)

    return address


def is_superuser_or_city_manager(user, location_details):
    super_user = False
    city_manager = False

    latitude = location_details.get('mLatitude', None)
    longitude = location_details.get('mLongitude', None)

    city = get_city_from_latlong(latitude, longitude)
    super_user = user.is_superuser
    if city:
        city_manager = is_city_manager(city, user)

    access = super_user or city_manager

    return access


def save_device_data(user, device_data):
    """
    Save device details of the users
    """
    qs = UserLoginDevice.objects.filter(device_user=user)
    if qs.exists():
        obj = qs.first()
        obj.device_user = user
        obj.imei_number = device_data.get('imei_number')
        obj.uuid = device_data.get('uuid')
        obj.device_id = device_data.get('device_id')
        obj.last_login_time = timezone.now()
        obj.save()
    else:
        device_data['device_user'] = user
        obj = UserLoginDevice.objects.create(**device_data)
    return obj
