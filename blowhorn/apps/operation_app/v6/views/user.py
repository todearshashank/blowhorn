from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.apps.operation_app.v6.services.notifications import AppNotificationDetails


class NotificationView(views.APIView, NotificationMixin):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        user = request.user

        try:
            response_data = AppNotificationDetails(user=user).get_user_notification_data(request)
        except Exception as e:
            message = e.args[0]
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        return Response(status=status.HTTP_200_OK, data=response_data)
