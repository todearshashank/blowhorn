import json

from django import db
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from config.settings import status_pipelines as StatusPipeline
from blowhorn.common import sms_templates
from blowhorn.apps.operation_app.v6.constants import *
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.apps.operation_app.v6.helper import validate_operation_data
from blowhorn.apps.operation_app.v6.services.driver import *
from blowhorn.driver.util import get_driver_missing_doc
from blowhorn.common.communication import Communication




logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverDetails(views.APIView):
    """
        Retrieve or update the driver instance
    """

    permission_classes = (IsAuthenticated,IsBlowhornStaff)

    def get(self, request, pk, format=None):
        """
            Returns the driver record data corresponding to the driver id
        """
        data = {"result": [], "next": 0, "previous": 0, "count": 0}

        driver_qs = Driver.objects.filter(id=pk)

        driver_qs = driver_qs.select_related(
            "owner_details",
            "operating_city",
            "current_address",
            "permanent_address",
            "user",
        )

        driver_qs = driver_qs.values(*DRIVER_LIST_FIELDS)

        if not driver_qs:
            logging.info("Invalid driver ID")
            return Response(
                data="Driver does not exist",
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )

        data["result"] = DriverListMixin().get_driver_detailed_list(driver_qs)
        return Response(status=status.HTTP_200_OK, data=data)

    def put(self, request, pk, format=None):
        """
            For updating the record of the driver corresponding to the id
        """
        driver = Driver.objects.filter(id=pk).first()
        old_status = driver.status

        if not driver:
            logging.info("Invalid driver ID")
            return Response(
                data="Driver does not exist",
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )
        logging.info("Driver for which the updation is needed : %s" %
                     str(driver))

        try:
            data = json.loads(request.body)
            driver_details = data.get("driver_details", {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get("driver_details", "{}"))
            logging.info("Request from the app dict : " +
                         str(request.data.dict()))

        response_dict = {"status": False, "message": ""}

        # if driver.owner_details and driver_details.get('driver_is_owner'):
        #     message = _("Cannot mark driver is owner true as the driver is mapped to a vendor")
        #     response_dict["message"] = message
        #     return Response(status=status.HTTP_400_BAD_REQUEST, data=response_dict)

        if driver_details.get("status") not in StatusPipeline.DRIVER_BANNED_STATUSES:
            validated_data = validate_operation_data(
                params=driver_details, is_update=True, user=request.user
            )

            if not validated_data.get("success"):
                response_dict["message"] = validated_data.get("error")
                return Response(
                    data=response_dict,
                    status=status.HTTP_400_BAD_REQUEST,
                    content_type="text/html; charset=utf-8",
                )
        else:
            try:
                changed_status = DriverAddUpdateMixin()._deactivate_driver(
                    driver_details, driver, request)

            except Exception as e:
                response_dict["message"] = e.args[0]
                return Response(
                    data=response_dict,
                    status=status.HTTP_400_BAD_REQUEST,
                    content_type="text/html; charset=utf-8",
                )

        if driver_details.get("status") == StatusPipeline.ACTIVE:
            missing_docs = get_driver_missing_doc(driver,
                                                  status=StatusPipeline.ACTIVE)
            if missing_docs:
                message = "Can't activate driver, docs missing - "
                message += ', '.join(missing_docs)
                response_dict["message"] = message
                return Response(status=status.HTTP_400_BAD_REQUEST, data=response_dict)

        with db.transaction.atomic():
            try:
                updated_driver = DriverAddUpdateMixin()._update_driver(
                    driver_details, driver, request
                )
                new_status = updated_driver['status']

                if (new_status != old_status) and new_status == StatusPipeline.ACTIVE:
                    message = sms_templates.template_driver_active
                    driver.user.send_sms(message['text'], message)

            except Exception as e:
                response_dict["message"] = e.args[0]
                return Response(
                    data=response_dict,
                    status=status.HTTP_400_BAD_REQUEST,
                    content_type="text/html; charset=utf-8",
                )

            return Response(status=status.HTTP_200_OK, data=updated_driver)


class DriversList(views.APIView):
    """
        List all the filtered driver records or create a new record
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def __get_pagination_data(self, queryset, page, num):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else objects.previous_page_number()
        )
        next = 0 if not objects.has_next() else objects.next_page_number()

        # pages_count = paginator.num_pages
        data = {
            "count": count,
            "previous": previous,
            "next": next,
            "objects": objects.object_list,
            "last_loaded_on": timezone.now().isoformat()
        }

        return data

    def get_queryset(self, query_params):
        filter_data = {}
        logging.info(
            "Following is the query params for the driver list :"
            " " + str(query_params)
        )

        page_to_be_displayed = (
            query_params.get("next")
            if query_params.get("next", "") and int(query_params["next"]) > 0
            else PAGE_TO_BE_DISPLAYED
        )
        logging.info(
            "Page to be displayed for the pagination "
            "purpose: % s" % page_to_be_displayed
        )

        number_of_entry = NUMBER_OF_ENTRY_IN_PAGE
        logging.info(
            "Number of entry per page for pagination " "purpose: %s" % number_of_entry
        )

        filter_data["operating_city__name"] = query_params.get(
            "operating_city")

        if query_params.get("timestamp", "") and query_params.get("timestamp") != 'null':
            try:
                timestamp = query_params.get('timestamp')
                # last_modified = ist_to_utc(timestamp)
            except Exception as e:
                logging.info("This is the error for the timestamp : %s " % e)
                return Response(
                    status=status.HTTP_404_NOT_FOUND,
                    data="Provide timestamp in correct format",
                )
            filter_data["modified_date__gte"] = timestamp

        driver_qs = Driver.objects.filter(
            **filter_data).order_by("status", "id")

        pagination_data = self.__get_pagination_data(
            driver_qs, page_to_be_displayed, number_of_entry
        )

        driver_qs = pagination_data.pop("objects")

        driver_qs = driver_qs.select_related("owner_details", "user")

        driver_qs = driver_qs.values(*DRIVER_LIST_FIELDS)

        return {"driver_list": driver_qs, "pagination_data": pagination_data}

    def get(self, request):
        """
            For fetching all the drivers for the given city
        """

        logging.info(
            "Following is the request data for fetching driver list : %s"
            % str(request.GET)
        )

        query_params = request.GET.dict()

        queryset_data = self.get_queryset(query_params)
        driver_list = queryset_data.get("driver_list")

        data = queryset_data.get("pagination_data")

        if driver_list:
            data["result"] = DriverListMixin().get_driver_list(driver_list)
            return Response(status=status.HTTP_200_OK, data=data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND, data="No driver found")

    def post(self, request):
        """
            For creating a new driver
        """

        try:
            data = json.loads(request.body)
            driver_details = data.get("driver_details", {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get("driver_details", "{}"))
            logging.info("Request from the app dict : " +
                         str(request.data.dict()))

        response_dict = {"status": False, "message": ""}

        validated_data = validate_operation_data(
            params=driver_details, is_update=False)

        if not validated_data.get("success"):
            response_dict["message"] = validated_data.get("error")
            return Response(
                data=response_dict,
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )

        try:
            created_driver = DriverAddUpdateMixin()._create_driver(
                driver_details, request
            )
        except Exception as e:
            logging.info("This is the exception : %s " % e)
            response_dict["message"] = e.args[0]
            return Response(
                data=response_dict,
                status=status.HTTP_400_BAD_REQUEST,
                content_type="text/html; charset=utf-8",
            )

        return Response(status=status.HTTP_200_OK, data=created_driver)


class DriverAppLink(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request):
        data = request.data
        mobile = data.get('mobile')

        if not mobile:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Provide Mobile number')

        if mobile:
            Communication.send_sms(
                sms_to=mobile,
                message=sms_templates.SHARE_APP_MESSAGE_DRIVER_APP['text'],
                priority='high',
                template_dict=sms_templates.SHARE_APP_MESSAGE_DRIVER_APP)
            logging.info("Sms sent to %s" % mobile)
            response_message = 'Link has been sent successfully to %s' % mobile
            return Response(status=status.HTTP_200_OK, data=_(response_message))

