import json
import pytz
import logging
import pandas as pd
from datetime import timedelta, datetime

from django import db
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.conf import settings
from django.db.models import Q


from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.parsers import MultiPartParser, FormParser
from blowhorn.oscar.core.loading import get_model
from config.settings import status_pipelines as StatusPipeline


from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.order.models import Order
from blowhorn.order.admin import apply_contract_permission_filter
from blowhorn.apps.operation_app.v6.services.order import assign_driver
from blowhorn.apps.operation_app.v6.constants import NO_OF_SHIPMENT_ORDERS_IN_PAGE, \
    PAGE_TO_BE_DISPLAYED

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Trip = get_model('trip', 'Trip')
Driver = get_model('driver', 'Driver')

SHIPMENT_STATUSES = [StatusPipeline.ORDER_NEW, StatusPipeline.DRIVER_ASSIGNED,
                     StatusPipeline.ORDER_PACKED, StatusPipeline.OUT_FOR_DELIVERY,
                     StatusPipeline.ORDER_RETURNED, StatusPipeline.ORDER_DELIVERED,
                     StatusPipeline.ORDER_RETURNED_TO_HUB, StatusPipeline.ORDER_LOST,
                     StatusPipeline.UNABLE_TO_DELIVER, StatusPipeline.DELIVERY_ACK]


class ShipmentOrderFilterData(views.APIView):
    """
        Retrieve list filter data for the shipment order view
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        user = request.user
        params = request.query_params

        start = params.get('date_placed_start', None)
        end = params.get('date_placed_end', None)
        start = datetime.strptime(start, '%d-%m-%Y') if start else None
        end = datetime.strptime(end, '%d-%m-%Y') if end else None
        statuses = SHIPMENT_STATUSES
        delivery_hubs = []
        pickup_hubs = []
        customers = []
        cities = []

        today = timezone.now()
        if not (start or end):
            start = today - timedelta(days=30)
            end = today + timedelta(days=10)
        orders = Order.objects.filter(
            pickup_datetime__range=(start, end),
            order_type=settings.SHIPMENT_ORDER_TYPE).exclude(
            customer_contract__customer__api_key=settings.FLASH_SALE_API_KEY)
        order_qs = apply_contract_permission_filter(user, orders)
        order_qs = order_qs.select_related(
        ).only('hub', 'pickup_hub', 'city', 'customer').distinct()
        orders_list = order_qs.values('hub_id', 'hub__name', 'pickup_hub_id', 'pickup_hub__name',
                                      'city__name', 'customer__name')

        for i in orders_list:
            delivery_hubs.append({
                'id': i.get('hub_id'),
                'name': i.get('hub__name')
            }) if i.get('hub_id') else None
            pickup_hubs.append({
                'id': i.get('pickup_hub_id'),
                'name': i.get('pickup_hub__name')
            }) if i.get('pickup_hub_id') else None
            customers.append(i.get('customer__name')) if i.get('customer__name') else None
            cities.append(i.get('city__name')) if i.get('city__name') else None
        response_data = {
            'statuses': statuses,
            'pickup_hubs': pd.DataFrame(pickup_hubs).drop_duplicates().to_dict('records'),
            'delivery_hubs': pd.DataFrame(delivery_hubs).drop_duplicates().to_dict('records'),
            'customers': list(set(customers)),
            'cities': list(set(cities))
        }
        return Response(status=status.HTTP_200_OK, data=response_data)


class ShipmentOrderListView(views.APIView):
    """
        Retrieve shipment orders for a user
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def __get_query(self):
        params = self.request.query_params
        query = Q(order_type=settings.SHIPMENT_ORDER_TYPE) & ~Q(
            customer_contract__customer__api_key=settings.FLASH_SALE_API_KEY)
        if not params:
            return query
        city = params.get('city', None)
        pickup_hub = params.get('pickup_hub', None)
        delivery_hub = params.get('delivery_hub', None)
        pickup_pincode = params.get('pickup_pincode', None)
        delivery_pincode = params.get('delivery_pincode', None)
        customer = params.get('customer', None)
        order_status = params.get('status', None)
        return_order = params.get('return_order', False)
        date_placed_start = params.get('date_placed_start', None)
        date_placed_end = params.get('date_placed_end', None)

        today = timezone.now()
        start = today - timedelta(days=30)
        end = today + timedelta(days=10)

        if not (date_placed_start or date_placed_end):
            query &= Q(pickup_datetime__range=(start, end))
        else:
            date_placed_start = datetime.strptime(date_placed_start, '%d-%m-%Y').date()
            date_placed_end = datetime.strptime(date_placed_end, '%d-%m-%Y').date()
            query &= (Q(date_placed__date__gte=date_placed_start) & Q(
                date_placed__date__lte=date_placed_end))
        if order_status:
            order_status = order_status.split(',')
            query &= Q(status__in=order_status)
        if pickup_hub:
            pickup_hub = pickup_hub.split(',')
            pickup_hub = list(map(int, pickup_hub))
            query &= Q(pickup_hub__id__in=pickup_hub)
        if delivery_hub:
            delivery_hub = delivery_hub.split(',')
            delivery_hub = list(map(int, delivery_hub))
            query &= Q(hub__id__in=delivery_hub)
        if pickup_pincode:
            pickup_pincode = pickup_pincode.split(',')
            query &= Q(pickup_address__postcode__in=pickup_pincode)
        if delivery_pincode:
            delivery_pincode = delivery_pincode.split(',')
            query &= Q(shipping_address__postcode__in=delivery_pincode)
        if customer:
            customer = customer.split(',')
            query &= Q(customer__name__in=customer)
        if return_order:
            query &= Q(return_order=True)

        return query

    def __get_paginated_data(self, queryset, page, num):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else objects.previous_page_number()
        )
        next = 0 if not objects.has_next() else objects.next_page_number()

        # pages_count = paginator.num_pages
        data = {
            "count": count,
            "previous": previous,
            "next": next,
            "objects": objects.object_list,
            "last_loaded_on": timezone.now().isoformat()
        }

        return data

    def __get_queryset(self):
        query = self.__get_query()
        query_params = self.request.query_params
        city = query_params.get('city', None)
        is_filter_applied = query_params.get('is_filter_applied', False)

        order_qs = Order.objects.filter(query).order_by('-updated_time')
        order_qs = apply_contract_permission_filter(self.request.user, order_qs)
        order_qs = order_qs.select_related(
            'customer', 'pickup_hub',
            'hub', 'city', 'driver', 'customer_contract'
        )
        cities = list(order_qs.values_list('city__name', flat=True))
        cities = list(set(cities))

        if order_qs.filter(city__name=city).count() > 0:
            order_qs = order_qs.filter(city__name=city)
        else:
            if not is_filter_applied:
                city = cities[0]
                order_qs = order_qs.filter(city__name=city)
            else:
                order_qs = Order.objects.none()

        page_to_be_displayed = (
            query_params.get("next")
            if query_params.get("next", "") and int(query_params["next"]) > 0
            else PAGE_TO_BE_DISPLAYED
        )
        number_of_entry = NO_OF_SHIPMENT_ORDERS_IN_PAGE
        pagination_data = self.__get_paginated_data(
            order_qs, page_to_be_displayed, number_of_entry
        )
        orders = pagination_data.pop('objects')

        return {'orders': orders, 'city': city, 'pagination_data': pagination_data}

    def get(self, request):
        data = self.__get_queryset()
        orders_list = []
        order_qs = data.pop('orders', None)
        for order in order_qs.iterator():
            orders_list.append(
                {
                    'id': order.id,
                    'number': order.number,
                    'status': order.status,
                    'reference_number': order.reference_number,
                    'customer_reference_number': order.customer_reference_number,
                    'city': order.city.name,
                    'pickup_hub': order.pickup_hub.name if order.pickup_hub else None,
                    'delivery_hub': order.hub.name if order.hub else None,
                    'customer': order.customer.name if order.customer else None,
                    'date_placed': order.date_placed.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime('%d-%b-%Y %H:%M'),
                    'updated_time': order.updated_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime('%d-%b-%Y %H:%M')
                }
            )
        if not orders_list:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('No orders found'))

        data['orders'] = orders_list
        return Response(status=status.HTTP_200_OK, data=data)


class ShipmentOrderCreateTrip(views.APIView):
    """
        Assign orders to a driver and create trip
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request):
        data = request.data
        order_ids = data.get('order_ids', None)
        driver_id = data.get('driver_id', None)

        if not driver_id:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Please select a driver'))

        orders = Order.objects.filter(order_type=settings.SHIPMENT_ORDER_TYPE, id__in=order_ids)

        trips = Trip.objects.filter(
            driver_id=driver_id,
            status__in=[
                StatusPipeline.TRIP_NEW,
                StatusPipeline.TRIP_IN_PROGRESS,
                StatusPipeline.DRIVER_ACCEPTED,
                StatusPipeline.TRIP_ALL_STOPS_DONE
            ]
        )

        if trips.exists():
            create_trip = False
            for trip in trips.iterator():
                if trip.status == StatusPipeline.TRIP_NEW and \
                        trip.planned_start_time >= timezone.now() + timedelta(hours=6):
                    create_trip = True
                else:
                    create_trip = False
                    break
            if not create_trip:
                message = 'Selected driver already has an ongoing trip: %s' % ', '.join(
                    [i.trip_number for i in trips])
                return Response(status=status.HTTP_400_BAD_REQUEST, data=message)
        driver = Driver.objects.only('id', 'driver_vehicle').get(pk=driver_id)
        success, message = assign_driver(orders, driver, request)
        if not success:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        return Response(status=status.HTTP_200_OK, data=message)


class ShipmentOrderAssignDriver(views.APIView):
    """
        Assigns packages to an ongoing trip
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def post(self, request):
        data = request.data
        driver_id = data.get('driver_id', None)
        order_ids = data.get('order_ids', None)

        orders = Order.objects.filter(order_type=settings.SHIPMENT_ORDER_TYPE, id__in=order_ids)
        driver = Driver.objects.filter(pk=driver_id).first()

        trips = Trip.objects.filter(
            driver=driver, status__in=[
                StatusPipeline.TRIP_NEW,
                StatusPipeline.TRIP_IN_PROGRESS,
                StatusPipeline.DRIVER_ACCEPTED,
                StatusPipeline.TRIP_ALL_STOPS_DONE],
            order__isnull=True
        )

        if not trips:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('No existing trip for driver %s' % driver))

        if trips.count() > 1:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('More than one shipment trip exists for driver %s' % driver))

        success, message = assign_driver(orders, driver, request, trips[0])
        if not success:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        return Response(status=status.HTTP_200_OK, data=message)
