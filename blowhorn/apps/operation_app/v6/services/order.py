import logging

from config.settings import status_pipelines as StatusPipeline

from blowhorn.apps.driver_app.v3.services.order.scan_orders import ScanOrderMixin
from blowhorn.driver.tasks import restore_driver_app

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def assign_driver(orders, driver, request, trip=None):
    """
        Assigns orders to a driver and creates trip
    """

    proceed = False
    records_with_end_statuses = []
    records_with_ongoing_trip = []

    order_numbers = []
    wms_orders = None
    if not isinstance(orders, list):
        wms_orders = orders.filter(customer_contract__is_wms_contract=True).values_list('number', flat=True)

    if wms_orders and wms_orders.exists():
        return False, "Use WMS app to assign WMS orders: %s" % list(wms_orders)

    for order in orders.iterator():
        order_numbers.append(order.number)
        if order.status in StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES:
            proceed = False
            records_with_end_statuses.append(order.number)

        elif order.status in [StatusPipeline.OUT_FOR_DELIVERY, StatusPipeline.DRIVER_ASSIGNED,
                              StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                              StatusPipeline.ORDER_MOVING_TO_HUB, StatusPipeline.ORDER_MOVING_TO_HUB_RTO,
                              StatusPipeline.ORDER_OUT_FOR_RTO] or \
            (order.status == StatusPipeline.ORDER_NEW and
             order.driver_id is not None):

            dict_ = {}
            stop = order.stops.exclude(trip__status__in=StatusPipeline.TRIP_END_STATUSES).first()

            if stop:
                dict_['Order'] = order.number
                dict_['Trip'] = stop.trip.trip_number
                proceed = False
                records_with_ongoing_trip.append(dict_)

    if not proceed:
        if records_with_ongoing_trip:
            return False, 'Orders fulfilled by other ongoing trip: %s' % records_with_ongoing_trip
        if records_with_end_statuses:
            return False, 'Orders are in end statuses,' \
                          ' Cannot create Trip: %s' % records_with_end_statuses

    try:
        trip = ScanOrderMixin(
            context={'request': request}, trip_id=trip.id if trip else None,
            hub_associate=request.user
        ).create_trip_stops_for_shipment_orders(
            driver=driver,
            order_numbers=order_numbers,
        )
    except Exception as e:
        message = str(e.args[0])
        logger.info('Driver assigning failed: %s' % str(e))
        logger.error('Driver Assign Failed')
        return False, message

    if trip:
        restore_driver_app(driver.id)
        return True, 'Orders assigned to trip %s' % trip

    return False, 'Order Assignment failed'
