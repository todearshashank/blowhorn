import json
from django.utils import timezone
from datetime import timedelta
from django.db.models import Q
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from blowhorn.apps.operation_app.v6.constants import PAGE_TO_BE_DISPLAYED
from blowhorn.users.constants import NOTIFICATION_GROUP_ORDER, NUMBER_OF_NOTIFICATIONS_IN_PAGE, \
    NOTIFICATION_STATUS_ACTIVE, FYI
from blowhorn.apps.operation_app.v5.services.order import OrderDetails
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.users.submodels.notification import AppNotification


class AppNotificationDetails(object):

    def __init__(self, **kwargs):
        """
        :param kwargs:
        """
        self.user = kwargs.get('user')
        self.response_data = dict(filterList=[])

    def __get_pagination_data(self, queryset, page, num):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count

        previous = (
            None if not objects.has_previous() else objects.previous_page_number()
        )
        next = 0 if not objects.has_next() else objects.next_page_number()

        # pages_count = paginator.num_pages
        data = {
            "count": count,
            "previous": previous,
            "next": next,
            "objects": objects.object_list,
        }

        return data

    def get_user_notification_data(self, request):

        """
        :param request:
        :return:
        """
        user = request.user
        page_to_be_displayed = (
            request.GET.get("next")
            if request.GET.get("next", "") and int(request.GET["next"]) > 0
            else PAGE_TO_BE_DISPLAYED
        )

        number_of_entry = NUMBER_OF_NOTIFICATIONS_IN_PAGE
        last_week = timezone.now() - timedelta(days=7)

        params = Q(status=NOTIFICATION_STATUS_ACTIVE) & (
            Q(action_owners=user) | Q(broadcast_list=user)) & Q(created_date__gte=last_week)

        message = request.GET.get('message')

        if message:
            params &= Q(message__icontains=message)
        qs_result = AppNotification.objects.filter(params).order_by('-created_date')

        pagination_data = self.__get_pagination_data(
            qs_result,
            page_to_be_displayed,
            number_of_entry
        )

        paginated_notifications = pagination_data.pop('objects')
        self.response_data['pagination_data'] = pagination_data

        self.response_data['notifications'] = []

        for notification in paginated_notifications:
            data = self.get_notification_details(notification, user)
            self.response_data['notifications'].append(data)

        return self.response_data

    def get_notification_details(self, notification, user):

        category = NotificationMixin().get_notification_category(notification, user)
        extra_context = json.loads(notification.extra_context)

        if category == FYI:
            # Remove redirect url for users for whom FYI
            extra_context.pop('redirect_url', None)

        data = dict(
            id=notification.id,
            message=notification.message,
            category=category,
            notification_group=notification.notification_group,
            created_date=notification.created_date.isoformat(),
            extra_context=extra_context
        )

        return data
