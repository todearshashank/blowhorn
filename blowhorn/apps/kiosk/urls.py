from django.conf.urls import url
from . import views

urlpatterns = [
            # List all rate packages (vehicle pricing).
            url(r'^profile/$', views.ProfileInformation.as_view(), name='profile'),
            url(r'^order/$', views.CreateOrder.as_view(), name='order'),


        ]
