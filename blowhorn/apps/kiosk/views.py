import logging
from datetime import datetime
from django.conf import settings
from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from rest_framework import status, generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import exceptions

from blowhorn.address.models import City
from blowhorn.common import sms_templates
from blowhorn.common.helper import CommonHelper
from blowhorn.contract.constants import CONTRACT_TYPE_KIOSK, CONTRACT_STATUS_ACTIVE
from blowhorn.contract.models import Contract, KioskLabourConfiguration
from blowhorn.contract.serializers import SellRateSerializer
from blowhorn.customer.models import Customer
from blowhorn.driver.payment_helpers.contract_helper import ContractHelper
from blowhorn.order.const import PAYMENT_STATUS_PAID
from blowhorn.order.serializers.OrderSerializer import OrderSerializer
from blowhorn.order.tasks import send_kiosk_terms_of_service, send_mail
from blowhorn.users.models import User
from blowhorn.vehicle.models import VehicleClass
from blowhorn.utils.functions import get_tracking_url

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CreateOrder(generics.CreateAPIView):

    def get_or_create_customer(self, phone_number, email, name):
        user, customer = None, None
        try:
            user = User.objects.get(email=email)
            customer = Customer.objects.get(user__email=email)
        except:
            with transaction.atomic():
                if not user:
                    user = User.objects.create_user(
                        email=email,
                        phone_number=phone_number,
                        name=name,
                        password='1234'
                    )
                if not customer:
                    customer = Customer.objects.create(user=user, name=name)
        return customer, user

    def __send_payment_link(self, order):
        template_dict = sms_templates.template_booking_completion
        message = template_dict['text'] % \
                  (order.number, order.total_payable,
                   get_tracking_url(self.request, order.number))
        order.user.send_sms(message, template_dict)
        send_mail.apply_async((order.id,), )

    def __calculate_order_cost(self, order):
        now = datetime.now().date()
        order.calculate_and_update_cost(None, planned_start_time=now)
        order.save()
        # self.__send_payment_link(order)
        return True

    def post(self, request):
        data = request.data
        customer_name = data.get('customer_name') or None
        customer_number = '+91' + data.get('customer_number')
        pre_paid = data.get('pre_paid')
        city_id = data.get('city_id', None)
        contract_id = data.get('contract_id')
        fixed_price = data.get('cost', 0) or 0
        floor_number = data.get('floor_number', 0) or 0
        number_of_items = data.get('number_of_items', 1) or 1
        labour_id = data.get('labour_id')
        pickup = data.get('pickup_address')
        dropoff = data.get('shipping_address')
        estimated_distance = data.get('estimated_distance', 0)
        data['is_exclude_vehicle_class'] = True
        email = data.get('customer_email', '')
        if not email:
            email = customer_number + '@' + settings.DEFAULT_DOMAINS.get(
                'customer')

        customer, user = self.get_or_create_customer(customer_number, email,
                                                     customer_name)
        if fixed_price and not contract_id:
            contract = Contract.objects.filter(
                city_id=city_id,
                contract_type=CONTRACT_TYPE_KIOSK,
                status=CONTRACT_STATUS_ACTIVE
            ).first()
            if contract is not None:
                contract_id = contract.pk
            else:
                response = {
                    'message': _('No active kiosk contract available for '
                                 'selected city')
                }
                return Response(status=400, data=response)

        data['customer'] = customer
        data['user'] = user
        data['city'] = City.objects.filter(id=city_id).first()
        data['customer_contract'] = contract_id
        data['fixed_price'] = fixed_price
        data['floor_number'] = floor_number
        data['number_of_items'] = number_of_items
        data['estimated_distance_km'] = estimated_distance

        if pre_paid:
            data['payment_status'] = PAYMENT_STATUS_PAID
        else:
            data['cash_on_delivery'] = fixed_price
        data['order_type'] = settings.SHIPMENT_ORDER_TYPE

        if not customer_name or not customer_number:
            response = {
                'message': 'Cannot create order without customer name and '
                           'number'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        context = {'request': request}
        pickup_geopoint = CommonHelper.get_geopoint_from_latlong(
            pickup.get('geopoint').get('latitude'),
            pickup.get('geopoint').get('longitude'))

        dropoff_geopoint = CommonHelper.get_geopoint_from_latlong(
            dropoff.get('geopoint').get('latitude'),
            pickup.get('geopoint').get('longitude'))

        data['pickup_address'].update({'geopoint': pickup_geopoint})
        data['shipping_address'].update({'geopoint': dropoff_geopoint})
        data['vehicle_class_preference'] = VehicleClass.objects.first()

        if labour_id:
            data['labour_info'] = {
                'no_of_labours': 1,
                'labour_cost': data.get('labour_pay')
            }

        order_serializer = OrderSerializer(context=context, data=data)
        try:
            order = order_serializer.save()
            if order:
                self.__calculate_order_cost(order)

        except exceptions.ParseError:
            logger.warning('Kiosk Order Creation Failure, ParseError %s' % be)
            error = be.get_full_details()
            extracted_message = str(error.get('message', {}).get('message'))
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_(extracted_message)
            )
        except BaseException as be:
            logger.warning('Kiosk Order Creation Failure: %s' % be)
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_('Failed to create order: %s' % be)
            )

        response = {
            'order_number': order.number
        }
        status_code = 200
        send_kiosk_terms_of_service.apply_async((order.id,), )
        return Response(response, status_code)


class ProfileInformation(generics.RetrieveUpdateAPIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        city_id = request.GET.get('city_id') or None
        date = datetime.today().date()
        if city_id:
            cities = City.objects.filter(id=city_id)
        else:
            cities = City.objects.all()

        response = []
        for city in cities:
            contracts = Contract.objects.filter(
                city=city, contract_type=CONTRACT_TYPE_KIOSK)

            for contract in contracts:
                contract_helper = ContractHelper(contract)
                sell_rate = contract_helper.get_sell_rate(date)
                sell_rate_data = SellRateSerializer(instance=sell_rate).data
                labours = list(KioskLabourConfiguration.objects.filter(
                    contract=contract).values())
                base_pay = sell_rate.base_pay if sell_rate.base_pay else 0
                sellvasslab_set = sell_rate_data.get('sellvasslab_set', [])
                response.append({
                    'contract': {
                        'id': contract.id,
                        'name': contract.name,
                        'base_pay': base_pay,
                        'vas_slabs': sellvasslab_set
                    },
                    'labours': labours
                })
        status_code = 200
        return Response(response, status_code)
