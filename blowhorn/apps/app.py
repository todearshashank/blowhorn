# from django.apps import AppConfig
# from django.conf.urls import url
# from blowhorn.apps.driver_app.app import application as driver_app
# from blowhorn.apps.customer_app.app import application as customer_app
# from blowhorn.apps.operation_app.app import application as operation_app
# from blowhorn.apps.kiosk.app import application as kiosk
# from blowhorn.apps.integrations.app import application as integration_app
#
#
# class BlowhornApplication(AppConfig):
#     def __init__(self):
#         app_name = 'blowhorn.address'
#         module_name = 'address'
#         self.app_name = 'blowhorn.address'
#         self.module_name = 'address'
#
#     label = 'apps'
#     verbose_name = 'App'
#
#     namespace = 'apps'
#
#     def get_urls(self):
#         urlpatterns = [
#             url(r'^customers/', customer_app.urls),
#             url(r'^drivers/', driver_app.urls),
#             url(r'^operations/', operation_app.urls),
#             url(r'^kiosk/', kiosk.urls),
#             url(r'^integrations/', integration_app.urls),
#         ]
#         return self.post_process_urls(urlpatterns)
