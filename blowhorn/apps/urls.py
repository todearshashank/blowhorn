from django.conf.urls import url, include


urlpatterns = [
            url(r'^customers/',  include('blowhorn.apps.customer_app.urls')),
            url(r'^drivers/', include('blowhorn.apps.driver_app.urls')),
            url(r'^operations/', include('blowhorn.apps.operation_app.urls')),
            url(r'^kiosk/', include('blowhorn.apps.kiosk.urls')),
            url(r'^integrations/', include('blowhorn.apps.integrations.urls')),
        ]
