from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline

# Country = get_model('address', 'Country')
Driver = get_model('driver', 'Driver')


VEHICLE_EXIST = 'Vehicle with corresponding registration number already exists'
PHONE_NUMBER_EXIST = "Phone Number already exists"


def get_detailed_data(params):
    """
        Fragmentize the requested parameters into various dictionary of parameters
        needed for creating the records of a specific relation
    """
    vehicle_parameters = params['vehicles'][0] if params.get('vehicles', []) else {}

    driver_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'can_drive_and_deliver': params.get('can_drive_and_deliver', False),
        'can_source_additional_labour': params.get('can_source_additional_labour', False),
        'driving_license_number': params.get('driving_license_number', params.get('mobile', '')),
        'work_status': params.get('work_status', ''),
        'working_preferences': params.get('working_preferences', []),
        'own_vehicle': params.get('driver_is_owner', False),
        'operating_city': params.get('operating_city', ''),
        'pan': params.get('pan_number', '')
    }

    vehicle_data = {
        'vehicle_model': vehicle_parameters.get('vehicle_model', ''),
        'vehicle_class': vehicle_parameters.get('vehicleClass', ''),
        'body_type': vehicle_parameters.get('body_type_name', ''),
        'registration_certificate_number': params.get('driver_vehicle', ''),
        'model_year': vehicle_parameters.get('vehicle_model_year', '')
    }

    user_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'password': params.get('password', None),
        'email': params.get('email', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'user_id': None,
        'is_active': True if params.get('status') == StatusPipeline.ACTIVE else False
    }

    vendor_data = {
        'phone_number': params.get('owner_contact_no', ''),
        'name': params.get('owner_name', ''),
        'pan': params.get('owner_pan_number', '')
    }

    detailed_data = {
        'driver_data': driver_data,
        'vehicle_data': vehicle_data,
        'user_data': user_data,
        'vendor_data': vendor_data
    }

    return detailed_data


def get_user_for_driver(user_data):
    """
        Creating/Updating the user details for the driver
    """
    return Driver.objects.upsert_user(
        email=user_data.get('email', None),
        name=user_data.get('name', ''),
        phone_number=user_data.get('phone', None),
        password=user_data.get('password', None),
        user_id=user_data.get('user_id', None),
        is_active=True if user_data['status'] == StatusPipeline.ACTIVE else False
    )
