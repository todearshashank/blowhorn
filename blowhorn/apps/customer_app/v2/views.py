# Create your views here.
import re
import traceback
import json
import logging
import operator
import phonenumbers
import requests
from datetime import datetime
from functools import reduce
from django.conf import settings
from django.contrib.auth import logout
from django.core.paginator import Paginator
from django.db import transaction
from django.db.models import Q, Prefetch, Case, When, Value, IntegerField
from django.utils import timezone
from django.urls import reverse

from rest_framework import status, generics
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import permission_classes
from fcm_django.models import FCMDevice
from dateutil.parser import parse

from blowhorn.address.mixins import AddressMixin
from blowhorn.address.models import UserAddress, City, State
from blowhorn.address.utils import within_city_working_hours, \
    get_city_from_latlong, get_city_from_geopoint, get_city
from blowhorn.address.helpers.location_helper import LocationHelper
from blowhorn.common.helper import CommonHelper
from blowhorn.common.notification import Notification
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.common import sms_templates
from blowhorn.common.communication import Communication
from blowhorn.customer.permissions import IsCustomer
from blowhorn.customer.serializers import CustomerProfileSerializer, \
    CustomerFeedbackSerializer, CustomerShippingAddressSerializer, \
    WaypointSerializer, CustomerTaxInformationSerializer
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.views import CustomerProfile, OrderCreate
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, \
    CustomerTaxInformation, CUSTOMER_CATEGORY_NON_ENTERPRISE
from blowhorn.customer.utils import get_vehicle_class_availability
from blowhorn.customer.constants import ALLOWED_TIME_FORMATS, \
    PICKUP_DATETIME_FORMAT
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME, \
    GTA
from blowhorn.contract.utils import ContractUtils
from blowhorn.contract.models import Contract
from blowhorn.order.models import OrderConstants, ItemCategory
from blowhorn.order.utils import OrderUtil
from blowhorn.order.serializers.OrderSerializer import OrderSerializer
from blowhorn.order.tasks import send_mail, push_customer_feedback_to_slack, initiate_paytm_refund
from blowhorn.order.const import PAYMENT_STATUS_UNPAID, PAYMENT_MODE_PAYTM, PAYMENT_STATUS_PAID, \
                                    ORDER_TYPE_NOW
from blowhorn.driver.serializers import AvailableDriverSerializer
from blowhorn.utils.functions import ist_to_utc, get_base_url, utc_to_ist
from blowhorn.apps.customer_app.v1.social_profile import FacebookProfile, \
    GoogleProfile
from blowhorn.vehicle.models import VehicleClass
from blowhorn.users.serializers import UserAddressSerializer
from config.settings import status_pipelines as StatusPipeline
from blowhorn.apps.customer_app.v1.helper import upsert_driver_rating
from blowhorn.apps.customer_app.v1.serializers.driver import \
    DriverRatingAggregateSerializer
from blowhorn.apps.driver_app.v1.serializers.driver import DriverLoginSerializer
from blowhorn.vehicle.models import CityVehicleClassAlias
from blowhorn.apps.customer_app.common import Slot
from blowhorn.utils.datetime_function import DateTime
from blowhorn.coupon.mixins import CouponMixin
from blowhorn.driver.mixins import DriverMixin
from blowhorn.oscar.core.loading import  get_model


User = get_model('users', 'User')
Customer = get_model('customer', 'Customer')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
OrderBatch = get_model('order', 'OrderBatch')
WayPoint = get_model('order', 'WayPoint')
Stop = get_model('trip', 'Stop')
Hub = get_model('address', 'Hub')
DriverActivity = get_model('driver', 'DriverActivity')
Driver = get_model('driver', 'Driver')
DriverRatingAggregate = get_model('driver', 'DriverRatingAggregate')
DriverRatingDetails = get_model('driver', 'DriverRatingDetails')
# Country = get_model('address', 'Country')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CustomerProfileInformation(generics.RetrieveUpdateAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = CustomerProfileSerializer

    def _get_latlng_list(self, coordinates):
        latlon_list = []
        for latlong_arr in coordinates:
            for latlong in latlong_arr:
                latlon_list.append({
                    "latitude": latlong[1],
                    "longitude": latlong[0],
                })
        return latlon_list

    def _get_coverage_area(self):
        coverage = []
        contracts = Contract.objects.get_distinct_cities_from_active_c2c_sme_contracts()
        for contract in contracts:
            city = contract.city
            pickup_coordinates = city.coverage_pickup.coords
            dropoff_coordinates = city.coverage_dropoff.coords
            coverage.append({
                "city": city.name,
                "pickup": self._get_latlng_list(pickup_coordinates),
                "dropoff": self._get_latlng_list(dropoff_coordinates),
            })

        return coverage

    def _get_order_without_rating(self, customer):
        order = Order.objects.filter(
            customer=customer,
            status=StatusPipeline.ORDER_DELIVERED,
            order_type=settings.C2C_ORDER_TYPE
        ).select_related('vehicle_class_preference').last()
        if not order:
            return {}
        if DriverRatingDetails.objects.filter(order=order).exists():
            return {}

        driver = order.driver
        vehicle_class = order.vehicle_class_preference
        vehicle_class_name = vehicle_class.commercial_classification
        vehicle_class_alias = CityVehicleClassAlias.objects.filter(
            city_id=order.city_id, vehicle_class=vehicle_class).first()
        if vehicle_class_alias:
            vehicle_class_name = vehicle_class_alias.alias_name

        return {
            'driver': DriverLoginSerializer(driver, fields=["id", "name",
                                                            "photo"]).data,
            'vehicle': {
                'rc_number': driver.driver_vehicle if driver else '',
                'alias_name': vehicle_class_name,
                'image_url': vehicle_class.image_selected.url,
                'app_icon': vehicle_class.app_icon
            },
            'order': {
                'number': order.number,
                'pickup_address_area': order.pickup_address.line3,
                'dropoff_address_area': order.shipping_address.line3,
                'stops_count': order.waypoint_set.all().count() - 1,
                'id': order.id,
                'completed_at': utc_to_ist(order.updated_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
            }
        }

    def _get_ratingwise_driver_remarks(self, order_type=settings.C2C_ORDER_TYPE):
        return DriverRatingAggregateSerializer(
            DriverRatingAggregate.objects.filter(order_type=order_type), many=True).data

    def get_profile_response(self, user, latitude, longitude, is_firebase_user_login):

        saved_locations = AddressMixin().get_saved_user_addresses(user, new_app=True)
        recent_locations = AddressMixin().get_recent_user_addresses(user, new_app=True)
        geopoint = CommonHelper.get_geopoint_from_latlong(
            latitude, longitude)
        customer = self.get_customer(user)

        trip = Trip.objects.filter(
            order__customer=customer, order__order_type=settings.C2C_ORDER_TYPE,
            status__in=[StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_ALL_STOPS_DONE]).order_by('-planned_start_time')
        trip = trip.select_related('order').first()

        city = None
        if geopoint:
            city = get_city_from_geopoint(geopoint, field='coverage_pickup')
        if not city:
            city = City.objects.first()
        customer_cancellation_reasons = []
        for key in OrderUtil().fetch_order_cancellation_reasons():
            _reason_dict = {
                'key': key,
                'value': key
            }
            customer_cancellation_reasons.append(_reason_dict)
        cityno = str(city.contact)
        gstin = CustomerTaxInformation.objects.filter(customer=customer).values_list('gstin', flat=True).first()

        profile = {
            "name": user.name,
            "email": user.email,
            "mobile": str(user.phone_number),
            "is_mobile_verified": user.is_mobile_verified,
            "is_email_verified": user.is_email_verified,
            "item_category": list(
                ItemCategory.objects.filter(is_active=True).values('category', 'id').order_by(
                    'category')),
            "saved_locations": saved_locations,
            "recent_locations": recent_locations,
            "time_slot_details": Slot().get_time_slots_for_city(city),
            "coverage": self._get_coverage_area(),
            'call_support': cityno,
            'call_support_promotion': cityno,
            'share_sub_text': settings.C2C_CONSTANTS['APP_SHARE_SUB_TEXT'],
            'share_body_text': settings.C2C_CONSTANTS['APP_SHARE_BODY_TEXT'],
            'customer_cancellation_reasons': customer_cancellation_reasons,
            "inprogress_order": str(trip.order.number) if trip else '',
            "is_business_user": True if customer and customer.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL else False,
            "legal_name": customer.legalname if customer and customer.legalname else '',
            "gstin": gstin if gstin else '',
            "driver_remarks_info": self._get_ratingwise_driver_remarks(),
            "payment_details": customer.get_payment_details(send_slack_notification=True)
        }

        order_without_rating = self._get_order_without_rating(customer)
        if order_without_rating:
            profile['order_without_rating'] = order_without_rating

        if is_firebase_user_login == 'false':
            firebase_auth_token = CommonHelper.generate_firebase_token(
                settings.FIREBASE_KEYS['customer'])
            profile.update({"firebase_auth_token": firebase_auth_token})

        return profile

    def get(self, request):
        """
        Customer Profile api
        :returns response for app
        """
        data = request.GET.dict()
        latitude = data.get('latitude')
        longitude = data.get('longitude')
        is_firebase_user_login = data.get("is_firebase_user_login")
        profile = self.get_profile_response(request.user, latitude, longitude, is_firebase_user_login)

        response = {
            'status': True,
            'message': profile
        }

        return Response(status=status.HTTP_200_OK, data=response, content_type='text/html; charset=utf-8')


class RatePackages(APIView, CustomerMixin):
    """
    Description     : API to get vehicle sell rate packages.
    Request type    : GET
    Params          : (lat, lng) or city
    Response        : Available Vehicle packages for given city / location.
    """

    permission_classes = (AllowAny, )

    def _get_city(self):
        params = self.request.GET
        city = params.get('city', None)
        if city:
            city = City.objects.get(id=city)
        else:
            latitude, longitude = params.get('latitude', None), params.get(
                'longitude', None)
            if latitude and longitude:
                city = get_city_from_latlong(latitude, longitude,
                                             field='coverage_pickup')
                # Passing Bangalore as default city if lat-lng didn't match
                if not city:
                    return City.objects.first()

        # Defaulting to all cities if no city was matched.
        if not city:
            return list(City.objects.all())
        return city

    def _get_contract_types(self, customer=None):
        # if customer and customer.is_SME():
        return [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]
        # else:
        #     return [CONTRACT_TYPE_C2C]

    def get(self, request):
        customer = self.get_customer(request.user)
        city = self._get_city()
        cities = city if isinstance(city, list) else [city]
        contract_types = self._get_contract_types(customer)
        response = {'data': '', 'errors': ''}
        if city and contract_types:
            response['data'] = ContractUtils.get_sell_packages(
                cities=cities, contract_types=contract_types, customer=customer)
            status_code = status.HTTP_200_OK
        else:
            response['errors'] = 'Insufficient arguments.'
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(response, status_code)


class BookingSlots(generics.ListAPIView):

    def get(self, request):
        data = request.GET.dict()
        latitude = data.get('latitude')
        longitude = data.get('longitude')
        city = get_city_from_latlong(latitude, longitude,
                                     field='coverage_pickup')

        if not city:
            city = City.objects.first()

        slots = Slot().get_time_slots_for_city(city)
        return Response(status=status.HTTP_200_OK, data=slots)


class UnPaidOrder(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsCustomer,)

    def get(self, request):
        customer = self.get_customer(request.user)
        response_data = ''
        status_code = status.HTTP_200_OK
        try:
            response_data = customer.get_unpaid_orders()
        except:
            response_data = 'Something went wrong'
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(status=status_code, data=response_data)


class CreateOrder(generics.CreateAPIView, CustomerMixin):
    permission_classes = (IsCustomer,)

    serializer_class = OrderSerializer

    def create(self, request):
        context = {'request': request}
        data = list(request.data.dict())

        try:
            data = json.loads(data[0])
        except ValueError as ve:
            print('Value Error: %s' % ve)
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Data is tampered or invalid.')

        user = request.user
        data['customer'] = self.get_customer(user)

        item_category = data.get('item_category')
        request.POST._mutable = True

        data_parsed = self._parse_params(data)
        if not data_parsed:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_message)

        address_pickup = CustomerShippingAddressSerializer(
            data=data_parsed.get('pickup_address', {}))
        address_drop_off = CustomerShippingAddressSerializer(
            data=data_parsed.get('shipping_address', {}))
        data['shipping_address'] = data_parsed.get('shipping_address', None)
        way_point_serializer = WaypointSerializer(
            data=data_parsed.get('waypoints_address', []), many=True)

        if address_pickup.is_valid(raise_exception=True) and address_drop_off.is_valid(raise_exception=True) and \
                way_point_serializer.is_valid(raise_exception=True):
            order_serializer = OrderSerializer(
                context=context, data=data_parsed)
            order = order_serializer.save()

            for item in item_category:
                order.item_category.add(item)
            order.save()
            way_point_serializer.save(order=order)
            OrderCreate().post_save(order)

        background_color = '#13A893'
        dict_response = {
            "status": True,
            "message": {
                'booking_id': order.number,
                'friendly_id': order.number,
                'tracking_url': 'http://%s/track/%s' % (settings.HOST, order.number),
                'booking_type': 'advanced',
                'failure_title': 'Thank You!',
                'failure_message': 'Sorry! Unable to take your booking',
                'success_title': 'Congratulations!',
                'success_message': 'We have received your booking and will contact you shortly',
                'background_color': background_color,
                'buttons': {
                    'done': True,
                    'call_support': False,
                }
            }
        }

        return Response(dict_response, status=status.HTTP_201_CREATED)

    def _parse_params(self, order_data):
        self.error_message = ''
        pickup = order_data.get('pickup_address')
        droppoff = order_data.get('dropoff_address')
        timeframe = order_data.get('timeframe', None)
        payment_mode = order_data.get('selectedPaymentMode', None)
        customer = order_data.get('customer')
        shipping_address = self.clean_shipping_address(droppoff)
        if not timeframe:
            self.error_message = 'Something went wrong!'
            return None

        now_or_later = timeframe.get('now_or_later', None)
        later_value = timeframe.get('later_value', '')
        pickup_datetime = self._clean_pickup_time(now_or_later, later_value)

        city = get_city(
            order_type='',
            geopoint=shipping_address.get('geopoint'),
            postcode=shipping_address.get('postcode')
        )
        order_data['city'] = city

        credit_amount_eligibility, pending_amount = customer.get_credit_amount_eligibility()
        credit_period_eligibility = customer.get_credit_days_eligibility()

        is_customer_eligible = credit_amount_eligibility and credit_period_eligibility
        if not is_customer_eligible:
            self.error_message = 'Please complete payment for previous trips to proceed with booking.'
            from blowhorn.customer.tasks import send_credit_limit_slack_notification
            if credit_amount_eligibility:
                pending_amount = 0

            send_credit_limit_slack_notification.delay(customer.id, pending_amount,
                                                       credit_period_eligibility)
            return None

        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=pickup_datetime,
            lat=pickup['geopoint'].get('latitude', 0),
            lon=pickup['geopoint'].get('longitude', 0),
            city=city
        )
        if not is_valid_booking_time:
            self.error_message = 'Service is unavailable at this time. Please schedule for later.'
            return None

        vehicle_class_preference = VehicleClass.objects.filter(
            contract__id=order_data.get('contract_id')).first()

        return {
            'pickup_address': self.clean_shipping_address(pickup),
            'shipping_address': shipping_address,
            # 'items': order_data.get('items', None),
            'coupon_code': order_data.get('coupon_code', None),
            'customer_contract': order_data.get('contract_id', None),
            'vehicle_class_preference': vehicle_class_preference,
            'pickup_datetime': pickup_datetime,
            'device_info': order_data.get('device_type', 'iOS'),
            'status': StatusPipeline.ORDER_NEW,
            'waypoints': self._clean_waypoints(order_data.get('waypoints', [])),
            'order_type': settings.C2C_ORDER_TYPE,
            'labour_info': self._clean_labour(order_data.get('labour_option', '')),
            'customer': order_data.get('customer', None),
            'device_type': order_data.get('device_type', ''),
            'booking_type': now_or_later,
            'auto_debit_paytm': order_data.get('isAuto_debit_paytm', False),
            'estimated_cost': order_data.get('estimatePriceLow', 0.0),
            'estimated_distance_km': order_data.get('distanceInKM', 0.0),
            'payment_mode': payment_mode,
            'estimated_time_in_mins': order_data.get('timeInMins', 0.0),
            'estimated_cost_lower': order_data.get('estimatePriceLow', 0.0),
            'estimated_cost_upper': order_data.get('estimatePriceHigh', 0.0),
            'eta_in_mins': order_data.get('etaValue', ''),
            'is_pod_required': order_data.get('is_pod_required', False)
        }

    def parse_phone_number(self, phone_number):
        if phone_number and not phone_number.startswith(
            settings.ACTIVE_COUNTRY_CODE):
            return settings.ACTIVE_COUNTRY_CODE + phone_number
            # return phonenumbers.parse(
            #         phone_number,
            #         settings.DEFAULT_COUNTRY.get('country_code')
            #     )
        return None

    # Clean methods
    def clean_shipping_address(self, address):

        # old app response commented
        # phone_no = address['address'].get('contact', {}).get('mobile', None)
        # contact_name = address['address'].get('contact', {}).get('name', '')
        # # if not phone_no:
        # phone_no = address['address'].get('contact', {}).get('phone_number', '')

        contact_name = address.get('contact', {}).get('name', '')
        phone_no = address.get('contact', {}).get('phone_number', '')

        parsed_number = self.parse_phone_number(phone_no)

        return {
            "title": "",
            "first_name": contact_name,
            "last_name": "",
            "line1": address['address'].get('line1', ''),
            "line2": '',
            "line3": address['address'].get('line3', ''),
            "line4": address['address'].get('line4', ''),
            "postcode": address['address'].get('postcode', None),
            "country": address['address'].get('country', settings.COUNTRY_CODE_A2),
            "phone_number": parsed_number if parsed_number else None,
            "geopoint": CommonHelper.get_geopoint_from_latlong(
                address['geopoint'].get('latitude', 0),
                address['geopoint'].get('longitude', 0)
            )
        }

    def _clean_pickup_time(self, now_or_later, later_value=None):
        if now_or_later == ORDER_TYPE_NOW:
            pickup_time = timezone.now()
        else:
            m = re.match(r'(\d{2} \S{3} \d{4}) (\d:\d{2})', later_value)
            pickup_time = '%s 0%s' % (
                m.group(1), m.group(2)) if m else later_value
            for f in ALLOWED_TIME_FORMATS:
                logging.info('Matching time "%s" with format "%s"' %
                             (later_value, f))
                try:
                    pickup_time = ist_to_utc(
                        datetime.strptime(later_value, f))
                    break
                except:
                    pickup_time = parse(later_value)

        return pickup_time

    def _clean_waypoints(self, waypoints):
        waypoints_cleaned = []
        for i, waypoint in enumerate(waypoints):
            contact = waypoint.get('contact')
            address = waypoint.get('address')
            geopoint = waypoint.get('geopoint')

            phone_no = contact.get('mobile', None) if contact else None
            if not phone_no:
                phone_no = contact.get('phone_number', '') if contact else ''

            waypoints_cleaned.append({
                "title": "",
                "first_name": contact.get('name', '') if contact else '',
                "last_name": "",
                "line1": address.get('line1', '') if address else '',
                "line2": '',
                "line3": address.get('line3', '') if address else '',
                "line4": address.get('line4', '') if address else '',
                "postcode": address.get('postal_code', None) if address else '',
                "phone_number": phone_no,
                "geopoint": CommonHelper.get_geopoint_from_latlong(geopoint.get('latitude', 0),
                                                                   geopoint.get('longitude', 0)),
                'sequence_id': i + 1
            })

        return waypoints_cleaned

    def _clean_labour(self, labour):
        count = 0
        cost = 0
        if isinstance(labour, str):
            if labour in ['light', 'heavy']:
                labour_cost = OrderConstants().get('LABOUR_COST', 400)
                LABOUR_COST_MAPPING = {
                    'light': labour_cost,
                    'medium': labour_cost,
                    'heavy': labour_cost,
                }
                count = 1
                cost = LABOUR_COST_MAPPING.get(labour, 400)

        elif isinstance(labour, dict):
            cost = labour.get('cost_per_labour', 0)
            count = labour.get('num_of_labours', 0)

        return {
            'no_of_labours': count,
            'labour_cost': cost
        }


class AvailableVehicles(APIView):
    """
    Description     : API to get vehicles which are available to serve
    Request type    : GET
    Params          : (lat, lng)
    Response        : Available Vehicles in a specified radius (for now 100 km)
    """

    permission_classes = (AllowAny, )

    def _get_location(self):
        params = self.request.GET
        latitude, longitude = params.get('latitude', None), params.get('longitude', None)
        if latitude and longitude:
            return LocationHelper.get_geopoint_from_latlong(latitude, longitude)

        return False

    def _get_available_vehicles(self, location):
        # driver_available = DriverActivity.objects.filter(
        #     driver__status=StatusPipeline.ACTIVE,
        #     driver__vehicles__isnull=False,
        #     event=DRIVER_ACTIVITY_IDLE, geopoint__distance_lte=(
        #         location, AVAILABLE_DRIVER_SEARCH_RADIUS_IN_METERS
        #     )
        # ).select_related('driver').prefetch_related('driver__vehicles')

        city = None
        if location:
            city = get_city_from_geopoint(location, field='coverage_pickup')
        if not city:
            city = City.objects.first()
        driver_available = DriverMixin().get_nearest_available_driver(location, city_id=city.id,
                                                                      vehicle_class_preference=VehicleClass.objects.all())

        driver_available = driver_available.select_related('driver').prefetch_related('driver__vehicles')

        available_vehicle = []
        if driver_available:
            driver_available_serializer = AvailableDriverSerializer(
                driver_available, many=True)
            available_vehicle = get_vehicle_class_availability(
                driver_available_serializer.data)

        return available_vehicle

    def get(self, request):
        response = {'data': '', 'errors': ''}
        location = self._get_location()
        if not location:
            response['errors'] = 'Location geopoint missing'
            status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)

        city = get_city_from_geopoint(location, field='coverage_pickup')
        if not city:
            response['errors'] = 'Area not covered'
            status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)

        available_vehicle = self._get_available_vehicles(location)

        if available_vehicle:
            response['data'] = available_vehicle
            status_code = status.HTTP_200_OK
        else:
            status_code = status.HTTP_400_BAD_REQUEST
        return Response(response, status_code)






@permission_classes([])
class CustomerLogin(generics.CreateAPIView, CustomerMixin):
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)
    """
    Customer / User Authentication to be done here and the response with the
    user token to be provided.

    """

    def post(self, request):
        data = request.POST.dict()
        email = data.get("email").strip()
        password = data.get("password").strip()
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        # app sends False as string so setting default as string
        is_firebase_user_login = data.get("is_firebase_user_login")

        login_response = User.objects.do_login(
            request=request, email=email, password=password)

        if login_response.status_code == status.HTTP_200_OK:
            user = login_response.data.get('user')
            token = login_response.data.get('token')
            response = {
                'status': True,
                'message': {
                    'name': user.name,
                    'email': user.email,
                    'mAuthToken': token,
                },
                "profile": CustomerProfileInformation().get_profile_response(
                    user, latitude, longitude, is_firebase_user_login)
            }

            if not self.get_customer(user):
                print('Users are not allowed to login register as customer')
                return Response(status=status.HTTP_401_UNAUTHORIZED, data='Users are not allowed to login')
            return Response(status=status.HTTP_200_OK, data=response, content_type='text/html; charset=utf-8')

        response = {
            'text': login_response.data,
        }
        return Response(status=login_response.status_code, data=response, content_type='text/html; charset=utf-8')


@permission_classes([])
class SocialLogin(generics.CreateAPIView):

    def set_response(self, request, user_info, latitude, longitude,
                     is_firebase_user_login, mobile=False):
        user = None
        if not user_info:
            response_status = False
            message = {
                'why': 'account_unavailable'
            }
        else:
            email = user_info.get('email')
            name = user_info.get('name')
            if not (email and name):
                response_status = False
                message = {
                    'why': 'account_unavailable',
                    'name': name,
                    'email': email,
                }
            else:
                user = User._default_manager.filter(email=email.lower()).first()
                if not user:
                    extra_fields = {
                        'is_email_verified': True
                    }
                    customer_response = Customer.objects.create_customer(
                        email=email,
                        name=name,
                        is_active=True,
                        **extra_fields
                    )

                    if customer_response.status_code != status.HTTP_200_OK:
                        return Response(status=customer_response.status_code,
                                        data=customer_response.data,
                                        content_type='text/html; charset=utf-8')

                    user = customer_response.data.get('user')

                login_response = User.objects.do_login(
                    request=request, user=user)

                if login_response.status_code != status.HTTP_200_OK:
                    response_status = False
                    message = login_response.data
                else:
                    response_status = True
                    message = {
                        'is_mobile_verified': user.is_mobile_verified,
                        'is_email_verified': user.is_email_verified,
                        'name': name,
                        'email': email,
                    }
                    if mobile:
                        # payload = jwt_payload_handler(user)
                        # mAuthToken = jwt_encode_handler(payload)
                        mAuthToken = login_response.data.get('token')
                        message['mAuthToken'] = mAuthToken

        response_dict = {
            'status': response_status,
            'message': message,
        }
        if user:
            profile = CustomerProfileInformation().get_profile_response(
                    user, latitude, longitude, is_firebase_user_login)
            response_dict.update({'profile': profile})

        return Response(status=status.HTTP_200_OK, data=response_dict,
                        content_type='text/html; charset=utf-8')


@permission_classes([])
class FacebookLogin(generics.CreateAPIView):
    queryset = User.objects.all()

    def post(self, request):
        data = request.data
        accessToken = data.get("access_token")
        isMobile = request.META.get('HTTP_MOBILE')
        mobile_app = True if isMobile else False
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        is_firebase_user_login = data.get('is_firebase_user_login')

        sp = FacebookProfile()
        user_info = sp.get(token=accessToken)
        return SocialLogin().set_response(request, user_info, latitude,
                                          longitude, is_firebase_user_login,
                                          mobile_app)

    def get_serializer_class(self):
        pass


@permission_classes([])
class GoogleLogin(generics.CreateAPIView):
    queryset = User.objects.all()

    def post(self, request):
        data = request.data
        accessToken = data.get("access_token")
        isMobile = request.META.get('HTTP_MOBILE')
        mobile_app = True if isMobile else False
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        is_firebase_user_login = data.get('is_firebase_user_login')

        sp = GoogleProfile()
        user_info = sp.get(token=accessToken)
        return SocialLogin().set_response(request, user_info, latitude,
                                          longitude, is_firebase_user_login,
                                          mobile_app)

    def get_serializer_class(self):
        pass


class CustomerMobileVerify(generics.CreateAPIView):

    def post(self, request):
        """
            Used by customer app to verify the OTP sent to it
            API Call: POST /mobileverify
        """
        data = request.data.dict()
        user_otp = data.get("otp")
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        is_firebase_user_login = data.get("is_firebase_user_login")
        user = request.user

        otp, created = User.objects.retrieve_otp(user)
        if str(user_otp) != otp:
            return Response(status=status.HTTP_402_PAYMENT_REQUIRED, data='OTP Mismatch')

        user.is_mobile_verified = True
        user.is_active = True
        user.save()
        response = CustomerProfileInformation().get_profile_response(
            user, latitude, longitude, is_firebase_user_login)
        return Response(status=status.HTTP_200_OK, data=response)


@permission_classes([])
class SaveUserLocation(generics.CreateAPIView, generics.UpdateAPIView):
    serializer_class = UserAddressSerializer

    def get(self, request):
        saved_locations = AddressMixin().get_saved_user_addresses(request.user, True)
        return Response(status=status.HTTP_200_OK, data=saved_locations)

    def _get_address(self, query_params):
        params = reduce(operator.and_, query_params) if query_params else None
        print("para         ", params)
        try:
            return UserAddress.objects.get(params)
        except UserAddress.DoesNotExist:
            return False

    def validate_data(self, data, request):
        # api for new customer app removed variable mapping

        parsed_data = {}
        country = Country.objects.filter(iso_3166_1_a2=settings.COUNTRY_CODE_A2).get()

        geopoint = CommonHelper.get_geopoint_from_latlong(data.get('latitude'), data.get('longitude'))

        if data.get('contact_mobile') and not data.get(
            'contact_mobile').startswith(settings.ACTIVE_COUNTRY_CODE):
            phone_number = settings.ACTIVE_COUNTRY_CODE + data.get('contact_mobile')
        else:
            phone_number = data.get('contact_mobile', '')

        parsed_data['identifier'] = data.get('name', '')
        parsed_data['country'] = country
        parsed_data['geopoint'] = geopoint
        parsed_data['user'] = request.user
        parsed_data['phone_number'] = phone_number
        parsed_data['first_name'] = data.get('contact_name', '')
        parsed_data['line1'] = data.get('line1')
        parsed_data['line3'] = data.get('line3')
        parsed_data['line4'] = data.get('line4')

        return parsed_data

    def post(self, request):
        request.POST._mutable = True
        data = request.data.dict()

        user = request.user
        identifier = data.get('name', '')

        if not identifier:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Please, Provide a name for address.')

        # Check for the existing address of user
        identifier = identifier.strip()
        context = {
            'user': user,
            'request': data
        }

        _data = self.validate_data(data, request)

        ua = UserAddress(**_data)
        hash = ua.generate_hash()

        user_address = self._get_address([Q(user=user), Q(identifier=identifier), Q(hash=hash)])
        if user_address:
            # Update address only if address is changed
            if user_address.hash != hash:
                serializer = self.serializer_class(user_address, data=data, context=context)
            else:
                message = 'Nothing to update'
                logger.info(message)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        try:
            user_address, created = UserAddress.objects.get_or_create(**_data)
            # UserAddress.objects.filter(user=user, identifier=identifier).update(**_data)
        except:
            pass

        saved_locations = AddressMixin().get_saved_user_addresses(user, True)
        logger.info('User address successfully saved: %s' % user)

        return Response(status=status.HTTP_200_OK, data=saved_locations)


class CustomerMyTrips(generics.RetrieveAPIView, CustomerMixin):

    def get(self, request, when=None):
        """
        returns the current, past and upcoming order based on request
        """
        data = request.GET.dict()
        cursor = data.get('cursor') if data.get('cursor') else 1

        user = User.objects.get(email=request.user)
        customer = self.get_customer(user)

        query_params = Q(order_type=settings.C2C_ORDER_TYPE, customer=customer)

        if when == StatusPipeline.APP_ORDER_STATUS_PAST:
            query = Q(status=StatusPipeline.ORDER_DELIVERED)
        if when == StatusPipeline.APP_ORDER_STATUS_NEW:
            ordering = 'pickup_datetime'
            query = Q(status=StatusPipeline.ORDER_NEW) | Q(status=StatusPipeline.DRIVER_ACCEPTED,
                                                           trip__status=StatusPipeline.TRIP_NEW)
        elif when == StatusPipeline.APP_ORDER_STATUS_CURRENT:
            ordering = '-pickup_datetime'
            query = ~Q(status__in=[StatusPipeline.ORDER_DELIVERED,
                                   StatusPipeline.ORDER_NEW,
                                   StatusPipeline.ORDER_CANCELLED]
                       )
            query = query & ~Q(status=StatusPipeline.DRIVER_ACCEPTED, trip__status=StatusPipeline.TRIP_NEW)
        query_params = query_params & query if query else query_params

        order_qs = Order.objects.filter(query_params)

        if when == StatusPipeline.APP_ORDER_STATUS_PAST:
            order_qs = order_qs.annotate(
                payment_status_int=Case(
                    When(payment_status=PAYMENT_STATUS_UNPAID, then=Value('0')),
                    default=Value('1'),
                    output_field=IntegerField()
                )
            ).order_by('payment_status_int', '-pickup_datetime')
        else:
            order_qs = order_qs.order_by(ordering)

        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        order = order_qs
        paginator = Paginator(order, 10)

        if int(cursor) <= paginator.num_pages:

            order_page = paginator.page(cursor)

            # @todo remove app parameter and use same dates and time format
            # in both app and website
            results_json = OrderSerializer().get_orders(
                request=request,
                results=order_page.object_list,
                app=True
            )

            current = results_json['current']
            past = results_json['past']
            upcoming = results_json['upcoming']
        else:
            current = []
            past = []
            upcoming = []

        cursor = int(cursor) + 1
        dict_ = {
            'current': current,
            'past': past,
            'upcoming': upcoming,
            'more': '',
            'cursor': str(cursor),
        }

        return Response(status=status.HTTP_200_OK, data=dict_, content_type='text/html; charset=utf-8')


class UpdateMobile(generics.CreateAPIView):

    def post(self, request):
        data = request.data.dict()
        mobile = data.get('mobile')
        debug = True if data.get('debug') == 'true' else False
        device_type = data.get('device_type', '')

        if not mobile:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid Mobile Number')

        user = request.user
        # Update the number only when the old number is not the same as the new
        # number
        phone_number = phonenumbers.parse(
            mobile, settings.COUNTRY_CODE_A2)

        if user.phone_number != phone_number:
            user.phone_number = phone_number
            user.save()

        otp, created = User.objects.retrieve_otp(user)
        if device_type == settings.USER_CLIENT_TYPE_ANDROID:
            template_dict = sms_templates.android_template_otp
        else:
            template_dict = sms_templates.template_otp

        message = template_dict['text'] % otp
        sms_to = phonenumbers.format_number(phone_number,
                                            phonenumbers.PhoneNumberFormat.E164)
        Communication.send_sms(sms_to=sms_to, message=message, priority='high',
                                template_dict=template_dict)
        return Response(status=status.HTTP_200_OK)


@permission_classes([])
class CustomerSignup(generics.CreateAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def post(self, request):
        data = request.data.dict()
        email = data.get('email', None)
        password = data.get('password', None)
        is_business_user = data.get('is_business_user', None)

        if is_business_user == 'true':
            is_business_user = True
        else:
            is_business_user = False

        # is_business_user = False
        legal_name = data.get('legal_name', None)
        name = data.get('name', None)
        gstin = data.get('gstin', None)

        response = {
            'status': False,
            'message': {
                'text': '',
            }
        }

        if not email or not password:
            response['message'] = 'email and password required'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        email = email.lower()
        if not re.compile(settings.EMAIL_REGEX).match(email):
            response['message'] = 'Please, Provide a valid email'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        if User.objects.filter(email=email).exists():
            response['message'] = 'User with this email already exists.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        if is_business_user and Customer.objects.filter(legalname=legal_name).exists():
            response['message'] = 'User with legalname already exists.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        tax_info = CustomerTaxInformation.objects.filter(gstin=gstin).first()
        if tax_info:
            response['message'] = 'Customer with GSTIN exist'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        with transaction.atomic():
            customer_response = Customer.objects.create_customer(
                email=email.lower(),
                password=password,
                name=data.get('name'),
                is_active=True
            )

            if customer_response.status_code != status.HTTP_200_OK:
                return Response(status=customer_response.status_code,
                                data=customer_response.data,
                                content_type='text/html; charset=utf-8')

            user = customer_response.data.get('user')
            customer = self.get_customer(user)
            if is_business_user and gstin and \
                    CustomerProfile().has_tax_info_edit_permission(user, customer):
                success, message = CustomerProfile()._update_tax_information(customer,
                                                                             name, is_business_user,
                                                                             legal_name, gstin, None)

                if not success:
                    _out = {
                        'status': False,
                        'message': message
                    }
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=_out)
            login_response = User.objects.do_login(request=request, user=user)

            if login_response.status_code != status.HTTP_200_OK:
                return Response(status=login_response.status_code,
                                data=login_response.data,
                                content_type='text/html; charset=utf-8')

            token = login_response.data.get('token')
        response['status'] = True
        response['message'] = {
            'name': user.name,
            'email': user.email,
            'mAuthToken': token,
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


class CustomerFCM(generics.CreateAPIView, CustomerMixin):

    def post(self, request):
        user = request.user
        customer = self.get_customer(user)
        if not customer:
            logger.info('Customer not found for user %s' % user)
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='You are not a customer')

        data = request.data.dict()
        # device_type = 'android'
        registration_id = data.get('registration-id')
        device_type = data.get('device_type', 'android')

        user_device = FCMDevice.objects.filter(user=user, active=True)
        if user_device.exists():
            if user_device.count() == 1:
                Notification.update_fcm_key_for_user(user_device.first(),
                                                     registration_id, '',
                                                     device_type)
        else:
            Notification.add_fcm_key_for_user(request.user, registration_id,
                                              '', device_type.lower())

        response = {
            'status': True,
            'message': 'Registration id successfully saved'
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


class CustomerFeedback(generics.CreateAPIView, CustomerMixin):

    def _set_field_details(self, title, val, short='false', link=None):
        return {
            'title': title,
            'value': '%s' % val if not link else '<%s|%s>' % (link, val),
            'short': short
        }

    def _get_slack_message(self, request, customer, user, feedback_text):
        author_name = '%s | %s' % (customer.name, user.phone_number)
        base_url = get_base_url(request)
        title_link = ''.join([base_url, reverse('admin:customer_customer_change', args=[customer.id])])

        title = '%s' % user.email
        recent_order = Order.objects.get_recent_booking_of_customer(customer)
        fields = []
        if recent_order:
            order_number = recent_order.get('number', '')
            order_url = ''.join([base_url, reverse('admin:order_bookingorder_changelist'),
                                '?q=', order_number])
            fields.extend([
                self._set_field_details('Recent Order', order_number, short='true', link=order_url),
                self._set_field_details('City', recent_order.get('city__name', ''), short='true')
            ])
        fields.append(self._set_field_details('Message', feedback_text))

        slack_message = {
            "attachments": [{
                "author_name": author_name,
                "title": title,
                "title_link": title_link,
                "thumb_url": settings.THUMBNAIL_LINK,
                "color": "#52c0d2",
                "fields": fields
            }]
        }
        return slack_message

    def post(self, request):
        data = request.data.dict()
        feedback_text = data.pop('feedback', '')
        user = request.user

        customer = self.get_customer(user)
        if not customer:
            logger.info('Feedback API: Customer not exists for %s' % user)
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        feedback_data = {
            'feedback': feedback_text,
            'customer': customer.id,
            'extra_details': json.dumps(data)
        }

        serializers = CustomerFeedbackSerializer(data=feedback_data)
        if serializers.is_valid():
            serializers.save()
            success_msg = True
            message = 'Success'
            slack_message = self._get_slack_message(request, customer, user,
                                                    feedback_text)
            push_customer_feedback_to_slack.apply_async((slack_message,),)
            response_dict = {
                'status': success_msg,
                'message': message
            }
            return Response(status=status.HTTP_200_OK, data=response_dict,
                            content_type='text/html; charset=utf-8')
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        content_type='text/html; charset=utf-8')


@permission_classes([])
class PasswordReset(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        """ Used by the users to reset the password
            API Call: POST /passwordforget
        """
        status_message = ''
        message = ''
        data = request.data.dict()
        user_email = data.get('email')
        if user_email:
            try:
                user = User.objects.get(email=user_email)
            except User.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

            url_protocol = 'https'  # request.META.get('wsgi.url_scheme', 'http')
            url = ''.join([url_protocol, '://',
                           request.META.get('HTTP_HOST', 'blowhorn.com'),
                           '/accounts/password/reset/'])

            client = requests.session()
            client.get(url)
            if 'csrftoken' in client.cookies:
                csrftoken = client.cookies['csrftoken']
                data = dict(email=user_email,
                            csrfmiddlewaretoken=csrftoken)
                client.post(url, data=data, headers=dict(Referer=url))
                status_message = True
                message = 'SUCCESS'

        else:
            status_message = False
            message = 'User e-mail cannot be blank'

        response_dict = {
            'status': status_message,
            'message': message,
        }
        return Response(status=status.HTTP_200_OK, data=response_dict,
                        content_type='text/html; charset=utf-8')


class CustomerLogout(generics.CreateAPIView):

    def post(self, request):
        data = request.data.dict()
        # Remove the FCM Key on logout for the driver device
        Notification.remove_fcm_key_for_user(
            user=request.user, registration_id=data.get('fcm_id'))
        logout(request)
        return Response(status=status.HTTP_200_OK)


class CustomerBookingStatus(generics.RetrieveAPIView):

    def post(self, request):
        data = request.data.dict()

        order = Order.objects.get(number=data.get('order_number'))

        if order and order.status==StatusPipeline.ORDER_NEW and \
            'cancel_booking' == data.get('booking_action', ''):
            reason = data.get('cancellation_reason')
            other_reason = data.get('other_cancellation_reason')

            remarks = reason + '  ' + other_reason + 'App'

            Order.objects.filter(id=order.id).update(status=StatusPipeline.ORDER_CANCELLED,
                                                    reason_for_cancellation=reason,
                                                    other_cancellation_reason=other_reason)

            event = Event.objects.create(
                status=StatusPipeline.ORDER_CANCELLED,
                order=order,
                remarks=remarks,
                created_by = request.user
            )

            if order.payment_mode == PAYMENT_MODE_PAYTM and \
                order.payment_status == PAYMENT_STATUS_PAID and order.amount_paid_online > 0 \
                and not order.is_refunded:
                initiate_paytm_refund.apply_async(
                    (order.id,), )
            # # @todo save is not saving user so saving it again
            # event.created_by = request.user
            # event.save()

            message = 'Booking Cancelled'
            response_dict = {
                'status': 200,
                'data': {},
                'message': message
            }
            return Response(status=status.HTTP_200_OK, data=response_dict,
                            content_type="application/json")

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Cannot cancel the order.Please contact Support',
                        content_type="application/json")


class LiveTracking(generics.CreateAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request, order_number):
        """
        api for live tracking
        """
        data = request.GET.dict()
        is_firebase_user_login = data.get("is_firebase_user_login")

        order_qs = Order.objects.filter(number=order_number,
                                        order_type=settings.C2C_ORDER_TYPE)
        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        # @todo remove app parameter and use same dates and time format
        # in both app and website
        o_json = OrderUtil().get_booking_details_for_customer(
            request=request,
            order=order_qs[0],
            route_info=True,
            app=True
        )

        if o_json and is_firebase_user_login == 'false':
            firebase_auth_token = CommonHelper.generate_firebase_token(
                settings.FIREBASE_KEYS['customer'])
            o_json.update({"firebase_auth_token": firebase_auth_token})

        return Response(status=status.HTTP_200_OK, data=o_json,
                        content_type="application/json")


class SendInvoice(generics.RetrieveAPIView):

    def post(self, request, order_number):
        order = Order.objects.get(number=order_number)
        send_mail.apply_async((order.id,), )
        return Response(status=status.HTTP_200_OK,
                        data="Invoice sent successfully")


class QuickBook(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        data = json.loads(request.body)
        dropoff_address = data.get('dropoff_address')
        pickup_address = data.get('pickup_address')
        context = {'request': request}

        data['pickup_address'] = CreateOrder().clean_shipping_address(pickup_address)
        data['shipping_address'] = CreateOrder().clean_shipping_address(dropoff_address)

        if not dropoff_address or \
                not pickup_address or not data.get('vehicle_class'):
            response = {
                'status': False,
                'message': 'Unable to place order'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        geopoint = data['shipping_address'].get('geopoint')

        city = get_city_from_geopoint(geopoint)
        now = utc_to_ist(timezone.now())
        # Check if outside of working hours.
        if not within_city_working_hours(city, now.time()):
            response = {
                'status': False,
                'message': 'No Vehicles available now'
            }
            return Response(status=status.HTTP_404_NOT_FOUND, data=response)

        address_pickup = CustomerShippingAddressSerializer(
            data=data.get('pickup_address', {}))
        address_drop_off = CustomerShippingAddressSerializer(
            data=data.get('shipping_address', {}))
        data['shipping_address'] = data.get('shipping_address', None)
        way_point_serializer = WaypointSerializer(
            data=data.get('waypoints_address', []), many=True)

        if address_pickup.is_valid(raise_exception=True) and address_drop_off.is_valid(raise_exception=True) and \
            way_point_serializer.is_valid(raise_exception=True):
            order_serializer = OrderSerializer(
                context=context, data=data)
            order = order_serializer.save()

            way_point_serializer.save(order=order)
            OrderCreate().post_save(order)

        background_color = '#13A893'
        dict_response = {
            "status": True,
            "message": {
                'booking_id': order.number,
                'friendly_id': order.number,
                'tracking_url': 'http://%s/track/%s' % (settings.HOST, order.number),
                'booking_type': 'advanced',
                'failure_title': 'Thank You!',
                'failure_message': 'Sorry! Unable to take your booking',
                'success_title': 'Congratulations!',
                'success_message': 'We have received your booking and will contact you shortly',
                'background_color': background_color,
                'buttons': {
                    'done': True,
                    'call_support': False,
                }
            }
        }

        return Response(dict_response, status=status.HTTP_201_CREATED)


class UpdateProfile(generics.CreateAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        customer = self.get_customer(user)
        data = request.POST.dict()

        gstin_ = data.get('gstin', None)
        name = data.get('name', None)
        legal_name = data.get('legal_name', None)
        response = {}

        if gstin_ and Customer.objects.filter(legalname=legal_name).exclude(id=customer.id).exists():
            response['message'] = 'User with legalname already exists.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')
        if gstin_:
            state = State.objects.get(gst_code=gstin_[0:2])
            tax_info = CustomerTaxInformation.objects.filter(gstin=gstin_).exclude(customer=customer)
            if tax_info.exists():
                response['message'] ='Customer with GSTIN exist'
                return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

            data = {
                'customer_id': customer.pk,
                'gstin': gstin_,
                'state': state
            }
            try:
                tx_info = CustomerTaxInformation.objects.get(
                    state=state, customer=customer)
                tax_serializer = CustomerTaxInformationSerializer(
                    tx_info, data=data)
            except:
                tax_serializer = CustomerTaxInformationSerializer(data=data)

            if tax_serializer.is_valid():
                try:
                    tax_serializer.save()
                except BaseException as be:
                    logger.warning('App - Error saving GSTIN: %s' % be)
                    response['message'] ='Failed to save GSTIN information'
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=response)

        if name:
            customer.name = name
            user.name = name
            user.save()

        if legal_name:
            customer.legalname = legal_name
            customer.customer_category = CUSTOMER_CATEGORY_NON_ENTERPRISE
            customer.service_tax_category = GTA

        if name or legal_name:
            customer.save()

        return Response(status=status.HTTP_200_OK)


class DriverRating(generics.CreateAPIView):
    """
    Save Driver Rating given by customer.

    request data: order_id, rating, remarks(list of ids), customer_remark
    response: success-> 200 with success message
              failure-> 500 if internal error with error message
                        400 if wrong request data sent with proper error message
    """

    permission_classes = (AllowAny, )

    def post(self, request):
        """Upsert driver rating and add driver rating details."""
        error_message = ''
        response_data = 'Driver rating saved successfully!'
        status_code = status.HTTP_200_OK

        data = request.POST.dict()
        if not data:
            data = request.data
        order_id = data.get('order_id')
        query = Q(pk=order_id)
        order = Order.objects.prefetch_queryset(
            query=query, select_related=['driver']
        ).only('pk', 'driver').first()
        if not order:
            status_code = status.HTTP_400_BAD_REQUEST
            error_message = 'Invalid Order!'
        rating = data.get('rating')
        remarks = data.get('remarks')
        if isinstance(remarks, str):
            remarks = json.loads(remarks)
        customer_remark = data.get('customer_remark', '')

        user = request.user
        if not user.is_authenticated:
            user = order.customer.user

        if not error_message:
            driver = order.driver
            rating_details_data = {
                'order': order,
                'rating': rating,
                'driver': driver,
                'customer_remark': customer_remark,
                'user': user
            }
            try:
                upsert_driver_rating(driver=driver, rating=rating)
                rating_details_list = []
                for remark_id in remarks:
                    rating_details_data['remark_id'] = remark_id
                    rating_details_list.append(DriverRatingDetails(**rating_details_data))
                DriverRatingDetails.objects.bulk_create(rating_details_list)
            except:
                logger.error(traceback.format_exc())
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                error_message = 'Something went wrong!'

        return Response(status=status_code, data=error_message or response_data)


class getEstimatedFare(APIView):
    permission_classes = (AllowAny,)

    def get_labour(self, data):
        labour_cost = 0
        labour_option = data.get('labour_option', '')
        if labour_option:
            return labour_option.get('cost_per_labour') * labour_option.get('num_of_labours')
        return labour_cost

    def post(self, request):

        data = list(request.data.dict())
        try:
             data = json.loads(data[0])
        except ValueError as ve:
             print('Value Error: %s' % ve)
             return Response(status=status.HTTP_400_BAD_REQUEST,
                             data='Data is tampered or invalid.')
        #data = request.data

        user = request.user
        order = Order()
        order.customer = CreateOrder().get_customer(user)
        contract_id = data.get('contract_id')

        item_category = data.get('item_category')
        self.item_category = ItemCategory.objects.filter(id__in=item_category)
        contract = Contract.objects.filter(id=contract_id)
        contract = contract.select_related('city').first()
        order.customer_contract = contract
        order.estimated_distance_km = data.get('distanceInKM', 0.0)
        order.estimated_time_in_mins = data.get('timeInMins', 0.0)

        timeframe = data.get('timeframe', None)
        now_or_later = timeframe.get('now_or_later', None)
        later_value = timeframe.get('later_value', '')
        order.pickup_datetime = CreateOrder()._clean_pickup_time(now_or_later, later_value)
        order.city = order.customer_contract.city
        coupon_code = data.get('coupon_code', '')
        coupon = None
        if coupon_code:
            coupon = CouponMixin().get_coupon_from_code(coupon_code)
        order.coupon = coupon
        order.is_pod_required = data.get('is_pod_required', False)

        order.calculate_and_update_cost()
        total_payable = order.total_payable
        labour_charge = self.get_labour(data)
        total_payable += labour_charge

        response = {
            'total_payable': total_payable,
            'discount': order.discount or 0,
            'before_discount': total_payable + (order.discount or 0)
        }

        return Response(status=200, data=response)

    def get_queryset(self):
        pass


class RescheduleBookingView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication, )

    def post(self, request):
        data = request.data
        if not isinstance(data, dict):
            data = data.dict()

        order_number = data.get('order_number', None)
        reschedule_details = data.get('reschedule_details')
        if isinstance(reschedule_details, str):
            reschedule_details = json.loads(reschedule_details)
        timeframe = reschedule_details.get('now_or_later')
        selected_datetime = reschedule_details.get('later_value')

        if timeframe and timeframe != ORDER_TYPE_NOW and not selected_datetime:
            return Response(status=400, data='Please select pickup datetime for later booking')

        order_qs = Order.objects.filter(number=order_number).select_related('customer')
        if not order_qs.exists():
            return Response(status=400, data='Order not found')

        order = order_qs.first()
        error = None
        if order.driver_id:
            error = 'Can\'t cancel the order. Driver is already assigned.'

        elif order.status in [StatusPipeline.ORDER_CANCELLED]:
            error = 'Can\'t reschedule cancelled order'

        if error:
            return Response(status=400, data=error)

        pickup_datetime = CreateOrder()._clean_pickup_time(
            timeframe, later_value=selected_datetime)
        customer = order.customer
        pickup_geopoint = order.pickup_address.geopoint
        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=pickup_datetime,
            lat=pickup_geopoint.y,
            lon=pickup_geopoint.x
        )
        if not is_valid_booking_time:
            message = 'Service is unavailable at this time. '\
                           'Please schedule for later.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        try:
            with transaction.atomic():
                # Converting to LOCAL timezone to store it as remarks
                old_pickup_datetime = DateTime().get_locale_datetime(
                    order.pickup_datetime)
                order_qs.update(pickup_datetime=pickup_datetime)
                remarks = '%s (%s -> %s)' % (
                    StatusPipeline.RESCHEDULE_DELIVERY,
                    old_pickup_datetime.strftime(PICKUP_DATETIME_FORMAT),
                    selected_datetime
                )
                event = Event.objects.create(
                    status=order.status,
                    order=order,
                    remarks=remarks
                )
                event.created_by = order.user
                event.save()
                OrderCreate().post_save(order)
                return Response(
                    status=200,
                    data='Rescheduled successfully'
                )
        except:
            return Response(status=400, data='Failed to reschedule')
