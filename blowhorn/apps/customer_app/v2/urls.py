from django.conf.urls import url
from . import views
from blowhorn.coupon.views import Coupon

urlpatterns = [
            # List all rate packages (vehicle pricing).
            url(r'^rate-packages/$', views.RatePackages.as_view(),
                name='v2-rate-packages-list'),
            url(r'^available-vehicles/$', views.AvailableVehicles.as_view(),
                name='v2-available-vehicles-list'),

            # List/ Create Order
            url(r'^createorder/$', views.CreateOrder.as_view(),
                name='v2-create-order'),
            url(r'^profile/$', views.CustomerProfileInformation.as_view(),
                name='v2-customer-profile'),
            url(r'^slots/$', views.BookingSlots.as_view(),
                name='v2-booking-slots'),
            url(r'^login/$', views.CustomerLogin.as_view(),
                name='v2-customer-login'),
            url(r'^googlelogin$', views.GoogleLogin.as_view(),
                name='v2-google-login'),
            url(r'^fblogin$', views.FacebookLogin.as_view(),
                name='v2-fb-login'),
            url(r'^mobileverify/$', views.CustomerMobileVerify.as_view(),
                name='v2-mobile-verify'),
            url(r'^saveuserlocation/$', views.SaveUserLocation.as_view(),
                name='v2-save-location'),
            url(r'^orders/unpaid$', views.UnPaidOrder.as_view(),
                name='v2-unpaid-orders'),
            url(r'^updatemobilenum/$', views.UpdateMobile.as_view(),
                name='v2-update-mobile'),
            url(r'^signup/$', views.CustomerSignup.as_view(),
                name='v2-customer-signup'),
            url(r'^fcmid/$', views.CustomerFCM.as_view(),
                name='v2-customer-updatefcmid'),
            url(r'^feedback/$', views.CustomerFeedback.as_view(),
                name='v2-customer-feedback'),
            url(r'^passwordreset/', views.PasswordReset.as_view(),
                name='v2-password-reset'),
            url(r'^logout/$', views.CustomerLogout.as_view(),
                name='v2-customer-logout'),
            url(r'^bookingstatus/$', views.CustomerBookingStatus.as_view(),
                name='v2-customer-bookingstatus'),
            url(r'^quickbook$', views.QuickBook.as_view(),
                name='v2-quickbook'),
            url(r'^updateprofile/', views.UpdateProfile.as_view(),
                name='v2-update-profile'),
            url(r'^orders/(\S+)$', views.CustomerMyTrips.as_view(),
                name='v2-my-trips'),
            url(r'^order/(\S+)$', views.LiveTracking.as_view(),
                name='v2-live-tracking'),

            url(r'invoice/(\S+)$', views.SendInvoice.as_view(),
                name='v2-send-invoice'),

            url(r'^coupon/', Coupon.as_view(), name='v2-coupon'),

            # driver rating
            url(r'^drivers/rating', views.DriverRating.as_view(),
                name='v2-rate-driver'),
            url(r'^getfare$', views.getEstimatedFare.as_view(),
                name='v2-rate-driver'),

            url(r'^reschedule/$', views.RescheduleBookingView.as_view(),
                name='v2-reschedule-booking'),
        ]
