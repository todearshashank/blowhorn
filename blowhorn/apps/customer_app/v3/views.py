import re
import json
import logging
import pytz
from datetime import datetime
from django.conf import settings
from django.utils import timezone
from django.db import transaction
from rest_framework import status, generics
from rest_framework.response import Response
from blowhorn.address.utils import get_city

from blowhorn.common.helper import CommonHelper
from blowhorn.customer.permissions import IsCustomer
from blowhorn.customer.serializers import CustomerShippingAddressSerializer, \
    WaypointSerializer
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.views import OrderCreate
from blowhorn.customer.constants import ALLOWED_TIME_FORMATS
from blowhorn.order.models import OrderConstants
from blowhorn.order.serializers.OrderSerializer import OrderSerializer
from blowhorn.utils.functions import ist_to_utc

from blowhorn.vehicle.models import VehicleClass
from config.settings import status_pipelines as StatusPipeline
from blowhorn.oscar.core.loading import get_model
from blowhorn.trip.serializers import TripSerializer

User = get_model('users', 'User')
Customer = get_model('customer', 'Customer')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
OrderBatch = get_model('order', 'OrderBatch')
WayPoint = get_model('order', 'WayPoint')
Stop = get_model('trip', 'Stop')
Hub = get_model('address', 'Hub')
DriverActivity = get_model('driver', 'DriverActivity')
Driver = get_model('driver', 'Driver')
DriverRatingAggregate = get_model('driver', 'DriverRatingAggregate')
DriverRatingDetails = get_model('driver', 'DriverRatingDetails')
# Country = get_model('address', 'Country')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CreateOrder(generics.CreateAPIView, CustomerMixin):
    permission_classes = (IsCustomer,)

    serializer_class = OrderSerializer

    def auto_debit_paytm(self, order):
        from blowhorn.payment.views import PayTmWithdrawal
        from django.http import HttpRequest

        withdraw_data = {
            'number': order.number,
            'amount': order.total_payable,
            'aut_debit': True
        }
        withdraw_request = HttpRequest()
        withdraw_request.method = 'POST'
        withdraw_request.data = json.dumps(withdraw_data)
        PayTmWithdrawal.as_view()(withdraw_request)

    def create(self, request):
        context = {'request': request}
        data = list(request.data.dict())
        #data = request.data

        try:
             data = json.loads(data[0])
        except ValueError as ve:
             print('Value Error: %s' % ve)
             return Response(status=status.HTTP_400_BAD_REQUEST,
                             data='Data is tampered or invalid.')

        user = request.user
        data['customer'] = self.get_customer(user)

        item_category = data.get('item_category')
        request.POST._mutable = True

        data_parsed = self._parse_params(data)
        if not data_parsed:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_message)

        address_pickup = CustomerShippingAddressSerializer(
            data=data_parsed.get('pickup_address', {}))
        address_drop_off = CustomerShippingAddressSerializer(
            data=data_parsed.get('shipping_address', {}))
        data['shipping_address'] = data_parsed.get('shipping_address', None)
        way_point_serializer = WaypointSerializer(
            data=data_parsed.get('waypoints_address', []), many=True)

        with transaction.atomic():
            # sid = transaction.savepoint()

            if address_pickup.is_valid(raise_exception=True) and address_drop_off.is_valid(raise_exception=True) and \
                    way_point_serializer.is_valid(raise_exception=True):
                order_serializer = OrderSerializer(
                    context=context, data=data_parsed)
                order = order_serializer.save()

                for item in item_category:
                    order.item_category.add(item)
                order.save()
                way_point_serializer.save(order=order)

                OrderCreate().post_save(order)

                background_color = '#13A893'
                dict_response = {
                    "status": True,
                    "message": {
                        'booking_id': order.number,
                        'friendly_id': order.number,
                        'tracking_url': 'http://%s/track/%s' % (settings.HOST, order.number),
                        'booking_type': 'advanced',
                        'failure_title': 'Thank You!',
                        'failure_message': 'Sorry! Unable to take your booking',
                        'success_title': 'Congratulations!',
                        'success_message': 'We have received your booking and will contact you shortly',
                        'background_color': background_color,
                        'buttons': {
                            'done': True,
                            'call_support': False,
                        }
                    }
                }

                return Response(dict_response, status=status.HTTP_201_CREATED)
                # else:
                #     transaction.savepoint_rollback(sid)
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Payment failed Try again')

    def _parse_params(self, order_data):
        self.error_message = ''
        pickup = order_data.get('pickup_address')
        droppoff = order_data.get('dropoff_address')
        timeframe = order_data.get('timeframe', None)
        payment_mode = order_data.get('selectedPaymentMode', None)
        customer = order_data.get('customer')
        shipping_address = self.clean_shipping_address(droppoff)
        if not timeframe:
            self.error_message = 'Something went wrong!'
            return None

        now_or_later = timeframe.get('now_or_later', None)
        later_value = timeframe.get('later_value', '')
        pickup_datetime = self._clean_pickup_time(now_or_later, later_value)

        city = get_city(
            order_type='',
            geopoint=shipping_address.get('geopoint'),
            postcode=shipping_address.get('postcode')
        )
        order_data['city'] = city

        credit_amount_eligibility, pending_amount = customer.get_credit_amount_eligibility()
        credit_period_eligibility = customer.get_credit_days_eligibility()

        is_customer_eligible = credit_amount_eligibility and credit_period_eligibility
        if not is_customer_eligible:
            self.error_message = 'Please complete payment for previous trips to proceed with booking.'
            from blowhorn.customer.tasks import send_credit_limit_slack_notification
            if credit_amount_eligibility:
                pending_amount = 0

            send_credit_limit_slack_notification.delay(customer.id, pending_amount,
                                                       credit_period_eligibility)
            return None

        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=pickup_datetime,
            lat=pickup['geopoint'].get('latitude', 0),
            lon=pickup['geopoint'].get('longitude', 0),
            city=city
        )
        if not is_valid_booking_time:
            self.error_message = 'Service is unavailable at this time. Please schedule for later.'
            return None

        vehicle_class_preference = VehicleClass.objects.filter(
            contract__id=order_data.get('contract_id')).first()

        return {
            'pickup_address': self.clean_shipping_address(pickup),
            'shipping_address': shipping_address,
            # 'items': order_data.get('items', None),
            'coupon_code': order_data.get('coupon_code', None),
            'customer_contract': order_data.get('contract_id', None),
            'vehicle_class_preference': vehicle_class_preference,
            'pickup_datetime': pickup_datetime,
            'device_info': order_data.get('device_type', 'iOS'),
            'status': StatusPipeline.ORDER_NEW,
            'waypoints': self._clean_waypoints(order_data.get('waypoints', [])),
            'order_type': settings.C2C_ORDER_TYPE,
            'labour_info': self._clean_labour(order_data.get('labour_option', '')),
            'customer': order_data.get('customer', None),
            'device_type': order_data.get('device_type', ''),
            'booking_type': now_or_later,
            'auto_debit_paytm': order_data.get('isAuto_debit_paytm', False),
            'estimated_cost': order_data.get('estimatePriceLow', 0.0),
            'estimated_distance_km': order_data.get('distanceInKM', 0.0),
            'payment_mode': payment_mode,
            'estimated_time_in_mins': order_data.get('timeInMins', 0.0),
            'estimated_cost_lower': order_data.get('estimatePriceLow', 0.0),
            'estimated_cost_upper': order_data.get('estimatePriceHigh', 0.0),
            'eta_in_mins': order_data.get('etaValue', ''),
            'is_pod_required': order_data.get('is_pod_required', False)
        }

    def parse_phone_number(self, phone_number):
        if phone_number and not phone_number.startswith(
            settings.ACTIVE_COUNTRY_CODE):
            return settings.ACTIVE_COUNTRY_CODE + phone_number
            # return phonenumbers.parse(
            #         phone_number,
            #         settings.DEFAULT_COUNTRY.get('country_code')
            #     )
        return None

    # Clean methods
    def clean_shipping_address(self, address):

        # old app response commented
        # phone_no = address['address'].get('contact', {}).get('mobile', None)
        # contact_name = address['address'].get('contact', {}).get('name', '')
        # # if not phone_no:
        # phone_no = address['address'].get('contact', {}).get('phone_number', '')

        contact_name = address.get('contact', {}).get('name', '')
        phone_no = address.get('contact', {}).get('phone_number', '')

        if not phone_no:
            phone_no = address.get('contact', {}).get('mobile', '')

        parsed_number = self.parse_phone_number(phone_no)

        return {
            "title": "",
            "first_name": contact_name,
            "last_name": "",
            "line1": address['address'].get('line1', ''),
            "line2": '',
            "line3": address['address'].get('line3', ''),
            "line4": address['address'].get('line4', ''),
            "postcode": address['address'].get('postcode', None),
            "country": address['address'].get('country',
                                              settings.COUNTRY_CODE_A2),
            "phone_number": parsed_number if parsed_number else None,
            "geopoint": CommonHelper.get_geopoint_from_latlong(
                address['geopoint'].get('latitude', 0),
                address['geopoint'].get('longitude', 0)
            )
        }

    def _clean_pickup_time(self, now_or_later, later_value=None):
        if now_or_later == 'now':
            pickup_time = timezone.now()
        else:
            m = re.match(r'(\d{2} \S{3} \d{4}) (\d:\d{2})', later_value)
            pickup_time = '%s 0%s' % (
                m.group(1), m.group(2)) if m else later_value
            for f in ALLOWED_TIME_FORMATS:
                logging.info('Matching time "%s" with format "%s"' %
                             (later_value, f))
                try:
                    curr_tz = pytz.timezone(settings.ACT_TIME_ZONE)
                    _dt = curr_tz.localize(datetime.strptime(later_value, f))
                    pickup_time = ist_to_utc(_dt)
                    break
                except:
                    pass

        return pickup_time

    def _clean_waypoints(self, waypoints):
        waypoints_cleaned = []
        for i, waypoint in enumerate(waypoints):
            contact = waypoint.get('contact')
            address = waypoint.get('address')
            geopoint = waypoint.get('geopoint')

            phone_no = contact.get('mobile', None) if contact else None
            if not phone_no:
                phone_no = contact.get('phone_number', '') if contact else ''

            waypoints_cleaned.append({
                "title": "",
                "first_name": contact.get('name', '') if contact else '',
                "last_name": "",
                "line1": address.get('line1', '') if address else '',
                "line2": '',
                "line3": address.get('line3', '') if address else '',
                "line4": address.get('line4', '') if address else '',
                "postcode": address.get('postal_code', None) if address else '',
                "phone_number": phone_no,
                "geopoint": CommonHelper.get_geopoint_from_latlong(geopoint.get('latitude', 0),
                                                                   geopoint.get('longitude', 0)),
                'sequence_id': i + 1
            })

        return waypoints_cleaned

    def _clean_labour(self, labour):
        count = 0
        cost = 0
        if isinstance(labour, str):
            if labour in ['light', 'heavy']:
                labour_cost = OrderConstants().get('LABOUR_COST', 400)
                LABOUR_COST_MAPPING = {
                    'light': labour_cost,
                    'medium': labour_cost,
                    'heavy': labour_cost,
                }
                count = 1
                cost = LABOUR_COST_MAPPING.get(labour, 400)

        elif isinstance(labour, dict):
            cost = labour.get('cost_per_labour', 0)
            count = labour.get('num_of_labours', 0)

        return {
            'no_of_labours': count,
            'labour_cost': cost
        }
