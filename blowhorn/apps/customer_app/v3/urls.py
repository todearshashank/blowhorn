from django.conf.urls import url
from . import views

urlpatterns = [
            # List/ Create Order
            url(r'^createorder/$', views.CreateOrder.as_view(),
                name='v2-create-order'),
        ]
