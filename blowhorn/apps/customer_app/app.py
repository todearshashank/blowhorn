# from django.conf.urls import url
#
# from django.apps import AppConfig
#
# from blowhorn.apps.customer_app.v1.app import application as v1
# from blowhorn.apps.customer_app.v2.app import application as v2
# from blowhorn.apps.customer_app.v3.app import application as v3
# from blowhorn.apps.customer_app.v4.app import application as v4
# from blowhorn.apps.customer_app.v5.app import application as v5
# from blowhorn.apps.customer_app.v6.app import application as v6
#
#
# class CustomerVersionedApplication(AppConfig):
#
#     label = 'customer_app'
#     name = 'blowhorn.customer_app'
#     verbose_name = 'Customer App'
#
#     namespace = 'customer_app'
#
#     def get_urls(self):
#         urlpatterns = [
#             url(r'^v1/', v1.urls),
#             url(r'^v2/', v2.urls),
#             url(r'^v3/', v3.urls),
#             url(r'^v4/', v4.urls),
#             url(r'^v5/', v5.urls),
#             url(r'^v6/', v6.urls),
#         ]
#         return self.post_process_urls(urlpatterns)
