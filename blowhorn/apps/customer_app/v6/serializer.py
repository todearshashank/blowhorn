from datetime import timedelta

import pytz
from django.conf import settings
from django.utils import timezone

from rest_framework import serializers

from blowhorn.address.mixins import AddressMixin
from blowhorn.order.const import PAYMENT_STATUS_PAID, PAYMENT_MODE_CASH
from blowhorn.order.models import Order, ProductRating
from config.settings import status_pipelines as StatusPipeline
from config.settings.status_pipelines import ORDER_CANCELLED_STATUS, ORDER_DELIVERED, RESCHEDULE_DELIVERY, ORDER_NEW, \
    DRIVER_ACCEPTED, OUT_FOR_DELIVERY, DRIVER_ASSIGNED, ORDER_CANCELLED, ORDER_RETURNED, UNABLE_TO_DELIVER


class OrderListSerializer(serializers.ModelSerializer):
    date_placed = serializers.SerializerMethodField(read_only=True)

    total_payable = serializers.SerializerMethodField(read_only=True)

    exp_delivery_start_time = serializers.SerializerMethodField(read_only=True)

    expected_delivery_time = serializers.SerializerMethodField(read_only=True)

    desc = serializers.SerializerMethodField(read_only=True)

    images = serializers.SerializerMethodField(read_only=True)

    is_payment_required = serializers.SerializerMethodField(read_only=True)

    delivered_datetime = serializers.SerializerMethodField(read_only=True)

    payment_status_desc = serializers.SerializerMethodField(read_only=True)


    class Meta:
        model = Order
        fields = ('id', 'number', "payment_status_desc", "date_placed", "payment_mode", "payment_status", "is_payment_required",
                  "desc", "total_payable", "exp_delivery_start_time", "expected_delivery_time",  "images",
                  "delivered_datetime", "payment_status_desc")

    def get_total_payable(self, order):
        return float(order.total_payable)

    def get_payment_status_desc(self, order):
        return "Payment Pending" if not order.payment_status == PAYMENT_STATUS_PAID else "Payment Done"

    def get_images(self, order):
        urls = []
        for line in order.order_lines.all():
            for images in line.product.images.all():
                urls.append(images.original.url)
        return urls

    def get_desc(self, order):
        if order.status in ORDER_CANCELLED_STATUS:
            return "Order Cancelled"
        expected_delivery_time = order.expected_delivery_time
        if not expected_delivery_time:
            expected_delivery_time = timezone.now() + timedelta(days=2)
        return 'Will be delivered by %s' %(expected_delivery_time.strftime('%b, %d %Y')) \
            if not order.status == ORDER_DELIVERED else 'Delivered on %s' %(expected_delivery_time.strftime('%b, %d %Y'))

    def get_date_placed(self, order):
        return order.date_placed.isoformat() if order.date_placed else ''

    def get_delivered_datetime(self, order):
        delivered_date = ''
        if order.status == ORDER_DELIVERED:
            events = order.event_set.all()
            for event in events:
                if event.status == ORDER_DELIVERED:
                    delivered_date = event.time.isoformat() if event.time else ''
        return delivered_date


    def get_expected_delivery_time(self, order):
        return order.expected_delivery_time.isoformat() if order.expected_delivery_time else ''

    def get_exp_delivery_start_time(self, order):
        return order.exp_delivery_start_time.isoformat() if order.exp_delivery_start_time else ''

    def get_is_payment_required(self, order):
        return True if order.status not in ORDER_CANCELLED_STATUS and \
                       not order.payment_status == PAYMENT_STATUS_PAID else False


class OrderCustomerSerializer(serializers.ModelSerializer):
    date_placed = serializers.SerializerMethodField(read_only=True)

    total_payable = serializers.SerializerMethodField(read_only=True)

    exp_delivery_start_time = serializers.SerializerMethodField(read_only=True)

    expected_delivery_time = serializers.SerializerMethodField(read_only=True)

    desc = serializers.SerializerMethodField(read_only=True)

    orderlines = serializers.SerializerMethodField(read_only=True)

    is_payment_required = serializers.SerializerMethodField(read_only=True)

    shipping_address = serializers.SerializerMethodField(read_only=True)

    delivered_datetime = serializers.SerializerMethodField(read_only=True)

    breakup = serializers.SerializerMethodField(read_only=True)

    events = serializers.SerializerMethodField(read_only=True)

    can_cancel = serializers.SerializerMethodField(read_only=True)

    api_key = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'number', "status", "date_placed", "payment_mode", "payment_status", "is_payment_required",
                  "orderlines", "desc", "total_payable", "exp_delivery_start_time", "expected_delivery_time",
                  "shipping_address", "delivered_datetime", "breakup", "events", "can_cancel", 'api_key')

    def get_events(self, order):
        data = []
        DEFAULT_STATUS = [ORDER_NEW]
        if order.status == ORDER_CANCELLED:
            DEFAULT_STATUS.append(ORDER_CANCELLED)
        else:
            DEFAULT_STATUS.extend([DRIVER_ASSIGNED, OUT_FOR_DELIVERY])
            if order.status == ORDER_RETURNED:
                DEFAULT_STATUS.append(ORDER_RETURNED)
            elif order.status == UNABLE_TO_DELIVER:
                DEFAULT_STATUS.append(UNABLE_TO_DELIVER)
            else:
                DEFAULT_STATUS.append(ORDER_DELIVERED)

        SORT_ORDER = DEFAULT_STATUS.copy()

        events = order.event_set.filter(status__in=DEFAULT_STATUS).order_by('time')
        for event in events:
            display_status = StatusPipeline.ORDER_B2C_STATUSES.get(event.status, '')
            data.append({
                "status": event.status,
                "date": event.time.isoformat(),
                "desc": StatusPipeline.ORDER_B2C_DESC.get(event.status, ''),
                "display_status": display_status,
                "is_current_status": True if event.status == order.status and \
                                             event.status not in [ORDER_DELIVERED, RESCHEDULE_DELIVERY] else False

            })
            if event.status in DEFAULT_STATUS:
                DEFAULT_STATUS.remove(event.status)

        # if not order.status == ORDER_DELIVERED:
        for status in DEFAULT_STATUS:
            expected_delivery_time =  order.expected_delivery_time
            if not expected_delivery_time:
                expected_delivery_time = timezone.now() + timedelta(days=2)
            data.append({
                "status": status,
                "date": "",
                "display_status": StatusPipeline.ORDER_B2C_STATUSES.get(status, ''),
                "desc": StatusPipeline.ORDER_B2C_FUTURE_DESC.get(status) % (expected_delivery_time.strftime('%b, %d %Y'))
                if status == ORDER_DELIVERED else StatusPipeline.ORDER_B2C_FUTURE_DESC.get(status),
                "is_current_status": False

            })

        custom_order = {key: i for i, key in enumerate(SORT_ORDER)}
        events = sorted(data, key=lambda d: custom_order[d['status']])
        return events

    def get_orderlines(self, order):
        line_info = []
        for line in order.order_lines.all():
            product = line.product
            stocks = product.stockrecords.all()
            for stock in stocks:
                selling_price = float(stock.price_excl_tax) if stock.price_excl_tax else 0
                if selling_price:
                    review = ProductRating.objects.filter(line=line).first()
                    _data = {
                        "image_url": [x.original.url for x in product.images.all()],
                        "product_id": product.id,
                        "is_rating_required": order.status == ORDER_DELIVERED,
                        "title": product.title,
                        "mrp_price": float(stock.price_retail) if stock.price_retail else 0,
                        "price": line.each_item_price,
                        "item_quantity": line.quantity,
                        "total_item_price": line.total_item_price,
                        "uom": stock.uom if stock.uom else '',
                        "pack_of": stock.pack_of if stock.pack_of else 1,
                    }
                    if review:
                        _data.update({"rating":{
                                      'id': review.id,
                                      "line_id": review.line_id,
                                      "product_id": review.product_id,
                                      "rating": review.rating or 0,
                                      "body": review.body,
                                      "title": review.title,
                                      "order_id": review.line.order_id
                                  }})
                    else:
                        _data.update({"rating": {}})
                    line_info.append(_data)
        return line_info

    def get_total_payable(self, order):
        return float(order.total_payable)

    def get_can_cancel(self, order):
        return not order.driver and not order.status in ORDER_CANCELLED_STATUS \
               and not order.payment_status == PAYMENT_STATUS_PAID

    def get_breakup(self, order):
        total_item_price_ = 0
        for line in order.order_lines.all():
            total_item_price_ += float(line.total_item_price)
        delivery_charge = float(order.total_payable) - float(total_item_price_)
        data = [
            {"key": "Item total price", "value": total_item_price_},
        ]
        if delivery_charge:
            data.append({
                "key": "Delivery Charge", "value": delivery_charge
            })

        data.append({"key": "Total", "value": order.total_payable})

        return data

    def get_desc(self, order):
        if order.payment_mode == PAYMENT_MODE_CASH:
            desc = "Cash On Delivery"
        elif not order.payment_status == PAYMENT_STATUS_PAID:
            desc = "Please Make Payment ASAP to avoid Cancellation of order"
        else:
            desc = "Payment Successfully received"
        return desc

    def get_date_placed(self, order):
        return order.date_placed.isoformat() if order.date_placed else ''

    def get_api_key(self, order):
        return settings.FLASH_SALE_API_KEY

    def get_delivered_datetime(self, order):
        delivered_date = ''
        if order.status == ORDER_DELIVERED:
            events = order.event_set.all()
            for event in events:
                if event.status == ORDER_DELIVERED:
                    delivered_date = event.time.isoformat() if event.time else ''
        return delivered_date


    def get_expected_delivery_time(self, order):
        return order.expected_delivery_time.isoformat() if order.expected_delivery_time else ''

    def get_exp_delivery_start_time(self, order):
        return order.exp_delivery_start_time.isoformat() if order.exp_delivery_start_time else ''

    def get_is_payment_required(self, order):
        return True if order.status not in ORDER_CANCELLED_STATUS else False

    def get_shipping_address(self, order):
        return AddressMixin().get_detail_dict_v1(order.shipping_address)
