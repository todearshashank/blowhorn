# from django.conf.urls import url
# from django.apps import AppConfig
# from . import views
#
# class CustomerVersionSix(AppConfig):
#
#     def get_urls(self):
#         urlpatterns = [
#             # User APIS
#
#             url(r'^login/$', views.Login.as_view(),
#                 name='v6-login'),
#             url(r'^validate/customer/$', views.ValidateCustomerNumber.as_view(),
#                 name='v6-validate-customer'),
#             url(r'^validate/otp/$', views.ValidateCustomerOtp.as_view(),
#                 name='v6-validate-customer'),
#             url(r'^fblogin/$', views.FacebookLogin.as_view(),
#                 name='v6-fb-login'),
#             url(r'^applelogin/$', views.AppleLogin.as_view(),
#                 name='v6-fb-login'),
#             url(r'^googlelogin/$', views.GoogleLogin.as_view(),
#                 name='v6-google-login'),
#             url(r'^register/$', views.Register.as_view(),
#                 name='v6-register-user'),
#             url(r'^cityinformation/$', views.CityInformation.as_view(),
#                 name='v6-city-info'),
#             url(r'^getsavedlocation/$', views.SavedLocation.as_view(),
#                 name='v6-saved-location'),
#             url(r'^getcancellationreason/$', views.CancellationReason.as_view(),
#                 name='v6-cancellation-reasons'),
#             url(r'^itemcategory/$', views.ItemCategoryList.as_view(),
#                 name='v6-item-category'),
#             url(r'^getfareestimate/$', views.getEstimatedFare.as_view(),
#                 name='v6-fare-estimate'),
#
#         ]
#
#         return self.post_process_urls(urlpatterns)
#
