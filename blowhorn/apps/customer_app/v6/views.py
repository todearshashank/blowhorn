import json
import re
from django.core.mail.message import EmailMultiAlternatives

from django.db import transaction
from phonenumbers import PhoneNumber
from rest_framework.authentication import BasicAuthentication
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from blowhorn.address.mixins import AddressMixin
from blowhorn.address.models import City
from blowhorn.address.utils import get_city_from_geopoint
from blowhorn.apps.customer_app.common import Slot
from blowhorn.apps.customer_app.v1.social_profile import FacebookProfile, GoogleProfile
from blowhorn.apps.customer_app.v5.views import CreateOrder
from blowhorn.common import sms_templates, apple_login
from blowhorn.common.helper import CommonHelper
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.contract.constants import VEHICLE_SERVICE_ATTRIBUTE_MINUTES, VAS_SERVICE_ATTRIBUTE_PICKUP_FLOOR_NUMBER, \
    VAS_SERVICE_ATTRIBUTE_POD
from blowhorn.contract.models import Contract
from blowhorn.coupon.mixins import CouponMixin

from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, CustomerTaxInformation,\
    Customer, CUSTOMER_SOURCE_MOBILE
from blowhorn.customer.views import CustomerProfile

from django.conf import settings
from rest_framework import status, generics

from blowhorn.driver.models import DriverConstants
from blowhorn.order.models import FlashSaleBanner, ItemCategory, Order, ShippingAddress
from blowhorn.order.utils import OrderUtil
from blowhorn.customer.models import Customer
from blowhorn.users.models import User
from blowhorn.common.tasks import send_sms
from blowhorn.users.utils import UserUtils
from blowhorn.users.permission import IsActive, IsMobileVerified
from django.template import loader



def get_login_response(customer, user, token):
    gstin_info = CustomerTaxInformation.objects.select_related('state')
    gstin_info = gstin_info.filter(customer=customer)
    gst_data = []
    for info in gstin_info:
        gst_data.append({
            "state_id": info.state_id,
            "state_name": info.state.name,
            "gstin": info.gstin

        })

    response = {
        'name': user.name,
        'mobile': str(user.phone_number),
        'email': user.email,
        'mAuthToken': token,
        'is_business_user': True if customer and customer.customer_category !=
                                    CUSTOMER_CATEGORY_INDIVIDUAL else False,
        "legal_name": customer.legalname if customer and customer.legalname else '',
        "gst_data": gst_data,
        "customer_id": customer.id,
        "wallet_available_balance": customer.get_customer_balance() if customer else 0,
        "onboarding_completed": True if customer.hub_set.exists() else False,
    }
    return response


class ValidateCustomerNumber(generics.RetrieveAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        phone_number = data.get('mobile')

        if phone_number:

            is_valid_number = re.match('^[0-9+]+$', str(phone_number))
            if not is_valid_number:
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid mobile number')

            phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, phone_number)
            customer = Customer.objects.filter(user__phone_number=phone_number).first()
            user = customer.user if customer else None
            if user:
                response = {"is_registered": True}
                if str(phone_number) in settings.TESTING_NUMBERS:
                    return Response(status=status.HTTP_200_OK, data=response)
                otp, created = User.objects.retrieve_otp(user)
                template_dict = sms_templates.user_login_otp
                message = template_dict['text'] % (str(customer), otp)
                template_json = json.dumps(template_dict)
                if not settings.DEBUG:
                    send_sms.apply_async(args=[str(user.phone_number), message, template_json])
            else:
                response = {"is_registered": False }
            return Response(status=status.HTTP_200_OK, data=response)
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid mobile number')


class ValidateCustomerOtp(generics.RetrieveAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        mobile = data.get('mobile')
        otp = data.get('otp')
        if not mobile or not otp:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Mobile number and otp is required')
        if mobile:
            phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, mobile)
            customer = Customer.objects.filter(user__phone_number=phone_number).first()
            user = customer.user if customer else None
            if user and not user.is_active and\
                    user.login_attempt > settings.INVALID_OTP_ATTEMPTS_ALLOWED:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='User is blocked, Please contact support')
            if user:
                # customer = self.get_customer(user)
                if customer:
                    Customer.objects.add_otp(customer=customer, otp=data.get('otp'),
                                             remarks='OTP entered by customer - Mobile')
                login_response = User.objects.do_login(
                    request=request, user=user, mobile=mobile, otp=otp)
                if login_response.status_code == status.HTTP_200_OK:
                    user = login_response.data.get('user')

                    if not customer or user.is_staff:
                        return Response(status=status.HTTP_401_UNAUTHORIZED, data='Users are not allowed to login')
                    token = login_response.data.get('token')
                    response = get_login_response(customer, user, token)
                    return Response(status=status.HTTP_200_OK, data=response)
                return Response(status=login_response.status_code, data=login_response.data)
            else:
                response = {"is_registered": False}
            return Response(status=status.HTTP_200_OK, data=response)

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid mobile number')


class Login(generics.RetrieveAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        data = request.data
        email = data.get('email')
        password = data.get('password')
        is_wms_login = data.get('is_wms_login', False)
        if not email or not email:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data='Email and password are required'
            )
        if email:
            user = User.objects.filter(
                email=email,
                is_staff=False,
                driver__isnull=True,
                vendor__isnull=True
            ).first()

            if user and is_wms_login and user.check_password(password) and not user.is_mobile_verified:
                response = {
                    'mobile': str(user.phone_number),
                    'is_mobile_verified': False
                }
                return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)
            if user and not user.is_active:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data='You\'re not allowed to login. Please contact support.'
                )
            if user:
                customer = self.get_customer(user)
                login_response = User.objects.do_login(
                    request=request,
                    email=email,
                    password=password
                )
                if login_response.status_code == status.HTTP_200_OK:
                    user = login_response.data.get('user')

                    if not customer or user.is_staff:
                        return Response(
                            status=status.HTTP_401_UNAUTHORIZED,
                            data='Users are not allowed to login'
                        )
                    token = login_response.data.get('token')
                    response = get_login_response(customer, user, token)
                    return Response(status=status.HTTP_200_OK, data=response)
                return Response(status=login_response.status_code, data=login_response.data)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Incorrect Email/Password')
            return Response(status=status.HTTP_200_OK, data=response)

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Email is required')


@permission_classes([])
class SocialLogin(generics.CreateAPIView, CustomerMixin):

    def set_response(self, request, user_info, create_user=False):
        if not user_info:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='account_unavailable')
        else:
            email = user_info.get('email')
            name = user_info.get('name')
            if not (email):
                return Response(status=status.HTTP_400_BAD_REQUEST, data='account_unavailable')
            else:
                user = User.objects.filter(email=email.lower(), is_active=True).first()
                if not user:
                    response = {
                        "is_registered": False,
                        "email": email
                    }
                    return Response(status=status.HTTP_200_OK, data=response)
                if user and user.is_staff:
                    return Response(status=status.HTTP_401_UNAUTHORIZED, data='Users are not allowed to login')
                login_response = User.objects.do_login(
                    request=request, user=user)

                if login_response.status_code != status.HTTP_200_OK:
                    message = login_response.data
                    return Response(status=login_response.status_code, data=message)
                else:
                    token = login_response.data.get('token')
                    customer = self.get_customer(user)
                    response = get_login_response(customer, user, token)
                    response.update({"is_registered": True,
                                     "email": email})


                return Response(status=status.HTTP_200_OK, data=response)


@permission_classes([])
class FacebookLogin(generics.CreateAPIView):
    queryset = User.objects.all()
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        accessToken = data.get("access_token")
        isMobile = request.META.get('HTTP_MOBILE')

        sp = FacebookProfile()
        user_info = sp.get(token=accessToken)
        return SocialLogin().set_response(request, user_info)

    def get_serializer_class(self):
        pass

@permission_classes([])
class AppleLogin(generics.CreateAPIView):
    queryset = User.objects.all()
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        accessToken = data.get("access_token")

        sp = apple_login.AppleOAuth2()
        user_info = sp.do_auth(accessToken)
        return SocialLogin().set_response(request, user_info)

    def get_serializer_class(self):
        pass



@permission_classes([])
class GoogleLogin(generics.CreateAPIView):
    queryset = User.objects.all()
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        accessToken = data.get("access_token")

        sp = GoogleProfile()
        user_info = sp.get(token=accessToken)
        return SocialLogin().set_response(request, user_info)

    def get_serializer_class(self):
        pass


class Register(generics.CreateAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def send_sms(self, customer_response, phone_number, user=None, customer=None):
        user = user or customer_response.data.get('user')
        customer = customer or customer_response.data.get('customer')
        otp, created = User.objects.retrieve_otp(user)
        template_dict = sms_templates.user_login_otp
        message = template_dict['text'] % (str(customer), otp)
        template_json = json.dumps(template_dict)
        send_sms.apply_async(args=[str(user.phone_number), message, template_json])

    def send_wms_welcome_mail(self, email):
        email_template_name = 'emailtemplates_v2/wms_welcome.html'
        html_content = loader.render_to_string(email_template_name,{})
        mail = EmailMultiAlternatives(
            "Welcome Aboard",
            html_content,
            to=[email,],
            from_email=settings.DEFAULT_FROM_EMAIL)
        mail.attach_alternative(html_content, "text/html")
        mail.send(fail_silently=True)

    def post(self, request):
        data = request.data
        is_wms_login = data.get('is_wms_login', False)
        phone_number = data.get('mobile')
        email = data.get('email', '') or ''
        name = data.get('name')
        password = data.get('password', None)
        source = data.get('source', None)
        is_business_user = data.get('is_business_user', None)
        # if is_business_user == 'true':
        #     is_business_user = True
        # else:
        #     is_business_user = False

        legal_name = data.get('legal_name', None)
        gstin = data.get('gstin', None)

        if not phone_number:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid phone number')

        phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, phone_number)

        email = email.lower()
        if not email or not re.compile(settings.EMAIL_REGEX).match(email):
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Please, provide a valid email')

        if UserUtils().is_staff_email(email):
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Company email is not allowed for signup')

        user = User.objects.filter(email=email).first()
        if user:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Email is already registered')

        if user and not user.is_active:
            customer = self.get_customer(user)
            if customer:
                self.send_sms(None, None, user=user, customer=customer)
                return Response(status=status.HTTP_200_OK, data='Registered successfully')

            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid user')

        if User.objects.filter(phone_number=phone_number, customer__isnull=False).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Mobile number is already Registered')

        with transaction.atomic():
            _dict = {
                'email': email.lower(),
                'name': name,
                'phone_number': data.get('mobile'),
                'source': source or CUSTOMER_SOURCE_MOBILE
            }
            if password:
                _dict['password'] = password

            if is_business_user:
                if not gstin:
                    return Response(
                        status=status.HTTP_400_BAD_REQUEST,
                        data='GSTIN is required for business customer',
                        content_type='text/html; charset=utf-8'
                    )
                if Customer.objects.filter(legalname=legal_name).exists():
                    return Response(
                        status=status.HTTP_400_BAD_REQUEST,
                        data='User with legalname already exists.',
                        content_type='text/html; charset=utf-8'
                    )

                tax_info = CustomerTaxInformation.objects.filter(gstin=gstin).first()
                if tax_info:
                    return Response(
                        status=status.HTTP_400_BAD_REQUEST,
                        data='Customer with GSTIN exist'
                    )
            customer_response = Customer.objects.create_customer(**_dict)
            customer = customer_response.data.get('customer')
            if is_business_user and gstin and \
                CustomerProfile().has_tax_info_edit_permission(user, customer):
                success, message = CustomerProfile()._update_tax_information(
                    customer,
                    name,
                    is_business_user,
                    legal_name,
                    gstin,
                    None
                )
            self.send_sms(customer_response, phone_number)
            if is_wms_login:
                self.send_wms_welcome_mail(email)
            return Response(status=status.HTTP_200_OK, data='Registered successfully')

class CityInformation(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)
    def _get_latlng_list(self, coordinates):
        latlon_list = []
        for latlong_arr in coordinates:
            for latlong in latlong_arr:
                latlon_list.append({
                    "latitude": latlong[1],
                    "longitude": latlong[0],
                })
        return latlon_list

    def _get_coverage_area(self):
        coverage = []
        contracts = Contract.objects.get_distinct_cities_from_active_c2c_sme_contracts()
        for contract in contracts:
            city = contract.city
            pickup_coordinates = city.coverage_pickup.coords
            dropoff_coordinates = city.coverage_dropoff.coords
            coverage.append({
                "city": city.name,
                "pickup": self._get_latlng_list(pickup_coordinates),
                "dropoff": self._get_latlng_list(dropoff_coordinates),
            })

        return coverage

    def get(self, request):
        data = request.data
        latitude = data.get('latitude')
        longitude = data.get('longitude')

        user = request.user
        if user.is_staff:
            return Response(status=status.HTTP_403_FORBIDDEN, data='Users are not allowed to login')

        geopoint = CommonHelper.get_geopoint_from_latlong(
            latitude, longitude)

        city = None
        if geopoint:
            city = get_city_from_geopoint(geopoint, field='coverage_pickup')
        if not city:
            city = City.objects.first()
        cityno = str(city.contact)
        response = {
            "time_slot_details": Slot().get_time_slots_for_city(city),
            "coverage": self._get_coverage_area(),
            'call_support': cityno,
            'share_sub_text': settings.C2C_CONSTANTS['APP_SHARE_SUB_TEXT'],
            'share_body_text': settings.C2C_CONSTANTS['APP_SHARE_BODY_TEXT'],
            "notification_title": DriverConstants().get("NOTIFICATION_TITLE", ""),
            "notification_url": DriverConstants().get("NOTIFICATION_URL", ""),
            "open_in_app": False if DriverConstants().get(
                "NOTIFICATION_URL_OPEN_IN_APP", "") == "False" else True
        }
        flashsale_banners = FlashSaleBanner.objects.filter(
            is_active=True, city=city).only('banner')
        response.update({"flash_sale_banner": [x.banner.url for x in flashsale_banners
                                               if x.banner and x.banner.url]})
        return Response(status=status.HTTP_200_OK, data=response)


class SavedLocation(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)
    def get(self, request):
        user = request.user
        saved_locations = AddressMixin().get_saved_user_addresses(user, new_app=True)
        recent_locations = AddressMixin().get_recent_user_addresses(user, new_app=True)
        response = {
            "saved_locations": saved_locations,
            "recent_locations": recent_locations

        }
        return Response(status=status.HTTP_200_OK, data=response)

class CancellationReason(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)
    def get(self, request):
        customer_cancellation_reasons = []
        for key in OrderUtil().fetch_order_cancellation_reasons():
            customer_cancellation_reasons.append({
                'key': key,
                'value': key
            })
        return Response(status=status.HTTP_200_OK, data=customer_cancellation_reasons)


class ItemCategoryList(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)

    def get(self, request):
        item_category = sorted(ItemCategory.objects.values(
            'category', 'id'), key=lambda x: x['category'])
        return Response(status=status.HTTP_200_OK, data=item_category)

class getEstimatedFare(APIView, CustomerMixin):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)
    # permission_classes = (AllowAny,)
    # authentication_classes = (
    #     CsrfExemptSessionAuthentication, BasicAuthentication)


    def get_labour(self, data):
        labour_cost = 0
        labour_option = data.get('labour_option', '')
        if labour_option:
            return labour_option.get('cost_per_labour') * labour_option.get('num_of_labours')
        return labour_cost

    def get_houseshifting_information(self, address):
        is_lift_accessible = address.get('is_lift_accessible')
        is_service_lift_available = address.get('is_service_lift_available')
        floor_number = address.get('floor_number')
        return floor_number, is_lift_accessible, is_service_lift_available

    def post(self, request):

        data = request.data
        device_type = data.get('device_type', None)
        user = request.user
        order = Order()
        if data.get('pickup_address') and data.get('dropoff_address'):
            pickup_address = ShippingAddress()
            dropoff_address = ShippingAddress()
            pickup_address.floor_number, pickup_address.is_lift_accessible, pickup_address.is_service_lift_available = \
                self.get_houseshifting_information(data.get('pickup_address'))
            dropoff_address.floor_number, dropoff_address.is_lift_accessible, dropoff_address.is_service_lift_available = \
                self.get_houseshifting_information(data.get('dropoff_address'))
            order.pickup_address = pickup_address
            order.shipping_address = dropoff_address
        order.customer = self.get_customer(user)
        contract_id = data.get('contract_id')

        item_category = data.get('item_category')
        self.item_category = ItemCategory.objects.filter(id__in=item_category)
        contract = Contract.objects.filter(id=contract_id)
        contract = contract.select_related('city').first()
        order.tip = data.get('tipAmount', 0)
        order.customer_contract = contract
        order.estimated_distance_km = data.get('distanceInKM', 0.0)
        order.estimated_time_in_mins = data.get('timeInMins', 0.0)

        timeframe = data.get('timeframe', None)
        now_or_later = timeframe.get('now_or_later', None)
        later_value = timeframe.get('later_value', '')
        order.pickup_datetime = CreateOrder()._clean_pickup_time(now_or_later, later_value)
        order.city = order.customer_contract.city
        coupon_code = data.get('coupon_code', '')
        coupon = None
        if coupon_code:
            coupon = CouponMixin().get_coupon_from_code(coupon_code)
        order.coupon = coupon
        order.is_pod_required = data.get('is_pod_required', False)

        labour_charge = self.get_labour(data)
        order.calculate_and_update_cost(labour_charge=labour_charge)
        total_payable = order.total_payable
        total_payable += labour_charge
        if order.invoice_details:
            components = json.loads(order.invoice_details)
        else:
            components = {}
        breakup = []

        time_charge = components.get('time')
        time_data = []
        time_start = 0
        distance_charge = components.get('distance')
        dist_data= []
        dist_start = 0
        vat_charge = components.get('vas_amount')
        pickup_floor_charges = []
        dropoff_floor_charges = []
        pod_amount = 0
        att = "Mins"
        charged_units = 0
        for x in time_charge:
            if not x.get('amount'):
                continue
            att = 'Min' if x['attribute'] == VEHICLE_SERVICE_ATTRIBUTE_MINUTES else 'Hr'
            slab = x['slab']
            start = slab.get('start')
            end = slab.get('end')
            if not time_start:
                time_start = slab.get('start')
            chargable_val = min(x['value']-start, end-start)
            display =  chargable_val +  start if not charged_units else chargable_val + charged_units
            time_data.append({
                'details': "From %s's %s to %s @ %s/%s" % (att, start, display , slab.get('rate'), att),
                'value': chargable_val,
                'amount':  x.get('amount') or 0
            })
            charged_units += float(display)

        charged_units = 0
        for x in distance_charge:
            if not x.get('amount'):
                continue
            slab = x['slab']
            start = slab.get('start')
            end = slab.get('end')
            if not dist_start:
                dist_start = start
            chargable_val = round(float(min(x['value'] - start, end - start)),2)
            display =  chargable_val +  start if not charged_units else chargable_val + charged_units
            dist_data.append({
                'details': "From Km's %s to %s @ %s/Km" % (start, display, slab.get('rate')),
                'value': chargable_val,
                'amount': x.get('amount') or 0
            })
            charged_units += float(display)

        charged_units = 0
        for x in vat_charge:
            if x.get('attribute') == VAS_SERVICE_ATTRIBUTE_POD:
                pod_amount = x.get('amount')
                continue
            if not x.get('amount'):
                continue
            slab = x['slab']
            start = slab.get('start')
            end = slab.get('end')
            chargable_val = round(float(min(x['value'] - start, end - start)),2)
            display = chargable_val +  start if not charged_units else chargable_val + charged_units
            if x.get('attribute') == VAS_SERVICE_ATTRIBUTE_PICKUP_FLOOR_NUMBER:
                pickup_floor_charges.append({
                    'details': "Charges for carrying goods from floor %s to %s @ %s/Floor" % (start, display, slab.get('rate')),
                    'value': chargable_val,
                    'amount': x.get('amount') or 0
                })
            else:
                dropoff_floor_charges.append({
                    'details': "Charges for carrying goods from floor %s to %s @ %s/Floor" % (
                    start, display, slab.get('rate')),
                    'value': chargable_val,
                    'amount': x.get('amount') or 0
                })
            charged_units += float(display)



        if components:
            breakup.append({
                'title': 'Minimum Fare',
                'details': [{
                    'details': 'For %skm and %s %s' % (dist_start, time_start, att),
                    'amount': components.get('fixed', 0) or 0,
                    'value': 1
                }]
            })


        if time_data:
            breakup.append({
                'title':'Time Charges',
                'details':time_data})
        if distance_charge:
            breakup.append({
                'title': 'Distance Charges',
                'details': dist_data})
        if pickup_floor_charges:
            breakup.append({
                'title': 'Pickup Floor Charges',
                'details': pickup_floor_charges})
        if dropoff_floor_charges:
            breakup.append({
                'title': 'Drop off Floor Charges',
                'details': dropoff_floor_charges})
        if pod_amount:
            breakup.append({
                'title': 'POD Charges',
                'details': [{
                    'details': 'POD Charges',
                    'amount': pod_amount,
                    'value': 0
                }],

            })

        if labour_charge:
            labour_option = data.get('labour_option', '')
            breakup.append({
                'title': 'Labour Charges',
                'details': [{
                    'details': 'Labour Charges ' + str(labour_option.get('num_of_labours'))  + ' * '
                               + str(labour_option.get('cost_per_labour')),
                    'amount': labour_charge,
                    'value': 0
                }],

            })

        if components.get('charge_for_carrying_goods', 0):
            breakup.append({
                'title': 'Goods Carrying Charges',
                'details': [{
                    'details': 'Goods Carrying Charges',
                    'amount': components.get('charge_for_carrying_goods', 0),
                    'value': 0
                }],

            })

        response = {
            'total_payable': total_payable,
            'fare_breakup': breakup,
            'discount': order.discount or 0,
            'before_discount': total_payable + (order.discount or 0)
        }

        return Response(status=200, data=response)
