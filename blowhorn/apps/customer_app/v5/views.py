import re
import json
import traceback
import logging
import requests
import phonenumbers
from datetime import datetime
from functools import reduce
import operator

from django.core.mail.message import EmailMultiAlternatives
from django.template import loader
from django.utils import timezone
from django.db import transaction
from django.urls import reverse

from blowhorn.oscar.core.loading import get_model
from rest_framework.response import Response

from blowhorn.apps.customer_app.v5.serializer import OrderCustomerSerializer, OrderListSerializer
from blowhorn.common import serializers as common_serializer
from blowhorn.common.helper import CommonHelper
from blowhorn.freshchat.whatsapp import send_whatsapp_notification
from dateutil.parser import parse
from rest_framework.decorators import permission_classes

from blowhorn.address.utils import AddressUtil
from blowhorn.common.utils import generate_pdf
from blowhorn.company.models import Office
from blowhorn.customer.permissions import IsCustomer
from blowhorn.common.payment import Payment
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.driver.constants import PAGE_TO_BE_DISPLAYED, NUMBER_OF_ENTRY_IN_PAGE
from blowhorn.driver.models import DriverConstants
from blowhorn.order.serializers.OrderSerializer import OrderSerializer
from django.db.models import Q, Prefetch, Case, When, Value, IntegerField
from blowhorn.order.const import BLOWHORN_WALLET
from django.core.paginator import Paginator
from django.conf import settings
from rest_framework import status, generics

from blowhorn.users.permission import IsActive, IsMobileVerified
from blowhorn.vehicle.models import VehicleClass
from config.settings import status_pipelines as StatusPipeline
from rest_framework.views import APIView
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from blowhorn.order.models import OrderConstants, ItemCategory, OrderLine, ProductRating
from blowhorn.contract.utils import ContractUtils
from blowhorn.contract.models import Contract
from blowhorn.coupon.mixins import CouponMixin
from blowhorn.coupon.serializers import CouponModelSerializer
from blowhorn.apps.customer_app.v1.helper import upsert_driver_rating
from blowhorn.payment.views import Razorpay as PaymentRazorpay
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.customer.serializers import CustomerProfileSerializer, \
    CustomerFeedbackSerializer, CustomerShippingAddressSerializer, \
    WaypointSerializer, CustomerTaxInformationSerializer, PaymentHelper
from blowhorn.apps.customer_app.v1.social_profile import FacebookProfile, \
    GoogleProfile
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, \
    CustomerTaxInformation, CUSTOMER_CATEGORY_NON_ENTERPRISE, PartnerTaxInformation
from blowhorn.customer.views import CustomerProfile, OrderCreate
from blowhorn.common import sms_templates
from blowhorn.common.communication import Communication
from blowhorn.address.models import UserAddress, City, State, Slots, get_closed_hour
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME, \
    GTA
from blowhorn.users.serializers import UserAddressSerializer
from blowhorn.address.mixins import AddressMixin
from blowhorn.common.notification import Notification
from django.contrib.auth import logout
from blowhorn.vehicle.models import CityVehicleClassAlias
from blowhorn.apps.driver_app.v1.serializers.driver import DriverLoginSerializer
from blowhorn.utils.functions import ist_to_utc, get_base_url, utc_to_ist
from blowhorn.apps.customer_app.v1.serializers.driver import \
    DriverRatingAggregateSerializer
from blowhorn.address.utils import within_city_working_hours, \
    get_city_from_latlong, get_city_from_geopoint, get_city
from blowhorn.order.utils import OrderUtil
from blowhorn.apps.customer_app.common import Slot
from fcm_django.models import FCMDevice
from blowhorn.address.helpers.location_helper import LocationHelper
from blowhorn.driver.mixins import DriverMixin
from blowhorn.driver.serializers import AvailableDriverSerializer
from blowhorn.customer.utils import get_vehicle_class_availability
from blowhorn.customer.constants import CUSTOMER_APP_TIME_FORMAT, PICKUP_DATETIME_FORMAT
from blowhorn.order.const import PAYMENT_STATUS_UNPAID, PAYMENT_MODE_PAYTM, PAYMENT_STATUS_PAID
from blowhorn.order.tasks import send_mail, push_customer_feedback_to_slack, initiate_paytm_refund
from blowhorn.utils.datetime_function import DateTime
from blowhorn.users.utils import UserUtils
from blowhorn.payment.mixins import PaymentMixin
from blowhorn.oscar.core.loading import get_model

from config.settings.status_pipelines import ORDER_DELIVERED

User = get_model('users', 'User')
Customer = get_model('customer', 'Customer')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
FlashSaleBanner = get_model('order', 'FlashSaleBanner')
ShippingAddress = get_model('order', 'ShippingAddress')
Event = get_model('order', 'Event')
OrderBatch = get_model('order', 'OrderBatch')
WayPoint = get_model('order', 'WayPoint')
Stop = get_model('trip', 'Stop')
Hub = get_model('address', 'Hub')
DriverActivity = get_model('driver', 'DriverActivity')
Driver = get_model('driver', 'Driver')
DriverRatingAggregate = get_model('driver', 'DriverRatingAggregate')
DriverRatingDetails = get_model('driver', 'DriverRatingDetails')
Country = get_model('address', 'Country')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([])
class CustomerLogin(generics.CreateAPIView, CustomerMixin):
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)
    """
    Customer / User Authentication to be done here and the response with the
    user token to be provided.

    """

    def post(self, request):
        data = request.POST.dict()
        email = data.get("email").strip()
        password = data.get("password").strip()
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        # app sends False as string so setting default as string
        is_firebase_user_login = data.get("is_firebase_user_login")

        login_response = User.objects.do_login(
            request=request, email=email, password=password)

        if login_response.status_code == status.HTTP_200_OK:
            user = login_response.data.get('user')
            if not self.get_customer(user):
                return Response(status=status.HTTP_401_UNAUTHORIZED, data='Users are not allowed to login')
            token = login_response.data.get('token')
            response = {
                'status': True,
                'message': {
                    'name': user.name,
                    'email': user.email,
                    'mAuthToken': token,
                },
                "profile": CustomerProfileInformation().get_profile_response(
                    user, latitude, longitude, is_firebase_user_login)
            }
            return Response(status=status.HTTP_200_OK, data=response, content_type='text/html; charset=utf-8')

        response = {
            'text': login_response.data,
        }
        return Response(status=login_response.status_code, data=response, content_type='text/html; charset=utf-8')


class FacebookLogin(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        data = request.data
        accessToken = data.get("access_token")
        isMobile = request.META.get('HTTP_MOBILE')
        mobile_app = True if isMobile else False
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        is_firebase_user_login = data.get('is_firebase_user_login')

        sp = FacebookProfile()
        user_info = sp.get(token=accessToken)
        return SocialLogin().set_response(request, user_info, latitude,
                                          longitude, is_firebase_user_login,
                                          mobile_app)

    def get_serializer_class(self):
        pass


@permission_classes([AllowAny])
class SocialLogin(generics.CreateAPIView):

    def set_response(self, request, user_info, latitude, longitude,
                     is_firebase_user_login, mobile=False):
        user = None
        if not user_info:
            response_status = False
            message = {
                'why': 'account_unavailable'
            }
        else:
            email = user_info.get('email')
            name = user_info.get('name')
            if not (email and name):
                response_status = False
                message = {
                    'why': 'account_unavailable',
                    'name': name,
                    'email': email,
                }
            else:
                user = User._default_manager.filter(email=email.lower()).first()
                if not user:
                    extra_fields = {
                        'is_email_verified': True
                    }
                    customer_response = Customer.objects.create_customer(
                        email=email,
                        name=name,
                        is_active=True,
                        **extra_fields
                    )

                    if customer_response.status_code != status.HTTP_200_OK:
                        return Response(status=customer_response.status_code,
                                        data=customer_response.data,
                                        content_type='text/html; charset=utf-8')

                    user = customer_response.data.get('user')

                login_response = User.objects.do_login(
                    request=request, user=user)

                if login_response.status_code != status.HTTP_200_OK:
                    response_status = False
                    message = login_response.data
                else:
                    response_status = True
                    message = {
                        'is_mobile_verified': user.is_mobile_verified,
                        'is_email_verified': user.is_email_verified,
                        'name': name,
                        'email': email,
                    }
                    mAuthToken = login_response.data.get('token')
                    message['mAuthToken'] = mAuthToken

        response_dict = {
            'status': response_status,
            'message': message,
        }
        if user:
            profile = CustomerProfileInformation().get_profile_response(
                user, latitude, longitude, is_firebase_user_login)
            response_dict.update({'profile': profile})

        return Response(status=status.HTTP_200_OK, data=response_dict,
                        content_type='text/html; charset=utf-8')


class GoogleLogin(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        data = request.data
        accessToken = data.get("access_token")
        isMobile = request.META.get('HTTP_MOBILE')
        mobile_app = True if isMobile else False
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        is_firebase_user_login = data.get('is_firebase_user_login')

        sp = GoogleProfile()
        user_info = sp.get(token=accessToken)
        return SocialLogin().set_response(request, user_info, latitude,
                                          longitude, is_firebase_user_login,
                                          mobile_app)

    def get_serializer_class(self):
        pass


@permission_classes([])
class CustomerSignup(generics.CreateAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def post(self, request):
        data = request.data.dict()
        email = data.get('email', None)
        password = data.get('password', None)
        is_business_user = data.get('is_business_user', None)

        if is_business_user == 'true':
            is_business_user = True
        else:
            is_business_user = False

        # is_business_user = False
        legal_name = data.get('legal_name', None)
        name = data.get('name', None)
        gstin = data.get('gstin', None)

        response = {
            'status': False,
            'message': {
                'text': '',
            }
        }

        if not email or not password:
            response['message'] = 'email and password required'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        email = email.lower()
        if not re.compile(settings.EMAIL_REGEX).match(email):
            response['message'] = 'Please, Provide a valid email'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        if UserUtils().is_staff_email(email):
            response['message'] = 'Company email is not allowed for signup'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        if User.objects.filter(email=email).exists():
            response['message'] = 'User with this email already exists.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        if is_business_user and Customer.objects.filter(legalname=legal_name).exists():
            response['message'] = 'User with legalname already exists.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        tax_info = CustomerTaxInformation.objects.filter(gstin=gstin).first()
        if tax_info:
            response['message'] = 'Customer with GSTIN exist'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        with transaction.atomic():
            customer_response = Customer.objects.create_customer(
                email=email.lower(),
                password=password,
                name=data.get('name'),
                is_active=True
            )

            if customer_response.status_code != status.HTTP_200_OK:
                return Response(status=customer_response.status_code,
                                data=customer_response.data,
                                content_type='text/html; charset=utf-8')

            user = customer_response.data.get('user')
            customer = self.get_customer(user)
            if is_business_user and gstin and \
                CustomerProfile().has_tax_info_edit_permission(user, customer):
                success, message = CustomerProfile()._update_tax_information(customer,
                                                                             name, is_business_user,
                                                                             legal_name, gstin, None)

                if not success:
                    _out = {
                        'status': False,
                        'message': message
                    }
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=_out)
            login_response = User.objects.do_login(request=request, user=user)

            if login_response.status_code != status.HTTP_200_OK:
                return Response(status=login_response.status_code,
                                data=login_response.data,
                                content_type='text/html; charset=utf-8')

            token = login_response.data.get('token')
        response['status'] = True
        response['message'] = {
            'name': user.name,
            'email': user.email,
            'mAuthToken': token,
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


@permission_classes([])
class PasswordReset(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        """ Used by the users to reset the password
            API Call: POST /passwordforget
        """
        status_message = ''
        message = ''
        data = request.data
        if not isinstance(data, dict):
            data = request.data.dict()

        user_email = data.get('email')
        if user_email:
            try:
                user = User.objects.get(email=user_email)
            except User.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

            url_protocol = 'https'  # request.META.get('wsgi.url_scheme', 'http')
            url = ''.join([url_protocol, '://',
                           request.META.get('HTTP_HOST', 'blowhorn.com'),
                           '/accounts/password/reset/'])

            client = requests.session()
            client.get(url)
            if 'csrftoken' in client.cookies:
                csrftoken = client.cookies['csrftoken']
                data = dict(email=user_email,
                            csrfmiddlewaretoken=csrftoken)
                client.post(url, data=data, headers=dict(Referer=url))
                status_message = True
                message = 'SUCCESS'

        else:
            status_message = False
            message = 'User e-mail cannot be blank'

        response_dict = {
            'status': status_message,
            'message': message,
        }
        return Response(status=status.HTTP_200_OK, data=response_dict,
                        content_type='text/html; charset=utf-8')


class UpdateMobile(generics.CreateAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)

    def post(self, request):
        data = request.data.dict()
        mobile = data.get('mobile')
        debug = True if data.get('debug') == 'true' else False
        device_type = data.get('device_type', '')

        if not mobile:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid Mobile Number')

        user = request.user
        # Update the number only when the old number is not the same as the new
        # number
        phone_number = phonenumbers.parse(
            mobile, settings.COUNTRY_CODE_A2)

        if user.phone_number != phone_number:
            user.phone_number = phone_number
            user.is_mobile_verified = False
            user.save()

        otp, created = User.objects.retrieve_otp(user)
        if device_type == settings.USER_CLIENT_TYPE_ANDROID:
            template_dict = sms_templates.android_template_otp
        else:
            template_dict = sms_templates.template_otp

        message = template_dict['text'] % otp
        sms_to = phonenumbers.format_number(phone_number,
                                            phonenumbers.PhoneNumberFormat.E164)
        Communication.send_sms(sms_to=sms_to, message=message, priority='high',
                                template_dict=template_dict)
        return Response(status=status.HTTP_200_OK)


class CustomerMobileVerify(generics.CreateAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)
    def post(self, request):
        """
            Used by customer app to verify the OTP sent to it
            API Call: POST /mobileverify
        """
        data = request.data.dict()
        user_otp = data.get("otp")
        latitude = data.get("latitude")
        longitude = data.get("longitude")
        is_firebase_user_login = data.get("is_firebase_user_login")
        user = request.user

        otp, created = User.objects.retrieve_otp(user)
        if str(user_otp) != otp:
            return Response(status=status.HTTP_402_PAYMENT_REQUIRED, data='OTP Mismatch')

        user.is_mobile_verified = True
        user.is_active = True
        user.save()
        response = CustomerProfileInformation().get_profile_response(
            user, latitude, longitude, is_firebase_user_login)
        return Response(status=status.HTTP_200_OK, data=response)


class UpdateProfile(generics.CreateAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        customer = self.get_customer(user)
        data = request.POST.dict()

        gstin_ = data.get('gstin', None)
        name = data.get('name', None)
        legal_name = data.get('legal_name', None)
        response = {}

        if gstin_ and Customer.objects.filter(legalname=legal_name).exclude(id=customer.id).exists():
            response['message'] = 'User with legalname already exists.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')
        if gstin_:
            state = State.objects.get(gst_code=gstin_[0:2])
            tax_info = CustomerTaxInformation.objects.filter(gstin=gstin_).exclude(customer=customer)
            if tax_info.exists():
                response['message'] = 'Customer with GSTIN exist'
                return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

            data = {
                'customer_id': customer.pk,
                'gstin': gstin_,
                'state': state
            }
            try:
                tx_info = CustomerTaxInformation.objects.get(
                    state=state, customer=customer)
                tax_serializer = CustomerTaxInformationSerializer(
                    tx_info, data=data)
            except:
                tax_serializer = CustomerTaxInformationSerializer(data=data)

            if tax_serializer.is_valid():
                try:
                    tax_serializer.save()
                except BaseException as be:
                    logger.warning('App - Error saving GSTIN: %s' % be)
                    response['message'] = 'Failed to save GSTIN information'
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=response)

        if name:
            customer.name = name
            user.name = name
            user.save()

        if legal_name:
            customer.legalname = legal_name
            customer.customer_category = CUSTOMER_CATEGORY_NON_ENTERPRISE
            customer.service_tax_category = GTA

        if name or legal_name:
            customer.save()

        return Response(status=status.HTTP_200_OK)


@permission_classes([])
class SaveUserLocation(generics.CreateAPIView, generics.UpdateAPIView):
    serializer_class = UserAddressSerializer

    def get(self, request):
        saved_locations = AddressMixin().get_saved_user_addresses(request.user, True)
        return Response(status=status.HTTP_200_OK, data=saved_locations)

    def _get_address(self, query_params):
        params = reduce(operator.and_, query_params) if query_params else None
        print("para         ", params)
        try:
            return UserAddress.objects.get(params)
        except UserAddress.DoesNotExist:
            return False

    def validate_data(self, data, request):
        # api for new customer app removed variable mapping

        parsed_data = {}
        country = Country.objects.filter(iso_3166_1_a2=settings.COUNTRY_CODE_A2).get()

        geopoint = CommonHelper.get_geopoint_from_latlong(data.get('latitude'), data.get('longitude'))

        if data.get('contact_mobile') and not data.get(
            'contact_mobile').startswith(settings.ACTIVE_COUNTRY_CODE):
            phone_number = settings.ACTIVE_COUNTRY_CODE + data.get('contact_mobile')
        else:
            phone_number = data.get('contact_mobile', '')

        parsed_data['identifier'] = data.get('name', '')
        parsed_data['country'] = country
        parsed_data['geopoint'] = geopoint
        parsed_data['user'] = request.user
        parsed_data['phone_number'] = phone_number
        parsed_data['first_name'] = data.get('contact_name', '')
        parsed_data['line1'] = data.get('line1')
        parsed_data['line3'] = data.get('line3')
        parsed_data['line4'] = data.get('line4')

        return parsed_data

    def post(self, request):
        request.POST._mutable = True
        data = request.data.dict()

        user = request.user
        identifier = data.get('name', '')

        if not identifier:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Please, Provide a name for address.')

        # Check for the existing address of user
        identifier = identifier.strip()
        context = {
            'user': user,
            'request': data
        }

        _data = self.validate_data(data, request)

        ua = UserAddress(**_data)
        hash = ua.generate_hash()

        user_address = self._get_address([Q(user=user), Q(identifier=identifier), Q(hash=hash)])
        if user_address:
            # Update address only if address is changed
            if user_address.hash != hash:
                serializer = self.serializer_class(user_address, data=data, context=context)
            else:
                message = 'Nothing to update'
                logger.info(message)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        try:
            user_address, created = UserAddress.objects.get_or_create(**_data)
            # UserAddress.objects.filter(user=user, identifier=identifier).update(**_data)
        except:
            pass

        saved_locations = AddressMixin().get_saved_user_addresses(user, True)
        logger.info('User address successfully saved: %s' % user)

        return Response(status=status.HTTP_200_OK, data=saved_locations)


class CustomerLogout(generics.CreateAPIView):

    def post(self, request):
        try:
            data = request.data.dict()
        except BaseException as be:
            data = {}
        # Remove the FCM Key on logout for the driver device
        fcm_id = data.get('fcm_id')
        if fcm_id:
            Notification.remove_fcm_key_for_user(
                user=request.user,
                registration_id=fcm_id
            )
        logout(request)
        return Response(status=status.HTTP_200_OK)


class CustomerProfileInformation(generics.RetrieveUpdateAPIView, CustomerMixin):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)
    serializer_class = CustomerProfileSerializer

    def _get_latlng_list(self, coordinates):
        latlon_list = []
        for latlong_arr in coordinates:
            for latlong in latlong_arr:
                latlon_list.append({
                    "latitude": latlong[1],
                    "longitude": latlong[0],
                })
        return latlon_list

    def _get_coverage_area(self):
        coverage = []
        contracts = Contract.objects.get_distinct_cities_from_active_c2c_sme_contracts()
        for contract in contracts:
            city = contract.city
            pickup_coordinates = city.coverage_pickup.coords
            dropoff_coordinates = city.coverage_dropoff.coords
            coverage.append({
                "city": city.name,
                "pickup": self._get_latlng_list(pickup_coordinates),
                "dropoff": self._get_latlng_list(dropoff_coordinates),
            })

        return coverage

    def get_profile_response(self, user, latitude, longitude, is_firebase_user_login):

        saved_locations = AddressMixin().get_saved_user_addresses(user, new_app=True)
        recent_locations = AddressMixin().get_recent_user_addresses(user, new_app=True)
        geopoint = CommonHelper.get_geopoint_from_latlong(
            latitude, longitude)
        customer = self.get_customer(user)

        city = None
        if geopoint:
            city = get_city_from_geopoint(geopoint, field='coverage_pickup')
        if not city:
            city = City.objects.first()
        customer_cancellation_reasons = []
        for key in OrderUtil().fetch_order_cancellation_reasons():
            _reason_dict = {
                'key': key,
                'value': key
            }
            customer_cancellation_reasons.append(_reason_dict)
        cityno = str(city.contact)
        gstin = CustomerTaxInformation.objects.filter(customer=customer).values_list('gstin', flat=True).first()

        profile = {
            "name": user.name,
            "email": user.email,
            "mobile": str(user.phone_number),
            "is_mobile_verified": user.is_mobile_verified,
            "is_email_verified": user.is_email_verified,
            "item_category": sorted(ItemCategory.objects.values('category', 'id'), key=lambda x: x['category']),
            "saved_locations": saved_locations,
            "recent_locations": recent_locations,
            "time_slot_details": Slot().get_time_slots_for_city(city),
            "coverage": self._get_coverage_area(),
            'call_support': cityno,
            'call_support_promotion': cityno,
            'share_sub_text': settings.C2C_CONSTANTS['APP_SHARE_SUB_TEXT'],
            'share_body_text': settings.C2C_CONSTANTS['APP_SHARE_BODY_TEXT'],
            'customer_cancellation_reasons': customer_cancellation_reasons,
            "is_business_user": True if customer and customer.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL else False,
            "legal_name": customer.legalname if customer and customer.legalname else '',
            "gstin": gstin if gstin else '',
            "customer_id": customer.id,
            "wallet_available_balance": customer.get_customer_balance() if customer else 0,
            "notification_title": DriverConstants().get("NOTIFICATION_TITLE", ""),
            "notification_url": DriverConstants().get("NOTIFICATION_URL", ""),
            "open_in_app": False if DriverConstants().get("NOTIFICATION_URL_OPEN_IN_APP", "") == "False" else True
        }

        flashsale_banners = FlashSaleBanner.objects.filter(is_active=True, city=city).only('banner')
        profile.update({"flash_sale_banner": [x.banner.url for x in flashsale_banners if x.banner and x.banner.url]})

        if is_firebase_user_login == 'false':
            firebase_auth_token = CommonHelper.generate_firebase_token(
                settings.FIREBASE_KEYS['customer'])
            profile.update({"firebase_auth_token": firebase_auth_token})

        return profile

    def get(self, request):
        """
        Customer Profile api
        :returns response for app
        """
        data = request.GET.dict()
        latitude = data.get('latitude')
        longitude = data.get('longitude')
        is_firebase_user_login = data.get("is_firebase_user_login")
        customer = self.get_customer(request.user)
        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        profile = self.get_profile_response(request.user, latitude, longitude, is_firebase_user_login)

        response = {
            'status': True,
            'message': profile
        }

        return Response(status=status.HTTP_200_OK, data=response, content_type='text/html; charset=utf-8')


class BookingSlots(generics.ListAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)

    def get(self, request):
        data = request.GET.dict()
        latitude = data.get('latitude')
        longitude = data.get('longitude')
        city = get_city_from_latlong(latitude, longitude,
                                     field='coverage_pickup')

        if not city:
            city = City.objects.first()

        slots = Slot().get_time_slots_for_city(city)
        return Response(status=status.HTTP_200_OK, data=slots)


class ProductSlots(generics.ListAPIView):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)
    def get(self, request):
        data = request.GET.dict()
        latitude = data.get('latitude')
        longitude = data.get('longitude')
        product_id = data.get('product_id', None)
        city = get_city_from_latlong(latitude, longitude,
                                     field='coverage_pickup')

        if not city:
            city = City.objects.first()

        num_days_future = city.future_booking_days
        time_func = DateTime()
        today = time_func.get_locale_datetime(None)
        max_date_to_show = time_func.add_timedelta(
            today, {'days': num_days_future})
        closed_datetime_list = get_closed_hour(city, max_date_to_show.date())

        response = []
        _dict = {}
        slots = []
        if product_id:
            slots = Slots.objects.filter(customer__isnull=True, city=city, product_id=product_id)
        if not slots:
            slots = Slots.objects.filter(customer__isnull=True, city=city)

        for i in range(num_days_future):
            buffer_days = OrderConstants().get('BUFFER_DAYS_FOR_DELIVERY', 2)
            date = time_func.add_timedelta(today, {'days': i + buffer_days})
            _dict = {
                'date': date.date()
            }
            slot_time = []
            for slot in slots:
                start = slot.start_time
                end = slot.end_time
                start_datetime = date.replace(hour=start.hour, minute=start.minute, tzinfo=None)
                end_datetime = date.replace(hour=end.hour, minute=end.minute, tzinfo=None)

                overlapping = False
                for dt, ch_start, ch_end in closed_datetime_list:
                    ch_start_datetime = datetime(dt.year, dt.month, dt.day, ch_start.hour, ch_start.minute)
                    ch_end_datetime = datetime(dt.year, dt.month, dt.day, ch_end.hour, ch_end.minute)
                    if PaymentHelper().get_overlap_time(start_datetime, end_datetime, ch_start_datetime,
                                                        ch_end_datetime):
                        overlapping = True
                        break
                if not overlapping:
                    slot_time.append({
                        'from': slot.start_time.strftime(CUSTOMER_APP_TIME_FORMAT),
                        'to': slot.end_time.strftime(CUSTOMER_APP_TIME_FORMAT)

                    })
            _dict.update({"slot_time": slot_time})
            response.append(_dict)

        return Response(status=status.HTTP_200_OK, data=response)


class OrderDetails(generics.CreateAPIView):
    def get(self, request, order_id):
        order = Order.objects.filter(id=order_id)
        order = order.prefetch_related('event_set').first()
        if order:
            order_info = OrderCustomerSerializer(order).data
            return Response(status=status.HTTP_200_OK, data=order_info)
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order')


class B2CInvoice(generics.RetrieveAPIView):

    def post(self, request):
        data = request.data
        order_id = data.get('order_id')
        order = Order.objects.filter(id=order_id).first()
        if order:
            if not order.status == ORDER_DELIVERED:
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Invoice will be available, once order is delivered')

            company_office = Office.objects.first()
            tax_info = None
            partner = None
            line_data = []
            total_item_price = 0
            total_qty = 0
            state = AddressUtil().get_state_from_city(order.city)
            for line in order.order_lines.all():
                if not tax_info:
                    tax_info = PartnerTaxInformation.objects.filter(partner=line.partner, state=state).first()
                if not tax_info:
                    tax_info = PartnerTaxInformation.objects.filter(partner=line.partner).first()
                if not partner:
                    partner = line.partner
                line_data.append({
                    "product": line.product,
                    "qty": line.quantity,
                    "amt": line.each_item_price,
                    "total": line.total_item_price
                })
                total_qty += int(line.quantity)
                total_item_price += float(line.total_item_price)
            delivery_charge = float(order.total_payable) - total_item_price

            if delivery_charge:
                total_qty += 1

            context = {
                "logo": settings.APPS_DIR.path("static") + "img/logo.png",
                "invoice_number": order.invoice_number,
                "billing_address": order.shipping_address,
                "shipping_address": order.shipping_address,
                "date_placed": order.date_placed.date(),
                "order_number": order.number,
                "invoice_date": order.expected_delivery_time.date(),
                "total_item": order.order_lines.count(),
                "line": line_data,
                "total_item_price": total_item_price,
                "total_qty": total_qty,
                "delivery_charge": delivery_charge,
                "total_payable": order.total_payable,
                "company": {
                    "name": partner.name if partner else "",
                    "address": tax_info.invoice_address if tax_info else "",
                    "gstin": tax_info.gstin if tax_info else "",
                    "state": tax_info.state.name if tax_info and tax_info.state else '',
                    "cin": company_office.company.cin,
                    "pan": company_office.company.pan,
                    "website" : settings.HOST
                }
            }
            invoice_template = "pdf/invoices/b2c/invoice.html"
            footer = {"template": "pdf/invoices/b2b_v2/footer.html", "context": context}
            header = {"template": "pdf/invoices/b2c/header.html", "context": context}
            byte = generate_pdf(invoice_template, context, footer=footer, header=header)
            file_name = str(order.invoice_number) + ".pdf" if order.invoice_number else str(order.id) + ".pdf"

            subject = 'Blowhorn Receipt for Booking %s' % (order.number)
            email_template_name = 'emailtemplates_v2/b2c_booking.html'
            context = {
                "CUSTOMER_NAME" : str(order.customer),
                "ORDER_NUMBER" : str(order.number),
                "BRAND_NAME" : settings.BRAND_NAME,
            }
            html_content = loader.render_to_string(email_template_name, context)
            mail = EmailMultiAlternatives(
                subject,
                html_content,
                to=[request.user.email],
                from_email=settings.B2C_DEFAULT_FROM_EMAIL)
            mail.attach_alternative(html_content, "text/html")
            mail.attach(file_name, byte, 'application/pdf')
            mail.send()
            return Response(status=status.HTTP_200_OK, data='Email sent sucessfully',
                            content_type='text/html; charset=utf-8')
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid order reference')


class CustomerFCM(generics.CreateAPIView, CustomerMixin):

    def post(self, request):
        user = request.user
        customer = self.get_customer(user)
        if not customer:
            logger.info('Customer not found for user %s' % user)
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='You are not a customer')

        data = request.data.dict()
        # device_type = 'android'
        registration_id = data.get('registration-id')
        device_type = data.get('device_type', 'android')

        user_device = FCMDevice.objects.filter(user=user, active=True)
        if user_device.exists():
            if user_device.count() == 1:
                Notification.update_fcm_key_for_user(user_device.first(),
                                                     registration_id, '',
                                                     device_type)
        else:
            Notification.add_fcm_key_for_user(request.user, registration_id,
                                              '', device_type.lower())

        response = {
            'status': True,
            'message': 'Registration id successfully saved'
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


class RatePackages(APIView, CustomerMixin):
    """
    Description     : API to get vehicle sell rate packages.
    Request type    : GET
    Params          : (lat, lng) or city
    Response        : Available Vehicle packages for given city / location.
    """

    # permission_classes = (AllowAny,)
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)

    def _get_city(self):
        params = self.request.GET
        city = params.get('city', None)
        if city:
            city = City.objects.get(id=city)
        else:
            latitude, longitude = params.get('latitude', None), params.get(
                'longitude', None)
            if latitude and longitude:
                city = get_city_from_latlong(latitude, longitude,
                                             field='coverage_pickup')
                # Passing Bangalore as default city if lat-lng didn't match
                if not city:
                    return City.objects.first()

        # Defaulting to all cities if no city was matched.
        if not city:
            return list(City.objects.all())
        return city

    def _get_contract_types(self, customer=None):
        # if customer and customer.is_SME():
        return [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]
        # else:
        #     return [CONTRACT_TYPE_C2C]

    def get(self, request):
        params = self.request.GET
        show_house_shifting_contract = params.get('show_house_shifting_contract', '')
        show_house_shifting_contract = True if show_house_shifting_contract == 'true' else False
        customer = self.get_customer(request.user)
        city = self._get_city()
        cities = city if isinstance(city, list) else [city]
        contract_types = self._get_contract_types(customer)
        response = {'data': '', 'errors': ''}
        if city and contract_types:
            response['data'] = ContractUtils.get_sell_packages(
                cities=cities, contract_types=contract_types, customer=customer,
                show_house_shifting_contract=show_house_shifting_contract)
            status_code = status.HTTP_200_OK
        else:
            response['errors'] = 'Insufficient arguments.'
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(response, status_code)


class AvailableVehicles(APIView):
    """
    Description     : API to get vehicles which are available to serve
    Request type    : GET
    Params          : (lat, lng)
    Response        : Available Vehicles in a specified radius (for now 100 km)
    """

    # permission_classes = (AllowAny,)
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)

    def _get_location(self):
        params = self.request.GET
        latitude, longitude = params.get('latitude', None), params.get('longitude', None)
        if latitude and longitude:
            return LocationHelper.get_geopoint_from_latlong(latitude, longitude)

        return False

    def _get_available_vehicles(self, location):
        # driver_available = DriverActivity.objects.filter(
        #     driver__status=StatusPipeline.ACTIVE,
        #     driver__vehicles__isnull=False,
        #     event=DRIVER_ACTIVITY_IDLE, geopoint__distance_lte=(
        #         location, AVAILABLE_DRIVER_SEARCH_RADIUS_IN_METERS
        #     )
        # ).select_related('driver').prefetch_related('driver__vehicles')

        city = None
        if location:
            city = get_city_from_geopoint(location, field='coverage_pickup')
        if not city:
            city = City.objects.first()
        driver_available = DriverMixin().get_nearest_available_driver(location, city_id=city.id,
                                                                      vehicle_class_preference=VehicleClass.objects.all())

        driver_available = driver_available.select_related('driver').prefetch_related('driver__vehicles')

        available_vehicle = []
        if driver_available:
            driver_available_serializer = AvailableDriverSerializer(
                driver_available, many=True)
            available_vehicle = get_vehicle_class_availability(
                driver_available_serializer.data)

        return available_vehicle

    def get(self, request):
        response = {'data': '', 'errors': ''}
        location = self._get_location()
        if not location:
            response['errors'] = 'Location geopoint missing'
            status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)

        city = get_city_from_geopoint(location, field='coverage_pickup')
        if not city:
            response['errors'] = 'Area not covered'
            status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)

        available_vehicle = self._get_available_vehicles(location)

        if available_vehicle:
            response['data'] = available_vehicle
            status_code = status.HTTP_200_OK
        else:
            status_code = status.HTTP_400_BAD_REQUEST
        return Response(response, status_code)


class getEstimatedFare(APIView):
    # permission_classes = (AllowAny,)
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified,)

    def get_labour(self, data):
        labour_cost = 0
        labour_option = data.get('labour_option', '')
        if labour_option:
            return labour_option.get('cost_per_labour') * labour_option.get('num_of_labours')
        return labour_cost

    def get_houseshifting_information(self, address):
        is_lift_accessible = address.get('is_lift_accessible')
        is_service_lift_available = address.get('is_service_lift_available')
        floor_number = address.get('floor_number')
        return floor_number, is_lift_accessible, is_service_lift_available

    def post(self, request):

        data = request.data
        device_type = data.get('device_type', None)
        user = request.user
        order = Order()
        if data.get('pickup_address') and data.get('dropoff_address'):
            pickup_address = ShippingAddress()
            dropoff_address = ShippingAddress()
            pickup_address.floor_number, pickup_address.is_lift_accessible, pickup_address.is_service_lift_available = \
                self.get_houseshifting_information(data.get('pickup_address'))
            dropoff_address.floor_number, dropoff_address.is_lift_accessible, dropoff_address.is_service_lift_available = \
                self.get_houseshifting_information(data.get('dropoff_address'))
            order.pickup_address = pickup_address
            order.shipping_address = dropoff_address
        order.customer = CreateOrder().get_customer(user)
        contract_id = data.get('contract_id')

        item_category = data.get('item_category')
        self.item_category = ItemCategory.objects.filter(id__in=item_category)
        contract = Contract.objects.filter(id=contract_id)
        contract = contract.select_related('city').first()
        order.tip = data.get('tipAmount', 0)
        order.customer_contract = contract
        order.estimated_distance_km = data.get('distanceInKM', 0.0)
        order.estimated_time_in_mins = data.get('timeInMins', 0.0)

        timeframe = data.get('timeframe', None)
        now_or_later = timeframe.get('now_or_later', None)
        later_value = timeframe.get('later_value', '')
        order.pickup_datetime = CreateOrder()._clean_pickup_time(now_or_later, later_value)
        order.city = order.customer_contract.city
        coupon_code = data.get('coupon_code', '')
        coupon = None
        if coupon_code:
            coupon = CouponMixin().get_coupon_from_code(coupon_code)
        order.coupon = coupon
        order.is_pod_required = data.get('is_pod_required', False)

        labour_charge = self.get_labour(data)
        order.calculate_and_update_cost(labour_charge=labour_charge)
        total_payable = order.total_payable
        total_payable += labour_charge
        fare_breakup = []
        if device_type in ['iOS', 'Android']:
            fare_breakup = OrderUtil().get_booking_details_for_customer(None, order)['fare_breakup_details']
            fare_breakup.append({'name': 'Labour Charges', 'value': labour_charge})
        response = {
            'total_payable': total_payable,
            'fare_breakup': fare_breakup,
            'discount': order.discount or 0,
            'before_discount': total_payable + (order.discount or 0)
        }

        return Response(status=200, data=response)


class getCoupon(APIView, CouponMixin):
    """
    Validation of given coupon code
    POST api/coupon/apply
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        is_valid, data = self.validate_coupon_data(request.user, data, True)
        if not is_valid:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=data)

        return Response(status=status.HTTP_200_OK, data=data)


class CreateOrder(generics.CreateAPIView, CustomerMixin):
    permission_classes = (IsActive, IsAuthenticated, IsMobileVerified, IsCustomer,)
    # permission_classes = (IsCustomer,)

    serializer_class = OrderSerializer

    def auto_debit_paytm(self, order):
        from blowhorn.payment.views import PayTmWithdrawal
        from django.http import HttpRequest

        withdraw_data = {
            'number': order.number,
            'amount': order.total_payable,
            'aut_debit': True
        }
        withdraw_request = HttpRequest()
        withdraw_request.method = 'POST'
        withdraw_request.data = json.dumps(withdraw_data)
        PayTmWithdrawal.as_view()(withdraw_request)

    def create(self, request):
        context = {'request': request}
        data = request.data

        # Store UTM params
        utm_data = data.pop('utm_data', None)

        user = request.user
        data['customer'] = self.get_customer(user)

        item_category = data.get('item_category')
        request.POST._mutable = True

        data_parsed = self._parse_params(data)
        if not data_parsed:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_message)

        if data_parsed.get('payment_mode') == BLOWHORN_WALLET:
            cust_balance = data['customer'].get_customer_balance()
            if cust_balance < data_parsed.get('estimated_cost'):
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='Insufficent Funds')

        address_pickup = CustomerShippingAddressSerializer(
            data=data_parsed.get('pickup_address', {}))
        address_drop_off = CustomerShippingAddressSerializer(
            data=data_parsed.get('shipping_address', {}))
        data['shipping_address'] = data_parsed.get('shipping_address', None)
        way_point_serializer = WaypointSerializer(
            data=data_parsed.get('waypoints_address', []), many=True)

        with transaction.atomic():
            # sid = transaction.savepoint()

            if address_pickup.is_valid(raise_exception=True) and address_drop_off.is_valid(raise_exception=True) and \
                way_point_serializer.is_valid(raise_exception=True):
                order_serializer = OrderSerializer(
                    context=context, data=data_parsed)
                order = order_serializer.save()

                for item in item_category:
                    order.item_category.add(item)
                order.save()
                if data_parsed.get('payment_mode') == BLOWHORN_WALLET:
                    debit_status = order.customer.put_order_amount_on_hold(order)
                    if not debit_status:
                        from blowhorn.order.const import PAYMENT_MODE_CASH
                        # if not enough funds change the payment mode
                        order.payment_mode = PAYMENT_MODE_CASH
                way_point_serializer.save(order=order)
                # from blowhorn.order.const import PAYMENT_STATUS_PAID, PAYMENT_MODE_CASH, PAYMENT_MODE_PAYTM
                # if order.payment_mode == PAYMENT_MODE_PAYTM and order.status != PAYMENT_STATUS_PAID:
                #     self.auto_debit_paytm(order)
                #     order = Order.objects.get(id=order.id)
                # if order.payment_status == PAYMENT_STATUS_PAID or order.payment_mode == PAYMENT_MODE_CASH:

                OrderCreate().post_save(order)
                background_color = '#13A893'
                dict_response = {
                    "status": True,
                    "message": {
                        'booking_id': order.number,
                        'friendly_id': order.number,
                        'tracking_url': 'http://%s/track/%s' % (settings.HOST, order.number),
                        'booking_type': 'advanced',
                        'failure_title': 'Thank You!',
                        'failure_message': 'Sorry! Unable to take your booking',
                        'success_title': 'Congratulations!',
                        'success_message': 'We have received your booking and will contact you shortly',
                        'background_color': background_color,
                        'buttons': {
                            'done': True,
                            'call_support': False,
                        },
                        "wallet_available_balance": order.customer.get_customer_balance()
                    }
                }
                if utm_data:
                    utm_data['order'] = order
                    utm_instance = OrderSerializer().create_utm_record(utm_data)

                return Response(dict_response, status=status.HTTP_201_CREATED)
                # else:
                #     transaction.savepoint_rollback(sid)

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Payment failed Try again')

    def _parse_params(self, order_data):
        self.error_message = ''
        pickup = order_data.get('pickup_address')
        droppoff = order_data.get('dropoff_address')
        timeframe = order_data.get('timeframe', None)
        payment_mode = order_data.get('selectedPaymentMode', None)
        customer = order_data.get('customer')
        shipping_address = self.clean_shipping_address(droppoff)
        if not timeframe:
            self.error_message = 'Something went wrong!'
            return None

        now_or_later = timeframe.get('now_or_later', None)
        later_value = timeframe.get('later_value', '')
        pickup_datetime = self._clean_pickup_time(now_or_later, later_value)

        city = get_city(
            order_type='',
            geopoint=shipping_address.get('geopoint'),
            postcode=shipping_address.get('postcode')
        )
        order_data['city'] = city

        credit_amount_eligibility, pending_amount = customer.get_credit_amount_eligibility()
        credit_period_eligibility = customer.get_credit_days_eligibility()

        is_customer_eligible = credit_amount_eligibility and credit_period_eligibility
        if not is_customer_eligible:
            self.error_message = 'Please complete payment for previous trips to proceed with booking.'
            # from blowhorn.customer.tasks import send_credit_limit_slack_notification
            # if credit_amount_eligibility:
            #     pending_amount = 0

            # send_credit_limit_slack_notification.delay(customer.id, pending_amount,
            #                                            credit_period_eligibility)
            return None

        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=pickup_datetime,
            lat=pickup['geopoint'].get('latitude', 0),
            lon=pickup['geopoint'].get('longitude', 0),
            city=city
        )
        if not is_valid_booking_time:
            self.error_message = 'Service is unavailable at this time. Please schedule for later.'
            return None

        vehicle_class_preference = VehicleClass.objects.filter(
            contract__id=order_data.get('contract_id')).first()

        return {
            'pickup_address': self.clean_shipping_address(pickup),
            'shipping_address': shipping_address,
            # 'items': order_data.get('items', None),
            'coupon_code': order_data.get('coupon_code', None),
            'tip': order_data.get('tipAmount', 0),
            'customer_contract': order_data.get('contract_id', None),
            'vehicle_class_preference': vehicle_class_preference,
            'pickup_datetime': pickup_datetime,
            'device_info': order_data.get('device_type', 'iOS'),
            'status': StatusPipeline.ORDER_NEW,
            'waypoints': self._clean_waypoints(order_data.get('waypoints', [])),
            'order_type': settings.C2C_ORDER_TYPE,
            'labour_info': self._clean_labour(order_data.get('labour_option', '')),
            'customer': order_data.get('customer', None),
            'device_type': order_data.get('device_type', ''),
            'booking_type': now_or_later,
            'auto_debit_paytm': order_data.get('isAuto_debit_paytm', False),
            'estimated_cost': order_data.get('estimatePriceLow', 0.0),
            'estimated_distance_km': order_data.get('distanceInKM', 0.0),
            'payment_mode': payment_mode,
            'estimated_time_in_mins': order_data.get('timeInMins', 0.0),
            'estimated_cost_lower': order_data.get('estimatePriceLow', 0.0),
            'estimated_cost_upper': order_data.get('estimatePriceHigh', 0.0),
            'eta_in_mins': order_data.get('etaValue', ''),
            'is_pod_required': order_data.get('is_pod_required', False),
            'return_order': order_data.get('return_order', False)
        }

    def parse_phone_number(self, phone_number):
        if phone_number and not phone_number.startswith(
            settings.ACTIVE_COUNTRY_CODE):
            return settings.ACTIVE_COUNTRY_CODE + phone_number
            # return phonenumbers.parse(
            #         phone_number,
            #         settings.DEFAULT_COUNTRY.get('country_code')
            #     )
        return None

    # Clean methods
    def clean_shipping_address(self, address):

        # old app response commented
        # phone_no = address['address'].get('contact', {}).get('mobile', None)
        # contact_name = address['address'].get('contact', {}).get('name', '')
        # # if not phone_no:
        # phone_no = address['address'].get('contact', {}).get('phone_number', '')

        contact_name = address.get('contact', {}).get('name', '')
        phone_no = address.get('contact', {}).get('phone_number', '')
        if not phone_no:
            phone_no = address.get('contact', {}).get('mobile', '')
        is_lift_available = address.get('is_service_lift_available', '')
        is_lift_accessible = address.get('is_lift_accessible', '')
        is_service_lift_available = True if is_lift_available in ['true', True] else False
        is_lift_accessible = True if is_lift_accessible in ['true', True] else False
        floor_number = address.get('floor_number', 0)

        parsed_number = self.parse_phone_number(phone_no)

        return {
            "title": "",
            "first_name": contact_name,
            "last_name": "",
            "line1": address['address'].get('line1', ''),
            "line2": '',
            "line3": address['address'].get('line3', ''),
            "line4": address['address'].get('line4', ''),
            "postcode": address['address'].get('postcode', None),
            "is_service_lift_available": is_service_lift_available,
            "floor_number": floor_number,
            "is_lift_accessible": is_lift_accessible,
            "country": address['address'].get(
                'country') or settings.COUNTRY_CODE_A2,
            "phone_number": parsed_number if parsed_number else None,
            "geopoint": CommonHelper.get_geopoint_from_latlong(
                address['geopoint'].get('latitude', 0),
                address['geopoint'].get('longitude', 0)
            )
        }

    def _clean_pickup_time(self, now_or_later, later_value=None):
        if now_or_later == 'now':
            pickup_time = timezone.now()
        else:
            pickup_time = parse(later_value)
        return pickup_time

    def _clean_waypoints(self, waypoints):
        waypoints_cleaned = []
        for i, waypoint in enumerate(waypoints):
            contact = waypoint.get('contact')
            address = waypoint.get('address')
            geopoint = waypoint.get('geopoint')

            phone_no = contact.get('mobile', None) if contact else None
            if not phone_no:
                phone_no = contact.get('phone_number', '') if contact else ''

            waypoints_cleaned.append({
                "title": "",
                "first_name": contact.get('name', '') if contact else '',
                "last_name": "",
                "line1": address.get('line1', '') if address else '',
                "line2": '',
                "line3": address.get('line3', '') if address else '',
                "line4": address.get('line4', '') if address else '',
                "postcode": address.get('postal_code', None) if address else '',
                "phone_number": phone_no,
                "geopoint": CommonHelper.get_geopoint_from_latlong(geopoint.get('latitude', 0),
                                                                   geopoint.get('longitude', 0)),
                'sequence_id': i + 1
            })

        return waypoints_cleaned

    def _clean_labour(self, labour):
        count = 0
        cost = 0
        if isinstance(labour, str):
            if labour in ['light', 'heavy']:
                labour_cost = OrderConstants().get('LABOUR_COST', 400)
                LABOUR_COST_MAPPING = {
                    'light': labour_cost,
                    'medium': labour_cost,
                    'heavy': labour_cost,
                }
                count = 1
                cost = LABOUR_COST_MAPPING.get(labour, 400)

        elif isinstance(labour, dict):
            cost = labour.get('cost_per_labour', 0)
            count = labour.get('num_of_labours', 0)

        return {
            'no_of_labours': count,
            'labour_cost': cost
        }


class RescheduleBookingView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        data = request.data
        if not isinstance(data, dict):
            data = data.dict()

        order_number = data.get('order_number', None)
        reschedule_details = data.get('reschedule_details')
        if isinstance(reschedule_details, str):
            reschedule_details = json.loads(reschedule_details)
        timeframe = reschedule_details.get('now_or_later')
        selected_datetime = reschedule_details.get('later_value')

        order_qs = Order.objects.filter(number=order_number).select_related('customer')
        if not order_qs.exists():
            return Response(status=400, data='Order not found')

        order = order_qs.first()
        error = None
        if order.driver_id:
            error = 'Can\'t cancel the order. Driver is already assigned.'

        elif order.status in [StatusPipeline.ORDER_CANCELLED]:
            error = 'Can\'t reschedule cancelled order'

        if error:
            return Response(status=400, data=error)

        pickup_datetime = CreateOrder()._clean_pickup_time(
            timeframe, later_value=selected_datetime)
        customer = order.customer
        pickup_geopoint = order.pickup_address.geopoint
        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=pickup_datetime,
            lat=pickup_geopoint.y,
            lon=pickup_geopoint.x
        )
        if not is_valid_booking_time:
            message = 'Service is unavailable at this time. ' \
                      'Please schedule for later.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        try:
            with transaction.atomic():
                # Converting to LOCAL timezone to store it as remarks
                old_pickup_datetime = DateTime().get_locale_datetime(
                    order.pickup_datetime)
                order_qs.update(pickup_datetime=pickup_datetime)
                remarks = '%s (%s -> %s)' % (
                    StatusPipeline.RESCHEDULE_DELIVERY,
                    old_pickup_datetime.strftime(PICKUP_DATETIME_FORMAT),
                    selected_datetime
                )
                event = Event.objects.create(
                    status=order.status,
                    order=order,
                    remarks=remarks
                )
                event.created_by = order.user
                event.save()
                OrderCreate().post_save(order)
                return Response(
                    status=200,
                    data='Rescheduled successfully'
                )
        except:
            return Response(status=400, data='Failed to reschedule')


class CustomerBookingStatus(generics.RetrieveAPIView):

    def post(self, request):
        data = request.data.dict()

        order = Order.objects.get(number=data.get('order_number'))

        if order and order.status == StatusPipeline.ORDER_NEW and \
            'cancel_booking' == data.get('booking_action', ''):
            reason = data.get('cancellation_reason')
            other_reason = data.get('other_cancellation_reason')

            remarks = reason + '  ' + other_reason + 'App'

            Order.objects.filter(id=order.id).update(status=StatusPipeline.ORDER_CANCELLED,
                                                     reason_for_cancellation=reason,
                                                     other_cancellation_reason=other_reason)

            event = Event.objects.create(
                status=StatusPipeline.ORDER_CANCELLED,
                order=order,
                remarks=remarks,
                created_by=request.user
            )

            pickup_datetime = datetime.utcfromtimestamp(
                order.pickup_datetime.timestamp())
            pickup_datetime_ist = utc_to_ist(pickup_datetime)

            # Send Whatsapp notification
            msg_template_data = {"template_name": "booking_cancel",
                                 "params_data": [order.customer.name,
                                                 order.number,
                                                 pickup_datetime_ist.strftime(
                                                     '%H:%M'),
                                                 pickup_datetime_ist.strftime(
                                                     '%d %b')]}
            send_whatsapp_notification([settings.ACTIVE_COUNTRY_CODE + str(
                order.customer.user.phone_number.national_number)],
                                       msg_template_data, order)

            if order.payment_mode == PAYMENT_MODE_PAYTM and \
                order.payment_status == PAYMENT_STATUS_PAID and order.amount_paid_online > 0 \
                and not order.is_refunded:
                initiate_paytm_refund.apply_async(
                    (order.id,), )
            if order.payment_mode == BLOWHORN_WALLET:
                order.customer.reverse_order_amount(order)
            # # @todo save is not saving user so saving it again
            # event.created_by = request.user
            # event.save()

            message = 'Booking Cancelled'
            response_dict = {
                'status': 200,
                'data': {},
                'message': message
            }
            return Response(status=status.HTTP_200_OK, data=response_dict,
                            content_type="application/json")

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Cannot cancel the order.Please contact Support',
                        content_type="application/json")


class CustomerMyTrips(generics.RetrieveAPIView, CustomerMixin):

    def get(self, request, when=None):
        """
        returns the current, past and upcoming order based on request
        """
        data = request.GET.dict()
        cursor = data.get('cursor') if data.get('cursor') else 1

        user = User.objects.get(email=request.user)
        customer = self.get_customer(user)

        query_params = Q(order_type=settings.C2C_ORDER_TYPE, customer=customer)

        if when == StatusPipeline.APP_ORDER_STATUS_PAST:
            query = Q(status=StatusPipeline.ORDER_DELIVERED)
        if when == StatusPipeline.APP_ORDER_STATUS_NEW:
            ordering = 'pickup_datetime'
            query = Q(status=StatusPipeline.ORDER_NEW) | Q(status=StatusPipeline.DRIVER_ACCEPTED,
                                                           trip__status=StatusPipeline.TRIP_NEW)
        elif when == StatusPipeline.APP_ORDER_STATUS_CURRENT:
            ordering = '-pickup_datetime'
            query = ~Q(status__in=[StatusPipeline.ORDER_DELIVERED,
                                   StatusPipeline.ORDER_NEW,
                                   StatusPipeline.ORDER_CANCELLED,
                                   StatusPipeline.ORDER_EXPIRED]
                       )
            query = query & ~Q(status=StatusPipeline.DRIVER_ACCEPTED, trip__status=StatusPipeline.TRIP_NEW)
        query_params = query_params & query if query else query_params

        order_qs = Order.objects.filter(query_params)

        if when == StatusPipeline.APP_ORDER_STATUS_PAST:
            order_qs = order_qs.annotate(
                payment_status_int=Case(
                    When(payment_status=PAYMENT_STATUS_UNPAID, then=Value('0')),
                    default=Value('1'),
                    output_field=IntegerField()
                )
            ).order_by('payment_status_int', '-pickup_datetime')
        else:
            order_qs = order_qs.order_by(ordering)

        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        order = order_qs
        paginator = Paginator(order, 10)

        if int(cursor) <= paginator.num_pages:

            order_page = paginator.page(cursor)

            # @todo remove app parameter and use same dates and time format
            # in both app and website
            results_json = OrderSerializer().get_orders(
                request=request,
                results=order_page.object_list,
                app=True,
                is_utc_time_format=True
            )

            current = results_json['current']
            past = results_json['past']
            upcoming = results_json['upcoming']
        else:
            current = []
            past = []
            upcoming = []

        cursor = int(cursor) + 1
        dict_ = {
            'current': current,
            'past': past,
            'upcoming': upcoming,
            'more': '',
            'cursor': str(cursor),
        }

        return Response(status=status.HTTP_200_OK, data=dict_, content_type='text/html; charset=utf-8')


class ProductList(generics.ListAPIView):

    def get(self, request):
        data = request.GET.dict()
        latitude = data.get('latitude')
        longitude = data.get('longitude')
        city = get_city_from_latlong(latitude, longitude,
                                     field='coverage_pickup')
        if not city:
            city = City.objects.first()

        flash_sale = FlashSaleBanner.objects.filter(is_active=True, city=city)
        flash_sale = flash_sale.prefetch_related('product')
        flash_sale = flash_sale.prefetch_related('product__stockrecords', "product__stockrecords__partner",
                                                 "product__attribute_values")
        flash_sale_data = []
        item_count = Line.objects.filter(basket__owner=request.user).count()

        for banner in flash_sale:
            products = banner.product.all()
            for product in products:
                attributes = []
                stocks = product.stockrecords.all()
                for attr in product.attribute_values.all():
                    attributes.append({
                        'key': attr.attribute.name,
                        'value': attr.value_text
                    })

                for stock in stocks:
                    selling_price = float(stock.price_excl_tax) if stock.price_excl_tax else 0
                    if selling_price:
                        _data = {
                            "image_url": [x.original.url for x in product.images.all()],
                            "desc": product.description,
                            "delivery_charge": float(stock.delivery_charge) if stock.delivery_charge else 0,
                            "product_id": product.id,
                            "rating": product.rating,
                            "title": product.title,
                            "max_qty": stock.max_qty,
                            "mrp_price": float(stock.price_retail) if stock.price_retail else 0,
                            "price": selling_price,
                            "uom": stock.uom if stock.uom else '',
                            "pack_of": stock.pack_of if stock.pack_of else 1,
                            "item_category": product.title,
                            "brand": str(stock.partner),
                            "stockrecord_id": stock.id,
                            "attributes": attributes
                        }
                        flash_sale_data.append(_data)

        response = {
            "api_key": settings.FLASH_SALE_API_KEY,
            "flash_sale_data": flash_sale_data,
            "cart_count": item_count,
            "amount_for_free_delivery": OrderConstants().get("AMOUNT_FOR_FREE_DELIVERY", 0)  # 0 means not applicable

        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type="application/json")


class InProgressTrip(generics.CreateAPIView, CustomerMixin):

    def _get_ratingwise_driver_remarks(self):
        return DriverRatingAggregateSerializer(
            DriverRatingAggregate.objects.all(), many=True).data

    def _get_order_without_rating(self, customer):
        order = Order.objects.filter(
            customer=customer,
            status=StatusPipeline.ORDER_DELIVERED,
            order_type=settings.C2C_ORDER_TYPE,
            driver__isnull=False,
        ).select_related('vehicle_class_preference').last()
        if not order:
            return {}
        if DriverRatingDetails.objects.filter(order=order).exists():
            return {}

        driver = order.driver
        vehicle_class = order.vehicle_class_preference
        vehicle_class_name = vehicle_class.commercial_classification
        vehicle_class_alias = CityVehicleClassAlias.objects.filter(
            city_id=order.city_id, vehicle_class=vehicle_class).first()
        if vehicle_class_alias:
            vehicle_class_name = vehicle_class_alias.alias_name

        return {
            'driver': DriverLoginSerializer(driver, fields=["id", "name",
                                                            "photo"]).data,
            'vehicle': {
                'rc_number': driver.driver_vehicle if driver else '',
                'alias_name': vehicle_class_name,
                'image_url': vehicle_class.image_selected.url,
                'app_icon': vehicle_class.app_icon
            },
            'order': {
                'number': order.number,
                'pickup_address_area': order.pickup_address.line3,
                'dropoff_address_area': order.shipping_address.line3,
                'stops_count': order.waypoint_set.all().count() - 1,
                'id': order.id,
                'completed_at': utc_to_ist(order.updated_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
            }
        }

    def get(self, request):
        customer = self.get_customer(request.user)
        trip = Trip.objects.filter(
            order__customer=customer, order__order_type=settings.C2C_ORDER_TYPE,
            status__in=[StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_ALL_STOPS_DONE]).select_related('order')
        trip = sorted(trip, key=lambda x: x.planned_start_time, reverse=True)
        trip = trip[0] if len(trip) else None

        order_without_rating = self._get_order_without_rating(customer)
        response = {
            'order_without_rating': order_without_rating,
            "driver_remarks_info": self._get_ratingwise_driver_remarks(),
            "inprogress_order": str(trip.order.number) if trip else '',
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type="application/json")


class LiveTracking(generics.CreateAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request, order_number):
        """
        api for live tracking
        """
        data = request.GET.dict()
        is_firebase_user_login = data.get("is_firebase_user_login")

        order_qs = Order.objects.filter(number=order_number,
                                        order_type=settings.C2C_ORDER_TYPE)
        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        # @todo remove app parameter and use same dates and time format
        # in both app and website
        o_json = OrderUtil().get_booking_details_for_customer(
            request=request,
            order=order_qs[0],
            route_info=True,
            app=True
        )

        if o_json and is_firebase_user_login == 'false':
            firebase_auth_token = CommonHelper.generate_firebase_token(
                settings.FIREBASE_KEYS['customer'])
            o_json.update({"firebase_auth_token": firebase_auth_token})

        return Response(status=status.HTTP_200_OK, data=o_json,
                        content_type="application/json")


class UnPaidOrder(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsCustomer,)

    def get(self, request):
        customer = self.get_customer(request.user)
        response_data = ''
        status_code = status.HTTP_200_OK
        try:
            response_data = customer.get_unpaid_orders()
        except:
            response_data = 'Something went wrong'
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(status=status_code, data=response_data)


class CustomerFeedback(generics.CreateAPIView, CustomerMixin):

    def _set_field_details(self, title, val, short='false', link=None):
        return {
            'title': title,
            'value': '%s' % val if not link else '<%s|%s>' % (link, val),
            'short': short
        }

    def _get_slack_message(self, request, customer, user, feedback_text):
        author_name = '%s | %s' % (customer.name, user.phone_number)
        base_url = get_base_url(request)
        title_link = ''.join([base_url, reverse('admin:customer_customer_change', args=[customer.id])])

        title = '%s' % user.email
        recent_order = Order.objects.get_recent_booking_of_customer(customer)
        fields = []
        if recent_order:
            order_number = recent_order.get('number', '')
            order_url = ''.join([base_url, reverse('admin:order_bookingorder_changelist'),
                                 '?q=', order_number])
            fields.extend([
                self._set_field_details('Recent Order', order_number, short='true', link=order_url),
                self._set_field_details('City', recent_order.get('city__name', ''), short='true')
            ])
        fields.append(self._set_field_details('Message', feedback_text))

        slack_message = {
            "attachments": [{
                "author_name": author_name,
                "title": title,
                "title_link": title_link,
                "thumb_url": settings.THUMBNAIL_LINK,
                "color": "#52c0d2",
                "fields": fields
            }]
        }
        return slack_message

    def post(self, request):
        data = request.data.dict()
        feedback_text = data.pop('feedback', '')
        user = request.user

        customer = self.get_customer(user)
        if not customer:
            logger.info('Feedback API: Customer not exists for %s' % user)
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        feedback_data = {
            'feedback': feedback_text,
            'customer': customer.id,
            'extra_details': json.dumps(data)
        }

        serializers = CustomerFeedbackSerializer(data=feedback_data)
        if serializers.is_valid():
            serializers.save()
            success_msg = True
            message = 'Success'
            slack_message = self._get_slack_message(request, customer, user,
                                                    feedback_text)
            push_customer_feedback_to_slack.apply_async((slack_message,), )
            response_dict = {
                'status': success_msg,
                'message': message
            }
            return Response(status=status.HTTP_200_OK, data=response_dict,
                            content_type='text/html; charset=utf-8')
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        content_type='text/html; charset=utf-8')


class DriverRating(generics.CreateAPIView):
    """
    Save Driver Rating given by customer.

    request data: order_id, rating, remarks(list of ids), customer_remark
    response: success-> 200 with success message
              failure-> 500 if internal error with error message
                        400 if wrong request data sent with proper error message
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """Upsert driver rating and add driver rating details."""
        error_message = ''
        response_data = 'Driver rating saved successfully!'
        status_code = status.HTTP_200_OK

        data = request.POST.dict()
        order_id = data.get('order_id')
        query = Q(pk=order_id)
        order = Order.objects.prefetch_queryset(
            query=query, select_related=['driver']
        ).only('pk', 'driver').first()
        if not order:
            status_code = status.HTTP_400_BAD_REQUEST
            error_message = 'Invalid Order!'
        rating = data.get('rating')
        remarks = json.loads(data.get('remarks'))
        customer_remark = data.get('customer_remark', '')

        if not error_message:
            driver = order.driver
            rating_details_data = {
                'order': order,
                'rating': rating,
                'driver': driver,
                'customer_remark': customer_remark,
                'user': request.user
            }
            try:
                upsert_driver_rating(driver=driver, rating=rating)
                rating_details_list = []
                for remark_id in remarks:
                    rating_details_data['remark_id'] = remark_id
                    rating_details_list.append(DriverRatingDetails(**rating_details_data))
                DriverRatingDetails.objects.bulk_create(rating_details_list)
            except:
                logger.error(traceback.format_exc())
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                error_message = 'Something went wrong!'

        return Response(status=status_code, data=error_message or response_data)


class ProductRatingView(generics.CreateAPIView):
    def post(self, request):
        data = request.data
        order_id = data.get('order_id')
        product_id = data.get('product_id')
        rating = data.get('rating')
        title = data.get('title')
        body = data.get('remarks')
        user = request.user

        line = OrderLine.objects.filter(order_id=order_id, product_id=product_id).first()
        if line:
            dict_ = {
                'user': user,
                'line': line,
                'product_id': product_id
            }
            product_rating = ProductRating.objects.filter(**dict_).first()
            dict_.update({
                'rating': rating,
                'body': body,
                "title": title,
                'status': 1
            })

            if product_rating:
                ProductRating.objects.filter(id=product_rating.id).update(**dict_)
            else:
                ProductRating.objects.create(**dict_)
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data="Invalid product")


class SendInvoice(generics.RetrieveAPIView):

    def post(self, request, order_number):
        order = Order.objects.get(number=order_number)
        send_mail.apply_async((order.id,), )
        return Response(status=status.HTTP_200_OK,
                        data="Invoice sent successfully")


@permission_classes([])
class Razorpay(generics.CreateAPIView):

    def post(self, request):
        data = request.data.dict()
        return PaymentRazorpay.as_view()(request._request, data)


class CustomerAccountBalance(APIView, CustomerMixin):

    def get(self, request):
        customer = self.get_customer(request.user)

        response = {
            "available_balance": customer.get_customer_balance() if customer else 0,
            "transaction_info": customer.get_last_transactions() if customer else []
        }

        return Response(status=status.HTTP_200_OK, data=response)


class AddBalanceRazorPay(APIView, CustomerMixin):

    def post(self, request):
        data = request.data
        customer = self.get_customer(request.user)
        order_number = data.get('booking_key')
        txn_id = data.get('txn_id') or data.get('txnid')
        amount = data.get('amount')
        payment_response = Payment().capture(
            payment_id=txn_id,
            amount=amount
        )

        if not payment_response:
            return status.HTTP_400_BAD_REQUEST, 'Failed to capture response'

        # capturing the response from razorpay
        amount = payment_response.get('amount', 0) / 100

        extra_args = {
            'amount_debited': float(amount),
            'reference': txn_id
        }
        payment_source = PaymentMixin().update_source(
            order=None,
            order_number=order_number,
            payment_type=settings.DEFAULT_PAYMENT_GATEWAY,
            **extra_args
        )
        is_payment_captured = payment_response.get('captured', False)

        if is_payment_captured:
            customer.add_balance_to_wallet(
                payment_source=payment_source,
                payment_response=payment_response,
                payment_type='razorpay'
            )

        res = {
            'status': is_payment_captured,
            'merchantTxnId': txn_id,
            'amount': amount,
            "available_balance": customer.get_customer_balance(),
            "transaction_info": customer.get_last_transactions()
        }

        return Response(status=status.HTTP_200_OK, data=res)


class PayViaBWallet(APIView, CustomerMixin):
    queryset = Customer.objects.all()

    def post(self, request):
        data = request.data
        number = data.get('number')
        order = Order.objects.get(number=number)
        customer = self.get_customer(request.user)

        with transaction.atomic():
            order = customer.debit_order_amount(order)
            if order.status == StatusPipeline.ORDER_PENDING:
                order.status = StatusPipeline.ORDER_NEW
            order.save()

            if order.payment_status == PAYMENT_STATUS_PAID:
                res = {
                    'status': True,
                    "available_balance": customer.get_customer_balance()
                }
                return Response(status=status.HTTP_200_OK, data=res)

        res = {
            'status': False,
            "available_balance": customer.get_customer_balance(),
        }

        return Response(status=status.HTTP_200_OK, data=res)


class AvailableCouponView(APIView, CustomerMixin):
    queryset = Customer.objects.all()

    def get(self, request):
        """
        POST <base_url>/customers/<version>/availablecoupons/
        :param request:
            data:
                city (str)
                time_frame (str)
                date (only if time_frame is `later`) (str: 21 Jan 2020 10:37)
                device_type (str)
        :return: valid active coupons
            {
                'data': [{
                    'code': 'FIRST50',
                    'description': 'Available to first time user.',
                    'terms_and_conditions': [
                        'Valid once per user.',
                        ...
                    ]
                },
                ...
            }
        """
        data = request.GET.dict()
        customer = self.get_customer(request.user)
        _status, data = CouponMixin().get_active_coupons(data)
        if not _status:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=data)

        coupon_serializer = CouponModelSerializer(
            data,
            many=True,
            context={
                'fields': ('code', 'message', 'terms_and_conditions')
            }
        )
        return Response(
            status=status.HTTP_200_OK,
            data={
                'data': coupon_serializer.data,
                'error_message': 'There is no coupon available now, Try after some time!'
            }
        )


class MyOrder(generics.RetrieveAPIView, CustomerMixin):
    def get(self, request):
        data = request.GET.dict()
        customer = self.get_customer(request.user)
        orders = Order.objects.filter(customer=customer, order_type=settings.SHIPMENT_ORDER_TYPE,
                                      order_lines__product__isnull=False).order_by('-pickup_datetime').distinct()
        orders = orders.select_related('shipping_address')
        orders = orders.prefetch_related('order_lines', 'event_set', 'order_lines__product',
                                         'order_lines__product__stockrecords',
                                         'order_lines__product__images')

        page_to_be_displayed = data.get('next') if data.get(
            'next', '') else PAGE_TO_BE_DISPLAYED
        number_of_entry = NUMBER_OF_ENTRY_IN_PAGE

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=orders, num=number_of_entry,
            page=page_to_be_displayed,
            serializer_method=OrderListSerializer
        )

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)
