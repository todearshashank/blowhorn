# from django.conf.urls import url
# from django.apps import AppConfig
# from blowhorn.coupon.views import Coupon
# from .views import Razorpay
# from . import views
#
# class CustomerVersionThree(AppConfig):
#
#     def get_urls(self):
#         urlpatterns = [
#             # User APIS
#             url(r'^login/$', views.CustomerLogin.as_view(),
#                 name='v2-customer-login'),
#             url(r'^fblogin$', views.FacebookLogin.as_view(),
#                 name='v2-fb-login'),
#             url(r'^googlelogin$', views.GoogleLogin.as_view(),
#                 name='v2-google-login'),
#             url(r'^signup/$', views.CustomerSignup.as_view(),
#                 name='v2-customer-signup'),
#             url(r'^passwordreset/', views.PasswordReset.as_view(),
#                 name='v2-password-reset'),
#             url(r'^updatemobilenum/$', views.UpdateMobile.as_view(),
#                 name='v2-update-mobile'),
#             url(r'^mobileverify/$', views.CustomerMobileVerify.as_view(),
#                 name='v2-mobile-verify'),
#             url(r'^updateprofile/', views.UpdateProfile.as_view(),
#                 name='v2-update-profile'),
#             url(r'^saveuserlocation/$', views.SaveUserLocation.as_view(),
#                 name='v2-save-location'),
#             url(r'^logout/$', views.CustomerLogout.as_view(),
#                 name='v2-customer-logout'),
#
#             # Profile & Needed APIS
#             url(r'^profile/$', views.CustomerProfileInformation.as_view(),
#                 name='v2-customer-profile'),
#             url(r'^slots/$', views.BookingSlots.as_view(),
#                 name='v2-booking-slots'),
#             url(r'^fcmid/$', views.CustomerFCM.as_view(),
#                 name='v2-customer-updatefcmid'),
#
#             # List/ Create Order
#             url(r'^rate-packages/$', views.RatePackages.as_view(),
#                 name='v2-rate-packages-list'),
#             url(r'^available-vehicles/$', views.AvailableVehicles.as_view(),
#                 name='v2-available-vehicles-list'),
#             url(r'^getfare$', views.getEstimatedFare.as_view(),
#                 name='v4-rate-driver'),
#             url(r'^coupon/', views.getCoupon.as_view(), name='v4-coupon'),
#             url(r'^availablecoupons/', views.AvailableCouponView.as_view(),
#                 name='v4-available-coupons'),
#
#             url(r'^createorder/$', views.CreateOrder.as_view(),
#                 name='v4-create-order'),
#             url(r'^reschedule/$', views.RescheduleBookingView.as_view(),
#                 name='v2-reschedule-booking'),
#
#             # Trips APIS
#             url(r'^bookingstatus/$', views.CustomerBookingStatus.as_view(),
#                 name='v2-customer-bookingstatus'),
#             url(r'^orders/unpaid$', views.UnPaidOrder.as_view(),
#                 name='v2-unpaid-orders'),
#             url(r'^orders/(\S+)$', views.CustomerMyTrips.as_view(), name='my-trips'),
#             # Common APIS
#             url(r'^feedback/$', views.CustomerFeedback.as_view(),
#                 name='v2-customer-feedback'),
#             url(r'^drivers/rating', views.DriverRating.as_view(),
#                 name='v2-rate-driver'),
#             url(r'invoice/(\S+)$', views.SendInvoice.as_view(),
#                 name='v2-send-invoice'),
#             url(r'^razorpay$', Razorpay.as_view(), name='old-customer-razorpay'),
#             url(r'^account/transaction', views.CustomerAccountBalance.as_view(), name='account-balance'),
#             url(r'^account/razorpay/status-check$', views.AddBalanceRazorPay.as_view(), name='raz-account-balance'),
#             url(r'^order/bwallet/withdraw$', views.PayViaBWallet.as_view(), name='wallet_withdraw'),
#             url(r'^order/(\S+)$', views.LiveTracking.as_view(),
#                 name='v2-live-tracking'),
#         ]
#
#         return self.post_process_urls(urlpatterns)
