from django.conf.urls import url
from . import views
from blowhorn.coupon.views import Coupon

urlpatterns = [
            # List all rate packages (vehicle pricing).
            url(r'^rate-packages/$', views.RatePackages.as_view(), name='rate-packages-list'),
            url(r'^available-vehicles/$', views.AvailableVehicles.as_view(), name='available-vehicles-list'),

            # List/ Create Order
            url(r'^createorder/$', views.CreateOrder.as_view(), name='create-order'),
            url(r'^profile/$', views.CustomerProfileInformation.as_view(), name='customer-profile'),
            url(r'^slots/$', views.BookingSlots.as_view(), name='booking-slots'),
            url(r'^login/$', views.CustomerLogin.as_view(), name='customer-login'),
            url(r'^googlelogin$', views.GoogleLogin.as_view(), name='google-login'),
            url(r'^fblogin$', views.FacebookLogin.as_view(), name='fb-login'),
            url(r'^mobileverify/$', views.CustomerMobileVerify.as_view(), name='mobile-verify'),
            url(r'^saveuserlocation/$', views.SaveUserLocation.as_view(), name='save-location'),
            url(r'^orders/unpaid$', views.UnPaidOrder.as_view(), name='unpaid-orders'),
            url(r'^updatemobilenum/$', views.UpdateMobile.as_view(), name='update-mobile'),
            url(r'^signup/$', views.CustomerSignup.as_view(), name='customer-signup'),
            url(r'^fcmid/$', views.CustomerFCM.as_view(), name='customer-updatefcmid'),
            url(r'^feedback/$', views.CustomerFeedback.as_view(), name='customer-feedback'),
            url(r'^passwordreset/', views.PasswordReset.as_view(), name='password-reset'),
            url(r'^logout/$', views.CustomerLogout.as_view(), name='customer-logout'),
            url(r'^bookingstatus/$', views.CustomerBookingStatus.as_view(), name='customer-bookingstatus'),
            url(r'^quickbook$', views.QuickBook.as_view(), name='quickbook'),
            url(r'^updateprofile/', views.UpdateProfile.as_view(), name='update-profile'),
            url(r'^orders/(\S+)$', views.CustomerMyTrips.as_view(), name='my-trips'),
            url(r'^order/(\S+)$', views.LiveTracking.as_view(), name='live-tracking'),
            url(r'invoice/(\S+)$', views.SendInvoice.as_view(), name='send-invoice'),
            url(r'^coupon/', Coupon.as_view(), name='v1-coupon'),

            # driver rating
            url(r'^drivers/rating', views.DriverRating.as_view(), name='rate-driver'),
        ]

# class CustomerVersionOne(AppConfig):
#
#     def get_urls(self):
#
#
#         return self.post_process_urls(urlpatterns)
