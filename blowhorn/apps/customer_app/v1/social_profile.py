import json
import logging
from urllib import request
from django.conf import settings

from google.oauth2 import id_token
from google.auth.transport import requests


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

GOOGLE_ACCOUNT_ISSUER = ['accounts.google.com', 'https://accounts.google.com']
FACEBOOK_VERIFICATION_URL = 'https://graph.facebook.com/me?fields=name,email&access_token='


class FacebookProfile(object):
    """
    Verifies Facebook auth token and returns decoded profile details
    """

    def _make_request(self, token):
        try:
            response = request.urlopen('%s%s' % (FACEBOOK_VERIFICATION_URL, token)).read().decode('UTF-8')
            return json.loads(response)
        except BaseException as be:
            logger.info('Failed to verify Facebook token. %s' % be)
            return None

    def get(self, token=None):
        response = self._make_request(token)
        name = response.get('name', None)
        email = response.get('email', None)
        result = {
            'name': name,
            'email': email,
        }
        return result


class GoogleProfile(object):
    """
    Verifies Google auth token and returns decoded profile details
    """

    def get(self, token=None, allowed_domains=None):
        logging.info('Checking Google token: %s' % token)
        if token:
            try:
                request = requests.Request()
                idinfo = id_token.verify_token(token, request)
                if not allowed_domains:
                    aud = idinfo.get('aud', None)
                    if aud not in [
                        settings.GOOGLE_CLIENT_ID,
                        settings.IOS_GOOGLE_CLIENT_ID,
                        settings.GOOGLE_CLIENT_ID_COSMOS
                    ]:
                        logger.info('Unrecognized client. %s' % aud)
                        raise Exception('Unrecognized client')

                    iss = idinfo.get('iss', None)
                    if iss not in GOOGLE_ACCOUNT_ISSUER:
                        logger.info('Wrong issuer. %s' % iss)
                        raise Exception('Wrong issuer')

                domain = idinfo.get('hd', None)
                if allowed_domains and domain not in allowed_domains:
                    logger.info('Incorrect domain: %s' % domain)
                    raise Exception('Incorrect domain')

                return {
                    'name': idinfo.get('name', None),
                    'email': idinfo.get('email', None)
                }

            except BaseException as be:
                logging.info('Invalid token. %s' % be)

        return None
