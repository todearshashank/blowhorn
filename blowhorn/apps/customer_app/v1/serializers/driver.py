# Django and System libraries

# 3rd party libraries
from rest_framework import serializers
from blowhorn.oscar.core.loading import get_model

# blowhorn imports

DriverRating = get_model('driver', 'DriverRating')
DriverRatingDetails = get_model('driver', 'DriverRatingDetails')
RatingCategory = get_model('driver', 'RatingCategory')
DriverRatingAggregate = get_model('driver', 'DriverRatingAggregate')


class DriverRatingSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        rating = DriverRating(**validated_data)
        rating.save()
        return rating

    def update(self, instance, validated_data):
        for attr in validated_data:
            setattr(instance, attr, validated_data.get(attr))

        return instance

    class Meta:
        model = DriverRating
        fields = '__all__'


class DriverRatingDetailsSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        rating_details = DriverRatingDetails(**validated_data)
        rating_details.save()
        return rating_details

    class Meta:
        model = DriverRatingDetails
        fields = '__all__'


class RatingCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = RatingCategory
        fields = ('id', 'remark')


class DriverRatingAggregateSerializer(serializers.ModelSerializer):
    remarks = RatingCategorySerializer(many=True, read_only=True)
    title = serializers.CharField(source='rating.title')
    rating = serializers.IntegerField(source='rating.rating')

    class Meta:
        model = DriverRatingAggregate
        fields = ('rating', 'title', 'remarks')
