# blowhorn/apps/customer_app/v1/test_api.py

from django.urls import reverse_lazy
from rest_framework import status

# Why is city called in customer/profile ??
def test_get_profile(api_client, test_customer, test_city):
    response = api_client.get(reverse_lazy("customer-profile"))
    assert response.status_code == status.HTTP_200_OK
    data = response.data
    assert isinstance(data, dict)
#    assert data["email"] == test_customer.user.email

def test_login_ok(api_client):
    request_data = {
    }
    response = api_client.post("customer-login", data=request_data)
#    assert response.status_code == status.HTTP_201_CREATED
