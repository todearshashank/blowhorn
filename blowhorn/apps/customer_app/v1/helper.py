"""driver related Helper Functions to be added here."""
# System and Django libraries
import logging

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model

# Blowhorn libraries
from blowhorn.apps.customer_app.v1.serializers.driver import (
    DriverRatingSerializer, DriverRatingDetailsSerializer
)
from blowhorn.apps.mixins import CreateModelMixin


Driver = get_model('driver', 'Driver')
DriverRating = get_model('driver', 'DriverRating')
DriverRatingDetails = get_model('driver', 'DriverRatingDetails')


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def _create_driver_rating(rating, driver):
    data = {
        'num_of_feedbacks': 1,
        'cumulative_rating': rating,
    }
    driver_rating = CreateModelMixin().create(data=data, serializer_class=DriverRatingSerializer)
    if not driver_rating:
        logger.error('Driver Rating creation Failed for driver:- "%s"', driver)
        return False

    Driver.objects.filter(pk=driver.id).update(driver_rating=driver_rating)
    logger.info('driver Rating Successfully created for driver:- "%s"', driver)
    return True


def _update_driver_rating(driver_rating, rating):
    num_of_feedbacks = driver_rating.num_of_feedbacks
    cumulative_rating = driver_rating.cumulative_rating

    new_cumulative_rating = (float(cumulative_rating) * num_of_feedbacks + rating) / (num_of_feedbacks + 1)
    num_of_feedbacks = num_of_feedbacks + 1
    cumulative_rating = round(new_cumulative_rating, 2)

    DriverRating.objects.filter(pk=driver_rating.id).update(
        num_of_feedbacks=num_of_feedbacks,
        cumulative_rating=cumulative_rating
    )
    return True


def upsert_driver_rating(driver, rating):
    """Create/Update driver Rating object for a driver."""
    if not isinstance(rating, float):
        rating = float(rating)

    driver_rating = driver.driver_rating
    if not driver_rating:
        return _create_driver_rating(rating, driver)
    else:
        return _update_driver_rating(driver_rating, rating)


def _create_driver_rating_details(data):
    driver_rating_details = CreateModelMixin().create(data=data, serializer_class=DriverRatingDetailsSerializer)
    if not driver_rating_details:
        logger.error('Driver Rating creation Failed for driver:- "%s"', data.get('driver'))
        return False

    logger.info('driver Rating Successfully created for driver:- "%s"', data.get('driver'))
    return True
