from datetime import datetime

from blowhorn.address.models import get_closed_hour
from blowhorn.customer.constants import CUSTOMER_APP_TIME_FORMAT
from blowhorn.utils.datetime_function import DateTime


class Slot(object):

    def get_time_slots_for_city(self, city):
        """
        Timeslots for given city
        :param city: instance of City
        :return: list of timeslot info
        """
        time_func = DateTime()
        num_days_future = city.future_booking_days
        start_time = city.start_time
        end_time = city.end_time
        start_hour = start_time.strftime(CUSTOMER_APP_TIME_FORMAT) \
            if start_time else ''
        end_hour = end_time.strftime(CUSTOMER_APP_TIME_FORMAT) \
            if end_time else ''
        today = time_func.get_locale_datetime(datetime.now())
        max_date_to_show = time_func.add_timedelta(
            today, {'days': num_days_future})
        closed_datetime_list = get_closed_hour(city, max_date_to_show.date())

        slots = []
        for i in range(num_days_future):
            date = time_func.add_timedelta(today, {'days': i})
            closed_hours = []
            if closed_datetime_list:
                for data in closed_datetime_list:
                    if date.date() == data[0]:
                        closed_hours.append({
                            'date': date.date(),
                            'from': data[1].strftime('%H:%M'),
                            'to': data[2].strftime('%H:%M'),
                        })

            slots.append({
                'date': date.date(),
                'from': start_hour,
                'to': end_hour,
                'closed_hours_details': closed_hours
            })

        return slots
