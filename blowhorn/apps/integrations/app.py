# from django.apps import AppConfig
# from django.conf.urls import url
#
# from blowhorn.apps.integrations.v1.insurance.app import application as v1_integration_app
# from blowhorn.apps.integrations.v1.finance.app import application as finance_app
# from blowhorn.apps.integrations.v1.finance.app import application as ceat
# from blowhorn.apps.integrations.v1.payment.app import application as payment
#
#
# class IntegrationsVersionedApplication(AppConfig):
#     def get_urls(self):
#         urlpatterns = [
#             url(r'^v1/', v1_integration_app.urls),
#             url(r'^v1/', finance_app.urls),
#             url(r'^v1/', ceat.urls),
#             url(r'^v1/', payment.urls),
#         ]
#         return self.post_process_urls(urlpatterns)
