from django.conf.urls import url, include


urlpatterns = [
    url(r'^v1/', include('blowhorn.apps.integrations.v1.insurance.urls')),#v1_integration_app.urls),
    url(r'^v1/', include('blowhorn.apps.integrations.v1.finance.urls')),#finance_app.urls),
    # url(r'^v1/', include('blowhorn.apps.integrations.v1.tyre.ceat.urls')),
    url(r'^v1/', include('blowhorn.apps.integrations.v1.payment.urls')),
]

# include('blowhorn.apps.customer_app.urls')),
