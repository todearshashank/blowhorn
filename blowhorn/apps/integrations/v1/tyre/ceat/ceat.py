from datetime import datetime, timedelta

import pytz
import requests
import json
from django.conf import settings
from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.permissions import AllowAny

from blowhorn.customer.models import CeatOrderPush, PartnerAccessKey, ContactCreation, PartnerLineMap
from blowhorn.driver.models import Driver
from blowhorn.order.models import Order
from config.settings.status_pipelines import ORDER_SENT_FOR_FULILMENT
from blowhorn.order.mixins import OrderPlacementMixin

CEAT_TYPE = 'Ceat Tyres'


headers = {'content-type': 'application/json'}

def create_ceat_order(order):
    # order = Order.objects.filter(number=order_number)
    # order = order.prefetch_related('lines').first()
    if order.status == ORDER_SENT_FOR_FULILMENT:
        return

    partner_key = PartnerAccessKey.objects.get(partner__name=CEAT_TYPE)
    last_token_time = datetime.utcfromtimestamp(int(partner_key.response_time) / 1000).replace(tzinfo=pytz.utc)
    access_token = partner_key.access_token

    if last_token_time <= (timezone.now() - timedelta(hours=22)):
        access_token = CEAT().TokenRefresh(partner_key)

    driver = Driver.objects.filter(user_id=order.user_id)
    driver = driver.select_related('user').first()
    shipping_address = driver.current_address or order.hub.address
    contact_reference = CEAT().CreateContact(access_token, driver, shipping_address)
    if not contact_reference:
        assert False, 'Contact creation failed'

    CEAT().CreateOrderRAML(access_token, contact_reference, shipping_address, order)
    CEAT().OrderPush(access_token, order)
    order.status = ORDER_SENT_FOR_FULILMENT
    OrderPlacementMixin().create_event(order, ORDER_SENT_FOR_FULILMENT, None, driver, None)
    order.save()


class CEAT(object):
    def TokenRefresh(self, partner_key):
        CEAT_TOKEN_REFRESH_URL = settings.CEAT_TOKEN_REFRESH_URL + settings.CEAT_CLIENT_ID + \
                                 '&client_secret=' + settings.CEAT_CLIENT_SECRET_KEY + '&refresh_token=%s'
        response = requests.post(CEAT_TOKEN_REFRESH_URL %(partner_key.refresh_token), headers=headers)

        data = json.loads(response.content)
        if response.status_code == status.HTTP_200_OK:
            partner_key.access_token = data['access_token']
            partner_key.response_time = data['issued_at']
            partner_key.save()
            return partner_key.access_token

        assert False, 'Token Refresh Failed'


    def CreateContact(self, access_token, driver, shipping_address):
        headers.update({'Authorization': 'OAuth '+ access_token})
        request_data = {
            "inputContact": {
                "Salutation": "Mr.",
                "FirstName": "",
                "LastName": driver.name,
                "AccountId": settings.CEAT_CONTACT_ACCOUNT_ID,
                "Gender__c": driver.gender,
                "MailingStreet": shipping_address.line1,
                "MailingCity": shipping_address.line4,
                "MailingState": shipping_address.state,
                "MailingPostalCode": shipping_address.postcode,
                "MailingCountry": shipping_address.country.name,
                "MobilePhone": driver.user.phone_number.national_number,
                "Source__c": "BlowHorn",
                "RecordTypeId": settings.CEAT_CONTACT_RECORD_TYPE_ID,
                "Email": ""

            }
        }
        response = requests.post(settings.CEAT_CREATE_CONTACT_URL ,  json=request_data ,headers=headers)
        response_data = json.loads(response.content)
        if response.status_code == status.HTTP_200_OK:
            ContactCreation.objects.create(user=driver.user, contact_reference=response_data['respDetails'], response=response_data)
            return response_data['respDetails']

        assert False, 'Contact creation failed'


    def CreateOrderRAML(self, access_token, contact_reference, shipping_address, order):
        headers.update({'Authorization': 'OAuth '+ access_token})
        discount = float(order.discount) if order.discount else 0
        total_amount = float(order.total_incl_tax) + float(order.shipping_incl_tax) - float(discount)
        lines = order.lines.all()
        records = []
        for line in lines:
            product = line.product
            records.append({
                "attributes": {
                    "type": "Order_Line_Items__c",
                    "referenceId": str(line.id)
                },
                "Material_Number__c": product.upc,
                "Material_Name__c": product.title,
                "ChargedPrice_ceat__c": float(line.line_price_incl_tax) or 0,
                "Quantity__c": line.quantity,
                "BaseAmt_ceat__c": float(line.line_price_excl_tax) or 0,
                "TaxAmount_ceat__c": float(line.line_price_incl_tax) - float(line.line_price_excl_tax),
                "FinalAmt_ceat__c": float(line.line_price_incl_tax),
                "IsActive__c": True
            })

        request_data = {
              "records": [{
                "attributes": {
                "type": "Order",
                "referenceId": order.number
              },
              "EffectiveDate": timezone.now().date().strftime('%Y-%m-%d'),
              "Status": "Draft",
              "Source_Ceat__c": "",
              "ShippingStreet": shipping_address.line1,
              "ShippingCity": shipping_address.line4,
              "ShippingState": shipping_address.state,
              "ShippingCountry": shipping_address.country.name,
              "ShippingPostalCode": shipping_address.postcode,
              "PaymentMethod_ceat__c": "Razorpay",
              "Currency_type__c": order.currency,
              "SubTotal_ceat__c" : float(order.total_incl_tax),
              "ShippingAmt_ceat__c": float(order.shipping_incl_tax),
              "DiscountAmt_ceat__c":  discount,
              "Order_Amount__c": total_amount,
              "AccountId": settings.CEAT_ACCOUNT_ID,
              "BillToContactId": contact_reference,
              "RecordTypeId": settings.CEAT_RECORD_TYPE_ID,
              "FullFilmentBy_ceat__c" : settings.BRAND_NAME,
              "Driver_Name_ceat__c": str(order.user.name) if order.user else '',
              "Driver_Number_ceat__c": str(order.user.phone_number.national_number)
              if order.user and order.user.phone_number else '',
              "Channel_ceat__c" : "",
              "Bill_To_Party_ceat__c" : str(order.hub.client_billing_code) if order.hub else "",
              "Ship_to_Party_ceat__c" : str(order.hub.shipping_code) if order.hub else "",
            "Order_Line_Items__r": {
                      "records":records
            }
              }]
            }

        response = requests.post(settings.CEAT_OREDER_RAML_URL,  json=request_data ,headers=headers)
        response_data = json.loads(response.content)
        partner_list = []
        if response.status_code == status.HTTP_201_CREATED:
            results = response_data['results']
            for result in results:
                data_ = {
                    "reference_id": result['id']
                }
                if result['referenceId'] == order.number:
                    data_.update({'order_id': order.id})
                else:
                    data_.update({'line_id':result['referenceId']})
                partner_list.append(PartnerLineMap(**data_))
            PartnerLineMap.objects.bulk_create(partner_list)
            return True
        assert False, 'Order Raml Failed'

    def OrderPush(self, access_token, order):
        headers.update({'Authorization': 'OAuth '+ access_token})
        order_info = PartnerLineMap.objects.filter(order=order).first()
        if order_info:
            request_data = {
                "orderId": order_info.reference_id
            }
            response = requests.post(settings.CEAT_ORDER_PUSH, json=request_data, headers=headers)
            response_data = json.loads(response.content)
            if response.status_code == status.HTTP_200_OK:
                soNumbers = response_data['respDetails']['soNumbers']
                CeatOrderPush.objects.create(order=order, reference_id=soNumbers)
                return
            assert False, 'order push Failed'
        assert False, 'Order push reference not found'



