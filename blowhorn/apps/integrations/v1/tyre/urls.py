from django.conf.urls import url, include


urlpatterns = [
    url(r'^payment/', include('blowhorn.apps.integrations.v1.tyre.ceat.urls')),
]

