
from rest_framework import views
from rest_framework.response import Response
from rest_framework import status

from django.utils import timezone
from django.db.models import F
from datetime import timedelta

from blowhorn.oscar.core.loading import get_model

from blowhorn.vehicle.constants import VEHICLE_INSURANCE_CONSTANT
from blowhorn.driver.models import DriverConstants, DriverVehicleMap
from blowhorn.apps.integrations.v1.insurance.symbo import Symbo, SymboBlowhornMap, SymboLead

Driver = get_model('driver', 'Driver')



class SymboEligibility(views.APIView):

    """
    check if driver is eligible to apply for symbo insurance
    """
    def get(self, request):
        data = request.GET.dict()
        print(request.GET)
        driver_id = data.get('driver_id', None)

        if not driver_id:
            driver = request.user.driver if hasattr(request.user, 'driver') else None
            driver_id = driver.id if driver else None

        if not driver_id:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Driver id missing')

        try:
            driver = Driver.objects.get(pk=driver_id)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Driver not found')

        expiry_date_list = DriverVehicleMap.objects.filter(driver=driver,
                                                      vehicle__vehicle_document__document_type__code=VEHICLE_INSURANCE_CONSTANT,

                                                      ).values_list('vehicle__vehicle_document__expiry_date', flat=True)

        expiry_date = expiry_date_list[0] if expiry_date_list else None

        posted_doc_ids = SymboBlowhornMap.objects.filter(
            vehicle_document__in=driver.drivervehiclemap_set.all()[
                0].vehicle.vehicle_document.all())

        if posted_doc_ids:
            _out = {
                'status': 'APPLIED',
                'message': 'You have already applied!',
                'expiry_date': expiry_date
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        BEFORE = timezone.now() - timedelta(days=DriverConstants().get('DAYS_BEFORE_EXPIRY', 30))
        AFTER = timezone.now() + timedelta(days=DriverConstants().get('DAYS_AFTER_EXPIRY', 30) + 1)
        driver_eligible = Driver.objects.filter(id=driver_id,
            drivervehiclemap__vehicle__vehicle_document__document_type__code=VEHICLE_INSURANCE_CONSTANT,
            drivervehiclemap__vehicle__vehicle_document__expiry_date__range=(
            BEFORE, AFTER),
            status='active').exists()

        if driver_eligible:
            message = {
                'message': 'You are eligible for vehicle insurance',
                'status': 'SUCCESS',
                'expiry_date': expiry_date
            }
            return Response(status=status.HTTP_200_OK, data=message)
        else:
            message = {
                'message': 'You are not eligible for vehicle insurance',
                'status': 'FAIL',
                'expiry_date': expiry_date
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)



    """
    apply for symbo insurance
    """
    def post(self, request):
        data = request.POST.dict()
        driver_id = data.get('driver_id', None)

        driver = Driver.objects.filter(id=driver_id).annotate(expiry_date=F(
            'drivervehiclemap__vehicle__vehicle_document__expiry_date'),
            vehicle=F(
                'drivervehiclemap__vehicle'),
            doc_id=F('drivervehiclemap__vehicle__vehicle_document'),
            vehicle_class=F(
                'drivervehiclemap__vehicle__vehicle_model__vehicle_class__commercial_classification')
        ).order_by(
            '-drivervehiclemap__vehicle__vehicle_document__document_type').distinct(
            'drivervehiclemap__vehicle__vehicle_document__document_type',
            'id')
        driver = driver.select_related('user', 'current_address',
                                       'permanent_address')
        if not driver:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='No driver found')

        try:
            Symbo().createLead(driver[0])
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Something went wrong')

        return Response(status=status.HTTP_200_OK, data='Success')
