import json
from urllib import request

from django.db.models import F
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated

from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.customer.models import CashFreePayoutHistory, CashFreeBeneficiary, Customer
from blowhorn.order.const import PAYMENT_STATUS_PAID, PAYMENT_MODE_CASHFREE
from blowhorn.order.models import Order, OrderLine
from config.settings import status_pipelines as StatusPipeline
from blowhorn.oscar.apps.payment.models import Source, SourceType

from blowhorn.payment.mixins import PaymentMixin
from django.conf import settings
from rest_framework.response import Response
import os
import requests
from rest_framework import generics, status, views

from config.settings.base import CASH_FREE_PAYOUTS_GENERATE_URL, \
    CASH_FREE_PAYOUTS_TRANSFER_GENERATE_URL, CASH_FREE_GENERATE_TOKEN_KEY, CASH_FREE_PAYOUTS_GENERATE_TOKEN_ID
from website.views import logger

headers = {'content-type: application/x-www-form-urlencoded'}


class CashFree(object):
    """
    creates order in cash free system
    """

    def create_order(self, order):

        customer_name = ''
        phone_number = ''
        email = ''
        contract = order.customer_contract
        if order.order_type == settings.DEFAULT_ORDER_TYPE or (
            contract and contract.customer and contract.customer.api_key == settings.FLASH_SALE_API_KEY
        ):
            customer = order.customer
        else:
            customer = order.customer_contract.customer
        user = customer.user

        host = os.environ.get('HTTP_HOST', 'https://blowhorn.com')
        data = {
            "appId": settings.CASH_FREE_APPID,
            "secretKey": settings.CASH_FREE_SECRET_KEY,
            "orderId": order.number,
            "orderAmount": int(order.total_payable),
            "orderCurrency": "INR",
            "customerName": customer.legalname or customer.name,
            "customerPhone": str(order.user.phone_number.national_number),
            "customerEmail": str(user.email),
            "returnUrl": '%s/track/%s' % (host, order.number),
            "notifyUrl": "%s/api/integrations/v1/payment/cashfree/capture/" % (host)

        }

        response = requests.post(settings.CASH_FREE_CREATE_ORDER_URL, data=data)
        response_data = json.loads(response.content)

        if response_data['status'] == 'OK':
            payment_link = response_data['paymentLink']


class CapturePayment(views.APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        order_id = data.get('orderId')
        amount = float(data.get("orderAmount", 0))
        txStatus = data.get("txStatus")
        headers = {
            'cache-control': 'no-cache',
            'content-type': 'application/x-www-form-urlencoded'
        }

        if not txStatus == 'SUCCESS':
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Transaction Failed')

        order = Order.objects.filter(number=order_id).first()
        if order:
            # if already captured return sucess
            if order.payment_status == PAYMENT_STATUS_PAID:
                data = {
                    "status": True,
                    "message": 'Payment Done'
                }
                return Response(status=status.HTTP_200_OK, data=data)

            if order.order_type in [settings.C2C_ORDER_TYPE, settings.SHIPMENT_ORDER_TYPE]:
                request_data = {
                    "appId": settings.CASH_FREE_EPOS_APPID,
                    "secretKey": settings.CASH_FREE_EPOS_SECRET_KEY,
                    "orderId": order.number
                }
            else:
                request_data = {
                    "appId": settings.CASH_FREE_APPID,
                    "secretKey": settings.CASH_FREE_SECRET_KEY,
                    "orderId": order.number
                }

            if order.order_type == settings.SHIPMENT_ORDER_TYPE:
                order_amount = float(order.cash_on_delivery)
            else:
                order_amount = float(order.total_payable)

            if amount != order_amount:
                #checks the order amt and app sent amt
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid amount paid')

            response = requests.post(settings.CASH_FREE_PAYMENT_STATUS_CHECK, data=request_data, headers=headers)
            response_data = json.loads(response.content)

            if response_data['status'] == 'ERROR':
                print('Failed to fetch Payment Info')
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to fetch Payment Info')

            if float(response_data['details']['orderAmount']) != order_amount:
                #checks the amount with cashfree response
                print('Invalid amount paid')
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid amount paid')

            if response_data['details']['orderStatus'] != 'PAID':
                print('Payment not received')
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Payment not received')

            payment_source = Source.objects.filter(order=order,
                                                   source_type__name=settings.CASH_FREE_PAYMENT_GATEWAY).first()
            if not payment_source:
                source_type = SourceType.objects.filter(
                    code=settings.CASH_FREE_PAYMENT_GATEWAY).first()
                Source.objects.get_or_create(
                    order=order,
                    source_type=source_type,
                    amount_allocated=order_amount if order else amount,
                    amount_debited=0,
                    amount_refunded=0,
                    reference=data.get("referenceId"))
                # PaymentMixin().create_source(
                #     settings.CASH_FREE_PAYMENT_GATEWAY,
                #     order=order,
                #     amount=order.total_payable
                # )

            extra_args = {
                'amount_debited': order_amount,
                'reference': data.get("referenceId")
            }
            PaymentMixin().update_source(
                order=order,
                payment_type=settings.CASH_FREE_PAYMENT_GATEWAY,
                **extra_args
            )

            if order_amount == amount:
                order.payment_status = PAYMENT_STATUS_PAID
                order.payment_mode = PAYMENT_MODE_CASHFREE
                if order.status == StatusPipeline.ORDER_PENDING:
                    order.status = StatusPipeline.ORDER_NEW
                order.save()
                OrderLine.objects.filter(order=order).update(is_settled=True)
                data = {
                    "status": True,
                    "message": 'Payment Done'
                }
            return Response(status=status.HTTP_200_OK, data=data)
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order')


class GenerateToken(generics.RetrieveAPIView):

    permission_classes = (AllowAny, )

    def get(self, request, order_id):
        order = Order.objects.filter(number=order_id).first()

        if not order:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid order number')

        is_epos = request.GET.get('is_epos')

        notify_url = None
        if is_epos:
            url = settings.CASH_FREE_EPOS_GENERATE_TOKEN_URL
            app_id = settings.CASH_FREE_EPOS_APPID
            secret_key = settings.CASH_FREE_EPOS_SECRET_KEY
            # host = self.request.META.get('HTTP_HOST')
            # notify_url = "https://%s/api/integrations/v1/payment/cashfree/capture/" % (
            #     host)
            notify_url = "/api/integrations/v1/payment/cashfree/capture/"
        else:
            url = settings.CASH_FREE_GENERATE_TOKEN_URL
            app_id = settings.CASH_FREE_APPID
            secret_key = settings.CASH_FREE_SECRET_KEY

        headers = {'Content-Type': 'application/json',
                   'x-client-id': app_id,
                   'x-client-secret': secret_key
        }

        if order.order_type == settings.SHIPMENT_ORDER_TYPE:
            order_amount = float(order.cash_on_delivery)
        else:
            order_amount = float(order.total_payable)

        data = {
            "orderId": order.number,
            "orderAmount": order_amount,
            "orderCurrency": "INR"
        }

        response = requests.post(url, json=data, headers=headers)
        response_data = json.loads(response.content)

        if response_data['status'] == 'OK':
            lines = OrderLine.objects.filter(order=order).annotate(vendor=F('partner__vendorpayout__vendor_name'),
                                                                  percent=F('partner__vendorpayout__percentage'),
                                                                  is_active=F('partner__vendorpayout__is_active'))

            vendor_payout = []
            for line in lines:
                if not line.is_active:
                    line.percentage = 1
                vendor_payout.append({
                    "vendorId": line.vendor,
                    "commissionAmount": line.cost_price * line.percent if line.percent else line.cost_price
                })

            response = {
                "token": response_data['cftoken'],
                "vendorSplit": vendor_payout,
                "notifyUrl": notify_url
            }
            return Response(status=status.HTTP_200_OK, data=response)
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to generate token')


def add_benificiary(customer, beneId, token):
    user = customer.user
    header = {'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + token
              }
    url = CASH_FREE_PAYOUTS_GENERATE_URL
    data = {
        "name": customer.name,
        "phone": str(user.phone_number.national_number),
        "email": str(user.email),
        "beneId": beneId,
        "address1": str(customer.current_address),
        "bankAccount": customer.bank_account,
        "ifsc": customer.ifsc_code,
    }
    print(data)
    response = requests.post(url, json=data, headers=header)
    response_data = json.loads(response.content)
    resp_status = response_data.get('status')
    if resp_status == 'SUCCESS':
        cash_free_beneficiary = CashFreeBeneficiary(
            bank_account=customer.bank_account,
            ifsc_code=customer.ifsc_code,
            customer=customer,
            bene_id=beneId,
            response_status=resp_status,
        )
        cash_free_beneficiary.save()
    else:
        logger.info('Failed to add cashfree benificiary: %s' % response_data)


def request_async_transfer(customer, total_amount, bene_id, transfer_id, bank_acount_no, payout_token):
    header = {'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + payout_token
              }
    url = CASH_FREE_PAYOUTS_TRANSFER_GENERATE_URL

    data = {
        "beneId": bene_id,
        "transferId": transfer_id,
        "amount": total_amount,
    }
    response = requests.post(url, json=data, headers=header)
    response_data = json.loads(response.content)
    resp_remarks = response_data.get('message')
    ref_id = response_data.get('data')
    response_id = ref_id.get('referenceId')
    resp_status = response_data.get('status')
    if resp_status == 'ACCEPTED':
        cash_free_payout_history = CashFreePayoutHistory(
            amount=total_amount,
            transferId=transfer_id,
            beneId=bene_id,
            customer=customer,
            bank_account=bank_acount_no,
            referenceId=response_id,
            response_status=resp_status,
            remarks=resp_remarks
        )
        cash_free_payout_history.save()
        Order.objects.filter(customer_payment_settled=False).update(customer_payment_settled=True)
    else:
        logger.info('Failed to transfer amount: %s' % response_data)

