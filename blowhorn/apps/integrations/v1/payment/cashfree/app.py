from django.conf.urls import url
from django.apps import AppConfig
from .views import *


class InsuranceApp(AppConfig):

    def get_urls(self):
        urlpatterns = [
            url(r'^cashfree/capture/', CapturePayment.as_view(), name='capture-payment'),
            url(r'^cashfree/generatetoken/(\S+)$', GenerateToken.as_view(), name='capture-payment'),
        ]

        return self.post_process_urls(urlpatterns)

