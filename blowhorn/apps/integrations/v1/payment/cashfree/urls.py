from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^cashfree/capture/', CapturePayment.as_view(), name='capture-payment'),
    url(r'^cashfree/generatetoken/(\S+)$', GenerateToken.as_view(), name='capture-payment'),
]
