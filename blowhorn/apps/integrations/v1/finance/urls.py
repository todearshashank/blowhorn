from django.conf.urls import url, include

urlpatterns = [
    url(r'^finance/',  include('blowhorn.apps.integrations.v1.finance.dvara.urls')),
]
