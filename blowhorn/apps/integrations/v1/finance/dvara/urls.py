from django.conf.urls import url
from .views import DriverHistoricalSatement
from .dvara import DriverInformation, LeadInformation

urlpatterns = [
    url(r'^historicalstatement/', DriverHistoricalSatement.as_view(), name='historical_statement'),
    url(r'^driverinformation/', DriverInformation.as_view(), name='driver_info'),
    url(r'^leadinfo/', LeadInformation.as_view(), name='leadinfo'),
]
