from datetime import timedelta

from django.utils import timezone
from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from blowhorn.customer.models import get_partner_from_api_key
from blowhorn.driver.models import DriverLedger, Driver
from blowhorn.driver.serializers import DriverLedgerSerializer
from blowhorn.driver.views import _get_eligibility_criteria


class DriverHistoricalSatement(generics.RetrieveAPIView):

    permission_classes = (AllowAny, )

    def get(self, request):
        data = request.GET
        driverId = data.get('driverId', None)
        api_key = request.META.get('HTTP_API_KEY', None)
        partner = get_partner_from_api_key(api_key)
        if not partner:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            driverId = int(driverId)
        except:
            _out = {
                'status': 'FAIL',
                'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)


        time_delta = timezone.now() -  timedelta(days=180)
        elig_query = _get_eligibility_criteria(partner)
        driver_data = {'id': driverId}
        if elig_query:
            driver_data.update(elig_query)

        driver = Driver.objects.filter(**driver_data).first()
        if not driver:
            _out = {
                'status': 'FAIL',
                'message': 'Driver not eligible for advance salary'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not driver.own_vehicle:
            _out = {
                'status': 'FAIL',
                'message': "Driver doesn't own a vehicle"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        ledger = DriverLedger.objects.filter(driver_id=driverId, transaction_time__gte=time_delta, is_settlement=True)
        ledger = ledger.select_related('ops_reference')
        ledger = ledger.prefetch_related('ops_reference__paymenttrip_set')
        data = []

        for entry in ledger:
            account_info = entry.bank_account.split('-')
            dp = entry.ops_reference

            data.append({
                "transaction_date": entry.transaction_time,
                "amount_credited": abs(entry.amount),
                "no_of_trips": dp.paymenttrip_set.count() if dp else 0,
                "bank_account": {
                    "name": account_info[2] if len(account_info) == 3 else '',
                    "account_no": account_info[1]  if len(account_info) == 3 else '',
                    "ifsc_code": account_info[0]  if len(account_info) == 3 else ''
                }
            })


        return Response(status=status.HTTP_200_OK,
                        data=data,
                        content_type="application/json")
