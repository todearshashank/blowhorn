import requests
import json
from django.conf import settings
from django.db import transaction
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.permissions import AllowAny

from blowhorn.customer.models import get_partner_from_api_key
from blowhorn.driver.models import Driver, DriverLoanDetails, DriverDocument
from blowhorn.integration_apps.sub_models.sub_models import PartnerLeadStatus, PartnerLeadHistory
from blowhorn.vehicle.models import VehicleDocument
from blowhorn.driver.views import _get_eligibility_criteria
from blowhorn.document.constants import ACTIVE

#from boto.s3.connection import S3Connection
#REGION_HOST = 's3.us-east-2.amazonaws.com'
#conn = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY, host=REGION_HOST)

headers = {'content-type': 'application/json', 'API_KEY': settings.DVARA_API_KEY}

DOCUMENT_TO_SEND = ['Aadhaar', 'PAN', 'RC', 'Bank_Passbook', 'Photo']

class DriverInformation(generics.RetrieveAPIView):


    def get_encrypted_document(self, url, bucket, headers):
        original_key = bucket.get_key(url, headers=headers)
        signed_url = ''
        if original_key:
            try:
                signed_url = original_key.generate_url(expires_in=172800, query_auth=True, force_http=True)
            except:
                pass
        return signed_url

    def post(self, request):
        data = request.data
        driver = request.user.driver
        driver_id = driver.id
        partner_id = data.get('client_id')
        driver = Driver.objects.filter(id=driver_id)
        driver = driver.select_related('permanent_address', 'current_address', 'bank_account').first()

        if driver:
            DriverLoanDetails.objects.create(driver=driver, partner_id=partner_id, is_driver_detail=True)
            permanent_address = driver.permanent_address
            current_address = driver.current_address
            bank_info = driver.bank_account
            DATE_FORMAT = '%d-%b-%Y'

            request_data = {
                "partner_user_id": str(driver_id),
                'name': driver.name,
                'gender': driver.gender,
                'DOB': driver.date_of_birth.strftime(DATE_FORMAT) if driver.date_of_birth else '',
                'doj': driver.active_from.strftime(DATE_FORMAT) if driver.active_from else '',
                'contact_no': str(driver.user.phone_number) if driver.user else '',
                'father_name': driver.father_name,
                'marital_status': driver.marital_status,
                'educational_detail': driver.educational_qualifications,
                'vehicle_no': driver.driver_vehicle,
                'license': driver.driving_license_number,
                'PAN': driver.pan,
                'owner': "Yes" if driver.own_vehicle else "No",
                "name_asper_bank": bank_info.account_name if bank_info else '',
                'account_no': bank_info.account_number if bank_info else '',
                "ifsc_code": bank_info.ifsc_code if bank_info else '',
                'pa_address': permanent_address.line1 if permanent_address else '',
                'pa_city': permanent_address.line4 if permanent_address else '',
                'pa_state': permanent_address.state if permanent_address else '',
                'pa_postcode': permanent_address.postcode if permanent_address else '',
                'pa_country': str(permanent_address.country) if permanent_address else '',
                'pa_complete_address': permanent_address.search_text if permanent_address else '',
                'ca_address': current_address.line1 if current_address else '',
                'ca_city': current_address.line4 if current_address else '',
                'ca_state': current_address.state if current_address else '',
                'ca_postcode': current_address.postcode if current_address else '',
                'ca_country': str(current_address.country) if current_address else '',
                'ca_complete_address': current_address.search_text if current_address else ''

            }

            driver_documents = DriverDocument.objects.filter(status=ACTIVE,
                                                             document_type__code__in=DOCUMENT_TO_SEND,
                                                             driver_id=driver_id)
            veh_doc= VehicleDocument.objects.filter(status=ACTIVE, document_type__code__in=DOCUMENT_TO_SEND,
                                                    vehicle__drivervehiclemap__driver_id=driver_id)

            for doc in driver_documents:
                # url = '/media/' + doc.file.name
                request_data.update({doc.document_type.code.lower() + '_link': doc.file.url if doc.file else ''})
                # response.update({doc.document_type.code + 'link':self.get_encrypted_document(url, bucket, headers)})

            for doc in veh_doc:
                # url = '/media/' + doc.file.name
                request_data.update({doc.document_type.code.lower() + '_link': doc.file.url if doc.file else ''})
                # response.update({doc.document_type.code + 'link':self.get_encrypted_document(url, bucket, headers)})

            response = requests.post(settings.DVARA_INITIATE_LOAN , json=request_data , headers=headers)

            data = json.loads(response.content)
            if response.status_code == status.HTTP_200_OK:
                return Response(status=status.HTTP_200_OK, data=data)
            return Response(status=status.HTTP_400_BAD_REQUEST, data=data)


class LeadInformation(generics.RetrieveAPIView):

    permission_classes = (AllowAny, )

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        driver_id = data.get('driver_id', None)
        loan_id = data.get('loan_id', None)
        loan_status = data.get('loan_status', None)
        data.update({"reference_id":loan_id})
        data.pop('loan_id', '')

        try:
            driver_id = int(driver_id)
        except:
            _out = {
                'status': 'FAIL',
                'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)
        partner = get_partner_from_api_key(api_key)

        if not partner:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if  not driver_id:
            _out = {
                'status': 'FAIL',
                'message': 'mobile or driverId is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if  not loan_id:
            _out = {
                'status': 'FAIL',
                'message': 'loan_id  is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if  not loan_status:
            _out = {
                'status': 'FAIL',
                'message': 'loan_status  is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        data.update({"partner_id":partner.id})

        lead = PartnerLeadStatus.objects.filter(reference_id=loan_id, partner_id=partner.id).first()

        with transaction.atomic():
            try:
                if lead :
                    PartnerLeadStatus.objects.filter(id=lead.id).update(**data)
                    _out = {
                        'status': 'SUCCESS',
                        'message': 'Loan Information Updated'
                    }
                else:
                    lead = PartnerLeadStatus(**data)
                    lead.save()
                    _out = {
                        'status': 'SUCCESS',
                        'message': 'Loan Information Captured'
                    }
                data.update({"partner_lead_id": lead.id})
                PartnerLeadHistory.objects.create(**data)
            except:
                _out = {
                    'status': 'FAIL',
                    'message': 'Loan  Capture Failed'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        return Response(status=status.HTTP_200_OK, data=_out)
