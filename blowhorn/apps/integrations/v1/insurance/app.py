# from django.conf.urls import url
# from django.apps import AppConfig
# from . import symbo
#
# from blowhorn.apps.integrations.v1.views import SymboEligibility
#
# class InsuranceApp(AppConfig):
#
#     def get_urls(self):
#         urlpatterns = [
#             url(r'^leadstatus/symbo$', symbo.GetLeadStatus.as_view(), name='lead-status'),
#             url(r'^eligibility/symbo', SymboEligibility.as_view(),
#                 name='symbo-insurance-eligibility'),
#         ]
#
#         return self.post_process_urls(urlpatterns)
