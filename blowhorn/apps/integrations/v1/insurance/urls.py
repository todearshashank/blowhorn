from django.conf.urls import url
from . import symbo

from blowhorn.apps.integrations.v1.views import SymboEligibility

urlpatterns = [
    url(r'^leadstatus/symbo$', symbo.GetLeadStatus.as_view(), name='lead-status'),
    url(r'^eligibility/symbo', SymboEligibility.as_view(),
        name='symbo-insurance-eligibility'),
]
