import requests
import json

from django.conf import settings
from django.utils import timezone
from datetime import datetime
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.permissions import AllowAny

from blowhorn.customer.models import Partner
from blowhorn.integration_apps.models import SymboLead, SymboBlowhornMap, SymboLeadStatus
from blowhorn.address.utils import AddressUtil

SYMBO_URL = settings.SYMBO_LEADMANAGEMENT_URL
api_key = settings.SYMBO_API_KEY

class Symbo(object):
    """
       Class for Symbo Insurance , creates Lead
       Get the Document.

    """

    def createLead(self, driver):
        """
           Creates lead on symbo system, n days before driver insurance expiry.

        """
        address = driver.current_address or driver.permanent_address
        driver_data = {
            "firstName": driver.name,
            "middleName": "",
            "lastName": "",
            "salutation": "MR",
            "leadSource": "WEB",
            "phone": "",
            "description": "",
            "mobile": driver.user.phone_number.as_international,
            "opportunities": ["Others"],
            "address": address.line1 if address else "",
            "city": address.line4 if address else str(driver.operating_city) or "",
            "state": address.state if address else str(AddressUtil().get_state_from_city(driver.operating_city)),
            "country": str(address.country) if address else settings.COUNTRY_CODE_A2,
            "pincode": address.postcode if address else "",
            "campaignId": "blowhorn",
            "expiry_date": str(driver.expiry_date),
            "city": str(driver.operating_city),
            "vehicle_number": driver.driver_vehicle,
            "vehicle_type": str(driver.vehicle_class),
            "email": "",
            "days_to_expire": (driver.expiry_date - timezone.now().date()).days if driver.expiry_date else ''

        }

        headers = {'content-type': 'application/json', 'x-api-key': api_key,
                   'clientId': 'BLOWHORN'}
        response = requests.post(SYMBO_URL, json=driver_data, headers=headers)
        data = json.loads(response.content)

        lead = SymboLead.objects.create(driver_id=driver.id, symbo_id=data.get('id'), referenceId=data.get('referenceId'), response=data,
                                 last_modified=datetime.fromtimestamp(data.get('lastModified')/1000))

        SymboBlowhornMap.objects.create(lead=lead,
                                        vehicle_document_id=driver.doc_id)


class GetLeadStatus(generics.RetrieveAPIView):

    permission_classes = (AllowAny, )

    def get(self, request):
        data = request.GET.dict()
        leadId = data.get('leadId')
        lead = SymboLead.objects.filter(id=leadId).first()
        if lead:
            LEAD_URL = SYMBO_URL + '/' + str(lead.symbo_id)
            headers = {'content-type': 'application/json', 'x-api-key': api_key,
                       'clientId': 'BLOWHORN'}
            response = requests.get(LEAD_URL, headers=headers)
            data = json.loads(response.content)

            lead_status = {
                'disposition': data.get('disposition', ''),
                'sub_Disposition': data.get('sub_Disposition', ''),
                'lead_id': lead.id,
                "response": data
            }

            SymboLeadStatus.objects.create(**lead_status)
            response_dict = {
                'status': 200,
                'data': data
            }
            return Response(status=status.HTTP_200_OK, data=response_dict,
                            content_type="application/json")

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='LeadId is Missing',
                        content_type="application/json")
