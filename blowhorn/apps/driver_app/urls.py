from django.conf.urls import url, include

urlpatterns = [
    url(r'^v1/', include('blowhorn.apps.driver_app.v1.urls')),
    url(r'^v2/', include('blowhorn.apps.driver_app.v2.urls')),
    url(r'^v3/', include('blowhorn.apps.driver_app.v3.urls')),
    url(r'^v4/', include('blowhorn.apps.driver_app.v4.urls')),
]
