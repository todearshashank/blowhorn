"""
Completes the trip that is in-progress.

Trip and its stops are marked as Completed.

Input Data: trip_id, driver_location_data

Response: {
            'is_all_stops_done': True,
            'current_step': -1,
            'is_trip_in_progress': False,
            'cost_components': [],
            'is_loaded': True,
            'next_stop_id': ''
        }

Created By: Gaurav Verma
"""
# Django and System libraries
import json
import logging
from django.utils.translation import ugettext_lazy as _

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import (
    TRIP_COMPLETED, UNATTEMPTED, TRIP_IN_PROGRESS
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.apps.driver_app.v1.helpers.order import \
    check_remaining_items_handover


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')


class TripComplete(object):
    """cancellation of trip changes done here."""

    def _initialize_attributes(self, data, pk):
        """
        Parse the input params.

        Any validation error on input params if required, should be raised here.
        """
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

        self.trip = Trip.objects.filter(
            pk=pk,
            status=TRIP_IN_PROGRESS
        ).first()
        if not self.trip:
            self.error_message = 'Invalid Trip.'
            return

        try:
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data !'
            return

        self.app_name = 'Unified App'
        self.app_version = data.get('app_version', '')

    def __make_updates(self):
        Stop.objects.filter(trip=self.trip).exclude(
            status=TRIP_COMPLETED).update(
                status=UNATTEMPTED
        )

        self.trip_data['status'] = TRIP_COMPLETED
        self.trip_data['current_step'] = -1

    def __save_updates(self):
        if self.trip_data:
            self.trip_data['app_name'] = self.app_name
            self.trip_data['app_version'] = self.app_version
            self.trip = UpdateModelMixin().update(data=self.trip_data, instance=self.trip,
                                                  serializer_class=self.serializer_class)
            try:
                self.trip.save()
            except Exception as e:
                self.error_message = str(e)
                self.status_code = status.HTTP_400_BAD_REQUEST
                return

    def _complete_trip(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        is_order_left = check_remaining_items_handover(self.trip, self.driver)
        if is_order_left:
            self.error_message = _(
                'Handover remianing orders/cash to Hub associate')
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        self.__make_updates()
        self.__save_updates()

        self.response_data = {
            'is_all_stops_done': True,
            'current_step': self.trip.current_step,
            'is_trip_in_progress': False,
            'cost_components': [],
            'is_loaded': True,
            'next_stop_id': ''
        }
