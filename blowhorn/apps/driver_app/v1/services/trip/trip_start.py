"""
Trip Start Changes are done here.

will be called when driver is on 2 screens:
- Start Trip
    -Update trip to in-progress
    -Update trip fields mentioned in the trip workflow

    If there is No Finished Loading Screen:
        -Update the first stop to In Progress
        -Update the distinct orders(single for b2b/c2c and multiple for shipment trips) to out for delivery
    else:
        - Update distinct orders status to At-Pickup
- Finished Loading
    -Update Trip Pickup_loading_timestamp
    -Update the first stop to In Progress
    -Update the distinct orders(single for b2b/c2c and multiple for shipment trips) to out for delivery

Input Data: 'trip_id',
            'location_details',
            trip_last_response:{
                'is_all_stops_done': False,
                'current_step': 1,
                'is_trip_in_progress': True,
                'is_loaded': True,
                'next_stop_id': ''
            },
            trip_data: {
                'meter_reading_start': 100,
                'assigned': 50
            },
            'confirmation_screen': 'true'/'false'(
                to differentiate whether it is start trip or finished loading screen
            )

Response:
    {
        'is_all_stops_done': False,
        'current_step': trip.current_step,
        'is_trip_in_progress': True,
        'is_loaded': True if trip.pickup_loading_timestamp,
        'next_stop_id': next inprogress stop id that is made
    }

Created By: Gaurav Verma
"""
# Django and System libraries
import json
import logging
from django.db.models import Q
from django.db import transaction
from django.utils import timezone
from dateutil import parser

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from config.settings.status_pipelines import ORDER_NEW, ORDER_OUT_FOR_PICKUP
from blowhorn.apps.driver_app.v1.serializers.trip import TripStopSerializer, TripSerializer
from blowhorn.apps.driver_app.v1.serializers.order import OrderSerializer
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import (
    TRIP_IN_PROGRESS, DRIVER_ARRIVED_AT_PICKUP,
    OUT_FOR_DELIVERY, DRIVER_ACCEPTED, TRIP_NEW,
    DRIVER_ASSIGNED, ORDER_MOVING_TO_HUB
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.apps.driver_app.v1.helpers.driver import _check_device_details
from blowhorn.contract.constants import END_TRIP
from blowhorn.trip.constants import ORDER_DELIVERY, HUB_DELIVERY


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')


class TripStartMixin(object):
    """Trip Start Changes are done here."""

    def _initialize_attributes(self, data, pk):
        location = data.get('location_details', None)
        self.context = {'request': self.request}
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.driver = self.request.user.driver
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        query = Q(pk=pk, status__in=[DRIVER_ACCEPTED, TRIP_IN_PROGRESS, TRIP_NEW])
        self.trip = Trip.objects.prefetch_queryset(
            query=query, stops_list=True, distinct_orders=True).first()
        if not self.trip:
            self.error_message = 'Trip does not exist.'
        try:
            trip_last_response = json.loads(data.get('trip_last_response'))
            self.current_step = int(trip_last_response.get('current_step'))
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data !'

        device_time = parser.parse(data.get('device_timestamp'))
        mt_start = self.trip_data.get('meter_reading_start', '')

        if mt_start and device_time and device_time :
            time_diff = device_time - timezone.now()
            if abs(time_diff.total_seconds())/60 > 60:
                self.error_message = 'Invalid time'

        if self.error_message:
            self.response_data = self.error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        # TODO change data.get('wait_time_capture')
        wait_time_capture = data.get('confirmation_screen', None)
        # TODO remove this bunch of code
        if wait_time_capture == 'false' or not wait_time_capture:
            self.wait_time_capture = False
        else:
            self.wait_time_capture = True

        self.stop_data = {}
        self.stop = None
        self.orders = []
        self.order_data = {}

        self.app_name = 'Unified App'
        self.app_version = data.get('app_version', '')
        self.app_code = data.get('app_code', '')

    def __make_updates(self):
        if self.wait_time_capture:
            self.__start_trip_wait_time_capture()
        else:
            if self.trip.actual_start_time:
                self.__finished_loading_screen()
            else:
                self.__start_trip_no_waiting_time_capture()

    def __get_next_step(self):
        """
        Function not used currently.

        Using self.current_step instead of self.trip.current_step...

        because in case of slow internet or multiple requests
        trip current step might be updated by other previous requests
        """
        # can be finished loading/reached stop/End Trip
        if not self.wait_time_capture and not self.trip.stops_list:
            return self.trip.trip_workflow.screens.get(name=END_TRIP).sequence
        return self.current_step + 1
        # # for c2c flow
        # if not trip.stops_list:
        #     if not self.wait_time_capture and not next_screen.confirmation_screen:
        #         return self.current_step + 2
        # return self.current_step + 1

    def __save_updates(self):
        logger.info('stop_data: %s' % self.stop_data)
        if self.stop and self.stop_data:
            self.stop = UpdateModelMixin().update(data=self.stop_data, instance=self.stop,
                                                  serializer_class=TripStopSerializer)
            self.stop.save()
        logger.info('trip_data: %s' % self.trip_data)
        if self.trip and self.trip_data:
            self.trip_data['app_name'] = self.app_name
            self.trip_data['app_version'] = self.app_version
            self.trip = UpdateModelMixin().update(data=self.trip_data, instance=self.trip,
                                                  serializer_class=TripSerializer)
            self.trip.save()
        logger.info('order_data: %s' % self.order_data)
        if self.orders and self.order_data:
            for order in self.orders:
                order_data = self.order_data
                if order.has_pickup:
                    if hasattr(order, 'new_status'):
                        order_data = {'status': order.new_status}
                    elif order.status in [ORDER_NEW, DRIVER_ASSIGNED,
                                          DRIVER_ACCEPTED,
                                          ORDER_OUT_FOR_PICKUP]:
                        order_data = {'status': ORDER_OUT_FOR_PICKUP}

                if self.order_data.get('status', None):
                    self.context['geopoint'] = self.geopoint
                    if hasattr(order, 'remarks'):
                        self.context['remarks'] = order.remarks
                order = UpdateModelMixin().update(data=order_data, instance=order,
                                                  serializer_class=OrderSerializer, context=self.context)
                order.save()

    def __check_valid_request(self):
        if self.trip.current_step == self.current_step:
            return True
        else:
            return False

    def __get_response_data(self):
        self.response_data = {
            'is_all_stops_done': False,
            'current_step': self.trip.current_step,
            'is_trip_in_progress': True,
            'is_loaded': not self.wait_time_capture,
            'next_stop_id': self.stop.id if self.stop else '',
            'stop_type': self.stop.stop_type if self.stop else ''
        }
        logger.info('Response data sent: %s' % self.response_data)

    def _start_trip(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return
        with transaction.atomic():
            is_valid_request = self.__check_valid_request()
            if is_valid_request:
                self.__make_updates()
                self.trip_data['current_step'] = self.__get_next_step()
                self.__save_updates()
            else:
                self.stop = self.trip.get_current_stop_in_progress()

            self.__get_response_data()
            _check_device_details(self.driver, self.app_version, self.app_code)

    def __start_trip_wait_time_capture(self):
        # start trip screen with confirmation
        logger.info('__start_trip_wait_time_capture')
        if self.trip.status != TRIP_IN_PROGRESS:
            self.trip_data['status'] = TRIP_IN_PROGRESS

        # c2c and b2c case
        if self.trip.distinct_orders:
            for stop in self.trip.distinct_orders:
                if stop.order and stop.order.status != DRIVER_ARRIVED_AT_PICKUP:
                    order = stop.order
                    self.orders.append(order)
                    self.order_data['status'] = DRIVER_ARRIVED_AT_PICKUP
                elif stop.waypoint and stop.waypoint.order.status != DRIVER_ARRIVED_AT_PICKUP:
                    order = stop.waypoint.order
                    self.orders.append(order)
                    self.order_data['status'] = DRIVER_ARRIVED_AT_PICKUP
        # b2b case
        if not self.trip.stops_list:
            order = self.trip.order
            if order.status != DRIVER_ARRIVED_AT_PICKUP:
                self.orders.append(order)
                self.order_data['status'] = DRIVER_ARRIVED_AT_PICKUP

    def __finished_loading_screen(self):
        # that means we are on finish loading
        logger.info('finished loading screen')
        if not self.trip.pickup_loading_timestamp:
            self.trip_data['pickup_loading_timestamp'] = timezone.now()
        self.__common_updates()

    def __start_trip_no_waiting_time_capture(self):
        # that means on Start trip screen(waiting capture is False)
        logger.info('__start_trip_no_waiting_time_capture')
        if self.trip.status != TRIP_IN_PROGRESS:
            self.trip_data['status'] = TRIP_IN_PROGRESS
        self.__common_updates()

    def __common_updates(self):
        # TODO order updates using serializer
        # c2c and b2c
        if self.trip.distinct_orders:
            for stop in self.trip.distinct_orders:
                if stop.order and stop.order.status != OUT_FOR_DELIVERY:
                    """
                    for stops of type HUB, update status for all orders related to stop
                    """
                    self.order_data['status'] = OUT_FOR_DELIVERY
                    if stop.stop_type == HUB_DELIVERY:
                        stop_orders = stop.stoporders_set.all()
                        for stop_order in stop_orders:
                            """
                            set new_status attribute based on stops so that it
                            can be used for updating order status
                            """
                            stop_order.order.new_status = ORDER_MOVING_TO_HUB
                            stop_order.order.remarks = stop_order.order.hub.name
                            self.orders.append(stop_order.order)
                    elif stop.stop_type == ORDER_DELIVERY:
                        order = stop.order
                        order.new_status = OUT_FOR_DELIVERY
                        self.orders.append(order)
                    else:
                        order = stop.order
                        self.orders.append(order)
                        self.order_data['status'] = OUT_FOR_DELIVERY

                elif stop.waypoint and stop.waypoint.order.status != OUT_FOR_DELIVERY:
                    order = stop.waypoint.order
                    self.orders.append(order)
                    self.order_data['status'] = OUT_FOR_DELIVERY

        # b2b
        if not self.trip.stops_list:
            order = self.trip.order
            if order.status != OUT_FOR_DELIVERY:
                print('Hi116')
                self.orders.append(order)
                self.order_data['status'] = OUT_FOR_DELIVERY
        else:
            # Put the first supposed stop to In-Progress
            stop_qs = sorted(self.trip.stops_list, key=lambda x: x.sequence)
            self.stop = stop_qs[0]
            if self.stop.status != TRIP_IN_PROGRESS:
                self.stop_data['status'] = TRIP_IN_PROGRESS
