"""
Accept/Reject Trip by driver from the home screen.

Input Data: action('accept'/'reject'), trip_id, Location_details

Response:
    -If Driver Accepts:
        - Trip changed to Accepted status
        - Order changed to Accepted status

        Returns Created Trip Data
    -If Driver Rejects:
        - Remove Driver from the Order
        - Trip changed to Suspended status
        - All Stops status changed to Suspended status
        - SMS sent to spocs of the contract for which the trip belongs.
        - send slack notification for trip rejection in #unallocated-orders channel
        Returns 'Driver Rejected' and accept/reject notification will be sent to the next driver.

Created By: Gaurav Verma
"""
# Django and System libraries
import logging
import json
from django.db.models import Q
from django.db import transaction
from django.conf import settings

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.common.helper import CommonHelper
from blowhorn.common import sms_templates
from config.settings.status_pipelines import DRIVER_ACCEPTED, TRIP_SUSPENDED, TRIP_NEW, ORDER_NEW
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.apps.driver_app.v1.helpers.driver import _get_support_number_from_contract
from blowhorn.common.tasks import send_sms
from blowhorn.apps.driver_app.v1.serializers.trip import TripSerializer, TripStopSerializer
from blowhorn.apps.driver_app.v1.serializers.order import OrderSerializer
from blowhorn.apps.driver_app.tasks import send_unallocated_orders_slack_notification
from blowhorn.apps.driver_app.v1.helpers.driver import _check_device_details


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
Order = get_model('order', 'Order')
WayPoint = get_model('order', 'WayPoint')


class TripDecision(object):
    """Make changes on accept/reject action choosen by driver from app for a trip."""

    def _initialize_attributes(self, data, trip_id):
        self.error_message = ''
        self.action = data.get('action', None)
        if self.action != 'accept':
            self.error_message = 'Trip can not be Rejected'
            return
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.context = {'request': self.request}
        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

        query = Q(pk=trip_id, status=TRIP_NEW)
        self.trip = Trip.objects.prefetch_queryset(query=query, stops_list=True).first()
        if not self.trip:
            self.error_message = 'Invalid Trip !'
            return
        # TODO need to get this from app
        self.app_name = 'Unified App'
        self.app_version = data.get('app_version', '')
        self.app_code = data.get('app_code', '')
        self.vehicle_class = str(self.trip.vehicle.vehicle_model.vehicle_class) if self.trip.vehicle else ''
        self.vehicle_rc = self.trip.vehicle.registration_certificate_number if self.trip.vehicle else ''
        self.order_data = {}
        self.trip_data = {}
        self.stop_data = {}
        self.orders = self.trip._get_orders_from_trip()
        self.stops = self.trip.stops_list
        self.reason = data.get('reason', '')

    def _make_decision(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return
        with transaction.atomic():
            if self.action == 'accept':
                self.response_data = self.__accept_order()
            elif self.action == 'reject':
                self.__reject_order()
                self.response_data = 'Driver Rejected'
            _check_device_details(self.driver, self.app_version, self.app_code)

    def __accept_order(self):
        self.trip_data['status'] = DRIVER_ACCEPTED
        self.order_data['status'] = DRIVER_ACCEPTED

        self.__save_updates()

        return self.serializer_class(instance=self.trip, context=self.context).data

    def __send_sms_to_contract_spocs(self):
        if not settings.ENABLE_TRIP_UPDATION_MESSAGE:
            return
        contract_id = self.trip.customer_contract.id if self.trip.customer_contract else None
        support_numbers = _get_support_number_from_contract(contract_id)

        for item in support_numbers:
            phone_number = item['spoc_mobile']
            
            if self.trip.hub:
                template_dict = sms_templates.trip_rejected_hub
                message = template_dict['text'] % (self.driver, self.trip, self.trip.hub)
            else:
                template_dict = sms_templates.trip_rejected
                message = template_dict['text'] % (self.driver, self.trip)

            template_json = json.dumps(template_dict)
            send_sms.apply_async(args=[phone_number, message, template_json])

    def __reject_order(self):
        self.trip_data['status'] = TRIP_SUSPENDED
        self.stop_data['status'] = TRIP_SUSPENDED
        self.order_data['driver'] = None
        self.order_data['status'] = ORDER_NEW

        self.__save_updates()
        self.__send_sms_to_contract_spocs()
        self.__send_slack_notification()

    def __send_slack_notification(self):
        order = self.orders[0] if self.orders else None
        if order and self.trip:
            data = {
                'message': 'Trip Rejected !!',
                'order_type': order.order_type,
                'order_id': order.id,
                'order_number': order.number,
                'trip_number': self.trip.trip_number,
                'hub_name': str(self.trip.hub),
                'customer_name': str(order.customer),
                'contract_name': str(self.trip.customer_contract),
                'driver_details': ('%s | %s') % (str(self.driver), self.request.user.phone_number),
                'reason': self.reason,
                'track_url_host': self.request.META.get('HTTP_HOST', 'blowhorn.com')
            }
            send_unallocated_orders_slack_notification.apply_async((data,), )

    def __save_updates(self):
        logger.info('trip_data: %s' % self.trip_data)
        if self.trip and self.trip_data:
            self.trip_data['app_name'] = self.app_name
            self.trip_data['app_version'] = self.app_version
            self.trip = UpdateModelMixin().update(data=self.trip_data, instance=self.trip,
                                                  serializer_class=TripSerializer)
            self.trip.save()
        logger.info('order_data: %s' % self.order_data)
        if self.orders and self.order_data:
            for order in self.orders:
                if order.status == self.order_data.get('status', None):
                    self.order_data.pop('status') if self.order_data.get('status') else None
                if self.order_data.get('status', None):
                    self.context['geopoint'] = self.geopoint
                    self.context['vehicle_class'] = self.vehicle_class
                    self.context['vehicle_rc'] = self.vehicle_rc
                order = UpdateModelMixin().update(data=self.order_data, instance=order,
                                                  serializer_class=OrderSerializer, context=self.context)
                if order:
                    order.save()

        logger.info('stop_data: %s' % self.stop_data)
        if self.stops and self.stop_data:
            for stop_obj in self.stops:
                stop = UpdateModelMixin().update(data=self.stop_data, instance=stop_obj,
                                                 serializer_class=TripStopSerializer)
                stop.save()
