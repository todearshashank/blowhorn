# Django and System libraries
import json
import logging
from datetime import timedelta
from django.db.models import Q
from django.db import transaction
from django.utils import timezone

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.driver_app.v1.serializers.trip import TripStopSerializer
from blowhorn.common.helper import CommonHelper
from config.settings import status_pipelines as StatusPipeline
from config.settings.status_pipelines import (
    TRIP_IN_PROGRESS, TRIP_END_STATUSES, TRIP_NEW,
    TRIP_ALL_STOPS_DONE,
    ORDER_PICKED
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.apps.driver_app.constant import EXPECTED_DELIVERY_TIME_DELTA


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Stop = get_model('trip', 'Stop')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
DriverLedger = get_model('driver', 'DriverLedger')
Event = get_model('order', 'Event')


class PickupMixin(object):

    def _initialize_attributes(self, data, trip_id):
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.user = self.request.user
        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

        self.pickedup_order_qs = None
        # if self.stop.stop_type == PICKUP:
        picked_orders = data.get('pickup_order_list')
        self.pickedup_order_qs = self.__get_orders_from_ref_num(
            json.loads(picked_orders))

        if not self.pickedup_order_qs:
            self.error_message = 'Scanned orders are not valid'
            self.status_code = status.HTTP_400_BAD_REQUEST

        query = Q(pk=trip_id, status=TRIP_IN_PROGRESS)
        self.trip = Trip.objects.prefetch_queryset(
            query=query, stops_list=True, distinct_orders=True).first()
        self.order_stops = Stop.objects.filter(order__in=self.pickedup_order_qs,
                                               trip=self.trip,
                                               status__in=[TRIP_NEW,
                                                           TRIP_IN_PROGRESS])

        self.completed_stop_ids = []
        for stop in self.order_stops:
            self.completed_stop_ids.append(stop.id)

        # Take any one stop for route trace update out of all the pick stops getting completed at once.
        self.stop = self.order_stops[0] if self.order_stops else None

        if not self.trip or not self.stop or (self.trip != self.stop.trip):
            self.error_message = 'Invalid Data Sent !'

        self.next_stop = None
        self.no_of_pending_stops = None
        self.next_stop = None
        self.is_last_stop = False

        try:
            trip_last_response = json.loads(data.get('trip_last_response'))
            self.current_step = int(trip_last_response.get('current_step'))
            print('self.current_step - ', self.current_step)
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data Sent !'

        if self.error_message:
            self.response_data = self.error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return


    def __get_response_data(self):
        self.response_data = {
            'is_all_stops_done': self.trip.is_all_stops_done(),
            'current_step': self.trip.current_step,
            'is_trip_in_progress': True,
            'is_loaded': True,
            'next_stop_id': self.next_stop.id if self.next_stop else '',
            'completed_stops_id': self.completed_stop_ids
        }
        logger.info('Response data sent: %s' % self.response_data)

    def __get_pending_stops_and_next_stop(self):
        """
        No dependency on status(In-Progress, New) of current stop and next stop.

        hence won't fail in case of multiple requests
        """
        stop_qs = self.trip.stops.exclude(status__in=TRIP_END_STATUSES)
        no_of_pending_stops = len(stop_qs)
        in_progress_stop = None
        if no_of_pending_stops > 0:
            for stop in stop_qs:
                if stop.status == TRIP_IN_PROGRESS:
                    in_progress_stop = stop
                    break
            if not in_progress_stop:
                in_progress_stops = sorted(stop_qs, key=lambda x: x.sequence)
                in_progress_stop = in_progress_stops[0]
            return no_of_pending_stops, in_progress_stop
        return no_of_pending_stops, None

    def __make_updates(self):
        order_status = ORDER_PICKED
        expected_delivery_time = timezone.now() + timedelta(
                hours=EXPECTED_DELIVERY_TIME_DELTA)
        self.pickedup_order_qs.update(
            status=order_status,
            modified_date=timezone.now(),
            modified_by=self.user,
            expected_delivery_time=expected_delivery_time
        )
        # Add order event
        order_event_list = []
        for order in self.pickedup_order_qs:
            order_event_list.append(Event(status=order_status,
                                          order=order,
                                          driver=self.driver,
                                          current_location=self.geopoint))
        Event.objects.bulk_create(order_event_list)

        #Update stops associated with orders
        self.order_stops.update(status=StatusPipeline.TRIP_COMPLETED,
                                end_time=timezone.now())

        stop_data = {'status': StatusPipeline.TRIP_COMPLETED}

        if self.stop:
            self.stop = UpdateModelMixin().update(data=stop_data,
                                                  instance=self.stop,
                                                  serializer_class=TripStopSerializer)
            self.stop.save()

    def __update_trip(self):
        self.no_of_pending_stops, self.next_stop = self.__get_pending_stops_and_next_stop()
        self.is_last_stop = True if self.no_of_pending_stops == 0 else False

        if self.is_last_stop:
            self.trip.current_step = self.current_step + 1
            self.trip.status = TRIP_ALL_STOPS_DONE
            self.trip.save()

        if self.next_stop:
            self.next_stop.status = StatusPipeline.TRIP_IN_PROGRESS
            self.next_stop.save()


    def _attempt_pickup(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():
            self.__make_updates()
            self.__update_trip()
            no_of_pending_stops, self.next_stop = self.__get_pending_stops_and_next_stop()

        self.__get_response_data()


    def __get_orders_from_ref_num(self, ref_list):
        # qs = Order.objects.filter(reference_number__in=[int(i) for i in ref_list if i is not None])
        # print('qs - ', qs)

        # qs = Order.objects.filter(
        #     reference_number__in=[str(i) for i in ref_list if i is not None])
        qs = Order.objects.filter(
            reference_number__in=ref_list)
        return qs
