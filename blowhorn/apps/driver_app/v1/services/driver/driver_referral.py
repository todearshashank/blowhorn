"""
creating a Driver Record referred by the user.

request data-> refer_driver_details={'name': 'gurva', 'city_id': 1, 'phone_number': '9971153201'}
response_data-> Returns bonus_amount, created_date, bonus_generated

Created By: Gaurav Verma
"""
# Django and System libraries
import json
import phonenumbers
import logging
from django.db import transaction
from django.conf import settings

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.helper import get_detailed_data, get_user_for_driver
from blowhorn.apps.mixins import CreateModelMixin
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import REFERRED
from blowhorn.apps.driver_app.v1.serializers.driver import DriverSerializer


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Driver = get_model('driver', 'Driver')


class DriverReferral(object):
    """create a Driver Record referred by the user."""

    def _initialize_attributes(self, data):
        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.refer_driver_details = json.loads(data.get('refer_driver_details'))
        self.name = self.refer_driver_details.get('name', None)
        self.city_id = self.refer_driver_details.get('city_id', None)
        if not self.name or not self.city_id:
            self.error_message = 'Invalid Data !'
        try:
            self.phone = phonenumbers.parse(
                self.refer_driver_details.get('phone_number', ''),
                settings.DEFAULT_COUNTRY.get('country_code'))
        except:
            self.error_message = 'Invalid Phone Number !'
            return

        filter_data = {
            'user__phone_number': self.phone
        }
        existing_driver = Driver.objects.filter(**filter_data).first()
        if existing_driver:
            logger.error("Referred driver already exists")
            self.error_message = 'Driver already exists !'

    def _create_driver(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():
            detailed_data = get_detailed_data({
                'name': self.name,
                'mobile': self.refer_driver_details.get('phone_number', ''),
                'operating_city': self.city_id
            })
            user_data = detailed_data.get('user_data')
            driver_data = detailed_data.get('driver_data')
            referred_driver_user = get_user_for_driver(user_data)

            driver_data['user'] = referred_driver_user.id
            driver_data['status'] = REFERRED
            driver_data['referred_by'] = self.request.user.id

            referred_driver = CreateModelMixin().create(
                data=driver_data, serializer_class=DriverSerializer)

            self.response_data = self.serializer_class(
                instance=referred_driver, context={'request': self.request}).data
