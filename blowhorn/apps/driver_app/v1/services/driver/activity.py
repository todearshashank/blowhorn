"""
- Creation/Updation/Deletion of Driver Activity Record (along with Creation of Driver Activity History Record).

Created By: Gaurav Verma
"""
# System and Django libraries
import json
import logging
from django.db import transaction
from datetime import datetime
import pytz
from django.utils import timezone
from django.conf import settings

# 3rd Party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# Blowhorn libraries
from blowhorn.common.helper import CommonHelper
from blowhorn.apps.mixins import CreateModelMixin, UpdateModelMixin, DestroyModelMixin
from blowhorn.apps.driver_app.v1.serializers.driver import ActivitySerializer
from blowhorn.utils.functions import ist_to_utc
from config.settings.status_pipelines import TRIP_IN_PROGRESS, DRIVER_ACCEPTED, TRIP_ALL_STOPS_DONE
from blowhorn.driver.constants import DRIVER_ACTIVITY_IDLE, DRIVER_ACTIVITY_OCCUPIED
from blowhorn.driver.tasks import delete_driver_activity_from_firebase


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DriverActivity = get_model('driver', 'DriverActivity')
Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')


class ActivityMixin(object):
    """Create, Update and delete operations of activity and activity history records implementation."""

    def _initialize_attributes(self, data):
        """Process data from request to create driver activity and activity history records."""
        self.context = {'request': self.request}
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.data = {}

        if not data:
            self.error_message = 'No data sent'
            return

        self.driver = self.request.user.driver
        self.response_data = ''

        self.data['driver'] = self.driver.pk
        location = data.get('location_details', None)
        try:
            location = json.loads(location)
        except BaseException:
            self.error_message = 'Invalid Location json sent'
            return

        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        if not self.geopoint:
            self.error_message = 'Invalid location sent'
            return

        self.data['geopoint'] = self.geopoint
        if location:
            self.data['location_accuracy_meters'] = location.get('mAccuracy', 0)
            # Converting meters per second to km/hr
            self.data['vehicle_speed_kmph'] = int(location.get('mSpeed', 0) * 3.6)
            gps_timestamp_str = datetime.fromtimestamp(
                location.get('mTime', 0) / 1e3).strftime('%Y-%m-%d %H:%M:%S')
            self.data['gps_timestamp'] = pytz.timezone(settings.TIME_ZONE).localize(
                datetime.strptime(gps_timestamp_str, '%Y-%m-%d %H:%M:%S'))

            device_timestamp = data.get('device_timestamp')
            time_params_received = True
            if device_timestamp and not isinstance(device_timestamp, datetime):
                self.data['device_timestamp'] = ist_to_utc(
                    datetime.strptime(device_timestamp, '%Y-%m-%dT%H:%M:%S%z')
                )
            else:
                time_params_received = False
                self.data['device_timestamp'] = timezone.now()

            other_details = {}

            for i in list(set(data) - set(self.data)):
                other_details[i] = data[i]
            other_details['time_params_received'] = time_params_received
            self.data['other_details'] = json.loads(json.dumps(other_details))

        self.online = data.get('online') == 'true'

        order = None
        stop = None
        trip = None
        event = DRIVER_ACTIVITY_IDLE
        stop_str = None
        order_str = None
        trip_str = None

        trip = Trip.objects.filter(
            driver=self.driver,
            status__in=[TRIP_IN_PROGRESS, DRIVER_ACCEPTED, TRIP_ALL_STOPS_DONE]
        ).first()

        if trip:
            event = DRIVER_ACTIVITY_OCCUPIED
            trip_str = trip.trip_number

            stop = Stop.objects.filter(trip=trip, status=TRIP_IN_PROGRESS).first()
            stop_str = stop.pk if stop else None

            order = trip.order if trip.order else (stop.order if stop else None)
            order_str = order.number if order else None

        # **WARNING: Do not use following ForeignKey relations. Will be removed soon.
        self.data['trip'] = trip.id if trip else None
        self.data['order'] = order.id if order else None
        self.data['stop'] = stop.id if stop else None
        self.data['trip_str'] = trip_str
        self.data['order_str'] = order_str
        self.data['stop_str'] = stop_str
        self.data['event'] = event
        self.activity_obj = DriverActivity.objects.filter(driver=self.driver).first()

    def _activity_updates(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        if not self.activity_obj:
            if self.online:
                self.create_activity()
        else:
            if self.online:
                self.update_activity()
            else:
                self.delete_activity()

    def create_activity(self):
        """Create driver Activity and Activity History records."""
        try:
            CreateModelMixin().create(data=self.data, serializer_class=ActivitySerializer)
        except:
            logger.info('Failed to create activity')
            pass

    def update_activity(self):
        """Update driver Activity and Create Activity History records."""
        if not self.activity_obj:
            return
        self.activity_obj = UpdateModelMixin().update(data=self.data, instance=self.activity_obj,
                                                      serializer_class=ActivitySerializer)
        self.activity_obj.save()

    def delete_activity(self):
        """Delete driver Activity and Create Activity History records."""
        if not self.activity_obj:
            return
        DestroyModelMixin().destroy(instance=self.activity_obj)
        delete_driver_activity_from_firebase.apply_async(
            args=[self.driver.id]
        )
