"""
Updates existing FCM Device details for a user.

Delete existing FCM Device details and create new one

Input Data: fcm_id, device_id, user, driver_location_data

Response: Returns 'Successfully Updated' with 200 status

Created By: Gaurav Verma
"""
# Django and System libraries
import logging
from django.db.models import Q
from django.db import transaction

# 3rd party libraries
from rest_framework import status

# blowhorn imports
from blowhorn.apps.driver_app.v1.helpers.trip import create_app_event
from blowhorn.common.notification import Notification
from fcm_django.models import FCMDevice
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import FCM_UPDATED

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class FcmUpdate(object):
    """Updates existing FCM Device details for a user."""

    def _initialize_attributes(self, data):
        self.error_message = ''
        self.response_data = 'Successfully Updated'
        self.status_code = status.HTTP_200_OK
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.device_id = data.get("device_id")
        self.device_type = 'android'
        self.registration_id = data.get('fcm_id')
        self.driver = self.request.user.driver

        if not (self.registration_id or self.device_id):
            self.error_message = 'Invalid data sent'

    def _update_fcm(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():
            fcm_devices = FCMDevice.objects.filter(
                Q(user=self.request.user) & ~Q(device_id=self.device_id)).delete()
            logger.info('%s other devices deleted. Driver %s' % (fcm_devices, self.request.user))

            # Store Driver FCM Id and device ID
            Notification.add_fcm_key_for_user(
                self.request.user,
                self.registration_id,
                self.device_id,
                self.device_type
            )
            create_app_event(FCM_UPDATED, self.geopoint, driver=self.request.user.driver,)
