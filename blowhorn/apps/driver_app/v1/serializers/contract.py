# Django and System libraries

# 3rd party libraries
from rest_framework import serializers
from blowhorn.oscar.core.loading import get_model

# blowhorn imports


TripWorkFlow = get_model('contract', 'TripWorkFlow')
Screen = get_model('contract', 'Screen')
ScreenField = get_model('contract', 'ScreenField')
ScreenButton = get_model('contract', 'ScreenButton')

START_TRIP = 'start_trip'
FINISHED_LOADING = 'finished_loading'
REACH_LOCATION = 'reached_stop'
FINISH_LOCATION = 'finished_stop'
END_TRIP = 'end_trip'
# @todo need to be removed
endpoint_dict = {
    START_TRIP: '/api/drivers/v1/trips/%trip_id%/start',
    FINISHED_LOADING: '/api/drivers/v1/trips/%trip_id%/start',
    REACH_LOCATION: '/api/drivers/v1/trips/%trip_id%/stops/%stop_id%/attempt',
    FINISH_LOCATION: '/api/drivers/v1/trips/%trip_id%/stops/%stop_id%/attempt',
    END_TRIP: '/api/drivers/v1/trips/%trip_id%/end',
}


class ScreenFieldSerializer(serializers.ModelSerializer):
    queryset = ScreenField.objects.all().order_by('sequence')

    class Meta:
        model = ScreenField
        exclude = ('created_date', 'modified_date',
                   'created_by', 'modified_by')


class ScreenButtonSerializer(serializers.ModelSerializer):

    class Meta:
        model = ScreenButton
        exclude = ('created_date', 'modified_date',
                   'created_by', 'modified_by')


class ScreenSerializer(serializers.ModelSerializer):
    input_fields = serializers.SerializerMethodField(read_only=True)
    button_fields = ScreenButtonSerializer(many=True, required=False)
    # TODO need to be removed
    endpoint = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Screen
        exclude = ('created_date', 'modified_date',
                   'created_by', 'modified_by')

    def get_endpoint(self, screen):
        return endpoint_dict.get(screen.name)

    def get_input_fields(self, screen):
        return ScreenFieldSerializer(screen.input_fields.all().order_by('sequence'), many=True).data

    def to_representation(self, instance):
        ret = super(ScreenSerializer, self).to_representation(instance)
        ret['parameters'] = ret['parameters'] if ret['parameters'] else []

        return ret


class TripWorkFlowSerializer(serializers.ModelSerializer):
    queryset = TripWorkFlow.objects.all()
    screens = ScreenSerializer(many=True, required=False)

    class Meta:
        model = TripWorkFlow
        exclude = ('created_date', 'modified_date',
                   'created_by', 'modified_by')
