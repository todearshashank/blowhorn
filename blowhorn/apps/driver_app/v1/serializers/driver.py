# Django and System libraries
import pytz
import json
from django.utils import timezone
from django.conf import settings
from datetime import datetime
from django.utils.translation import ugettext_lazy as _

# 3rd party libraries
from rest_framework import serializers
from blowhorn.oscar.core.loading import get_model
from rest_framework.response import Response
from rest_framework import status

# blowhorn imports
from blowhorn.common.serializers import BaseSerializer, CustomDateTimeField
from blowhorn.common.helper import CommonHelper
from blowhorn.utils.crypt import Cipher
from blowhorn.utils.functions import ist_to_utc
from blowhorn.apps.driver_app.v1.constants import (
    ON_TRIP_LOCATION_UPDATE_INTERVAL,
    ON_TRIP_LOCATION_UPDATE_FAST_INTERVAL,
    OFF_TRIP_LOCATION_UPDATE_INTERVAL,
    ALARM_LOCATION_INTERVAL,
    OFFLINE_LOCATION_UPDATE_INTERVAL
)
from blowhorn.contract.constants import CONTRACT_TYPE_FIXED
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.constants import DRIVER_ACTIVITY_IDLE, \
    DRIVER_ACTIVITY_OCCUPIED, PERKFINANCE_MOBILE_NUMBER
from blowhorn.driver.tasks import delete_driver_activity_from_firebase
from blowhorn.apps.driver_app.v1.helpers.driver import _get_support_number
from blowhorn.apps.mixins import CreateModelMixin
from config.settings.status_pipelines import TRIP_IN_PROGRESS, DRIVER_ACCEPTED, \
    TRIP_ALL_STOPS_DONE
from dateutil.relativedelta import relativedelta

Driver = get_model('driver', 'Driver')
DriverError = get_model('driver', 'DriverError')
UserSession = get_model('users', 'UserSession')
DeviceDetails = get_model('driver', 'DeviceDetails')
Vehicle = get_model('vehicle', 'Vehicle')
Vendor = get_model('vehicle', 'Vendor')
DriverActivity = get_model('driver', 'DriverActivity')
DriverActivityHistory = get_model('driver', 'DriverActivityHistory')
DriverPayment = get_model('driver', 'DriverPayment')
DriverCashPaid = get_model('driver', 'DriverCashPaid')
DriverLedger = get_model('driver', 'DriverLedger')
Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
DriverReferralConfiguration = get_model('driver', 'DriverReferralConfiguration')
Stop = get_model('trip', 'Stop')
Order = get_model('order', 'Order')
City = get_model('address', 'City')


class VehicleSerializer(BaseSerializer):
    body_type_name = serializers.CharField(source='body_type.body_type')
    vehicle_model = serializers.CharField(source='vehicle_model.model_name')
    vehicle_class = serializers.CharField(
        source='vehicle_model.vehicle_class.commercial_classification')

    class Meta:
        model = Vehicle
        fields = (
            'vehicle_model', 'body_type_name', 'registration_certificate_number', 'vehicle_class',
            'model_year')


class DriverLoginSerializer(BaseSerializer):
    phone_number = serializers.SerializerMethodField(read_only=True)
    photo = serializers.SerializerMethodField(read_only=True)
    operating_city = serializers.SerializerMethodField()
    vehicles = serializers.SerializerMethodField()
    driver_rating = serializers.SerializerMethodField()
    support_phone_number = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()
    firebase_auth_token = serializers.SerializerMethodField()
    cities = serializers.SerializerMethodField()
    is_online = serializers.SerializerMethodField(read_only=True)
    perkfinance_phone_number = serializers.SerializerMethodField(read_only=True)
    show_perkfinance_info = serializers.SerializerMethodField(read_only=True)
    qr_encoded_value = serializers.SerializerMethodField(read_only=True)

    # is_tds_accpeted = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Driver
        fields = '__all__'

    def get_qr_encoded_value(self, obj):
        user = obj.user
        if user:
            dict_for_qr = dict(id=user.national_number)
            return Cipher.encode(json.dumps(dict_for_qr))
        return ''

    def get_is_online(self, driver):
        return DriverActivity.objects.filter(driver=driver).exists()

    # def get_is_tds_accpeted(self, driver):
    #     return driver.is_tds_accepted()

    def get_token(self, driver):
        if self.context:
            return self.context.get("token", '')

    def get_firebase_auth_token(self, driver):
        if self.context:
            return self.context.get("firebase_auth_token", '')

    def get_support_phone_number(self, driver):
        return _get_support_number(driver=driver)

    def get_phone_number(self, driver):
        return str(driver.user.phone_number)[-10:]

    def get_photo(self, driver):
        if driver.photo:
            media_url = CommonHelper().get_media_url_prefix()
            return media_url + str(driver.photo)
        return ''

    def get_operating_city(self, driver):
        return driver.operating_city.name if driver.operating_city else ''

    def get_vehicles(self, driver):
        vehicles = driver.vehicles.all()
        vehicles_response = []
        for vehicle in vehicles:
            vehicles_response.append(VehicleSerializer(
                vehicle,
                fields=["vehicle_model", "registration_certificate_number",
                        "vehicle_class"]).data)

        return vehicles_response

    def get_cities(self, driver):
        return City.objects.all().values('name', 'id')

    def get_driver_rating(self, driver):
        return driver.driver_rating.cumulative_rating if driver.driver_rating else 0.0

    def get_perkfinance_phone_number(self, driver):
        return PERKFINANCE_MOBILE_NUMBER

    def get_show_perkfinance_info(self, driver):
        start = timezone.now() - relativedelta(months=2)

        if driver.id in settings.PERKFINANCE_EXCLUDE_DRIVER:
            return True

        return Driver.objects.filter(
            id=driver.id,
            status=StatusPipeline.ACTIVE,
            resourceallocation__contract__contract_type=CONTRACT_TYPE_FIXED,
            own_vehicle=True, driverpayment__start_datetime__lte=start.date()).exists()


class DeviceDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceDetails
        fields = '__all__'


class UserSessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserSession
        fields = '__all__'


class ActivityHistorySerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        activity_history = DriverActivityHistory(**validated_data)
        activity_history.save()

        return activity_history

    class Meta:
        model = DriverActivityHistory
        fields = '__all__'


class ActivitySerializer(serializers.ModelSerializer):

    def create(self):
        data = self.get_validated_data()
        if data:
            driver_existing_activity = DriverActivity._default_manager.filter(
                driver=data.get('driver')).first()

            if driver_existing_activity is not None:
                return None

            driver_activity = DriverActivity(**data)
            try:
                driver_activity.save()
            except BaseException:
                print('Failed to create activity')

            # Dumping to DriverActivityHistory would be deprecated soon
            # driver_activity_history = DriverActivityHistory(**data)
            # driver_activity_history.save()

            return driver_activity

        return data

    def update(self):
        data = self.get_validated_data()
        if data:
            driver_existing_activity = DriverActivity._default_manager.filter(
                driver=data.get('driver')).first()
            if driver_existing_activity is None:
                return None

            try:
                # TODO whole code need to be changed for driver activity today itself.
                driver_activity = DriverActivity(**data)
                # Set the ID to update
                driver_activity.id = driver_existing_activity.id
                driver_activity.created_time = driver_existing_activity.created_time
                driver_activity.save()

                # Dumping to DriverActivityHistory would be deprecated soon
                # driver_activity_history = DriverActivityHistory(**data)
                # driver_activity_history.save()

                return driver_activity

            except:
                return None

        return data

    def delete(self):
        data = self.get_validated_data()
        if data:
            driver_existing_activity = DriverActivity._default_manager.filter(
                driver=data.get('driver')).first()

            if driver_existing_activity is not None:
                driver_existing_activity.delete()
                driver = data.get('driver')
                delete_driver_activity_from_firebase.apply_async(
                    args=[driver.id]
                )
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=_("Driver activity doesn't exists.")
                )

            # Dumping to DriverActivityHistory would be deprecated soon
            # driver_activity_history = DriverActivityHistory(**data)
            # driver_activity_history.save()

            return Response(
                status=status.HTTP_200_OK,
                data=_("Driver Activity Deleted.")
            )

        return data

    def get_validated_data(self):
        data = self.initial_data

        # Validate whether all required inputs are there or not?
        mandatory_fields = ['latitude', 'longitude',
                            'gps_timestamp', 'device_timestamp']

        for field in mandatory_fields:
            if field not in data:
                return False

        order = None
        stop = None
        trip = None

        event = DRIVER_ACTIVITY_IDLE
        stop_str = None
        order_str = None
        trip_str = None

        # trip_id = data.get('trip_id', None)

        # if trip_id:

        #     trip = Trip.objects.filter(pk=trip_id).first()

        trip = Trip.objects.filter(
            driver=data.get('driver'),
            status__in=[TRIP_IN_PROGRESS, DRIVER_ACCEPTED, TRIP_ALL_STOPS_DONE]
        ).first()

        if trip:
            event = DRIVER_ACTIVITY_OCCUPIED
            trip_str = trip.trip_number

            # stop = Stop.objects.filter(pk=data.get('stop_id', None)).first()

            # stop_str = data.get('stop_id')

            stop = Stop.objects.filter(trip=trip, status=TRIP_IN_PROGRESS).first()
            stop_str = stop.pk if stop else None

            # order_number = data.get('order_number', None)

            # if order_number:

            #     order = Order.objects.filter(number=order_number).first()

            #     order_str = order_number

            order = trip.order if trip.order else (stop.order if stop else None)
            order_str = order.number if order else None

        # Set / Derive the model fields

        validated_data = {}
        validated_data['driver'] = data.get('driver')

        # **WARNING: Do not use following ForeignKey relations. Will be removed soon.
        validated_data['trip'] = trip
        validated_data['order'] = order
        validated_data['stop'] = stop
        validated_data['trip_str'] = trip_str
        validated_data['order_str'] = order_str
        validated_data['stop_str'] = stop_str
        validated_data['gps_timestamp'] = pytz.timezone(
            settings.TIME_ZONE).localize(
            datetime.strptime(
                data.get('gps_timestamp'),
                '%Y-%m-%d %H:%M:%S'))
        device_timestamp = data.get('device_timestamp')
        time_params_received = True
        if device_timestamp and not isinstance(device_timestamp, datetime):
            validated_data['device_timestamp'] = ist_to_utc(
                datetime.strptime(
                    device_timestamp,
                    '%Y-%m-%dT%H:%M:%S%z'))
        else:
            time_params_received = False
            validated_data['device_timestamp'] = timezone.now()
        validated_data['location_accuracy_meters'] = data.get(
            'location_accuracy_meters', 0
        )
        validated_data['vehicle_speed_kmph'] = data.get(
            'vehicle_speed_kmph', 0
        )
        validated_data['geopoint'] = CommonHelper.get_geopoint_from_latlong(
            data.get('latitude'), data.get('longitude')
        )
        validated_data['event'] = event

        # All the keys not present in validated data will be encoded as other
        # details.

        other_details = {}
        for i in list(set(data) - set(validated_data)):
            other_details[i] = data[i]
        other_details['time_params_received'] = time_params_received
        validated_data['other_details'] = json.loads(json.dumps(other_details))

        return validated_data

    class Meta:
        model = DriverActivity
        fields = '__all__'


class DutyStatusSerializer(BaseSerializer):
    on_trip_location_update_interval = serializers.SerializerMethodField()
    on_trip_location_update_fast_interval = serializers.SerializerMethodField()
    off_trip_location_update_interval = serializers.SerializerMethodField()
    offline_location_update_interval = serializers.SerializerMethodField()
    alarm_location_interval = serializers.SerializerMethodField()
    support_phone_number = serializers.SerializerMethodField()

    class Meta:
        model = DriverActivity
        fields = ('on_trip_location_update_interval', 'on_trip_location_update_fast_interval',
                  'off_trip_location_update_interval', 'support_phone_number',
                  'offline_location_update_interval', 'alarm_location_interval')

    def get_support_phone_number(self, driver):
        return _get_support_number(driver=driver)

    def get_on_trip_location_update_interval(self, driver):
        return ON_TRIP_LOCATION_UPDATE_INTERVAL

    def get_on_trip_location_update_fast_interval(self, driver):
        return ON_TRIP_LOCATION_UPDATE_FAST_INTERVAL

    def get_off_trip_location_update_interval(self, driver):
        return OFF_TRIP_LOCATION_UPDATE_INTERVAL

    def get_offline_location_update_interval(self, driver):
        return OFFLINE_LOCATION_UPDATE_INTERVAL

    def get_alarm_location_interval(self, driver):
        return ALARM_LOCATION_INTERVAL


class DriverCashPaidSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverCashPaid
        exclude = ('modified_by', 'created_by',
                   'created_date', 'modified_date')


class DriverPaymentSerializer(serializers.ModelSerializer):
    start_datetime = serializers.DateField(format=settings.APP_DATE_FORMAT)
    end_datetime = serializers.DateField(format=settings.APP_DATE_FORMAT)
    breakups = serializers.SerializerMethodField()
    no_of_days = serializers.SerializerMethodField()
    base_amount_per_day_rate = serializers.SerializerMethodField()
    contract_name = serializers.SerializerMethodField()
    customer_name = serializers.SerializerMethodField()
    cash_paid_record = serializers.SerializerMethodField()
    trip_total_amount = serializers.SerializerMethodField()

    class Meta:
        model = DriverPayment
        exclude = ('modified_by', 'created_by', 'calculation', 'modified_date',
                   'created_date')

    # if zero value is not need to send to app
    # then uncomment the below code
    # def to_representation(self, value):
    #     repr_dict = super(DriverPaymentSerializer, self).to_representation(value)
    #     return OrderedDict((k, v) for k, v in repr_dict.items()
    #                        if v not in [None, [], '', {}, 0, "0.00"])

    def get_no_of_days(self, driver_payment):
        try:
            data = json.loads(driver_payment.calculation)
            base = data.get('base')
            return base.get('days') if base else 0
        except:
            return 0

    def get_trip_total_amount(self, driver_payment):
        return float(driver_payment.total_earnings + driver_payment.toll_charges + driver_payment.labour_cost)

    def get_customer_name(self, driver_payment):
        customer_name = ''
        if driver_payment.contract.customer:
            customer_name = driver_payment.contract.customer.name
        return customer_name

    def get_base_amount_per_day_rate(self, driver_payment):
        try:
            data = json.loads(driver_payment.calculation)
            base = data.get('base')
            return base.get('rate') if base else 0
        except:
            return 0

    def get_contract_name(self, driver_payment):
        return driver_payment.contract.name

    def get_cash_paid_record(self, driver_payment):
        return DriverCashPaidSerializer(driver_payment.drivercashpaid_set.all(), many=True).data

    def get_breakups(self, driver_payment):
        try:
            data = json.loads(driver_payment.calculation)

            variable_components = (
                ('Extra Kms', 'Km', data.get('distance')),
                ('Packages', '', data.get('shipment')),
            )
            attribute = None
            time = data.get('time')
            for t in time:
                attribute = t.get('attribute')
                break

            variable_components += ('Overtime', attribute, data.get('time')),
            trip_wise_breakup = []
            breakup = []
            for title, unit, components in variable_components:
                if not components:
                    continue

                for t in components:
                    slab = t.get('slab', {})

                    if t.get('trip'):
                        trip_amount = t.get('base_amount') + t.get('labour_cost', 0) + t.get('toll_charges', 0) + \
                                      t.get('amount')
                        date_ = datetime.strptime(t.get('date'), '%d-%b-%Y')

                        trip_wise_breakup.append({
                            "trip_number": t.get('trip'),
                            "base_payment": t.get('base_amount'),
                            "labour_amount": t.get('labour_cost', 0),
                            "toll_charges": t.get('toll_charges', 0),
                            "trip_date": date_.strftime('%b %d, %Y %H:%M'),
                            "trip_value": t.get('value'),
                            "trip_unit": unit,
                            "slab_start": slab.get('start'),
                            "slab_end": slab.get('end'),
                            "slab_rate": slab.get('rate'),
                            "slab_amount": t.get('amount'),
                            "slab_size": slab.get('step_size'),
                            "trip_amount": trip_amount
                        })
                    else:
                        breakup.append({
                            "trip_unit": unit,
                            "trip_value": t.get('value'),
                            "slab_start": slab.get('start'),
                            "slab_end": slab.get('end'),
                            "slab_rate": slab.get('rate'),
                            "slab_amount": t.get('amount'),
                            "slab_size": slab.get('step_size')
                        })

            return trip_wise_breakup + breakup
        except:
            return []


class DriverLedgerSerializer(serializers.ModelSerializer):
    transaction_time = CustomDateTimeField(format=settings.APP_DATETIME_FORMAT)
    created_date = CustomDateTimeField(format=settings.APP_DATETIME_FORMAT)
    modified_date = CustomDateTimeField(format=settings.APP_DATETIME_FORMAT)
    amount_paid = serializers.SerializerMethodField()

    class Meta:
        model = DriverLedger
        exclude = ('modified_by', 'created_by', 'amount')

    def get_amount_paid(self, ledger):
        return abs(ledger.amount)


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = '__all__'


class DriverReferralConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = DriverReferralConfiguration
        fields = ('bonus_amount', 'min_active_duration')


class DriverReferralSerializer(serializers.ModelSerializer):
    bonus_amount = serializers.SerializerMethodField()
    created_date = CustomDateTimeField(format=settings.APP_DATETIME_FORMAT)
    bonus_generated = serializers.SerializerMethodField()

    class Meta:
        model = Driver
        fields = ('name', 'created_date', 'bonus_generated', 'bonus_amount')

    def get_bonus_generated(self, driver):
        return 'Bonus Generated' if driver.bonus_generated else 'Bonus not generated yet'

    def get_bonus_amount(self, driver):
        return driver.referral_config.bonus_amount if driver.referral_config else ''


class DriverErrorSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        error_obj = DriverError(**validated_data)
        error_obj.save()
        return error_obj

    class Meta:
        model = DriverError
        fields = '__all__'


class DriverVehicleSerializer(BaseSerializer):
    vehicles = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    id = serializers.SerializerMethodField()

    class Meta:
        model = Driver
        fields = ('id', 'name', 'vehicles',)

    def get_name(self, payment):
        return payment.driver.name

    def get_id(self, payment):
        return payment.driver_id

    def get_vehicles(self, payment):
        vehicles = payment.driver.vehicles.all()

        return VehicleSerializer(
            vehicles, fields=["vehicle_model", "registration_certificate_number", "vehicle_class"], many=True).data


class VendorSerializer(BaseSerializer):
    drivers = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()

    class Meta:
        model = Vendor
        fields = ['id', 'name', 'drivers', 'token', 'phone_number']

    def get_drivers(self, vendor):
        payments = DriverPayment.objects.filter(owner_details=vendor).distinct('driver_id')

        return DriverVehicleSerializer(payments, many=True).data

    def get_token(self, driver):
        if self.context:
            return self.context.get("token", '')
