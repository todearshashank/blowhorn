# Django and System libraries

# 3rd party libraries
from rest_framework import serializers
from blowhorn.oscar.core.loading import get_model

# blowhorn imports


Customer = get_model('customer', 'Customer')
CustomerRating = get_model('customer', 'CustomerRating')


class CustomerRatingSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        rating = CustomerRating(**validated_data)
        rating.save()
        return rating

    def update(self, instance, validated_data):
        for attr in validated_data:
            setattr(instance, attr, validated_data.get(attr))

        return instance

    class Meta:
        model = CustomerRating
        fields = '__all__'


class CustomerSerializer(serializers.ModelSerializer):

    phone_number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Customer
        fields = ['name', 'phone_number']

    def get_phone_number(self, customer):
        return str(customer.user.phone_number) if customer.user_id else ''
