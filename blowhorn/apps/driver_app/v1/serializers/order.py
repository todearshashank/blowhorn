"""Order model Related serializers to be added here regarding driver app."""
# Django and System libraries
import json
import re
import phonenumbers
from django.conf import settings
from django.utils.translation import gettext as _
from django.utils import timezone
from django.db.models import Sum
from phonenumber_field.phonenumber import PhoneNumber
# 3rd party libraries
from rest_framework import serializers, exceptions
from blowhorn.oscar.core.loading import get_model
from blowhorn.oscarapi.utils.settings import overridable
from blowhorn.oscarapi.serializers.checkout import BillingAddressSerializer

# blowhorn imports
from config.settings.status_pipelines import ORDER_END_STATUSES
from blowhorn.order.mixins import OrderPlacementMixin
from blowhorn.address.utils import get_hub_from_pincode
from blowhorn.apps.driver_app.v1.serializers.customer import CustomerSerializer
from blowhorn.common.serializers import CustomDateTimeField, BaseSerializer
from config.settings import status_pipelines as StatusPipeline


Order = get_model('order', 'Order')
Labour = get_model('order', 'Labour')
WayPoint = get_model('order', 'WayPoint')
ShippingAddress = get_model('order', 'ShippingAddress')
Country = get_model('address', 'Country')
OrderLine = get_model('order', 'OrderLine')


class ShippingAddressSerializer(serializers.ModelSerializer):
    queryset = ShippingAddress.objects.all()

    class Meta:
        model = ShippingAddress
        exclude = ('country', )


class WayPointSerializer(serializers.ModelSerializer):

    shipping_address = ShippingAddressSerializer()

    class Meta:
        model = WayPoint
        fields = '__all__'


class LabourSerializer(serializers.ModelSerializer):

    class Meta:
        model = Labour
        fields = ('no_of_labours', 'labour_cost')


class OrderLineSerializer(serializers.ModelSerializer):

    sku = serializers.SerializerMethodField(read_only=True)

    def get_sku(self, orderline):
        if orderline.sku:
            return orderline.sku.name
        return None

    class Meta:
        model = OrderLine
        fields = ('sku_id', 'sku', 'quantity', 'delivered')


class OrderSerializer(BaseSerializer):

    shipping_address = ShippingAddressSerializer(
        many=False, required=False)
    pickup_address = ShippingAddressSerializer(
        many=False, required=False)
    num_of_stops = serializers.SerializerMethodField(read_only=True)
    customer = CustomerSerializer(many=False, required=False)
    pickup_datetime = CustomDateTimeField(format=settings.APP_DATETIME_FORMAT, required=False)
    date_placed = CustomDateTimeField(format=settings.APP_DATETIME_FORMAT, required=False)
    labour = serializers.SerializerMethodField(read_only=True)
    order_lines = serializers.SerializerMethodField(read_only=True)

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop('exclude_fields') if kwargs.get('exclude_fields', None) else []
        super(OrderSerializer, self).__init__(*args, **kwargs)
        self.geopoint = self.context.get("geopoint", None) if self.context else None
        self.remarks = self.context.get("remarks", None) if self.context else None
        self.hub_associate = self.context.get("hub_associate", None) if self.context else None
        self.vehicle_class = self.context.get("vehicle_class", None) if self.context else None
        self.vehicle_rc = self.context.get("vehicle_rc", None) if self.context else None

        for field in exclude_fields:
            self.fields.pop(field)

    def validate(self, attrs):
        if attrs.get('status', None):
            attrs['updated_time'] = timezone.now()
            event_data = {}
            event_data['status'] = attrs.get('status')
            event_data['current_location'] = self.geopoint
            event_data['remarks'] = self.remarks
            event_data['hub_associate'] = self.hub_associate
            attrs['event_data'] = event_data

            if attrs['status'] in ORDER_END_STATUSES:
                attrs['calculate_gps_distance'] = True
        if self.vehicle_class:
            attrs['vehicle_class'] = self.vehicle_class
        if self.vehicle_rc:
            attrs['vehicle_rc'] = self.vehicle_rc

        return attrs

    def update(self, instance, validated_data):
        for attr in validated_data:
            if attr == 'event_data' and instance.driver:
                validated_data['event_data']['driver'] = instance.driver
            if attr == 'calculate_gps_distance':
                instance.total_distance = instance.trip_set.all().aggregate(
                    distance=Sum('gps_distance')).get('distance', 0)
            setattr(instance, attr, validated_data.get(attr))
        return instance

    def to_representation(self, instance):
        ret = super(OrderSerializer, self).to_representation(instance)
        ret['cost_components'] = json.loads(ret['cost_components']) if ret.get('cost_components') \
                                                                       and instance.status == StatusPipeline.ORDER_DELIVERED else []
        if instance.customer and (instance.customer.credit_amount > 0 or instance.customer.credit_period > 0):
            ret['total_amount'] = float(0.0)
            ret['allow_payment_selection'] = False
        else:
            ret['total_amount'] = float(instance.total_payable) - float(instance.amount_paid_online)
            ret['allow_payment_selection'] = settings.ALLOW_PAYMENT_SELECTION if instance.payment_mode else True

        ret['items'] = list(instance.item_category.values_list('category', flat=True))

        return ret

    def get_num_of_stops(self, order):
        return len(order.waypoint_set.all()) - 1

    def get_labour(self, order):
        labour = Labour.objects.filter(order=order).last()
        if labour:
            return LabourSerializer(labour).data
        return None

    def get_order_lines(self, order):
        if order.order_lines:
            return OrderLineSerializer(order.order_lines, many=True, required=False).data
        return None

    class Meta:
        model = Order
        # fields = '__all__'
        exclude =('dispatch_log', 'calculation_details', 'invoice_details',)
        read_only_fields = ('number', 'reference_number', 'customer')
        # fields = ('id', 'number', 'reference_number', 'status', 'pickup_datetime',
        #           'expected_delivery_time', 'cash_on_delivery', 'shipping_address',
        #           'pickup_address', 'cost_components', 'payment_mode', 'items',
        #           'num_of_stops', 'customer', 'labour', 'date_placed', 'driver')


class CreateOrderOnTheRunSerializer(serializers.Serializer, OrderPlacementMixin):

    total = serializers.DecimalField(
        decimal_places=2, max_digits=12, required=False)
    shipping_address = ShippingAddressSerializer(many=False, required=False)
    billing_address = BillingAddressSerializer(many=False, required=False)
    pickup_address = ShippingAddressSerializer(
        many=False, required=False)
    lines = serializers.HyperlinkedIdentityField(
        view_name='order-lines-list')

    def validate(self, attrs):
        request = self.context['request']
        data = request.data
        valid_parameters = ['trip_id', 'customer', 'customer_id', 'customer_email', 'customer_mobile',
                            'customer_name', 'delivery_address', "customer_id",
                            'delivery_lat', 'delivery_lon', 'store_code',
                            'delivery_postal_code', 'expected_delivery_time',
                            'cash_on_delivery', 'order_creation_time', "total_payable",
                            'reference_number', 'shipment_info', 'is_offline_order',
                            'csv_upload', 'pickup_address', "payment_mode", "device_type",
                            'pickup_postal_code', 'items', 'customer_contract',
                            'lines', 'created_by_driver', "bill_number", "bill_date", "order_mode", "remarks"
                            ]

        # Raise exception if any extra params present in data
        for param in data:
            if param not in valid_parameters:
                message = _('%s is not a valid parameter' % param)
                _out = {
                    'status': 'FAIL',
                    'message': message
                }
                raise exceptions.ParseError(_out)

        shipment_info = {}
        customer_name = data.get('customer_name', None)
        if customer_name:
            shipment_info['customer_name'] = customer_name

        customer_mobile = data.get('customer_mobile', None)
        if not customer_mobile:
            _out = {
                'status': 'FAIL',
                'message': 'Customer mobile number is mandatory'
            }
            raise exceptions.ParseError(_out)
        try:
            customer_phone = PhoneNumber.from_string(
                customer_mobile, settings.DEFAULT_COUNTRY.get('country_code'))
            if not customer_phone.is_valid():
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid customer mobile number'
                }
                raise exceptions.ParseError(_out)
        except phonenumbers.NumberParseException:
            _out = {
                'status': 'FAIL',
                'message': 'Invalid customer mobile number'
            }
            raise exceptions.ParseError(_out)

        shipment_info['customer_mobile'] = customer_mobile

        customer_email = data.get('customer_email', None)
        if customer_email:
            if not re.search(settings.EMAIL_REGEX, customer_email):
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid customer email id'
                }
                raise exceptions.ParseError(_out)
            shipment_info['customer_email'] = customer_email

        shipping_address = data.get('delivery_address', None)
        if shipping_address:
            shipping_postal_code = data.get('delivery_postal_code', None)

            country_code = data.get('country_code', settings.DEFAULT_COUNTRY.get('country_code'))
            postal_code_regex = ShippingAddress.POSTCODES_REGEX.get(country_code)
            if shipping_postal_code and not re.search(postal_code_regex, shipping_postal_code):
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid delivery postal code'
                }
                raise exceptions.ParseError(_out)
            shipping_lat = data.get('delivery_lat', None)
            shipping_lon = data.get('delivery_lon', None)
            attrs['shipping_address'] = self.get_updated_address(
                shipping_address,
                shipping_postal_code,
                shipping_lat,
                shipping_lon,
            )
            attrs['pickup_address'] = attrs['shipping_address']
        else:
            _out = {
                'status': 'FAIL',
                'message': 'No delivery Address'
            }
            raise exceptions.ParseError(_out)

        hub = get_hub_from_pincode(shipping_postal_code) if shipping_postal_code else None
        if hub:
            attrs['pickup_address'] = {
                'line1': hub.address.line1,
                'postcode': hub.address.postcode,
            }
            if hub.address.geopoint:
                coords = hub.address.geopoint.coords
                attrs['pickup_address'].update({
                    'latitude': coords[1],
                    'longitude': coords[0]
                })

        if customer_mobile:
            attrs['shipping_address']['phone_number'] = customer_mobile

        if customer_name:
            attrs['shipping_address']['first_name'] = customer_name

        attrs['shipment_info'] = shipment_info
        attrs['hub'] = hub
        attrs['order_type'] = settings.SHIPMENT_ORDER_TYPE
        attrs['customer'] = data.get('customer')
        attrs['customer_contract'] = data.get('customer_contract')
        attrs['date_placed'] = timezone.now()
        attrs['cash_on_delivery'] = data.get('cash_on_delivery', 0.00)
        attrs['lines'] = data.get('lines', None)
        attrs['created_by_driver'] = data.get('created_by_driver')

        return attrs

    def create(self, validated_data):

        try:
            request = self.context['request']
            response = self.place_order(
                user=request.user,
                order_data=validated_data,
            )
            return response
        except ValueError as e:
            raise exceptions.ParseError(str(e))

    class Meta:
        model = Order
        fields = overridable('OSCARAPI_ORDER_FIELD', default=(
            'number', 'owner', 'billing_address', 'currency', 'total_incl_tax',
            'total_excl_tax', 'shipping_incl_tax', 'shipping_excl_tax',
            'shipping_address', 'status', 'date_placed')
        )
