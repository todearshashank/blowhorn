"""
Contains All the View Classes that are solely related to drivers.

Login, Logout, Duty Status, Driver Activity, Earnings, Referrals etc...

Created by: Gaurav Verma
"""
# Django and System libraries.
import json
import logging
import datetime
from datetime import timedelta
import traceback
import random
import pytz
from dateutil import parser

# 3rd party libraries
from rest_framework import status
from rest_framework import generics, views
from rest_framework.decorators import permission_classes
from django.shortcuts import render_to_response
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from blowhorn.oscar.core.loading import get_model
from django.utils import timezone
from django.conf import settings
from dateutil.relativedelta import relativedelta

# blowhorn imports
from blowhorn.apps.driver_app.v1.serializers.driver import (
    DriverLoginSerializer, DriverPaymentSerializer,
    DriverLedgerSerializer, DutyStatusSerializer,
    DriverReferralSerializer
)

from blowhorn.document.models import DocumentType, DocumentEvent
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT, VERIFICATION_PENDING
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.driver import constants as driver_constants
from blowhorn.driver.permissions import IsActiveDriver
from blowhorn.common import serializers as common_serializer
from blowhorn.apps.driver_app.v1.services.driver.login import LogIn
from blowhorn.apps.driver_app.v1.services.driver.fcm import FcmUpdate
from blowhorn.apps.driver_app.v1.services.driver.logout import LogOut
from blowhorn.apps.driver_app.v1.services.driver.driver_referral import DriverReferral
# from blowhorn.apps.driver_app.v1.services.driver.activity import ActivityMixin
from blowhorn.apps.driver_app.utils import _get_api_reponse
from blowhorn.apps.driver_app.v1.helpers.driver import create_error_object
from config.settings.status_pipelines import TRIP_COMPLETED
# TODO need to be removed
from rest_framework.response import Response
from blowhorn.driver.serializers import DriverActivitySerializer
from blowhorn.users.permission import HasValidSession
from blowhorn.common import sms_templates
from blowhorn.driver.constants import TOS_ACCEPTED, TOS_REJECTED
from blowhorn.driver.serializers import DriverDocumentSerializer
from blowhorn.vehicle.serializers import VehicleDocumentSerializer, VendorDocumentSerializer
from blowhorn.driver.models import DriverDocument, APPROVED, DriverTds, UNAPPROVED
from blowhorn.vehicle.models import VehicleDocument, VendorDocument
from blowhorn.address.models import State
from blowhorn.address import utils as address_utils
from blowhorn.contract.models import Contract
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE
from blowhorn.utils.functions import minutes_to_dhms

DriverActivity = get_model('driver', 'DriverActivity')
DriverGPSInformation = get_model('driver', 'DriverGPSInformation')
City = get_model('address', 'City')
DriverPayment = get_model('driver', 'DriverPayment')
DriverCashPaid = get_model('driver', 'DriverCashPaid')
DriverLedger = get_model('driver', 'DriverLedger')
PaymentTrip = get_model('driver', 'PaymentTrip')
Driver = get_model('driver', 'Driver')
Vendor = get_model('vehicle', 'Vendor')
DriverReferrals = get_model('driver', 'DriverReferrals')
DriverLogInfo = get_model('driver', 'DriverLogInfo')
Order = get_model('order', 'Order')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([])
class Login(generics.CreateAPIView, LogIn):
    """
    Driver Authentication to be done here.

    through 1. QR Code Scan or 2. mobile + password

    Deletes all user sessions and django sessions.

    Saves the new session for user.

    POST(phone_number, qr_code data, password, device_id, fcm_id, device_details, location_details)

    Updates Driver Device details and driver log info
    and the response with the user token to be provided.

    On Success: Returns User session token, Firebase Auth token and driver Info with 200 status
    On Failure: Returns 500 with the error.

    """

    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)
    serializer_class = DriverLoginSerializer

    def post(self, request):
        """POST method for driver login changes."""
        data = request.data.dict()
        if not self._is_valid_build_code(data.get('app_code', None)):
            return Response(status=400, data=_('Update the app from playstore..!'))

        self._initialize_attributes(data)
        try:
            self._login()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Logout(generics.CreateAPIView, LogOut):
    """
    Driver Logout changes to be done here.

    Logging out the driver.
    Remove Fcm Key for user
    Delete User's Session record

    POST(user, location_details)

    On Success: Returns 'Driver Logged Out successfully' with 200 status
    On Failure: Returns 500 with the error.
    """

    permission_classes = (IsActiveDriver, IsAuthenticated)

    def post(self, request):
        """POST method for driver logout changes."""
        data = request.data.dict()
        self._initialize_attributes(data)
        try:
            if data.get('online') == 'true':
                request.data._mutable = True
                request.data['online'] = 'false'
                DutyStatus().post(request)
            self._do_logout()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint, )

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Fcm(generics.CreateAPIView, FcmUpdate):
    """
    Updates existing FCM Device details for a user.

    Delete existing FCM Device details and create new one

    POST(fcm_id, device_id, user, location_details)

    On Success: Returns 'Successfully Updated' with 200 status
    On Failure: Returns 500 with the error.
    """

    permission_classes = (IsActiveDriver, IsAuthenticated, HasValidSession)

    def post(self, request):
        """POST method for Updating existing FCM Device for a user."""
        data = request.data.dict()
        self._initialize_attributes(data)

        try:
            self._update_fcm()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint, )

        return _get_api_reponse(self.status_code, self.error_message,
                                self.response_data, content_type='charset utf-8; text/plain')


class Tos(views.APIView):
    permission_classes = (IsActiveDriver, IsAuthenticated, HasValidSession)

    def get_oldest_unaccepted_tds_year(self, query, active_from):

        from blowhorn.driver.util import get_financial_year
        today = timezone.now().date() - relativedelta(years=2)
        date = max(today,
                   datetime.datetime(2018, 4, 1).date(), active_from.date() if active_from else today)
        financial_year = ''
        while date.year <= timezone.now().year:
            financial_year = get_financial_year(date)
            tds_query = query & Q(year=financial_year, is_tds_accepted=True)
            is_tds_accepted = DriverTds.objects.filter(tds_query)
            date += relativedelta(years=1)
            if is_tds_accepted:
                financial_year = ''
            else:
                break
        return financial_year

    def get(self, request):

        driver, vendor = None, None
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        if driver:
            query = Q(driver=driver)
            active_from = driver.active_from
        else:
            vendor = request.user.vendor
            query = Q(vendor=vendor)
            active_from = None

        if (driver and driver.tos_accepted) or (vendor and vendor.tos_accepted):
            tos_status = TOS_ACCEPTED
        else:
            tos_status = TOS_REJECTED
        financial_year = self.get_oldest_unaccepted_tds_year(query, active_from)

        if not ((driver and driver.pan and
         driver.own_vehicle) or (vendor and vendor.pan)):
            financial_year = ''
        data = {
            'driver_tos': tos_status,
            'is_tds_accepted': False if financial_year else True,
            'unaccepted_tds_year': financial_year,
            'tos_link': '/drivertermsofservice'
        }

        return Response(status=status.HTTP_200_OK, data=data)

    def post(self, request):
        """
        Driver TOS Accept/Reject APi
        :param request:
        :return:
        """
        data = request.data.dict()
        driver_tos = data.get('driver_tos')
        if driver_tos == TOS_ACCEPTED:
            tos_accepted = True
        else:
            tos_accepted = False

        driver = request.user.driver if hasattr(request.user, 'driver') else None
        if driver:
            Driver.objects.filter(id=driver.id).update(tos_accepted=tos_accepted, tos_response_date=timezone.now())
        else:
            vendor = request.user.vendor
            Vendor.objects.filter(id=vendor.id).update(tos_accepted=tos_accepted, tos_response_date=timezone.now())

        return Response(status=status.HTTP_200_OK)


class Activity(generics.CreateAPIView, generics.UpdateAPIView):
    """

    API endpoint to log the driver activity.

    i.e. Current Location, Accuracy meters,

    Vehicle Speed, Battery Percentage etc...

    Permissions added.Only Active Drivers can access this api

    POST(location_details)

    On Success: Returns {'status': True}
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    # serializer_class = ActivitySerializer
    serializer_class = DriverActivitySerializer

    def post(self, request, data=None):
        """POST method for saving driver Activity details."""
        context = {'request': request}
        if not data:
            data = request.data.dict()
        response = None
        driver = request.user.driver
        data['driver'] = driver
        location = data.get('location_details', None)
        try:
            location = json.loads(location)
        except BaseException:
            logger.error('Invalid Location json sent')
            return Response(status=status.HTTP_200_OK,
                            data="Invalid Location json sent")
        if location:
            data['latitude'] = location.get('mLatitude')
            data['longitude'] = location.get('mLongitude')
            data['location_accuracy_meters'] = location.get('mAccuracy')
            # Converting meters per second to km/hr
            data['vehicle_speed_kmph'] = int(location.get('mSpeed', 0) * 3.6)
            data['gps_timestamp'] = datetime.datetime.fromtimestamp(
                location.get('mTime', 0) / 1e3).strftime('%Y-%m-%d %H:%M:%S')
        online = data.get('online')
        if online == 'true':
            data['event'] = driver_constants.ONLINE_DRIVER_ACTION
        else:
            data['event'] = driver_constants.OFFLINE_DRIVER_ACTION
        driver_activity_serializer = self.serializer_class(
            data=data, context=context)
        driver_existing_activity = None
        try:
            driver_existing_activity = DriverActivity.objects.get(
                driver=driver)
        except DriverActivity.DoesNotExist:
            if online == 'true':
                # In case there is no driver activity if "off_duty" is sent
                # as event in data, DO NOTHING
                response = driver_activity_serializer.create()
                if not response:
                    logger.error(
                        'Driver activity creation failed for driver:- "%s-%s"',
                        driver.user.name, driver.user.phone_number)
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=_("Driver activity creation failed."))
                logger.info('Created Driver Activity for driver:- "%s-%s"',
                            driver.user.name,
                            driver.user.phone_number.national_number)
        if driver_existing_activity:
            if online == 'false':
                driver_activity_serializer.delete()
                logger.info('Deleted Driver Activity for driver:- "%s-%s"',
                            driver.user.name,
                            driver.user.phone_number.national_number)
            else:
                response = driver_activity_serializer.update()
                if not response:
                    logger.error(
                        'Driver activity updation failed for driver:- "%s-%s"',
                        driver.user.name, driver.user.phone_number)
                    Response(status=status.HTTP_200_OK, data={'status': True})
                    # return Response(status=status.HTTP_400_BAD_REQUEST,
                    #                 data=_("Driver activity updation failed."))
                logger.info(
                    'Driver Activity Update Successful for driver:- "%s-%s"',
                    driver.user.name, driver.user.phone_number.national_number)
        return Response(status=status.HTTP_200_OK, data={'status': True})


# TODO mutilple request
class DutyStatus(views.APIView):
    """
    API to Log the Driver Duty Status(online/offline) and Activity.

    Permissions added.Only Active Drivers can access this api

    POST(online, location_details)
    {
        "online": "true" / "false"
    }
    On Success: Returns Time Intervals after which each driver activity call will be made from app
    On Failure: Returns 500 with the error.
    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = DutyStatusSerializer

    def post(self, request):
        """POST method for Logging the Driver Duty Status(online/offline) and Activity."""
        data = request.data.dict()
        online = data.get('online')
        error_message = ''
        response_data = ''
        status_code = status.HTTP_200_OK
        driver = request.user.driver

        if online == 'true':
            action = driver_constants.ONLINE_DRIVER_ACTION
        else:
            action = driver_constants.OFFLINE_DRIVER_ACTION

        try:
            if online != 'true':
                Activity.as_view()(request._request, data)
            request.user.driver.update_driver_log_info(action)

            response_data = self.serializer_class(
                driver, context={'request': request}).data
        except BaseException as e:
            error_message = ('Error: %s, Call Support' % e)
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=None, )

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)


class Statement(generics.RetrieveAPIView):
    """Show Previous Settlement amount of driver and next settlement."""

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = DriverLedgerSerializer

    def get(self, request):
        """GET method for getting ledger data of driver."""
        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor') == 'true' else False
        driver = request.user.driver if not is_vendor else Driver.objects.get(id=data.get('driver_id'))
        error_message = ''
        geopoint = None
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED
        vendor = request.user.vendor if is_vendor else ''
        response_data = {
            "count": 0,
            "previous": 0,
            "next": 0,
            "result": [],
            "unprocessed_trips_count": 0,
            "next_settlement": 0.0
        }

        query = Q(driver=driver, is_settlement=True)

        if is_vendor:
            query = query & Q(owner_details=vendor)

        if driver.own_vehicle or is_vendor:
            driver_ledger = DriverLedger.objects.filter(
                query).order_by('-transaction_time')

            unprocessed_trips = PaymentTrip.objects.filter(
                payment__status=driver_constants.UNAPPROVED,
                payment__driver=driver,
                trip__status=TRIP_COMPLETED).distinct('trip').count()

            next_settlement_amount = DriverLedger.objects.filter(
                driver=driver).values_list('balance').order_by('-transaction_time').first()

            logging.info('Page to be displayed : %s' % next)
            try:
                driver_ledger_serializer = common_serializer.PaginatedSerializer(
                    queryset=driver_ledger,
                    num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                    page=next,
                    serializer_method=self.serializer_class)
                response_data = driver_ledger_serializer.data
                response_data['unprocessed_trips_count'] = unprocessed_trips
                response_data['next_settlement'] = float(next_settlement_amount[0]) if next_settlement_amount else 0.0

            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                logger.error(traceback.format_exc())
                create_error_object(driver, traceback.format_exc(),
                                    request.build_absolute_uri(),
                                    current_location=geopoint, )

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)


# TODO need to change url from earnings to payment
class Payments(generics.RetrieveAPIView):
    """
    Returns driver Payments paginated data.

    Permissions added.Only Active Drivers can access this api
    GET()
    {
        "next": 0
    }

    On Success: Will return 200 and the payments data as json
    On Failure: Will return 500 with the error.
    """

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = DriverPaymentSerializer

    def get(self, request):
        """GET method for getting payments data of driver."""
        driver = request.user.driver
        data = request.GET.dict()
        error_message = ''
        response_data = {}
        geopoint = None
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        if driver.own_vehicle:
            payment_records = DriverPayment.objects.filter(
                driver=driver, status=driver_constants.APPROVED, calculation__isnull=False)
            payment_records = payment_records.prefetch_related(
                'drivercashpaid_set').order_by('-start_datetime')
            try:
                driver_payment_serializer = common_serializer.PaginatedSerializer(
                    queryset=payment_records,
                    num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                    page=next,
                    serializer_method=self.serializer_class)
                response_data = driver_payment_serializer.data
            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                logger.error(traceback.format_exc())
                create_error_object(driver, traceback.format_exc(),
                                    request.build_absolute_uri(),
                                    current_location=geopoint, )

        # TODO need to be removed
        return Response(status=status_code,
                        data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)


class LedgerStatement(generics.RetrieveAPIView):
    """
    Returns driver ledger paginated data.

    Permissions added.Only Active Drivers can access this api
    GET()
    {
        "next": 0
    }

    On Success: Will return 200 and the ledger data as json
    On Failure: Will return 500 with the error.
    """

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = DriverLedgerSerializer

    def get(self, request):
        """GET method for getting ledger data of driver."""
        driver = request.user.driver
        data = request.GET.dict()
        error_message = ''
        response_data = {}
        geopoint = None
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        if driver.own_vehicle:
            driver_ledger = DriverLedger.objects.filter(
                driver=driver).order_by('-transaction_time')

            logging.info('Page to be displayed : %s' % next)
            try:
                driver_ledger_serializer = common_serializer.PaginatedSerializer(
                    queryset=driver_ledger,
                    num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                    page=next,
                    serializer_method=self.serializer_class)
                response_data = driver_ledger_serializer.data
            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                logger.error(traceback.format_exc())
                create_error_object(driver, traceback.format_exc(),
                                    request.build_absolute_uri(),
                                    current_location=geopoint, )

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)


class DocumentUpload(generics.CreateAPIView):
    """
    Document upload from driver app.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsActiveDriver, HasValidSession)

    def get_spoc(self, driver=None, vendor=None, vehicle=None):

        query = None
        if driver:
            query = Q(driver_document__driver=driver, status=VERIFICATION_PENDING)
        elif vendor:
            query = Q(vendor_document__vendor=vendor, status=VERIFICATION_PENDING)
        elif vehicle:
            query = Q(vendor_document__vendor=vendor, status=VERIFICATION_PENDING)

        if query:
            spoc = DocumentEvent.copy_data.filter(query).first()
            if spoc and spoc.action_owner_id:
                return spoc.action_owner_id
            spoc_query = Q(status=CONTRACT_STATUS_ACTIVE, spocs__is_active=True)

            if driver:
                spoc_query = spoc_query & Q(city=driver.operating_city)
            spocs = Contract.objects.filter(spoc_query). \
                exclude(id__in=Contract.objects.filter(status=CONTRACT_STATUS_ACTIVE,
                                                       resourceallocation__drivers=driver.id if driver else None
                                                       ).values_list('id')).values_list('spocs')
            spoc_count = spocs.count()
            if not spoc_count:
                spocs = City.objects.filter(id=driver.operating_city_id, process_associates__is_active=True). \
                    values_list('process_associates')
                spoc_count = spocs.count()
            spoc_id = spocs[random.randint(0, spoc_count - 1)][0]

            return spoc_id

    def get(self, request):

        driver = request.user.driver if hasattr(request.user, 'driver') else None
        vendor = request.user.vendor if hasattr(request.user, 'vendor') else None

        driver_document_type = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT)
        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)
        doc_list = []
        veh_doc_list = []

        for doc_type in driver_document_type:
            doc_data = {
                'code': doc_type.code,
                'name': doc_type.name,
                'identifier': doc_type.identifier,
                'is_expire_needed': doc_type.is_expire_needed,
            }
            if driver:
                doc_info = DriverDocument.objects.filter(driver_id=driver.id, document_type=doc_type).order_by('-created_date')
                doc_serializer = DriverDocumentSerializer
            else:
                doc_info = VendorDocument.objects.filter(vendor_id=vendor.id, document_type=doc_type).order_by('-created_date')
                doc_serializer = VendorDocumentSerializer
            doc_data.update({'document': doc_serializer(doc_info, many=True).data})
            doc_data.update({'is_doc_available': True if len(doc_data['document'])>0 else False})
            doc_list.append(doc_data)

        for doc_type in vehicle_document_type:
            doc_data = {
                'code': doc_type.code,
                'name': doc_type.name,
                'identifier': doc_type.identifier,
                'is_expire_needed': doc_type.is_expire_needed,
            }

            query = Q(vehicle__driver=driver) if driver else Q(vehicle__vendor=vendor)
            query = query & Q(document_type=doc_type)
            doc_info = VehicleDocument.objects.filter(query).order_by('-created_date')
            doc_data.update({'document': VehicleDocumentSerializer(doc_info, many=True).data})
            veh_doc_list.append(doc_data)

        sorted_list = sorted(doc_list, key=lambda k: k['document'][0]['status_id'] if k['is_doc_available'] else 100)

        result = {
            'document_list': sorted_list,
            'vehicle_doc_list': veh_doc_list,
            'state': address_utils.get_state_list()
        }

        return Response(status=status.HTTP_200_OK, data=result,
                        content_type='text/html; charset=utf-8')

    def post(self, request):
        doc = request.data.dict()
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        vendor = request.user.vendor if hasattr(request.user, 'vendor') else None

        state = State.objects.filter(
            name=doc.get('document_state')).first()

        doc_type = DocumentType.objects.filter(code=doc.get('doc_type')).first()
        identifier = doc.get('identifier')

        document_data = {
            'file': doc.get('file', None),
            'document_type': doc_type.id,
            'status': VERIFICATION_PENDING,
            'number': doc.get('doc_number'),
            'state': state.pk,
            'created_by': request.user,
            'modified_by': request.user

        }
        if doc.get('expiry_date', ''):
            document_data.update({'expiry_date': parser.parse(doc.get('expiry_date')).date()})
        if identifier == VEHICLE_DOCUMENT:
            vehicle = driver.vehicles.all()
            if not vehicle:
                return Response(data='Bad Request no vehicle associated with driver',
                                status=status.HTTP_400_BAD_REQUEST)
            if vehicle and vehicle.count() != 1:
                return Response(data='more then one vehicle associated with driver',
                                status=status.HTTP_400_BAD_REQUEST)
            elif vehicle:
                document_data.update({'vehicle': vehicle.first().id})

        if driver:
            if identifier == DRIVER_DOCUMENT:
                document_data.update({'driver': driver.id})
                document_serializer = DriverDocumentSerializer(
                    data=document_data)
            if identifier == VEHICLE_DOCUMENT:
                document_serializer = VehicleDocumentSerializer(
                    data=document_data)
        elif vendor:
            if identifier == DRIVER_DOCUMENT:
                document_data.update({'vendor': vendor.id})
                document_serializer = VendorDocumentSerializer(
                    data=document_data)
            if identifier == VEHICLE_DOCUMENT:
                document_serializer = VehicleDocumentSerializer(
                    data=document_data)

        if document_serializer.is_valid():
            #always driver or vendor is passed
            doc = document_serializer.save(created_by=document_data.get(
                'created_by'), modified_by=document_data.get(
                'modified_by'))
            from blowhorn.driver.util import create_document_event

            if identifier == DRIVER_DOCUMENT and driver:
                create_document_event(driver_document=doc, driver=driver, created_by=request.user)
            elif identifier == DRIVER_DOCUMENT and vendor:
                create_document_event(vendor_document=doc, vendor=vendor, created_by=request.user)
            elif identifier == VEHICLE_DOCUMENT:
                create_document_event(vehicle_document=doc, driver=driver, created_by=request.user)

            return Response(status=status.HTTP_200_OK,
                            data={'message': 'Details Added Successfully'})
        else:
            logging.info(document_serializer.errors)
            return Response(document_serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class DriverReferralsView(views.APIView, DriverReferral):
    """
    Description     : API related to referred driver.

    Request type    : GET, POST

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = DriverReferralSerializer
    ordering_fields = ('-planned_start_time',)

    def get(self, request):
        """GET method for getting data of drivers referred by the user."""
        driver = request.user.driver
        data = request.GET.dict()
        error_message = ''
        response_data = {}
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        try:
            referral_qs = self.__get_queryset()
            referral_qs_serializer = common_serializer.PaginatedSerializer(
                queryset=referral_qs,
                num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                page=next,
                serializer_method=self.serializer_class,
                context={'request': request})
            response_data = referral_qs_serializer.data

        except BaseException as e:
            error_message = ('Error: %s, Call Support' % e)
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(driver, traceback.format_exc(),
                                request.build_absolute_uri(),
                                current_location=None, )

        # TODO need to be removed
        return Response(status=status_code,
                        data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)

    def __get_queryset(self):
        referral_qs = self.__get_query()
        referral_qs = referral_qs.select_related(
            'referral_config', 'referred_by')
        return referral_qs

    def __get_query(self):
        driver = self.request.user.driver
        return Driver.objects.filter(referred_by=driver.user)

    def post(self, request):
        """
        POST method for creating a Driver Record referred by the user.

        request data-> refer_driver_details={'name': 'gurva', 'city_id': 1, 'phone_number': '9971153201'}
        response_data->
            On Success: Returns bonus_amount, created_date, bonus_generated
            On Failure: Returns 500 with the error.
        """
        data = request.data.dict()
        self._initialize_attributes(data)

        try:
            self._create_driver()
        except BaseException as e:
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            self.error_message = ('Error: %s, Call Support' % e)
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint, )

        # TODO need to be removed
        return Response(status=self.status_code, data=self.response_data or self.error_message)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Arrival(generics.CreateAPIView):
    """
    Send message to customer regarding driver arrival.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsActiveDriver, HasValidSession)

    def post(self, request):
        """Send message to customer when driver is in vicinity of customer's pickup location."""
        error_message = ''
        response_data = 'Message sent successfully'
        status_code = status.HTTP_200_OK
        data = request.data.dict()

        order_id = data.get('order_id')
        if not order_id:
            error_message = 'Order number not sent !'

        order = Order._default_manager.filter(pk=order_id).select_related(
            'customer', 'customer__user').first()
        if not order:
            error_message = 'Order does not exist !'
        else:
            customer = order.customer
            if not customer or not customer.user:
                error_message = 'Order without customer !'
            elif not customer.user.phone_number:
                error_message = 'Customer Phone Number empty !'

        if error_message:
            status_code = status.HTTP_400_BAD_REQUEST
        else:
            template_dict = sms_templates.template_driver_arriving
            customer.user.send_sms(template_dict['text'], template_dict)
        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)


class DeviceStatus(generics.CreateAPIView):
    """
    Send message to customer regarding driver arrival.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsActiveDriver, HasValidSession)

    def post(self, request):
        """Save driver device details when phone gets switched off anf on."""
        error_message = ''
        self.driver = request.user.driver
        response_data = 'Details saved successfully'
        status_code = status.HTTP_200_OK
        data = request.data.dict()

        try:
            switch_off_details = json.loads(data.get('before_battery_percent'))
            switch_on_details = json.loads(data.get('after_battery_percent'))
        except BaseException:
            logger.error('Invalid Location json sent')
            return Response(status=status.HTTP_200_OK,
                            data="Invalid Location json sent")

        switch_off_details['action'] = 'Switched Off'
        switch_off_details['action_date'] = data.get('dev_shutdown_time')

        switch_on_details['action'] = 'Switched On'
        switch_on_details['action_date'] = data.get('dev_boot_time')

        try:
            self._save_details(details=switch_off_details)
            self._save_details(details=switch_on_details)
        except BaseException as e:
            logger.error(traceback.format_exc())
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            error_message = ('Error: %s, Call Support' % e)
            create_error_object(self.driver, traceback.format_exc(),
                                request.build_absolute_uri(),
                                current_location=None)

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)

    def _save_details(self, details, data={}):
        last_plugged_at = details.pop('LastChargingPluggedDateTime') \
            if details.get('LastChargingPluggedDateTime') else None
        last_unplugged_at = details.pop('LastChargingUnPluggedDateTime') \
            if details.get('LastChargingUnPluggedDateTime') else None
        action_date = details.pop('action_date') if details.get('action_date') else None

        data['last_plugged_at'] = datetime.datetime.strptime(last_plugged_at, '%Y-%m-%dT%H:%M:%S%z') \
            if last_plugged_at else None

        data['last_unplugged_at'] = datetime.datetime.strptime(last_unplugged_at, '%Y-%m-%dT%H:%M:%S%z') \
            if last_unplugged_at else None
        data['action_date'] = datetime.datetime.strptime(action_date, '%Y-%m-%dT%H:%M:%S%z') \
            if action_date else None

        data['action'] = details.pop('action') if details.get('action') else ''
        data['driver'] = self.driver
        data['battery'] = details.pop('batteryLevel') \
            if details.get('batteryLevel') else 0
        data['charging_status'] = details.pop('chargingStatus') \
            if details.get('chargingStatus') else False
        data['charging_mode'] = details.pop('chargingMode') \
            if details.get('chargingMode') else ''
        data['significant_level'] = details.pop('significantLevel') \
            if details.get('significantLevel') else ''
        data['extra_details'] = details

        log_info = DriverLogInfo(**data)
        log_info.save()


class PaymentDetails(generics.RetrieveAPIView):
    """
    Returns driver Payments data.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 and the payments data as json
    On Failure: Will return 500 with the error.
    """

    permission_classes = (IsActiveDriver, HasValidSession)
    def get_payment_data(self, payment_records):
        data = []
        approved_sum = 0
        paid_sum = 0
        for record in payment_records:
            if record.status == APPROVED and record.is_settled:
                paid_sum += record.net_pay
                sequence = 3
            elif record.status == APPROVED and not record.is_settled:
                approved_sum += record.net_pay
                sequence = 2
            else:
                sequence = 1

            trip_dict = []
            count = 0
            for paymenttrip in record.paymenttrip_set.all():
                trip = paymenttrip.trip
                if trip.status in [TRIP_COMPLETED]:
                    count += 1
                    trip_dict.append({
                        'planned_start_time': trip.planned_start_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(settings.TIME_FORMAT),
                        'actual_start_time': trip.actual_start_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(settings.TIME_FORMAT) if trip.actual_start_time else '',
                        'actual_end_time': trip.actual_end_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(settings.TIME_FORMAT) if trip.actual_end_time else '',
                        'total_time': str(minutes_to_dhms(trip.total_time)),
                        'total_distance': str(round(trip.gps_distance or trip.total_distance, 2)) + ' KM',
                        'area1': trip.hub.name if trip.hub else trip.order.pickup_address.line3
                                if trip.order and trip.order.pickup_address else '',
                        'area2': trip.order.shipping_address.line3
                                               if trip.order and trip.order.shipping_address and not trip.hub else '',
                        'customer_name': str(trip.order.customer) if trip.order and not record.contract.customer else''
                    })

            data.append({
                'id': record.id,
                'sequence': sequence,
                'customer': str(record.contract.customer) if record.contract and record.contract.customer else '',
                'net_pay': record.net_pay,
                'payment_type': record.payment_type,
                'toll_charges': record.toll_charges,
                'labour_cost': record.labour_cost,
                'gross_pay': record.gross_pay,
                'start_datetime': record.start_datetime.strftime(settings.DATE_FORMATS),
                'end_datetime': record.end_datetime.strftime(settings.DATE_FORMATS),
                'trip_count': count,
                'trip_details': trip_dict
            })

        return data, paid_sum, approved_sum

    def get_payment_response(self, driver, vendor):
        today = timezone.now().date()
        start = today - timedelta(days=60)
        end = today
        query = Q(driver=driver, status__in=[APPROVED, UNAPPROVED], start_datetime__gte=start, start_datetime__lte=end)
        response_data = {}
        if vendor:
            query = query & Q(driverledger__owner_details=vendor)
        payment_records = DriverPayment.objects.filter(query)
        payment_records = payment_records.prefetch_related(
            'drivercashpaid_set', 'paymenttrip_set', 'paymenttrip_set__trip__hub',
            'paymenttrip_set__trip', 'paymenttrip_set__trip__order',
            'paymenttrip_set__trip__order__shipping_address', 'paymenttrip_set__trip__order__customer',
            'paymenttrip_set__trip__order__pickup_address').order_by('-start_datetime').exclude(net_pay=0)
        payment_records = payment_records.select_related('contract', 'contract__customer')

        data, paid_sum, approved_sum = self.get_payment_data(payment_records)
        response_data['data'] = data
        response_data['processed_amount'] = paid_sum
        response_data['due_for_payment'] = approved_sum

        return response_data

    def get(self, request):
        """GET method for getting payments data of driver."""

        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor') == 'true' else False
        driver = request.user.driver if not is_vendor else Driver.objects.get(id=data.get('driver_id'))
        vendor = request.user.vendor if is_vendor else ''

        response_data = {}
        status_code = status.HTTP_200_OK

        if driver.own_vehicle or vendor:
            response_data = self.get_payment_response(driver, vendor)


        return Response(status=status_code,
                        data=response_data,
                        content_type='text/html; charset=utf-8')


class driver_tds(generics.RetrieveAPIView):

    def get(self, request):
        from blowhorn.driver.util import get_financial_year

        driver, vendor = None, None
        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor', '') == 'true' else False
        financial_year = data.get('unaccepted_tds_year', '')
        if not financial_year:
            financial_year = get_financial_year(timezone.now().date())
        if not is_vendor:
            driver = request.user.driver
        else:
            vendor = request.user.vendor

        if financial_year and ((driver and driver.pan and
                                driver.own_vehicle) or (vendor and vendor.pan)):
            return render_to_response(
                'admin/driver/tds.html',
                {
                    'driver': driver if not is_vendor else vendor,
                    'financial_year': financial_year
                }
            )

        return Response(status=400,
                        data='Pan of driver is not available or is not owner',
                        content_type='text/html; charset=utf-8')

    def post(self, request):
        from blowhorn.driver.util import get_financial_year

        data = request.data.dict()
        is_vendor = True if data.get('is_vendor') == 'true' else False
        financial_year = data.get('unaccepted_tds_year', '')
        if not financial_year:
            financial_year = get_financial_year(timezone.now().date())

        if not is_vendor:
            driver = request.user.driver
            query = Q(year=financial_year, driver=driver)
            tds_data = {
                "year": financial_year,
                "driver": driver
            }
        else:
            vendor = request.user.vendor
            tds_data = {
                "year": financial_year,
                "vendor": vendor
            }
            query = Q(year=financial_year, vendor=vendor)

        driver_tds = DriverTds.objects.filter(query).first()
        if not driver_tds:
            driver_tds = DriverTds(**tds_data)
            driver_tds.save()
            driver_tds.created_by = request.user

        tds_status = data.get('tds_status')

        if tds_status == TOS_ACCEPTED:
            driver_tds.is_tds_accepted = True
            driver_tds.accepted_date = timezone.now()
        else:
            driver_tds.is_tds_accepted = False
        driver_tds.modified_by = request.user
        driver_tds.save()

        return Response(status=status.HTTP_200_OK)


class GpsInformation(generics.CreateAPIView):

    def post(self, request):
        data = request.data.dict()
        driver = request.user.driver
        trip_id = int(data.get('trip_id', None))
        is_gps_on = True if data.get('is_gps_on', False) == 'true' else False
        device_timestamp = data.get('device_timestamp')
        network_ontime = data.get('network_ontime', None)
        network_offtime = data.get('network_offtime', None)

        gps_info = DriverGPSInformation(driver=driver, trip_id=trip_id, network_ontime=network_ontime,
                             is_gps_on=is_gps_on, device_timestamp=device_timestamp, network_offtime=network_offtime)
        gps_info.save()

        return Response(status=status.HTTP_200_OK)
