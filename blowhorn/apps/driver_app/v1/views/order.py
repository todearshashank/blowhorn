"""
Contains All the View Classes that are solely related to Orders.

-Scan Multiple Orders(scanned by drivers) and creating trip,
-Accept/Reject Booking/c2c order by driver,
-Check Payment status of an order whether paid or not, etc...

Created by: Gaurav Verma
"""
# Django and System libraries
import json
import logging
import traceback
from django.db.models import F
from django.db import transaction
from django.http import HttpRequest

# 3rd party libraries
from rest_framework import generics, exceptions, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from blowhorn.oscar.core.loading import get_model
from django.conf import settings

# blowhorn imports
from blowhorn.apps.driver_app.v1.serializers.trip import TripSyncSerializer
from blowhorn.apps.driver_app.v1.serializers.order import CreateOrderOnTheRunSerializer
from blowhorn.common.helper import CommonHelper
from blowhorn.apps.driver_app.v1.services.order.scan_orders import ScanOrderMixin
from blowhorn.apps.driver_app.v1.services.order.decision import OrderDecision
from blowhorn.driver.permissions import IsActiveDriver
from config.settings.status_pipelines import ORDER_DELIVERED, TRIP_COMPLETED, \
    ORDER_END_STATUSES
from blowhorn.order.views import OrderShipment, OrderList
from blowhorn.apps.driver_app.utils import _get_api_reponse
from blowhorn.apps.driver_app.v1.helpers.driver import create_error_object
from blowhorn.order.const import PAYMENT_STATUS_PAID
from blowhorn.users.permission import HasValidSession
from blowhorn.order.models import OrderDispatch


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
Order = get_model('order', 'Order')
OrderLine = get_model('order', 'OrderLine')


class ScanOrders(generics.CreateAPIView, ScanOrderMixin):
    """
    Description     : Get valid Shipment orders to be scanned and create trip for scanned orders.

    Request type    : GET, POST

    """

    permission_classes = (IsActiveDriver, HasValidSession)
    serializer_class = TripSyncSerializer

    def get(self, request):
        """Get all the NEW and PACKED shipment orders."""
        self.error_message = ''
        self.response_data = ''
        self.status_code = status.HTTP_200_OK
        try:
            self.__get_order_response()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.response_data or self.error_message)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)

    def __get_order_response(self):
        customers_list = self.request.query_params.get('ids').split(',')
        self.response_data = Order.objects.filter(customer__id__in=customers_list).exclude(
            status__in=ORDER_END_STATUSES
        ).values('customer_id', 'reference_number', 'number')

        if not self.response_data:
            self.error_message = 'No Orders Found'
            self.status_code = status.HTTP_404_NOT_FOUND

        # TODO need to be removed
        self.response_data = {
            'status': 'PASS',
            'order_list': self.response_data,
            'message': 'Synchronisation done. Scan the orders'
        }

    def post(self, request):
        """
        - Create trip for the Orders(shipment) scanned by driver.

        - Create Stops for orders scanned in between the trip.

        Permissions added.Only Active Drivers can access this api

        POST(order_numbers(list), trip_id(can be none), location_details)

        On Success: Returns Created Trip Data
        On Failure: Returns 500 with the error.
        """
        request_vars = request.data.dict()
        self._initialize_attributes(request_vars)

        try:
            self._assign_orders_to_driver()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Decision(generics.RetrieveAPIView, OrderDecision):
    """
    Accept/Reject Booking/c2c order by driver.

    Permissions added.Only Active Drivers can access this api
    POST(action('accept'/'reject'), order_id, location_details)

    On Success:
        -If Driver Accepts:
            Returns Created Trip Data
        -If Driver Rejects:
            Returns 'Driver Rejected' and accept/reject notification will be sent to the next driver.
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = TripSyncSerializer

    def post(self, request, order_id):
        """POST method for Accept/Reject decision for Booking/c2c order by driver."""
        data = request.POST.dict()
        self._initialize_attributes(data, order_id)

        try:
            self._make_decision()
        except BaseException as e:
            self.response_data = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                order=self.order)
        # TODO need to be removed
        return Response(data=self.error_message or self.response_data, status=self.status_code)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class CheckPaymentStatus(generics.RetrieveAPIView):
    """
    checking the payment status of an order.

    Permissions added.Only Active Drivers can access this api
    POST(order_id, location_details)

    On Success: {'status': True/False}
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)

    def post(self, request, order_id):
        """POST method for checking the payment status of an order."""
        error_message = ''
        status_code = status.HTTP_200_OK
        response = {
            'status': False
        }
        order = Order.objects.filter(pk=order_id)
        if not order:
            error_message = 'Invalid Order!!!'
            status_code = status.HTTP_400_BAD_REQUEST
        else:
            if order[0].payment_status == PAYMENT_STATUS_PAID:
                response['status'] = True
        # TODO need to be removed
        return Response(data=error_message or response, status=status_code)

        return _get_api_reponse(status_code, error_message, response)


class DispatchAcknowledgement(generics.ListCreateAPIView):

    permission_classes = (IsActiveDriver, HasValidSession)

    def post(self, request):
        data = request.data.dict()
        dispatch_id= data.get('dispatch_id')
        notification_type = data.get('notification_type')
        dispatch = OrderDispatch.objects.filter(id=dispatch_id).first()
        if dispatch_id:
            if notification_type == settings.MQTT_RESPONSE_CONST:
                dispatch.mqtt_response = True
            elif notification_type == settings.FCM_RESPONSE_CONST:
                dispatch.fcm_response = True
            dispatch.save()

        return Response(status=200, data='updated sucessfully')

# TODO changes for multiple requests
class CreateOrderOnTheRun(generics.ListCreateAPIView):
    """
    -Create shipment order using order data sent by app.

    -Add Stop for that order in the trip sent by app.

    Permissions added.Only Active Drivers can access this api
    POST(
        order_data(
            'delivery_address', 'customer_mobile', 'trip_id',
            'delivery_lat', 'delivery_lon', sku_details
        ),
        location_details
    )

    On Success: Trip Data
    On Failure: Returns 400 with the error.

    """

    permission_classes = (IsActiveDriver, HasValidSession)

    def __parse_params(self, data):
        self.order_numbers = []
        order_lines = {}
        order_data = data.get('order_data')
        self.address_data = json.loads(order_data)
        sku_data = self.address_data.get('sku_details', None)

        if sku_data:
            sku_data = self.address_data.pop('sku_details')
            for data in sku_data:
                line_data = []
                if order_lines.get(data.get('customer_id')):
                    line_data = order_lines.pop(data.get('customer_id'))

                line_data.append({
                    'sku': data.get('sku_name').split('-')[0],
                    'quantity': data.get('quantity')
                })
                order_lines[data.get('customer_id')] = line_data
        self.address_data.pop('otp', None)
        self.reason = self.address_data.pop('reason', None)
        self.address_data['customer_name'] = self.address_data.pop('shop_name', '') + '-' \
            + self.address_data.pop('name', '')
        self.geopoint = CommonHelper.get_geopoint_from_latlong(
            self.address_data.get('delivery_lat'),
            self.address_data.get('delivery_lon'))
        self.order_data = {
            'address_data': self.address_data,
            'order_lines': order_lines
        }

    def post(self, request):
        """POST method for creating shipment order and adding stop for the order in the trip."""
        self.data = request.data.dict()
        self.__parse_params(self.data)

        missing_fields = []
        mandatory_fields = ['delivery_address', 'customer_mobile', 'trip_id', 'delivery_lat', 'delivery_lon']
        for field in mandatory_fields:
            if not self.order_data.get('address_data').get(field, None):
                missing_fields.append(field)

        self.trip_id = self.order_data.get('address_data').get('trip_id', None)

        if missing_fields:
            return Response(status=status.HTTP_400_BAD_REQUEST, data="Invalid data")

        for customer_id in self.order_data.get('order_lines').keys():
            data = self.order_data.get('address_data')
            data['customer_id'] = customer_id
            data['lines'] = self.order_data.get('order_lines')[customer_id]
            try:
                order_number = self.__create_order(data, request)
            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                logger.error(traceback.format_exc())
                return Response(error_message, status=status.HTTP_400_BAD_REQUEST)
            # self.order_numbers.append(order_number)
            # TODO need to be removed
            break

        # TODO this logic has to be removed and change according to unified driver app
        from blowhorn.trip.mixins import TripMixin
        trip = Trip.objects.get(pk=self.trip_id)
        trip_details = TripMixin().get_order_response(trip)
        # TODO need to use below line
        # trip_details['order_numbers'] = self.order_numbers
        trip_details['order_number'] = order_number
        return Response(trip_details, status=status.HTTP_200_OK)

    def __create_order(self, order_data, request):
        with transaction.atomic():
            order_number = None
            first_request = HttpRequest()
            first_request.method = 'POST'
            first_request.META = request.META
            order_data['serializer_class'] = CreateOrderOnTheRunSerializer
            order_data['created_by_driver'] = True
            first_request.data = order_data
            response = OrderShipment.as_view()(first_request)

            if response.status_code != 200:
                raise exceptions.APIException('Order Creation Failed!!!')
            order_number = response.data['message']['awb_number']

            second_request = HttpRequest()
            second_request.method = 'POST'
            second_request.META = request.META
            second_request.data = {
                'location_details': self.data.get('location_details', None),
                'item_ids': [{'number': order_number}, ],
                'trip_id': self.trip_id
            }
            response = OrderList.as_view()(second_request)
            if response.status_code != 200:
                raise exceptions.APIException('Order assignment failed!!!')

            # TODO need to be changed
            from blowhorn.order.serializers.OrderSerializer import OrderSerializer
            order = Order.objects.filter(number=order_number).first()
            order_serializer = OrderSerializer(order)
            order_serializer.update_status(
                order_status=ORDER_DELIVERED,
                geopoint=self.geopoint,
                order=order,
                reason=self.reason
            )
            Stop.objects.filter(trip_id=self.trip_id,
                                waypoint__order=order
                                ).update(status=TRIP_COMPLETED)
            OrderLine.objects.filter(order__number=order_number).update(delivered=F('quantity'))
            return order_number
