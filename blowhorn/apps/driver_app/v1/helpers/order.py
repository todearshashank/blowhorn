# 3rd party libraries
from blowhorn.oscar.core.loading import get_model

from config.settings.status_pipelines import (ORDER_MOVING_TO_HUB,
                                              ORDER_MOVING_TO_HUB_RTO,
                                              ORDER_PICKED, ORDER_OUT_FOR_RTO)

StopOrders = get_model('trip', 'StopOrders')
Stop = get_model('trip', 'Stop')


def check_remaining_items_handover(trip, driver):

    """
    Check if there is any pending order with driver which were picked
     or for hub to hub movement. Also check if driver has any COD cash.
    """

    if not driver or not trip:
        return True

    stop_orders = StopOrders.objects.filter(trip=trip, order__status__in=[
        ORDER_MOVING_TO_HUB, ORDER_MOVING_TO_HUB_RTO]).exists()

    if stop_orders:
        return True

    orders_picked = Stop.objects.filter(trip=trip, order__has_pickup=True,
                                        order__status__in=[ORDER_PICKED,
                                                           ORDER_OUT_FOR_RTO]).exists()

    if orders_picked or driver.cod_balance > 0:
        return True
