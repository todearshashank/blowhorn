from django.conf import settings
from django.db.models import F
from rest_framework.response import Response
from rest_framework import status

from blowhorn.oscar.core.loading import get_model

from config.settings.status_pipelines import TRIP_COMPLETED
from blowhorn.apps.mixins import CreateModelMixin
from config.settings import status_pipelines as StatusPipeline


ResourceAllocation = get_model('contract', 'ResourceAllocation')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
Contract = get_model('contract', 'Contract')


def _parse_contact(support_numbers):
    for contact in support_numbers:
        if contact.get('spoc_name') == '':
            contact['spoc_name'] = 'Blowhorn'


def statuspipeline_check(function):
    def wrap(request, *args, **kwargs):
        pk = int(kwargs.get('pk') or kwargs.get('trip_id'))
        trip = Trip.objects.filter(id=pk)\
            .select_related('order').first()
        order = trip.order
        if (trip and trip.status == TRIP_COMPLETED and
            trip.current_step == -1) and \
                (order and order.status == StatusPipeline.ORDER_DELIVERED):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid order status.. try sync..')
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def _get_support_number_from_contract(contract_id):
    support_numbers = Contract.objects.filter(
        pk=contract_id
    ).prefetch_related('spocs').annotate(
        spoc_name=F('spocs__name'), spoc_mobile=F('spocs__phone_number')
    ).values('spoc_name', 'spoc_mobile')

    if support_numbers:
        _parse_contact(support_numbers)
        return support_numbers

    return [{'spoc_name': 'Blowhorn', 'spoc_mobile': settings.BLOWHORN_SUPPORT_NUMBER}, ]


def _get_support_number_from_resource_allocation(driver):
    support_numbers = ResourceAllocation.objects.filter(drivers=driver).annotate(
        spoc_name=F('contract__spocs__name'), spoc_mobile=F('contract__spocs__phone_number')
    ).values('spoc_name', 'spoc_mobile').distinct()

    if support_numbers:
        _parse_contact(support_numbers)
        return support_numbers

    trip = Trip.objects.filter(
        driver=driver, status=TRIP_COMPLETED,
    ).exclude(actual_end_time=None).order_by('-actual_end_time').first()

    if trip:
        contract_id = trip.customer_contract.id if trip.customer_contract else None
        return _get_support_number_from_contract(contract_id)

    return [{'spoc_name': 'Blowhorn', 'spoc_mobile': settings.BLOWHORN_SUPPORT_NUMBER}, ]


def _get_support_number(driver=None, contract_id=None):
    """Get spocs name and mobile number from contract, if not send default support number."""
    if contract_id:
        return _get_support_number_from_contract(contract_id)

    if driver:
        return _get_support_number_from_resource_allocation(driver)

    return [{'spoc_name': 'Blowhorn', 'spoc_mobile': settings.BLOWHORN_SUPPORT_NUMBER}, ]


def create_error_object(driver, error, url, **kwargs):
    from blowhorn.apps.driver_app.v1.serializers.driver import DriverErrorSerializer
    error_obj_data = {
        'driver': driver.id,
        'traceback': error,
        'url': url
    }
    for attr in kwargs:
        if attr == 'current_location':
            error_obj_data[attr] = kwargs.get(attr, None)
        else:
            try:
                error_obj_data[attr] = kwargs.get(attr, None).id if kwargs.get(attr, None) else None
            except:
                pass
    try:
        CreateModelMixin().create(data=error_obj_data, serializer_class=DriverErrorSerializer)
    except:
        pass


def _check_device_details(driver, app_version, app_code):
    from blowhorn.apps.driver_app.v1.serializers.driver import DeviceDetailSerializer

    device_details = driver.device_details
    if not device_details:
        device_details_data = {
            'app_version_name': app_version,
            'app_version_code': app_code
        }
        device_details = CreateModelMixin().create(
            data=device_details_data, serializer_class=DeviceDetailSerializer)
        driver.device_details = device_details
        driver.save()
    elif device_details.app_version_name != app_version or device_details.app_version_code != app_code:
        device_details.app_version_name = app_version
        device_details.app_version_code = app_code
        device_details.save()
