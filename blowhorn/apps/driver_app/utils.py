from Crypto.Cipher import AES
import base64
from rest_framework.response import Response


class AESCipher(object):

    def __init__(self):
        self.block_size = 32
        self.padding_char = '{'
        self.key = b'\x00\xa3\x19\xf6FV\xb4\xedg|D\x8dT\xc5gg\xb4z\xe7\xca\xfc4a\xb2|B\x17\x01\x87[\xdb\xa9'

    def encode(self, raw_text):
        crypto = AES.new(self.key, mode=AES.MODE_ECB)
        padded_text = self.pad_string(raw_text)
        encrypted_text = crypto.encrypt(padded_text)
        return base64.b64encode(encrypted_text)

    def decode(self, encoded_text):
        crypto = AES.new(self.key, mode=AES.MODE_ECB)
        decoded_text = base64.b64decode(encoded_text)
        decoded_text = crypto.decrypt(decoded_text)
        return self.unpad_string(decoded_text)

    def pad_string(self, string):
        string = string + (self.block_size - len(string) %
                           self.block_size) * self.padding_char
        return string.encode('utf-8')

    def unpad_string(self, string):
        string = string.decode('utf-8')
        return string.rstrip(self.padding_char)

LegacyCipher = AESCipher()


def _get_api_reponse(status_code, error_message, data, content_type='charset utf-8; application/json'):
    if error_message:
        content_type = 'charset utf-8; text/plain'
    response_data = {
        'message': error_message,
        'data': data
    }
    return Response(status=status_code, data=response_data, content_type=content_type)
