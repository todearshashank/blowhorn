from django.conf import settings
from django.db.models import F
from rest_framework.response import Response
from rest_framework import status
from django.utils import timezone
from datetime import timedelta

from blowhorn.oscar.core.loading import get_model

from config.settings.status_pipelines import TRIP_COMPLETED
from blowhorn.apps.mixins import CreateModelMixin
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.constants import YEARLY, MONTHLY, FORTNIGHTLY, WEEKLY, DAILY, HOURLY, TILL_ACCEPTED, AD_HOC

ResourceAllocation = get_model('contract', 'ResourceAllocation')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
Contract = get_model('contract', 'Contract')


def _parse_contact(support_numbers):
    for contact in support_numbers:
        if contact.get('spoc_name') == '':
            contact['spoc_name'] = 'Blowhorn'


def statuspipeline_check(function):
    def wrap(request, *args, **kwargs):
        pk = int(kwargs.get('pk') or kwargs.get('trip_id'))
        trip = Trip.objects.filter(id=pk)\
            .select_related('order').first()
        order = trip.order
        if (trip and trip.status == TRIP_COMPLETED and
            trip.current_step == -1) and \
                (order and order.status == StatusPipeline.ORDER_DELIVERED):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid order status.. try sync..')
        else:
            return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def _get_support_number_from_contract(contract_id):
    support_numbers = Contract.objects.filter(
        pk=contract_id
    ).prefetch_related('spocs').annotate(
        spoc_name=F('spocs__name'), spoc_mobile=F('spocs__phone_number')
    ).values('spoc_name', 'spoc_mobile')

    if support_numbers:
        _parse_contact(support_numbers)
        return support_numbers

    return [{'spoc_name': 'Blowhorn', 'spoc_mobile': settings.BLOWHORN_SUPPORT_NUMBER}, ]


def _get_support_number_from_resource_allocation(driver):
    support_numbers = ResourceAllocation.objects.filter(drivers=driver).annotate(
        spoc_name=F('contract__spocs__name'), spoc_mobile=F('contract__spocs__phone_number')
    ).values('spoc_name', 'spoc_mobile').distinct()

    if support_numbers:
        _parse_contact(support_numbers)
        return support_numbers

    trip = Trip.objects.filter(
        driver=driver, status=TRIP_COMPLETED,
    ).exclude(actual_end_time=None).order_by('-actual_end_time').first()

    if trip:
        contract_id = trip.customer_contract.id if trip.customer_contract else None
        return _get_support_number_from_contract(contract_id)

    return [{'spoc_name': 'Blowhorn', 'spoc_mobile': settings.BLOWHORN_SUPPORT_NUMBER}, ]


def _get_support_number(driver=None, contract_id=None):
    """Get spocs name and mobile number from contract, if not send default support number."""
    if contract_id:
        return _get_support_number_from_contract(contract_id)

    if driver:
        return _get_support_number_from_resource_allocation(driver)

    return [{'spoc_name': 'Blowhorn', 'spoc_mobile': settings.BLOWHORN_SUPPORT_NUMBER}, ]


def create_error_object(driver, error, url, **kwargs):
    from blowhorn.apps.driver_app.v3.serializers.driver import DriverErrorSerializer
    error_obj_data = {
        'driver': driver.id,
        'traceback': error,
        'url': url
    }
    for attr in kwargs:
        if attr == 'current_location':
            error_obj_data[attr] = kwargs.get(attr, None)
        else:
            try:
                error_obj_data[attr] = kwargs.get(attr, None).id if kwargs.get(attr, None) else None
            except:
                pass
    try:
        CreateModelMixin().create(data=error_obj_data, serializer_class=DriverErrorSerializer)
    except:
        pass


def _check_device_details(driver, app_version, app_code):
    from blowhorn.apps.driver_app.v3.serializers.driver import DeviceDetailSerializer

    device_details = driver.device_details
    if not device_details:
        device_details_data = {
            'app_version_name': app_version,
            'app_version_code': app_code
        }
        device_details = CreateModelMixin().create(
            data=device_details_data, serializer_class=DeviceDetailSerializer)
        driver.device_details = device_details
        driver.save()
    elif device_details.app_version_name != app_version or device_details.app_version_code != app_code:
        device_details.app_version_name = app_version
        device_details.app_version_code = app_code
        device_details.save()


def check_driver_criteria(driver, driver_criteria):
    event_status = False
    if driver_criteria.operating_cities.exists() and\
            driver.operating_city not in driver_criteria.operating_cities.all():
        event_status = True
    if not event_status and driver_criteria.own_vehicle and not driver.own_vehicle:
        event_status = True
    if not event_status and driver_criteria.vehicle_classes.exists() and \
        driver.vehicles.first().vehicle_model.vehicle_class not in \
            driver_criteria.vehicle_classes.all():
        event_status = True
    if not event_status and driver_criteria.driver_ids and \
            driver.id not in driver_criteria.driver_ids:
        event_status = True
    return event_status


def check_interval_criteria(interval, last_response_time):
    event_status = False
    if interval == YEARLY and not (last_response_time.year < timezone.now().year):
        event_status = True
    if interval == MONTHLY and not (last_response_time.month < timezone.now().month):
        event_status = True
    if interval == FORTNIGHTLY and not (last_response_time + timedelta(days=14) <= timezone.now()):
        event_status = True
    if interval == WEEKLY and not (last_response_time + timedelta(days=7) <= timezone.now()):
        event_status = True
    if interval == DAILY and not (last_response_time + timedelta(days=1) <= timezone.now()):
        event_status = True
    if interval == HOURLY and not (last_response_time + timedelta(hours=1) <= timezone.now()):
        event_status = True
    if interval == TILL_ACCEPTED:
        event_status = True
    return event_status


def send_field_name_values(ln, form_language):

    if ln == 'kn':
        return form_language.question_kn, form_language.field_values_kn
    elif ln == 'mr':
        return form_language.question_mr, form_language.field_values_mr
    elif ln == 'te':
        return form_language.question_te, form_language.field_values_te
    elif ln == 'ta':
        return form_language.question_ta, form_language.field_values_ta
    elif ln == 'hi':
        return form_language.question_hi, form_language.field_values_hi
    else:
        return form_language.question_en, form_language.field_values_en


def send_notification_content(ln, content):

    if ln == 'kn':
        return content.title_kn, content.short_description_kn, content.description_kn, content.img_kannada
    elif ln == 'mr':
        return content.title_mr, content.short_description_mr, content.description_mr, content.img_marathi
    elif ln == 'te':
        return content.title_te, content.short_description_te, content.description_te, content.img_telugu
    elif ln == 'ta':
        return content.title_ta, content.short_description_ta, content.description_ta, content.img_tamil
    elif ln == 'hi':
        return content.title_hi, content.short_description_hi, content.description_hi, content.img_hindi
    else:
        return content.title_en, content.short_description_en, content.description_en, content.img_english
