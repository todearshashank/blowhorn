# 3rd party libraries
from django.db.models import Count, F

from blowhorn.oscar.core.loading import get_model

from config.settings.status_pipelines import (ORDER_MOVING_TO_HUB,
                                              ORDER_MOVING_TO_HUB_RTO,
                                              ORDER_PICKED, ORDER_OUT_FOR_RTO,
                                              UNABLE_TO_DELIVER, OUT_FOR_DELIVERY,
                                              ORDER_RETURNED)

StopOrders = get_model('trip', 'StopOrders')
Stop = get_model('trip', 'Stop')


def check_remaining_items_handover(trip, driver):

    """
    Check if there is any pending order with driver which were picked
     or for hub to hub movement. Also check if driver has any COD cash.
    """

    if not driver or not trip:
        return False, {}

    stop_orders = StopOrders.objects.filter(trip=trip, order__status__in=[
        ORDER_MOVING_TO_HUB, ORDER_MOVING_TO_HUB_RTO]).exists()

    stop_orders_count = StopOrders.objects.filter(trip=trip, order__status__in=[
        ORDER_MOVING_TO_HUB, ORDER_MOVING_TO_HUB_RTO]).annotate(
        stop_type=F('stop__stop_type')).values('stop_type').annotate(
        order_count=Count('stop_type'))

    trip_order_details = {}
    if stop_orders_count:
        trip_order_details['orders'] = stop_orders_count
    if driver.cod_balance > 0:
        trip_order_details['cash_collected'] = driver.cod_balance

    if stop_orders:
        return True, trip_order_details

    orders_picked = Stop.objects.filter(trip=trip,
                                        order__status__in=[ORDER_PICKED,
                                                           ORDER_OUT_FOR_RTO]).exists()

    # TODO: return stop type based order count
    driver_orders_count = Stop.objects.filter(trip=trip,
                                              order__status__in=[ORDER_PICKED,
                                                                 ORDER_OUT_FOR_RTO,
                                                                 UNABLE_TO_DELIVER,
                                                                 OUT_FOR_DELIVERY,
                                                                 ORDER_RETURNED
                                                                 ]
                                              ).values('stop_type').annotate(order_count=Count('stop_type'))

    trip_order_details = {}
    if driver.cod_balance > 0:
        trip_order_details['cash_collected'] = driver.cod_balance
    if driver_orders_count:
        trip_order_details['orders'] = driver_orders_count

    if orders_picked or driver.cod_balance > 0:
        return True, trip_order_details

    if trip.trip_workflow.is_wms_flow and (driver.cod_balance or driver_orders_count):
        return True, trip_order_details

    return False, {}
