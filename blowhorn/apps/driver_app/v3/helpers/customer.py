"""Customer related Helper Functions to be added here."""
# System and Django libraries
import logging

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model

# Blowhorn libraries
from blowhorn.apps.driver_app.v3.serializers.customer import CustomerRatingSerializer
from blowhorn.apps.mixins import CreateModelMixin
from blowhorn.order.models import OrderRating

Customer = get_model('customer', 'Customer')
CustomerRating = get_model('customer', 'CustomerRating')


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def _create_customer_rating(rating, customer):
    data = {
        'num_of_feedbacks': 1,
        'cumulative_rating': rating,
    }
    customer_rating = CreateModelMixin().create(data=data, serializer_class=CustomerRatingSerializer)
    if not customer_rating:
        logger.error('Customer Rating creation Failed for customer:- "%s"', customer)
        return

    Customer.objects.filter(pk=customer.id).update(customer_rating=customer_rating)
    logger.info('Customer Rating Successfully created for customer:- "%s"', customer)


def _update_customer_rating(customer_rating, rating):
    num_of_feedbacks = customer_rating.num_of_feedbacks
    cumulative_rating = customer_rating.cumulative_rating

    new_cumulative_rating = (float(cumulative_rating) * num_of_feedbacks + rating) / (num_of_feedbacks + 1)
    num_of_feedbacks = num_of_feedbacks + 1
    cumulative_rating = round(new_cumulative_rating, 2)

    CustomerRating.objects.filter(pk=customer_rating.id).update(
        num_of_feedbacks=num_of_feedbacks,
        cumulative_rating=cumulative_rating
    )
    # data = {
    #     'num_of_feedbacks': num_of_feedbacks + 1,
    #     'cumulative_rating': round(new_cumulative_rating, 2),
    # }
    # customer_rating = UpdateModelMixin().update(
    #     data=data, instance=customer_rating, serializer_class=CustomerRatingSerializer)
    # customer_rating.save()


def upsert_customer_rating(driver, order, rating):
    """Create/Update Customer Rating object for a customer."""
    customer = order.customer
    if not isinstance(rating, float):
        rating = float(rating)

    customer_rating = customer.customer_rating
    if not customer_rating:
        _create_customer_rating(rating, customer)
    else:
        _update_customer_rating(customer_rating, rating)

    OrderRating.objects.filter(driver=driver, order=order).update(rating=rating)

