"""
- Create trip for the Orders(shipment) scanned by driver.

- Create Stops for orders scanned in between the trip.

Input Data: order_numbers(list), trip_id(can be none), location_details

Response: Returns Created Trip Data

Created By: Gaurav Verma
"""
# System and Django libraries
import json
import logging
from django.db import transaction
from django.db.models import Q, Count
from django.utils import timezone
from django.conf import settings

# 3rd Party libraries
from rest_framework import status

from blowhorn.order.models import OrderLine
from blowhorn.oscar.core.loading import get_model

# Blowhorn libraries
from blowhorn.trip.models import LAST_MILE, FIRST_MILE
from config.settings.status_pipelines import (
    DRIVER_ACCEPTED, TRIP_IN_PROGRESS, TRIP_ALL_STOPS_DONE,
    OUT_FOR_DELIVERY, TRIP_NEW, ORDER_RETURNED,
    UNABLE_TO_DELIVER, DRIVER_ASSIGNED
)
from config.settings import status_pipelines as StatusPipeline

from blowhorn.apps.driver_app.v3.helpers.trip import create_stop, create_trip
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.apps.driver_app.v3.serializers.trip import TripSerializer
from blowhorn.common.helper import CommonHelper
from blowhorn.customer.tasks import broadcast_order_details_to_dashboard
from blowhorn.trip.constants import PICKUP, HUB_DELIVERY, ORDER_DELIVERY, \
    HUB_RTO, ORDER_RTO
from blowhorn.apps.driver_app.v3.constants import ORDER_STATUS_BY_STOPS
from blowhorn.address.utils import get_hub_from_pincode
from blowhorn.apps.driver_app.v3.services.trip.send_sms import SendSMS


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
StopOrders = get_model('trip', 'StopOrders')
Order = get_model('order', 'Order')
ShippingAddress = get_model('order', 'ShippingAddress')
Hub = get_model('address', 'Hub')
Event = get_model('order', 'Event')


class ScanOrderMixin(object):
    """scan orders and create trip or add orders as stop in between the trip."""

    def __init__(self, **kwargs):
        """Method called from order/admin.py for storing which associates created the trip for orders."""
        self.context = kwargs.pop('context', None)
        self.geopoint = kwargs.pop('geopoint', None)
        self.trip_id = kwargs.pop('trip_id', None)
        self.hub_associate = kwargs.pop('hub_associate', None)
        if self.hub_associate:
            self.order_status = DRIVER_ASSIGNED
        else:
            self.order_status = DRIVER_ACCEPTED
        self.remarks = None
        self.address = None
        self.event_list = []

    def _initialize_attributes(self, data):

        self.created_by_driver = True
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.context = {'request': self.request}
        self.trip_id = data.get('trip_id', None)
        self.order_status = DRIVER_ACCEPTED
        self.hub_associate = None
        self.pickup_hub = None

        proceed = True
        records_with_end_statuses = []
        records_with_ongoing_trip = []

        order_numbers = data.get('order_numbers') or None
        try:
            self.order_numbers = json.loads(order_numbers)
        except ValueError:
            logger.error('Invalid format to send the Items.')
            self.error_message = "Invalid format to send the Items."

        if not self.order_numbers:
            self.error_message = "Items list is empty."

        order_qs = Order.objects.filter(number__in=self.order_numbers)
        orders_list = []
        for order in order_qs:
            orders_list.append(order.number)
            if order.status in StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES:
                proceed = False
                records_with_end_statuses.append(order.number)

            elif order.status in [StatusPipeline.OUT_FOR_DELIVERY, StatusPipeline.DRIVER_ASSIGNED,
                                  StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                  StatusPipeline.ORDER_MOVING_TO_HUB, StatusPipeline.ORDER_MOVING_TO_HUB_RTO,
                                  StatusPipeline.ORDER_OUT_FOR_RTO] or \
                (order.status == StatusPipeline.ORDER_NEW and
                 order.driver_id is not None):

                dict_ = {}
                stop = order.stops.exclude(trip__status__in=StatusPipeline.TRIP_END_STATUSES).first()
                if stop:
                    dict_['Order'] = order.number
                    dict_['Trip'] = stop.trip.trip_number
                    proceed = False
                    records_with_ongoing_trip.append(dict_)
        if not proceed:
            if records_with_ongoing_trip:
                self.error_message = "Orders fulfilled by other ongoing trip: %s" % \
                          records_with_ongoing_trip

            if records_with_end_statuses:
                self.error_message = "Orders fulfilled by other ongoing trip: %s" % records_with_end_statuses

        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.address = CommonHelper.get_address_from_location_json(location)
        self.event_list = []

    # TODO need to revisit again
    def __check_valid_request(self, driver, trip_id):
        trip = None
        if driver and not trip_id:
            # TODO revisit again
            trip = Trip.objects.filter(
                driver=driver, current_step__gt=0).first()
        return trip

    def _assign_orders_to_driver(self):

        with transaction.atomic():
            # TODO revisit this function
            # trip = self.__check_valid_request(self.driver, self.trip_id)
            trip = False
            try:
                if not trip:
                    trip = self.create_trip_stops_for_shipment_orders(
                        driver=self.driver,
                        order_numbers=self.order_numbers,
                        geopoint=self.geopoint,
                        trip_id=self.trip_id
                    )
            except:
                trip = False

            if not trip:
                logger.error('Driver Assigning Failed')
                self.status_code = status.HTTP_400_BAD_REQUEST
                if not self.error_message:
                    self.error_message = 'Driver Assigning Failed'
            else:
                self.response_data = self.serializer_class(instance=trip, context=self.context).data

    def create_trip_stops_for_shipment_orders(self, driver, order_numbers,
                                              **kwargs):
        """Create a new trip for driver and assigning driver to trip as well
        as all orders."""
        trip_workflow = kwargs.get('trip_workflow', None)
        can_ship_wms_order = kwargs.get('can_ship_wms_order', False)
        with transaction.atomic():

            is_kiosk_order = kwargs.get('is_kiosk_order', False)
            order_qs = self.__get_orders_sequence_wise(order_numbers)

            wms_orders = order_qs.filter(customer_contract__is_wms_contract=True).values_list('number', flat=True)
            if wms_orders.exists() and not can_ship_wms_order:
                self.error_message = "Use WMS app to assign WMS orders: %s" % list(wms_orders)
                return False

            if not order_qs.exists():
                return False
            self.trip_workflow = trip_workflow
            self.trip = self.__get_or_create_shipment_trip(driver, self.trip_id)
            if not self.trip:
                return False

            response, first_order = self.__create_stops_from_shipment_orders(
                order_qs, driver)

            if response:
                if not self.trip_id:
                    if Trip.objects.filter(driver=driver,
                                        status__in=[StatusPipeline.TRIP_IN_PROGRESS,
                                                    StatusPipeline.DRIVER_ACCEPTED,
                                                    StatusPipeline.DRIVER_ASSIGNED],
                                        ).exclude(pk=self.trip.id).exists():
                        self.status_code = status.HTTP_400_BAD_REQUEST
                        self.error_message = 'Driver already has a trip, sync again!'
                        raise BaseException(
                            'Driver already has a trip, sync again!')

                if Stop.objects.filter(trip=self.trip).values('order',
                                                              'stop_type').annotate(
                    order_stop_count=Count('order')).filter(
                    order_stop_count__gt=1).exists():
                    self.status_code = status.HTTP_400_BAD_REQUEST
                    self.error_message = 'Order already added, sync again!'
                    raise BaseException('Order already added, sync again!')

                self.__update_trip(
                    order=first_order, number_of_orders=len(order_qs),
                    **{'is_kiosk_order': is_kiosk_order, 'trip_workflow': trip_workflow.id if trip_workflow else None})
                self.trip.is_route_optimal = False
                self.trip.trip_type = LAST_MILE if driver.operating_city == first_order.city else FIRST_MILE
                self.trip.save()
                return self.trip
            return response

    def __get_orders_sequence_wise(self, order_numbers):
        """
        Method to return list of orders sequence wise.

        (currently on basis of priority & expected delivery time,
        make changes here if sequencing needs to be changed)
        """
        from datetime import timedelta
        pickup_datetime_threshold = timezone.now() - timedelta(days=180)
        query_params = Q(number__in=order_numbers)
        query_params &= Q(order_type=settings.SHIPMENT_ORDER_TYPE)
        query_params &= Q(pickup_datetime__gte=pickup_datetime_threshold)
        query_params &= ~Q(status__in=StatusPipeline.SHIPMENT_ORDER_END_STATUSES)
        return Order.objects.prefetch_related('waypoint_set').\
            select_related('pickup_address', 'shipping_address', 'hub', 'customer_contract',
                           'customer_contract__trip_workflow').filter(query_params).extra(
            select={
                'expected_delivery_time_null': 'expected_delivery_time is null'

            },
            order_by=['priority','expected_delivery_time_null']
        )

    def __get_or_create_shipment_trip(self, driver, trip_id, **kwargs):
        if not trip_id:
            trip = Trip.objects.filter(driver=driver, status__in=[
                StatusPipeline.TRIP_IN_PROGRESS,
                StatusPipeline.DRIVER_ACCEPTED,
                StatusPipeline.DRIVER_ASSIGNED]).exists()
            if trip:
                return False
            trip_workflow = kwargs.get('trip_workflow', None)
            trip = create_trip(
                driver_id=driver.id,
                blowhorn_contract=driver.contract.id if driver.contract else None,
                status=DRIVER_ACCEPTED,
                trip_workflow=trip_workflow
            )
        else:
            query = Q(pk=trip_id)
            trip = Trip.objects.prefetch_queryset(query=query, stops_list=True).first()
            # TODO need to use only trip.status == TRIP_ALL_STOPS_DONE condition and remove
            # second condition after unified driver app is rolled out to b2c module
            if trip.status == TRIP_ALL_STOPS_DONE:
                return False
        return trip

    def __create_stops_from_shipment_orders(self, order_qs, driver):
        # TODO current;y just addding sequence to stops, logic needed to be changed
        sequence = 1
        if self.trip_id:
            sequence = len(self.trip.stops_list) + 1

        attempted_packages = False
        first_order = None
        proceed = False
        stop_added = False
        for order in order_qs:
            if not first_order:
                first_order = order
            if order.status in [ORDER_RETURNED] and not order.return_to_origin:
                response = True
                attempted_packages = True
            else:
                response, stop_added = self.__add_b2c_order_as_trip_stop(order=order, sequence=sequence)
                proceed = True
                sequence = sequence + 1

            if not response:
                return False, None
            if stop_added:
                self.__update_order(order, attempted_packages, driver)

        if not proceed:
            return False, first_order
        if self.event_list:
            Event.objects.bulk_create(self.event_list)
        return True, first_order

    def __update_order(self, order, attempted_packages, driver):
        order_data = {}
        order_data['driver'] = driver.id
        if self.trip_id:  # scanning packages in between the trip
            if self.trip.status == TRIP_IN_PROGRESS:
                if hasattr(order, 'new_status') and order.new_status:
                    order_data['status'] = order.new_status
                    if order.new_status in [StatusPipeline.ORDER_MOVING_TO_HUB, StatusPipeline.ORDER_MOVING_TO_HUB_RTO]:
                        self.context['remarks'] = self.remarks
                else:
                    order_data['status'] = OUT_FOR_DELIVERY
            elif self.trip.status in [TRIP_NEW, DRIVER_ACCEPTED]:
                order_data['status'] = self.order_status
        elif not attempted_packages:
            order_data['status'] = self.order_status

        if order_data.get('status', None):
            self.context['geopoint'] = self.geopoint
            self.context['address'] = self.address
            self.context['hub_associate'] = self.hub_associate
            _event_data = {}
            _event_data['order_id'] = order.id
            _event_data['driver_id'] = driver.id
            _event_data['status'] = order_data.get('status')
            _event_data['current_location'] = self.geopoint
            _event_data['current_address'] = self.address
            _event_data['remarks'] = self.remarks
            _event_data['hub_associate'] = self.hub_associate
            self.event_list.append(Event(**_event_data))

        # order = UpdateModelMixin().update(data=order_data, instance=order,
        #                                   serializer_class=OrderSerializer, context=self.context)
        # order.save()
        order_data['updated_time'] = timezone.now()
        ob = Order.objects.filter(id=order.id)
        ob.update(**order_data)
        if self.trip and self.trip.status == TRIP_IN_PROGRESS and order_data['status'] == OUT_FOR_DELIVERY:
            SendSMS().send_message_for_order_event(ob.first(),
                        order_data['status'], TRIP_NEW, trip=self.trip)

        broadcast_order_details_to_dashboard.apply_async((order.id,), )

    def __add_b2c_order_as_trip_stop(self, order, sequence):
        # Assigns each order as Trip Stops
        stop_added = False
        stop_in_trip = Stop.objects.filter(order=order, trip=self.trip).first()
        add_order = False
        if stop_in_trip and stop_in_trip.status == StatusPipeline.TRIP_COMPLETED \
            and order.status in StatusPipeline.RETRIP_ORDER_STATUSES:
            add_order = True

        # stop_in_other_trip = Stop.objects.filter(order=order,
        #                                          trip__status__in=[TRIP_NEW,
        #                                                            TRIP_IN_PROGRESS,
        #                                                            TRIP_ALL_STOPS_DONE]).exclude(
        #     trip=self.trip).exists()
        #
        #
        # if stop_in_other_trip:
        #     return False, False

        if not stop_in_trip or add_order:
            # TODO use below line after unified app is fully rolled out to shipment orders
            # return create_stop(trip=self.trip, order=order.id, sequence=sequence)
            waypoint = order.waypoint_set.all().first()
            waypoint_id = waypoint.id if waypoint else None
            self.remarks = None
            self.pickup_hub = None
            stop_type, stop_address_id, stop = self.get_stop_type(order)
            # pass stop_address param to create pickup stop
            if not stop:
                stop = create_stop(trip=self.trip, order=order.id, sequence=sequence,
                            waypoint=waypoint_id, stop_type=stop_type,
                            stop_address=stop_address_id)
            if not stop:
                self.status_code = status.HTTP_400_BAD_REQUEST
                self.error_message = 'Failed to add stop'
                return False, stop_added
            if stop and order.has_pickup and stop_type in [HUB_DELIVERY, HUB_RTO]:
                """
                For every hub delivery add order to StopOrder table, as there
                can be multiple orders as part of same stop for Hub delivery.
                Currently hub delivery is applicable only for orders with pickup.
                """
                self.__add_order_to_stop(order, stop)

            order.new_status = None
            if stop.stop_type and self.trip_id:  # scanning packages in between the trip
                order.new_status = ORDER_STATUS_BY_STOPS.get(stop.stop_type)
            if stop_type == HUB_DELIVERY and self.trip_workflow and self.trip_workflow.is_wms_flow:
                order.xdk = True
                order.save()
            stop_added = True
        return True, stop_added

    def __update_trip(self, order, number_of_orders, **kwargs):
        trip_data = {
            'assigned': self.trip.assigned + number_of_orders,
        }
        trip_workflow = kwargs.pop('trip_workflow', None)
        if trip_workflow:
            trip_data.update({"trip_workflow": trip_workflow})
        else:
            trip_data.update({"trip_workflow": order.customer_contract.trip_workflow_id
            if order.customer_contract.trip_workflow else None})

        if not self.trip.customer_contract:
            trip_data['customer_contract'] = order.customer_contract.id

        if not kwargs.pop('is_kiosk_order', False) and hasattr(order, 'hub') and order.hub:
            trip_data.update({'hub': order.hub_id})

        self.trip = UpdateModelMixin().update(data=trip_data,
                                              instance=self.trip,
                                              serializer_class=TripSerializer)
        self.trip.save()
        # """
        # perform trip update without using serializer
        # """
        # Trip.objects.filter(id=self.trip.id).update(**trip_data)

    def get_stop_type(self, order):
        stop_type = ORDER_DELIVERY
        stop_address = None
        stop = None
        if order.has_pickup:
            #TODO: handle status PICKUP_ATTEMPTED, check if any other valid statuses needs to added
            if order.status in [StatusPipeline.ORDER_NEW,
                                StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.PICKUP_ATTEMPTED,
                                StatusPipeline.DRIVER_ASSIGNED,
                                StatusPipeline.DRIVER_ACCEPTED,
                                StatusPipeline.ORDER_PICKUP_FAILED] \
                and order.latest_hub is None:
                stop_type = PICKUP
                stop_address = order.pickup_address_id
            else:  # TODO : check if any condition needed here --> if order.status == StatusPipeline.REACHED_AT_HUB:
                if order.pickup_address:
                    self.pickup_hub = get_hub_from_pincode(
                        order.pickup_address.postcode)
                stop_type = self.check_for_hub_movement(order)
                if stop_type == HUB_DELIVERY or stop_type == HUB_RTO:
                    self.remarks = order.hub.name
                    #check if stop already exists with orders for same hub delivery, add order to existing stop
                    stop = self.__hub_stop_exists(order, stop_type)
                    if stop:
                        # return stop as stop doesn't needs to be added, only order has to be added to StopOrders
                        return stop_type, None, stop
                    # TODO: shipping adress in hub needs to be populated or else use shipping_address_from_hub_address
                    if stop_type == HUB_RTO:
                        stop_address = self.pickup_hub.shipping_address.id if \
                            self.pickup_hub.shipping_address else \
                            self.get_shipping_address_from_hub_address(
                                self.pickup_hub)
                        if not self.pickup_hub.shipping_address:
                            Hub.objects.filter(id=self.pickup_hub.id).update(
                                shipping_address=stop_address)
                    else:
                        stop_address = order.hub.shipping_address.id if \
                            order.hub.shipping_address else \
                            self.get_shipping_address_from_hub_address(order.hub)
                        if not order.hub.shipping_address:
                            Hub.objects.filter(id=order.hub.id).update(
                                shipping_address=stop_address)
                elif stop_type == ORDER_RTO:
                    stop_address = order.pickup_address_id
        return stop_type, stop_address, stop

    def check_for_hub_movement(self, order, trip_work_flow=None):
        trip = Trip.objects.filter(stops__order=order, status__in=[TRIP_NEW, TRIP_IN_PROGRESS]).first()
        create_delivery_stop = False
        if trip:
            workflow = trip.trip_workflow if not trip_work_flow else trip_work_flow
            if workflow.perform_delivery:
                create_delivery_stop = True
        if not order.has_pickup or create_delivery_stop:
            return ORDER_DELIVERY
        #TODO: handle population of latest_hub
        """
        If order is of type pickup, order has been picked up, and
        pickup pincode is same as delivery picode, then order can be delivered
        as part of same trip by again scanning the order to add delivery stop
        """

        if (order.status == StatusPipeline.ORDER_PICKED
            and order.latest_hub is None
            and self.pickup_hub == order.hub) \
            or ((order.latest_hub == order.hub \
                 or order.status in [
                     StatusPipeline.ORDER_RETURNED_TO_HUB,
                     StatusPipeline.OUT_FOR_DELIVERY,
                     StatusPipeline.UNABLE_TO_DELIVER]) \
                and not order.return_to_origin):
            stop_type = ORDER_DELIVERY
        elif order.latest_hub == self.pickup_hub and order.return_to_origin:
            stop_type = ORDER_RTO
        else:
            stop_type = HUB_DELIVERY

        if stop_type == HUB_DELIVERY and order.return_to_origin:
            stop_type = HUB_RTO
            self.remarks = self.pickup_hub.name

        return stop_type

    def __hub_stop_exists(self, order, stop_type):
        """
        check if a stop exists in the trip with hub address same as the hub of this order
        :return - True if it exists, also returns stop of the hub
        """
        hub = self.pickup_hub if stop_type is HUB_RTO else order.hub

        stop_order = StopOrders.objects.filter(trip=self.trip, hub=hub,
                                               stop__stop_type=stop_type).first()
        if stop_order:
            return stop_order.stop

        return None

    def __add_order_to_stop(self, order, stop):
        if stop.stop_type == HUB_RTO:
            hub = self.pickup_hub
        else:
            hub = order.hub
        StopOrders.objects.create(order=order, stop=stop,
                                  hub=hub, trip=self.trip)

    def get_shipping_address_from_hub_address(self, hub):
        ship_addr = {
            'line1': hub.address.line1,
            'line2': hub.address.line2,
            'line3': hub.address.line3,
            'line4': hub.address.line4,
            'state': hub.address.state,
            'postcode': hub.address.postcode,
            'country': hub.address.country,
            'geopoint': hub.address.geopoint
        }
        #TODO: add shipping address to hub if not there
        addr = ShippingAddress.objects.create(**ship_addr)
        return addr.id


