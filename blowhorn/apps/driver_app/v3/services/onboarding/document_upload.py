import logging
import random
from django.utils.translation import ugettext_lazy as _
from django.db.models import Value, CharField, Q
from django.db.models.expressions import F
from rest_framework import status

from blowhorn.address.utils import AddressUtil
from config.settings.status_pipelines import ONBOARDING, SUPPLY
from blowhorn.vehicle.serializers import VehicleDocumentPageSerializer, VehicleDocumentSerializer, VendorDocumentPageSerializer, VendorDocumentSerializer
from blowhorn.driver.serializers import DriverDocumentPageSerializer, DriverDocumentSerializer
from blowhorn.document.constants import (ACTIVE, DOCUMENT_AADHAR, DOCUMENT_DL, DOCUMENT_PHOTO, DOCUMENT_RC,
        DRIVER_DOCUMENT, REJECT, VEHICLE_DOCUMENT, VENDOR_DOCUMENT,
        VERIFICATION_PENDING)
from blowhorn.integration_apps.decentro.user_update import UserUpdate
from blowhorn.apps.operation_app.v3.helper import add_vehicle
from blowhorn.apps.driver_app.v3.constants import DL_VALID_OCR_STATES
from blowhorn.oscar.core.loading import get_model

Driver = get_model('driver', 'Driver')
DriverVehicleMap = get_model('driver', 'DriverVehicleMap')
DriverDocument = get_model('driver', 'DriverDocument')
Contract = get_model('contract', 'Contract')
DocumentType = get_model('document', 'DocumentType')
DocumentEvent = get_model('document', 'DocumentEvent')
VehicleDocument = get_model('vehicle', 'VehicleDocument')
VendorDocument = get_model('vehicle', 'VendorDocument')
Vehicle= get_model('vehicle', 'Vehicle')
Vendor = get_model('vehicle', 'Vendor')
City = get_model('address', 'City')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class DocumentProcessor(object):

    def __init__(self, user, driver, vendor, data):
        self.page_count = int(data.get('page_count',1))
        self.identifier = data.get('identifier', None)
        self.doc_number = data.get('doc_number', None)
        self.is_self_reg = data.get('is_self_reg', None)
        self.name = data.get('name', None)
        self.code = data.get('doc_type', None)
        self.state_name = data.get('document_state', None)
        self.driver = driver
        self.vendor = vendor
        self.user = user
        self.vehicle_model = data.get('vehicle_model', None)
        self.body_type = data.get('body_type', None)
        self.model_year = data.get('model_year', None)
        self.result = data.get('result', None)

        self.input_file_list = []
        for i in range(1,self.page_count+1):
            name = 'file{}'.format(i)
            f = data.get(name, None)
            self.input_file_list.append(f)
    
    def get_document_event_spoc(self, document):
        support_number = None
        identifier = document.document_type.identifier
        try:
            if identifier == DRIVER_DOCUMENT:
                if self.driver:
                    doc_evs = DocumentEvent.objects.filter(
                                driver_document=document)
                elif self.vendor:
                    doc_evs = DocumentEvent.objects.filter(
                            vendor_document=document)
            elif identifier == VENDOR_DOCUMENT:
                doc_evs = DocumentEvent.objects.filter(
                            vendor_document=document)
            else:
                doc_evs = DocumentEvent.objects.filter(
                            vehicle_document=document)

            if doc_evs.exists():
                de = doc_evs.latest('modified_date')
                if de.action_owner and de.action_owner.phone_number:
                    support_number = str(de.action_owner.phone_number.national_number)

        except Exception as e:
            pass

        return support_number

    def __get_spoc_number(self):
        city_id = 1
        if self.driver:
            city_id = self.driver.operating_city
        elif self.vendor:
            vendor_data = self.vendor.preferred_locations.first()
            if vendor_data.exists():
                city_id = vendor_data.city_id

        spocs = Contract.objects.filter(city=city_id, 
                    spocs__email__isnull=False).values_list(
                    'spocs__phone_number',flat = True
                )

        if spocs.count() == 0:
            spocs = City.objects.filter(id=city_id, process_associates__is_active=True). \
                        values_list('process_associates__phone_number',flat=True)

        spoc_count = spocs.count()
        number = spocs[random.randint(0, spoc_count - 1)]
        return number
    
    def get_state(self):
        if self.driver:
            city_id = self.driver.operating_city
        elif self.vendor:
            vendor_data = self.vendor.preferred_locations.first()
            if vendor_data.exists():
                city_id = vendor_data.city_id

        state = AddressUtil().get_state_from_city(city_id)
        return state

    def list_partner_documents(self, code=None, is_document_mandatory=False,
                                    is_mandatory_for_registration=False, identifier_list=[]):
        _dqs = {}
        if code:
            _dqs['code'] = code
        else:
            if self.driver:
                _dqs['is_driver_document'] = True
            elif self.vendor:
                _dqs['is_fleet_document'] = True

            if is_document_mandatory:
                _dqs['is_document_mandatory'] = is_document_mandatory

            if is_mandatory_for_registration:
                _dqs['is_mandatory_for_registration'] = is_mandatory_for_registration

            if identifier_list:
                _dqs['identifier__in'] = identifier_list

        doclist = DocumentType.objects.filter(**_dqs).annotate(
                        page_count=F('no_of_pages'), status=Value('', CharField())).order_by('-sequence').values(
                            'name', 'code', 'is_ocr_eligible', 'instructions', 
                            'instruction_heading', 'identifier',
                            'no_of_pages', 'regex', 'error_message', 'page_count', 'status'
                        )

        total_count = len(doclist)
        active_count = 0
        user_docs = []
        photo = None
        if self.driver:
            photo = self.driver.photo.url if self.driver.photo else photo
            _doc = DriverDocument.objects.filter(driver=self.driver)
            if _doc.exists():
                user_docs.append(_doc) 

            vehicle_list = self.driver.vehicles.values()
            if vehicle_list.exists():
                vehicle_id = vehicle_list[0].get('id')
                _doc = VehicleDocument.objects.filter(vehicle_id = vehicle_id)
                if _doc.exists():
                    user_docs.append(_doc) 

        elif self.vendor:
            photo = self.vendor.photo.url if self.vendor.photo else photo
            _doc = VendorDocument.objects.filter(vendor=self.vendor)
            if _doc.exists():
                user_docs.append(_doc)

        document_list = []
        # support_number_found = False
        if user_docs:
            for doctype in doclist:
                _code = doctype['code']

                if _code == DOCUMENT_DL:
                    state = self.get_state()
                    if state.name in DL_VALID_OCR_STATES:
                        doctype['is_ocr_eligible'] = True

                if _code == DOCUMENT_PHOTO:
                    if photo:
                        doctype['status'] = ACTIVE
                        active_count +=1
                else:
                    tempdoc = None
                    if doctype['identifier'] == DRIVER_DOCUMENT:
                        tempdoc = user_docs[0].filter(document_type__code = _code)
                    else:
                        if len(user_docs) == 2:
                            tempdoc = user_docs[1].filter(document_type__code = _code)

                    if tempdoc and tempdoc.exists():
                        _td = tempdoc.latest('created_date')
                        doctype['status'] = _td.status
                        if _td.status == ACTIVE:
                            active_count +=1

                document_list.append(doctype)
        else:
            for doctype in doclist:
                if doctype['code'] == DOCUMENT_DL:
                    state = self.get_state()
                    if state.name in DL_VALID_OCR_STATES:
                        doctype['is_ocr_eligible'] = True
                if doctype['code'] == DOCUMENT_PHOTO and photo:
                        doctype['status'] = ACTIVE
                document_list.append(doctype)           

        if len(document_list) == active_count:
            if self.driver and self.driver.status in [SUPPLY, ONBOARDING]:
                self.driver.status=ACTIVE
                self.driver.save()
            if self.vendor and self.vendor.status in [SUPPLY, ONBOARDING]:
                self.vendor.status=ACTIVE
                self.vendor.save()

        return total_count, active_count, document_list

    def add_user_photo(self):
        if self.driver:
            if not self.driver.photo:
                self.driver.photo = self.input_file_list[0]
                if self.driver.status == SUPPLY:
                    self.driver.status=ONBOARDING
                self.driver.save()

        elif self.vendor:
            if not self.vendor.photo:
                self.vendor.photo = self.input_file_list[0]
                if self.vendor.status == SUPPLY:
                    self.vendor.status=ONBOARDING
                self.vendor.save()

    def upload_partner_document(self):
        doc_type = DocumentType.objects.filter(code=self.code).first()

        if len(self.input_file_list) == 0:
            message = 'Please upload the document'
            return status.HTTP_202_ACCEPTED, message

        if self.code == DOCUMENT_PHOTO:
            self.add_user_photo()
            return status.HTTP_202_ACCEPTED, "Photo uploaded successfully"

        _doc_number = UserUpdate(self.driver, self.vendor, self.identifier, doc_type, self.result).get_document_number()
        self.doc_number = _doc_number if _doc_number else None
        if not self.doc_number:
            message = "Document Number not found"
            return status.HTTP_400_BAD_REQUEST, message

        driver_doc_exists = False
        vendor_doc_exists = False
        if self.driver:
            if doc_type.code != DOCUMENT_AADHAR:
                driver_doc_exists = DriverDocument.objects.filter(document_type_id=doc_type.id, number=self.doc_number,
                                        status=ACTIVE).exclude(driver_id=self.driver.id).exists()

            exclude_param = Q()
            if self.driver.vehicles.first():
                exclude_param = Q(vehicle_id=self.driver.vehicles.first().id)

        elif self.vendor:
            vendor_doc_exists = VendorDocument.objects.filter(document_type_id=doc_type.id, number=self.doc_number,
                                    status=ACTIVE).exclude(vendor_id=self.vendor.id).exists()

        vehicle_doc_exists = VehicleDocument.objects.filter(document_type_id=doc_type.id, number=self.doc_number,
            status=ACTIVE).exclude(exclude_param).exists()

        if driver_doc_exists or vendor_doc_exists or vehicle_doc_exists:
            message = 'Please call support since document number %s already exists' % self.doc_number
            return status.HTTP_400_BAD_REQUEST, message
        
        is_valid_no, _doc_number = UserUpdate(self.driver, self.vendor, self.identifier, doc_type, self.result)._process_document()
        
        docstatus = ACTIVE if is_valid_no else VERIFICATION_PENDING
        document_data = {
            'file': self.input_file_list[0],
            'document_type': doc_type.id,
            'status': docstatus,
            'number': self.doc_number,
            'created_by': self.user,
            'modified_by': self.user
        }

        vehicle = None
        if self.driver and self.identifier == VEHICLE_DOCUMENT:
            msg, vehicle = self.process_vehicle_documents()
            if msg:
                return status.HTTP_400_BAD_REQUEST, msg
            document_data.update({'vehicle': vehicle.id, 'number' : self.doc_number})
        
        if self.driver:
            if self.identifier == DRIVER_DOCUMENT:
                document_data.update({'driver': self.driver.id})
                document_serializer = DriverDocumentSerializer(data=document_data)

            if self.identifier == VEHICLE_DOCUMENT:
                document_serializer = VehicleDocumentSerializer(data=document_data)

        elif self.vendor:
            if self.identifier == DRIVER_DOCUMENT:
                document_data.update({'vendor': self.vendor.id})
                document_serializer = VendorDocumentSerializer(data=document_data)
            # if self.identifier == VEHICLE_DOCUMENT:
            #     document_serializer = VehicleDocumentSerializer(data=document_data)

        if document_serializer.is_valid():
            if self.identifier == DRIVER_DOCUMENT:
                if self.driver:
                    DriverDocument.objects.filter(driver = self.driver,
                            document_type = doc_type,
                            status = VERIFICATION_PENDING).update(
                            status = REJECT)
                
                elif self.vendor:
                    VendorDocument.objects.filter(vendor = self.vendor,
                            document_type = doc_type,
                            status = VERIFICATION_PENDING).update(
                            status = REJECT)

            elif self.identifier == VEHICLE_DOCUMENT:
                VehicleDocument.objects.filter(vehicle = vehicle,
                        document_type = doc_type,
                        status = VERIFICATION_PENDING).update(
                        status = REJECT)

            #always driver or vendor is passed
            doc = document_serializer.save(created_by=document_data.get(
                'created_by'), modified_by=document_data.get(
                'modified_by'))

            if docstatus == ACTIVE:
                self.process_events(vehicle)

            if doc:
                document_page_list = [
                    {
                        'page_number' : (i+1),
                        'file' : self.input_file_list[i] 
                    } for i in range(self.page_count)
                ]

                document_page_list[0]['file'] = doc.file
                dps_list = []
                if self.identifier == DRIVER_DOCUMENT:
                    if self.driver:
                        for element in document_page_list:
                            element.update({'driver_document' : doc.id})
                            documentpage_serializer = DriverDocumentPageSerializer(data=element)
                            if documentpage_serializer.is_valid():
                                dps_list.append(documentpage_serializer)
                    elif self.vendor:
                        for element in document_page_list:
                            element.update({'vendor_document' : doc.id})
                            documentpage_serializer = VendorDocumentPageSerializer(data=element)
                            if documentpage_serializer.is_valid():
                                dps_list.append(documentpage_serializer)

                if self.identifier == VEHICLE_DOCUMENT:
                    for element in document_page_list:
                        element.update({'vehicle_document' : doc.id})
                        documentpage_serializer = VehicleDocumentPageSerializer(data=element)
                        if documentpage_serializer.is_valid():
                            dps_list.append(documentpage_serializer)

                if len(dps_list) == self.page_count:
                    for element in dps_list:
                        element.save(created_by=document_data.get(
                            'created_by'), modified_by=document_data.get(
                            'modified_by'))

            return status.HTTP_202_ACCEPTED, {}
        else:
            return status.HTTP_400_BAD_REQUEST, document_serializer.errors

    def process_events(self, vehicle):
        if self.identifier == DRIVER_DOCUMENT:
            if self.driver:
                DocumentEvent.objects.filter(driver_document__driver =
                        self.driver, driver_document__document_type__code = self.code,
                        status = VERIFICATION_PENDING
                        ).update(status = ACTIVE)
            elif self.vendor:
                DocumentEvent.objects.filter(vendor_document__vendor =
                        self.vendor, vendor_document__document_type__code = self.code, 
                        status = VERIFICATION_PENDING
                        ).update(status = ACTIVE)
        elif self.identifier == VEHICLE_DOCUMENT and vehicle:
            DocumentEvent.objects.filter(vehicle_document__vehicle__driver =
                        self.driver, vehicle_document__document_type__code = self.code,
                        status = VERIFICATION_PENDING
                        ).update(status = ACTIVE)

    def process_vehicle_documents(self):
        '''
            Format the vehicle data as per the model
            and return in dictionary format
        ''' 
        message = None
        vehicle = None
        vehicles = self.driver.vehicles.all()
        vehicles_mapped_count = 0 if not vehicles else vehicles.count()

        if vehicles_mapped_count > 1:
            message = "Driver is associated with more than one vehicle"
            return message, vehicle

        if self.code == DOCUMENT_RC:
            if vehicles_mapped_count == 1:
                message = 'Driver is already associated with a vehicle'
                return message, vehicle

            _vehicle = Vehicle.objects.filter(registration_certificate_number=self.doc_number).first()
            if not _vehicle:
                vehicle_data = {
                    'registration_certificate_number': self.doc_number,
                    'vehicle_model': self.vehicle_model,
                    'body_type': self.body_type,
                    'model_year': self.model_year
                }
                _vehicle = add_vehicle(vehicle_params=vehicle_data, is_update=False)

            vehicle = _vehicle
            dvm = DriverVehicleMap.objects.filter(vehicle=_vehicle).first()
            if dvm:
                if dvm.driver != self.driver:
                    message = 'This vehicle number is already mapped to another driver'
                    return message, vehicle
            else:
                DriverVehicleMap.objects.create(driver=self.driver, vehicle=vehicle)

        else:
            if vehicles_mapped_count == 0:
                    message = 'Please upload the RC first to add vehicle'
                    return message, vehicle
            else:
                vehicle = vehicles[0]

        return message, vehicle