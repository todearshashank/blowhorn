# Django and System libraries
import json
import logging
from datetime import timedelta
from django.db.models import Q
from django.db import transaction
from django.utils import timezone

# 3rd party libraries
from rest_framework import status

from blowhorn.apps.driver_app.v3.serializers.contract import TripWorkFlowSerializer
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.driver_app.v3.serializers.trip import TripStopSerializer
from blowhorn.apps.driver_app.v3.services.trip.send_sms import SendSMS
from blowhorn.common.helper import CommonHelper
from blowhorn.order.admin import assign_driver
from config.settings import status_pipelines as StatusPipeline
from config.settings.status_pipelines import (
    TRIP_IN_PROGRESS, TRIP_END_STATUSES, TRIP_NEW,
    TRIP_ALL_STOPS_DONE, ORDER_PICKED, ORDER_RETURNED_RTO, ORDER_PICKUP_FAILED,
    ORDER_RETURNED_RTO_FAILED
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.customer.tasks import broadcast_order_details_to_dashboard
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_SHIPMENT

from blowhorn.apps.driver_app.constant import EXPECTED_DELIVERY_TIME_DELTA


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Stop = get_model('trip', 'Stop')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
DriverLedger = get_model('driver', 'DriverLedger')
Event = get_model('order', 'Event')


class PickupMixin(object):

    def _initialize_attributes(self, data, trip_id):
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.user = self.request.user
        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.address = None

        self.pickedup_order_qs = None
        self.trip_workflow = None
        self.action = data.get('action', None)
        # if self.stop.stop_type == PICKUP:
        picked_orders = data.get('pickup_order_list')
        self.pickedup_order_qs = self.__get_orders_from_ref_num(
            json.loads(picked_orders))

        rto_orders = data.get('rto_order_list')
        self.rto_order_qs = self.__get_orders_from_ref_num(
            json.loads(rto_orders))

        if not self.pickedup_order_qs and not self.rto_order_qs:
            self.error_message = 'Scanned orders are not valid'
            self.status_code = status.HTTP_400_BAD_REQUEST

        if self.pickedup_order_qs and self.rto_order_qs:
            self.error_message = 'Cannot scan pickup and return orders in same attempt'
            self.status_code = status.HTTP_400_BAD_REQUEST

        if self.pickedup_order_qs:
            if self.action and self.action not in [ORDER_PICKED, ORDER_PICKUP_FAILED]:
                logger.info('Status sent from driver app: %s' % self.action)
                self.error_message = 'Invalid status sent'
                self.status_code = status.HTTP_400_BAD_REQUEST

            self.order_status = self.action if self.action else ORDER_PICKED
            self.order_qs = self.pickedup_order_qs
        else:
            if self.action and self.action not in [ORDER_RETURNED_RTO, ORDER_RETURNED_RTO_FAILED]:
                logger.info('Status sent from driver app: %s' % self.action)
                self.error_message = 'Invalid status sent'
                self.status_code = status.HTTP_400_BAD_REQUEST

            self.order_status = self.action if self.action else ORDER_RETURNED_RTO
            self.order_qs = self.rto_order_qs

        query = Q(pk=trip_id, status=TRIP_IN_PROGRESS)
        self.trip = Trip.objects.prefetch_queryset(
            query=query, stops_list=True, distinct_orders=True).first()
        self.order_stops = Stop.objects.filter(order__in=self.order_qs,
                                               trip=self.trip,
                                               status__in=[TRIP_NEW,
                                                           TRIP_IN_PROGRESS])

        if self.order_qs and self.order_qs[0].customer_contract and \
            self.order_qs[0].customer_contract.contract_type in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
            self.address = CommonHelper.get_address_from_location_json(location)

        self.completed_stop_ids = []
        for stop in self.order_stops:
            self.completed_stop_ids.append(stop.id)

        # Take any one stop for route trace update out of all the pick stops getting completed at once.
        self.stop = self.order_stops[0] if self.order_stops else None

        if not self.trip or not self.stop or (self.trip != self.stop.trip):
            self.error_message = 'Invalid Data Sent !'

        self.next_stop = None
        self.no_of_pending_stops = None
        self.next_stop = None
        self.is_last_stop = False

        try:
            trip_last_response = json.loads(data.get('trip_last_response'))
            self.current_step = int(trip_last_response.get('current_step'))
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data Sent !'

        if self.error_message:
            self.response_data = self.error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return


    def __get_response_data(self):
        self.response_data = {
            'is_all_stops_done': self.trip.is_all_stops_done(),
            'current_step': self.trip.current_step,
            'is_trip_in_progress': True,
            'is_loaded': True,
            'next_stop_id': self.next_stop.id if self.next_stop else '',
            'completed_stops_id': self.completed_stop_ids
        }
        if self.next_stop:
            order = self.next_stop.order
            if order:
                contract = order.customer_contract
                self.response_data.update({"order_workflow":  TripWorkFlowSerializer(contract.trip_workflow).data})
        if self.trip_workflow and self.trip_workflow.pick_orders and self.trip_workflow.perform_delivery:
            self.response_data.update({"sync": True})
        logger.info('Response data sent: %s' % self.response_data)

    def __get_pending_stops_and_next_stop(self):
        """
        No dependency on status(In-Progress, New) of current stop and next stop.

        hence won't fail in case of multiple requests
        """
        stop_qs = self.trip.stops.exclude(status__in=TRIP_END_STATUSES)
        no_of_pending_stops = len(stop_qs)
        in_progress_stop = None
        if no_of_pending_stops > 0:
            for stop in stop_qs:
                if stop.status == TRIP_IN_PROGRESS:
                    in_progress_stop = stop
                    break
            if not in_progress_stop:
                in_progress_stops = sorted(stop_qs, key=lambda x: x.sequence)
                in_progress_stop = in_progress_stops[0]
            return no_of_pending_stops, in_progress_stop
        return no_of_pending_stops, None

    def __make_updates(self):
        expected_delivery_time = timezone.now() + timedelta(
                hours=EXPECTED_DELIVERY_TIME_DELTA)
        self.order_qs.update(
            status=self.order_status,
            updated_time=timezone.now(),
            modified_date=timezone.now(),
            modified_by=self.user,
            expected_delivery_time=expected_delivery_time
        )

        for order in self.order_qs:
            broadcast_order_details_to_dashboard.apply_async((order.id,), )

        # Add order event
        order_event_list = []
        for order in self.order_qs:
            order_event_list.append(Event(status=self.order_status,
                                          order=order,
                                          driver=self.driver,
                                          current_location=self.geopoint,
                                          current_address=self.address))
        Event.objects.bulk_create(order_event_list)

        #Update stops associated with orders
        self.order_stops.update(status=StatusPipeline.TRIP_COMPLETED,
                                end_time=timezone.now())

        stop_data = {'status': StatusPipeline.TRIP_COMPLETED}

        if self.stop:
            self.stop = UpdateModelMixin().update(data=stop_data,
                                                  instance=self.stop,
                                                  serializer_class=TripStopSerializer)
            self.stop.save()
        if self.order_status == ORDER_PICKED:
            self.trip_workflow = self.trip.trip_workflow
            if self.trip_workflow.pick_orders and self.trip_workflow.perform_delivery:
                assign_driver(self.order_qs, None, self.driver, self.request, self.trip)

    def __update_trip(self):
        self.no_of_pending_stops, self.next_stop = self.__get_pending_stops_and_next_stop()
        self.is_last_stop = True if self.no_of_pending_stops == 0 else False
        if self.action == ORDER_PICKED:
            self.trip.picked = self.trip.picked + self.pickedup_order_qs.count()
            self.trip.save()

        if self.is_last_stop:
            self.trip.current_step = self.current_step + 1
            self.trip.status = TRIP_ALL_STOPS_DONE
            self.trip.save()

        if self.next_stop:
            self.next_stop.status = StatusPipeline.TRIP_IN_PROGRESS
            self.next_stop.save()

    def __send_msg(self):

        for order in self.order_qs:
            SendSMS().send_message_for_order_event(order, self.order_status,
                                                   trip=self.trip)

    def _attempt_pickup(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():
            self.__make_updates()
            self.__update_trip()
            no_of_pending_stops, self.next_stop = self.__get_pending_stops_and_next_stop()
            self.__send_msg()

        self.__get_response_data()

    def __get_orders_from_ref_num(self, ref_list):

        # qs = Order.objects.filter(
        #     reference_number__in=[str(i) for i in ref_list if i is not None])
        qs = Order.objects.filter(Q(number__in=ref_list))
        return qs
