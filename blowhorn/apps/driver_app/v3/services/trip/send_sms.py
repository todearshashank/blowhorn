import logging
import re
import json
import phonenumbers
from django.conf import settings
from random import randint

# blowhorn imports
from blowhorn.common.tasks import send_sms
from blowhorn.utils.functions import utc_to_ist

from blowhorn.common import sms_templates
from blowhorn.trip.models import Trip
from blowhorn.order.const import PAYMENT_STATUS_PAID
from blowhorn.order.models import OrderMessage
from blowhorn.customer.models import SmsConfig
from blowhorn.logapp.models import SmsLog
from blowhorn.utils.mail import Email
from blowhorn.apps.driver_app.v3.helpers.trip import get_asset_for_trip


class SendSMS(object):

    def send_otp_for_stop(self, order):
        error_msg = None
        if not order:
            error_msg = 'No customer order to send sms'
            return error_msg, None

        phone_number = order.shipping_address.phone_number \
            if order.shipping_address else None
        try:
            phonenumbers.parse(phone_number,
                               settings.COUNTRY_CODE_A2)
        except:
            logging.error(
                'Failed to parse mobile number for order otp for mobile no. {}'.format(
                    phone_number))

        otp = order.otp
        if not otp:
            otp = randint(1001, 9999)

        try:
            time_format = '%-I:%M %p'
            delivery_time = utc_to_ist(order.expected_delivery_time).strftime(
                time_format)

            template_dict = sms_templates.CUSTOMER_DELIVERY_OTP_TEMPLATE
            message = template_dict['text'] % (
                order.customer.name, order.reference_number, delivery_time,
                order.driver.user.phone_number, otp)

            template_json = json.dumps(template_dict)
            send_sms.apply_async(args=[str(phone_number), message, template_json],
                kwargs={"customer_id":order.customer.id})

        except Exception as e:
            error_msg = e.args[0]
            return error_msg, None

        return error_msg, otp

    def send_otp_for_end_trip(self, trip_id, geopoint, request=None):
        error_msg = None
        otp_sent = 0

        trip = Trip.objects.filter(id=trip_id).select_related('hub').first()

        if not trip:
            error_msg = 'No Valid trip found'
            return error_msg, None

        hub = trip.hub
        hub_contacts = hub.customer_hub_contact.all().values('mobile', 'email')

        # phone_numbers = Hub.objects.filter(
        #     customer=trip.customer_contract.customer,
        #     address__geopoint__distance_lte=(
        #     geopoint, Distance(m=100))).values_list(
        #     'customer_hub_contact__mobile', flat=True)

        phone_numbers = []
        email_ids = []

        for hub_contact in hub_contacts:
            if hub_contact.get('mobile'):
                phone_numbers.append(hub_contact.get('mobile'))
            if hub_contact.get('email'):
                email_ids.append(hub_contact.get('email'))

        phone_numbers = list(set(phone_numbers))
        email_ids = list(set(email_ids))

        if not phone_numbers:
            error_msg = 'No Hub customer contact found for the store'
            return error_msg, None

        otp = trip.otp

        if not otp:
            otp = randint(1001, 9999)
            Trip.objects.filter(id=trip.id).update(otp=otp)

        asset_msg = get_asset_for_trip(trip)

        if asset_msg:

            template_dict = sms_templates.COLLECT_ASSET_TEMPLATE
            asset_msg = template_dict['text'] % (trip.customer_contract.customer.name, asset_msg)

        for phone_number in phone_numbers:
            try:
                phonenumbers.parse(phone_number,
                                   settings.COUNTRY_CODE_A2)
            except:
                logging.error(
                    'Failed to parse mobile number for order otp for mobile no. {}'.format(
                        phone_number))
                continue

            template_dict = sms_templates.END_TRIP_ASSET_STORE_TEMPLATE if asset_msg else \
                sms_templates.END_TRIP_OTP_TEMPLATE
            message = template_dict['text'] % (
            otp, trip.driver.name, trip.driver.user.phone_number)

            message = asset_msg + message
            template_json = json.dumps(template_dict)
            send_sms.apply_async(args=[str(phone_number), message, template_json])

            subject = 'BLOWHORN - OTP For driver %s' % trip.driver
            if email_ids and settings.ENABLE_SLACK_NOTIFICATIONS:
                Email().send_email_message(email_ids, subject, message)
            otp_sent += 1

        if otp_sent == 0:
            error_msg = 'Failed to send msg to Hub customer contact'

        return error_msg, otp

    def send_message_for_order_event(self, order, status, stop_status=None,
                                     trip=None, site_url=None, msg_data=None):

        error_msg = None
        otp = order.otp
        sms_configs = SmsConfig.objects.filter(contract=order.customer_contract,
                                               customer=order.customer,
                                               event=status,
                                               stop_status=stop_status,
                                               is_active=True)

        if not sms_configs:
            return None, None

        phone_number = order.shipping_address.phone_number \
            if order.shipping_address else None
        try:
            phonenumbers.parse(phone_number, settings.COUNTRY_CODE_A2)
        except Exception as e:
            print('ERROR:', e)
            logging.info(
                'Failed to parse mobile number for order otp for mobile no. {}'.format(
                    phone_number))

        order_msg_list = []

        for sms_config in sms_configs:
            if not sms_config.template_id:
                continue

            message = sms_config.msg_template
            template_dict = {
                'text' : message,
                'template_id' : sms_config.template_id,
                'template_type' : 'TXN'
            }

            fields = re.findall(r"\{([a-zA-Z_]+)\}", message)
            time_format = '%-I:%M %p'
            date_format = '%-d-%-m-%y'
            delivery_time = 'NA'
            delivery_date = 'NA'
            if order.expected_delivery_time:
                delivery_time = utc_to_ist(
                    order.expected_delivery_time).strftime(
                    time_format)
                delivery_date = utc_to_ist(
                    order.expected_delivery_time).strftime(
                    date_format)

            for field in fields:
                to_repalce = '{' + field + '}'

                replace_by = None
                if field == 'customer_name':
                    replace_by = order.customer.name
                elif field == 'reference_number':
                    replace_by = order.reference_number
                elif field == 'customer_reference_number':
                    replace_by = order.customer_reference_number
                elif field == 'order_number':
                    replace_by = order.number
                elif field == 'delivery_date':
                    replace_by = delivery_date
                elif field == 'delivery_time':
                    replace_by = delivery_time
                elif field == 'driver_name':
                    replace_by = order.driver.name
                elif field == 'phone_number':
                    replace_by = str(order.driver.user.phone_number)
                elif field == 'order_otp':
                    replace_by = order.otp or 'NA'
                elif field == 'tracking_link':
                    tracking_link = '%s/track-parcel/%s' % (
                        settings.HOST, order.number)
                    replace_by = tracking_link
                elif field == 'feedback_link':
                    replace_by = '%s/feedback/%s' % (settings.HOST, order.number)
                elif field == 'delivery_remarks':
                    replace_by = msg_data.get('remarks')
                if not replace_by:
                    replace_by = 'NA'
                message = message.replace(to_repalce, replace_by)

            template_json = json.dumps(template_dict)
            if not settings.ENABLE_SLACK_NOTIFICATIONS:
                SmsLog.objects.create(message=message,
                        template_id=template_dict['template_id'],
                        provider=settings.DEFAULT_SMS_PROVIDER,
                        to_number=str(phone_number),
                        customer_id=order.customer.id)
            send_sms.apply_async(args=[str(phone_number), message, template_json],
                            kwargs={"customer_id":order.customer.id})
            _dict = {
                'order': order,
                'trip': trip,
                'msg_event': status,
                'message': message
            }
            order_msg_list.append(OrderMessage(**_dict))

        if order_msg_list:
            OrderMessage.objects.bulk_create(order_msg_list)
            return error_msg, otp

    def send_tracking_link(self, user, order):
        """
        :param user:
        :param order:
        :return:
        """
        message = None
        if order.payment_status == PAYMENT_STATUS_PAID:
            template_dict = sms_templates.template_item_delivered
            message = template_dict['text'] % order.number
        else:
            tracking_link = '%s/track/%s' % (settings.HOST, order.number)
            template_dict = sms_templates.template_booking_completion
            message = template_dict['text'] % (
                order.number,
                order.total_payable,
                tracking_link
            )

        user.send_sms(message, template_dict)
