"""
Reorder the Trip Stops.

Makes current In-Progress Stop to New.
Makes the Stop sent from app from New to In-Progress.

Permissions added.Only Active Drivers can access this api
Input Data: trip_id, stop_id, Location_details

Response: {"stop_id": id of the stop which is made In-Progress}

Created By: Gaurav Verma
"""
# Django and System libraries
import logging
from django.db import transaction

# 3rd party libraries
from rest_framework import status

from blowhorn.apps.driver_app.v3.serializers.contract import TripWorkFlowSerializer
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.driver_app.v3.serializers.trip import TripSerializer, TripStopSerializer
from blowhorn.apps.driver_app.v3.serializers.order import OrderSerializer
from blowhorn.apps.driver_app.v3.services.trip.send_sms import SendSMS
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import (
    TRIP_COMPLETED, TRIP_NEW, TRIP_IN_PROGRESS,
    ORDER_DELIVERED, DELIVERY_ACK, OUT_FOR_DELIVERY,
    UNABLE_TO_DELIVER, ORDER_RETURNED,
    REORDERED_STOP_NEW
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.apps.driver_app.v3.helpers.trip import create_app_event


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Stop = get_model('trip', 'Stop')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')


class ReorderStops(object):
    """
    Make changes in the stops statuses keeping only one stop in
    In-Progress status
    """

    def _initialize_params(self, trip_id, stop_id, data):
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.driver = self.request.user.driver
        self.response_data = {"stop_id": int(stop_id)}
        # self.target_stop = Stop.objects.filter(pk=int(stop_id), status=TRIP_NEW).first()
        self.target_stop = Stop.objects.filter(pk=int(stop_id)).exclude(
            order__status__in=[ORDER_DELIVERED]).first()
        if not self.target_stop:
            self.error_message = 'Target Stop doesn\'t exist'
            return
        self.context = {'request': self.request}
        # self.reattempt = data.get('reattempt', 'false') == 'true'
        # TODO above code need to be used but reattempt is not sent from driver app for now
        self.reattempt = False

        is_target_stop_completed = self.target_stop.status == TRIP_COMPLETED
        is_target_order_delivered = self.target_stop.order.status in [ORDER_DELIVERED, DELIVERY_ACK]
        # if self.reattempt:
        if self.target_stop.order.status in [UNABLE_TO_DELIVER, ORDER_RETURNED]:
            self.reattempt = True
            if not is_target_stop_completed or is_target_order_delivered:
                self.error_message = 'Target Stop can\'t be Reordered'
                return
        elif not self.target_stop.status == TRIP_NEW:
            self.error_message = 'Target Stop can\'t be Reordered'
            return
        self.target_order = self.target_stop.order
        self.trip = Trip.objects.filter(pk=trip_id).first()
        if not self.trip or self.target_stop.trip != self.trip:
            self.error_message = 'Trip doesn\'t exist'
            return
        # update current stop in progress to New
        self.current_stop_qs = Stop.objects.filter(trip=self.trip, status=TRIP_IN_PROGRESS).exclude(pk=stop_id)
        self.target_stop_data = {}
        self.order_data = {}
        self.trip_data = {}
        if self.target_stop:
            order = self.target_stop.order
            if order:
                contract = order.customer_contract
                self.response_data.update({"order_workflow":  TripWorkFlowSerializer(contract.trip_workflow).data})

    def _reorder_stops(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():
            self.__make_updates()
            self.__save_updates()

    def __make_updates(self):
        self.__target_stop_updates()
        self.__current_stop_updates()
        self.__trip_updates()

    def __save_updates(self):
        logger.info('target_stop: %s' % self.target_stop_data)
        if self.target_stop and self.target_stop_data:
            self.target_stop_data['geopoint'] = self.geopoint

            self.target_stop = UpdateModelMixin().update(data=self.target_stop_data,
                                                         instance=self.target_stop,
                                                         serializer_class=TripStopSerializer)
            self.target_stop.save()

            if self.target_stop.status == TRIP_IN_PROGRESS and self.target_order:
                # CHECK: Now sms is sent from start trip
                # error_msg, otp = SendSMS().send_otp_for_stop(self.target_order)
                SendSMS().send_message_for_order_event(self.target_order,
                                                       self.target_order.status,
                                                       self.target_stop.status,
                                                       trip=self.trip)

        logger.info('trip_data: %s' % self.trip_data)
        if self.trip and self.trip_data:
            self.trip_data['geopoint'] = self.geopoint
            self.trip = UpdateModelMixin().update(data=self.trip_data, instance=self.trip,
                                                  serializer_class=TripSerializer)
            self.trip.save()
        logger.info('order_data: %s' % self.order_data)
        if self.target_order or self.order_data:
            if self.order_data.get('status'):
                self.context['geopoint'] = self.geopoint
            if self.order_data:
                self.target_order = UpdateModelMixin().update(data=self.order_data,
                                                              instance=self.target_order,
                                                              serializer_class=OrderSerializer,
                                                              context=self.context)
            self.target_order.save()

    def __current_stop_updates(self):
        if self.current_stop_qs.exists():
            create_app_event(
                REORDERED_STOP_NEW,
                self.geopoint,
                driver=self.driver,
                trip=self.trip,
                order=self.current_stop_qs[0].order,
                stop=self.current_stop_qs[0]
            )
            self.current_stop_qs.update(status=TRIP_NEW, start_time=None)

    def __target_stop_updates(self):
        if self.target_stop.status == TRIP_IN_PROGRESS:
            return
        self.target_stop_data['status'] = TRIP_IN_PROGRESS

    def __trip_updates(self):
        if self.reattempt:
            if self.target_order.status == UNABLE_TO_DELIVER:
                self.trip_data['returned'] = self.trip.returned - 1
            if self.target_order.status == ORDER_RETURNED:
                self.trip_data['rejected'] = self.trip.rejected - 1
            self.trip_data['attempted'] = self.trip.attempted - 1

            self.__order_updates()

    def __order_updates(self):
        if self.target_order.status == OUT_FOR_DELIVERY:
            return
        self.order_data['status'] = OUT_FOR_DELIVERY
        self.order_data['geopoint'] = self.geopoint
