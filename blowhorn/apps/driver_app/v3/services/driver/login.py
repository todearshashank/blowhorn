"""
Driver Authentication to be done here.

through 1. QR Code Scan or 2. mobile + password

-Deletes all user sessions and django sessions.
-Saves the new session for user.
-Updates Driver Device details and driver log info

Input Data: phone_number, qr_code data, password, device_id, fcm_id, device_details, location_details

Response: Returns User session token, Firebase Auth token and driver Info with 200 status

Created By: Gaurav Verma
"""
# Django and System libraries
import json
import logging
from django.conf import settings
from django.db.models import Q
from django.db import transaction

# 3rd party libraries
from rest_framework import status
from rest_framework.response import Response
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from config.settings.status_pipelines import LOG_IN, FCM_UPDATED
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.helper import CommonHelper
from blowhorn.apps.driver_app.utils import LegacyCipher
from blowhorn.common.notification import Notification
from blowhorn.driver.constants import LOGIN_DRIVER_ACTION
from blowhorn.users.constants import LOGIN_VALIDATION_MESSAGES
from blowhorn.driver.models import DriverConstants
from fcm_django.models import FCMDevice
from blowhorn.apps.mixins import UpdateModelMixin, CreateModelMixin
from blowhorn.apps.driver_app.v3.serializers.driver import (
    DeviceDetailSerializer,
    UserSessionSerializer,
    VendorSerializer,
)
from blowhorn.apps.driver_app.v3.helpers.trip import create_app_event
from blowhorn.apps.driver_app.v4.helpers.driver import verify_user


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

User = get_model('users', 'User')
Driver = get_model('driver', 'Driver')
UserSession = get_model('users', 'UserSession')


class LogIn(object):
    """Driver Authentication is done here."""

    def _is_valid_build_code(self, build_code):
        """
        Checks given build code with minimum build code required
        returns: boolean
        """
        return build_code and int(build_code) >= int(DriverConstants().get('MINIMUM_BUILD_CODE_REQUIRED', 132))

    def _initialize_attributes(self, data):
        self.error_message = ''
        self.login_response = None
        self.response_data = None
        self.status_code = status.HTTP_200_OK
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.phone_number = data.get("username")
        self.qr_code = data.get("qr_code", None)
        self.password = data.get("password")
        self.device_id = data.get("device_id")
        self.uuid = data.get("uuid")
        self.registration_id = data.get('fcm_id', None)
        self.device_details = json.loads(data.get('device_details'))
        self.device_details['app_name'] = 'Unified App'
        self.device_type = 'android'
        self.is_vendor = True if data.get('is_vendor') == 'true' else False
        self.otp = data.get('otp', None)

        if not ((self.phone_number and self.password) or self.qr_code or (self.phone_number and self.otp)):
            self.error_message = 'Invalid data sent'

    def _login(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return ''

        if self.qr_code:
            self.login_response = self._login_using_qr_code()

        elif self.otp:
            self.login_response = self._login_with_otp()

        elif self.phone_number:
            self.login_response = self._login_with_phone_number()

        if self.login_response and self.login_response.get('is_success', False):
            if self.is_vendor:
                response = self.get_vendor_response()
            else:
                response = self._get_login_response()
            self.status_code = response.get('status')
            self.response_data = response.get('message')

        else:
            self.response_data = self.login_response.get('message') if self.login_response \
                else LOGIN_VALIDATION_MESSAGES['invalid_credentials']
            self.status_code = self.login_response.get('status') if self.login_response \
                else status.HTTP_401_UNAUTHORIZED

    def _is_active_driver(self, user):
        if hasattr(user,'driver') and user.driver.status != StatusPipeline.ACTIVE:
            return {
                'is_success': False,
                'status': status.HTTP_400_BAD_REQUEST,
                'message': 'Inactive Driver'
            }
        else:
            return {'is_success': True}

    def _active_user(self, user):
        '''
            This method check whether the driver/vendor is not inactive or
            blacklisted. If yes, returns True for the login else
            False.
        '''
        status_not_allowed = [StatusPipeline.INACTIVE,StatusPipeline.BLACKLISTED]
        if hasattr(user,'driver') and not self.is_vendor and \
                            user.driver.status in status_not_allowed:
            return {
                'is_success': False,
                'status': status.HTTP_400_BAD_REQUEST,
                'message': 'Driver is inactive'
            }

        elif hasattr(user,'vendor') and self.is_vendor and \
                            user.vendor.status in status_not_allowed:
            return {
                'is_success': False,
                'status': status.HTTP_400_BAD_REQUEST,
                'message': 'Vendor is inactive'
            }

        else:
            return {'is_success': True}

    def _login_using_qr_code(self):

        try:
            information = LegacyCipher.decode(self.qr_code)
            information = json.loads(information)
            driver_mobile = information.get('id')

            if driver_mobile:
                email = '%s@%s' % (driver_mobile,
                                   settings.DEFAULT_DOMAINS.get('driver') if not self.is_vendor else
                                   settings.DEFAULT_DOMAINS.get('vendor'))
                user = User.objects.filter(email=email).first()
                response = self._active_user(user)
                if not response.get('is_success', False):
                    return response
                if user:
                    return User.objects.do_login_new(
                        request=self.request, user=user, device_id=self.device_id)
        except BaseException:
            pass

        return {
            'is_success': False,
            'status': status.HTTP_401_UNAUTHORIZED,
            'message': LOGIN_VALIDATION_MESSAGES['invalid_credentials']
        }

    def _login_with_phone_number(self):

        email = '%s@%s' % (self.phone_number,
                           settings.DEFAULT_DOMAINS.get('driver') if not self.is_vendor else
                           settings.DEFAULT_DOMAINS.get('vendor'))
        user = User.objects.filter(email=email).prefetch_related(
            'driver__user', 'vendor__user').first()
        is_valid_user, response_message = verify_user(user, self.is_vendor)
        if not is_valid_user:
            return {
                'is_success': False,
                'status': status.HTTP_400_BAD_REQUEST,
                'message': response_message
            }

        response = self._active_user(user)
        if not response.get('is_success'):
            return response
        return User.objects.do_login_new(request=self.request,
                                         email=user.email,
                                         password=self.password,
                                         device_id=self.device_id)

    def _login_with_otp(self):

        email = '%s@%s' % (self.phone_number,
                           settings.DEFAULT_DOMAINS.get('driver') if not self.is_vendor else
                           settings.DEFAULT_DOMAINS.get('vendor'))
        user = User.objects.filter(email=email).prefetch_related(
            'driver__user', 'vendor__user').first()
        response = self._active_user(user)

        if not response.get('is_success'):
            return response

        return User.objects.do_login_new(request=self.request,
                                         user=user,
                                         otp=self.otp,
                                         mobile=self.phone_number,
                                         device_id=self.device_id)

    def get_vendor_response(self):
        message = self.login_response.get('message')
        user = message.get('user')
        token = message.get('token')
        logger.info(
            'LogIn Successful for Vendor with phone_number:- "%s"',
            self.phone_number)

        if not hasattr(user, 'vendor'):
            response_status = status.HTTP_400_BAD_REQUEST
            response = 'Invalid user'
        else:
            response_status = status.HTTP_200_OK
            vendor = user.vendor
            context = {"token": token}

            response = VendorSerializer(vendor, context=context).data

        return {
            'status': response_status,
            'message': response
        }

    def _get_login_response(self):
        with transaction.atomic():
            message = self.login_response.get('message')
            user = message.get('user')
            token = message.get('token')

            current_session_key = self.request.session.session_key
            session_keys = list(UserSession.objects.get_session_keys_using_user(
                user, token))
            UserSession.objects.remove_sessions_using_session_keys(session_keys)
            UserSession.objects.remove_django_sessions(session_keys)
            self._save_user_session(user.pk, current_session_key, token)

            firebase_token = CommonHelper.generate_firebase_token(
                settings.FIREBASE_KEYS['driver'])

            driver = user.driver
            if self.registration_id:
                self.__store_fcm(user)

            context = {"token": token, "firebase_auth_token": firebase_token}

            create_app_event(LOG_IN, self.geopoint, driver=driver,
                             device_id=self.device_id, uuid=self.uuid,
                             imei_number=self.device_details.get('imei_number'))
            self._save_device_details(
                driver, driver.device_details, self.device_details)
            driver.update_driver_log_info(LOGIN_DRIVER_ACTION)

            response = self.serializer_class(driver, context=context).data

            logger.info(
                'LogIn Successful for Driver with phone_number:- "%s"',
                self.phone_number)
            return {
                'status': status.HTTP_200_OK,
                'message': response
            }

    def __store_fcm(self, user):
        fcm_devices = FCMDevice.objects.filter(
            Q(user=user, active=True) & ~Q(
                device_id=self.device_id)).delete()
        logging.info('%s other devices deleted. Driver %s' %
                     (fcm_devices, user))
        # Store Driver FCM Id and device ID
        Notification.add_fcm_key_for_user(
            user,
            self.registration_id,
            self.device_id,
            self.device_type
        )

        create_app_event(FCM_UPDATED, self.geopoint, driver=user.driver,
                         device_id=self.device_id, uuid=self.uuid,
                         imei_number=self.device_details.get('imei_number'))

    def _save_device_details(self, driver, device_details, device_details_data):
        if device_details:
            device_details = UpdateModelMixin().update(
                data=device_details_data, instance=device_details,
                serializer_class=DeviceDetailSerializer)
            device_details.save()
        else:
            device_details = CreateModelMixin().create(
                data=device_details_data, serializer_class=DeviceDetailSerializer)
            driver.device_details = device_details
            driver.save()

    def _save_user_session(self, user_id, current_session_key, token):
        session_data = {
            'user': user_id,
            'session_key': current_session_key,
            'token': token
        }
        session_serializer = UserSessionSerializer(data=session_data)
        if session_serializer and session_serializer.is_valid():
            session_serializer.save()
        else:
            logger.info('Invalid data for UserSession: %s' %
                        session_serializer.errors)
