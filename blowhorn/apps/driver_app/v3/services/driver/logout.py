"""
Driver Logout changes to be done here.

Logging out the driver.
Remove Fcm Key for user
Delete User's Session record

Input Data: user, location_details

On Success: Returns 'Driver Logged Out successfully' with 200 status

Created By: Gaurav Verma
"""
# Django and System libraries
import logging
from django.db import transaction
from django.contrib.auth import logout
# 3rd party libraries
from blowhorn.oscar.core.loading import get_model
from rest_framework import status
# blowhorn imports
from config.settings.status_pipelines import LOG_OUT
from blowhorn.common.helper import CommonHelper
from blowhorn.driver.constants import LOGOUT_DRIVER_ACTION
from blowhorn.apps.driver_app.v3.helpers.trip import create_app_event
from blowhorn.common.notification import Notification


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


UserSession = get_model('users', 'UserSession')


class LogOut(object):
    """Driver Logout changes to be done here."""

    def _initialize_attributes(self, data):
        self.error_message = ''
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.driver = None
        self.is_vendor = True if data.get('is_vendor','') == 'true' else False
        if not self.is_vendor:
            self.driver = self.request.user.driver
        else:
            self.vendor = self.request.user.vendor
        self.device_id = data.get("device_id")
        self.uuid = data.get("uuid")
        self.imei_number = data.get("imei_number")
        self.user = self.request.user
        self.response_data = 'Driver Logged Out successfully'
        self.status_code = status.HTTP_200_OK

        self.token = self.request.META.get('HTTP_TOKEN')
        if not self.token:
            self.error_message = 'Token not sent !!!'

    def _do_logout(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():
            logout(self.request)
            Notification.remove_fcm_key_for_user(
                user=self.user,
                device_type='android'
            )
            UserSession.objects.remove_session_using_token(self.token)
            if not self.is_vendor:
                create_app_event(LOG_OUT, self.geopoint, driver=self.driver,
                                 device_id=self.device_id, uuid=self.uuid,
                                 imei_number=self.imei_number)
                self.driver.update_driver_log_info(LOGOUT_DRIVER_ACTION)
                logger.info('App Event "%s" created for driver:- %s', LOG_OUT, self.driver)
