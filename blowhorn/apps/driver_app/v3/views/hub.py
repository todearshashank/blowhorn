from rest_framework import generics, exceptions, status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from blowhorn.common.helper import CommonHelper
from blowhorn.order.models import OrderLine
from blowhorn.oscar.core.loading import get_model
from django.conf import settings
from django.db.models import Q, Count, F
from django.utils import timezone
from datetime import timedelta
from config.settings import status_pipelines as StatusPipeline
from django.db import transaction

from blowhorn.trip.constants import HUB_DELIVERY
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.driver.permissions import IsActiveDriver
from blowhorn.users.permission import HasValidSession
from blowhorn.apps.driver_app.v3.constants import ORDER_STATUS_BY_STOPS
from blowhorn.apps.driver_app.v3.serializers.address import HubSerializer
from blowhorn.apps.driver_app.v3.services.order.scan_orders import ScanOrderMixin
from blowhorn.apps.driver_app.v3.serializers.trip import TripSyncSerializer
from blowhorn.apps.driver_app.v3.helpers.trip import create_trip_for_hub_stops, \
    create_stop

Hub = get_model('address', 'Hub')
City = get_model('address', 'City')
Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
StopOrders = get_model('trip', 'StopOrders')


class HubView(views.APIView):

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)

    def get(self, request):

        params = self.request.GET

        blowhorn_hub = params.get('blowhornHub', "false")
        city_id = params.get('city_id')
        try:
            city = City.objects.get(pk=city_id)
            filter_param = Q(city=city)
        except:
            _out = {
                'message': 'Invalid city id'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        name = params.get('name')
        if name:
            filter_param &= Q(name__icontains=name)

        customer_id = params.get('customer_id')
        if customer_id:
            filter_param &= Q(customer_id=customer_id)

        hub_name = params.get('qr')
        if hub_name:
            filter_param &= Q(name=hub_name)

        if blowhorn_hub == "true":
            filter_param &= Q(customer__isnull=True)
        else:
            filter_param &= Q(customer__isnull=False)

        hubs_qs = Hub.objects.filter(filter_param).select_related('address')
        _next = params.get('next')
        try:
            hub_serializer = common_serializer.PaginatedSerializer(
                queryset=hubs_qs,
                num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                page=_next,
                serializer_method=HubSerializer)
            response_data = hub_serializer.data
            return Response(status=status.HTTP_200_OK, data=response_data)
        except:
            _out = {
                'message': 'Failed to fetch Hub data'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)


class BulkPickupShipment(views.APIView):
    permission_classes = (IsAuthenticated, IsActiveDriver)

    def get(self, request):

        _params = request.query_params
        hub_id = _params.get('hub_id')

        try:
            hub = Hub.objects.get(pk=hub_id)
        except:
            response = {'message': 'Invalid hub id'}
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        driver = request.user.driver if hasattr(request.user, 'driver') else None
        if not driver:
            response = {'message': 'Driver not found'}
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        order_status = StatusPipeline.ORDER_NEW

        pickup_date_start = timezone.now() - timedelta(days=21)

        orders = Order.objects.filter(pickup_hub=hub, status=order_status,
                                      order_type=settings.SHIPMENT_ORDER_TYPE,
                                      pickup_datetime__gte=pickup_date_start).annotate(
            sku_count=Count('order_lines')).values(
            'customer_id', 'reference_number', 'number', 'id', 'sku_count').annotate(
            ignore_order_lines=F('customer_contract__trip_workflow__ignore_order_lines'))

        if not orders:
            response = {'message': 'No Orders Found'}
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        response = {
            'order_list': orders
        }

        return Response(status=status.HTTP_200_OK, data=response)

        # _next = _params.get('next')
        # try:
        #     order_serializer = common_serializer.PaginatedSerializer(
        #         queryset=orders,
        #         num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
        #         page=_next,
        #         serializer_method=BulkPickupOrderSerializer)
        #     response_data = order_serializer.data
        #     return Response(status=status.HTTP_200_OK, data=response_data)
        # except:
        #     _out = {'message':  'Failed to fetch orders'}
        #     return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

    def post(self, request):
        order_data = request.data
        hub_id = order_data.get('hub_id')
        dropoff_hub_id = order_data.get('dropoff_hub_id')
        order_ids = order_data.get('order_ids')
        valid_hub = Hub.objects.filter(id=hub_id).first() if hub_id else None
        dropoff_hub = Hub.objects.filter(id=dropoff_hub_id).first() if dropoff_hub_id else None
        hub_name = valid_hub.name if valid_hub else None

        if not hub_id or not dropoff_hub_id:
            _out = {
                'message': 'Hub id and Drop off hub id is mandatory'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if hub_id and not valid_hub:
            _out = {
                'message': 'Hub id is invalid'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if dropoff_hub_id and not dropoff_hub:
            _out = {
                'message': 'Drop off Hub id is invalid'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not order_ids:
            _out = {
                'message': 'No order ids received'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            order_pks = [int(i) for i in order_ids]
        except:
            _out = {
                'message': 'Invalid order ids'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        wms_orders = Order.objects.filter(id__in=order_pks, customer_contract__is_wms_contract=True).values_list(
            'number', flat=True)
        if wms_orders.exists():
            _out = {
                'message': "Use WMS app to assign WMS orders: %s" % list(wms_orders)
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        user = request.user

        driver = user.driver if hasattr(user, 'driver') else None
        if not driver:
            _out = {
                'message': 'Driver not found for the user'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)
        location = order_data.get('location_details', None)
        geopoint = CommonHelper.get_geopoint_from_location_json(location)
        address = CommonHelper.get_address_from_location_json(location)
        trip_id = order_data.get('trip_id')
        trip = None
        if trip_id:
            try:
                trip = Trip.objects.get(pk=trip_id)
            except:
                _out = {
                    'message': 'Invalid city id'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        query = Q(status=StatusPipeline.ORDER_NEW)
        query &= Q(id__in=order_pks, order_type=settings.SHIPMENT_ORDER_TYPE)
        orders = Order.objects.filter(query)
        if not orders.exists():
            _out = {
                'message': 'orders does not exist'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        order_event_list = []
        hub_movement_order_event_list = []
        order_numbers = []
        stop_orders_list = []
        stop_type = HUB_DELIVERY
        if trip_id:
            new_order_status = ORDER_STATUS_BY_STOPS.get(stop_type)
        else:
            new_order_status = StatusPipeline.DRIVER_ACCEPTED

        waypoint = orders[0].waypoint_set.all().first()
        waypoint_id = waypoint.id if waypoint else None

        stop_hub = dropoff_hub

        if stop_hub.shipping_address:
            stop_address_id = stop_hub.shipping_address.id
        else:
            stop_address_id = ScanOrderMixin().get_shipping_address_from_hub_address(stop_hub)
            stop_hub.shipping_address_id = stop_address_id
            stop_hub.save()

        try:
            with transaction.atomic():
                trip = create_trip_for_hub_stops(trip, driver)
                if trip.status in [StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]:
                    new_order_status = StatusPipeline.DRIVER_ACCEPTED

                trip_data = {
                    'customer_contract_id': orders[0].customer_contract.id,
                    'trip_workflow_id': orders[0].customer_contract.trip_workflow.id if orders[
                        0].customer_contract.trip_workflow else None
                }

                Trip.objects.filter(id=trip.id).update(**trip_data)
                trip = Trip.objects.filter(id=trip.id).first()

                if not trip:
                    _out = {
                        'message': 'Failed to create trip/No valid trip found'
                    }
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

                stop = Stop.objects.filter(trip=trip, stop_type=stop_type, stop_address_id=stop_address_id
                                           ).exclude(status=StatusPipeline.TRIP_COMPLETED).first()

                if not stop:
                    sequence = trip.stops.all().count() + 1
                    stop = create_stop(trip=trip, order=orders[0].id, sequence=sequence,
                                       waypoint=waypoint_id, stop_type=stop_type,
                                       stop_address=stop_address_id)
                for order in orders:
                    order_numbers.append(order.number)
                    order_event_list.append(Event(status=StatusPipeline.ORDER_PICKED,
                                                  order=order,
                                                  driver=driver,
                                                  remarks=hub_name,
                                                  current_location=geopoint,
                                                  current_address=address))
                    hub_movement_order_event_list.append(Event(status=new_order_status,
                                                               order=order,
                                                               driver=driver,
                                                               remarks=stop_hub.name,
                                                               current_location=geopoint,
                                                               current_address=address))
                    stop_orders_list.append(StopOrders(trip=trip,
                                                       hub=stop_hub,
                                                       order=order,
                                                       stop=stop
                                                       ))
                    # hub_stops_dict[order.hub_id].append(order.id)

                Event.objects.bulk_create(order_event_list)
                Event.objects.bulk_create(hub_movement_order_event_list)
                orders.update(status=new_order_status,
                              latest_hub=valid_hub,
                              driver=driver,
                              updated_time=timezone.now())
                StopOrders.objects.bulk_create(stop_orders_list)

                # location = order_data.get('location_details', None)
                # geopoint = CommonHelper.get_geopoint_from_location_json(location)

                # trip = ScanOrderMixin(
                #     context={'request': request},
                #     trip_id=trip.id if trip else None,
                #     geopoint=geopoint
                # ).create_trip_stops_for_shipment_orders(
                #     driver=driver,
                #     order_numbers=order_numbers,
                #     **{'is_kiosk_order': False}
                # )
        except:
            _out = {
                'message': 'orders update failed'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        response_data = TripSyncSerializer(instance=trip).data
        return Response(status=status.HTTP_200_OK, data=response_data)

