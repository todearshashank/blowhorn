"""
Contains All the View Classes that are solely related to drivers.

Login, Logout, Duty Status, Driver Activity, Earnings, Referrals etc...

Created by: Gaurav Verma
"""
# Django and System libraries.
import json
import requests
import logging
import datetime
from datetime import timedelta
import traceback
import random
import pytz
import re
import datetime
import math
from dateutil import parser
from django.db.models import Prefetch

# 3rd party libraries
from rest_framework import status, generics, views
from rest_framework.decorators import permission_classes
from rest_framework.exceptions import ValidationError
from django.shortcuts import render_to_response
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from django.db import transaction
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from blowhorn.oscar.core.loading import get_model
from rest_framework.permissions import AllowAny
from phonenumber_field.phonenumber import PhoneNumber

from blowhorn.order.models import CallMaskingLog
from blowhorn.users.models import User
from blowhorn.users.utils import get_partner_user_from_phone_number
from blowhorn.vehicle.utils import add_vendor
from django.utils import timezone
from django.conf import settings
from django.contrib.auth import logout

# blowhorn imports
from blowhorn.apps.driver_app.v3.serializers.driver import (
    DriverLoginSerializer, DriverPaymentSerializer,
    DriverLedgerSerializer, DutyStatusSerializer,
    DriverReferralSerializer, VendorSerializer,
    UserSignUpSerializer
)

from blowhorn.document.models import DocumentType, DocumentEvent
from blowhorn.document.constants import (DRIVER_DOCUMENT, VEHICLE_DOCUMENT, VERIFICATION_PENDING,
                                         ACTIVE, REJECT, VENDOR_DOCUMENT, DOCUMENT_RC, DOCUMENT_PHOTO,
                                         DOCUMENT_COVID_PASS)
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.common.communication import Communication
from blowhorn.common.helper import CommonHelper
from blowhorn.driver import constants as driver_constants
from blowhorn.driver.permissions import IsActiveDriver
from blowhorn.common import serializers as common_serializer
from blowhorn.apps.driver_app.v3.services.driver.login import LogIn
from blowhorn.apps.driver_app.v3.services.driver.fcm import FcmUpdate
from blowhorn.apps.driver_app.v3.services.driver.logout import LogOut
from blowhorn.apps.driver_app.v3.services.driver.driver_referral import DriverReferral
from blowhorn.apps.driver_app.v3.serializers.driver import DriverSerializer

from blowhorn.apps.driver_app.utils import _get_api_reponse
from blowhorn.apps.driver_app.v3.helpers.driver import create_error_object, check_driver_criteria, \
    check_interval_criteria, send_field_name_values, send_notification_content
from blowhorn.apps.mixins import CreateModelMixin
from blowhorn.freshchat.whatsapp import send_whatsapp_notification
from config.settings.status_pipelines import TRIP_COMPLETED
from config.settings import status_pipelines as StatusPipeline

from blowhorn.apps.driver_app.v3.services.onboarding.document_upload import DocumentProcessor
from blowhorn.integration_apps.invoid.driver_mixin import DriverAddUpdateMixin
# TODO need to be removed
from rest_framework.response import Response
from blowhorn.driver.serializers import DriverActivitySerializer
from blowhorn.users.permission import HasValidSession
from blowhorn.common import sms_templates
from blowhorn.driver.constants import TOS_ACCEPTED, TOS_REJECTED, IN_APP, PUSH
from blowhorn.driver.serializers import DriverDocumentSerializer, DriverDocumentPageSerializer
from blowhorn.vehicle.serializers import (VehicleDocumentSerializer, VendorDocumentSerializer,
                                          VehicleDocumentPageSerializer)
from blowhorn.driver.models import BankAccount, DriverDocument, APPROVED, DriverTds, UNAPPROVED, DriverManager, \
    DriverAcknowledgement, DriverNotificationCriteria, DriverNotificationResponse, \
    NotificationCampaign, NotificationReadStatus, DriverResponse
from blowhorn.vehicle.models import VehicleDocument, VendorDocument, Vehicle, VendorPreferredLocation
from blowhorn.address.models import State
from blowhorn.address import utils as address_utils
from blowhorn.contract.models import Contract
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE
from blowhorn.utils.functions import minutes_to_dhms
from blowhorn.common import sms_templates
from blowhorn.apps.driver_app.constant import VENDOR_MANDATORY_DOCUMENTS
from blowhorn.utils.mail import Email

DriverActivity = get_model('driver', 'DriverActivity')
DriverGPSInformation = get_model('driver', 'DriverGPSInformation')
City = get_model('address', 'City')
DriverPayment = get_model('driver', 'DriverPayment')
DriverCashPaid = get_model('driver', 'DriverCashPaid')
DriverLedger = get_model('driver', 'DriverLedger')
PaymentTrip = get_model('driver', 'PaymentTrip')
Driver = get_model('driver', 'Driver')
Vendor = get_model('vehicle', 'Vendor')
DriverReferrals = get_model('driver', 'DriverReferrals')
DriverLogInfo = get_model('driver', 'DriverLogInfo')
Order = get_model('order', 'Order')
DeliveryWorkerActivity = get_model('driver', 'DeliveryWorkerActivity')
Hub = get_model('address', 'Hub')
UserSession = get_model('users', 'UserSession')
Trip = get_model('trip', 'Trip')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def get_login_response(user, token, hub):

    response = {
        'name': user.name,
        'mobile': str(user.phone_number),
        'email': user.email,
        'mAuthToken': token,
        'hub': {"name": hub.name, "id": hub.id}

    }
    return response


@permission_classes([])
class Login(generics.CreateAPIView, LogIn):
    """
    Driver Authentication to be done here.

    through 1. QR Code Scan or 2. mobile + password

    Deletes all user sessions and django sessions.

    Saves the new session for user.

    POST(phone_number, qr_code data, password, device_id, fcm_id, device_details, location_details)

    Updates Driver Device details and driver log info
    and the response with the user token to be provided.

    On Success: Returns User session token, Firebase Auth token and driver Info with 200 status
    On Failure: Returns 500 with the error.

    """

    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)
    serializer_class = DriverLoginSerializer

    def post(self, request):
        """POST method for driver login changes."""
        data = request.data.dict()
        if not self._is_valid_build_code(data.get('app_code', None)):
            return Response(status=400, data=_('Update the app from playstore..!'))

        self._initialize_attributes(data)
        try:
            self._login()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Logout(generics.CreateAPIView, LogOut):
    """
    Driver Logout changes to be done here.

    Logging out the driver.
    Remove Fcm Key for user
    Delete User's Session record

    POST(user, location_details)

    On Success: Returns 'Driver Logged Out successfully' with 200 status
    On Failure: Returns 500 with the error.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """POST method for driver logout changes."""
        data = request.data.dict()
        self._initialize_attributes(data)
        try:
            if data.get('online') == 'true':
                request.data._mutable = True
                request.data['online'] = 'false'
                DutyStatus().post(request)
            self._do_logout()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint, )

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)

class SendOTP(generics.CreateAPIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        phone_number = data.get('phone_number', None)
        is_vendor = True if data.get('is_vendor') == 'true' else False

        email = '%s@%s' % (phone_number, settings.DEFAULT_DOMAINS.get(
            'driver') if not is_vendor else settings.DEFAULT_DOMAINS.get('vendor'))
        user = User.objects.filter(email=email).first()
        response_data = {}
        if not user:
            response_data['message'] = _('Mobile number not registered')
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

        partner = None
        if is_vendor and hasattr(user, 'vendor'):
            partner = user.vendor

        elif (not is_vendor and hasattr(user, 'driver')):
            partner = user.driver

        if not partner:
            response_data['message'] = _('Mobile number not registered')
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

        if partner.status in [StatusPipeline.INACTIVE, StatusPipeline.BLACKLISTED]:
            response_data['message'] = _('You are not an active driver, contact support to login')
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

        stored_otp, created = User.objects.retrieve_otp(user)
        if stored_otp:
            if settings.ENABLE_SLACK_NOTIFICATIONS:
                template_dict = sms_templates.password_reset_otp
                Communication.send_sms(sms_to=user.phone_number.national_number,
                                       message=template_dict['text'] % stored_otp,
                                       priority='high', template_dict=template_dict)
            response_data['message'] = _('OTP sent successfully')
            return Response(status=status.HTTP_200_OK, data=response_data)

        response_data['message'] = _('No user found')
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)


class ResetPassword(generics.CreateAPIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        phone_number = data.get('phone_number', None)
        otp = data.get('otp', None)
        password = data.get('password', None)
        is_vendor = True if data.get('is_vendor') == 'true' else False

        email = '%s@%s' % (phone_number, settings.DEFAULT_DOMAINS.get(
            'driver') if not is_vendor else settings.DEFAULT_DOMAINS.get('vendor'))
        user = User.objects.filter(email=email).first()
        response_data = {}

        stored_otp, created = User.objects.retrieve_otp(user)
        if created:
            response_data['message'] = _('Password reset failed')
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

        if str(otp) != str(stored_otp) and password:
            response_data['message'] = _('Please enter a valid OTP')
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

        user.set_password(password)
        user.save()
        response_data['message'] = _('Password reset successfully')
        return Response(status=status.HTTP_200_OK, data=response_data)


class Fcm(generics.CreateAPIView, FcmUpdate):
    """
    Updates existing FCM Device details for a user.

    Delete existing FCM Device details and create new one

    POST(fcm_id, device_id, user, location_details)

    On Success: Returns 'Successfully Updated' with 200 status
    On Failure: Returns 500 with the error.
    """

    permission_classes = (IsActiveDriver, IsAuthenticated, HasValidSession)

    def post(self, request):
        """POST method for Updating existing FCM Device for a user."""
        data = request.data
        if not isinstance(data, dict):
            data = data.dict()

        self._initialize_attributes(data)
        try:
            self._update_fcm()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint, )

        return _get_api_reponse(self.status_code, self.error_message,
                                self.response_data, content_type='charset utf-8; text/plain')


class Tos(views.APIView):
    permission_classes = (IsActiveDriver, IsAuthenticated, HasValidSession)

    def get_oldest_unaccepted_tds_year(self, query, active_from):

        from blowhorn.driver.util import get_financial_year

        current_financial_year = get_financial_year(timezone.now())
        tds_query = query & Q(year=current_financial_year, is_tds_accepted=True)
        is_tds_accepted = DriverTds.objects.filter(tds_query).first()
        financial_year = '' if is_tds_accepted else current_financial_year
        return financial_year

    def get(self, request):

        driver, vendor = None, None
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        if driver:
            query = Q(driver=driver) & Q(driver__own_vehicle=True)
            active_from = driver.active_from
        else:
            vendor = request.user.vendor
            query = Q(vendor=vendor)
            active_from = None

        if (driver and driver.tos_accepted) or (vendor and vendor.tos_accepted):
            tos_status = TOS_ACCEPTED
        else:
            tos_status = TOS_REJECTED
        financial_year = self.get_oldest_unaccepted_tds_year(query, active_from)

        if not ((driver and driver.pan and
         driver.own_vehicle) or (vendor and vendor.pan)):
            financial_year = ''
        data = {
            'driver_tos': tos_status,
            'is_tds_accepted': False if financial_year else True,
            'unaccepted_tds_year': financial_year,
            'tos_link': '/drivertermsofservice'
        }

        return Response(status=status.HTTP_200_OK, data=data)

    def post(self, request):
        """
        Driver TOS Accept/Reject APi
        :param request:
        :return:
        """
        data = request.data.dict()
        driver_tos = data.get('driver_tos')
        if driver_tos == TOS_ACCEPTED:
            tos_accepted = True
        else:
            tos_accepted = False

        driver = request.user.driver if hasattr(request.user, 'driver') else None
        if driver:
            Driver.objects.filter(id=driver.id).update(tos_accepted=tos_accepted, tos_response_date=timezone.now())
        else:
            vendor = request.user.vendor
            Vendor.objects.filter(id=vendor.id).update(tos_accepted=tos_accepted, tos_response_date=timezone.now())

        return Response(status=status.HTTP_200_OK)


class DriverPushNotificationView(views.APIView):
    permission_classes = (IsActiveDriver, IsAuthenticated, HasValidSession)

    def get(self, request):

        driver = request.user.driver if hasattr(request.user, 'driver') else None

        if driver:
            data = request.data
            notification_id = data.get('notification_id')
            notification_list = []
            message = {
                'message': 'Notification not found'
            }
            driver_language = request.META.get('HTTP_ACCEPT_LANGUAGE', None)
            driver.last_used_locale = driver_language

            start_date = timezone.now() - timedelta(days=30)
            _param = Q(driver=driver, created_date__gte=start_date)
            if notification_id:
                _param &= Q(notification_campaign_id=notification_id)
            driver_notification = NotificationReadStatus.objects.filter(_param).select_related('notification_campaign')
            if notification_id and not driver_notification.exists():
                return Response(status=status.HTTP_404_NOT_FOUND, data=message)

            for notification_status in driver_notification:
                if notification_status.notification_campaign.notification_type == PUSH:
                    notification_content = notification_status.notification_campaign.content
                    if notification_content:
                        content = send_notification_content(driver_language, notification_content)

                        notification_list.append({
                            "id": notification_status.notification_campaign.id,
                            "title": content[0],
                            "description": content[2],
                            "is_read": notification_status.is_read,
                            "sent_at": notification_status.created_date

                        })

            return Response(status=status.HTTP_200_OK, data=notification_list)


class DriverStatusView(views.APIView):
    permission_classes = (IsActiveDriver, IsAuthenticated, HasValidSession)

    def put(self, request, notification_id):
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        message = {
            'message': 'Notification not found'
        }
        if not notification_id:
            return Response(status=status.HTTP_404_NOT_FOUND, data=message)
        if driver:
            read_status = NotificationReadStatus.objects.filter(notification_campaign_id=notification_id, driver=driver
                                                                )
            if not read_status.exists() or not read_status[0].notification_campaign.notification_type == PUSH:
                return Response(status=status.HTTP_404_NOT_FOUND, data=message)

            marked = read_status.update(is_read=True)

            if marked == 1:
                read_message = {
                    'message': 'Notification marked as read'
                }
                return Response(status=status.HTTP_200_OK, data=read_message)


class DriverEvents(views.APIView):
    permission_classes = (IsActiveDriver, IsAuthenticated, HasValidSession)

    def get(self, request):

        driver = request.user.driver if hasattr(request.user, 'driver') else None
        if driver:
            driver_language = request.META.get('HTTP_ACCEPT_LANGUAGE', None)
            driver.last_used_locale = driver_language

            notification_list = []
            fields_list = []

            active_campaigns = NotificationCampaign.objects.filter(notification_type=IN_APP, is_active=True).\
                prefetch_related('notification_response')
            save_response = False
            for notification in active_campaigns:

                event_status = False
                last_response = DriverNotificationResponse.objects.filter(notification_campaign_id=notification.id,
                                                                          driver=driver).last()
                if last_response:
                    last_response_time = last_response.last_response_time
                    interval = notification.notification_interval
                    event_status = save_response = check_interval_criteria(interval, last_response_time)

                if notification and notification.notification_type == IN_APP and (notification.is_active is True)\
                   and (notification.start_time < timezone.now()) and timezone.now() < notification.end_time and \
                        not save_response:

                    driver_criteria = DriverNotificationCriteria.objects.filter(
                        notification_campaign_id=notification.id).first()

                    if driver_criteria:
                        event_status = check_driver_criteria(driver, driver_criteria)

                    if not event_status:
                        fields = notification.notification_response.all()
                        if fields.exists():
                            fields_list = []
                            for field in fields:
                                form_language = field.notification_form
                                name, field_values = send_field_name_values(driver_language, form_language)
                                fields_list.append({
                                      "id": field.id,
                                      "name": name,
                                      "field_type": field.field_type,
                                      "field_values": field_values
                                })
                        if notification.content:
                            title, short_description, description, image = send_notification_content(driver_language,
                                                                                                 notification.content)
                            notification_list.append({
                                "id": notification.id,
                                "title": title,
                                "short_description": short_description,
                                "description": description,
                                "image_url": image.url if image else "",
                                "url": notification.url,
                                "start_time": notification.start_time,
                                "end_time": notification.end_time,
                                "mandatory": notification.mandatory,
                                "fields": fields_list
                            })
                            fields_list = []

            return Response(status=status.HTTP_200_OK, data=notification_list)

    def post(self, request):
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        data = request.data

        message = {
            'message': 'Notification response saved'
        }
        notification_id = data.get('form_id')
        notification = NotificationCampaign.objects.filter(id=notification_id, is_active=True).first()

        if driver and notification:

            fields = data.get('fields')
            last_response = DriverNotificationResponse.objects.filter(
                notification_campaign_id=notification_id, driver=driver).last()

            save_response = False
            confirm_response = True
            if last_response and notification:
                last_response_time = last_response.last_response_time
                interval = notification.notification_interval
                save_response = check_interval_criteria(interval, last_response_time)
                confirm_response = True if last_response_time + timedelta(minutes=10) < timezone.now() else False
            if not save_response and confirm_response:
                notification_response = DriverNotificationResponse.objects.create(driver=driver,
                                                                                  notification_campaign_id=
                                                                                  notification_id)
                driver_responses = []
                if fields:
                    for field in fields:
                        field_id = field.get('field_id')
                        field_value = field.get('value')
                        driver_responses.append(DriverResponse(
                            driver_notification_response_id=
                            notification_response.id, notification_id=
                            notification_id, form_field_id=field_id,
                            field_response=field_value
                        ))
                if driver_responses:
                    DriverResponse.objects.bulk_create(driver_responses)
        return Response(status=status.HTTP_200_OK, data=message)


class Activity(generics.CreateAPIView, generics.UpdateAPIView):
    """

    API endpoint to log the driver activity.

    i.e. Current Location, Accuracy meters,

    Vehicle Speed, Battery Percentage etc...

    Permissions added.Only Active Drivers can access this api

    POST(location_details)

    On Success: Returns {'status': True}
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, HasValidSession)
    # serializer_class = ActivitySerializer
    serializer_class = DriverActivitySerializer

    def post(self, request, data=None):
        """POST method for saving driver Activity details."""
        context = {'request': request}
        if not data:
            data = request.data.dict()
        response = None
        user = request.user
        if not user.is_active_driver():
            """
            Just a preventive check for vendor.
            This api should not be triggered from PARTNER App.
            """
            return Response(status=status.HTTP_200_OK, data={'status': True})
        driver = user.driver
        data['driver'] = driver
        location = data.get('location_details', None)
        try:
            location = json.loads(location)
        except BaseException:
            # logger.error('Invalid Location json sent')
            return Response(status=status.HTTP_200_OK,
                            data="Invalid Location json sent")
        if location:
            data['latitude'] = location.get('mLatitude')
            data['longitude'] = location.get('mLongitude')
            data['location_accuracy_meters'] = location.get('mAccuracy')
            # Converting meters per second to km/hr
            _mspeed = float(location.get('mSpeed', 0))
            if not math.isnan(_mspeed):
                data['vehicle_speed_kmph'] = int(_mspeed * 3.6)
            # data['vehicle_speed_kmph'] = int(location.get('mSpeed', 0) * 3.6)
            data['gps_timestamp'] = datetime.datetime.fromtimestamp(
                location.get('mTime', 0) / 1e3).strftime('%Y-%m-%d %H:%M:%S')
        online = data.get('online')
        if online == 'true':
            data['event'] = driver_constants.ONLINE_DRIVER_ACTION
        else:
            data['event'] = driver_constants.OFFLINE_DRIVER_ACTION
        driver_activity_serializer = self.serializer_class(
            data=data, context=context)
        driver_existing_activity = None
        try:
            driver_existing_activity = DriverActivity.objects.get(
                driver=driver)
        except DriverActivity.DoesNotExist:
            if online == 'true':
                # In case there is no driver activity if "off_duty" is sent
                # as event in data, DO NOTHING
                response = driver_activity_serializer.create()
                if not response:
                    # logger.error(
                    #     'Driver activity creation failed for driver:- "%s-%s"',
                    #     driver.user.name, driver.user.phone_number)
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=_("Driver activity creation failed."))
                # logger.info('Created Driver Activity for driver:- "%s-%s"',
                #             driver.user.name,
                #             driver.user.phone_number.national_number)
        if driver_existing_activity:
            if online == 'false':
                driver_activity_serializer.delete()
                # logger.info('Deleted Driver Activity for driver:- "%s-%s"',
                #             driver.user.name,
                #             driver.user.phone_number.national_number)
            else:
                response = driver_activity_serializer.update()
                if not response:
                    # logger.error(
                    #     'Driver activity updation failed for driver:- "%s-%s"',
                    #     driver.user.name, driver.user.phone_number)
                    Response(status=status.HTTP_200_OK, data={'status': True})
                    # return Response(status=status.HTTP_400_BAD_REQUEST,
                    #                 data=_("Driver activity updation failed."))
                # logger.info(
                #     'Driver Activity Update Successful for driver:- "%s-%s"',
                #     driver.user.name, driver.user.phone_number.national_number)
        return Response(status=status.HTTP_200_OK, data={'status': True})


# TODO mutilple request
class DutyStatus(views.APIView):
    """
    API to Log the Driver Duty Status(online/offline) and Activity.

    Permissions added.Only Active Drivers can access this api

    POST(online, location_details)
    {
        "online": "true" / "false"
    }
    On Success: Returns Time Intervals after which each driver activity call will be made from app
    On Failure: Returns 500 with the error.
    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = DutyStatusSerializer

    def post(self, request):
        """POST method for Logging the Driver Duty Status(online/offline) and Activity."""
        data = request.data.dict()
        online = data.get('online')
        error_message = ''
        response_data = ''
        status_code = status.HTTP_200_OK
        driver = request.user.driver

        if online == 'true':
            action = driver_constants.ONLINE_DRIVER_ACTION
        else:
            action = driver_constants.OFFLINE_DRIVER_ACTION

        try:
            if online != 'true':
                Activity.as_view()(request._request, data)
            request.user.driver.update_driver_log_info(action)

            response_data = self.serializer_class(
                driver, context={'request': request}).data
        except BaseException as e:
            error_message = ('Error: %s, Call Support' % e)
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=None, )

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)


class Statement(generics.RetrieveAPIView):
    """Show Previous Settlement amount of driver and next settlement."""

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = DriverLedgerSerializer

    def get(self, request):
        """GET method for getting ledger data of driver."""
        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor') == 'true' else False
        driver = request.user.driver if not is_vendor else Driver.objects.get(id=data.get('driver_id'))
        error_message = ''
        geopoint = None
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED
        vendor = request.user.vendor if is_vendor else ''
        response_data = {
            "count": 0,
            "previous": 0,
            "next": 0,
            "result": [],
            "unprocessed_trips_count": 0,
            "next_settlement": 0.0
        }

        query = Q(driver=driver, is_settlement=True)

        if is_vendor:
            query = query & Q(owner_details=vendor)

        if driver.own_vehicle or is_vendor:
            driver_ledger = DriverLedger.objects.filter(
                query).order_by('-transaction_time')

            unprocessed_trips = PaymentTrip.objects.filter(
                payment__status=driver_constants.UNAPPROVED,
                payment__driver=driver,
                trip__status=TRIP_COMPLETED).distinct('trip').count()

            next_settlement_amount = DriverLedger.objects.filter(
                driver=driver).values_list('balance').order_by('-transaction_time').first()

            logging.info('Page to be displayed : %s' % next)
            try:
                driver_ledger_serializer = common_serializer.PaginatedSerializer(
                    queryset=driver_ledger,
                    num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                    page=next,
                    serializer_method=self.serializer_class)
                response_data = driver_ledger_serializer.data
                response_data['unprocessed_trips_count'] = unprocessed_trips
                response_data['next_settlement'] = float(next_settlement_amount[0]) if next_settlement_amount else 0.0

            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                logger.error(traceback.format_exc())
                create_error_object(driver, traceback.format_exc(),
                                    request.build_absolute_uri(),
                                    current_location=geopoint, )

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)


# TODO need to change url from earnings to payment
class Payments(generics.RetrieveAPIView):
    """
    Returns driver Payments paginated data.

    Permissions added.Only Active Drivers can access this api
    GET()
    {
        "next": 0
    }

    On Success: Will return 200 and the payments data as json
    On Failure: Will return 500 with the error.
    """

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = DriverPaymentSerializer

    def get(self, request):
        """GET method for getting payments data of driver."""
        driver = request.user.driver
        data = request.GET.dict()
        error_message = ''
        response_data = {}
        geopoint = None
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        if driver.own_vehicle:
            payment_records = DriverPayment.objects.filter(
                driver=driver, status=driver_constants.APPROVED, calculation__isnull=False)
            payment_records = payment_records.prefetch_related(
                'drivercashpaid_set').order_by('-start_datetime')
            try:
                driver_payment_serializer = common_serializer.PaginatedSerializer(
                    queryset=payment_records,
                    num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                    page=next,
                    serializer_method=self.serializer_class)
                response_data = driver_payment_serializer.data
            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                logger.error(traceback.format_exc())
                create_error_object(driver, traceback.format_exc(),
                                    request.build_absolute_uri(),
                                    current_location=geopoint, )

        # TODO need to be removed
        return Response(status=status_code,
                        data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)


class LedgerStatement(generics.RetrieveAPIView):
    """
    Returns driver ledger paginated data.

    Permissions added.Only Active Drivers can access this api
    GET()
    {
        "next": 0
    }

    On Success: Will return 200 and the ledger data as json
    On Failure: Will return 500 with the error.
    """

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = DriverLedgerSerializer

    def get(self, request):
        """GET method for getting ledger data of driver."""
        driver = request.user.driver
        data = request.GET.dict()
        error_message = ''
        response_data = {}
        geopoint = None
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        if driver.own_vehicle:
            driver_ledger = DriverLedger.objects.filter(
                driver=driver).order_by('-transaction_time')

            logging.info('Page to be displayed : %s' % next)
            try:
                driver_ledger_serializer = common_serializer.PaginatedSerializer(
                    queryset=driver_ledger,
                    num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                    page=next,
                    serializer_method=self.serializer_class)
                response_data = driver_ledger_serializer.data
            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                logger.error(traceback.format_exc())
                create_error_object(driver, traceback.format_exc(),
                                    request.build_absolute_uri(),
                                    current_location=geopoint, )

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)


class DocumentUpload(generics.CreateAPIView):
    """
    Document upload from driver app.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsAuthenticated,)

    def get_spoc(self, driver=None, vendor=None, vehicle=None):

        query = None
        if driver:
            query = Q(driver_document__driver=driver, status=VERIFICATION_PENDING)
        elif vendor:
            query = Q(vendor_document__vendor=vendor, status=VERIFICATION_PENDING)
        elif vehicle:
            query = Q(vendor_document__vendor=vendor, status=VERIFICATION_PENDING)

        if query:
            spoc = DocumentEvent.copy_data.filter(query).first()
            if spoc and spoc.action_owner_id:
                return spoc.action_owner_id
            spoc_query = Q(status=CONTRACT_STATUS_ACTIVE, spocs__is_active=True)

            if driver:
                spoc_query = spoc_query & Q(city=driver.operating_city)
            spocs = Contract.objects.filter(spoc_query).\
                exclude(id__in=Contract.objects.filter(status=CONTRACT_STATUS_ACTIVE,
                                                       resourceallocation__drivers=driver.id if driver else None
                                                       ).values_list('id')).values_list('spocs')
            spoc_count = spocs.count()

            if not spoc_count:
                spocs = City.objects.filter(id=driver.operating_city_id, process_associates__is_active=True). \
                    values_list('process_associates')
                spoc_count = spocs.count()

            spoc_id = spocs[random.randint(0, spoc_count - 1)][0]

            return spoc_id

    def get(self, request):

        driver = request.user.driver if hasattr(request.user, 'driver') else None
        vendor = request.user.vendor if hasattr(request.user, 'vendor') else None

        driver_document_type = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT)
        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)
        doc_list = []
        veh_doc_list = []

        for doc_type in driver_document_type:
            doc_data = {
                'code': doc_type.code,
                'name': doc_type.name,
                'identifier': doc_type.identifier,
                'is_expire_needed': doc_type.is_expire_needed,
            }
            if driver:
                doc_info = DriverDocument.objects.filter(driver_id=driver.id, document_type=doc_type).order_by('-created_date')
                doc_serializer = DriverDocumentSerializer
            else:
                doc_info = VendorDocument.objects.filter(vendor_id=vendor.id, document_type=doc_type).order_by('-created_date')
                doc_serializer = VendorDocumentSerializer
            doc_data.update({'document': doc_serializer(doc_info, many=True).data})
            doc_data.update({'is_doc_available': True if len(doc_data['document'])>0 else False})
            doc_list.append(doc_data)

        for doc_type in vehicle_document_type:
            doc_data = {
                'code': doc_type.code,
                'name': doc_type.name,
                'identifier': doc_type.identifier,
                'is_expire_needed': doc_type.is_expire_needed,
            }

            query = Q(vehicle__driver=driver) if driver else Q(vehicle__vendor=vendor)
            query = query & Q(document_type=doc_type)
            doc_info = VehicleDocument.objects.filter(query).order_by('-created_date')
            doc_data.update({'document': VehicleDocumentSerializer(doc_info, many=True).data})
            veh_doc_list.append(doc_data)

        sorted_list = sorted(doc_list, key=lambda k: k['document'][0]['status_id'] if k['is_doc_available'] else 100)

        result = {
            'document_list': sorted_list,
            'vehicle_doc_list': veh_doc_list,
            'state': address_utils.get_state_list()
        }

        return Response(status=status.HTTP_200_OK, data=result,
                        content_type='text/html; charset=utf-8')

    def post(self, request):
        doc = request.data.dict()
        logging.info(doc)
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        is_driver = driver is not None
        vendor = request.user.vendor if hasattr(request.user, 'vendor') else None
        page_count = int(doc.get('page_count',1))
        identifier = doc.get('identifier')
        doc_number = doc.get('doc_number','--')
        is_self_reg = doc.get('is_self_reg', None)

        doc_type = DocumentType.objects.filter(code=doc.get('doc_type')).first()
        state = State.objects.filter(name=doc.get('document_state')).first()

        invoid_check_required = False
        # if doc_type.code in [DOCUMENT_PHOTO, DOCUMENT_COVID_PASS]:
        #     invoid_check_required = False

        input_file_list = []
        for i in range(1,page_count+1):
            name = 'file{}'.format(i)
            f = doc.get(name,None)
            input_file_list.append(f)

        if len(input_file_list) == 0:
            return Response(status=status.HTTP_202_ACCEPTED,
                                data = 'Please send the uploaded document',
                                content_type='text/html; charset=utf-8')

        driver_doc_exists = DriverDocument.objects.filter(
            document_type_id=doc_type.id, number=doc_number,
            status=DriverDocument.ACTIVE).exclude(
            driver_id=driver.id
        ).exists()

        exclude_param = Q()
        if driver.vehicles.first():
            exclude_param = Q(vehicle_id=driver.vehicles.first().id)

        vehicle_doc_exists = VehicleDocument.objects.filter(
            document_type_id=doc_type.id, number=doc_number,
            status=VehicleDocument.ACTIVE).exclude(exclude_param).exists()

        if driver_doc_exists or vehicle_doc_exists:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Document number %s already exists, please contact support') % doc_number)

        document_data = {
            'file': input_file_list[0],
            'document_type': doc_type.id,
            'status': VERIFICATION_PENDING,
            'number': doc_number,
            'state': state.id if state else None,
            'created_by': request.user,
            'modified_by': request.user
        }

        if driver and identifier == VEHICLE_DOCUMENT:
            vehicles = driver.vehicles.all()
            vehicles_mapped_count = 0 if not vehicles else vehicles.count()

            if vehicles_mapped_count > 1:
                return Response(data='Driver is associated with more than one vehicle',
                                status=status.HTTP_202_ACCEPTED)

            if doc_type.code == DOCUMENT_RC:

                if is_self_reg and is_self_reg == 'true':
                    vehicle_list = Vehicle.objects.filter(registration_certificate_number=doc_number)
                    if vehicle_list.exists():
                        return Response(data='This vehicle number is already registered to other driver',
                                        status=status.HTTP_202_ACCEPTED)

                if vehicles_mapped_count == 1:
                    return Response(data='Driver is already associated with a vehicle',
                                    status=status.HTTP_202_ACCEPTED)
                else:
                    vehicle_data = {
                        'registration_certificate_number': doc_number,
                        'vehicle_model': doc.get('vehicle_model'),
                        'body_type': doc.get('body_type'),
                        'model_year': doc.get('model_year')
                    }

                    vehicle = DriverAddUpdateMixin._add_vehicle(driver.id, vehicle_data)
                    if isinstance(vehicle,Vehicle):
                        document_data.update({'vehicle': vehicle.id, 'number' : doc_number})
                    else:
                        return vehicle
            else:
                if vehicles_mapped_count == 0:
                    rc_equivalent_type = DocumentType.objects.get(document_type='RC')
                    message = 'Please upload the {} first to add vehicle'.format(
                                rc_equivalent_type.document_type)
                    return Response(data=message,
                                status=status.HTTP_202_ACCEPTED)

                else:
                    vehicle = vehicles[0]
                    document_data.update({'vehicle': vehicle.id, 'number' : doc_number})

        if driver:
            if identifier == DRIVER_DOCUMENT:
                if doc_type.code == DOCUMENT_PHOTO:
                    document_data['status'] = ACTIVE
                document_data.update({'driver': driver.id})
                document_serializer = DriverDocumentSerializer(data=document_data)

            if identifier == VEHICLE_DOCUMENT:
                document_serializer = VehicleDocumentSerializer(data=document_data)

        elif vendor:
            if identifier == DRIVER_DOCUMENT:
                document_data.update({'vendor': vendor.id})
                document_serializer = VendorDocumentSerializer(data=document_data)
            if identifier == VEHICLE_DOCUMENT:
                document_serializer = VehicleDocumentSerializer(data=document_data)

        if document_serializer.is_valid():

            if identifier == DRIVER_DOCUMENT:
                DriverDocument.objects.filter(driver = driver,
                        document_type = doc_type,
                        status = VERIFICATION_PENDING).update(
                        status = REJECT)

            elif identifier == VEHICLE_DOCUMENT:
                VehicleDocument.objects.filter(vehicle = vehicle,
                        document_type = doc_type,
                        status = VERIFICATION_PENDING).update(
                        status = REJECT)

            #always driver or vendor is passed
            doc = document_serializer.save(created_by=document_data.get(
                'created_by'), modified_by=document_data.get(
                'modified_by'))

            if driver and doc:
                document_page_list = [
                    {
                        'page_number' : (i+1),
                        'file' : input_file_list[i] } for i in range(page_count)
                ]

                document_page_list[0]['file'] = doc.file
                dps_list = []
                if identifier == DRIVER_DOCUMENT:
                    for element in document_page_list:
                        element.update({'driver_document' : doc.id})
                        documentpage_serializer = DriverDocumentPageSerializer(data=element)
                        if documentpage_serializer.is_valid():
                            dps_list.append(documentpage_serializer)


                if identifier == VEHICLE_DOCUMENT:
                    for element in document_page_list:
                        element.update({'vehicle_document' : doc.id})
                        documentpage_serializer = VehicleDocumentPageSerializer(data=element)
                        if documentpage_serializer.is_valid():
                            dps_list.append(documentpage_serializer)

                if len(dps_list) == page_count:
                    for element in dps_list:
                        e = element.save(created_by=document_data.get(
                            'created_by'), modified_by=document_data.get(
                            'modified_by'))

            # if invoid_check_required:
            #     pid = None
            #     if driver:
            #         pid = driver.id
            #     elif vendor:
            #         pid = vendor.id

            #     if pid:
            #         send_document_for_verification_to_invoid.apply_async((
            #                 pid,
            #                 identifier,
            #                 doc.id,
            #                 is_driver),)

            return Response(status=status.HTTP_200_OK,data={},content_type='text/html; charset=utf-8')
            # return Response(status=status.HTTP_400_BAD_REQUEST,data={},content_type='text/html; charset=utf-8')

        else:
            return Response(status=status.HTTP_202_ACCEPTED,
                                data = document_serializer.errors,
                                content_type='text/html; charset=utf-8')


class DriverReferralsView(views.APIView, DriverReferral):
    """
    Description     : API related to referred driver.

    Request type    : GET, POST

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = DriverReferralSerializer
    ordering_fields = ('-planned_start_time',)

    def get(self, request):
        """GET method for getting data of drivers referred by the user."""
        driver = request.user.driver
        data = request.GET.dict()
        error_message = ''
        response_data = {}
        status_code = status.HTTP_200_OK
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        try:
            referral_qs = self.__get_queryset()
            referral_qs_serializer = common_serializer.PaginatedSerializer(
                queryset=referral_qs,
                num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                page=next,
                serializer_method=self.serializer_class,
                context={'request': request})
            response_data = referral_qs_serializer.data

        except BaseException as e:
            error_message = ('Error: %s, Call Support' % e)
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(driver, traceback.format_exc(),
                                request.build_absolute_uri(),
                                current_location=None, )

        # TODO need to be removed
        return Response(status=status_code,
                        data=error_message or response_data,
                        content_type='text/html; charset=utf-8')

        return _get_api_reponse(status_code, error_message, response_data)

    def __get_queryset(self):
        referral_qs = self.__get_query()
        referral_qs = referral_qs.select_related(
            'referral_config', 'referred_by')
        return referral_qs

    def __get_query(self):
        driver = self.request.user.driver
        return Driver.objects.filter(referred_by=driver.user)

    def post(self, request):
        """
        POST method for creating a Driver Record referred by the user.

        request data-> refer_driver_details={'name': 'gurva', 'city_id': 1, 'phone_number': '9971153201'}
        response_data->
            On Success: Returns bonus_amount, created_date, bonus_generated
            On Failure: Returns 500 with the error.
        """
        data = request.data.dict()
        self._initialize_attributes(data)

        try:
            self._create_driver()
        except BaseException as e:
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            self.error_message = ('Error: %s, Call Support' % e)
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint, )

        # TODO need to be removed
        return Response(status=self.status_code, data=self.response_data or self.error_message)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Arrival(generics.CreateAPIView):
    """
    Send message to customer regarding driver arrival.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsActiveDriver, HasValidSession)

    def post(self, request):
        """Send message to customer when driver is in vicinity of customer's pickup location."""
        error_message = ''
        response_data = 'Message sent successfully'
        status_code = status.HTTP_200_OK
        data = request.data.dict()

        order_id = data.get('order_id')
        if not order_id:
            error_message = 'Order number not sent !'

        order = Order._default_manager.filter(pk=order_id).select_related(
            'customer', 'customer__user').first()
        if not order:
            error_message = 'Order does not exist !'
        else:
            customer = order.customer
            if not customer or not customer.user:
                error_message = 'Order without customer !'
            elif not customer.user.phone_number:
                error_message = 'Customer Phone Number empty !'

        if error_message:
            status_code = status.HTTP_400_BAD_REQUEST
        else:
            template_dict = sms_templates.template_driver_arriving
            customer.user.send_sms(template_dict['text'], template_dict)
            # Send Whatsapp notification
            msg_template_data = {"template_name": "driver_arrival_at_pickup",
                                 "params_data": [order.customer.name,
                                                 order.number,
                                                 order.driver.driver_vehicle]}
            send_whatsapp_notification([settings.ACTIVE_COUNTRY_CODE + str(
                order.customer.user.phone_number.national_number)],
                                       msg_template_data, order)
        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)


class DeviceStatus(generics.CreateAPIView):
    """
    Send message to customer regarding driver arrival.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsActiveDriver, HasValidSession)

    def post(self, request):
        """Save driver device details when phone gets switched off anf on."""
        error_message = ''
        response_data = 'Details saved successfully'
        status_code = status.HTTP_200_OK

        if request.user.is_active_driver():
            self.driver = request.user.driver

            data = request.data
            if not isinstance(data, dict):
                data = data.dict()

            try:
                switch_off_details = json.loads(data.get('before_battery_percent'))
                switch_on_details = json.loads(data.get('after_battery_percent'))
            except BaseException:
                # logger.error('Invalid Location json sent')
                return Response(status=status.HTTP_200_OK,
                                data="Invalid Location json sent")

            switch_off_details['action'] = 'Switched Off'
            switch_off_details['action_date'] = data.get('dev_shutdown_time')

            switch_on_details['action'] = 'Switched On'
            switch_on_details['action_date'] = data.get('dev_boot_time')

            try:
                self._save_details(details=switch_off_details)
                self._save_details(details=switch_on_details)
            except BaseException as e:
                logger.error(traceback.format_exc())
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                error_message = ('Error: %s, Call Support' % e)
                create_error_object(self.driver, traceback.format_exc(),
                                    request.build_absolute_uri(),
                                    current_location=None)

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)

    def _save_details(self, details, data={}):
        last_plugged_at = details.pop('LastChargingPluggedDateTime') \
            if details.get('LastChargingPluggedDateTime') else None
        last_unplugged_at = details.pop('LastChargingUnPluggedDateTime') \
            if details.get('LastChargingUnPluggedDateTime') else None
        action_date = details.pop('action_date') if details.get('action_date') else None

        data['last_plugged_at'] = datetime.datetime.strptime(last_plugged_at, '%Y-%m-%dT%H:%M:%S%z') \
            if last_plugged_at else None

        data['last_unplugged_at'] = datetime.datetime.strptime(last_unplugged_at, '%Y-%m-%dT%H:%M:%S%z') \
            if last_unplugged_at else None
        data['action_date'] = datetime.datetime.strptime(action_date, '%Y-%m-%dT%H:%M:%S%z') \
            if action_date else None

        data['action'] = details.pop('action') if details.get('action') else ''
        data['driver'] = self.driver
        data['battery'] = details.pop('batteryLevel') \
            if details.get('batteryLevel') else 0
        data['charging_status'] = details.pop('chargingStatus') \
            if details.get('chargingStatus') else False
        data['charging_mode'] = details.pop('chargingMode') \
            if details.get('chargingMode') else ''
        data['significant_level'] = details.pop('significantLevel') \
            if details.get('significantLevel') else ''
        data['extra_details'] = details

        log_info = DriverLogInfo(**data)
        log_info.save()


class PaymentDetails(generics.RetrieveAPIView):
    """
    Returns driver Payments data.

    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 and the payments data as json
    On Failure: Will return 500 with the error.
    """

    permission_classes = (IsActiveDriver, HasValidSession)
    def get_payment_data(self, payment_records):
        data = []
        approved_sum = 0
        paid_sum = 0
        for record in payment_records:
            if record.status == APPROVED and record.is_settled:
                paid_sum += record.net_pay
                sequence = 3
            elif record.status == APPROVED and not record.is_settled:
                approved_sum += record.net_pay
                sequence = 2
            else:
                sequence = 1

            trip_dict = []
            count = 0
            for paymenttrip in record.paymenttrip_set.all():
                trip = paymenttrip.trip
                if trip.status in [TRIP_COMPLETED]:
                    count += 1
                    trip_dict.append({
                        'planned_start_time': trip.planned_start_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(settings.TIME_FORMAT),
                        'actual_start_time': trip.actual_start_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(settings.TIME_FORMAT) if trip.actual_start_time else '',
                        'actual_end_time': trip.actual_end_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(settings.TIME_FORMAT) if trip.actual_end_time else '',
                        'total_time': str(minutes_to_dhms(trip.total_time)),
                        'total_distance': str(round(trip.gps_distance or trip.total_distance, 2)) + ' KM',
                        'area1': trip.hub.name if trip.hub else trip.order.pickup_address.line3
                                if trip.order and trip.order.pickup_address else '',
                        'area2': trip.order.shipping_address.line3
                                               if trip.order and trip.order.shipping_address and not trip.hub else '',
                        'customer_name': str(trip.order.customer) if trip.order and not record.contract.customer else''
                    })

            data.append({
                'id': record.id,
                'sequence': sequence,
                'customer': str(record.contract.customer) if record.contract and record.contract.customer else '',
                'net_pay': record.net_pay,
                'payment_type': record.payment_type,
                'toll_charges': record.toll_charges,
                'labour_cost': record.labour_cost,
                'gross_pay': record.gross_pay,
                'start_datetime': record.start_datetime.strftime(settings.DATE_FORMATS),
                'end_datetime': record.end_datetime.strftime(settings.DATE_FORMATS),
                'trip_count': count,
                'trip_details': trip_dict
            })

        return data, paid_sum, approved_sum

    def get_payment_response(self, driver, vendor):
        today = timezone.now().date()
        start = today - timedelta(days=60)
        end = today
        query = Q(driver=driver, status__in=[APPROVED, UNAPPROVED], start_datetime__gte=start, start_datetime__lte=end)
        response_data = {}
        if vendor:
            query = query & Q(driverledger__owner_details=vendor)
        payment_records = DriverPayment.objects.filter(query)
        payment_records = payment_records.prefetch_related(
            'drivercashpaid_set', 'paymenttrip_set', 'paymenttrip_set__trip__hub',
            'paymenttrip_set__trip', 'paymenttrip_set__trip__order',
            'paymenttrip_set__trip__order__shipping_address', 'paymenttrip_set__trip__order__customer',
            'paymenttrip_set__trip__order__pickup_address').order_by('-start_datetime').exclude(net_pay=0)
        payment_records = payment_records.select_related('contract', 'contract__customer')

        data, paid_sum, approved_sum = self.get_payment_data(payment_records)
        response_data['data'] = data
        response_data['processed_amount'] = paid_sum
        response_data['due_for_payment'] = approved_sum

        return response_data

    def get(self, request):
        """GET method for getting payments data of driver."""

        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor') == 'true' else False
        driver = request.user.driver if not is_vendor else Driver.objects.get(id=data.get('driver_id'))
        vendor = request.user.vendor if is_vendor else ''

        response_data = {}
        status_code = status.HTTP_200_OK

        if driver.own_vehicle or vendor:
            response_data = self.get_payment_response(driver, vendor)


        return Response(status=status_code,
                        data=response_data,
                        content_type='text/html; charset=utf-8')


class driver_tds(generics.RetrieveAPIView):

    def get(self, request):
        from blowhorn.driver.util import get_financial_year

        driver, vendor = None, None
        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor', '') == 'true' else False
        financial_year = data.get('unaccepted_tds_year', '')
        if not financial_year:
            financial_year = get_financial_year(timezone.now().date())
        if not is_vendor:
            driver = request.user.driver
        else:
            vendor = request.user.vendor

        if financial_year and ((driver and driver.pan and
                                driver.own_vehicle) or (vendor and vendor.pan)):
            return render_to_response(
                'admin/driver/tds.html',
                {
                    'driver': driver if not is_vendor else vendor,
                    'financial_year': financial_year
                }
            )

        return Response(status=400,
                        data='Pan of driver is not available or is not owner',
                        content_type='text/html; charset=utf-8')

    def post(self, request):
        from blowhorn.driver.util import get_financial_year

        data = request.data.dict()
        is_vendor = True if data.get('is_vendor') == 'true' else False
        financial_year = data.get('unaccepted_tds_year', '')
        if not financial_year:
            financial_year = get_financial_year(timezone.now().date())

        if not is_vendor:
            driver = request.user.driver
            query = Q(year=financial_year, driver=driver)
            tds_data = {
                "year": financial_year,
                "driver": driver
            }
        else:
            vendor = request.user.vendor
            tds_data = {
                "year": financial_year,
                "vendor": vendor
            }
            query = Q(year=financial_year, vendor=vendor)

        driver_tds = DriverTds.objects.filter(query).first()
        if not driver_tds:
            driver_tds = DriverTds(**tds_data)
            driver_tds.save()
            driver_tds.created_by = request.user

        tds_status = data.get('tds_status')

        if tds_status == TOS_ACCEPTED:
            driver_tds.is_tds_accepted = True
            driver_tds.accepted_date = timezone.now()
        else:
            driver_tds.is_tds_accepted = False
        driver_tds.modified_by = request.user
        driver_tds.save()

        return Response(status=status.HTTP_200_OK)


class GpsInformation(generics.CreateAPIView):

    def post(self, request):
        if not request.user.is_active_driver():
            return Response(status=status.HTTP_200_OK)

        data = request.data.dict()
        driver = request.user.driver
        trip_id = int(data.get('trip_id', None))
        is_gps_on = True if data.get('is_gps_on', False) == 'true' else False
        device_timestamp = data.get('device_timestamp')
        network_ontime = data.get('network_ontime', None)
        network_offtime = data.get('network_offtime', None)

        try:
            gps_info = DriverGPSInformation(
                driver=driver,
                trip_id=trip_id,
                network_ontime=network_ontime,
                is_gps_on=is_gps_on,
                device_timestamp=device_timestamp,
                network_offtime=network_offtime
            )
            gps_info.save()
        except ValidationError as ve:
            logger.info('Validation: %s' % ve)
        except BaseException as be:
            logger.info('Other exception: %s' % be)

        # @NOTE: App is not handling other status and it will display toast. so returning 200
        return Response(status=status.HTTP_200_OK)

class PartnerDetails(views.APIView):

    permission_classes = (HasValidSession,)

    def get(self, request):
        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor', '') == 'true' else False

        if is_vendor:
            vendor = request.user.vendor
            data = VendorSerializer(vendor).data
        else:
            driver = request.user.driver
            data = DriverLoginSerializer(driver).data

        return Response(status=status.HTTP_200_OK, data=data)


class Partner(views.APIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            input = request.data
            location_details = request.data.get("location_details",'{}')
            location = json.loads(location_details)
            phone_number = input.get('phone_number', None)
            is_driver = input.get('is_driver', None)
            is_vendor = input.get('is_vendor', None)
            otp = input.get('otp', None)

            is_vendor = True if is_vendor == 'true' else False
            is_driver = True if not is_vendor else False

            if not otp:
                return Response(status=status.HTTP_417_EXPECTATION_FAILED,
                                data=_('OTP is missing'))

            user = get_partner_user_from_phone_number(
                                phone_number, is_driver, is_vendor)

            if not user:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=_('User does not exist for the partner'))

            if user.is_active_user():
                return Response(status=status.HTTP_417_EXPECTATION_FAILED,
                                    data=_('Active user already exists. Please try to login'))

            if not user.last_otp_sent_at or not user.last_otp_value:
                return Response(status=status.HTTP_417_EXPECTATION_FAILED,
                                data=_('OTP is expired'))

            delta = timezone.now() - user.last_otp_sent_at
            if delta > timedelta(minutes=15):
                return Response(status=status.HTTP_417_EXPECTATION_FAILED,
                            data=_('OTP is expired'))

            if int(otp) != int(user.last_otp_value):
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE,
                        data=_('OTP is invalid'))

            pin = str(random.randint(1001, 9999))
            user.set_password(pin)
            user.is_active = True
            user.is_mobile_verified = True
            user.date_joined = timezone.now()
            user.save()

            is_inactive_user, is_partner_created = self.__add_partner(
                                        user, is_driver, is_vendor, location)

            if is_inactive_user:
                pin = '----'
                user.set_password(pin)
                user.is_active = False
                user.is_mobile_verified = False
                user.save()
                return Response(status=status.HTTP_417_EXPECTATION_FAILED,
                                data=_('You are an inactive or blacklisted user'))

            if not is_partner_created:
                return Response(status=status.HTTP_417_EXPECTATION_FAILED,
                                data=_('Partner already exists. Please try to login'))

            Communication.send_sms(sms_to=phone_number,
                    message=sms_templates.template_user_password['text'] % otp,
                    priority='high',
                    template_dict=sms_templates.template_user_password)
            return Response(status=status.HTTP_200_OK, data={'pin' : pin})

        except Exception as e:
            logger.error(str(e))
            return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=_('Please try again in sometime'))

    def __add_partner(self, user, is_driver, is_vendor, location):
        is_inactive_user = False
        is_partner_created = True

        if is_driver:
            partner = Driver.objects.filter(user__email=user.email).first()
        elif is_vendor:
            partner = Vendor.objects.filter(user__email=user.email).first()

        if partner and (partner.status in
                    [StatusPipeline.INACTIVE,StatusPipeline.BLACKLISTED]):
            is_inactive_user = True

        if not partner and not is_inactive_user:
            data = {'name' : user.name}
            if user.city:
                city = City.objects.filter(name=user.city).first()

            if not city:
                operating_cities = address_utils.get_operating_cities(location)
                city = City.objects.get(name=operating_cities[0])

            if is_driver:
                data['operating_city'] = city.id
                data['user'] = user.id
                data['active_from'] = timezone.now()
                data['created_by'] = user
                data['source'] = driver_constants.USER_ACCESS_DRIVER_APP
                data['supply_source'] = driver_constants.USER_ACCESS_DRIVER_APP
                CreateModelMixin().create(data, serializer_class=DriverSerializer)

            elif is_vendor:
                data['user'] = user
                data['phone_number'] = str(user.phone_number.national_number)
                data['source'] = driver_constants.USER_ACCESS_DRIVER_APP
                vendor = add_vendor(data)
                _location = {'vendor' : vendor , 'city' : city}

                self.__inform_spoc_and_user(user, city)

                vendor.preferred_locations.add(
                        VendorPreferredLocation.objects.create(**_location))
                vendor.created_by = user
                vendor.modified_date = timezone.now()
                vendor.modified_by = user
                vendor.save()

            is_partner_created = True

        return is_inactive_user,is_partner_created

    def __inform_spoc_and_user(self, user, city):

        associate_list_check = False
        spocs = Contract.objects.filter(city=city.id).exclude(spocs__email__isnull=True,
                            spocs__is_active=False).values('spocs__email',
                                                            'spocs__name',
                                                            'spocs__email')
        if spocs.count() == 0:
            associate_list_check = True
            spocs = City.objects.filter(id=city.id, process_associates__is_active=True). \
                        values('process_associates__email',
                                'process_associates__name',
                                'process_associates__phone_number')

        spoc_count = spocs.count()
        spoc = spocs[random.randint(0, spoc_count - 1)]

        spoc_user = {}
        if associate_list_check:
            spoc_user['name'] = spoc.get('process_associates__name')
            spoc_user['email'] = spoc.get('process_associates__email')
            spoc_user['phone_number'] = spoc.get('process_associates__phone_number')
        else:
            spoc_user['name'] = spoc.get('spocs__name')
            spoc_user['email'] = spoc.get('spocs__email')
            spoc_user['phone_number'] = spoc.get('spocs__phone_number')

        self.__send_vendor_onboarding_mail_for_spoc(user, spoc_user)

    def __send_vendor_onboarding_mail_for_spoc(self, user, spoc_user):
        subject = 'Fleet Owner Registration Notification - %s | %s' % (user.name,user.phone_number.national_number)
        spoc_name = spoc_user.get('name','the spoc')
        spoc_email = spoc_user.get('email',None)
        spoc_phone = spoc_user.get('phone_number',None)

        template_dict = sms_templates.template_vendor_registration
        message = template_dict['text'] % (spoc_name, spoc_phone)
        user.send_sms(message, template_dict)

        if settings.ENABLE_SLACK_NOTIFICATIONS:
            body = 'Dear %s, <br>' % spoc_name
            body += '%s has registered on the Partner App as fleet owner. <br>' % str(user.name)
            body += 'Please onboard them through Cosmos. <br>'
            body += '<br><br>Regards,<br>' + 'Team %s'  %('Blowhorn' if settings.COUNTRY_CODE_A2 == 'IN' else company.name)

            Email().send_email_message([spoc_email], subject, body)
class UserSignUp(views.APIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            serializer = UserSignUpSerializer(data=request.data)
            if not serializer.is_valid():
                message = list(serializer.errors.values())[0][0]
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_(message))

            name = serializer.validated_data.get('name')
            city = serializer.validated_data.get('city')
            phone_number = str(serializer.validated_data.get('phone_number'))
            is_driver = serializer.validated_data.get('is_driver')
            is_vendor = serializer.validated_data.get('is_vendor')

            user = get_partner_user_from_phone_number(
                phone_number, is_driver, is_vendor)

            if user and user.is_active_user():
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_('Mobile number already registered. Please try to login'))

            if not user:
                user = self.__create_user(name, phone_number, city, is_driver, is_vendor)

            is_old_otp = False
            if user.last_otp_sent_at and user.last_otp_value:
                delta = timezone.now() - user.last_otp_sent_at
                if delta <= timedelta(minutes=15):
                    otp = user.last_otp_value
                    is_old_otp = True
                else:
                    otp = random.randint(1001, 9999)
            else:
                otp = random.randint(1001, 9999)

            Communication.send_sms(sms_to=phone_number, message=sms_templates.template_otp['text'] % otp,
                                       priority='high', template_dict=sms_templates.template_otp)

            if not is_old_otp:
                user.last_otp_sent_at = timezone.now()
                user.last_otp_value = otp
                user.save()

            return Response(status=status.HTTP_200_OK, data=_('User successfully created'))

        except Exception as e:
            logger.error(str(e))
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Please try again in sometime'))

    def __create_user(self, name, phone_number, city, is_driver=False, is_vendor=False):
        domain = None
        user = None

        if is_driver:
            domain = settings.DEFAULT_DOMAINS.get('driver')
        elif is_vendor:
            domain = settings.DEFAULT_DOMAINS.get('vendor')
        else:
            return user

        if settings.ACTIVE_COUNTRY_CODE not in phone_number:
            phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE,
                                       phone_number)

        if not user:
            name = re.sub('\W+', ' ', name)
            user_data = {
                'email': None,
                'name': name,
                'phone_number': str(phone_number),
                'is_active': False,
                'domain': domain,
                'city': city,
            }
            user = DriverManager().upsert_user(**user_data)

        return user


class DocumentList(views.APIView):
    """
        Driver's document list along with the status
    """

    permission_classes = (IsAuthenticated,)

    def get_document_event_spoc(self, identifier, document):
        support_number = None
        try:
            if identifier == DRIVER_DOCUMENT:
                doc_evs = DocumentEvent.objects.filter(
                            driver_document=document)
            else:
                doc_evs = DocumentEvent.objects.filter(
                            vehicle_document=document)

            if doc_evs.exists():
                de = doc_evs.latest('modified_date')
                if de.action_owner and de.action_owner.phone_number:
                    support_number = str(de.action_owner.phone_number.national_number)

        except Exception as e:
            pass

        return support_number

    def __get_spoc_number(self, driver, vendor):
        if driver:
            city_id = driver.operating_city
        elif vendor:
            vendor_data = vendor.preferred_locations.first()
            if vendor_data.exists():
                city_id = vendor_data.city_id
            else:
                city_id = 1

        spocs = Contract.objects.filter(city=city_id).exclude(spocs__email__isnull=True,
                            spocs__is_active=True).values_list('spocs__phone_number',flat = True)

        if spocs.count() == 0:
            spocs = City.objects.filter(id=city_id, process_associates__is_active=True). \
                        values_list('process_associates__phone_number',flat=True)

        spoc_count = spocs.count()
        number = spocs[random.randint(0, spoc_count - 1)]
        return number

    def get(self, request):
        document_list = []
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        vendor = request.user.vendor if hasattr(request.user, 'vendor') else None
        mandatory_driver_doc_type = DocumentType.objects.filter(
                                        is_mandatory_for_registration=True,
                                        identifier=DRIVER_DOCUMENT).values_list('code',
                                        flat = True)
        mandatory_vehicle_doc_type = DocumentType.objects.filter(
                                        is_mandatory_for_registration=True,
                                        identifier=VEHICLE_DOCUMENT).values_list('code',
                                        flat = True)

        if not driver and not vendor:
            return Response(status=status.HTTP_412_PRECONDITION_FAILED,
                            data='User is not a registered driver or vendor',
                            content_type='text/html; charset=utf-8')

        mapping_list = {}

        if driver:
            mapping_list[DRIVER_DOCUMENT] = mandatory_driver_doc_type
            mapping_list[VEHICLE_DOCUMENT] =  mandatory_vehicle_doc_type
        elif vendor:
            mapping_list[VENDOR_DOCUMENT] = VENDOR_MANDATORY_DOCUMENTS

        support_number_found = False
        for k,v in mapping_list.items():

            doclist = v
            if k == DRIVER_DOCUMENT:
                documents = DriverDocument.objects.filter(driver=driver.id)
            elif k == VEHICLE_DOCUMENT:
                vehicle_list = driver.vehicles.values()
                documents = None

                if vehicle_list.exists():
                    vehicle_id = vehicle_list[0].get('id')
                    documents = VehicleDocument.objects.filter(vehicle_id = vehicle_id)
            else:
                document = VendorDocument.objects.filter(vendor=vendor.id)

            for doc in doclist:
                dstatus = None
                if documents and documents.exists():
                    fdoc = documents.filter(document_type__code = doc)

                    if fdoc.exists():
                        d = fdoc.latest('created_date')
                        dstatus = d.status

                        if not support_number_found:
                            support_phone_number = self.get_document_event_spoc(k,d)
                            if support_phone_number:
                                support_number_found = True

                document_type = DocumentType.objects.get(code = doc)

                doc_data = {
                    'name' : document_type.name,
                    'code' : document_type.code,
                    'regex' : document_type.regex,
                    'error_message' : document_type.error_message,
                    'status' : dstatus,
                    'page_count' : document_type.no_of_pages,
                    'identifier' : document_type.identifier,
                    'text' : True
                }

                document_list.append(doc_data)

            if not support_number_found:
                support_phone_number = self.__get_spoc_number(driver,vendor)

        ddata = {'document_list': document_list,
                 'states': address_utils.get_state_list(),
                 'support_phone_number': support_phone_number}
        return Response(status=status.HTTP_200_OK, data=ddata,
                            content_type="text/html; charset=utf-8")


class DriverAck(generics.RetrieveAPIView):
    def post(self, request):
        data = request.data
        order_id= data.get('order_id')
        trip_id = data.get('trip_id')
        info = {
            "order_id": order_id if order_id and not order_id == 'null' else None,
            "driver": request.user.driver,
            "trip_id": trip_id if trip_id and not trip_id == 'null' else None,
            "is_accepted": data.get('is_accepted') == 'true'
        }
        DriverAcknowledgement.objects.create(**info)
        return Response(status=status.HTTP_200_OK, data='success')

class CallMasking(generics.RetrieveAPIView):
    def post(self, request):
        data = request.data
        order_id= data.get('order_id')
        user = request.user
        url = settings.EXOTEL_MASKING % (settings.EXOTEL_API_KEY, settings.EXOTEL_API_TOKEN, settings.EXOTEL_API_SID)
        order = Order.objects.filter(id=order_id).first()
        if not order:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order id')

        dropoff = order.shipping_address
        customer_number = str(dropoff.phone_number)
        if not customer_number:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Customer number Not available')
        data = {
            'From': str(user.phone_number),
            'To': customer_number,
            'CallerId': '08047090952',
            "StatusCallback": settings.HOST_URL+ '/api/exotel/call/update',
            "StatusCallbackEvent": "terminal",
            "StatusCallbackContentType": "application/json"
        }
        res = requests.post(url, data=data)
        data = json.loads(res.content)
        response = {
            "message": "Failed to make a call"
        }
        if res.status_code == status.HTTP_200_OK:
            call_details = data['Call']
            details = {
                "to_number": call_details['To'],
                "from_number": call_details['From'],
                "sid": call_details['Sid'],
                "caller_id": call_details['PhoneNumberSid'],
                "order": order,
                "driver": order.driver
            }
            CallMaskingLog.objects.create(**details)
            response = {
                "message": "you will get a callback soon"
            }

        return Response(status=status.HTTP_200_OK, data=response)


class PartnerEarnings(generics.RetrieveAPIView):
    """
    Returns driver Payments data.
    Permissions added.Only Active Drivers can access this api

    On Success: Will return 200 and the payments data as json
    On Failure: Will return 500 with the error.
    """

    permission_classes = (IsActiveDriver, HasValidSession)

    def get_payment_data(self, payment_records):
        data = []
        approved_sum = 0
        unapproved_sum = 0
        paid_sum = 0

        for record in payment_records:
            if record.status == APPROVED and record.is_settled:
                paid_sum += record.net_pay
                sequence = 3
            elif record.status == APPROVED and not record.is_settled:
                approved_sum += record.net_pay
                sequence = 2
            elif record.status == UNAPPROVED:
                unapproved_sum += record.net_pay
                sequence = 1

            trip_dict = []
            count = 0

            for paymenttrip in record.paymenttrip_set.all():
                trip = paymenttrip.trip
                if trip.customer_contract:
                    customer = trip.customer_contract.customer
                elif trip.order:
                    customer = trip.order.customer
                else:
                    customer = ''
                if trip.status in [TRIP_COMPLETED]:
                    count += 1
                    trip_dict.append({
                        'planned_start_time': trip.planned_start_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(settings.TIME_FORMAT),
                        'actual_start_time': trip.actual_start_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(
                            settings.TIME_FORMAT) if trip.actual_start_time else '',
                        'actual_end_time': trip.actual_end_time.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime(
                            settings.TIME_FORMAT) if trip.actual_end_time else '',
                        'total_time': str(minutes_to_dhms(trip.total_time)),
                        'total_distance': str(
                            round(trip.total_distance, 2)) + ' KM',
                        'area1': trip.hub.name if trip.hub else trip.order.pickup_address.line3
                        if trip.order and trip.order.pickup_address else '',
                        'area2': trip.order.shipping_address.line3
                        if trip.order and trip.order.shipping_address and not trip.hub else '',
                        'customer_name': str(customer),
                        'assigned': str(trip.assigned) if trip.assigned else '',
                        'attempted': str(trip.attempted) if trip.attempted else '',
                        'delivered': str(trip.delivered) if trip.delivered else '',
                        'rejected': str(trip.rejected) if trip.rejected else ''
                    })

            data.append({
                'id': record.id,
                'sequence': sequence,
                'customer': str(
                    record.contract.customer) if record.contract and record.contract.customer else '',
                'net_pay': record.net_pay,
                'payment_type': record.payment_type,
                'toll_charges': record.toll_charges,
                'labour_cost': record.labour_cost,
                'gross_pay': record.gross_pay,
                'start_datetime': record.start_datetime.strftime(settings.DATE_FORMATS),
                'end_datetime': record.end_datetime.strftime(settings.DATE_FORMATS),
                'trip_count': count,
                'trip_details': trip_dict
            })
        return data, paid_sum, approved_sum, unapproved_sum

    def get_payment_response(self, driver, vendor, driver_id, start, end, next):

        today = timezone.now().date()
        if not start:
            start = today - timedelta(days=60)
        if not end:
            end = today

        query = Q()
        if driver:
            query = Q(driver=driver)
        query = query & Q(status__in=[APPROVED, UNAPPROVED],
                          start_datetime__gte=start,
                          start_datetime__lt=end)

        response_data = {}
        if vendor:
            query = query & Q(owner_details=vendor) & Q(driver_id=driver_id)

        payment_records = DriverPayment.objects.filter(query).distinct()
        payment_records = payment_records.prefetch_related(
            'drivercashpaid_set', 'paymenttrip_set', 'paymenttrip_set__trip__hub',
            'paymenttrip_set__trip', 'paymenttrip_set__trip__order',
            'paymenttrip_set__trip__order__shipping_address',
            'paymenttrip_set__trip__order__customer',
            'paymenttrip_set__trip__order__pickup_address').order_by('-start_datetime').exclude(
            net_pay=0)

        payment_records = payment_records.select_related('contract', 'contract__customer')

        payment_record_serializer = common_serializer.PaginatedSerializer(
            queryset=payment_records,
            num=driver_constants.NUMBER_OF_PAYMENT_RECORDS_IN_PAGE,
            page=next
        )

        queryset = payment_record_serializer.data.pop('result')
        data, paid_sum, approved_sum, unapproved_sum = self.get_payment_data(
            queryset)

        response_data['data'] = data
        response_data['pagination_data'] = payment_record_serializer.data
        response_data['processed_amount'] = paid_sum
        response_data['due_for_payment'] = approved_sum
        response_data['unprocessed_amount'] = unapproved_sum

        return response_data

    def get(self, request):

        """GET method for getting payments data of driver."""

        data = request.GET.dict()
        is_vendor = True if data.get('is_vendor') == 'true' else False
        driver = request.user.driver if not is_vendor else ''
        vendor = request.user.vendor if is_vendor else ''
        driver_id = data.get('driver_id', None)
        start_date = data.get('start_date', None)
        end_date = data.get('end_date', None)

        start_date = datetime.datetime.strptime(start_date, '%d-%m-%Y') if start_date else None
        if start_date:
            start_date = pytz.timezone(settings.IST_TIME_ZONE).localize(
                datetime.datetime.combine(start_date, datetime.datetime.min.time())).astimezone(
                pytz.timezone(settings.TIME_ZONE))

        end_date = datetime.datetime.strptime(end_date, '%d-%m-%Y') if start_date else None
        if end_date:
            end_date = pytz.timezone(settings.IST_TIME_ZONE).localize(
                datetime.datetime.combine(end_date, datetime.datetime.min.time())).astimezone(
                pytz.timezone(settings.TIME_ZONE))

        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        response_data = {}
        status_code = status.HTTP_200_OK

        if (driver and driver.own_vehicle) or vendor:
            response_data = self.get_payment_response(driver, vendor, driver_id, start_date,
                                                      end_date, next)

        return Response(status=status_code, data=response_data,
                        content_type='text/html; charset=utf-8')

# Below api used for new app
class CommonDocumentUpload(generics.CreateAPIView):
    """
    Document upload from driver app.
    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        data = request.data.dict()
        logging.info(data)
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        vendor = request.user.vendor if hasattr(request.user, 'vendor') else None

        if not (driver or vendor):
            message = 'User is not a registered driver or fleet'
            return Response(status=status.HTTP_400_BAD_REQUEST, data = message)

        dp = DocumentProcessor(request.user, driver, vendor, data)
        status_code, message = dp.upload_partner_document()
        return Response(status=status_code, data = message)

class CommonDocumentList(generics.CreateAPIView):
    """
    Get Document List
    On Success: Will return 200 with success message
    On Failure: Will return 400 with error message.
    """

    permission_classes = (IsAuthenticated,)

    def get(self, request):
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        vendor = request.user.vendor if hasattr(request.user, 'vendor') else None
        print(request.user, driver.name, vendor)

        if not (driver or vendor):
            message = 'User is not a registered driver or fleet'
            return Response(status=status.HTTP_400_BAD_REQUEST, data = message)

        dp = DocumentProcessor(request.user, driver, vendor, {})
        total_acount, active_cont, document_list = dp.list_partner_documents(is_mandatory_for_registration=True)
        data = {
            'document_list' : document_list,
            'total_count' : total_acount,
            'active_count' : active_cont
        }
        return Response(status=status.HTTP_200_OK, data=data)


@permission_classes([])
class DeliveryWorkerLogin(generics.RetrieveAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        phone_number = data.get('phone_number')
        password = data.get('password')
        hub_qr_code = data.get('hub_qr_code')
        latitude = data.get('lat')
        longitude = data.get('lng')
        app_version = data.get('app_version')

        if not phone_number or not password:
            resp = {
                "message": 'Mobile number and password is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        hub = Hub.objects.filter(qr_code=hub_qr_code).first()
        if not hub:
            resp = {
                "message": 'Invalid Hub QR code'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        if settings.ACTIVE_COUNTRY_CODE not in phone_number:
            phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE,
                                       phone_number)
        user = User.objects.filter(phone_number=phone_number, is_staff=False).first()

        if user:
            if not user.is_active:
                resp = {
                    "message": 'User is Inactive, Please contact Admin'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

            login_response = User.objects.do_login(request=request, email=user.email, password=password)
            if login_response.status_code == status.HTTP_200_OK:
                user = login_response.data.get('user')
                token = login_response.data.get('token')
                response = get_login_response(user, token, hub)
                geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)
                DeliveryWorkerActivity.objects.create(user=user, hub=hub, activity_time=timezone.now(),
                                                     activity=driver_constants.LOGIN,
                                                     location=geopoint, app_version=app_version)
                return Response(status=status.HTTP_200_OK, data=response)
            resp = {"message": login_response.data}
            return Response(status=login_response.status_code, data=resp)

        resp = {"message": 'User Not Registered'}
        return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)


class DeliveryWorkerLogout(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        from blowhorn.common.notification import Notification
        token = request.META.get('HTTP_TOKEN', None)
        data = request.GET.dict()
        param_data = request.data
        hub_qr_code = param_data.get('hub_qr_code')
        latitude = param_data.get('lat')
        longitude = param_data.get('lng')
        app_version = param_data.get('app_version')

        hub = Hub.objects.filter(qr_code=hub_qr_code).first()

        if not hub:
            resp = {
                "message": 'Invalid Hub QR code'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        with transaction.atomic():
            geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)
            DeliveryWorkerActivity.objects.create(user=request.user, hub=hub, activity_time=timezone.now(),
                                                  activity=driver_constants.LOGOUT,
                                                  location=geopoint, app_version=app_version)
            Notification.remove_fcm_key_for_user(
                user=request.user, registration_id=data.get('fcm_id'))
            logout(request)
            UserSession.objects.remove_session_using_token(token)

        resp = {
            "message": 'Success'
        }
        return Response(status=status.HTTP_200_OK, data=resp)


class DeliveryWorkerActivityView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        token = request.META.get('HTTP_TOKEN', None)
        data = request.data
        user = request.user
        trip_number = data.get('trip_number')
        latitude = data.get('lat')
        longitude = data.get('lng')
        app_version = data.get('app_version')

        trip = Trip.objects.filter(trip_number=trip_number, status__in=[StatusPipeline.TRIP_NEW,
                                                                        StatusPipeline.TRIP_IN_PROGRESS,
                                                                        StatusPipeline.DRIVER_ACCEPTED]).first()
        if not trip:
            resp = {
                "message": 'Invalid Trip number'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        worker_activity_qs = DeliveryWorkerActivity.objects.filter(trip=trip)

        if worker_activity_qs.count() > 1:
            resp = {
                "message": 'There can be only 2 delivery workers per trip'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        worker_activity = worker_activity_qs.last()

        if worker_activity:
            delivery_worker = worker_activity.user
            logout_history = DeliveryWorkerActivity.objects.filter(user=delivery_worker,
                                                                   activity=driver_constants.LOGOUT,
                                                                   activity_time__gte=worker_activity.activity_time
                                                                   ).last()

            if not logout_history:
                resp = {
                    "message": 'Trip already has delivery worker'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)
        DeliveryWorkerActivity.objects.create(user=user, activity_time=timezone.now(),
                                              trip=trip, driver=trip.driver,
                                              activity=driver_constants.IN_TRIP,
                                              location=geopoint, app_version=app_version)
        resp = {
            "message": 'Success'
        }
        return Response(status=status.HTTP_200_OK, data=resp)

