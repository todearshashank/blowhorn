"""
Contains All the View Classes that are solely related to Trips.

-Trip Sync, Upcoming Trips, Past Trips
-Trip Start, Attempt Stop, Trip End, Cancellation of trip
-Trip Fields Validation, Trip Decision(Accept/Reject), Reordering Stops in Trip etc...

Created by: Gaurav Verma
"""
# Django and System libraries
import pytz
import logging
import traceback
from datetime import datetime, timedelta
from django.db.models import Q, F, ExpressionWrapper, DateTimeField, Prefetch, Sum
from django.utils import timezone
from django.conf import settings
from django.db.models.functions import (
    ExtractDay, ExtractMonth, ExtractYear,
    Cast
)

# 3rd party libraries
from rest_framework import generics, views
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from blowhorn.oscar.core.loading import get_model
from random import randint
from django.core.exceptions import ValidationError

# blowhorn imports
from blowhorn.apps.driver_app.v3.serializers.trip import (
    TripSyncSerializer, TripSerializer,
    CreateTripOnTheRunSerializer
)
from config.settings.status_pipelines import (
    DRIVER_ACCEPTED, TRIP_IN_PROGRESS,
    TRIP_NEW, TRIP_COMPLETED, TRIP_ALL_STOPS_DONE,
    TRIP_VEHICLE_LOAD
)
from blowhorn.common.helper import CommonHelper
from blowhorn.apps.driver_app.v3.helpers.trip import create_trip
from blowhorn.driver.permissions import IsActiveDriver
from blowhorn.apps.driver_app.v3.services.trip.trip_start import TripStartMixin
from blowhorn.apps.driver_app.v3.services.trip.attempt_stop import AttemptStopMixin
from blowhorn.apps.driver_app.v3.services.trip.attempt_pickup_return import PickupMixin
from blowhorn.apps.driver_app.v3.services.trip.attempt_hubstop import HubStopMixin
from blowhorn.apps.driver_app.v3.services.trip.trip_end import TripEndMixin
from blowhorn.apps.driver_app.v3.services.trip.stop_reorder import ReorderStops
from blowhorn.apps.driver_app.v3.services.trip.send_sms import SendSMS
from blowhorn.apps.driver_app.v3.services.trip.decision import TripDecision
from blowhorn.apps.driver_app.v3.services.trip.cancel import TripCancel
from blowhorn.apps.driver_app.v3.services.trip.complete import TripComplete
from blowhorn.apps.driver_app.v3.services.trip.trip_validation import TripValidation
from blowhorn.driver import constants as driver_constants
from blowhorn.common import serializers as common_serializer
from blowhorn.contract.constants import (
    CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED,
)
from blowhorn.apps.driver_app.utils import _get_api_reponse
from blowhorn.apps.driver_app.v3.helpers.driver import create_error_object, statuspipeline_check
from blowhorn.users.permission import HasValidSession


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
Driver = get_model('driver', 'Driver')
PartnerFeedback = get_model('trip','PartnerFeedback')


class TripSync(generics.CreateAPIView):
    """
    Sends Latest Trip Data to the driver app.

    Permissions added.Only Active Drivers can access this api
    POST(user, driver_location_data)

    On Success: Trip Data
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = TripSyncSerializer

    def post(self, request):
        """Return the trip response for the driver to resume to its latest state."""
        # TODO create app event
        self.driver = self.request.user.driver
        data = request.POST.dict()
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.context = {'request': request}
        error_message = ''
        response_data = {}
        status_code = status.HTTP_200_OK
        trip = self.__get_queryset().first()
        if trip:
            try:
                response_data = self.serializer_class(
                    instance=trip, context=self.context).data

            except BaseException as e:
                error_message = ('Error: %s, Call Support' % e)
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                logger.error(traceback.format_exc())
                create_error_object(self.driver, traceback.format_exc(),
                                    self.request.build_absolute_uri(),
                                    current_location=self.geopoint,
                                    trip=trip)
        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)

    def __get_queryset(self):
        # TODO look into it again
        query_params = Q(driver=self.driver,
                         status__in=[TRIP_IN_PROGRESS, DRIVER_ACCEPTED,
                                     TRIP_VEHICLE_LOAD, TRIP_ALL_STOPS_DONE])
        return Trip.objects.prefetch_queryset(
            query=query_params,
            select_related=['hub', 'hub__address'],
            prefetch_related=['stops', ]
        )


class UpcomingTrips(generics.ListAPIView):
    """
    Sends upcoming Trips Data to the driver app.

    Permissions added.Only Active Drivers can access this api
    GET(user, driver_location_data)

    On Success: Upcoming Trips Data
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = TripSyncSerializer
    ordering_fields = ('planned_start_time',)

    def get(self, request):
        """Return the upcoming trip response to the driver app for accept/reject."""
        # TODO change to POST method because of driver error entry
        data = request.POST.dict()
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

        context = {'request': request}
        error_message = ''
        response_data = {}
        status_code = status.HTTP_200_OK
        self.driver = self.request.user.driver

        try:
            trip_qs = self.__get_queryset()
            response_data = self.serializer_class(trip_qs, many=True, context=context).data

        except BaseException as e:
            error_message = ('Error: %s, Call Support' % e)
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint)
        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)

    def __get_queryset(self):
        trip_qs = self.__get_query()
        trip_qs = trip_qs.prefetch_related(
            Prefetch(
                'stops',
                queryset=Stop.objects.all().select_related(
                    'order', 'waypoint__shipping_address', 'order__shipping_address'
                )
            )
        ).select_related(
            'driver', 'order', 'hub', 'hub__address', 'trip_workflow',
            'customer_contract', 'order__customer', 'order__customer__user',
            'customer_contract__customer', 'customer_contract__customer__user',
            'order__shipping_address'
        )
        trip_qs = trip_qs.filter(trip_workflow__isnull=False)
        trip_qs = trip_qs.prefetch_related('stops')
        trip_qs = trip_qs.select_related('order__customer', 'customer_contract',
                                         'customer_contract__customer')

        return trip_qs

    # TODO needs changes
    def __get_query(self):
        completed_trips_without_rating = self.driver.completed_trips_without_rating()
        if completed_trips_without_rating.exists():
            return completed_trips_without_rating

        assigned_trips = self.driver.get_assigned_trips()
        if assigned_trips.exists():
            return assigned_trips

        new_trips = self.driver.get_new_trips()
        if new_trips.exists():
            return new_trips

        # TODO Payment Screen Sync in upcoming trips
        # pending_payment_c2c_trips = self.driver.get_pending_payment_c2c_trips()
        # if pending_payment_c2c_trips.exists():
        #     return pending_payment_c2c_trips

        return self.__get_todays_trips()

    def __get_todays_trips(self):
        '''
            This method gets the current day trips for the driver:
            Contract Type: Spot and Fixed order
            Trip time lies between supposed start time and supposed end time
            (supposed_starttime = planned start time - contract_before_hours)
            (supposed_endtime = planned_start_time + contract_after_hours)
        '''

        local_tz = pytz.timezone(settings.IST_TIME_ZONE)
        todays_date = timezone.now().astimezone(local_tz).date()

        return Trip.objects.annotate(
            scheduled_day=ExtractDay('planned_start_time', tzinfo=local_tz),
            scheduled_month=ExtractMonth('planned_start_time', tzinfo=local_tz),
            scheduled_year=ExtractYear('planned_start_time', tzinfo=local_tz),
            current_time=Cast(timezone.now(), output_field=DateTimeField()),
            supposed_start_time=ExpressionWrapper(
                F('planned_start_time') - timedelta(hours=1) * F('customer_contract__hours_before_scheduled_at'),
                output_field=DateTimeField()
            ),
            supposed_end_time=ExpressionWrapper(
                F('planned_start_time') + timedelta(hours=1) * F('customer_contract__hours_after_scheduled_at'),
                output_field=DateTimeField()
            ),

        ).filter(
            driver=self.driver, status=TRIP_NEW,
            scheduled_day=todays_date.day,
            scheduled_month=todays_date.month,
            scheduled_year=todays_date.year,
            customer_contract__contract_type__in=[CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED],
            current_time__gte=F('supposed_start_time'),
            current_time__lte=F('supposed_end_time')
        )


class PastTrips(generics.RetrieveAPIView):
    """
    Sends past Trips Data to the driver app.

    Permissions added.Only Active Drivers can access this api
    GET(user, driver_location_data)

    On Success: Past Trips Data
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = TripSyncSerializer
    ordering_fields = ('-planned_start_time',)

    def get(self, request):
        """Return the past trip response to the driver app for accept/reject."""
        # TODO change to POST method because of driver error entry
        data = request.GET.dict()
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        error_message = ''
        response_data = ''
        status_code = status.HTTP_200_OK
        self.is_vendor = True if data.get('is_vendor') == 'true' else False
        self.driver = self.request.user.driver if not self.is_vendor else Driver.objects.get(id=data.get('driver_id'))
        self.vendor =  self.request.user.vendor if self.is_vendor else None
        self.driver_id = data.get('driver_id', None)
        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        trip_qs = self.__get_queryset()
        try:
            trip_sync_serializer = common_serializer.PaginatedSerializer(
                queryset=trip_qs,
                num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                page=next,
                serializer_method=self.serializer_class,
                context={'request': request},
                past_trips=True)
            response_data = trip_sync_serializer.data
        except BaseException as e:
            error_message = ('Error: %s, Call Support' % e)
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint)

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)

    def __get_queryset(self):
        trip_qs = self.__get_query()
        trip_qs = trip_qs.prefetch_related('stops')
        trip_qs = trip_qs.select_related('order__customer', 'customer_contract',
                                         'customer_contract__customer')
        return trip_qs.order_by('-planned_start_time')

    def __get_query(self):
        query = Q(driver=self.driver, status=TRIP_COMPLETED)
        if self.is_vendor:
            query = query & Q(driver_id=self.driver_id)

        return Trip.objects.filter(query)


class Cancel(generics.UpdateAPIView, TripCancel):
    """
    Cancels the trip that is in-progress.

    Trip and its stops are marked as Cancelled.
    SMS will be sent to the spocs for the trip contract.

    Permissions added.Only Active Drivers can access this api
    POST(trip_id, driver_location_data)

    On Success: {
                'is_all_stops_done': True,
                'current_step': -1,
                'is_trip_in_progress': False,
                'cost_components': [],
                'is_loaded': True,
                'next_stop_id': ''
            }
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = TripSerializer

    def post(self, request, pk):
        """POST method for cancellation of trip changes."""
        data = request.POST.dict()
        self._initialize_attributes(data, pk)

        try:
            self._cancel_trip()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, order=self.trip.order if self.trip.order else None)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class TripFieldsValidation(generics.UpdateAPIView, TripValidation):
    """
    Sends Min and max values of Trip fields to the app for validation.

    Permissions added.Only Active Drivers can access this api
    POST(screen_name, driver_location_data)

    On Success:
        {
            'validation': {
                'meter_reading_end': {
                    'min': 10,
                    'max': 100
                },
                'delivered': {
                    'min': 10,
                    'max': 100
                }
            },
            'display': {
                'meter_reading_start': 100,
                'assigned': 50
            }
        }
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = TripSerializer

    def post(self, request, trip_id):
        """Send validations and display data to the driver app."""
        data = request.POST.dict()
        self._initialize_attributes(data, trip_id)

        try:
            self._get_trip_validations()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, order=self.trip.order if self.trip.order else None)
        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class TripStart(generics.ListCreateAPIView, TripStartMixin):
    """
    Trip Start Changes are done here.

    will be called when driver is on 2 screens:
    - Start Trip
        -Update trip to in-progress
        -Update trip fields mentioned in the trip workflow

        If there is No Finshed Loading Screen:
            -Update the first stop to In Progress
            -Update the distinct orders(single for b2b/c2c and multiple for shipment trips) to out for delivery
        else:
            - Update distinct orders status to At-Pickup
    - Finished Loading
        -Update Trip Pickup_loading_timestamp
        -Update the first stop to In Progress
        -Update the distinct orders(single for b2b/c2c and multiple for shipment trips) to out for delivery

    Permissions added.Only Active Drivers can access this api
    POST(
        'trip_id',
        'location_details',
        trip_last_response:{
            'is_all_stops_done': False,
            'current_step': 1,
            'is_trip_in_progress': True,
            'is_loaded': True,
            'next_stop_id': ''
        },
        trip_data: {
            'meter_reading_start': 100,
            'assigned': 50
        },
        'confirmation_screen': 'true'/'false'(
            to differentiate whether it is start trip or finished loading screen
        )
    )

    On Success:
        {
            'is_all_stops_done': False,
            'current_step': trip.current_step,
            'is_trip_in_progress': True,
            'is_loaded': True if trip.pickup_loading_timestamp,
            'next_stop_id': next inprogress stop id that is made
        }
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)

    @statuspipeline_check
    def post(self, request, pk):
        """Post method for trip start changes."""
        data = request.POST.dict()
        self._initialize_attributes(data, pk)

        try:
            self._start_trip()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, stop=self.stop,
                                order=self.stop.order if self.stop else self.trip.order)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class AttemptStop(generics.ListCreateAPIView, AttemptStopMixin):
    """
    Change to be done on Trip Stop that is In-Progress.

    will be called when driver is on 2 screens:
    - Reached Stop
        -Update Trip Status to All-Stops-Done if Last Stop
        -Update Trip Current Step value
        -Update the Current In-Progress Stop to Completed
        -Update the Next Stop in New Status to In-Progress

        If there is No Finished Work Screen:
            -Update Trip Status to All-Stops-Done if Last Stop
            -Update Trip Current Step value
            -Update the Current In-Progress Stop to Completed
            -Update Reach Time for Current Stop
            -Update the Next Stop in New Status to In-Progress
        else:
            -Update Reach Time for Current Stop
            -Update Trip Current Step value
    - Finished Work
        -Update Trip Status to All-Stops-Done if Last Stop
        -Update Trip Current Step value
        -Update the Current In-Progress Stop to Completed
        -Update the Next Stop in New Status to In-Progress

    If 'action' is sent in the Input Data:
        - Update Order status == data.get('action')
        - Update Trip Packages Field accordingly

    Permissions added.Only Active Drivers can access this api
    POST(
        'trip_id', 'stop_id',
        'location_details',
        trip_last_response:{
            'is_all_stops_done': False,
            'current_step': 1,
            'is_trip_in_progress': True,
            'is_loaded': True,
            'next_stop_id': ''
        },
        trip_data: {
            'meter_reading_start': 100,
            'assigned': 50
        },
        'confirmation_screen': 'true'/'false'(
            to differentiate whether it is start trip or finished loading screen
        ),
        'action': 'Delivered'/'Unable-To-Deliver'/'Returned'(
            order statuses sent from app as it is stored that we store in db
            PS. see status_pipelines.py
        ),
        'payment_type': 'Cash'/'Online',
        'cash_collected', 'delivered_to', 'undelivered_reason'
    )

    On Success:
        {
            'is_all_stops_done': trip.is_all_stops_done(),
            'current_step': trip.current_step,
            'is_trip_in_progress': True,
            'is_loaded': True,
            'next_stop_id': next inprogress stop id that is made if present else ''
        }
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)

    # TODO need to add description
    @statuspipeline_check
    def post(self, request, trip_id, stop_id):
        """Change to be done on Trip Stop that is In-Progress."""
        data = request.POST.dict()
        self._initialize_attributes(data, trip_id, stop_id)

        try:
            self._attempt_stop()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, stop=self.stop,
                                order=self.order)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class AttemptPickupStop(generics.ListAPIView, PickupMixin):

    def post(self, request, trip_id):
        data = request.POST.dict()
        self._initialize_attributes(data, trip_id)

        try:
            self._attempt_pickup()
        except BaseException as e:
            print(e)
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, stop=self.stop
                                )

        return Response(status=self.status_code,
                        data=self.error_message or self.response_data)


class AttemptHubStop(generics.ListAPIView, HubStopMixin):

    def post(self, request, trip_id, stop_id):
        data = request.POST.dict()
        self._initialize_attributes(data, trip_id, stop_id)

        try:
            self._attempt_hubstop()
        except BaseException as e:
            print(e)
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, stop=self.stop
                                )

        return Response(status=self.status_code,
                        data=self.error_message or self.response_data)


class TripEnd(generics.ListCreateAPIView, TripEndMixin):
    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)

    # TODO need to add
    @statuspipeline_check
    def post(self, request, pk):
        data = request.POST.dict()
        self._initialize_attributes(data, pk)

        try:
            self._end_trip()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, stop=self.last_stop,
                                order=self.order)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class CreateTripOnTheRun(generics.ListCreateAPIView):
    """Creates trip with meter reading start value."""

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = CreateTripOnTheRunSerializer

    def post(self, request):
        """Create trip with meter reading start value."""
        # Disabled creation of empty trip from backend
        return Response(
            status=status.HTTP_400_BAD_REQUEST,
            data='Disabled creation of trip from app.'
        )
        data = request.POST.dict()
        location = data.get('location_details', None)
        geopoint = CommonHelper.get_geopoint_from_location_json(location)
        driver = request.user.driver
        meter_reading_start = int(request.POST.get('meter_reading', 0))

        trip = create_trip(
            driver_id=driver.id, blowhorn_contract_id=driver.contract.id if driver.contract else None,
            status=TRIP_IN_PROGRESS, meter_reading_start=meter_reading_start)
        if not trip:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data='Something went wrong.')

        context = {'geopoint': geopoint}
        response = self.serializer_class(instance=trip, context=context).data

        logger.info(
            'Trip "%s" creation Successful by driver:- "%s"', trip, driver)
        return Response(response, status=status.HTTP_200_OK)


class StopReorder(generics.ListCreateAPIView, ReorderStops):
    """
    Reorder the Trip Stops.

    Makes current In-Progress Stop to New.
    Makes the Stop sent from app from New to In-Progress.

    Permissions added.Only Active Drivers can access this api
    POST(trip_id, stop_id, Location_details)

    On Success: {"stop_id": id of the stop which is made In-Progress}
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)

    # TODO need to add description
    def post(self, request, trip_id, stop_id):
        """Make changes in the stops statuses keeping only one stop in In-Progress status."""
        data = request.data.dict()
        self._initialize_params(trip_id, stop_id, data)

        try:
            self._reorder_stops()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, stop=self.target_stop,
                                order=self.target_stop.order)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Decision(generics.ListCreateAPIView, TripDecision):
    """
    Accept/Reject Trip by driver from the home screen.

    Permissions added.Only Active Drivers can access this api
    POST(action('accept'/'reject'), trip_id, Location_details)

    On Success:
        -If Driver Accepts:
            - Trip changed to Accepted status
            - Order changed to Accepted status

            Returns Created Trip Data
        -If Driver Rejects:
            - Trip changed to Suspended status
            - All Stops status changed to Suspended status
            - SMS sent to spocs of the contract for which the trip belongs.

            Returns 'Driver Rejected' and accept/reject notification will be sent to the next driver.
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = TripSyncSerializer

    @statuspipeline_check
    def post(self, request, trip_id):
        """POST method for Accept/Reject decision for trip by driver."""
        data = request.POST.dict()
        self._initialize_attributes(data, trip_id)

        try:
            self._make_decision()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip,
                                order=self.trip.order)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class Complete(generics.UpdateAPIView, TripComplete):
    """
    Completes the trip that is in-progress.

    Trip and its stops are marked as Completed.

    Permissions added.Only Active Drivers can access this api
    POST(trip_id, driver_location_data)

    On Success: {
                'is_all_stops_done': True,
                'current_step': -1,
                'is_trip_in_progress': False,
                'cost_components': [],
                'is_loaded': True,
                'next_stop_id': ''
            }
    On Failure: Returns 500 with the error.

    """

    permission_classes = (IsAuthenticated, IsActiveDriver, HasValidSession)
    serializer_class = TripSerializer

    def post(self, request, pk):
        """POST method for completion of trip changes."""
        data = request.POST.dict()
        self._initialize_attributes(data, pk)

        try:
            self._complete_trip()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint,
                                trip=self.trip, order=self.trip.order if self.trip.order else None)

        # TODO need to be removed
        return Response(status=self.status_code, data=self.error_message or self.response_data)

        return _get_api_reponse(self.status_code, self.error_message, self.response_data)


class SendOtp(views.APIView):
    """
    API to send SMS to end customer/store contact
    """

    def post(self, request, trip_id):
        data = request.POST.dict()

        stop_id = data.get('stop_id')
        location = data.get('location_details', None)
        geopoint = CommonHelper.get_geopoint_from_location_json(location)

        if stop_id:
            # send OTP for order delivery
            try:
                stop = Stop.objects.select_related('order').get(pk=stop_id)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='invalid stop data')

            error_msg, otp = SendSMS().send_message_for_order_event(stop.order,
                                                                    stop.order.status)

            if error_msg:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=error_msg)
            _response = {
                'otp': otp,
                'message': 'OTP sent successfully'
            }
            return Response(status=status.HTTP_200_OK, data=_response)

        # Since stop id is not there, send OTP for end trip
        if not geopoint:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Location geopoint not available')

        error_msg, otp = SendSMS().send_otp_for_end_trip(trip_id, geopoint,
                                                         request)

        if error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=error_msg)

        _response = {
            'otp': otp,
            'message': 'OTP sent successfully'
        }

        return Response(status=status.HTTP_200_OK, data=_response)


class PartnerTrips(generics.RetrieveAPIView):
    """
    Sends past Trips Data to the driver app.

    Permissions added.Only Active Drivers can access this api
    GET(user, driver_location_data)
    On Success: Past Trips Data
    On Failure: Returns 500 with the error.
    """

    permission_classes = (IsAuthenticated, IsActiveDriver,)
    serializer_class = TripSyncSerializer
    ordering_fields = ('-planned_start_time',)

    def get(self, request):
        """Return the past trip response to the driver app for accept/reject."""
        # TODO change to POST method because of driver error entry
        data = request.GET.dict()
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        error_message = ''
        response_data = ''
        status_code = status.HTTP_200_OK

        self.is_vendor = True if data.get('is_vendor') == 'true' else False
        self.driver = self.request.user.driver if not self.is_vendor else Driver.objects.get(
            id=data.get('driver_id'))
        self.driver_id = data.get('driver_id', None)
        self.vendor = self.request.user.vendor if self.is_vendor else None

        start_date = data.get('start_date', None)
        end_date = data.get('end_date', None)

        start_date = datetime.strptime(start_date, '%d-%m-%Y') if start_date else None
        if start_date:
            start_date = pytz.timezone(settings.IST_TIME_ZONE).localize(
                datetime.combine(start_date, datetime.min.time())).astimezone(
                pytz.timezone(settings.TIME_ZONE))

        end_date = datetime.strptime(end_date, '%d-%m-%Y') if start_date else None
        if end_date:
            end_date = pytz.timezone(settings.IST_TIME_ZONE).localize(
                datetime.combine(end_date, datetime.min.time())).astimezone(
                pytz.timezone(settings.TIME_ZONE))

        next = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        trip_qs, trip_params = self.__get_queryset(start_date, end_date)
        try:
            trip_sync_serializer = common_serializer.PaginatedSerializer(
                queryset=trip_qs,
                num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                page=next,
                serializer_method=self.serializer_class,
                context={'request': request},
                past_trips=True)

            response_data = trip_sync_serializer.data
            response_data = {
                'trips': response_data,
                'trip_params': trip_params
            }

        except BaseException as e:
            error_message = ('Error: %s, Call Support' % e)
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            logger.error(traceback.format_exc())
            create_error_object(self.driver, traceback.format_exc(),
                                self.request.build_absolute_uri(),
                                current_location=self.geopoint)

        # TODO need to be removed
        return Response(status=status_code, data=error_message or response_data)

        return _get_api_reponse(status_code, error_message, response_data)

    def __get_queryset(self, start_date, end_date):
        trip_qs = self.__get_query(start_date, end_date)
        trip_params = trip_qs.aggregate(total_time=Sum('total_time'),
                                        total_distance=Sum('total_distance'))
        trip_qs = trip_qs.prefetch_related('stops')
        trip_qs = trip_qs.select_related('order__customer', 'customer_contract',
                                         'customer_contract__customer')
        return trip_qs.order_by('-planned_start_time'), trip_params

    def __get_query(self, start_date, end_date):
        query = Q(driver=self.driver, status=TRIP_COMPLETED)
        if self.is_vendor:
            query = query & Q(driver_id=self.driver_id)
        if start_date and end_date:
            query = query & Q(actual_end_time__gte=start_date, actual_end_time__lt=end_date)
        return Trip.objects.filter(query)


class TripFeedback(generics.CreateAPIView):
    """
    Saves the trip feed back from user
    """
    permission_classes = (IsAuthenticated,IsActiveDriver)

    def post(self, request, trip_id):
        ''' Save the trip feedback'''
        data = request.data
        driver = request.user.driver if hasattr(request.user, 'driver') else None

        feedback = data.get('feedback',None)
        feedback_dict = {}
        trip = Trip.objects.filter(id=trip_id).first()

        if trip and feedback:
            feedback_dict = {'trip' : trip,
                                'event_time' : timezone.now(),
                                'feedback' : feedback,
                                'driver' : driver
                            }

            ob = PartnerFeedback(**feedback_dict)
            ob.save()

        return Response(status=status.HTTP_200_OK, data={})
