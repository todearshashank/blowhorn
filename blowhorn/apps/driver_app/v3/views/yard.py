from django.db import transaction
from django.utils import timezone
from rest_framework import generics, exceptions, status, views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.address.models import Hub
from blowhorn.apps.driver_app.v3.serializers.address import HubSerializer
from blowhorn.driver.permissions import IsActiveDriver
from blowhorn.wms.submodels.yard import YardActivity, Yard


class YardActivities(views.APIView):
    permission_classes = (IsAuthenticated, IsActiveDriver)

    def post(self, request):
        data = request.GET.dict()
        qr_code = data.get('qr_code')
        hub = Hub.objects.filter(qr_code=qr_code).first()

        if not hub or (hub and not hub.is_wms_site):
            response = {'message': 'Invalid qrcode'}
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        if not driver:
            response = {'message': 'Driver not found'}
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        with transaction.atomic():
            yard = Yard.objects.filter(driver=driver, hub=hub)
            if yard:
                check_in = False
                Yard.objects.filter(driver=driver, hub=hub).delete()
                YardActivity.objects.filter(driver=driver, hub=hub).update(check_out=timezone.now())
            else:
                check_in = True
                Yard.objects.create(driver=driver, hub=hub)
                YardActivity.objects.create(driver=driver, hub=hub, check_in=timezone.now())
            hub_data = HubSerializer(hub).data
            resp = {
                "check_in": check_in,
                "hub": hub_data

            }
        return Response(status=status.HTTP_200_OK, data=resp)
