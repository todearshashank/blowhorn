# Django and System libraries

# 3rd party libraries
from rest_framework import serializers
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.address.models import GenericAddress as Address

Hub = get_model('address', 'Hub')


class AddressSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        self.name = kwargs.pop('name') if kwargs.get('name', None) else ''
        super(AddressSerializer, self).__init__(*args, **kwargs)

    def get_first_name(self, address):
        return self.name if self.name else address.first_name

    class Meta:
        model = Address
        fields = '__all__'


class HubSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(source='get_name')
    address = serializers.SerializerMethodField()

    def get_address(self, hub):
        # hub will be order hub for shipment order and
        # for kiosk we are passing pikupaddress as hub
        #to handle app crash from backend
        address = hub.address if isinstance(hub, Hub) else hub
        if address:
            return AddressSerializer(instance=address).data
        return None

    class Meta:
        model = Hub
        fields = ('id', 'name', 'address', 'qr_code')

    def get_name(self, hub):
        return str(hub)
