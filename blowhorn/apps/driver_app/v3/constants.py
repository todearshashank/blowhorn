from blowhorn.trip.constants import HUB_DELIVERY, ORDER_DELIVERY, PICKUP, \
    ORDER_RTO, HUB_RTO
from config.settings.status_pipelines import ORDER_MOVING_TO_HUB, \
    ORDER_OUT_FOR_PICKUP, OUT_FOR_DELIVERY, ORDER_OUT_FOR_RTO, \
    ORDER_MOVING_TO_HUB_RTO
from blowhorn.integration_apps.invoid.constants import (DRIVER_INVOID_MANDATORY_DOCS,
                                                        VEHICLE_INVOID_MANDATORY_DOCS)
from blowhorn.document.constants import DOCUMENT_COVID_PASS
from blowhorn.driver.constants import YES_NO, OKAY
"""Constants used in the driver_app application to be define here."""
ON_TRIP_LOCATION_UPDATE_INTERVAL = 15000  # in milliseconds
ON_TRIP_LOCATION_UPDATE_FAST_INTERVAL = 10000  # in milliseconds
OFF_TRIP_LOCATION_UPDATE_INTERVAL = 60000  # in milliseconds
ALARM_LOCATION_INTERVAL = 60000  # in milliseconds
OFFLINE_LOCATION_UPDATE_INTERVAL = 60000  # in milliseconds

ORDER_STATUS_BY_STOPS = {
    HUB_DELIVERY: ORDER_MOVING_TO_HUB,
    ORDER_DELIVERY: OUT_FOR_DELIVERY,
    PICKUP: ORDER_OUT_FOR_PICKUP,
    ORDER_RTO: ORDER_OUT_FOR_RTO,
    HUB_RTO: ORDER_MOVING_TO_HUB_RTO
}

APP_DRIVER_MANDATORY_DOCS = DRIVER_INVOID_MANDATORY_DOCS + [DOCUMENT_COVID_PASS,]
APP_VEHICLE_MANDATORY_DOCS =  VEHICLE_INVOID_MANDATORY_DOCS
DL_VALID_OCR_STATES = ['Delhi','Karnataka', 'Madhya Pradesh',
       'Rajasthan', 'Punjab', 'Bihar' ]

"""Driver Notification constants"""
INPUT_BUTTONS_MAPPING = {
    YES_NO: {
        'positive': 'Yes',
        'negative': 'No'
    },
    OKAY: {
        'positive': 'Okay',
        'negative': ''
    }
}
