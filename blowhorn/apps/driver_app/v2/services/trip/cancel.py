"""
Cancels the trip that is in-progress.

Trip and its stops are marked as Cancelled.
SMS will be sent to the spocs for the trip contract.

Input Data: trip_id, driver_location_data

Response: {
            'is_all_stops_done': True,
            'current_step': -1,
            'is_trip_in_progress': False,
            'cost_components': [],
            'is_loaded': True,
            'next_stop_id': ''
        }

Created By: Gaurav Verma
"""
# Django and System libraries
import json
import logging
from django.utils import timezone
from django.conf import settings

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.driver_app.v2.serializers.trip import TripSerializer
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import (
    TRIP_COMPLETED, TRIP_CANCELLED,
    TRIP_IN_PROGRESS, TRIP_ALL_STOPS_DONE, DRIVER_ACCEPTED
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.common.tasks import send_sms
from blowhorn.common import sms_templates
from blowhorn.apps.driver_app.v1.helpers.driver import _get_support_number_from_contract
from blowhorn.apps.driver_app.tasks import send_unallocated_orders_slack_notification


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')


class TripCancel(object):
    """cancellation of trip changes done here."""

    def _initialize_attributes(self, data, pk):
        """
        Parse the input params.

        Any validation error on input params if required, should be raised here.
        """
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.driver = self.request.user.driver

        self.trip = Trip.objects.filter(
            pk=pk,
            status__in=[DRIVER_ACCEPTED, TRIP_IN_PROGRESS, TRIP_ALL_STOPS_DONE]
        ).first()
        if not self.trip:
            self.error_message = 'Invalid Trip.'
        try:
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data !'

        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

        self.app_name = 'Unified App'
        self.app_version = data.get('app_version', '')
        self.reason = data.get('reason', None)

    def __make_updates(self):
        Stop.objects.filter(trip=self.trip).exclude(
            status=TRIP_COMPLETED).update(
                end_time=timezone.now(),
                status=TRIP_CANCELLED
        )

        self.trip_data['status'] = TRIP_CANCELLED
        self.trip_data['current_step'] = -1

    def __save_updates(self):
        if self.trip_data:
            self.trip_data['app_name'] = self.app_name
            self.trip_data['app_version'] = self.app_version
            self.trip = UpdateModelMixin().update(data=self.trip_data, instance=self.trip,
                                                  serializer_class=TripSerializer)
            self.trip.save()

    def __send_slack_notification(self):
        orders = self.trip._get_orders_from_trip()
        order = orders[0] if orders else None
        if order and self.trip:
            data = {
                'message': 'Trip Cancelled !!',
                'order_type': order.order_type,
                'order_id': order.id,
                'order_number': order.number,
                'trip_number': self.trip.trip_number,
                'hub_name': str(self.trip.hub),
                'customer_name': str(order.customer),
                'contract_name': str(self.trip.customer_contract),
                'driver_details': '%s | %s' % (str(self.driver),
                                               self.request.user.phone_number),
                'reason': self.reason,
                'track_url_host': self.request.META.get('HTTP_HOST',
                                                        'blowhorn.com')
            }
            send_unallocated_orders_slack_notification.apply_async((data,), )

    def __send_sms_to_contract_spocs(self):
        if not settings.ENABLE_TRIP_UPDATION_MESSAGE:
            return
        contract_id = self.trip.customer_contract.id if self.trip.customer_contract else None
        support_numbers = _get_support_number_from_contract(contract_id)

        for item in support_numbers:
            phone_number = item['spoc_mobile']
            
            if self.trip.hub:
                template_dict = sms_templates.trip_cancelled_hub
                message = template_dict['text'] % (self.driver, self.trip, self.trip.hub)
            else:
                template_dict = sms_templates.trip_cancelled
                message = template_dict['text'] % (self.driver, self.trip)

            template_json = json.dumps(template_dict)
            send_sms.apply_async(args=[phone_number, message, template_json])

    def _cancel_trip(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return
        self.__make_updates()
        self.__save_updates()
        self.__send_sms_to_contract_spocs()
        self.__send_slack_notification()
        self.response_data = {
            'is_all_stops_done': True,
            'current_step': self.trip.current_step,
            'is_trip_in_progress': False,
            'cost_components': [],
            'is_loaded': True,
            'next_stop_id': ''
        }
