"""
Input data: screen_name, location_details.

Sends Min and max values of Trip fields to the app for validation.

Response:
        {
            'validation': {
                'meter_reading_end': {
                    'min': 10,
                    'max': 100
                },
                'delivered': {
                    'min': 10,
                    'max': -1 # if negative value then app will not apply the validation
                }
            },
            'display': {
                'name': meter_reading_start',
                'value': 100,
                'can_edit': True/False,
                'display': 'Opening Kms'
            }
        }

Created by: Gaurav Verma
"""
# Django and System libraries
import logging
from django.utils import timezone
from datetime import timedelta
from django.db.models import Q, Prefetch
from django.conf import settings

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.common.helper import CommonHelper
from blowhorn.utils.functions import utc_to_ist
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, SCREEN_FIELD_NAME, CONTRACT_TYPE_SHIPMENT
from blowhorn.trip.constants import AVG_SPEED_OF_VEHICLE_KMS

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
Screen = get_model('contract', 'Screen')


class TripValidation(object):
    """Trip fields validation data(min and max values) and display data to be sent."""

    def _initialize_attributes(self, data, pk):
        """
        Parse the input params.

        Any validation error on input params if required, should be raised here.
        """
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.driver = self.request.user.driver

        current_screen_name = data.get('screen_name')
        query = Q(
            pk=pk,
            current_step__gte=0
        )
        select_related = ['trip_workflow']
        prefetch_related = [Prefetch(
            'trip_workflow__screens',
            queryset=Screen.objects.filter(name=current_screen_name).prefetch_related('input_fields'),
            to_attr='current_screen'
        ), ]

        self.trip = Trip.objects.prefetch_queryset(
            query=query,
            select_related=select_related,
            prefetch_related=prefetch_related
        ).first()
        if not self.trip:
            self.error_message = 'Invalid Trip. Sync the App'
            return

        self.current_screen = self.trip.trip_workflow.current_screen[0]
        self.input_fields = self.current_screen.input_fields.values_list('name', flat=True)

        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

    def _get_trip_validations(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        self.response_data = {
            'validation': self.__get_validation_data(),
            'display': {},
            'display_list': self.__get_display_data()
        }
        print(self.response_data)

    def __get_validation_data(self):
        trip_validations = {}

        if 'meter_reading_start' in self.input_fields:
            # TODO need to change value to last trip end meter reading for min value of meter reading start
            min_meter_reading = self.trip.vehicle.last_meter_reading or 0
            trip_validations['meter_reading_start'] = {
                'min': min_meter_reading,
                'min_msg': 'Meter reading cannot be lesser than last closing meter reading %d' % min_meter_reading,
                'max': None  # TODO change to -1
            }

        if 'meter_reading_end' in self.input_fields:
            # TODO need to change value to last trip end meter reading
            try:
                start_time = None
                if self.trip.mt_start_time:
                    start_time = self.trip.mt_start_time
                elif self.trip.actual_start_time:
                    start_time = self.trip.actual_start_time

                trip_validations['avg_speed'] = AVG_SPEED_OF_VEHICLE_KMS

                if start_time:
                    trip_validations['mt_start_time'] = start_time + timedelta(
                        hours=5.5)

                    trip_start_time_minutes = (timezone.now() - start_time).total_seconds() / 60
                    max_value = trip_start_time_minutes / 60 * AVG_SPEED_OF_VEHICLE_KMS
                    max_value = max_value + self.trip.meter_reading_start
                    max_value = int(max_value) + 1
            except:
                max_value = 10000000  # TODO use -1 after all drivers have app version greater than 1.0.4

            trip_validations['meter_reading_end'] = {
                'min': self.trip.meter_reading_start if self.trip.meter_reading_start else 0,
                'min_msg': 'Closing meter reading cannot be less than Opening KM',
                'max': max_value,
                'max_msg': 'Odometer reading cannot be %s km in %s, Odometer reading should be less than %d'
            }

        if 'assigned' in self.input_fields:
            trip_validations['assigned'] = {
                'min': 0,
                'max': 250
            }

        # for field in self.input_fields:
        #     if field in ['delivered', 'attempted', 'rejected', 'pickups',
        #                  'returned', 'delivered_cash', 'delivered_mpos']:
        #         trip_validations[field] = {
        #             'min': 0,
        #             'max': self.trip.assigned if self.trip.assigned > 0 else -1
        #         }

        return trip_validations

    def __get_display_data(self):
        display_fields = [
            {
                'name': 'trip_number',
                'display': 'Trip',
                'value': self.trip.trip_number,
                'can_edit': False
            }
        ]
        if self.trip.actual_start_time:
            display_fields.append({
                'name': 'actual_start_time',
                'display': 'Started at',
                'value': utc_to_ist(self.trip.actual_start_time).strftime(settings.APP_DATETIME_FORMAT),
                'can_edit': False
            })

        fields = ['assigned', 'delivered', 'attempted', 'rejected', 'pickups',
                  'returned', 'delivered_cash', 'delivered_mpos',
                  'meter_reading_start', 'meter_reading_end']

        for field in fields:
            if hasattr(self.trip, field) and getattr(self.trip, field) and getattr(self.trip, field) > 0:
                field_data = {}
                field_data['name'] = field
                field_data['value'] = getattr(self.trip, field)
                field_data['can_edit'] = False
                field_data['display'] = CommonHelper.get_display_of_tuple(
                    pair=SCREEN_FIELD_NAME, index=field)

                # TODO need to add can_edit field in Input Fields model
                if field == 'assigned' and \
                    self.trip.customer_contract and \
                        self.trip.customer_contract.contract_type not in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
                    field_data['can_edit'] = True
                display_fields.append(field_data)

        return display_fields
