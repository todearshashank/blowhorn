# Django and System libraries
import json
import logging
from django.db.models import Q
from django.db import transaction
from django.utils import timezone

# 3rd party libraries
from rest_framework import status, exceptions
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.driver_app.v2.serializers.trip import TripStopSerializer
from blowhorn.common.helper import CommonHelper
from config.settings import status_pipelines as StatusPipeline
from config.settings.status_pipelines import (
    TRIP_IN_PROGRESS, TRIP_END_STATUSES, TRIP_NEW,
    TRIP_ALL_STOPS_DONE
)
from blowhorn.apps.mixins import UpdateModelMixin

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Stop = get_model('trip', 'Stop')
Trip = get_model('trip', 'Trip')
StopOrders = get_model('trip', 'StopOrders')
Order = get_model('order', 'Order')
DriverLedger = get_model('driver', 'DriverLedger')
Event = get_model('order', 'Event')


class HubStopMixin(object):

    def _initialize_attributes(self, data, trip_id, stop_id):
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.user = self.request.user
        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

        query = Q(pk=trip_id, status=TRIP_IN_PROGRESS)
        self.trip = Trip.objects.prefetch_queryset(
            query=query, stops_list=True, distinct_orders=True).first()
        self.stop = Stop.objects.filter(pk=stop_id, status__in=[TRIP_NEW,
                                                                TRIP_IN_PROGRESS
                                                                ]).first()

        if not self.trip or not self.stop or (self.trip != self.stop.trip):
            self.error_message = 'Invalid Data Sent !'

        self.next_stop = None
        self.no_of_pending_stops = None
        self.next_stop = None
        self.is_last_stop = False

        try:
            trip_last_response = json.loads(data.get('trip_last_response'))
            self.current_step = int(trip_last_response.get('current_step'))
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data Sent !'

        if self.error_message:
            self.response_data = self.error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return


    def __get_response_data(self):
        self.response_data = {
            'is_all_stops_done': self.trip.is_all_stops_done(),
            'current_step': self.trip.current_step,
            'is_trip_in_progress': True,
            'is_loaded': True,
            'next_stop_id': self.next_stop.id if self.next_stop else ''
        }
        logger.info('Response data sent: %s' % self.response_data)

    def __get_pending_stops_and_next_stop(self):
        """
        No dependency on status(In-Progress, New) of current stop and next stop.

        hence won't fail in case of multiple requests
        """
        stop_qs = self.trip.stops.exclude(status__in=TRIP_END_STATUSES)
        no_of_pending_stops = len(stop_qs)
        if no_of_pending_stops > 0:
            stop_qs = sorted(stop_qs, key=lambda x: x.sequence)
            return no_of_pending_stops, stop_qs[0]
        return no_of_pending_stops, None

    def __make_updates(self):

        if self._check_for_order_handover():
            self.error_message = 'Handover all orders to hub associate'
            self.status_code = status.HTTP_400_BAD_REQUEST
            raise exceptions.ValidationError(
                'Handover all orders to hub associate')

        stop_data = {'status': StatusPipeline.TRIP_COMPLETED}

        if self.stop:
            self.stop = UpdateModelMixin().update(data=stop_data,
                                                  instance=self.stop,
                                                  serializer_class=TripStopSerializer)
            self.stop.save()

    def __update_trip(self):
        self.no_of_pending_stops, self.next_stop = self.__get_pending_stops_and_next_stop()
        self.is_last_stop = True if self.no_of_pending_stops == 0 else False

        if self.is_last_stop:
            self.trip.current_step = self.current_step + 1
            self.trip.status = TRIP_ALL_STOPS_DONE
            self.trip.save()

        if self.next_stop:
            self.next_stop.status = StatusPipeline.TRIP_IN_PROGRESS
            self.next_stop.save()

    def _attempt_hubstop(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        try:
            with transaction.atomic():
                self.__make_updates()
                self.__update_trip()
                no_of_pending_stops, self.next_stop = self.__get_pending_stops_and_next_stop()
        except Exception as e:
            self.error_message = e.args[0]
            self.status_code = status.HTTP_400_BAD_REQUEST

        self.__get_response_data()

    def _check_for_order_handover(self):

        return StopOrders.objects.filter(Q(stop=self.stop, trip=self.trip) & ~Q(
            order__status__in=[StatusPipeline.REACHED_AT_HUB,
                               StatusPipeline.REACHED_AT_HUB_RTO])).exists()
