# Django and System libraries
import json
import logging
from django.db.models import Q
from django.db import transaction
from django.conf import settings
from django.forms.models import model_to_dict
from django.utils.translation import ugettext_lazy as _
import copy

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.driver_app.v2.serializers.trip import TripStopSerializer, TripSerializer
from blowhorn.apps.driver_app.v2.serializers.order import OrderSerializer
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import (
    TRIP_COMPLETED, TRIP_CANCELLED, TRIP_ALL_STOPS_DONE,
    ORDER_DELIVERED, TRIP_IN_PROGRESS, ORDER_MOVING_TO_HUB, ORDER_PICKED
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.apps.driver_app.v1.helpers.customer import upsert_customer_rating
from blowhorn.order.const import (
    PAYMENT_STATUS_PAID,
    PAYMENT_MODE_CASH,
    PAYMENT_MODE_CARD,
    PAYMENT_MODE_ONLINE
)
from blowhorn.order.tasks import withdraw_amount_from_paytm
from blowhorn.apps.driver_app.v1.helpers.driver import _check_device_details
from blowhorn.apps.driver_app.v1.helpers.order import \
    check_remaining_items_handover
from blowhorn.contract.constants import CONTRACT_TYPE_KIOSK
from blowhorn.order.models import OrderRating, OrderContainerHistory, \
    OrderContainer, Container
from .send_sms import SendSMS

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Trip = get_model('trip', 'Trip')
StopOrders = get_model('trip', 'StopOrders')
Stop = get_model('trip', 'Stop')
Order = get_model('order', 'Order')
Driver = get_model('driver', 'Driver')


class TripEndMixin(object):

    def _initialize_attributes(self, data, pk):
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.driver = self.request.user.driver
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.context = {'request': self.request}

        self.cash_collected = float(data.get('cash_collected')) if data.get('cash_collected', None) else None
        query = Q(pk=pk, status__in=[TRIP_ALL_STOPS_DONE, TRIP_IN_PROGRESS, TRIP_COMPLETED])
        select_related = ['driver']
        self.trip = Trip.objects.prefetch_queryset(
            query=query, stops_list=True, select_related=select_related).first()
        if not self.trip:
            self.error_message = 'Trip does not exist !'

        try:
            trip_last_response = json.loads(data.get('trip_last_response'))
            self.current_step = int(trip_last_response.get('current_step'))
            self.is_all_stops_done = trip_last_response.get('is_all_stops_done')
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data Sent !'

        if self.error_message:
            self.response_data = self.error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        # TODO change data.get('wait_time_capture')
        wait_time_capture = data.get('confirmation_screen', None)
        # TODO remove this bunch of code
        if wait_time_capture == 'false' or not wait_time_capture:
            self.wait_time_capture = False
        else:
            self.wait_time_capture = True

        self.last_stop_data = {}
        self.last_stop = None
        self.order = None
        if self.trip.order:
            order_query = Q(pk=self.trip.order.id)
            select_related = ['customer__user', 'labour']
            prefetch_related = ['city__state_set',
                                'customer__customertaxinformation_set', 'trip_set']

            self.order = Order.objects.prefetch_queryset(
                query=order_query, select_related=select_related,
                prefetch_related=prefetch_related).first()
        self.order_data = {}
        # Code for skipping the last stop and show directly on End Trip Screen in C2C trip
        # self.complete_trip = self.trip.is_all_stops_done()
        self.complete_trip = True
        self.payment_type = data.get('payment_type', None)
        self.update_order_payment = True if self.payment_type and self.order else False
        self.response = {"is_ignore": True}
        self.rating = data.get('rating', None)
        self.cost_components = False
        if self.order:
            self.order.end_trip_api = True

        self.app_name = 'Unified App'
        self.app_version = data.get('app_version', '')
        self.app_code = data.get('app_code', '')
        self.trip_distance_km = data.get('trip_distance_km', '')
        self.trip_location_points = data.get('trip_location_points', '')
        self.app_rds_points = data.get('trip_sent_location', '')

        # delete records from order container and put data in order container history
        self.order_containers = OrderContainer.objects.filter(trip=self.trip)
        self.containers = []
        self.oc_hist_list = []

    def __check_valid_request(self):
        internet_spikes = False
        if not self.is_all_stops_done:
            if self.trip.stops_list:
                if self.complete_trip:
                    internet_spikes = True
            else:
                if self.trip.status == TRIP_COMPLETED:
                    internet_spikes = True
        elif self.trip.current_step != self.current_step:
            internet_spikes = True

        return not internet_spikes

    def __get_response_data(self):
        cost_components = []
        if self.order and self.order.cost_components and self.cost_components:
            # TODO add try except
            if self.order.order_type == settings.C2C_ORDER_TYPE and \
                    self.order.customer_contract.contract_type != CONTRACT_TYPE_KIOSK:
                cost_components = json.loads(self.order.cost_components)

            payment_mode = self.order.payment_mode
            if payment_mode != PAYMENT_MODE_CASH:
                payment_mode = PAYMENT_MODE_ONLINE

            if self.order.customer and (self.order.customer.credit_amount > 0 or self.order.customer.credit_period > 0):
                total_amount = 0.0
                allow_payment_selection = False
            else:
                total_amount = float(self.order.total_payable) - float(self.order.amount_paid_online)
                allow_payment_selection = settings.ALLOW_PAYMENT_SELECTION if self.order.payment_mode else True
                # remaining amount should be collected by cash
                if self.order.amount_paid_online > 0 and total_amount > 0:
                    payment_mode = PAYMENT_MODE_CASH
        else:
            total_amount = 0.0
            allow_payment_selection = False
            # cost_components = []
            payment_mode = None

        self.response_data = {
            'is_all_stops_done': True,
            'current_step': self.trip.current_step,
            'is_trip_in_progress': False,
            'cost_components': cost_components,
            'is_loaded': True,
            'next_stop_id': '',
            'total_amount': total_amount,
            'allow_payment_selection': allow_payment_selection,
            'payment_mode': payment_mode
        }
        logger.info('Response data sent: %s' % self.response_data)

    def _end_trip(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():

            is_valid_request = self.__check_valid_request()
            is_valid_request = True

            is_order_left = False

            if not self.trip.trip_workflow.end_trip_otp:
                is_order_left = check_remaining_items_handover(self.trip,
                                                           self.driver)
            if is_order_left:
                self.error_message = _(
                    'Handover remaining orders/cash to Hub associate')
                self.status_code = status.HTTP_400_BAD_REQUEST
                return

            if is_valid_request:

                self.__make_updates()
                if self.update_order_payment and not self.__update_order_payment(self.order.total_payable):
                    raise BaseException('Order Amount not paid')

                self.__save_updates()
                if self.order and self.order.order_type == settings.C2C_ORDER_TYPE:
                    self.__calculate_c2c_payment()
                    self.order.save()
                    if self.order.customer_contract.contract_type == CONTRACT_TYPE_KIOSK:
                        SendSMS().send_tracking_link(self.order.user, self.order)

                current_step = self.__get_next_step()
                self.trip_data['current_step'] = current_step
                self.trip.current_step = current_step
                self.__get_response_data()
                self.__save_updates()
                _check_device_details(self.driver, self.app_version, self.app_code)

    def __calculate_c2c_payment(self):
        if not self.order.cost_components and self.order.status != PAYMENT_STATUS_PAID:
            self.order.calculate_and_update_cost(self.trip)
            self.cost_components = True
            if self.order.auto_debit_paytm and self.order.status != PAYMENT_STATUS_PAID:
                withdraw_amount_from_paytm.apply_async(
                    (self.order.number, str(self.order.total_payable),))

    def __make_updates(self):
        # Code for skipping the last stop and show directly on End Trip Screen in C2C trip
        # if not self.complete_trip:
        #     if self.trip.stops_list:
        #         stop_qs = sorted(self.trip.stops_list, key=lambda x: x.sequence, reverse=True)
        #         self.last_stop = stop_qs[0]
        #         if self.last_stop.status != TRIP_COMPLETED:
        #             self.last_stop_data['status'] = TRIP_COMPLETED
        #     if self.trip.status != TRIP_ALL_STOPS_DONE:
        #         self.trip_data['status'] = TRIP_ALL_STOPS_DONE

        #     if self.order and self.trip.customer_contract.contract_type == CONTRACT_TYPE_C2C:
        #         self.__calculate_c2c_payment()
        #         if self.order.status != DRIVER_ARRIVED_AT_DROPOFF:
        #             self.order_data['status'] = DRIVER_ARRIVED_AT_DROPOFF
        #         if self.order.auto_debit_paytm:
        #             withdraw_amount_from_paytm.apply_async(
        #                 (self.order.number, str(self.order.total_payable),))
        #     else:
        #         self.complete_trip = True

        if self.complete_trip:
            # if self.order and self.trip.customer_contract.contract_type == CONTRACT_TYPE_C2C:
            #     self.__calculate_c2c_payment()
            if self.trip.status not in [TRIP_CANCELLED, TRIP_COMPLETED]:
                self.trip_data['status'] = TRIP_COMPLETED
            if self.order:
                if self.order.status != ORDER_DELIVERED:
                    self.order_data['status'] = ORDER_DELIVERED
            if self.rating:
                upsert_customer_rating(self.driver, self.order, float(self.rating))
            elif self.order and self.order.order_type == settings.DEFAULT_ORDER_TYPE:
                OrderRating.objects.get_or_create(driver=self.driver, order=self.order)

            # delete records from order container and put data in order container history
            for order_container in self.order_containers:
                self.containers.append(order_container.container_id)
                self.oc_hist_list.append(
                    OrderContainerHistory(trip=order_container.trip,
                                          stop=order_container.stop,
                                          order=order_container.order,
                                          container_number=order_container.container.number,
                                          container_type=order_container.container.container_type.name,
                                          customer=order_container.container.customer
                                          ))

    def __get_next_step(self):
        if self.order and self.order.cost_components and self.cost_components:
            return self.current_step

        if self.complete_trip:
            # Specifies that trip has been completed
            return -1
        return self.current_step

    def __save_updates(self, **kwargs):
        logger.info('last_stop_data: %s' % self.last_stop_data)
        if self.last_stop and self.last_stop_data:
            self.last_stop_data['geopoint'] = self.geopoint
            self.last_stop = UpdateModelMixin().update(data=self.last_stop_data, instance=self.last_stop,
                                                       serializer_class=TripStopSerializer)
            self.last_stop.save()

        logger.info('trip_data: %s' % self.trip_data)
        if self.trip and self.trip_data:
            self.trip_data['geopoint'] = self.geopoint
            self.trip_data['app_name'] = self.app_name
            self.trip_data['app_version'] = self.app_version
            if self.trip_distance_km:
                self.trip_data['app_distance'] = self.trip_distance_km
            if self.trip_location_points:
                self.trip_data['app_points_count'] = self.trip_location_points
            if self.app_rds_points:
                self.trip_data['app_rds_points'] = self.app_rds_points
            self.trip = UpdateModelMixin().update(data=self.trip_data, instance=self.trip,
                                                  serializer_class=TripSerializer)
            try:
                self.trip.save()
            except Exception as e:
                self.error_message = str(e)
                self.status_code = status.HTTP_400_BAD_REQUEST
                return

        logger.info('order_data: %s' % self.order_data)
        if self.order or self.order_data:
            if self.order_data.get('status', None):
                self.context['geopoint'] = self.geopoint
            if self.order_data:
                self.order = UpdateModelMixin().update(data=self.order_data, instance=self.order,
                                                       serializer_class=OrderSerializer,
                                                       context=self.context)
            self.order.save()

        # on trip end delete order container data, and move it to history table.
        if self.order_containers:
            if self.oc_hist_list:
                OrderContainerHistory.objects.bulk_create(self.oc_hist_list)
            self.order_containers.delete()
            Container.objects.filter(id__in=self.containers).delete()
            Driver.objects.filter(id=self.driver.id).update(cod_balance=0)


    # TODO revisit this
    # same function used in trip end.use common function
    def __update_order_payment(self, order_amount):
        order = self.order
        if self.order.payment_status == PAYMENT_STATUS_PAID:
            return True

        if not self.cash_collected:
            self.cash_collected = float(order.total_payable)

        if self.payment_type == PAYMENT_MODE_CARD:
            if order.cash_collected < order_amount:
                return False
            self.order_data['payment_status'] = PAYMENT_STATUS_PAID
            self.order_data['payment_mode'] = PAYMENT_MODE_CARD
        elif self.payment_type == PAYMENT_MODE_CASH:
            self.order_data['payment_mode'] = PAYMENT_MODE_CASH
            self.trip_data['total_cash_collected'] = \
                float(self.trip.total_cash_collected) + \
                self.cash_collected if self.trip.total_cash_collected else self.cash_collected
            self.order_data['payment_status'] = PAYMENT_STATUS_PAID

        return True
        # code for blowhorn wallet implementation
        # if self.cash_collected:
        #     self.trip_data['total_cash_collected'] += self.cash_collected
        # if self.cash_collected == order_amount:
        #     self.order_data['payment_status'] = PAYMENT_STATUS_PAID
        # elif self.cash_collected < order_amount:
        #     self.order_data['payment_status'] = PAYMENT_STATUS_PARTIALLY_PAID
        #     # TODO
        #     logger.info('money unpaid (order.total_incl_tax - cash_collected)')
        # else:
        #     # in case of no change
        #     # TODO
        #     logger.info('add money to blowhorn wallet (cash_collected - order.total_incl_tax)')
