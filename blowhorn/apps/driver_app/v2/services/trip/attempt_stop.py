"""
Change to be done on Trip Stop that is In-Progress.

will be called when driver is on 2 screens:
- Reached Stop
    -Update Trip Status to All-Stops-Done if Last Stop
    -Update Trip Current Step value
    -Update the Current In-Progress Stop to Completed
    -Update the Next Stop in New Status to In-Progress

    If there is No Finished Work Screen:
        -Update Trip Status to All-Stops-Done if Last Stop
        -Update Trip Current Step value
        -Update the Current In-Progress Stop to Completed
        -Update Reach Time for Current Stop
        -Update the Next Stop in New Status to In-Progress
    else:
        -Update Reach Time for Current Stop
        -Update Trip Current Step value
- Finished Work
    -Update Trip Status to All-Stops-Done if Last Stop
    -Update Trip Current Step value
    -Update the Current In-Progress Stop to Completed
    -Update the Next Stop in New Status to In-Progress

If 'action' is sent in the Input Data:
    - Update Order status == data.get('action')
    - Update Trip Packages Field accordingly

Input Data: 'trip_id', 'stop_id',
            'location_details',
            trip_last_response:{
                'is_all_stops_done': False,
                'current_step': 1,
                'is_trip_in_progress': True,
                'is_loaded': True,
                'next_stop_id': ''
            },
            trip_data: {
                'meter_reading_start': 100,
                'assigned': 50
            },
            'confirmation_screen': 'true'/'false'(
                to differentiate whether it is start trip or finished loading screen
            ),
            'action': 'Delivered'/'Unable-To-Deliver'/'Returned'(
                order statuses sent from app as it is stored that we store in db
                PS. see status_pipelines.py
            ),
            'payment_type': 'Cash'/'Online',
            'cash_collected', 'delivered_to', 'undelivered_reason'

Response:
    {
        'is_all_stops_done': trip.is_all_stops_done(),
        'current_step': trip.current_step,
        'is_trip_in_progress': True,
        'is_loaded': True,
        'next_stop_id': next inprogress stop id that is made if present else ''
    }

Created by: Gaurav Verma
"""
# Django and System libraries
import json
import logging
from datetime import datetime
from django.db.models import Q, F
from django.db import transaction
from django.utils import timezone
from django.conf import settings

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.apps.driver_app.v2.serializers.trip import TripSerializer, \
    TripStopSerializer
from blowhorn.apps.driver_app.v2.services.trip.send_sms import SendSMS
from blowhorn.apps.driver_app.v2.serializers.order import OrderSerializer
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import (
    TRIP_IN_PROGRESS, TRIP_END_STATUSES,
    TRIP_ALL_STOPS_DONE, TRIP_COMPLETED,
    ORDER_DELIVERED, ORDER_RETURNED,
    UNABLE_TO_DELIVER, DRIVER_ARRIVED_AT_DROPOFF,
    RESCHEDULE_DELIVERY
)
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.order.const import (
    PAYMENT_STATUS_PAID,
    PAYMENT_MODE_CASH,
    PAYMENT_MODE_CARD
)
from blowhorn.customer.tasks import broadcast_order_details_to_dashboard
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_SHIPMENT
from blowhorn.order.models import OrderLine
from blowhorn.utils.functions import get_base_url


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Stop = get_model('trip', 'Stop')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
DriverLedger = get_model('driver', 'DriverLedger')


class AttemptStopMixin(object):
    """Change to be done on Trip Stop that is In-Progress."""

    def _initialize_attributes(self, data, trip_id, stop_id):
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.context = {'request': self.request}
        self.driver = self.request.user.driver
        self.site_url = get_base_url(self.request)
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)
        self.order_lines = data.get('order_lines', None)

        query = Q(pk=trip_id, status=TRIP_IN_PROGRESS)
        self.trip = Trip.objects.prefetch_queryset(
            query=query, stops_list=True, distinct_orders=True).first()
        self.stop = Stop.objects.filter(pk=stop_id, status=TRIP_IN_PROGRESS).first()
        if not self.trip or not self.stop or (self.trip != self.stop.trip):
            self.error_message = 'Invalid Data Sent !'

        try:
            trip_last_response = json.loads(data.get('trip_last_response'))
            self.current_step = int(trip_last_response.get('current_step'))
            self.trip_data = json.loads(data['trip_data']) if data.get('trip_data', None) else {}
        except BaseException:
            self.error_message = 'Invalid Data Sent !'

        if self.error_message:
            self.response_data = self.error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        # TODO change data.get('wait_time_capture')
        wait_time_capture = data.get('confirmation_screen', None)
        # TODO remove this bunch of code
        if wait_time_capture == 'false' or not wait_time_capture:
            self.wait_time_capture = False
        else:
            self.wait_time_capture = True

        self.stop_data = {}
        self.order_data = {}
        self.action = data.get('action', None) != 'false'
        if self.action:
            self.action = data.get('action', None)
        elif self.trip.customer_contract in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
            self.action = ORDER_DELIVERED
        self.msg_status = self.action
        # TODO logic need to be improved
        self.order = self.stop.order if self.action else self.trip.order
        self.next_stop = None
        self.payment_type = data.get('payment_type', None)

        # for cod orders
        self.cash_collected = float(data.get('cash_collected'))\
            if data.get('cash_collected', None) else None
        self.update_order_payment = True if self.payment_type and self.order \
            else False
        self.delivered_to = data.get('delivered_to', None)
        self.reason = data.get('undelivered_reason', None)
        self.reschedule_time = data.get('reschedule_time', None)

        self.no_of_pending_stops, self.next_stop = self.__get_pending_stops_and_next_stop()
        self.is_last_stop = True if self.no_of_pending_stops == 0 else False
        self.is_second_last_stop = True if self.no_of_pending_stops == 1 else False
        self.remarks = None

    def __get_pending_stops_and_next_stop(self):
        """
        No dependency on status(In-Progress, New) of current stop and next stop.

        hence won't fail in case of multiple requests
        """
        stop_qs = self.trip.stops.exclude(pk=self.stop.id)\
            .exclude(status__in=TRIP_END_STATUSES)
        no_of_pending_stops = len(stop_qs)
        if no_of_pending_stops > 0:
            stop_qs = sorted(stop_qs, key=lambda x: x.sequence)
            return no_of_pending_stops, stop_qs[0]
        return no_of_pending_stops, None

    def __make_updates(self):
        if self.wait_time_capture:
            self.__reach_location_wait_time_capture()
        else:
            if self.stop.reach_time:
                self.__finished_work_screen()
            else:
                self.__reach_location_no_wait_time_capture()

            if self.action:
                if self.order.status != self.action:
                    self.order_data['status'] = self.action
                    self.__update_order_fields()

    def __save_updates(self):
        logger.info('trip_data: %s' % self.trip_data)
        broadcast_order_updates = False
        if self.trip and self.trip_data:
            self.trip = UpdateModelMixin().update(
                data=self.trip_data,
                instance=self.trip,
                serializer_class=TripSerializer)
            self.trip.save()
        logger.info('order_data: %s' % self.order_data)
        if self.order and self.order_data:
            if self.order_data.get('status', None):
                self.context['geopoint'] = self.geopoint
                self.context['remarks'] = self.remarks
            self.order = UpdateModelMixin().update(
                data=self.order_data,
                instance=self.order,
                serializer_class=OrderSerializer,
                context=self.context)
            self.order.save()
            broadcast_order_updates = True
        if self.next_stop and not self.wait_time_capture:
            self.next_stop_data = {'status': TRIP_IN_PROGRESS}
            logger.info('next_stop_data: %s' % self.next_stop_data)
            self.next_stop = UpdateModelMixin().update(
                data=self.next_stop_data,
                instance=self.next_stop,
                serializer_class=TripStopSerializer)
            self.next_stop.save()

            # if self.trip.trip_workflow.otp_required and \
            #         self.next_stop.status == TRIP_IN_PROGRESS:
                # error_msg, otp = SendSMS().send_otp_for_stop(self.next_stop.order)

        logger.info('stop_data: %s' % self.stop_data)
        if self.stop and self.stop_data:
            self.stop = UpdateModelMixin().update(
                data=self.stop_data, instance=self.stop,
                serializer_class=TripStopSerializer)
            self.stop.save()

        if self.order.status == ORDER_DELIVERED:
            if self.order.cash_on_delivery and self.order.cash_on_delivery > 0:
                self.driver.save()

            if self.order_lines:
                try:
                    order_lines = json.loads(self.order_lines)
                    for order_line in order_lines:
                        removed = order_line.get('removed') or 0
                        line_id = order_line.get('id')
                        OrderLine.objects.filter(
                            order_id=self.order,
                            id=line_id
                        ).update(
                            qty_delivered=F('quantity') - F(
                                'qty_removed') - removed,
                            delivered=1
                        )
                        broadcast_order_updates = True
                except:
                    self.error_message = 'Invalid Order Line details'

        if broadcast_order_updates:
            broadcast_order_details_to_dashboard.apply_async((self.order.id,), )

        if self.order and self.order_data.get('status'):
            SendSMS().send_message_for_order_event(self.order,
                                                   self.msg_status,
                                                   trip=self.trip)
        if self.next_stop and self.next_stop_data.get(
            'status') and self.next_stop.order:
            SendSMS().send_message_for_order_event(self.next_stop.order,
                                                   self.next_stop.order.status,
                                                   self.next_stop_data.get(
                                                       'status'),
                                                   self.trip,
                                                   self.site_url
                                                   )

    def __get_response_data(self):
        self.response_data = {
            'is_all_stops_done': self.trip.is_all_stops_done(),
            'current_step': self.trip.current_step,
            'is_trip_in_progress': True,
            'is_loaded': True,
            'next_stop_id': self.next_stop.id if self.next_stop else ''
        }
        logger.info('Response data sent: %s' % self.response_data)

    # TODO revisit again
    def __check_valid_request(self):
        if (self.trip.current_step != self.current_step) or (self.stop.status != TRIP_IN_PROGRESS):
            return False
        return True

    def _attempt_stop(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        is_valid_request = self.__check_valid_request()

        if is_valid_request:
            with transaction.atomic():
                if self.update_order_payment and not self.__update_order_payment():
                    raise BaseException('Order Amount not paid')
                self.__make_updates()
                self.__save_updates()
        else:
            self.next_stop = self.trip.get_current_stop_in_progress()

        self.__get_response_data()

    # TODO revisit this
    """
    this function is no more same as function used in trip end. This function
    is applicable for stops which invloves payment for order
    """
    def __update_order_payment(self):
        order = self.order
        if self.action == ORDER_DELIVERED:
            if self.payment_type == PAYMENT_MODE_CARD:
                # if order.cash_collected != order.total_payable:
                #     return False
                self.order_data['payment_status'] = PAYMENT_STATUS_PAID
                self.order_data['payment_mode'] = PAYMENT_MODE_CARD
                self.order_data['amount_paid_online'] = self.cash_collected

            elif self.payment_type == PAYMENT_MODE_CASH:
                self.order_data['payment_mode'] = PAYMENT_MODE_CASH
                self.order_data['cash_collected'] = self.cash_collected

                if self.cash_collected:
                    self.trip_data['total_cash_collected'] = \
                        float(self.trip.total_cash_collected) + float(
                            self.cash_collected)

                    self.driver.cod_balance = float(
                        self.driver.cod_balance) + float(self.cash_collected)

                # if self.cash_collected == order.total_payable:
                #     self.order_data['payment_status'] = PAYMENT_STATUS_PAID
                # elif self.cash_collected < order.total_incl_tax:
                #     self.order_data['payment_status'] = PAYMENT_STATUS_PARTIALLY_PAID
                #     # TODO
                #     logger.info('money unpaid (order.total_incl_tax - cash_collected)')
                # else:
                #     # in case of no change
                #     # TODO
                #     logger.info('add money to blowhorn wallet (cash_collected - order.total_incl_tax)')
        return True

    def __finished_work_screen(self):
        logger.info('__finished_work_screen')
        # that means Finished Work screen
        if self.is_last_stop:
            if self.trip.status != TRIP_ALL_STOPS_DONE:
                """
                using self.current_step instead of self.trip.current_step
                because in case of slow internet or multiple requests
                trip current step might be updated by other previous requests
                """
                self.trip_data['current_step'] = self.current_step + 1
                self.trip_data['status'] = TRIP_ALL_STOPS_DONE
        else:
            self.trip_data['current_step'] = self.current_step - 1

        if self.stop.status != TRIP_COMPLETED:
            self.stop_data['status'] = TRIP_COMPLETED

    def __reach_location_wait_time_capture(self):
        logger.info('__reach_location_wait_time_capture')
        if not self.stop.reach_time:
            self.stop_data['reach_time'] = timezone.now()

        # Code for skipping the last stop and show directly on End Trip Screen in C2C trip
        # if self.is_last_stop and not self.trip.trip_workflow.is_reorder:
        #     self.trip_data['current_step'] = self.current_step + 2
        # else:
        #     self.trip_data['current_step'] = self.current_step + 1
        self.trip_data['current_step'] = self.current_step + 1

    def __reach_location_no_wait_time_capture(self):
        logger.info('__reach_location_no_wait_time_capture')
        # that means Reach Location Screen
        if self.is_last_stop:
            if self.order.status != DRIVER_ARRIVED_AT_DROPOFF:
                self.order_data['status'] = DRIVER_ARRIVED_AT_DROPOFF
            if self.trip.status != TRIP_ALL_STOPS_DONE:
                self.trip_data['status'] = TRIP_ALL_STOPS_DONE
                # TODO need to be removed one line
                self.trip_data['geopoint'] = self.geopoint
                self.trip_data['current_step'] = self.current_step + 1

        # Code for skipping the last stop and show directly on End Trip Screen in C2C trip
        # if self.is_second_last_stop and not self.trip.trip_workflow.is_reorder:
        #     self.trip_data['current_step'] = self.current_step + 1

        if self.stop.status != TRIP_COMPLETED:
            self.stop_data['status'] = TRIP_COMPLETED
            if self.stop.order and self.stop.order.order_type in [
                settings.SUBSCRIPTION_ORDER_TYPE, settings.SPOT_ORDER_TYPE,
                settings.C2C_ORDER_TYPE]:
                self.stop_data['reach_time'] = timezone.now()

    def __update_order_fields(self):

        if self.action == ORDER_DELIVERED:
            self.trip_data['delivered'] = self.trip.delivered + 1
            # if self.order.cash_on_delivery and self.order.cash_on_delivery > 0 \
            #     and not self.cash_collected:
            #     """
            #     If driver enters amount then use that, else cash collected
            #     will be same as cod amount
            #     """
            #     cash_collected_by_driver = 0
            #     if self.cash_collected and self.payment_type == PAYMENT_MODE_CASH:
            #         cash_collected_by_driver = self.cash_collected
            #     elif not self.cash_collected:
            #         cash_collected_by_driver = self.order.cash_on_delivery
            #
            #     self.trip_data['total_cash_collected'] = \
            #         float(cash_collected_by_driver) + \
            #         float(self.trip.total_cash_collected)
            #
            #     self.driver.cod_balance = float(
            #         self.driver.cod_balance) + float(cash_collected_by_driver)

            if self.order.customer.notification_url and self.order.customer.ecode:
                self.order_data['_send_status_notification'] = True

        if self.delivered_to:
            self.remarks = self.delivered_to
        elif self.reason:
            self.remarks = self.reason

        if self.action == ORDER_RETURNED:
            self.trip_data['rejected'] = self.trip.rejected + 1
            if self.order.has_pickup:
                self.order_data['return_to_origin'] = True
        if self.action == UNABLE_TO_DELIVER:
            self.trip_data['returned'] = self.trip.returned + 1

        if self.action == UNABLE_TO_DELIVER and self.reschedule_time:
            self.remarks += '(Rescheduled to %s)' % self.reschedule_time

            self.order_data['expected_delivery_time'] = datetime.strptime(
                self.reschedule_time, '%Y-%m-%d %H:%M')
            self.msg_status = RESCHEDULE_DELIVERY
        # self.order_data['cash_collected'] = self.cash_collected
