"""
Accept/Reject Booking/c2c order by driver.

Input Data: action('accept'/'reject'), order_id, location_details

Response:
    -If Driver Accepts:
        Returns Created Trip Data
    -If Driver Rejects:
        Returns 'Driver Rejected' and accept/reject notification will be sent to the next driver.

Created By: Gaurav Verma
"""
# Django and System libraries
import logging
from django.db.models import Q, Prefetch
from django.db import transaction
from django.utils import timezone

# 3rd party libraries
from rest_framework import status
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.common.helper import CommonHelper
from config.settings.status_pipelines import DRIVER_ACCEPTED, TRIP_COMPLETED, \
    ORDER_NEW
from blowhorn.customer.tasks import broadcast_order_details_to_dashboard
from blowhorn.apps.driver_app.v2.helpers.trip import create_stop, create_trip
from blowhorn.apps.driver_app.v2.serializers.order import OrderSerializer
from blowhorn.apps.mixins import UpdateModelMixin
from blowhorn.order.models import OrderDispatch
from config.settings import status_pipelines as StatusPipeline

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
Order = get_model('order', 'Order')
WayPoint = get_model('order', 'WayPoint')


class OrderDecision(object):
    """Accept/Reject Booking/c2c order by driver."""

    def _initialize_attributes(self, data, order_id):
        """
        Parse the input params.

        Any validation error on input params if required, should be raised here.
        """
        self.error_message = ''
        self.status_code = status.HTTP_200_OK
        self.response_data = ''
        self.context = {'request': self.request}
        self.trip = None
        self.driver = self.request.user.driver
        location = data.get('location_details', None)
        self.geopoint = CommonHelper.get_geopoint_from_location_json(location)

        if Trip.objects.filter(driver=self.driver, status=DRIVER_ACCEPTED).exists():
            self.error_message = 'Existing trip in Accepted present!!!'
            return
        # TODO need to get this from app
        self.app_name = 'Unified App'
        self.app_version = self.driver.device_details.app_version_name \
            if self.driver.device_details else None

        query = Q(pk=order_id, status=ORDER_NEW)
        waypoint_prefetch = Prefetch(
            'waypoint_set',
            queryset=WayPoint.objects.all().prefetch_related('stops')
        )
        self.order = Order.objects.prefetch_queryset(
            query=query, prefetch_related=[waypoint_prefetch, ]).first()
        if not self.order:
            self.error_message = 'Order does not exist !'

        self.action = data.get('action', None)
        self.dispatch_id = data.get('dispatch_id', None)
        self.order_data = {}

    def _make_decision(self):
        if self.error_message:
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():
            if self.action == 'accept':
                self.__accept_order()
            else:
                self.__reject_order()
                self.response_data = 'Driver Rejected'

    def __reject_order(self):
        self.order.rejected_drivers.add(self.driver)
        self.order.save()
        if self.action == 'autoreject':
            status = StatusPipeline.DRIVER_NO_RESPONSE
        else:
            status = StatusPipeline.DRIVER_REJECTED
        if self.dispatch_id:
            OrderDispatch.objects.filter(id=self.dispatch_id, order=self.order, driver=self.driver,
                                     status=StatusPipeline.DRIVER_DISPATCHED).update(status=status,
                                     modified_date=timezone.now(), response_point=self.geopoint)

    def __accept_order(self):
        # Create and Assign trip to driver
        self.trip = self._create_trip_stops_c2c(order=self.order, driver=self.driver)
        if not self.trip:
            return False
        if self.dispatch_id:
            OrderDispatch.objects.filter(id=self.dispatch_id, order=self.order, driver=self.driver,
                                     status=StatusPipeline.DRIVER_DISPATCHED).update(status=DRIVER_ACCEPTED,
                                     modified_date=timezone.now(), response_point=self.geopoint)

        self.__update_order()
        self.response_data = self.serializer_class(instance=self.trip, context=self.context).data

    def __update_order(self):
        vehicle_class = self.trip.vehicle.vehicle_model.vehicle_class \
            if self.trip.vehicle else ''

        self.order_data['status'] = DRIVER_ACCEPTED
        self.order_data['driver'] = self.driver.id
        self.context['geopoint'] = self.geopoint
        self.context['vehicle_class'] = vehicle_class
        self.context['vehicle_rc'] = \
            self.trip.vehicle.registration_certificate_number
        self.order = UpdateModelMixin().update(
            data=self.order_data, instance=self.order,
            serializer_class=OrderSerializer, context=self.context)
        self.order.save()
        broadcast_order_details_to_dashboard.apply_async((self.order.id,), )

    def _create_trip_stops_c2c(self, driver, order):
        contract_id = order.customer_contract.id if order.customer_contract \
            else None
        trip = create_trip(
            driver_id=self.driver.id,
            order=order.id,
            blowhorn_contract=self.driver.contract.id if self.driver.contract
            else None,
            estimated_distance_km=order.estimated_distance_km,
            customer_contract=contract_id,
            invoice_driver=self.driver.id,
            trip_workflow=order.customer_contract.trip_workflow_id,
            trip_acceptence_time=timezone.now(),
            status=DRIVER_ACCEPTED,
            app_name=self.app_name,
            app_version=self.app_version
        )
        if trip:
            stops_created = self._add_c2c_order_as_trip_stop(order=order, trip=trip)
            if not stops_created:
                trip.delete()
                return False
        return trip

    def _add_c2c_order_as_trip_stop(self, order, trip):
        waypoint_qs = order.waypoint_set.all()
        if 0 == len(waypoint_qs):
            logger.error('No waypoints are there for order "%s"', order)
            return False

        for waypoint in waypoint_qs:
            stop = waypoint.stops.all().last()
            if stop and stop.status == TRIP_COMPLETED:
                continue

            if not Stop.objects.filter(waypoint=waypoint, trip=trip).exists():
                stop = create_stop(trip=trip, order=order.id,
                                   sequence=waypoint.sequence_id,
                                   waypoint=waypoint.id)
                if not stop:
                    logger.info('Trip Stop Creation Failure')
                    logger.info('Trip:- "%s" Deletion Successful', trip)
                    return False

                logger.info('Assigned order:- "%s", waypoint:- %d to stop:- %d',
                            order, waypoint.id, stop.id)
        return True
