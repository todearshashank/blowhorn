# from django.conf.urls import url
# from django.apps import AppConfig
# from blowhorn.apps.driver_app.v1.views import driver as driver_views
# from blowhorn.apps.driver_app.v2.views import trip as trip_views
# from blowhorn.apps.driver_app.v2.views import order as order_views
# from blowhorn.order.views import OrderDocumentUpload
# from blowhorn.apps.operation_app.v2.views.driver import DriverPhoto, DriverDocumentUpload
#
#
# class DriverVersionOne(AppConfig):
#
#     def get_urls(self):
#         urlpatterns = [
#             # driver views apis
#             url(r'^login$', driver_views.Login.as_view(), name='driver-login-v1'),
#             url(r'^tos', driver_views.Tos.as_view(), name='driver-tos-v1'),
#             url(r'^fcm$', driver_views.Fcm.as_view(), name='driver-fcm-v1'),
#             url(r'^activity$', driver_views.Activity.as_view(), name='driver-activity'),
#             url(r'^dutystatus$', driver_views.DutyStatus.as_view(), name='driver-duty-status'),
#             url(r'^arrival$', driver_views.Arrival.as_view(), name='driver-arrival'),
#             url(r'^devicestatus$', driver_views.DeviceStatus.as_view(), name='driver-device-status'),
#             url(r'^earnings$', driver_views.Payments.as_view(), name='driver-earnings'),
#             url(r'^statement', driver_views.Statement.as_view(), name='driver-ledger-statement'),
#             url(r'^ledgers$', driver_views.LedgerStatement.as_view(), name='driver-statements'),
#             url(r'^referrals$', driver_views.DriverReferralsView.as_view(), name='driver-referrals'),
#             url(r'^logout$', driver_views.Logout.as_view(), name='driver-logout-v1'),
#             url(r'^(?P<pk>[\w\-]+)/photo$', DriverPhoto.as_view(), name='driver-photo-v1'),
#             url(r'^documents$', DriverDocumentUpload.as_view(), name='driver-documents-v1'),
#             # trip views apis
#             url(r'^trips/sync$', trip_views.TripSync.as_view(), name='driver-trip-sync'),
#             url(r'^trips/upcoming$', trip_views.UpcomingTrips.as_view(), name='driver-upcoming-trip'),
#             url(r'^trips/past$', trip_views.PastTrips.as_view(), name='driver-past-trips'),
#             url(r'^trips/(?P<pk>[\w\-]+)/cancel$', trip_views.Cancel.as_view(), name='trip-cancel-v2'),
#             url(r'^trips/(?P<pk>[\w\-]+)/complete$', trip_views.Complete.as_view(), name='trip-complete-v2'),
#             url(r'^trips/(?P<pk>[\w\-]+)/start$', trip_views.TripStart.as_view(), name='trip-start'),
#             url(r'^trips/(?P<trip_id>[\w\-]+)/stops/(?P<stop_id>[\w\-]+)/attempt$',
#                 trip_views.AttemptStop.as_view(), name='stop-attempt'),
#             url(r'^trips/(?P<pk>[\w\-]+)/end$', trip_views.TripEnd.as_view(), name='trip-end'),
#             url(r'^trips/create$', trip_views.CreateTripOnTheRun.as_view(), name='trip-create-on-the-run'),
#             url(r'^trips/(?P<trip_id>[\w\-]+)/decision$', trip_views.Decision.as_view(), name='trip-decision-v2'),
#             url(r'^trips/(?P<trip_id>[\w\-]+)/stops/(?P<stop_id>[\w\-]+)/reorder$',
#                 trip_views.StopReorder.as_view(), name='stop-reorder-v2'),
#             url(r'^trips/(?P<trip_id>[\w\-]+)/validation$', trip_views.TripFieldsValidation.as_view(),
#                 name='trip-validation-v2'),
#             url(r'^orderpickrto/(?P<trip_id>[\w\-]+)/stops/attempt$',
#                 trip_views.AttemptPickupStop.as_view(), name='pickup-rto-attempt'),
#             url(r'^trips/(?P<trip_id>[\w\-]+)/stops/(?P<stop_id>[\w\-]+)/hubattempt$',
#                 trip_views.AttemptHubStop.as_view(), name='hub-attempt'),
#             # order views api
#             url(r'^orders/scan$', order_views.ScanOrders.as_view(), name='orders-scan'),
#             url(r'^orders/create$', order_views.CreateOrderOnTheRun.as_view(), name='order-create-on-the-run'),
#             url(r'^orders/(?P<order_id>[\w\-]+)/decision$', order_views.Decision.as_view(), name='order-decision-v2'),
#             url(r'^orders/documents/(?P<order_number>[\w\-]+)/upload$',
#                 OrderDocumentUpload.as_view(), name='order-documents-v1'),
#             url(r'^orders/(?P<order_id>[\w\-]+)/paymentstatus$', order_views.CheckPaymentStatus.as_view(),
#                 name='order-paymentstatus'),
#             url(r'^dispatch/ack', order_views.DispatchAcknowledgement.as_view(),
#                 name='dispatch_ack'),
#             url(r'^documents/upload', driver_views.DocumentUpload.as_view(),
#                 name='upload_document'),
#             url(r'^paymentdetails$', driver_views.PaymentDetails.as_view(),
#                 name='payment_details'),
#             url(r'^tds$', driver_views.driver_tds.as_view(),
#                 name='tds'),
#             url(
#                 r'^trips/(?P<trip_id>[\w\-]+)/stops/(?P<stop_id>[\w\-]+)/assets$',
#                 order_views.OrderAssets.as_view(),
#                 name='asset-capture-details'),
#             url(
#                 r'^trips/(?P<trip_id>[\w\-]+)/stops/(?P<stop_id>[\w\-]+)/orderlines$',
#                 order_views.OrderLinesView.as_view(),
#                 name='asset-capture-details'),
#             url(
#                 r'^trips/(?P<trip_id>[\w\-]+)/sendotp$',
#                 trip_views.SendOtp.as_view(),
#                 name='send-otp'),
#             url(r'^gpsstatus', driver_views.GpsInformation.as_view(),
#                 name = 'gpsinfo'),
#         ]
#
#         return self.post_process_urls(urlpatterns)
