# Django and System libraries
from django.utils import timezone
from django.db.models import F, Value, CharField, Q
from django.db.models.functions import Concat
from django.conf import settings

# 3rd party libraries
from rest_framework import serializers
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from config.settings.status_pipelines import (
    TRIP_NEW, TRIP_IN_PROGRESS,
    TRIP_END_STATUSES, DRIVER_ACCEPTED, ORDER_NEW, ORDER_OUT_FOR_PICKUP,
    DRIVER_ASSIGNED
)
from blowhorn.apps.driver_app.v2.serializers.order import ShippingAddressSerializer, OrderSerializer
from blowhorn.apps.driver_app.v1.serializers.address import HubSerializer
from blowhorn.apps.driver_app.v2.serializers.contract import TripWorkFlowSerializer
from blowhorn.contract.constants import (
    CONTRACT_TYPE_SME, CONTRACT_TYPE_C2C, CONTRACT_TYPE_KIOSK,
    CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_FLASH_SALE
)
from blowhorn.utils.functions import utc_to_ist
from blowhorn.apps.driver_app.v2.helpers.trip import (
    get_route_trace_for_trip_with_stops,
    get_route_trace_for_trip_without_stops,
    get_route_trace_for_stop,
    get_route_trace_for_stop_using_mongo,
    get_route_trace_for_trip_without_stops_using_mongo,
    get_route_trace_for_trip_with_stops_using_mongo
)
from blowhorn.address.utils import get_city_from_geopoint
from blowhorn.apps.driver_app.v1.helpers.driver import _get_support_number_from_contract
from django.contrib.gis.geos import MultiLineString
from blowhorn.apps.driver_app.v1.serializers.customer import CustomerSerializer
from blowhorn.order.const import PAYMENT_MODE_CASH
from blowhorn.trip.constants import calculate_distance_using, DAH, Mongo, \
    trip_gpsdistance_from, PICKUP, HUB_DELIVERY, HUB_RTO, ORDER_RTO
from django.contrib.gis.geos import LineString, MultiLineString

Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
TripConstants = get_model('trip', 'TripConstants')
SKUCustomerMap = get_model('customer', 'SKUCustomerMap')
Contract = get_model('contract', 'Contract')


class TripStopSerializer(serializers.ModelSerializer):

    shipping_address = serializers.SerializerMethodField(read_only=True)
    hub_order_list = serializers.SerializerMethodField(read_only=True)

    def validate(self, attrs):
        if attrs.get('status') == TRIP_NEW:
            attrs['start_time'] = None
        elif attrs.get('status') == TRIP_IN_PROGRESS:
            attrs['start_time'] = timezone.now()
        elif attrs.get('status') in TRIP_END_STATUSES:
            attrs['end_time'] = timezone.now()
            attrs['calculate_gps_distance'] = True

        return attrs

    def create(self, validated_data):
        stop = Stop(**validated_data)
        stop.save()
        return stop

    def update(self, instance, validated_data):
        for attr in validated_data:
            if attr == 'calculate_gps_distance':
                dah_distance, mongo_distance, dah_route_trace, mongo_route_trace = None, None, LineString([]), LineString([])
                if DAH in calculate_distance_using:
                    dah_route_trace, dah_distance = get_route_trace_for_stop(
                        instance, validated_data.get('geopoint', None))

                if Mongo in calculate_distance_using:
                    mongo_route_trace, mongo_distance = get_route_trace_for_stop_using_mongo(
                        instance, validated_data.get('geopoint', None))

                if trip_gpsdistance_from == DAH:
                    instance.route_trace = dah_route_trace
                    instance.distance = dah_distance
                    instance._route_trace = mongo_route_trace
                    instance._distance = mongo_distance
                else:
                    instance.route_trace = mongo_route_trace
                    instance.distance = mongo_distance
                    instance._route_trace = dah_route_trace
                    instance._distance = dah_distance

            else:
                setattr(instance, attr, validated_data.get(attr))
            # setattr(instance, attr, validated_data.get(attr))

        return instance

    class Meta:
        model = Stop
        fields = '__all__'

    def to_representation(self, instance):
        ret = super(TripStopSerializer, self).to_representation(instance)
        ret['order'] = self.get_order(instance)
        return ret

    def get_order(self, stop):
        order = None
        if stop.waypoint:
            order = stop.waypoint.order
        else:
            order = stop.order
        if not order:
            return {}
        if self.context:
            self.context['stop'] = stop

        return OrderSerializer(order, fields=[
            'id', 'number', 'reference_number', 'status', 'is_assests_captured', 'is_items_verified',
            'pickup_datetime', 'expected_delivery_time',
            'cash_on_delivery', 'shipping_address', 'order_lines', 'otp',
            'return_to_origin'
        ], context=self.context).data

    def get_shipping_address(self, stop):
        if stop.stop_address:
            address = stop.stop_address
        elif not stop.waypoint:
            address = stop.order.shipping_address if stop.order else None
        else:
            address = stop.waypoint.shipping_address if stop.waypoint else None
        if not address:
            return ''
        return ShippingAddressSerializer(address, context=self.context).data

    def get_hub_order_list(self, stop):
        if stop.stop_type in [HUB_DELIVERY, HUB_RTO]:
            stop_orders = stop.stoporders_set.all()
            return [stop_order.order.reference_number for stop_order in stop_orders]


class TripSyncSerializer(serializers.ModelSerializer):
    stops = serializers.SerializerMethodField(read_only=True)
    hub = serializers.SerializerMethodField(read_only=True)#HubSerializer(read_only=True)
    order = serializers.SerializerMethodField(read_only=True)
    trip_workflow = TripWorkFlowSerializer(many=False, required=False)
    is_all_stops_done = serializers.SerializerMethodField(read_only=True)
    is_trip_in_progress = serializers.SerializerMethodField(read_only=True)
    is_loaded = serializers.SerializerMethodField(read_only=True)
    # TODO need to be removed
    customer_name = serializers.SerializerMethodField(read_only=True)
    customer = serializers.SerializerMethodField(read_only=True)
    started_at = serializers.SerializerMethodField(read_only=True)
    completed_at = serializers.SerializerMethodField(read_only=True)
    scheduled_at = serializers.SerializerMethodField(read_only=True)
    support_phone_number = serializers.SerializerMethodField()
    num_of_stops = serializers.SerializerMethodField(read_only=True)
    total_time = serializers.SerializerMethodField(read_only=True)
    total_distance = serializers.SerializerMethodField(read_only=True)
    total_cash_collected = serializers.SerializerMethodField(read_only=True)
    current_step = serializers.SerializerMethodField(read_only=True)
    trip_extra_charge_limit = serializers.SerializerMethodField(read_only=True)
    pickup_order_ref_num = serializers.SerializerMethodField(read_only=True)
    rto_order_ref_num = serializers.SerializerMethodField(read_only=True)

    def __init__(self, *args, **kwargs):
        show_past_trips_data = kwargs.pop('past_trips') if kwargs.get('past_trips', False) else False
        super(TripSyncSerializer, self).__init__(*args, **kwargs)

        if show_past_trips_data:
            for field in list(self.fields.keys()):
                allowed_fields = ['started_at', 'completed_at', 'scheduled_at', 'total_time',
                                  'trip_number', 'customer_name', 'num_of_stops', 'total_distance',
                                  'total_cash_collected']
                if field not in allowed_fields:
                    self.fields.pop(field)
        else:
            unused_fields = ['total_time', 'customer_name', 'num_of_stops', 'total_distance', 'total_cash_collected']
            for unused_field in unused_fields:
                self.fields.pop(unused_field)

    # TODO logic need to be changed
    def get_current_step(self, trip):
        if trip.current_step < 0:
            return trip.current_step
        if trip.status in [TRIP_NEW, DRIVER_ACCEPTED]:
            return 0

        screens = trip.trip_workflow.screens.all().values_list('name', 'sequence')
        screens_dict = {}
        for n, i in enumerate(screens):
            screens_dict[i[0]] = i[1]

        if trip.status == TRIP_IN_PROGRESS:
            # can be finished loading/reached location/finished work
            if 'finished_loading' in screens_dict and not trip.pickup_loading_timestamp:
                return screens_dict['finished_loading']

            if 'reached_stop' in screens_dict:
                no_of_pending_stops, current_stop = self.__get_pending_stops_and_current_stop(trip)
                # is_last_stop = no_of_pending_stops == 1
                # # is_second_last_stop = no_of_pending_stops == 2
                # is_c2c_trip = trip.customer_contract.contract_type == 'C2C'

                if 'finished_stop' in screens_dict:
                    if current_stop.reach_time:
                        # if is_last_stop and is_c2c_trip:
                        #     return screens_dict['end_trip']
                        return screens_dict['finished_stop']
                    else:
                        return screens_dict['reached_stop']
                else:
                    if not current_stop:
                        return screens_dict['end_trip']
                    return screens_dict['reached_stop']
            else:
                # b2b case
                return screens_dict['end_trip']
        elif trip.is_all_stops_done():
            return screens_dict['end_trip']
        return trip.current_step

    def get_total_cash_collected(self, trip):
        return trip.order.total_payable \
            if trip.order and trip.order.order_type == settings.C2C_ORDER_TYPE and \
            trip.order.payment_mode == PAYMENT_MODE_CASH else None

    def get_num_of_stops(self, trip):
        return len(trip.stops.all())

    def get_hub(self, trip):
        hub = trip.hub
        if not hub and not trip.order:
            stop = trip.stops.first()
            #send pickup address as hub address
            # to work with shipment flow for kiosk
            hub = stop.order.pickup_address

            return HubSerializer(hub).data
        if hub:
            return HubSerializer(hub).data
        return None

    def get_trip_extra_charge_limit(self, trip):
        return float(TripConstants().get('TRIP_EXTRA_CHARGE_LIMIT', 200))

    def __get_pending_stops_and_current_stop(self, trip):
        """
        No dependency on status(In-Progress, New) of current stop and next stop.

        hence won't fail in case of multiple requests
        """
        stop_qs = trip.stops.exclude(status__in=TRIP_END_STATUSES)
        no_of_pending_stops = len(stop_qs)
        if no_of_pending_stops > 0:
            current_stop = trip.stops.filter(status=TRIP_IN_PROGRESS).first()
            return no_of_pending_stops, current_stop
        return no_of_pending_stops, None

    def get_total_time(self, trip):
        time = trip.total_time
        if time:
            return '%s hr %s min' % (int(time / 60), int(time % 60))
        return '0 hr 0 min'

    def get_total_distance(self, trip):
        total_distance = trip.total_distance
        if total_distance:
            return "%.1f" % total_distance
        return 0

    def get_support_phone_number(self, trip):
        contract_id = trip.customer_contract.id if trip.customer_contract else None
        return _get_support_number_from_contract(contract_id)

    def get_stops(self, trip):
        self.context['trip'] = trip
        return TripStopSerializer(trip.stops.all().order_by('sequence'), many=True, context=self.context).data

    def get_order(self, trip):
        order_list = trip.stops.filter(order__has_pickup=True).values_list(
            'order__reference_number', flat=True)

        if trip.order:
            return OrderSerializer(trip.order, context=self.context).data
        return None

    def get_is_loaded(self, trip):
        screen_names = trip.trip_workflow.screens.all().values_list('name', flat=True) if trip.trip_workflow else None
        if screen_names and 'finished_loading' in screen_names:
            return True if trip.pickup_loading_timestamp else False
        return True

    def get_is_all_stops_done(self, trip):
        return trip.is_all_stops_done()

    def get_is_trip_in_progress(self, trip):
        return trip.is_trip_in_progress()

    # TODO need to be removed
    def get_customer_name(self, trip):
        if trip.order and trip.order.order_type == settings.C2C_ORDER_TYPE:
            return trip.order.customer.name
        return trip.customer_contract.customer.name if trip.customer_contract else ''

    def get_customer(self, trip):
        if trip.customer_contract and trip.customer_contract.contract_type in [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME,
                                                                               CONTRACT_TYPE_KIOSK, CONTRACT_TYPE_FLASH_SALE]:
            customer = trip.order.customer if trip.order else None
        else:
            customer = trip.customer_contract.customer if trip.customer_contract else None
            if customer:
                return {
                    'name': customer.name,
                    'phone_number': ''
                }
            return {}

        if customer:
            return CustomerSerializer(instance=customer).data

        return {}

    def get_started_at(self, trip):
        if trip.actual_start_time:
            return utc_to_ist(trip.actual_start_time).strftime(settings.APP_DATETIME_FORMAT)
        return ''

    def get_scheduled_at(self, trip):
        if trip.planned_start_time:
            return utc_to_ist(trip.planned_start_time).strftime(settings.APP_DATETIME_FORMAT)
        return ''

    def get_completed_at(self, trip):
        if trip.actual_end_time:
            return utc_to_ist(trip.actual_end_time).strftime(settings.APP_DATETIME_FORMAT)
        return ''

    def get_pickup_order_ref_num(self, trip):
        return [x.order.reference_number for x in
                trip.stops.filter(stop_type=PICKUP,
                                  status__in=[TRIP_NEW, TRIP_IN_PROGRESS])]

    def get_rto_order_ref_num(self, trip):
        return [x.order.reference_number for x in
                trip.stops.filter(stop_type=ORDER_RTO,
                                  status__in=[TRIP_NEW, TRIP_IN_PROGRESS])]

    class Meta:
        model = Trip
        fields = ('id', 'trip_number', 'status', 'is_all_stops_done', 'is_trip_in_progress',
                  'started_at', 'completed_at', 'scheduled_at', 'current_step', 'is_loaded',
                  'support_phone_number', 'customer', 'order', 'hub', 'stops', 'trip_workflow',
                  'trip_extra_charge_limit', 'total_time', 'customer_name', 'num_of_stops',
                  'total_distance', 'total_cash_collected', 'pickup_order_ref_num',
                  'rto_order_ref_num')


class TripSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        if attrs.get('status') == TRIP_IN_PROGRESS:
            attrs['actual_start_time'] = timezone.now()
        if attrs.get('status') in TRIP_END_STATUSES:
            attrs['actual_end_time'] = timezone.now()
            attrs['calculate_gps_distance'] = True
        if attrs.get('status') == DRIVER_ACCEPTED:
            attrs['trip_acceptence_time'] = timezone.now()

        if attrs.get('meter_reading_start'):
            attrs['mt_start_time'] = timezone.now()
        if attrs.get('meter_reading_end'):
            attrs['mt_end_time'] = timezone.now()

        return attrs

    def create(self, validated_data):
        trip = Trip(**validated_data)
        trip.save()
        return trip

    def update(self, instance, validated_data):
        for attr in validated_data:
            if attr == 'meter_reading_end':
                instance.store_vehicle_last_meter_reading(
                    validated_data.get(attr))

            if attr == 'calculate_gps_distance':
                dah_distance, mongo_distance, dah_route_trace, mongo_route_trace = None, None, MultiLineString([]), MultiLineString([])
                if not instance.stops.all().exists():
                    if DAH in calculate_distance_using:
                        dah_route_trace, dah_distance = get_route_trace_for_trip_without_stops(instance)
                        if dah_route_trace:
                            dah_route_trace = MultiLineString([dah_route_trace, ])
                        else:
                            dah_route_trace = MultiLineString([])
                    if Mongo in calculate_distance_using:
                        mongo_route_trace, mongo_distance = get_route_trace_for_trip_without_stops_using_mongo(instance)
                        if mongo_route_trace:
                            mongo_route_trace = MultiLineString([mongo_route_trace, ])
                        else:
                            mongo_route_trace = MultiLineString([])
                else:
                    if DAH in calculate_distance_using:
                        dah_route_trace, dah_distance = get_route_trace_for_trip_with_stops(instance)
                    if Mongo in calculate_distance_using:
                        mongo_route_trace, mongo_distance = get_route_trace_for_trip_with_stops_using_mongo(instance)

                if trip_gpsdistance_from == DAH:
                    instance.route_trace = dah_route_trace
                    instance.gps_distance = dah_distance
                    instance._route_trace = mongo_route_trace
                    instance._gps_distance = mongo_distance
                else:
                    instance.route_trace = mongo_route_trace
                    instance.gps_distance = mongo_distance
                    instance._route_trace = dah_route_trace
                    instance._gps_distance = dah_distance
            else:
                setattr(instance, attr, validated_data.get(attr))
        return instance

    class Meta:
        model = Trip
        fields = '__all__'


class CreateTripOnTheRunSerializer(serializers.ModelSerializer):
    sku_list = serializers.SerializerMethodField(read_only=True)

    def get_sku_list(self, trip):
        query = Q(status=CONTRACT_STATUS_ACTIVE)
        geopoint = self.context.get('geopoint')
        if geopoint:
            city = get_city_from_geopoint(geopoint)
            query = query & Q(city=city)

        return SKUCustomerMap.objects.filter(customer__in=Contract.objects.filter(query).values('customer')).annotate(
            sku_name=Concat(F('sku__name'), Value('-'), F('customer__name'), output_field=CharField()),
            customer_id=F('customer__id')).values('sku_name', 'customer_id')

    class Meta:
        model = Trip
        fields = ('id', 'sku_list')
