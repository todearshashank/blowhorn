"""Trip Related Helper functions to be added here."""
# System and Django libraries
import logging
from django.contrib.gis.geos import LineString, MultiLineString
from django.utils import timezone
from django.db.models import Q, Count
from datetime import timedelta
from django.contrib.gis.db.models.functions import Length

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model

# Blowhorn libraries
from blowhorn.address.utils import AddressUtil
from blowhorn.utils.functions import geopoints_to_km
from config.settings.status_pipelines import TRIP_NEW
from blowhorn.apps.mixins import CreateModelMixin
from blowhorn.driver.tasks import push_notification_to_driver_app
from blowhorn.activity.mixins import ActivityMixin
from blowhorn.trip.constants import DAH, Mongo, trip_gpsdistance_from
from blowhorn.order.models import OrderContainer, Container


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
AppEvents = get_model('trip', 'AppEvents')
DriverActivity = get_model('driver', 'DriverActivity')
DriverActivityHistory = get_model('driver', 'DriverActivityHistory')
DriverVehicleMap = get_model('driver', 'DriverVehicleMap')


def calculate_route_trace_and_distance(stop, trip, geopoint=None,
                                       start_time=None, end_time=None):
    """Calculate `route_trace` and `distance` for any `stop`given the `stop` and `trip`."""

    # start_date = timezone.now().date() - timedelta(days=1)
    # end_date = timezone.now().date()
    #
    # if start_time and end_time:
    #     query_params = Q(created_time__range=(start_time, end_time))
    # else:
    #     query_params = Q(created_time__date__range=(start_date, end_date))
    #
    # query_params = query_params & Q(trip=trip) & Q(driver=trip.driver)

    query_params = Q(trip=trip) & Q(driver=trip.driver)

    if stop:
        query_params = query_params & Q(stop=stop)

    activity_history = DriverActivityHistory.objects.filter(
        query_params
    ).only('gps_timestamp', 'location_accuracy_meters', 'geopoint',
           'vehicle_speed_kmph')

    route_trace, activity_geopoints = LineString([]), []
    distance = 0

    if len(activity_history) > 0:
        processed_geopoints = AddressUtil().get_processed_geopoints(activity_history)
        activity_geopoints = [x.geopoint for x in processed_geopoints]

    activity_gpt_len = len(activity_geopoints)

    if activity_geopoints and activity_gpt_len > 1:
        # if there are no activity geopoints or count is less than 2 do not update route trace
        distance = geopoints_to_km([x for x in activity_geopoints])
        route_trace = LineString(activity_geopoints)

    return route_trace, distance


def get_route_trace_for_trip_without_stops(trip, start_time=None,
                                           end_time=None):
    """Calculate `route_trace` and `distance` for any `trip` without 'stops'`."""
    return calculate_route_trace_and_distance(stop=None, trip=trip,
                                              start_time=start_time,
                                              end_time=end_time)


def get_route_trace_for_trip_with_stops(trip):
    """Calculate route trace for trip with stops."""
    total_distance = 0
    route_trace = MultiLineString([])

    if trip_gpsdistance_from == DAH:
        trip_stop = Stop.objects.filter(trip=trip, route_trace__isnull=False)
        route_trace_field = 'route_trace'
        distance_field = 'distance'
    else:
        trip_stop = Stop.objects.filter(trip=trip, _route_trace__isnull=False)
        route_trace_field = '_route_trace'
        distance_field = '_distance'
    if trip_stop.exists():
        stop_route = trip_stop.values_list(route_trace_field, flat=True)
        if stop_route:
            route_trace = MultiLineString(list(stop_route))
            total_distance = sum(trip_stop.values_list(distance_field, flat=True))

    if total_distance == 0:
        calc_route_trace, total_distance = get_route_trace_for_trip_without_stops(
            trip)
        if calc_route_trace:
            route_trace = MultiLineString([calc_route_trace, ])

    logger.info('Route Trace Calculation for trip:- "%s" successful' % trip)

    return route_trace, total_distance


def get_route_trace_for_stop(stop, geopoint=None):
    """Calculate route trace for stops."""
    return calculate_route_trace_and_distance(stop=stop, trip=stop.trip, geopoint=geopoint)


def create_app_event(event, geopoint, **kwargs):
    """Create App Event for actions triggered from the driver app."""
    AppEvents.objects.create(
        event=event,
        order=kwargs.get('order', None),
        trip=kwargs.get('trip', None),
        driver=kwargs.get('driver', None),
        geopoint=geopoint if geopoint else None,
        hub_associate=kwargs.get('hub_associate', None),
        stop=kwargs.get('stop', None)
    )


def create_stop(trip, **kwargs):
    """Create Stop for a trip."""
    from blowhorn.apps.driver_app.v2.serializers.trip import TripStopSerializer
    stop_data = {
        'trip': trip.id,
    }

    for attr in kwargs:
        stop_data[attr] = kwargs.get(attr, None)
    if not stop_data.get('status'):
        stop_data['status'] = TRIP_NEW

    stop = CreateModelMixin().create(data=stop_data, serializer_class=TripStopSerializer)

    logger.info('Stop %d created successfully for order:- "%s"', stop.id, kwargs.get('order', None))
    return stop


def create_trip(driver_id, **kwargs):
    """Create trip."""
    from blowhorn.apps.driver_app.v2.serializers.trip import TripSerializer

    dv_map = DriverVehicleMap._default_manager.filter(driver_id=driver_id).first()
    if not dv_map:
        return False

    trip_data = {
        'vehicle': dv_map.vehicle_id,
        'driver': driver_id
    }
    for attr in kwargs:
        trip_data[attr] = kwargs.get(attr, None)
    if not trip_data.get('status'):
        trip_data['status'] = TRIP_NEW
    if not trip_data.get('planned_start_time'):
        trip_data['planned_start_time'] = timezone.now()

    trip = CreateModelMixin().create(
        data=trip_data,
        serializer_class=TripSerializer
    )
    if not trip:
        logger.error('Trip creation Failed for driver:- "%s"', driver_id)
    else:
        push_notification_to_driver_app.apply_async((driver_id, 'new_trip'),
                                                    eta=timezone.now() + timedelta(minutes=0.2))
        logger.info('Trip:- "%s" created successfully for driver:- "%s"', trip, driver_id)
    return trip


"""
###########################################################################################
route trace calculation using Mongo
###########################################################################################
"""


def calculate_route_trace_and_distance_using_mongo(trip, stop=None, geopoint=None):
    """Calculate `route_trace` and `distance` for any `stop`given the `stop` and `trip`."""
    route_trace, activity_geopoints = LineString([]), []
    distance = 0

    if not trip:
        return route_trace, distance

    activity_geopoints = ActivityMixin().get_prior_route(trip=trip, stop=stop)
    if len(activity_geopoints) > 1:
        # if there are no activity geopoints or count is less than 2 do not update route trace
        distance = geopoints_to_km([x for x in activity_geopoints])
        route_trace = LineString(activity_geopoints)

    return route_trace, distance


def get_route_trace_for_trip_without_stops_using_mongo(trip):
    """Calculate `route_trace` and `distance` for any `trip` without 'stops'`."""
    return calculate_route_trace_and_distance_using_mongo(stop=None, trip=trip)


def get_route_trace_for_trip_with_stops_using_mongo(trip):
    """Calculate route trace for trip with stops."""
    total_distance = 0
    route_trace = MultiLineString([])

    if trip_gpsdistance_from == Mongo:
        trip_stop = Stop.objects.filter(
            trip=trip, route_trace__isnull=False
        ).annotate(g_len=Length('route_trace')).filter(g_len__gt=0)
        route_trace_field = 'route_trace'
        distance_field = 'distance'
    else:
        trip_stop = Stop.objects.filter(trip=trip, _route_trace__isnull=False)
        route_trace_field = '_route_trace'
        distance_field = '_distance'
    if trip_stop.exists():
        stop_route = trip_stop.values_list(route_trace_field, flat=True)
        if stop_route:
            route_trace = MultiLineString(list(stop_route))
            total_distance = sum(trip_stop.values_list(distance_field, flat=True))

    if total_distance == 0:
        calc_route_trace, total_distance = get_route_trace_for_trip_without_stops_using_mongo(
            trip)
        if calc_route_trace:
            route_trace = MultiLineString([calc_route_trace, ])

    logger.info('Route Trace Calculation for trip:- "%s" successful' % trip)

    return route_trace, total_distance


def get_route_trace_for_stop_using_mongo(stop, geopoint=None):
    """Calculate route trace for stops."""
    return calculate_route_trace_and_distance_using_mongo(stop=stop, trip=stop.trip, geopoint=geopoint)


def get_asset_for_trip(trip):
    assets = OrderContainer.objects.filter(trip=trip,
        container__container_type__return_asset=True).values(
        'container__container_type__name').annotate(
        cont_type_count=Count('container'))

    asset_msg = ''

    if trip.driver.cod_balance > 0:
        asset_msg = asset_msg + ' Rs. ' + str(trip.driver.cod_balance)

    return asset_msg
