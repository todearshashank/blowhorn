import logging
import traceback

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.decorators import method_decorator

from rest_framework.decorators import permission_classes
from rest_framework import status, generics, views
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.authentication import BasicAuthentication

from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.common.decorators import verify_app_code
from blowhorn.apps.driver_app.v3.services.driver.login import LogIn
from blowhorn.apps.driver_app.v3.serializers.driver import DriverLoginSerializer
from blowhorn.apps.driver_app.v4.helpers.driver import verify_user
from blowhorn.common import sms_templates
from blowhorn.common.communication import Communication
from blowhorn.users.models import User

from config.settings import status_pipelines as StatusPipeline

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([])
class Login(generics.CreateAPIView, LogIn):
    """
    Driver Authentication to be done here.

    through 1. QR Code Scan or 2. mobile + password

    Deletes all user sessions and django sessions.

    Saves the new session for user.

    POST(phone_number, qr_code data, password, device_id, fcm_id, device_details, location_details)

    Updates Driver Device details and driver log info
    and the response with the user token to be provided.

    On Success: Returns User session token, Firebase Auth token and driver Info with 200 status
    On Failure: Returns 500 with the error.

    """

    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)
    serializer_class = DriverLoginSerializer

    @verify_app_code(login_url='/', redirect_field_name='', message='Update the app from playstore..!')
    def post(self, request):
        """POST method for driver login changes."""
        data = request.data.dict()

        self._initialize_attributes(data)
        try:
            self._login()
        except BaseException as e:
            self.error_message = ('Error: %s, Call Support' % e)
            logger.error(traceback.format_exc())
            self.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR

        message = {'message': self.error_message} if self.error_message else self.response_data

        return Response(status=self.status_code, data=message,
                        content_type='text/html; charset=utf-8')


class SendOTP(generics.CreateAPIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        phone_number = data.get('mobile_number', None)
        is_vendor = True if data.get('is_vendor') == 'true' else False

        email = '%s@%s' % (phone_number,
                           settings.DEFAULT_DOMAINS.get('driver') if not is_vendor else \
                               settings.DEFAULT_DOMAINS.get('vendor'))
        user = User.objects.filter(email=email).prefetch_related(
            'driver__user', 'vendor__user').first()
        response_data = {}

        is_valid_user, response_message = verify_user(user, is_vendor)

        if not is_valid_user:
            response_data['message'] = response_message
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

        stored_otp, created = User.objects.retrieve_otp(user)
        if stored_otp:
            if settings.ENABLE_SLACK_NOTIFICATIONS:
                template_dict = sms_templates.driver_vendor_login_otp
                Communication.send_sms(sms_to=user.phone_number.national_number,
                                       message=template_dict['text'] % (user.name, stored_otp),
                                       priority='high', template_dict=template_dict)
            response_data['message'] = _('OTP sent successfully')
            return Response(status=status.HTTP_200_OK, data=response_data)

        response_data['message'] = _('No user found')
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response_data)

