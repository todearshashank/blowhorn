from django.conf.urls import url
from blowhorn.apps.driver_app.v4.views import driver as driver_views


urlpatterns = [
            # driver views apis
            url(r'^login$', driver_views.Login.as_view(), name='driver-login-v4'),
            url(r'^login/otp', driver_views.SendOTP.as_view(), name='password-reset-otp'),
    ]
