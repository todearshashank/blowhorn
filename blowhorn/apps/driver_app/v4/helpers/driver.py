from config.settings import status_pipelines as StatusPipeline


def verify_user(user, is_vendor):

    if not user:
        _user = 'Fleet Owner' if is_vendor else 'Driver'
        return False, 'Your mobile number is not registered as %s, Please signup your account' % _user
    partner = None

    if hasattr(user, 'vendor') and not is_vendor:
        return False, 'You are already registered as Fleet owner. Login using Fleet Owner'

    if hasattr(user, 'driver') and is_vendor:
        return False, 'You are already registered as Driver. Login using Driver'

    if hasattr(user, 'vendor') and is_vendor:
        partner = user.vendor
    elif hasattr(user, 'driver') and not is_vendor:
        partner = user.driver

    if not partner:
        return False, 'Your mobile number is not registered, Please signup your account'

    if partner.status in [StatusPipeline.INACTIVE, StatusPipeline.BLACKLISTED]:
        return False, 'You are not an active driver, contact support to login'

    return True, 'Valid user'
