# from django.apps import AppConfig
# from django.conf.urls import url
#
# from blowhorn.apps.driver_app.v1.app import application as v1_driver_app
# from blowhorn.apps.driver_app.v2.app import application as v2_driver_app
# from blowhorn.apps.driver_app.v3.app import application as v3_driver_app
#
#
# class DriverVersionedApplication(AppConfig):
#     def get_urls(self):
#         urlpatterns = [
#             url(r'^v1/', v1_driver_app.urls),
#             url(r'^v2/', v2_driver_app.urls),
#             url(r'^v3/', v3_driver_app.urls),
#         ]
#         return self.post_process_urls(urlpatterns)
