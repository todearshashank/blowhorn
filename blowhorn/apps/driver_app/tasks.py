"""All taks to be added here needed because of any action done from driver app."""
# System and Django imports
from __future__ import absolute_import, unicode_literals
import requests
import logging
import traceback
from django.conf import settings

# 3rd party libraries
from celery import shared_task

# blowhorn imports
from blowhorn import celery_app

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@celery_app.task()
def send_unallocated_orders_slack_notification(data):
    """Task to send slack notification in Channel: unallocated-orders if driver rejects/cancels the trip from app."""
    if settings.ENABLE_SLACK_NOTIFICATIONS:
        _slack_url = settings.TRIP_REJECTION_SLACK_URL
        _slack_channel = settings.TRIP_REJECTION_SLACK_CHANNEL
    else:
        _slack_url = getattr(settings, 'SLACK_URL', None)
        _slack_channel = getattr(settings, 'SLACK_CHANNELS', {}).get('PAYMENTS', None)

    message = data.get('message', None)
    order_type = data.get('order_type', None)
    order_id = data.get('order_id', None)
    order_number = data.get('order_number', None)
    trip_number = data.get('trip_number', None)
    hub_name = data.get('hub_name', None)
    customer_name = data.get('customer_name', None)
    contract_name = data.get('contract_name', None)
    driver_details = data.get('driver_details', None)
    reason = data.get('reason', None)
    track_url_host = data.get('track_url_host', 'blowhorn.com')

    order_type_dict = {
        settings.C2C_ORDER_TYPE: 'bookingorder',
        settings.SUBSCRIPTION_ORDER_TYPE: 'fixedorder',
        settings.SPOT_ORDER_TYPE: 'spotorder',
        settings.SHIPMENT_ORDER_TYPE: 'shipmentorder',

    }
    track_url = 'http://%s/admin/order/%s/%s/change/' % (
        track_url_host,
        order_type_dict.get(order_type),
        order_id
    )

    slack_message = {
        #            "text": title,
        "attachments": [
            {
                "author_name": 'Order:%s | Trip:%s | Customer:%s | Contract:%s | Hub:%s | Reason:%s' % (
                    order_number, trip_number, customer_name,
                    contract_name, hub_name, reason if reason else 'not captured'),
                "title": '%s Click here to assign driver to the Order' % message,
                "title_link": track_url,
                "text": 'Driver : %s' % (driver_details),
                "color": "good"
            }
        ]
    }

    slack_message.update({'channel': _slack_channel})
    try:
        requests.post(url=_slack_url, json=slack_message)
    except:
        logging.error(traceback.format_exc())
