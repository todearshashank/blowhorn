# System and Django libraries
import re
import json
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib.gis.geos import MultiLineString
from django.db import transaction
from django.contrib.admin import SimpleListFilter
from django.db.models import Q
from django.contrib.gis.measure import Distance

from blowhorn.oscar.core.loading import get_model

# Blowhorn imports
from blowhorn.address.models import City, Hub, State
from blowhorn.address.serializer import CityModelSerializer
from blowhorn.common.helper import CommonHelper
from blowhorn.address.constants import PINCODE_REGEX, DEFAULT_CITY, \
    DEFAULT_LOADING_TIME_MINUTES, DEFAULT_UNLOADING_TIME_MINUTES
from blowhorn.utils.functions import smoothen_geopoints
from blowhorn.contract.models import Contract
from blowhorn.contract.constants import CONTRACT_STATUS_DRAFT
from blowhorn.common.redis_helper import StrictRedisHelper


ShippingAddress = get_model('order', 'ShippingAddress')

class AddressUtil(object):

    def updateMapWidgetOptions(self, options, widgetName):
        widget_settings = list(settings.MAP_WIDGETS[widgetName])
        for key in options:
            widget_settings.append((key, options[key]))

        updated_settings = {
            widgetName: tuple(widget_settings)
        }
        return updated_settings

    def get_state_from_code(self, code):
        """
        :param code: 2-digit state code
        :return: instance of State
        """
        return State.objects.filter(gst_code=code)

    def get_state_from_city(self, city):
        state = State.objects.filter(cities=city)
        if state.exists():
            return state.first()
        return None

    def get_latlng_from_geopoint(self, geopoint):
        return {
            'lat': geopoint.y,
            'lng': geopoint.x,
            'lon': geopoint.x
        }

    def get_contact_from_address(self, address):
        phone_number = address.phone_number.national_number if address.phone_number else ''
        return {
            'name': address.first_name,
            'mobile': str(phone_number),
            'email': ''
        }

    def get_full_address_from_dict(self, address_dict):
        return ', '.join(address_dict.values())

    def get_postcode_from_address_str(self, addr_str):
        address_group = re.match(PINCODE_REGEX, addr_str)
        postcode = None
        if address_group:
            postcode = address_group.groupdict()['zipcode']
        return postcode

    def get_processed_geopoints(self, locations):
        sorted_points = sorted(locations, key=lambda x: x.gps_timestamp)
        smoothened_points = smoothen_geopoints(sorted_points)
        return smoothened_points

    def convert_multilinestring_to_latlong(self, stored_route_trace):
        """
        :param stored_route_trace: route_trace from Trip (MultiLineString)
        :return: route_trace in latlong format (list of lists segragated by stops)
        """
        route_trace = []
        for r in stored_route_trace.coords:
            stop_trace = [{
                'lat': p[1],
                'lng': p[0]
            } for p in r]
            route_trace.append(stop_trace)

        return route_trace


    def get_route_trace_from_multilinestring(self, geopoints_array):
        """
        :param geopoints_array: route_trace from Trip (MultiLineString)
        :return: route_trace in latlong format (list of dict)
            route trace from all stops are merged into single polyline
        """
        route_trace = []
        if not isinstance(geopoints_array, MultiLineString):
            return route_trace

        for r in geopoints_array.coords:
            stop_trace = [{
                'lat': p[1],
                'lng': p[0],
                'latitude': p[1],
                'longitude': p[0]
            } for p in r]

            route_trace.extend(stop_trace)

        return route_trace

    def get_national_number_from_user_phone(self, user):
        phone_number = ''
        if user.phone_number:
            phone_number = '%s' % user.national_number
        return phone_number

    def get_loading_unloading_time(self, city=None):
        """
        Returns loading/unloading time for given city, if no city return for
        Bengaluru city
        """

        try:
            if not city:
                city = City.objects.get(pk=1)

            """if no configuration exists set default"""
            if city:
                loading_time = city.loading_mins
                unloading_time = city.unloading_mins
            else:
                loading_time = DEFAULT_LOADING_TIME_MINUTES
                unloading_time = DEFAULT_UNLOADING_TIME_MINUTES
        except:
            loading_time = DEFAULT_LOADING_TIME_MINUTES
            unloading_time = DEFAULT_UNLOADING_TIME_MINUTES

        return loading_time, unloading_time


def get_city_from_geopoint(geopoint, field=None):
    """
    :param geopoint: Point obj
    :param field: field_name (coverage_pickup, coverage_dropoff, shipment_coverage)
    :return:
    """
    if field is None:
        field = 'coverage_dropoff'

    if field not in ['coverage_dropoff', 'coverage_pickup', 'shipment_coverage']:
        return None

    param = {
        '%s__contains' % field: geopoint
    }
    return City._default_manager.filter(**param).first()


def get_hub_from_geopoint(geopoint, customer=None):
    """
    :param geopoint: Point obj
    :param customer: instance of Customer (optional)
    :return: instance of hub
    """
    params = {
        'coverage__contains': geopoint
    }
    if customer is not None:
        params['customer'] = customer
    else:
        params['customer__isnull'] = True

    return Hub._default_manager.filter(**params).first()


def get_hub_from_pincode(pincode, customer=None):
    """
    Returns Hub using pincode and customer
    :param pincode: pincode (int)
    :param customer: instance of Customer (optional)
    """
    params = {
        'pincodes__contains': [pincode],
    }
    if customer is not None:
        params['customer'] = customer
    else:
        params['customer__isnull'] = True
    return Hub._default_manager.filter(**params).first()


def get_hub_from_store_code(store_code, customer=None, pincode=None):
    return Hub._default_manager.filter(
        store_code=store_code,
        customer=customer,
        pincodes__contains=[pincode]).first()


def get_city_from_pincode(pincode):
    params = {
        'pincodes__contains': [pincode],
    }
    hub =  Hub._default_manager.filter(**params).first()
    return hub.city if hub else None


def get_city(order_type, **kwargs):
    geopoint = kwargs.get('geopoint', None)
    postcode = kwargs.get('postcode', None)
    hub = kwargs.get('hub', None)
    city = None
    if geopoint:
        city = get_city_from_geopoint(geopoint)

    if not city and order_type == settings.SHIPMENT_ORDER_TYPE and postcode:
        if not hub:
            customer = kwargs.get('customer', None)
            contract_id = kwargs.get('contract_id', None)
            contract = Contract.objects.filter(id=contract_id).first()
            if contract and contract.use_customer_hub and customer:
                hub = get_hub_from_pincode(postcode, customer=customer)
            else:
                hub = get_hub_from_pincode(postcode)
        city = hub.city if hub else None
    return city


def get_cities_geojson(cities=None):
    if cities is None:
        cities = City.objects.all()
    city_serializer = CityModelSerializer(
        cities,
        many=True,
        context={
            'fields': ('id', 'name', 'coverage_pickup', 'coverage_dropoff', 'shipment_coverage',
                       'future_booking_days', 'contact', 'address')
        }
    )
    return city_serializer.data


def get_city_contact_details():
    cities_json = get_cities_geojson()
    city_contacts = {}
    for city in cities_json:
        city_name = city.get('name')
        city_contacts[city_name] = city.get('contact')
    return city_contacts


def get_coverage_json(coordinates):
    latlon = []
    if coordinates:
        for c in coordinates[0]:
            latlon.append({
                'lat': float(c[1]),
                'lon': float(c[0]),
                'lng': float(c[0])
            })
    return latlon


def get_city_details(cities=None):
    cities_json = get_cities_geojson(cities=cities)
    city_coverage = {}
    city_contacts = {}
    for city in cities_json:
        city_name = city.get('name')
        city_contacts[city_name] = city.get('contact')

        coverage_pickup = city.get('coverage_pickup')
        coverage_dropoff = city.get('coverage_dropoff')
        shipment_coverage = city.get('shipment_coverage') or {}
        city_coverage[city_name] = {
            'coverage': {
                'pickup': get_coverage_json(coverage_pickup.get('coordinates')),
                'dropoff': get_coverage_json(coverage_dropoff.get('coordinates')),
                'shipment': get_coverage_json(shipment_coverage.get('coordinates')),
            }
        }

    return city_coverage, city_contacts


def get_city_coverage(cities=None):
    cities_json = get_cities_geojson(cities=cities)
    cities = []
    for city in cities_json:
        city_id = city.get('id')
        city_name = city.get('name')

        coverage_pickup = city.get('coverage_pickup')
        coverage_pickup = get_coverage_json(coverage_pickup.get('coordinates'))

        coverage_dropoff = city.get('coverage_dropoff')
        coverage_dropoff = get_coverage_json(coverage_dropoff.get('coordinates'))

        shipment_coverage = city.get('coverage_dropoff')
        shipment_coverage = get_coverage_json(shipment_coverage.get('coordinates'))

        cities.append({
            'city_name': city_name,
            'id': city_id,
            'coverage':  {
                'pickup': coverage_pickup,
                'dropoff': coverage_dropoff,
                'shipment': shipment_coverage
            }
        })

    return cities


def get_full_address_str(addr):
    return ', '.join(addr.active_address_fields())


def get_city_from_latlong(latitude=None, longitude=None, field=None):
    if latitude == 'null' or longitude == 'null':
        latitude, longitude = None, None

    if latitude and longitude:
        geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)
        return get_city_from_geopoint(geopoint, field=field)
    else:
        return None


def within_city_working_hours(city, pickup_time):
    """
    :param city: instance of City
    :param pickup_time: time object
    :return: Boolean
    """
    if city:
        if city.start_time <= pickup_time and city.end_time >= pickup_time:
            return True
    return False


def get_operating_cities(location_details):
    operating_cities = []
    location_details = location_details if location_details else {}
    latitude = location_details.get('mLatitude', None)
    longitude = location_details.get('mLongitude', None)

    current_city = get_city_from_latlong(latitude, longitude)
    if current_city:
        operating_cities = list(City.objects.all().exclude(
            name=current_city.name).values_list('name', flat=True))
        operating_cities.insert(0, current_city.name)
    else:
        operating_cities = list(City.objects.all().exclude(
            name=settings.DEFAULT_CITY).values_list('name', flat=True))
        operating_cities.insert(0, settings.DEFAULT_CITY)

    return operating_cities


def get_state_list():
    states = State.objects.all().values_list('name', flat=True)
    return states


def hub_qr_code_generator():
    return CommonHelper().generate_random_string(20)


def get_hubs_for_user(user):
    """
    fetch hubs for user with role hub associate
    """
    return Hub.objects.filter(associates=user).values('id', 'name')


def get_hub_contacts_from_geopoint(trip, geopoint):
    return list(set(Hub.objects.filter(customer=trip.customer_contract.customer,
                             address__geopoint__distance_lte=(
                             geopoint, Distance(m=100))).values_list(
        'customer_hub_contact__mobile', flat=True)))


def get_shipping_addr_hub(hub):
    ship_addr = {
        'line1': hub.address.line1,
        'line2': hub.address.line2,
        'line3': hub.address.line3,
        'line4': hub.address.line4,
        'state': hub.address.state,
        'postcode': hub.address.postcode,
        'country': hub.address.country,
        'geopoint': hub.address.geopoint
    }
    # TODO: add shipping address to hub if not there
    addr = ShippingAddress.objects.create(**ship_addr)
    return addr


class HubFilter(SimpleListFilter):
    """
    Customized hub list filter
    Show only logged in user associated contract hubs
    """
    title = 'Hub'
    parameter_name = 'hub'

    def lookups(self, request, model_admin):
        contracts = Contract.objects.exclude(status=CONTRACT_STATUS_DRAFT)
        if not request.user.is_superuser:
            contracts = contracts.filter(
                Q(spocs=request.user) | Q(supervisors=request.user)
            ).distinct()
            '''
            Fetch the hubs for logged-in user from associated customer contract
            based on hub and contract city
            # Resource allocation is not available for spoc contract
            # so better to get hub from customer itself
            '''
            customers = []
            cities = []
            for contract in contracts:
                if contract.customer:
                    customers.append(contract.customer.id)
                cities.append(contract.city.id)

            # Contract always have city
            hubs = Hub.objects.filter(city__in=list(cities))
            # c2c contract not having customers, B2B have customers
            if customers:
                hubs = hubs.filter(customer_id__in=list(customers))

            return [(hub.id, hub) for hub in hubs]

        else:
            hubs = Hub.objects.all()
            return [(hub.id, hub) for hub in hubs]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(hub=self.value())


@receiver(post_save, sender=State)
def update_state_city_cache(sender, instance, **kwargs):
    update_flag = True
    transaction.on_commit(lambda: state_city_cache(update_flag))


def state_city_cache(update_flag):
    conn = StrictRedisHelper().get_connection()
    cache_data = conn.get("state_city_map")

    if not cache_data or update_flag:
        states = State.objects.filter(cities__isnull=False).values_list('id', 'cities')
        state_cities = {}
        for state in states:
            if not state[0] in state_cities:
                state_cities[state[0]] = [state[1]]
            else:
                state_cities.update({state[0]: state_cities[state[0]] + [state[1]]})
        conn.set("state_city_map", json.dumps(state_cities))
    state_map = json.loads(conn.get("state_city_map"))
    return state_map


def get_wms_hub_for_customer(customer, site_id=None):
    if not customer:
        return None

    if site_id:
        return Hub.objects.filter(customer=customer, id=site_id, is_wms_site=True)
    else:
        return Hub.objects.filter(customer=customer, is_wms_site=True)
