# System and Django imports
from django.core.validators import RegexValidator
from django.contrib.gis.db import models
from django.db.models import Manager as GeoManager
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import ArrayField
import datetime as dtime

# 3rd party libraries
from blowhorn.oscar.core.loading import get_model
from blowhorn.oscar.apps.address.abstract_models import (AbstractUserAddress,
                                                         AbstractShippingAddress, AbstractCountry)
from blowhorn.oscar.models.fields import AutoSlugField
from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField
from phonenumber_field.modelfields import PhoneNumberField

# blowhorn modules
from blowhorn.common.models import BaseModel
from blowhorn.customer.models import Customer
from blowhorn.users.models import User
from datetime import datetime
from blowhorn.address.constants import SMS_PROVIDERS, CITY_TIERS
from blowhorn.common.middleware import current_request


# Client = get_model('wms', 'Client')


class Country(AbstractCountry):
    pass

class UserAddressManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('country')


class UserAddress(AbstractUserAddress):
    geopoint = models.PointField(null=True, blank=True, srid=4326, help_text=_(
        "Represented as (longitude, latitude)"))
    identifier = models.CharField(_("Address Identifier"), null=True,
                                  blank=True, max_length=255,
                                  help_text=_("Address Identifier"))
    objects = UserAddressManager()

    def save(self, *args, **kwargs):
        super(UserAddress, self).save(*args, **kwargs)


class GenericAddressManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('country')


class GenericAddress(AbstractShippingAddress):
    geopoint = models.PointField(null=True, blank=True, srid=4326, help_text=_(
        "Represented as (longitude, latitude)"))
    identifier = models.CharField(_("Address Identifier"), null=True,
                                  blank=True, max_length=255,
                                  help_text=_("Address Identifier"))
    objects = GenericAddressManager()


class CityManager(GeoManager):
    def get_queryset(self):
        return super().get_queryset().select_related('address')

    def get_cities_for_managerial_user(self, user):
        return self.get_queryset().filter(managers=user)

    def get_cities_for_given_user(self, user):
        qs = super().get_queryset()
        query = Q(managers=user) | Q(operation_managers=user)
        return qs.filter(query).distinct()

    def get_cities_list(self):
        """
        :return: list of cities with dict containing pk, name
        """
        city_qs = super().get_queryset().only('pk', 'name').order_by('name')
        return [{'id': city.pk, 'name': city.name} for city in city_qs]

    def get_super_queryset(self):
        return super().get_queryset()


class City(models.Model):
    """
    City model
    """
    import datetime
    history = AuditlogHistoryField()
    name = models.CharField(
        _("City Name"), max_length=255, blank=False,
        unique=True)

    tier = models.CharField(
        _('Tier'), max_length=50, null=True, blank=True, choices=CITY_TIERS
    )
    # Help search
    slug = AutoSlugField(populate_from='name', max_length=255)

    # This field is deprecated
    coverage = models.PolygonField(
        help_text=_("The coverage area for the city"), null=True, blank=True)

    coverage_pickup = models.PolygonField(
        help_text=_("The coverage area for pickup location"), null=True,
        blank=True)

    coverage_dropoff = models.PolygonField(
        help_text=_("The coverage area for dropoff location"), null=True,
        blank=True)

    shipment_coverage = models.PolygonField(
        help_text=_("The coverage area for shipment orders"), null=True,
        blank=True)

    contact = PhoneNumberField(
        _("Phone number/Mobile Number"), null=True, blank=True,
        help_text=_("Contact number of city branch office"))

    supply_contact_number = PhoneNumberField(
        _("Supply Contact Number"), null=True, blank=True,
        help_text=_("Supply contact number of city branch office"))

    default_vehicle_class = models.ForeignKey('vehicle.VehicleClass',
                                              null=True, blank=True, on_delete=models.CASCADE,
                                              help_text=_(
                                                  "Shown as default selected vehicle class for c2c bookings in app"))

    default_contract = models.ForeignKey('contract.Contract',
                                         null=True, blank=True, related_name='city_contract', on_delete=models.CASCADE,
                                         help_text=_("Shown as default selected rate card for c2c bookings in app"))

    loading_mins = models.IntegerField(
        _('Loading time in mins'), blank=False, null=False, default=25,
        help_text=_("Loading time used in Fare estimation for c2c bookings in app"))

    unloading_mins = models.IntegerField(
        _('Unloading time in mins'), blank=False, null=False, default=20,
        help_text=_("Unloading time used in Fare estimation for c2c bookings in app"))

    start_time = models.TimeField(help_text=_("Booking acceptance start time "
                                              "for the city"), null=True, blank=True)
    end_time = models.TimeField(help_text=_("Booking acceptance end time for "
                                            "the city"), null=True, blank=True)
    address = models.OneToOneField(GenericAddress, null=True, blank=True,
                                   related_name='city_branch_address',
                                   on_delete=models.CASCADE)
    managers = models.ManyToManyField(User, related_name="city_managers",
                                      limit_choices_to={'is_staff': True})
    finance_users = models.ManyToManyField(User, related_name="finance_users",
                                           limit_choices_to={'is_staff': True})
    operation_managers = models.ManyToManyField(
        User, related_name="operation_managers", blank=True,
        limit_choices_to={'is_staff': True})

    sales_representatives = models.ManyToManyField(
        User, related_name="sales_representatives", blank=True,
        limit_choices_to={'is_staff': True})

    supply_users = models.ManyToManyField(
        User, related_name="supply_users", blank=True,
        limit_choices_to={'is_staff': True})

    autodispatch_driver = models.ManyToManyField(
        'driver.Driver', related_name="autodispatch_drivers", blank=True)

    process_associates = models.ManyToManyField(
        User, related_name="process_associates", blank=True,
        limit_choices_to={'is_staff': True})

    head_office = models.ForeignKey('company.Office', null=True,
                                    on_delete=models.CASCADE)

    future_booking_days = models.IntegerField(
        _('Booking Days Allowed'), default=14,
        help_text=_("Number of Days booking is allowed from today "))

    night_hours_start_time = models.TimeField(help_text=_("Night Hours start time "), default=datetime.time(18, 00))

    night_hours_end_time = models.TimeField(help_text=_("Night Hours end time "), default=datetime.time(6, 00))

    # sms_provider = models.CharField(_('SMS Provider'), max_length=10, null=True, blank=True, choices=SMS_PROVIDERS)

    objects = CityManager()

    def __str__(self):
        return self.name

    def clean(self):
        if not self.coverage_pickup and self.coverage_dropoff:
            self.coverage_pickup = self.coverage_dropoff
            self.shipment_coverage = self.coverage_dropoff

        elif not self.coverage_dropoff and self.coverage_pickup:
            self.coverage_dropoff = self.coverage_pickup
            self.shipment_coverage = self.coverage_pickup

    @property
    def coverage_pickup_centroid(self):
        """
        Returns the centroid from coordinates defined in coverage.
        """
        return self.coverage_pickup.centroid

    @property
    def shipment_coverage_centroid(self):
        """
        Returns the centroid from coordinates defined in coverage.
        """
        return self.shipment_coverage.centroid

    @property
    def coverage_dropoff_centroid(self):
        """
        Returns the centroid from coordinates defined in coverage.
        """
        return self.coverage_dropoff.centroid

    class Meta:
        verbose_name = _("City")
        verbose_name_plural = _("Cities")

    @property
    def service_state(self):
        """
        Returns the State where Blowhorn services - has to be present in
        company.
        """

        return self.head_office.state if self.head_office else None


auditlog.register(City)


class State(models.Model):
    """
    Indian State model
    """
    name = models.CharField(
        _("State Name"), max_length=255, blank=False, unique=True)

    code = models.CharField(
        _("State Code"), max_length=2, blank=False)

    gst_code = models.CharField(
        _("GST State Code"), max_length=2, blank=False)

    cities = models.ManyToManyField(City, blank=True)

    country = models.ForeignKey('address.Country', on_delete=models.PROTECT,
                                null=True, blank=False)

    # Help search
    slug = AutoSlugField(populate_from='name', max_length=255)

    rcm_message = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("State")
        verbose_name_plural = _("States")


class WarehouseManager(GeoManager):
    def get_queryset(self):
        return super().get_queryset()


class Warehouse(models.Model):
    """
    Warehouse Model
    """
    history = AuditlogHistoryField()
    name = models.CharField(
        _("Warehouse"), max_length=255, blank=False,
        help_text=_("Warehouse"))

    # The city this Warehouse is part of
    city = models.ForeignKey(City, null=True, on_delete=models.PROTECT)

    # Help search
    slug = AutoSlugField(populate_from='name', max_length=255)

    address = models.OneToOneField(GenericAddress, null=True, blank=True,
                                   related_name='warehouse_address', on_delete=models.CASCADE)
    geopoint = models.PointField(null=True, blank=True, srid=4326,
                                 help_text=_("Represented as (longitude, latitude)"))

    # For GeoManager Queries
    objects = WarehouseManager()

    # TODO: Maybe check if the geopoint is in the coverage area of the city
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Warehouse")
        verbose_name_plural = _("Warehouses")


class Slots(BaseModel):
    """
    Slots for product delivery
    """

    city = models.ForeignKey(City, on_delete=models.PROTECT)

    customer = models.ForeignKey(Customer, null=True, blank=True, on_delete=models.PROTECT)

    vehicle_class = models.ForeignKey('vehicle.VehicleClass', null=True, blank=True, on_delete=models.PROTECT)

    # product = models.ForeignKey('catalogue.Product', null=True,blank=True, on_delete=models.PROTECT)

    contract = models.ForeignKey('contract.Contract', null=True, blank=True, on_delete=models.PROTECT)

    hub = models.ForeignKey('address.Hub', null=True, blank=True, on_delete=models.PROTECT)

    start_time = models.TimeField(help_text=_("Slot start time "), default=dtime.time(8, 00))

    end_time = models.TimeField(help_text=_("Slot end time "), default=dtime.time(12, 00))

    autodispatch_drivers = models.ManyToManyField('driver.Driver', blank=True)

    is_dispatch_required = models.BooleanField(_('Is Dispatch Required'), default=False)

    max_orders = models.PositiveIntegerField(_('Max orders to accept for this slot'), default=1)

    radius_based_order = models.BooleanField(_('Accept Order based on radius'),
                                             default=False)

    radius = models.FloatField(_('Radius to accept the Order'), default=7)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), default=False, null=True, blank=True)

    def __str__(self):
        return str(self.city)

    class Meta:
        verbose_name = _("Slot")
        verbose_name_plural = _("Slots")


class HubManager(GeoManager):
    def get_queryset(self):
        return super().get_queryset().select_related('customer')


class CustomerHubContact(models.Model):
    name = models.CharField(
        _("Contact name"), max_length=100, null=True)

    mobile = PhoneNumberField(
        _("Mobile Number"),
        help_text=_("Contact person's primary mobile number e.g. +91{10 digit mobile number}"))

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        null=True, blank=True
    )

    role = models.CharField(
        _("Organisation Role"), max_length=100, blank=True)

    remarks = models.CharField(
        _("Remarks"), max_length=255, null=True, blank=True)

    def __str__(self):
        return "%s | %s" % (self.name, self.mobile)


class Hub(models.Model):
    """
    Hub Model
    """

    history = AuditlogHistoryField()
    name = models.CharField(
        _("Hub Name"), max_length=255, blank=False,
        help_text=_("Hub Name"))
    delivery_hub_sn = models.CharField(
        _("Hub Short Name"), max_length=255, blank=True, null=True,
        help_text=_("Delivery Hub Short Name"))

    awb_code = models.CharField(_("Hub Short Identifier for AWB"), max_length=10,
                                    blank=True, null=True,
                                    help_text=_("Please enter 3 letter unique code"))

    # The city this hub is part of
    city = models.ForeignKey(City, null=True, on_delete=models.PROTECT)

    # Help search
    slug = AutoSlugField(populate_from='name', max_length=255)

    address = models.OneToOneField(GenericAddress,
                                   related_name='hub_address', null=True, blank=True,
                                   on_delete=models.CASCADE)

    shipping_address = models.ForeignKey('order.ShippingAddress',
                                         null=True,
                                         verbose_name='Hub address as shipping address',
                                         on_delete=models.PROTECT)

    pincodes = ArrayField(models.PositiveIntegerField(
        validators=[RegexValidator(r'^\d{4,6}$')]), blank=True, null=True)

    # If null, means it is managed by blowhorn
    customer = models.ForeignKey(Customer, null=True, blank=True,
                                 on_delete=models.PROTECT)

    store_code = models.CharField(_('Store Code'), max_length=50, blank=True,
                                  null=True)

    qr_code = models.CharField(
        _("QR Code"), max_length=25, blank=False, default='CUSTOMERQRCODE',
        help_text=_("QR Code has to be given to Hub Supervisor for drivers to start the trip"))

    associates = models.ManyToManyField(User, related_name='hub_associates',
                                        blank=True,
                                        limit_choices_to={'is_staff': True})

    supervisors = models.ManyToManyField(User, related_name='hub_supervisors',
                                         blank=True,
                                         limit_choices_to={'is_staff': True})

    customer_hub_contact = models.ManyToManyField(CustomerHubContact,
                                                  related_name='customer_hub_contact',
                                                  blank=True)

    client_billing_code = models.IntegerField(_('Client billing code'), blank=True, null=True,
                                              help_text=_("Client Billing Code"), default=0)

    shipping_code = models.IntegerField(_('Shipping code'), blank=True, null=True,
                                        help_text=_("Ship To Code"), default=0)

    coverage = models.PolygonField(
        help_text=_("The coverage area for the slot"), null=True,
        blank=True
    )

    extended_coverage = models.PolygonField(
        help_text=_("The Extended coverage area for the slot"), null=True,
        blank=True
    )

    length = models.FloatField(_('Length'), null=True, blank=True)

    breadth = models.FloatField(_('Breadth'), null=True, blank=True)

    height = models.FloatField(_('Height'), null=True, blank=True)

    uom = models.CharField(_('Unit Of Measurement'), max_length=50, null=True, blank=True)

    is_wms_site = models.BooleanField(_('Is WMS site'), default=False)

    # client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.PROTECT)

    # For GeoManager Queries
    objects = HubManager()

    # TODO: Maybe check if the geopoint is in the coverage area of the city
    __original_pincodes = None

    def __init__(self, *args, **kwargs):
        super(Hub, self).__init__(*args, **kwargs)
        self.__original_pincodes = self.pincodes

    class Meta:
        verbose_name = _("Hub")
        verbose_name_plural = _("Hubs")
        unique_together = ("customer", "name")

    def save(self, *args, **kwargs):
        _request = current_request()
        pincode_list = []
        added_pincode_list = []
        remove_pincode_list = []
        current_user = _request.user if _request else ''

        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')

                if field_name in ['pincodes']:
                    old_values = field.value_from_object(old) or []
                    new_values = field.value_from_object(self) or []

                    for pincode in new_values:
                        if pincode not in old_values:
                            added_pincode_list.append(pincode)

                    for pin_codes in old_values:
                        if pin_codes not in new_values:
                            remove_pincode_list.append(pin_codes)

                    if old_values != new_values:
                        pincode_list.append(HubHistory(field=field_name, added_pincodes=str(added_pincode_list),
                                                       removed_pincodes=str(remove_pincode_list), modified_by=current_user,
                                                       hub=old))
            HubHistory.objects.bulk_create(pincode_list)

        return super().save(*args, **kwargs)

    def __str__(self):
        if self.customer:
            return '%s-%s' % (self.customer, self.name)
        else:
            return self.name


class Area(models.Model):
    name = models.CharField(_('Name'), max_length=50)
    geopoint = models.PointField(null=True, blank=True, srid=4326,
                                 help_text=_("Represented as (longitude, latitude)"))
    city = models.ForeignKey(City, related_name='city', on_delete=models.PROTECT)
    state = models.ForeignKey(State, related_name='state', on_delete=models.PROTECT)
    country = models.ForeignKey('address.Country', related_name='country', on_delete=models.PROTECT)
    history = AuditlogHistoryField()

    class Meta:
        verbose_name = _("Area")
        verbose_name_plural = _("Areas")

    def __str__(self):
        return self.name


auditlog.register(Hub)


class ClosedHour(models.Model):
    """
    model to choose close operations on particular date and time
    """
    city = models.ForeignKey(City, on_delete=models.CASCADE)

    date = models.DateField(help_text=_("Date on which booking should be closed"))

    start_time = models.TimeField(help_text=_("Time to close the booking"), null=True, blank=True)

    end_time = models.TimeField(help_text=_("Time to reopen the booking"), null=True, blank=True)


class HubHistory(BaseModel):
    """
    Capturing the pincode history
    """
    hub = models.ForeignKey(Hub, null=True, blank=True,
                            on_delete=models.PROTECT)
    field = models.CharField(max_length=80)
    added_pincodes = models.TextField(_("Added Pincodes"))
    removed_pincodes = models.TextField(_("Removed Pincodes"))

    class Meta:
        verbose_name = _('Hub History')
        verbose_name_plural = _('Hub History')

    def __str__(self):
        return self.hub.name


def get_closed_hour(city, max_date_to_show):
    """
    :param city:
    :return: list of future datetimes including today
    """
    dates = []
    # print(city, max_date_to_show.date())
    closed_hours = ClosedHour.objects.filter(city=city, date__gte=datetime.today(), date__lte=max_date_to_show)
    for hours in closed_hours:
        dates.append((hours.date, hours.start_time, hours.end_time))

    return dates

from blowhorn.oscar.apps.address.models import *  # noqa
