from django.contrib.gis.geos import GEOSGeometry

from blowhorn.address.models import City


class LocationHelper:

    @staticmethod
    def get_geopoint_from_latlong(latitude, longitude):
        geopoint_info = '{ "type": "Point", "coordinates": [ %s, %s ] }' % (
            longitude, latitude)
        return GEOSGeometry(geopoint_info)

    @classmethod
    def get_city_from_lat_lng(cls, latitude, longitude):
        geo_point = cls.get_geopoint_from_latlong(latitude, longitude)
        return cls.get_city_from_geopoint(geo_point)

    @staticmethod
    def get_city_from_geopoint(geopoint):
        """
        This method is deprecated. Use method from `address/utils.py`
        """
        return City.objects.filter(coverage_dropoff__contains=geopoint).first()
