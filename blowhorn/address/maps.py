import requests
import time
import json
import itertools
import logging
from urllib import request as urlrequest, parse as urlparse
from django.conf import settings
import geocoder

GEOCODE_BASE_URL = 'https://maps.google.com/maps/api/geocode/json'
DISTANCE_MATRIX_BASE_URL = 'https://maps.google.com/maps/api/distancematrix/json'


class DistanceMatrixResult:
    def __init__(self, element_dict):
        status = element_dict.get('status')
        self.duration_s = None
        self.distance_km = None
        self.valid = False
        if status == 'OK':
            self.duration_s = element_dict.get('duration').get('value')
            self.distance_km = element_dict.get('distance').get('value')/1000.0
            self.valid = True

    def __repr__(self):
        return str(self.__dict__)
        return 'duration_s = %s\tdistance_km = %s' % (self.duration_s, self.distance_km)

class DistanceMatrixResultParser:
    def __init__(self, elements):
        self.elements = elements
        self.results = []
        for element in self.elements:
            self.results.append(DistanceMatrixResult(element))

    def get(self):
        return self.results

    def __repr__(self):
        return str(self.results)
        return '\n'.join([str(x) for x in self.results])

class GoogleMaps:
    def get_lat_lng(address):
        #Geocoding quite often fails for no reason. Let's do some repeated attempts.
        lat_lng_dict = {}
        success = False
        for i in range(settings.MAPS_OPERATION_RETRY_COUNT):
            try:
                g = geocoder.google(address)
                if g.ok:
                    lat_lng_dict = {
                        'lat': g.latlng[0],
                        'lng': g.latlng[1]
                    }
                success = True
                if not lat_lng_dict.get('lat') or not lat_lng_dict.get('lng'):
                    success = False
                break

            except:
                logging.exception('Get lat lng : iteration=%s' % i)
                time.sleep(settings.MAPS_OPERATION_BACKOFF_TIME_S)

        return success, lat_lng_dict

    def get_address(location):
        latlng = [coord for coord in location]
        lat_lng = {
            'lat': float(latlng[1]),
            'lng': float(latlng[0])
        }
        url = GEOCODE_BASE_URL + '?latlng=%f,%f&sensor=false&key=%s' % (float(latlng[1]), float(latlng[0]), settings.GOOGLE_MAPS_API_KEY)

        # Geocoding quite often fails for no reason. Let's do some repeated attempts.
        success = False
        for i in range(settings.MAPS_OPERATION_RETRY_COUNT):
            try:
                result_str = urlrequest.urlopen(url)
                result_dict = json.loads(result_str.read().decode('UTF-8'))
                logging.info("Address is %s", result_dict["results"][0]["address_components"][0]["long_name"] + "," +
                            result_dict["results"][0]["address_components"][1]["long_name"])
                # address_dict = result_dict["results"][0]["address_components"][0]["long_name"] + "," + \
                #             result_dict["results"][0]["address_components"][1]["long_name"]
                address_dict = result_dict["results"][0]['formatted_address']
                success = True
                break
            except:
                logging.exception('Get lat lng : iteration=%s' % i)
            time.sleep(settings.MAPS_OPERATION_BACKOFF_TIME_S)

        if not success:
            address_dict = None
            try:
                logging.error(json.dumps(result_dict, indent=2))
            except:
                pass
        return address_dict

    def get_distances_1_to_1(origin, destination):
        result = GoogleMaps.get_distances_n_to_1(origin, destination)
        return result[0]

    def get_distances_n_to_1(origin_geo_pts, destination):
        destination_ = destination
        if isinstance(destination, list):
            if len(destination) == 0:
                assert False, 'Empty destination list'
            elif len(destination) > 1:
                logging.warn('Expecting only 1 destination. Found multiple : %s' % destination)
                logging.warn('Using 1st destination : %s' % destination[0])
            destination_ = destination[0]
        result = GoogleMaps.get_distances_n_to_m(origin_geo_pts, destination_)
        return list(itertools.chain(*result))

    def get_distances_n_to_m(origin_geo_pts, destinations):
        if not origin_geo_pts or not destinations:
            logging.info('Nothing in destinations of origins. Check data!')
            assert False
            return None

        origins_list_of_strings = origin_geo_pts
        if isinstance(origin_geo_pts, list):
            origins_list_of_strings = '|'.join(['%s' % (x) for x in origin_geo_pts])
        if isinstance(destinations, list):
            destinations = '|'.join(destinations)

        geo_args = {
            'origins': origins_list_of_strings,
            'destinations': destinations,
            'sensor': 'false' ,
            'key': settings.GOOGLE_MAPS_API_KEY,
        }

        url = DISTANCE_MATRIX_BASE_URL + '?' + urlparse.urlencode(geo_args)
        results = []
        success = False
        for i in range(settings.MAPS_OPERATION_RETRY_COUNT):
            try:
                result_str = urlrequest.urlopen(url)
                result_dict = json.loads(result_str.read().decode('UTF-8'))
                logging.debug(json.dumps(result_dict, indent=2))
                status = result_dict.get('status')
                rows = result_dict["rows"]
                results = []
                for row in rows:
                    elements = row["elements"]
                    result_obj = DistanceMatrixResultParser(elements).get()
                    results.append(result_obj)
                break
            except:
                logging.exception('Get distances_n_to_m : iteration=%s' % i)
            time.sleep(settings.MAPS_OPERATION_BACKOFF_TIME_S)

        return results
