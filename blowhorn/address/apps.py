# from django.conf.urls import url
# from django.apps import AppConfig
# from django.urls import include, path
#
#
# class AddressApplication(AppConfig):
#     label = 'address'
#     name = 'blowhorn.address'
#     verbose_name = 'Address'
#     namespace = 'address'
#     def get_urls(self):
#         from .views import CityDetails, CityList, CityView, \
#             HubsForContractFilter, CoverageAreaPincodes, CityCoverageView, \
#             CheckCityOperational, HubDetails
#         urlpatterns = [
#             url(r'^coverage/cities$', CityView.as_view(), name='city'),
#             url(r'^coverage/city$', CityList.as_view(), name='city-list'),
#             url(r'^coverage/city/(?P<pk>[0-9]+)$', CityDetails.as_view(), name='city-detail'),
#             url(r'^city/coverage$', CityCoverageView.as_view(),
#                 name='city-coverage-detail'),
#             url(r'^hubs/filters/$', HubsForContractFilter.as_view(),
#                 name='hubs-contract-filter'),
#             url(r'^pincodes/(?P<city>[\w-]+)$', CoverageAreaPincodes.as_view(),
#                 name='city-pincodes'),
#             url(r'^hubs/$', HubDetails.as_view(),
#                 name='customer-hub-details'),
#             url(r'^checkcity/shipmentoperation', CheckCityOperational.as_view(),
#                 name='city-check-operation'),
#         ]
#         return self.post_process_urls(urlpatterns)
#
#
# # polls_patterns = ([
# #     path('', views.IndexView.as_view(), name='index'),
# #     path('<int:pk>/', views.DetailView.as_view(), name='detail'),
# # ], 'polls')
#
# # application = AddressApplication()
