from django.conf import settings

from blowhorn.address.models import UserAddress
from blowhorn.address.utils import get_full_address_str
from blowhorn.order.models import Order, ShippingAddress

from django.db.models import Q


class AddressMixin(object):

    def get_address_dict(self, addr):
        dict_ = {
            'name': addr.identifier,
            'address': {
                'street_address': addr.line1,
                'area': addr.line2,
                'city': addr.line4,
                'postal_code': addr.postcode,
                'full_address': get_full_address_str(addr)
            }
        }
        return dict_

    def get_detail_dict(self, addr):
        """ For save location purpose """

        if addr.line3:
            full_address = addr.line1
        else:
            full_address = get_full_address_str(addr)

        if addr.phone_number:
            phone_number = str(addr.phone_number.national_number)
        else:
            phone_number = ''

        _dict = {
            'pk': addr.pk,
            'name': addr.identifier or addr.line2 or addr.line3 or '',
            'address': {
                'line': addr.line1 or '',
                'area': addr.line2 or addr.line3 or '',
                'city': addr.line4 or '',
                'state': '',
                'landmark':'',
                'what3words':'',
                'country': addr.country_id if addr.country_id else
                    settings.COUNTRY_CODE_A2,
                'postal_code': addr.postcode or '',
                'full_address': full_address or '',
            },
            'geopoint': {
                'lat': addr.geopoint.y if addr.geopoint else 0,
                'lng': addr.geopoint.x if addr.geopoint else 0,
            },
            'contact': {
                'name': addr.first_name,
                'mobile': phone_number,
            },
        }

        return _dict


    def get_detail_dict_v1(self, addr):
        """ For save location purpose """
        if addr.phone_number:
            phone_number = str(addr.phone_number.national_number)
        else:
            phone_number = ''

        _dict = {
            'pk': addr.pk,
            'name': addr.identifier,
            'address': {
                'line1': addr.line1,
                'line3': addr.line2 or addr.line3,
                'line4': addr.line4,
                'country': addr.country_id if addr.country_id else
                    settings.COUNTRY_CODE_A2,
                'postcode': addr.postcode,
            },
            'contact': {
                'name': addr.first_name,
                'mobile': phone_number,
            },
        }

        if addr and addr.geopoint:
            _dict['geopoint'] = {
                'latitude': addr.geopoint.y,
                'longitude': addr.geopoint.x,
            }

        return _dict

    def get_saved_user_addresses(self, user, new_app=False):
        results = UserAddress.objects.filter(user=user)
        addresses = []
        for x in results:
            if x.identifier:
                if new_app:
                    addresses.append(self.get_detail_dict_v1(x))
                else:
                    addresses.append(self.get_detail_dict(x))
        saved_locations = list(reversed(addresses))

        return saved_locations

    def get_recent_user_addresses(self, user, new_app=False):
        orders = Order.objects.filter(user=user).order_by('-created_date'). \
                     values('pickup_address_id', 'shipping_address_id')[:10]

        ids = []
        for pk in orders:
            ids.append(pk['pickup_address_id'])
            ids.append(pk['shipping_address_id'])

        results = ShippingAddress.objects.filter(
            id__in=ids).distinct('line2', 'line3')[:5]
        addresses = []
        for x in results:
            if new_app:
                addresses.append(self.get_detail_dict_v1(x))
            else:
                addresses.append(self.get_detail_dict(x))
        recent_locations = list(reversed(addresses))
        return recent_locations
