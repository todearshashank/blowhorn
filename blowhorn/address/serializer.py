from collections import OrderedDict
from django.conf import settings
from django.utils import six

from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from phonenumber_field.serializerfields import PhoneNumberField
from phonenumber_field.phonenumber import PhoneNumber

from blowhorn.common.serializers import BaseSerializer
from blowhorn.address.models import City, GenericAddress as Address, Slots, State
from blowhorn.order.models import ShippingAddress
from blowhorn.address.models import Hub, State


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'


class ContactNumberSerializer(PhoneNumberField):
    def to_representation(self, value):
        if isinstance(value, PhoneNumber):
            formatted_number = '+%s %s' % (
                value.country_code, value.national_number)
            return formatted_number
        return six.text_type(value)


class CitySerializer(GeoFeatureModelSerializer):
    address = AddressSerializer()
    contact = ContactNumberSerializer()

    def create(self, validated_data):
        address_data = validated_data.pop('address')
        address = Address.objects.create(**address_data)
        city = City.objects.create(address=address, **validated_data)
        return city

    def update(self, instance, validated_data):
        address_details = validated_data.pop('address')
        address = instance.address
        address.line1 = address_details.get('line1')
        address.line2 = address_details.get('line2')
        address.line3 = address_details.get('line3')
        address.country = address_details.get('country')
        address.save()

        instance.name = validated_data.get('name', instance.name)
        instance.contact = validated_data.get('contact', instance.contact)
        instance.start_time = validated_data.get('start_time', instance.start_time)
        instance.end_time = validated_data.get('end_time', instance.end_time)
        instance.coverage = validated_data.get('coverage', instance.coverage)
        instance.save()

        return instance

    class Meta:
        model = City
        geo_field = 'coverage_pickup'
        fields = '__all__'


class CityModelSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    contact = ContactNumberSerializer()

    def __init__(self, *args, **kwargs):
        super(CityModelSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = City
        # fields = '__all__'


class FormattedAddressSerializer(serializers.ModelSerializer):
    """
    Return place, city and pincode only
    """
    place = serializers.CharField(source='line2')
    city = serializers.CharField(source='line4')
    postalCode = serializers.CharField(source='postcode')

    class Meta:
        model = Address
        fields = ('place', 'city', 'postalCode')


class ShippingAddressSerializer(BaseSerializer):

    class Meta:
        model = ShippingAddress
        fields = '__all__'


class HubSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(source='get_name')

    class Meta:
        model = Hub
        fields = ('id', 'name')

    def get_name(self, hub):
        return str(hub)

class HubGeoFeatureSerializer(GeoFeatureModelSerializer):

    def __init__(self, *args, **kwargs):
        super(HubGeoFeatureSerializer, self).__init__(*args, **kwargs)

        self.Meta.geo_field = self.context.get('geo_field', None) or 'coverage'
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = Hub
        geo_field = 'coverage'
        fields = ('coverage', 'name')


class SlotModelSerializer(serializers.ModelSerializer):
    commercial_classification = serializers.SerializerMethodField()
    city = serializers.CharField(source='city.name')
    hub = serializers.CharField(source='hub.name')
    coverage = serializers.SerializerMethodField()
    extended_coverage = serializers.SerializerMethodField()
    pincodes = serializers.SerializerMethodField()

    class Meta:
        model = Slots
        fields = '__all__'

    def get_commercial_classification(self, obj):
        vehicle_class = obj.vehicle_class
        if vehicle_class is not None:
            return vehicle_class.commercial_classification
        return None

    def get_coverage(self, obj):
        hub = obj.hub
        if hub:
            hub_data = HubGeoFeatureSerializer(
                hub,
                context={
                    'fields': ('coverage', 'name'),
                    'geo_field': 'coverage'
                }
            ).data
            return hub_data.get('geometry', {})

        return {}

    def get_extended_coverage(self, obj):
        hub = obj.hub
        if hub:
            hub_data = HubGeoFeatureSerializer(
                hub,
                context={
                    'fields': ('extended_coverage', 'name'),
                    'geo_field': 'extended_coverage'
                }
            ).data
            return hub_data.get('geometry', {})

        return {}

    def get_pincodes(self, obj):
        hub = obj.hub
        if obj.hub:
            return hub.pincodes

        return []


class StateSerializer(serializers.ModelSerializer):
    cities = serializers.SerializerMethodField(source='get_cities')

    class Meta:
        model = State
        fields = ('id', 'name', 'cities')

    def get_cities(self, obj):
        return list(obj.cities.values('id', 'name'))
