from datetime import timedelta
from django.utils import timezone
from blowhorn.oscar.apps.address.admin import *  # noqa
from django.conf import settings
from django.contrib import admin
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils.html import format_html
from django.contrib import messages

from array_tags import widgets
from mapwidgets.widgets import GooglePointFieldWidget

from blowhorn.customer.models import Customer
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL
from django.utils.translation import ugettext_lazy as _

from .forms import (
    UserAddressForm, HubForm,
    CityForm, CityEditForm,
    GenericAddressForm, StateForm, SlotForm
)
from blowhorn.address.models import (
    City, Hub, Warehouse, GenericAddress,
    UserAddress, State, Area, ClosedHour, CustomerHubContact,
    Slots, HubHistory)
from blowhorn.address.utils import AddressUtil
from phonenumber_field.modelfields import PhoneNumberField
from blowhorn.utils.widgets.phonenumber_field import PhoneNumberPrefixWidget
from blowhorn.address.utils import hub_qr_code_generator
from blowhorn.users.models import User
from blowhorn.common.tasks import update_cms

from ..contract.constants import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME, CONTRACT_STATUS_ACTIVE


class CityAdmin(admin.ModelAdmin):
    formfield_overrides = {
        PhoneNumberField: {
            "widget": PhoneNumberPrefixWidget(
                attrs={'placeholder': 'Contact', 'class': "form-control"},
                initial=settings.ACTIVE_COUNTRY_CODE
            )}
    }

    list_display = ('name', 'tier', 'contact', 'get_working_hours', 'get_area',
                    'get_managers',)

    fieldsets = (
        ('General', {
            'fields': ('name', 'tier', 'contact', 'supply_contact_number', 'start_time', 'end_time',
                       'default_vehicle_class', 'default_contract',
                       'loading_mins', 'unloading_mins', 'address',
                       'head_office', 'future_booking_days',
                       'night_hours_start_time', 'night_hours_end_time',)
        }),
        ('Coverage', {
            'fields': ('coverage_pickup', 'coverage_dropoff', 'shipment_coverage',)
        }),
        ('Permissions', {
            'fields': ('managers', 'operation_managers',
                       'sales_representatives', 'autodispatch_driver',
                       'supply_users', 'process_associates')
        })
    )

    search_fields = ['name']

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = CityEditForm
        else:
            self.form = CityForm
        return super(CityAdmin, self).get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        # TODO. This doesn't give the exact area, as the transformation
        # SRID 3857 is not right. Alternative is to pass the attribute
        # geography=True to the coverage field in the model. Then the
        # answer is right just by calling obj.coverage.area
        qs = super().get_queryset(request)
        qs = qs.annotate(
            coverage_pickup_g=models.functions.Transform('coverage_pickup', 3857),
            coverage_dropoff_g=models.functions.Transform('coverage_dropoff', 3857),
            shipment_coverage_g=models.functions.Transform('shipment_coverage', 3857)
        )
        qs = qs.annotate(
            pickup_coverage_area=models.functions.Area('coverage_pickup_g'),
            dropoff_coverage_area=models.functions.Area('coverage_dropoff_g'),
            shipment_coverage_area=models.functions.Area('shipment_coverage_g'),
        )
        qs = qs.prefetch_related('managers')
        return qs

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'sales_representatives':
            kwargs['queryset'] = User.objects.filter(
                is_staff=True).exclude(email__icontains='blowhorn.net')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_working_hours(self, obj):
        return '%s to %s' % (obj.start_time, obj.end_time)

    get_working_hours.short_description = 'Working Hours'

    def get_area(self, obj):
        template = """
            <div>
                <div><b>Pickup:</b> {coverage_pickup}</div>
                <div><b>Dropoff:</b> {coverage_dropoff}</div>
                <div><b>Shipment:</b> {shipment_coverage}</div>
            </div>
        """.format(coverage_pickup='%.1f' % obj.pickup_coverage_area.sq_km,
                   coverage_dropoff='%.1f' % obj.dropoff_coverage_area.sq_km,
                   shipment_coverage='%.1f' % obj.shipment_coverage_area.sq_km)
        return format_html(template)

    get_area.short_description = 'Area (Sq.km)'

    def get_managers(self, obj):
        return ', '.join(map(str, obj.managers.all()))

    get_managers.short_description = 'Managers'

    def save_model(self, request, obj, form, change):
        is_c2c_sme_contract = City.objects.filter(id=obj.id,
                                                  contract__contract_type__in=[CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME],
                                                  contract__status=CONTRACT_STATUS_ACTIVE).exists()
        if is_c2c_sme_contract:
            update_cms.apply_async((obj.id, False, False), eta=timezone.now() + timedelta(seconds=5))
        super().save_model(request, obj, form, change)


class WarehouseAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget},
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    list_display = ('name', 'city', 'address')
    list_filter = ('city',)

    list_select_related = (
        'city', 'address',
    )


class UserAddressAdmin(admin.ModelAdmin):
    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'line1'},
        'GooglePointFieldWidget'
    )
    formfield_overrides = {
        models.PointField:
            {"widget": GooglePointFieldWidget(
                settings=PointFieldWidgetSettings)},
        ArrayField: {'widget': widgets.AdminTagWidget},
    }

    form = UserAddressForm

    fieldsets = (
        (None, {
            'fields': ('title', 'first_name', 'last_name', 'phone_number',
                       'user', 'line1', 'geopoint', 'line4', 'state',
                       'postcode', 'country',)
        }),
    )


class GenericAddressAdmin(admin.ModelAdmin):
    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'line1'},
        'GooglePointFieldWidget'
    )
    formfield_overrides = {
        models.PointField:
            {"widget": GooglePointFieldWidget(
                settings=PointFieldWidgetSettings)},
        ArrayField: {'widget': widgets.AdminTagWidget},
    }

    form = GenericAddressForm
    fieldsets = (
        ('Address', {
            'fields': ('line1', 'geopoint', 'state', 'postcode', 'country',)
        }),
    )


def regenerate_qr_code(modeladmin, request, queryset):
    for hub in queryset:
        hub.qr_code = hub_qr_code_generator()
        hub.save()

    messages.success(request, "QR Code regenerated for  %s" %
                     list(queryset.values_list('name', flat=True)))


class CustomerHubContactInline(admin.TabularInline):
    # model = Hub.customer_hub_contact.through
    model = CustomerHubContact

    fields = ('name', 'mobile')

    verbose_name = _('Customer Contact')
    verbose_name_plural = _('Customer Contacts')


class CustomerHubContactAdmin(admin.ModelAdmin):
    list_display = ['name', 'mobile']


class HubHistoryInline(admin.TabularInline):
    model = HubHistory

    list_display = ('field', 'removed_pincodes',
                    'added_pincodes', 'modified_by', 'modified_date')

    readonly_fields = list_display

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class HubAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget},
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    search_fields = ['name', ]

    # inlines = [CustomerHubContactInline, ]

    inlines = [HubHistoryInline]

    list_display = ('name', 'awb_code', 'delivery_hub_sn', 'city', 'customer', 'get_qr_code', 'address',
                    'client_billing_code', 'shipping_code', 'get_area')
    list_filter = (
        ('city', admin.RelatedOnlyFieldListFilter),
        ('customer', admin.RelatedOnlyFieldListFilter),
        'is_wms_site')

    list_select_related = (
        'city', 'customer', 'address',
    )

    form = HubForm
    actions = [regenerate_qr_code, ]
    fieldsets = (
        ('General', {
            'fields': ('name', 'awb_code', 'delivery_hub_sn', 'city', 'store_code', 'address', 'pincodes',
                       'customer', 'client_billing_code', 'shipping_code', 'is_wms_site')
        }),
        ('Coverage', {
            'fields': (('coverage', 'extended_coverage'))
        }),
        ('Contacts', {
            'fields': ('supervisors', 'associates', 'customer_hub_contact')
        }),
    )

    def customer(self, obj):
        if obj:
            return obj.customer.name
        return None

    def get_qr_code(self, obj):
        if obj.qr_code:
            return format_html("""
                <a title='Click here to download qr code'
                onClick='showQrCode("%s", "%s", "%s", "%s")'>View</a>
                """ % (obj.qr_code, obj.name, obj.city.name, obj.customer))
        return '--NA--'

    get_qr_code.short_description = _('QR Code')

    def get_area(self, obj):
        if obj.coverage_area:
            return '%.2f' % obj.coverage_area.sq_km
        return 0

    get_area.short_description = 'Area (Sq.km)'

    def get_queryset(self, request):
        return super().get_queryset(request) \
            .select_related('customer', 'city', 'address', 'address__country') \
            .annotate(coverage_g=models.functions.Transform('coverage', 3857)) \
            .annotate(coverage_area=models.functions.Area('coverage_g'))

    # show non individual customers for hub
    # null means blowhorn managed
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return False

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/website/js/lib/jquery.qrcode.min.js',
              'website/js/lib/dom-to-img.js',
              '/static/admin/js/hub/hub.js',
              )


class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'gst_code', 'get_cities',)

    filter_horizontal = ('cities',)

    search_fields = ['name', 'code', 'gst_code', 'cities__name']

    form = StateForm

    def get_cities(self, obj):
        return ', '.join([str(x) for x in obj.cities.all()])

    get_cities.short_description = _('Cities')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        super(StateAdmin, self).save_model(request, obj, form, change)


class AreaAdmin(admin.ModelAdmin):
    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'name'},
        'GooglePointFieldWidget'
    )
    formfield_overrides = {
        models.PointField:
            {"widget": GooglePointFieldWidget(
                settings=PointFieldWidgetSettings)},
    }

    list_display = ('name', 'city', 'state')
    search_fields = ('name', 'city__name', 'state__name', 'country__name')

    list_select_related = (
        'city', 'state',
    )


class ClosedHourAdmin(admin.ModelAdmin):
    list_display = ('city', 'date', 'get_start_time', 'get_end_time',)

    def get_start_time(self, obj):
        if obj.start_time:
            return obj.start_time.strftime('%H:%M')
        return None

    get_start_time.short_description = 'Start time'

    def get_end_time(self, obj):
        if obj.end_time:
            return obj.end_time.strftime('%H:%M')
        return None

    get_end_time.short_description = 'End time'


class SlotsAdmin(admin.ModelAdmin):
    form = SlotForm
    list_filter = ('city',
                   ('customer', admin.RelatedOnlyFieldListFilter),)
    list_display = ('city', 'customer', 'get_start_time', 'get_end_time', 'vehicle_class', 'hub')

    def get_start_time(self, obj):
        if obj.start_time:
            return obj.start_time.strftime('%H:%M')
        return None

    get_start_time.short_description = 'Start time'

    def get_end_time(self, obj):
        if obj.end_time:
            return obj.end_time.strftime('%H:%M')
        return None

    get_end_time.short_description = 'End time'

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            fieldsets = (
                ('General', {
                    'fields':
                        (('city', 'start_time', 'end_time', 'vehicle_class', "is_dispatch_required",
                          'contract', 'hub', ('radius_based_order', 'radius')))
                }),
            )
        else:
            fieldsets = (
                ('General', {
                    'fields':
                        (('city', 'start_time', 'end_time', 'vehicle_class', 'customer', 'autodispatch_drivers',
                          "is_dispatch_required", 'contract', 'hub', ('radius_based_order', 'radius')))
                }),
            )

        return fieldsets

    def get_queryset(self, request):
        return super().get_queryset(request) \
            .select_related('vehicle_class', 'contract', 'hub', 'customer', 'city') \
            .prefetch_related('autodispatch_drivers')


admin.site.register(State, StateAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Hub, HubAdmin)
admin.site.register(CustomerHubContact, CustomerHubContactAdmin)
admin.site.register(Warehouse, WarehouseAdmin)
admin.site.unregister(UserAddress)
admin.site.register(UserAddress, UserAddressAdmin)
admin.site.register(GenericAddress, GenericAddressAdmin)
admin.site.register(ClosedHour, ClosedHourAdmin)
admin.site.register(Slots, SlotsAdmin)
