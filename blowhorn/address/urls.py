from django.conf.urls import url
# from . import views
from .views import CityDetails, CityList, CityView, \
    HubsForContractFilter, CoverageAreaPincodes, CityCoverageView, \
    CheckCityOperational, HubDetails, StateList, CitiesList

urlpatterns = [
            url(r'^coverage/cities$', CityView.as_view(), name='city'),
            url(r'^coverage/city$', CityList.as_view(), name='city-list'),
            url(r'^coverage/city/(?P<pk>[0-9]+)$', CityDetails.as_view(), name='city-detail'),
            url(r'^city/coverage$', CityCoverageView.as_view(),
                name='city-coverage-detail'),
            url(r'^hubs/filters/$', HubsForContractFilter.as_view(),
                name='hubs-contract-filter'),
            url(r'^pincodes/(?P<city>[\w-]+)$', CoverageAreaPincodes.as_view(),
                name='city-pincodes'),
            url(r'^hubs/$', HubDetails.as_view(),
                name='customer-hub-details'),
            url(r'^checkcity/shipmentoperation', CheckCityOperational.as_view(),
                name='city-check-operation'),
            url(r'^state$', StateList.as_view(), name='state-list'),
            url(r'^state/(?P<id>[\w-]+)/city$', CitiesList.as_view(), name='state-city-list'),
        ]


