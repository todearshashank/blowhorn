# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-06-12 13:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0005_auto_20180605_1743'),
        ('address', '0016_state_rcm_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='head_office',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='company.Office'),
        ),
    ]
