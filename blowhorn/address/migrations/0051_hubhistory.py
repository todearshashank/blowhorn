# Generated by Django 2.2.24 on 2021-09-16 09:37

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('address', '0050_hub_extended_coverage'),
    ]

    operations = [
        migrations.CreateModel(
            name='HubHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('field', models.CharField(max_length=80)),
                ('added_pincodes', models.TextField(verbose_name='Added Pincodes')),
                ('removed_pincodes', models.TextField(verbose_name='Removed Pincodes')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='hubhistory_created_by', to=settings.AUTH_USER_MODEL)),
                ('hub', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='address.Hub')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='hubhistory_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Hub History',
                'verbose_name_plural': 'Hub History',
            },
        ),
    ]
