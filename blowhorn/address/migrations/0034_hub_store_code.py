# Generated by Django 2.1.1 on 2019-08-02 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0033_city_supply_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='hub',
            name='store_code',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Store Code'),
        ),
    ]
