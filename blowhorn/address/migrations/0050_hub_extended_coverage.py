# Generated by Django 2.2.20 on 2021-05-04 06:07

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0049_hub_awb_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='hub',
            name='extended_coverage',
            field=django.contrib.gis.db.models.fields.PolygonField(blank=True, help_text='The Extended coverage area for the slot', null=True, srid=4326),
        ),
    ]
