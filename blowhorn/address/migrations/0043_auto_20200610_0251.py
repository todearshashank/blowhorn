# Generated by Django 2.1.1 on 2020-06-10 02:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0121_auto_20200605_0849'),
        ('address', '0042_city_tier'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='hub',
            unique_together={('customer', 'name')},
        ),
    ]
