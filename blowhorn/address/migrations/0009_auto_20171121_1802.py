# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-21 18:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0008_auto_20171114_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='name',
            field=models.CharField(max_length=255, unique=True, verbose_name='City Name'),
        ),
        migrations.AlterField(
            model_name='state',
            name='name',
            field=models.CharField(max_length=255, unique=True, verbose_name='State Name'),
        ),
    ]
