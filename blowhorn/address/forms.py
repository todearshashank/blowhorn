import re
from django import forms
from django.conf import settings
from django.contrib.gis import forms as normalforms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

import floppyforms
from phonenumber_field.phonenumber import PhoneNumber

from config.settings.status_pipelines import ACTIVE
from .models import Hub, City, State
from blowhorn.contract.models import Contract
from blowhorn.address.widgets import GooglePolygonFieldWidget
from blowhorn.contract.constants import (
    CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME
)
from blowhorn.users.models import User
from ..customer.models import Customer, CUSTOMER_CATEGORY_INDIVIDUAL
from ..driver.models import Driver


def prefixed_phone_number(clean_data, phone_number_key):
    phone_number = clean_data.get(phone_number_key)
    if phone_number:
        phone_number = PhoneNumber(
            settings.ACTIVE_COUNTRY_CODE, phone_number)
    return phone_number


class GMapPolygonWidget(floppyforms.gis.PolygonWidget,
                        floppyforms.gis.BaseGMapWidget):
    google_maps_api_key = settings.GOOGLE_MAPS_API_KEY


class UserAddressForm(forms.ModelForm):
    line1 = forms.CharField(label='Full Address')
    postcode = forms.CharField(label='Post/Zip-code')

    def clean(self):
        user = self.cleaned_data.get('user')
        if not user:
            raise ValidationError({'user': "User is mandatory"})


class GenericAddressForm(forms.ModelForm):
    line1 = forms.CharField(label='Full Address')
    postcode = forms.CharField(label='Post/Zip-code')

    def clean(self):
        address = self.cleaned_data
        if not address.get('geopoint'):
            raise ValidationError("Geopoint is needed for the address")


class SlotForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super(SlotForm, self).__init__(*args, **kwargs)
        if 'autodispatch_drivers' in self.fields:
            qs = Driver.objects.filter(status=ACTIVE)
            self.fields['autodispatch_drivers'].autocomplete = False
            self.fields['autodispatch_drivers'].queryset = qs
        if 'customer' in self.fields:
            qs = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
            self.fields['customer'].autocomplete = False
            self.fields['customer'].queryset = qs


class HubForm(forms.ModelForm):
    coverage = normalforms.PolygonField(widget=GooglePolygonFieldWidget, required=False)
    extended_coverage = normalforms.PolygonField(widget=GooglePolygonFieldWidget, required=False)

    def __init__(self, *args, **kwargs):
        customer = self.base_fields["customer"]

        # remove the add_another, change_selected, remove_selected option
        # on customer field in detail view by setting
        # can_add_related, can_change_related, can_delete_related
        # to False on the widget
        customer.widget.can_add_related = False
        customer.widget.can_change_related = False
        customer.widget.can_delete_related = False
        super(HubForm, self).__init__(*args, **kwargs)

        staff_users = User.objects.staff_users()
        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = staff_users

        if 'associates' in self.fields:
            self.fields['associates'].autocomplete = False
            self.fields['associates'].queryset = staff_users

        if 'customer_hub_contact' in self.fields:
            self.fields['customer_hub_contact'].autocomplete = False

    def clean(self):
        address = self.cleaned_data.get('address')
        hub_name = self.cleaned_data.get('name')
        awb_code = self.cleaned_data.get('awb_code')

        hub_obj = Hub.objects.filter(name=hub_name,
                                     customer=self.instance.customer).exclude(id=self.instance.pk).first()
        if hub_obj:
            raise ValidationError({
                'name': 'Hub Name already exists'
            })

        if self.instance.awb_code and self.instance.awb_code != awb_code:
            raise ValidationError({
                'awb_code': 'The awb unique identifier cannot be changed once assigned'
            })

        if awb_code:
            _hub = Hub.objects.filter(awb_code=awb_code, customer=self.instance.customer
                                        ).exclude(id=self.instance.pk).first()
            if _hub:
                raise ValidationError({
                    'awb_code': 'The awb unique identifier already exists'
                })

            coderex = re.match('^[A-Z]{3}$',awb_code)
            if not coderex:
                raise ValidationError({
                    'awb_code': 'The awb unique identifier should be 3 letters caps only'
                })

        if not address:
            raise ValidationError({
                'address': "Address is mandatory"})
        else:
            if not address.geopoint:
                raise ValidationError("Geopoint is needed for the address")

            hub = Hub.objects.filter(address=address).exclude(id=self.instance.id)
            if hub.exists():
                raise ValidationError(
                    "There is already a hub with this address")


class CityForm(normalforms.ModelForm):
    coverage_pickup = normalforms.PolygonField(widget=GooglePolygonFieldWidget)
    coverage_dropoff = normalforms.PolygonField(widget=GooglePolygonFieldWidget)
    shipment_coverage = normalforms.PolygonField(widget=GooglePolygonFieldWidget)

    def __init__(self, *args, **kwargs):
        super(CityForm, self).__init__(*args, **kwargs)
        users = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')
        self.fields['managers'].autocomplete = False
        self.fields['managers'].queryset = users
        self.fields['operation_managers'].autocomplete = False
        self.fields['operation_managers'].queryset = users
        self.fields['sales_representatives'].autocomplete = False
        self.fields['sales_representatives'].queryset = users
        self.fields['supply_users'].autocomplete = False
        self.fields['supply_users'].queryset = users
        self.fields['process_associates'].autocomplete = False
        self.fields['process_associates'].queryset = users
        self.fields['default_contract'].autocomplete = False
        contract_types = [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]
        qs = Contract.objects.filter(contract_type__in=contract_types)
        self.fields['default_contract'].queryset = qs

    def clean(self):
        name = self.cleaned_data['name']
        tier = self.cleaned_data['tier']
        if City.objects.filter(name__iexact=name).exists():
            raise ValidationError("There is already a city present with same "
                                  "name")
        process_associates = self.cleaned_data.get('process_associates', None)
        if not process_associates or (process_associates and not process_associates.exists()):
            raise ValidationError({'process_associates': 'process associates are mandatory'})
        if not tier:
            raise ValidationError({
                'tier': _('Tier is mandatory')
            })


class CityEditForm(normalforms.ModelForm):
    coverage_pickup = normalforms.PolygonField(widget=GooglePolygonFieldWidget)
    coverage_dropoff = normalforms.PolygonField(widget=GooglePolygonFieldWidget)
    shipment_coverage = normalforms.PolygonField(widget=GooglePolygonFieldWidget)

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            city = kwargs['instance']
            if city:
                contact_obj = city.contact
                if contact_obj:
                    contact = contact_obj.__dict__.get('national_number')
                    kwargs['initial'] = {'contact': str(contact).zfill(10)}
        super(CityEditForm, self).__init__(*args, **kwargs)
        users = User.objects.filter(
            is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')
        self.fields['managers'].autocomplete = False
        self.fields['managers'].queryset = users
        self.fields['operation_managers'].autocomplete = False
        self.fields['operation_managers'].queryset = users
        self.fields['default_contract'].autocomplete = False
        contract_types = [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]
        qs = Contract.objects.filter(contract_type__in=contract_types,
                                     city=city)
        self.fields['default_contract'].queryset = qs
        self.fields['sales_representatives'].autocomplete = False
        self.fields['sales_representatives'].queryset = users

        self.fields['supply_users'].autocomplete = False
        self.fields['supply_users'].queryset = users
        self.fields['process_associates'].autocomplete = False
        self.fields['process_associates'].queryset = users

    def clean(self):
        name = self.cleaned_data['name']
        tier = self.cleaned_data['tier']

        if City.objects.filter(name__iexact=name).exclude(id=self.instance.id).exists():
            raise ValidationError("There is already a city present with same name")

        process_associates = self.cleaned_data.get('process_associates', None)
        if not process_associates or (process_associates and not process_associates.exists()):
            raise ValidationError({'process_associates': 'process associates are mandatory'})
        if not tier:
            raise ValidationError({
                'tier': _('Tier is mandatory')
            })


class StateForm(normalforms.ModelForm):
    def clean(self):
        name = self.cleaned_data['name']

        if State.objects.filter(name__iexact=name).exclude(id=self.instance.id).exists():
            raise ValidationError("There is already a state present with same name")
