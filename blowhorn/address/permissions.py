from .models import City


def is_city_manager(city, user):
    ''' returns True if user is a manager for the passed City '''
    return City.objects.filter(id=city.id).filter(managers=user).exists()


def is_operation_manager(city, user):
    ''' returns True if user is a an operation manager for of given City '''
    return City.objects.filter(id=city.id).filter(operation_managers=user).exists()


def is_sales_manager(city, user):
    ''' returns True if user is a an operation manager for of given City '''
    return City.objects.filter(id=city.id, sales_representatives=user).exists()
