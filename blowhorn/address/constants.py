PINCODE_REGEX = '^.*(?P<zipcode>\d{6}).*$'

DEFAULT_CITY = 'Bengaluru'
DEFAULT_LOADING_TIME_MINUTES = 25
DEFAULT_UNLOADING_TIME_MINUTES = 20
UPDATE_FLAG = False

country_calling_codes = {
    '91': 'IN',
    '27': 'ZA',
}

EXOTEL, KALEYRA, TWILIO = ('exotel', 'kaleyra', 'twilio')
SMS_PROVIDERS = (
    (EXOTEL, 'Exotel'),
    (KALEYRA, 'Kaleyra'),
    (TWILIO, 'Twilio')
)

TIER_1, TIER_2, TIER_3 = ('TIER_1', 'TIER_2', 'TIER_3')
CITY_TIERS = (
    (TIER_1, 'TIER_1'),
    (TIER_2, 'TIER_2'),
    (TIER_3, 'TIER_3')
)