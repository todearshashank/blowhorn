import json
from django.conf import settings
from django.db.models import Q
from django.template.response import TemplateResponse
from django.utils.translation import ugettext_lazy as _

from rest_framework import generics
from rest_framework import status
from rest_condition import Or
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, views
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import AllowAny

from blowhorn.address.models import State
from blowhorn.oscar.core.loading import get_model

from blowhorn.address.serializer import CitySerializer, HubSerializer
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsValidAPIKeyHolder, IsCustomer
from blowhorn.address.utils import get_city_from_latlong


City = get_model('address', 'City')
Hub = get_model('address', 'Hub')
Contract = get_model('contract', 'Contract')
Customer = get_model('customer', 'Customer')


class CityView(APIView):
    template_name = 'admin/city_coverage.html'

    def get(self, request):
        if request.user.is_staff:
            return TemplateResponse(request, self.template_name,
                                    {'google_map_api_key': settings.GOOGLE_MAPS_API_KEY})
        else:
            # Redirect to 401 permission denied page
            pass

class CityList(generics.ListAPIView):
    serializer_class = CitySerializer
    queryset = City.objects.all()

    def get(self, request):
        cities = City.objects.all()
        serializer = CitySerializer(cities, many=True)
        return Response(serializer.data)

class CityCoverageView(APIView):
    permission_classes = (AllowAny,)
    queryset = City.objects.all()

    def __get_json(self, coordinates):
        latlon = []
        if coordinates:
            for pt in coordinates:
                latlon.append({
                    'lat': pt[1],
                    'lng': pt[0]
                })
        return latlon

    def get(self, request):
        data = request.GET
        print('DATA: ', data)
        city = City.objects.get_super_queryset().filter(
            name=data.get('city_name')
        ).only(
            'coverage_pickup', 'coverage_dropoff'
        ).first()
        if not city:
            return Response(status=400, data=_('No city found'))

        coverage = {
            'pickup': [],
            'dropoff': []
        }
        coverage_pickup = city.coverage_pickup
        # NOTE: Using GeoJSON serializer is expensive
        if len(coverage_pickup):
            coverage['pickup'] = self.__get_json(coverage_pickup[0].coords)

        coverage_dropoff = city.coverage_dropoff
        if len(coverage_dropoff):
            coverage['dropoff'] = self.__get_json(coverage_dropoff[0].coords)

        return Response(status=200, data=coverage)


class CityDetails(generics.RetrieveUpdateAPIView):
    serializer_class = CitySerializer
    queryset = City.objects.all()

    def get(self, request, pk=None):
        try:
            city = City.objects.get(pk=pk)
        except City.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = CitySerializer(city)
        return Response(serializer.data)

    def put(self, request, pk=None):
        try:
            city = City.objects.get(pk=pk)
        except City.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        data = json.loads(request.data.get('data'))
        serializer = CitySerializer(city, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        if self.request.user.is_staff:
            return City.objects.all()


class HubsForContractFilter(views.APIView):
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request):
        params = request.GET

        contract = []
        hub = []

        if params.get('contract_id', ''):
            contract = Contract.objects.filter(
                id=params.get('contract_id')).first()
        else:
            hubs = Hub.objects.all()

        if contract:
            customer = contract.customer.id
            city = contract.city.id
            hubs = Hub.objects.filter(customer_id=customer, city_id=city)

        if hubs:
            hub_serializer = HubSerializer(hubs, many=True)
            return Response(status=status.HTTP_200_OK,
                            data=hub_serializer.data)
        return Response(status=status.HTTP_404_NOT_FOUND)


class CoverageAreaPincodes(views.APIView):
    permission_classes = (AllowAny, IsValidAPIKeyHolder)

    def get(self, request, city):
        """
        Get the list of pincodes of our service area for a particular city
        """

        if not city:
            response = {
                'status': 'FAIL',
                'message': 'Please provide the city name',
                'cities_we_serve': list(City.objects.all().values_list(
                    'name', flat=True).order_by('id'))
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        pincodes = Hub.objects.filter(
            city__name=city).values_list('pincodes', flat=True)

        if not pincodes:
            response = {
                'status': 'FAIL',
                'message': 'We don\'t provide service in this city',
                'cities_we_serve': list(City.objects.all().values_list(
                    'name', flat=True).order_by('id'))
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        pincode_list = []

        for pin in pincodes:
            if pin:
                pincode_list.extend(pin)

        if pincode_list:
            pincode_list = list(set(pincode_list))

        response = {
            'status': 'PASS',
            'message': {
                'pincodes': pincode_list
            }
        }
        return Response(status=status.HTTP_200_OK,
                        data=response)


class CheckCityOperational(views.APIView):
    permission_classes = (AllowAny, )
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        # Pass a list of lat-long and orders and check operational city corresponding lat-long pair
        data = request.data
        response = {
            "operational_orders":[],
            "non_operational_orders":[]
        }
        for order in data["rows"]:
            order_details = order ["order"]
            pickup_city = get_city_from_latlong(latitude=order_details["pickup"]["lat"],
                longitude=order_details["pickup"]["lng"],
                field=order_details["pickup"]["coverage"])
            delivery_city = get_city_from_latlong(latitude=order_details["delivery"]["lat"],
                longitude=order_details["delivery"]["lng"],
                field=order_details["delivery"]["coverage"])
            if (pickup_city is not None) and (delivery_city is not None):
                response["operational_orders"].append(order_details["order_number"])
            else:
                if pickup_city is None:
                    response["non_operational_orders"].append({
                        "order_number": order_details["order_number"],
                        "coverage": order_details["pickup"]["coverage"]
                    })
                elif delivery_city is None:
                    response["non_operational_orders"].append({
                        "order_number": order_details["order_number"],
                        "coverage": order_details["delivery"]["coverage"]
                    })

        return Response(response, status.HTTP_200_OK)


class HubDetails(views.APIView, CustomerMixin):
    permission_classes = (AllowAny, Or(IsValidAPIKeyHolder,IsCustomer),)

    def get(self, request):
        """
        Get the list of hubs for that customer
        """
        params = self.request.GET
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()
        else:
            customer = self.get_customer(request.user)

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        city_name = params.get('city', None)
        query = Q(customer=customer)
        if city_name:
            city = City.objects.get(name=city_name)
            if city:
                query = query & Q(city=city)

        response = {
            "status": status.HTTP_200_OK
        }
        hubs = Hub.objects.filter(query)
        hud_data = []
        for hub in hubs:
            geopoint = hub.address.geopoint
            hud_data.append({
                "hub_id": hub.id,
                "hub_code": hub.awb_code,
                "hub_name": hub.name,
                "hub_address": hub.address.line1 if hub.address else '',
                "hub_location":{
                    'lat': geopoint.y if geopoint else '',
                    'lng': geopoint.x if geopoint else '',
                }
            })
        response.update({"data": hud_data})
        return Response(status=status.HTTP_200_OK, data=response)

class StateList(generics.ListAPIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        states = State.objects.values('id','name')
        return Response(status=status.HTTP_200_OK, data=states)

class CitiesList(generics.ListAPIView):
    permission_classes = (AllowAny,)

    def get(self, request, id):
        cities = City.objects.filter(state__id=id).values('id','name')
        return Response(status=status.HTTP_200_OK, data=cities)
