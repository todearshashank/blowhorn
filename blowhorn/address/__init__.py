# default_app_config = 'blowhorn.address.apps'

VERSION = (0, 1, 9)
__version__ = '.'.join(map(str, VERSION))

from blowhorn.address.widgets import GooglePolygonFieldWidget
