from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from blowhorn.changelog.views import ChangelogView


urlpatterns = [
    url(r'^admin/changelogs', login_required(ChangelogView.as_view()),
        name='admin-changelog'),
]
