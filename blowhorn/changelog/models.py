from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django_fsm import FSMField, transition

from blowhorn.common.models import BaseModel
from blowhorn.changelog.constants import CHANGELOG_DRAFT, \
    CHANGELOG_PUBLISHED, CHANGELOG_UPCOMING
from .transition_conditions import is_allowed_to_publish,\
    is_allowed_for_upcoming, can_publish_changelog


class TechProduct(BaseModel):

    name = models.CharField(_('Product Name'), max_length=255,
                            unique=True, db_index=True)

    description = models.TextField(_('Description'), null=True, blank=True)

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name


class Release(BaseModel):

    product = models.ForeignKey(
        TechProduct, related_name='release_products',
        related_query_name='release_products',
        on_delete=models.PROTECT
    )

    version = models.CharField(_('Release Version'), max_length=50, null=True,
                               blank=True, db_index=True)

    date_of_release = models.DateField(_('Date of Release'),
                                       null=True, blank=True, db_index=True)

    status = FSMField(default=CHANGELOG_DRAFT, protected=False, db_index=True)

    class Meta:
        verbose_name = _('Release')
        verbose_name_plural = _('Releases')
        unique_together = ('product', 'version')

    def __str__(self):
        return '%s | %s' % (
            self.product, (self.version or self.date_of_release)
        )

    @transition(field=status, source=[CHANGELOG_DRAFT, CHANGELOG_UPCOMING],
                target=CHANGELOG_PUBLISHED,
                permission=[],
                conditions=[is_allowed_to_publish])
    def publish(self):
        Changelog.objects.filter(release=self).update(
            status=CHANGELOG_PUBLISHED
        )

    @transition(field=status, source=CHANGELOG_DRAFT,
                target=CHANGELOG_UPCOMING,
                permission=[],
                conditions=[])
    def upcoming(self):
        Changelog.objects.filter(release=self).update(
            status=CHANGELOG_UPCOMING
        )


class Changelog(BaseModel):

    content = models.TextField(_('HTML Content'))

    tags = ArrayField(models.CharField(max_length=255), blank=True, null=True)

    release = models.ForeignKey(
        Release, related_query_name='releases',
        related_name='releases', on_delete=models.PROTECT,
        blank=True, null=True
    )

    product = models.ForeignKey(
        TechProduct, related_name='products', related_query_name='products',
        on_delete=models.PROTECT
    )

    status = FSMField(default=CHANGELOG_DRAFT, protected=False, db_index=True)

    class Meta:
        verbose_name = _('Changelog')
        verbose_name_plural = _('Changelog')

    def __str__(self):
        if self.release:
            return '%s' % self.release
        else:
            return '%s | %s' % (
                self.product.__str__(), self.created_date.date()
            )

    def save(self, *args, **kwargs):
        if self.release:
            if not self.product:
                self.product = self.release.product

            release_status = self.release.status
            if release_status not in CHANGELOG_DRAFT:
                self.status = release_status

        super().save(*args, **kwargs)

    @transition(field=status, source=CHANGELOG_DRAFT,
                target=CHANGELOG_UPCOMING,
                permission=[],
                conditions=[is_allowed_for_upcoming])
    def upcoming(self):
        pass

    @transition(field=status, source=CHANGELOG_DRAFT,
                target=CHANGELOG_PUBLISHED,
                permission=[],
                conditions=[can_publish_changelog])
    def publish(self):
        pass
