from django.conf.urls import url
from blowhorn.changelog.views import changelog_view, \
            changelog_support_data_view

urlpatterns = [
            url(r'^changelog$', changelog_view, name='changelog'),
            url(r'^changelog/support-data$', changelog_support_data_view, name='changelog-support-data'),
        ]
