from rest_framework import serializers

from blowhorn.changelog.constants import AUDITLOG_FIELDS
from blowhorn.changelog.models import Changelog, TechProduct, Release


class TechProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = TechProduct
        fields = '__all__'


class ReleaseSerializer(serializers.ModelSerializer):
    product = TechProductSerializer(read_only=True)

    class Meta:
        model = Release
        fields = '__all__'


class ChangelogSerializer(serializers.ModelSerializer):
    product = TechProductSerializer(read_only=True)
    release = ReleaseSerializer(read_only=True)

    class Meta:
        model = Changelog
        exclude = AUDITLOG_FIELDS
