from django.conf import settings

from blowhorn.changelog.constants import CHANGELOG_UPCOMING
from blowhorn.changelog.models import TechProduct


class ChangelogMixin(object):

    def get_support_data(self):
        logo_url = '%s/static/website/images/%s/logo/logo.png' % (
            settings.HOST, settings.WEBSITE_BUILD
        )
        return {
            'current_build': settings.WEBSITE_BUILD,
            'logo_url': logo_url,
            'homepage_url': settings.HOST,
            'products': list(TechProduct.objects.values('pk', 'name')),
            'statuses': {
                'upcoming': CHANGELOG_UPCOMING,
                'published': 'Releases'
            }
        }
