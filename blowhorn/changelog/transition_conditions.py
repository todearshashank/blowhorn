from django.utils.translation import ugettext_lazy as _

from blowhorn.changelog.constants import CHANGELOG_PUBLISHED


def is_allowed_to_publish(instance):
    """
       Release version and release date should be mandatory
    """
    if instance.version and instance.date_of_release:
        return True

    is_allowed_for_upcoming.hint = _(
        'Add Version and Date of Release to publish'
    )
    return False


def is_allowed_for_upcoming(instance):
    return not instance.release or (
        instance.release and instance.release.status == instance.status
    )


def can_publish_changelog(instance):
    if not instance.release or (
            instance.release and
            instance.release.status not in [CHANGELOG_PUBLISHED]
    ):
        can_publish_changelog.hint = _(
            'Map a published release to publish this changelog'
        )
        return False
    return True
