import logging
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template.response import TemplateResponse
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

from rest_framework.decorators import api_view
from rest_framework.response import Response

from blowhorn.changelog.mixins import ChangelogMixin
from blowhorn.changelog.models import Changelog
from blowhorn.changelog.serializers import ChangelogSerializer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ChangelogView(TemplateView, ChangelogMixin):
    template_name = "admin/changelog/changelog.html"

    def get(self, request, **kwargs):
        return TemplateResponse(
            request, self.template_name, self.get_support_data()
        )


@login_required
@api_view(['GET'])
def changelog_support_data_view(request):
    if request.method == 'GET':
        return Response(
            status=200,
            data=ChangelogMixin().get_support_data()
        )

    return Response(
        status=405,
        data=_('Method not allowed')
    )


@login_required
@api_view(['GET'])
def changelog_view(request):
    if request.method == 'GET':
        data = request.GET.dict()
        response_type = data.pop('response_type', None)
        related_models = ['product', 'release']
        qs = Changelog.objects.select_related(
            *related_models
        ).filter(**data).order_by(
            '-release__date_of_release',
            '-release__version'
        )
        if response_type == 'json':
            return Response(
                status=200,
                data=ChangelogSerializer(qs, many=True).data
            )
        if not qs.exists():
            return Response(
                status=404,
                data='No %s releases found.' % data.get('status')
            )
        qs = qs.values(
            'content', 'tags',
            'release__version',
            'release__date_of_release',
            'product__name'
        )
        return render_to_response(
            'admin/changelog/partials/main_content.html',
            {'data': qs}
        )
    return render_to_response('website/404_not_found.html', {'params': "{}"},
                              status=400)
