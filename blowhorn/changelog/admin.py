from django import forms
from django.contrib import admin
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from ckeditor.widgets import CKEditorWidget
from array_tags import widgets

from blowhorn.common.admin import BaseAdmin
from blowhorn.changelog.constants import AUDITLOG_FIELDS, CHANGELOG_PUBLISHED
from blowhorn.changelog.models import TechProduct, Release, Changelog
from blowhorn.utils.fsm_mixins import FSMTransitionMixin


class TechProductAdmin(BaseAdmin):
    list_display = ['name'] + AUDITLOG_FIELDS
    list_filter = ('name',)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return AUDITLOG_FIELDS
        return []

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (('General', {
                'fields': ['name', 'description']
            }),
            ('Auditlog', {
                'fields': AUDITLOG_FIELDS
            }),)

        return (
            ('General', {
                'fields': ['name', 'description']
            }),
        )


class ReleaseAdmin(FSMTransitionMixin, BaseAdmin):
    list_filter = (
        ('product', admin.RelatedOnlyFieldListFilter),
        ('date_of_release', admin.DateFieldListFilter),
        'status'
    )
    search_fields = ['version']
    list_display = ['id', 'version', 'date_of_release', 'status', 'product'] \
                   + AUDITLOG_FIELDS
    fsm_field = ['status']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('product', 'created_by', 'modified_by')

    def has_delete_permission(self, request, obj=None):
        return obj and obj.status != CHANGELOG_PUBLISHED

    def get_readonly_fields(self, request, obj=None):
        return ['status']


class ChangelogAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())

    def clean(self):
        super(ChangelogAdminForm, self).clean()
        release = self.cleaned_data.get('release')
        product = self.cleaned_data.get('product')
        if release and release.product != product:
            raise ValidationError({
                'release': _('Product from release and changelog should be same')
            })

    class Meta:
        model = Changelog
        fields = '__all__'


class ChangelogAdmin(FSMTransitionMixin, BaseAdmin):
    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    form = ChangelogAdminForm
    list_display = ['id', 'release', 'product', 'status', 'tags'
                    ] + AUDITLOG_FIELDS
    list_filter = ['product', 'status']
    search_fields = ['tags', 'release__version']
    fsm_field = ['status']

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (('General', {
                'fields': ['product', 'release', 'content', 'tags', 'status']
            }),
            ('Auditlog', {
                'fields': AUDITLOG_FIELDS
            }),)

        return (
            ('General', {
                'fields': ['product', 'release', 'content', 'tags']
            }),
        )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('product', 'release', 'created_by',
                                 'modified_by')

    def get_readonly_fields(self, request, obj=None):
        """
            No readonly fields
        """
        return AUDITLOG_FIELDS + ['status']

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'release':
            kwargs['queryset'] = Release.objects.order_by('-version')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Changelog, ChangelogAdmin)
admin.site.register(TechProduct, TechProductAdmin)
admin.site.register(Release, ReleaseAdmin)
