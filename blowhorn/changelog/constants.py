CHANGELOG_DRAFT = 'draft'
CHANGELOG_UPCOMING = 'upcoming'
CHANGELOG_PUBLISHED = 'published'
CHANGELOG_STATUSES = (
    (CHANGELOG_DRAFT, 'Draft'),
    (CHANGELOG_UPCOMING, 'Upcoming'),
    (CHANGELOG_PUBLISHED, 'Published')
)

AUDITLOG_FIELDS = ['created_by', 'created_date', 'modified_by', 'modified_date']
