class MongoRouter:
    """
    A router to control all database operations on models in the
    activity application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read activity models go to mongo_db.
        """
        if model._meta.app_label == 'activity':
            return 'activity'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write activity models go to mongo_db.
        """
        if model._meta.app_label == 'activity':
            return 'activity'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the activity app is involved.
        """
        if obj1._meta.app_label == 'activity' or \
           obj2._meta.app_label == 'activity':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the activity app only appears in the 'mongo_db'
        database.
        """
        if app_label == 'activity':
            return db == 'activity'
        return None
