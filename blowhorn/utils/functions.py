import re
import math
import json
import logging
import datetime
import functools
import phonenumbers
from dateutil import tz
from math import sqrt, radians, cos
from django.conf import settings
from django.contrib.admin.options import IncorrectLookupParameters
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.utils.html import mark_safe
from django.utils.translation import ugettext_lazy as _

from blowhorn.driver import constants as driver_const
from blowhorn.common.helper import CommonHelper
from blowhorn.utils.datetime_function import DateTime

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

EARTH_RADIUS_KM = 6373.0  # radius of earth in km
VEHICLE_THEORETICAL_SPEED_LIMIT_KMPH = 100
VEHICLE_THEORETICAL_SPEED_LIMIT_AVG_KMPH = 30

IST_OFFSET_HOURS = 5.5
IST = tz.tzoffset('IST', datetime.timedelta(hours=IST_OFFSET_HOURS))

# Function to convert UTC to IST


def utc_to_ist(date):
    return DateTime().get_locale_datetime(date)


# Function to convert UTC to IST
def get_in_ist_timezone(date):
    return date.astimezone(IST)


# Function to convert UTC to IST
def ist_to_utc(date):
    time_util = DateTime()
    return time_util.get_locale_datetime(date, time_util.get_timezone('UTC'))


def format_datetime(datetime_obj, target_format):
    if datetime_obj:
        return utc_to_ist(datetime_obj).strftime(target_format)
    return ''


def timedelta_to_str(delta):
    if not isinstance(delta, datetime.timedelta):
        return ''
    seconds = delta.total_seconds()
    hours = seconds // 3600
    minutes = (seconds - (hours * 3600)) // 60
    return '%d Hours %d Mins' % (hours, minutes)


# Format timedelta object
def formatTD(td):
    hours = td.seconds // 3600
    minutes = (td.seconds % 3600) // 60
    seconds = td.seconds % 60
    return '%s:%s:%s' % (hours, minutes, seconds)


def timedelta_to_hmsstr(delta):
    if not isinstance(delta, datetime.timedelta):
        return ''
    seconds = delta.total_seconds()
    hours = seconds // 3600
    minutes = (seconds - (hours * 3600)) // 60
    return '%dh %dm %ss' % (hours, minutes, str(seconds)[0])


def minutes_to_hmstr(total_minutes):
    if isinstance(total_minutes, int) or isinstance(total_minutes, float):
        hours = total_minutes // 60
        minutes = total_minutes % 60
        return '%2dh %02dm' % (hours, minutes)

    return '-'


def minutes_to_hh_mm(total_minutes):
    if isinstance(total_minutes, int) or isinstance(total_minutes, float):
        hours = total_minutes // 60
        minutes = total_minutes % 60
        return '%2d:%02d' % (hours, minutes)

    return '-'


def minutes_to_dhms(total_minutes):
    """
    :param total_minutes: minutes in FLOAT or INT
    :returns Formatted time-string (day, hour, minute, seconds)
    """
    if not isinstance(total_minutes, int) and \
            not isinstance(total_minutes, float):
        return ''

    seconds = total_minutes * 60
    days = seconds // (24 * 3600)

    seconds = seconds % (24 * 3600)
    hours = seconds // 3600

    seconds %= 3600
    minutes = seconds // 60

    seconds %= 60

    periods = [('d', int(days)), ('h', str('%02d' % hours)),
               ('m', str('%02d' % minutes)), ('s', str('%02d' % seconds))]
    time_string = ' '.join('{}{}'.format(value, name)
                           for name, value in periods
                           if value)
    return time_string


def splice_route_info(route_info, start_time=None, end_time=None):
    return [x for x in route_info if (start_time is None or x.created_time >= start_time)
            and (end_time is None or x.created_time <= end_time)]


def dist_between_2_points(p1, p2):
    """ Distance between 2 points in kms.
        p1 and p2 have attributes lat and lon
    """

    delta_lat = radians(p2.y - p1.y)
    delta_lon = radians(p2.x - p1.x)
    unit_radius_distance_squared = \
        delta_lat ** 2 + cos(radians(p1.y)) * \
        cos(radians(p2.y)) * delta_lon ** 2
    earth_radius_distance = EARTH_RADIUS_KM * \
        sqrt(unit_radius_distance_squared)
    return earth_radius_distance


def get_current_delta(date, now=None):
    if not isinstance(date, datetime.datetime):
        return ''

    if now is None:
        now = utc_to_ist(datetime.datetime.now())
    else:
        assert isinstance(now, datetime.datetime)

    delta = now - date
    seconds = int(delta.total_seconds())
    if seconds > 3600:
        hours = seconds / 3600
        minutes = (seconds - hours * 3600) / 60
        return '-%dh %dm' % (hours, minutes)

    elif seconds > 60:
        minutes = seconds / 60
        seconds = seconds - minutes * 60
        return '-%dm %ds' % (minutes, seconds)

    elif seconds > 10:
        return '-%ds' % (seconds)

    else:
        return ''


def get_tracking_url(request, order_number):
    tracking_url = '%s/track/%s' % (request.META.get('HTTP_HOST',
                                                     'blowhorn.com'), order_number)
    return tracking_url


def validate_phone_number(phone_number):
    """
    Validation Rule for the phone number used are as below:
        - Phone number should be of exactly 10 digits
        - If the phone number starts with 0 then it should immidiately be followed by a
          non-zero digit and then any digit between 0 to 9 else it will result into error
        - If the phone number starts with non-zero digit then it can be
          followed by any digit including zero
    """
    phone_number = phonenumbers.parse(phone_number, settings.COUNTRY_CODE_A2)
    match = phonenumbers.is_valid_number(phone_number)
    if match:
        return phone_number
    else:
        raise ValidationError(_("Please enter a valid mobile number"))


# Function to convert list of coordinates into distance in km
# Using a very simple formula
def geopoints_to_km(points, include_large_delta=True):
    distance = 0.0
    for i, p in enumerate(points[:-1]):
        p1 = points[i]
        p2 = points[i + 1]
        this_d = dist_between_2_points(p1, p2)
        if this_d < 1 or include_large_delta:
            distance += this_d
    return distance


def lat_lon_to_km(points, include_large_delta=True):
    distance = 0.0
    if points and points[0] and points[1]:
        this_d = dist_between_2_points(points[1], points[0])
        if this_d < 1 or include_large_delta:
            distance += this_d
    return distance


def smoothen_geopoints(
    points,  # Array of locations
    max_location_accuracy_meters=10,
    min_time_delta_seconds=1,
    max_speed_limit=VEHICLE_THEORETICAL_SPEED_LIMIT_KMPH,
):
    """ Function to smoothen a given array of geopoints
        based on gps accuracy, time closeness of adjacent points
        calculated speed between adjacent points
    """

    accurate_points = [
        p for p in points if float(p.location_accuracy_meters) < 30]
    smoothened_points = accurate_points[:1]
    max_speed_violated_points = 0
    low_speed_small_distance_points = 0
    time_closeness_points = 0
    total_distance_km = 0.0
    total_distance_small_distance_inclusive_km = 0.0
    smoothened_distance_km = 0.0
    for i, p in enumerate(accurate_points[1:]):
        p1 = smoothened_points[-1]
        p2 = p
        if p1.geopoint:
            p1_geopoint, p2_geopoint = p1.geopoint, p2.geopoint
        else:
            p1_geopoint = CommonHelper.get_geopoint_from_latlong(
                p1.latitude, p1.longitude)
            p2_geopoint = CommonHelper.get_geopoint_from_latlong(
                p2.latitude, p2.longitude)

        distance_km = dist_between_2_points(p1_geopoint, p2_geopoint)
        p1_gps_timestamp, p2_gps_timestamp = p1.gps_timestamp, p2.gps_timestamp
        if type(p1_gps_timestamp) == str:
            p1_gps_timestamp = datetime.datetime.strptime(
                p1_gps_timestamp, "%Y-%m-%d %H:%M:%S")
            p2_gps_timestamp = datetime.datetime.strptime(
                p2_gps_timestamp, "%Y-%m-%d %H:%M:%S")
        time_delta_secs = (p2_gps_timestamp - p1_gps_timestamp).total_seconds()
        total_distance_km += distance_km

        if time_delta_secs < min_time_delta_seconds or distance_km < 0.02:
            time_closeness_points += 1
            continue

        this_accuracy = float(p1.location_accuracy_meters) + \
                        float(p2.location_accuracy_meters)
        print(distance_km, type(distance_km), this_accuracy, type(this_accuracy))
        if distance_km * 1000 < this_accuracy:
            continue

        # Sanity check if this point makes sense like a random gps jump will
        # result in high speed
        speed_instant_kmph = distance_km * 60 * 60 / time_delta_secs
        if speed_instant_kmph > max_speed_limit:
            max_speed_violated_points += 1
            continue

        # Filter out points that usually occur when device is stationary for a
        # long time and add to noise
        # - Calculated speed < 5% max
        # - GPS speed < 5% max
        # - Distance < accuracy threshold
        if speed_instant_kmph < 0.05 * max_speed_limit \
            and float(p2.vehicle_speed_kmph) < 0.05 * max_speed_limit \
            and (distance_km * 1000) < max_location_accuracy_meters:
            low_speed_small_distance_points += 1
            total_distance_small_distance_inclusive_km += distance_km
            continue

        smoothened_distance_km += distance_km
        smoothened_points.append(p2)

#    logging.info('Max speed violated points: %d' % max_speed_violated_points)
#    logging.info('Low speed small distance points: %d' % low_speed_small_distance_points)
#    logging.info('Time closeness points: %d' % time_closeness_points)
#    logging.info('Smoothened Points = %d' % len(smoothened_points))
#    logging.info('Unfiltered Distance : %.1f km' % total_distance_km)
#    logging.info('Small distance inclusive Distance : %.1f km' % total_distance_small_distance_inclusive_km)
#    logging.info('Smoothened Distance = %.1f km' % smoothened_distance_km)
    return smoothened_points


def validate_name(name):
    """
    :param name: Name to be validated
    :return: name if it is valid else False
    """
    # Name should be only alphabetic and can have only single space between
    # adjacent words
    reg_exp = re.compile(r"^([A-Za-z]){1}( ?[A-Za-z]){1,}$", re.IGNORECASE)
    match = reg_exp.match(name)
    if match is not None:
        return name
    else:
        return False


def validate_vehicle_registration_number(vehicle_number):
    """
    :param vehicle_number: Vehicle registration number to be validated
    :return: vehicle number if it is valid else return False
    """
    # Supports vehicle number like DL3C1234, KA051234, CAE111, KA05EC1111
    reg_exp = re.compile(r"^[A-Za-z]{2,}[A-Za-z0-9]*[0-9]+$", re.IGNORECASE)
    match = reg_exp.match(vehicle_number)
    if match is not None:
        return vehicle_number
    else:
        return False


def log_db_queries(f):
    """
    Decorator to log the database queries for a function
    """
    from django.db import connection

    def new_f(*args, **kwargs):
        initial_query_count = len(connection.queries)
        res = f(*args, **kwargs)
        after_query_count = len(connection.queries)
        if after_query_count > initial_query_count:
            logging.debug("*" * 80)
            logging.debug("Query log for %s" % f)
            logging.debug("Query COUNT : %d" %
                          (after_query_count - initial_query_count))
            logging.debug("Query TIME: %.1fms" % (functools.reduce(
                lambda x, y: x + float(y["time"]), connection.queries, 0.0) * 1000))
            for q in connection.queries[initial_query_count:after_query_count]:
                logging.debug("%s: %s" % (q["time"], q["sql"]))
        return res
    return new_f


def calculate_angle_between_two_points(point_a, point_b):
    """
    :param point_a, point_b: location (GEODJANGO Point)
    :returns: angle in degress (clockwise from north)
    """
    if not isinstance(point_a, Point) or not isinstance(point_b, Point):
        logger.info('Given input is not a Point object: %s %s' %
                    (point_a, point_b))
        return 0

    delta_y = point_b.y - point_a.y
    delta_x = (point_b.x - point_a.x) * math.cos(
        math.radians(point_b.y))
    orientation = 90 - math.degrees(math.atan2(delta_y, delta_x))
    return orientation


def get_base_url(request):
    """
    :param request: django request object
    :return: Host url (base)
    """
    if not request:
        if settings.DEBUG:
            return 'http://localhost'
        return settings.HOST or 'http://blowhorn.com'

    url_protocol = 'https'
    # The wsgi url scheme is not working as expected
    # url_protocol = 'https'  # request.META.get('wsgi.url_scheme', 'http')
    return ''.join([url_protocol, '://', request.META.get('HTTP_HOST', 'blowhorn.com')])


def get_host(request):
    """
    :param request: django request object
    :return: host name
    """
    return request.META.get('HTTP_HOST', None)


def validate_age(date_of_birth, date_format):
    """
        Validation for the age of the Driver. If Driver's age is above 18
        then return date of birth else throw an error
    """
    try:
        born = datetime.datetime.strptime(str(date_of_birth), date_format)
        now = datetime.datetime.now()
        calc_age = ((now - born).days / driver_const.NUMBER_OF_DAYS_IN_YEAR)
        if driver_const.DRIVER_MIN_AGE <= calc_age:
            return date_of_birth
        else:
            raise ValidationError(
                "Driver should be above " + str(driver_const.DRIVER_MIN_AGE) + " years of age")
    except ValueError:
        raise ValidationError('Incorrect data format, should be YYYY-MM-DD')


def get_value_from_obj(obj, key, default=None):
    if hasattr(obj, key):
        return getattr(obj, key, default)
    return None


def get_formatted_log(instance, field_name):
    """
    :param instance: instance
    :param field_name: name of log field in instance
    :return: html formatted log
    """
    if instance and hasattr(instance, field_name):
        changes = json.loads(getattr(instance, field_name))
        headers = changes[0].keys() if len(changes) > 0 else []
        template = """
            <table class='history'><thead>
        """
        for h in headers:
            template += """<th>{header}</th>""".format(header=h)

        template += """</thead><tbody>"""
        for row in changes:
            template += """
                <tr>
                    <td>{field}</td>
                    <td>{old_val}</td>
                    <td>{new_val}</td>
                </tr>
            """.format(
                field=row.get('field'),
                old_val=row.get('old'),
                new_val=row.get('new')
            )
        template += "</tbody></table>"
        return mark_safe(template)
    return ''


def set_slack_field_details(title, val, short='false', link=None):
    return {
        'title': title,
        'value': '%s' % val if not link else '<%s|%s>' % (link, val),
        'short': short
    }


def convert_date_hierarchy_params(date_hierarchy, lookup_params):
    """
    Extracted block from `get_filters()` in `ChangeList` class for conversion
    of date_hierarchy while using ajax API calls
    :param date_hierarchy: field defined in `date_hierarchy` in Admin class
    :param lookup_params: dict of query params
    :return: updated lookup_params dict with date-range
    """
    year = lookup_params.pop('%s__year' % date_hierarchy, None)
    if year is not None:
        month = lookup_params.pop('%s__month' % date_hierarchy, None)
        day = lookup_params.pop('%s__day' % date_hierarchy, None)
        try:
            from_date = datetime.datetime(
                int(year),
                int(month if month is not None else 1),
                int(day if day is not None else 1),
            )
        except ValueError as e:
            raise IncorrectLookupParameters(e) from e
        # if settings.USE_TZ:
        #     from_date = make_aware(from_date)
        if day:
            to_date = from_date + datetime.timedelta(days=1)
        elif month:
            to_date = (from_date + datetime.timedelta(days=32)).replace(day=1)
        else:
            to_date = from_date.replace(year=from_date.year + 1)
        lookup_params.update({
            '%s__gte' % date_hierarchy: from_date,
            '%s__lt' % date_hierarchy: to_date,
        })

    return lookup_params
