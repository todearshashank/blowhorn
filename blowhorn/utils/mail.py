# System and Django libraries
import os
import zipfile
import smtplib
import datetime
import logging
import mimetypes
import json
from dateutil.parser import parse
from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Case, When, Value, F, IntegerField, Sum, Min, Subquery, OuterRef
from django.template import loader
from django.db.models.functions import Coalesce
from django.utils import timezone
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model

# Blowhorn imports
from blowhorn.utils.functions import utc_to_ist
from blowhorn.utils.static_map import get_static_map_url
from blowhorn.customer.submodels.invoice_models import CustomerInvoice, InvoiceHistory, MISDocument
from blowhorn.customer.models import CustomerReceivable
from blowhorn.utils.html_to_pdf import render_to_pdfstream
from blowhorn.customer.constants import (SENT_TO_CUSTOMER, CUSTOMER_CONFIRMED,
                                         CUSTOMER_ACKNOWLEDGED, PARTIALLY_PAID,
                                         CREDIT_PERIOD_START_DATE, PAYMENT_DONE,
                                         CUSTOMER_MAIL_SUBJECT,
                                         CREDIT_PERIOD_MAIL_SUBJECT, CREDIT_PERIOD_MAIL_BODY,
                                         CREDIT_AMOUNT_MAIL_SUBJECT, CREDIT_AMOUNT_MAIL_BODY,
                                         APPROVED, TERMS_AND_CONDITION, KIOSK_TERMS_OF_SERVICE_ROW_TEMPLATE,
                                         KIOSK_TERMS_OF_SERVICE_TEMPLATE)

DATE_FORMAT = '%d-%b-%Y'

CustomerInvoice = get_model('customer', 'CustomerInvoice')
Contract = get_model('contract', 'Contract')
Company = get_model('company', 'Company')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class BlowhornMail(object):

    def __init__(self, subject, recipients):
        self.subject = subject
        self.recipients = recipients
        self.htmlbody = ''
        self.sender = settings.ADMIN_SENDER_ID
        self.senderpass = settings.EMAIL_HOST_PASSWORD
        self.attachments = []

    def send(self):
        msg = MIMEMultipart('alternative')
        msg['From'] = self.sender
        msg['Subject'] = self.subject
        # TO must be array of the form ['mailsender135@gmail.com']
        # msg['To'] = ", ".join(self.recipients)
        msg['To'] = self.recipients
        msg.preamble = ""
        # check if there are attachments if yes, add them
        if self.attachments:
            self.attach(msg)
        # add html body after attachments
        msg.attach(MIMEText(self.htmlbody, 'html'))
        # send
        s = smtplib.SMTP('smtp.gmail.com:587')
        s.ehlo()
        s.starttls()
        s.login(self.sender, self.senderpass)
        s.sendmail(self.sender, self.recipients, msg.as_string())
        s.quit()

    def htmladd(self, html):
        self.htmlbody = self.htmlbody + '<p></p>' + html

    def attach(self, msg):
        for f in self.attachments:

            ctype, encoding = mimetypes.guess_type(f)
            if ctype is None or encoding is not None:
                ctype = "application/octet-stream"

            maintype, subtype = ctype.split("/", 1)

            if maintype == "text":
                fp = open(f)
                # Note: we should handle calculating the charset
                attachment = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "image":
                fp = open(f, "rb")
                attachment = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "audio":
                fp = open(f, "rb")
                attachment = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(f, "rb")
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                fp.close()
                encoders.encode_base64(attachment)
            attachment.add_header(
                "Content-Disposition", "attachment", filename=f)
            attachment.add_header('Content-ID', '<{}>'.format(f))
            msg.attach(attachment)

    def addattach(self, files):
        self.attachments = self.attachments + files


class Email(object):

    def send_credit_period(self):
        # Inactive

        # list2 = ContractInvoiceFragment.objects.values('contract__city', 'contract__customer__id')
        newline_delimiter = '\n'
        invoices = CustomerInvoice.objects.filter(
            status__in=[APPROVED, CUSTOMER_CONFIRMED, SENT_TO_CUSTOMER,
                        CUSTOMER_ACKNOWLEDGED, PARTIALLY_PAID],
                        customer__enterprise_credit_applicable=True,
            service_period_start__gte=CREDIT_PERIOD_START_DATE).values(
            'customer__name', 'customer__id',
            'customer__enterprise_credit_amount',
            'customer__enterprise_credit_period')\
            .annotate(tsum=Sum('total_amount'), min_date=Min('invoice_date'))\
            .order_by('customer_id').exclude(status=PAYMENT_DONE)\
            .annotate(sales_spoc=ArrayAgg('contract_invoices__contract__city__sales_representatives__email',
            distinct=True))
        u_amount = CustomerReceivable.objects.filter(deposit_date__gte=CREDIT_PERIOD_START_DATE).values(
            'customer_id').annotate(uamount=Coalesce(Sum('unallocated_amount'), 0))
        invoices = invoices.annotate(
            unallocated_amount=Subquery(u_amount.filter(customer_id=OuterRef('customer_id')).values('uamount'),
                                        output_field=IntegerField()))

        receivable = dict(CustomerReceivable.objects.filter(status=APPROVED).values_list('customer_id')
                          .annotate(rsum=Sum('amount')))
        for i in invoices:

            if i.get('customer__enterprise_credit_period', 0) \
                and i.get('customer__enterprise_credit_period', 0) > 0 \
                and (datetime.date.today() - (i.get('min_date'))).days > (
                    i.get('customer__enterprise_credit_period', 0) or 0):
                    sales_recipients = self.__clean_recipients(i.get('sales_spoc'))
                    sales_recipients += self.__clean_recipients(settings.DEFAULT_CREDIT_LIMIT_NOTIFICATIONS)
                    subject = CREDIT_PERIOD_MAIL_SUBJECT.format(
                        cust=i['customer__name'],
                    )

                    html_content = CREDIT_PERIOD_MAIL_BODY.format(
                        customer=i.get('customer__name'),
                        period=i.get('customer__enterprise_credit_period', 0),
                        newline_delimiter=newline_delimiter)

                    if settings.ENABLE_SLACK_NOTIFICATIONS:
                        mail = EmailMultiAlternatives(
                            subject,
                            html_content,
                            to=sales_recipients,
                            from_email=settings.DEFAULT_FROM_EMAIL)

                        # mail.send()

            if i['unallocated_amount'] is not None:
                paid = float(i['tsum']) - receivable.get(i.get('customer__id'), 0) \
                       - float(i['unallocated_amount'])
            else:
                paid = float(i['tsum']) - receivable.get(i.get('customer__id'), 0)

            if i.get('customer__enterprise_credit_amount', 0) and \
                i.get('customer__enterprise_credit_amount', 0) > 0 and paid > \
                (i.get('customer__enterprise_credit_amount', 0) or 0):
                        sales_recipients = self.__clean_recipients(i.get('sales_spoc'))
                        sales_recipients += self.__clean_recipients(settings.DEFAULT_CREDIT_LIMIT_NOTIFICATIONS)
                        subject = CREDIT_AMOUNT_MAIL_SUBJECT.format(
                            cust=i['customer__name'],
                        )

                        html_content = CREDIT_AMOUNT_MAIL_BODY.format(
                            customer=i.get('customer__name'),
                            amount=i.get('customer__enterprise_credit_amount',0),
                            newline_delimiter=newline_delimiter)

                        if settings.ENABLE_SLACK_NOTIFICATIONS:
                            email = EmailMultiAlternatives(
                                subject,
                                html_content,
                                to=sales_recipients,
                                from_email=settings.DEFAULT_FROM_EMAIL)

                            # email.send()

    def send_receipt(self, payment_mode, order, trip, customer_email=None,
                     pdf_data=None):
        order_number = order.number
        today = utc_to_ist(timezone.now()).strftime('%d %b, %Y')
        customer_email = customer_email or [order.customer.user.email, ]
        customer_name = order.customer.user.name
        pickup_geopt = order.pickup_address.geopoint
        dropoff_geopt = order.shipping_address.geopoint
        contract = order.customer_contract
        is_default_build = settings.WEBSITE_BUILD == settings.DEFAULT_BUILD
        try:
            route_static_map_url = get_static_map_url(
                [], pickup_geopt, dropoff_geopt)
        except:
            route_static_map_url = None

        subject = '{brand_name} receipt for booking {order_number}'.format(
            brand_name=settings.BRAND_NAME,
            order_number=order_number
        )
        site_url = os.environ.get('HTTP_ORIGIN', '') or settings.HOST
        static_base = settings.ROOT_DIR.path('website/static')
        logo_url = '%s/static/website/images/%s/logo/logo.png' % (
            settings.HOST, settings.WEBSITE_BUILD
        )

        currency_symbol_img = ''
        if is_default_build:
            currency_symbol_img = '%s/website/images/%s/currency/currency128x128.png' % \
                (static_base, settings.WEBSITE_BUILD)

        additional_data = {
            'currency': {
                'url': currency_symbol_img,
                'symbol': contract.currency_symbol
            },
            'logo_url': logo_url,
            'logo_size': {
                'width': '120px' if is_default_build else '60px',
                'height': '32px' if is_default_build else '60px',
            },
            'invoice_link': '%s/invoice/%s' % (site_url, order_number),
            'download_link': '%s/api/order/%s/invoice?output=pdf&download=true'
                             % (site_url, order_number),
            'tracking_link': '%s/track/%s' % (site_url, order_number),
            'feedback': {
                'contact_mail': settings.COMPANY_SUPPORT_EMAIL,
                'subject': 'My%20{brand_name}%20experience'.format(
                    brand_name=settings.BRAND_NAME)
            },
            'brand_name': settings.BRAND_NAME,
            'social_links': {
                'facebookId': settings.SOCIAL_LINKS.get('facebookId'),
                'twitterId': settings.SOCIAL_LINKS.get('twitterId'),
                'linkedinId': settings.SOCIAL_LINKS.get('linkedinId'),
            },
            'current_build': settings.WEBSITE_BUILD,
        }
        template_values = {
            'today': today,
            'blowhorn_url': os.environ.get('HTTP_ORIGIN', ''),
            'customer_name': customer_name,
            'route_static_map': route_static_map_url,
            'trip': trip,
            'driver_picture': 'cid:driverpicture',
            'booking_datetime': parse(trip['booking_datetime']),
            'trip_date': parse(trip['date']),
            'pickup_date': parse(trip['pickup']['date']),
            'dropoff_date': parse(trip['dropoff']['date']),
            'company_legal_name': ''
        }
        template_values.update(additional_data)
        email_template_name = 'website/email_receipt.html'
        html_content = loader.render_to_string(
            email_template_name, template_values)
        mail = EmailMessage(
            subject,
            html_content,
            to=customer_email,
            from_email=settings.DEFAULT_FROM_EMAIL
        )
        if pdf_data:
            filename = "invoice_%s.pdf" % order.number
            if isinstance(pdf_data, dict):
                pdf_data.update(additional_data)
                pdf_data = render_to_pdfstream(
                    'website/invoice_c2c/invoice.html', {'data': pdf_data})
            mail.attach(filename, pdf_data, 'application/pdf')

        mail.content_subtype = 'html'
        mail.send()

    def __clean_recipients(self, emails):
        """
        To remove duplicates and None values
        :param emails: email-ids (list/str)
        :return: unique list of email ids
        """
        if not emails:
            return []

        recipients = []
        if isinstance(emails, str):
            recipients = emails.split(',')
        else:
            recipients = emails

        recipients = list(set([r.lower() for r in recipients if r]))
        return recipients

    def __clean_cc_recipients(self, cc, requesting_user):
        """
        :param cc: email-ids (list/str)
        :param requesting_user: instance of User
        :return: unique list of email ids
            Company invoice email and requested user's email will be added by
            default
        """
        cc_recipients = [settings.DEFAULT_INVOICE_MAIL]
        if requesting_user:
            cc_recipients.append(requesting_user.email)

        if cc:
            if isinstance(cc, str):
                cc = cc.split(',')

            if settings.DEFAULT_INVOICE_MAIL in cc:
                cc_recipients.remove(settings.DEFAULT_INVOICE_MAIL)

            cc_recipients.extend(cc)

        cc_recipients = list(set([r.lower() for r in cc_recipients if r]))
        return cc_recipients

    def send_invoice(self, customer_invoice, emails, start_date, end_date,
                     zip_invoice=False, requesting_user=None, cc=None,
                     is_test_mail=False):
        start_date = start_date.strftime(DATE_FORMAT)
        end_date = end_date.strftime(DATE_FORMAT)

        if is_test_mail:
            emails = settings.DEFAULT_TESTMAIL
        recipients = self.__clean_recipients(emails)
        cc_recipients = self.__clean_cc_recipients(cc, requesting_user)
        cc_recipients = list(set(cc_recipients).difference(set(recipients)))

        if zip_invoice:
            customer = customer_invoice[0].customer
            invoice_state = customer_invoice[0].invoice_state
        else:
            customer = customer_invoice.customer
            invoice_state = customer_invoice.invoice_state

        subject = CUSTOMER_MAIL_SUBJECT.format(
            customer=customer,
            period_start=start_date,
            period_end=end_date,
            invoice_state=invoice_state
        )

        context = {
            "start_date" : start_date,
            "end_date" : end_date,
            "brand_name" : settings.BRAND_NAME,
            "invoice_link" : '%s/dashboard/enterpriseinvoice/pending' % settings.HOST
        }

        email_template_name = 'emailtemplates_v2/customer_invoice.html'
        html_content = loader.render_to_string(email_template_name, context)

        mail = EmailMultiAlternatives(
            subject,
            html_content,
            to=recipients,
            cc=cc_recipients,
            from_email=settings.DEFAULT_INVOICE_MAIL)
        mail.attach_alternative(html_content, "text/html")

        if not zip_invoice:
            _file = customer_invoice.file.read()
            mail.attach(os.path.basename(str(customer_invoice.file)), _file)
            supporting_docs = MISDocument.objects.filter(invoice=customer_invoice)

            for doc in supporting_docs:
                document = doc.mis_doc
                if document:
                    _file = document.read()
                    mail.attach(os.path.basename(str(doc.mis_doc.file)), _file)


            print("Sending invoice..")
            if is_test_mail or settings.ENABLE_SLACK_NOTIFICATIONS:
                mail.send()

        else:
            supporting_docs = MISDocument.objects.filter(invoice__in=customer_invoice)
            with zipfile.ZipFile(settings.TEMPORARY_FILE_DIR + '/' + str(customer) + '.zip', 'w') as myzip:
                for f in customer_invoice:
                    myzip.writestr(str(f.invoice_number) +
                                    '.pdf', f.file.read())

                for doc in supporting_docs:
                    document = doc.mis_doc
                    if document:
                        _file = document.read()
                        myzip.writestr(str(os.path.basename(document.file.name)), _file)

            zf = open(myzip.filename, 'rb')
            _file = zf.read()

            mail.attach(os.path.basename(
                str(myzip.filename)), _file, 'text/zip')

            print("Sending invoice..")
            if is_test_mail or settings.ENABLE_SLACK_NOTIFICATIONS:
                mail.send()

            os.remove(settings.TEMPORARY_FILE_DIR +
                        '/' + str(customer) + '.zip')

            history_objs = []
            if not is_test_mail:
                due_date = datetime.date.today() + datetime.timedelta(days=10)

                for invoice in customer_invoice:
                    his_due_date = due_date if not (
                        invoice.status == SENT_TO_CUSTOMER) else invoice.due_date

                    history_objs.append(
                        InvoiceHistory(
                            old_status=invoice.status,
                            new_status=SENT_TO_CUSTOMER,
                            invoice_id=invoice.id,
                            action_owner=invoice.current_owner,
                            due_date=his_due_date,
                            created_by=requesting_user if requesting_user else None
                        )
                    )

                if history_objs:
                    InvoiceHistory.objects.bulk_create(history_objs)

                cust_invoice = CustomerInvoice.objects.filter(
                    id__in=customer_invoice)
                cust_invoice.update(
                    modified_date=datetime.datetime.now(),
                    modified_by=requesting_user,
                    status=SENT_TO_CUSTOMER,
                    due_date=Case(When(status=APPROVED,
                                    then=Value(due_date)),
                                When(status=SENT_TO_CUSTOMER, then=F('due_date')),
                                default=Value(due_date)))

    def get_cost_beakup(self, order):
        if order.invoice_details:
            components = json.loads(order.invoice_details)
        else:
            components = {}

        data = {}
        data['cgst_percent'] = 'CGST@0%'
        data['cgst_amount'] = 0
        data['sgst_percent'] = 'SGST@0%'
        data['sgst_amount'] = 0
        data['igst_percent'] = 'IGST@0%'
        data['igst_amount'] = 0
        data['vat_percent'] = 'VAT@0%'
        data['vat_amount'] = 0

        data['base_fare'] = 0
        data['time_amount'] = 0
        data['dist_amount'] = 0
        data['labour'] = 0
        data['additional_fare'] = 0
        data['vas'] = 0

        if components:
            data['base_fare'] = components.get('fixed', 0)
            cgst = components.get('tax_distribution', None) and \
                   components.get('tax_distribution').get('CGST')

            if cgst:
                data['cgst_percent'] = 'CGST@' + \
                                       str(cgst.get('percentage', 0)) + '%'
                data['cgst_amount'] = cgst.get('amount', 0)

            sgst = components.get('tax_distribution', None) and \
                   components.get('tax_distribution').get('SGST')

            if sgst:
                data['sgst_percent'] = 'SGST@' + \
                                       str(sgst.get('percentage', 0)) + '%'
                data['sgst_amount'] = sgst.get('amount', 0)

            igst = components.get('tax_distribution', None) and \
                   components.get('tax_distribution').get('IGST')

            if igst:
                data['igst_percent'] = 'IGST@' + \
                                       str(igst.get('percentage', 0)) + '%'
                data['igst_amount'] = igst.get('amount', 0)

            vat = components.get('tax_distribution', None) and \
                   components.get('tax_distribution').get('VAT')

            if vat:
                data['vat_percent'] = 'VAT@' + \
                                       str(vat.get('percentage', 0)) + '%'
                data['vat_amount'] = vat.get('amount', 0)

            time_cal = components.get('time', '')
            time_amount = 0

            night_charges = 0
            # night_cal = components.get('Night Charges', '')
            # for cal in night_cal:
            #     night_charges += cal.get('amount', 0)
            # data['night_charges'] = night_charges

            for time in time_cal:
                time_amount += time.get('amount', 0)
            data['time_amount'] = time_amount

            dist_cal = components.get('distance', '')
            dist_amount = 0
            for dist in dist_cal:
                dist_amount += dist.get('amount', 0)

            vas_charges = components.get('vas_amount', [])
            vas_amount = 0
            for val in vas_charges:
                if val.get('attribute') == 'Night Charges':
                    night_charges += val.get('amount', 0)
                else:
                    vas_amount += val.get('amount', 0)

            vas_items = components.get('vas_item', [])
            vas_item_amount = 0
            for val in vas_items:
                vas_item_amount += val.get('amount', 0)

            data['vas'] = vas_amount
            data['night_charges'] = night_charges
            data['dist_amount'] = dist_amount
            data['labour'] = components.get('labour', 0)
            data['invoice_number'] = order.number

        return data

    def send_terms_of_service(self, order):
        subject = 'Blowhorn Order Confirmation - %s' % order.number
        context = {
            "brand_name" : settings.BRAND_NAME,
            "customer_name" : order.customer.name,
            "terms" : TERMS_AND_CONDITION
        }
        email_template_name = 'emailtemplates_v2/termsnconditions.html'
        html_content = loader.render_to_string(email_template_name, context)

        mail = EmailMultiAlternatives(
            subject, html_content, to=[order.customer.user.email],
            from_email=settings.DEFAULT_FROM_EMAIL)
        mail.attach_alternative(html_content, "text/html")
        mail.send()

    def clean_email_address(self, email):
        if email:
            name, domain = email.split('@')
            if domain in settings.DEFAULT_DOMAINS:
                return ''
            return email
        else:
            return ''

    def send_notification_to_spocs_regarding_driver_document(self, user, subject, body):
        """

        :param user:  spoc to which mail has to be sent
        :param driver:
        :return:
        """
        email = self.clean_email_address(user.email)

        if email:
            mail = EmailMessage(
                subject, body, to=[email], from_email=settings.DEFAULT_FROM_EMAIL)
            mail.content_subtype = 'html'
            mail.send()

    def email_spoc_supervisor(self, contract_id, status):
        spoc_email_list = []
        supervisor_email_list = []
        vehicle_class_list = []
        body_type_list = []

        contract = Contract.objects.get(pk=contract_id)

        spocs_email_list = [str(x) for x in contract.spocs.all()]
        spocs_email = ', '.join(spocs_email_list)

        supervisors_email_list = [str(x) for x in contract.supervisors.all()]
        supervisors_email = ', '.join(supervisors_email_list)

        vehicleclass = ', '.join(str(x)
                                 for x in contract.vehicle_classes.all())
        bodytypes = ', '.join(str(x) for x in contract.body_types.all())

        to_email_list = spocs_email_list + supervisors_email_list

        subject = 'Status change notification for contract %s' % (
            contract.name)
        content = 'Contract - %s has been updated to status %s.\n\nContract details:- \n' % (
            contract.name, status)
        content = content + 'Contract type                - %s\n' % (
            contract.contract_type)
        content = content + 'Description                  - %s\n' % (
            contract.description)
        content = content + 'Customer                     - %s\n' % (
            contract.customer)
        content = content + 'Customer Division            - %s\n' % (
            contract.division)
        content = content + 'Vehicle Classes              - %s\n' % (
            vehicleclass)
        content = content + 'Body Types                   - %s\n' % (bodytypes)
        content = content + 'Payment Cycle                - %s\n' % (
            contract.payment_cycle)
        content = content + 'GTA/BSS                      - %s\n' % (
            contract.service_tax_category)
        content = content + 'Supervisor email id list     - %s\n' % (
            supervisors_email)
        content = content + 'SPOC email id list           - %s\n' % (
            spocs_email)
        company = Company.objects.first()
        content = content + '\n\n\nRegards,\n' + 'Team %s\n' 'Blowhorn' if settings.COUNTRY_CODE_A2 == 'IN' else company.name

        mail = EmailMultiAlternatives(
            subject, content, to=to_email_list,
            from_email=settings.DEFAULT_FROM_EMAIL)
        # mail.send()

    def send_credit_limit_mail(self, email, notification_details):
        # email = email.split(',')

        company = Company.objects.first()
        subject = 'Customer exceeded the Credit Limit'

        if notification_details.get('name'):
            html_content = 'Hi, <br>' + str(notification_details['name']) + \
                           ' ' + '(' + str(notification_details.get('legal_name')) + ')'
        else:
            html_content = 'Hi, <br>' + \
                           str(notification_details.get('legal_name'))

        html_content += ' has exceeded the ' + \
                        str(notification_details.get('limit_percntg')) + '%'
        html_content += ' of its credit limit amount (Rs. ' + str(
            notification_details.get('credit_limit')) + ').<br>'
        html_content += '<br><br>' + 'Regards, <br> %s Team' % ('Blowhorn' if settings.COUNTRY_CODE_A2 == 'IN' else company.name)

        mail = EmailMultiAlternatives(
            subject, html_content, to=email,
            from_email=settings.DEFAULT_FROM_EMAIL)

        mail.attach_alternative(html_content, "text/html")
        # mail.send()

    def email_invoice_assignment_notification(self, notification_details):
        assignee_email = notification_details.get('assignee_email', None)

        if not assignee_email:
            return

        context = { 'brand_name' : settings.BRAND_NAME }
        if notification_details.get('invoice'):
            _temp = str(notification_details.get('invoice'))
            context['identifier'] = 'Number'
            context['invoice_numbrid'] = _temp
            subject = "Invoice Number %s assigned to you" % (_temp)

        else:
            _temp = str(notification_details.get('invoice_id'))
            context['identifier'] = 'ID'
            context['invoice_numbrid'] = str(notification_details.get('invoice'))
            subject = "Invoice ID %s assigned to you" % (_temp)

        context['status'] = notification_details.get('status')
        context['due_date'] = str(notification_details.get('due_date'))
        context['reason'] = notification_details.get('reason', 'NA')

        _name = notification_details.get('assignor_name', None)
        if not _name:
            _name = notification_details.get('assignor_email')
        context['assignor'] = _name

        _mis = notification_details.get('supporting_doc', None)
        context['is_mis_present'] = 'Yes' if _mis else 'No'

        email_template_name = 'emailtemplates_v2/invoice_assignment.html'
        html_content = loader.render_to_string(email_template_name, context)

        mail = EmailMultiAlternatives(
            subject, html_content, to=assignee_email,
            from_email=settings.DEFAULT_FROM_EMAIL)
        mail.attach_alternative(html_content, "text/html")

        if _mis:
            _file = _mis.read()
            mail.attach(os.path.basename(str(_mis)), _file)

        if settings.ENABLE_SLACK_NOTIFICATIONS:
            mail.send()

    def send_invoice_confirmation_mail_to_sales_representative(self, subject,
                                                               content,
                                                               recipients):
        """
        Notifies to sales representative who sent invoice to customer
        """
        if not isinstance(recipients, list):
            recipients = [recipients]
        recipients = ['%s' % recipient for recipient in recipients]
        mail = EmailMultiAlternatives(subject, content,
                                      to=recipients,
                                      cc=[settings.DEFAULT_INVOICE_MAIL],
                                      from_email=settings.DEFAULT_FROM_EMAIL)
        # mail.send()

    def send_email_message(self, email_ids, subject, body,
            from_email=settings.DEFAULT_FROM_EMAIL):
        if email_ids:
            mail = EmailMessage(
                subject, body, to=email_ids, from_email=from_email)
            mail.content_subtype = 'html'
            mail.send()

    def send_mail_with_attachment(self, recipients, subject, body,
                                  attachment_path):
        """
        :param recipients: list of emails
        :param subject:
        :param body:
        :param attachment_path:
        :return:
        """
        if settings.DEBUG:
            recipients = settings.DEFAULT_TESTMAIL
        else:
            recipients = self.__clean_recipients(recipients)

        logger.info('recipients: %s' % recipients)
        mail = EmailMultiAlternatives(
            subject,
            body,
            to=recipients,
            from_email=settings.DEFAULT_FROM_EMAIL
        )
        data = open(attachment_path, 'rb').read()
        mail.attach(
            os.path.basename(attachment_path),
            data
        )
        mail.send()
        # Cleanup file
        os.remove(attachment_path)

    def send_otp_email(self, recipients, subject, body):

        mail = EmailMultiAlternatives(subject, body,
                                      to=recipients,
                                      cc=[settings.DEFAULT_INVOICE_MAIL],
                                      from_email=settings.DEFAULT_FROM_EMAIL)
        mail.send()

    def send_donation_confirmation(self, recipient, txn_id):
        is_default_build = settings.WEBSITE_BUILD == settings.DEFAULT_BUILD
        content = {
            'homepage_url': settings.HOMEPAGE_URL,
            'logo': {
                'styles': {
                    'width': '120px' if is_default_build else '60px',
                    'height': '32px' if is_default_build else '60px',
                }
            },
            'feedback': {
                'contact_mail': settings.COVID19_SUPPORT_MAIL,
                'subject': 'Thank you for your donation'
            },
            'app_links': {
                'ios': {
                    'url': settings.IOS_APP_LINK,
                    'img': 'https://testblowhorn.s3.us-east-2.amazonaws.com/resource/applestore-badge.png'
                },
                'android': {
                    'url': settings.ANDROID_APP_LINK,
                    'img': 'https://testblowhorn.s3.us-east-2.amazonaws.com/resource/google-play-badge.png'
                },
            },
            'brand_name': settings.BRAND_NAME,
            'social_links': {
                'facebookId': settings.SOCIAL_LINKS.get('facebookId'),
                'twitterId': settings.SOCIAL_LINKS.get('twitterId'),
                'linkedinId': settings.SOCIAL_LINKS.get('linkedinId'),
            },
            'current_build': settings.WEBSITE_BUILD,
            'txn_id': txn_id
        }

        html_content = loader.render_to_string(
            'website/emails/donation_confirm.html', content
        )

        subject = 'Thank you for your generous donation towards our Blowhorn frontline drivers'
        from_email = 'Blowhorn <%s>' % settings.DEFAULT_FROM_EMAIL
        self.send_email_message([recipient], subject, html_content, from_email=from_email)

    def send_mail_with_html_content(self, recipients, subject, html_content, filename, file, body=None):
        recipients = self.__clean_recipients(recipients)
        if settings.ENABLE_SLACK_NOTIFICATIONS:
            mail = EmailMessage(
                subject,
                html_content,
                to=recipients,
                from_email=settings.DEFAULT_INVOICE_MAIL
            )

            mail.attach(filename, file)
            mail.content_subtype = 'html'
            mail.send()

