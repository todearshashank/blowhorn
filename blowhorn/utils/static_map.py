#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from urllib import request as urlrequest, parse as urlparse
from django.conf import settings


def encode_coords(coords):
    '''Encodes a polyline using Google's polyline algorithm

    See http://code.google.com/apis/maps/documentation/polylinealgorithm.html
    for more information.

    :param coords: Coordinates to transform (object having properties lat and lon)
    longitude).
    :type coords: list
    :returns: Google-encoded polyline string.
    :rtype: string
    '''

    result = []

    prev_lat = 0
    prev_lng = 0

    # for cord in coords:
    for lng_, lat_ in coords:
        x, y = lat_, lng_
        lat, lng = int(x * 1e5), int(y * 1e5)

        d_lat = _encode_value(lat - prev_lat)
        d_lng = _encode_value(lng - prev_lng)

        prev_lat, prev_lng = lat, lng

        result.append(d_lat)
        result.append(d_lng)

    return ''.join(c for r in result for c in r)


def _split_into_chunks(value):
    while value >= 32:  # 2^5, while there are at least 5 bits

        # first & with 2^5-1, zeros out all the bits other than the first five
        # then OR with 0x20 if another bit chunk follows
        yield (value & 31) | 0x20
        value >>= 5
    yield value


def _encode_value(value):
    # Step 2 & 4
    value = ~(value << 1) if value < 0 else (value << 1)

    # Step 5 - 8
    chunks = _split_into_chunks(value)

    # Step 9-10
    return (chr(chunk + 63) for chunk in chunks)


from blowhorn.utils.functions import dist_between_2_points


def get_tuple_of_index_distance_point(points):
    tuple_index_distance_point = []
    for i, p in enumerate(points[1:-2]):
        p1 = points[i]
        p2 = points[i + 1]
        distance = dist_between_2_points(p1, p2)
        tuple_index_distance_point.append(
            (i, distance, p1)
        )
    return tuple_index_distance_point


def prune_and_encode_coords(route):
    """ URL length limit is set to ~2050. So to restrict the length after encoding the path
        we've to prune the available points. Let's restrict the encoded path limit to 1750.
    """
    points_list = []
    for coords in route:
        for point in coords:
            points_list.append(point)

    ENCODED_PATH_LENGTH_LIMIT = 1600
    points_str = urlparse.quote(encode_coords(points_list))

    if len(points_str) <= ENCODED_PATH_LENGTH_LIMIT:
        return urlparse.unquote(points_str)

    tuple_index_distance_point = get_tuple_of_index_distance_point(points_list)
    sorted_on_adj_dist = sorted(
        tuple_index_distance_point, key=lambda x: -x[1])
    length_list = len(points_list)
    while len(points_str) > ENCODED_PATH_LENGTH_LIMIT:
        delta = len(points_str) - ENCODED_PATH_LENGTH_LIMIT
        to_reduce = int(float(delta) / len(points_str)) * length_list
        new_length = length_list - to_reduce
        sorted_on_adj_dist = sorted_on_adj_dist[:new_length - 2]
        resorted_on_index_list = sorted(sorted_on_adj_dist)
        points_only_list = [x[2] for x in resorted_on_index_list]
        pruned_list = [points_list[0]] + points_only_list + [points_list[-1]]
        points_str = urlparse.quote(encode_coords(pruned_list))
        length_list = len(points_only_list)

    return urlparse.unquote(points_str)


def get_static_map_url(points, pickup, dropoff):

    encoded_path = None
    if points:
        encoded_path = prune_and_encode_coords(points)

    begin_pt = '%s,%s' % (pickup.y, pickup.x)
    end_pt = '%s,%s' % (dropoff.y, dropoff.x)
    # import os
    # host = os.environ['HTTP_HOST']

    params = [
        ('sensor', 'false'),
        ('size', '800x350'),
        ('maptype', 'roadmap'),
        ('key', settings.GOOGLE_MAPS_API_KEY),
        ('markers', 'color:red|label:P|%s' % begin_pt),
        ('markers', 'color:green|label:D|%s' % end_pt),
        # ('markers', 'icon:http://%s/images/marker_pickup_small.png|%s' % (host, begin_pt)),
        # ('markers', 'icon:http://%s/images/marker_dropoff_small.png|%s' % (host, end_pt)),
    ]

    if encoded_path:
        params.append('path', 'color:0xF74757FF|geodesic:true|weight:5|enc:' + \
         encoded_path)

    # markers="markers=color:red|label:P|%s&markers=color:green|label:D|%s&" % (begin_pt, end_pt)
    # path="path=color:0xff0000ff|weight:5|enc:" + encoded_path
    # static_map_url =
    # "http://maps.googleapis.com/maps/api/staticmap?sensor=false&size=279x217&maptype=roadmap&"
    # + markers+path
    static_map_url = "http://maps.googleapis.com/maps/api/staticmap"
    url = static_map_url + '?' + urlparse.urlencode(params)
    return url
