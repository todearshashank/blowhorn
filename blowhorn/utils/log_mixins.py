from auditlog.mixins import LogEntryAdminMixin as LogEntryAdminMixin_

class LogEntryAdminMixin(LogEntryAdminMixin_):
    def created(self, obj):
        return obj.timestamp
    created.short_description = 'Created'
