import logging
import operator
import pytz
from datetime import datetime, timedelta
from dateutil import tz, parser
from django.conf import settings
from django.utils import timezone

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DateTime(object):

    ISO_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f%z'
    timedelta_operators = {
        "+": operator.add,
        "-": operator.sub
    }
    valid_timedelta_inputs = ['microseconds', 'milliseconds', 'seconds',
                              'minutes', 'hours', 'days', 'weeks', 'fold']

    def get_timezone(self, locale):
        """
        :param locale: locale string
        :return: tz object
        """
        return tz.gettz(locale)

    def convert_iso_string_to_datetime(self, iso_string, use_locale=None):
        """
        :param iso_string: datetime string in iso-format
        :param use_locale: locale string
        :return: datetime object
        """
        if use_locale is None:
            use_locale = timezone.utc
        return parser.parse(iso_string).astimezone(use_locale)

    def str_to_datetime(self, datetime_str, format):
        """
        :param datetime_str: datetime string
        :param format: date formatter string
            ref: https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes
        :return: datetime object
        """
        if not datetime_str or not format:
            return None

        try:
            return datetime.strptime(datetime_str, format)
        except ValueError:
            logger.info('Time data %s does not match with %s' % (datetime_str, format))
            return None


    def get_locale_datetime(self, input_date, use_locale=None):
        """
        Converts given datetime to datetime in given locale
        :param input_date: datetime/timezone object
        :param use_locale: locale string
        :return: datetime object in given locale
        """
        if input_date is None:
            input_date = timezone.now()

        if use_locale is None:
            use_locale = self.get_timezone(settings.ACT_TIME_ZONE)

        return input_date.astimezone(use_locale)

    def add_timedelta(self, input_date, delta_info):
        """
        :param input_date: datetime/timezone object
        :param delta_info: dict of delta info
        :return: given datetime + delta
        """
        return input_date + timedelta(**delta_info)

    def substract_timedelta(self, input_date, delta_info):
        """
        :param input_date: datetime/timezone object
        :param delta_info: dict of delta info
        :return: given datetime - delta
        """
        return input_date - timedelta(**delta_info)

    def get_timezone_offset():
        """
            Get offset based on timezone
        """
        tz = pytz.timezone(settings.ACT_TIME_ZONE)
        dt = datetime.utcnow()
        offset_seconds = tz.utcoffset(dt).seconds
        offset_hours = offset_seconds / 3600.0
        return offset_hours
