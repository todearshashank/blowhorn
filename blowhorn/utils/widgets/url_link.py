from django.core.exceptions import ImproperlyConfigured
from django.forms.utils import flatatt
from django.forms.widgets import Widget
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _


class URLLink(Widget):
    """
    URLLink v1.0.0: initial
    Custom widget for displaying url link
    attrs: attributes of input element.
    url_template: template of url with placeholder for value
    display_value: Custom text to display

    """
    url_template = None
    display_value = None

    def __init__(self, attrs=None, url_template=None, display_value=None):
        if url_template is None:
            raise ImproperlyConfigured(_('`url_template` is mandatory'))

        self.url_template = url_template
        self.display_value = display_value
        super(URLLink, self).__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        if value is None:
            value = ''

        try:
            href = self.url_template % value
        except ValueError:
            raise ImproperlyConfigured(
                _('`url_template` must contain string formatter')
            )

        extra_attrs = {
            'href': href,
            'target': '_blank'
        }
        input_attrs = {
            'type': 'hidden',
            'name': name,
            'value': value
        }
        input_final_attrs = self.build_attrs(attrs, input_attrs)
        link_final_attrs = self.build_attrs(attrs, extra_attrs)
        return format_html(
            '<input{} /><a {}>{}</a>',
            flatatt(input_final_attrs),
            flatatt(link_final_attrs),
            self.display_value or value,
        )
