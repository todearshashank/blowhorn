from django.contrib import admin
from django.utils.encoding import force_text, smart_text
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.filters import (
    RelatedOnlyFieldListFilter, SimpleListFilter as BaseSimpleListFilter
)
from django.core.exceptions import ImproperlyConfigured, ValidationError


class GenericDependancyFilter(RelatedOnlyFieldListFilter):
    """
    Generic filter for dependant list filter.
    Configuration:
        related_filter_model_fields:
            - type: dict
            - description: 
                * A dict containing field_path and it's dependant related model field_path as list
                  in current admin.
                * Declare this variable in respective model-admin
                - Example:
                    related_filter_model_fields = {
                        'customer_contract': ['city', 'customer'],
                        'hub': ['city', 'customer']
                    }
    """

    related_model_lookup_fields = {}

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.related_model_lookup_fields = {}
        if not hasattr(model_admin, 'related_filter_model_fields') or \
            model_admin.related_filter_model_fields is None:
            raise ImproperlyConfigured(
                "The dependancy list filter with field_path '%s' does not specify "
                "a 'related_filter_model_fields'." % field_path)

        related_fields = model_admin.related_filter_model_fields.get(field_path)
        for val in request.GET:
            # processed_param = val.replace(('%s__' % field_path), '')
            for f in related_fields:
                f_str = ('%s__id' % f)
                if f_str in val:
                    self.related_model_lookup_fields[f_str] = request.GET.get(val)

        super(GenericDependancyFilter, self).__init__(
            field, request, params, model, model_admin, field_path)

    def choices(self, changelist):
        selected = self.lookup_val is None and not self.lookup_val_isnull
        yield {
            'selected': selected,
            'query_string': changelist.get_query_string(
                {},
                [self.lookup_kwarg, self.lookup_kwarg_isnull]
            ),
            'display': _('All'),
        }
        is_lookup_val_present = False
        for pk_val, val in self.lookup_choices:
            yield {
                'selected': self.lookup_val == smart_text(pk_val),
                'query_string': changelist.get_query_string({
                    self.lookup_kwarg: pk_val,
                }, [self.lookup_kwarg_isnull]),
                'display': val,
            }

            if pk_val and self.lookup_val and int(pk_val) == int(self.lookup_val):
                is_lookup_val_present = True

        if not selected and self.lookup_val and not is_lookup_val_present:
            # Need to add previous value if lookup_val is not found in current choices
            # Since, unable to modify queryparam for list-view from here
            yield {
                'selected': self.lookup_val == smart_text(self.lookup_val),
                'query_string': changelist.get_query_string({
                    self.lookup_kwarg: self.lookup_val,
                }, [self.lookup_kwarg_isnull]),
                'display': _('-- RECHOOSE THIS --'),
            }

        if self.include_empty_choice:
            yield {
                'selected': bool(self.lookup_val_isnull),
                'query_string': changelist.get_query_string({
                    self.lookup_kwarg_isnull: 'True',
                }, [self.lookup_kwarg]),
                'display': self.empty_value_display,
            }

    def field_choices(self, field, request, model_admin):
        pk_qs = model_admin.get_queryset(request)
        pk_qs = pk_qs.filter(**self.related_model_lookup_fields)
        pk_qs = pk_qs.distinct().values_list('%s__pk' % self.field_path, flat=True)
        return field.get_choices(include_blank=False, limit_choices_to={'pk__in': pk_qs})


def simple_list_filter(*args, **kwargs):
    class BasicListFilter(BaseSimpleListFilter):
        """
        Generic basic list for customized query-matching
        """
        title = _(kwargs.pop('title', None))
        parameter_name = kwargs.pop('parameter_name', None)
        lookup_choices = kwargs.pop('lookup_choices', None)
        query_match_type = kwargs.pop('query_match_type', 'icontains')
        lookup_param = '%s__%s' % (parameter_name, query_match_type)

        if not lookup_choices:
            raise ImproperlyConfigured(_('`lookup_choices` is required'))

        def lookups(self, request, model_admin):
            return [c for c in self.lookup_choices]

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(**{self.parameter_name: self.value()})
            else:
                return queryset

    return BasicListFilter


def multiple_choice_list_filter(**kwargs):
    class MultipleChoiceListFilter(admin.SimpleListFilter):
        """
        Configuration:
            A dict. containing following data:
                - title (required): Label for filter
                - parameter_name (optional; title will be considered):
                    db column name
                - lookup_choices (list of strings)

        e.g.: multiple_choice_list_filter(**{
            'title': 'status',
            'parameter_name': 'status__in',
            'lookup_choices': [o1, o2, o3]
        })

        version: 1.0.0
        - List Choice filter with multiple value selection support
        - lookups method should be defined in child class
        version: 2.0.0
        - Checkbox UI for selecting values to prevent page refresh on every
            selection
        - Option to pass kwargs for config. instead of creating subclass
        """
        title = _(kwargs.pop('title', None))
        parameter_name = kwargs.pop('parameter_name', None)
        if parameter_name is None:
            parameter_name = '%s__in' % title
        lookup_choices = kwargs.pop('lookup_choices', None)
        template = 'multiple_choice_list_filter/filter.html'

        def lookups(self, request, model_admin):
            """
            returns: a list of tuples (value, verbose value)
            """
            if not self.lookup_choices:
                ImproperlyConfigured(_('Choices are mandatory'))

            return [(c, c) for c in self.lookup_choices]

        def queryset(self, request, queryset):
            if request.GET.get(self.parameter_name):
                extra_kwargs = {
                    self.parameter_name: request.GET[self.parameter_name].split(',')
                }
                queryset = queryset.filter(**extra_kwargs)
            return queryset

        def value_as_list(self):
            return self.value().split(',') if self.value() else []

        def choices(self, changelist):

            def amend_query_string(include=None, exclude=None):
                selections = self.value_as_list()
                if include and include not in selections:
                    selections.append(include)
                if exclude and exclude in selections:
                    selections.remove(exclude)
                if selections:
                    csv = ','.join(selections)
                    return changelist.get_query_string({self.parameter_name: csv})
                else:
                    return changelist.get_query_string(remove=[self.parameter_name])

            yield {
                'selected': self.value() is None,
                'query_string': changelist.get_query_string(
                    remove=[self.parameter_name]),
                'display': 'Reset',
                'reset': True,
            }
            for lookup, title in self.lookup_choices:
                yield {
                    'selected': str(lookup) in self.value_as_list(),
                    'query_string': changelist.get_query_string(
                        {self.parameter_name: lookup}),
                    'include_query_string': amend_query_string(include=str(lookup)),
                    'exclude_query_string': amend_query_string(exclude=str(lookup)),
                    'display': title,
                }
    return MultipleChoiceListFilter
