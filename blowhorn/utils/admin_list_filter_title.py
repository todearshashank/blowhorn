from django.contrib import admin


def list_filter_title(title):
    class Wrapper(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance

    return Wrapper


def list_related_filter_title(title):
    class Wrapper(admin.RelatedFieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.RelatedFieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance

    return Wrapper


def list_related_only_filter_title(title):
    class Wrapper(admin.RelatedOnlyFieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.RelatedOnlyFieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance

    return Wrapper
