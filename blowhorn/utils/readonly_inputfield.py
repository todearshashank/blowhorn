from django.forms.utils import flatatt
from django.forms.widgets import Widget
from django.utils.html import format_html


class ReadOnlyInput(Widget):
    """
    Custom widget for displaying formatted string
    @todo:
    1. Make generic to accept additional parameters
    and extra data in tooltip.
    """
    display_value = None

    def __init__(self, attrs=None, display_value=None):
        self.display_value = display_value
        super(ReadOnlyInput, self).__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        if value is None:
            value = ''

        extra_attrs = {
            'type': 'hidden',
            'name': name,
            'value': value
        }
        final_attrs = self.build_attrs(attrs, extra_attrs)
        return format_html('<input{} />{}', flatatt(final_attrs),
                self.display_value or value)
