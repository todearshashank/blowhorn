import json
from django.forms.utils import flatatt
from django.forms.widgets import DateTimeInput
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape

try:
    from django.utils.encoding import force_unicode as force_text
except ImportError:
    from django.utils.encoding import force_text


class DateTimePicker(DateTimeInput):
    """
        Widget is using bootstrap-datetimepicker.
        Initialize widget as below:
            models.DateField: {
              "widget": DateTimePicker(options={"format": "DD-MM-YYYY"})
            }

        For available options visit:
            https://eonasdan.github.io/bootstrap-datetimepicker

        Known Issues:
            1. If option with format is not passed,
                formatting is not working properly.
            2. Calendar position in inline is not proper,
                because of absolute position.
    """

    format_map = (('DDD', r'%j'),
                  ('DD', r'%d'),
                  ('MMMM', r'%B'),
                  ('MMM', r'%b'),
                  ('MM', r'%m'),
                  ('YYYY', r'%Y'),
                  ('YY', r'%y'),
                  ('HH', r'%H'),
                  ('hh', r'%I'),
                  ('mm', r'%M'),
                  ('ss', r'%S'),
                  ('a', r'%p'),
                  ('ZZ', r'%z'),
                  )

    @classmethod
    def conv_datetime_format_py2js(cls, datetime_format):
        for js, py in cls.format_map:
            datetime_format = datetime_format.replace(py, js)
        return datetime_format

    @classmethod
    def conv_datetime_format_js2py(cls, datetime_format):
        for js, py in cls.format_map:
            datetime_format = datetime_format.replace(js, py)
        return datetime_format

    html_template = '''
        <div%(div_attrs)s>
            <input%(input_attrs)s/>
            <div id="option_%(picker_id)s" hidden>%(options)s</div>
        </div>'''

    def __init__(self, attrs=None, format=None, options=None, div_attrs=None,
                 icon_attrs=None):

        if not icon_attrs:
            icon_attrs = {'class': 'glyphicon glyphicon-calendar'}

        if not div_attrs:
            div_attrs = {
                'class': 'input-group date',
                'style': 'position:relative;'
            }
        if format is None and options and options.get('format'):
            format = self.conv_datetime_format_js2py(options.get('format'))

        super(DateTimePicker, self).__init__(attrs, format)

        if 'class' not in self.attrs:
            self.attrs['class'] = 'form-control bDate'

        self.div_attrs = div_attrs and div_attrs.copy() or {}
        self.icon_attrs = icon_attrs and icon_attrs.copy() or {}
        self.picker_id = self.div_attrs.get('id') or None

        self.options = {"format": "DD-MM-YYYY", "useCurrent": False}
        if options:
            self.options.update(options and options.copy())
        if format and not self.options.get('format') and \
                not self.attrs.get('date-format'):
            self.options['format'] = self.conv_datetime_format_py2js(format)

    def render(self, name, value, attrs=None, renderer=None):
        if value is None:
            value = ''

        extra_attrs = {
            'type': self.input_type,
            'name': name
        }
        extra_attrs.update(self.attrs)
        input_attrs = self.build_attrs(attrs, extra_attrs)

        if value != '':
            input_attrs['value'] = force_text(self.format_value(value))
        input_attrs = dict([(key, conditional_escape(val)) for key, val in
                            input_attrs.items()])

        if not self.picker_id:
            self.picker_id = input_attrs.get('id', '').replace(' ', '_')
        self.div_attrs['id'] = (input_attrs.get('id', '') +
                                '_pickers').replace(' ', '_')
        picker_id = conditional_escape(self.picker_id)
        div_attrs = dict(
            [(key, conditional_escape(val)) for key, val in
             self.div_attrs.items()])
        icon_attrs = dict([(key, conditional_escape(val)) for key, val in
                           self.icon_attrs.items()])
        html = self.html_template % dict(div_attrs=flatatt(div_attrs),
                                         input_attrs=flatatt(input_attrs),
                                         icon_attrs=flatatt(icon_attrs),
                                         picker_id=picker_id,
                                         options=json.dumps(self.options or {}))

        return mark_safe(force_text(html))

    class Media:
        js = [
            "website/js/lib/jquery.min.js",
            "website/js/lib/bootstrap.min.js",
            "website/js/lib/moment.min.js",
            "website/js/lib/bootstrap-datetimepicker.min.js",
            "admin/js/datetime_picker_shortcuts.js"
        ]
        css = {'all': (
            'website/css/bootstrap-datetimepicker.css',),
        }
