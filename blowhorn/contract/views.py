# Django and System libraries
import logging
from django.conf import settings
from django.db.models import Q

# 3rd party libraries
from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import BasicAuthentication

# blowhorn imports
from blowhorn.contract.constants import (
    CONTRACT_STATUS_ACTIVE,
    CONTRACT_TYPE_HYPERLOCAL,
    CONTRACT_TYPE_SPOT,
    CONTRACT_TYPE_FIXED,
    CONTRACT_TYPE_SHIPMENT,
    CONTRACT_TYPE_C2C,
    CONTRACT_TYPE_SME
)
from blowhorn.contract.models import Contract
from blowhorn.common.middleware import CsrfExemptSessionAuthentication

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class ContractFilter(views.APIView):
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request):
        params = request.GET

        query_params = None

        if params.get('city_name', ''):
            query_params = Q(city__name=params.get('city_name'))

        if params.get('vehicle_class_preference', ''):
            param = Q(vehicle_classes=params.get(
                'vehicle_class_preference'))
            query_params = query_params & param if query_params else param

        if params.get('order_type', None) == settings.C2C_ORDER_TYPE:
            param = Q(
                contract_type__in=[CONTRACT_TYPE_SME,
                                   CONTRACT_TYPE_C2C]
            )
            query_params = query_params & param if query_params else param
        elif params.get('order_type', None) == settings.SHIPMENT_ORDER_TYPE:
            param = Q(
                contract_type__in = [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL],
                status=CONTRACT_STATUS_ACTIVE
            )
            query_params = query_params & param if query_params else param
        elif params.get('order_type', None):
            param = Q(
                contract_type__in=[CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SPOT],
                status=CONTRACT_STATUS_ACTIVE
            )
            query_params = query_params & param if query_params else param

        contract = Contract.objects.filter(
                query_params).values('id', 'name')

        if contract.exists():
            return Response(status=status.HTTP_200_OK,
                            data=contract)
        return Response(status=status.HTTP_404_NOT_FOUND)
