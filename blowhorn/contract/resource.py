from django.db import connection
from blowhorn.oscar.core.loading import get_model
from import_export import resources, fields

Contract = get_model('contract', 'Contract')
ResourceAllocation = get_model('contract', 'ResourceAllocation')

class ContractResource(resources.ModelResource):


    vehicle_classes = fields.Field(column_name="Vehicle Classes")

    body_types = fields.Field(column_name="Body Types")

    customer = fields.Field(column_name="Customers")

    city = fields.Field(column_name="City")

    invoice_state = fields.Field(column_name="Invoice State")

    service_state = fields.Field(column_name="Service State")

    division = fields.Field(column_name="Customer Division")

    payment_cycle = fields.Field(column_name="Payment Cycle")

    spocs = fields.Field(column_name="Spocs")

    expected_margin = fields.Field(column_name="Expected Margin")

    def dehydrate_vehicle_classes(self, contract):
        return ', '.join([str(x) for x in contract.vehicle_classes.all()])

    def dehydrate_body_types(self, contract):
        return ', '.join([str(x) for x in contract.body_types.all()])

    def dehydrate_customer(self, contract):
        if contract.customer:
            return str(contract.customer)
        return ''

    def dehydrate_city(self, contract):
        if contract.city:
            return str(contract.city)
        return ''

    def dehydrate_invoice_state(self, contract):
        if contract.invoice_state:
            return str(contract.invoice_state)
        return ''

    def dehydrate_service_state(self, contract):
        if contract.service_state:
            return str(contract.service_state)
        return ''

    def dehydrate_division(self, contract):
        if contract.division:
            return str(contract.division)
        return ''

    def dehydrate_payment_cycle(self, contract):
        if contract.payment_cycle:
            return str(contract.payment_cycle)
        return ''

    def dehydrate_spocs(self, contract):
        return ', '.join([str(x) for x in contract.spocs.all()])

    def dehydrate_expected_margin(self, contract):
        if contract.expected_margin:
            return str(contract.expected_margin)
        return ''

    def before_export(self, queryset, *args, **kwargs):
        self.initial_query_count = len(connection.queries)
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        import functools
        import logging
        print('Queries Count at end : %d' % len(connection.queries))
        self.after_query_count = len(connection.queries)
        logging.debug("Query TIME: %.1fms" % (functools.reduce(lambda x,y: x + float(y["time"]), connection.queries, 0.0)*1000))
        for q in connection.queries[self.initial_query_count:self.after_query_count]:
            logging.debug("%s: %s" % (q["time"], q["sql"]))

    class Meta:
        model = Contract
        fields = (
            'name', 'customer', 'expected_margin', 'status', 'division', 'city', 'service_tax_category', 'payment_cycle',
            'vehicle_classes', 'body_types', 'trip_noshow_hrs', 'spocs', 'created_date', 'created_by', 'modified_by')
        export_order = (
            'name', 'customer', 'expected_margin', 'status', 'division', 'city', 'service_tax_category', 'payment_cycle',
            'vehicle_classes', 'body_types', 'trip_noshow_hrs', 'spocs', 'created_date', 'created_by', 'modified_by')
