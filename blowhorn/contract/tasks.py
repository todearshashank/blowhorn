from celery.schedules import crontab
# from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from celery import shared_task
from django.conf import settings
from datetime import datetime, timedelta, date

from blowhorn.common.decorators import redis_batch_lock
from blowhorn.contract.utils import SubscriptionService, \
    reminder_call_for_drivers, avg_duty_hrs_for_contract
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.utils.mail import Email
from blowhorn.contract.models import Contract, ContractConfiguration
from blowhorn.users.models import User
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE, \
    CONTRACT_STATUS_INACTIVE, CONTRACT_TYPE_BLOWHORN, \
    CONTRACT_TYPE_SME, CONTRACT_TYPE_C2C, CONTRACT_TYPE_FIXED, \
    CONTRACT_TYPE_SPOT, DEFAULT_CONTRACT_DEACTIVATION_DURATION_IN_DAYS
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.constants import UNAPPROVED
from blowhorn.contract.utils import getStaticMargin
# from config.settings.base import celery_app
from blowhorn import celery_app


logger = get_task_logger(__name__)

@celery_app.task
# @periodic_task(run_every=(crontab(hour="1", minute="30")))
@redis_batch_lock(period=84600, expire_on_completion=False)
def generate_expected_margin():
    contract_ids = Contract.objects.all().values_list('id', flat=True)
    for i in contract_ids:
        margin = getStaticMargin(i)
        if isinstance(margin, str):
            continue
        Contract.objects.filter(id=i).update(expected_margin=margin)


@celery_app.task
# @periodic_task(run_every=(crontab(hour="5, 11, 17, 23", minute="30")))
@redis_batch_lock(period=19800, expire_on_completion=False)
def generate_subscription_orders():
    """
    Daily job at night 11:00 to auto generate subscription
    orders and blanket trips for all customer contracts
    """

    logger.info("generating orders")
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return
    SubscriptionService().create_services()
    SubscriptionService().create_unplanned_services()
    SubscriptionService().create_subscription_trip()


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def notify_contract_status_change(self, contract_id, status):
    """
    send email to spocs and supervisors for contract status change
    """
    Email().email_spoc_supervisor(contract_id, status)


@celery_app.task
# @periodic_task(run_every=(crontab(minute="29", hour="20")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def deactivate_contract():
    """
    Daily job at morning 2:00 A.M. to deactivate the contracts which
    has no trips since the configurable number of days
    """
    contract_id_to_be_updated = []
    deactivation_duration_qs = ContractConfiguration.objects.filter(
        name='Contract Deactivate')

    if not deactivation_duration_qs:
        deactivation_duration = DEFAULT_CONTRACT_DEACTIVATION_DURATION_IN_DAYS
    else:
        config = deactivation_duration_qs.first()
        deactivation_duration = config.value

    from_date = date.today() - timedelta(days=int(deactivation_duration))

    contracts = Contract.objects.filter(
        status=CONTRACT_STATUS_ACTIVE, created_date__lt=from_date)

    contracts = contracts.filter(
        contract_type__in=[CONTRACT_TYPE_SPOT,
                           CONTRACT_TYPE_FIXED])

    contracts = contracts.exclude(trip__planned_start_time__gt=from_date)

    contracts = contracts.prefetch_related(
        'trip_set', 'order_set', 'driverpayment_set')

    contracts_count = contracts.count()
    logger.info("Total number of trips : %s" % contracts_count)

    for contract in contracts:
        if contract.trip_set.all():
            # If any trip is in open status, do not deactivate the
            # contract
            if contract.trip_set.filter(
                status__in=[StatusPipeline.TRIP_NEW,
                            StatusPipeline.TRIP_IN_PROGRESS,
                            StatusPipeline.DRIVER_ACCEPTED,
                            StatusPipeline.TRIP_ALL_STOPS_DONE]):
                continue

            last_trip = contract.trip_set.latest('id')
            if last_trip.planned_start_time.date() > from_date:
                continue

        # TODO: send an alert if any order is in either New or
        # Out for Delivery option for the contract with no trip since
        # the configurable days
        if contract.order_set.filter(
                status__in=[StatusPipeline.ORDER_NEW,
                            StatusPipeline.OUT_FOR_DELIVERY]):
            continue

        # TODO: send an alert if any driver payment is still unapproved
        # for the contract with no trip since the configurable days
        if contract.driverpayment_set.filter(status=UNAPPROVED):
            continue

        contract_id_to_be_updated.append(contract.id)

    logger.info("These are the contracts to be updated : %s" %
                contract_id_to_be_updated)

    user_updating = User.objects.get(email=settings.CRON_JOB_DEFAULT_USER)

    Contract.objects.filter(id__in=contract_id_to_be_updated).update(
        modified_by=user_updating, status=CONTRACT_STATUS_INACTIVE,
        modified_date=datetime.now())


@celery_app.task
# @periodic_task(run_every=crontab(minute="*/60"))
@redis_batch_lock(period=2700, expire_on_completion=False)
def make_reminder_calls():
    """
    Run the job after every 60 mins and make call to all those drivers
    who are in resource allocation where driver reminder is selected
    """

    reminder_call_for_drivers()

@celery_app.task
# @periodic_task(run_every=crontab(minute=15, hour=23))
def calculate_duty_hrs():
    """
    Run jobs every day at 4:45 AM to calculate average duty hours
    for all active in contracts
    """
    avg_duty_hrs_for_contract()
