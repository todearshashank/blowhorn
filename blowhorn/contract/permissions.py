# from blowhorn.contract.models import Contract
from django.db.models import Q
from blowhorn.customer.constants import CREDIT_NOTE, DEBIT_NOTE
from blowhorn.contract.constants import CONTRACT_TYPE_BLOWHORN, \
    CONTRACT_TYPE_C2C
from blowhorn.customer.constants import SENT_FOR_APPROVAL


def is_spoc(contract, user):
    ''' returns True if user is a SPOC for the passed contract '''
    if contract:
        return type(contract).objects.filter(id=contract.id).filter(
            spocs=user).exists()
    else:
        return False


def is_supervisor(contract, user):
    ''' returns True if user is a SPOC for the passed contract '''
    if contract:
        return type(contract).objects.filter(id=contract.id).filter(
            supervisors=user).exists()
    return False


def is_default_invoice_type(invoice):
    return not invoice.is_blanket_invoice and not (
        invoice.invoice_type in [CREDIT_NOTE, DEBIT_NOTE] and
        invoice.related_invoice is None)


def is_spoc_or_supervisor(contract, user):
    ''' returns True if user is a SPOC or Supervisor
    for the passed contract '''
    if contract:
        return type(contract).objects.filter(id=contract.id).filter(
            Q(supervisors=user) | Q(spocs=user)).exists()
    else:
        return False


def is_superuser_or_supervisor(contract, user):
    ''' returns True is user is Superuser or SPOC '''
    if contract:
        return user.is_superuser or is_supervisor(contract, user)
    else:
        return False


def is_invoice_supervisor(invoice, user):
    ''' returns True is user is Supervisor of any related contract '''
    from blowhorn.contract.models import Contract
    if not user.is_superuser:
        if invoice:
            if Contract.objects.filter(
                    Q(contractinvoicefragment__invoice=invoice) |
                    Q(contractinvoicefragment__invoice=invoice.related_invoice,
                        contractinvoicefragment__invoice__isnull=False)).filter(
                    supervisors=user).exists():
                return True
            elif invoice.customer:
                return invoice.customer.contract_set.filter(
                    city__state__in=[invoice.bh_source_state,
                                     invoice.invoice_state],
                    supervisors=user).exists()
            else:
                return False
        else:
            return False
    return True


def is_invoice_spoc(invoice, user):
    ''' returns True is user is Spoc of any related contract '''
    from blowhorn.contract.models import Contract
    if not user.is_superuser:
        if invoice:
            if Contract.objects.filter(
                    Q(contractinvoicefragment__invoice=invoice) |
                    Q(contractinvoicefragment__invoice=invoice.related_invoice,
                        contractinvoicefragment__invoice__isnull=False)).filter(
                    spocs=user).exists():
                return True
            elif invoice.customer:
                return invoice.customer.contract_set.filter(
                    city__state__in=[invoice.bh_source_state,
                                     invoice.invoice_state],
                    spocs=user).exists()
            else:
                return False
        else:
            return False
    return True


def has_approval_permission(invoice, user):
    '''returns True if user is a supervisor for non credit notes
        For credit notes, only executive users have permissions to approve
    '''
    from blowhorn.customer.models import InvoiceHistory

    sent_for_approval = InvoiceHistory.objects.filter(invoice=invoice, new_status=SENT_FOR_APPROVAL).last()
    if sent_for_approval and sent_for_approval.created_by == user:
        return False
    if invoice.is_credit_debit_note:
        from blowhorn.customer.invoice_transition_conditions import is_executive
        return is_executive(user)
    else:
        return is_invoice_supervisor(invoice, user)


def is_citymanager(contract, user):
    ''' returns True if user is city manger for contract city'''
    from blowhorn.address.models import City
    if contract:
        cities = City.objects.filter(managers=user)
        return type(contract).objects.filter(id=contract.id).filter(
            city__in=cities).exists()
    else:
        return False


def is_city_manager(instance, user):
    from blowhorn.address.models import State

    return State.objects.filter(cities__managers=user, name=instance.bh_source_state).exists()


def is_city_manager_or_supervisor(instance, user):
    if instance:
        return is_city_manager(instance, user) or is_invoice_supervisor(instance, user)
    else:
        return False


def is_finance_user(instance, user):
    from blowhorn.company.models import Company

    return Company.objects.filter(finance_user=user).exists()


def is_sales_representative(contract, user):
    """
    :param contract:
    :param user:
    :return: True if user is a sales_representative
    """
    from blowhorn.address.models import City
    if contract.contract_type in [CONTRACT_TYPE_BLOWHORN, CONTRACT_TYPE_C2C]:
        return True
    return City.objects.filter(sales_representatives=user).exists()
