# Django and System libraries
import logging

# 3rd party libraries
from rest_framework import views
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status, filters, generics

# blowhorn imports
from blowhorn.address.models import City
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.contract.mixins import ResourceAllocationMixin
from blowhorn.contract.serializers import ContractResourceSerializer, \
    ResourceAllocationHistorySerializer
from blowhorn.contract.models import Resource, ResourceAllocation, \
    ResourceAllocationHistory
from blowhorn.users.mixins import AuthMixin
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.contract.admin import ContractSale, ContractOperation

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ResourceAllocationSupportData(views.APIView, ResourceAllocationMixin,
                                    AuthMixin):
    """
    GET api/resourceallocation/data
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def _get_contract_module_link(self, user):
        if user.is_superuser:
            return 'admin/contract/contract'

        check_perms = ['change', 'view']
        if self.has_model_permissions(user, ContractSale, check_perms,
                                      'contract'):
            return 'admin/contract/contractsale'

        if self.has_model_permissions(user, ContractOperation, check_perms,
                                      'contract'):
            return 'admin/contract/contractoperation'

    def get(self, request):
        cities = City.objects.get_cities_list()
        data = {
            'cities': cities,
            'contract_url': self._get_contract_module_link(request.user)
        }
        return Response(status=status.HTTP_200_OK, data=data)


class ContractResourceList(generics.ListAPIView, ResourceAllocationMixin):
    """
        GET api/resourceallocation/contract/
        Fetches paginated contracts
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)
    serializer_class = ContractResourceSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('name', 'id')

    def get_queryset(self):
        return self.serializer_class().queryset(self.request.user,
                                                self.request.query_params)


class ResourceAllocationView(views.APIView, ResourceAllocationMixin):
    """
    GET/POST api/resourceallocation/resources/<contract_pk>
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def get(self, request, contract_pk=None):
        resource_allocations = self.get_resource_allocations(contract_pk)
        drivers = self.get_drivers(contract_pk)
        hubs = self.get_customer_hubs(contract_pk)
        data = {
            'resource_allocations': resource_allocations,
            'drivers': drivers,
            'hubs': hubs
        }
        return Response(status=status.HTTP_200_OK, data=data)

    def post(self, request, contract_pk=None):
        data = request.data
        resource_pk = data.get('id', None)
        is_valid, cleaned_data = self.get_validated_data(
            contract_pk, data, resource_pk=resource_pk)
        if not is_valid:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=cleaned_data)

        drivers = cleaned_data.pop('drivers', [])
        drivers = [d.get('id') for d in drivers]
        ra_instance = None
        created = False
        if not resource_pk:
            ra_instance = self.create_resource_allocation(cleaned_data)
            created = True

        else:
            ra_instance = self.update_resource_allocation(cleaned_data)

        success, message = \
            self.change_drivers_in_resources(ra_instance, drivers, created)

        if not success:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

        action = 'created' if created else 'updated'
        response_data = {
            'data': self.to_repr_resourceallocation(ra_instance),
            'message': 'Resource %s successfully' % action,
            'created': created
        }
        return Response(status=status.HTTP_200_OK, data=response_data)


class ResourceAllocationDeleteView(views.APIView, ResourceAllocationMixin):
    """
        POST resourceallocation/resources/remove/<resource_pk>
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def post(self, request, resource_pk=None):
        if not resource_pk:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Select a resource allocation to delete')
        try:
            deleted = Resource.objects.filter(
                resourceallocation_id=resource_pk).delete()
        except BaseException:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Failed to delete resources')

        try:
            deleted_resource = ResourceAllocation.objects.filter(
                pk=resource_pk).delete()
        except BaseException:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Failed to delete Resource Allocation')

        return Response(status=status.HTTP_200_OK,
                        data='Resource Allocation deleted successfully')


class ResourceAllocationHistoryView(views.APIView, ResourceAllocationMixin):
    """
        POST resourceallocation/contract/resources/history/<contract_pk>
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)
    serializer_class = ResourceAllocationHistorySerializer

    def get(self, request, contract_pk=None):
        queryset = self.get_queryset(contract_pk)
        serializer = self.serializer_class(queryset, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    def get_queryset(self, contract_pk):
        return ResourceAllocationHistory.objects\
            .filter(contract_id=contract_pk)\
            .select_related('created_by', 'driver')\
            .order_by('-event_time')
