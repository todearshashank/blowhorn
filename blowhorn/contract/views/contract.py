# Django and System libraries
import logging
import operator
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from functools import reduce

# 3rd party libraries
from rest_framework import views
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny

# blowhorn imports
# noinspection PyUnresolvedReferences
from blowhorn.contract.constants import (
    CONTRACT_STATUS_ACTIVE,
    CONTRACT_TYPE_HYPERLOCAL,
    CONTRACT_TYPE_SPOT,
    CONTRACT_TYPE_FIXED,
    CONTRACT_TYPE_SHIPMENT,
    CONTRACT_TYPE_C2C,
    CONTRACT_TYPE_SME,
    MAX_DAYS_ALLOWED,
    MIN_DAYS_ALLOWED, DEAFCOM_MAX_DAYS_ALLOWED, DEAFCOM_MIN_DAYS_ALLOWED
)
from blowhorn.contract.models import Contract, ContractTerm
from blowhorn.address.models import Hub
from blowhorn.customer.models import CustomerDivision
from blowhorn.customer.views import get_customer_from_api_key
from blowhorn.order.models import OrderConstants
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from staticSlabsDataset import VehicleSlabs



logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ContractFilter(views.APIView):
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request):
        params = request.GET

        query_params = None

        if params.get('city_name', ''):
            query_params = Q(city__name=params.get('city_name'))

        if params.get('vehicle_class_preference', ''):
            param = Q(vehicle_classes=params.get(
                'vehicle_class_preference'))
            query_params = query_params & param if query_params else param

        if params.get('order_type', None) == settings.C2C_ORDER_TYPE:
            param = Q(
                contract_type__in=[CONTRACT_TYPE_SME,
                                   CONTRACT_TYPE_C2C]
            )
            query_params = query_params & param if query_params else param
        elif params.get('order_type', None) == settings.SHIPMENT_ORDER_TYPE:
            param = Q(
                contract_type__in = [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL],
                status=CONTRACT_STATUS_ACTIVE
            )

            query_params = query_params & param if query_params else param
        elif params.get('order_type', None):
            param = Q(
                contract_type__in=[CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SPOT],
                status=CONTRACT_STATUS_ACTIVE
            )
            query_params = query_params & param if query_params else param

        contract = Contract.objects.filter(query_params).values('id', 'name')

        if contract.exists():
            return Response(status=status.HTTP_200_OK,
                            data=contract)
        return Response(status=status.HTTP_404_NOT_FOUND)


class ListContract(generics.ListAPIView):

    permission_classes = (IsAuthenticated,)
    # permission_classes=[AllowAny]

    def get(self, request):
        response_data = {}
        status_code = status.HTTP_200_OK
        max_days_allowed = OrderConstants.objects.filter(name=MAX_DAYS_ALLOWED).values_list('value', flat=True)
        min_days_allowed = OrderConstants.objects.filter(name=MIN_DAYS_ALLOWED).values_list('value', flat=True)

        response_data['max_days_allowed'] = int(list(max_days_allowed)[
            0]) if max_days_allowed.exists() else settings.ALLOWED_FUTURE_DAYS

        response_data['min_days_allowed'] = int(list(min_days_allowed)[
            0]) if min_days_allowed.exists() else settings.MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME

        deafcom_max_days_allowed = OrderConstants.objects.filter(name=DEAFCOM_MAX_DAYS_ALLOWED).values_list('value', flat=True)
        deafcom_min_days_allowed = OrderConstants.objects.filter(name=DEAFCOM_MIN_DAYS_ALLOWED).values_list('value', flat=True)

        response_data['deafcom_max_days_allowed'] = int(list(deafcom_max_days_allowed)[
            0]) if deafcom_max_days_allowed.exists() else settings.DEAFCOM_ALLOWED_FUTURE_DAYS

        response_data['deafcom_min_days_allowed'] = int(list(deafcom_min_days_allowed)[
            0]) if deafcom_min_days_allowed.exists() else settings.DEAFCOM_MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME

        response_data['current_date'] = timezone.now().isoformat()

        contracts_list = []
        contracts = self.get_queryset()
        for contract in contracts:
            data = {}
            data['id'] = contract.id
            data['name'] = str(contract)
            data['contract_description'] = str(contract.description)
            data['vehicle_classes'] = contract.vehicle_classes.all().values_list('commercial_classification', flat=True)
            data['body_types'] = contract.body_types.all().values_list('body_type', flat=True)
            data['city'] = contract.city.name
            data['hubs'] = Hub.objects.filter(
                customer_id=contract.customer_id,
                city_id=contract.city_id
            ).values('id', 'name')
            contracts_list.append(data)
        response_data['contracts'] = contracts_list
        return Response(status=status_code, data=response_data)

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        return Contract.objects.filter(query).filter(
            Q(spocs=self.request.user) | Q(supervisors=self.request.user)).distinct()

    def get_query(self, params):
        customer_id = params.get('customer_id', None)
        _status = params.get('status', CONTRACT_STATUS_ACTIVE)
        contract_type = params.get('contract_type', None)

        query = []
        if _status:
            statuses = _status.split(",")
            status_query = []
            if statuses:
                status_query.append(Q(status__in=statuses))
            query.append(reduce(operator.or_, status_query))
        if customer_id:
            query.append(Q(customer_id=customer_id))
        if contract_type:
            query.append(Q(contract_type=contract_type))
        return reduce(operator.and_, query)


class CalculateMargin(views.APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        data = request.data.dict()
        num_trips = int(data.get('num_trips') or 0.0)
        dist1 = float(data.get('dist1') or 0.0)
        time1 = float(data.get('time1') or 0.0)
        dist2 = float(data.get('dist2') or 0.0)
        time2 = float(data.get('time2') or 0.0)
        dist3 = float(data.get('dist3') or 0.0)
        time3 = float(data.get('time3') or 0.0)
        leave_num = float(data.get('leave_num') or 0.0)
        cterm = ContractTerm.objects.filter(contract_id=data.get('contract_id')).latest('wef')
        contract_id = data.get('contract_id')
        buy_rate = cterm.buy_rate
        sell_rate = cterm.sell_rate
        value = VehicleSlabs().margincalcuser(num_trips, dist1, time1,
            dist2, time2, dist3, time3, leave_num, contract_id)
        if value!='':
            return Response(status=status.HTTP_200_OK,data=value)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class CustomerDivisionView(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = get_customer_from_api_key(api_key)

        if not customer:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        divisions = CustomerDivision.objects.filter(customer=customer).values_list('name', flat=True)
        response_data= {
            'divisions': divisions
        }
        return Response(status=status.HTTP_200_OK, data=response_data)

class CalculateDatasetMargin(views.APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data

        leave_num = float(data.get('leave_num') or 0.0)
        contract_id = data.get('contract_id')
        value = VehicleSlabs().DisplayAll(contract_id, leave_num)

        if isinstance(value, str):
            return Response(status=status.HTTP_404_NOT_FOUND, data=value)

        return Response(status=status.HTTP_200_OK, data=value)


class ContractSearchView(views.APIView):
    """
    List all active contracts for the user
    """
    def get(self, request):
        user = request.user
        search_term = request.GET.get('term', None)
        if not user.is_staff:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Unauthorized')

        contracts = list(Contract.objects.filter(
            Q(spocs=user) | Q(supervisors=user) | Q(name__icontains=search_term),
            status=CONTRACT_STATUS_ACTIVE
        ).distinct().values('id', 'name', 'description'))
        return Response(status=status.HTTP_200_OK, data=contracts)
