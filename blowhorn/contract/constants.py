from django.utils.translation import ugettext_lazy as _
from config.settings import status_pipelines as StatusPipeline

CONTRACT_PERMISSIONS = ['change', 'add', 'delete']

# Display Keys
KEY_OPENING_KMS = 'Opening kms'
KEY_CLOSING_KMS = 'Closing kms'
KEY_PACKAGES_RECEIVED = 'Assigned-Total Packages'
KEY_CASH_PACKAGES = 'Delivered Packages-Cash'
KEY_MPOS_PACKAGES = 'Delivered Packages-MPOS'
KEY_ATTEMPTED = 'Return-Attempts'
KEY_REJECTED = 'Return-Rejects'
KEY_RETURNED = 'Return Orders'
KEY_END_TRIP_OTP = 'End Trip OTP'


# Storage Keys
SKEY_OPENING_KMS = 'meter_reading_start'
SKEY_CLOSING_KMS = 'meter_reading_end'
SKEY_PACKAGES_RECEIVED = 'packages_received'
SKEY_CASH_PACKAGES = 'delivered_cash_packages'
SKEY_MPOS_PACKAGES = 'delivered_mpos_packages'
SKEY_ATTEMPTED = 'return_attempted'
SKEY_REJECTED = 'return_rejected'
SKEY_RETURNED = 'return_orders'
SKEY_END_TRIP_OTP = 'otp'

KEYS_LIST = [SKEY_OPENING_KMS, SKEY_CLOSING_KMS, SKEY_PACKAGES_RECEIVED, SKEY_CASH_PACKAGES,
             SKEY_MPOS_PACKAGES, SKEY_ATTEMPTED, SKEY_REJECTED, SKEY_RETURNED, SKEY_END_TRIP_OTP]

dkey_to_skey_mapping = {
    KEY_OPENING_KMS: SKEY_OPENING_KMS,
    KEY_CLOSING_KMS: SKEY_CLOSING_KMS,
    KEY_PACKAGES_RECEIVED: SKEY_PACKAGES_RECEIVED,
    KEY_CASH_PACKAGES: SKEY_CASH_PACKAGES,
    KEY_MPOS_PACKAGES: SKEY_MPOS_PACKAGES,
    KEY_ATTEMPTED: SKEY_ATTEMPTED,
    KEY_REJECTED: SKEY_REJECTED,
    KEY_RETURNED: SKEY_RETURNED,
    KEY_END_TRIP_OTP: SKEY_END_TRIP_OTP
}

RETURN_PKG_ERR_MSG = 'Return Orders cannot be higher than Total Orders'
CASH_ORDER_ERR_MSG = 'Cash Orders cannot be higher than Total Orders'


def get_skey_for_dkey(dkey):
    return dkey_to_skey_mapping.get(dkey, dkey)


TIME_FORMAT = '%H:%M'
ACTION_SUCCESS = 'SUCCESS'  # Show success animation for 2 seconds
ACTION_DISPLAY = 'DISPLAY'  # Display given data
ACTION_FORM = 'FORM'  # Display given data and show form to enter data
CONTRACT_TYPE_SPOT = u'Spot'
CONTRACT_TYPE_FIXED = u'Fixed'
CONTRACT_TYPE_BLOWHORN = u'Blowhorn'
CONTRACT_TYPE_SHIPMENT = u'Shipment'
CONTRACT_TYPE_C2C = u'C2C'
CONTRACT_TYPE_SME = u'SME'
CONTRACT_TYPE_KIOSK = u'Kiosk'
CONTRACT_TYPE_FLASH_SALE = u'FlashSale'
CONTRACT_TYPE_ONDEMAND = u'Ondemand'
CONTRACT_TYPE_DEAFCOM = u'Defcom'
CONTRACT_TYPE_HYPERLOCAL = u'Hyperlocal'

POD_CONST = 'POD'

BASE_PAYMENT_CYCLE_PER_TRIP = u'Per-Trip'
BASE_PAYMENT_CYCLE_DAILY = u'Daily'
BASE_PAYMENT_CYCLE_WEEK = u'Weekly'
BASE_PAYMENT_CYCLE_FORTNIGHTLY = u'FortNightly'
BASE_PAYMENT_CYCLE_MONTH = u'Month'

""" Contract statuses """
CONTRACT_STATUS_DRAFT = 'Draft'
CONTRACT_STATUS_ACTIVE = 'Active'
CONTRACT_STATUS_INACTIVE = 'Inactive'

CONTRACT_STATUSES = (
    (CONTRACT_STATUS_DRAFT, _('Draft')),
    (CONTRACT_STATUS_ACTIVE, _('Active')),
    (CONTRACT_STATUS_INACTIVE, _('Inactive')),
)

CONTRACT_TYPES = (
    (CONTRACT_TYPE_SPOT, _('Spot')),
    (CONTRACT_TYPE_FIXED, _('Fixed')),
    (CONTRACT_TYPE_SHIPMENT, _('Shipment')),
    (CONTRACT_TYPE_KIOSK, _('Kiosk')),
    (CONTRACT_TYPE_FLASH_SALE, _('FlashSale')),
    (CONTRACT_TYPE_ONDEMAND, _('On-Demand')),
    (CONTRACT_TYPE_DEAFCOM, _('Defcom')),
    (CONTRACT_TYPE_HYPERLOCAL, _('Hyperlocal')),
)

############################################################

""" Flow Attributes Type """
INTEGER = 2
TEXT = 1
BOOLEAN = 3
PHONE_NUMBER = 4

# Order Configuration constants
MAX_DAYS_ALLOWED = 'MAX_DAYS_ALLOWED'
MIN_DAYS_ALLOWED = 'MIN_DAYS_ALLOWED'

DEAFCOM_MAX_DAYS_ALLOWED = 'DEAFCOM_MAX_DAYS_ALLOWED'
DEAFCOM_MIN_DAYS_ALLOWED = 'DEAFCOM_MIN_DAYS_ALLOWED'

SCREEN_FIELDS_DATA_TYPES = (
    (INTEGER, _('Integer')),
    (TEXT, _('Text')),
    (BOOLEAN, _('Boolean')),
    (PHONE_NUMBER, _('Phone Number')),
)

START_METER_READING = 'meter_reading_start'
END_METER_READING = 'meter_reading_end'
ASSIGNED = 'assigned'
DELIVERED = 'delivered'
ATTEMPTED = 'attempted'
REJECTED = 'rejected'
RETURNED = 'returned'
PICKUPS = 'pickups'
DELIVERED_MPOS = 'delivered_mpos'
DELIVERED_CASH = 'delivered_cash'
PARKING_CHARGE = 'parking_charges'
TOLL_CHARGE = 'toll_charges'
DELIVER_BUTTON = StatusPipeline.ORDER_DELIVERED
REJECT_BUTTON = StatusPipeline.ORDER_RETURNED
UNABLE_TO_DELIVER_BUTTON = StatusPipeline.UNABLE_TO_DELIVER
PICKUP_BUTTON = StatusPipeline.ORDER_PICKED
PICKUP_FAILED_BUTTON = StatusPipeline.ORDER_PICKUP_FAILED
RTO_BUTTON = StatusPipeline.ORDER_RETURNED_RTO
RTO_FAILED_BUTTON = StatusPipeline.ORDER_RETURNED_RTO_FAILED
END_TRIP_OTP = 'otp'

SCREEN_FIELD_PRIORITY = {
    START_METER_READING: 1,
    END_METER_READING: 2,
    ASSIGNED: 3,
    DELIVERED: 4,
    DELIVERED_MPOS: 5,
    DELIVERED_CASH: 6,
    ATTEMPTED: 7,
    REJECTED: 8,
    RETURNED: 9,
    PICKUPS: 10,
    TOLL_CHARGE: 11,
    PARKING_CHARGE: 12,
    END_TRIP_OTP: 13
}

SCREEN_FIELD_NAME = (
    (START_METER_READING, _('Opening Km')),
    (END_METER_READING, _('Closing Km')),
    (ASSIGNED, _('Total-Assigned Packages')),
    (DELIVERED, _('Delivered Packages')),
    (ATTEMPTED, _('Returns-Attempted Packages')),
    (REJECTED, _('Returns-Rejected Packages')),
    (RETURNED, _('Returned Packages')),
    (PICKUPS, _('Pickups Packages')),
    (DELIVERED_MPOS, _('Delivered-Mpos Packages')),
    (DELIVERED_CASH, _('Delivered-Cash Packages')),
    (TOLL_CHARGE, _('Toll Charges')),
    (PARKING_CHARGE, _('Parking Charges')),
    (END_TRIP_OTP, _('End Trip OTP'))
)

SCREEN_BUTTON_NAME = (
    (DELIVER_BUTTON, _('Deliver')),
    (REJECT_BUTTON, _('Reject')),
    (UNABLE_TO_DELIVER_BUTTON, _('Unable To Deliver')),
    (PICKUP_BUTTON, _('Pickup')),
    (PICKUP_FAILED_BUTTON, _('Pickup-Failed')),
    (RTO_BUTTON, _('Returned-To-Origin')),
    (RTO_FAILED_BUTTON, _('Returned-To-Origin-Failed'))
)

POD = 'pod_image'
CONFIRMATION_POPUP = 'confirmation_popup'
SIGN = 'sign_image'

SCREEN_BUTTON_PARAMETER_NAME = (
    (POD, _('Proof Of Delivery')),
    (CONFIRMATION_POPUP, _('Confirmation Popup')),
    (SIGN, _('Customer Signature')),
)

SCAN_QR_CODE = 'scan_qr_code'
DRIVER_ARRIVAL_NOTIFICATION_TO_CUSTOMER = 'arrival_notification'
START_ODOMETER_PROOF = 'start_meter_reading_proof'
END_ODOMETER_PROOF = 'end_meter_reading_proof'
TRIP_DATA_PROOF = 'trip_proof'
VERIFY_ITEMS = 'verify_items'
SCAN_ORDER = 'scan_order'
CASHFREE_PAYMENT = 'cashfree_payment'
CARD_PAYMENT = 'card_payment'

SCREEN_PARAMETERS = (
    (TOLL_CHARGE, _('Capture Toll Charges Proof')),
    (PARKING_CHARGE, _('Capture Parking Charges Proof')),
    (START_ODOMETER_PROOF, _('Start Odometer Proof')),
    (END_ODOMETER_PROOF, _('End Odometer Proof')),
    (SCAN_QR_CODE, _('Scan QR Code')),
    (DRIVER_ARRIVAL_NOTIFICATION_TO_CUSTOMER, _('Send Driver Arrival Notification to Customer')),
    (TRIP_DATA_PROOF, _('Capture Driver Data Logs')),
    (VERIFY_ITEMS, _('Verify Items')),
    (SCAN_ORDER, _('Scan Order')),
    (CASHFREE_PAYMENT, _('Cashfree Payment')),
    (CARD_PAYMENT, _('Card Payment')),
)

START_TRIP = 'start_trip'
IN_TRANSIT = 'in_transit'
VEHICLE_LOAD = 'vehicle_load'
FINISHED_LOADING = 'finished_loading'
REACH_LOCATION = 'reached_stop'
FINISH_LOCATION = 'finished_stop'
END_TRIP = 'end_trip'

SCREEN_NAME = (
    (START_TRIP, _('Start Trip')),
    (IN_TRANSIT, _('Hand Over For Transit')),
    (VEHICLE_LOAD, _('Load Vehicle')),
    (FINISHED_LOADING, _('Finished Loading')),
    (REACH_LOCATION, _('Reached Stop')),
    (FINISH_LOCATION, _('Finished Work')),
    (END_TRIP, _('End Trip')),
)

#############################################################

AGGREGATE_CYCLE_DAILY = u'Per-Day'
AGGREGATE_CYCLE_CYCLE = u'Per-Cycle'
AGGREGATE_CYCLE_MONTH = u'Per-Month'
AGGREGATE_CYCLE_TRIP = u'Trip'
AGGREGATE_CYCLE_FIRST_TRIP = u'First Trip of the day'
AGGREGATE_CYCLE_SECOND_TRIP = u'Second Trip of the day'
AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP = u'Third Trip onwards'

BUY_RATE_AGGREGATE_CYCLE_OPTIONS = (
    (AGGREGATE_CYCLE_DAILY, _('Per-Day')),
    (AGGREGATE_CYCLE_CYCLE, _('Per-Cycle')),
    (AGGREGATE_CYCLE_MONTH, _('Per-Month')),
    (AGGREGATE_CYCLE_TRIP, _('Per-Trip')),
    (AGGREGATE_CYCLE_FIRST_TRIP, _('First Trip of the day')),
    (AGGREGATE_CYCLE_SECOND_TRIP, _('Second Trip of the day')),
    (AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP, _('Third Trip onwards'))
)

SELL_RATE_AGGREGATE_CYCLE_OPTIONS = (
    (AGGREGATE_CYCLE_DAILY, _('Per-Day')),
    (AGGREGATE_CYCLE_MONTH, _('Per-Month')),
    (AGGREGATE_CYCLE_TRIP, _('Per-Trip')),
)

PAY_CYCLE_CYCLE = u'End-of-Cycle'
PAY_CYCLE_MONTH = u'End-of-Month'

PAY_CYCLE_OPTIONS = (
    (PAY_CYCLE_CYCLE, _('End-of-Cycle')),
    (PAY_CYCLE_MONTH, _('End-of-Month')),
)

VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES = u'Kilometres'
VEHICLE_SERVICE_ATTRIBUTE_HOURS = u'Hours'
VEHICLE_SERVICE_ATTRIBUTE_MINUTES = u'Minutes'
VEHICLE_SERVICE_ATTRIBUTES = (
    (VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES, _('Kilometres')),
    (VEHICLE_SERVICE_ATTRIBUTE_HOURS, _('Hours')),
    (VEHICLE_SERVICE_ATTRIBUTE_MINUTES, _('Minutes')),
)

C2C_VEHICLE_SERVICE_ATTRIBUTES = (
    (VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES, _('Kilometres')),
    (VEHICLE_SERVICE_ATTRIBUTE_MINUTES, _('Minutes')),
)

SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED = u'Assigned'
SHIPMENT_SERVICE_ATTRIBUTE_OUT_FOR_DELIVERY = u'%s' % StatusPipeline.OUT_FOR_DELIVERY
SHIPMENT_SERVICE_ATTRIBUTE_RTO = u'%s' % StatusPipeline.ORDER_RETURNED_RTO
SHIPMENT_SERVICE_ATTRIBUTE_PICKED = u'Picked'
SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED = u'Delivered'
SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED = u'Attempted'
SHIPMENT_SERVICE_ATTRIBUTE_REJECTED = u'Rejected'
SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS = u'Pickups'
SHIPMENT_SERVICE_ATTRIBUTE_RETURNED = u'Returned'
SHIPMENT_SERVICE_ATTRIBUTE_MPOS = u'MPOS Packages'
SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES = u'Cash Packages'
SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED = u'Assigned - Attempted + Returned'
SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED = u'Assigned - Attempted + Rejected'
SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED = u'Delivered + Rejected'
SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED = u'Assigned - Attempted'
SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED = u'Assigned - Attempted + Returned + Rejected'
SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED = u'Assigned -  Returned'

SHIPMENT_SERVICE_ATTRIBUTES = (
    (SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED, _('Shipments assigned')),
    (SHIPMENT_SERVICE_ATTRIBUTE_OUT_FOR_DELIVERY, _('Out-For-Delivery')),
    (SHIPMENT_SERVICE_ATTRIBUTE_RTO, _('Returned-To-Origin')),
    (SHIPMENT_SERVICE_ATTRIBUTE_PICKED, _('Shipments Picked')),
    (SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED, _('Shipments delivered')),
    (SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED, _('Shipments attempted')),
    (SHIPMENT_SERVICE_ATTRIBUTE_REJECTED, _('Shipments rejected')),
    (SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS, _('Shipment MFN seller pickup')),
    (SHIPMENT_SERVICE_ATTRIBUTE_RETURNED, _('Shipment c-returned')),
    (SHIPMENT_SERVICE_ATTRIBUTE_MPOS, _('Shipment MPOS packages')),
    (SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES, _('Shipment CASH packages')),
    (SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
     _(u'Assigned - Attempted + Returned')),
    (SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
     _(u'Assigned - Attempted + Rejected')),
    (SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED, _(u'Delivered + Rejected')),
    (SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED, _(u'Assigned - Attempted')),
    (SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
     _(u'Assigned - Attempted + Returned + Rejected')),
    (SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED, _(u'Assigned -  Returned')),

)

DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS = u'Cubic Centimetres'
DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS = u'Kilograms'
DIMENSION_SERVICE_ATTRIBUTE_GRAMS = u'grams'
DIMENSION_SERVICE_ATTRIBUTES = (
    (DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS, _('Cubic Centimetres')),
    (DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS, _('Kilograms')),
    (DIMENSION_SERVICE_ATTRIBUTE_GRAMS, _('Grams')),
)

VAS_SERVICE_ATTRIBUTE_FLOOR_NUMBER = u'Floor Number'
VAS_SERVICE_ATTRIBUTE_PICKUP_FLOOR_NUMBER = u'Pickup Floor Number'
VAS_SERVICE_ATTRIBUTE_DROPPOFF_FLOOR_NUMBER = u'Dropoff Floor Number'
VAS_SERVICE_ATTRIBUTE_NUMBER_OF_ITEMS = u'Number of Items'
VAS_SERVICE_ATTRIBUTE_POD = u'Proof of Delivery'
VAS_SERVICE_ATTRIBUTE_LABOUR = u'Labour'
VAS_SERVICE_ATTRIBUTE_TRACKING = u'Tracking'
VAS_SERVICE_ATTRIBUTE_DELIVERY = u'Delivery'
VAS_SERVICE_ATTRIBUTE_NIGHT_HALT = u'Night Halt'
VAS_SERVICE_ATTRIBUTE_OMS = u'Order Management'
VAS_SERVICE_ATTRIBUTE_CASH_HANDLING = u'Cash Handling'
VAS_SERVICE_ATTRIBUTE_SMS = u'SMS Charges'
VAS_SERVICE_ATTRIBUTE_CALL = u'Call Charges'
VAS_SERVICE_ATTRIBUTE_DEFCOM_SELL_RATE= u'Defcom Sell Rate'

VAS_SERVICE_ATTRIBUTES = (
    (VAS_SERVICE_ATTRIBUTE_FLOOR_NUMBER, _('Floor Number')),
    (VAS_SERVICE_ATTRIBUTE_NUMBER_OF_ITEMS, _('Number of Items')),
    (VAS_SERVICE_ATTRIBUTE_POD, _('Proof of Delivery')),
    (VAS_SERVICE_ATTRIBUTE_LABOUR, _('Labour')),
    (VAS_SERVICE_ATTRIBUTE_TRACKING, _('Tracking')),
    (VAS_SERVICE_ATTRIBUTE_DELIVERY, _('Delivery')),
    (VAS_SERVICE_ATTRIBUTE_NIGHT_HALT, _('Night Halt')),
    (VAS_SERVICE_ATTRIBUTE_OMS, _('Order Management')),
    (VAS_SERVICE_ATTRIBUTE_CASH_HANDLING, _('Cash Handling')),
    (VAS_SERVICE_ATTRIBUTE_DEFCOM_SELL_RATE, _('Defcom Sell Rate')),
)

NT_SERVICE_ATTRIBUTE_INSURANCE = u'Insurance'
NT_SERVICE_ATTRIBUTE_POD = u'POD'
NT_SERVICE_ATTRIBUTES = (
    (NT_SERVICE_ATTRIBUTE_INSURANCE, _('Insurance')),
    (NT_SERVICE_ATTRIBUTE_POD, _('Proof Of Delivery')),
)

MESSAGE_FORMATS = {
    "SUBJECT": {
        "PERMISSION_GRANT": "You have been granted the role of {role} for contract {contract_name}.",
        "PERMISSION_REVOKE": "Your role of {role} has been revoked for contract {contract_name}"
    },
    "CONTENT": {
        "PERMISSION_GRANT":
            """
            Role : {role}
            Contract Name : {contract_name}
            Contract ID : {contract_id}
            Permission : GRANT
            """,
        "PERMISSION_REVOKE":
            """
            Role : {role}
            Contract Name : {contract_name}
            Contract ID : {contract_id}
            Permission : REVOKE
            """,
    }
}


# Sell Rate aggregate attributes.
CITY = u'City'
HUB = u'Hub'
DRIVER = u'Driver'
VEHICLE_CLASS = u'Vehicle Class'

# Service Tax Categories
GTA = u'GTA'
BSS = u'BSS'
EXEMPT = u'TAX EXEMPT'
VAT = u'VAT'

SERVICE_TAX_CATEGORIES_CODE_MAP = {
    'GTA': {
        'code': 996511,
        'name': 'Goods Transportation Agency'
    },
    'BSS': {
        'code': 998599,
        'name': 'Business Support Services'
    },
    'TAX EXEMPT': {
        'code': 996511,
        'name': 'TAX EXEMPT'
    },
    'Toll Charges': {
        'code': '',
        'name': 'TOLL CHARGES'
    },
    'Parking Charges': {
        'code': '',
        'name': 'PARKING CHARGES'
    },
    'VAT': {
        'code': '',
        'name': 'Service Charges'
    }

}

SERVICE_TAX_CATEGORIES = (
    (GTA, _('Goods Transportation Agency - GTA')),
    (BSS, _('Business Support Services - BSS')),
    (VAT, _('Value Added Tax - VAT')),
    (EXEMPT, _('Tax Exempt')),
)

DESCRIPTION_FOR_INVOICE = {
    'GTA': 'Transportation Service',
    'BSS': 'Transportation & Other Service',
    'EXTRA_KMS': 'Charges for extra kms',
    'EXTRA_HRS': 'Charges for extra hrs',
    'OTHERS': 'Additional Services'
}

EXTRA_ATTRIBUTE_CATEGORIES = {
    'KILOMETRES': {
        'TYPE': 'Extra kms',
        'CODE': 'EXTRA_KMS'
    },
    'HOURS': {
        'TYPE': 'Extra hrs',
        'CODE': 'EXTRA_HRS'
    }
}

DEFAULT_CONTRACT_DEACTIVATION_DURATION_IN_DAYS = 60

BILLING_TYPE_FULL_DAY = 'full-day'
BILLING_TYPE_HOURLY = 'hourly'
CONTRACT_BILLING_TYPE_MAPPING = {
    BILLING_TYPE_FULL_DAY: CONTRACT_TYPE_SME,
    BILLING_TYPE_HOURLY: CONTRACT_TYPE_C2C
}

DAYS_AHEAD_ORDER = 2

VAS_CHARGES = ['Toll Charges', 'Parking Charges']

CURRENCY_CHOICES = (
    ('INR', _('INR')),
    ('ZAR', _('ZAR'))
)

PAYMENT_INTERVAL_DAYS = {
    BASE_PAYMENT_CYCLE_DAILY: 1,
    BASE_PAYMENT_CYCLE_WEEK: 7,
    BASE_PAYMENT_CYCLE_FORTNIGHTLY: 15,
    BASE_PAYMENT_CYCLE_MONTH: 30
}

