# System and Django libraries
from datetime import datetime
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.forms.models import BaseInlineFormSet
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from recurrence.forms import RecurrenceField

# Blowhorn imports
from .models import BuyRate, SellRate, Contract, ServiceHSNCodeMap
from config.settings import status_pipelines as StatusPipeline
from blowhorn.users.models import User
from blowhorn.utils.functions import utc_to_ist
from blowhorn.driver.models import Driver, DriverPayment
from blowhorn.customer.models import CustomerDivision
from blowhorn.contract.constants import (
    CONTRACT_TYPE_HYPERLOCAL,
    CONTRACT_TYPE_SME,
    CONTRACT_TYPE_FIXED,
    CONTRACT_TYPE_SHIPMENT,
    CONTRACT_TYPE_BLOWHORN,
    PAY_CYCLE_CYCLE,
    AGGREGATE_CYCLE_MONTH,
    BASE_PAYMENT_CYCLE_PER_TRIP, BASE_PAYMENT_CYCLE_DAILY,
    CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_DEAFCOM, VAS_SERVICE_ATTRIBUTE_DEFCOM_SELL_RATE
)
from blowhorn.trip.constants import MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START
from .utils import get_per_day_amount
from blowhorn.contract.models import ContractTerm, ServiceHSNCodeMap
from blowhorn.trip.models import Trip

APPROVED = u'Approved'


class PaymentIntervalFormSet(BaseInlineFormSet):
    """
    Payment Interval validation to ensure
    1. No overlapping cycles happen
    2. All days in a month are covered
    """

    def clean(self):
        super(PaymentIntervalFormSet, self).clean()
        p_start_day = None
        p_end_day = None
        total_days = 0
        for form in self.forms:
            data = form.cleaned_data
            start_day = data.get('start_day')
            end_day = data.get('end_day')

            if not start_day or not end_day:
                raise ValidationError(
                    _('Payment Interval Start and End day is Mandatory or '
                        'Remove the Empty Slabs')
                )
            if p_end_day is not None and ((start_day - 1) != p_end_day):
                raise ValidationError(
                    _('Payment Cycles not in Sequence or are overlapping')
                )
            if end_day >= start_day:
                total_days += end_day - start_day + 1
            else:
                total_days += (31 - start_day + 1) + end_day

            p_end_day = end_day
            p_start_day = start_day  # Loop ends

        if total_days != 31:
            raise ValidationError(_('All Days not covered'))


class ResourceFormSet(BaseInlineFormSet):

    def __init__(self, *args, **kwargs):
        super(ResourceFormSet, self).__init__(*args, **kwargs)

        for form in self.forms:
            current_date = timezone.now()
            current_date = datetime.combine(
                current_date, form.instance.shift_start_time)
            current_date = utc_to_ist(current_date)
            form.initial['shift_start_time'] = current_date.time()


class ResourceAllocationInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ResourceAllocationInlineForm, self).__init__(*args, **kwargs)
        self.fields['drivers'].autocomplete = False
        self.fields['drivers'].queryset = Driver.objects.filter(
            status=StatusPipeline.ACTIVE,
            user__email__iregex=settings.DRIVER_DOMAIN_REGEX
        )


def ContractTermForm(is_c2c, blowhorn_contract=None):
    class ContractFormSet(BaseInlineFormSet):

        def clean(self, *args, **kwargs):
            count = 0
            wef_date_list = []
            for form in self.forms:
                data = form.cleaned_data

                WEF_date = data.get('wef')
                term = ContractTerm.objects.filter(id=form.instance.id).first()
                if term:
                    if DriverPayment.objects.filter(status=APPROVED, buy_rate_id=term.buy_rate_id).exists() and \
                        (term.buy_rate_id and term.buy_rate_id != form.instance.buy_rate_id or
                            term.sell_rate_id and term.sell_rate_id != form.instance.sell_rate_id):
                        raise ValidationError(
                            _('Cannot change buy rate and sell rate when DP is Approved')
                        )

                delete_flag = data.get('DELETE')

                if not WEF_date:
                    raise ValidationError(
                        "W.E.f. date is Mandatory for 'Contract Terms'"
                    )
                elif WEF_date > timezone.now().date() and not delete_flag:
                    count += 1

                if count > 1:
                    raise ValidationError(
                        _("Can't have more then one future date for "
                          "`Contract Terms`")
                    )
                wef_date_list.append(WEF_date)

                # if not blowhorn_contract:
                    # if not data.get('wef') or not data.get(
                    #         'buy_rate') or not data.get('sell_rate'):
                    #     raise ValidationError(
                    #         _('All Values in Contract Terms are mandatory')
                    #     )

                if blowhorn_contract:
                    if not data.get('wef') or not data.get('buy_rate'):
                        raise ValidationError(
                            _('Buyrate/W.E.F. are mandatory')
                        )

            if len(wef_date_list) != len(set(wef_date_list)):
                raise ValidationError(
                    _('With effective date can not be duplicate')
                )
            if is_c2c or blowhorn_contract:
                if not self.forms:
                    raise ValidationError(_('Contract Terms is mandatory'))

        def save_new(self, form, commit=True):
            obj = super().save_new(form, commit=False)
            contract = obj.contract
            for form in self.forms:
                data = form.cleaned_data
                sell_rate = data.get('sell_rate')
                if sell_rate.sellvasslab_set.filter(
                        vas_attribute__attribute=VAS_SERVICE_ATTRIBUTE_DEFCOM_SELL_RATE) and \
                        contract.contract_type != CONTRACT_TYPE_DEAFCOM:
                    raise ValidationError(
                        _('Defcom sell rate vas attribute is for Defcom contract only')
                    )

            if commit:
                if contract and contract.status == CONTRACT_STATUS_ACTIVE and \
                        contract.contract_type == CONTRACT_TYPE_BLOWHORN and obj.buy_rate is None:
                    raise ValidationError(
                        _('Active contracts must have buy rate defined')
                    )
                elif contract and contract.status == CONTRACT_STATUS_ACTIVE and \
                        (obj.sell_rate is None or obj.buy_rate is None):
                    raise ValidationError(
                        _('Active contracts must have sell rate and buy rate defined')
                    )
                obj.save()
            return obj

        def save_existing(self, form, instance, commit=True):
            obj = super().save_existing(
                    form, instance, commit=False)
            contract = obj.contract
            if commit:
                if contract.contractterm_set.latest('wef') == obj:
                    if contract and contract.status == CONTRACT_STATUS_ACTIVE and \
                            contract.contract_type == CONTRACT_TYPE_BLOWHORN and obj.buy_rate is None:
                        raise ValidationError(
                            _('Active contracts must have buy rate defined')
                        )
                    elif contract and contract.status == CONTRACT_STATUS_ACTIVE and\
                            (obj.sell_rate is None or obj.buy_rate is None):
                        raise ValidationError(
                            _('Active contracts must have sell rate and buy rate defined')
                        )
                obj.save()
            return obj

    return ContractFormSet


class ContractForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'division' in self.fields:
            contract = kwargs.get('instance')
            self.fields['division'].autocomplete = False
            if contract and contract.customer:
                self.fields['division'].queryset = \
                    CustomerDivision.objects.filter(customer=contract.customer)
            else:
                self.fields['division'].queryset = \
                    CustomerDivision.objects.none()

        if 'service_schedule' in self.fields:
            self.fields['service_schedule'] = RecurrenceField(required=False)

    def clean(self, *args, **kwargs):
        error_msg = []
        customer = self.cleaned_data.get('customer')
        if customer is None:
            error_msg.append('Customer(General Tab) is mandatory')

        if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            if customer and customer.is_gstin_mandatory and not customer.get_gst_info():
                raise ValidationError(_(
                    "Please add GST details for the customer first!"
                ))
        is_inter_city_contract = self.cleaned_data.get('is_inter_city_contract', False)
        source_city = self.cleaned_data.get('source_city', False)
        destination_city = self.cleaned_data.get('city', None)
        if is_inter_city_contract and not source_city:
            error_msg.append('source City is mandatory for inter city contract')

        before_hours = self.cleaned_data.get('hours_before_scheduled_at', None)
        after_hours = self.cleaned_data.get('hours_after_scheduled_at', None)
        trip_workflow = self.cleaned_data.get('trip_workflow', None)
        contract_type = self.cleaned_data.get('contract_type', None)
        service_schedule = self.cleaned_data.get('service_schedule', None)
        avg_basket_value = self.cleaned_data.get('avg_basket_value', None)

        if avg_basket_value == 0 and contract_type in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
            error_msg.append('Please add the average basket value for the contract.')

        if is_inter_city_contract and not contract_type == CONTRACT_TYPE_SHIPMENT:
            error_msg.append('Inter City applicable only for Shipment contract type.')

        if is_inter_city_contract and source_city == destination_city:
            error_msg.append('For Inter city source and destination city must be different')

        if not before_hours or before_hours <= 0 or not before_hours <= MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START:
            error_msg.append('Earliest a trip can begin hours(General Tab) '
                             'should be greater than 0 and less than %s' % MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START)

        if not after_hours or after_hours <= 0:
            error_msg.append('Latest a trip can begin hours(General Tab) '
                             'should be greater than 0')

        if not trip_workflow:
            error_msg.append('Trip Work Flow is mandatory')

        if 'service_schedule' in self.fields and \
                contract_type == CONTRACT_TYPE_FIXED and not service_schedule:
            error_msg.append('Service Schedule (General Tab) is mandatory for '
                             'Fixed Contract')

        if error_msg:
            raise ValidationError(error_msg)


class ContractEditForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'division' in self.fields:
            contract = kwargs.get('instance')
            self.fields['division'].autocomplete = False
            if contract and contract.customer:
                self.fields['division'].queryset = \
                    CustomerDivision.objects.filter(customer=contract.customer)
            else:
                self.fields[
                    'division'].queryset = CustomerDivision.objects.none()

        if 'service_schedule' in self.fields and \
                self.instance.contract_type != CONTRACT_TYPE_FIXED:
            self.fields['service_schedule'] = RecurrenceField(required=False)

    def clean(self, *args, **kwargs):
        error_msg = []
        before_hours = self.cleaned_data.get('hours_before_scheduled_at', None)
        after_hours = self.cleaned_data.get('hours_after_scheduled_at', None)
        trip_workflow = self.cleaned_data.get('trip_workflow', None)
        is_inter_city_contract = self.cleaned_data.get('is_inter_city_contract', False)
        destination_city = self.cleaned_data.get('city', None)
        source_city = self.cleaned_data.get('source_city', False)
        contract_type = self.cleaned_data.get('contract_type', None)

        if is_inter_city_contract and not source_city:
            error_msg.append('source City is mandatory for inter city contract')

        if not before_hours or before_hours <= 0 or not before_hours <= MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START:
            error_msg.append('Earliest a trip can begin hours(General Tab) '
                             'should be greater than 0 and less than %s' % MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START)

        if not after_hours or after_hours <= 0:
            error_msg.append('Latest a trip can begin hours(General Tab) '
                             'should be greater than 0')

        if not trip_workflow:
            error_msg.append('Trip Work Flow is mandatory')

        if 'trip_workflow' in self.changed_data:
            if Trip.objects.filter(customer_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists() or Trip.objects.filter(
                    blowhorn_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists():
                error_msg.append(
                    'Trip Workflow cannot be changed as there are ongoing trips for this workflow, '
                    'try after the trips are completed')

        if is_inter_city_contract and not contract_type == CONTRACT_TYPE_SHIPMENT:
            error_msg.append('Inter City applicable only for Shipment contract type.')

        if is_inter_city_contract and source_city == destination_city:
            error_msg.append('For Inter city source and destination city must be different')

        if error_msg:
            raise ValidationError(error_msg)


class ContractFormSales(ContractForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'service_schedule' in self.fields:
            self.fields['service_schedule'] = RecurrenceField(required=False)

    def clean(self, *args, **kwargs):
        error_msg = []
        customer = self.cleaned_data.get('customer')
        contract_type = self.cleaned_data.get('contract_type', None)
        service_schedule = self.cleaned_data.get('service_schedule', None)
        if customer is None:
            error_msg.append('Customer (General Tab) is mandatory')
        is_inter_city_contract = self.cleaned_data.get('is_inter_city_contract', False)
        source_city = self.cleaned_data.get('source_city', False)
        destination_city = self.cleaned_data.get('city', None)
        avg_basket_value = self.cleaned_data.get('avg_basket_value', None)

        if avg_basket_value == 0 and contract_type in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
            error_msg.append('Please add the average basket value for the contract.')

        if is_inter_city_contract and not contract_type == CONTRACT_TYPE_SHIPMENT:
            error_msg.append('Inter City applicable only for Shipment contract type.')

        if is_inter_city_contract and source_city == destination_city:
            error_msg.append('For Inter city source and destination city must be different')

        if is_inter_city_contract and not source_city:
            error_msg.append('source City is mandatory for inter city contract')

        if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            if customer and customer.is_gstin_mandatory and not customer.get_gst_info():
                raise ValidationError(_(
                    "Please add GST details for the customer first!"
                ))

        if 'service_schedule' in self.fields and \
                contract_type == CONTRACT_TYPE_FIXED and not service_schedule:
            error_msg.append('Service Schedule (General Tab) is mandatory for '
                             'Fixed Contract')

        if error_msg:
            raise ValidationError(error_msg)


class ContractEditFormSales(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'division' in self.fields:
            contract = kwargs.get('instance')
            self.fields['division'].autocomplete = False
            if contract and contract.customer:
                self.fields['division'].queryset = \
                    CustomerDivision.objects.filter(customer=contract.customer)
            else:
                self.fields['division'].queryset = \
                    CustomerDivision.objects.none()

        if 'service_schedule' in self.fields and \
                self.instance.contract_type != CONTRACT_TYPE_FIXED:
            self.fields['service_schedule'] = RecurrenceField(required=False)

    def clean(self, *args, **kwargs):
        error_msg = []
        service_tax_category = self.cleaned_data.get('service_tax_category',None)
        hsn_code = self.cleaned_data.get('hsn_code', None)
        is_inter_city_contract = self.cleaned_data.get('is_inter_city_contract', False)
        source_city = self.cleaned_data.get('source_city', False)
        if is_inter_city_contract and not source_city:
            error_msg.append('source City is mandatory for inter city contract')
        contract = self.instance

        if hsn_code and service_tax_category and \
            hsn_code.service_tax_category != service_tax_category:
            error_msg.append('HSN code should belong selected GTA/BSS category')

        if 'currency_code' in self.changed_data and contract.status == CONTRACT_STATUS_ACTIVE:
            error_msg.append('Cannot change currency after activating contract')

        if 'trip_workflow' in self.changed_data:
            if Trip.objects.filter(customer_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists() or Trip.objects.filter(
                    blowhorn_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists():
                error_msg.append(
                    'Trip Workflow cannot be changed as there are ongoing trips for this workflow, '
                    'try after the trips are completed')

        if error_msg:
            raise ValidationError(error_msg)


class ContractEditFormOperation(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

        if 'division' in self.fields:
            contract = kwargs.get('instance')
            self.fields['division'].autocomplete = False
            if contract and contract.customer:
                self.fields['division'].queryset = \
                    CustomerDivision.objects.filter(customer=contract.customer)
            else:
                self.fields['division'].queryset = \
                    CustomerDivision.objects.none()

        if 'service_schedule' in self.fields and \
                self.instance.contract_type != CONTRACT_TYPE_FIXED:
            self.fields['service_schedule'] = RecurrenceField(required=False)

    def clean(self, *args, **kwargs):
        error_msg = []
        before_hours = self.cleaned_data.get('hours_before_scheduled_at', None)
        is_inter_city_contract = self.cleaned_data.get('is_inter_city_contract', False)
        source_city = self.cleaned_data.get('source_city', False)
        after_hours = self.cleaned_data.get('hours_after_scheduled_at', None)
        trip_workflow = self.cleaned_data.get('trip_workflow', None)
        service_tax_category = self.cleaned_data.get('service_tax_category', None)
        hsn_code = self.cleaned_data.get('hsn_code', None)
        contract = self.instance

        if is_inter_city_contract and not source_city:
            error_msg.append('source City is mandatory for inter city contract')

        if not before_hours or before_hours <= 0 or not before_hours <= MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START:
            error_msg.append('Earliest a trip can begin hours(General Tab) '
                             'should be greater than 0 and less than %s' % MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START)

        if not after_hours or after_hours <= 0:
            error_msg.append('Latest a trip can begin hours(General Tab) '
                             'should be greater than 0')

        if not trip_workflow:
            error_msg.append('Trip Work Flow is mandatory')

        if hsn_code and service_tax_category and \
            hsn_code.service_tax_category != service_tax_category:
            error_msg.append('HSN code should belong selected GTA/BSS category')

        if 'currency_code' in self.changed_data and contract.status == CONTRACT_STATUS_ACTIVE:
            error_msg.append('Cannot change currency after activating contract')

        if 'trip_workflow' in self.changed_data:
            if Trip.objects.filter(customer_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists() or Trip.objects.filter(
                    blowhorn_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists():
                error_msg.append(
                    'Trip Workflow cannot be changed as there are ongoing trips for this workflow, '
                    'try after the trips are completed')

        if error_msg:
            raise ValidationError(error_msg)


class SMEContractForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')
        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

    def clean(self, *args, **kwargs):
        error_msg = []
        vehicle_classes = self.cleaned_data.get('vehicle_classes')
        if vehicle_classes and vehicle_classes.count() > 1:
            error_msg.append(
                'Vehicle classes(General Tab) : '
                'Cant choose more then 2 vehicles')

        name = self.cleaned_data.get('name')
        if name:
            if Contract.objects.filter(
                Q(contract_type=CONTRACT_TYPE_SME,
                    name=name.upper()) & ~Q(
                    pk=self.instance.id)
            ).exists():
                error_msg.append(
                    'Contract Name(General Tab) : '
                    'SME Contract with the same name already exists')

        before_hours = self.cleaned_data.get('hours_before_scheduled_at', None)
        after_hours = self.cleaned_data.get('hours_after_scheduled_at', None)
        trip_workflow = self.cleaned_data.get('trip_workflow', None)

        if not before_hours or before_hours <= 0 or not before_hours <= MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START:
            error_msg.append('Earliest a trip can begin hours(General Tab) '
                             'should be greater than 0 and less than %s' % MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START)

        if not after_hours or after_hours <= 0:
            error_msg.append('Latest a trip can begin hours(General Tab) should be greater than 0')

        if not trip_workflow:
            error_msg.append('Trip Work Flow is mandatory')

        if 'trip_workflow' in self.changed_data:
            if Trip.objects.filter(customer_contract_id=self.instance.id).exclude(
                status__in=StatusPipeline.TRIP_END_STATUSES).exists() or Trip.objects.filter(
                blowhorn_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists():
                raise ValidationError({
                    'trip_workflow': 'Trip Workflow cannot be changed as there are ongoing trips for this workflow, '
                    'try after the trips are completed'})


        if error_msg:
            raise ValidationError(error_msg)

        self.cleaned_data['name'] = self.cleaned_data.get('name').upper() if name else None


class C2CContractForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')
        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

    def clean(self, *args, **kwargs):
        error_msg = []
        vehicle_classes = self.cleaned_data.get('vehicle_classes')
        if vehicle_classes and vehicle_classes.count() > 1:
            error_msg.append('vehicle_classes: Cant choose more then 1 vehicles')

        city = self.cleaned_data.get('city')
        if city and vehicle_classes:
            name = 'C2C_' + str(city).upper() + '_' + \
                str(vehicle_classes[0]).upper()
            c2c_contract = Contract.objects.filter(name=name).exclude(
                id=self.instance.id)

            if c2c_contract:
                error_msg.append('C2C Contract for the city and Vehicle already exist ')

        before_hours = self.cleaned_data.get('hours_before_scheduled_at', None)
        after_hours = self.cleaned_data.get('hours_after_scheduled_at', None)
        trip_workflow = self.cleaned_data.get('trip_workflow', None)

        if not before_hours or before_hours <= 0 or not before_hours <= MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START:
            error_msg.append('Earliest a trip can begin hours(General Tab) '
                             'should be greater than 0 and less than %s' % MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START)

        if not after_hours or after_hours <= 0:
            error_msg.append('Latest a trip can begin hours(General Tab) should be greater than 0')

        if not trip_workflow:
            error_msg.append('Trip Work Flow is mandatory')

        if 'trip_workflow' in self.changed_data:
            if Trip.objects.filter(customer_contract_id=self.instance.id).exclude(
                status__in=StatusPipeline.TRIP_END_STATUSES).exists() or Trip.objects.filter(
                blowhorn_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists():
                raise ValidationError({
                    'trip_workflow': 'Trip Workflow cannot be changed as there are ongoing trips for this workflow, '
                    'try after the trips are completed'})


        if error_msg:
            raise ValidationError(error_msg)


class BlowhornContractForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')
        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

    def clean(self, *args, **kwargs):
        vehicle_classes = self.cleaned_data.get('vehicle_classes')
        if vehicle_classes and vehicle_classes.count() > 1:
            raise ValidationError({
                'vehicle_classes': 'Cant choose more then 1 vehicles'
            })

        if 'trip_workflow' in self.changed_data:
            if Trip.objects.filter(customer_contract_id=self.instance.id).exclude(
                status__in=StatusPipeline.TRIP_END_STATUSES).exists() or Trip.objects.filter(
                blowhorn_contract_id=self.instance.id).exclude(
                    status__in=StatusPipeline.TRIP_END_STATUSES).exists():
                raise ValidationError({
                    'trip_workflow': 'Trip Workflow cannot be changed as there are ongoing trips for this workflow, '
                    'try after the trips are completed'})


class KioskContractForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        if 'supervisors' in self.fields:
            self.fields['supervisors'].autocomplete = False
            self.fields['supervisors'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')
        if 'spocs' in self.fields:
            self.fields['spocs'].autocomplete = False
            self.fields['spocs'].queryset = User.objects.filter(
                is_staff=True, is_active=True).exclude(email__icontains='blowhorn.net')

    def clean(self):
        payment_cycle = self.cleaned_data.get('payment_cycle')
        trip_workflow = self.cleaned_data.get('trip_workflow')
        if not payment_cycle:
            raise ValidationError({'payment_cycle': 'Payment Cycle is Mandatory '})
        if not trip_workflow:
            raise ValidationError({'trip_workflow': 'Trip Workflow is Mandatory '})
        if 'trip_workflow' in self.changed_data:
            raise ValidationError({
                'trip_workflow': 'Trip Workflow cannot be changed as there are ongoing trips for this workflow, '
                'try after the trips are completed'})


class BuyRateForm(forms.ModelForm):
    reason_for_change = forms.CharField(widget=forms.HiddenInput(),
                                        required=False)

    def clean(self, *args, **kwargs):
        buyrate_name = self.cleaned_data.get('name')
        deduction_amount = self.cleaned_data.get('driver_deduction_per_leave')
        base_payment_interval = self.cleaned_data.get('base_payment_interval')
        driver_base_payment_amount = self.cleaned_data.get('driver_base_payment_amount')

        if base_payment_interval and driver_base_payment_amount:
            amount_per_day = get_per_day_amount(driver_base_payment_amount, base_payment_interval)

            if (deduction_amount and deduction_amount < amount_per_day) and \
                not base_payment_interval in [BASE_PAYMENT_CYCLE_PER_TRIP, BASE_PAYMENT_CYCLE_DAILY]:
                raise ValidationError({
                    'driver_deduction_per_leave': 'Should be greater then one day payment: %s ' % round(amount_per_day,2)
                })

        buy_rate = BuyRate.objects.filter(name=buyrate_name).exclude(
            id=self.instance.id)

        if buy_rate:
            raise ValidationError({
                'name': 'Buy Rate with this name already exist '
            })


class SellRateForm(forms.ModelForm):
    reason_for_change = forms.CharField(widget=forms.HiddenInput(),
                                        required=False)

    def clean(self, *args, **kwargs):
        sellrate_name = self.cleaned_data.get('name')
        sell_rate = SellRate.objects.filter(name=sellrate_name).exclude(
            id=self.instance.id)

        if sell_rate:
            raise ValidationError({
                'name': 'Sell Rate with this name already exist '
            })


class SlabForm(forms.ModelForm):
    step_size = forms.IntegerField(min_value=1)

    def clean(self, *args, **kwargs):
        when_to_pay = self.cleaned_data.get('when_to_pay')
        interval = self.cleaned_data.get('interval')
        start = self.cleaned_data.get('start')
        end = self.cleaned_data.get('end')
        split_payment = self.cleaned_data.get('split_payment')

        if when_to_pay == PAY_CYCLE_CYCLE and interval == AGGREGATE_CYCLE_MONTH:
            raise ValidationError({
                'interval': "For should be less then or equal to 'WHEN TO PAY' "
            })

        # check split_payment for False value as sell rate will form not have this field
        # and hence it will get None value
        if interval == AGGREGATE_CYCLE_MONTH and split_payment is False:
            raise ValidationError({
                'interval': "Split flag should be checked when FOR is %s"
                            % AGGREGATE_CYCLE_MONTH
            })

        if start and end and start > end:
            raise ValidationError({
                'start': "Start should be less then or equal to End"
            })


class ResourceAllocationHistoryFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(ResourceAllocationHistoryFormSet, self).__init__(*args, **kwargs)
        """display maximum 500 latest records"""
        self.queryset = self.queryset[:500]

class ServiceHSNCodeMapForm(forms.ModelForm):

    def clean(self, *args, **kwargs):
        tax_category = self.cleaned_data.get('service_tax_category')
        hsn_code = self.cleaned_data.get('hsn_code')
        is_active = self.cleaned_data.get('is_active')

        mapping = ServiceHSNCodeMap.objects.filter(service_tax_category=tax_category,
                                hsn_code=hsn_code).exclude(id=self.instance.id)

        if mapping.exists():
            raise ValidationError(_('Mapping already exists. Please '))
