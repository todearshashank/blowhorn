# Django and System libraries
import logging
import pytz
from datetime import datetime
from django.db.models import Q, Count
from django.utils import timezone
from django.conf import settings

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.models import Contract, ResourceAllocation, Resource
from blowhorn.address.models import City, Hub
from blowhorn.driver.models import Driver
from blowhorn.contract.constants import (CONTRACT_TYPE_FIXED,
                                         CONTRACT_STATUS_ACTIVE)
from blowhorn.utils.functions import ist_to_utc, utc_to_ist

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ResourceAllocationMixin(object):

    def is_manager_for_city(self, city_pk, user):
        """
        :param city_pk: pk of City
        :param user: instance of User
        :return: boolean
        """
        return City.objects.filter(pk=city_pk, managers=user).exists()

    def to_repr_contract(self, contract):
        """
        :param contract: instance of Contract
        :return: dict containing contract info
        """
        return {
            'id': contract.get('pk'),
            'name': contract.get('name'),
            'description': contract.get('description'),
            'city_id': contract.get('city_id'),
            'active_drivers': contract.get('active_drivers', 0),
            'division': {
                'id': contract.get('division__pk'),
                'name': contract.get('division__name')
            },
            'customer': {
                'id': contract.get('customer__pk'),
                'name': contract.get('customer__name')
            },
        }

    def get_customer_list(self):
        """
        @todo customer data for filter
        :return: list of dict with customer pk and name
        """
        return []

    def get_drivers(self, contract_pk):
        """
        :param contract_pk: pk of Contract
        :return: list of active drivers filtered based on vehicle class
            and body type
        """
        contract = Contract.objects.get(pk=contract_pk)
        drivers_qs = Driver.objects.filter(
            operating_city_id=contract.city_id,
            status=StatusPipeline.ACTIVE,
            vehicles__vehicle_model__vehicle_class__in=contract.vehicle_classes.all(),
            vehicles__body_type__in=contract.body_types.all()
        ).only('pk', 'name', 'driver_vehicle').order_by('name')

        return [{'id': d.pk, 'name': '%s | %s' % (d.name, d.driver_vehicle)}
                for d in drivers_qs]

    def get_customer_hubs(self, contract_pk):
        """
        :param contract_pk: pk of Contract
        :return: list of belongs to customer (contract-->customer)
        """
        contract = Contract.objects.only('customer_id', 'division_id',
                                         'city_id', 'service_schedule').get(
            pk=contract_pk)
        hub_qs = Hub.objects.only('pk', 'name', 'customer_id', 'pincodes').filter(customer_id=contract.customer_id)
        return [{'id': h.pk, 'name': '%s' % h.name} for h in hub_qs]

    def to_repr_resourceallocation(self, instance):
        """
        :param instance: instance of ResourceAllocation
        :return: dict containing resource allocation info
        """
        return {
            'id': instance.pk,
            'hub': {
                'id': instance.hub_id,
                'name': instance.hub.name
            },
            'drivers': sorted([{
                'id': d.pk,
                'name': d.name,
                'vehicle_number': d.driver_vehicle}
                for d in instance.drivers.all()], key=lambda k: k.get('name')),
            'shift_start_time': '%s' % self.get_ist_time(instance.shift_start_time),
            'unplanned_additional_drivers': instance.unplanned_additional_drivers,
            'remind_driver': instance.remind_driver,
            'exotel_app_id': instance.exotel_app_id
        }

    def get_resource_allocations(self, contract_pk):
        """
        :param contract_pk: pk of Contract
        :return: List of resource-allocation associated with given contract
        """
        resource_allocations_qs = ResourceAllocation.objects.filter(
            contract_id=contract_pk).select_related('contract', 'hub'). \
            order_by('hub__name')

        return [self.to_repr_resourceallocation(r) for r in
                resource_allocations_qs]

    def get_utc_time(self, shift_start_time):
        """
        :param shift_start_time: shift start time (str) in IST +0530
        :return: UTC (datetime.time)
        """
        _time = shift_start_time
        if isinstance(shift_start_time, str):
            _time = datetime.strptime(shift_start_time, '%H:%M').time()

        current_date = timezone.now()
        current_date = datetime.combine(current_date, _time)
        old_timezone = pytz.timezone(settings.ACT_TIME_ZONE)
        new_timezone = pytz.timezone('UTC')
        new_date = old_timezone.localize(
            current_date).astimezone(new_timezone)
        return new_date.time()

    def get_ist_time(self, shift_start_time):
        """
        :param shift_start_time: shift start time (datetime.time) in UTC
        :return: time in ACT_TIMEZONE (datetime.time)
        """
        _time = ''
        current_date = timezone.now()
        current_date = datetime.combine(current_date, shift_start_time)
        return utc_to_ist(current_date).isoformat()

    def get_validated_data(self, contract_pk, data, resource_pk=None):
        """
        :param contract_pk: pk of Contract
        :param data: dict of resource details
        :param resource_pk: pk of resource
        :return: validated data of resource
        """
        hub_pk = data.pop('hub', {}).get('id')
        if not hub_pk:
            return False, '`hub` cannot be empty'

        shift_time_dict = data.get('shift_start_time', None)
        if not shift_time_dict:
            return False, '`shift start time` cannot be empty'

        shift_start_time = ':'.join(
            [
                '%s' % shift_time_dict.get('hour', '9'),
                '%s' % shift_time_dict.get('minute', '00')
            ]
        )

        if data.get('unplanned_additional_drivers', None) is None:
            data['unplanned_additional_drivers'] = 0

        data['contract_id'] = contract_pk
        data['hub_id'] = hub_pk
        data['shift_start_time'] = self.get_utc_time(shift_start_time)

        if not data.get('id', None) and ResourceAllocation.objects.filter(
            contract_id=contract_pk,
            hub_id=hub_pk,
            shift_start_time=data['shift_start_time']
        ).exists() or data.get('id', None) and \
            ResourceAllocation.objects.filter(
            contract_id=contract_pk,
            hub_id=hub_pk,
            shift_start_time=data['shift_start_time']).exclude(
                id=resource_pk
            ).exists():
            return False, '`Hub` with same `Shift start time` already exists.'

        return True, data

    def create_resource_allocation(self, data):
        """
        :param data: dict of resource allocation
        :return: instance of Resource Allocation
        """
        return ResourceAllocation.objects.create(**data)

    def update_resource_allocation(self, data):
        """
        :param data: dict of resource allocation
        :return: instance of Resource Allocation
        """
        qs = ResourceAllocation.objects.filter(pk=data.get('id'))
        updated = qs.update(**data)
        logger.info('Updated resource allocation: %s' % updated)
        return qs.first()

    def __create_resource(self, instance, driver_id):
        """
        :param instance: instance of ResourceAllocation
        :param driver_id:  pk of Driver
        :return: instance of Resource
        """
        resource = Resource(driver_id=driver_id, resourceallocation=instance)
        resource.save()
        return resource

    def __get_added_resources(self, initial_drivers, drivers):
        """
        :param initial_drivers: list of pk of drivers associated with resource
        :param drivers: master data of driver from client side
        :return: list of pk of newly added drivers
        """
        return [x for x in drivers if x not in initial_drivers]

    def __get_removed_resources(self, initial_drivers, drivers):
        """
        :param initial_drivers: list of pk of drivers associated with resource
        :param drivers: master data of driver from client side
        :return: list of pk of newly added drivers
        """
        return [x for x in initial_drivers if x not in drivers]

    def change_drivers_in_resources(self, instance, drivers, created):
        """
        :param instance: instance of ResourceAllocation
        :param drivers: list of pk of drivers from client
        :param created: indicates whether given `instance`
            is created or updated (boolean)
        :return: Number of drivers added and removed
        """
        resources_added = []
        resources_removed = []
        resources_added_count = 0
        resources_removed_count = 0
        if created:
            resources_added = drivers
        else:
            initial_drivers = [i.pk for i in instance.drivers.all()]
            resources_added = self.__get_added_resources(initial_drivers,
                                                         drivers)
            resources_removed = self.__get_removed_resources(initial_drivers,
                                                             drivers)

        # Single entity creation to trigger post_save() signal
        for driver_id in resources_added:
            try:
                resource = self.__create_resource(instance, driver_id)
                resources_added_count += 1
            except BaseException as be:
                return False, 'Failed to created resources for %s' % driver_id

        if any(resources_removed):
            r_instances = Resource.objects.filter(
                resourceallocation=instance,
                driver_id__in=resources_removed
            )
            try:
                resources_removed_count = r_instances.delete()
            except BaseException as be:
                return False, 'Failed to remove resources: %s' % resources_removed

        return True, '%s added, %s removed' % (
            resources_added_count, resources_removed_count)
