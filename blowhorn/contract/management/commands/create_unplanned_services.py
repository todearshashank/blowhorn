import argparse
from django.core.management.base import BaseCommand, CommandError
from blowhorn.contract.utils import SubscriptionService


class Command(BaseCommand):
    help = """Creates unplanned services for `Active` contracts
            optional arg: CONTRACT_NAME - generates order/trips for the specified contract"""

    def add_arguments(self, parser):
        parser.add_argument('-c', '--contract', nargs='?', help='Contract Name')
        parser.add_argument('-i', '--contract_id', nargs='?', type=int, help='DB Contract ID')

    def handle(self, *args, **kwargs):
        if kwargs['contract'] and kwargs['contract_id']:
            self.stdout.write('Enter either contract name or id, not both')
            return
        if kwargs['contract']:
            self.stdout.write('generating subscription services for contract %s' % (kwargs['contract']))
        elif kwargs['contract_id']:
            self.stdout.write('generating subscription services for contract %d' %(kwargs['contract_id']))
        else:
            self.stdout.write('generating subscription services for all contracts')

        SubscriptionService().create_unplanned_services(**kwargs)
