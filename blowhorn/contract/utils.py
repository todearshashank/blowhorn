# System and Django libraries
import re
import logging
import pytz
import calendar
import math
import datetime as TimeObject
from datetime import datetime, timedelta
from django.utils import timezone
from django.conf import settings
from django.db import transaction
from django.db.models import Q, Prefetch, F, Avg, OuterRef, Subquery
from django.core.exceptions import ValidationError

from blowhorn.address.models import Slots
from blowhorn.common.middleware import current_request
from datetime import date
from datetime import datetime as dt
from functools import reduce
import operator

from blowhorn.contract.mixins import ResourceAllocationMixin
from config.settings.status_pipelines import TRIP_COMPLETED, DRIVER_ACCEPTED

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model
from blowhorn.oscar.core import prices

# Blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.models import Contract, ResourceAllocation, \
    ContractTerm, SellVehicleSlab, SellVASSlab
from blowhorn.order.utils import OrderNumberGenerator
from blowhorn.order.models import ShippingAddress
from blowhorn.contract.serializers import VehicleClassPackageSerializer
from blowhorn.contract.constants import (
    CONTRACT_TYPE_C2C,
    CONTRACT_TYPE_FIXED,
    CONTRACT_STATUS_ACTIVE,
    DAYS_AHEAD_ORDER,
)
from blowhorn.users.models import User
from blowhorn.exotel.calls import ExotelCall
from blowhorn.vehicle.models import VehicleClass, CityVehicleClassAlias
from blowhorn.vehicle.utils import VehicleUtils
from blowhorn.address.utils import AddressUtil
from blowhorn.exotel.constants import *
from blowhorn.exotel.models import CallDetail
from blowhorn.contract.constants import BASE_PAYMENT_CYCLE_DAILY, BASE_PAYMENT_CYCLE_FORTNIGHTLY, \
    BASE_PAYMENT_CYCLE_MONTH, BASE_PAYMENT_CYCLE_WEEK, BASE_PAYMENT_CYCLE_PER_TRIP
from blowhorn.utils.functions import ist_to_utc
from blowhorn.oscar.core.loading import get_model
from blowhorn.trip.mixins import TripMixin
from blowhorn.order.utils import OrderCreator

Order = get_model('order', 'Order')
WayPoint = get_model('order', 'WayPoint')
Event = get_model('order', 'Event')
OrderBatch = get_model('order', 'OrderBatch')
Trip = get_model('trip', 'Trip')

DATE_FORMAT = "%b %d %Y"


class SubscriptionService(object):
    """
    Generate subscription orders and blanket trips
    """

    def __init__(self, logger=None):
        if not logger:
            logger = logging.getLogger(__name__)
        self.logger = logger
        self.before_after_days = 0

    def get_hub_address_as_shipping_address(self, hub):
        shipping_address_list = []
        shipping_address_list.append(
            ShippingAddress(**{
                'line1': hub.address.line1,
                'line2': hub.address.line2,
                'line3': hub.address.line3,
                'line4': hub.address.line4,
                'state': hub.address.state,
                'postcode': hub.address.postcode,
                'country': hub.address.country,
                'geopoint': hub.address.geopoint
            })
        )
        shipping_address_list.append(
            ShippingAddress(**{
                'line1': hub.address.line1,
                'line2': hub.address.line2,
                'line3': hub.address.line3,
                'line4': hub.address.line4,
                'state': hub.address.state,
                'postcode': hub.address.postcode,
                'country': hub.address.country,
                'geopoint': hub.address.geopoint
            })
        )
        return shipping_address_list

    def get_shipping_address_list_from_route(self, route):
        shipping_address_list = []
        stops = route.stops.all().order_by('seq_no')

        for stop in stops:
            shipping_address_list.append(
                ShippingAddress(**{
                    'first_name': stop.contact_name if stop.contact_name else '',
                    'line1': stop.line1,
                    'line2': stop.line2,
                    'line3': stop.line3,
                    'line4': stop.line4,
                    'state': stop.state,
                    'postcode': stop.postcode,
                    'phone_number': stop.contact_mobile if stop.contact_mobile else None,
                    'country': stop.country,
                    'geopoint': stop.geopoint
                })
            )
        return ShippingAddress.objects.bulk_create(shipping_address_list)

    def _get_resources_from_contract(self, contract):
        ''' Returns the hub/driver map for passed contract '''
        # @TODO use get and ensure only one record
        resources = ResourceAllocation.objects.filter(
            contract=contract).prefetch_related('drivers')
        if resources:
            return resources
        else:
            print('No driver associated with this contract ', contract)
            print('No Resource Allocation for this contract ', contract)
            return None

    def save_enterprise_order(self, user, pickup_datetime, customer, city,
                              hub, driver, invoice_driver, order_type, contract,
                              blowhorn_contract, created_by, batch=None, route=None,
                              device_type='Admin', vehicle_class=None, division=None, meter_reading_start=None,
                              meter_reading_end=None, actual_start_time=None, actual_end_time=None,
                              trip_acceptence_time=None, trip_base_pay=None):
        if driver and Order.objects.filter(
            customer_contract=contract,
            driver=driver, hub=hub,
            order_type=order_type,
            pickup_datetime=pickup_datetime).exists():
            print("Order already exists")
            return 0, None

        ''' Revisit this, order_total defaulted to 0 '''
        TAX_PERCENT = 0
        total_cost_excluding_tax = 0
        TAX_ABSOLUTE = TAX_PERCENT * total_cost_excluding_tax * 0.01
        order_total = prices.Price(
            currency=settings.OSCAR_DEFAULT_CURRENCY,
            excl_tax=total_cost_excluding_tax,
            tax=TAX_ABSOLUTE
        )
        if not created_by:
            created_by = User.objects.filter(
                email=settings.CRON_JOB_DEFAULT_USER).first()
        shipping_address_list = self.get_hub_address_as_shipping_address(hub)
        with transaction.atomic():
            # Save the address
            hub_address, dropoff_address = ShippingAddress.objects.bulk_create(
                shipping_address_list)
            address_obj_list = None
            # Create the order
            if route:
                address_obj_list = self.get_shipping_address_list_from_route(
                    route)

            order = OrderCreator().create_order_model(
                order_number=OrderNumberGenerator().order_number(
                    order_type=order_type),
                user=user,
                shipping_address=dropoff_address,
                pickup_address=hub_address,
                billing_address=None,
                total=order_total,
                pickup_datetime=pickup_datetime,
                date_placed=timezone.now(),
                items=[''],
                status=StatusPipeline.ORDER_NEW,
                order_type=order_type,
                customer=customer,
                city=city,
                hub=hub,
                driver=driver,
                customer_contract=contract,
                created_by=created_by,
                created_date=timezone.now(),
                batch=batch,
                device_type=device_type,
                customer_category=customer.customer_category,
                vehicle_class_preference=vehicle_class,
                division=division
            )
            if order:

                # create waypoint for fixed order
                from blowhorn.order.mixins import OrderPlacementMixin
                OrderPlacementMixin().add_waypoint(order, order.shipping_address, user, 1)
                Event.objects.create(
                    status=StatusPipeline.ORDER_NEW,
                    order=order,
                    address=hub_address,
                    hub_associate=created_by,
                    driver=driver
                )

                # Create a blanket trip
                trip = TripMixin().create_trip(
                    driver=driver,
                    invoice_driver=invoice_driver,
                    contract=contract,
                    blowhorn_contract=blowhorn_contract,
                    order=order,
                    hub=hub,
                    planned_start_time=pickup_datetime,
                    isBatchUser=True,
                    batch=batch,
                    meter_reading_start=meter_reading_start,
                    meter_reading_end=meter_reading_end,
                    actual_start_time=actual_start_time,
                    actual_end_time=actual_end_time,
                    trip_acceptence_time=trip_acceptence_time,
                    trip_base_pay=trip_base_pay
                )
                if not order.created_by:
                    Order.objects.filter(pk=order.id).update(
                        created_by=created_by)
                if address_obj_list:
                    dropoff_waypoint = order.waypoint_set.filter(
                        shipping_address_id=order.shipping_address_id)
                    sequence_id = 1
                    for shipping_address in address_obj_list:
                        data = {
                            'sequence_id': sequence_id,
                            'order': order,
                            'shipping_address': shipping_address,
                        }

                        waypoint = WayPoint(**data)
                        waypoint.save()
                        logging.info(
                            'Waypoint %d Addition Successful', waypoint.id)
                        sequence_id += 1
                    dropoff_waypoint.update(sequence_id=sequence_id)
                from blowhorn.apps.driver_app.v3.services.order.decision import OrderDecision
                OrderDecision()._add_c2c_order_as_trip_stop(order=order, trip=trip)

            # If pickup_datetime is "after" tommorow, create `Scheduled Off`
            # trips. Applicable for Fixed Orders only
            if order_type and not order_type == settings.SUBSCRIPTION_ORDER_TYPE:
                return 1, order
            today = timezone.now() + timedelta(days=self.before_after_days)
            date_diff = (pickup_datetime.date() - today.date()).days
            threshold_utc_time = TimeObject.time(18, 30, 00)
            if pickup_datetime.time() >= threshold_utc_time:
                date_diff += 1
            if date_diff > 1:
                missing_days = set(today.date() + timedelta(days=x)
                                   for x in range(1, date_diff))
                for day in missing_days:  # Create schedule off trip
                    # Check if Scheduled off trip exists for the day
                    self.create_scheduled_off_trip(driver, contract,
                                                   StatusPipeline.TRIP_SCHEDULED_OFF,
                                                   day, hub, batch,
                                                   blowhorn_contract)

                    """
                    below commented logic has been put in above function
                    """
                    # if Trip.objects.filter(driver=driver,
                    #                        customer_contract=contract,
                    #                        status=StatusPipeline.TRIP_SCHEDULED_OFF,
                    #                        planned_start_time=day).exists():
                    #     print("Scheduled off trip exists for contract(%s), driver(%s), day(%s)",
                    #           (contract, driver, day))
                    #     continue
                    # # Create trip for the missing interval
                    # TripMixin().create_trip(
                    #     driver=driver,
                    #     contract=contract,
                    #     blowhorn_contract=blowhorn_contract,
                    #     status=StatusPipeline.TRIP_SCHEDULED_OFF,
                    #     planned_start_time=day,
                    #     hub=hub,
                    #     isBatchUser=True,
                    #     batch=batch)
            return 1, order

    def find_pickup_time(self, contract, shift_start_time):
        '''
        Find out the pick up time based on contract service calendar
        and shift start time from resource allocation
        '''
        # Find out the next service occurence
        # ON or after NOW
        # Since TimeField Doesn't have info about timezone
        # time above 18:30 will cause erraneous data.
        threshold_utc_time = TimeObject.time(18, 30, 00)
        day = timezone.now().replace(tzinfo=None)
        day = day + timedelta(days=self.before_after_days)
        occurence = contract.service_schedule.after(day, inc=True)

        if occurence:
            # Find out the order pickup time
            shift_start_time = dt.combine(occurence,
                                          shift_start_time)

            if (shift_start_time.time() >= threshold_utc_time):
                return shift_start_time.replace(tzinfo=pytz.UTC) - timedelta(1)
            else:
                return shift_start_time.replace(tzinfo=pytz.UTC)
        return None

    def create_spot_order(self, contract, hub, driver, invoice_driver,
                          pickup_datetime, created_by):
        # print("Creating spot order for contract(%s), "
        #       "hub(%s), driver(%s), pickupdatetime(%s)"
        #       % (contract, hub, driver, pickup_datetime))

        self.save_enterprise_order(
            user=contract.customer.user,
            pickup_datetime=pickup_datetime,
            customer=contract.customer,
            city=contract.city,
            hub=hub,
            driver=driver,
            invoice_driver=invoice_driver,
            order_type=settings.SPOT_ORDER_TYPE,
            contract=contract,
            division=contract.division,
            blowhorn_contract=driver.contract if driver else None,
            created_by=created_by)

    def create_bulk_spot_order(
        self, contract, hub, pickup_datetime, created_by, count):
        print("Creating spot order for contract(%s), "
              "hub(%s), pickupdatetime(%s)"
              % (contract, hub, pickup_datetime))

        user = contract.customer.user
        customer = contract.customer
        city = contract.city
        order_type = settings.SPOT_ORDER_TYPE

        ''' Revisit this, order_total defaulted to 0 '''
        TAX_PERCENT = 0
        total_cost_excluding_tax = 0
        TAX_ABSOLUTE = TAX_PERCENT * total_cost_excluding_tax * 0.01
        total = prices.Price(
            currency=settings.OSCAR_DEFAULT_CURRENCY,
            excl_tax=total_cost_excluding_tax,
            tax=TAX_ABSOLUTE
        )

        if not created_by:
            created_by = User.objects.filter(
                email=settings.CRON_JOB_DEFAULT_USER).first()
        with transaction.atomic():
            # Create the order
            order_field_values = {
                'currency': total.currency,
                'total_incl_tax': total.incl_tax,
                'total_excl_tax': total.excl_tax,
                'billing_address': None,
                'pickup_datetime': pickup_datetime,
                'date_placed': timezone.now(),
                'items': [''],
                'status': StatusPipeline.ORDER_NEW,
                'order_type': order_type,
                'customer': customer,
                'customer_category': customer.customer_category,
                'city': city,
                'customer_contract': contract,
                'created_by': created_by,
                'created_date': timezone.now()
            }

            if user and user.is_authenticated:
                order_field_values['user_id'] = user.id

            hub_address_list = []
            for i in range(count):
                # Save the address
                shipping_address_list = self.get_hub_address_as_shipping_address(
                    hub)
                hub_address_list = hub_address_list + shipping_address_list

            hub_addr = ShippingAddress.objects.bulk_create(hub_address_list)
            spot_order_list = []
            j = 0
            for i in range(count):
                order_number = OrderNumberGenerator().order_number(
                    order_type=order_type)
                order_data = {
                    'number': order_number,
                    'hub': hub,
                    'shipping_address': hub_addr[j],
                    'pickup_address': hub_addr[j + 1]
                }
                order_data.update(order_field_values)
                spot_order_list.append(Order(**order_data))
                j = j + 2

            spot_orders = Order.objects.bulk_create(spot_order_list)

            if contract:
                trip_workflow = contract.trip_workflow
            trip_field_values = {
                'status': StatusPipeline.TRIP_NEW,
                'customer_contract': contract,
                'planned_start_time': pickup_datetime if pickup_datetime else timezone.now(),
                'hub': hub if hub else None,
                'trip_workflow': trip_workflow,
                'created_date': timezone.now(),
                'created_by': created_by
            }

            event_field_values = {
                'status': StatusPipeline.ORDER_NEW,
                'hub_associate': created_by
            }

            trip_list = []
            event_list = []
            for i in range(count):
                trip_number = Trip._get_trip_reference_number(self)
                trip_data = {
                    'trip_number': trip_number,
                    'order': spot_orders[i]
                }
                trip_data.update(trip_field_values)
                trip_list.append(Trip(**trip_data))

                event_data = {
                    'order': spot_orders[i],
                    'address': hub_addr[i]
                }
                event_data.update(event_field_values)
                event_list.append(Event(**event_data))

            trips = Trip.objects.bulk_create(trip_list)
            Event.objects.bulk_create(event_list)

        # code here
        from blowhorn.order.mixins import OrderPlacementMixin
        from blowhorn.apps.driver_app.v1.services.order.decision import OrderDecision

        for i in range(count):
            OrderPlacementMixin().add_waypoint(
                spot_orders[i], spot_orders[i].shipping_address, user, 1)
            OrderDecision()._add_c2c_order_as_trip_stop(
                order=spot_orders[i], trip=trips[i])

    def get_active_contracts(self, contract=None, contract_id=None,
                             contract_list=None):
        if contract and contract_id:
            return None
        if contract:
            return Contract.objects.filter(
                name=contract, status=CONTRACT_STATUS_ACTIVE
            )
        elif contract_id:
            return Contract.objects.filter(
                id=contract_id, status=CONTRACT_STATUS_ACTIVE
            )
        elif contract_list:
            return Contract.objects.filter(
                id__in=contract_list, status=CONTRACT_STATUS_ACTIVE
            )
        else:
            return Contract.objects.filter(status=CONTRACT_STATUS_ACTIVE)

    # @TODO refactor common logic from create_services and create_unplanned_services

    def create_deafcom_order(self, contract, hub, driver, invoice_driver,
                             pickup_datetime, created_by, meter_reading_start, meter_reading_end, actual_start_time,
                             actual_end_time, trip_base_pay):
        # print("Creating deafcom order for contract(%s), "
        #       "hub(%s), driver(%s), pickupdatetime(%s)"
        #       % (contract, hub, driver, pickup_datetime))

        self.save_enterprise_order(
            user=contract.customer.user,
            pickup_datetime=pickup_datetime,
            customer=contract.customer,
            city=contract.city,
            hub=hub,
            driver=driver,
            invoice_driver=invoice_driver,
            order_type=settings.DEAFCOM_ORDER_TYPE,
            contract=contract,
            division=contract.division,
            blowhorn_contract=driver.contract if driver else None,
            created_by=created_by,
            meter_reading_start=meter_reading_start,
            meter_reading_end=meter_reading_end,
            actual_start_time=actual_start_time,
            actual_end_time=actual_end_time,
            trip_acceptence_time=actual_start_time,
            trip_base_pay=trip_base_pay
        )

    def create_bulk_deafcom_order(self, contract, hub, pickup_datetime, created_by, 
            count, meter_reading_start, meter_reading_end,
            actual_start_time, actual_end_time):

        user = contract.customer.user
        customer = contract.customer
        city = contract.city
        order_type = settings.DEAFCOM_ORDER_TYPE

        ''' Revisit this, order_total defaulted to 0 '''
        TAX_PERCENT = 0
        total_cost_excluding_tax = 0
        TAX_ABSOLUTE = TAX_PERCENT * total_cost_excluding_tax * 0.01
        total = prices.Price(
            currency=settings.OSCAR_DEFAULT_CURRENCY,
            excl_tax=total_cost_excluding_tax,
            tax=TAX_ABSOLUTE
        )

        if not created_by:
            created_by = User.objects.filter(
                email=settings.CRON_JOB_DEFAULT_USER).first()
        with transaction.atomic():
            # Create the order
            order_field_values = {
                'currency': total.currency,
                'total_incl_tax': total.incl_tax,
                'total_excl_tax': total.excl_tax,
                'billing_address': None,
                'pickup_datetime': pickup_datetime,
                'date_placed': timezone.now(),
                'items': [''],
                'status': StatusPipeline.ORDER_NEW,
                'order_type': order_type,
                'customer': customer,
                'customer_category': customer.customer_category,
                'city': city,
                'customer_contract': contract,
                'created_by': created_by,
                'created_date': timezone.now(),
                'meter_reading_start': meter_reading_start,
                'meter_reading_end': meter_reading_end,
                'actual_start_time': actual_start_time,
                'actual_end_time': actual_end_time,

            }

            if user and user.is_authenticated:
                order_field_values['user_id'] = user.id

            hub_address_list = []
            for i in range(count):
                # Save the address
                shipping_address_list = self.get_hub_address_as_shipping_address(
                    hub)
                hub_address_list = hub_address_list + shipping_address_list

            hub_addr = ShippingAddress.objects.bulk_create(hub_address_list)
            deafcom_order_list = []
            j = 0
            for i in range(count):
                order_number = OrderNumberGenerator().order_number(
                    order_type=order_type)
                order_data = {
                    'number': order_number,
                    'hub': hub,
                    'shipping_address': hub_addr[j],
                    'pickup_address': hub_addr[j + 1]
                }
                order_data.update(order_field_values)
                deafcom_order_list.append(Order(**order_data))
                j = j + 2

            deafcom_orders = Order.objects.bulk_create(deafcom_order_list)

            if contract:
                trip_workflow = contract.trip_workflow
            trip_field_values = {
                'status': StatusPipeline.TRIP_NEW,
                'customer_contract': contract,
                'planned_start_time': pickup_datetime if pickup_datetime else timezone.now(),
                'hub': hub if hub else None,
                'trip_workflow': trip_workflow,
                'created_date': timezone.now(),
                'created_by': created_by,
                'meter_reading_start': meter_reading_start,
                'meter_reading_end': meter_reading_end,
                'actual_start_time': actual_start_time,
                'actual_end_time': actual_end_time,
            }

            event_field_values = {
                'status': StatusPipeline.ORDER_NEW,
                'hub_associate': created_by
            }

            trip_list = []
            event_list = []
            for i in range(count):
                trip_number = Trip._get_trip_reference_number(self)
                trip_data = {
                    'trip_number': trip_number,
                    'order': deafcom_orders[i]
                }
                trip_data.update(trip_field_values)
                trip_list.append(Trip(**trip_data))

                event_data = {
                    'order': deafcom_orders[i],
                    'address': hub_addr[i]
                }
                event_data.update(event_field_values)
                event_list.append(Event(**event_data))

            trips = Trip.objects.bulk_create(trip_list)
            Event.objects.bulk_create(event_list)

        # code here
        from blowhorn.order.mixins import OrderPlacementMixin
        from blowhorn.apps.driver_app.v1.services.order.decision import OrderDecision

        for i in range(count):
            OrderPlacementMixin().add_waypoint(
                deafcom_orders[i], deafcom_orders[i].shipping_address, user, 1)
            OrderDecision()._add_c2c_order_as_trip_stop(
                order=deafcom_orders[i], trip=trips[i])

    # @TODO refactor common logic from create_services and create_unplanned_services
    def create_unplanned_services(self, *args, **kwargs):
        """
        Create subscription services without drivers | unplanned
        """
        i_contract = kwargs.pop('contract', None)
        i_contract_id = kwargs.pop('contract_id', None)
        self.before_after_days = kwargs.pop('days', 0)
        if i_contract and i_contract_id:
            return

        print("Create subscription services without drivers | unplanned")

        date_batch = timezone.now().date() + timedelta(days=self.before_after_days)
        desc_batch = date_batch.strftime('%d-%m-%Y')
        desc_batch = "Unplanned Orders  %s" % (desc_batch)
        batch = OrderBatch.objects.create(
            description=desc_batch, number_of_entries=0)
        rec_processed = 0

        contracts = None
        if i_contract:
            contracts = self.get_active_contracts(contract=i_contract)
        elif i_contract_id:
            contracts = self.get_active_contracts(contract_id=i_contract_id)
        else:
            contracts = self.get_active_contracts()

        for contract in contracts:
            if contract.contract_type != CONTRACT_TYPE_FIXED:
                print("Subscription Orders is for Fixed Contracts only (%s)" % contract)
                continue
            print("Processing contract (%s)..." % contract)
            # for each active contract
            resources = self._get_resources_from_contract(contract)
            created_by = User.objects.filter(
                email=settings.CRON_JOB_DEFAULT_USER).first()

            if not resources:
                continue

            for resource in resources:
                # for each hub in the contract
                pickup_datetime = self.find_pickup_time(
                    contract, resource.shift_start_time)

                if not pickup_datetime:
                    continue

                # Quit if no dummy resource allocation needed
                if resource.unplanned_additional_drivers < 1:
                    continue

                print("Creating subscription orders for contract(%s),"
                      "hub(%s), unplanned_resources(%d), pickup_at(%s)"
                      % (contract, resource.hub,
                         resource.unplanned_additional_drivers,
                         pickup_datetime))

                now = dt.now()
                now = now + timedelta(days=self.before_after_days)
                threshold_pickup_time = None
                threshold_utc_time = TimeObject.time(18, 30, 00)
                # if shift-start-time is before 5:30AM, when creating order,
                # planned start time will fall in current date (UTC), which will results
                # in validation failure and creation of duplicate orders for same date
                if resource.shift_start_time >= threshold_utc_time:
                    threshold_pickup_time = now.replace(
                        hour=18, minute=30, second=0, tzinfo=pytz.UTC)
                else:
                    threshold_pickup_time = now + timedelta(1)

                # Find out how many dummy orders already exist
                existing_orders = Order.objects.filter(
                    customer_contract=contract,
                    hub=resource.hub,
                    pickup_datetime__time=resource.shift_start_time,
                    driver__isnull=True,
                    order_type=settings.SUBSCRIPTION_ORDER_TYPE,
                    pickup_datetime__date__gte=threshold_pickup_time
                ).count()

                if existing_orders >= resource.unplanned_additional_drivers:
                    print("%d Subscriptions exist for requirements of %d" %
                          (existing_orders,
                           resource.unplanned_additional_drivers))
                    continue

                # Add subscription orders with no known drivers
                remaining_orders = resource.unplanned_additional_drivers - \
                                   existing_orders
                while remaining_orders > 0:
                    order_processed, new_order = self.save_enterprise_order(
                        user=contract.customer.user,
                        pickup_datetime=pickup_datetime,
                        customer=contract.customer,
                        division=contract.division,
                        city=contract.city,
                        hub=resource.hub,
                        driver=None,  # driver not known
                        invoice_driver=None,
                        order_type=settings.SUBSCRIPTION_ORDER_TYPE,
                        contract=contract,
                        blowhorn_contract=None,
                        created_by=created_by,
                        batch=batch,
                        device_type='Celery')
                    # decrement the remaining orders to be generated by 1
                    remaining_orders -= 1
                    rec_processed += order_processed
        batch.end_time = timezone.now()
        batch.number_of_entries = rec_processed
        batch.save()

    def create_subscription_trip(self, *args, **kwargs):
        slots = Slots.objects.filter(contract__isnull=False, hub__isnull=False)
        slots = slots.select_related('contract')
        slots = slots.prefetch_related('autodispatch_drivers')
        from blowhorn.apps.driver_app.v3.helpers.trip import create_trip
        for slot in slots:
            # converting time to utc
            start_time = ResourceAllocationMixin().get_utc_time(slot.start_time)
            planned_start_time = self.find_pickup_time(slot.contract, start_time)
            if not planned_start_time:
                continue
            for driver in slot.autodispatch_drivers.all():
                if not driver.status == StatusPipeline.ACTIVE:
                    continue
                if Trip.objects.filter(driver=driver,
                                       customer_contract=slot.contract,
                                       planned_start_time=planned_start_time).exists():
                    continue
                create_trip(driver.id, customer_contract=slot.contract_id,
                            blowhorn_contract=driver.contract_id if driver.contract else None,
                            status=DRIVER_ACCEPTED, trip_workflow=slot.contract.trip_workflow_id,
                            planned_start_time=planned_start_time, hub=slot.hub_id)

    def create_services(self, *args, **kwargs):
        """
        Get all data required for generating subscription service
        """
        i_contract = kwargs.pop('contract', None)
        i_contract_id = kwargs.pop('contract_id', None)
        self.before_after_days = kwargs.pop('days', 0)

        contracts = None
        if i_contract and i_contract_id:
            return
        if i_contract:
            contracts = self.get_active_contracts(contract=i_contract)
        elif i_contract_id:
            contracts = self.get_active_contracts(contract_id=i_contract_id)
        else:
            contracts = self.get_active_contracts()

        created_by = User.objects.filter(
            email=settings.CRON_JOB_DEFAULT_USER).first()

        date_batch = timezone.now().date() + timedelta(days=self.before_after_days)
        current_datetime = timezone.now() + timedelta(days=self.before_after_days)
        desc_batch = date_batch.strftime('%d-%m-%Y')
        desc_batch = "Fixed Orders for  %s" % (desc_batch)
        batch = OrderBatch.objects.create(
            description=desc_batch, number_of_entries=0)
        rec_processed = 0
        for contract in contracts:
            # for each active contract
            if contract.contract_type != CONTRACT_TYPE_FIXED:
                print("Subscription Orders is for Fixed Contracts only (%s)" %
                      (contract))
                continue
            resources = self._get_resources_from_contract(contract)

            if not resources:
                continue
            for resource in resources:
                # for each hub in the contract
                pickup_datetime = self.find_pickup_time(
                    contract, resource.shift_start_time)

                drivers = resource.drivers.all()

                if not drivers:
                    continue

                for driver in drivers:
                    if driver.status != StatusPipeline.ACTIVE:
                        continue

                    if not pickup_datetime or not driver.driver_vehicle:
                        continue

                    if pickup_datetime > current_datetime + timedelta(
                        days=DAYS_AHEAD_ORDER):
                        self.create_scheduled_off_trip(driver, contract,
                                                       StatusPipeline.TRIP_SCHEDULED_OFF,
                                                       current_datetime.date() + timedelta(
                                                           days=1),
                                                       resource.hub, batch,
                                                       driver.contract)
                        continue

                    driver_resource = driver.resource_set.filter(
                        resourceallocation=resource).first()
                    route = driver_resource.route if driver_resource else None
                    # for each driver assigned to hub
                    print("Creating subscription order for contract(%s),"
                          "hub(%s), driver(%s), pickupat(%s)"
                          % (contract, resource.hub, driver, pickup_datetime))

                    # Don't create order if already there
                    # i.e. one subscription order per (contract, driver, hub,
                    # date)
                    """
                    # below code to restrict one fixed order per day per driver has been commented
                    now = datetime.now()
                    threshold_pickup_time = None
                    threshold_utc_time = TimeObject.time(18, 30, 00)
                    # if shift-start-time is before 5:30AM, when creating order,
                    # planned start time will fall in current date (UTC), which will results
                    # in validation failure and creation of duplicate orders for same date
                    if resource.shift_start_time >= threshold_utc_time:
                        threshold_pickup_time = now.replace(hour=18, minute=30, second=0, tzinfo=pytz.UTC)
                    else:
                        threshold_pickup_time = now + timedelta(1)

                    if Order.objects.filter(
                            customer_contract=contract,
                            driver=driver, hub=resource.hub,
                            order_type=settings.SUBSCRIPTION_ORDER_TYPE,
                            pickup_datetime__date__gte=threshold_pickup_time).exists():
                        print("Subscription Order already exists")
                        continue
                    """

                    order_processed, new_order = self.save_enterprise_order(
                        user=contract.customer.user,
                        pickup_datetime=pickup_datetime,
                        customer=contract.customer,
                        city=contract.city,
                        hub=resource.hub,
                        driver=driver,
                        invoice_driver=driver,
                        order_type=settings.SUBSCRIPTION_ORDER_TYPE,
                        contract=contract,
                        blowhorn_contract=driver.contract,
                        created_by=created_by,
                        batch=batch,
                        route=route,
                        division=contract.division,
                        device_type='Celery')
                    rec_processed += order_processed
        batch.end_time = timezone.now()
        batch.number_of_entries = rec_processed
        batch.save()

    def create_order_for_added_resource(self, contract, driver, resource, route=None):
        self.logger.info("generating fixed order for driver (%s) added in"
                         " resource allocation for contract (%s) "
                         % (driver, contract))
        # resource = ResourceAllocation.objects.filter(
        #    contract=contract, drivers=driver)
        if not resource:
            return

        """same logic from find_pickup_time() used here,
           find_pickup_time() was not called directly as here fixed order
           needs to be generated for the same day if today is a valid service day"""
        threshold_utc_time = TimeObject.time(18, 30, 00)
        day = timezone.now().replace(tzinfo=None) - timedelta(days=1)
        occurence = contract.service_schedule.after(day, inc=True)

        if not occurence:
            self.logger.info(
                "No service schedule found for fixed order generation")

            # Find out the order pickup time
        shift_start_time = dt.combine(occurence,
                                      resource.shift_start_time)

        if (shift_start_time.time() >= threshold_utc_time):
            shift_start_time = shift_start_time.replace(
                tzinfo=pytz.UTC) - timedelta(1)
        else:
            shift_start_time = shift_start_time.replace(tzinfo=pytz.UTC)

        after_hours_time = shift_start_time + \
                           timedelta(hours=contract.hours_after_scheduled_at)
        before_hours_time = shift_start_time - \
                            timedelta(hours=contract.hours_before_scheduled_at)
        now = timezone.now()

        # check if current time falls within trip before and after start time
        if now < before_hours_time or now > after_hours_time:
            self.logger.info("nothing to generate for today")
            return
        self.logger.info("generating order for today..")

        _request = current_request()
        current_user = _request.user if _request else ''

        self.save_enterprise_order(
            user=contract.customer.user,
            pickup_datetime=shift_start_time,
            customer=contract.customer,
            city=contract.city,
            hub=resource.hub,
            driver=driver,
            invoice_driver=driver,
            order_type=settings.SUBSCRIPTION_ORDER_TYPE,
            contract=contract,
            blowhorn_contract=driver.contract,
            created_by=current_user,
            route=route,
            device_type='Celery')

    def create_scheduled_off_trip(self, driver, contract, status, day, hub,
                                  batch, blowhorn_contract):
        # Check if Scheduled off trip exists for the day
        if Trip.objects.filter(driver=driver,
                               customer_contract=contract,
                               status=status,
                               planned_start_time=day).exists():
            print(
                "Scheduled off trip exists for contract(%s), driver(%s), day(%s)",
                (contract, driver, day))
            return
        # Create trip for the day
        TripMixin().create_trip(
            driver=driver,
            contract=contract,
            blowhorn_contract=blowhorn_contract,
            status=status,
            planned_start_time=day,
            hub=hub,
            isBatchUser=True,
            batch=batch)


class ContractUtils(object):
    """ All logical functions related to contract app should be here
    """

    def get_active_contracts(self):
        """
        :return: list of active contracts
        :
        """
        return [x for x in Contract.objects.filter(
            status=CONTRACT_STATUS_ACTIVE)]

    @staticmethod
    def get_contract(city, vehicle_class, contract_type=CONTRACT_TYPE_C2C):
        return Contract.objects.filter(
            status=CONTRACT_STATUS_ACTIVE,
            contract_type=contract_type,
            city=city,
            vehicle_classes__commercial_classification=vehicle_class,
        ).first()

    def get_c2c_contract(self, city, vehicle_class):
        """ Get the contracts for C2C order based on city and vehicle class"""
        contract = Contract.objects.filter(
            contract_type=CONTRACT_TYPE_C2C,
            city=city,
            vehicle_classes__commercial_classification=vehicle_class
        ).first()
        return contract

    def get_c2c_contracts(self, city):
        """
        :param city: instance of City (optional)
        :return: Queryset of C2CContract
        """
        if city:
            qs = Contract.objects.filter(
                city=city, contract_type=CONTRACT_TYPE_C2C
            ).order_by('vehicle_classes__vehicle_class_sequence')
        else:
            qs = Contract.objects.filter(
                contract_type=CONTRACT_TYPE_C2C
            ).order_by('vehicle_classes__vehicle_class_sequence')

        # Active contracts filter
        qs = qs.filter(status=CONTRACT_STATUS_ACTIVE)
        # qs = qs.filter(contractterm__wef__lte=datetime.now().date())  # Effective contracts filter
        qs = qs.select_related('city')
        qs = qs.prefetch_related('vehicle_classes')
        qs = qs.prefetch_related('contractterm_set')
        qs = qs.prefetch_related('contractterm_set__sell_rate')
        return qs.prefetch_related(
            'contractterm_set__sell_rate__sellvehicleslab_set')

    @staticmethod
    def get_contracts_for_cities(cities, contract_types=[], show_house_shifting_contract=False):
        """
        Returns the contracts queryset for given combination of
        `contract_types` and `cities`.
        """

        query = [Q(status=CONTRACT_STATUS_ACTIVE), Q(city__in=cities),
                 Q(is_house_shifting_contract=show_house_shifting_contract)]
        # query_contract_type = []
        if contract_types:
            query.append(Q(contract_type__in=contract_types))
        # for contract_type in contract_types:
        #     query_contract_type.append(Q(contract_type=contract_type))
        # if query_contract_type:
        #     query.append(reduce(operator.or_, query_contract_type))

        return Contract.objects.filter(reduce(operator.and_, query)). \
            prefetch_related('labourconfiguration', 'contractterm_set',
                             'contractterm_set__sell_rate__sellvasslab_set',
                             'contractterm_set__sell_rate__sellvehicleslab_set',
                             'vehicle_classes', 'customer__customerdivision',
                             ) \
            .select_related('city', 'customer') \
            .order_by('vehicle_classes__vehicle_class_sequence')

    @classmethod
    def get_active_customer_contract(cls, customer, **kwargs):
        parms = {
            'status' : CONTRACT_STATUS_ACTIVE,
            'customer' : customer
        }

        for key,value in kwargs.items():
            parms[key] = value

        result = Contract.objects.filter(**parms).exists()
        return result

    @classmethod
    def __get_contract_terms(cls, contracts):
        from blowhorn.contract.contract_helpers.contract_helper import \
            ContractHelper
        terms = []
        for contract in contracts:
            term = ContractHelper().get_contract_term(
                contract.contractterm_set.all(),
                timezone.now().date()
            )
            if term:
                terms.append(term.id)

        return ContractTerm.objects.filter(pk__in=terms) \
            .order_by('contract__vehicle_classes__vehicle_class_sequence', 'sell_rate__base_pay') \
            .select_related('contract', 'sell_rate') \
            .prefetch_related(
            Prefetch(
                'sell_rate__sellvehicleslab_set',
                queryset=SellVehicleSlab.objects.order_by('start'),
            ),
            Prefetch(
                'sell_rate__sellvasslab_set',
                queryset=SellVASSlab.objects.order_by('start')
            )
        )

    @classmethod
    def __get_vehicle_classes(cls, contracts):
        return VehicleClass.objects.filter(
            id__in=contracts.values_list('vehicle_classes', flat=True))

    @classmethod
    def __get_vehicle_class_alias(cls, cities):
        city_vehicle_class_alias_mapping = {}
        ciy_vehicle_class_alias = CityVehicleClassAlias.objects.filter(
            city__in=cities).values_list('city_id', 'vehicle_class_id',
                                         'alias_name')
        for (city_id, vehicle_class_id, alias_name) in ciy_vehicle_class_alias:
            city_vehicle_class_alias_mapping[
                str(city_id) + str(vehicle_class_id)] = alias_name
        return city_vehicle_class_alias_mapping

    @classmethod
    def __get_sorted_vehicle_classes(cls, order_expected, vehicle_classes):
        sorted_vehicle_classes = [0] * max(order_expected.values())
        for vehicle_class in vehicle_classes:
            if order_expected.get(vehicle_class.id, None):
                # Only considering the vehicle if sequence number is present.
                sorted_vehicle_classes[
                    order_expected.get(vehicle_class.id) - 1] = vehicle_class
                vehicle_classes = [vehicle_class for vehicle_class in
                                   sorted_vehicle_classes if vehicle_class]
        return vehicle_classes

    @classmethod
    def get_sell_packages(cls, cities, contract_types=[], customer=None,
                          is_authenticated=False, show_house_shifting_contract=False):
        """
        :param cities: queryset of cities
        :param contract_types: list of contract types
        :param customer: instance of Customer
        :param is_authenticated: boolean
        :return: contract package details given `contract_type` and `cities`.
        * Applicable only for Individuals and SME.
        """

        assert contract_types != [], "`contract_types` should be a valid " \
                                     "argument for this function."

        city_vehicle_class_alias_mapping = cls.__get_vehicle_class_alias(
            cities)
        result = []
        for city in cities:
            contracts = cls.get_contracts_for_cities([city], contract_types, show_house_shifting_contract)
            contract_terms = cls.__get_contract_terms(contracts)
            vehicle_classes = cls.__get_vehicle_classes(contracts)

            # Ordering the vehicle classes basis the contract sequence
            # TODO : This code can be optimized
            order_expected = dict(contracts.values_list(
                'vehicle_classes', 'vehicle_classes__vehicle_class_sequence'
            ).exclude(Q(vehicle_classes__vehicle_class_sequence=None) |
                      Q(vehicle_classes=None)))
            if order_expected:
                vehicle_classes = cls.__get_sorted_vehicle_classes(
                    order_expected, vehicle_classes)

            default_vehicle_class_id, default_customer_contract_id = \
                VehicleUtils().get_default_vehicle_class_contract(
                    customer=customer, city=city,
                    is_authenticated=is_authenticated)
            loading_time, unloading_time = \
                AddressUtil().get_loading_unloading_time(city=city)

            result.append(
                {
                    'city': city.name,
                    'city_id': city.id,
                    'vehicles': VehicleClassPackageSerializer(
                        vehicle_classes,
                        many=True,
                        context={
                            'contract_terms': contract_terms,
                            'city_vehicle_class_alias_mapping': city_vehicle_class_alias_mapping,
                            'city_id': city.id,
                            'default_vehicle_class_id': default_vehicle_class_id,
                            'default_contract_id': default_customer_contract_id,
                            'loading_mins': loading_time,
                            'unloading_mins': unloading_time
                        }
                    ).data
                }
            )
        return result[0] if len(result) == 1 else result


def get_payment_interval(reference_date, payment_cycle):
    """
    Returns the applicable payment interval for given `payment_cycle` and `reference_date`.
    """

    day = reference_date.day
    intervals = payment_cycle.paymentinterval_set.values_list(
        'start_day', 'end_day')
    for start, end in list(intervals):
        if end < start:
            days = list(range(start, 32)) + list(range(1, end + 1))
        else:
            days = list(range(start, end + 1))
        if day in days:
            if start <= end:
                date_start = reference_date.replace(day=start)

                last_day = calendar.monthrange(
                    reference_date.year, reference_date.month)[1]
                if end <= last_day:
                    date_end = reference_date.replace(day=end)
                else:
                    date_end = reference_date.replace(day=last_day)
            else:
                current_month = reference_date.month
                previous_month = current_month - 1 if current_month != 1 else 12
                previous_year = reference_date.year if current_month != 1 else reference_date.year - 1
                date_start, date_end = reference_date.replace(day=start, month=previous_month, year=previous_year), \
                                       reference_date.replace(day=end)
            return date_start, date_end
    return None, None


def get_per_day_amount(driver_base_payment_amount, base_payment_interval):
    if base_payment_interval == BASE_PAYMENT_CYCLE_PER_TRIP:
        amount_per_day = driver_base_payment_amount
    elif base_payment_interval == BASE_PAYMENT_CYCLE_DAILY:
        amount_per_day = driver_base_payment_amount
    elif base_payment_interval == BASE_PAYMENT_CYCLE_WEEK:
        amount_per_day = driver_base_payment_amount / 7
    elif base_payment_interval == BASE_PAYMENT_CYCLE_FORTNIGHTLY:
        amount_per_day = driver_base_payment_amount / 15
    elif base_payment_interval == BASE_PAYMENT_CYCLE_MONTH:
        amount_per_day = driver_base_payment_amount / 30
    return amount_per_day


def reminder_call_for_drivers():
    """
    Functionality to make calls to the driver from the selected resource
    allocation instaces based on the shift start time and remind driver field
    """
    if not settings.EXOTEL_SID:
        return

    current_time = dt.now()
    time_now = timezone.now()
    time_after_hlf_an_hr = current_time + timedelta(minutes=60)
    time_with_zone_after_an_hr = time_now + timedelta(minutes=60)
    logging.info("This is the current time : %s" % str(current_time))

    res_allc_in_hlf_hr = ResourceAllocation.objects.filter(remind_driver=True)
    res_allc_in_hlf_hr = res_allc_in_hlf_hr.exclude(drivers__isnull=True)
    res_allc_in_hlf_hr = res_allc_in_hlf_hr.filter(
        shift_start_time__gte=current_time, shift_start_time__lte=time_after_hlf_an_hr)
    res_allc_in_hlf_hr = res_allc_in_hlf_hr.filter(
        contract__trip__planned_start_time__gte=time_now,
        contract__trip__planned_start_time__lte=time_with_zone_after_an_hr,
        contract__trip__driver=F('drivers'))
    res_allc_in_hlf_hr = res_allc_in_hlf_hr.select_related(
        'contract', 'contract__city')
    res_allc_in_hlf_hr = res_allc_in_hlf_hr.prefetch_related(
        'drivers', 'drivers__user')
    res_allc_in_hlf_hr = res_allc_in_hlf_hr.values(
        'drivers__user__phone_number', 'exotel_app_id', 'contract__city__contact',
        'drivers__user__is_mobile_whitelisted')

    logging.info("These are the drivers : %s " % res_allc_in_hlf_hr)

    if res_allc_in_hlf_hr:
        logging.info("Has to call the drivers")
        for res_allc in res_allc_in_hlf_hr:
            dict_ = {
                'from_phone_number': re.compile(r'(\d{10})$').search(
                    res_allc['drivers__user__phone_number']).group(1),
                'caller_id': re.compile(r'(\d{10})$').search(
                    res_allc['contract__city__contact']).group(1) if res_allc.get(
                    'contract__city__contact', None) else None,
                'app_id': res_allc['exotel_app_id']

            }

            if not res_allc.get('drivers__user__is_mobile_whitelisted', False):
                try:
                    resp = ExotelCall().number_whitelisting(
                        dict_['from_phone_number'])
                except ValidationError as e:
                    logging.info(
                        "This is the error while whitelisting the number : %s" % e)
                    continue

                if not resp.get('status', False):
                    logging.info(
                        "This is the error while whitelisting the number : %s" % e)
                    continue

            try:
                response = ExotelCall().call_flow(input_data=dict_,
                                                  request=None)
            except Exception as e:
                logging.info(
                    "This is the error while calling : %s" % e.args[0])
                continue

            logging.info('Below is the response %s' % (response.content,))

            if response and response.status_code == 200:
                response_content = response.json()
                call_response = response_content.get("Call", {})
                try:
                    created_timestamp = ist_to_utc(datetime.strptime(
                        call_response.get("StartTime"), "%Y-%m-%d %H:%M:%S"))
                except:
                    created_timestamp = dt.now()

                call_data = {
                    "sid": call_response.get("Sid"),
                    "created_timestamp": created_timestamp,
                    "client_number": re.compile(r'(\d{10})$').search(
                        call_response.get("From")).group(1),
                    "agent_number": re.compile(r'(\d{10})$').search(
                        call_response.get("To")).group(1),
                    "recipient": DRIVER,
                    "call_type": OUTGOING
                }
                CallDetail.objects.create(**call_data)
            else:
                continue


def avg_duty_hrs_for_contract():
    """
    Find avg duty hrs for each active contract based on trips completed
    """

    Contract.objects.filter(status=CONTRACT_STATUS_ACTIVE).update(
        avg_duty_hrs=Subquery(
            Trip.objects.filter(status=StatusPipeline.TRIP_COMPLETED,
                                customer_contract=OuterRef('id')).values(
                'customer_contract').annotate(hrs=Avg('total_time') / 60).values(
                'hrs')[:1]))


from blowhorn.contract.models import *
from blowhorn.trip.models import *


# from django.db.models import Sum


def getStaticMargin(contract_id):
    # contract = Contract.objects.filter(id=contract_id)#all().values_list('id')
    contract_terms = ContractTerm.objects.filter(contract_id=contract_id)

    if contract_terms.exists():
        trips = Trip.objects.filter(customer_contract_id=contract_id,
                                    status=TRIP_COMPLETED)
        # print('Total Distance ',x.aggregate(total=Sum('total_distance')))
        tripCount = trips.count()
        dates = trips.distinct('planned_start_time__date').count()
        buy_rate = ContractTerm.objects.filter(contract_id=contract_id).latest(
            'wef').buy_rate
        sell_rate = ContractTerm.objects.filter(contract_id=contract_id).latest(
            'wef').sell_rate
        if buy_rate is None:
            return ''
        if sell_rate is None:
            return ''
        baseBuyInt = buy_rate.base_payment_interval
        baseSellInt = sell_rate.base_pay_interval
        # ASSUMPTION MADE THAT 1 TRIP PER 1 DAY ##### ALL CONVERT TO MONTH #########
        if baseBuyInt == 'Daily':
            factorB = 30  # dates
        elif baseBuyInt == 'Weekly':
            factorB = 4
        elif baseBuyInt == 'Per-Trip':
            factorB = 30  # tripCount
        elif baseBuyInt == 'Month':
            factorB = 1
        elif baseBuyInt == 'FortNightly':
            factorB = 2
        if baseSellInt == 'Daily':
            factorS = 30  # dates
        elif baseSellInt == 'Weekly':
            factorS = 4
        elif baseSellInt == 'Per-Trip':
            factorS = 30  # tripCount
        elif baseSellInt == 'Month':
            factorS = 1
        elif baseSellInt == 'FortNightly':
            factorS = 2

        # today = timezone.now().date()
        # startDay = ContractTerm.objects.filter(contract_id=contract_id).earliest('wef').wef
        # for i in contracts:
        #     x1=Trip.objects.filter(customer_contract_id = i)

        # dates1=trips.distinct('planned_start_time__date').count()
        if factorB == tripCount or factorB == dates:
            baseBuy1 = float(
                ContractTerm.objects.filter(contract_id=contract_id).latest(
                    'wef').buy_rate.driver_base_payment_amount or 0) * factorB
        else:
            baseBuy1 = float(
                ContractTerm.objects.filter(contract_id=contract_id).latest(
                    'wef').buy_rate.driver_base_payment_amount or 0) * factorB  # ((today-startDay).days/factorB)
        if factorS == tripCount or factorS == dates:
            baseSell1 = float(
                ContractTerm.objects.filter(contract_id=contract_id).latest(
                    'wef').sell_rate.base_pay or 0) * factorS
        else:
            baseSell1 = float(
                ContractTerm.objects.filter(contract_id=contract_id).latest(
                    'wef').sell_rate.base_pay or 0) * factorS  # ((today-startDay).days/factorS)
        if baseSell1:
            margin1 = ((baseSell1 - baseBuy1) / baseSell1) * 100
        else:
            margin1 = 0
        # print('Margin ',margin1)
        return round(margin1, 3)
    return ''


def getOperMargin(contract_id):
    trips = Trip.objects.filter(customer_contract_id=contract_id,
                                status=TRIP_COMPLETED)
    tripCount = trips.count()
    if not tripCount:
        return 'NA'
    else:
        return (tripCount, ' LATER')


def getSlabMargin(contract_id):
    # fOR 1 TRIP MAKE A DIALOG BOX GIVING THE LIST
    return '1-trip'


def getSlabMarginTwo(contract_id):
    # FOR 2 TRIPS MAKE A DIALOG BOX GIVING  THE  LIST
    return '2-trip'


def getSlabMarginThree(contract_id):
    return '3+trips'
