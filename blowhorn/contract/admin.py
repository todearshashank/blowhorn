# System and Django Libraries
from collections import defaultdict
from datetime import datetime, timedelta
from functools import partial
from django.contrib import admin, messages
from django.contrib.admin import SimpleListFilter
from django.db.models import Q, Count, Prefetch, F
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
from django.contrib.postgres.aggregates import StringAgg, ArrayAgg

# Third party libraries
import nested_admin
import os
import csv

# Blowhorn project libraries
from .models import CONTRACT_STATUS_ACTIVE
from .models import (
    Contract,
    ServiceHSNCodeMap,
    PaymentInterval,
    PaymentCycle,
    ContractConfiguration,
    ResourceAllocation,
    Resource,
    SellRate,
    SellRateHistory,
    SellShipmentSlab,
    SellPincodeShipmentSlab,
    SellVehicleSlab,
    SellDimensionSlab,
    SellVASSlab,
    SellNTSlab,
    BuyRate,
    BuyRateHistory,
    BuyShipmentSlab,
    BuyVehicleSlab,
    BuyDimensionSlab,
    BuyVASSlab,
    BuyNTSlab,
    ResourceAllocationHistory,
    ContractTerm,
    ServiceScheduleHistory,
    C2CContract,
    BlowhornContract,
    SMEContract,
    TripWorkFlow,
    Screen,
    ScreenField,
    ScreenButton,
    LabourConfiguration,
    KioskLabourConfiguration,
    ItemCategory,
    KioskContract,
    FlashSaleContract,
    VASAttribute,
    WorkType
)
from blowhorn.common.admin import BaseAdmin
from blowhorn.customer.utils import expected_margin
from blowhorn.customer.models import Customer
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL
from blowhorn.address.models import Hub, City
from blowhorn.driver.models import Driver, DriverPayment
from blowhorn.driver.models import APPROVED
from config.settings import status_pipelines as StatusPipeline
from blowhorn.users.models import User
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from blowhorn.contract.permissions import is_superuser_or_supervisor, \
    is_citymanager, is_spoc
from blowhorn.utils.functions import ist_to_utc
from blowhorn.contract.constants import (
    CONTRACT_TYPE_FIXED, CONTRACT_STATUS_DRAFT, CONTRACT_TYPE_C2C,
    CONTRACT_TYPE_SME, CONTRACT_TYPE_BLOWHORN, CONTRACT_TYPE_KIOSK,
    CONTRACT_TYPE_FLASH_SALE, CONTRACT_TYPE_ONDEMAND,
    CONTRACT_PERMISSIONS)
from blowhorn.contract.forms import (
    PaymentIntervalFormSet,
    ResourceFormSet,
    ContractEditFormSales,
    ContractFormSales,
    ContractEditFormOperation,
    ContractTermForm,
    ContractForm,
    BuyRateForm,
    SellRateForm,
    SlabForm,
    C2CContractForm,
    BlowhornContractForm,
    SMEContractForm,
    ContractEditForm,
    ResourceAllocationHistoryFormSet,
    KioskContractForm,
    ServiceHSNCodeMapForm
)
from blowhorn.driver.tasks import generate_driver_payment
from blowhorn.contract.resource import ContractResource
from blowhorn.customer.models import CustomerInvoice, Route
from blowhorn.common.tasks import send_zippped_consignment_notes, update_cms
from blowhorn.trip.models import Trip
from blowhorn.utils.widgets.url_link import URLLink
from blowhorn.utils.functions import utc_to_ist
from blowhorn.users.mixins import AuthMixin

DATE_FORMAT = '%d-%b-%Y'
BASE_PAYMENT_INTERVAL = 'Daily'


class MarginFilter(SimpleListFilter):
    title = _('Margin')
    parameter_name = 'expected_margin'

    def lookups(self, request, model_admin):
        return [
            ('0', 'less than 0'),
            ('0&5', '0 to 5'),
            ('5&10', '5 to 10'),
            ('10&15', '10 to 15'),
            ('15', 'greater than 15')
        ]

    def queryset(self, request, queryset):
        if self.value():
            val = [int(x) for x in self.value().split('&')]
            if len(val) == 1:
                if val[0] <= 0:
                    return queryset.filter(expected_margin__lt=int(self.value()))
                elif len(val) == 1 and val[0] > 0:
                    return queryset.filter(expected_margin__gte=int(self.value()))
            else:
                return queryset.filter(Q(expected_margin__gte=val[0]) &
                                       Q(expected_margin__lt=val[1]))


class ContractFilter(SimpleListFilter):
    title = _('Contract')
    parameter_name = 'contract'

    def lookups(self, request, model_admin):
        cities = City.objects.get_cities_for_managerial_user(request.user)
        contracts = Contract.objects.exclude(status=CONTRACT_STATUS_DRAFT)
        if not request.user.is_superuser:
            contracts = contracts.filter(
                Q(spocs=request.user) | Q(supervisors=request.user) |
                Q(city__in=cities)
            ).distinct()
        return [(c.id, c) for c in contracts]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(customer_contract=self.value())


class PaymentIntervalAdminInline(admin.TabularInline):
    model = PaymentInterval
    extra = 0
    formset = PaymentIntervalFormSet


class PaymentCycleAdmin(admin.ModelAdmin):
    model = PaymentCycle
    extra = 0
    inlines = [PaymentIntervalAdminInline, ]
    list_display = ('name', 'cycle_info')

    search_fields = ['name']

    def cycle_info(self, obj):
        return ', '.join([str(c) for c in
                          PaymentInterval.objects.filter(cycle=obj.id)])


admin.site.register(PaymentCycle, PaymentCycleAdmin)


class ResourceAllocationHistoryAdminInline(admin.TabularInline):
    model = ResourceAllocationHistory
    extra = 0
    formset = ResourceAllocationHistoryFormSet
    readonly_fields = ('event_time', 'driver', 'action', 'modified_by', )
    list_display = readonly_fields
    ordering = ('-event_time', )

    def get_queryset(self, request):
        qs = super(ResourceAllocationHistoryAdminInline,
                   self).get_queryset(request)
        qs = qs.select_related('driver')
        qs = qs.select_related('modified_by')
        return qs

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ServiceScheduleHistoryAdminInline(admin.TabularInline):
    model = ServiceScheduleHistory
    extra = 0
    readonly_fields = (
        'old_value', 'new_value', 'modified_date', 'modified_by',)
    list_display = readonly_fields
    ordering = ('-modified_date', )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ResourceInline(nested_admin.NestedTabularInline):
    model = Resource
    extra = 0
    readonly_fields = ['route']

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(ResourceInline,
                          self).formfield_for_dbfield(db_field, **kwargs)

        if db_field.name == "driver":
            formfield.queryset = ResourceAllocationAdmin(Contract,
                                                         self.admin_site).driver_qs

        elif db_field.name == "route":
            formfield.queryset = ResourceAllocationAdmin(Contract,
                                         self.admin_site).route_qs

        return formfield


class ResourceAllocationAdminInline(nested_admin.NestedTabularInline):
    model = ResourceAllocation
    extra = 0
    fields = ('hub', 'shift_start_time', 'unplanned_additional_drivers')
    # filter_horizontal = ('drivers', )
    # form = ResourceAllocationInlineForm
    formset = ResourceFormSet
    inlines = [ResourceInline]

    def get_formset(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield,
                                               request=request, obj=obj)
        return super(
            ResourceAllocationAdminInline, self
        ).get_formset(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        contract = kwargs.pop('obj', None)
        formfield = super(ResourceAllocationAdminInline,
                          self).formfield_for_dbfield(db_field, **kwargs)

        customer_id = contract.customer_id
        if customer_id and db_field.name == "hub":
            ''' Show only hubs for this customer '''
            formfield.queryset = Hub.objects.filter(customer_id=customer_id)

        return formfield


class ResourceAllocationAdmin(nested_admin.NestedModelAdmin):
    list_display = (
        'name', 'description', 'division', 'city', 'customer', 'active_drivers',
        'unplanned_additional_drivers',
    )
    fields = ('name', 'description', 'city', 'division','remind_driver', 'exotel_app_id')
    readonly_fields = ('name', 'description', 'city', 'division',)
    list_select_related = ('city', 'customer', 'division')
    list_filter = ('name', 'description',
                   ('city', admin.RelatedOnlyFieldListFilter),
                   ('customer', admin.RelatedOnlyFieldListFilter))
    inlines = [ResourceAllocationAdminInline]

    def active_drivers(self, obj):
        return obj.active_drivers

    def unplanned_additional_drivers(self, obj):
        resource = obj.resourceallocation_set.all()
        count = 0
        for res in resource:
            count += res.unplanned_additional_drivers
        return count

    active_drivers.short_description = _('Active Drivers')

    # Resource allocation allowed for only active contracts
    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super(ResourceAllocationAdmin, self).get_queryset(request)
        qs = qs.filter(status=CONTRACT_STATUS_ACTIVE,
                       contract_type=CONTRACT_TYPE_FIXED)
        if not request.user.is_superuser:
            cities = City.objects.get_cities_for_managerial_user(request.user)
            qs = qs.filter(
                Q(city__in=cities) |
                Q(spocs=request.user) |
                Q(supervisors=request.user)
            )
        qs = qs.select_related('city', 'customer', 'division')
        qs = qs.prefetch_related('resourceallocation_set')
        qs = qs.annotate(
            active_drivers=Count('resourceallocation__drivers', distinct=True))
        return qs

    def __get_driver_qs(self, contract):
        return Driver.objects.filter(
            operating_city_id=contract.city_id,
            status=StatusPipeline.ACTIVE,
            vehicles__vehicle_model__vehicle_class__in=contract.vehicle_classes.all(),
            vehicles__body_type__in=contract.body_types.all()
        ).distinct('pk')

    def __get_route_qs(self, customer_id):
        return Route.objects.filter(
                customer_id=customer_id).select_related(
                'customer').distinct('pk')

    def get_object(self, request, object_id, from_field=None):
        contract = super().get_object(request, object_id, from_field=None)
        if contract:
            setattr(ResourceAllocationAdmin, 'driver_qs',
                    self.__get_driver_qs(contract) or Driver.objects.none())
            setattr(ResourceAllocationAdmin, 'route_qs', self.__get_route_qs(
                contract.customer_id) or Route.objects.none())
        return contract

    def get_formset(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield,
                                               request=request, obj=obj)
        return super(
            ResourceAllocationAdmin, self
        ).get_formset(request, obj, **kwargs)

    def save_formset(self, request, form, formset, change):
        for form in formset.forms:
            if hasattr(form.instance, 'shift_start_time'):
                shift_start_time = form.instance.shift_start_time
                current_date = timezone.now()
                current_date = datetime.combine(current_date, shift_start_time)
                current_date = ist_to_utc(current_date)
                form.instance.shift_start_time = current_date.time()
        formset.save()


class ContractResourceView(Contract):

    class Meta:
        proxy = True
        verbose_name = _('Resource Allocation')
        verbose_name_plural = _('Resource Allocations')


# admin.site.register(ContractResourceView, ResourceAllocationAdmin)


def ContractTermInline(is_c2c, blowhorn_contract=None):
    class ContractTermAdminInline(admin.TabularInline):
        list_display = ('wef', 'sell_rate', 'buy_rate', 'static_margin', 'marg_trip')
        fields = ('wef', 'sell_rate', 'buy_rate', 'first_mile_buy_rate', 'middle_mile_buy_rate',
                  'static_margin', 'marg_trip')
        model = ContractTerm
        extra = 0
        formset = ContractTermForm(is_c2c, blowhorn_contract=blowhorn_contract)
        list_select_related = ('sell_rate', 'buy_rate',)
        readonly_fields = ('static_margin', 'marg_trip',)

        def static_margin(self, obj):
            fetch = None
            if obj and obj.contract_id:
                from .utils import getStaticMargin
                fetch = getStaticMargin(obj.contract_id)
            return fetch
        '''
        def oper_margin(self,obj):
            x=None
            if obj and obj.contract_id:
                from .utils import  getOperMargin
                x= getOperMargin(obj.contract_id)
            return x
        '''

        def marg_trip(self,obj):
            template="""
                <div><center><button type='button' onclick='toShowGraph(%s)'>Custom Margin</button></center></div>
                <div><center><button type='button' onclick='datasetGraph(%s)'>Dataset Margin</button></center></div>
            """ % (obj.contract_id, obj.contract_id)
            return format_html(template)

        static_margin.short_description = _("Expected Margin")
        # oper_margin.short_description='DUMMY'
        marg_trip.short_description = _("n-Trip-Margins")
        marg_trip.allow_tag = True

        def get_readonly_fields(self, request, obj=None):
            if obj and not obj.is_inter_city_contract:
                self.readonly_fields += ('first_mile_buy_rate', 'middle_mile_buy_rate',)
            if obj and (not is_superuser_or_supervisor(obj, request.user) and
                        not is_citymanager(obj, request.user) and not is_spoc(
                            obj, request.user)
                        and obj.created_by != request.user):
                return self.readonly_fields + self.list_display
            return self.readonly_fields

        def has_delete_permission(self, request, obj=None):
            return False

    return ContractTermAdminInline


def generate_payments(modeladmin, request, queryset):
    """
    Custom Action Button for generation of driver payments
    """
    ids = list(queryset.values_list('id', flat=True))
    generate_driver_payment.apply_async(contract_list=ids)
    messages.info(request, 'Generating payments for selected contract.'
                           'Wait for sometime and goto Payment Batches')


generate_payments.short_description = _('Generate Driver Payments')


def generate_zipped_consignment_notes(modeladmin, request, queryset):
    contract_ids = []
    customer_name = []
    try:
        email = request.user.email
    except:
        messages.error(
            request,
            ("Please login via valid account")
        )
        return

    contracts = queryset.values('id', 'customer__name')

    for contract in contracts:
        contract_ids.append(contract.get('id'))
        if contract.get('customer__name'):
            customer_name.append(contract['customer__name'])

    customer_name = set(customer_name)

    trips = Trip.objects.filter(
        customer_contract_id__in=contract_ids, has_consignment_note=True,
        consignment_batch_id__isnull=False).values(
        'trip_number', 'consignment_trip__content')

    if not trips:
        messages.error(
            request,
            ("Selected contracts have no Trips with consignment note")
        )
        return

    send_zippped_consignment_notes.apply_async((list(trips),
                                                email, list(customer_name)),)

    messages.success(
        request, ("Consignment Notes will be mailed to you"))


generate_zipped_consignment_notes.short_description = _('Get Zipped '
                                                        'Consignment Notes')


# def generate_invoices(modeladmin, request, queryset):
#     """
#     Custom Action Button for generation of customer Invoices
#     """
#
#     generate_customer_invoices.delay(
#         contracts=list(queryset.values_list('id', flat=True)))
#
#
# generate_invoices.short_description = _('Generate Customer Invoices')

def contractterm_inline(is_c2c, blowhorn_contract=None):
    class ContractTermAdminInline(admin.TabularInline):
        list_display = ('wef', 'buy_rate', 'first_mile_buy_rate', 'middle_mile_buy_rate',
                        'sell_rate','static_margin', 'marg_trip')
        fields = ('wef', 'buy_rate', 'sell_rate', 'first_mile_buy_rate', 'middle_mile_buy_rate',
                  'static_margin', 'marg_trip')
        model = ContractTerm
        extra = 0
        formset = ContractTermForm(is_c2c, blowhorn_contract=blowhorn_contract)
        list_select_related = ('sell_rate', 'buy_rate',)
        readonly_fields = ('static_margin', 'marg_trip',)

        def static_margin(self, obj):
            fetch = None
            if obj and obj.contract_id:
                from .utils import getStaticMargin
                fetch = getStaticMargin(obj.contract_id)
            return fetch

        def marg_trip(self, obj):
            template = """
                <div><center><button type='button' onclick='toShowGraph(%s)'>Custom Margin</button></center></div>
                <div><center><button type='button' onclick='datasetGraph(%s)'>Dataset Margin</button></center></div>
            """ % (obj.contract_id, obj.contract_id)
            return format_html(template)

        static_margin.short_description = _("Expected  Margin")
        marg_trip.short_description = _("n-Trip-Margins")
        marg_trip.allow_tag = True

        def has_delete_permission(self, request, obj=None):
            return False

        def get_formset(self, request, obj=None, **kwargs):
            kwargs['formfield_callback'] = partial(self.formfield_for_dbfield,
                                                   request=request)
            return super().get_formset(request, obj, **kwargs)

        def get_readonly_fields(self, request, obj=None):
            if obj and not obj.is_inter_city_contract:
                self.readonly_fields += ('first_mile_buy_rate', 'middle_mile_buy_rate',)
            if obj and (not is_superuser_or_supervisor(obj, request.user) and
                        not is_citymanager(obj, request.user) and not is_spoc(
                            obj, request.user) and not (request.user.is_sales_representative(obj.city))
                        and obj.created_by != request.user):
                return self.readonly_fields + self.list_display
            return self.readonly_fields

        def formfield_for_dbfield(self, db_field, **kwargs):
            request = kwargs.get('request', None)
            if db_field.name == 'buy_rate' and \
                    not AuthMixin().has_model_permissions(
                        request.user, BuyRate, CONTRACT_PERMISSIONS, 'contract'):
                kwargs['widget'] = URLLink(
                    attrs={'class': db_field.name},
                    url_template='/admin/contract/buyrate/%s')

            if db_field.name == 'sell_rate' and \
                not AuthMixin().has_model_permissions(
                    request.user, SellRate, CONTRACT_PERMISSIONS, 'contract'):
                kwargs['widget'] = URLLink(
                    attrs={'class': db_field.name},
                    url_template='/admin/contract/sellrate/%s')
            return super().formfield_for_dbfield(db_field, **kwargs)

    return ContractTermAdminInline


class ServiceHSNCodeMapAdmin(admin.ModelAdmin):
    form = ServiceHSNCodeMapForm
    list_display = ('service_tax_category', 'hsn_code', 'is_active')
    search_fields = ['hsn_code']
    list_filter = ('service_tax_category', )


class BaseContractAdmin(FSMTransitionMixin, BaseAdmin):
    resource_class = ContractResource
    fsm_field = ['status', ]
    save_as = True
    save_on_top = True
    list_display = (
        'name', 'get_expected_margin', 'description', 'contract_type', 'customer', 'hsn_code',
        'division', 'city', 'status', 'get_vehicle_classes', 'get_body_types',
        'get_term_details', 'service_tax_category', 'number_of_working_days', 'work_type')

    list_select_related = ('customer', 'city',)
    filter_horizontal = ('vehicle_classes', 'body_types', 'supervisors',
                         'spocs')
    inlines = [contractterm_inline(is_c2c=False)]
    list_filter = ('city', 'status', 'contract_type', MarginFilter)
    search_fields = ['contract_type', 'customer__user__name', 'city__name',
                     'status', 'name', 'description']
    actions = [generate_payments, generate_zipped_consignment_notes, 'export_contract_data']

    def get_expected_margin(self, obj):
        return expected_margin(obj.expected_margin)

    get_expected_margin.short_description = 'Expected Margin'
    get_expected_margin.admin_order_field = 'expected_margin'

    def get_actions(self, request):
        """
        Remove `Delete` and Add `Generate Payments`
        """
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        if not request.user.is_superuser:
            del actions['generate_payments']
        return actions

    def has_delete_permission(self, request, obj=None):
        """
        Only draft contract can be deleted
        """
        return obj and obj.status == CONTRACT_STATUS_DRAFT

    def get_service_schedule(self, obj):
        text_rule = 'Inclusion rule:'
        for rule in obj.service_schedule.rrules:
            rule = rule.to_text()
            if rule.startswith('weekly') or \
                rule.startswith('daily') or rule.startswith('monthly') or \
                    rule.startswith('annually'):
                text_rule += '\n'
            text_rule += '%s ' % rule
        text_rule += '\nExclusion rule:'
        for rule in obj.service_schedule.exrules:
            rule = rule.to_text()
            if rule.startswith('weekly') or rule.startswith('daily') or \
                    rule.startswith('monthly') or rule.startswith('annually'):
                text_rule += '\n'
            text_rule += '%s ' % rule
        return text_rule

    get_service_schedule.short_description = 'Service Calendar'

    def next_30_day_occurrences(self, obj):
        """
        Method to show the order auto-generation calender for the next 30 days
        """
        if not obj:
            return ''

        occurrences = defaultdict(list)
        distinct_months = []
        if not obj.service_schedule:
            return ''

        for o in obj.service_schedule.between(
            datetime.now(), datetime.now() + timedelta(days=31), inc=True
        ):
            month = o.strftime('%B')
            distinct_months.append(
                month) if month not in distinct_months else None
            occurrences[month].append(o)

        return_value = ''
        for month, days in occurrences.items():
            if not days:
                continue
            return_value += '%9s: Mon Tue Wed Thu Fri Sat Sun\n' % month

            # For the days which are not present, fill with blanks so that
            # all days of the week are present before dumping as text
            padded_days = []
            start_day = days[0].day - days[0].isoweekday()
            for n in days:
                padded_days.extend(['-'] * (n.day - start_day - 1))
                padded_days.append(n.day)
                start_day = n.day

            # Dump 1 week at a time (7 days at a time)
            while padded_days:
                week_data = padded_days[:7]
                return_value += '%9s  %s\n' % ('',
                                               ' '.join(['%3s' % n for n in
                                                         week_data]))
                padded_days = padded_days[7:]

            return_value += '\n'

        return format_html('<pre>%s</pre>' % return_value)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)

        if db_field.name == 'hsn_code':
            kwargs['queryset'] = ServiceHSNCodeMap.objects.filter(is_active=True)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name in ('supervisors', 'spocs'):
            kwargs['queryset'] = User.objects.filter(
                is_staff=True, is_active=True
            ).exclude(email__icontains='blowhorn.net')
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def get_vehicle_classes(self, obj):
        return ', '.join([str(x) for x in obj.vehicle_classes.all()])

    get_vehicle_classes.short_description = _('Vehicle Class(es)')

    def get_body_types(self, obj):
        return ', '.join([str(x) for x in obj.body_types.all()])

    get_body_types.short_description = _('Body Type(s)')

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_term_details(self, obj):
        terms_sorted = sorted(obj.contractterm_set.all(), key=lambda x: x.wef)
        terms_before = [
            term for term in terms_sorted if term.wef <= timezone.now().date()]
        terms_after = [
            term for term in terms_sorted if term.wef > timezone.now().date()]

        wef_qs = None
        if terms_before:
            wef_qs = terms_before[-1]
        elif terms_after:
            wef_qs = terms_after[0]

        if wef_qs:
            buy_rate_name = '-'
            if wef_qs.buy_rate:
                buy_rate_name = wef_qs.buy_rate.name

            sell_rate_name = '-'
            if wef_qs.sell_rate:
                sell_rate_name = wef_qs.sell_rate.name
            return format_html("{} <b>|</b> {} <b>|</b> {}",
                               wef_qs.wef.strftime(DATE_FORMAT),
                               buy_rate_name, sell_rate_name)

        return ''

    get_term_details.short_description = _('WEF | BuyRate | SellRate')

    def export_contract_data(self, request, queryset):

        ids = queryset.values_list('id', flat=True)
        queryset = Contract.copy_data.filter(id__in=ids)

        file_path = 'contract_details'

        queryset.annotate(
            wef=F('contractterm__wef'),
            buy_rate=F('contractterm__buy_rate__name'),
            sell_rate=F('contractterm__sell_rate__name'),
            get_vehicle=ArrayAgg('vehicle_classes__commercial_classification',
                                  delimiter=',',
                                  distinct=True),
            body_type=ArrayAgg('body_types__body_type', delimiter=',', distinct=True)

        ).to_csv(
            file_path,
            'name', 'expected_margin', 'description', 'contract_type', 'customer__legalname', 'division__name',
            'city__name', 'status', 'get_vehicle', 'body_type', 'wef', 'buy_rate', 'sell_rate',
            'service_tax_category', 'number_of_working_days', 'work_type'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow(['Contract Name', 'Expected Margin', 'Description', 'Contract Type', 'Customer',
                        'Customer Division', 'City', 'Status', 'Vehicle Class(es)', 'Body Type(s)',
                        'WEF', 'BuyRate', 'SellRate', 'GTA/BSS', 'No. of working days', 'Work Type'
                        ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response

    export_contract_data.short_description = 'Export Contract Details in excel'

    class Media:
        css = {
            'all': ('/static/admin/css/custom_contract.css',
                    '/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/contract_admin.js',
              '/static/admin/js/contract_confirmation_pop_up.js',
              '/static/admin/js/contract/marginGraph.js',
              '/static/admin/js/contract/margin_dataset_graph.js',
              )


class ContractAdmin(BaseContractAdmin):
    inlines = [ContractTermInline(is_c2c=False),
               ResourceAllocationHistoryAdminInline,
               ServiceScheduleHistoryAdminInline]

    list_filter = ('city', 'status', 'contract_type', 'is_wms_contract', 'is_inter_city_contract', 'hsn_code')
    search_fields = ['contract_type', 'customer__user__name', 'city__name',
                     'status', 'name', 'description']
    actions = [generate_payments, generate_zipped_consignment_notes, 'export_contract_data']
    form = ContractForm

    def get_fieldsets(self, request, obj=None):
        user = request.user
        if is_superuser_or_supervisor(obj, user) or obj is None:
            fieldsets = (
                (None, {
                    'fields': ('contract_type', 'name', 'description',
                               'status', 'customer', 'division', 'city', 'source_city',
                               'vehicle_classes', 'body_types', 'is_wms_contract', 'is_inter_city_contract',
                               'service_schedule', 'next_30_day_occurrences',
                               'payment_cycle', 'trip_noshow_hrs', 'expected_delivery_hours',
                               'service_tax_category', 'hsn_code', 'avg_basket_value',
                               'is_pod_applicable', 'use_customer_hub',
                               'hours_before_scheduled_at', 'avg_basket_value',
                               'hours_after_scheduled_at', 'item_categories',
                               'additional_info', 'number_of_working_days', 'work_type')
                }),)
        else:
            fieldsets = (
                (None, {
                    'fields': ('contract_type', 'description', 'name',
                               'status', 'customer', 'division', 'city', 'source_city',
                               'vehicle_classes', 'body_types', 'is_wms_contract', 'is_inter_city_contract',
                               'get_service_schedule', 'expected_delivery_hours',
                               'next_30_day_occurrences', 'payment_cycle',
                               'trip_noshow_hrs', 'service_tax_category',
                               'hsn_code', 'hours_before_scheduled_at', 'avg_basket_value',
                               'hours_after_scheduled_at', 'item_categories',
                               'additional_info', 'number_of_working_days', 'work_type')
                }),)

        fieldsets += (
            ('Trip WorkFlow', {
                'fields': ('trip_workflow',),
            }),
            ('Permissions', {
                'fields': ('supervisors', 'spocs', ),
            }),
            ('Audit Log', {
                'fields': ('created_date', 'created_by', 'modified_date',
                           'modified_by'),
            }),
        )

        return fieldsets

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = ContractEditForm
        else:
            self.form = ContractForm
        return super().get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        """
        Don't allow edit of `city` and `customer` for edit as contract name
        depends on it. `name` should never be allowed to edit
        """
        user = request.user
        if obj:  # edit
            read_only = [
                'name', 'customer', 'city', 'job_last_run_time', 'status',
                'created_date', 'created_by', 'modified_date', 'modified_by',
                'contract_type', 'next_30_day_occurrences',
                'get_service_schedule']
            if obj.status == CONTRACT_STATUS_ACTIVE:
                read_only.extend(['payment_cycle', 'work_type', 'number_of_working_days', 'is_wms_contract',
                                  'is_inter_city_contract', 'source_city'])

            if not is_superuser_or_supervisor(obj, user) and not \
                    is_citymanager(obj, user):
                read_only += ['supervisors', 'spocs',
                              'body_types', 'vehicle_classes',
                              'payment_cycle', 'trip_noshow_hrs',
                              'additional_info']
        else:
            read_only = [
                'name', 'job_last_run_time',
                'created_date', 'created_by',
                'modified_date', 'modified_by',
                'status', 'next_30_day_occurrences', 'get_service_schedule'
            ]

        if not user.is_sales_representative(obj.city if obj else None):
            read_only += ['description']

        return read_only

    def get_queryset(self, request):
        """ Display contracts if user is either creator/supervisor/spoc of
            that particular contract
        """
        user = request.user
        qs = super(ContractAdmin, self).get_queryset(request)
        qs = qs.filter(customer__isnull=False)
        qs = qs.prefetch_related('contractterm_set')
        qs = qs.prefetch_related('contractterm_set__buy_rate')
        qs = qs.prefetch_related('contractterm_set__sell_rate')
        qs = qs.prefetch_related('vehicle_classes', 'spocs', 'supervisors')
        qs = qs.prefetch_related('body_types')
        qs = qs.select_related('created_by', 'modified_by')
        qs = qs.select_related('payment_cycle')

        if not user.is_superuser:
            cities = City.objects.get_cities_for_managerial_user(request.user)
            qs = qs.filter(Q(supervisors=user) | Q(
                spocs=user) | Q(created_by=user) | Q(city__in=cities))
            qs = qs.distinct()
        return qs

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        if object_id:
            user = request.user
            contract = Contract.objects.get(pk=object_id)
            if not is_superuser_or_supervisor(contract, user) and not \
               is_citymanager(contract, user) and contract.created_by != user:
                extra_context['show_save'] = False
                extra_context['show_save_and_continue'] = False
                extra_context['show_save_and_add_another'] = False
                if is_spoc(contract, user):
                    self.save_as = False

        return super().change_view(request, object_id,
                                   extra_context=extra_context)


admin.site.register(Contract, ContractAdmin)


class ContractConfigurationAdmin(admin.ModelAdmin):
    model = ContractConfiguration
    list_display = ('name', 'description', 'value')


admin.site.register(ContractConfiguration, ContractConfigurationAdmin)


class BuyRateInlineCommon(admin.TabularInline):

    """ Common Base class for all buy Rate inline models
        Done to set permissions at one place
    """
    extra = 0
    max_num = 0
    form = SlabForm

    def _has_buyrate_perms(self, user, perms):
        return AuthMixin().has_model_permissions(
                user, BuyRate, perms, 'contract')

    def has_add_permission(self, request, obj):
        return self._has_buyrate_perms(request.user, ['add'])

    def has_delete_permission(self, request, obj=None):
        return self._has_buyrate_perms(request.user, ['delete']) and (
                obj and not obj.is_readonly)

    def get_readonly_fields(self, request, obj=None):
        if not self._has_buyrate_perms(
                request.user, ['add', 'change']) or (obj and obj.is_readonly):
            return self.fields
        return []

    def get_max_num(self, request, obj=None, **kwargs):
        if obj and obj.is_readonly:
            return 0
        return None


class BuyShipmentSlabAdminInline(BuyRateInlineCommon):
    model = BuyShipmentSlab
    fields = ('when_to_pay', 'split_payment', 'attribute', 'interval', 'fixed_amount',
              'step_size', 'start', 'end', 'rate', 'is_ceiling_applicable')


class BuyVehicleSlabAdminInline(BuyRateInlineCommon):
    fields = ('when_to_pay', 'split_payment', 'attribute', 'interval', 'fixed_amount',
              'step_size', 'start', 'end', 'rate', 'is_ceiling_applicable')
    model = BuyVehicleSlab


class BuyDimensionSlabAdminInline(BuyRateInlineCommon):
    model = BuyDimensionSlab
    fields = ('when_to_pay', 'split_payment', 'attribute', 'interval', 'fixed_amount',
              'step_size', 'start', 'end', 'rate', 'is_ceiling_applicable')


class BuyVASSlabAdminInline(BuyRateInlineCommon):
    model = BuyVASSlab
    fields = ('when_to_pay', 'split_payment', 'attribute', 'interval', 'fixed_amount',
              'step_size', 'start', 'end', 'rate', )


class BuyNTSlabAdminInline(BuyRateInlineCommon):
    model = BuyNTSlab
    fields = ('when_to_pay', 'split_payment', 'attribute', 'interval', 'fixed_amount',
              'step_size', 'start', 'end', 'rate', )


class BuyRateHistoryInline(admin.TabularInline):
    model = BuyRateHistory
    readonly_fields = ['reason', 'field', 'old_value',
                       'new_value', 'user', 'modified_time']
    ordering = ['-modified_time']

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

get_fk_model = {
    'sellrate': 'sell_rate',
    'buyrate': 'buy_rate'
}


class ContractTermsInline(admin.TabularInline):

    def __init__(self, parent_model, admin_site):
        self.fk_name = get_fk_model.get(parent_model._meta.model_name)
        super(ContractTermsInline, self).__init__(parent_model, admin_site)

    verbose_name_plural = _('Usage')
    model = ContractTerm
    extra = 0
    max_num = 0
    can_delete = False
    can_add = False
    fields = ('wef', 'get_contract', 'get_division',)
    readonly_fields = fields

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('contract', 'contract__division')
        return qs

    def get_contract(self, obj):
        return obj.contract.name

    get_contract.short_description = 'Contract'

    def get_division(self, obj):
        return obj.contract.division

    get_division.short_description = 'Division'


class BuyRateAdmin(admin.ModelAdmin):

    list_display = ('name', 'handle', 'created_date', 'created_by', 'modified_date',
                    'modified_by', 'usage')

    list_select_related = ('created_by', 'modified_by',)

    search_fields = ['name', 'handle', 'created_date', 'modified_date',
                     'created_by__email', 'modified_by__email']

    inlines = [
        BuyShipmentSlabAdminInline,
        BuyVehicleSlabAdminInline,
        BuyDimensionSlabAdminInline,
        BuyVASSlabAdminInline,
        BuyNTSlabAdminInline,
        ContractTermsInline,
        BuyRateHistoryInline,
    ]

    def usage(self, obj):
        cont_list = []
        if hasattr(obj, 'buy_rate'):
            terms = obj.buy_rate.all()
            cont_list = [term.contract.name for term in terms]
        return cont_list

    usage.short_description = "Used in Contract(s)"
    form = BuyRateForm

    def get_readonly_fields(self, request, obj=None):
        """ Rate card can be edited only if there are no approved payments
            generated using this
        """
        if obj:
            approved_payments = DriverPayment.objects.filter(
                status=APPROVED, buy_rate=obj)
            obj.is_readonly = approved_payments.exists()

        if "handle" not in self.readonly_fields:
            self.readonly_fields = self.readonly_fields + ('handle',)

        if obj and obj.is_readonly:
            return self.readonly_fields + ('base_payment_interval',
                                           'driver_base_payment_amount',
                                           'driver_deduction_per_leave',)

        return self.readonly_fields

    def get_queryset(self, request):
        user = request.user
        qs = super(BuyRateAdmin, self).get_queryset(request)
        if user.is_superuser:
            return qs

        """
            Display only contract related buy rates based on if user is either
            creator/supervisor/spoc of contracts
        """
        cities = City.objects.filter(
            Q(managers=user) | Q(sales_representatives=user))
        qs = qs.filter(Q(created_by=user) |
                       Q(id__in=ContractTerm.objects.filter(
                           Q(contract__spocs=user) |
                           Q(contract__supervisors=user) |
                           Q(contract__created_by=user) |
                           Q(contract__city__in=cities)).values_list(
                           'buy_rate_id', flat=True)))
        qs = qs.prefetch_related(
            Prefetch(
                'buy_rate',
                queryset=ContractTerm.objects.all().select_related('contract')
            )
        )
        return qs

    def save_formset(self, request, form, formset, change):
        """
            Given an inline formset save it to the database.
        """
        instances = formset.save(commit=False)
        buyratechange_list = []
        parent_data = form.cleaned_data
        reason_for_change = parent_data.get('reason_for_change')

        if change:
            for form in formset.forms:
                if form.changed_data:
                    old_value = ''
                    for field in form.changed_data:
                        if form.instance.pk:
                            old_version = form.instance.__class__.objects.get(
                                pk=form.instance.pk)
                            old_value = old_version.__dict__.get(field)
                        new_value = form.cleaned_data.get(field)
                        if old_value != new_value:
                            buyratechange_list.append(BuyRateHistory(
                                buy_rate=form.cleaned_data['buy_rate'],
                                field='%s : %s' % (
                                    form.instance.__class__.__name__, field),
                                old_value=old_value,
                                new_value=new_value,
                                reason=reason_for_change,
                                user=request.user))
                            if field == 'split_payment':
                                DriverPayment.objects.filter(
                                    buy_rate=form.cleaned_data[
                                        'buy_rate']).update(is_stale=True,
                                                            stale_reason='Buy rate changed')

        if buyratechange_list:
            BuyRateHistory.objects.bulk_create(buyratechange_list)
        formset.save()

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            reason_for_change = data.get('reason_for_change')
            buyrate_list = []
            current_user = request.user

            if change:

                old = BuyRate.objects.get(pk=obj.id)
                for field in old._meta.fields:
                    field_name = field.__dict__.get('name')

                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(obj)

                    if old_value != current_value:
                        old_value = str(getattr(old, field_name))
                        new_value = str(getattr(obj, field_name))

                        buyrate_list.append(BuyRateHistory(
                            field=field_name, old_value=str(old_value),
                            new_value=new_value,
                            user=current_user,
                            buy_rate=old,
                            reason=reason_for_change))

            if buyrate_list:
                BuyRateHistory.objects.bulk_create(buyrate_list)

        obj.save()
        super().save_model(request, obj, form, change)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        if object_id:
            user = request.user

            cont = Contract.objects.filter(contractterm__buy_rate=object_id)
            cities = City.objects.filter(managers=user)
            buy_rate = BuyRate.objects.get(pk=object_id)

            """
            Do not show save buttons if user is only a spoc for related
            contracts
            """
            has_perm = AuthMixin().atleast_one_permission(request.user, BuyRate,
                                                          CONTRACT_PERMISSIONS,
                                                          'contract')
            if not has_perm or (
                not user.is_superuser
                and cont.filter(spocs=user).exists()
                and not cont.filter(
                    Q(city__in=cities) |
                    Q(supervisors=user) |
                    Q(created_by=user)
                ).exists() and buy_rate.created_by != user):
                extra_context['show_save'] = False
                extra_context['show_save_and_continue'] = False
        return super().change_view(
            request, object_id, extra_context=extra_context
        )

    class Media:
        css = {
            'all': ('website/css/toastr.css',
                    '/static/website/css/bootstrap.css')
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/buy_rate_change_reason.js',
              '/static/admin/js/contract_usage_info.js',)


admin.site.register(BuyRate, BuyRateAdmin)


class BaseSellSlabAdminInline(admin.TabularInline):

    def _has_sellrate_perms(self, user, perms):
        return AuthMixin().has_model_permissions(
                user, SellRate, perms, 'contract')

    def get_readonly_fields(self, request, obj=None):
        if not self._has_sellrate_perms(request.user, ['add', 'change']):
            return self.fields
        return []

    def has_delete_permission(self, request, obj=None):
        return self._has_sellrate_perms(request.user, ['delete'])

    def has_add_permission(self, request, obj=None):
        return self._has_sellrate_perms(request.user, ['add'])


class SellShipmentSlabAdminInline(BaseSellSlabAdminInline):
    model = SellShipmentSlab
    form = SlabForm
    fields = ('sku', 'attribute', 'interval',
              'aggregate_level', 'fixed_amount', 'step_size', 'start', 'end',
              'rate', 'is_ceiling_applicable')
    extra = 0

    def get_formset(self, request, obj=None, **kwargs):
        """
        Override the formset function in order to remove the add and change
        buttons beside the foreign key pull-down
        menus in the inline.
        """

        formset = super(SellShipmentSlabAdminInline,
                        self).get_formset(request, obj, **kwargs)
        form = formset.form
        if 'sku' in form.base_fields:
            widget = form.base_fields['sku'].widget
            widget.can_add_related = False
            widget.can_change_related = False
            widget.can_delete_related = False
        return formset

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        from blowhorn.customer.models import SKU
        if db_field.name == 'sku':
            qs = SKU.objects.filter(include_in_sellshipmentslab=True)
            kwargs['queryset'] = qs
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class SellVehicleSlabAdminInline(BaseSellSlabAdminInline):
    model = SellVehicleSlab
    form = SlabForm
    fields = ('attribute', 'interval', 'aggregate_level', 'fixed_amount',
              'step_size', 'start', 'end', 'rate',  'is_ceiling_applicable')
    extra = 0


class SellPincodeShipmentSlabAdminInline(BaseSellSlabAdminInline):
    model = SellPincodeShipmentSlab
    form = SlabForm
    fields = ('pincodes', 'attribute', 'interval', 'aggregate_level',
              'fixed_amount', 'step_size', 'start', 'end', 'rate', 'is_ceiling_applicable')
    extra = 0


class SellDimensionSlabAdminInline(BaseSellSlabAdminInline):
    model = SellDimensionSlab
    form = SlabForm
    fields = ('attribute', 'interval', 'fixed_amount', 'aggregate_level',
              'step_size', 'start', 'end', 'rate', 'is_ceiling_applicable')
    extra = 0


class SellVASSlabAdminInline(BaseSellSlabAdminInline):
    model = SellVASSlab
    form = SlabForm
    fields = ('vas_attribute', 'interval', 'fixed_amount', 'aggregate_level',
              'step_size', 'start', 'end', 'rate', 'is_ceiling_applicable')
    extra = 0


class SellNTSlabAdminInline(BaseSellSlabAdminInline):
    model = SellNTSlab
    form = SlabForm
    fields = ('attribute', 'interval', 'fixed_amount',
              'step_size', 'start', 'end', 'rate', 'is_ceiling_applicable')
    extra = 0


class SellRateHistoryInline(admin.TabularInline):
    model = SellRateHistory
    readonly_fields = ['reason', 'field', 'old_value',
                       'new_value', 'user', 'modified_time']
    ordering = ['-modified_time']

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class SellRateAdmin(admin.ModelAdmin):

    list_display = ('name', 'handle', 'created_date', 'created_by', 'modified_date',
                    'modified_by', 'usage')

    list_select_related = (
        'created_by', 'modified_by',
    )

    search_fields = ['name', 'created_date', 'modified_date',
                     'created_by__email', 'modified_by__email']

    inlines = [
        SellShipmentSlabAdminInline,
        SellPincodeShipmentSlabAdminInline,
        SellVehicleSlabAdminInline,
        SellDimensionSlabAdminInline,
        SellVASSlabAdminInline,
        SellNTSlabAdminInline,
        ContractTermsInline,
        SellRateHistoryInline,
    ]

    form = SellRateForm

    def usage(self, obj):
        terms = obj.contractterm_set.all()
        cont_list = []
        for term in terms:
            cont_list.append(term.contract.name)
        return cont_list
    usage.short_description = "Used in Contract(s)"


    def get_readonly_fields(self, request, obj=None):

        if "handle" not in self.readonly_fields:
            self.readonly_fields = self.readonly_fields + ('handle',)

        return self.readonly_fields

    def get_queryset(self, request):
        user = request.user
        qs = super(SellRateAdmin, self).get_queryset(request)
        if user.is_superuser:
            return qs

        """
        Display only contract related sell rates based on if user is either
        creator/supervisor/spoc of contracts
        """
        cities = City.objects.filter(
                Q(managers=user) | Q(sales_representatives=user))
        qs = qs.filter(Q(created_by=user) | Q(
            id__in=ContractTerm.objects.filter(
                Q(contract__spocs=user) |
                Q(contract__supervisors=user) |
                Q(contract__created_by=user) |
                Q(contract__city__in=cities)).values_list(
                'sell_rate_id', flat=True)))
        qs = qs.prefetch_related(Prefetch(
            'contractterm_set',
            queryset=ContractTerm.objects.all().select_related('contract')))
        return qs

    def save_formset(self, request, form, formset, change):
        """
        Given an inline formset save it to the database.
        """
        instances = formset.save(commit=False)
        sellratechange_list = []
        parent_data = form.cleaned_data
        reason_for_change = parent_data.get('reason_for_change')
        is_changed = False

        for form in formset.forms:
            if form.changed_data:
                is_changed = True
                if change:
                    old_value = ''
                    for field in form.changed_data:
                        if form.instance.pk:
                            old_version = form.instance.__class__.objects.get(
                                pk=form.instance.pk)
                            old_value = old_version.__dict__.get(field)
                        new_value = form.cleaned_data.get(field)
                        if old_value != new_value:
                            sellratechange_list.append(SellRateHistory(
                                sell_rate=form.cleaned_data['sell_rate'],
                                field='%s : %s' % (
                                    form.instance.__class__.__name__, field),
                                old_value=old_value,
                                new_value=new_value,
                                reason=reason_for_change,
                                user=request.user))

        if sellratechange_list:
            SellRateHistory.objects.bulk_create(sellratechange_list)

        if is_changed:
            sell_rate = None
            if len(instances):
                sell_rate = instances[0].sell_rate

            if sell_rate:
                CustomerInvoice.objects.filter(
                    contract_invoices__entities__sell_rate=sell_rate).filter(
                    status__in=StatusPipeline.INVOICE_EDIT_STATUSES).update(
                    is_stale=True, stale_reason='Sell rate changed')

        formset.save()

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            reason_for_change = data.get('reason_for_change')
            sellrate_list = []
            current_user = request.user

            if change:

                old = SellRate.objects.get(pk=obj.id)
                for field in old._meta.fields:
                    field_name = field.__dict__.get('name')

                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(obj)

                    if old_value != current_value:
                        old_value = str(getattr(old, field_name))
                        new_value = str(getattr(obj, field_name))

                        sellrate_list.append(SellRateHistory(
                            field=field_name, old_value=str(old_value),
                            new_value=new_value, user=current_user,
                            sell_rate=old, reason=reason_for_change))

            if sellrate_list:
                SellRateHistory.objects.bulk_create(sellrate_list)

        obj.save()
        super().save_model(request, obj, form, change)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        if object_id:
            user = request.user

            cont = Contract.objects.filter(contractterm__sell_rate=object_id)
            cities = City.objects.filter(managers=user)
            sell_rate = SellRate.objects.get(pk=object_id)

            """
            Do not show save buttons if user is only a spoc for
            related contracts
            """
            has_perm = AuthMixin().atleast_one_permission(
                request.user, SellRate, CONTRACT_PERMISSIONS, 'contract')
            if not has_perm or (
                (not user.is_superuser and cont.filter(spocs=user).exists() and
                    not cont.filter(
                        Q(city__in=cities) | Q(supervisors=user) |
                        Q(created_by=user)).exists() and
                 sell_rate.created_by != user)):
                extra_context['show_save'] = False
                extra_context['show_save_and_continue'] = False
        return super().change_view(
            request, object_id, extra_context=extra_context
        )

    class Media:
        css = {
            'all': ('website/css/toastr.css',
                    '/static/website/css/bootstrap.css')
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/sell_rate_change_reason.js',
              '/static/admin/js/contract_usage_info.js',)


admin.site.register(SellRate, SellRateAdmin)


class CommonContractAdmin(BaseAdmin):
    list_display = (
        'name', 'city', 'status', 'get_vehicle_classes', 'get_contract_term', 'is_wms_contract',
        'is_inter_city_contract', 'source_city',)

    def get_vehicle_classes(self, obj):
        return ', '.join([str(x) for x in obj.vehicle_classes.all()])
    get_vehicle_classes.short_description = "Vehicle Class(es)"

    inlines = [ContractTermInline(is_c2c=True)]

    list_filter = ('name', 'city',)

    def get_queryset(self, request):

        qs = super().get_queryset(request)
        qs = qs.select_related('city')
        qs = qs.prefetch_related('vehicle_classes')
        qs = qs.prefetch_related('contractterm_set')
        qs = qs.prefetch_related('contractterm_set__buy_rate')
        qs = qs.prefetch_related('contractterm_set__sell_rate')
        if not request.user.is_superuser:
            cities = City.objects.filter(managers=request.user)
            qs = qs.filter(
                Q(spocs=request.user) | Q(supervisors=request.user) |
                Q(city__in=cities)
            ).distinct()
        return qs

    def get_contract_term(self, obj):
        terms_sorted = sorted(
            obj.contractterm_set.all(), key=lambda x: x.wef)
        terms_before = [
            term for term in terms_sorted if term.wef <= timezone.now().date()]
        terms_after = [
            term for term in terms_sorted if term.wef > timezone.now().date()]

        term_qs = None
        if terms_before:
            term_qs = terms_before[-1]
        elif terms_after:
            term_qs = terms_after[0]

        buy_rate_name = ''
        sell_rate_name = ''
        if term_qs and term_qs.buy_rate:
            buy_rate_name = term_qs.buy_rate.name or ''
        if term_qs and term_qs.sell_rate:
            sell_rate_name = term_qs.sell_rate.name or '-'

        if term_qs:
            return format_html("{} <b>|</b> {} <b>|</b> {}",
                               term_qs.wef.strftime(DATE_FORMAT),
                               buy_rate_name, sell_rate_name)
        else:
            return ''
    get_contract_term.short_description = 'WEF | BuyRate | SellRate'

    def get_actions(self, request):
        """
        Remove `Delete` action for non-superusers
        """
        actions = super().get_actions(request)

        if 'delete_selected' in actions and not request.user.is_superuser:
            del actions['delete_selected']
        return actions


class BlowhornContractAdmin(CommonContractAdmin):
    form = BlowhornContractForm

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(contract_type=CONTRACT_TYPE_BLOWHORN)

    inlines = [ContractTermInline(is_c2c=False, blowhorn_contract=True), ]

    fieldsets = (
        (None, {
            'fields': ('description', 'name', 'status', 'city',
                       'vehicle_classes', 'payment_cycle', 'number_of_working_days', 'work_type')
        }),

        ('Permissions', {
            'fields': ('supervisors', 'spocs',),
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
            vehicle_classes = form.cleaned_data['vehicle_classes']
            vehicle_classes = str(vehicle_classes.all()[0])
            obj.name = vehicle_classes
            obj.contract_type = CONTRACT_TYPE_BLOWHORN
        else:
            obj.modified_by = request.user
        obj.status = CONTRACT_STATUS_ACTIVE

        super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, obj=None):
        read_only = ['name', 'created_date', 'created_by', 'modified_date',
                     'modified_by', 'status']
        if obj:
            read_only += ['city', 'payment_cycle', 'vehicle_classes']
            user = request.user
            if not is_superuser_or_supervisor(obj, user) and not \
                    is_citymanager(obj, user):
                read_only += ['supervisors', 'spocs']
        return read_only


admin.site.register(BlowhornContract, BlowhornContractAdmin)


class LabourConfigurationInline(admin.StackedInline):
    model = LabourConfiguration
    extra = 1

    # def has_add_permission(self, request, obj=None):
    #     return False

    def has_delete_permission(self, request, obj=None):
        return False


class KioskLabourConfigurationInline(admin.TabularInline):
    model = KioskLabourConfiguration
    extra = 0


class C2CContractAdmin(FSMTransitionMixin, CommonContractAdmin):

    form = C2CContractForm
    fsm_field = ['status', ]
    list_filter = ('city', 'customer', 'status', MarginFilter)
    list_display = (
        'name', 'get_expected_margin', 'city', 'status', 'get_vehicle_classes',
        'get_contract_term')
    inlines = [ContractTermInline(is_c2c=True), LabourConfigurationInline]
    fieldsets = (
        (None, {
            'fields':
            ('name', 'city', 'status', 'vehicle_classes', 'currency_code',
             'supervisors', 'description', 'spocs', 'trip_workflow', 'is_house_shifting_contract',
             'is_pod_applicable', 'hours_before_scheduled_at',
             'hours_after_scheduled_at', 'number_of_working_days', 'work_type')
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def get_expected_margin(self, obj):
        return expected_margin(obj.expected_margin)

    get_expected_margin.short_description = 'Expected Margin'
    get_expected_margin.admin_order_field = 'expected_margin'

    def get_readonly_fields(self, request, obj=None):
        """
        When editing allow buy_rate and sell_rate only
        """
        if obj:  # edit
            read_only = ['name', 'city', 'status', 'vehicle_classes',
                         'created_date', 'created_by', 'modified_date',
                         'modified_by', ]
            if not is_superuser_or_supervisor(obj, request.user):
                read_only += ['supervisors', 'spocs']
        else:
            read_only = [
                'name', 'created_date', 'created_by', 'modified_date',
                'modified_by', ]
        return read_only

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(contract_type=CONTRACT_TYPE_C2C)

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
            if form.cleaned_data:
                vehicle_classes = form.cleaned_data['vehicle_classes']
                vehicle_classes = str(vehicle_classes.all()[0])
                obj.name = vehicle_classes
                obj.contract_type = CONTRACT_TYPE_C2C
                obj.status = CONTRACT_STATUS_ACTIVE
                obj.payment_cycle = PaymentCycle.objects.filter(
                    name='Daily').first()
        else:
            obj.modified_by = request.user
        update_cms.apply_async((obj.city_id, False, obj.id), eta=timezone.now() + timedelta(seconds=5))

        super().save_model(request, obj, form, change)

    class Media:
        css = {
            'all': ('/static/admin/css/custom_contract.css',
                    '/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              # '/static/admin/js/contract_admin.js',
              # '/static/admin/js/contract_confirmation_pop_up.js',
              '/static/admin/js/contract/marginGraph.js',
              '/static/admin/js/contract/margin_dataset_graph.js',
            )


admin.site.register(C2CContract, C2CContractAdmin)


class SMEContractAdmin(FSMTransitionMixin, CommonContractAdmin):

    form = SMEContractForm
    fsm_field = ['status', ]
    list_filter = ('name', 'city', 'customer', 'status',)
    inlines = [ContractTermInline(is_c2c=True), LabourConfigurationInline]

    fieldsets = (
        (None, {
            'fields':
                ('name', 'city', 'status', 'currency_code', 'vehicle_classes', 'supervisors', 'description',
                 'spocs', 'trip_workflow', 'hours_before_scheduled_at', 'is_house_shifting_contract',
                 'hours_after_scheduled_at', 'number_of_working_days', 'work_type')
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        """
        When editing allow buy_rate and sell_rate only
        """
        if obj:  # edit
            read_only = ['city', 'vehicle_classes', 'status', 'created_date',
                         'created_by', 'modified_date', 'modified_by', ]
            if not is_superuser_or_supervisor(obj, request.user):
                read_only += ['supervisors', 'spocs']
        else:
            read_only = [
                'created_date', 'created_by', 'modified_date', 'modified_by', ]
        return read_only

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(contract_type=CONTRACT_TYPE_SME)

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
            if form.cleaned_data:
                vehicle_classes = form.cleaned_data['vehicle_classes']
                vehicle_classes = str(vehicle_classes.all()[0])
                obj.contract_type = CONTRACT_TYPE_SME
                obj.status = CONTRACT_STATUS_ACTIVE
                # @todo needs to be reconsidered
                obj.payment_cycle = PaymentCycle.objects.filter(
                    name='Daily').first()
        else:
            obj.modified_by = request.user
        update_cms.apply_async((obj.city_id, False, obj.id), eta=timezone.now() + timedelta(seconds=5))

        super().save_model(request, obj, form, change)

    class Media:
        css = {
            'all': ('/static/admin/css/custom_contract.css',
                    '/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/contract_admin.js',
              '/static/admin/js/contract_confirmation_pop_up.js',
              '/static/admin/js/contract/marginGraph.js',
              '/static/admin/js/contract/margin_dataset_graph.js',
              )


admin.site.register(SMEContract, SMEContractAdmin)


class ScreenAdminInline(admin.TabularInline):

    model = Screen
    fields = (
        'name', 'sequence', 'input_fields', 'button_fields', 'parameters',
        'geofence', 'confirmation_screen'
    )
    extra = 0


class TripWorkFlowAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_date', 'created_by', 'modified_date',
                    'modified_by')

    list_select_related = (
        'created_by', 'modified_by',
    )

    inlines = [
        ScreenAdminInline,
    ]

    search_fields = ['name', 'created_date', 'modified_date',
                     'created_by__email', 'modified_by__email']

    fieldsets = (
        ('Trip WorkFlow Info', {
            'fields': (
                'name', 'is_reorder', 'can_cancel_trip', 'can_scan_orders',
                'can_complete_trip', 'otp_required', 'show_sku_details',
                'ignore_order_lines', 'capture_assets', 'end_trip_otp',
                'pick_orders', 'perform_delivery',
                'odometer_proof', 'is_call_masking', 'report_to_pickup_hub',
                'optimise_route', 'is_wms_flow'
            )
        }),
    )


admin.site.register(TripWorkFlow, TripWorkFlowAdmin)


class ScreenFieldAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_date', 'created_by',
                    'modified_date', 'modified_by')

    list_select_related = (
        'created_by', 'modified_by',
    )

    search_fields = ['name', 'created_date', 'modified_date',
                     'created_by__email', 'modified_by__email']

    fieldsets = (
        ('Screen Fields Info', {
            'fields': (
                'name', 'data_type',
            )
        }),
    )


admin.site.register(ScreenField, ScreenFieldAdmin)


class ScreenButtonAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_date', 'created_by',
                    'modified_date', 'modified_by')

    list_select_related = (
        'created_by', 'modified_by',
    )

    search_fields = ['name', 'created_date', 'modified_date',
                     'created_by__email', 'modified_by__email']

    fieldsets = (
        ('Screen Fields Info', {
            'fields': (
                'name', 'parameters'
            )
        }),
    )


admin.site.register(ScreenButton, ScreenButtonAdmin)


class ItemCategoryAdmin(admin.ModelAdmin):

    list_display = ('name', 'min_weight', 'max_weight')

    list_select_related = (
        'created_by', 'modified_by',
    )

    readonly_fields = ['created_date', 'created_by',
                       'modified_date', 'modified_by', ]

    search_fields = ['name', 'created_by__email', 'modified_by__email']

    fieldsets = (
        ('Item Category Details', {
            'fields': (
                'name', 'description', 'min_weight', 'max_weight'
            )
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )


admin.site.register(ItemCategory, ItemCategoryAdmin)


class KioskContractAdmin(FSMTransitionMixin, CommonContractAdmin):

    fsm_field = ['status', ]
    list_filter = ('city', 'customer', 'status',)
    inlines = [ContractTermInline(is_c2c=True), KioskLabourConfigurationInline]
    form = KioskContractForm
    fieldsets = (
        (None, {
            'fields':
            ('name', 'city', 'payment_cycle',
             'trip_workflow', 'number_of_working_days', 'work_type')
        }),
        ('Permissions', {
            'fields': ('spocs', 'supervisors',)
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        """
        When editing allow buy_rate and sell_rate only
        """
        if obj:  # edit
            read_only = ['name', 'city', 'status', 'created_date',
                         'created_by', 'modified_date', 'modified_by', ]
            if obj.status == CONTRACT_STATUS_ACTIVE:
                read_only.extend(['work_type', 'number_of_working_days', ])
            if not is_superuser_or_supervisor(obj, request.user):
                read_only += ['supervisors', 'spocs']
        else:
            read_only = [
                'created_date', 'created_by', 'modified_date', 'modified_by', ]
        return read_only

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(contract_type=CONTRACT_TYPE_KIOSK)

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
            if form.cleaned_data:
                obj.contract_type = CONTRACT_TYPE_KIOSK
                obj.status = CONTRACT_STATUS_ACTIVE
        else:
            obj.modified_by = request.user

        super().save_model(request, obj, form, change)


admin.site.register(KioskContract, KioskContractAdmin)


class FlashSaleContractAdmin(FSMTransitionMixin, CommonContractAdmin):
    fsm_field = ['status', ]
    list_filter = ('city', 'customer', 'status',)

    form = KioskContractForm
    fieldsets = (
        (None, {
            'fields':
                ('name', 'city', 'payment_cycle',
                 'trip_workflow',)
        }),
        ('Permissions', {
            'fields': ('spocs', 'supervisors',)
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        """
        When editing allow buy_rate and sell_rate only
        """
        if obj:  # edit
            read_only = ['name', 'city', 'status', 'created_date',
                         'created_by', 'modified_date', 'modified_by', ]
            if not is_superuser_or_supervisor(obj, request.user):
                read_only += ['supervisors', 'spocs']
        else:
            read_only = [
                'created_date', 'created_by', 'modified_date', 'modified_by', ]
        return read_only

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(contract_type=CONTRACT_TYPE_FLASH_SALE)

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
            if form.cleaned_data:
                obj.contract_type = CONTRACT_TYPE_FLASH_SALE
                obj.status = CONTRACT_STATUS_ACTIVE
        else:
            obj.modified_by = request.user

        super().save_model(request, obj, form, change)


admin.site.register(FlashSaleContract, FlashSaleContractAdmin)


class ContractSale(Contract):

    class Meta:
        proxy = True
        verbose_name = _('Contract (Sales)')
        verbose_name_plural = _('Contracts (Sales)')


class ContractOperation(Contract):

    class Meta:
        proxy = True
        verbose_name = _('Contract (Operations)')
        verbose_name_plural = _('Contracts (Operations)')


class ContractSalesAdmin(BaseContractAdmin):

    def save_model(self, request, obj, form, change):
        # add created by for new record else modified by
        if not change:
            obj.created_by = request.user
        else:
            obj.modified_by = request.user
        super().save_model(request, obj, form, change)

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            fieldsets = (
                (None, {
                    'fields': (
                        'contract_type', 'customer', 'city', 'source_city', 'division',
                        'vehicle_classes', 'body_types', 'service_schedule', 'avg_basket_value',
                        'service_tax_category', 'hsn_code', 'status', 'is_wms_contract', 'is_inter_city_contract',
                        'currency_code', 'is_pod_applicable', 'item_categories',
                        'description', 'additional_info', 'number_of_working_days', 'work_type')
                }),
                ('Permissions', {
                    'fields': ('supervisors',),
                }),
            )
        else:
            fieldsets = (
                (None, {
                    'fields': (
                        'name', 'description', 'status', 'contract_type',
                        'customer', 'city', 'source_city', 'division', 'vehicle_classes',
                        'body_types', 'service_schedule', 'avg_basket_value',
                        'next_30_day_occurrences', 'item_categories',
                        'additional_info', 'currency_code', 'service_tax_category',
                        'hsn_code', 'is_pod_applicable', 'is_wms_contract', 'is_inter_city_contract',
                        # Readonly for sales
                        'payment_cycle', 'trip_noshow_hrs',
                        'hours_before_scheduled_at',
                        'hours_after_scheduled_at',
                        'use_customer_hub', 'number_of_working_days', 'work_type',
                    )
                }),
                ('Trip WorkFlow', {
                    'fields': ('trip_workflow',),
                }),
                ('Permissions', {
                    'fields': ('supervisors', 'spocs',),
                }),
                ('Audit Log', {
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                }),
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        read_only = []
        common_fields = ['name', 'job_last_run_time', 'created_date',
                         'created_by', 'modified_date', 'modified_by', 'status']
        if obj:  # edit
            read_only = [
                'payment_cycle', 'trip_noshow_hrs',
                'hours_before_scheduled_at', 'hours_after_scheduled_at',
                'next_30_day_occurrences', 'city', 'trip_workflow', 'spocs',
                'contract_type', 'customer', 'use_customer_hub'
            ]
            if obj.status == CONTRACT_STATUS_ACTIVE:
                read_only.extend(['work_type', 'number_of_working_days', 'is_wms_contract',
                                  'is_inter_city_contract', 'source_city'])
        user = request.user
        if not user.is_sales_representative(obj.city if obj else None):
            read_only += ['description']

        return read_only + common_fields

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = ContractEditFormSales
        else:
            self.form = ContractFormSales
        return super().get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        """ Display contracts if user is sales-representative of contract city
        """
        user = request.user
        qs = super(ContractSalesAdmin, self).get_queryset(request)
        qs = qs.select_related('customer')
        qs = qs.filter(customer__isnull=False)
        qs = qs.prefetch_related('contractterm_set')
        qs = qs.prefetch_related('contractterm_set__buy_rate')
        qs = qs.prefetch_related('contractterm_set__sell_rate')
        qs = qs.prefetch_related('vehicle_classes', 'spocs', 'supervisors',
                                 'body_types')
        qs = qs.select_related('created_by', 'modified_by', 'payment_cycle')
        if not user.is_superuser:
            cities = City.objects.filter(sales_representatives=user)
            qs = qs.filter(city__in=cities).distinct()
        return qs


admin.site.register(ContractSale, ContractSalesAdmin)


class ContractOperationAdmin(BaseContractAdmin):

    def save_model(self, request, obj, form, change):
        # add value to modified by field in case of proxy model
        if change:
            obj.modified_by = request.user

        super().save_model(request, obj, form, change)

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            (None, {
                'fields': ('name', 'status', 'contract_type',
                           'customer', 'city', 'source_city', 'division',
                           'vehicle_classes', 'body_types',
                           'service_schedule', 'next_30_day_occurrences',
                           'item_categories', 'service_tax_category', 'is_wms_contract', 'is_inter_city_contract',
                           'hsn_code',
                           # Editable for operation staff
                           'payment_cycle', 'currency_code', 'trip_noshow_hrs',
                           'is_pod_applicable', 'use_customer_hub', 'expected_delivery_hours',
                           'hours_before_scheduled_at',
                           'hours_after_scheduled_at', 'description',
                           'additional_info', 'number_of_working_days', 'work_type')
            }),
            ('Trip WorkFlow', {
                'fields': ('trip_workflow',),
            }),
            ('Permissions', {
                'fields': ('supervisors', 'spocs',),
            }),
            ('Audit Log', {
                'fields': ('created_date', 'created_by', 'modified_date',
                           'modified_by'),
            }),
        )
        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        read_only = [
            'name', 'status', 'contract_type', 'customer', 'city', 'division',
            'vehicle_classes', 'body_types', 'next_30_day_occurrences',
            'item_categories', 'created_date', 'created_by', 'modified_date',
            'modified_by',
        ]
        if obj.payment_cycle and obj.status not in [CONTRACT_STATUS_DRAFT]:
            read_only += ['payment_cycle']

        if obj.status == CONTRACT_STATUS_ACTIVE:
            read_only.extend(
                ['work_type', 'number_of_working_days', 'is_wms_contract', 'is_inter_city_contract', 'source_city'])

        user = request.user

        if not is_superuser_or_supervisor(obj, user) and not \
                is_citymanager(obj, user):
            read_only += ['supervisors', 'spocs']

        if not user.is_sales_representative(obj.city if obj else None):
            read_only += ['description']

        return read_only

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = ContractEditFormOperation
        return super().get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        """ Display contracts if user is either supervisor/spoc of
            that particular contract
        """
        user = request.user
        qs = super(ContractOperationAdmin, self).get_queryset(request)
        qs = qs.select_related('customer')
        qs = qs.filter(customer__isnull=False)
        qs = qs.prefetch_related('contractterm_set')
        qs = qs.prefetch_related('contractterm_set__buy_rate')
        qs = qs.prefetch_related('contractterm_set__sell_rate')
        qs = qs.prefetch_related('vehicle_classes', 'spocs', 'supervisors',
                                 'body_types')
        qs = qs.select_related('created_by', 'modified_by', 'payment_cycle')
        if not user.is_superuser:
            qs = qs.filter(Q(supervisors=user) | Q(spocs=user)).distinct()
        return qs

    def has_add_permission(self, request):
        """
            Do not allow addition of contract by Operations Staff
        """
        return False


class WorkTypeAdmin(admin.ModelAdmin):
    list_display = ('work_type', )
    search_fields = ['work_type']

    fieldsets = (
        ('General', {
            'fields': ('work_type',)
        }),
        ('Audit Log', {
            'fields': ('created_by', 'created_date', 'modified_by', 'modified_date')
        })
    )
    readonly_fields = ('created_by', 'created_date', 'modified_by', 'modified_date')

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(ContractOperation, ContractOperationAdmin)
admin.site.register(ServiceHSNCodeMap, ServiceHSNCodeMapAdmin)
admin.site.register(VASAttribute)
admin.site.register(WorkType, WorkTypeAdmin)
