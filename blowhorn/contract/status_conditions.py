from blowhorn.oscar.core.loading import get_model
from django.conf import settings
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.constants import UNAPPROVED
from blowhorn.contract.constants import CONTRACT_TYPE_FIXED, CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_HYPERLOCAL, \
    CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_BLOWHORN, CONTRACT_TYPE_SPOT, CONTRACT_TYPE_KIOSK, CONTRACT_TYPE_DEAFCOM

''' Whether a contract can be made `Active` '''


def can_activate(contract):
    """
    Can the contract be activated?
    """
    # If the contract does not exist
    if not contract:
        return False
    if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
        # If work days and work type are not defined for fixed contract
        if contract.contract_type == CONTRACT_TYPE_FIXED and not (
                contract.work_type and contract.number_of_working_days):
            can_activate.hint = 'Define Work Type and Number of Working Days.'
            return False

        # if work type is not defined
        if contract.contract_type in [CONTRACT_TYPE_SPOT, CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL,
                                      CONTRACT_TYPE_KIOSK] and not contract.work_type:
            can_activate.hint = 'Define Work Type.'
            return False

    # If payment cycle is not defined
    if contract.contract_type in [CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SPOT] and not contract.trip_workflow:
        can_activate.hint = 'Define Trip Work Flow.'
        return False

    # If payment cycle is not defined
    if not contract.payment_cycle:
        can_activate.hint = 'Define Payment cycle.'
        return False

    # If contract type is Fixed and service schedule is not defined
    if contract.contract_type == CONTRACT_TYPE_FIXED and not contract.service_schedule:
        can_activate.hint = 'Define Service Calendar.'
        return False

    if contract.contract_type == CONTRACT_TYPE_SHIPMENT and contract.is_inter_city_contract and not contract.source_city:
        can_activate.hint = 'Define Source City.'
        return False

    # If contract type is Subscription allow only one Active contract per
    # customer, city
    contracts = get_model('contract', 'Contract')
    if contract.contract_type == CONTRACT_TYPE_SHIPMENT and not contract.is_inter_city_contract and\
        contracts.objects.filter(customer=contract.customer, city=contract.city, contract_type=CONTRACT_TYPE_SHIPMENT,
                                 status=CONTRACT_STATUS_ACTIVE, is_inter_city_contract=False).exists():
        can_activate.hint = 'Only one active contract allowed per customer per city for Shipment type'
        return False

    if contract.contract_type == CONTRACT_TYPE_SHIPMENT and contract.is_inter_city_contract and \
        contracts.objects.filter(customer=contract.customer, city=contract.city, contract_type=CONTRACT_TYPE_SHIPMENT,
                                 status=CONTRACT_STATUS_ACTIVE, is_inter_city_contract=True, source_city=contract.source_city).exists():
        can_activate.hint = 'Only one active contract allowed per customer per city for Shipment type'
        return False
    
    if contract.contract_type == CONTRACT_TYPE_HYPERLOCAL and not contract.is_inter_city_contract and\
        contracts.objects.filter(customer=contract.customer, city=contract.city, contract_type=CONTRACT_TYPE_HYPERLOCAL,
                                 status=CONTRACT_STATUS_ACTIVE, is_inter_city_contract=False).exists():
        can_activate.hint = 'Only one active contract allowed per customer per city for Hyperlocal type'
        return False

    # contracts = get_model('contract', 'Contract')
    if contract.contract_type == CONTRACT_TYPE_DEAFCOM and \
        contracts.objects.filter(customer=contract.customer, city=contract.city, contract_type=CONTRACT_TYPE_DEAFCOM,
                                     status=CONTRACT_STATUS_ACTIVE).exists():
        can_activate.hint = 'Only one active contract allowed per customer per city for Deafcom type'
        return False

    # If buy or sell rates are not defined

    # Check condition that contract has a Contract term
    # with a buy rate and a sell rate
    contract_term = get_model('contract', 'ContractTerm')
    if contract.contract_type == CONTRACT_TYPE_BLOWHORN and contract_term.objects.filter(contract=contract) \
        .filter(buy_rate__isnull=False).exists():
            return True

    if contract_term.objects.filter(contract=contract) \
        .filter(buy_rate__isnull=False) \
        .filter(sell_rate__isnull=False) \
            .exists():
        return True
    else:
        can_activate.hint = 'Define Buy Rate and Sell Rate.'
        return False


''' Whether a contracy can be made `Inactive` '''


def can_deactivate(contract):
    """
    Can the contract be de-activated
    """
    # If the contract does not exist
    if not contract:
        return False

    # If there are pending trips/orders
    trip = get_model('trip', 'Trip')
    if trip.objects.filter(customer_contract_id=contract.id,
                           status__in=[StatusPipeline.TRIP_NEW, StatusPipeline.TRIP_IN_PROGRESS,
                                       StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.TRIP_ALL_STOPS_DONE]
                           ).exists():
        can_deactivate.hint = 'Close Pending Trips.'
        return False

    order = get_model('order', 'Order')
    if order.objects.filter(customer_contract_id=contract.id,
                            status__in=[StatusPipeline.ORDER_NEW, StatusPipeline.OUT_FOR_DELIVERY]).exists():
        can_deactivate.hint = 'Close Pending Orders.'
        return False

    # If there are pending payment records
    payment = get_model('driver', 'DriverPayment')
    if payment.objects.filter(contract=contract, status=UNAPPROVED).exists():
        can_deactivate.hint = 'Close Pending Driver Payments.'
        return False

    return True
