from pytz import timezone
from collections import defaultdict
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Q, Count
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from blowhorn.address.models import City
from blowhorn.common.serializers import BaseSerializer
from blowhorn.vehicle.models import VehicleClass
from blowhorn.contract.constants import (CONTRACT_TYPE_FIXED, VAS_SERVICE_ATTRIBUTE_POD,
                                         CONTRACT_STATUS_ACTIVE)
from blowhorn.contract.mixins import ResourceAllocationMixin
from blowhorn.contract.models import SellVehicleSlab, SellRate, \
    LabourConfiguration, Contract, ResourceAllocationHistory, SellVASSlab



class SellVehicleSlabSerializer(serializers.ModelSerializer):

    class Meta:
        model = SellVehicleSlab
        fields = '__all__'


class SellVasSlabSerializer(serializers.ModelSerializer):
    vas_attribute = serializers.SerializerMethodField()

    class Meta:
        model = SellVASSlab
        fields = '__all__'

    def get_vas_attribute(self, obj):
        if obj.vas_attribute:
            return obj.vas_attribute.attribute


class SellRateSerializer(serializers.ModelSerializer):
    sellvehicleslab_set = SellVehicleSlabSerializer(many=True, required=False)
    sellvasslab_set = SellVasSlabSerializer(many=True, required=False)

    def __init__(self, *args, **kwargs):
        super(SellRateSerializer, self).__init__(*args, **kwargs)
        exclude = self.context.get('exclude', None)
        fields = self.context.get('fields', None)
        if fields and exclude:
            raise ImproperlyConfigured(
                _('Either use `fields` or `exclude_fields`'))

        if fields:
            self.Meta.fields = fields
        elif exclude:
            self.Meta.exclude = exclude
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = SellRate
        # fields = '__all__'


class LabourConfigurationSerializer(serializers.ModelSerializer):
    max_labours = serializers.SerializerMethodField()
    cost_per_labour = serializers.SerializerMethodField()
    contract = serializers.SerializerMethodField()
    class Meta:
        model = LabourConfiguration
        fields = '__all__'

    def get_max_labours(self, obj):
        return obj.max_labours or 0

    def get_cost_per_labour(self, obj):
        return obj.cost_per_labour or 0

    def get_contract(self, obj):
        return obj.contract_id or 0


class VehicleClassPackageSerializer(BaseSerializer):

    def __init__(self, *args, **kwargs):
        super(VehicleClassPackageSerializer, self).__init__(*args, **kwargs)
        self.contract_terms = self.context.get('contract_terms', [])
        self.vehicle_class_term_id_map = defaultdict(list)
        if self.contract_terms:
            for term_id, vehicle_class in self.contract_terms.values_list('id', 'contract__vehicle_classes'):
                self.vehicle_class_term_id_map[vehicle_class].append(term_id)
        self.city_id = self.context.get('city_id', 0)
        self.city_vehicle_class_alias_mapping = self.context.get('city_vehicle_class_alias_mapping', {})
        self.default_vehicle_class_id = self.context.get('default_vehicle_class_id', 0)
        self.default_contract_id = self.context.get('default_contract_id', 0)
        self.loading_mins = self.context.get('loading_mins', 0)
        self.unloading_mins = self.context.get('unloading_mins', 0)

    classification = serializers.SerializerMethodField()
    alias_name = serializers.SerializerMethodField()
    icons = serializers.SerializerMethodField()
    dimensions = serializers.SerializerMethodField()
    license_category = serializers.SerializerMethodField()
    packages = serializers.SerializerMethodField()
    default_vehicle = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField()
    vehicle_app_icon_name = serializers.SerializerMethodField()
    vehicle_capacity = serializers.SerializerMethodField()

    class Meta:
        model = VehicleClass
        fields = ['id', 'classification', 'alias_name', 'description', 'vehicle_capacity',
                  'vehicle_app_icon_name', 'icons', 'dimensions',
                  'license_category', 'packages', 'default_vehicle', 'time']

    def get_classification(self, vehicle_class):
        return vehicle_class.commercial_classification

    def get_vehicle_capacity(self, vehicle_class):
        if vehicle_class.carrying_capacity:
            return vehicle_class.carrying_capacity.split(',')
        return []

    def get_alias_name(self, vehicle_class):
        # Returns the vehicle class alias if present,
        # otherwise `commercial_classification`
        return self.city_vehicle_class_alias_mapping.get(
            str(self.city_id) + str(vehicle_class.id),
            vehicle_class.commercial_classification)

    def get_icons(self, vehicle_class):
        return {
            'standard': str(vehicle_class.image.url)
            if vehicle_class.image and vehicle_class.image.url else None,
            'selected': str(vehicle_class.image_selected.url)
            if vehicle_class.image_selected and vehicle_class.image_selected.url else None,
            'detail': str(vehicle_class.image_detail.url)
            if vehicle_class.image_detail and vehicle_class.image_detail.url else None,
        }

    def get_dimensions(self, vehicle_class):
        return {
            'length': vehicle_class.length,
            'breadth': vehicle_class.breadth,
            'height': vehicle_class.height,
            'capacity': vehicle_class.capacity
        }

    def get_license_category(self, vehicle_class):
        return {
            'code': vehicle_class.licence_category_code,
            'description': vehicle_class.licence_category_desc
        }

    def get_default_vehicle(self, vehicle_class):
        if vehicle_class.id == self.default_vehicle_class_id:
            return True
        else:
            return False

    def get_time(self, vehicle_class):
        return {
            'loading_mins': self.loading_mins,
            'unloading_mins': self.unloading_mins
        }

    def get_vehicle_app_icon_name(self, vehicle_class):
        return vehicle_class.app_icon if vehicle_class.app_icon else ''

    def get_packages(self, vehicle_class):
        packages = []

        self.contract_terms = self.contract_terms.prefetch_related('sell_rate')
        self.contract_terms = self.contract_terms.prefetch_related('sell_rate__sellvasslab_set')
        self.contract_terms = self.contract_terms.prefetch_related('sell_rate__sellvehicleslab_set')
        self.contract_terms = self.contract_terms.select_related('contract')
        self.contract_terms = self.contract_terms.prefetch_related('contract__labourconfiguration')

        for term in self.contract_terms:
            if term.id in self.vehicle_class_term_id_map.get(vehicle_class.id, []):
                sell_rate_data = SellRateSerializer(instance=term.sell_rate).data
                contract = term.contract
                if self.default_contract_id == contract.id:
                    default_contract = True
                else:
                    default_contract = False
                labour_option = contract.labourconfiguration \
                    if hasattr(contract, 'labourconfiguration') else None

                vas_slab = sell_rate_data.get('sellvasslab_set', '')
                pod_cost = 0
                for slab in vas_slab:
                    vas_attribute = slab.get('vas_attribute')
                    if vas_attribute == VAS_SERVICE_ATTRIBUTE_POD:
                        pod_cost = slab.get('fixed_amount', 0)
                labour_configuration = LabourConfigurationSerializer(
                    labour_option).data

                if not labour_configuration:
                    labour_configuration = {
                        'id': 0,
                        'max_labours': 0,
                        'cost_per_labour': 0,
                        'contract': 0
                    }
                term.sell_rate.contract = contract
                term.sell_rate.city = City.objects.get(id=self.city_id)

                _dict = {
                    'is_pod_applicable': contract.is_pod_applicable,
                    'currency': contract.currency_code,
                    'pod_cost': float(pod_cost),
                    'title': term.sell_rate.display_text,
                    'description': term.sell_rate.display_description,
                    'contract_id': term.contract_id,
                    'default_contract': default_contract,
                    'sell_rate': sell_rate_data,
                    'labour_configuration': labour_configuration
                }
                packages.append(_dict)
        return packages


class ContractResourceSerializer(serializers.ModelSerializer,
                                 ResourceAllocationMixin):
    id = serializers.IntegerField()
    name = serializers.CharField()
    description = serializers.CharField()
    customer = serializers.SerializerMethodField()
    customer_url = serializers.SerializerMethodField()
    division = serializers.SerializerMethodField()
    city_id = serializers.IntegerField(source='city.id')
    active_drivers = serializers.IntegerField()

    class Meta:
        model = Contract
        fields = ['id', 'name', 'description', 'customer', 'division',
                  'city_id', 'active_drivers', 'customer_url']

    def get_customer(self, obj):
        return {
            'id': obj.customer_id,
            'name': '%s' % obj.customer,
        }

    def get_division(self, obj):
        return {
            'id': obj.division_id,
            'name': '%s' % obj.division
        }

    def get_customer_url(self, obj):
        return reverse('admin:customer_businesscustomer_change',
                       args=[obj.customer_id])

    def __get_query_params(self, query):
        params = {}
        for p in query:
            params[p] = query[p]
        return params

    def queryset(self, user, query):
        qs = self.Meta.model.objects.filter(status=CONTRACT_STATUS_ACTIVE,
                                            contract_type=CONTRACT_TYPE_FIXED)

        # query_params = self.__get_query_params(query)
        city_pk = query.get('city_id', 0)
        if not user.is_superuser:
            if not city_pk:
                cities = City. objects.get_cities_for_managerial_user(user)
                qs = qs.filter(
                    Q(city__in=cities) | Q(spocs=user) | Q(supervisors=user))
            else:
                is_manager = self.is_manager_for_city(city_pk, user)
                qs = qs.filter(city_id=city_pk)
                if not is_manager:
                    qs = qs.filter(Q(spocs=user) | Q(supervisors=user))

        elif city_pk:
            qs = qs.filter(city_id=city_pk)

        # qs = qs.filter(**query_params)
        search_text = query.get('search', None)
        if search_text:
            qs = qs.filter(
                Q(customer__name__icontains=search_text) |
                Q(name__icontains=search_text) |
                Q(division__name__icontains=search_text)
            )
        qs = qs.select_related('customer', 'division', 'city')
        qs = qs.prefetch_related('resourceallocation_set')
        qs = qs.annotate(
            active_drivers=Count('resourceallocation__drivers', distinct=True)
        )
        return qs.only('name', 'description', 'customer', 'division', 'city',
                       'service_schedule')


class ResourceAllocationHistorySerializer(serializers.ModelSerializer):
    event_time = serializers.SerializerMethodField()
    modified_by = serializers.SerializerMethodField()
    driver = serializers.SerializerMethodField()

    class Meta:
        model = ResourceAllocationHistory
        exclude = ('created_date', 'created_by', 'modified_date', 'contract')

    def get_event_time(self, obj):
        if obj.event_time:
            return obj.event_time.astimezone(
                timezone('Asia/Kolkata')).isoformat()
        return '-- NA --'

    def get_modified_by(self, obj):
        return '%s' % obj.modified_by

    def get_driver(self, obj):
        return '%s' % obj.driver
