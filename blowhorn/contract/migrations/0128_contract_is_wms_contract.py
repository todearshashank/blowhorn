# Generated by Django 2.2.24 on 2021-12-06 09:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0127_auto_20211123_0828'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='is_wms_contract',
            field=models.BooleanField(default=False, verbose_name='Is WMS Contract'),
        ),
    ]
