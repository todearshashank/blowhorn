# Generated by Django 2.1.1 on 2020-10-12 20:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0109_auto_20200804_1124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contract',
            name='contract_type',
            field=models.CharField(choices=[('Spot', 'Spot'), ('Fixed', 'Fixed'), ('Shipment', 'Shipment'), ('Kiosk', 'Kiosk'), ('FlashSale', 'FlashSale'), ('Ondemand', 'On-Demand')], db_index=True, default='Fixed', max_length=15, verbose_name='Contract Type'),
        ),
    ]
