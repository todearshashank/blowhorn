# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-15 13:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0003_auto_20171114_1925'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='buyrate',
            name='trip_noshow_hrs',
        ),
        migrations.AddField(
            model_name='contract',
            name='trip_noshow_hrs',
            field=models.IntegerField(default=24, verbose_name='Hrs to mark trip as NO-SHOW'),
        ),
    ]
