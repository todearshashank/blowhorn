# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-16 11:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0022_auto_20180112_0651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buydimensionslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
        migrations.AlterField(
            model_name='buyntslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
        migrations.AlterField(
            model_name='buyshipmentslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
        migrations.AlterField(
            model_name='buyvasslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
        migrations.AlterField(
            model_name='buyvehicleslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
        migrations.AlterField(
            model_name='selldimensionslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
        migrations.AlterField(
            model_name='sellntslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
        migrations.AlterField(
            model_name='sellvasslab',
            name='interval',
            field=models.CharField(choices=[('Per-Day', 'Per-Day'), ('Per-Cycle', 'Per-Cycle'), ('Per-Month', 'Per-Month')], default='Per-Month', max_length=25, verbose_name='For'),
        ),
    ]
