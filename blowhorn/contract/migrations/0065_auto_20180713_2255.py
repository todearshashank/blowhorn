# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-07-13 22:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0064_auto_20180710_0653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='screen',
            name='name',
            field=models.CharField(choices=[('start_trip', 'Start Trip'), ('finished_loading', 'Finished Loading'), ('reached_stop', 'Reached Stop'), ('finished_stop', 'Finished Work'), ('end_trip', 'End Trip')], default='start_trip', max_length=50, verbose_name='Screen Name'),
        ),
    ]
