# Generated by Django 2.1.1 on 2019-03-26 12:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0087_auto_20190315_0924'),
    ]

    operations = [
        migrations.AddField(
            model_name='resourceallocation',
            name='exotel_app_id',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Exotel Applet ID'),
        ),
        migrations.AddField(
            model_name='resourceallocation',
            name='remind_driver',
            field=models.BooleanField(default=False, verbose_name='Remind Driver'),
        ),
    ]
