# Generated by Django 2.1.1 on 2019-10-23 09:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0095_auto_20190930_1112'),
    ]

    operations = [
        migrations.AddField(
            model_name='labourconfiguration',
            name='payment_per_labour',
            field=models.FloatField(default=400, help_text='Cost per labour'),
        ),
        migrations.AlterField(
            model_name='labourconfiguration',
            name='cost_per_labour',
            field=models.FloatField(default=400, help_text='Revenue per labour'),
        ),
    ]
