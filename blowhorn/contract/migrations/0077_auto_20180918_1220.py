# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-18 12:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0076_auto_20180906_0814'),
    ]

    operations = [
        migrations.AddField(
            model_name='tripworkflow',
            name='can_scan_orders',
            field=models.BooleanField(default=False, verbose_name='Should Scan Order option be visible in App'),
        ),
        migrations.AlterField(
            model_name='screenbutton',
            name='name',
            field=models.CharField(choices=[('Delivered', 'Deliver'), ('Returned', 'Reject'), ('Unable-To-Deliver', 'Unable To Deliver')], default='meter_reading_start', max_length=50, verbose_name='Field Name'),
        ),
    ]
