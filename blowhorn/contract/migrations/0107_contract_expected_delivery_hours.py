# Generated by Django 2.1.1 on 2020-06-29 08:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0106_tripworkflow_report_to_pickup_hub'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='expected_delivery_hours',
            field=models.TimeField(blank=True, help_text='Maximum Hours for Order Delivery', null=True, verbose_name='Expected Delivery Hours'),
        ),
    ]
