# Generated by Django 2.1.1 on 2020-02-05 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0099_sellrate_charge_for_carrying_goods'),
    ]

    operations = [
        migrations.AddField(
            model_name='sellrate',
            name='apply_flat_pricing',
            field=models.BooleanField(default=False, verbose_name='Apply Flat pricing'),
        ),
    ]
