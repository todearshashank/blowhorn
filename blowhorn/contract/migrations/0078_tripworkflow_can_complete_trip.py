# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-18 22:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0077_auto_20180918_1220'),
    ]

    operations = [
        migrations.AddField(
            model_name='tripworkflow',
            name='can_complete_trip',
            field=models.BooleanField(default=False, verbose_name='Can Driver complete Trip before completion of all stops'),
        ),
    ]
