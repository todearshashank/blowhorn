# Generated by Django 2.1.1 on 2020-06-03 06:58

import blowhorn.common.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0103_tripworkflow_perform_delivery'),
    ]

    operations = [
        migrations.AlterField(
            model_name='screen',
            name='parameters',
            field=blowhorn.common.models.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('toll_charges', 'Capture Toll Charges Proof'), ('parking_charges', 'Capture Parking Charges Proof'), ('start_meter_reading_proof', 'Start Odometer Proof'), ('end_meter_reading_proof', 'End Odometer Proof'), ('scan_qr_code', 'Scan QR Code'), ('arrival_notification', 'Send Driver Arrival Notification to Customer'), ('trip_proof', 'Capture Driver Data Logs'), ('scan_order', 'Scan Order')], max_length=50, null=True, verbose_name='Screen Parameters'), blank=True, null=True, size=None),
        ),
    ]
