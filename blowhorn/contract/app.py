# from django.conf.urls import url
#
# from django.apps import AppConfig
#
# from blowhorn.contract.views.contract import ContractFilter, ListContract,\
#         CalculateMargin, CalculateDatasetMargin, CustomerDivisionView, ContractSearchView
# from blowhorn.contract.views.resource_allocation import (
#     ContractResourceList, ResourceAllocationView, ResourceAllocationSupportData,
#     ResourceAllocationDeleteView, ResourceAllocationHistoryView)
#
#
# class ContractApplication(AppConfig):
#
#     def get_urls(self):
#         urlpatterns = [
#             url(r'^contract/filter$', ContractFilter.as_view(),
#                 name='contract-filter'),
#             url(r'^contracts$', ListContract.as_view(),
#                 name='contract-list'),
#             url(r'^resourceallocation/data$',
#                 ResourceAllocationSupportData.as_view(),
#                 name='resourceallocation-data'),
#             url(r'^resourceallocation/contract$',
#                 ContractResourceList.as_view(),
#                 name='resourceallocation-contracts'),
#             url(r'^resourceallocation/resources/(?P<contract_pk>[0-9]+)$',
#                 ResourceAllocationView.as_view(),
#                 name='resourceallocation-resources'),
#             url(r'^resourceallocation/resources/remove/(?P<resource_pk>[0-9]+)$',
#                 ResourceAllocationDeleteView.as_view(),
#                 name='resourceallocation-resource-remove'),
#             url(r'^resourceallocation/contract/resources/history/(?P<contract_pk>[0-9]+)$',
#                 ResourceAllocationHistoryView.as_view(),
#                 name='resourceallocation-resource-history'),
#             url(r'^margin-calc$', CalculateMargin.as_view(),
#                 name='CalculateMargin'),
#             url(r'^margin-data-calc$', CalculateDatasetMargin.as_view(),
#                 name='CalculateDatasetMargin'),
#             url(r'^getdivision$', CustomerDivisionView.as_view(),
#                 name='getdivision$'),
#             url(r'^contract/search', ContractSearchView.as_view(),
#                 name='contract-search'),
#         ]
#
#         return self.post_process_urls(urlpatterns)
