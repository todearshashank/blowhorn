import logging
from math import ceil
from django.forms.models import model_to_dict

from blowhorn.contract.models import SellShipmentSlab, SellVehicleSlab, SellPincodeShipmentSlab, \
    SellDimensionSlab, SellVASSlab
from blowhorn.contract.constants import (
    BASE_PAYMENT_CYCLE_DAILY,
    BASE_PAYMENT_CYCLE_WEEK,
    BASE_PAYMENT_CYCLE_FORTNIGHTLY,
    BASE_PAYMENT_CYCLE_MONTH,
    BASE_PAYMENT_CYCLE_PER_TRIP,
    AGGREGATE_CYCLE_MONTH,
    AGGREGATE_CYCLE_TRIP,
    AGGREGATE_CYCLE_DAILY,
    VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES,
    VEHICLE_SERVICE_ATTRIBUTE_HOURS,
    VEHICLE_SERVICE_ATTRIBUTE_MINUTES,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
    SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
    SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
    DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS,
    DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS,
    DIMENSION_SERVICE_ATTRIBUTE_GRAMS,
    VAS_SERVICE_ATTRIBUTE_SMS,
    SHIPMENT_SERVICE_ATTRIBUTE_RTO,
    VAS_SERVICE_ATTRIBUTE_CASH_HANDLING,
    SHIPMENT_SERVICE_ATTRIBUTE_OUT_FOR_DELIVERY, VAS_SERVICE_ATTRIBUTE_DEFCOM_SELL_RATE, CONTRACT_TYPE_DEAFCOM
)

SHIPMENT_ATTRIBUTES = [
    VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES,
    VEHICLE_SERVICE_ATTRIBUTE_HOURS,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
    SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
    SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_RTO,
    SHIPMENT_SERVICE_ATTRIBUTE_OUT_FOR_DELIVERY


]

DATE_FORMAT = "%d-%b-%Y"

SLABS = [
    SellVehicleSlab, SellShipmentSlab, SellPincodeShipmentSlab, SellDimensionSlab, SellVASSlab
]


class SellRateHelper(object):

    def __init__(self, sell_rate, start, end, days_count=30, debug=False):
        self.debug = debug
        self.is_ceiling_applicable = False
        self.sell_rate = sell_rate
        self.sku_configured = SellShipmentSlab.objects.filter(
            sell_rate=sell_rate).values_list('sku_id', flat=True).exclude(sku_id__isnull=True)
        pin_code = SellPincodeShipmentSlab.objects.filter(
            sell_rate=sell_rate).values_list('pincodes', flat=True).exclude(pincodes__isnull=True)
        self.pin_codes_list = [pin.split(',') for pin in pin_code]
        self.days_count = days_count
        self.base_pay_distribution_from_slabs = {}
        self.trip_wise_calculation = False
        self.start = start
        self.end = end

    def calculate_basic_pay(self, trip_set, days_this_month=30):
        """
        Returns the total basic amount for given date range.
        (Based on pro rata calculations.)
        """

        base_pay_cycles = [
            BASE_PAYMENT_CYCLE_DAILY, BASE_PAYMENT_CYCLE_WEEK, BASE_PAYMENT_CYCLE_FORTNIGHTLY,
            BASE_PAYMENT_CYCLE_MONTH, BASE_PAYMENT_CYCLE_PER_TRIP
        ]

        base_pay_interval = self.sell_rate.base_pay_interval
        assert base_pay_interval in base_pay_cycles, "Base pay Interval is incorrect."

        base_pay_per_cycle = float(self.sell_rate.base_pay) if self.sell_rate.base_pay else 0

        if base_pay_interval in [
            BASE_PAYMENT_CYCLE_DAILY, BASE_PAYMENT_CYCLE_WEEK, BASE_PAYMENT_CYCLE_FORTNIGHTLY,
            BASE_PAYMENT_CYCLE_MONTH]:
            base_amount_per_day = 0.0

            if base_pay_interval == BASE_PAYMENT_CYCLE_DAILY:
                base_amount_per_day = round(base_pay_per_cycle, 2)
            elif base_pay_interval in [BASE_PAYMENT_CYCLE_WEEK,
                                       BASE_PAYMENT_CYCLE_FORTNIGHTLY, BASE_PAYMENT_CYCLE_MONTH]:
                # base_amount_per_day = round(base_pay_per_cycle / 7, 2)
                if days_this_month:
                    base_amount_per_day = round(base_pay_per_cycle / days_this_month, 2)
                else:
                    base_amount_per_day = 0
                # base_amount_per_day = round(base_pay_per_cycle, 2) / days_this_month
            # elif base_pay_interval == BASE_PAYMENT_CYCLE_FORTNIGHTLY:
            #     base_amount_per_day = round(base_pay_per_cycle, 2) / days_this_month
            # elif base_pay_interval == BASE_PAYMENT_CYCLE_MONTH:
            #     if days_this_month:
            #     base_amount_per_day = round(base_pay_per_cycle / days_this_month, 2)
            # else:
            #     base_amount_per_day = 0

            count_work_days_by_driver = trip_set.get_count_work_days_driver_wise()

            base_pay_by_driver = {}
            total_charges, total_days_count = 0, 0
            for driver_id, days_count in count_work_days_by_driver.items():
                base_pay_by_driver[driver_id] = days_count * base_amount_per_day
                total_charges += base_pay_by_driver[driver_id]
                total_days_count += days_count

            calculation_data = {
                "distinct_drivers": len(count_work_days_by_driver),
                "count_days": total_days_count,
                "amount_per_day": base_amount_per_day,
                "amount_total": float(round(total_charges, 2)),
                "distribution_days": count_work_days_by_driver,
                "distribution_amount": base_pay_by_driver,
                "attribute_actual": "days"
            }

            return round(total_charges, 2), calculation_data
        elif base_pay_interval == BASE_PAYMENT_CYCLE_PER_TRIP:
            base_amount_per_trip = base_pay_per_cycle
            count_trips_by_driver = trip_set.get_count_trips_driver_wise()

            base_pay_by_driver = {}
            total_charges, total_trips_count = 0, 0
            for driver_id, trips_count in count_trips_by_driver.items():
                base_pay_by_driver[driver_id] = trips_count * base_amount_per_trip
                total_charges += base_pay_by_driver[driver_id]
                total_trips_count += trips_count

            calculation_data = {
                "distinct_drivers": len(count_trips_by_driver),
                "count_days": total_trips_count,
                "amount_per_day": base_amount_per_trip,
                "amount_total": round(total_charges, 2),
                "distribution_days": count_trips_by_driver,
                "distribution_amount": base_pay_by_driver,
                "attribute_actual": "trips"
            }
            return round(total_charges, 2), calculation_data

    def _get_slab_amount(self, slab, value, all_slabs=[]):
        amount = 0
        value_extra = 0
        fixed_amount = 0
        if slab.end is None:
            print('End slab is None. Skipping slab calculation')
            return amount, value_extra, fixed_amount

        if slab.is_ceiling_applicable:
            self.is_ceiling_applicable = True
            value = ceil(value)

        if self.sell_rate.apply_flat_pricing:
            return self._get_slab_flat_pricing_amount(slab, value, all_slabs=all_slabs)

        # range is not considering the float values
        if slab.start <= value <= slab.end: #value in range(slab.start, slab.end + 1):
            value_extra = value - slab.start
            amount += float(value_extra) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount
        elif value > slab.end and self.sell_rate.apply_step_function:
            value_extra = slab.end - slab.start
            amount += float(value_extra) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount
        elif value > slab.start and self.sell_rate.apply_step_function:
            value_extra = (float(value) - float(slab.start))
            amount += float(value_extra) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount

        return amount, value_extra, fixed_amount

    def _get_best_fit_slab(self, current_slab, value, all_slabs=[]):
        """

        :param current_slab:
        :param value:
        :param all_slabs: slabs defined on each contracts
        :return: best fit slab
        """
        best_end = None
        current_diff = None
        for slab in all_slabs:
            if slab.attribute == current_slab.attribute:
                new_diff = slab.end - value
                if new_diff >= 0 and value >= slab.start:
                    if not best_end:
                        best_end = slab.end
                        current_diff = new_diff
                    if current_diff and new_diff < current_diff:
                        best_end = slab.end
        return best_end

    def _get_slab_flat_pricing_amount(self, slab, value, all_slabs=[]):
        amount = 0
        value_extra = 0
        fixed_amount = 0
        best_end = self._get_best_fit_slab(slab, value, all_slabs=all_slabs)

        if best_end and slab.end == best_end:
            value_extra = value - slab.start
            amount += float(value_extra) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount

        return amount, value_extra, fixed_amount

    def _get_attribute_value(self, date_start, date_end, slab, trip_set=None, order_set=None):
        interval = slab.interval
        attribute = slab.attribute
        sku = slab.sku if hasattr(slab, 'sku') else None
        aggregate_level = slab.aggregate_level
        values = None

        if trip_set:
            if interval == AGGREGATE_CYCLE_TRIP:
                if attribute == VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES:
                    values = trip_set.get_tripwise_distances()
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                    values = trip_set.get_tripwise_times_in_hrs()
                elif attribute in SHIPMENT_ATTRIBUTES:
                    values = trip_set.get_tripwise_shipmentvalue(attribute)
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_MINUTES:
                    values = trip_set.get_tripwise_times_in_min()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS:
                    values = trip_set.get_tripwise_weights()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_GRAMS:
                    values = trip_set.get_tripwise_grams()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS:
                    values = trip_set.get_tripwise_volumes()
                elif hasattr(slab, 'vas_attribute'):
                    if slab.vas_attribute.attribute == VAS_SERVICE_ATTRIBUTE_DEFCOM_SELL_RATE and \
                            trip_set.contract.contract_type == CONTRACT_TYPE_DEAFCOM:
                        values = trip_set.get_tripwise_defcom_vas_slab()
                assert values is not None, 'Attribute values cannot be None'

            else:
                # Calculations would be on top of `trip_set`
                if attribute == VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES:
                    values = trip_set.get_daywise_distances()
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                    values = trip_set.get_daywise_triptimes()
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_MINUTES:
                    values = trip_set.get_day_wise_trip_minutes()
                elif attribute in SHIPMENT_ATTRIBUTES:
                    values = trip_set.get_daywise_shipmentvalue(attribute)
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS:
                    values = trip_set.get_daywise_weights()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS:
                    values = trip_set.get_daywise_volumes()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_GRAMS:
                    values = trip_set.get_daywise_grams()
                elif hasattr(slab, 'vas_attribute'):
                    if slab.vas_attribute.attribute == VAS_SERVICE_ATTRIBUTE_DEFCOM_SELL_RATE and \
                            trip_set.contract.contract_type == CONTRACT_TYPE_DEAFCOM:
                        values = trip_set.get_daywise_vas_slab()

                assert values is not None, 'Attribute values cannot be None'
        if order_set:
            if attribute in [VEHICLE_SERVICE_ATTRIBUTE_HOURS, VEHICLE_SERVICE_ATTRIBUTE_MINUTES,
                             VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES]:
                single_customer_stop_trips = order_set.has_single_customer_trips()
                assert single_customer_stop_trips is True, 'Please change contract to exclude vehicle slab'
            if interval == AGGREGATE_CYCLE_TRIP:
                if attribute == VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES:
                    values = order_set.get_orderwise_distances(date_start, date_end, aggregate_level)
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                    values = order_set.get_orderwise_times(date_start, date_end, aggregate_level)
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_MINUTES:
                    values = order_set.get_orderwise_times(date_start, date_end, aggregate_level, hours=False)
                elif attribute in SHIPMENT_ATTRIBUTES:
                    if sku:
                        values = order_set.get_orderwise_sku(slab, date_start, date_end)
                    else:
                        # considering seconds will make the date to previous day
                        # so trimming it.
                        start = self.start.strftime('%Y-%m-%d %H:%M')
                        end = self.end.strftime('%Y-%m-%d %H:%M')
                        values = order_set.get_orderwise_shipmentvalue(slab, start, end)
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS:
                    values = order_set.get_orderwise_weights()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS:
                    values = order_set.get_orderwise_volumes()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_GRAMS:
                    values = order_set.get_orderwise_grams()
                elif hasattr(slab, 'vas_attribute'):
                    if slab.vas_attribute.attribute == VAS_SERVICE_ATTRIBUTE_SMS:
                        values = order_set.get_orderwise_sms_count()
                    elif slab.vas_attribute.attribute == VAS_SERVICE_ATTRIBUTE_CASH_HANDLING:
                        values = order_set.get_orderwise_cash_collected()
            else:
                # Calculations would be on top of `order_set`
                if attribute == VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES:
                    values = order_set.get_daywise_distances(date_start, date_end, aggregate_level)
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                    values = order_set.get_daywise_times(date_start, date_end, aggregate_level)
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_MINUTES:
                    values = order_set.get_daywise_times(date_start, date_end, aggregate_level, hours=False)
                elif attribute in SHIPMENT_ATTRIBUTES:
                    if sku:
                        values = order_set.get_daywise_sku(slab, date_start, date_end)
                    else:
                        # considering seconds will make the date to previous day
                        # so trimming it.
                        start = self.start.strftime('%Y-%m-%d %H:%M')
                        end = self.end.strftime('%Y-%m-%d %H:%M')
                        values = order_set.get_daywise_shipmentvalue(slab, start, end)
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS:
                    values = order_set.get_daywise_weights()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS:
                    values = order_set.get_daywise_volumes()
                elif attribute == DIMENSION_SERVICE_ATTRIBUTE_GRAMS:
                    values = order_set.get_daywise_grams()
                elif hasattr(slab, 'vas_attribute'):
                    if slab.vas_attribute.attribute == VAS_SERVICE_ATTRIBUTE_SMS:
                        values = order_set.get_day_wise_sms_count()
                    elif slab.vas_attribute.attribute == VAS_SERVICE_ATTRIBUTE_CASH_HANDLING:
                        values = order_set.get_day_wise_cash_collected()

            if values is None:
                assert False, 'Attribute values cannot be None'

        if values and interval in [AGGREGATE_CYCLE_MONTH]:
            return sum([x[1] for x in values])

        return values

    def _get_attributes_from_slab(self, slab, sku):
        if isinstance(slab, SellVASSlab):
            attribute = slab.vas_attribute.attribute
        elif sku:
            attribute = str(sku.name)
        else:
            attribute = str(slab.attribute)
        return attribute

    def _get_amount_for_given_slabs(self, date_start, date_end, slabs, trip_set=None, order_set=None, all_slabs=[]):

        assert trip_set != None or order_set != None, "Either `trip_set` or `order_set` is required."

        amount = 0
        fixed_amount = 0
        trip = None
        calculation = []

        for slab in slabs:
            slab.sku_configured = self.sku_configured
            slab.pin_codes_list = self.pin_codes_list
            attr_values = self._get_attribute_value(date_start, date_end,
                                                    slab, trip_set=trip_set, order_set=order_set)
            slab_display = model_to_dict(slab)
            slab_display.update({"name": slab._meta.label})
            sku = slab.sku if hasattr(slab, "sku") else None
            slab_display.update({"sku_name": sku.name if sku else ''})
            attr = self._get_attributes_from_slab(slab, sku)

            if slab.interval == AGGREGATE_CYCLE_TRIP:
                for day, value, trip in attr_values:
                    slab_amount, value_extra, fixed_amount_ = self._get_slab_amount(slab, value, all_slabs=all_slabs)
                    fixed_amount += fixed_amount_
                    amount = float(amount) + float(slab_amount)
                    calculation.append({
                        'attribute': attr,
                        'slab': slab_display,
                        'date': day.strftime(DATE_FORMAT),
                        'value': value,
                        'amount': slab_amount,
                        'value_extra': value_extra
                    })
            else:
                if isinstance(attr_values, list):

                    for day, value in attr_values:
                        slab_amount, value_extra, fixed_amount_ = self._get_slab_amount(slab, value, all_slabs=all_slabs)
                        fixed_amount += fixed_amount_
                        amount = float(amount) + float(slab_amount)
                        calculation.append({
                            'attribute': attr,
                            'slab': slab_display,
                            'date': day.strftime(DATE_FORMAT),
                            'value': value,
                            'amount': slab_amount,
                            'value_extra': value_extra
                        })

                else:
                    slab_amount, value_extra, fixed_amount_ = self._get_slab_amount(slab, attr_values, all_slabs=all_slabs)
                    amount += slab_amount
                    fixed_amount += fixed_amount_
                    calculation.append({
                        'attribute': attr,
                        'slab': slab_display,
                        'value': attr_values,
                        'amount': slab_amount,
                        'value_extra': value_extra
                    })

        return amount, calculation, fixed_amount, trip

    def calculate_service_charges(self, date_start, date_end, trip_set=None, order_set=None,
                                  slab_types_applicable=[], base_pay_distribution_from_slabs={}):
        """
        Returns service charges for given `trip_set` (or `order_set`) and slabs.
        """
        assert trip_set != None or order_set != None, "Either `trip_set` or `order_set` is required."

        if not slab_types_applicable:
            slab_types_applicable = SLABS

        amount_total = 0
        calculation = []

        if trip_set:
            # Calculation basis the given `trip_set`. (Default flow)
            for slab_type in slab_types_applicable:
                all_slabs = slab_type.objects.filter(sell_rate=self.sell_rate)
                for slab in all_slabs:
                    if not hasattr(slab, "aggregate_level"):
                        all_trip_set_buckets = [{"aggregate_value": None, "trip_set": trip_set}]
                        aggregate_level = None
                    else:
                        aggregate_level = slab.aggregate_level
                        all_trip_set_buckets = trip_set.get_trips_by_aggregate_level(aggregate_level)

                    # Applying slabs on each trip_set grouped by aggregate level.
                    total = 0
                    for trip_set_bucket in all_trip_set_buckets:
                        _trip_set = trip_set_bucket["trip_set"]
                        aggregate_value = trip_set_bucket["aggregate_value"]
                        amount, calc, fixed_amount, trip = self._get_amount_for_given_slabs(date_start, date_end,
                                                                                            slabs=[slab],
                                                                                            all_slabs=all_slabs,
                                                                                            trip_set=_trip_set)

                        # logging.info("Aggregation : %s %s " % (aggregate_level, aggregate_value))
                        # logging.info("Calculation (%s  (%s)) : %s and Fixed Amount: %s" % (
                        #     aggregate_level, trip_set_bucket["aggregate_value"], calc, fixed_amount))

                        if amount > 0:
                            calc_cleaned = []
                            for e in calc:
                                if e['amount'] > 0:
                                    e.update({"aggregate_value": aggregate_value})
                                    calc_cleaned.append(e)
                            calculation.append(calc_cleaned)
                        total = float(total) + float(amount)

                        if fixed_amount:

                            # This is basis an assumption that `fixed` part of slab would only come for `driver` level aggregation.
                            amount_as_per_pro_rata = fixed_amount / self.days_count
                            key = str(str(slab._meta.app_label) + "-" + str(slab._meta.model_name))
                            if not key in base_pay_distribution_from_slabs.keys():
                                base_pay_distribution_from_slabs[key] = {}

                            if not (slab.attribute, aggregate_level, aggregate_value, self.sell_rate.id) in \
                                   base_pay_distribution_from_slabs[key].keys():
                                # for each contract the amount will be calculated
                                # so the grouping must have unique key so adding sellrate
                                base_pay_distribution_from_slabs[key][
                                    (slab.attribute, aggregate_level, aggregate_value, self.sell_rate.id)] = 0

                            if slab.interval in [AGGREGATE_CYCLE_TRIP, AGGREGATE_CYCLE_DAILY]:
                                self.trip_wise_calculation = True
                                amount_as_per_pro_rata = fixed_amount
                                base_pay_distribution_from_slabs[key][
                                    (slab.attribute, aggregate_level, aggregate_value, self.sell_rate.id)] += amount_as_per_pro_rata
                            else:
                                base_pay_distribution_from_slabs[key][
                                    (slab.attribute, aggregate_level, aggregate_value, self.sell_rate.id)] += amount_as_per_pro_rata

                    # logging.info(
                    #     "Calculating service charges for slab %s %s with aggregate level %s and aggregated groups count %s \
                    #     totalled to %s" % (slab_type, slab.id, aggregate_level, len(all_trip_set_buckets), total)
                    # )
                    amount_total = float(amount_total) + float(total)

            return round(amount_total, 2), calculation, base_pay_distribution_from_slabs

        else:
            total_fixed_amt = 0
            # Calculation basis the given `order_set`. (Shipments flow)
            print("Calculation basis the given `orders`. (Shipments flow)")
            main_order_set = order_set
            for slab_type in slab_types_applicable:
                all_slabs = slab_type.objects.filter(sell_rate=self.sell_rate)
                for slab in all_slabs:
                    if not hasattr(slab, "aggregate_level"):
                        all_buckets = [{"aggregate_value": None, "order_set": main_order_set}]
                        aggregate_level = None
                    else:
                        aggregate_level = slab.aggregate_level
                        all_buckets = main_order_set.get_orders_by_aggregate_level(aggregate_level)

                    # Applying slabs on each order_set grouped by aggregate level.
                    total = 0
                    for order_set_bucket in all_buckets:
                        order_set = order_set_bucket["order_set"]
                        aggregate_value = order_set_bucket["aggregate_value"]
                        amount, calc, fixed_amount, trip = self._get_amount_for_given_slabs(date_start, date_end,
                                                                                            slabs=[slab],
                                                                                            order_set=order_set,
                                                                                            all_slabs=all_slabs)
                        total_fixed_amt += float(fixed_amount)

                        if amount > 0:
                            calc_cleaned = []
                            for e in calc:
                                if e['amount'] > 0:
                                    e.update({"aggregate_value": aggregate_value})
                                    calc_cleaned.append(e)
                            calculation.append(calc_cleaned)
                        total += amount

                    amount_total += total


            return round(amount_total, 2), calculation, base_pay_distribution_from_slabs, total_fixed_amt
