from blowhorn.contract.models import Contract
from blowhorn.contract.constants import (
    CONTRACT_TYPE_C2C,
)


class ContractHelper(object):

    def get_contract(self, city, vehicle_class):
        """
        Returns contract for given city and vehicle class
        """
        contract = Contract.objects.filter(city=city,
                                           vehicle_classes=vehicle_class,
                                           contract_type__in=[
                                               CONTRACT_TYPE_C2C
                                           ])

        contract = contract.prefetch_related('contractterm_set__sell_rate')

        contract = contract.prefetch_related(
            'contractterm_set__sell_rate__sellvehicleslab_set')
        contract = contract.prefetch_related(
            'contractterm_set__sell_rate__sellshipmentslab_set')

        self.contract = contract

        return contract

    def get_contract_from_id(self, id):
        """
        Returns contract for given city and vehicle class
        """
        contract = Contract.objects.filter(id=id)

        contract = contract.prefetch_related('contractterm_set__sell_rate')

        contract = contract.prefetch_related(
            'contractterm_set__sell_rate__sellvehicleslab_set')
        contract = contract.prefetch_related(
            'contractterm_set__sell_rate__sellshipmentslab_set')

        self.contract = contract

        return contract

    def get_contract_term(self, contract_terms, date):
        """ Get the applicable contract term for given date """
        terms_before = [term for term in contract_terms if term.wef <= date]
        if not terms_before:
            return None

        sorted_terms = sorted(terms_before, key=lambda x: x.wef)
        return sorted_terms[-1]

    def get_sell_rate(self, contract_terms, date):
        """ Get the applicable sell rate for given date """
        term = self.get_contract_term(contract_terms, date)
        if not term:
            return None

        sell_rate = term.sell_rate
        return sell_rate

    def get_buy_rate(self, contract_terms, date):
        """ Get the applicable buy rate for given date """
        term = self.get_contract_term(contract_terms, date)
        if not term:
            return None

        buy_rate = term.buy_rate
        return buy_rate
