import datetime
from babel import numbers
from django.utils import timezone, translation
from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from recurrence.fields import RecurrenceField
from django.core.validators import RegexValidator
from collections import defaultdict
from blowhorn.oscar.core.validators import non_python_keyword
from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField
from datetime import timedelta
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from blowhorn.oscar.models.fields import NullCharField
from postgres_copy import CopyManager

from blowhorn.common.models import BaseModel
from blowhorn.driver.models import Driver
from blowhorn.users.models import User
from django_fsm import FSMField, transition
from blowhorn.contract.permissions import is_sales_representative
from .status_conditions import can_activate, can_deactivate
from django.core.exceptions import ValidationError
from blowhorn.driver.models import DriverPayment
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.constants import (
    CONTRACT_TYPE_FIXED,
    CONTRACT_TYPE_HYPERLOCAL,
    CONTRACT_TYPE_SME,
    CONTRACT_TYPE_BLOWHORN,
    CONTRACT_TYPE_KIOSK,
    CONTRACT_TYPE_SHIPMENT,
    CONTRACT_TYPE_C2C,
    CONTRACT_TYPE_SME,
    CONTRACT_TYPE_ONDEMAND,
    CONTRACT_TYPES,
    MESSAGE_FORMATS,
    CITY,
    HUB,
    DRIVER,
    VEHICLE_CLASS,
    SCREEN_FIELDS_DATA_TYPES,
    START_METER_READING,
    SCREEN_FIELD_NAME,
    INTEGER,
    SCREEN_BUTTON_PARAMETER_NAME,
    SCREEN_NAME,
    START_TRIP,
    SCREEN_BUTTON_NAME,
    SCREEN_PARAMETERS,
    SCREEN_FIELD_PRIORITY,
    CONTRACT_TYPE_FLASH_SALE,
    CURRENCY_CHOICES

)
from blowhorn.oscar.core.loading import get_model
from blowhorn.contract.constants import (
    BASE_PAYMENT_CYCLE_DAILY,
    BASE_PAYMENT_CYCLE_PER_TRIP,
    BASE_PAYMENT_CYCLE_WEEK,
    BASE_PAYMENT_CYCLE_FORTNIGHTLY,
    BASE_PAYMENT_CYCLE_MONTH,
    CONTRACT_STATUS_ACTIVE,
    CONTRACT_STATUS_DRAFT,
    CONTRACT_STATUS_INACTIVE,
    #    CONTRACT_STATUSES,
    BUY_RATE_AGGREGATE_CYCLE_OPTIONS,
    SELL_RATE_AGGREGATE_CYCLE_OPTIONS,
    AGGREGATE_CYCLE_MONTH,
    PAY_CYCLE_CYCLE,
    PAY_CYCLE_OPTIONS,
    VEHICLE_SERVICE_ATTRIBUTES,
    SHIPMENT_SERVICE_ATTRIBUTES,
    # C2C_VEHICLE_SERVICE_ATTRIBUTES,
    DIMENSION_SERVICE_ATTRIBUTES,
    VAS_SERVICE_ATTRIBUTES,
    VAS_SERVICE_ATTRIBUTE_POD,
    NT_SERVICE_ATTRIBUTES,
    SERVICE_TAX_CATEGORIES,
    VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES,
    VEHICLE_SERVICE_ATTRIBUTE_HOURS,
    VEHICLE_SERVICE_ATTRIBUTE_MINUTES,

)
from django.db.models.signals import m2m_changed, post_delete, post_save
from django.dispatch import receiver
from blowhorn.common.tasks import send_async_mail
from blowhorn.common.models import ChoiceArrayField
from blowhorn.trip.models import TripConstants
from blowhorn.common.helper import CommonHelper
from blowhorn.customer.models import CustomerInvoice, SKU, Route
attribute_display = {"minutes": " min", "kilometres": " km", "hours": "hr"}
attribute_display_map = {"minutes": " mins", "kilometres": " kms",  "hours": "hr"}

AGGREGATE_LEVEL_CHOICES = (
        (CITY, CITY),
        (HUB, HUB),
        (DRIVER, DRIVER),
        (VEHICLE_CLASS, VEHICLE_CLASS),
    )

class PaymentCycle(BaseModel):
    """
    A custom payment schedule to handle limitations in RecurrenceField
    e.g.
    Fortnightly Custom Schedule
    2 intervals
        1. 1 - 15
        2. 16 - 31
    """

    history = AuditlogHistoryField()

    name = models.CharField(
        _('Base Payment Cycle'), max_length=50, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Payment Cycle')
        verbose_name_plural = _('Payment Cycles')


auditlog.register(PaymentCycle)


class PaymentInterval(models.Model):
    """
    Interval - One Pair (start, end)
    """

    history = AuditlogHistoryField()

    cycle = models.ForeignKey(PaymentCycle, on_delete=models.PROTECT)

    start_day = models.IntegerField(null=False, db_index=True)

    end_day = models.IntegerField(null=False, db_index=True)

    def __str__(self):
        return u'%d-%d' % (self.start_day, self.end_day)

    class Meta:
        verbose_name = _('Payment Cycle')
        verbose_name_plural = _('Payment Cycles')


auditlog.register(PaymentInterval)


class AbstractSlab(models.Model):
    """
    Abstract class for storing slabs
    """

    history = AuditlogHistoryField()

    when_to_pay = models.CharField(_('When To Pay'),
                                   max_length=25, choices=PAY_CYCLE_OPTIONS,
                                   default=PAY_CYCLE_CYCLE)

    split_payment = models.BooleanField(_("Split"), default=False)

    interval = models.CharField(
        _('For'), max_length=25, choices=BUY_RATE_AGGREGATE_CYCLE_OPTIONS,
        default=AGGREGATE_CYCLE_MONTH)

    step_size = models.IntegerField(_('Step Size'),
                                    default=1)

    start = models.IntegerField(_("Start"))

    end = models.IntegerField(_("End"), null=True)

    rate = models.DecimalField(
        _("Rate"), max_digits=10, decimal_places=2, default=0)

    fixed_amount = models.DecimalField(
        _("Fixed Amount"), max_digits=10, decimal_places=2, default=0)

    is_ceiling_applicable = models.BooleanField(
        _("Ceiling Applicable"), default=False, db_index=True)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.id)


class AbstractSellSlab(AbstractSlab):

    aggregate_level = models.CharField(
        _('Aggregate Level'), max_length=25,
        choices=AGGREGATE_LEVEL_CHOICES, default=CITY)

    interval = models.CharField(
        _('For'), max_length=25, choices=SELL_RATE_AGGREGATE_CYCLE_OPTIONS,
        default=AGGREGATE_CYCLE_MONTH)

    class Meta:
        abstract = True

BASE_PAYMENT_OPTIONS = (
    (BASE_PAYMENT_CYCLE_DAILY, _('Daily')),
    (BASE_PAYMENT_CYCLE_PER_TRIP, _('Per-Trip')),
    (BASE_PAYMENT_CYCLE_WEEK, _('Weekly')),
    (BASE_PAYMENT_CYCLE_FORTNIGHTLY, _('FortNightly')),
    (BASE_PAYMENT_CYCLE_MONTH, _('Month')),
)


class BuyRate(BaseModel):
    """
    Buy rate to be defined by Operations once the SLA is setup
    with which you would pay the driver
    """
    history = AuditlogHistoryField()

    handle = models.CharField(
        _('Rate Card Handle'), max_length=100, null=True, blank=True)

    name = models.CharField(
        _('Rate Card Name'), max_length=50, unique=True)

    base_payment_interval = models.CharField(_('Base Payment Interval'),
                                             max_length=25,
                                             choices=BASE_PAYMENT_OPTIONS,
                                             default=AGGREGATE_CYCLE_MONTH)

    driver_base_payment_amount = models.DecimalField(
        _('Driver Base Payment Amount'), max_digits=10, decimal_places=2,
        default=0)

    driver_deduction_per_leave = models.DecimalField(
        _('Driver Deduction Amount Per Leave'), max_digits=10,
        decimal_places=2, default=0)
    apply_step_function = models.BooleanField(_('Consider Multiple slabs?'),
                                              default=False)
    objects = models.Manager()

    def __str__(self):
        return str(self.id) + " - " + str(self.handle)

    def __init__(self, *args, **kwargs):
        """ overriding the init method as we want to detect changes
            in important fields when saving
        """
        super().__init__(*args, **kwargs)
        self.__important_fields = [
            'base_payment_interval',
            'driver_base_payment_amount',
            'driver_deduction_per_leave'
        ]
        for field in self.__important_fields:
            setattr(self, '__original_%s' % field, getattr(self, field))

    def have_important_fields_changed(self):
        for field in self.__important_fields:
            orig = '__original_%s' % field
            if getattr(self, orig) != getattr(self, field):
                return True
        return False

    def save(self, *args, **kwargs):
        # Mark Driver Payments as stale when important fields in the
        # rate card are updated
        if self:
            kms_slab = self.buyvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES).order_by('start')
            min_slab = self.buyvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_MINUTES).order_by('start')
            hrs_slab = self.buyvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_HOURS).order_by('start')

            rate_string = ""
            start_string = ""
            if kms_slab:
                kms_slab = kms_slab[0]
                start_string = start_string + str(kms_slab.start) + "Km_"
                rate_string = rate_string + str(kms_slab.rate)
            if min_slab:
                min_slab = min_slab[0]
                start_string = start_string + str(min_slab.start) + "Mins_"
                rate_string = rate_string + "_" + str(min_slab.rate)
            if hrs_slab:
                hrs_slab = hrs_slab[0]
                start_string = start_string + str(hrs_slab.start) + "Hrs_"
                rate_string = rate_string + "_" + str(hrs_slab.rate)

            self.handle = "Buy"

            if self.driver_base_payment_amount:
                self.handle = self.handle + "_" + str(self.driver_base_payment_amount)
                if len(rate_string)>0 or len(start_string)>0:
                    self.handle = self.handle + "_" + start_string + rate_string
            else:
                if len(rate_string)>0 or len(start_string)>0:
                    self.handle = self.handle + "_" + start_string + rate_string
                else:
                    self.handle = self.handle + "_Rate_Shipment_VAS" + str(self.id)

        if self.have_important_fields_changed():
            print('Important Fields changed. Marking Driver Payment Records as stale')
            DriverPayment.objects.filter(buy_rate=self).update(is_stale=True,
                                                               stale_reason='Buy rate fields changed')
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('Driver Buy Rate')
        verbose_name_plural = _('Driver Buy Rates')


auditlog.register(BuyRate)


class SellRate(BaseModel):
    """
    Sell rate to be defined by Sales once the SLA is setup
    with which you would invoice the customer
    """
    history = AuditlogHistoryField()

    name = models.CharField(
        _('Rate Card Name'), max_length=50,
        help_text="Readable rate card name. This will be displayed on "
                  "customer portals / apps.")

    handle = models.CharField(
        _("Rate Card Handle"), max_length=100, null=True, blank=True)

    base_pay = models.DecimalField(
        _('Base pay (per driver)'), max_digits=10, decimal_places=2,
        null=True, blank=True)

    base_pay_interval = models.CharField(
        _('Base pay Interval (per driver)'),
        max_length=25, choices=BASE_PAYMENT_OPTIONS,
        default=BASE_PAYMENT_CYCLE_MONTH
    )

    call_rate = models.DecimalField(
        _('Call Masking Charge Percentage'), max_digits=10, decimal_places=2,
        null=True, blank=True,
        help_text="100% of call charges added if blank or 0 else for example if 30"
                  " is entered then 130% of call charges added to invoice")

    apply_step_function = models.BooleanField(_('Consider Multiple slabs?'),
                                              default=False)
    apply_flat_pricing = models.BooleanField(_('Apply Flat pricing'),
                                             default=False)

    charge_for_carrying_goods = models.DecimalField(
        _('Charge for carrying goods'), max_digits=10, decimal_places=2,
        null=True, blank=True)
    objects = models.Manager()

    @property
    def display_text(self):
        return str(self.id) + " - " + str(self.handle)

    def get_rate(self, slab, temp_result, slab_count, count, package_extra):
        temp_result[slab.attribute].append(
            {
                "above": float(slab.start),
                "for_every": float(slab.step_size),
                "rate": float(slab.rate),
                "title": "Travel charge after %s  : INR %s" % (str(slab.start) +' '+ str(
                                                                     attribute_display.get(slab.attribute.lower())),
                                                                 str(slab.rate).replace('.00', '') + '/' + str(
                                                                     attribute_display.get(slab.attribute.lower()))
                                                                 )
                if slab_count == (count+1) else ("Travel charge for %s-%s : INR %s" % (
                    str(slab.start),
                    str(slab.end) + str(attribute_display.get(slab.attribute.lower())),
                    str(slab.rate).replace('.00', '') + '/' + str(attribute_display.get(slab.attribute.lower())),

                )
                                      )
            }
        )

        package_extra.append(
            {
                "rate_title": "%s" % (str(slab.rate).replace('.00', '') + '/' + str(
                    attribute_display.get(slab.attribute.lower()))),
                "rate_desc": "Travel charge after %s" % (str(slab.start) + ' ' + str(
                    attribute_display.get(slab.attribute.lower())) if (slab is not None) else '')
                if slab_count == (count + 1) else ("Travel charge for %s-%s" % (
                    str(slab.start),
                    str(slab.end) + str(attribute_display.get(slab.attribute.lower())),
                                                  )
                                                   )
            }
        )

    def get_display_description(self, output=dict):

        def humanized_rate(value, unit_plural, detailed=False):
            """
            Returns a readable text for the given rate and unit.
            """

            if value != 1:
                result = str(value) + " " + unit_plural
            else:
                result = "1 " + \
                    unit_plural[:-1] if detailed else "" + unit_plural[:-1]
            return result

        result = None
        if output == dict:
            # ensure plurality.


            temp_result = defaultdict(list)
            package_extra_km = []
            package_extra_min = []

            kms_slab = self.sellvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES).order_by('start')
            min_slab = self.sellvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_MINUTES).order_by('start')
            hrs_slab = self.sellvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_HOURS).order_by('start')
            vas = self.sellvasslab_set.all()
            vas = vas.select_related('vas_attribute')

            kms_count = kms_slab.count()
            min_count = min_slab.count()
            hrs_count = hrs_slab.count()

            result = {
                "base": {
                    "amount": float(self.base_pay or 0),
                    "interval": "",
                    "text": str(self.base_pay).replace('.00', '') + " *",
                    "package_title": self.contract.description if hasattr(self, 'contract') else '',
                    "base_rate_title": str(self.base_pay).replace('.00', ''),
                    "base_rate_desc": "Base charges for %s km and %s mins" % (kms_slab[0].start if kms_slab else 0,
                                                                              min_slab[0].start if min_slab else 0)
                },
                "extra": [],
                "package_extra_km": [],
                "package_extra_min": [],
            }

            for count, slab in enumerate(kms_slab):
                self.get_rate(slab, temp_result, kms_count, count, package_extra_km)

            for count, slab in enumerate(min_slab):
                self.get_rate(slab, temp_result, min_count, count, package_extra_min)

            for count, slab in enumerate(hrs_slab):
                self.get_rate(slab, temp_result, hrs_count, count, package_extra_min)

            for slab in vas:
                if slab.vas_attribute.attribute == VAS_SERVICE_ATTRIBUTE_POD:
                    # POD is fixed amount it cannot be the slabs
                    continue
                display = (slab.vas_attribute.display_attribute or slab.vas_attribute.attribute).lower()
                temp_result[display].append(
                    {
                        "above": float(slab.start),
                        "for_every": float(slab.step_size),
                        "rate": float(slab.rate),
                        "title": "Night Travel charge for (%s-%s)  : INR %s" % (
                                    self.city.night_hours_start_time.strftime('%I:%M %p'),
                                    self.city.night_hours_end_time.strftime('%I:%M %p'),
                                    str(slab.rate).replace('.00', '') + '/' + str(attribute_display.get(display)),

                                )

                    }
                )
                package_extra_min.append(
                    {
                        "rate_title": "%s" % (str(slab.rate).replace('.00', '') + '/' + str(attribute_display.get(display))),
                        "rate_desc": "Night Travel charge for (%s-%s)" % (
                                    self.city.night_hours_start_time.strftime('%I:%M %p'),
                                    self.city.night_hours_end_time.strftime('%I:%M %p'))
                    }
                )

            # Sorting the slabs by `start`
            for attribute, data in temp_result.items():
                result['extra'].append({
                    'attribute': attribute,
                    'detail': data
                })

            result['package_extra_km'] = package_extra_km
            result['package_extra_min'] = package_extra_min

        return result

    display_description = property(get_display_description)

    def __str__(self):
        return str(self.id) + " - " + str(self.handle)

    def __init__(self, *args, **kwargs):
        """ overriding the init method as we want to detect changes
            in important fields when saving
        """
        super().__init__(*args, **kwargs)
        self.__important_fields = [
            'base_pay',
            'base_pay_interval'
        ]
        for field in self.__important_fields:
            setattr(self, '__original_%s' % field, getattr(self, field))

    def have_important_fields_changed(self):
        for field in self.__important_fields:
            orig = '__original_%s' % field
            if getattr(self, orig) != getattr(self, field):
                return True
        return False

    def save(self, *args, **kwargs):
        if self:
            kms_slab = self.sellvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES).order_by('start')
            min_slab = self.sellvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_MINUTES).order_by('start')
            hrs_slab = self.sellvehicleslab_set.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_HOURS).order_by('start')

            rate_string = ""
            start_string = ""
            if kms_slab:
                kms_slab = kms_slab[0]
                start_string = start_string + str(kms_slab.start) + "Km_"
                rate_string = rate_string + str(kms_slab.rate)
            if min_slab:
                min_slab = min_slab[0]
                start_string = start_string + str(min_slab.start) + "Mins_"
                rate_string = rate_string + "_" + str(min_slab.rate)
            if hrs_slab:
                hrs_slab = hrs_slab[0]
                start_string = start_string + str(hrs_slab.start) + "Hrs_"
                rate_string = rate_string + "_" + str(hrs_slab.rate)

            self.handle = "Sell"

            if self.base_pay:
                self.handle = self.handle + "_" + str(self.base_pay)
                if len(rate_string)>0 or len(start_string)>0:
                    self.handle = self.handle + "_" + start_string + rate_string
            else:
                if len(rate_string)>0 or len(start_string)>0:
                    self.handle = self.handle + "_" + start_string + rate_string
                else:
                    self.handle = self.handle + "_Rate_Shipment_VAS" + str(self.id)

        if self and self.id and self.have_important_fields_changed():
            CustomerInvoice.objects.filter(
                contract_invoices__entities__sell_rate=self).filter(
                status__in=StatusPipeline.INVOICE_EDIT_STATUSES).update(
                is_stale=True, stale_reason='Sell rate changed')
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('Customer Sell Rate')
        verbose_name_plural = _('Customer Sell Rates')


auditlog.register(SellRate)


class BuyShipmentSlab(AbstractSlab):
    """
    Buy Rates for Shipment slabs
    """
    history = AuditlogHistoryField()

    buy_rate = models.ForeignKey(BuyRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Shipment Attribute'), max_length=50,
        choices=SHIPMENT_SERVICE_ATTRIBUTES)

    class Meta:
        verbose_name = _('Shipment Slab')
        verbose_name_plural = _('Shipment Slabs')


auditlog.register(BuyShipmentSlab)


class SellShipmentSlab(AbstractSellSlab):
    """
    Sell Rates for Shipment slabs
    """
    history = AuditlogHistoryField()

    sell_rate = models.ForeignKey(SellRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Shipment Attribute'), max_length=50,
        choices=SHIPMENT_SERVICE_ATTRIBUTES)

    sku = models.ForeignKey(SKU, on_delete=models.PROTECT, null=True, blank=True)

    class Meta:
        verbose_name = _('Sell Shipment Slab')
        verbose_name_plural = _('Sell Shipment Slabs')


auditlog.register(SellShipmentSlab)


class SellPincodeShipmentSlab(AbstractSellSlab):
    """
    Sell Rates for Shipment slabs
    """
    history = AuditlogHistoryField()

    sell_rate = models.ForeignKey(SellRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Shipment Attribute'), max_length=50,
        choices=SHIPMENT_SERVICE_ATTRIBUTES)

    pincodes = models.CharField(
        _('Pincodes'), max_length=4000, validators=[RegexValidator(r'^[0-9,]*$',
        'Enter pincodes(numbers only) separated by comma, space not allowed')],
        help_text='Enter multiple pincodes separated with comma withuot any spaces')

    class Meta:
        verbose_name = _('Sell Pincode Shipment Slab')
        verbose_name_plural = _('Sell Pincode Shipment Slabs')


auditlog.register(SellPincodeShipmentSlab)


class BuyVehicleSlab(AbstractSlab):
    """
    Buy Rates for Vehicle slabs
    """
    history = AuditlogHistoryField()

    buy_rate = models.ForeignKey(BuyRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Vehicle Attribute'), max_length=25,
        choices=VEHICLE_SERVICE_ATTRIBUTES)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.buy_rate.save()

    class Meta:
        verbose_name = _('Vehicle Slab')
        verbose_name_plural = _('Vehicle Slabs')


auditlog.register(BuyVehicleSlab)


class SellVehicleSlab(AbstractSellSlab):
    """
    Sell Rates for Vehicle slabs
    """
    history = AuditlogHistoryField()

    sell_rate = models.ForeignKey(SellRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Vehicle Attribute'), max_length=25,
        choices=VEHICLE_SERVICE_ATTRIBUTES)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.sell_rate.save()

    class Meta:
        verbose_name = _('Sell Vehicle Slab')
        verbose_name_plural = _('Sell Vehicle Slabs')


auditlog.register(SellVehicleSlab)


class BuyDimensionSlab(AbstractSlab):
    """
    Buy Rates for Dimension slabs
    """
    history = AuditlogHistoryField()

    buy_rate = models.ForeignKey(BuyRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Dimension Attribute'), max_length=25,
        choices=DIMENSION_SERVICE_ATTRIBUTES)

    class Meta:
        verbose_name = _('Dimension Slab')
        verbose_name_plural = _('Dimension Slabs')


auditlog.register(BuyDimensionSlab)


class SellDimensionSlab(AbstractSellSlab):
    """
    Sell Rates for Dimension slabs
    """
    history = AuditlogHistoryField()

    sell_rate = models.ForeignKey(SellRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Dimension Attribute'), max_length=25,
        choices=DIMENSION_SERVICE_ATTRIBUTES)

    class Meta:
        verbose_name = _('Sell Dimension Slab')
        verbose_name_plural = _('Sell Dimension Slabs')


auditlog.register(SellDimensionSlab)


class BuyVASSlab(AbstractSlab):
    """
    Buy Rates for VAS slabs
    """
    history = AuditlogHistoryField()

    buy_rate = models.ForeignKey(BuyRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('VAS Attribute'), max_length=25,
        choices=VAS_SERVICE_ATTRIBUTES)

    is_tax_applicable = models.NullBooleanField(_('Is tax applicable?'),
                                            null=True, blank=True)

    class Meta:
        verbose_name = _('VAS Slab')
        verbose_name_plural = _('VAS Slabs')


auditlog.register(BuyVASSlab)


class VASAttribute(models.Model):
    attribute = models.CharField(
        _('VAS Attribute'), max_length=45)

    display_attribute = models.CharField(
        _('Display Attribute'), max_length=45, null=True)

    def __str__(self):
        return self.attribute


class SellVASSlab(AbstractSlab):
    """
    Sell Rates for VAS slabs
    """
    history = AuditlogHistoryField()

    sell_rate = models.ForeignKey(SellRate, on_delete=models.PROTECT)

    aggregate_level = models.CharField(
        _('Aggregate Level'), max_length=25,
        choices=AGGREGATE_LEVEL_CHOICES, default=CITY)

    vas_attribute = models.ForeignKey(VASAttribute, null=True, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('VAS Attribute'), max_length=25,
        choices=VAS_SERVICE_ATTRIBUTES)

    class Meta:
        verbose_name = _('Sell VAS Slab')
        verbose_name_plural = _('Sell VAS Slabs')


auditlog.register(SellVASSlab)


class BuyNTSlab(AbstractSlab):
    """
    Buy Rates for Non-Transport slabs
    """
    history = AuditlogHistoryField()

    buy_rate = models.ForeignKey(BuyRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Non-Transport Attribute'), max_length=25,
        choices=NT_SERVICE_ATTRIBUTES)

    class Meta:
        verbose_name = _('Non-Transport Slab')
        verbose_name_plural = _('Non-Transport Slabs')


auditlog.register(BuyNTSlab)


class SellNTSlab(AbstractSlab):
    """
    Sell Rates for Non-Transport slabs
    """
    history = AuditlogHistoryField()

    sell_rate = models.ForeignKey(SellRate, on_delete=models.PROTECT)

    attribute = models.CharField(
        _('Non-Transport Attribute'), max_length=25,
        choices=NT_SERVICE_ATTRIBUTES)

    class Meta:
        verbose_name = _('Sell Non-Transport Slab')
        verbose_name_plural = _('Sell Non-Transport Slabs')


class BuyRateHistory(models.Model):

    """
    Capturing the buy rate history
    """
    buy_rate = models.ForeignKey(BuyRate, null=True, blank=True,
                                 on_delete=models.PROTECT)
    field = models.CharField(max_length=80)
    old_value = models.CharField(max_length=50, null=True, blank=True)
    new_value = models.CharField(max_length=50, null=True, blank=True)
    reason = models.CharField(max_length=100, null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Buy Rate History')
        verbose_name_plural = _('Buy Rate History')

    def __str__(self):
        return self.buy_rate.name


class SellRateHistory(models.Model):

    """
    Capturing the sell rate history
    """
    sell_rate = models.ForeignKey(SellRate, null=True, blank=True,
                                 on_delete=models.PROTECT)
    field = models.CharField(max_length=80)
    old_value = models.CharField(max_length=4000, null=True, blank=True)
    new_value = models.CharField(max_length=4000, null=True, blank=True)
    reason = models.CharField(max_length=100, null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Sell Rate History')
        verbose_name_plural = _('Sell Rate History')

    def __str__(self):
        return self.sell_rate.name


auditlog.register(SellNTSlab)


class ScreenField(BaseModel):

    history = AuditlogHistoryField()

    name = models.CharField(
        _('Field Name'), max_length=50,
        default=START_METER_READING,
        choices=SCREEN_FIELD_NAME)

    display = models.CharField(
        _('Field Display'), max_length=50)

    data_type = models.IntegerField(
        _('Data Type'), default=INTEGER,
        choices=SCREEN_FIELDS_DATA_TYPES, null=True, blank=True)

    sequence = models.IntegerField(
        _('Display Sequence'), default=100, null=True, blank=True)

    def save(self, *args, **kwargs):

        if self.name:
            self.display = CommonHelper.get_display_of_tuple(
                pair=SCREEN_FIELD_NAME, index=self.name)
            self.sequence = SCREEN_FIELD_PRIORITY.get(self.name, 100)

        return super(ScreenField, self).save()

    def __str__(self):
        return self.get_name_display()

    class Meta:
        verbose_name = _('Screen Field')
        verbose_name_plural = _('Screen Fields')


auditlog.register(ScreenField)


class ScreenButton(BaseModel):

    history = AuditlogHistoryField()

    name = models.CharField(
        _('Field Name'), max_length=50,
        default=START_METER_READING,
        choices=SCREEN_BUTTON_NAME)

    display = models.CharField(
        _('Field Display'), max_length=50)

    parameters = ChoiceArrayField(
        models.CharField(
            _('Field Parameters'), max_length=50,
            choices=SCREEN_BUTTON_PARAMETER_NAME,
            null=True, blank=True),
        null=True, blank=True
    )

    def save(self, *args, **kwargs):

        if self.name:
            self.display = CommonHelper.get_display_of_tuple(
                pair=SCREEN_BUTTON_NAME, index=self.name)

        return super(ScreenButton, self).save()

    def __str__(self):
        return '%s%s' % (self.name, self.parameters)

    class Meta:
        verbose_name = _('Screen Button')
        verbose_name_plural = _('Screen Buttons')


auditlog.register(ScreenButton)


class TripWorkFlowManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().prefetch_related('screens')


class TripWorkFlow(BaseModel):

    history = AuditlogHistoryField()

    name = models.CharField(
        _('WorkFlow Name'), max_length=50, unique=True)

    is_reorder = models.BooleanField(_('ReOrdering of Flow'), default=False)

    is_wms_flow = models.BooleanField(_('Is Wms Work Flow'), default=False)

    can_cancel_trip = models.BooleanField(_('Can Driver cancel Trip'), default=False)

    can_scan_orders = models.BooleanField(_('Should Scan Order option be visible in App'), default=False)

    can_complete_trip = models.BooleanField(
        _('Can Driver complete Trip before completion of all stops'),
        default=False)

    send_sms_alert = models.BooleanField(
        _('Send out for delivery alert to customer'), default=False
    )

    otp_required = models.BooleanField(_('Is OTP required for customer delivery'), default=False)

    show_sku_details = models.BooleanField(_('Show SKU Details'), default=False)

    ignore_order_lines = models.BooleanField(_('Ignore Order Lines/SKUs'),
                                             help_text=_(
                                                 'Behave like orders without order lines/SKUs'),
                                             default=False)

    capture_assets = models.BooleanField(_('Capture assets/container details'),
                                         default=False)

    end_trip_otp = models.BooleanField(_('Is OTP required for end trip'),
                                       default=False)

    pick_orders = models.BooleanField(_('Perform Pickup for orders'),
                                      default=False)

    perform_delivery = models.BooleanField(_('Perform Delivery of orders in same trip'),
                                      default=False)

    odometer_proof = models.BooleanField(_('Capture odometer proof'),
                                         default=False)
    is_call_masking = models.BooleanField(_('Enable Call Masking'),
                                         default=False)

    report_to_pickup_hub = models.BooleanField(_('Report to Pickup Hub'),
                                               default=False)

    optimise_route = models.BooleanField(
        _('Allow to optimise route in driver app'), default=False)

    objects = TripWorkFlowManager()

    def __str__(self):

        return self.name

    class Meta:
        verbose_name = _('Trip WorkFlow')
        verbose_name_plural = _('Trip WorkFlows')


auditlog.register(TripWorkFlow)


class ScreenManager(models.Manager):

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.prefetch_related('button_fields')
        qs = qs.prefetch_related(
            models.Prefetch(
                'input_fields',
                queryset=ScreenField.objects.order_by('sequence')
            )
        )
        return qs


class Screen(BaseModel):

    history = AuditlogHistoryField()

    name = models.CharField(
        _('Screen Name'), max_length=50,
        default=START_TRIP,
        choices=SCREEN_NAME)

    display = models.CharField(
        _('Parameter Display'), max_length=50)

    sequence = models.IntegerField(null=False)

    input_fields = models.ManyToManyField(ScreenField, blank=True)

    button_fields = models.ManyToManyField(ScreenButton, blank=True)

    geofence = models.PositiveIntegerField(_("Geofence Radius(in metres)"),
                                           null=True, blank=True)

    confirmation_screen = models.BooleanField(
        _('Confirmation Screen of Flow'), default=False)

    trip_workflow = models.ForeignKey(TripWorkFlow, on_delete=models.PROTECT,
                                      null=True, blank=True,
                                      related_name='screens')

    parameters = ChoiceArrayField(
        models.CharField(
            _('Screen Parameters'), max_length=50,
            choices=SCREEN_PARAMETERS,
            null=True, blank=True),
        null=True, blank=True
    )

    objects = ScreenManager()

    def save(self, *args, **kwargs):

        if self.name:
            self.display = CommonHelper.get_display_of_tuple(
                pair=SCREEN_NAME, index=self.name)

        return super(Screen, self).save()

    class Meta:
        unique_together = ("sequence", "trip_workflow")
        verbose_name = _('Screen')
        verbose_name_plural = _('Screens')


auditlog.register(Screen)


class ContractManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            'customer', 'city', 'division')

    def get_contracts_list(self):
        """
        :return: list containing dict of contract pk, name and city_id
        """
        contract_qs = super().get_queryset().only('pk', 'name', 'city__id')
        return [
            {'id': contract.pk, 'name': contract.name, 'city': contract.city_id}
            for contract in contract_qs
        ]

    def get_distinct_cities_from_active_c2c_sme_contracts(self):
        return super().get_queryset().filter(
            contract_type__in=[CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME],
            status=CONTRACT_STATUS_ACTIVE).exclude(
            city__future_booking_days=0).select_related('city')\
            .distinct('city')


class ServiceHSNCodeMap(BaseModel):
    service_tax_category = models.CharField(
        _('GTA/BSS'), max_length=12, choices=SERVICE_TAX_CATEGORIES,
        db_index=True)

    hsn_code = models.CharField(
        _('HS Code'), max_length=10)

    is_active = models.BooleanField('Is Active', default=False)

    def __str__(self):
        return '%s - %s' % (self.service_tax_category, self.hsn_code)


class Contract(BaseModel):
    """
    Contract - to be populated by Sales once SLA is finalised
    """

    history = AuditlogHistoryField()

    description = NullCharField(_('Description'),
                                max_length=50, null=True, blank=True,
                                help_text=_('Identifiable text or description of the contract'
                                            ' (will be displayed in dropdown while selecting in order, trip pages)'),
                                validators=[
                                    RegexValidator(
                                        regex=r'^[a-zA-Z0-9_ ]+$',
                                        message=_("Special characters not allowed")
                                    ), non_python_keyword
                                ]
                                )

    name = models.CharField(
        _("Contract Name"), max_length=100, db_index=True,
        validators=[
            RegexValidator(
                regex=r'^[a-zA-Z0-9_]+$',
                message=_("Special characters not allowed")
            ), non_python_keyword
        ], unique=True)

    status = FSMField(default=CONTRACT_STATUS_DRAFT,
                      protected=False, db_index=True)

    is_house_shifting_contract = models.BooleanField(_("Is HouseShifting Contract"), default=False)

    is_inter_city_contract = models.BooleanField(_("Is Inter City Contract"), default=False)

    is_wms_contract = models.BooleanField(_("Is WMS Contract"), default=False)

    contract_type = models.CharField(
        _('Contract Type'), default=CONTRACT_TYPE_FIXED, max_length=15,
        choices=CONTRACT_TYPES, db_index=True)

    customer = models.ForeignKey('customer.Customer', on_delete=models.PROTECT,
                                 null=True, blank=True)

    division = models.ForeignKey('customer.CustomerDivision',
                                 verbose_name=_('Customer Division'),
                                 null=True, blank=True,
                                 on_delete=models.PROTECT)

    city = models.ForeignKey('address.City', on_delete=models.PROTECT, verbose_name=_('Destination City'),
                             help_text=_("Last Mile"))

    source_city = models.ForeignKey('address.City', on_delete=models.PROTECT, verbose_name=_('Source City'),
                                         null=True, blank=True, related_name='source_city', help_text=_("First Mile"))

    expected_delivery_hours = models.TimeField(
        _('Expected Delivery Hours'),
        help_text=_("Maximum Hours for Order Delivery"),
        null=True, blank=True)

    trip_workflow = models.ForeignKey(
        TripWorkFlow,
        verbose_name='Trip WorkFlow',
        on_delete=models.PROTECT, blank=True, null=True)

    service_tax_category = models.CharField(
        _('GTA/BSS'), max_length=12, choices=SERVICE_TAX_CATEGORIES, db_index=True)

    hsn_code = models.ForeignKey(
        ServiceHSNCodeMap, blank=True, null=True,
        verbose_name='HSN Code', on_delete=models.PROTECT)

    vehicle_classes = models.ManyToManyField('vehicle.VehicleClass')

    body_types = models.ManyToManyField('vehicle.BodyType')

    service_schedule = RecurrenceField(
        _('Service Calendar'),
        help_text=_(
            "Calendar rule to decide days on which orders are auto-generated"))

    __orig_service_schedule = None

    job_last_run_time = models.DateTimeField(
        _('Job Last Ran At'), null=True, blank=True)

    payment_cycle = models.ForeignKey(
        PaymentCycle, null=True, blank=True,
        verbose_name='Payment Cycle', on_delete=models.PROTECT,
        help_text=_("Payment cycle is required to activate contract"))

    supervisors = models.ManyToManyField(User, related_name='supervisors',
                                         blank=False)

    spocs = models.ManyToManyField(User, related_name='spocs', blank=True)

    trip_noshow_hrs = models.IntegerField(_('Hrs to mark trip as No-Show'), default=6,
                                          validators=[MinValueValidator(0), MaxValueValidator(48)])

    hours_before_scheduled_at = models.IntegerField(_('Earliest a trip can begin (hours) '),
                                                    help_text=_(
                                                        'How early before the scheduled time can the trip be started?'),
                                                    default=2, null=True, blank=True,
                                                    validators=[MaxValueValidator(36)])

    hours_after_scheduled_at = models.IntegerField(_('Latest a trip can begin (hours) '),
                                                   help_text=_(
                                                       'How late after the scheduled time can the trip be started?'),
                                                   default=4, null=True, blank=True, validators=[MaxValueValidator(36)])

    avg_duty_hrs = models.IntegerField(_('Average Duty Hrs'), null=True,
                                       blank=True)

    number_of_working_days = models.IntegerField(_('Number of Working Days'), null=True, blank=True)

    work_type = models.ForeignKey('contract.WorkType', null=True, blank=True,
                                  on_delete=models.DO_NOTHING)

    item_categories = models.ManyToManyField(
        'contract.ItemCategory', related_name='item_categories', blank=True)

    additional_info = models.TextField(_("Additional Information"),
                                       null=True, blank=True)

    is_pod_applicable = models.BooleanField(_("Capture POD"), default=False)

    expected_margin = models.FloatField(_("No slab margin"), null=True,
                                        blank=True)

    use_customer_hub = models.NullBooleanField(help_text=_(
        'Use customer\'s hub for delivery'
    ))

    currency_code = models.CharField(_("Currency"), max_length=3, null=False,
                                     choices=CURRENCY_CHOICES, blank=False,
                                     default='INR')

    avg_basket_value = models.DecimalField(_('Average Basket Value'),
                            max_digits=10, decimal_places=2, default=0)

    objects = ContractManager()
    copy_data = CopyManager()

    class Meta:
        verbose_name = _("Contract")
        verbose_name_plural = _("Contracts")

    def format_currency(self, amount=None, currency_code=None):
        """
        :param amount:
        :return:
        """
        if not amount:
            amount = 0.00

        currency_code = currency_code or self.currency_code
        _symbol = settings.CURRENCY_WITHOUT_SYMBOL.get(currency_code)
        language_info = translation.get_language_info(translation.get_language())
        if not _symbol:
            _symbol = numbers.format_currency(
                amount,
                currency_code,
                locale=language_info.get('code', 'en')
            )
        return _symbol

    def get_currency_symbol(self, currency_code=None):
        """
        Returns currency symbol by,
            converting given currency code if present
            otherwise, configured currency code in current contract
        """
        _currency = self.format_currency(currency_code=currency_code)
        return _currency[0] if len(_currency) > 0 \
            else settings.OSCAR_DEFAULT_CURRENCY

    @property
    def currency_symbol(self):
        return self.get_currency_symbol()

    @property
    def service_state(self):
        """
        Returns the state where Blowhorn services - has to be present in company.
        """

        return self.city.service_state

    @property
    def invoice_state(self):
        """
        Returns the state where Customer would like to be invoiced for this contract.
        """

        invoice_config = self.invoice_configs.first()
        return invoice_config.invoice_state if invoice_config else None

    def cc_customer(self):
        return self.customer

    @staticmethod
    def autocomplete_search_fields():
        return 'name', 'description'

    @transition(
        field=status,
        source=[CONTRACT_STATUS_DRAFT, CONTRACT_STATUS_INACTIVE],
        target=CONTRACT_STATUS_ACTIVE,
        permission=is_sales_representative,
        conditions=[can_activate])
    # Allow activation of contract only if
    # 1. Rate Cards are defined (both sell and rate)
    # 2. Payment Cycle is defined
    # 3. If the actor has the permission (is_sales_representative)
    def activate(self):
        print("Attempting to make the contract active")
        from blowhorn.contract.tasks import notify_contract_status_change
        notify_contract_status_change.apply_async((self.id, CONTRACT_STATUS_ACTIVE,),)

    @transition(field=status, source=CONTRACT_STATUS_ACTIVE,
                target=CONTRACT_STATUS_INACTIVE,
                permission=is_sales_representative,
                conditions=[can_deactivate])
    # Allow de-activation of contract only if
    # 1. No Open / In-Progress Trips or Order (Subscription/SPOT)
    # 2. No Open Driver Payments
    # 3. If the actor has the permission (is_sales_representative)
    def deactivate(self):
        print("Attempting to mark the contract as inactive")
        from blowhorn.contract.tasks import notify_contract_status_change
        notify_contract_status_change.apply_async((self.id, CONTRACT_STATUS_INACTIVE,),)

    def generate_blowhorn_contract_name(self):
        contract_name = 'BH_' + self.city.name.upper() + '_' + self.name.upper()
        existing_contract_names = Contract.objects.values_list('name',
                                                               flat=True)

        if not existing_contract_names:  # No contracts at all
            contract_name = contract_name + '1'
        else:
            # Find list of contract sequences with similar name
            num_list = [x.split(contract_name)[1] for x in existing_contract_names
                        if x.startswith(contract_name)]

            if num_list:
                # Get the highest sequence
                sorted_num_list = sorted(map(int, num_list), reverse=True)
                # Increment by 1 and make the new name
                contract_name = contract_name + str(sorted_num_list[0] + 1)
            else:  # First of its kind
                contract_name = contract_name + '1'
        self.name = contract_name

    def generate_contract_name(self):
        """
        This method generates a name for Customer Contract
        based on Customer Name and City
        Ensures uniqueness by augmenting with number
        e.g. CC_SPOT_AMAZON_BANGALORE_1, CC_FIXED_AMAZON_MUMBAI_2
        """
        default_prefix = 'CC_'
        prefix_qs = ContractConfiguration.objects.filter(
            name='Contract Prefix')

        if not prefix_qs:
            prefix = default_prefix
        else:
            if prefix_qs.exists():
                config = prefix_qs.first()
                prefix = config.value
                prefix = prefix + '_'
            else:
                prefix = default_prefix

        prefix += self.contract_type.upper() + '_'
        contract_name = prefix + str(self.customer.user.name).upper() + '_' + \
            str(self.city.name).upper() + '_' if not self.is_inter_city_contract else \
        prefix + str(self.customer.user.name).upper() + '_' + str(self.source_city) + '_' + \
            str(self.city.name).upper() + '_'

        existing_contract_names = Contract.objects.values_list('name',
                                                               flat=True)

        if not existing_contract_names:  # No contracts at all
            contract_name = contract_name + '1'
        else:
            # Find list of contract sequences with similar name
            num_list = [x.split(contract_name)[1] for x in existing_contract_names
                        if x.startswith(contract_name)]

            if num_list:
                # Get the highest sequence
                sorted_num_list = sorted(map(int, num_list), reverse=True)
                # Increment by 1 and make the new name
                contract_name = contract_name + str(sorted_num_list[0] + 1)
            else:  # First of its kind
                contract_name = contract_name + '1'
        self.name = contract_name

    def is_active_contract_exist(self, contract_type, is_inter_city_contract):
        return Contract.objects.filter(
            customer=self.customer,
            city=self.city,
            contract_type=contract_type,
            status=CONTRACT_STATUS_ACTIVE,
            is_inter_city_contract=is_inter_city_contract
        ).exclude(id=self.id).exists()

    def clean(self, *args, **kwargs):
        '''Remove service-schedule for non-subscription contracts
        (In case, User selected it before changing contract type from
        DEFAULT [Fixed] to SPOT)'''
        if not self.contract_type in [
                CONTRACT_TYPE_FIXED,
                CONTRACT_TYPE_SHIPMENT,
                CONTRACT_TYPE_HYPERLOCAL,
                CONTRACT_TYPE_ONDEMAND]:
            self.service_schedule = ''

        ''' Allow only one Shipment/On-demand Contract for a customer, city '''
        if self.status == CONTRACT_STATUS_ACTIVE:
            if self.contract_type == CONTRACT_TYPE_SHIPMENT and\
                self.is_active_contract_exist(CONTRACT_TYPE_SHIPMENT, is_inter_city_contract=self.is_inter_city_contract):
                    raise ValidationError(
                        _('Active Shipment Contract exists for this customer'))

            if self.contract_type == CONTRACT_TYPE_ONDEMAND and\
                self.is_active_contract_exist(CONTRACT_TYPE_ONDEMAND,
                                              is_inter_city_contract=self.is_inter_city_contract):
                    raise ValidationError(
                        _('Active On-demand Contract exists for this customer'))

            if self.contract_type == CONTRACT_TYPE_HYPERLOCAL and\
                self.is_active_contract_exist(CONTRACT_TYPE_HYPERLOCAL,
                                              is_inter_city_contract=self.is_inter_city_contract):
                    raise ValidationError(
                        _('Active Hyperlocal Contract exists for this customer'))
        super().clean(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        """
        Capture service schedule changes,
        avoiding signals, hence __init__ override
        """
        super().__init__(*args, **kwargs)
        self.__orig_service_schedule = self.service_schedule

    def generate_c2c_contract_name(self):
        """
        This method ensures a unique name is generated for a C2C contract
        e.g. C2C_BANGALORE_MINI-TRUCK
        """
        if self.contract_type == CONTRACT_TYPE_KIOSK:
            self.name = 'KIOSK_' + str(self.city.name).upper() + \
                '_' + (self.name).upper()
        elif self.contract_type == CONTRACT_TYPE_FLASH_SALE:
            self.name = 'FS_' + str(self.city.name).upper() + \
                        '_' + (self.name).upper()
        else:
            self.name = 'C2C_' + str(self.city.name).upper() + \
                '_' + (self.name).upper()

    def save(self, *args, **kwargs):
        """
        Override save method for generating unique name
        """
        if not self.is_house_shifting_contract:
            self.is_house_shifting_contract = False

        if not self.pk:  # Ensure this happens for insert and not updates
            if self.contract_type == CONTRACT_TYPE_BLOWHORN:
                self.generate_blowhorn_contract_name()
            elif self.contract_type != CONTRACT_TYPE_SME:
                if self.customer_id:
                    self.generate_contract_name()
                else:
                    self.generate_c2c_contract_name()

        super(Contract, self).save(*args, **kwargs)

        if self.service_schedule != self.__orig_service_schedule:
            ''' Store it in service schedule history '''
            new_list = []
            for rule in self.service_schedule.rrules:
                new_list.append(rule.to_text())
            for rule in self.service_schedule.exrules:
                new_list.append(rule.to_text())
            old_list = []
            for rule in self.__orig_service_schedule.rrules:
                old_list.append(rule.to_text())
            for rule in self.__orig_service_schedule.exrules:
                old_list.append(rule.to_text())
            obj = ServiceScheduleHistory(contract=self, old_value=', '.join(
                old_list), new_value=', '.join(new_list))
            obj.save()

        self.__orig_service_schedule = self.service_schedule

    @property
    def get_start_and_end_time_to_restore_driver(self):
        # Past trip acceptance time

        if self.hours_after_scheduled_at > 0:
            start_date = timezone.now() - timedelta(
                hours=self.hours_after_scheduled_at)
        else:
            start_date = timezone.now() - timedelta(
                hours=int(TripConstants().get(settings.TRIP_ACCEPTANCE_HOUR_AFTER, 0)))

        # upcomings trip acceptance time
        if self.hours_before_scheduled_at > 0:
            end_date = timezone.now() + timedelta(
                hours=self.hours_before_scheduled_at)

        else:
            end_date = timezone.now() + timedelta(
                hours=int(TripConstants().get(settings.TRIP_ACCEPTANCE_HOUR_BEFORE, 4)))

        return start_date, end_date

    def __str__(self):
        contract_str = ''
        if self.division:
            contract_str = '%s-%s' % (self.division, self.name)
        else:
            contract_str = '%s' % self.name

        return contract_str + (' (%s)' % self.description) if \
                self.description else contract_str


auditlog.register(Contract)


class ServiceScheduleHistory(BaseModel):
    """
    Model to store service schedule changes
    as auditlog library does not do a great job
    """
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE)

    old_value = models.TextField()

    new_value = models.TextField()

    class Meta:
        verbose_name = _('Service Calendar History')
        verbose_name_plural = _('Service Calendar History')


class ResourceAllocationHistory(BaseModel):
    """
    Model to store when a driver was added/removed from the contract
    """
    contract = models.ForeignKey(
        Contract, related_name='contract', on_delete=models.PROTECT)
    driver = models.ForeignKey(
        'driver.Driver', related_name='driver', on_delete=models.PROTECT)
    event_time = models.DateTimeField(_('Event Time'))
    action = models.CharField(_('Action'), max_length=50)

    class Meta:
        verbose_name = _('Resource Allocation History')
        verbose_name_plural = _('Resource Allocation History')


class Resource(models.Model):
    """
        Resource for resource allocation
    """
    driver = models.ForeignKey(Driver, on_delete=models.PROTECT, blank=True,
                               null=True)
    route = models.ForeignKey('customer.Route', on_delete=models.PROTECT,
                              blank=True, null=True)
    resourceallocation = models.ForeignKey('ResourceAllocation', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Resource')
        verbose_name_plural = _('Resources')


class ResourceAllocation(BaseModel):
    """
    Drivers can be added to HUB(s) which can be selected for a contract
    """
    history = AuditlogHistoryField()
    contract = models.ForeignKey(Contract, on_delete=models.PROTECT)
    hub = models.ForeignKey('address.Hub', on_delete=models.PROTECT)
    drivers = models.ManyToManyField('driver.Driver', blank=True,
                                     through='Resource')
    unplanned_additional_drivers = models.IntegerField(
        _('Unplanned additional drivers'), default=0)
    shift_start_time = models.TimeField(
        _('Shift Start Time'), default=datetime.time(9, 00))
    remind_driver = models.BooleanField(_('Remind Driver'), default=False)
    exotel_app_id = models.CharField(_('Exotel Applet ID'),
        max_length=50, null=True, blank=True)

    def __str__(self):
        return "%s-%s" % (self.contract.name, self.hub.name)

    class Meta:
        verbose_name = _('Resource Allocation')
        verbose_name_plural = _('Resource Allocations')


auditlog.register(ResourceAllocation)

"""
Add an entry to ResourceAllocationHistory
whenever drivers M2M relation is modified
https://docs.djangoproject.com/en/1.11/ref/signals/
"""

@receiver(post_save, sender=Resource)
def driver_added(sender, **kwargs):
    Driver = get_model('driver', 'Driver')
    instance = kwargs.pop('instance', None)
    created = kwargs.pop('created', False)
    if instance and isinstance(instance, Resource) and created:
        contract = Contract.objects.filter(id=instance.resourceallocation.contract_id).get()
        driver = Driver.objects.get(pk=instance.driver_id)
        route = Route.objects.filter(pk=instance.route_id).get() if instance.route else None
        obj = ResourceAllocationHistory(
            contract=contract,
            driver=driver,
            event_time=timezone.now(),
            action='post_add')
        obj.save()
        # generate an order for every new driver added to resource allocation
        from blowhorn.contract.utils import SubscriptionService
        resource_allocation = ResourceAllocation.objects.filter(id=instance.resourceallocation_id).get()
        SubscriptionService().create_order_for_added_resource(contract, driver, resource_allocation, route)


@receiver(post_delete, sender=Resource)
def driver_modified(sender, **kwargs):
    Driver = get_model('driver', 'Driver')
    instance = kwargs.pop('instance', None)
    if instance and isinstance(instance, Resource):
        contract = Contract.objects.filter(id=instance.resourceallocation.contract_id).get()
        driver = Driver.objects.get(pk=instance.driver_id)
        obj = ResourceAllocationHistory(
            contract=contract,
            driver=driver,
            event_time=timezone.now(),
            action='post_remove')
        obj.save()

# m2m_changed.connect(drivers_changed, sender=ResourceAllocation.drivers.through, weak=False)
# m2m_changed.connect(drivers_changed, sender='driver.Driver', weak=False)


class ContractTerm(models.Model):
    """
    The validity of contract
    """
    history = AuditlogHistoryField()

    wef = models.DateField(_('With Effect From'), db_index=True)

    contract = models.ForeignKey(Contract, on_delete=models.CASCADE)

    buy_rate = models.ForeignKey(BuyRate, null=True, blank=True, verbose_name=_('Last mile Buy Rate'),
                                 on_delete=models.PROTECT, related_name='buy_rate')

    first_mile_buy_rate = models.ForeignKey(BuyRate, null=True, blank=True, on_delete=models.PROTECT,
                                           related_name='firstmilebuyrate')

    middle_mile_buy_rate = models.ForeignKey(BuyRate, null=True, blank=True, on_delete=models.PROTECT,
                                            related_name='middlemilebuyrate')

    sell_rate = models.ForeignKey(SellRate, null=True, blank=True,
                                  on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Contract Term')
        verbose_name_plural = _('Contract Terms')


auditlog.register(ContractTerm)

# Place holder for different types for configurations

CONFIGURATION_CLASSES = (
    (u'Contract Prefix', _('Contract Prefix')),
    (u'Contract Deactivate', _('Contract Deactivation Duration')),
)


class ContractConfiguration(BaseModel):
    """
    Contract Configurations
    """
    history = AuditlogHistoryField()

    name = models.CharField(
        _('Configuration Name'), max_length=25, choices=CONFIGURATION_CLASSES)

    description = models.CharField(
        _('Configuration Description'), max_length=50)

    value = models.CharField(
        _('Configuration Value'), max_length=25)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Configuration')
        verbose_name_plural = _('Configurations')


auditlog.register(ContractConfiguration)


@receiver(m2m_changed, sender=Contract.supervisors.through)
@receiver(m2m_changed, sender=Contract.spocs.through)
def contract_user_changed(sender, **kwargs):
    action = kwargs.pop('action', None)
    instance = kwargs.pop('instance', None)
    pk_set = kwargs.pop('pk_set', None)

    if instance and isinstance(instance, Contract) and action in [
            "pre_add", "pre_remove"]:

        sender_role_mapping = {
            Contract.supervisors.through: "Supervisor",
            Contract.spocs.through: "Spoc"
        }

        role = sender_role_mapping.get(sender, None)

        if role:
            if action == "pre_add":
                subject_format = MESSAGE_FORMATS["SUBJECT"]["PERMISSION_GRANT"]
                content_format = MESSAGE_FORMATS["CONTENT"]["PERMISSION_GRANT"]
            else:
                subject_format = MESSAGE_FORMATS[
                    "SUBJECT"]["PERMISSION_REVOKE"]
                content_format = MESSAGE_FORMATS[
                    "CONTENT"]["PERMISSION_REVOKE"]

            # This is expected to be async in prod env.
            # send_async_mail.delay(
            #     list(User.objects.filter(id__in=list(pk_set)
            #                              ).values_list('email', flat=True)),
            #     subject_format.format(role=role, contract_name=instance.name),
            #     content_format.format(
            #         role=role, contract_name=instance.name,
            #         contract_id=instance.id),
            # )


class C2CContract(Contract):

    class Meta:
        proxy = True
        verbose_name = _('C2C Contract')
        verbose_name_plural = _('C2C Contracts')


class SMEContract(Contract):

    class Meta:
        proxy = True
        verbose_name = _('SME Contract')
        verbose_name_plural = _('SME Contracts')


class BlowhornContract(Contract):

    class Meta:
        proxy = True
        verbose_name = _('Blowhorn Contract')
        verbose_name_plural = _('Blowhorn Contracts')


class KioskContract(Contract):

    class Meta:
        proxy = True
        verbose_name = _('Kiosk Contract')
        verbose_name_plural = _('Kiosk Contracts')


class FlashSaleContract(Contract):

    class Meta:
        proxy = True
        verbose_name = _('FlashSale Contract')
        verbose_name_plural = _('FlashSale Contracts')


class LabourConfiguration(models.Model):
    max_labours = models.PositiveIntegerField(default=1,
                                              help_text=_('Max. number of labours allowed '
                                                          'in booking for this contract (including driver)'))
    payment_per_labour = models.FloatField(default=400,
                                           help_text=_('Cost per labour'))
    contract = models.OneToOneField(Contract, on_delete=models.CASCADE)

    cost_per_labour = models.FloatField(default=400,
                                        help_text=_('Revenue per labour'))

    class Meta:
        verbose_name = _('Labour Configuration')
        verbose_name_plural = _('Labour Configuration')

    def __str__(self):
        return '%s x %s' % (self.max_labours, self.cost_per_labour)


class KioskLabourConfiguration(models.Model):
    no_of_labours = models.PositiveIntegerField(default=1,
                                                help_text=_('Max. number of labours allowed '
                                                            'in booking for this contract (including driver)'))

    labour_cost = models.FloatField(default=400,
                                    help_text=_('labour cost'))

    labour_category = models.CharField(_('Labour Category'), max_length=55)

    contract = models.ForeignKey(Contract, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Kisok Labour Configuration')
        verbose_name_plural = _('Kisok Labour Configuration')

    def __str__(self):
        return '%s x %s' % (self.no_of_labours, self.labour_cost)


class ItemCategory(BaseModel):
    """
        For storing various categories of items and their weight range
    """
    name = models.CharField(
        _('Item Category Name'), max_length=40)
    min_weight = models.IntegerField(_('Minimum Weight (in Kgs'), default=0)
    max_weight = models.IntegerField(_('Maximum Weight (in Kgs'), default=0)
    description = models.CharField(
        _('Item Configuration Description'), max_length=80, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Item Category')
        verbose_name_plural = _('Item Categories')


class WorkType(BaseModel):
    """
        Work types for contract
    """
    work_type = models.CharField(_("Work Type"), max_length=256, null=True, blank=True)

    def __str__(self):
        return self.work_type
