from django.conf.urls import url
# from oscarapi.app import RESTApiApplication
from django.apps import AppConfig
from blowhorn.oscar.core.loading import get_class
from blowhorn.common.firebase_token import FirebaseToken
from django.urls import include, path


# class Blowhorn(AppConfig):
#     def __init__(self):
#         app_name = 'BLowhorn_app'
#         app_module = 'Blowhorn'
#         self.app_name = 'BLowhorn_app'
#         self.app_module = 'Blowhorn'
#     # address_app = get_class('address.apps', 'AddressApplication')
#     # trip_app = get_class('trip.app', 'application')
#     # driver_app = get_class('driver.app', 'application')
#     # order_app = get_class('order.app', 'application')
#     # customer_app = get_class('customer.app', 'application')
#     # payment_app = get_class('payment.app', 'application')
#     # tracking_app = get_class('livetracking.app', 'application')
#     # vehicle_app = get_class('vehicle.app', 'application')
#     # contract_app = get_class('contract.app', 'application')
#     # apps_app = get_class('apps.app', 'application')
#     # exotel_app = get_class('exotel.app', 'application')
#     # coupon_app = get_class('coupon.app', 'application')
#     # mitra_app = get_class('mitra.app', 'application')
#     # changelog_app = get_class('changelog.app', 'application')
#     # routeoptimiser_app = get_class('route_optimiser.app', 'application')
#     # report_app = get_class('report.app', 'application')
#     # wms_app = get_class('wms.app', 'application')
#
#     def get_urls(self):
#         # Add custom URL's here
#         urls = [
#             url(r'^firebasetoken/(?P<token_id>[\w-]+)$',
#                 FirebaseToken.as_view(),
#                 name='firebase-token'),
#         ]
#         urls += self.address_app.get_urls(self)
#         # urls += self.driver_app.get_urls()
#         # urls += self.order_app.get_urls()
#         # urls += self.trip_app.get_urls()
#         # urls += self.customer_app.get_urls()
#         # urls += self.payment_app.get_urls()
#         # urls += self.tracking_app.get_urls()
#         # urls += self.vehicle_app.get_urls()
#         # urls += self.contract_app.get_urls()
#         # urls += self.apps_app.get_urls()
#         # urls += self.exotel_app.get_urls()
#         # urls += self.coupon_app.get_urls()
#         # urls += self.changelog_app.get_urls()
#         # urls += self.mitra_app.get_urls()
#         # urls += self.routeoptimiser_app.get_urls()
#         # urls += self.report_app.get_urls()
#         # urls += self.wms_app.get_urls()
#
#         # Append the OscarAPI's URL's at the end
#         urls += super(Blowhorn, self).get_urls()
#         return urls

# polls_patterns = ([
#     path('', views.IndexView.as_view(), name='index'),
#     path('<int:pk>/', views.DetailView.as_view(), name='detail'),
# ], 'polls')
#
# urlpatterns = [
#     path('polls/', include(polls_patterns)),
# ]

# application = Blowhorn()
