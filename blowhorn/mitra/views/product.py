import json

from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from blowhorn.basket.models import Line
from rest_framework.permissions import AllowAny

from blowhorn.mitra.mixins.product import ProductMixin, ProductClassMixin, \
    ProductCartMixin
from blowhorn.users.models import User


class ProductList(views.APIView, ProductMixin):
    permission_classes = (IsAuthenticated,)

    def get_user(self, user):
        if hasattr(user, 'driver'):
            return user.driver
        if hasattr(user, 'customer'):
            return user.customer
        if hasattr(user, 'vendor'):
            return user.vendor

    def get(self, request):
        request_user = self.get_user(request.user)
        data = request.query_params
        self._initialize_attribites()
        _data = self._get_product_list(request_user, data)
        return Response(status=self.status, data=self.error or _data)


class ProductClass(views.APIView, ProductClassMixin):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        data = request.query_params
        request_user = ProductList().get_user(request.user)
        city_id = data.get('operating_city_id')
        _data = self._get_product_class_list(city_id, request_user)
        return Response(status=status.HTTP_200_OK, data=_data)


class ProductCart(views.APIView, ProductCartMixin):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        self._initialize_attribites()
        response ={
            "basket_id":0,
            "products":[]
        }
        _data = self._get_products_from_basket(request.user)
        return Response(status=status.HTTP_200_OK, data=_data or response)

    def post(self, request):
        data = request.data
        cart_data = data.get('cart_data', [])
        if isinstance(cart_data, str):
            cart_data = json.loads(cart_data)
        self._initialize_attribites()
        for item in cart_data:
            self._add_product_to_basket(request.user, item)
        item_count = Line.objects.filter(basket__owner=request.user).count()
        msg = 'Cart updated'
        data = {
            "msg": msg,
            "cart_count": item_count
        }
        return Response(status=self.status, data=self.error or data)
