from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from blowhorn.stock.models import ProductStockRecord
from django.db.models import Sum

from blowhorn.mitra.mixins.order import MitraOrderHistoryMixin, EcommOrderMixin, \
    EcommConfirmOrderMixin


class OrderHistory(views.APIView, MitraOrderHistoryMixin):
    permission_classes = (IsAuthenticated,)

    def get(self, request):

        self._initialize_attributes(request.user)
        try:
            self._get_order_history()
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Something went wrong')

        return Response(status=self.status, data=self.error or self.response)


class EcommCreateOrder(views.APIView, EcommOrderMixin):
    permission_classes = (IsAuthenticated, )

    def get_product_price(self, data):
        stock_ids = [x['stockrecord_id'] for x in data.get('products', [])]
        total = ProductStockRecord.objects.filter(id__in=stock_ids).aggregate(amt=Sum('price_excl_tax')).get('amt', 0)
        return total

    def post(self, request):
        data = request.data
        amt = self.get_product_price(data)
        if not amt or (amt != data['total_payable']):
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order Amount')
        self._initialize_attributes(data, request.user)
        ord_num = self.create_ecomm_order(request)
        if not ord_num:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='failed to create order')
        return Response(status=status.HTTP_200_OK, data=ord_num)


class EcommConfirmOrder(views.APIView, EcommConfirmOrderMixin):
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data
        self._initialize_attributes(data, request.user)
        self.confirm_ecomm_order(data)

