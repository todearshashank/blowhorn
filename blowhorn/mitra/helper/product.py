from blowhorn.mitra.constant import CATALOGUE_IMAGE_PATH


def get_product_image_Path(product):
    return CATALOGUE_IMAGE_PATH.format(product_class=product.product_class.name,
                                       product=product.name)
