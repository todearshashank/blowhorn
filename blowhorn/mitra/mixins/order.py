import requests
import json
from types import SimpleNamespace

from rest_framework import status

from django.conf import settings

from blowhorn.oscar.core.loading import get_model
from blowhorn.basket.models import Basket
from blowhorn.stock.models import WeightBased
# from blowhorn.oscar.apps.order.utils import OrderNumberGenerator, OrderCreator
from blowhorn.partner.strategy import Selector

from blowhorn.integration_apps.sub_models.sub_models import PartnerLeadStatus
from config.settings import status_pipelines as StatusPipeline
from datetime import datetime
from blowhorn.integration_apps.models import SymboLead
from blowhorn.mitra.helper.order import EcommOrderCreator, EcommOrderNumberGenerator
from blowhorn.address.utils import get_shipping_addr_hub
from blowhorn.utils.functions import utc_to_ist
from config.settings.base import PARTNER_WEB_VIEW_MAPPING

SYMBO_URL = settings.SYMBO_LEADMANAGEMENT_URL
api_key = settings.SYMBO_API_KEY

Hub = get_model('address', 'Hub')
Order = get_model('order', 'Order')

class MitraOrderHistoryMixin(object):

    def _initialize_attributes(self, user):
        self.driver = None
        if user.driver:
            self.driver = user.driver
        self.user = user

        # TODO:
        self.from_date = datetime.strptime('01-01-2020', '%d-%m-%Y')
        self.order_history = []
        self.response = {}
        self.status = status.HTTP_200_OK
        self.error = None


    def _get_order_history(self):
        if self.driver:
            self._get_symbo_orders()
            self._get_partner_orders()

        self._fetch_ecomm_orders()

        if not self.order_history:
            self.response['msg'] =  'No orders found'
        self.response['order_history'] = self.order_history

    def _get_symbo_orders(self):
        if self.driver:
            symbo_leads = SymboLead.objects.filter(driver=self.driver, created_date__gte=self.from_date)
            for lead in symbo_leads:
                lead_status = 'Applied'
                if lead:
                    try:
                        LEAD_URL = SYMBO_URL + '/' + str(lead.symbo_id)
                        headers = {'content-type': 'application/json',
                                   'x-api-key': api_key,
                                   'clientId': 'BLOWHORN'}
                        response = requests.get(LEAD_URL, headers=headers)
                        data = json.loads(response.content)
                        lead_status = data.get('sub_Disposition', '')
                    except:
                        pass
                    order_date = utc_to_ist(lead.created_date).strftime('%d-%m-%Y, %I:%M %p')
                _dict = {'status': lead_status,
                         'order_date': order_date,
                         'product': 'Insurance',
                         'brand': 'Symbo',
                         'web_view_link': ''
                         }

                self.order_history.append(_dict)

    def _get_partner_orders(self):
        dvara_leads = PartnerLeadStatus.objects.filter(driver=self.driver, created_date__gte=self.from_date)
        for lead in dvara_leads:
            order_date = utc_to_ist(lead.created_date).strftime('%d-%m-%Y, %I:%M %p')
            _dict = {'status': lead.loan_status,
                     'order_date': order_date,
                     'product': 'Loan',
                     'brand': str(lead.partner),
                     'web_view_link': PARTNER_WEB_VIEW_MAPPING.get('Dvara') % (self.driver.id)
                     }
            self.order_history.append(_dict)

    def _fetch_ecomm_orders(self):
        order_qs = Order.objects.filter(order_type=settings.ECOMM_ORDER_TYPE,
                                        status__in=[StatusPipeline.ORDER_PLACED, StatusPipeline.ORDER_SENT_FOR_FULILMENT],
                                        user=self.user).prefetch_related(
            'basket__lines__product').order_by('-date_placed')
        for order in order_qs:
            basket_line = order.basket.lines.all()[0]
            product = basket_line.product.product_class.name
            brand = basket_line.stockrecord.partner.name
            _dict = dict(
                status=order.status,
                order_date=utc_to_ist(order.date_placed).strftime('%d-%m-%Y, %I:%M %p'),
                product=product,
                brand=brand,
                web_view_link=''
            )
            try:
                # fetch order line details
                order_lines = []
                for line in order.lines.all():
                    line_prices = line.prices.all()
                    line_dict = {}
                    line_dict['product_id'] = line.product.id
                    line_dict['title'] = line.product.title
                    line_dict['description'] = line.product.description
                    line_dict['stockrecord_id'] = line.stockrecord.id
                    if line_prices:
                        line_dict['quantity'] = line_prices[0].quantity
                        line_dict['price'] = float(line_prices[0].price_incl_tax)
                        line_dict['image'] = line.product.images.all()[
                        0].original.url if line.product.images.all() else None
                    order_lines.append(line_dict)
                _dict['lines'] = order_lines
            except:
                self.error = 'Failed to fetch order line details'
                self.status = status.HTTP_400_BAD_REQUEST
            self.order_history.append(_dict)


class EcommOrderMixin(object):

    def _initialize_attributes(self, data, user):
        self.user = user
        self.driver = user.driver
        self.error = None
        self.status = status.HTTP_200_OK
        basket_id = data.get('basket_id')
        try:
            self.basket = Basket.objects.get(pk=basket_id)
        except:
            self.status = status.HTTP_400_BAD_REQUEST
            self.error = 'Invalid cart details'
            return
        self.price = data.get('total_payable')

        hub_id = data.get('hub_id')
        try:
            self.hub = Hub.objects.get(pk=hub_id)
        except:
            self.status = status.HTTP_400_BAD_REQUEST
            self.error = 'Invalid hub details'
            return


    def create_ecomm_order(self, request):

        # check if order already exists with same basket id
        order = Order.objects.filter(basket=self.basket).first()
        self.basket.strategy = Selector().strategy(request=request,
                                                   user=request.user)
        if order:
            EcommOrderCreator().remove_order_lines(order)
            for line in self.basket.all_lines():
                EcommOrderCreator().create_line_models(order, line)
                EcommOrderCreator().update_stock_records(line)
            return dict(order_number=order.number)
        order_number = EcommOrderNumberGenerator().order_number(self.basket)

        shipping_address = get_shipping_addr_hub(self.hub)
        # Hub.objects.filter(id=self.hub_id).update(shipping_address=shipping_address)

        order_status = StatusPipeline.ORDER_PENDING
        date_placed=datetime.now()

        total = SimpleNamespace(currency=settings.OSCAR_DEFAULT_CURRENCY,
                                incl_tax=self.price, excl_tax=self.price)
        shipping_method = WeightBased.objects.first()
        # shipping_charge = OrderAndItemCharges.objects.first()
        # shipping_charge = shipping_charge.calculate(self.basket)
        shipping_charge = 0.00
        order = EcommOrderCreator().place_order(user=self.user, basket=self.basket,
                                        total=total,
                                        shipping_address=shipping_address,
                                        order_number=order_number,
                                        status=order_status,
                                        date_placed=date_placed,
                                        shipping_method=shipping_method,
                                        shipping_charge=shipping_charge,
                                        hub=self.hub,
                                        order_type=settings.ECOMM_ORDER_TYPE,
                                        total_payable=self.price)

        return dict(order_number=order.number) or None


class EcommConfirmOrderMixin(object):

    def _initialize_attributes(self, data, user):
        self.user = user
        self.driver = user.driver
        self.error = None
        self.status = status.HTTP_200_OK
        self.is_txn_success = data.get('is_txn_success')
        self.txn_number = data.get('txn_number')
        self.order_data = {}
        self.total_amount = data.get('total_amount')

    def confirm_ecomm_order(self, data):
        if self.is_txn_success:
            self.order_data['status'] = StatusPipeline.ORDER_PLACED
            self.order_data['total_incl_tax'] = self.total_amount
