from rest_framework import status

from blowhorn.address.models import Hub
from blowhorn.stock.models import Product, ProductClass
from blowhorn.basket.models import Basket, Line
from django.db import connection

from django.db.models import Q, F

from blowhorn.customer.models import Partner
from config.settings.base import PARTNER_WEB_VIEW_MAPPING, PARTNER_APPLY_MAPPING, PARTNER_ELIGILIBITY_URL


class ProductMixin(object):

    def _initialize_attribites(self):
        self.status = status.HTTP_200_OK
        self.error = None

    def _get_product_list(self, request_user, data={}):

        partner_id = data.get('partner_id')
        product_class_id = data.get('product_class_id')

        query_params = Q(availability_role=request_user._meta.object_name) | Q(availability_role__isnull=True)
        if product_class_id:
            query_params = Q(product_class_id=product_class_id)
        if partner_id:
            query_params = query_params & Q(product_class__partner__id=partner_id)
        query_params = query_params & (
                Q(stockrecords__price_excl_tax__isnull=False) | Q(
                  product_class__partner__value_text='detail'))

        products = Product.objects.filter(query_params).prefetch_related(
            'attribute_values', 'stockrecords').distinct()
        filter_list = ['---Select---']
        product_details = []
        for product in products:
            _dict = {}
            _dict['product_id'] = product.id
            _dict['title'] = product.title
            _dict['description'] = product.description
            if product.description and product.description not in filter_list:
                filter_list.append(product.description)

            _dict['attributes'] = []
            for attr in product.attribute_values.all():
                _attributes = {}
                _attributes['key'] = attr.attribute.name
                _attributes['value'] = attr.value_text
                _dict['attributes'].append(_attributes)

            images = product.images.all()

            _dict['image'] = images[0].original.url if images else None
            stock = product.stockrecords.all()[0] if product.stockrecords.all() else None
            if stock:
                _dict['stockrecord_id'] = stock.id
                _dict['price'] = float(stock.price_excl_tax)
                _dict['mrp_price'] = float(stock.price_retail) if stock.price_retail else 0

            product_details.append(_dict)

        if not product_details:
            self.status = status.HTTP_400_BAD_REQUEST
            self.error = 'No valid product found'
            return {}

        catalogue_list = {'filters': filter_list, 'products': product_details}

        return catalogue_list


class ProductClassMixin(object):

    def _get_product_class_list(self, city_id, request_user):
        # return None
        product_classes = ProductClass.objects.filter(products__availability_role=request_user._meta.object_name
                                                      ).prefetch_related('partner').distinct()

        product_class_list = []
        partners_map = dict(Partner.objects.values_list('legalname', 'id'))
        for product_class in product_classes:
            _dict = dict(id=product_class.id,
                         caption=product_class.caption,
                         image=product_class.image.url if product_class.image else None,
                         is_active=product_class.is_active)
            _dict['partners'] = []
            for partner in product_class.partner.all():
                _partner = dict(id=partner.id,
                                caption=partner.caption,
                                image=partner.image.url if partner.image else None,
                                identifier = partner.value_text)

                _partner.update({
                    "client_id": partners_map.get(partner.caption) or 0,
                    "eligibility_url": PARTNER_ELIGILIBITY_URL.get(partner.caption) or "" ,
                    "apply_url": PARTNER_APPLY_MAPPING.get(partner.caption, '') or "",
                    "webview_url": PARTNER_WEB_VIEW_MAPPING.get(partner.caption, '')
                                   % (request_user.id) if PARTNER_WEB_VIEW_MAPPING .get(partner.caption, '') else '',
                })
                # if value text is detail, it means this partner has just the product detail screen to show(like insurance)
                if partner.value_text == 'detail':
                    catalogue_list = ProductMixin()._get_product_list(request_user,
                        dict(partner_id=partner.id))
                    _partner['product'] = catalogue_list['products'][0] if catalogue_list.get('products') else None
                _dict['partners'].append(_partner)

            product_class_list.append(_dict)

        catalogue_list = {'product_classes': product_class_list}

        hubs = Hub.objects.filter(city_id=city_id, client_billing_code__isnull=False).values('id', 'name')

        catalogue_list['hub_list'] = list(hubs)

        return catalogue_list


class ProductCartMixin(object):

    def _initialize_attribites(self):
        self.status = status.HTTP_200_OK
        self.error = None

    def _get_products_from_basket(self, user):
        basket = Basket.objects.filter(owner=user).exclude(status='Submitted').first()
        cart_lines = Line.objects.filter(basket=basket).select_related(
            'product', 'stockrecord').prefetch_related('product__product_images')
        if not basket:
            return None
        _data = {'basket_id': basket.id}
        basket_lines = []
        _data['products'] = []
        try:
            for line in cart_lines:
                _dict = {}
                _dict['product_id'] = line.product.id
                _dict['title'] = line.product.title
                _dict['desc'] = line.product.description
                _dict['stockrecord_id'] = line.stockrecord.id
                _dict['max_qty'] = line.stockrecord.max_qty
                _dict['uom'] = line.stockrecord.uom if line.stockrecord.uom else ''
                _dict['pack_of'] = line.stockrecord.pack_of if line.stockrecord.pack_of else 1
                _dict['quantity'] = line.quantity
                _dict['price'] = float(line.price_excl_tax or 0)
                _dict['delivery_charge'] = float(line.stockrecord.delivery_charge or 0)
                _dict['mrp_price'] = float(
                    line.stockrecord.price_retail) if line.stockrecord and line.stockrecord.price_retail else 0
                _dict['image'] = line.product.images.all()[
                    0].original.url if line.product.images.all() else None

                basket_lines.append(_dict)
            _data['products'] = basket_lines
        except:
            self.status = status.HTTP_200_OK
            self.error = 'Failed to get data from cart'
        return _data

    def _add_product_to_basket(self, user, cart_data):
        qty = cart_data.get('quantity', 0)
        is_update = cart_data.get('isUpdated')

        if qty < 0:
            self.status = status.HTTP_400_BAD_REQUEST
            self.error = 'Invalid quantity'
            return

        product_id = cart_data.get('product_id')
        stockrecord_id = cart_data.get('stockrecord_id')
        price_excl_tax = cart_data.get('price')
        mrp_price = cart_data.get('mrp_price')

        try:
            # TODO: set correct value for line reference
            basket = Basket.objects.filter(Q(owner=user) & ~Q(status='Submitted')).first()

            if not basket:
                basket = Basket.objects.create(owner=user)

            # connection._rollback()
            qs = Line.objects.filter(basket=basket, product_id=product_id,
                                     stockrecord_id=stockrecord_id)

            if qs:
                if qty == 0:
                    qs.delete()
                    return
                if is_update:
                    qs.update(quantity=F('quantity') + qty)

                else:
                    qs.update(quantity=qty)

            else:
                Line.objects.create(basket=basket, product_id=product_id,
                                    stockrecord_id=stockrecord_id,
                                    line_reference=stockrecord_id, quantity=qty,
                                    price_excl_tax=price_excl_tax,
                                    )

        except BaseException as be:
            self.status = status.HTTP_400_BAD_REQUEST
            self.error = 'Failed to create cart'
