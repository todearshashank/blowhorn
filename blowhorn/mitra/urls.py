from django.conf.urls import url
from django.apps import AppConfig

from .views.order import (
    OrderHistory,
    EcommCreateOrder,
    EcommConfirmOrder
)

from .views.product import (
    ProductList,
    ProductClass,
    ProductCart
)


urlpatterns = [
    url(r'^mitra/order/history/$',
        OrderHistory.as_view(), name='order-history'),
    url(r'mitra/product/list', ProductList.as_view(), name='product-listing'),
    url(r'mitra/product/services', ProductClass.as_view(),
        name='product-listing'),
    url(r'mitra/product/cart', ProductCart.as_view(),
        name='product-cart'),
    url(r'mitra/order/create', EcommCreateOrder.as_view(),
        name='create-order'),
    url(r'mitra/order/confirm', EcommConfirmOrder.as_view(),
        name='create-confirm'),
]
