from django.forms.widgets import ClearableFileInput, Input, CheckboxInput
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape, format_html, html_safe
from django.utils.translation import ugettext_lazy as _


class ImagePreviewWidget(ClearableFileInput):
    """
        Basically a ClearableFileInput widget
        1. Preview for document
        2. Filename below the preview
        3. File will open in new window on click of filename/preview
    """
    template_with_initial = (
        """<div style="display:flex;flex-direction: column;">
            <img id="%(img_id)s" src="%(initial_url)s" width="100px" height="100px"
            title="%(hover_help)s" onClick="openDocument('%(initial_url)s')">
            <span class="file-name" style="font-size:10px;"
            onClick="openDocument('%(initial_url)s')">%(file_name)s</span>
            %(clear_template)s<br />%(input)s
            </div>
        """
    )
    INPUT_CLASS = 'image-preview'
    HELP_TEXT = {
        'upload': _('Click here to upload the document'),
        'change': _('Click here to open in new window')
    }
    INITIAL_URL = '/static/assets/Assets/avatar.png'

    def __init__(self, attrs=None):
        super(ImagePreviewWidget, self).__init__(attrs)
        self.attrs['class'] = self.INPUT_CLASS

    def __get_filename(self, initial_url):
        file_name = ''
        if initial_url:
            file_name = initial_url.split('/')[-1]

        return file_name

    def render(self, name, value, attrs=None, renderer=None):
        id_for_label = self.id_for_label(attrs.get('id'))
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
            'id_for_label': id_for_label,
            'img_id': id_for_label + '_img',
            'hover_help': self.HELP_TEXT['upload']
        }
        template = '%(input)s'
        substitutions['input'] = Input.render(self, name, value, attrs)
        if self.is_initial(value):
            template = self.template_with_initial
            substitutions.update({'hover_help': self.HELP_TEXT['change']})
            initial_url = '%s' % value.url
            substitutions['initial_url'] = initial_url
            substitutions.update({'file_name': self.__get_filename(initial_url)})
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = \
                    CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})

        return mark_safe(template % substitutions)

    class Media:
        js = ('/static/admin/js/image_preview_widget.js', )
