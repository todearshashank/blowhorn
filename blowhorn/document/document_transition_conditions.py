from blowhorn.address.models import City

def is_action_owner_or_superuser(instance, user):
    if City.objects.filter(managers=user).exists():
        return True
    if instance.action_owner == user or user.is_superuser:
        return True
    return False


def is_doc_eligible_for_activate(instance):
    from blowhorn.document.models import is_doc_number_exist
    record = is_doc_number_exist(instance)
    if record and record.exists():
        is_doc_eligible_for_activate.hint = 'document number exist check %s' % record
        return False
    return True
