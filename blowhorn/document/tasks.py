from blowhorn import celery_app
from blowhorn.common.decorators import redis_batch_lock
from blowhorn.document.utils import mark_document_expired, send_alert_to_driver

@celery_app.task
@redis_batch_lock(period=10800, expire_on_completion=True)
def send_expiry_alert():
    """
    Daily job at 7:00 AM
    """
    mark_document_expired()
    send_alert_to_driver()


# @celery_app.task
# @periodic_task(run_every=(crontab(hour="21")))
# @redis_batch_lock(period=84600, expire_on_completion=True)
# def send_mail_to_spocs_for_insurance_expiry():
#     if settings.DEBUG:
#         return

#     """
#     Sends mail to spocs for expired vehicle insurance
#     """
#     from blowhorn.driver.models import DriverVehicleMap
#     document_code_list = [VEHICLE_INSURANCE_CONSTANT,
#                             VEHICLE_FITNESS_CERTIFICATE_CONSTANT]
#     document_types = DocumentType.objects.filter(code__in = document_code_list)
#     mappings = DriverVehicleMap.objects.filter(driver__status = ACTIVE)

#     for mapping in mappings:
#         if not mapping.vehicle:
#             continue

#         documents = VehicleDocument.objects.filter(vehicle = mapping.vehicle,
#                         document_type__in = document_types)

#         for doctype in document_types:
#             eachtypedocs = documents.filter(document_type = doctype)
#             name = doctype.name

#             if not eachtypedocs.exists():
#                 send_mail_to_spocs(name, mapping.driver, mapping.vehicle,
#                                     expiry_days=None, document_absent = True)
#                 continue

#             doc = eachtypedocs.latest('date_uploaded')
#             if not doc.expiry_date:
#                 send_mail_to_spocs(name, mapping.driver, mapping.vehicle,
#                                     expiry_days=None)
#             else:
#                 delta = doc.expiry_date - datetime.now().date()
#                 if delta.days <= 30:
#                     send_mail_to_spocs(name, mapping.driver, mapping.vehicle,
#                                         expiry_days=delta.days)
