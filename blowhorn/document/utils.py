from django.conf import settings
from datetime import date
from django.db.models import Q

from datetime import timedelta
from django.utils import timezone

from blowhorn.common.helper import CommonHelper
from config.settings import status_pipelines as StatusPipeline
from blowhorn.document.constants import (ACTIVE, DOCUMENT_EXPIRED_MAIL_BODY,
        DOCUMENT_EXPIRING_TODAY_MAIL_BODY, DOUCMENT_EXPIRING_TOMORROW_MAIL_BODY,
        DOCUMENT_EXPIRING_MAIL_BODY, DOCUMENT_EXPIRY_DATE_UNAVAILABLE, DOCUMENT_UNAVAILABLE)
from django.core.mail import EmailMessage
from django.conf import settings

MEDIA_UPLOAD_STRUCTURE = getattr(settings, "MEDIA_UPLOAD_STRUCTURE", "")


def generate_file_path(instance, filename):
    """
    Returns the file path as per the defined directory structure.
    """

    doc_code = instance.document_type.code.replace(" ", "_") if hasattr(instance, "document_type") else ""
    module_name = instance._meta.app_label + "s"
    instance_label = instance._meta.object_name.lower().replace("document", "")
    if hasattr(instance, instance_label+"_id"):
        instance_handle = instance_label + "_" + str(getattr(instance, instance_label+"_id"))
    else:
        instance_handle = instance_label + "_" + str(instance.id)

    file_name = str(date.today()) + "/" + CommonHelper().get_unique_friendly_id() + "/" + filename.upper()

    return MEDIA_UPLOAD_STRUCTURE.format(
        module_name=module_name,
        instance_handle=instance_handle,
        doc_code=doc_code,
        file_name=file_name
    ).replace("//", "/")


def mark_driver_onboarding(mandatory_doc_type, driver, driver_document=None, vehicle=None, status=None):
    if driver:
        missing_doc = []
        driver_status = status or driver.status
        if driver_status == StatusPipeline.ACTIVE:
            for doc_type in mandatory_doc_type:
                if driver_document:
                    docs = [doc for doc in driver.driver_document.all() if
                           doc.status == ACTIVE and doc.document_type == doc_type]
                else:
                    docs = [doc for doc in vehicle.vehicle_document.all() if
                           doc.status == ACTIVE and doc.document_type == doc_type ]
                if not docs:
                    missing_doc.append(str(doc_type))

        # DONOT SAVE CHANGES HERE AS THIS FUNCTION IS GETTING USED TO FETCH MISSING DOCS

        # if missing_doc:
        #     print('Queries Count at begin dd s: %d' % len(connection.queries))
        #     driver.status = StatusPipeline.ONBOARDING
            # driver.save()
        return missing_doc if missing_doc else ''


def get_manager_city_id(user):
    from blowhorn.address.models import City

    return City.objects.filter(managers=user).values_list('id')

def mark_document_expired():
    from blowhorn.driver.models import DriverDocument
    from blowhorn.document.constants import EXPIRED
    from blowhorn.vehicle.models import VehicleDocument
    DriverDocument.objects.filter(expiry_date__lt=timezone.now().date(),
                                  document_type__is_expire_needed=True).update(status=EXPIRED)
    VehicleDocument.objects.filter(expiry_date__lt=timezone.now().date(),
                                   document_type__is_expire_needed=True).update(status=EXPIRED)

def send_mail_to_spocs(name, driver, vehicle, expiry_days=None, document_absent = False):
    from blowhorn.trip.models import Trip
    '''
        This function sends email to spocs on the basis of
        expiry date for vehicle insurance
    '''
    vehicle = vehicle.registration_certificate_number

    trips = Trip.objects.filter(driver = driver.id)

    if trips.exists():
        latest_trip = trips.latest('trip_creation_time')

        if not latest_trip.customer_contract:
            return

        spocs_email_list = [str(x) for x in latest_trip.customer_contract.spocs.all()]
        spocs_email = ', '.join(spocs_email_list)

        # supervisors_email_list = [str(x) for x in latest_trip.customer_contract.supervisors.all()]
        # supervisors_email = ', '.join(supervisors_email_list)

        to_list = spocs_email_list
        flag = False
        # + supervisors_email_list

        lines = ['Dear Spocs,',]
        if document_absent:
            s = '{2} Unavailable | {0} | {1}'
            b = DOCUMENT_UNAVAILABLE

        elif expiry_days is None:
            s = '{2} Details Unavailable | {0} | {1}'
            b = DOCUMENT_EXPIRY_DATE_UNAVAILABLE

        elif expiry_days == 0:
            s = '{2} Expiring Today | {0} | {1}'
            b = DOCUMENT_EXPIRING_TODAY_MAIL_BODY

        elif expiry_days == 1:
            s = '{2} Expiring Tomorrow | {0} | {1}'
            b = DOUCMENT_EXPIRING_TOMORROW_MAIL_BODY

        elif expiry_days in range(2,31):
            s = '{2} Expiring Soon | {0} | {1}'
            b = DOCUMENT_EXPIRING_MAIL_BODY

        elif expiry_days < 0:
            s = '{2} Expired | {0} | {1}'
            b = DOCUMENT_EXPIRED_MAIL_BODY
        else:
            flag = True

        subject = s.format(driver.name,vehicle,name)
        if not flag:
            lines.append(b.format(name,driver.name,expiry_days))
            lines.append('<br>')
            lines.append('Regards,')
            lines.append('Team Blowhorn')
            body = '<br>'.join(lines)

            if len(to_list) !=0:
                mail = EmailMessage(subject, body, to=[to_list],
                            from_email=settings.DEFAULT_FROM_EMAIL)
                mail.content_subtype = 'html'
                # mail.send()

def send_alert_to_driver():
    from blowhorn.driver.models import Driver, DriverDocument, DriverConstants
    from blowhorn.vehicle.models import VehicleDocument
    from blowhorn.common.notification import Notification
    time_period = timezone.now().date() + timedelta(days=int(DriverConstants().get('EXPIRY_DAYS', 30)))
    drivers = Driver.objects.filter(
        Q(driver_document__expiry_date__range=(timezone.now().date(), time_period)) |
        Q(drivervehiclemap__vehicle__vehicle_document__expiry_date__range=(
        timezone.now().date(), time_period))).distinct('id')

    for driver in drivers:
        data = []
        driver_doc = DriverDocument.objects.filter(expiry_date__range=(timezone.now().date(), time_period), driver=driver)
        driver_doc = driver_doc.prefetch_related('document_type')
        vehicle_doc = VehicleDocument.objects.filter(expiry_date__range=(timezone.now().date(), time_period),
                                                     vehicle__drivervehiclemap__driver=driver)
        vehicle_doc = vehicle_doc.prefetch_related('document_type')

        for doc in driver_doc:
            days = (doc.expiry_date - timezone.now().date()).days
            data.append({
                'file': doc.file.url,
                'document_type': doc.document_type.name,
                'expiry_date': str(doc.expiry_date),
                'description': 'Your document will expire in %s days' % days

                })
        for doc in vehicle_doc:
            days = (doc.expiry_date - timezone.now().date()).days
            data.append({
                'file': doc.file.url,
                'document_type': doc.document_type.name,
                'expiry_date': str(doc.expiry_date),
                'description': 'Your doc will expire in %s days' % days
                })

        if data:
            message = {
                'blowhorn_notification': {
                    'action': 'doc_expiry',
                    'details': data
                }
            }
            Notification.push_to_fcm_device(driver.user, message)
