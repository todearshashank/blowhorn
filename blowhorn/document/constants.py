DRIVER_DOCUMENT = 'Driver Document'
VEHICLE_DOCUMENT = 'Vehicle Document'
VENDOR_DOCUMENT = 'Vendor Document'
DOCUMENT_NOT_SPECIFIED = 'Not Specified'

VERIFICATION_PENDING = "verification_pending"
ACTIVE = "active"
INACTIVE = "inactive"
REJECT = 'Rejected'
EXPIRED = "Expired"

DOCUMENT_RC = 'RC'
DOCUMENT_COVID_PASS = 'COVIDPass'
DOCUMENT_PHOTO = 'Photo'
DOCUMENT_PAN = 'PAN'
DOCUMENT_DL = 'DL'
DOCUMENT_AADHAR = 'Aadhaar'
BANK_ACCOUNT= 'Bank_Passbook'


DOCUMENT_STATUSES = [
    {
        'code': VERIFICATION_PENDING,
        'name': "Verification pending",
    },
    {
        'code': ACTIVE,
        'name': "Active",
    },
    {
        'code': INACTIVE,
        'name': "Inactive"
    }
]

BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING = {
    'DRIVING_LICENCE': 'DL',
    'VOTER_ID': 'Voter_ID',
    'AADHAR': 'Aadhaar',
    'RC': 'RC',
    # 'BANK_ACCOUNT': 'Bank_Passbook',
    'PAN': 'PAN'
}

BETTERPLACE_DRIVER_DOCUMENT_CODE = ['DRIVING_LICENCE', 'VOTER_ID', 'AADHAR', 'PAN']
BETTERPLACE_VEHICLE_DOCUMENT_CODE = ['RC']

BETTERPLACE_DOCUMENT_TYPE_CHOICES = (
    ('DRIVING_LICENCE', 'Driving Licence'),
    ('VOTER_ID', 'Voter ID'),
    ('AADHAR', 'AADHAR'),
    # ('BANK_ACCOUNT', 'Bank Account'),
    ('PAN', 'PAN'),
    ('RC', 'RC'),
)

STATE_REQUIRED_FOR_THE_DOCUMENTS = ['DRIVING_LICENCE', 'VOTER_ID']


DOC_STATUS_IDENTIFIER_MAPPING={
    'DL': 'driving_license_number',
    'PAN': 'pan',
    'RC': 'registration_certificate_number'
    # 'Bank_Passbook': 'account_number',

}


DOC_VALIDATION_REGEX_MAPPING = {
    'PAN': r'^[A-Za-z]{5}\d{4}[A-Za-z]{1}$',
    'RC':  r'^[A-Z]{2}[0-9]{1,2}'

}

DOCUMENT_EXPIRED_MAIL_BODY = 'The {0} for driver {1} has expired.' + \
                ' Please make sure the document is renewed and updated in the system' + \
                ' immediately for uninterrupted services.'

DOCUMENT_EXPIRING_TODAY_MAIL_BODY = 'The {0} for the driver {1} is expiring today.' + \
                ' Please make sure the document is renewed and updated in the system' + \
                ' for uninterrupted services.'

DOUCMENT_EXPIRING_TOMORROW_MAIL_BODY = 'The {0} for the driver {1} is expiring tomorrow.' + \
                ' Please make sure the document is renewed and updated in the system' + \
                ' for uninterrupted services.'

DOCUMENT_EXPIRING_MAIL_BODY = 'The {0} for the driver {1} is going to expire in {2} days.' + \
                ' Please make sure the document is renewed and updated in the system' + \
                ' for uninterrupted services.'

DOCUMENT_EXPIRY_DATE_UNAVAILABLE = 'The {0} expiry date for driver {1}' + \
                ' is not present in the system. It is mandatory to have' + \
                ' details with expiry date.<br>Please action and update.'

DOCUMENT_UNAVAILABLE = 'The {0} for driver {1}' + \
                ' is not present in the system. It is mandatory to have' + \
                ' document with expiry date.<br>Please action and update.'

