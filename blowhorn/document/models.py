from django.db import models
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField
from django.core.validators import RegexValidator
from django.core.exceptions import ImproperlyConfigured, ValidationError
from blowhorn.oscar.core.validators import non_python_keyword
from blowhorn.oscar.models.fields import AutoSlugField
from datetime import date, datetime
from django_fsm import FSMField, transition
from .file_validator import FileValidator
from .constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT, DOCUMENT_NOT_SPECIFIED, DOCUMENT_RC
from config.settings.status_pipelines import BACKGROUND_VERIFICATION_STATUSES, PENDING
from .utils import generate_file_path
from blowhorn.common.models import BaseModel
from blowhorn.document.constants import VERIFICATION_PENDING, ACTIVE, INACTIVE, \
        REJECT, DOC_STATUS_IDENTIFIER_MAPPING, DOCUMENT_COVID_PASS, DOCUMENT_DL, DOCUMENT_PAN
from blowhorn.document.document_transition_conditions import (
    is_action_owner_or_superuser, is_doc_eligible_for_activate
)
from config.settings import status_pipelines as StatusPipeline
from postgres_copy import CopyManager
from blowhorn.common.middleware import current_request
from django.utils import timezone


def mark_other_doc_inactive(self):
    """
    make old doc of same type inactive
    :return:
    """
    from blowhorn.driver.models import DriverDocument
    from blowhorn.vehicle.models import VehicleDocument, VendorDocument

    if self.driver_document:
        document_type = self.driver_document.document_type
        DriverDocument.objects.filter(
            driver=self.driver_document.driver,
            status=StatusPipeline.ACTIVE,
            document_type=document_type,
            document_type__name=document_type.name,
        ).exclude(id=self.driver_document_id).update(status=StatusPipeline.INACTIVE)
    elif self.vehicle_document:
        document_type = self.vehicle_document.document_type
        VehicleDocument.objects.filter(
            vehicle=self.vehicle_document.vehicle,
            status=StatusPipeline.ACTIVE,
            document_type=document_type,
            document_type__name=document_type.name,
        ).exclude(id=self.vehicle_document_id).update(status=StatusPipeline.INACTIVE)
    elif self.vendor_document:
        document_type = self.vendor_document.document_type
        VendorDocument.objects.filter(
            vendor=self.vendor_document.vendor,
            status=StatusPipeline.ACTIVE,
            document_type=document_type,
            document_type__name=document_type.name,
        ).exclude(id=self.vendor_document_id).update(status=StatusPipeline.INACTIVE)


def get_attributes(doc):
    if doc.driver_document:
        code = doc.driver_document.document_type.code
        key = DOC_STATUS_IDENTIFIER_MAPPING.get(code, '')
        number = doc.driver_document.number
    elif doc.vehicle_document:
        code = doc.vehicle_document.document_type.code
        key = DOC_STATUS_IDENTIFIER_MAPPING.get(code,'')
        number = doc.vehicle_document.number
    elif doc.vendor_document_id:
        code = doc.vendor_document.document_type.code
        key = DOC_STATUS_IDENTIFIER_MAPPING.get(code,'')
        number = doc.vendor_document.number
    return key, number


def is_doc_number_exist(doc):
    from blowhorn.driver.models import Driver
    from blowhorn.vehicle.models import Vehicle, Vendor
    key, number = get_attributes(doc)
    record = None
    if key and number:
        if doc.driver_document:
            driver = doc.driver_document.driver
            record = Driver.objects.filter(**{key: number}).exclude(id=driver.id)
        elif doc.vehicle_document:
            vehicle = doc.vehicle_document.vehicle_id
            record = Vehicle.objects.filter(**{key: number}).exclude(id=vehicle)
        elif doc.vendor_document_id and key != 'driving_license_number':
            # vendor model does not have driving_license_number
            #not updating it when activating
            vendor_id = doc.vendor_document.vendor_id
            record = Vendor.objects.filter(**{key: number}).exclude(id=vendor_id)
        return record
    return None


def save_archive(self, archive, status):
    from blowhorn.driver.util import get_driver_missing_doc
    from blowhorn.vehicle.models import Vehicle, Vendor
    from blowhorn.driver.models import Driver
    mandatory_driver_doc_type = DocumentType.objects.filter(is_document_mandatory=True, identifier=DRIVER_DOCUMENT)
    mandatory_vehicle_doc_type = DocumentType.objects.filter(is_document_mandatory=True,
                                                             identifier=VEHICLE_DOCUMENT)

    if self.driver_document:
        driver = self.driver_document.driver
        code = self.driver_document.document_type.code
        key = DOC_STATUS_IDENTIFIER_MAPPING.get(code,'')
        number = self.driver_document.number

        if code == DOCUMENT_COVID_PASS and status==ACTIVE:
            driver_data = {'has_covid_pass' : True}
            Driver.objects.filter(id=driver.id).update(**driver_data)

        if key and number and status==ACTIVE:
            driver_data = {key : number}
            Driver.objects.filter(id=driver.id).update(**driver_data)

        archive.driver_document_id = self.driver_document_id
        self.driver_document.status = status
        self.driver_document.save()

    elif self.vehicle_document:
        archive.vehicle_document_id = self.vehicle_document_id
        self.vehicle_document.status = status
        code = self.vehicle_document.document_type.code
        key = DOC_STATUS_IDENTIFIER_MAPPING.get(code,'')
        number=self.vehicle_document.number

        if key and number and status==ACTIVE:
            Vehicle.objects.filter(id=self.vehicle_document.vehicle_id).update(**{key: number})
        driver = self.vehicle_document.vehicle.driver_set.first()
        self.vehicle_document.save()

    elif self.vendor_document_id:
        driver = None
        archive.vendor_document_id = self.vendor_document_id
        self.vendor_document.status = status
        code = self.vendor_document.document_type.code
        key = DOC_STATUS_IDENTIFIER_MAPPING.get(code,'')
        number = self.vendor_document.number

        if key and number and status==ACTIVE and key != 'driving_license_number':
            Vendor.objects.filter(id=self.vendor_document.vendor_id).update(**{key: number})
        self.vendor_document.save()
    if not self.vendor_document_id:
        missing_doc = get_driver_missing_doc(
            driver, mandatory_driver_doc_type, mandatory_vehicle_doc_type
        )

        if driver and status == ACTIVE and (missing_doc or driver.status == StatusPipeline.SUPPLY):
            driver = Driver.objects.get(id=driver.id)
            driver.status = StatusPipeline.ONBOARDING
            driver.save()
    archive.save()


@python_2_unicode_compatible
class DocumentType(models.Model):
    name = models.CharField(
        _("Document Type"),
        max_length=64,
        null=False,
        unique=True,
        help_text=_("Document name like Registration Certificate, Licence, etc.."),
    )
    code = models.CharField(
        _("Document Code"),
        max_length=25,
        validators=[
            RegexValidator(
                regex=r"^[a-zA-Z_][a-zA-Z_]+$",
                message=_("Code can only contain the letters a-z, A-Z and underscore"),
            ),
            non_python_keyword,
        ],
        help_text=_("Short name for document. RC for Registration Certificate, etc.."),
    )
    creation_date = models.DateTimeField(_("Creation Date"), auto_now_add=True)

    IDENTIFIERS = (
        (DRIVER_DOCUMENT, _("Driver Document")),
        (VEHICLE_DOCUMENT, _("Vehicle Document")),
        (DOCUMENT_NOT_SPECIFIED, _("Not Specified")),
    )

    identifier = models.CharField(
        _("Document Identifier"),
        default=DOCUMENT_NOT_SPECIFIED,
        max_length=100,
        choices=IDENTIFIERS,
    )

    is_document_mandatory = models.BooleanField(
        _("Is Mandatory"), default=False, db_index=True
    )
    is_expire_needed = models.BooleanField(
        _("Is Expire Needed"), default=False, db_index=True
    )
    is_mandatory_for_registration = models.BooleanField(
        _("Is Mandatory for Registration"), default=False, db_index=True
    )
    is_driver_document = models.BooleanField(
        _("Is Driver Document"), default=False, db_index=True
    )
    is_fleet_document = models.BooleanField(
        _("Is Fleet Document"), default=False, db_index=True
    )
    is_ocr_eligible = models.BooleanField(
        _("Is OCR "), default=False, db_index=True
    )
    sequence = models.PositiveIntegerField(_('Sequence'), default = 0)
    
    instruction_heading = models.CharField(_("Instruction Heading"), 
        max_length=255, null=True, blank=True
    )
    instructions = JSONField(blank=True, null=True)

    no_of_pages = models.PositiveIntegerField(_('No of Pages'), default = 1)

    regex = models.CharField(_("Regex"), default='.*', max_length=100)

    error_message = models.CharField(_("error message"), default='error', max_length=150)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Document Type")
        verbose_name_plural = _("Document Types")


@python_2_unicode_compatible
class DocumentPage(BaseModel):
    page_number = models.PositiveIntegerField(_("Page Number"))

    file = models.FileField(
        upload_to=generate_file_path, validators=[FileValidator()], max_length=256)

    uploaded_at = models.DateTimeField(_('Date Uploaded'), auto_now_add = True)

    def __str__(self):
        return self.file

    class Meta:
        abstract = True
        app_label = "documentpage"
        verbose_name = _("DocumentPage")
        verbose_name_plural = _("DocumentPages")


@python_2_unicode_compatible
class Document(BaseModel):
    file = models.FileField(
        upload_to=generate_file_path, validators=[FileValidator()], max_length=256
    )
    document_type = models.ForeignKey(
        DocumentType,
        on_delete=models.PROTECT,
        help_text=_("Document type like Registration Certificate, Licence, etc.."),
    )

    number = models.CharField(
        _("Document Number"),
        max_length=100,
        default="----",
        help_text=_("Input the number from the file uploaded.."),
    )

    VERIFICATION_PENDING = "verification_pending"
    ACTIVE = "active"
    INACTIVE = "inactive"
    REJECTED = REJECT
    EXPIRED = "Expired"
    DOCUMENT_STATUSES = (
        (VERIFICATION_PENDING, _("Verification pending")),
        (ACTIVE, _("Active")),
        (INACTIVE, _("Inactive")),
        (REJECTED, _("Rejected")),
        (EXPIRED, _("Expired")),
    )

    state = models.ForeignKey(
        "address.State",
        null=True,
        blank=True,
        verbose_name="Document Issuing State",
        on_delete=models.PROTECT,
    )

    status = models.CharField(
        _("Status"),
        max_length=25,
        choices=DOCUMENT_STATUSES,
        default=VERIFICATION_PENDING,
        help_text=_("Current status of document. Active/inactive, etc.."),
    )

    bgv_status = models.CharField(
        _("Verification Status"),
        max_length=30,
        choices=BACKGROUND_VERIFICATION_STATUSES,
        default=PENDING,
        help_text=_("Current status of document verification."),
    )

    bgv_remark = models.CharField(
        _("Background Verification Remark"), max_length=255, null=True, blank=True
    )

    verification_requested_on = models.DateField(
        _("Document Verification Requested On"), null=True, blank=True
    )
    verification_completed_on = models.DateField(
        _("Document Verification Completed On"), null=True, blank=True
    )

    verification_extra_details = JSONField(blank=True, null=True)

    expiry_date = models.DateField(
        _("Expiry Date"), blank=True, null=True, help_text=_("Expiry date of document")
    )
    date_uploaded = models.DateTimeField(_("Date uploaded"), auto_now_add=True)

    def __str__(self):
        return self.file

    class Meta:
        abstract = True
        app_label = "document"
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")


class DocumentEvent(BaseModel):

    status = FSMField(default=VERIFICATION_PENDING, protected=False, db_index=True)

    action_owner = models.ForeignKey(
        "users.User", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    driver_document = models.ForeignKey(
        "driver.DriverDocument", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    vehicle_document = models.ForeignKey(
        "vehicle.VehicleDocument", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    vendor_document = models.ForeignKey(
        "vehicle.VendorDocument", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    copy_data = CopyManager()
    objects = models.Manager()

    def __str__(self):
        return str(self.id)

    @transition(
        field=status,
        source=VERIFICATION_PENDING,
        target=ACTIVE,
        permission=is_action_owner_or_superuser,
        conditions=[is_doc_eligible_for_activate]
    )
    def activate(self):
        print("activating document ")
        archive = DocumentArchive(
            new_status=ACTIVE,
            action_owner_id=self.action_owner_id,
            old_status=VERIFICATION_PENDING,
            document_event=self,
        )
        # self.action_owner = None
        save_archive(self, archive, ACTIVE)
        mark_other_doc_inactive(self)
        # self.delete()

    @transition(
        field=status,
        source=ACTIVE,
        target=INACTIVE,
        permission=is_action_owner_or_superuser,
    )
    def inactivate(self):
        print("marking document inactive ")
        archive = DocumentArchive(
            new_status=INACTIVE,
            action_owner_id=self.action_owner_id,
            old_status=ACTIVE,
            document_event=self,
        )
        # mark_driver_onboarding(self.driver_document)
        if hasattr(self, "reason"):
            archive.reason = self.reason

        if self.driver_document:
            driver = self.driver_document.driver
            if self.driver_document.document_type.code == DOCUMENT_DL:
                driver.driving_license_number = None
            if self.driver_document.document_type.code == DOCUMENT_PAN:
                driver.pan = None
            driver.save()
        if self.vendor_document:
            vendor = self.vendor_document.vendor
            if self.vendor_document.document_type.code == DOCUMENT_PAN:
                vendor.pan = None
                vendor.save()
        save_archive(self, archive, INACTIVE)

    @transition(
        field=status,
        source=VERIFICATION_PENDING,
        target=REJECT,
        permission=is_action_owner_or_superuser,
    )
    def reject(self):
        print("rejecting inactive ")
        archive = DocumentArchive(
            new_status=REJECT,
            action_owner_id=self.action_owner_id,
            old_status=VERIFICATION_PENDING,
            document_event=self,
        )

        # Remove the driver vehicle mapping is rc is rejected
        if self.vehicle_document and self.vehicle_document.document_type.code == DOCUMENT_RC:
            from blowhorn.integration_apps.invoid.driver_mixin import DriverAddUpdateMixin
            DriverAddUpdateMixin().delete_drivervehicle_mapping(self.vehicle_document.vehicle,
                                        self.action_owner)

        if hasattr(self, "reason"):
            archive.reason = self.reason
        save_archive(self, archive, REJECT)


class DocumentArchive(BaseModel):

    old_status = models.CharField(_("Old Status"), max_length=125)

    new_status = models.CharField(_("New Status"), max_length=125)

    reason = models.CharField(_("Reason"), null=True, blank=True, max_length=250)

    action_owner = models.ForeignKey(
        "users.User", null=True, blank=True, on_delete=models.PROTECT
    )

    document_event = models.ForeignKey(DocumentEvent, on_delete=models.DO_NOTHING)

    driver_document = models.ForeignKey(
        "driver.DriverDocument", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    vehicle_document = models.ForeignKey(
        "vehicle.VehicleDocument", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    vendor_document = models.ForeignKey(
        "vehicle.VendorDocument", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return str(self.id)


@python_2_unicode_compatible
class DocumentClass(models.Model):
    name = models.CharField(
        _("Document Class"),
        max_length=128,
        unique=True,
        null=False,
        help_text=_("Document class like Proof of address, etc.."),
    )
    slug = AutoSlugField(_("Slug"), max_length=128, unique=True, populate_from="name")
    document_types = models.ManyToManyField(
        "document.DocumentType", blank=True, verbose_name=_("Types")
    )
    creation_date = models.DateTimeField(_("Creation Date"), auto_now_add=True)

    class Meta:
        app_label = "document"
        verbose_name = _("Document Class")
        verbose_name_plural = _("Document Classes")

    def __str__(self):
        return self.name

    @property
    def document_type_summary(self):
        _types = [o.name for o in self.document_types.all()]
        return ", ".join(_types)
