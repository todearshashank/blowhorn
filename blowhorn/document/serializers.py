from rest_framework import serializers
from django.utils import timezone

from blowhorn.document.models import DocumentType, DocumentPage
from blowhorn.document.constants import VERIFICATION_PENDING, ACTIVE, EXPIRED
from blowhorn.driver.models import DriverConstants


class DocumentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentType
        fields = ('code', 'name', 'no_of_pages')


class DocumentPageSerializer(serializers.ModelSerializer):
    class Meta:
        # model = DocumentPage
        fields = ('page_number', 'file', 'uploaded_at')
        abstract = True


class DocumentSerializer(serializers.ModelSerializer):
    doc_status = serializers.SerializerMethodField()
    doc_state = serializers.SerializerMethodField(required=False)
    status_id = serializers.SerializerMethodField()

    def get_doc_status(self, obj):
        if obj.expiry_date:
            if obj.expiry_date < timezone.now().date():
                return EXPIRED
            days_to_expire = (obj.expiry_date - timezone.now().date()).days
            if obj.status == ACTIVE and \
                days_to_expire <= int(
                    DriverConstants().get('DAYS_TO_ALERT_USER_BEFORE_DOC_EXPIRY', 30)):
                return 'Expires in %s days' % days_to_expire if days_to_expire > 1\
                    else 'Expires today'
        if obj.status == VERIFICATION_PENDING:
            return 'Pending'

        return obj.status

    def get_status_id(self, obj):
        """
        status id used for sorting
        :param obj:
        :return:
        """
        if obj.expiry_date:
            if obj.expiry_date < timezone.now().date():
                return 4
            days_to_expire = (obj.expiry_date - timezone.now().date()).days
            if obj.status == ACTIVE and \
                days_to_expire <= int(
                    DriverConstants().get('DAYS_TO_ALERT_USER_BEFORE_DOC_EXPIRY', 30)):
                return 1
        if obj.status == VERIFICATION_PENDING:
            return 3

        if obj.status == ACTIVE:
            return 2
        return 5

    def get_doc_state(self, obj):
        if obj.state:
            return str(obj.state)
        return ''

    class Meta:
        # model = Document
        fields = (
            'id', 'file', 'document_type', 'number', 'status', 'doc_status', 'doc_state',
            'expiry_date', 'status_id')
        abstract = True


class ExtendedDocumentSerializer(DocumentSerializer):
    class Meta:
        # model = Document
        fields = DocumentSerializer.Meta.fields + ('number', 'state')
        abstract = True
