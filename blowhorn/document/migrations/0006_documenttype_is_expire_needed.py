# Generated by Django 2.1.1 on 2019-03-05 12:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0005_auto_20190115_0645'),
    ]

    operations = [
        migrations.AddField(
            model_name='documenttype',
            name='is_expire_needed',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Is Expire Needed'),
        ),
    ]
