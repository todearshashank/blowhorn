from django.conf import settings
from django import forms
from datetime import timedelta
from django.urls import reverse
from django.db.models import Q, Func, CharField, F, Value, Case, When
from django.contrib import admin, messages
from .models import DocumentType, DocumentArchive, DocumentEvent, DocumentPage
from .models import DocumentClass, mark_other_doc_inactive, save_archive
from django.utils.html import format_html, mark_safe
from blowhorn.document.constants import VERIFICATION_PENDING, ACTIVE
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from .utils import get_manager_city_id
from blowhorn.utils.functions import get_in_ist_timezone, utc_to_ist
from django.utils import timezone
import os
import csv
from django.http import HttpResponse
from django.contrib.admin.helpers import ActionForm
from blowhorn.users.models import User
from blowhorn.driver.models import DriverDocument, DriverDocumentPage
from blowhorn.vehicle.models import VehicleDocument, VehicleDocumentPage, VendorDocumentPage


class DocumentClassAdmin(admin.ModelAdmin):
    list_display = ("name", "document_type_summary", "creation_date")
    search_fields = ["name"]


class DocumentPageAdmin(admin.ModelAdmin):
    list_display = ("page_number", "file", "uploaded_at")

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_delete_permission(self, request, obj=None):
        return False


class DocumentTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "no_of_pages","identifier",  "sequence",
                        "is_driver_document", "is_fleet_document", 
                        "is_document_mandatory", "is_ocr_eligible",
                        "is_mandatory_for_registration", "instruction_heading", "instructions")
    
    search_fields = ["name", "code"]
    
    list_filter = ["is_driver_document", "is_fleet_document", 
                        "is_document_mandatory", "is_ocr_eligible",
                        "is_mandatory_for_registration"]

class DocumentBaseAdmin(admin.ModelAdmin):
    search_fields = [
        "driver_document__driver__name",
        "vehicle_document__vehicle__registration_certificate_number",
        "driver_document__driver__vehicles__registration_certificate_number",
        "action_owner__email",
    ]

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_delete_permission(self, request, obj=None):
        return False


def mark_document_active(modeladmin, request, queryset):
    if queryset.filter(status=ACTIVE).exists():
        messages.error(request, "Some Document Active Before.")
        return

    for doc in queryset:
        from blowhorn.document.models import is_doc_number_exist
        record = is_doc_number_exist(doc)
        if record and record.exists():
            messages.error(request,
                           'Duplicate doc(%s)!!document number exists check %s' % (doc.id, record))
        archive = DocumentArchive(
            new_status=ACTIVE,
            action_owner_id=doc.action_owner_id,
            old_status=VERIFICATION_PENDING,
            document_event=doc,
        )
        if doc.driver_document:
            archive.driver_document_id = doc.driver_document_id
            doc.driver_document.status = ACTIVE
            doc.driver_document.save()
        elif doc.vehicle_document:
            archive.vehicle_document_id = doc.vehicle_document_id
            doc.vehicle_document.status = ACTIVE
            doc.vehicle_document.save()
        elif doc.vendor_document_id:
            archive.vendor_document_id = doc.vendor_document_id
            doc.vendor_document.status = ACTIVE
            doc.vendor_document.save()
        save_archive(doc, archive, ACTIVE)
        doc.status = ACTIVE
        # doc.action_owner_id = None
        doc.save()
        mark_other_doc_inactive(doc)
    messages.success(request, "Document Set to Active.")
    # doc.delete()


class DocumentArchiveInlineAdmin(admin.TabularInline):
    fields = [
        "id",
        "get_driver",
        "old_status",
        "new_status",
        "created_by",
        "action_owner",
        "get_reason",
    ]

    extra = 0
    model = DocumentArchive

    def get_readonly_fields(self, request, obj=None):
        return (
            "id",
            "get_driver",
            "old_status",
            "new_status",
            "action_owner",
            "created_by",
            "action_owner",
            "get_reason",
        )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_reason(self, obj):
        return obj.reason

    get_reason.short_description = "reason"

    def get_driver(self, obj):
        if obj.driver_document:
            return obj.driver_document.driver
        elif obj.vehicle_document:
            return [str(x) for x in obj.vehicle_document.vehicle.driver_set.all()]

    get_driver.short_description = "Driver"

    def get_queryset(self, request):
        qs = super(DocumentArchiveInlineAdmin, self).get_queryset(request)
        qs = qs.prefetch_related(
            "driver_document", "driver_document__driver", "vehicle_document"
        )
        qs = qs.select_related("action_owner", "created_by")

        return qs


class DocumentForm(forms.ModelForm):
    reason = forms.CharField(required=False)
    reasoninactive = forms.CharField(required=False)


def export_document_data(modeladmin, request, queryset):
    pass


def export_document_event(modeladmin, request, queryset):
    if queryset:
        document_events = queryset.values_list("id", flat=True)
        queryset = DocumentEvent.copy_data.filter(id__in=document_events)
    else:
        queryset = DocumentEvent.copy_data.all()
    queryset = queryset.order_by("-id")

    file_part = "document_event"
    queryset.annotate(
        formatted_created_date=Func(
            F("created_date") + timedelta(hours=5.5),
            Value("DD/MM/YYYY HH24:MI:SS"),
            function="to_char",
            output_field=CharField(),
        ),
        driver=Case(
            When(
                driver_document__isnull=False, then=F("driver_document__driver__name")
            ),
            output_field=CharField(),
        ),
        operating_city=Case(
            When(
                driver_document__isnull=False,
                then=F("driver_document__driver__operating_city__name"),
            ),
            output_field=CharField(),
        ),
        vehicle=Case(
            When(
                vehicle_document__isnull=False,
                then=F("vehicle_document__vehicle__registration_certificate_number"),
            ),
            output_field=CharField(),
        ),
        vendor=Case(
            When(
                vendor_document__isnull=False, then=F("vendor_document__vendor__name")
            ),
            output_field=CharField(),
        ),
        document_type=Case(
            When(
                driver_document__isnull=False,
                then=F("driver_document__document_type__name"),
            ),
            When(
                vehicle_document__isnull=False,
                then=F("vehicle_document__document_type__name"),
            ),
            When(
                vendor_document__isnull=False,
                then=F("vendor_document__document_type__name"),
            ),
            output_field=CharField(),
        ),
    ).to_csv(
        file_part,
        "id",
        "driver",
        "operating_city",
        "vehicle",
        "vendor",
        "document_type",
        "status",
        "action_owner__email",
        "formatted_created_date",
        "created_by__email",
    )

    file_name = (
        "%s_" % str(utc_to_ist(timezone.now()))
        + os.path.splitext(file_part)[0]
        + ".csv"
    )
    with open(file_part, "r") as inFile, open(file_name, "w") as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow(
            [
                "Event ID",
                "Driver",
                "Operating City",
                "Vehicle",
                "Vendor",
                "Document Type",
                "Status",
                "Action Owner",
                "Created At",
                "Created By",
            ]
        )

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1

        os.remove(file_part)

    if os.path.exists(file_name):
        with open(file_name, "rb") as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response["Content-Disposition"] = "inline; filename=" + os.path.basename(
                file_name
            )
            os.remove(file_name)
            return response


def reassign_document(modeladmin, request, queryset):
    """
    Custom Action Button for assigning shipment orders to a driver and create a
    new trip
    """
    new_owner_id = request.POST.get('user')
    if not new_owner_id:
        messages.error(request, "action owner not selected")
        return False
    if queryset.exclude(status=VERIFICATION_PENDING).exists():
        messages.error(request, "Action Owner can be changed only for Verification Pending Document")
        return False
    for query in queryset:
        archive = DocumentArchive(old_status=query.status, action_owner_id=new_owner_id,
                                  new_status=VERIFICATION_PENDING, created_by=request.user,
                                  document_event=query)
        archive.save()
    queryset.update(action_owner_id=new_owner_id)
    messages.success(
        request, "Action Owner Changed successfully")


reassign_document.short_description = ('Reassign Document')


class AssignActionOwnerForm(ActionForm):
    user = forms.ModelChoiceField(
        queryset=User.objects.filter(is_active=True, is_staff=True), required=False)


class DocumentEventAdmin(FSMTransitionMixin, DocumentBaseAdmin):
    list_display = (
        "id",
        "get_driver",
        "get_vendor",
        "get_driver_doc",
        "get_vehicle_doc",
        "get_vendor_doc",
        "get_doc_type",
        "status",
        "action_owner",
        "created_by",
    )
    list_filter = ("status", "driver_document__driver__operating_city")
    form = DocumentForm
    action_form = AssignActionOwnerForm
    actions = [mark_document_active, export_document_event, reassign_document]
    inlines = [DocumentArchiveInlineAdmin]
    fsm_field = ["status"]

    def get_actions(self, request):
        """
        Remove reassign for non super user
        """
        actions = super().get_actions(request)
        if not request.user.is_superuser and not request.user.is_city_manager:
            del actions['reassign_document']
            del actions['mark_document_active']
        return actions

    fieldsets = (
        (
            "General",
            {
                "fields": (
                    "id",
                    "get_driver",
                    "get_driver_doc",
                    "get_vehicle_doc",
                    "get_vendor_doc",
                    "status",
                    "reason",
                    "reasoninactive",
                    "created_by",
                    "get_doc_number"
                )
            },
        ),
        ("Document Pages",
         {
             "fields": (
                 'get_document_pages',
             )
         })
    )

    def get_readonly_fields(self, request, obj=None):
        return (
            "id",
            "get_driver",
            "get_driver_doc",
            "get_vehicle_doc",
            "get_vendor_doc",
            "status",
            "action_owner",
            "get_doc_type",
            "created_by",
            "get_doc_number",
            "get_document_pages"
        )

    def get_driver_doc(self, obj):
        if obj.driver_document:
            url = obj.driver_document.file.url
            return format_html(
                '<a href="%s" target="_blank"><img src="%s" height="100" width="100"/></a>'
                % (url, url)
            )
        return None

    get_driver_doc.short_description = "Driver Document"
    get_driver_doc.allow_tags = True

    def get_doc_type(self, obj):
        if obj.vehicle_document:
            return obj.vehicle_document.document_type
        elif obj.driver_document:
            return obj.driver_document.document_type
        elif obj.vendor_document:
            return obj.vendor_document.document_type
        return None

    get_doc_type.short_description = "Document Type"

    def get_doc_number(self, obj):
        if obj.vehicle_document:
            return obj.vehicle_document.number
        elif obj.driver_document:
            return obj.driver_document.number
        elif obj.vendor_document:
            return obj.vendor_document.number
        return None

    get_doc_number.short_description = "Document Number"

    def get_vehicle_doc(self, obj):
        if obj.vehicle_document:
            url = obj.vehicle_document.file.url
            return format_html(
                '<a href="%s" target="_blank"><img src="%s" height="100" width="100"/></a>'
                % (url, url)
            )
        return None

    get_vehicle_doc.short_description = "Vehicle Document"

    def get_vendor_doc(self, obj):
        if obj.vendor_document:
            url = obj.vendor_document.file.url
            return format_html(
                '<a href="%s" target="_blank"><img src="%s" height="100" width="100"/></a>'
                % (url, url)
            )
        return None

    get_vendor_doc.short_description = "Vendor Document"

    def get_document_pages(self, obj):
        if obj.driver_document:
            docs = DriverDocumentPage.objects.filter(driver_document=obj.driver_document)
        elif obj.vehicle_document:
            docs = VehicleDocumentPage.objects.filter(vehicle_document=obj.vehicle_document)
        elif obj.vendor_document:
            docs = VendorDocumentPage.objects.filter(vendor_document=obj.vendor_document)
        else:
            return None
        if docs.exists():
            template = """<table><thead>
                            <th style="font-size:15px; padding-left:25px">ID</th>
                            <th style="font-size:15px; padding-left:25px">Document Page</th>
                            <th style="font-size:15px; padding-left:25px">Page Number</th>
                            <th style="font-size:15px; padding-left:25px">Uploaded by</th>
                            <th style="font-size:15px; padding-left:25px; padding-right:25px">Uploaded At</thead><tbody>"""
            for doc in docs.order_by('page_number'):
                template += """
                    <tr>
                        <td style="padding:25px">{id}</td>
                        <td style="padding:25px"><a href={url} target="_blank"><img src={url} height="100" width="100"/></a></td>
                        <td style="padding:25px"><b>{page_number}</b></td>
                        <td style="padding:25px">{uploaded_by}</td>
                        <td style="padding:25px">{uploaded_at}</td>
                    </tr>
                    """.format(id=doc.id, url=doc.file.url, page_number=doc.page_number,
                               uploaded_by=str(doc.created_by),
                               uploaded_at=utc_to_ist(doc.uploaded_at).strftime(
                                   settings.ADMIN_DATETIME_FORMAT))
            template += """</tbody></table>"""
            return mark_safe(template)
        return None

    get_document_pages.short_description = 'Document Pages'

    def get_driver(self, obj):
        if obj.driver_document:
            link = reverse(
                "admin:driver_driver_change", args=[obj.driver_document.driver_id]
            )
            return format_html(
                '<a href="%s">%s</a>' % (link, obj.driver_document.driver)
            )
        elif obj.vehicle_document:
            return [str(x) for x in obj.vehicle_document.vehicle.driver_set.all()]

    get_driver.short_description = "Driver"

    def get_vendor(self, obj):
        if obj.vendor_document:
            link = reverse(
                "admin:vehicle_vendor_change", args=[obj.vendor_document.vendor_id]
            )
            return format_html(
                '<a href="%s">%s</a>' % (link, obj.vendor_document.vendor)
            )

        return None

    get_vendor.short_description = "Vendor"

    def get_queryset(self, request):
        user = request.user
        qs = super(DocumentEventAdmin, self).get_queryset(request)
        qs = qs.prefetch_related(
            "driver_document",
            "driver_document__driver",
            "vehicle_document",
            "vendor_document",
        )
        qs = qs.prefetch_related(
            "driver_document__document_type",
            "vehicle_document__document_type",
            "vendor_document__document_type",
        )
        qs = qs.prefetch_related(
            "driver_document__driver",
            "vehicle_document__vehicle",
            "vehicle_document__vehicle__driver_set",
        )
        qs = qs.prefetch_related("vendor_document__vendor")
        qs = qs.select_related("action_owner", "created_by")

        city_ids = get_manager_city_id(user)
        if city_ids and not user.is_superuser:
            qs = qs.filter(
                Q(driver_document__driver__operating_city_id__in=city_ids)
                | Q(vehicle_document__vehicle__driver__operating_city_id__in=city_ids)
                | Q(vendor_document__vendor__driver__operating_city_id__in=city_ids)
                | Q(vendor_document__vendor__vendorpreferredlocation__city_id__in=city_ids)
            )

        if not user.is_superuser and not city_ids:
            qs = qs.filter(Q(action_owner=user) | Q(created_by=user))
        return qs

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            if "reason" in form.changed_data:
                if form.cleaned_data.get("reason", ""):
                    obj.reason = form.cleaned_data.get("reason", "")

            super().save_model(request, obj, form, change)

    class Media:
        css = {"all": ("/static/website/css/bootstrap.css",)}
        js = (
            "/static/website/js/lib/jquery.min.js",
            "/static/website/js/lib/bootstrap.min.js",
            "admin/js/document/document.js",
        )


admin.site.register(DocumentType, DocumentTypeAdmin)
admin.site.register(DocumentClass, DocumentClassAdmin)
# admin.site.register(DocumentArchive, DocumentArchiveAdmin)
admin.site.register(DocumentEvent, DocumentEventAdmin)
