from django.apps import AppConfig


class DocumentConfig(AppConfig):
    name = 'blowhorn.document'
