import datetime
from datetime import timedelta

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding
from botocore.signers import CloudFrontSigner

from django.conf import settings


def rsa_signer(message):
    with open('cloudfront.pem', 'rb') as key_file:
       private_key = serialization.load_pem_private_key(
           key_file.read(),
           password=None,
            backend=default_backend()
       )
    return private_key.sign(message, padding.PKCS1v15(), hashes.SHA1())

def get_signed_url(url, days=1):
    file_path = settings.CLOUDFRONT_BASE_URL + '/media/' + url
    key_id = settings.CLOUDFRONT_KEY_ID
    expire_date = datetime.datetime.now() + timedelta(days=days)
    cloudfront_signer = CloudFrontSigner(key_id, rsa_signer)
    # Create a signed url that will be valid until the specfic expiry date
    # provided using a canned policy.
    signed_url = cloudfront_signer.generate_presigned_url(
        file_path, date_less_than=expire_date)

    return signed_url

