from django.conf import settings
from blowhorn.common.payment_gateways import *


class Payment:
    def __init__(self, payment_gateway=None):
        if not payment_gateway:
            payment_gateway = settings.DEFAULT_PAYMENT_GATEWAY
        self.payment_gateway = self._get_payment_gateway_settings(payment_gateway)

    def pay(self, amount, reference, payment_gateway=None):
        pass

    def fetch(self, payment_id, account='default'):
        return self.payment_gateway.fetch(payment_id, account=account)

    def capture(self, payment_id, amount, account='default'):
        return self.payment_gateway.capture(payment_id, amount, account=account)

    def all(self, data, account='default'):
        return self.payment_gateway.all(data, account=account)

    def _get_payment_gateway_settings(self, payment_gateway):
        try:
            payment_gateway = eval(payment_gateway)
        except:
            payment_gateway = None
        return payment_gateway
