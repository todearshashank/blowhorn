from itertools import chain, islice, tee
import json
import re
import requests
import logging
import random
import string
from datetime import timedelta, datetime
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from django.core import serializers
from django.core.cache import cache

import python_jwt as jwt
# import Crypto.PublicKey.RSA as RSA
from jwcrypto.jwk import JWK
from jwcrypto.common import base64url_encode

DRIVER_REQUEST_IN_PROGRESS = 'DRIVER_REQUEST_IN_PROGRESS'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

TON = 'TON'
KG = 'KG'
GRAM = 'GM'
EACHES = 'EACHES'
BOX = 'BOX'
PALLET = 'PALLET'

ALLOWED_UOM_ALIAS = {
        'kg' : KG,
        'tonnes' : TON,
        'ton' : TON,
        'tons' : TON,
        'kgs' : KG,
        'kilograms' : KG,
        'kilogram' : KG,
        'g' : GRAM,
        'gm' : GRAM,
        'gms' : GRAM,
        'grams' : GRAM,
        'gram' : GRAM,
        'box' : BOX,
        'boxes' : BOX,
        'pallet' : PALLET,
        'pallets' : PALLET,
}


class DateTimeEncoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)


class CommonHelper:

    def encode_day(self, nday, size, chars):
        a = nday
        nday_enc = []

        while a:
            nday_enc.append(a % size)
            a = int(a / size)

        encoded = ''.join([chars[x] for x in reversed(nday_enc)])
        return encoded

    def get_unique_friendly_id(self, prefix=None):
        chars = string.digits + string.ascii_uppercase
        size = len(chars)
        year = 2015
        friendly_id = None
        reference_date = datetime(year, 1, 1)
        now = datetime.now()
        delta = now - reference_date
        days_since = delta.days
        day = self.encode_day(days_since, size, chars)
        for size in [3, 4]:
            for i in range(5):
                code = ''.join(random.sample(chars, size))
                friendly_id = day + code

        if prefix:
            friendly_id = prefix + '-' + friendly_id

        return friendly_id.upper()

    def get_public_info_from_model_obj(model_obj):
        public_items = {}
        model_obj = vars(model_obj)
        for item in model_obj:
            if not '_' == item[:1]:
                public_items[item] = model_obj[item]

        return public_items

    def get_json_data_from_model_obj(model_obj):

        model_obj = serializers.serialize("json", [model_obj])

        model_obj = json.loads(model_obj[1:-1])

        return model_obj['fields']

    def get_geopoint_from_latlong(latitude, longitude):
        if latitude and longitude:
            if re.match('^[+-]*[0-9.]+$',str(latitude)) and re.match('^[+-]*[0-9.]+$',str(longitude)):
                geopoint_info = '{ "type": "Point", "coordinates": [ %s, %s ] }' % (
                    longitude, latitude)
                return GEOSGeometry(geopoint_info)
        return None

    def get_geopoint_from_location_text(address):
        return None

    def generate_firebase_token(firebase_app_secret):
        # Get your service account's email address and private key from the
        # JSON key file
        service_account_email = settings.SERVICE_ACCOUNT_EMAIL
        private_key_bytes = bytes(settings.FIREBASE_PRIVATE_KEY, 'utf-8')
        private_key = JWK.from_pem(private_key_bytes)
        try:
            payload = {
                "iss": service_account_email,
                "sub": service_account_email,
                "aud": settings.SERVICE_ACCOUNT_AUD,
                "uid": firebase_app_secret,
                "claims": {
                    "premium_account": False,
                }
            }
            exp = timedelta(minutes=settings.FIREBASE_AUTH_TOKEN_EXPIRY_MINUTES)
            jwt_token = jwt.generate_jwt(payload, private_key, "RS256", exp)
            return jwt_token

        except Exception as e:
            print(e)
            logging.info("Error creating custom token ")
            return None

    def generate_api_key(length=30):
        return ''.join(random.sample(
            string.ascii_lowercase + string.ascii_uppercase + string.digits,
            30)
        )

    def get_geopoint_from_location_json(location):

        if location:
            try:
                if isinstance(location, str):
                    location = json.loads(location)
                if isinstance(location, dict):
                    latitude = location.get('mLatitude') if location.get(
                        'mLatitude', None) else location.get('lat', None)
                    longitude = location.get('mLongitude') if location.get(
                        'mLongitude', None) else location.get('lon', None)

                    if latitude and longitude:
                        return CommonHelper.get_geopoint_from_latlong(
                            latitude, longitude)
            except BaseException:
                pass
                # logger.error('Invalid Location json sent')
        return None

    def get_address_from_location_json(location):
        if location:
            try:
                if isinstance(location, str):
                    location = json.loads(location)
                if isinstance(location, dict):
                    latitude = location.get('mLatitude') if location.get(
                        'mLatitude', None) else location.get('lat', None)
                    longitude = location.get('mLongitude') if location.get(
                        'mLongitude', None) else location.get('lon', None)

                    if latitude and longitude:
                        geocode_url = settings.GEOCODE_BASE_URL + '?latlng=%s,%s&key=%s' % (
                            latitude, longitude, settings.GOOGLE_GEOCODING_DISTANCEMATRIX_API_KEY)
                        result = requests.get(geocode_url)
                        address = json.loads(result.content)
                        return address['results']

            except BaseException:
                pass
                # logger.error('Invalid Location json sent')
        return None

    def get_media_url_prefix(self):

        if settings.READ_DOT_ENV_FILE:
            return 'https://s3.%s.amazonaws.com/%s/media/' % (
                settings.AWS_S3_REGION_NAME,
                settings.AWS_STORAGE_BUCKET_NAME
            )
        else:
            return settings.MEDIA_URL

    @staticmethod
    def generate_random_string(size, alphabets_allowed=True, numbers_allowed=True, case_sensitive=False, prefix=''):
        """ Returns a random string given the size and conditions.
        """

        choices = ""

        if alphabets_allowed:
            choices += string.ascii_uppercase
            if case_sensitive:
                choices += string.ascii_lowercase

        if numbers_allowed:
            choices += string.digits

        return prefix + ''.join(random.choice(choices) for _ in range(size))

    def check_preceding_request(key):
        if not cache.get(key):
            cache.set(key, DRIVER_REQUEST_IN_PROGRESS)
            return True
        return False

    def get_display_of_tuple(pair, index):

        pair_dict = dict(pair)
        display = pair_dict.get(index, None)

        if display:
            return display.title()

        return None

    def get_uom(uom):
        value = None
        if uom and uom != '':
            _uom = uom.lower()
            if _uom in ALLOWED_UOM_ALIAS:
                value = ALLOWED_UOM_ALIAS.get(_uom)

        return value

    def get_uom_list():
        return list(ALLOWED_UOM_ALIAS.keys())

    def prev_next_in_iterator(source):
        """
        Returns previous and next element along with current element
        :param source: list
        """
        prevs, items, nexts = tee(source, 3)
        prevs = chain([None], prevs)
        nexts = chain(islice(nexts, 1, None), [None])
        return zip(prevs, items, nexts)

    def extract_from_google_address_components(self, address, extract_components=[]):
        """
        Extracts google address components and returns merged string
        address: list or dict of google geocoded address
        extract_components: code of components to extract (list of str)
        """
        if isinstance(address, str):
            address = json.loads(address)

        if isinstance(address, list) and len(address) > 0:
            address = address[0]

        if not address:
            return ''

        address_components = address.get('address_components', {})
        extracted_components = [i.get('long_name', '') for i in address_components
            if [x for x in extract_components if x in i.get('types', [])]
        ]
        return ', '.join(extracted_components)
