import json
import six
from django.conf import settings

from rest_framework import renderers
from rest_framework.parsers import BaseParser
from rest_framework.exceptions import ParseError


class PreserveRawBodyJSONParser(BaseParser):
    """
        `body` from django request can't be accessed,
        once data stream has been read by DRF parser.
        This will add a new attribute `raw_body` to request and
        stores unprocessed data stream
    """
    media_type = 'application/json'
    renderer_class = renderers.JSONRenderer

    def parse(self, stream, media_type=None, parser_context=None):
        parser_context = parser_context or {}
        encoding = parser_context.get('encoding', settings.DEFAULT_CHARSET)
        request = parser_context.get('request')
        try:
            data = stream.read().decode(encoding)
            setattr(request, 'raw_body', data)
            return json.loads(data)
        except ValueError as exc:
            raise ParseError('JSON parse error - %s' % six.text_type(exc))
