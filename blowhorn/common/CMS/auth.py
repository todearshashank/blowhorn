import os

from django.conf import settings
import requests
import json

from django.utils import timezone
from rest_framework import status

from blowhorn.customer.submodels.partner_models import PartnerAccessKey
PARTNER_NAME = 'BLOWHORN_CMS'

headers = {'content-type': 'application/json'}


class Auth(object):

    def login(self):
        payload = {
            "identifier": settings.CMS_IDENTIFIER,
            "password": settings.CMS_PASSWORD
        }
        response = requests.post(settings.CMS_LOGIN_URL, json=payload, headers=headers)
        data = json.loads(response.content)
        if response.status_code == status.HTTP_200_OK:
            jwt = data['jwt']
            return jwt

    def get_header(self):
        try:
            token = 'Bearer ' + self.login()
            PartnerAccessKey.objects.filter(partner__name=PARTNER_NAME).update(access_token=token, refresh_token=token,
                                                                               response_time=timezone.now().timestamp())
        except:
            partner_key = PartnerAccessKey.objects.get(partner__name=PARTNER_NAME)
            token = partner_key.access_token
        return token

    def _get_latlng_list(self, coordinates):
        latlon_list = []
        for latlong_arr in coordinates:
            for latlong in latlong_arr:
                latlon_list.append({
                    "latitude": latlong[1],
                    "longitude": latlong[0],
                })
        return latlon_list

    def _get_coverage_area(self, city):
        coverage = []
        pickup_coordinates = city.coverage_pickup.coords
        dropoff_coordinates = city.coverage_dropoff.coords
        coverage.append({
            "city": city.name,
            "city_id": city.id,
            "pickup": self._get_latlng_list(pickup_coordinates),
            "dropoff": self._get_latlng_list(dropoff_coordinates),
        })
        return coverage

    def upsert_city(self, cities):
        headers.update({'Authorization': self.get_header()})
        for city in cities:
            response = requests.get(settings.CMS_CITY_URL + '?city_id=' + str(city.id), headers=headers)
            response_data = json.loads(response.content)
            if response.status_code == status.HTTP_200_OK:
                payload = {
                    "name": city.name,
                    "start": str(city.start_time),
                    "end": str(city.end_time),
                    "night_start": str(city.night_hours_start_time),
                    "night_end": str(city.night_hours_end_time),
                    "future_booking_days": city.future_booking_days,
                    "coverage": self._get_coverage_area(city)

                }
                if response_data:
                    cms_id = response_data[0].get('id', None)
                    if cms_id:
                        response = requests.put(settings.CMS_CITY_URL + str(cms_id), data=json.dumps(payload),
                                                headers=headers)
                        response_data = json.loads(response.content)

                if not response_data:
                    payload.update({'city_id': city.id,
                                    "address": str(city)})
                    response = requests.post(settings.CMS_CITY_URL, data=json.dumps(payload), headers=headers)
                    response_data = json.loads(response.content)
                if response.status_code == status.HTTP_200_OK:
                    print("city updated", response_data)
                else:
                    print("city update failed ", str(city), response_data)

    def upsert_vehicle_class(self, vehicle_classes):
        headers.update({'Authorization': self.get_header()})
        for vehicle_class in vehicle_classes:
            response = requests.get(settings.CMS_VEHICLE_CLASS_URL + '?vehicle_class_id=' + str(vehicle_class.id),
                                    headers=headers)
            response_data = json.loads(response.content)
            if response.status_code == status.HTTP_200_OK:
                payload = {
                    "commercial_class": vehicle_class.commercial_classification,
                    "description": vehicle_class.description,
                    "extra_info": vehicle_class.carrying_capacity
                }
                if response_data:
                    cms_id = response_data[0].get('id', None)
                    if cms_id:
                        response = requests.put(settings.CMS_VEHICLE_CLASS_URL + str(cms_id), data=json.dumps(payload),
                                                headers=headers)
                        response_data = json.loads(response.content)

                if not response_data:
                    payload.update({"vehicle_class_id": vehicle_class.id})
                    response = requests.post(settings.CMS_VEHICLE_CLASS_URL, data=json.dumps(payload),
                                             headers=headers)
                    response_data = json.loads(response.content)
                if response.status_code == status.HTTP_200_OK:
                    print("city updated", response_data)
                else:
                    print("city update failed ", str(vehicle_class), response_data)

    def upsert_rate_card(self, cities, data):
        headers.update({'Authorization': self.get_header()})
        for city in cities:
            response = requests.get(settings.CMS_RATE_CARD_URL + '?city_id=' + str(city.id), headers=headers)
            response_data = json.loads(response.content)
            if response.status_code == status.HTTP_200_OK:
                payload = {
                    "ratecard": data

                }
                if response_data:
                    cms_id = response_data[0].get('id', None)
                    if cms_id:
                        response = requests.put(settings.CMS_RATE_CARD_URL + str(cms_id), data=json.dumps(payload),
                                                headers=headers)
                        response_data = json.loads(response.content)

                if not response_data:
                    payload.update({'city_id': city.id})
                    response = requests.post(settings.CMS_RATE_CARD_URL, data=json.dumps(payload), headers=headers)
                    response_data = json.loads(response.content)
                if response.status_code == status.HTTP_200_OK:
                    print("rate rate updated", response_data)
                else:
                    print("rate rate update failed ", str(city), response_data)



