import os
import base64
import redis
import json
import ast
from bson import json_util

from django.conf import settings

from blowhorn.common.extra_ds import DotDict
from blowhorn.common.redis_helper import StrictRedisHelper


REDIS_HOST = getattr(settings, 'REDIS_HOST', 'localhost')


class RedisObject(object):
    """A base object backed up by redis.
    Generally, use RedisDict or RedisList rather than using this directly."""

    def __init__(self, id=None):
        """Create or load a RedisObject."""

        self.redis = StrictRedisHelper().get_connection()

        if id:
            self.id = id
        else:
            self.id = base64.urlsafe_b64encode(os.urandom(9)).decode('utf-8')

        if ':' not in self.id:
            self.id = self.__class__.__name__ + ':' + self.id

    def __bool__(self):
        """Check if an object currently exists.
        Returns `True` on success, otherwise `False`."""

        return self.redis.exists(self.id)

    def __eq__(self, other):
        """Tests if two redis objects are equal (they have the same key)"""

        return self.id == other.id

    def __str__(self):
        """Return string representation of this object."""

        return self.id

    def delete(self):
        """Delete this object from redis"""

        self.redis.delete(self.id)

    @staticmethod
    def decode_value(type, value):
        """Decode a value if it is non-None, otherwise, decode with no
        arguments."""

        if type == json:
            return DotDict(json.loads(value)) if value else DotDict({})
        elif type == dict:
            return ast.literal_eval(value) if value else {}
        return type(value) if value else type()

    @staticmethod
    def encode_value(value):
        """Encode a value using json.dumps, with default = str"""

        if type(value) == dict:
            value = json.dumps(value, default=json_util.default)
        return str(value)
