from django.conf import settings
from blowhorn.driver.constants import DRIVER_APP_MESSAGE_KEY

template_truck_arrived_at_dropoff = {
    'text': 'Your vehicle has arrived at the drop off point - #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853250136460',
    'template_type': 'TXN'
}

template_driver_acceptance = {
    'text': "Your driver for booking #%s is %s contact number %s. Vehicle: %s, %s. Track at %s - #{}".format(
        settings.BRAND_NAME),
    'template_id': '1107161188292918154',
    'template_type': 'TXN'
}

template_booking_acceptance = {
    'text': "Your booking %s has been accepted. Track at %s -#{}".format(settings.BRAND_NAME),
    'template_id': '1107161853835662363',
    'template_type': 'TXN'
}

template_otp = {
    'text': '<#> Thanks for registering with {}. Your one time password(OTP) for account verification is: %s. {}'.format(
        settings.BRAND_NAME, DRIVER_APP_MESSAGE_KEY),
    'template_id': '1107161190389291636',
    'template_type': 'OTP'
}

android_template_otp = {
    'text': '<#> Thanks for registering with {}. Your one time password(OTP) for account verification is: %s. {}'.format(
        settings.BRAND_NAME, settings.ANDROID_TEMPLATE_OTP_KEY),
    'template_id': '1107161190389291636',
    'template_type': 'OTP'
}

driver_vendor_login_otp = {
    'text': 'Dear %s, %s is your one time password(OTP) for your {} account. {}'.format(settings.BRAND_NAME,
                                                                                        DRIVER_APP_MESSAGE_KEY),
    'template_id': '1107164033170599037',
    'template_type': 'OTP'
}

password_reset_otp = {
    'text': '<#> One time password(OTP) to reset your account password is %s. {}'.format(
        DRIVER_APP_MESSAGE_KEY),
    'template_id': '1107160147917875273',
    'template_type': 'OTP'
}

template_item_delivered = {
    'text': 'Your %s has been delivered! #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853839993047',
    'template_type': 'TXN'
}

template_booking_completion = {
    'text': 'Your booking #%s is complete. The bill is {} %s. Pay online at %s -#{}'.format(
        settings.CURRENCY_WITHOUT_SYMBOL.get(settings.OSCAR_DEFAULT_CURRENCY, 'INR'), settings.BRAND_NAME),
    'template_id': '1107161853852860476',
    'template_type': 'TXN'
}

# temporary for mz
template_order_completion = {
    'text': 'Your %s order %s is complete. View and pay invoice at %s',
    'template_id': '',
    'template_type': 'TXN'
}

prepaid_template_booking_completion = {
    'text': 'Your booking #%s is complete. You can download the invoice at %s - #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853859615562',
    'template_type': 'TXN'
}

template_driver_arriving = {
    'text': 'Your {} vehicle is arriving now'.format(settings.BRAND_NAME),
    'template_id': '1107161182836875540',
    'template_type': 'TXN'
}

paytm_debit_failure = {
    'text': 'Payment Failed via PayTm for booking #%s because %s, The bill is INR %s. Pay online at %s #{}'.format(
        settings.BRAND_NAME),
    'template_id': '1107161183141632719',
    'template_type': 'TXN'
}

paytm_debit_bulk_failure = {
    'text': 'Payment Failed via PayTm for bookings %s because %s, The bill is INR %s - #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853259267671',
    'template_type': 'TXN'
}

paytm_debit_success = {
    'text': 'Payment of amount INR %s successful via PayTm for booking #%s. - #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853867397654',
    'template_type': 'TXN'
}

template_booking_cancelled = {
    'text': 'Your booking %s scheduled at %s on %s has been cancelled. - #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853268889118',
    'template_type': 'TXN'
}

template_driver_active = {
    'text': 'Awesome! We are all set. Now you can accept trips and earn money. Welcome to {}'.format(
        settings.BRAND_NAME),
    'template_id': '1107161188230599829',
    'template_type': 'TXN'
}

template_user_password = {
    'text': 'Thanks for registering with {}. Please use pin %s for login. #{}'.format(settings.BRAND_NAME,
                                                                                      settings.BRAND_NAME),
    'template_id': '1107161188258820963',
    'template_type': 'TXN'
}

template_vendor_registration = {
    'text': 'Thanks for registering with {}. Our team will contact you soon for further process. In case of any queries, please reachout to %s at %s. #{}'.format(
        settings.BRAND_NAME, settings.BRAND_NAME),
    'template_id': '1107161188270173593',
    'template_type': 'TXN'
}

payment_due = {
    'text': 'Hi %s! Your trip on %s of Rs %d is due for payment. Pay here! %s. - #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853875903891',
    'template_type': 'TXN'
}

payment_due_multiple = {
    'text': 'Hi %s! You have a past due of Rs %d for %d bookings. Pay here! %s. #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161853884099550',
    'template_type': 'TXN'
}

covid_19_declaration_customer = {
    'text': "For your booking %s, download the covid-19 essential commodities declaration from https://go.aws/2UoTo1H - #Blowhorn (Stay indoors, help fight COVID-19!)",
    'template_id': '1107161189163652707',
    'template_type': 'TXN'
}

user_login_otp = {
    'text': 'Dear %s, Please use the pin %s for login. #{}'.format(settings.BRAND_NAME),
    'template_id': '1107161190131463675',
    'template_type': 'OTP'
}

wallet_credit = {
    'text': 'Dear %s, INR %s added successfully to {} wallet via %s. Transaction id: %s. #{}'.format(
        settings.BRAND_NAME, settings.BRAND_NAME),
    'template_id': '1107161183665785413',
    'template_type': 'TXN'
}

wallet_debit = {
    'text': 'Paid %s to order %s on %s. Updated Balance: %s. For more details, visit %s. #{}'.format(
        settings.BRAND_NAME, settings.BRAND_NAME),
    'template_id': '1107161183685583035',
    'template_type': 'TXN'
}

cosmos_assign_driver_sms = {
    'text': "Dear Customer, your request for vehicle booking has been successfully placed, and driver has been assigned. The driver will reach your location shortly. "
            "Regards, " \
            "Team {}.".format(settings.BRAND_NAME),
    'template_id': '1107161188136516678',
    'template_type': 'TXN'
}

delivery_otp_verification = {
    'text': "Please provide the OTP: %d to the delivery associate #{}".format(settings.BRAND_NAME),
    'template_id': '1107161853918144673',
    'template_type': 'OTP'
}

trip_rejected = {
    'text': "Driver %s rejected Trip %s #{}".format(settings.BRAND_NAME),
    'template_id': '1107161853923382801',
    'template_type': 'TXN'
}

trip_rejected_hub = {
    'text': "Driver %s rejected Trip %s for hub %s #{}".format(settings.BRAND_NAME),
    'template_id': '1107161853931122805',
    'template_type': 'TXN'
}

trip_cancelled = {
    'text': "Driver %s cancelled Trip %s #{}".format(settings.BRAND_NAME),
    'template_id': '1107161853937517245',
    'template_type': 'TXN'
}

trip_cancelled_hub = {
    'text': "Driver %s cancelled Trip %s for hub %s #{}".format(settings.BRAND_NAME),
    'template_id': '1107161853941991251',
    'template_type': 'TXN'
}

SHARE_APP_MESSAGE_DRIVER_APP = {
    'text': "Hello Partners! Request you to download our all-new driver app: https://blowhorn.com/api/drivers/app. -{}".format(settings.BRAND_NAME),
    'template_id': '1107163065414217395',
    'template_type': 'TXN'
}

APP_DOWNLOAD_LINK_TEMPLATE = {
    'text': 'Download the Blowhorn App to book your truck for hire now from https://blowhorn.com/app - #{}'.format(
        'blowhornist'),
    'template_id': '1107160602159449559',
    'template_type': 'TXN'
}

CUSTOMER_DELIVERY_OTP_TEMPLATE = {
    'text': "Dear Customer, Your %s home delivery order" \
            " %s is out for delivery & expected to be" \
            " delivered today before %s. Please contact delivery" \
            " person %s. Share the OTP: %s to the delivery person. - #{}.".format(settings.BRAND_NAME),
    'template_id': '1107161853293672149',
    'template_type': 'OTP'
}

END_TRIP_OTP_TEMPLATE = {
    'text': "Please share the OTP: %s to %s, contact - %s. #{}.".format(settings.BRAND_NAME),
    'template_id': '1107161183467294302',
    'template_type': 'OTP'
}

END_TRIP_ASSET_STORE_TEMPLATE = {
    'text': "Please share the OTP: %s to %s, contact - %s. #{}.".format(settings.BRAND_NAME),
    'template_id': '1107161944143758580',
    'template_type': 'OTP'
}

COLLECT_ASSET_TEMPLATE = {
    'text': "Dear %s, Verify the cash on delivery amount %s.",
    'template_id': '1107161944143758580',
    'template_type': 'TXN'
}
