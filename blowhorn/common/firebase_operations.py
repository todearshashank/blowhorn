import json
import logging
from .firebase import firebase
from django.conf import settings

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def firebase_add_update_database(data, url_id):
    """ Function to add data to Firebase at given url """
    obj = firebase.FirebaseApplication(settings.FIREBASE_DATABASE_URL, None)
    authentication = firebase.FirebaseAuthentication(
        settings.FIREBASE_DATABASE_SECRET,
        settings.FIREBASE_ADMIN_AUTH_EMAIL,
        extra={'uid': settings.FIREBASE_ADMIN_AUTH_EMAIL_UID}
    )
    obj.authentication = authentication
    obj.put('/', url_id, data)


def firebase_delete_database_entry(url_id):
    """ Function to delete data in Firebase at given url """
    obj = firebase.FirebaseApplication(settings.FIREBASE_DATABASE_URL, None)
    authentication = firebase.FirebaseAuthentication(
        settings.FIREBASE_DATABASE_SECRET,
        settings.FIREBASE_ADMIN_AUTH_EMAIL,
        extra={'uid': settings.FIREBASE_ADMIN_AUTH_EMAIL_UID}
    )
    obj.authentication = authentication
    obj.delete('/', url_id)


def delete_active_driver(driver_id):
    """ Function to delete an active driver in Firebase"""
    url = '%s/%s' % (settings.FIREBASE_ACTIVE_DRIVERS_URL, driver_id)
    firebase_delete_database_entry(url)


def delete_active_drivers():
    """ Function to delete all active drivers in Firebase"""
    firebase_delete_database_entry(settings.FIREBASE_ACTIVE_DRIVERS_URL)


def broadcast_to_firebase(url, _dict):
    """
    :param url: url to push data in firebase realtime database
    :param _dict: data in JSON format
    """
    firebase_add_update_database(json.dumps(_dict), url)


class FirebaseService(object):

    def __init__(self):
        self.obj = self.__init_object()

    def __init_object(self):
        """
        Initializes firebase db object
        """
        fire_db = firebase.FirebaseApplication(settings.FIREBASE_DATABASE_URL, None)
        authentication = firebase.FirebaseAuthentication(
            settings.FIREBASE_DATABASE_SECRET,
            settings.FIREBASE_ADMIN_AUTH_EMAIL,
            extra={'uid': settings.FIREBASE_ADMIN_AUTH_EMAIL_UID}
        )
        fire_db.authentication = authentication
        return fire_db

    def retrieve_keys(self, channel):
        """
        Retrieves keys from given channel
        :param channel: name of firebase channel (str)
        """
        keys = []
        try:
            resp = self.obj.get(channel, None)
            keys = resp and resp.keys() or []
        except BaseException as be:
            logger.info('Failed to retrieve keys for %s. %s' % (channel, be))

        return keys

    def delete_dashboard_updates(self, customer_ids=None):
        """
        Deletes shipment orders in firebase
        :param customer_ids: keys of firebase (list)
        """
        if not customer_ids:
            customer_ids = self.retrieve_keys('/dashboard')

        for id in customer_ids:
            logger.info('Deleting shipment updates for customer %s' % id)
            try:
                self.obj.delete('/', '/dashboard/%s' % id)
            except BaseException as e:
                logger.info('Failed to delete shipment updates for customer %s. %s' % (id, e))
