from django.db import models
from audit_log.models.fields import CreatingUserField, LastUserField
from django.utils import timezone
from django import forms
from django.contrib.postgres.fields import ArrayField

class ChoiceArrayField(ArrayField):
    """
    A field that allows us to store an array of choices.

    Uses Django 1.9's postgres ArrayField
    and a MultipleChoiceField for its formfield.
    """

    def formfield(self, **kwargs):
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)


class BaseModel(models.Model):
    """
    Common Model to store created and modified objects
    """

    created_date = models.DateTimeField(auto_now_add=True)
    created_by = CreatingUserField(related_name="%(class)s_created_by")

    modified_date = models.DateTimeField(auto_now=True)
    modified_by = LastUserField(related_name="%(class)s_modified_by")

    def save(self, *args, **kwargs):
        """
        auto_now and auto_now_add inherit editable=False and don't
        display on admin, so override save to manage them
        """
        if not self.pk:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(BaseModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True
