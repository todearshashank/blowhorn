import razorpay
from django.conf import settings


def get_client(account):
    account = settings.RAZORPAY_ACCOUNTS.get(account, {})
    _id, _key = account.get('id', ''), account.get('secret', '')
    client = razorpay.Client(auth=(_id, _key))
    client.set_app_details({"title": "Blowhorn", "version": "1.0"})
    return client


def fetch(payment_id, account='default'):
    if account not in ['donation']:
        account = 'default'
    client = get_client(account)
    return client.payment.fetch(payment_id)


def capture(payment_id, amount, account='default'):
    if account not in ['donation']:
        account = 'default'
    client = get_client(account)
    try:
        return client.payment.capture(payment_id, amount)
    except BaseException as be:
        return {
            'already_captured': True
        }

def all(data, account='default'):
    if account not in ['donation']:
        account = 'default'
    client = get_client(account)
    try:
        return client.payment.all(data=data)
    except BaseException as be:
        return {}
