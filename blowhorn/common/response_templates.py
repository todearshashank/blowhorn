PASS = 'PASS'
FAIL = 'FAIL'
RESPONSE_TEMPLATE = {
    "status": "{status_code}",
    "message": "{message}"
}
