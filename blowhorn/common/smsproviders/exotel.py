# Settings for Exotel API
# Get your SID and token from http://my.exotel.in/<your-account>/settings/site#api-settings
import requests
from django.conf import settings
import json
from blowhorn.logapp.models import SmsLog
sid = 'blowhorn'
token = '9b8323476f10d676135e583c8372c7ee4bad775e'
url = 'https://twilix.exotel.in/v1/Accounts/%s/Sms/send.json' % sid


def send_message(sms_from, sms_to, message, template_dict, priority='', customer_id=None):

    parm = {
            'From': sms_from,
            'To': sms_to,
            'Body': message,
            'Priority': priority,
            'DltEntityId': settings.DLT_ENTITY_ID,
            'StatusCallback': settings.SMS_CALLBACK_URL
        }

    tid = template_dict.get('template_id', None)
    if tid:
        parm['DltTemplateId'] = tid

    sms_obj = None
    try:
        sms_obj = SmsLog.objects.create(message=message, template_id=tid, 
                    provider='exotel', to_number=sms_to, customer_id=customer_id)
    except Exception as e:
        pass

    response = requests.post(url.format(sid=sid, status_call_back=settings.SMS_CALLBACK_URL),
                             auth=(sid, token), data=parm)

    if response:
        sms_response = response.__dict__
        sms_content = json.loads(sms_response['_content'])
        ssid = sms_content.get('SMSMessage').get('Sid')
        detailed_status = sms_content.get('SMSMessage').get('DetailedStatus')
        sms_status = sms_content.get('SMSMessage').get('Status')
        sms_status = sms_status.title() if sms_status else sms_status

    else:
        detailed_status = 'SMS not delivered'
        sms_status = 'Failed'
        ssid = None

    if sms_obj:
        SmsLog.objects.filter(id=sms_obj.id).update(ssid=ssid, detailed_status=detailed_status,
                                                    sms_status=sms_status)

    return response
