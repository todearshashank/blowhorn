import requests
import re
import json
import logging

from rest_framework import status

from django.conf import settings
from blowhorn.logapp.models import SmsLog

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Settings for Kaleyra API
KALEYRA_URL = 'https://api.kaleyra.io/v1/%s/messages' % settings.KALYERA_API_SID
HEADERS = {'content-type': 'application/json', 'api-key' : settings.KALEYRA_API_KEY}


def send_message(sms_from, sms_to, message, template_dict, priority='', customer_id=None):
    is_valid = re.match('^\+91\d{10}$',str(sms_to))
    if not is_valid:
        is_no_valid = re.match('^\d{10}$',str(sms_to))
        if is_no_valid:
            sms_to = settings.ACTIVE_COUNTRY_CODE + str(sms_to)
        else:
            return

    _callback = {
            "url": settings.SMS_CALLBACK_URL,
            "method": settings.CALLBACK_METHOD,
            "header": {"api-key": settings.KALEYRA_HEADER_API_KEY}
    }

    if settings.CALLBACK_COUNT:
        _callback["retry"] = {
            "count": settings.CALLBACK_COUNT, 
            "interval": [settings.TIME_INTERVAL, settings.TIME_INTERVAL, settings.TIME_INTERVAL]
        }

    _data = {
        'sender' : 'BLWHRN',
        'to': sms_to,
        'body': message,
        'template_id' : template_dict['template_id'],
        'source' : "API",
        'type' : template_dict['template_type'],
        'callback': _callback
    }
    ssid = None
    sms_obj = None
    errors = {}
    try:
        sms_obj = SmsLog.objects.create(message=message, template_id=template_dict['template_id'],
                                        provider='kaleyra', to_number=sms_to, ssid=ssid,
                                        customer_id=customer_id)
    except Exception as e:
        logger.info('%s - %s' % ('smserr',e))

    response = requests.post(KALEYRA_URL, json=_data, headers=HEADERS)
    detailed_status = 'SMS was not sent'
    sms_status = 'Failed'
    data = json.loads(response.content)
    
    if response.status_code == status.HTTP_202_ACCEPTED:
        ssid = data.get('id', None)
        _errors = data.get('errors', {})

        if _errors:
            errors = json.dumps(_errors)
        else:
            detailed_status = 'SMS has been queued'
            sms_status = 'Queued'

    if sms_obj:
        sms_obj.ssid = ssid
        sms_obj.detailed_status = detailed_status
        sms_obj.errors = errors
        sms_obj.sms_status = sms_status
        sms_obj.save()

    return response
