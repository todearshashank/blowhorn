from django.conf import settings
import requests

# Settings for Twilio API

sid = settings.TWILIO_SID
token = settings.TWILIO_TOKEN
url = 'https://api.twilio.com/2010-04-01/Accounts/{sid}/Messages.json'

msg_from = '+17174960709'


def send_message(sms_from, sms_to, message, template_dict, priority='', customer_id=None):
    _data = {
        'From' :msg_from,
        'To': sms_to,
        'Body': message
    }
    _auth = (sid, token)
    final_url = url.format(sid=sid)

    return requests.post(final_url, data=_data, auth=_auth)
