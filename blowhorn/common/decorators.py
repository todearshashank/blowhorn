from urllib.parse import urlparse
from django.utils.decorators import available_attrs
from django.utils.functional import wraps
from django.utils import timezone
from django.contrib import messages
from django.shortcuts import resolve_url
from django.contrib.auth.views import redirect_to_login
from django.conf import settings

from blowhorn.common.celery_tasks import execute
from blowhorn.common.redis_helper import RedisHelper
from blowhorn.oscar.core.loading import get_model
from blowhorn.driver.models import DriverConstants
from django.contrib.auth import REDIRECT_FIELD_NAME
default_message = 'Invalid App code'


def redis_batch_lock(period=300, expire_on_completion=True):
    """
    Concurrency (and Sequence) control for any method (or task).
    :param period: Number of seconds for which lock should be imposed.
    :param expire_on_completion: Denotes whether completion of method (or task) should expire the lock.
    If set as `true`, it would over come period if task (or method) is completed prior to end of period.
    If set as `false`, would wait for end of period for lock to expire.
    """

    def wrapper(f):
        try:
            @wraps(f, assigned=available_attrs(f))
            def _wrapper(*args, **kwargs):
                if RedisHelper().add_or_get_lock(f.__name__, period):
                    try:
                        execute.delay(f.__module__, f.func_name, *args, **kwargs)
                    except Exception as e:
                        f(*args, **kwargs)
                    if expire_on_completion:
                        RedisHelper().expire_lock(f.__name__)
            _wrapper.func = f
            return _wrapper
        except Exception:
            return f
    return wrapper


def user_passes_test(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME, message=default_message):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if not test_func(args[0].META.get('HTTP_APP_CODE')):
                messages.add_message(request, messages.ERROR, message)
            if test_func(args[0].META.get('HTTP_APP_CODE')):
                return view_func(request, *args, **kwargs)
            path = args[0].build_absolute_uri()
            resolved_login_url = resolve_url(login_url or settings.LOGIN_URL)
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
            current_scheme, current_netloc = urlparse(path)[:2]
            if ((not login_scheme or login_scheme == current_scheme) and
                    (not login_netloc or login_netloc == current_netloc)):
                path = args[0].get_full_path()
            return redirect_to_login(
                path, resolved_login_url, redirect_field_name)
        return _wrapped_view
    return decorator


def verify_app_code(view_func=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='/login',
                    message=default_message):
    """
    Decorator for verifying app code while login
    """
    actual_decorator = user_passes_test(
        lambda build_code: build_code and int(build_code) >= int(DriverConstants().get(
            'MINIMUM_BUILD_CODE_REQUIRED', 132)),
        login_url=login_url,
        redirect_field_name=redirect_field_name,
        message=message
    )

    if view_func:
        return actual_decorator(view_func)

    return actual_decorator

