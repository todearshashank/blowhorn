import json
import logging
import urllib.request
import urllib.parse
from django.utils import timezone
from django.conf import settings

from fcm_django.models import FCMDevice
from blowhorn.oscar.core.loading import get_model
from django.db.models import Q

from blowhorn.legacy.adapter import LegacyAdapter
from blowhorn.order.models import Labour

Order = get_model('order', 'Order')
OrderConstants = get_model('order', 'OrderConstants')
Trip = get_model('trip', 'Trip')
DriverVehicleMap = get_model('driver', 'DriverVehicleMap')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

USER_CLIENT_TYPE_IOS = 'ios'


class Notification:

    def push_to_fcm_device(user, data, title=None, body=None, api_key=None,
                           device_id=None):
        """
        Pushes notification to device of given user(s)
        :param data: json
        :param title: notification title
        :param body: notification body
        :param api_key: FCM server api key
        :param device_id: registered device-id as in FCMDevice in DB
        :return: Status of push notification
        """

        query_params = Q(user=user, active=True)
        if device_id:
            query_params &= ~Q(device_id=device_id)
        user_device = FCMDevice.objects.filter(query_params).last()

        if user_device is None:
            return False

        # If api_key is none, it will pick from settings
        message_status = user_device.send_message(
            title=title,
            body=body,
            data=data,
            api_key=api_key
        )
        print('FCM push status: ', message_status)
        return message_status

    @staticmethod
    def push_to_fcm_devices(title, body, data=None, api_key=None,
                            users=None, email_list=None, user_pks=None):
        """
        Pushes notification to device of given user(s)
        :param users: instances of User
        :param email_list: list of email ids
        :param user_pks: list of pk of User
        :param data: json
        :param title: notification title (appears in statusbar)
        :param body: notification body (appears in statusbar)
        :param api_key: FCM server api key
        :return: Status of push notification
        """

        if (not users and not email_list and not user_pks) or (
                users and email_list and user_pks):
            raise ValueError('Provide either `users` or `email_list` or '
                             '`user_pks`')

        query_params = Q(active=True)
        if users is not None:
            query_params &= Q(user__in=users)
        elif email_list is not None:
            query_params &= Q(user__email__in=email_list)
        elif user_pks is not None:
            query_params &= Q(user_id__in=user_pks)

        devices = FCMDevice.objects.filter(query_params).distinct('user_id')
        logger.info('FCM Devices: %s' % devices)
        if not devices.exists():
            return False

        message_status = devices.send_message(
            title=title or 'New notification',
            body=body or data.get('message', ''),
            data=data,
            api_key=api_key
        )
        print('FCM push status: ', message_status)
        return message_status

    def send_notification(order, driver, action='booking'):
        #
        data = Notification._modify_data_if_legacy_app(order, driver, action)
        if data:
            return Notification.push_to_fcm_device(driver.user, data)
        return False

    def send_push_notification_to_driver_app(driver, action):
        #
        data = {'blowhorn_notification': {"action": action}}
        return Notification.push_to_fcm_device(driver.user, data)

    def send_accept_reject_notification(order, driver, action='booking', dispatch_id=None):
        from blowhorn.apps.driver_app.v3.serializers.order import OrderDispatchSerializer
        response = {}
        response['data'] = OrderDispatchSerializer(instance=order).data
        response['action'] = action
        response['dispatch_id'] = dispatch_id
        response['dispatch_time'] = OrderConstants().get('DISPATCH_TIMER_SECONDS', 30)
        print("res :",response)

        return Notification.push_to_fcm_device(driver.user, {'blowhorn_notification': response})

    def get_mqtt_response(order, action='booking', dispatch_id=None):
        from blowhorn.apps.driver_app.v3.serializers.order import \
            OrderDispatchSerializer
        response = {}
        response['data'] = OrderDispatchSerializer(instance=order).data
        response['action'] = action
        response['dispatch_id'] = dispatch_id
        response['dispatch_time'] = OrderConstants().get('DISPATCH_TIMER_SECONDS', 30)

        return {'blowhorn_notification': json.dumps(response)}

    def send_approval_unapproval_notification(user, driver, message):
        notification_dict = {
            'id': driver.id,
            'driver': str(driver),
            'message': message,
            'reason_for_rejection': driver.bgv_unapproval_reason,
            'is_superuser': False
        }

        data = {
            'notification': notification_dict
        }

        return Notification.push_to_fcm_device(user, data)

    def send_driver_data_to_manager(user, driver, message):
        notification_dict = {
            'id': driver.id,
            'driver': str(driver),
            'message': message,
            'reason_for_rejection': driver.bgv_unapproval_reason,
            'is_superuser': False
        }

        data = {
            'notification': notification_dict
        }

        return Notification.push_to_fcm_device(user, data)

    def push_status_to_customer(user, data, api_key=None):
        """
        :param data: order data (json)
        :param api_key: FCM Server Key
        :return: message status (json)
        """
        title = None
        body = None
        response_data = None

        user_device = FCMDevice.objects.filter(
            user=user, active=True).first()

        if user_device is None:
            return False

        data['route_info'] = []

        if user_device.type == USER_CLIENT_TYPE_IOS:
            title = 'Booking Update'
            body = data.get('user_message')
        else:
            response_data = {
                'v7': data
            }

        # If api_key is none, it will pick from settings
        message_status = user_device.send_message(
            title=title,
            body=body,
            data=response_data,
            api_key=api_key
        )
        print('FCM Notification status for customer device: ', message_status)
        return message_status

    def send_action_to_driver(user, data, device_id=None):
        # instance = NotificationReadStatus.objects.create(**dictionary)
        # data['id'] = instance.id
        json_data = {
            "value_json": data,
            "blowhorn_notification": data
        }
        return Notification.push_to_fcm_device(user, json_data, device_id=device_id)

    def _modify_data_if_legacy_app(order, driver, action):
        # Spoofing the inputs, ideally should be fetched from user device and
        # role.
        is_legacy_application = True
        if is_legacy_application:
            return LegacyAdapter().get_booking_for_driver(order, driver, action)
        return None

    def _modify_data_if_legacy_app_on_order_completion(order):
        driver = order.driver
        user = order.driver
        driver_vehicle_map = DriverVehicleMap.objects.get(driver=driver)
        vehicle = driver_vehicle_map.vehicle
        vehicle_model = vehicle.vehicle_model

        labour = Labour.objects.filter(order_id=order.id)
        labour = labour[0] if labour else labour

        waypoint = order.waypoint_set.first()
        stop = waypoint.stops.first()
        trip = stop.trip

        trip_time_in_minutes = (
            (timezone.now() - trip.actual_start_time).total_seconds()) / 60

        data = None
        if trip:
            data = {
                'value_json': {
                    "status": "completed",
                    "dropoff_area": order.shipping_address.search_text,
                    "display_address": order.shipping_address.search_text,
                    "labour_cat": "",
                    "type": "booking",
                    "dropoff_address": order.shipping_address.search_text,
                    "items": order.items,
                    "pickup_time": order.pickup_datetime.strftime('%H:%M %p'),
                    "driver": {
                        "name": driver.user.name,
                        "vehicle_number": vehicle.registration_certificate_number,
                        "rating": float(driver.driver_rating.cumulative_rating) if driver.driver_rating else 0,
                        "number": driver.user.phone_number.as_national,
                        "vehicle_model": vehicle_model.model_name
                    },
                    "trip_cost": float(order.total_incl_tax),
                    "advance_payment_amount": None,
                    "booking_id": order.number,
                    "booking_key": order.number,
                    "pickup_address": order.pickup_address.search_text,
                    "trip_time": "%s Hours %0.2f Mins" % (int(trip_time_in_minutes / 60), trip_time_in_minutes % 60),
                    "labour_count": 0,
                    "action": "status_update",
                    "message": "Your {} trip is complete!".format(settings.BRAND_NAME),
                    "pickup_area": order.pickup_address.search_text,
                    "pickup_date": order.pickup_datetime.strftime('%d-%b-%Y'),
                    "labour_cph": float(labour.labour_cost if labour else 0),
                }
            }
        return data

    def add_fcm_key_for_user(user, registration_id, device_id, device_type):
        # Add Registration Key for a USER and Device combination in FCM.
        name = '%s-%s' % (user.email, device_type)

        # @todo: Making the OLD registration ids inactive
        # FCMDevice.objects.filter(user=user, device_type=device_type).update(active=False)

        # Creating a new token
        fcm_device = FCMDevice(
            name=name,
            user=user,
            registration_id=registration_id,
            type=device_type,
            device_id=device_id,
        )
        if fcm_device.save():
            return True
        return False

    def update_fcm_key_for_user(user_device, registration_id, device_id, device_type):
        user_device.registration_id = registration_id
        user_device.device_id = device_id
        user_device.device_type = device_type

        user_device.save()

    def remove_fcm_key_for_user(user, registration_id=None, device_type=None):
        # Remove entry for a USER and registration_id combination in FCM, when
        # user logouts.
        user_fcms = FCMDevice.objects.filter(user=user)
        if registration_id is not None:
            return user_fcms.filter(registration_id=registration_id).delete()
        elif device_type is not None:
            return user_fcms.filter(type=device_type).delete()

    def driver_app_action(driver, action):
        data = {"action": action}
        return Notification.push_to_fcm_device(driver.user, data)
