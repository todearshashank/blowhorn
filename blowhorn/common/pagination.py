from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import connection

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from collections import OrderedDict
from blowhorn.order.utils import OrderUtil


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 200

class ShipmentSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 200


class TripPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 200

    def get_paginated_response(self, data):

        order_results_json = []
        for order in data:
            response_dict = OrderUtil().get_booking_details_for_customer(
                request=self.request,
                order=order
            )
            if response_dict:
                order_results_json.append(
                    response_dict
                )
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', order_results_json)
        ]))


class LargeTablePaginator(Paginator):
    """
    Warning: Postgresql only hack

    Overrides the count method of QuerySet objects to get an
    estimate instead of actual count when not filtered.
    However, this estimate can be stale and hence not fit for
    situations where the count of objects actually matter.
    """

    def _get_count(self):
        if getattr(self, '_count', None) is not None:
            return self._count

        query = self.object_list.query
        if not query.where:
            try:
                cursor = connection.cursor()
                cursor.execute(
                    "SELECT reltuples FROM pg_class WHERE relname = %s",
                    [query.model._meta.db_table])
                self._count = int(cursor.fetchone()[0])
            except:
                self._count = super(LargeTablePaginator, self).count
        else:
            self._count = super(LargeTablePaginator, self).count

        return self._count

    count = property(_get_count)


class PaginationMixin(object):
    records_per_page = 1000

    def get_pagination_data(self, queryset, page):
        paginator = Paginator(queryset, self.records_per_page)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        previous_page = None if not objects.has_previous() \
            else objects.previous_page_number()
        next_page = 0 if not objects.has_next() else objects.next_page_number()

        return {
            "count": paginator.count,
            "previous": previous_page,
            "next": next_page,
            "results": objects.object_list,
        }
