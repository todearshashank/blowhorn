from django.conf import settings
from .smsproviders import *
from django.core.mail import send_mail
import logging

class Communication:
    def send_sms(sms_to, message, priority, template_dict, provider=None, city=None, customer_id=None):
        if not provider:
            provider = settings.DEFAULT_SMS_PROVIDER

        provider = Communication._get_provider(provider)
        sms_from = None
        provider.send_message(sms_from=sms_from, sms_to=sms_to,
                              message=message, template_dict=template_dict,
                              priority=priority, customer_id=customer_id)

    def send_email(to, sender, subject, message):
        logging.info('Email: to=%s, subject=%s' % (to, subject))
        send_mail(subject, message, sender, [to], fail_silently=False,)

    def _get_provider(provider):
        try:
            provider = eval(provider)
        except:
            provider = None

        return provider
