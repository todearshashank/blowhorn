import re
import pytz
import os
import json
import xlsxwriter
import calendar
from calendar import monthrange
from datetime import datetime, timedelta
from enum import Enum
import pdfkit
import tempfile

from django.shortcuts import render
from django.conf import settings
from django.template.loader import render_to_string
from django.db.models import Sum, FloatField

from blowhorn.utils.functions import utc_to_ist, minutes_to_hmstr, \
    get_in_ist_timezone, minutes_to_hh_mm


class ChoiceEnum(Enum):

    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


class AbsoluteSum(Sum):
    name = 'AbsoluteSum'
    template = '%(function)s(%(absolute)s(%(expressions)s))'

    def __init__(self, expression, **extra):
        super(AbsoluteSum, self).__init__(
            expression, absolute='ABS ', output_field=FloatField(), **extra)

    def __repr__(self):
        return "SUM(ABS(%s))".format(
            self.arg_joiner.join(str(arg) for arg in self.source_expressions)
        )


def get_repr(value):
    if callable(value):
        return '%s' % value()
    return value


def get_field(instance, field):
    field_path = field.split('.')
    attr = instance
    for elem in field_path:
        try:
            attr = getattr(attr, elem)
        except AttributeError:
            return None
        except ValueError:
            return None
    return attr


class TimeUtils(object):
    """ Class defines any Date/ Time related methods.
    """

    @staticmethod
    def get_start_end(date_time, frequency_days):
        """ Returns the start_date and end_date of given datetime and frequency of days (7, 30, 15)
        """

        if frequency_days == 7:
            start = date_time.replace(
                hour=0, minute=0, second=0, microsecond=0) - timedelta(days=date_time.weekday())
            end = start + timedelta(days=6)
            return start, end

        elif frequency_days == 30:
            start = date_time.replace(
                day=1, hour=0, minute=0, second=0, microsecond=0)
            end = (start + timedelta(days=monthrange(date_time.year,
                                                     start.month)[1])) - timedelta(days=1)
            return start, end

        elif frequency_days == 15:
            current_week_number = date_time.date().isocalendar()[1]
            if current_week_number % 2 == 0:
                start = date_time.replace(hour=0, minute=0, second=0, microsecond=0) - \
                    timedelta(days=date_time.weekday() + 7)
            else:
                start = date_time.replace(hour=0, minute=0, second=0, microsecond=0) - \
                    timedelta(days=date_time.weekday())
            end = start + timedelta(days=13)
            return start, end

        return None, None

    @staticmethod
    def _next_weekday(w_date, weekday):
        """ Find the next weekday after a given date.
        """

        days_ahead = weekday - w_date.weekday()

        if days_ahead <= 0:
            days_ahead += 7

        return w_date + timedelta(days_ahead)

    @staticmethod
    def get_falling_date_range(p_date, cycle, return_type=None):
        """ Get Date range for which date is falling in given payment cycle.
        """

        start_date, end_date = None, None

        if type(p_date) == str:
            p_date = datetime.strptime(p_date, "%Y-%m-%d").date()

        if cycle == "WEEKLY":
            if p_date.weekday() == 6:
                start_date, end_date = p_date - timedelta(days=6), p_date
            else:
                next_sunday = TimeUtils._next_weekday(p_date, 6)
                start_date, end_date = next_sunday - \
                    timedelta(days=6), next_sunday

        elif cycle == "MONTHLY":
            start_day, end_day = calendar.monthrange(p_date.year, p_date.month)
            start_date, end_date = p_date.replace(
                day=1), p_date.replace(day=end_day)

        elif cycle == "FORTNIGHTLY":
            if p_date.day <= 15:
                start_date, end_date = p_date.replace(
                    day=1), p_date.replace(day=15)
            else:
                start_day, end_day = calendar.monthrange(
                    p_date.year, p_date.month)
                start_date, end_date = p_date.replace(
                    day=16), p_date.replace(day=end_day)

        elif cycle == "DAILY":
            start_date, end_date = p_date, p_date

        if return_type:
            return return_type(start_date), return_type(end_date)

        return start_date, end_date

    @staticmethod
    def get_all_cyclic_dates(date_start, date_end, cycle):
        """ Returns the date ranges between the start date and end date as per the payment cycles.
        """

        assert date_end > date_start, "`date_end` should be ahead of `date_start`."
        assert cycle in ["WEEKLY", "MONTHLY", "FORTNIGHTLY",
                         "DAILY"], "Unexpected value for cycle given."

        ranges = []

        if type(date_start) == str:
            date_start = datetime.strptime(date_start, "%Y-%m-%d").date()

        if type(date_end) == str:
            date_end = datetime.strptime(date_end, "%Y-%m-%d").date()

        while True:

            start, end = TimeUtils.get_falling_date_range(date_start, cycle)

            if date_end < end and start > date_end:
                break

            if (start, end) not in ranges:
                ranges.append((start, end))

            if cycle == "WEEKLY":
                date_start += timedelta(days=7)
            elif cycle == "MONTHLY":
                if date_start.month == 12:
                    date_start = date_start.replace(
                        day=1, month=1, year=date_start.year + 1)
                else:
                    date_start = date_start.replace(
                        day=1, month=date_start.month + 1)
            elif cycle == "FORTNIGHTLY":
                date_start = end + timedelta(days=1)
            elif cycle == "DAILY":
                date_start = end + timedelta(days=1)

        return ranges

    @staticmethod
    def date_str_to_datetime(date_input):
        """ Converts given date string (format: "%Y-%m-%d") to datetime object
        """

        if type(date_input) == str:
            date_input = datetime.strptime(date_input, "%Y-%m-%d")
        return date_input

    @staticmethod
    def date_to_datetime_range(date_input):
        """ Returns the date time range for a given date (accepts str/ date).
        """

        if type(date_input) == str:
            date_input = TimeUtils.date_str_to_datetime(date_input).date()

        date_start = pytz.timezone(settings.ACT_TIME_ZONE).localize(
            datetime.combine(date_input, datetime.min.time()))
        date_end = pytz.timezone(settings.ACT_TIME_ZONE).localize(
            datetime.combine(date_input, datetime.max.time()))

        return date_start, date_end


def camel_case_to_snake_case(name):
    """
    Returns the snake case representation of given camel case string.
    """

    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name.replace(" ", ""))
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def generate_pdf(template, context, **kwargs):
    """
    Generates pdf (in bytes) corresponding to given html template and context.
    """

    header, footer = kwargs.pop("header", None), kwargs.pop("footer", None)

    options = {'--load-error-handling': 'skip', }

    if header:
        with tempfile.NamedTemporaryFile(suffix='.html', delete=False) as header_html:
            options['header-html'] = header_html.name
            header_html.write(render_to_string(header.get("template", ""), {
                              'data': header.get("context", {})}).encode('utf-8'))
    if footer:
        with tempfile.NamedTemporaryFile(suffix='.html', delete=False) as footer_html:
            options['footer-html'] = footer_html.name
            footer_html.write(render_to_string(footer.get("template", ""), {
                              'data': footer.get("context", {})}).encode('utf-8'))

    html = render(None, template, {'data': context})

    file = pdfkit.from_string(html.getvalue().decode(
        "utf-8"), False, options=options)

    return file


def export_to_spreadsheet(data, filename, data_mapping_constant, only_format=False):
    """
    Generates the spreadsheet with the corresponding data
    """

    file_address = filename

    workbook = xlsxwriter.Workbook(file_address)
    worksheet = workbook.add_worksheet()

    style_heading = workbook.add_format(
        {'font_name': 'Times New Roman', 'font_color': 'red', 'bold': True})
    style_data = workbook.add_format(
        {'font_name': 'Times New Roman', 'font_color': 'black', 'bold': False})

    start = 0
    worksheet.write(0, start, 'Sl No.', style_heading)

    for column_count, heading in enumerate(data_mapping_constant,start):
        worksheet.write(0, column_count+1, heading['name'], style_heading)

    if not only_format:
        for row_count, result in enumerate(data):
            worksheet.write(row_count + 1, 0, row_count+1, style_data)
            for column_count, heading in enumerate(data_mapping_constant):
                try:
                    if result.get(heading['label'], ''):
                        if heading['type'] == 'string':
                            result[heading['label']] = str(
                                result[heading['label']])
                        elif heading['type'] == 'distance':
                            result[heading['label']] = "{0:.2f}".format(
                                result[heading['label']]) + ' kms'
                        elif heading['type'] == 'time':
                            result[heading['label']] = minutes_to_hmstr(
                                result[heading['label']])
                        elif heading['type'] == 'time_hh_mm':
                            result[heading['label']] = minutes_to_hh_mm(
                                result[heading['label']])
                        elif heading['type'] == 'datetime':
                            result[heading['label']] = get_in_ist_timezone(
                                result[heading['label']]).strftime("%d-%m-%Y %I:%M %p")
                        elif heading['type'] == 'date':
                            result[heading['label']] = get_in_ist_timezone(
                                result[heading['label']]).strftime("%d-%m-%Y")
                        elif heading['type'] == 'object':
                            values = ''
                            for field in heading['fields']:
                                values = values + ', ' + \
                                    result[heading['label']][field] if values else result[
                                        heading['label']][field]
                            result[heading['label']] = values
                    else:
                        result[heading['label']] = ''
                    worksheet.write(row_count + 1, column_count+1,
                                    str(result.get(heading['label'], '')), style_data)

                except AttributeError:
                    worksheet.write(row_count + 1, column_count+1,
                                    result[heading['label']], style_data)

    worksheet.set_default_row(20)
    workbook.close()
    return file_address

def export_lists_to_spreadsheet(data, filename, headers):
    """
    Generates the spreadsheet with the corresponding data
    """

    file_address = filename

    workbook = xlsxwriter.Workbook(file_address)
    worksheet = workbook.add_worksheet()

    style_heading = workbook.add_format(
        {'font_name': 'Times New Roman', 'font_color': 'red', 'bold': True})
    style_data = workbook.add_format(
        {'font_name': 'Times New Roman', 'font_color': 'black', 'bold': False})
    row_iteration = 0
    style = style_data
    for count, section in enumerate(data):
        for row_count, record in enumerate(section):
            if row_count == 0 and headers[count]:
                style = style_heading
            else:
                style = style_data
            worksheet.write_row(row_count + row_iteration, 0, record, style)
        worksheet.set_row(row_count + 1, None, None)
        worksheet.set_row(row_count + 2, None, None)
        row_iteration = row_count + 3

    worksheet.set_default_row(20)

    workbook.close()
    return file_address
