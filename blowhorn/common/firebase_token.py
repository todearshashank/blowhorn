from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status
from django.conf import settings
from .helper import CommonHelper
import Crypto.PublicKey.RSA as RSA


SERVICE_ACCOUNT_EMAIL = settings.SERVICE_ACCOUNT_EMAIL


class FirebaseToken(APIView):
    permission_classes = (AllowAny, )

    def get(self, request, token_id=None):
        if not token_id:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        token = CommonHelper.generate_firebase_token(settings.FIREBASE_ADMIN_USER_UID)
        data = {
            'firebase_auth_token': token
        }
        return Response(status=status.HTTP_200_OK, data=data)
