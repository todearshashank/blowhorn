import pytz
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.conf import settings
from django.utils import timezone

from rest_framework import serializers


class BaseSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        fields = kwargs.pop('fields', [])
        remove_fields = kwargs.pop('remove_fields', [])
        self.alt_name_map = kwargs.pop('alt_name_map', {})

        super(BaseSerializer, self).__init__(*args, **kwargs)

        ## Dynamically select (or remove) fields from serializer
        if fields:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)

        if remove_fields:
            # Drop any fields that are specified in the `remove_fields` argument.
            for field_name in remove_fields:
                self.fields.pop(field_name)

    @staticmethod
    def combine(in_place, *serializers):
        """
        Method combines data from different serializers.
        if in_place is true, then all data is combined in one single object
        else, they are key marked with keys as names given as the second argument
        example: true, serializer_a, serializer_b) will combine all fields of both of them in one single object.
        example: false, [serializer_a, 'A'], [serializer_b, 'B'] will give {'A': {}, and 'B': {}}
        """

        final_serialized_data = {}
        if in_place:
            for serializer in serializers:
                final_serialized_data.update(serializer.data)
        else:
            for (serializer, name) in serializers:
                final_serialized_data[name] = serializer.data

        return final_serialized_data

    def to_representation(self, obj):
        """
        Dynamically change field name of serializer. Takes argument `alt_name_map` for this purpose.
        alt_name_map = {<actual_name_1>: <new_name_1>, <actual_name_2>: <new_name_2>}
        Example:
         SomeSerializer(instance, fields=["field_1", "field_xyz"], alt_name_map={"field_xyz": "field_2"}).data
         Outputs => {'field_1': <some_val_1>, 'field_2': <some_val_2>}
        """

        primitive_repr = super(BaseSerializer, self).to_representation(obj)
        for existing_field_name, new_field_name in self.alt_name_map.items():
            primitive_repr[new_field_name] = primitive_repr[existing_field_name]
            primitive_repr.pop(existing_field_name)
        return primitive_repr


class PaginatedSerializer():

    def __init__(self, queryset, page, num, serializer_method=None, context=None, **kwargs):
        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count
        previous = None if not objects.has_previous() else objects.previous_page_number()
        next = 0 if not objects.has_next() else objects.next_page_number()
        if serializer_method:
            serializer = serializer_method(objects, many=True, context=context, **kwargs)
            data = serializer.data
        else:
            data = objects.object_list
        self.pages_count = paginator.num_pages
        self.data = {'count': count, 'previous': previous, 'next': next, 'result': data}


class CustomDateTimeField(serializers.DateTimeField):
    def to_representation(self, value):
        tzname = settings.ACT_TIME_ZONE
        if not tzname:
            tzname = None

        value = timezone.localtime(value, timezone=pytz.timezone(tzname))
        value = value.strftime(
            settings.APP_DATETIME_FORMAT)

        return super().to_representation(value)
