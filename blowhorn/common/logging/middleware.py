import json
import copy
import time
import uuid
import traceback
from django.conf import settings

from .logger import BHLogger
from .filters import SafeLoggingFilter
from blowhorn.common.middleware_mixin import  MiddlewareMixin

LOG_LEVEL = getattr(settings, 'LOG_LEVEL', 1)
logger = BHLogger(__name__, LOG_LEVEL)


class RequestLoggingMiddleware(MiddlewareMixin):

    def process_request(self, request):
        self.start_time = time.time()
        if "multipart/form-data" not in request.META.get('CONTENT_TYPE', []):
            request_details = {}
            if request.POST:
                request_details = dict(request.POST)
            elif request.GET:
                request_details = dict(request.GET)
            elif request.body:
                try:
                    request_body_obj = json.loads(request.body.decode('utf-8'))
                    request_details = copy.deepcopy(request_body_obj)
                except ValueError:
                    pass
            request_details = SafeLoggingFilter().get_cleansed_multivaluedict(request_details)
            request_id = self._generate_id()
            setattr(request, 'request_id', request_id)
            META = getattr(request, 'META', {})
            logger.info(
                "Logging Api Request",
                extra={
                    "info": {
                        "log_type": "api",
                        "url": request.get_full_path(),
                        "log_sub_type": request.method,
                        "request_params": request_details,
                        "request_id": getattr(request, "request_id", None),
                        "remote_addr": META.get('REMOTE_ADDR', '-'),
                        "server_protocol": META.get('SERVER_PROTOCOL', '-'),
                        "http_user_agent": META.get('HTTP_USER_AGENT', '-'),
                    }
                }
            )

    def process_response(self, request, response):
        if hasattr(self, 'start_time'):
            req_time = time.time() - self.start_time
            META = getattr(request, 'META', {})
            logger.info(
                "Logging Api Response",
                extra={
                    "info": {
                        "log_type": "api",
                        "url": request.get_full_path(),
                        "log_sub_type": request.method,
                        "response_status": response.status_code,
                        "response_time": req_time,
                        "request_user": self.get_request_user(request),
                        "request_id": getattr(request, "request_id", None),
                        "remote_addr": META.get('REMOTE_ADDR', '-'),
                        "server_protocol": META.get('SERVER_PROTOCOL', '-'),
                        "http_user_agent": META.get('HTTP_USER_AGENT', '-'),
                    }
                }
            )
        return response

    def get_request_user(self, request):
        user_details = {}
        try:
            if request and request.user:
                user_details = {
                    "user_type": request.user.__class__.__name__,
                    "user_id": request.user.id
                }
        except Exception as e:
            user_details = {}
        return user_details

    def process_exception(self, request, exception):
        data = {
            'message': str(exception),
            'exception': traceback.format_exc()
        }
        logger.error(
            str(exception),
            extra={
                "info": {
                    "log_type": "exception",
                    "url": request.get_full_path(),
                    "log_sub_type": str(exception.__class__.__name__),
                    "exception_data": data
                }
            }
        )

    def _generate_id(self):
        return uuid.uuid4().hex
