import sys
import logging

logging.raiseExceptions = 0


class BHLogger(logging.getLoggerClass()):

    def __init__(self, name, level):
        self.level = level
        self.logger = logging.getLogger(name)
        self.logger.setLevel(self.level)

        stream_handler = logging.StreamHandler(sys.stdout)
        stream_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s - %(info)s')
        stream_handler.setFormatter(stream_formatter)
        self.logger.addHandler(stream_handler)

    def log(self, message, level=None, extra=None):
        if level is None:
            level = self.level
        if extra is None:
            extra = {'info': {}}
        self.logger.log(level, message, extra=extra)

    def debug(self, message, extra=None):
        if extra is None:
            extra = {'info': {}}
        self.logger.debug(message, extra=extra)

    def info(self, message, extra=None):
        if extra is None:
            extra = {'info': {}}
        self.logger.info(message, extra=extra)

    def warn(self, message, extra=None):
        if extra is None:
            extra = {'info': {}}
        self.logger.warn(message, extra=extra)

    def error(self, message, extra=None):
        if extra is None:
            extra = {'info': {}}
        self.logger.error(message, extra=extra)

    def exception(self, message, extra=None):
        if extra is None:
            extra = {'info': {}}
        self.logger.exception(message, extra=extra)
