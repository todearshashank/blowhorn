import copy


CLEANSED_SUBSTITUTE = "**********"
SENSITIVE_PARAMETERS = ['password']


class SafeLoggingFilter(object):

    def get_cleansed_multivaluedict(self, multivaluedict=None):
        if multivaluedict:
            cleansed = copy.deepcopy(multivaluedict)
            # Cleanse only the specified parameters.
            for param in SENSITIVE_PARAMETERS:
                if param in cleansed:
                    cleansed[param] = CLEANSED_SUBSTITUTE
            return cleansed
        return multivaluedict
