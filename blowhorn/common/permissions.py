from rest_framework import permissions


class BlockApi(permissions.BasePermission):
    """
    Permission to allow access for Active Drivers.
    """

    def has_permission(self, request, view):
        return False
