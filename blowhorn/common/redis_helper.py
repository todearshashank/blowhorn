import redis
from django.conf import settings
from django_redis import get_redis_connection

REDIS_HOST = getattr(settings, 'REDIS_HOST', 'localhost')


class Singleton(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs)
        return cls._instances[cls]


class RedisHelper(metaclass=Singleton):

    def __init__(self, ):
        self.pool = get_redis_connection('default').connection_pool
        self.redis_client_object = redis.Redis(connection_pool=self.pool)

    def add_or_get_lock(self, method_identifier, delay=300):
        lock_status_key = method_identifier
        lock_status = 1
        existing_lock = self.redis_client_object.setnx(
            lock_status_key, lock_status)
        if existing_lock:
            self.redis_client_object.expire(name=lock_status_key, time=delay)
        return existing_lock

    def get_lock(self, method_identifier):
        return True if self.redis_client_object.get(method_identifier) \
            else False

    def expire_lock(self, task_name):
        self.redis_client_object.delete(task_name)


class StrictRedisHelper(metaclass=Singleton):

    def __init__(self, ):
        self.pool = get_redis_connection('default').connection_pool
        self.redis_client_object = redis.StrictRedis(
            host=REDIS_HOST, decode_responses=True)

    def get_connection(self):
        return self.redis_client_object
