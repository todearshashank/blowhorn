import warnings

from django import forms
from django.core.exceptions import ValidationError


try:
    from jsoneditor.fields.django_jsonfield import JSONField as JSONFieldBase
except ImportError:
    from django.db import models

    class JSONFieldBase(models.TextField):

        def __init__(self, *args, **kwargs):
            warnings.warn(
                '"jsoneditor" module not available, to enable json mode '
                'please run: "pip install djongo[json]"', stacklevel=2)
            models.TextField.__init__(self, *args, **kwargs)


class NoSqlJSONField(JSONFieldBase):

    def get_prep_value(self, value):
        return value


class CustomModelChoiceField(forms.ModelChoiceField):
    """
        version: 1.1.0
        Inherited from ModelChoiceField for displaying custom label
        for options.
        config:
            - field_names: An extra positional argument accepts list.
                e.g.: field_names=['pk', 'name']
            - separator: An optional arg. accepts char as separator
                for given str_fields. if not passed, eiphen will be used.
    """

    def __init__(self, field_names=None, separator=None, *args, **kwargs):
        super(CustomModelChoiceField, self).__init__(*args, **kwargs)
        self.field_names = field_names
        self.separator = separator or '-'

    def label_from_instance(self, obj):
        if self.field_names is not None:
            if not isinstance(self.field_names, list):
                raise ValidationError('`field_names` should be list.')

            return self.separator.join(
                [('%s' % getattr(obj, fn, '')) for fn in self.field_names])

        return super(CustomModelChoiceField, self).label_from_instance(obj)
