from django.core import mail
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.conf import settings

DEFAULT_FROM_EMAIL = getattr(settings, "DEFAULT_FROM_EMAIL")


class Mailer:
    """
    Send email messages helper class
    """

    def __init__(self, from_email=DEFAULT_FROM_EMAIL):
        self.connection = mail.get_connection()
        self.from_email = from_email

    def send_messages(self, to_emails, subject, content=None, template=None, context=None):
        """
        :param to_emails: recipient email address[es]
        :param subject: Email message subject
        :param content: Email message content (provided in case no template is used.)
        :param template: Email template
        :param context: data to be used for generating template.
        """

        messages = self.__generate_messages(to_emails, subject, content, template, context)
        self.__send_mail(messages)

    def __send_mail(self, mail_messages):
        """
        Send email messages
        :param mail_messages:
        """

        self.connection.open()
        self.connection.send_messages(mail_messages)
        self.connection.close()

    def __generate_messages(self, to_emails, subject, content=None, template=None, context=None):
        """
        Generate email message with/ without Django template.
        :param to_emails: recipient email address[es]
        :param subject: Email message subject
        :param subject: Email message content (provided in case no template is used.)
        :param template: Email template
        :param context: data to be used for generating template.
        """

        messages = []
        if template:
            message_template = get_template(template)
            for recipient in to_emails:
                message_content = message_template.render(context)
                message = EmailMessage(subject, message_content, to=[recipient], from_email=self.from_email)
                message.content_subtype = 'html'
                messages.append(message)
        else:
            for recipient in to_emails:
                message = EmailMessage(subject, content, to=[recipient], from_email=self.from_email)
                messages.append(message)
        return messages
