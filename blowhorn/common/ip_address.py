import binascii
import logging
import socket

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class IpAddress(object):

    def get_host_by_name(self, domain_name):
        """
        :param domain_name:
        :return:
        """
        try:
            return socket.gethostbyname(domain_name)
        except socket.error:
            logger.warning('Failed to resolve name')
            return None

    def ip_in_subnetwork(self, ip_address, subnetwork):
        """
        Returns True if the given IP address belongs to the
        subnetwork expressed in CIDR notation, otherwise False.
        Both parameters are strings.

        Both IPv4 addresses/subnetworks (e.g. "192.168.1.1"
        and "192.168.1.0/24") and IPv6 addresses/subnetworks (e.g.
        "2a02:a448:ddb0::" and "2a02:a448:ddb0::/44") are accepted.
        """

        ip_integer, version1 = self.ip_to_integer(ip_address)
        ip_lower, ip_upper, version2 = self.subnetwork_to_ip_range(subnetwork)
        if version1 != version2:
            logger.warning('Incompatible IP versions')
            return False

        return ip_lower <= ip_integer <= ip_upper

    def ip_to_integer(self, ip_address):
        """
        Converts an IP address expressed as a string to its
        representation as an integer value and returns a tuple
        (ip_integer, version), with version being the IP version
        (either 4 or 6).

        Both IPv4 addresses (e.g. "192.168.1.1") and IPv6 addresses
        (e.g. "2a02:a448:ddb0::") are accepted.
        """

        # try parsing the IP address first as IPv4, then as IPv6
        for _version in (socket.AF_INET, socket.AF_INET6):
            try:
                ip_hex = socket.inet_pton(_version, ip_address)
                ip_integer = int(binascii.hexlify(ip_hex), 16)
                return ip_integer, 4 if _version == socket.AF_INET else 6
            except socket.error:
                logger.info('Failed to convert given ip: %s, version: %s' %
                            (ip_address, _version))
                pass

        return False

    def subnetwork_to_ip_range(self, subnetwork):
        """
        Returns a tuple (ip_lower, ip_upper, version) containing the
        integer values of the lower and upper IP addresses respectively
        in a subnetwork expressed in CIDR notation (as a string), with
        version being the subnetwork IP version (either 4 or 6).

        Both IPv4 subnetworks (e.g. "192.168.1.0/24") and IPv6
        subnetworks (e.g. "2a02:a448:ddb0::/44") are accepted.
        """
        try:
            fragments = subnetwork.split('/')
            network_prefix = fragments[0]
            netmask_len = int(fragments[1])
            # try parsing the subnetwork first as IPv4, then as IPv6
            for version in (socket.AF_INET, socket.AF_INET6):
                ip_len = 32 if version == socket.AF_INET else 128
                try:
                    suffix_mask = (1 << (ip_len - netmask_len)) - 1
                    netmask = ((1 << ip_len) - 1) - suffix_mask
                    ip_hex = socket.inet_pton(version, network_prefix)
                    ip_lower = int(binascii.hexlify(ip_hex), 16) & netmask
                    ip_upper = ip_lower + suffix_mask
                    return ip_lower, ip_upper, 4 if version == socket.AF_INET \
                        else 6
                except socket.error:
                    logger.info('Failed to parse given subnet: %s' % subnetwork)
                    pass
        except:
            pass

        return False
