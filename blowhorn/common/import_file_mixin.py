import os
import logging
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

from import_export.admin import ImportMixin
from import_export.resources import Resource
from import_export.formats.base_formats import CSV, XLS, XLSX, JSON

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEFAULT_FORMATS = [CSV, XLS, XLSX, JSON]
FORMAT_INDEX = {
    'csv': 0,
    'xls': 1,
    'xlsx': 0
}


class ImportFileMixin(object):
    base_mixin = ImportMixin()
    resource = Resource()
    #: available import formats
    formats = DEFAULT_FORMATS
    #: import data encoding
    from_encoding = "utf-8"
    file_obj = None

    def get_import_formats(self):
        """
        :return: available import formats as method references
        """
        return [f for f in self.formats if f().can_import()]

    def __get_file_extension(self, filename):
        """
        :param filename: imported filename (str)
        :return: file extension(str)
        """
        return os.path.splitext(filename)[1].replace('.', '')

    def process_imported_file(self, file_obj):
        import_formats = self.get_import_formats()
        file_ext = self.__get_file_extension(file_obj.name)
        file_format_index = FORMAT_INDEX.get(file_ext)
        if file_format_index is None:
            return False,\
                   _('Uploaded file format not supported.Supported file'
                     ' formats: %s' % ', '.join(FORMAT_INDEX.keys()))

        input_format = import_formats[file_format_index]()
        data = file_obj.file.read()
        if not input_format.is_binary() and self.from_encoding:
            data = force_text(data, self.from_encoding)

        dataset = input_format.create_dataset(data)
        logger.info(dataset)
        return True, dataset

    def get_headers(self, dataset):
        """
        :param dataset: processed data from file (Tablib:Dataset)
        :return: List of headers found in dataset
        """
        return [row for row in dataset.dict[0]]
