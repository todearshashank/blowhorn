import time
from django.conf import settings
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (Content, MimeType,
    Mail, TrackingSettings, ClickTracking, To)
from blowhorn.customer.models import AccountUser, Customer

class SendgridEmail:

    def send_mail(self, content, subject, email=settings.DEFAULT_TESTMAIL, cc=None):
        try:
            sendgrid_client = SendGridAPIClient(settings.SENDGRID_API_KEY)
            content = Content(MimeType.html, content)
            message = Mail(settings.DEFAULT_FROM_EMAIL, email, subject, content)
            tracking_settings = TrackingSettings()
            tracking_settings.click_tracking = ClickTracking(enable=False, enable_text=False)

            message.tracking_settings = tracking_settings
            sendgrid_client.send(message=message)

        except Exception as e:
            print(e)

    def send_mass_mail(self, content, subject, emails=settings.DEFAULT_TESTMAIL, is_test_mail=True):
        try:
            sendgrid_client = SendGridAPIClient(settings.SENDGRID_API_KEY)
            content = Content(MimeType.html, content)
            message = Mail(settings.DEFAULT_FROM_EMAIL, None, subject, content)
            message.to = settings.DEFAULT_FROM_EMAIL

            tracking_settings = TrackingSettings()
            tracking_settings.click_tracking = ClickTracking(enable=False, enable_text=False)
            message.tracking_settings = tracking_settings
            email_len = len(emails)
            times = int(email_len/500)
            current = 0
            print(email_len,times)

            for _ in range(times):
                t = current
                current = t + 500
                print(t,current)
                _temp = emails[t:current]
                if is_test_mail:
                    print(_temp)
                else:
                    print('Sending mails...')
                    if settings.ENABLE_SLACK_NOTIFICATIONS:
                        message.bcc = _temp
                        sendgrid_client.send(message=message)
                        time.sleep(100)

            print(current,email_len)
            if current < email_len:
                _temp = emails[current::]
                if is_test_mail:
                    print(_temp)
                else:
                    print('Sending mails...')
                    if settings.ENABLE_SLACK_NOTIFICATIONS:
                        message.bcc = _temp
                        sendgrid_client.send(message=message)

        except Exception as e:
            print(e)

    def send_email_business_customer(self, content, subject, is_test_mail=True):
        emails = Customer.objects.filter(user__is_active=True
                    ).exclude(customer_category='Individual').values_list('user__email', flat=True)
        self.send_mass_mail(content, subject, emails, is_test_mail)

    def send_email_account_user(self, content, subject, is_test_mail=True):
        emails = AccountUser.objects.filter(status='active').values_list('user__email', flat=True)
        self.send_mass_mail(content, subject, emails, is_test_mail)

    def send_email_sales_internal(self, content, subject, is_test_mail=True):
        emails = ['all.sales@blowhorn.com']
        self.send_mass_mail(content, subject, emails, is_test_mail)
