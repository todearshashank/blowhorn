# from celery.task import task
from celery import Celery
app = Celery()


@app.task(name='execute')
def execute(module_name, method_name, *args, **kwargs):
    try:
        m = __import__(module_name)
        if m.__name__ != module_name:
            modules = module_name.split('.')[1:]
            for module in modules:
                m = getattr(m, module)
        f = getattr(m, method_name)
        if hasattr(f, 'func'):
            f = f.func
        return f(*args, **kwargs)
    except:
        pass
