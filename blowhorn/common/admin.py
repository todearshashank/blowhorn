import re
import operator
from datetime import datetime
from functools import reduce
from django.contrib import admin, messages
from django.contrib.admin.utils import lookup_needs_distinct
from django.core.exceptions import FieldDoesNotExist
from django.db import models
from django.http import HttpResponseRedirect

from blowhorn.utils.datetimerangefilter import DateRangeFilter, DateFilter, \
    DateTimeRangeFilter

AUDITLOG_FIELDS = ['created_date', 'created_by', 'modified_date', 'modified_by']


def get_next_in_date_hierarchy(request, date_hierarchy):
    if date_hierarchy + '__day' in request.GET:
        return 'hour'
    if date_hierarchy + '__month' in request.GET:
        return 'day'
    if date_hierarchy + '__year' in request.GET:
        return 'week'
    return 'month'


def get_list_filter_field_names(modaladmin):
    """
    To get date related filter names in an admin class
    :param modaladmin:
    :return:
    """
    list_filter_names = []
    for filter_item in modaladmin.list_filter:
        if isinstance(filter_item, (list, tuple)) \
            and isinstance(filter_item[1], type) \
            and issubclass(filter_item[1], (DateRangeFilter, DateFilter,
                                            DateTimeRangeFilter)):
            list_filter_names.append(filter_item[0])
    return list_filter_names


def get_filter_dict(modeladmin, params):
    """
    To get query params as dict for export
    """
    date_filter_fields = get_list_filter_field_names(modeladmin)
    params = params[1].split('&')
    _dict = {}
    for param in params:
        if not param:
            continue

        exp = param.split('=')
        _key = exp[0]
        if _key in ['q', 'all']:
            continue

        if len(exp) > 1:
            val = exp[1]
            if not val:
                continue

            if '__in' in _key:
                val = val.split(',')

            splitted_key = _key.split('__')
            if splitted_key[0] in date_filter_fields:
                _key = '%s__date__%s' % (
                    splitted_key[0],
                    splitted_key[1])
                val = datetime.strptime(val, '%d-%m-%Y').strftime('%Y-%m-%d')

            if 'q' not in _key:
                _dict[_key] = val
    return _dict


class NonEditableTabularInlineAdmin(admin.TabularInline):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        editable_fields = getattr(self, "editable_fields", [])
        read_only_fields = list(set(
            [field.name for field in self.opts.local_fields if field.name not in editable_fields] +
            [field.name for field in self.opts.local_many_to_many if field.name not in editable_fields])
        )
        read_only_fields_custom = set(self.readonly_fields) - set(field.name for field in self.opts.local_fields)
        read_only_fields.extend(read_only_fields_custom)
        return read_only_fields


class BaseAdmin(admin.ModelAdmin):
    SEARCH_TERM_TOKEN_REGEX = r'"(.*?)"'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        try:
            return super().change_view(
                request, object_id, extra_context=extra_context)

        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        try:
            return super().add_view(request, extra_context=extra_context)
        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def get_multi_search_fields(self, request):
        """
        Returns fields defined for multi-search in admin class
        """
        if hasattr(self, 'multi_search_fields'):
            return self.multi_search_fields
        return []

    def get_split_delimiter(self, request):
        """
        Return split delimiter defined in admin class
        """
        if hasattr(self, 'multi_search_split_delimiter'):
            return self.multi_search_split_delimiter
        return None

    def split_search_term(self, request, search_term):
        """
        Returns list of search values by splitting search_term with given
        delimiter
        """
        separator = self.get_split_delimiter(request)
        return [b.strip() for b in search_term.split(separator) if b]

    def can_split_searchterm(self, search_term):
        """
        :param search_term: string from search field
        :return: empty if search term is not inside `double quotes`
        """
        return not re.match(self.SEARCH_TERM_TOKEN_REGEX, search_term)

    def get_searchterm(self, search_term):
        """
        :param search_term: string from search field surrounded by
        `double-quotes`
        :return: string present between double-quotes
        """
        return re.findall(self.SEARCH_TERM_TOKEN_REGEX, search_term)

    def get_search_results(self, request, queryset, search_term):
        """
        - Overridden method of default django admin get_search_results
            to support multi-search

        - Multi-search can be enabled by defining
            `multi_search_fields` as list/tuple in admin class
        """
        # Apply keyword searches.
        def construct_search(field_name):
            if field_name.startswith('^'):
                return "%s__istartswith" % field_name[1:]
            elif field_name.startswith('='):
                return "%s__iexact" % field_name[1:]
            elif field_name.startswith('@'):
                return "%s__search" % field_name[1:]
            # Use field_name if it includes a lookup.
            opts = queryset.model._meta
            lookup_fields = field_name.split()
            # Go through the fields, following all relations.
            prev_field = None
            for path_part in lookup_fields:
                if path_part == 'pk':
                    path_part = opts.pk.name
                try:
                    field = opts.get_field(path_part)
                except FieldDoesNotExist:
                    # Use valid query lookups.
                    if prev_field and prev_field.get_lookup(path_part):
                        return field_name
                else:
                    prev_field = field
                    if hasattr(field, 'get_path_info'):
                        # Update opts to follow the relation.
                        opts = field.get_path_info()[-1].to_opts
            # Otherwise, use the field with icontains.
            return "%s__icontains" % field_name

        use_distinct = False
        distinct_on = []
        try:
            if search_term:
                search_fields = self.get_search_fields(request)
                can_split_searchterm = self.can_split_searchterm(search_term)

                splitted_params = []
                multi_search_fields = []
                if can_split_searchterm:
                    splitted_params = self.split_search_term(request, search_term)
                    multi_search_fields = self.get_multi_search_fields(request)

                if multi_search_fields and len(splitted_params) > 1:
                    search_lookups = ['%s__in' % f for f in multi_search_fields]
                    search_params = [models.Q(**{s_lookup: splitted_params}) for
                                     s_lookup in search_lookups]

                    if search_params:
                        queryset = queryset.filter(
                            reduce(operator.or_, search_params))

                    distinct_on = search_lookups

                elif search_fields:
                    orm_lookups = [construct_search(str(search_field))
                                   for search_field in search_fields]

                    if not can_split_searchterm:
                        search_term = self.get_searchterm(search_term)
                    else:
                        search_term = self.split_search_term(request, search_term)

                    for bit in search_term:
                        or_queries = [models.Q(**{orm_lookup: bit})
                                      for orm_lookup in orm_lookups]
                        queryset = queryset.filter(reduce(operator.or_, or_queries))
                    distinct_on = orm_lookups
        except:
            queryset = queryset.none()
            # err = "Multi search can be used for fields: [%s]" % multi_search_fields
            # messages.error(request, err)

        use_distinct |= any(
            lookup_needs_distinct(self.opts, search_spec) for
            search_spec in distinct_on)

        return queryset, use_distinct


class NonEditableAdmin(BaseAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = []
        if obj:
            editable_fields = getattr(self, "editable_fields", [])
            read_only_fields = list(set(
                [field.name for field in self.opts.local_fields if field.name not in editable_fields] +
                [field.name for field in self.opts.local_many_to_many if field.name not in editable_fields])
            )
            read_only_fields_custom = set(self.readonly_fields) - set(field.name for field in self.opts.local_fields)
            read_only_fields.extend(read_only_fields_custom)
        return read_only_fields
