import logging
import requests
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Bitbucket(object):
    client_key = None
    client_secret = None
    AUTH_URL = 'https://bitbucket.org/site/oauth2/access_token'
    API_BASE_URL = 'https://api.bitbucket.org/2.0/'
    CONTENT_URL_TEMPLATE = 'https://api.bitbucket.org/2.0/repositories/{username}/{repo_slug}/src/{branch_name}/{file_path}'
    # grant_type=client_credentials
    """
    {
        "access_token": "<access_token>", 
        "scopes": "project",
        "expires_in": 7200,
        "refresh_token": "<refresh_token>",
        "token_type": "bearer"
    }
    <access_token>: Pass this token in future requests
    """

    def __init__(self, client_key=None, client_secret=None, auth_retry_count=2,
                 fetch_retry_count=3):
        self.client_key = client_key or settings.BITBUCKET_AUTH.get('client_key', None)
        self.client_secret = client_secret or settings.BITBUCKET_AUTH.get('client_secret', None)
        if not client_key or not client_secret:
            raise ImproperlyConfigured(
                _('`CLIENT_KEY` and `CLIENT_SECRET` are required')
            )
        self.auth_retry_count = auth_retry_count
        self.fetch_retry_count = fetch_retry_count

    def __make_auth_request(self):
        """
        :return:
        """
        return requests.post(
            self.AUTH_URL,
            auth=(self.client_key, self.client_secret),
            data=dict(grant_type='client_credentials')
        )

    def __make_fetch_request(self, url):
        return requests.get(url)

    def authenticate(self):
        """
        :return:
        """
        try:
            response = self.__make_auth_request()
            if response.status_code == 200:
                return response.json()
            else:
                logger.info('Failed to authenticate: %s' % response)
                return None
        except TimeoutError:
            self.auth_retry_count -= 1
            if self.auth_retry_count > 0:
                self.authenticate()
            return None
        except BaseException as be:
            logger.info('Failed to get auth token: %s' % be)
            return None

    def fetch_file_content(self, username, repo_slug, branch_name, file_path):
        """
        Example: https://api.bitbucket.org/2.0/repositories/aksharadt/api-documents/src/master/swagger.json
        :return:
        """

        url = self.CONTENT_URL_TEMPLATE.format(
            username=username,
            repo_slug=repo_slug,
            branch_name=branch_name,
            file_path=file_path
        )
        try:
            response = self.__make_auth_request()
            return response.json()
        except TimeoutError:
            self.fetch_retry_count -= 1
            if self.fetch_retry_count > 0:
                self.authenticate()
            return None
        except BaseException as be:
            logger.info('Failed to get auth token: %s' % be)
            return None
