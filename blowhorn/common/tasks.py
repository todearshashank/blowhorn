# System & Django
import os
import logging
import json
import zipfile

from django.template import loader
from django.conf import settings
from django.core.mail import EmailMessage

# Blowhorn Modules
from blowhorn import celery_app
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.common.communication import Communication
from blowhorn.common.firebase_operations import firebase_add_update_database
from blowhorn.common.CMS.auth import Auth
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_SME

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@celery_app.task()
def send_async_mail(recepients, subject, body='', attachments=[]):
    email_ = EmailMessage(
        subject=subject,
        body=body,
        to=recepients,
    )
    # if attachments:
    for attachment in attachments:
        filename, content, mime = attachment
        email_.attach(filename, content, mime)
    email_.send(fail_silently=True)


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def send_sms(self, recipient, message, template_json, priority='high', customer_id=None):
    logger.info('Sending message to phone number: %s' % recipient)
    template_dict = json.loads(template_json)
    if settings.ENABLE_SLACK_NOTIFICATIONS:
        Communication.send_sms(sms_to=recipient, message=message, 
            template_dict=template_dict, priority=priority,
            customer_id=customer_id)

@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def update_cms(self, city_ids=None, vehicle_class_ids=None, rate_card=None):
    if settings.DEBUG:
        return
    from blowhorn.address.models import City
    from blowhorn.contract.utils import ContractUtils
    if city_ids and not rate_card:
        cities = City.objects.filter(id__in=[city_ids])
        Auth().upsert_city(cities)
    elif vehicle_class_ids:
        from blowhorn.vehicle.models import VehicleClass
        vehicle_class = VehicleClass.objects.filter(id__in=[vehicle_class_ids])
        Auth().upsert_vehicle_class(vehicle_class)
    elif rate_card:
        cities = City.objects.filter(id__in=[city_ids], contract__contract_type__in=[CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME],
                                     contract__status=CONTRACT_STATUS_ACTIVE)
        for city in cities:
            data = ContractUtils.get_sell_packages(
                cities=[city], contract_types=[CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME])
            Auth().upsert_rate_card([city], data)


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES,
             ignore_result=False)
def send_zippped_consignment_notes(self, trips, email, customer_names):
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    from django.core.mail import EmailMultiAlternatives
    email = email.split(',')
    subject = 'Consignment Notes'

    context = {
        "customer_names" : customer_names,
        "brand_name" : settings.BRAND_NAME,
    }

    email_template_name = 'emailtemplates_v2/consignment_notes.html'
    html_content = loader.render_to_string(email_template_name, context)

    mail = EmailMultiAlternatives(
        subject, html_content, to=email,
        from_email=settings.DEFAULT_FROM_EMAIL)

    mail.attach_alternative(html_content, "text/html")

    with zipfile.ZipFile(settings.TEMPORARY_FILE_DIR + '/' +
                         'consignment_trip' + '.zip', 'w',
                         compression=zipfile.ZIP_DEFLATED) as myzip:
        for trip in trips:
            myzip.writestr(str(trip.get('trip_number')) + ".txt",
                           trip.get('consignment_trip__content'))

    zf = open(myzip.filename, 'rb')
    _file = zf.read()

    mail.attach(os.path.basename(
        str(myzip.filename)), _file, 'text/zip')
    mail.send()

    os.remove(settings.TEMPORARY_FILE_DIR +
              '/' + 'consignment_trip' + '.zip')


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def broadcast_to_firebase(_dict=None):
    _dict = json.loads(_dict)
    url = _dict.pop('url')
    firebase_add_update_database(json.dumps(_dict), url)
