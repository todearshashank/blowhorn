import json
from django.conf import settings
from threading import current_thread

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from blowhorn.common.middleware_mixin import MiddlewareMixin

""" Refer https://stackoverflow.com/questions/30871033/django-rest-framework-remove-csrf/33778953
	To use this add
	authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
	in calling methods
"""
_requests = {}
user = None

class CsrfExemptSessionAuthentication(SessionAuthentication):

   def enforce_csrf(self, request):
       return


def current_request():
    return _requests.get(current_thread().ident)


class RequestMiddleware(MiddlewareMixin):

    def process_request(self, request):
        _requests[current_thread().ident] = request
        user = request.user

    def process_response(self, request, response):
        # when response is ready, request should be flushed
        _requests.pop(current_thread().ident, None)
        return response

    def process_exception(self, request, exception):
        # if an exception has happened, request should be flushed too
        _requests.pop(current_thread().ident, None)
        raise exception
