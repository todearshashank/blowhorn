from django.conf import settings
from elasticsearch import Elasticsearch, helpers

from django.utils import timezone
from datetime import timedelta

from blowhorn.order.models import Order

def bulk_index_orders():
    """
    Bulk indexing of documents updated since last run
    """
    # not using elastic search for shipment order page
    # the new shipment order page is not used
    return
    curr_time = timezone.now()
    last_ran = curr_time - timedelta(hours=2)
    orders = Order.def_obj.filter(order_type='shipment',
                                  modified_date__gte=last_ran,
                                  hub__isnull=False).select_related(
        'city', 'customer_contract', 'customer', 'driver', 'hub',
        'pickup_hub').only('number', 'reference_number', 'customer_reference_number',
                           'city__name', 'city__id','expected_delivery_time',
                           'customer_contract__name',
                           'customer_contract__service_schedule',
                           'customer__name', 'hub__name', 'pickup_hub__name',
                           'driver__name', 'driver__driver_vehicle',
                           'hub__pincodes', 'pickup_hub__pincodes')

    elastic = Elasticsearch(
        [{'host': settings.ES_HOST, 'port': settings.ES_PORT}])

    _response = helpers.bulk(elastic, bulk_order_json(orders))


def bulk_order_json(order_qs):
    """
    generator function to return index doc for each record
    """
    _index = 'order_' + settings.ELASTICSEARCH_INDEX_POSTFIX
    for order in order_qs:
        _document = {"_index": _index, "_id": order.id, "_type": "_doc", "id": order.id, "number": order.number,
                           "reference_number": order.reference_number,
                           "customer_reference_number": order.customer_reference_number,
                           "customer_contract": {"name": order.customer_contract.name if order.customer_contract else None,
                           "pk": order.customer_contract.id if order.customer_contract else None,
                                                 "city": {"name": order.city.name, "pk": order.city.id}},
                           "customer": {"name": order.customer.name if order.customer else None,
                           "pk": order.customer.id, },
                           "hub": {"name": order.hub.name if order.hub else None,
                           "pk": order.hub.id if order.hub else None, },
                           "pickup_hub": {"name": order.pickup_hub.name if order.pickup_hub else None,
                           "pk": order.pickup_hub.id if order.pickup_hub else None},
                           "driver": {"name": order.driver.name if order.driver else None,
                           "driver_vehicle": order.driver.driver_vehicle if order.driver else None,
                           "pk": order.driver.id if order.driver else None}}
        yield _document

def delete_index(index_name):
    """
    delete index from elasticsearch
    """
    elastic = Elasticsearch(
        [{'host': settings.ES_HOST, 'port': settings.ES_PORT}])

    _response = elastic.indices.delete(index=index_name)

    return _response


