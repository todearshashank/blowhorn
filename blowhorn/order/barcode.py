from reportlab.graphics.barcode import createBarcodeDrawing
from reportlab.graphics.shapes import Drawing

from reportlab.lib import units
from django.conf import settings

class barcodeDrawing(Drawing):
    
    def get_barcode(value, width, barWidth = 0.05 * units.inch, fontSize = 30, \
        humanReadable = True):

        barcode = createBarcodeDrawing('Code128', value = value, \
            barWidth = barWidth, fontSize = fontSize, \
                humanReadable = humanReadable)

        drawing_width = width
        barcode_scale = drawing_width / barcode.width
        drawing_height = barcode.height * barcode_scale

        drawing = Drawing(drawing_width, drawing_height)
        drawing.scale(barcode_scale, barcode_scale)
        drawing.add(barcode, name='barcode')
        return drawing

    def get_image(self, value1):
        path = settings.MEDIA_ROOT + '/uploads/barcode/'
        barcode = barcodeDrawing.get_barcode(value1 , width = 300)
        barcode.save(formats=['png'],outDir=path, fnRoot=value1)