# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-06 11:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0015_auto_20180105_1048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='payment_status',
            field=models.CharField(blank=True, choices=[('Un-Paid', 'Un-Paid'), ('Paid', 'Paid')], default='Un-Paid', max_length=250, null=True, verbose_name='Payment Status'),
        ),
        migrations.AlterField(
            model_name='order',
            name='vehicle_class_preference',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='vehicle.VehicleClass', verbose_name='Vehicle Class'),
        ),
    ]
