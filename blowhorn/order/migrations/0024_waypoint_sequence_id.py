# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-20 21:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0023_auto_20180116_0615'),
    ]

    operations = [
        migrations.AddField(
            model_name='waypoint',
            name='sequence_id',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Sequence ID'),
        ),
    ]
