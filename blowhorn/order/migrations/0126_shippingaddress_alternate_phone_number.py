# Generated by Django 2.1.1 on 2020-10-19 11:49

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0125_auto_20201019_0528'),
    ]

    operations = [
        migrations.AddField(
            model_name='shippingaddress',
            name='alternate_phone_number',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, help_text='Alternate Contact Number', max_length=128, null=True, verbose_name='Alternate Phone number'),
        ),
    ]
