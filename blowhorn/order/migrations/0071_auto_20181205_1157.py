# Generated by Django 2.1.1 on 2018-12-05 11:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0070_order_dispatch_log'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderdispatch',
            name='fcm_response',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='orderdispatch',
            name='mqtt_response',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='orderline',
            name='qty_delivered',
            field=models.IntegerField(default=0, verbose_name='Quantity Delivered'),
        ),
        migrations.AlterField(
            model_name='orderline',
            name='delivered',
            field=models.IntegerField(default=0, verbose_name='Delivered Units'),
        ),
    ]
