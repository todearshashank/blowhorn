# Generated by Django 2.2.23 on 2021-06-09 09:16

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0154_invoiceuploadbatch_file'),
        ('order', '0143_order_is_secondary_region'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderbatch',
            name='customer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='customer.Customer'),
        ),
        migrations.AddField(
            model_name='orderbatch',
            name='source',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Source'),
        ),
        migrations.AddField(
            model_name='orderbatch',
            name='successfull',
            field=models.IntegerField(blank=True, null=True, verbose_name='Successfull Order Creation Count'),
        ),
        migrations.AddField(
            model_name='orderbatch',
            name='summary',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Sync Summary'),
        ),
    ]
