# Generated by Django 2.1.1 on 2020-02-10 09:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0097_auto_20200205_1138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ordercommunication',
            name='contact_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Communicator Name'),
        ),
    ]
