# Generated by Django 2.1.1 on 2019-04-25 06:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0078_order_exp_delivery_start_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='amount_paid_online',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='amount paid online'),
        ),
        migrations.AddField(
            model_name='order',
            name='calculated_amount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='calculated amount after delivery'),
        ),
        migrations.AddField(
            model_name='order',
            name='calculation_details',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='extra_charges',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Extra Charges that has be to collected by cash'),
        ),
        migrations.AddField(
            model_name='order',
            name='payable_excluding_extra_charges',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Amount excluding extra charges'),
        ),
    ]
