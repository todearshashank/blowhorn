# Generated by Django 2.1.1 on 2019-08-12 09:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0087_auto_20190802_1805'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderline',
            name='item_sequence_no',
            field=models.IntegerField(blank=True, null=True, verbose_name='Item sequence number'),
        ),
    ]
