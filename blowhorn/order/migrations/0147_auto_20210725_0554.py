# Generated by Django 2.2.24 on 2021-07-25 05:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wms', '0005_auto_20210701_1502'),
        ('order', '0146_orderdocument_is_document_uploaded'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='xdk',
            field=models.NullBooleanField(verbose_name='Is Cross Dock?'),
        ),
        migrations.AddField(
            model_name='orderline',
            name='client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='wms.Client'),
        ),
        migrations.AddField(
            model_name='orderline',
            name='tracking_level',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Tracking Level'),
        ),
        migrations.AlterField(
            model_name='order',
            name='payment_mode',
            field=models.CharField(blank=True, choices=[('Online', 'Online'), ('Cash', 'Cash'), ('Cashfree', 'CashFree'), ('Ezetap', 'Ezetap'), ('Cheque', 'Cheque'), ('PayTm', 'PayTm'), ('Wallet', 'Wallet'), ('Bank Transfer', 'Bank Transfer'), ('Debit/Credit Card', 'Debit/Credit Card')], db_index=True, max_length=250, null=True, verbose_name='Payment Mode'),
        ),
    ]
