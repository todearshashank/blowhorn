# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-06-20 02:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0070_auto_20180615_0602'),
        ('order', '0054_auto_20180615_0602'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewShipmentOrder',
            fields=[
            ],
            options={
                'verbose_name': 'New Shipment Order',
                'verbose_name_plural': 'New Shipment Orders',
                'proxy': True,
                'indexes': [],
            },
            bases=('order.order',),
        ),
        migrations.AddField(
            model_name='order',
            name='invoice_driver',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='invoiced_orders', to='driver.Driver'),
        ),
    ]
