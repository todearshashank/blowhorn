# Generated by Django 2.2.24 on 2021-10-19 12:54

import audit_log.models.fields
import blowhorn.order.helper
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0165_auto_20211012_1630'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('order', '0152_auto_20211012_1607'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShippingLabelRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('file', models.FileField(blank=True, max_length=256, null=True, upload_to=blowhorn.order.helper.generate_file_path, verbose_name='Uploaded File')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='shippinglabelrequest_created_by', to=settings.AUTH_USER_MODEL)),
                ('customer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='customer.Customer')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='shippinglabelrequest_modified_by', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Shipping Label Request',
            },
        ),
    ]
