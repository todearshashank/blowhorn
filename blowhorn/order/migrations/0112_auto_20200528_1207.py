# Generated by Django 2.1.1 on 2020-05-28 12:07

import blowhorn.order.helper
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0111_auto_20200528_0707'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderbatch',
            name='file',
            field=models.FileField(blank=True, max_length=256, null=True, upload_to=blowhorn.order.helper.generate_file_path, verbose_name='Uploaded File'),
        ),
        migrations.AddField(
            model_name='shippingaddress',
            name='address_type',
            field=models.CharField(blank=True, choices=[('Residential', 'Residential'), ('Commercial', 'Commercial')], max_length=30, null=True, verbose_name='Address Type'),
        ),
    ]
