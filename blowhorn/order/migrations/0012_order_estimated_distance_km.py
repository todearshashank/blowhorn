# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-20 10:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0011_auto_20171212_0945'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='estimated_distance_km',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=12, verbose_name='Estimated Distance'),
        ),
    ]

