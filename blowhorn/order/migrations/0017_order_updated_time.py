# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-06 15:45
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0016_auto_20180106_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='updated_time',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2018, 1, 6, 15, 45, 32, 227681, tzinfo=utc), verbose_name='Updated Date Time'),
            preserve_default=False,
        ),
    ]
