# Generated by Django 2.1.1 on 2020-10-10 02:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0122_orderawbsequence'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderawbsequence',
            name='generated_awb',
            field=models.TextField(blank=True, null=True, verbose_name='AWB Numbers'),
        ),
    ]
