import collections
import logging
from datetime import timedelta
import math
from django.db.models import Count, Sum, ExpressionWrapper, F, DateTimeField, Q

from blowhorn.driver.payment_helpers.trips_helper import TripSet
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.utils import camel_case_to_snake_case
from blowhorn.contract.constants import (
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED, DRIVER
)
from blowhorn.order.const import STATUS_ORDER_EVENT_FIELD_MAPPING, ORDER_INVOICING_FIELDS
from blowhorn.order.models import Event, OrderLine, Order, OrderMessage, CallMaskingLog
from blowhorn.trip.models import Stop, Trip
from blowhorn.utils.functions import IST_OFFSET_HOURS
from config.settings.status_pipelines import (
    ORDER_DELIVERED, DELIVERY_ACK,
)
from blowhorn.wms.submodels.sku import WhSku

SHIPMENT_ATTR_TO_ORDERLINE_STATUS_MAPPING = {
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED: 'delivered',
}

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class OrderSet(object):

    expression_planned_start_time_ist = ExpressionWrapper(
        F('planned_start_time') + timedelta(hours=IST_OFFSET_HOURS), output_field=DateTimeField()
    )

    def __init__(self, orders, orders_applicable=None, **kwargs):
        self.orders = orders
        self.orders_applicable = orders_applicable
        self.bucket_type = kwargs.pop("bucket_type", None)
        self.bucket_value = kwargs.pop("bucket_value", None)
        self.considered_order_ids = []

    def has_single_customer_trips(self):
        customer_count = 0
        if self.orders_applicable:
            trip_list = Stop.objects.filter(order__in=self.orders_applicable
                                                ).values_list('trip',flat=True)
            customer_count = Stop.objects.filter(
                                    trip__in=trip_list).distinct(
                                    ).values_list('order__customer',
                                    flat=True).count()


        return customer_count == 1

    def get_call_masking_logs(invoice):
        total_call_charges = 0
        orders_set = set()
        orders = Order.objects.filter(invoiceorder__invoice=invoice).values_list('id', flat=True)
        call_logs = CallMaskingLog.objects.filter(order_id__in = orders).annotate(
                                total_price=Sum('price')).order_by('order')

        for call in call_logs:
            if call.total_price:
                total_call_charges += float(call.total_price)
                orders_set.add(call.order)

        order_list = list(orders_set)
        return order_list, total_call_charges

    def get_orders_by_aggregate_level(self, aggregate_level):
        """
        Returns grouped order_sets by an aggregate level.
        `aggregate_level` takes any of the values `city`, `hub`, `vehicle_class`\
        `driver`, `vehicle_number`.

        input : city
        response : [{"value": "Bangalore", "order_set": <OrderSet instance_1>},
                    {"value": "Chennai", "order_set": <OrderSet instance_2>}]
        """

        aggregate_level = camel_case_to_snake_case(aggregate_level)

        field_name = {
            "city": "city",
            "hub": "hub",
            "vehicle_class": "vehicle_class_preference",
            "driver": "invoice_driver",
            "customer" : "customer"
        }.get(aggregate_level, None)

        distinct_values = self.orders.filter(
            **{"%s__isnull" % field_name: False}
        ).values_list(field_name, flat=True).distinct()

        self.order_set_buckets = []
        for value in distinct_values:
            self.order_set_buckets.append({
                "aggregate_value": value,
                "order_set": OrderSet(
                    orders=self.orders.filter(**{field_name: value}).only(*ORDER_INVOICING_FIELDS),
                    orders_applicable=self.orders_applicable,
                    bucket_type=field_name,
                    bucket_value=value
                )
            })

        return self.order_set_buckets

    def get_daywise_shipmentvalue(self, slab, start, end):
        """
        Returns the daywise shipment count for given attribute (status).
        `attribute` takes any of `SHIPMENT_SERVICE_ATTRIBUTES` defined in
        contract-constants.
        * Field mapping (`STATUS_ORDER_EVENT_FIELD_MAPPING`) is expected to be
        filled for any new statuses.
        """

        attribute = slab.attribute
        pincodes = slab.pincodes if hasattr(slab, 'pincodes') else None
        pincodes_list = []
        if pincodes:
            pincodes_list = pincodes.split(",")

        shipment_count_date_wise = None
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED:
            # `Delivered` count can be fetched directly from order.
            if pincodes_list:
                orders_to_be_considered = self.orders.filter(
                    status__in=[ORDER_DELIVERED, DELIVERY_ACK],
                    shipping_address__postcode__in=pincodes_list,
                    event__status=ORDER_DELIVERED,
                    event__time__range=(start, end)).only(*ORDER_INVOICING_FIELDS).distinct()
            else:
                orders_to_be_considered = self.orders.filter(
                    status__in=[ORDER_DELIVERED, DELIVERY_ACK],
                    event__status__in=[ORDER_DELIVERED, DELIVERY_ACK],
                    event__time__range=(start, end)).only(*ORDER_INVOICING_FIELDS).distinct()
                # self.order is considering the order which is not part of this invoice
                # as we use event to get the order , so making the bucket to have
                # the order of the current invoice
                orders_to_be_considered = orders_to_be_considered.filter(id__in=self.orders_applicable).only(*ORDER_INVOICING_FIELDS)

            # sku based order are excluded for calculating delivered count
            # and it will be taken care outside this function
            order_accounted = OrderLine.objects.filter(order__in=orders_to_be_considered,
                                                       sku_id__in=slab.sku_configured).values_list('order_id',
                                                                                                    flat=True)

            orders_to_be_considered = orders_to_be_considered.exclude(
                id__in=order_accounted).only(*ORDER_INVOICING_FIELDS)

            if not pincodes_list:
                # pincode is given priority for considering delivered orders
                # if the order shipping pincode is defined in slabs then it will be invoiced
                # based on pincode, the assumption is that same order cannot be invoiced using pincode slab
                # for attempted and shipment slab for delivered
                orders_to_be_considered = orders_to_be_considered.exclude(
                    shipping_address__postcode__in=slab.pin_codes_list).only(*ORDER_INVOICING_FIELDS)

            # self.considered_order_ids = orders_to_be_considered.values_list('id', flat=True)
            shipment_count_date_wise = list(orders_to_be_considered.extra(
                {'pickup_date': "date(pickup_datetime)"}).values('pickup_date').annotate(
                pickup_count=Count('id')).values_list('pickup_date', 'pickup_count'))
        elif attribute in STATUS_ORDER_EVENT_FIELD_MAPPING.keys():
            # Any other status other than `Delivered` is expected to be retrieved from orders `Event`.
            applicable_statuses = STATUS_ORDER_EVENT_FIELD_MAPPING.get(attribute, None)
            if applicable_statuses:
                events_considered = Event.objects.filter(order__in=self.orders,
                                                         status__in=applicable_statuses,
                                                         time__range=(
                                                             start, end))
                if pincodes_list:
                    events_considered = events_considered.filter(order__shipping_address__postcode__in=pincodes_list)

                # self.considered_order_ids = events_considered.values_list('order_id', flat=True).distinct('order_id')
                shipment_count_date_wise = list(events_considered.extra(
                    {'date': "date(time)"}).values('date').annotate(count=Count('id')).values_list('date', 'count'))

        return shipment_count_date_wise

    def get_orderwise_shipmentvalue(self, slab, start, end):
        attribute = slab.attribute
        pincodes = slab.pincodes if hasattr(slab, 'pincodes') else None
        pincodes_list = []
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)

        if pincodes:
            pincodes_list = pincodes.split(",")

        shipment_count_date_wise = None
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED:
            # `Delivered` count can be fetched directly from order.
            if pincodes_list:
                orders_to_be_considered = orders.filter(
                    status__in=[ORDER_DELIVERED, DELIVERY_ACK],
                    shipping_address__postcode__in=pincodes_list,
                    event__status=ORDER_DELIVERED,
                    event__time__range=(start, end)).only(*ORDER_INVOICING_FIELDS).distinct()
            else:
                orders_to_be_considered = orders.filter(
                    status__in=[ORDER_DELIVERED, DELIVERY_ACK],
                    event__status__in=[ORDER_DELIVERED, DELIVERY_ACK],
                    event__time__range=(start, end)).only(*ORDER_INVOICING_FIELDS).distinct()
                orders_to_be_considered = orders_to_be_considered.filter(id__in=self.orders_applicable).only(*ORDER_INVOICING_FIELDS)

            order_accounted = OrderLine.objects.filter(order__in=orders_to_be_considered,
                                                       sku_id__in=slab.sku_configured).values_list('order_id',
                                                                                                    flat=True)

            orders_to_be_considered = orders_to_be_considered.exclude(
                id__in=order_accounted).only(*ORDER_INVOICING_FIELDS)

            if not pincodes_list:
                orders_to_be_considered = orders_to_be_considered.exclude(
                    shipping_address__postcode__in=slab.pin_codes_list).only(*ORDER_INVOICING_FIELDS)

            shipment_count_date_wise = list(orders_to_be_considered.annotate(
                count=Count('id')).values_list('pickup_datetime', 'count', 'id'))
        elif attribute in STATUS_ORDER_EVENT_FIELD_MAPPING.keys():
            # Any other status other than `Delivered` is expected to be retrieved from orders `Event`.
            applicable_statuses = STATUS_ORDER_EVENT_FIELD_MAPPING.get(attribute, None)
            if applicable_statuses:
                events_considered = Event.objects.filter(order__in=self.orders,
                                                         status__in=applicable_statuses,
                                                         time__range=(start, end))
                if pincodes_list:
                    events_considered = events_considered.filter(order__shipping_address__postcode__in=pincodes_list)

                shipment_count_date_wise = list(events_considered.annotate(count=Count('id')).values_list('time', 'count', 'id'))

        return shipment_count_date_wise

    def _get_daywise_orders(self, orders):
        daywise_orders = collections.defaultdict(set)

        for order in orders:
            daywise_orders[order.date_placed.date()].add(order)
        return daywise_orders

    def get_daywise_distances(self, date_start, date_end, aggregate_level):
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        trips_qp = {
            "stops__order__in" : orders,
            "planned_start_time__gte" : date_start,
            "planned_start_time__lte" : date_end
        }

        if aggregate_level==DRIVER:
            trips_qp["invoice_driver"] = orders[0].invoice_driver

        trips = Trip.objects.filter(**trips_qp).annotate(
                                    planned_start_time_ist=self.expression_planned_start_time_ist).distinct()

        order_distances = []
        daywise_trips = TripSet._get_daywise_trips_v2(trips)

        for day, trips in daywise_trips.items():
            total_distance_for_the_day = math.ceil(
                        sum([trip.total_distance for trip in trips if trip.total_distance])
                    )
            order_distances.append((day, total_distance_for_the_day))

        return order_distances

    def get_daywise_times(self, date_start, date_end, aggregate_level, hours=True):
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        trips_qp = {
            "stops__order__in" : orders,
            "planned_start_time__gte" : date_start,
            "planned_start_time__lte" : date_end
        }

        if aggregate_level==DRIVER:
            trips_qp["invoice_driver"] = orders[0].invoice_driver

        trips = Trip.objects.filter(**trips_qp).annotate(
                                    planned_start_time_ist=self.expression_planned_start_time_ist).distinct()

        order_times = []
        daywise_trips = TripSet._get_daywise_trips_v2(trips)

        for day, trips in daywise_trips.items():
            total_time_for_the_day = 0
            for trip in trips:
                if not hours:
                    total_time_for_the_day += math.ceil(trip.total_time)
                else:
                    total_time_for_the_day += math.ceil(trip.total_time/60)
            order_times.append((day, total_time_for_the_day))
        return order_times

    def _get_order_trips(self, order):
        trip = None
        trip_count = 0

        if order:
            stop = order.stops.first()
            if hasattr(stop, 'trip'):
                stop_trip = stop.trip
                if stop_trip and \
                    stop_trip.status == StatusPipeline.TRIP_COMPLETED:
                    trip = stop_trip
                    trip_count = Stop.objects.filter(trip=trip,
                                        order__status__in=[ORDER_DELIVERED, DELIVERY_ACK]
                                        ).count()

        return trip,trip_count

    def get_orderwise_distances(self, date_start, date_end, aggregate_level):
        order_distances = []
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        trips_qp = {
            "stops__order__in" : orders,
            "planned_start_time__gte" : date_start,
            "planned_start_time__lte" : date_end
        }

        if aggregate_level==DRIVER:
            trips_qp["invoice_driver"] = orders[0].invoice_driver

        trips = Trip.objects.filter(**trips_qp).annotate(
                                    planned_start_time_ist=self.expression_planned_start_time_ist).distinct()

        for trip in trips:
            order_distances.append((trip.planned_start_time, trip.total_distance, trip.trip_number))
        return order_distances

    def get_orderwise_times(self, date_start, date_end, aggregate_level, hours=True):
        order_times = []
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        trips_qp = {
            "stops__order__in" : orders,
            "planned_start_time__gte" : date_start,
            "planned_start_time__lte" : date_end
        }

        if aggregate_level==DRIVER:
            trips_qp["invoice_driver"] = orders[0].invoice_driver

        trips = Trip.objects.filter(**trips_qp).annotate(
                                    planned_start_time_ist=self.expression_planned_start_time_ist).distinct()

        for trip in trips:
            if hours:
                trip.total_distance = trip.total_distance / 60
            order_times.append((trip.planned_start_time, trip.total_distance, trip.trip_number))
        return order_times

    def get_daywise_weights(self):
        order_weights = []
        daywise_orders = self._get_daywise_orders(self.orders.filter(status__in=[ORDER_DELIVERED, DELIVERY_ACK]))

        for day, orders in daywise_orders.items():
            total_weight_for_the_day = math.ceil(
                sum([order.weight for order in orders if order.weight])
            )
            order_weights.append((day, total_weight_for_the_day))
        return order_weights

    def get_orderwise_weights(self):
        order_weights = []
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        for order in orders:
            order_weights.append((order.pickup_datetime, order.weight, order.number))
        return order_weights

    def get_orderwise_volumes(self):
        order_volumes = []
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        for order in orders:
            order_volumes.append((order.pickup_datetime, order.volume, order.number))
        return order_volumes

    def get_orderwise_grams(self):
        order_grams = []
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        for order in orders:
            order_grams.append((order.pickup_datetime, order.weight*1000, order.number))
        return order_grams

    def get_daywise_volumes(self):
        order_distances = []
        daywise_orders = self._get_daywise_orders(self.orders.filter(status__in=[ORDER_DELIVERED, DELIVERY_ACK]))

        for day, orders in daywise_orders.items():
            total_distance_for_the_day = math.ceil(
                sum([order.volume for order in orders if order.volume])
            )
            order_distances.append((day, total_distance_for_the_day))
        return order_distances

    def get_daywise_grams(self):
        order_distances = []
        daywise_orders = self._get_daywise_orders(self.orders.filter(status__in=[ORDER_DELIVERED, DELIVERY_ACK]))

        for day, orders in daywise_orders.items():
            total_distance_for_the_day = math.ceil(
                sum([order.weight*1000 for order in orders if order.weight])
            )
            order_distances.append((day, total_distance_for_the_day))
        return order_distances


    def get_daywise_sku(self, slab, date_start, date_end):
        order_skus = []
        daywise_orders = self._get_daywise_orders(
            self.orders.filter(status__in=[ORDER_DELIVERED, DELIVERY_ACK]))
        attribute = SHIPMENT_ATTR_TO_ORDERLINE_STATUS_MAPPING.get(
            slab.attribute)
        whsku = WhSku.objects.filter(name=slab.sku.name).first() if slab.sku else None
        query = Q(sku=slab.sku)
        if whsku:
            query |= Q(wh_sku=whsku)

        if attribute:
            for day, orders in daywise_orders.items():
                order_line_sum = OrderLine.objects.filter(
                    query, order__in=orders,
                    order__event__status=slab.attribute,
                    order__event__created_date__range=(date_start, date_end)
                ).distinct().aggregate(delivered_sum=Sum(attribute))
                _sum = order_line_sum.get('delivered_sum', 0) \
                    if order_line_sum.get('delivered_sum', 0) else 0
                order_skus.append((day, _sum))
        return order_skus

    def get_orderwise_sku(self, slab, date_start, date_end):
        order_skus = []
        # daywise_orders = self._get_orderwise_orders(
        #     self.orders.filter(status__in=[ORDER_DELIVERED, DELIVERY_ACK]))
        attribute = SHIPMENT_ATTR_TO_ORDERLINE_STATUS_MAPPING.get(
            slab.attribute)
        whsku = WhSku.objects.filter(name=slab.sku.name).first() if slab.sku else None
        query = Q(sku=slab.sku)
        if whsku:
            query |= Q(wh_sku=whsku)

        if attribute:
            orders = self.orders.only(*ORDER_INVOICING_FIELDS)

            for order in orders:
                order_line_sum = OrderLine.objects.filter(
                    query, order=order,
                    order__event__status=slab.attribute,
                    order__event__created_date__range=(date_start, date_end)
                ).distinct().aggregate(delivered_sum=Sum(attribute))
                _sum = order_line_sum.get('delivered_sum', 0) \
                    if order_line_sum.get('delivered_sum', 0) else 0
                order_skus.append((order.pickup_datetime, _sum, 1))
        return order_skus

    def get_day_wise_sms_count(self):
        order_sms_count_by_day = []
        daywise_orders = self._get_daywise_orders(self.orders.filter(status__in=[ORDER_DELIVERED, DELIVERY_ACK]))

        for day, orders in daywise_orders.items():
            total_sms_for_the_day = OrderMessage.objects.filter(order__in=orders).count()
            order_sms_count_by_day.append((day, total_sms_for_the_day))
        return order_sms_count_by_day

    def get_day_wise_cash_collected(self):
        total_cash_collected = []
        daywise_orders = self._get_daywise_orders(self.orders.filter(status__in=[ORDER_DELIVERED, DELIVERY_ACK]))

        for day, orders in daywise_orders.items():
            cash_collected = sum([order.cash_collected for order in orders if order.cash_collected])
            total_cash_collected.append((day, cash_collected))
        return total_cash_collected

    def get_orderwise_sms_count(self):
        sms_count = []
        order_sms_mapping = dict(OrderMessage.objects.filter(order__in=self.orders).annotate(
            order_sms=Count('order_id')).values_list('order_id', 'order_sms'))

        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        for order in orders:
            sms_count.append((order.pickup_datetime, order_sms_mapping.get(order.id) or 0 ,order))
        return sms_count

    def get_orderwise_cash_collected(self):
        total_cash_collected = []
        #looks like we need to filter everywhere if we use only
        orders = self.orders.only(*ORDER_INVOICING_FIELDS)
        for order in orders:
            total_cash_collected.append((order.pickup_datetime, order.cash_collected or 0, order.number))
        return total_cash_collected
