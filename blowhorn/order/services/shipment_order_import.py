# Django and System libraries
import json
import itertools
import traceback
import logging
import math
import pytz
import re
from operator import itemgetter
from random import randint
from collections import Counter
import phonenumbers
from phonenumber_field.phonenumber import PhoneNumber

from django.db import transaction
from django.utils import timezone
from openpyxl import load_workbook
from django.conf import settings
from datetime import timedelta,datetime
from django.utils.translation import ugettext_lazy as _

# 3rd party libraries
from rest_framework import status, exceptions
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.common.import_file_mixin import ImportFileMixin
from blowhorn.customer.constants import (SHIPMENT_DATA_TO_BE_IMPORTED,
    ORDERLINE_LABEL_NAME_MAPPING, ORDERLINE_MANDATORY_HEADERS,
    ORDERLINE_MANDATORY_VALUES, ORDERLINE_IMPORT_BATCH_DESCRIPTION,
    SHIPMENT_ADMIN_IMPORT, SHIPMENT_CUSTOMER_IMPORT, COMPANY_ID,
    SHIPMENT_NOT_MANDATORY_LABEL)
from blowhorn.order.const import RESIDENTIAL_ADDRESS, COMMERCIAL_ADDRESS, CARRIER_MODE
from blowhorn.common.helper import CommonHelper
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.order.utils import OrderNumberGenerator
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_HYPERLOCAL,\
    CONTRACT_TYPE_SHIPMENT
from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED, DISCOUNT_TYPE_PERCENTAGE

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
OrderLine = get_model('order', 'OrderLine')
OrderLineHistoryBatch = get_model('order', 'OrderLineHistoryBatch')
OrderLineHistory = get_model('order', 'OrderLineHistory')
ShippingAddress = get_model('order', 'ShippingAddress')
OrderBatch = get_model('order', 'OrderBatch')
Hub = get_model('address', 'Hub')
Country = get_model('address', 'Country')
Customer = get_model('customer', 'Customer')
CustomerDivision = get_model('customer', 'CustomerDivision')
Contract = get_model('contract', 'Contract')
SKUCustomerMap = get_model('customer', 'SKUCustomerMap')
SKU = get_model('customer', 'SKU')
WhSku = get_model('wms', 'WhSku')


class ShipmentOrderImport(object):

    def _initialize_attributes(self, data):
        '''
            This method intializes the variables and perform basic prechecks
            on input file like file name, customer id and empty file check.
            Also does the row processing of the excel sheet where each row
            is equivalent to one order.
        '''
        rowwise_flag = data.get('is_rowwise',False)
        self.file_name = data.get('shipment_orders')
        self.response = {'data': '', 'errors': ''}
        self.status_code = status.HTTP_200_OK
        self.is_rowwise = rowwise_flag in ['true',True]
        self.values_list = []
        self.records_with_duplicate_header = []
        self.contract_avg_basket_price = {}

        # Load the workbook and intial validation
        error_message = self._initial_validations()
        if error_message:
            self.response['errors'] = error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        # Initialize error lists for further processing
        self._initialize_error_list()

    def _initial_validations(self):
        error_message = None
        # Process further only if file name present
        if not self.file_name:
            error_message = 'No File Passed'
            return error_message

        # Load the workbook
        try:
            wb_obj = load_workbook(filename=self.file_name, data_only=True)
        except Exception as e:
            error_message = 'The file may be corrupted or not in xlsx format.' \
                            ' Please download a new sample sheet.'
            return error_message

        sheet_obj = wb_obj.active
        headers = [cell.value for cell in sheet_obj[1]]
        self.no_of_rows = sheet_obj.max_row

        self.source = SHIPMENT_ADMIN_IMPORT
        if not self.request.user.is_staff:
            self.source = SHIPMENT_CUSTOMER_IMPORT

        # Validate the format
        error_message = self._validate_sku_format(headers)
        if error_message:
            return error_message

        self.customer = None
        self.customer_id = None
        if self.request.user.is_staff:
            is_valid_customer_id= self._get_customer_id(sheet_obj, headers)
            if not is_valid_customer_id:
                error_message = 'Please provide a file having data with valid customer id'
                return error_message
        else:
            customer = CustomerMixin().get_customer(self.request.user)
            if customer:
                self.customer = customer
                self.customer_id = customer.id
            else:
                error_message = 'No Customer Found'
                return error_message

        # Process the rows for order data and throw error if empty file
        row_error = self._row_processing(sheet_obj, headers)
        if not self.values_list:
            error_message = 'File is Empty'
            return error_message

        if len(row_error) != 0 and self.is_rowwise:
            self.values_list = []
            ls = ','.join(row_error)
            error_message = 'Invalid format for SKU Details column for rows ' \
                                        + ls + ' :: {"Item1" : quantity, "Item2" : quantity ,...}'
            return error_message

        if self.records_with_duplicate_header:
            self.values_list = []
            ls = ','.join(self.records_with_duplicate_header)
            error_message = "Sku name is same as header for the rows: %s" % ls

        return error_message

    def _validate_sku_format(self, headers):
        '''
            To validate the row/column wise format of the excel sheet
        '''
        success = False
        error_message = None
        required_headers = {shipment_data['name'] for shipment_data in
                                    SHIPMENT_DATA_TO_BE_IMPORTED}

        if self.source == SHIPMENT_CUSTOMER_IMPORT:
            SHIPMENT_NOT_MANDATORY_LABEL.append(COMPANY_ID)

        [required_headers.remove(x) for x in SHIPMENT_NOT_MANDATORY_LABEL if x in required_headers]
        required_headers_present = required_headers.issubset(set(headers))

        if not required_headers_present:
            error_message = 'Please download a new sample file as mandatory fields are missing' \
                        ' or new fields have been added/updated'
        else:
            if self.is_rowwise:
                if 'SKU Details' in headers:
                    success = True
            else:
                if 'SKU Details' not in headers:
                    success = True

            if not success:
                error_message = 'Please upload valid file with headers as per the selected SKU orientation'

        if success:
            column_counts = Counter(headers)
            duplicate_columns = [x for x,y in column_counts.items() if y > 1 and x != None]
            if duplicate_columns:
                ls = ','.join(duplicate_columns)
                error_message = 'Please remove the duplicate columns ' + ls

        return error_message

    def _load_and_check_json(self,sku_data):
        '''
            To format and validate the sku details column for row based sku
        '''
        json_sku = {}
        sku_data = str(sku_data)

        if not re.match('^{.*}$',sku_data):
            return json_sku

        left_double_quote = '\u201c'
        right_double_quote = '\u201d'

        if left_double_quote in sku_data:
            sku_data = sku_data.replace(left_double_quote, '"')

        if right_double_quote in sku_data:
            sku_data = sku_data.replace(right_double_quote, '"')

        if "'" in sku_data:
            sku_data = sku_data.replace("'", '"')

        try:
            json_sku = json.loads(sku_data)
        except Exception as e:
            pass

        return json_sku

    def _row_processing(self, sheet_obj, headers):
        '''
            This method does the enrichment of order data
            fetched from excel sheet based on row wise or
            column wise input.
            Also SKUs not present are created in the system.
        '''
        self.order_line_list = []
        row_error = []
        required_headers = [shipment_data['name'] for shipment_data in
                                    SHIPMENT_DATA_TO_BE_IMPORTED]
        required_headers = required_headers + ['Sl No.']
        total_headers = required_headers + ['Sl No.', 'SKU Details']
        index = 0

        for row in sheet_obj.iter_rows(min_row=2):
            order_data_dict = {}
            order_data_dict['no_of_order_lines'] = 0
            value_count = 0

            index += 1
            for key, cell in zip(headers, row):
                sku_data = cell.value
                if sku_data:
                    value_count = value_count + 1

                if key in required_headers:
                    order_data_dict[key] = sku_data

                else:
                    if not key or not sku_data or len(str(sku_data)) == 0:
                        continue

                    if self.is_rowwise:
                        if key != 'SKU Details':
                            continue
                        # Process the sku data in json format
                        json_sku = self._load_and_check_json(sku_data)

                        if len(json_sku) == 0:
                            row_error.append(str(cell.row))

                        for sku,value in json_sku.items():
                            if value == 0:
                                continue

                            if sku in total_headers:
                                self.records_with_duplicate_header.append(str(index))

                            # Get or create the sku's
                            self._order_sku_getorcreate(sku, value)
                            order_data_dict['no_of_order_lines'] += 1

                    else:
                        # Column wise sku processing
                        self._order_sku_getorcreate(key, sku_data)
                        order_data_dict['no_of_order_lines'] += 1


            if value_count > 3:
                self.values_list.append(order_data_dict)

        return row_error

    def _get_customer_id(self,sheet_obj, headers):
        '''
            This method returns the customer id from the data
            and stops row processing once found.
        '''
        temp_id = None
        is_valid_customer_id = False
        for row in sheet_obj.iter_rows(min_row=2):
            for key, cell in zip(headers, row):
                if key == COMPANY_ID:
                    temp_id = cell.value
                    break
            if temp_id:
                break

        if temp_id:
            self.customer_id = temp_id
            self.customer = Customer.objects.filter(pk=temp_id).first()

        is_valid_customer_id = True if self.customer else False
        # self.skus_dict = self.get_skus_for_the_customer(self.customer_id)

        return is_valid_customer_id

    def _initialize_error_list(self):
        '''
            This method initializes the intance variables
            for records with errors required in later processsing
        '''
        self.row_index = 1
        self.records_with_errors = set()
        self.records_without_customer = []
        self.records_without_hub = []
        self.records_with_invalid_pickup_postcode = []
        self.records_with_invalid_delivery_postcode = []
        self.records_without_pickup_address= []
        self.records_without_delivery_address= []
        self.records_with_invalid_phone = []
        self.records_with_invalid_alternate_phone = []
        self.records_with_duplicate_reference = []
        self.records_with_duplicate_awb = []
        self.records_without_undefined_pincode_contract = []
        self.records_without_contract = []
        self.records_with_invalid_expected_datetime = []
        self.records_with_past_expected_datetime = []
        self.records_with_weight_volume_negative = []
        self.records_with_invalid_priority = []
        self.records_with_invalid_latlong = []
        self.records_with_invalid_awb = []
        self.records_with_invalid_address_type_flag = []
        self.records_with_invalid_return_order_flag = []
        self.records_with_invalid_hub = set()
        self.records_with_invalid_division = []
        self.order_data_list = []
        self.records_with_undefined_hyperlocal_contract = []
        self.records_with_invalid_hyperlocal_flag = []
        self.shipping_address_list = []
        self.pickup_address_list = []
        self.event_line_list = []
        self.records_with_invalid_carrier_mode = []

        country_code = settings.COUNTRY_CODE_A2
        self.country = Country.objects.get(pk=country_code)

    def _order_sku_getorcreate(self, sku, value):
        '''
            This method gets or creates the sku and sku customer map.
            Adds the sku id and quantity to instance var order_line_list
        '''

        sku_ob, success = SKU.objects.get_or_create(name=sku)
        sku_cust_map, success = SKUCustomerMap.objects.get_or_create(sku=sku_ob,
                                                                     customer=self.customer)

        success = True
        wh_sku_ob = WhSku.objects.filter(name=sku, client__customer=self.customer).first()
        if not wh_sku_ob:
            pack_config = self.customer.packconfig_set.first()
            client = self.customer.client_set.first()
            supplier = self.customer.supplier_set.first()

            if not pack_config:
                from blowhorn.wms.models import WmsConstants
                from blowhorn.wms.submodels.sku import PackConfig
                default_pack_config_id = WmsConstants().get('DEFAULT_PACK_CONFIG_ID', 13)
                pack_config = PackConfig.objects.filter(id=int(default_pack_config_id)).first()
            if not client:
                from blowhorn.wms.submodels.location import Client
                name = 'Default Client - %s' % self.customer.name
                client = Client.objects.create(name=name, customer=self.customer)
            if not supplier:
                from blowhorn.wms.submodels.inbound import Supplier
                name = 'Default Supplier - %s' % self.customer.name
                supplier = Supplier.objects.create(name=name, customer=self.customer)

            wh_sku_ob = WhSku.objects.create(name=sku,
                                             pack_config=pack_config,
                                             client=client,
                                             supplier=supplier,
                                             )

        if not wh_sku_ob:
            success = False

        # sku_cust_map, success = SKUCustomerMap.objects.get_or_create(sku=sku_ob,
        #                                     customer=self.customer)
        self.order_line_list.append(
            {
                'sku_id': sku_ob.id,
                'wh_sku_id': wh_sku_ob.id,
                'quantity': value,
            }
        )

        return success

    def get_pincode_wise_hub_details(self, customer):
        pincode_wise_hub = {}
        self.name_wise_hub = {}
        customer_hubs = Hub.objects.filter(customer=customer)
        hubs_without_customer = Hub.objects.filter(customer__isnull=True)

        for hub in (customer_hubs | hubs_without_customer).values(
                'pincodes', 'id', 'address__geopoint', 'city', 'name',
                'address__line1', 'address__postcode'):
            if not hub['pincodes']:
                hub_details = {}
                hub_details['id'] = hub['id']
                hub_details['name'] = hub['name']
                hub_details['geopoint'] = hub['address__geopoint']
                hub_details['city_id'] = hub['city']
                hub_details['line1'] = hub['address__line1']
                hub_details['postcode'] = hub['address__postcode']
                # pincode_wise_hub[str(pincode)] = hub_details
                self.name_wise_hub[hub['name'].lower()] = hub_details

            else:
                for pincode in hub['pincodes']:
                    hub_details = {}
                    hub_details['id'] = hub['id']
                    hub_details['name'] = hub['name']
                    hub_details['geopoint'] = hub['address__geopoint']
                    hub_details['city_id'] = hub['city']
                    hub_details['line1'] = hub['address__line1']
                    hub_details['postcode'] = hub['address__postcode']
                    pincode_wise_hub[str(pincode)] = hub_details
                    self.name_wise_hub[hub['name'].lower()] = hub_details

        return pincode_wise_hub

    def intstring_empty_or_negative_check(self, data):
        is_valid = False
        data = str(data) or None
        if data and re.match('^([+])*\d+(\.)*\d*$',data):
            is_valid,data = self.input_float_validation(data)

        if is_valid:
            data = int(data)
        else:
            data = None

        return is_valid,data

    def _check_for_division(self, division_name):
        division = None

        if division_name:
            division_name = str(division_name).strip().lower()

            for rid,name in self.customer_divisions:
                if name.lower() == division_name:
                    division = rid
                    break

            if not division:
                self.records_with_invalid_division.append(self.row_index)

        return division

    def yesno_flag_processing(self, field_name, error_list, val):
        flag_value = val.get(field_name) or None
        flag = False

        if flag_value:
            if type(flag_value) in [int, float]:
                error_list.append(self.row_index)
            elif flag_value.lower() in ['yes','y']:
                flag = True
            elif flag_value.lower() in ['no','n']:
                flag = False
            else:
                error_list.append(self.row_index)
        else:
            flag = False

        return flag

    def validate_mobile_number(self, phone_number, error_list):
        _phone = None
        _, _phone_number = self.input_float_validation(phone_number)
        try:
            _phone = PhoneNumber.from_string(
                str(_phone_number), settings.COUNTRY_CODE_A2)
            if not _phone.is_valid():
                error_list.append(self.row_index)
        except phonenumbers.NumberParseException:
            error_list.append(self.row_index)

        return _phone

    def __parse_params(self, data, pincode_wise_hub):
        fail = False
        order_numbers_list = []
        reference_number_dict = {}
        reference_number_list = []
        awb_number_list = []
        awb_number_dict = {}
        lat_long_regex = "^[+-]*\d+(\.)*(\d+)*$"
        awb_number_regex = "^[-,_\w]*$"

        for _, val in enumerate(data):
            delivery_hub_id = None
            pickup_hub_id = None
            delivery_geopoint = None
            pickup_geopoint = None
            delivery_postcode = None
            pickup_postcode = None
            self.row_index += 1
            shipment_info = {}
            order_data = {}
            pickup_hub = {}

            # Pincode check for delivery and pickup
            delivery_postcode = val.get('Delivery Pincode')
            is_delivery_postcode_valid,delivery_postcode = \
                                self.input_float_validation(delivery_postcode)

            pickup_postcode = val.get('Pickup Pincode')
            is_pickup_postcode_valid = False
            if pickup_postcode:
                is_pickup_postcode_valid,pickup_postcode = \
                                    self.input_float_validation(pickup_postcode)

            if pickup_postcode and not is_pickup_postcode_valid:
                self.records_with_invalid_pickup_postcode.append(self.row_index)
                self.records_with_errors.add(self.row_index)
                fail = True

            if not is_delivery_postcode_valid:
                self.records_with_invalid_delivery_postcode.append(self.row_index)
                self.records_with_errors.add(self.row_index)
                fail = True

            # Hub Name
            _pickup_hubname = val.get('Pickup Hub') or None
            pickup_hub_name = val.get('Hub') or _pickup_hubname
            if pickup_hub_name:
                pickup_hub_name = str(pickup_hub_name).strip().lower()
                pickup_hub = self.name_wise_hub.get(pickup_hub_name, {})
                if not pickup_hub:
                    fail = True
                    self.records_with_invalid_hub.add(self.row_index)

            # Reference and Customer Reference Number
            reference_number = val.get('Reference Number') or None
            customer_reference_number = val.get('Customer Reference Number') or None

            awb_number = val.get('AWB Number') or None
            if not re.match(awb_number_regex, str(awb_number)):
                self.records_with_invalid_awb.append(self.row_index)

            if reference_number:
                _,reference_number = self.input_float_validation(reference_number)

            if customer_reference_number:
                _,customer_reference_number = self.input_float_validation(customer_reference_number)

            # Division
            division_name = val.get('Division') or None
            division = self._check_for_division(division_name)

            # Priority
            priority = val.get('Priority', None) or None
            if priority:
                is_valid,priority = self.intstring_empty_or_negative_check(priority)
                if not is_valid:
                    self.records_with_invalid_priority.append(self.row_index)


            # carrier-mode
            carrier_mode = val.get('Carrier Mode', None) or None
            if carrier_mode and carrier_mode not in CARRIER_MODE:
                 self.records_with_invalid_carrier_mode.append(self.row_index)

            # Phone number
            pickup_phone = None
            delivery_phone = None
            alternate_delivery_phone = None
            pickup_phone_number = val.get('Pickup Customer Phone', None) or None
            delivery_phone_number = val.get('Delivery Customer Phone', None) or None
            alternate_delivery_phone_number = val.get('Alternate Delivery Customer Phone', None) or None

            if pickup_phone_number:
                pickup_phone = self.validate_mobile_number(pickup_phone_number, self.records_with_invalid_phone)

            if delivery_phone_number:
                delivery_phone = self.validate_mobile_number(delivery_phone_number, self.records_with_invalid_phone)

            if alternate_delivery_phone_number:
                alternate_delivery_phone = self.validate_mobile_number(alternate_delivery_phone_number,
                                                self.records_with_invalid_alternate_phone)

            if not delivery_phone:
                self.records_with_invalid_phone.append(self.row_index)

            # Return order flag
            return_order = self.yesno_flag_processing('Is Return Order (Y/N)',
                                    self.records_with_invalid_return_order_flag,
                                    val)

            # Expected delivery time
            curr_tz = pytz.timezone(settings.ACT_TIME_ZONE)
            delivery_time = None
            try:
                expected_delivery_time = val.get('Expected Delivery DateTime YYYY-MM-DD HH24:MI') or None
                if expected_delivery_time:
                    if type(expected_delivery_time) == str:
                        dt = curr_tz.localize(datetime.strptime(expected_delivery_time, '%Y-%m-%d %H:%M'))
                    elif type(expected_delivery_time) == datetime:
                        dt = curr_tz.localize(expected_delivery_time)
                    else:
                        raise ValueError

                    delivery_time = dt.astimezone(pytz.utc)
                    if delivery_time <= datetime.now(pytz.utc):
                        self.records_with_past_expected_datetime.append(self.row_index)
                        delivery_time = None

            except ValueError as ve:
                self.records_with_invalid_expected_datetime.append(self.row_index)
                delivery_time = None

            # Pickup time
            pickup_time = None
            try:
                pickup_datetime = val.get('Pickup DateTime YYYY-MM-DD HH24:MI') or None
                if pickup_datetime:
                    if type(pickup_datetime) == str:
                        dt = curr_tz.localize(datetime.strptime(pickup_datetime, '%Y-%m-%d %H:%M'))
                    elif type(pickup_datetime) == datetime:
                        dt = curr_tz.localize(pickup_datetime)
                    else:
                        raise ValueError

                    pickup_time = dt.astimezone(pytz.utc)
                    if pickup_time <= datetime.now(pytz.utc):
                        self.records_with_past_expected_datetime.append(self.row_index)
                        pickup_time = None
                else:
                    pickup_time = timezone.now()

            except ValueError as ve:
                self.records_with_invalid_expected_datetime.append(self.row_index)
                pickup_time = None

            # Reference number
            if reference_number:
                if reference_number in reference_number_list:
                    self.records_with_duplicate_reference.append(self.row_index)
                else:
                    reference_number_list.append(reference_number)
                    reference_number_dict[reference_number] = self.row_index

            if awb_number:
                if awb_number in awb_number_list:
                    self.records_with_duplicate_awb.append(self.row_index)
                else:
                    awb_number_list.append(awb_number)
                    awb_number_dict[awb_number] = self.row_index

            # Is Commercial Address Flag
            is_commercial_addr = self.yesno_flag_processing('Is Commercial Address (Y/N)',
                                    self.records_with_invalid_address_type_flag,
                                    val)

            if self.source != SHIPMENT_CUSTOMER_IMPORT:
                if val.get(COMPANY_ID) != self.customer_id:
                    self.records_without_customer.append(self.row_index)
                    fail = True

            # Customer name and email for pickup & delivery
            pickup_customer_name = val.get('Pickup Customer Name')
            delivery_customer_name = val.get('Delivery Customer Name')
            if delivery_customer_name:
                shipment_info['customer_name'] = delivery_customer_name

            delivery_customer_email = val.get('Delivery Customer Email')
            if delivery_customer_email:
                shipment_info['customer_email'] = delivery_customer_email

            if not awb_number:
                number = self.get_number(order_numbers_list)
            if not fail:
                # Pickup details population
                if pickup_hub:
                    pickup_geopoint = pickup_hub.get('geopoint', None)
                    pickup_address_line1 = pickup_hub.get('line1')
                    pickup_address_postcode = pickup_hub.get('postcode')
                    city_id = pickup_hub.get('city_id')
                    pickup_hub_id = pickup_hub.get('id')

                elif pickup_postcode:
                    pickup_address_line1 = val.get('Pickup Address', None) or None
                    if not pickup_address_line1:
                        self.records_without_pickup_address.append(self.row_index)

                    pickup_address_postcode = str(pickup_postcode)
                    pickup_latitude = val.get('Pickup Latitude', None)
                    pickup_longitude = val.get('Pickup Longitude', None)
                    if pickup_latitude and pickup_longitude:
                        if not re.match(lat_long_regex,str(pickup_latitude)) or not (re.match(lat_long_regex,str(pickup_longitude))):
                            self.records_with_invalid_latlong.append(self.row_index)

                        if self.row_index not in self.records_with_invalid_latlong:
                            pickup_geopoint = CommonHelper.get_geopoint_from_latlong(
                                                latitude=pickup_latitude,
                                                longitude=pickup_longitude)

                    if pickup_postcode in pincode_wise_hub:
                        city_id = pincode_wise_hub[pickup_postcode]['city_id']
                        pickup_hub_id = pincode_wise_hub[pickup_postcode]['id']
                    else:
                        fail = True
                        self.records_without_hub.append(self.row_index)

                else:
                    if delivery_postcode in pincode_wise_hub:
                        pickup_hub_id = pincode_wise_hub[delivery_postcode]['id']
                        city_id = pincode_wise_hub[delivery_postcode]['city_id']
                        pickup_address_line1 = pincode_wise_hub[delivery_postcode]['line1']
                        pickup_address_postcode = pincode_wise_hub[delivery_postcode]['postcode']

                        if pincode_wise_hub[delivery_postcode]['geopoint'].y and \
                            pincode_wise_hub[delivery_postcode]['geopoint'].x:
                            pickup_geopoint = CommonHelper.get_geopoint_from_latlong(
                                latitude=pincode_wise_hub[delivery_postcode]['geopoint'].y,
                                longitude=pincode_wise_hub[delivery_postcode]['geopoint'].x)
                    else:
                        fail = True
                        self.records_without_hub.append(self.row_index)

                # Contract data
                contract_data = {}
                # is_hyperlocal flag addition
                is_hyperlocal = self.yesno_flag_processing('Is HyperLocal Order (Y/N)',
                                        self.records_with_invalid_hyperlocal_flag,
                                        val)

                if not fail:
                    if is_hyperlocal:
                        contract_data = self.hyperlocal_contract_dict.get(city_id, None)
                    else:
                        contract_data = self.contract_dict.get(city_id, None)

                if contract_data:
                    contract_id = contract_data['id']
                    expected_delivery_hours = contract_data.get('expected_delivery_hours', None)
                    if not delivery_time and expected_delivery_hours:
                        eph_timedelta = timedelta(hours=expected_delivery_hours.hour,
                                                    minutes=expected_delivery_hours.minute,
                                                    seconds=expected_delivery_hours.second,
                                                    microseconds=expected_delivery_hours.microsecond)

                        delivery_time = datetime.now() + eph_timedelta

                else:
                    self.records_without_undefined_pincode_contract.append(self.row_index)
                    fail = True

            # Delivery details population
            latitude = val.get('Delivery Latitude', None)
            longitude = val.get('Delivery Longitude', None)

            line1 = val.get('Delivery Address') or None
            if not line1:
                fail = True
                self.records_without_delivery_address.append(self.row_index)

            if not fail:
                if _pickup_hubname:
                    pickup_hub = None

                if pickup_hub:
                    delivery_hub_id = pickup_hub_id
                else:
                    if delivery_postcode in pincode_wise_hub:
                        delivery_hub_id = pincode_wise_hub[delivery_postcode]['id']
                    else:
                        fail = True
                        self.records_without_hub.append(self.row_index)

                delivery_address_line1 = str(line1)
                delivery_address_postcode = str(delivery_postcode)

                if latitude and not re.match(lat_long_regex,str(latitude)):
                    self.records_with_invalid_latlong.append(self.row_index)

                if longitude and not self.records_with_invalid_latlong and \
                        not re.match(lat_long_regex,str(longitude)):
                    self.records_with_invalid_latlong.append(self.row_index)

                if latitude and longitude and not self.records_with_invalid_latlong:
                    delivery_geopoint = CommonHelper.get_geopoint_from_latlong(
                        latitude=latitude,
                        longitude=longitude
                    )

            alternate_pickup_phone = None
            if return_order and alternate_delivery_phone_number:
                alternate_pickup_phone = alternate_delivery_phone

            if not fail:
                self.pickup_address_list.append(
                    ShippingAddress(**{
                        'first_name': pickup_customer_name or '',
                        'phone_number': pickup_phone,
                        'alternate_phone_number': alternate_pickup_phone or pickup_phone,
                        'line1': pickup_address_line1,
                        'postcode': pickup_address_postcode,
                        'geopoint': pickup_geopoint,
                        'country': self.country
                    })
                )

                self.shipping_address_list.append(
                    ShippingAddress(**{
                        'first_name': delivery_customer_name or '',
                        'address_type': COMMERCIAL_ADDRESS if is_commercial_addr else RESIDENTIAL_ADDRESS,
                        'line1': delivery_address_line1,
                        'postcode': delivery_address_postcode,
                        'phone_number': delivery_phone,
                        'alternate_phone_number': alternate_delivery_phone or delivery_phone,
                        'country': self.country,
                        'geopoint': delivery_geopoint
                    })
                )

                # Create otp for the order
                otp_required = contract_data.get('trip_workflow__otp_required', False)
                if otp_required:
                    order_data['otp'] = randint(1001, 9999)

                if awb_number:
                    order_data['number'] = awb_number
                else:
                    order_data['number'] = number

                order_data['expected_delivery_time'] = delivery_time
                order_data['pickup_datetime'] = pickup_time
                order_data['hub_id'] = delivery_hub_id
                order_data['pickup_hub_id'] = pickup_hub_id
                order_data['has_pickup'] = contract_data.get('trip_workflow__pick_orders', False)
                order_data['status'] = StatusPipeline.ORDER_NEW
                order_data['shipment_info'] = shipment_info
                order_data['city_id'] = city_id
                order_data['user'] = self.customer.user
                order_data['customer_id'] = self.customer_id
                order_data['customer_category'] = self.customer.customer_category
                order_data['order_type'] = settings.SHIPMENT_ORDER_TYPE
                order_data['customer_contract_id'] = contract_id
                order_data['reference_number'] = reference_number
                order_data['customer_reference_number'] = customer_reference_number
                order_data['batch_id'] = self.batch.id
                order_data['priority'] = priority
                order_data['division_id'] = division
                order_data['return_order'] = return_order
                order_data['source'] = self.source
                order_data['carrier_mode'] = carrier_mode

                cash_on_delivery = val.get('Cash on Delivery', 0.0) or 0.0
                try:
                    cash_on_delivery = float(cash_on_delivery)
                except ValueError as ve:
                    logger.info('Invalid value: %s' % ve)
                    cash_on_delivery = 0.0

                order_data['is_avg_price'] = False
                order_data['total_incl_tax'] = cash_on_delivery
                basket_value = contract_data['avg_basket_value']
                if cash_on_delivery == 0 and basket_value > 0:
                    order_data['total_incl_tax'] = basket_value
                    order_data['is_avg_price'] = True

                shipping_charges = val.get('Shipping Charges', 0.0) or 0.0
                try:
                    shipping_charges = float(shipping_charges)
                except ValueError as ve:
                    logger.info('Invalid value: %s' % ve)
                    shipping_charges = 0.0

                weight = val.get('Weight (in kg)', 0.0) or 0.0
                try:
                    weight = float(weight)
                except ValueError as ve:
                    logger.info('Invalid value: %s' % ve)
                    weight = 0.0

                volume = val.get('Volume (in cc)', 0.0) or 0.0
                try:
                    volume = float(volume)
                except ValueError as ve:
                    logger.info('Invalid value: %s' % ve)
                    volume = 0.0

                if weight < 0 or volume < 0:
                    self.records_with_weight_volume_negative.append(self.row_index)

                order_data['cash_on_delivery'] = cash_on_delivery
                order_data['shipping_charges'] = shipping_charges
                order_data['weight'] = weight
                order_data['volume'] = volume
                order_data['delivery_instructions'] = val.get('Delivery Instructions','')
                order_data['no_of_order_lines'] = val.get('no_of_order_lines', 0)

                self.order_data_list.append(order_data)

        # Duplicate reference number validation
        if self.customer_id and reference_number_dict:
            ref_list = list(reference_number_dict.keys())
            order_refs = Order.objects.filter(reference_number__in=ref_list,
                            customer=self.customer).values_list(
                            'reference_number', flat=True)

            if order_refs:
                duplicate_refs = [y for x,y in reference_number_dict.items() if (x in order_refs) or
                                    (str(x) in order_refs)]
                self.records_with_duplicate_reference.extend(duplicate_refs)

            if self.records_with_duplicate_reference:
                self.records_with_duplicate_reference.sort()

        if awb_number_dict:
            awb_list = list(awb_number_dict.keys())
            order_numbers = Order.objects.filter(
                number__in=awb_list).values_list('number', flat=True)

            if order_numbers:
                duplicate_awb = [y for x, y in awb_number_dict.items() if
                                 (x in order_numbers) or (
                                         str(x) in order_numbers)]
                self.records_with_duplicate_awb.extend(duplicate_awb)

            if self.records_with_duplicate_awb:
                self.records_with_duplicate_awb.sort()

        return fail

    def get_skus_for_the_customer(self, customer_id):
        '''
            This method gets the sku's id for the customer id
        '''
        skus_dict = {}
        if customer_id:
            sku_ids = SKUCustomerMap.objects.filter(
                customer_id=customer_id).values_list('sku_id', flat=True)
            skus = SKU.objects.filter(id__in=sku_ids)

            for sku in skus:
                skus_dict[sku.name] = sku.id

        return skus_dict

    def input_float_validation(self,data):
        is_valid = False
        data = str(data)
        temp = data.replace('.','')

        if temp.isdigit():
            data_float = float(data)
            if math.floor(data_float) == data_float:
                is_valid = True
                data = str(int(data_float)).strip()

        return is_valid,data

    def generate_order_from_data(self, data, file_name, no_of_entries):
        try:
            self.customer = Customer.objects.get(pk=self.customer_id)
        except Customer.DoesNotExist:
            self.response['errors'] = 'Customer ID is invalid'
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        pincode_wise_hub = self.get_pincode_wise_hub_details(self.customer)
        if not pincode_wise_hub:
            self.response['errors'] = 'There is no hub defined for this customer'
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        self.customer_divisions = list(CustomerDivision.objects.filter(
                                    customer=self.customer_id).values_list('id','name'))

        # fetch the shipment and hyperlocal contracts
        self.contract_dict = {}
        self.hyperlocal_contract_dict = {}
        contract_dict = Contract.objects.filter(
            customer_id=self.customer_id,
            contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL],
            status=CONTRACT_STATUS_ACTIVE,
        ).values('city_id', 'id', 'expected_delivery_hours',
                        'trip_workflow__pick_orders', 'contract_type',
                        'trip_workflow__otp_required', 'avg_basket_value')

        for record in contract_dict:
            _contract_type = record.get('contract_type')
            _city_id = record.get('city_id')
            if _contract_type == CONTRACT_TYPE_SHIPMENT:
                self.contract_dict[_city_id] = record
            elif _contract_type == CONTRACT_TYPE_HYPERLOCAL:
                self.hyperlocal_contract_dict[_city_id] = record

        if not (self.contract_dict or self.hyperlocal_contract_dict):
            self.response['errors'] = 'Contract not defined'
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

        self.batch = OrderBatch.objects.create(file=self.file_name,
            customer=self.customer, source=self.source,
            description=file_name, number_of_entries=no_of_entries)
        if not self.batch:
            raise exceptions.ValidationError("Batch not created")

        fail = self.__parse_params(data, pincode_wise_hub)
        self.__raise_error(fail)

        try:
            self.create_shipment_orders_in_batch()
        except BaseException:
            self.batch.delete()
            raise exceptions.ValidationError(traceback.format_exc())

        self.batch.end_time = timezone.now()
        self.batch.save()

        self.response = {
            'batch': self.batch.id,
            'number_of_orders': len(self.values_list)
        }
        return

    def __raise_error(self, fail):
        error_msg = ''

        if self.records_with_invalid_pickup_postcode:
            error_msg += (
                "Invalid pickup postal code for rows: %s \n" %
                self.records_with_invalid_pickup_postcode)

        if self.records_with_invalid_delivery_postcode:
            error_msg += (
                "Invalid delivery postal code for rows: %s \n" %
                self.records_with_invalid_delivery_postcode)

        if self.records_with_invalid_phone:
            error_msg += (
                "Invalid mobile number for rows: %s \n" %
                self.records_with_invalid_phone)

        if self.records_with_invalid_alternate_phone:
            error_msg += (
                "Invalid  alternate mobile number for rows: %s \n" %
                self.records_with_invalid_alternate_phone)


        if self.records_without_hub:
            error_msg += (
                "Out of {} service coverage area. Define a hub for"
                " the postcodes for "
                "rows {} \n".format(settings.BRAND_NAME,self.records_without_hub))

        if self.records_without_customer:
            error_msg += ("Customer Id is invalid in rows: %s \n" %
                          self.records_without_customer)

        if self.records_without_contract:
            error_msg += (
                "Define a shipment type Contract for customer for rows: %s \n" %
                self.records_without_contract)

        if self.records_with_duplicate_reference:
            error_msg += (
                "Reference number is duplicate for the rows: %s \n" %
                self.records_with_duplicate_reference)

        if self.records_with_duplicate_awb:
            error_msg += (
                "AWB number is duplicate for the rows: %s \n" %
                self.records_with_duplicate_awb)

        if self.records_with_invalid_expected_datetime:
            error_msg += (
                "Expected Delivery/Pickup Datetime is not in format YYYY-MM-DD HH24:MI for the rows: %s \n" %
                self.records_with_invalid_expected_datetime)

        if self.records_with_past_expected_datetime:
            error_msg += (
                "Expected Delivery Datetime is past datetime for the rows: %s \n" %
                self.records_with_past_expected_datetime)

        if self.records_with_weight_volume_negative:
            error_msg += (
                "Weight/Volume parameter is negative for the rows: %s \n" %
                self.records_with_weight_volume_negative)

        if self.records_with_invalid_priority:
            error_msg += (
                "Priority is not a valid integer (1,2,3..) for the rows: %s \n" %
                self.records_with_invalid_priority)
        if self.records_with_invalid_carrier_mode:
            error_msg += (
                "Invalid values in Carrier Mode for the rows: %s , valid values are - %s \n" %
                (self.records_with_invalid_carrier_mode, CARRIER_MODE))

        if self.records_with_invalid_awb:
            error_msg += (
                "AWB number is invalid for the rows: %s \n" %
                self.records_with_invalid_awb)

        if self.records_with_invalid_latlong:
            error_msg += (
                "Latitude & Longitude is invalid for the rows: %s \n" %
                self.records_with_invalid_latlong)

        if self.records_with_invalid_address_type_flag:
            error_msg += (
                "Is Commercial Address field is not [Y/N, Yes/No] for the rows: %s \n" %
                self.records_with_invalid_address_type_flag)

        if self.records_with_invalid_hub:
            error_msg += (
                "Hub name is invalid for the rows: %s \n" %
                self.records_with_invalid_hub)

        if self.records_with_invalid_division:
            error_msg += (
                "Division name is invalid for the rows: %s \n" %
                self.records_with_invalid_division)

        if self.records_without_pickup_address:
            error_msg += (
                "Pickup Address is not present for the rows: %s \n" %
                self.records_without_pickup_address)

        if self.records_without_delivery_address:
            error_msg += (
                "Delivery Address is not present for the rows: %s \n" %
                self.records_without_delivery_address)

        if self.records_without_undefined_pincode_contract:
            error_msg += (
                "Pincode not defined in the contract for the rows: %s \n" %
                self.records_without_undefined_pincode_contract)

        if self.records_with_undefined_hyperlocal_contract:
            error_msg += (
                "Hyperlocal contract not defined for the rows: %s \n" %
                self.records_with_undefined_hyperlocal_contract)

        if self.records_with_invalid_hyperlocal_flag:
            error_msg += (
                "Is Hyperlocal Order field is not [Y/N, Yes/No] for the rows: %s \n" %
                self.records_with_invalid_hyperlocal_flag)

        if fail:
            error_msg += ("Some Mandatory Fields:Company ID, ADDRESS,"
                          "PIN CODE "
                          "are Missing in rows: %s \n" %
                          self.records_with_errors)

        if error_msg != '':
            self.batch.delete()
            raise exceptions.ValidationError({'errors':error_msg})

    def create_shipment_orders_in_batch(self):
        order_creation_data_list = []
        i = 0
        with transaction.atomic():
            no_of_order_lines = []
            shipping_addresses = ShippingAddress.objects.bulk_create(self.shipping_address_list)
            pickup_addresses = ShippingAddress.objects.bulk_create(self.pickup_address_list)
            for i, order_data in enumerate(self.order_data_list):
                no_of_order_lines.append(order_data.pop('no_of_order_lines'))
                order_data['shipping_address_id'] = shipping_addresses[i].id
                order_data['pickup_address_id'] = pickup_addresses[i].id
                order_creation_data_list.append(Order(**order_data))

            orders = Order.objects.bulk_create(order_creation_data_list)

            start = 0
            end = -1
            for j, order in enumerate(orders):
                order_lines_count = no_of_order_lines[j]
                if order_lines_count:
                    start = end + 1
                    end = end + order_lines_count
                    self._update_order_line_list_with_order(order, start, end)
                self._create_new_order_event(order)
            OrderLine.objects.bulk_create(self.order_line_list)
            Event.objects.bulk_create(self.event_line_list)

    def _update_order_line_list_with_order(self, order, start, end):
        while start <= end:
            self.order_line_list[start]['order_id'] = order.id
            self.order_line_list[start] = OrderLine(**self.order_line_list[start])
            start += 1

    def _create_new_order_event(self, order):
        self.event_line_list.append(
            Event(**{
                'status': StatusPipeline.ORDER_NEW,
                'order_id': order.id,
                'address_id': order.pickup_address_id,
                'hub_associate_id': self.request.user.id,
                'remarks': 'Order created with import batch id = %s' % order.batch_id
            })
        )

    def get_number(self, number_list):
        number = OrderNumberGenerator().order_number(settings.SHIPMENT_ORDER_TYPE)
        if number in number_list:
            return self.get_number(number_list)

        number_list.append(number)
        return number


class ShipmentOrderlineImport(ImportFileMixin):
    trace_log = {}
    customer = None

    def prepare_data_to_create(self, data, user):
        self.customer = CustomerMixin().get_customer(user)
        file_obj = data.get('shipment_orders')
        if not file_obj:
            return False, _('No File uploaded')
        is_valid, response = self.process_imported_file(file_obj)
        if not is_valid:
            return is_valid, response

        total_rows = len(response)
        if total_rows == 0:
            return False, _('Empty sheet uploaded')

        headers = self.get_headers(response)
        missing_headers = [x for x in ORDERLINE_MANDATORY_HEADERS
                           if x not in headers]
        if missing_headers:
             return False, 'Missing details %s' % ', '.join(missing_headers)

        self.trace_log.update({
            'uploading_customer': self.customer.__str__(),
            'total_rows_in_sheet': total_rows
        })

        numeric_columns = ['each_item_price', 'quantity', 'cgst', 'sgst', 'igst', 'total_item_price',
                           'total_amount_incl_tax', 'weight', 'volume', 'mrp',
                           'discounts']

        error_list = []
        orderline_data = []
        validation_errors = []
        invalid_discount_type = []
        invalid_discount_percentage = []
        invalid_negative_values = []
        invalid_hsn_code = []
        invalid_uom = []
        min_row = 2
        index = min_row
        discount_percentage = False
        for row in response:
            _dict = {}
            found_error = False
            for key, _value in zip(headers, row):
                _key = ORDERLINE_LABEL_NAME_MAPPING.get(key, None)
                if _key is not None:
                    if _key in ORDERLINE_MANDATORY_VALUES and not _value:
                        validation_errors.append(str(index))
                        found_error = True
                        break

                    if _key == 'hsn_code' and _value and len(str(_value)) > 10:
                        invalid_hsn_code.append(str(index))
                        found_error = True

                    if _key == 'discount_type' and not _value:
                        _value = DISCOUNT_TYPE_FIXED

                    if _key == 'discount_type' and _value not in [DISCOUNT_TYPE_FIXED, DISCOUNT_TYPE_PERCENTAGE]:
                        invalid_discount_type.append(str(index))

                    discount_percentage = _key == 'discount_type' and _value and _value == DISCOUNT_TYPE_PERCENTAGE

                    if _key == 'discounts' and _value and discount_percentage and not (0 <= float(_value) <= 100):
                        invalid_discount_percentage.append(str(index))

                    if _key in numeric_columns:
                        if _value and _value < 0:
                            invalid_negative_values.append(str(index))
                            found_error = True

                        if not _value:
                            _value = 0

                    if _key == 'uom':
                        if _value and _value != '':
                            uom = CommonHelper.get_uom(_value)
                            if not uom:
                                invalid_uom.append(str(index))
                                found_error = True
                        else:
                            _value = 'kg'

                    _dict['%s' % _key] = _value

            index += 1
            if found_error or not _dict:
                continue
            orderline_data.append(_dict)

        if validation_errors:
            error_list.append(
                'Mandatory columns - Item Name/Order Number/Quantity missing for rows: %s' % ', '.join(validation_errors))

        if invalid_uom:
            error_list.append('Invalid UOM for rows: %s \n Available options: %s' % (','.join(invalid_uom), '\n'.join(CommonHelper.get_uom_list())))

        if invalid_negative_values:
            error_list.append('Negative entires are present for rows : %s' % ', '.join(invalid_negative_values))

        if invalid_hsn_code:
            error_list.append('The HSN code is more than 10 characters for rows : %s' % ', '.join(invalid_hsn_code))

        if invalid_discount_type:
            error_list.append('Discount Type should be fixed or percentage for rows: %s' % ', '.join(invalid_discount_type))

        if invalid_discount_percentage:
            error_list.append(
                'Discounts should be between 0 to 100 for percentage discount_type for rows : %s' % ','.join(invalid_discount_percentage))

        if error_list:
            return False, error_list

        grouped_data = {}
        sorted_orderlines = sorted(orderline_data, key=itemgetter('number'))
        for order_number, orderlines in itertools.groupby(sorted_orderlines, key=lambda x: x['number']):
            grouped_data[order_number] = list(orderlines)

        if validation_errors:
            self.trace_log.update({'validation_errors': validation_errors})
        return validation_errors, grouped_data


    def upload_orderlines(self, data):
        valid_status_list = [StatusPipeline.ORDER_NEW, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]
        orderline_history_list=[]
        order_basket_value_dict = {}
        error_list = []

        orderline_batch = OrderLineHistoryBatch.objects.create(
            description=ORDERLINE_IMPORT_BATCH_DESCRIPTION % self.customer)
        with transaction.atomic():
            existing_orderlines = OrderLine.objects\
                .select_related('order', 'sku')\
                .filter(
                    order__number__in=data.keys(),
                    order__customer=self.customer
                )
            self.trace_log.update({
                'existing_orderlines_in_orders': [
                    o.sku.__str__() for o in existing_orderlines
                ]
            })
            if existing_orderlines.exists():
                deleted_records = existing_orderlines.delete()
                self.trace_log.update({
                    'orderlines_deleted': deleted_records[0]
                })

            sku_dict = ShipmentOrderImport().get_skus_for_the_customer(
                self.customer.pk)
            self.trace_log.update({
                'sku_associated_with_customer': sku_dict
            })
            processed_data = []
            invalid_order_numbers = []
            invalid_status_orders = []

            for number, lines in data.items():
                number = ('%s' % number).split('.')[0]
                total_basket_value = 0
                order = Order.objects.select_related('customer').filter(
                            number=number, customer=self.customer).first()

                if not order:
                    invalid_order_numbers.append(number)
                    continue

                if order.status not in valid_status_list:
                    invalid_status_orders.append(str(number))
                    continue

                order_id = order.pk
                for v in lines:
                    sku_name = v.pop('sku')
                    sku_id = sku_dict.get(sku_name, None)

                    created = False
                    if not sku_id:
                        sku, created = SKU.objects.get_or_create(name=sku_name)
                        sku_id = sku.id
                        if created:
                            sku_dict[sku_name] = sku_id

                    sku_ob = WhSku.objects.filter(name=sku_name, client__customer=self.customer).first()
                    if not sku_ob:

                        pack_config = self.customer.packconfig_set.first()
                        client = self.customer.client_set.first()
                        supplier = self.customer.supplier_set.first()

                        if not pack_config:
                            from blowhorn.wms.models import WmsConstants
                            from blowhorn.wms.submodels.sku import PackConfig
                            default_pack_config_id = WmsConstants().get('DEFAULT_PACK_CONFIG_ID', 13)
                            pack_config = PackConfig.objects.filter(id=int(default_pack_config_id)).first()
                        if not client:
                            from blowhorn.wms.submodels.location import Client
                            name = 'Default Client - %s' % self.customer.name
                            client = Client.objects.create(name=name, customer=self.customer)
                        if not supplier:
                            from blowhorn.wms.submodels.inbound import Supplier
                            name = 'Default Supplier - %s' % self.customer.name
                            supplier = Supplier.objects.create(name=name, customer=self.customer)

                        sku_ob = WhSku.objects.create(name=sku_name,
                                                      pack_config=pack_config,
                                                      client=client,
                                                      supplier=supplier,
                                                      )

                    mapping, _created = SKUCustomerMap.objects.get_or_create(sku_id=sku_id, customer=self.customer)

                    v['order_id'] = order_id
                    v['sku_id'] = sku_id
                    v['wh_sku_id'] = sku_ob.id

                    _total_basket_value = float(v.get('total_amount_incl_tax',0) or 0) or float(v.get('total_item_price',0) or 0)
                    if _total_basket_value == 0:
                        _total_basket_value = (float(v.get('quantity',0) or 0) * float(v.get('each_item_price',0) or 0))

                    total_basket_value += _total_basket_value
                    orderline_history_list.append(OrderLineHistory(
                        orderline_batch=orderline_batch,
                        order=order,
                        sku_id=sku_id,
                        quantity=v.get('quantity',0),
                        each_item_price=v.get('each_item_price',0),
                        total_item_price=v.get('total_item_price',0),
                        source='%s-%s' % ('Spreadsheet', 'Upload')
                    ))
                    # Remove number
                    v.pop('number', None)

                    processed_data.append(OrderLine(**v))
                self.trace_log.update({
                    'order': number,
                    'num_of_lines': len(lines),
                    'orderline_data': lines
                })

                if total_basket_value > 0 and (order.cash_on_delivery == 0):
                    order_basket_value_dict[number] = total_basket_value

            self.trace_log.update({
                'invalid_order_numbers': invalid_order_numbers,
            })

            if invalid_status_orders:
                return False, _('The %s orders are in invalid status to be updated' % ', '.join(invalid_status_orders))

            try:
                if orderline_history_list:
                    OrderLine.objects.bulk_create(processed_data)
                    OrderLineHistory.objects.bulk_create(orderline_history_list)

                for _o,_price in order_basket_value_dict.items():
                    Order.objects.filter(number=_o).update(total_incl_tax=_price, is_avg_price=False)

            except:
                return False, {
                        'validation_errors': self.trace_log.get('validation_errors', []),
                        'invalid_order_numbers': self.trace_log.get('invalid_order_numbers', []),
                    }
            orderline_batch.number_of_entries = len(processed_data)
            orderline_batch.end_time = timezone.now()
            orderline_batch.request = json.dumps(self.trace_log)
            orderline_batch.save()
            return True, {
                'validation_errors': self.trace_log.get('validation_errors', []),
                'invalid_order_numbers': self.trace_log.get('invalid_order_numbers', [])
            }
