# System and Django Libraries
import uuid
import io
import os
import pytz
import zipfile
import boto3
from io import StringIO
from django.core.files.base import ContentFile
from datetime import timedelta
from django.conf import settings
from django.contrib.postgres.aggregates import StringAgg
from django.db.models import (
    ExpressionWrapper, F, Sum, CharField, Subquery, Q,
    Value, OuterRef, Func, Count, Case, When, DecimalField)
from django.db.models.functions import Concat
from django.http import HttpResponse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

# 3rd Party Libraries
from blowhorn.oscar.core.loading import get_model

# Blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.customer.constants import SHIPMENT_ORDER_MINIMAL_EXPORT
from blowhorn.utils.functions import utc_to_ist
from blowhorn.utils.datetime_function import DateTime

Order = get_model('order', 'Order')
OrderLine = get_model('order', 'OrderLine')
OrderDocument = get_model('order', 'OrderDocument')
Event = get_model('order', 'Event')
State = get_model('address', 'State')
UserReport = get_model('customer', 'UserReport')

s3 = boto3.client('s3', aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

tz_diff = DateTime.get_timezone_offset()

class ShipmentExport(object):
    """
    Process the given queryset and construct data for given fieldset
    and populate excel sheet with given set of column headers
    """

    def __init__(self, filename_prefix='shipment_orders', query_params=None,
                 field_names=None, column_names=None, timestamp_format=None,
                 email_data=None):
        """
        :param filename_prefix: str
        :param query_params: filter parameters (dict/instance of Q)
        :param field_names: database columns (list)
        :param column_names: column headers for excel (list)
        """
        self.filename_prefix = filename_prefix
        self.query_params = query_params
        self.field_names = field_names or SHIPMENT_ORDER_MINIMAL_EXPORT.keys()
        self.column_names = column_names or SHIPMENT_ORDER_MINIMAL_EXPORT.values()
        self.timestamp_format = timestamp_format or 'DD/MM/YYYY HH24:MI:SS'
        self.email_data = email_data

    def get_base_queryset(self):
        """
        :return: filtered queryset
        """
        if self.query_params and isinstance(self.query_params, dict):
            if settings.USE_READ_REPLICA:
                return Order.copy_data.using('readrep1').filter(**self.query_params)
            else:
                return Order.copy_data.filter(**self.query_params)

        if settings.USE_READ_REPLICA:
            return Order.copy_data.using('readrep1').filter(self.query_params)
        else:
            return Order.copy_data.filter(self.query_params)

    def get_filename(self, event, ext):
        """
        :return: Filename and path with given prefix and timestamp
        """
        _time = utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT)
        name = '%s_%s.%s' % (os.path.splitext(self.filename_prefix)[0],
                    uuid.uuid4().hex, ext)
        _path = f"media/exports/shipments/{event}/{_time}/{name}"
        return name, _path

    def export_pod_documents(self):
        _pod_bufferio = io.BytesIO()
        if settings.USE_READ_REPLICA:
            orders = Order.objects.using('readrep1').filter(self.query_params).values_list('pk', flat=True)
            documents = OrderDocument.objects.using('readrep1').filter(order_id__in=orders)
        else:
            orders = Order.objects.filter(self.query_params).values_list('pk', flat=True)
            documents = OrderDocument.objects.filter(order_id__in=orders)

        if documents.exists():
            with zipfile.ZipFile(_pod_bufferio, "a", zipfile.ZIP_DEFLATED, False) as myzip:
                for f in documents:
                    tmp_filename = '%s_%s_%s' % (f.order,
                                        f.document_type.lower(),
                                        f.file.name.split('/')[-1])
                    myzip.writestr(tmp_filename,f.file.read())

        return self.write_to_excel_file(_pod_bufferio, 'pod', ext='zip')

    def get_event(self, events, field_name='time', return_value=True):
        """
        To get particular event instance
        :param events: one or more status (list)
        :param field_name: db-column name of Event model from which value has
            to be fetched
        :param return_value: whether to return value or queryset (boolean)
        :return: queryset or value based on `return_value`
        """
        if settings.USE_READ_REPLICA:
            event_qs = Event.objects.using('readrep1').filter(order=OuterRef('pk'), status__in=events).order_by('-time')
        else:
            event_qs = Event.objects.filter(order=OuterRef('pk'), status__in=events).order_by('-time')

        if return_value:
            return Subquery(event_qs.values(field_name)[:1])
        return event_qs

    def export_orderline_details(self):
        _orderline_bufferio = StringIO()

        if settings.USE_READ_REPLICA:
            order_pks = list(Order.objects.using('readrep1').filter(self.query_params).values_list('pk', flat=True))
            order_lines_qs = OrderLine.copy_data.using('readrep1').filter(order_id__in=order_pks
                                        ).select_related('sku', 'order')
        else:
            order_pks = list(Order.objects.filter(self.query_params).values_list('pk', flat=True))
            order_lines_qs = OrderLine.copy_data.filter(order_id__in=order_pks
                                        ).select_related('sku', 'order')

        invalid_picked_status = [StatusPipeline.ORDER_NEW, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]
        order_lines_qs.annotate(
                order_number=F('order__number'),
                order_status=F('order__status'),
                item_name=Case(
                    When(Q(sku__isnull=False),
                        then=ExpressionWrapper(F('sku__name'),
                        output_field=CharField()
                    )),
                    default=F('wh_sku__name'),
                    output_field=CharField(),
                ),
                itemcode=F('item_code'),
                item_seq_no=F('item_sequence_no'),
                order_discounts=F('discounts'),
                order_discount_type=F('discount_type'),
                itemuom=F('uom'),
                cost_per_item=F('each_item_price'),
                ordered_quantity=F('quantity'),
                picked_quantity=Case(
                        When(~Q(order__status__in=invalid_picked_status),
                             then=ExpressionWrapper(F('quantity') - F('qty_removed'),
                                    output_field=DecimalField())),
                        default=0,
                        output_field=DecimalField(),
                    ),
                # picked_quantity=ExpressionWrapper(F('quantity') - F('qty_removed'),
                #                     output_field=DecimalField()),
                quantity_delivered=F('qty_delivered'),
                quantity_returned=Case(
                        When(order__status = StatusPipeline.ORDER_DELIVERED,
                             then=ExpressionWrapper(F('quantity') - F('qty_delivered') - F('qty_removed'),
                                    output_field=DecimalField())),
                        default=0,
                        output_field=DecimalField(),
                    ),
                net_amount_for_ordered_quantity=F('total_item_price'),
                net_amount_for_order_delivered=ExpressionWrapper(F('qty_delivered') * F('each_item_price'),
                                                output_field=DecimalField())
            ).order_by('order_id').to_csv(_orderline_bufferio, *self.field_names)

        return self.write_to_excel_file(_orderline_bufferio, 'orderline_details')

    def export_order_mis(self):
        _mis_bufferio = StringIO()
        if settings.USE_READ_REPLICA:
            _mis_qs = Order.copy_data.using('readrep1').filter(self.query_params)
        else:
            _mis_qs = Order.copy_data.filter(self.query_params)

        _mis_qs.annotate(
            company_name = F('customer__name'),
            city_name = F('city__name'),
            delivery_hub = F('hub__name'),
            order_customer_reference_number = F('customer_reference_number'),
            order_reference_number = F('reference_number'),
            order_number = F('number'),
            order_status = F('status'),
            order_date_placed = Func(
                F('date_placed'),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            customer_name = Concat(
                                F('shipping_address__title'), Value(' '),
                                F('shipping_address__first_name'), Value(' '),
                                F('shipping_address__last_name'),
                                output_field=CharField()
                            ),
            phone_number = F('shipping_address__phone_number'),
            address = F('shipping_address__search_text'),
            postcode = F('shipping_address__postcode'),

        ).order_by('updated_time').to_csv(_mis_bufferio, *self.field_names)

        return self.write_to_excel_file(_mis_bufferio, 'order_mis')

    def export_order_ndr(self):
        _ndr_bufferio = StringIO()
        attempted_status = [
                            # StatusPipeline.ORDER_DELIVERED,
                            # StatusPipeline.ORDER_RETURNED,
                            StatusPipeline.UNABLE_TO_DELIVER
                        ]

        attempted_event = self.get_event(attempted_status, return_value=False)
        attempted_time = self.get_event(attempted_status, return_value=True)

        if settings.USE_READ_REPLICA:
            _ndr_qs = Order.copy_data.using('readrep1').filter(self.query_params)
        else:
            _ndr_qs = Order.copy_data.filter(self.query_params)

        _ndr_qs.annotate(
            company_name = F('customer__name'),
            city_name = F('city__name'),
            delivery_hub = F('hub__name'),
            order_customer_reference_number = F('customer_reference_number'),
            order_reference_number = F('reference_number'),
            order_number = F('number'),
            order_status = F('status'),
            order_date_placed = Func(
                F('date_placed'),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            no_of_attempts = Subquery(
                attempted_event.order_by().values('order').annotate(c=Count('pk')).values('c'),
                output_field=CharField()
            ),
            first_attempted_at=Func(
                attempted_time,
                Value("DD/MM/YYYY HH24: MI"),
                function='to_char',
                output_field=CharField()
            ),
            customer_name = Concat(
                                F('shipping_address__title'), Value(' '),
                                F('shipping_address__first_name'), Value(' '),
                                F('shipping_address__last_name'),
                                output_field=CharField()
                            ),
            phone_number = F('shipping_address__phone_number'),
            address = F('shipping_address__search_text'),
            postcode = F('shipping_address__postcode'),
            _remarks=ExpressionWrapper(
                (Subquery(attempted_event.values('remarks')[:1])),
                output_field=CharField()
            ),

        ).order_by('updated_time').to_csv(_ndr_bufferio, *self.field_names)

        return self.write_to_excel_file(_ndr_bufferio, 'order_ndr')

    def export_mis_summary(self):
        _mis_bufferio = StringIO()
        if settings.USE_READ_REPLICA:
            orders = Order.copy_data.using('readrep1').filter(self.query_params)
        else:
            orders = Order.copy_data.filter(self.query_params)

        qs_a = orders.annotate(
            order_status=Case(
                When(status=StatusPipeline.ORDER_DELIVERED, then=Value(StatusPipeline.ORDER_DELIVERED)),
                When(status=StatusPipeline.ORDER_RETURNED, then=Value(StatusPipeline.ORDER_RETURNED)),
                default=Value(StatusPipeline.UNABLE_TO_DELIVER),
                output_field=CharField()
            )).values('order_status').annotate(count=Count('id'))

        qs_b = orders.annotate(order_status=Value('X_Total_', CharField()),
                                count=Count('pk')).values('order_status'
                                ).annotate(count=Count('pk'))


        final_qs = qs_a.union(qs_b).order_by('order_status')

        final_qs.to_csv(_mis_bufferio, *self.field_names)

        return self.write_to_excel_file(_mis_bufferio, 'mis_summary')

    def export_orders_with_events(self):
        """
        Export order data with events (minimal + order events)
        :return: excel file with order and order event data
        """
        _events_bufferio = StringIO()
        queryset = self.get_base_queryset()
        pack_time = self.get_event([StatusPipeline.ORDER_PACKED])
        picked_time = self.get_event([StatusPipeline.ORDER_PICKED])
        out_on_road_time = self.get_event([StatusPipeline.OUT_FOR_DELIVERY])
        if not picked_time:
            picked_time = out_on_road_time

        attempted_event = self.get_event([
            StatusPipeline.ORDER_DELIVERED,
            StatusPipeline.ORDER_RETURNED,
            StatusPipeline.UNABLE_TO_DELIVER], return_value=False)
        delivered_time = self.get_event([
                StatusPipeline.ORDER_DELIVERED,
                StatusPipeline.DELIVERY_ACK])
        driver_assigned = self.get_event([StatusPipeline.DRIVER_ASSIGNED])
        unable_to_deliver = self.get_event([StatusPipeline.UNABLE_TO_DELIVER])
        order_returned_to_hub = self.get_event([StatusPipeline.ORDER_RETURNED_TO_HUB])
        order_returned = self.get_event(StatusPipeline.ORDER_RETURNED)
        order_lost = self.get_event([StatusPipeline.ORDER_LOST])
        delivery_ack = self.get_event([StatusPipeline.DELIVERY_ACK])
        returned_ack = self.get_event([StatusPipeline.RETURNED_ACK])
        unable_to_deliver_ack = self.get_event([StatusPipeline.UNABLE_TO_DELIVER_ACK])
        attempted_timestamps = StringAgg(
            Case(
                When(event__status__in=[
                        StatusPipeline.ORDER_DELIVERED,
                        StatusPipeline.ORDER_RETURNED,
                        StatusPipeline.UNABLE_TO_DELIVER
                    ],
                    then=Func(F('event__time') + timedelta(hours=tz_diff),
                        Value(self.timestamp_format),
                        function='to_char',
                        output_field=CharField()
                    )
                ),
                output_field=CharField()
            ),
            delimiter=',', distinct=True,
            output_field=CharField()
        )
        number_of_attempts = Subquery(
            attempted_event.order_by().values('order').annotate(c=Count('*')).values('c'),
            output_field=CharField()
        )

        if settings.USE_READ_REPLICA:
            state_query = State.objects.using('readrep1').filter(cities=OuterRef('city_id'))
            docs = OrderDocument.objects.using('readrep1').filter(order=OuterRef('pk'))
        else:
            state_query = State.objects.filter(cities=OuterRef('city_id'))
            docs = OrderDocument.objects.filter(order=OuterRef('pk'))

        queryset.select_related(
            'customer', 'customer__user', 'city', 'driver', 'driver__user',
            'shipping_address'
        ).prefetch_related(
            'trip_set', 'order_lines', 'order_lines__sku', 'documents__file',
            'event_set'
        )

        local_tz = pytz.timezone(settings.ACT_TIME_ZONE)
        with timezone.override(local_tz):
            queryset.annotate(
                _remarks=ExpressionWrapper(
                    (Subquery(attempted_event.values('remarks')[:1])),
                    output_field=CharField()
                ),
                state=ExpressionWrapper(
                    (Subquery(state_query.values('cities__name')[0:1])),
                    output_field=CharField()
                ),
                sku=StringAgg('order_lines__sku__name', delimiter=','),
                total_sku_qty=Sum('order_lines__quantity'),
                # latitude=Func('shipping_address__geopoint', function='ST_Y'),
                # longitude=Func('shipping_address__geopoint', function='ST_X'),
                expected_time=Func(
                    F('expected_delivery_time') + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                last_packed_at=Func(
                    pack_time + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                last_out_on_road_at=Func(
                    out_on_road_time + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                last_attempted_at=Func(
                    (Subquery(attempted_event.values('time')[:1])) + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                formatted_date_placed=Func(
                    F('date_placed') + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                driver_assigned=Func(
                    driver_assigned + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                picked_time=Func(
                    picked_time + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                delivered_time=Func(
                    delivered_time + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                unable_to_deliver=Func(
                    unable_to_deliver + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                order_returned_to_hub=Func(
                    order_returned_to_hub + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                order_returned=Func(
                    order_returned + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                order_lost=Func(
                    order_lost + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                delivery_ack=Func(
                    delivery_ack + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                returned_ack=Func(
                    returned_ack + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                unable_to_deliver_ack=Func(
                    unable_to_deliver_ack + timedelta(hours=tz_diff),
                    Value(self.timestamp_format),
                    function='to_char',
                    output_field=CharField()
                ),
                attempted_timestamps=attempted_timestamps,
                number_of_attempts=number_of_attempts,
                pod_docs=Subquery(docs.values_list('file')[:1]),
            ).to_csv(_events_bufferio, *self.field_names)

        return self.write_to_excel_file(_events_bufferio, 'order_events')

    def export_minimal_details(self):
        _minimal_bufferio = StringIO()
        queryset = self.get_base_queryset()
        pack_time = self.get_event([StatusPipeline.ORDER_PACKED])
        picked_time = self.get_event([StatusPipeline.ORDER_PICKED])
        out_on_road_time = self.get_event([StatusPipeline.OUT_FOR_DELIVERY])
        if not picked_time:
            picked_time = out_on_road_time

        attempted_event = self.get_event([
            StatusPipeline.ORDER_DELIVERED,
            StatusPipeline.ORDER_RETURNED,
            StatusPipeline.UNABLE_TO_DELIVER], return_value=False)
        
        if settings.USE_READ_REPLICA:
            state_query = State.objects.using('readrep1').filter(cities=OuterRef('city_id'))
            docs = OrderDocument.objects.using('readrep1').filter(order=OuterRef('pk'))
        else:
            state_query = State.objects.filter(cities=OuterRef('city_id'))
            docs = OrderDocument.objects.filter(order=OuterRef('pk'))

        queryset.select_related(
            'customer', 'customer__user', 'city', 'driver', 'driver__user',
            'shipping_address'
        ).prefetch_related(
            'trip_set', 'order_lines', 'order_lines__sku', 'documents__file'
        ).annotate(
            expected_time=Func(
                F('expected_delivery_time') + timedelta(hours=tz_diff),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            last_packed_at=Func(
                pack_time + timedelta(hours=tz_diff),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            last_out_on_road_at=Func(
                out_on_road_time + timedelta(
                    hours=tz_diff),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            last_attempted_at=Func(
                (Subquery(attempted_event.values('time')[:1])) + timedelta(
                    hours=tz_diff),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            _remarks=ExpressionWrapper(
                (Subquery(attempted_event.values('remarks')[:1])),
                output_field=CharField()
            ),
            state=ExpressionWrapper(
                (Subquery(state_query.values('cities__name')[0:1])),
               output_field=CharField()
            ),
            sku=StringAgg('order_lines__sku__name', delimiter=','),
            total_sku_qty=Sum('order_lines__quantity'),
            formatted_date_placed=Func(
                F('date_placed') + timedelta(hours=tz_diff),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            picked_time=Func(
                picked_time + timedelta(hours=tz_diff),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()),
            latitude=Func('shipping_address__geopoint', function='ST_Y'),
            longitude=Func('shipping_address__geopoint', function='ST_X'),
            pod_docs=Subquery(docs.values('file')[:1])
        ).to_csv(_minimal_bufferio, *self.field_names)

        return self.write_to_excel_file(_minimal_bufferio, 'minimal_details')

    def save_exported_file(self, bufferio, export_filename, ext):
        if not self.email_data:
            return

        _user_email = None
        if 'user_email' in self.email_data:
            _user_email = self.email_data.get('user_email')
            del self.email_data['user_email']

        if ext == 'zip':
            report = ContentFile(bufferio.getvalue())
        else:
            report = ContentFile(bufferio.getvalue().encode('utf-8'))

        user_report = UserReport(**self.email_data)
        user_report.file.save(export_filename, report)
        user_report.save()

        period = self.email_data.get('period', None)
        if period == 'Adhoc':
            from blowhorn.report.services.scheduled_reports import ReportService
            ReportService().send_export_request_reports(user_report, _user_email)

    def write_to_excel_file(self, bufferio, event_name, ext='csv'):
        """
        Writes data to excel file or saves the data in file for reporting
        :return: excel file in HttpResponse
        """
        _data = bufferio.getvalue()
        if _data:
            export_filename, export_path = self.get_filename(event_name, ext)
            bufferio.seek(0)
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                            Body=_data, Key=export_path)

            if self.email_data:
                self.save_exported_file(bufferio, export_filename, ext)

            else:
                response = HttpResponse(
                            bufferio, content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = \
                    'inline; filename=%s' % os.path.basename(export_filename)
                return 200, response
        else:
            return 400, HttpResponse(_('<h1>Data doesnt exist</h1>'))
