import os
import re
import json
import uuid
from django.utils import timezone

import barcode
from barcode.writer import ImageWriter
from django.core.files.base import ContentFile
from django.core.exceptions import ValidationError
from django.conf import settings

from django.db.models.query import Q, Prefetch
from blowhorn.common.utils import generate_pdf
from blowhorn.order.models import Order, OrderLine, ShippingLabelRequest
from blowhorn.users.models import User
from blowhorn.wms.submodels.sku import WhKitSku


class ShippingLabel:
    '''
        This class is used to generate the shipping label.
        The input data should have ids[] or order_numbers[] or json data to generate label.

        JSON format ::
        {
            "awb_number": "SH-XXXXXXX",
            "order_id": XXXX,
            "date_placed": "XX MON YYYY",
            "weight": XX,
            "amount_to_collect": XX,
            "dimensions": {
                "length": X,
                "breadth": Y,
                "height": Z,
                "uom" : "xx"
            },
            "primary_mobile_no" : XXXXXXXXXX,
            "customer_name": "ABC DEF",
            "shipping_address" : "XXX, 2nd cross XXXXXXXXX",
            "delivery_pincode": XXXXXX,
            "return_address": "XXX, 10th cross XXXXXXXXX, XXXXXX"
        }
    '''

    def __init__(self, customer_id, user, data):
        # data initialization
        ids = data.get('ids[]', [])
        order_ids = data.get('order_numbers[]', [])
        self.order_ids = order_ids.split(',') if order_ids else []
        self.ids = ids.split(',') if ids else []
        self.is_with_sku = json.loads(data.get('is_with_sku', 'false'))
        self.customer_id = customer_id
        self.user = user
        self.parms = data
        self.barcode_list = []
        self.name = ''

    def validate_json_input(self):
        # prepare the order data
        _awb = self.parms.get('awb_number')
        if not _awb:
            raise ValidationError('AWB Number is mandatory for shipping label')

        data = {
            'number' : _awb,
            'order_id' : self.parms.get('order_id', ''),
            'date_placed' : self.parms.get('date_placed', timezone.now().strftime('%d %b %Y')),
            'invoice_number' : self.parms.get('invoice_number', ''),
            'return_address' : self.parms.get('return_address'),
        }

        _shipping_address = self.parms.get('shipping_address', None) or None
        _pincode = self.parms.get('delivery_pincode', None) or None
        _mobile_number = self.parms.get('primary_mobile_no', None)
        if not _shipping_address or not _pincode:
            raise ValidationError('Delivery Addresss with pincode is mandatory to generate shipping label')

        is_valid_number = re.match('^[0-9+]+$', str(_mobile_number)) if _mobile_number else False
        if _mobile_number and not is_valid_number:
            raise ValidationError('Primary contact number is invalid/missing')

        data['shipping_address'] = {
                'name' : self.parms.get('customer_name', ''),
                'address' : _shipping_address,
                'pincode' : _pincode,
                'mobile' : '%s | %s' % (self.parms.get('primary_mobile_no'),
                    self.parms.get('secondary_mobile_no')) if self.parms.get('secondary_mobile_no', None) \
                    else self.parms.get('primary_mobile_no', '')
        }

        # process cod
        data['amount_to_collect'] = data.get('cash_on_delivery', 0) or 0
        data['payment_mode'] = 'COD' if data['amount_to_collect'] > 0 else 'Online'

        # process weight
        _weight = self.parms.get('weight')
        _uom = self.parms.get('weight_uom', 'KGs')
        if _weight:
            data['weight'] = '%s %s' % (self.parms.get('weight'), _uom)

        # process dimentions
        _dimensions = self.parms.get('dimensions', None)
        if _dimensions:
            _dimensions_uom = _dimensions.get('uom', 'cc')
            _length = _dimensions.get('length', 0)
            _breadth = _dimensions.get('breadth', 0)
            _height = _dimensions.get('height', 0)
            data['dimension'] = '%sx%sx%s %s' % (_length, _breadth, _height, _dimensions_uom)

        # format the sku data if needed
        data['sku_details'] = self.parms.get('sku_details', []) if self.is_with_sku else []

        # barcode generation
        data['logo'] = '%s/images/logo/icon.png' % settings.ROOT_DIR.path('website/static')
        data['ref_barcode'] = self.get_order_barcode(_awb, font_size=20, text_distance=5)

        return data

    def get_query_data(self):
        # fetch the queryset
        _qp = Q(customer_id=self.customer_id)
        if self.ids:
            _qp = _qp & Q(id__in=self.ids)
        elif self.order_ids:
            _qp = _qp & Q(number__in=self.order_ids)
        else:
            _qp = _qp & Q(**self.parms)

        qs = Order.objects.filter(_qp).select_related('shipping_address',
                'pickup_address', 'dimension')

        if qs and self.is_with_sku:
            qs = qs.prefetch_related(
                Prefetch(
                    'order_lines',
                    queryset=OrderLine.objects.all().select_related('wh_sku', 'sku')
                )
            )

        return qs

    def get_order_barcode(self, order_number, font_size=10, text_distance=5):
        self.name = '%s' % uuid.uuid4().hex
        _url = '/tmp/%s' % self.name
        _barcode = barcode.get('code128', order_number, writer=ImageWriter())
        filename = _barcode.save(_url, {'font_size': font_size, 'text_distance': text_distance})
        self.barcode_list.append(_url)
        return filename

    def get_formatted_order(self, order):
        # prepare the order data
        # _mobile = '%s | %s' % (order.shipping_address.phone_number.national_number,
        #             order.shipping_address.alternate_phone_number) if order.shipping_address.alternate_phone_number \
        #             else order.shipping_address.phone_number.national_number

        # only show mobile number if call masking not enabled
        # if order.customer_contract.trip_workflow.is_call_masking:
        #     _mobile = ''

        data = {
            'printing_header': 'Shipping Label' if self.is_with_sku else 'Shipping Manifest',
            'number': order.number,
            'order_id': order.id,
            'date_placed': order.date_placed.strftime('%d %b %Y'),
            'invoice_number' : order.invoice_number or '',
            'weight': '%s KGs' % order.weight if order.weight else '',
            'payment_mode': 'COD' if order.cash_on_delivery > 0 else 'Online',
            'amount_to_collect': '%s' % (order.cash_on_delivery),
            'dimension': '%sx%sx%s %s' % (
                int(order.dimension.length) if order.dimension.length==int(order.dimension.length) else order.dimension.length,
                int(order.dimension.breadth) if order.dimension.breadth==int(order.dimension.breadth) else order.dimension.breadth,
                int(order.dimension.height) if order.dimension.height==int(order.dimension.height) else order.dimension.height,
                'cc') if order.dimension else '',
            'shipping_address' : {
                'name': order.shipping_address.first_name,
                'address': ','.join(order.shipping_address.active_address_fields()),
                'pincode': order.shipping_address.postcode,
            },
            'pickup_hub_name': order.pickup_hub.name if order.pickup_hub else '',
            'delivery_hub_name': order.hub if order.hub else '',
            'return_address': ','.join(order.pickup_address.active_address_fields()),
        }

        # format the sku data if needed
        if self.is_with_sku:
            skus = []
            if order.order_lines.filter(wh_kit_sku__isnull=False).exists():
                kit_sku_ids = list(order.order_lines.filter(wh_kit_sku__isnull=False).values_list('wh_kit_sku_id',
                                                                                                  flat=True).distinct())
                kit_sku_qs = WhKitSku.objects.filter(id__in=kit_sku_ids)
                for whkitsku in kit_sku_qs:
                    _temp = {
                        'quantity': whkitsku.quantity,
                        'total_item_price': whkitsku.total_item_price,
                        'name': '%s ( %s )' % (whkitsku.name,
                                               whkitsku.description) if whkitsku.description else whkitsku.name
                    }
                    skus.append(_temp)

            for line in order.order_lines.filter(wh_kit_sku__isnull=True).all():
                _temp = {
                    'quantity': line.quantity,
                    'each_item_price': line.each_item_price,
                    'total_item_price': line.total_amount_incl_tax if line.total_amount_incl_tax else line.total_item_price,
                }
                if line.wh_sku:
                    _temp['name'] = line.wh_sku.name.split('|')[0]
                elif line.sku:
                    _temp['name'] = '%s | %s' % (line.sku.name, line.item_category) if line.item_category\
                        else line.sku.name
                if line.weight:
                    _temp['name'] = '%s | %s kg' % (_temp['name'], line.weight)
                skus.append(_temp)

            data['sku_details'] = skus

        # barcode generation
        data['logo'] = '%s/images/logo/icon.png' % settings.ROOT_DIR.path('website/static')
        data['ref_barcode'] = self.get_order_barcode(order.reference_number or order.customer_reference_number or order.number,
                                font_size=20)
        data['number_barcode'] = self.get_order_barcode(order.number, font_size=17)

        return data

    def generate_output_format(self, context, label_ob):
        _template_name = 'pdf/shipping_label/main.html'
        _url = None
        bytes = generate_pdf(_template_name, context)
        if bytes:
            filename = '%s.pdf' % self.name
            label_ob.file.save(filename, ContentFile(bytes), save=True)
            label_ob.save()
            _url = label_ob.file.url

        [os.remove(_path) for _path in self.barcode_list if os.path.exists(_path)]
        return _url

    def create(self):
        context = []
        _parm = {
            'customer_id': self.customer_id
        }
        if isinstance(self.user, User):
            _parm['user'] = self.user
        label_request = ShippingLabelRequest.objects.create(**_parm)

        if self.ids or self.order_ids:
            orders = self.get_query_data()
            if not orders:
                raise ValidationError('Please provide valid Order Numbers/IDs')
            for order in orders:
                _temp = self.get_formatted_order(order)
                context.append(_temp)
        else:
            _temp = self.validate_json_input()
            context.append(_temp)

        return self.generate_output_format(context, label_request)
