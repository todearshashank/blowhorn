import requests
import json
import random
import string
from datetime import datetime, timedelta

from django.conf import settings
from blowhorn.order.const import unicom_currency_code_list, PAYMENT_MODE_CASH

class UnicomShipmentOrder(object):

    """
        Unicommerce Shipment Order: This class handles order
        creation from unicommerce.
    """

    def process_order_data(self, data):
        """
            Maps unicommere fields to blowhorn order fields
        """
        input_data = {}

        input_data['delivery_mode'] = data.get('serviceType', None)
        
        input_data['total_payable'] = data.get('totalAmount', 0)
        payment_mode = data.get('paymentMode', None)
        if payment_mode == 'COD':
            input_data['is_cod'] = "True"
            input_data['payment_mode'] = PAYMENT_MODE_CASH
            input_data['cash_on_delivery'] = data.get('collectableAmount' , 0)

        return_flag = data.get('returnShipmentFlag' , "False")
        input_data['is_return_order'] = return_flag.capitalize()

        shipment_details = data.get('Shipment', None)
        if not shipment_details:
            _out = {
                'status': 'Failed',
                'message': 'Please provide the shipment item details'
            }
            return _out

        # order_date = shipment_details.get('orderDate', None)
        try:
            delivery_time = shipment_details.get('fullFilllmentTat', None)
            if delivery_time:
                delivery_time = datetime.strptime(delivery_time, '%d-%b-%Y %H:%M:%S')
                if delivery_time > datetime.now() + timedelta(minutes=2):
                    delivery_time = delivery_time.strftime('%Y-%m-%d %H:%M:%S')
                    if input_data['is_return_order'] == 'True':
                        input_data['pickup_datetime'] = delivery_time
                    else:
                        input_data['expected_delivery_time'] = delivery_time

        except Exception:
            _out = {
                'status': 'Failed',
                'message': 'Invalid fullFilllmentTat format'
            }
            return _out

        length = shipment_details.get('length', None)
        breadth = shipment_details.get('breadth', None)
        height = shipment_details.get('height', None)
        weight = shipment_details.get('weight', None)

        if length or breadth or height:
            input_data['length'] = str(float(length) / 10 if length else 0)
            input_data['breadth'] = str(float(breadth) / 10 if breadth else 0)
            input_data['height'] = str(float(height) / 10 if height else 0)
        
        if weight:
            input_data['weight'] =  str(float(weight) / 1000 if weight else 0)

        currency_code = data.get('currencyCode', None)
        if currency_code and currency_code in unicom_currency_code_list:
            input_data['currency_code'] = currency_code
        else:
            _out = {
                'status': 'Failed',
                'message': 'Please provide valid currency code'
            }
            return _out

        item_details = []
        items = shipment_details.get('items')
        for item in items:
            temp = {}
            temp['item_name'] = item['name']
            temp['item_quantity'] = item['quantity']
            item_details.append(temp)

        if item_details:
            input_data['item_details'] = item_details

        code = shipment_details.get('code', None)
        reference_number = self.generate_random_reference_number(code)
        input_data['reference_number'] = reference_number

        # customer reference number
        _crn = shipment_details.get('orderCode', None)
        if _crn:
            input_data['customer_reference_number'] = _crn

        delivery_address_details = data.get('deliveryAddressDetails', None)
        if not delivery_address_details:
            _out = {
                'status': 'Failed',
                'message': 'Please provide the delivery address details'
            }
            return _out

        input_data['customer_name'] = delivery_address_details.get('name', None)
        input_data['customer_email'] = delivery_address_details.get('email', None)
        input_data['customer_mobile'] = delivery_address_details.get('phone', None)
        input_data['delivery_postal_code'] = delivery_address_details.get('pincode', None)

        # delivery address
        _delivery_address = "{} {}".format(
                                delivery_address_details.get('address1')
                                    if delivery_address_details.get('address1') else '',
                                delivery_address_details.get('address2') 
                                    if delivery_address_details.get('address2') else ''
                            )
        delivery_address = _delivery_address.strip()
        address_details = [
                            delivery_address,
                            delivery_address_details.get('city'),
                            delivery_address_details.get('state'),
                            delivery_address_details.get('country')
                        ]
        input_data['delivery_address'] = ','.join(address_details)

        # pickup address
        pickedup_address_details = data.get('pickupAddressDetails', None)
        if not pickedup_address_details:
            _out = {
                'status': 'Failed',
                'message': 'Please provide the pickup address details'
            }
            return _out

        input_data['pickup_postal_code'] = pickedup_address_details.get('pincode', None)
        _pickup_address = "{} {}".format(
                            pickedup_address_details.get('address1')
                                if pickedup_address_details.get('address1') else '',
                            pickedup_address_details.get('address2')
                                if pickedup_address_details.get('address2') else ''
                        )
        pickup_address = _pickup_address.strip()
        
        address_details = [
                            pickup_address,
                            pickedup_address_details.get('city'),
                            pickedup_address_details.get('state'),
                            pickedup_address_details.get('country')
                        ]
        
        input_data['pickup_customer_name'] = pickedup_address_details.get('name')
        input_data['pickup_customer_mobile'] = pickedup_address_details.get('phone')
        input_data['pickup_address'] = ','.join(address_details)
        input_data['source'] = 'unicommerce'
        return input_data

    def generate_random_reference_number(self, code):
        random_string = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 6))
        if code:
            # reference_number = code + '-' + random_string
            reference_number = code
        else:
            reference_number = 'REF' + random_string
        return reference_number

    def create_order(self, input_data, api_key):
        """
            Call blowhorn shipment order api and create order
        """

        result = {}
        headers = {
            "api-key" : api_key,
            "Content-type": "application/json"
        }

        host_url = settings.HOST
        client_url = host_url + '/api/orders/shipment'
        response = requests.post(client_url, json=input_data, headers=headers)
        order = json.loads(response.content)
        
        if type(order) == dict:
            status = order.get('status')
            message = order.get('message')

        else:
            message = order[0]
            status = 'Failed'

        if status == 'PASS':
            final_response = {
                "status" : "SUCCESS",
                "waybill" : order['message'].get('awb_number'),
                "shippingLabel" : "",
                "courierName" : "Blowhorn"
            }

        else:
            final_response = {
                "status" : "Failed",
                "message" : message
            }

        result = {
            "status" : response.status_code,
            "data" : final_response
        }
        return result