from django.db import connection


def get_radial_distance(pickup_lat, pickup_lon, dropoff_lat, dropoff_lon):
    with connection.cursor() as cursor:
        cursor.execute("select ST_Distance(ST_GeogFromText('SRID=4326; POINT(%s %s )'),ST_GeogFromText("
                       "'SRID=4326;POINT(%s %s )'))", [pickup_lon, pickup_lat, dropoff_lon, dropoff_lat])
        row = cursor.fetchone()
        print("ff------------", "select ST_Distance(ST_GeogFromText('SRID=4326; POINT(%s %s )'),ST_GeogFromText("
                       "'SRID=4326;POINT(%s %s )'))", [pickup_lon, pickup_lat, dropoff_lon, dropoff_lat], row)

    return row[0]/1000 if row else 0
