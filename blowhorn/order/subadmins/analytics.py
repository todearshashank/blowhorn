import os
import csv
import json
from datetime import timedelta
from django.conf import settings
from django.contrib import admin
from django.db.models import Func, F, Value, CharField
from django.http import HttpResponse
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import ugettext as _

from blowhorn.common.admin import BaseAdmin, AUDITLOG_FIELDS
from blowhorn.order.models import UTMTracking
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from blowhorn.utils.functions import utc_to_ist


def export_utm_data(modeladmin, request, queryset):
    file_path = 'utm_data'
    UTMTracking.copy_data.filter(
        **request.GET.dict()
    ).annotate(
        created_datetime=Func(
            F('created_date') + timedelta(hours=5.5),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        )
    ).to_csv(
        file_path, 'order__number', 'order__city', 'utm_source', 'utm_medium',
        'utm_campaign', 'created_datetime'
    )
    modified_file = '%s_%s.csv' % (
        utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT),
        os.path.splitext(file_path)[0])

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)
        w.writerow(['Order', 'City', 'Source', 'Medium', 'Campaign', 'Created Date'])
        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        os.remove(file_path)

    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = \
                'inline; filename=%s' % os.path.basename(modified_file)
            os.remove(modified_file)
            return response

    return HttpResponse(400, _('Failed to export file'))


class UTMTrackingAdmin(BaseAdmin):

    list_display = ['id', 'get_order_number', 'get_order_status', 'get_city', 'utm_source', 'utm_medium',
                    'utm_campaign', 'get_device_type', 'created_date']
    list_filter = [('order__city', admin.RelatedOnlyFieldListFilter),
                   ('created_date', DateRangeFilter)]
    search_fields = ('order__number', 'utm_source', 'utm_medium',
                     'utm_campaign', 'order__device_type')
    actions_selection_counter = False
    show_full_result_count = False
    actions = [export_utm_data]

    fieldsets = (
        ('General', {
            'fields':
                ('get_order_number', 'get_order_status', 'get_city', 'utm_source', 'utm_medium',
                 'utm_campaign', 'extra_data')
        }),
        ('Auditlog', {
            'fields': AUDITLOG_FIELDS
        }),
    )

    def get_order_number(self, obj):
        if obj.order_id:
            link = reverse("admin:order_bookingorder_change", args=[obj.order_id])
            return format_html('<a href="%s">%s</a>' % (link, obj.order))

        return None

    get_order_number.short_description = _('Order')
    get_order_number.allow_tags = True

    def get_order_status(self, obj):
        if obj.order_id:
            return obj.order.status

        return None

    get_order_status.short_description = _('Order Status')

    def get_device_type(self, obj):
        if obj.order_id:
            return obj.order.device_type

        return None

    get_device_type.short_description = _('Device Type')

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related(
            'order', 'order__city', 'created_by', 'modified_by'
        )

    def get_actions(self, request):
        """
        Remove `Delete` action
        """
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']

        return actions

    def get_city(self, obj):
        return obj.order.city

    get_city.short_description = _('City')


admin.site.register(UTMTracking, UTMTrackingAdmin)
