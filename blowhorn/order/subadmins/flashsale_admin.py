from django.contrib import admin
from django.contrib.admin import helpers
from blowhorn.order.models import Order, OrderLine
from django.utils.translation import ugettext as _
from django.conf import settings
from django.urls import reverse
from config.settings import status_pipelines as StatusPipeline
from django.utils.html import format_html
from blowhorn.contract.constants import CONTRACT_TYPE_FLASH_SALE
from blowhorn.order.admin import TripAdminInline, AssignDriverActionForm, create_new_trip_for_orders, \
    assign_orders_in_trip, EventAdminInline, OrderLineInline, OrderDispatchAdminInline
from django.db import transaction

from blowhorn.order.utils import create_trip_when_driver_assigned
from blowhorn.trip.models import Trip, Stop
from blowhorn.order.models import OrderCostHistory, ShippingAddress
from blowhorn.driver.models import DriverPayment
from blowhorn.driver.constants import APPROVED
from blowhorn.common.admin import NonEditableAdmin
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from django import forms
from django.contrib import messages

class FlashSaleOrder(Order):

    class Meta:
        proxy = True
        verbose_name = _('Flash Sale Booking')
        verbose_name_plural = _('Flash Sale Booking')

class StoreFrontOrder(Order):

    class Meta:
        proxy = True
        verbose_name = _('Store Front Booking')
        verbose_name_plural = _('Store Front Booking')


class FlashSaleSettlementView(OrderLine):

    class Meta:
        proxy = True
        verbose_name = _('Store Front Settlement')
        verbose_name_plural = _('Store Front Settlement')


class FlashSaleForm(forms.ModelForm):

    custom_dropoff_address =forms.CharField( widget=forms.Textarea, required=False)


# def create_new_trip_for_orders(modeladmin, request, queryset):
#     driver_id = request.POST.get('driver')
#     if not driver_id:
#         messages.error(request, "Driver not selected")
#         return False
#     driver = Driver.objects.get(id=driver_id)
#
#     #supend the existing trip
#     query=Q(status=StatusPipeline.ORDER_DELIVERED) | Q(payment_status=PAYMENT_STATUS_UNPAID)
#     if queryset.filter(query).exists():
#         messages.error(request, _("Delivered/Un-Paid Order can't be assigned to new driver"))
#         return False
#     Trip.objects.filter(order__in=queryset).exclude(
#         status__in=StatusPipeline.TRIP_END_STATUSES).update(status=StatusPipeline.TRIP_SUSPENDED)
#     Stop.objects.filter(trip__order__in=queryset).exclude(
#         status__in=StatusPipeline.TRIP_END_STATUSES).update(status=StatusPipeline.TRIP_SUSPENDED)
#
#     for order in queryset:
#         order.driver = driver
#         create_trip_when_driver_assigned(order, request, order.trip_set
#                                          .exclude(status=StatusPipeline.TRIP_END_STATUSES).first())
#         order.status = StatusPipeline.DRIVER_ACCEPTED
#         order.save()
#     messages.success(request, 'Driver Assigned Sucessfully')



class FlashSaleAdmin(FSMTransitionMixin, NonEditableAdmin):
    fsm_field = ['status', ]
    list_filter = [
        'city', ('pickup_datetime', DateRangeFilter),
        'customer__customer_category',
        ('driver', admin.RelatedOnlyFieldListFilter), 'status',
        'payment_status', 'payment_mode'
    ]

    list_display = (
        'number', 'get_customer', 'get_customer_phone_number', 'status', 'driver',
        'city', 'pickup_datetime', 'date_placed', 'payment_status',
        'payment_mode', 'invoice_number',
    )

    actions = [create_new_trip_for_orders, assign_orders_in_trip]
    action_form = AssignDriverActionForm

    fieldsets = (
        ('General', {
            'fields':
                (('get_customer', 'driver'), 'get_customer_phone_number',
                 'customer_category', 'invoice_number', 'pickup_datetime',
                 'city', 'status', 'booking_type', 'date_placed',
                 'get_invoice_url',
                 'customer_contract', 'number',)
        }),
        ('Cost', {
            'fields': ('estimated_cost', 'actual_cost', 'revised_cost',
                       'adjustment_reason', 'is_pod_required',
                       'total_excl_tax', 'discount', ('tax', 'rcm',),
                       'total_incl_tax', 'toll_charges', 'total_payable',
                       'payment_status',
                       'payment_mode', 'get_txn_id',)

        }),
        ('Others', {
            'fields': ('estimated_distance_km',)
        }),
        ('Address Details', {
            'fields': (('shipping_contact_phone_number',
                        'shipping_contact_name', 'shipping_address'))
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),)

    def get_customer(self, obj):
        if obj.customer:
            link = reverse("admin:customer_customer_change",
                           args=[obj.customer_id])
            return format_html(
            '<a href="%s">%s</a>' % (link, obj.customer))
        else:
            return None

    get_customer.allow_tags = True
    get_customer.short_description = 'Customer'

    inlines = [TripAdminInline, EventAdminInline, OrderLineInline, OrderDispatchAdminInline]

    def get_customer_phone_number(self, obj):
        if obj and obj.customer and obj.customer.user:
            return obj.customer.user.phone_number
        return None

    get_customer_phone_number.short_description ='Mobile Number'

    def shipping_contact_phone_number(self, obj):
        return obj.shipping_address.phone_number if obj.shipping_address else ''

    shipping_contact_phone_number.short_description = 'Drop Off Contact Number'

    def shipping_contact_name(self, obj):
        return obj.shipping_address.first_name if obj.shipping_address else ''

    shipping_contact_name.short_description = 'Drop Off Contact Name'

    def get_txn_id(self, obj):
        transaction_ids = set()
        for x in obj.sources.all():
            for x in x.transactions.all():
                transaction_ids.add(x.reference)
        transaction_ids.update(
            list(
                obj.order_entities.exclude(
                    transaction_order__transactions__TXNID=None
                ).values_list('transaction_order__transactions__TXNID', flat=True).distinct()
            )
        )
        return list(transaction_ids)

    def get_invoice_url(self, obj):
        if obj.number and obj.status == StatusPipeline.ORDER_DELIVERED:
            host_ = self.request.META.get('HTTP_HOST', 'blowhorn.com')

            return format_html("""
                <a href="http://""" + host_ + """/invoice/""" + obj.number + """ " target="_blank">
                %s
                </a>
                """ % (obj.number)
            )

        return '-'

    get_invoice_url.short_description = 'View Invoice'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.filter(order_type=settings.C2C_ORDER_TYPE,
                                   customer_contract__contract_type=CONTRACT_TYPE_FLASH_SALE)
        qs = qs.select_related('customer', 'shipping_address')
        qs = qs.select_related('customer__user')
        qs = qs.prefetch_related('event_set')
        qs = qs.select_related('driver', 'customer_contract').distinct().order_by('-date_placed')
        self.request=request

        return qs

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = [f.name for f in self.model._meta.fields]
        read_only_fields.remove('payment_mode')
        read_only_fields.remove('payment_status')
        if obj.status != StatusPipeline.ORDER_DELIVERED:
            read_only_fields.remove('driver')
            read_only_fields.remove('revised_cost')
            read_only_fields.remove('shipping_address')
            read_only_fields.remove('is_pod_required')
        if obj.status == StatusPipeline.ORDER_DELIVERED and obj.payment_status != 'Paid':
            read_only_fields.remove('revised_cost')
            read_only_fields.remove('adjustment_reason')
            # read_only_fields.remove('payment_mode')
            # read_only_fields.remove('payment_status')
        read_only_fields.extend(['created_date', 'created_by', 'modified_date','modified_by',
                                 'date_placed', 'get_customer', 'get_txn_id',
                                 'get_customer_phone_number', 'get_invoice_url',
                                 'shipping_contact_phone_number', 'shipping_contact_name',])

        return read_only_fields

    def get_form(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        return super().get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'shipping_address':
            qs = ShippingAddress.objects.filter(
                waypoint__order=self.parent_obj
            ).select_related('country')
            kwargs['queryset'] = qs

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        if form.is_valid() and 'revised_cost' in form.changed_data:
            obj.update_invoice_information()
        super(FlashSaleAdmin, self).save_model(request, obj, form, change)

    def save_model(self, request, obj, form, change):
        with transaction.atomic():
            if form.is_valid():
                if change:
                    old_order_obj = Order.objects.get(id=obj.id)
                    if obj.status == StatusPipeline.ORDER_DELIVERED:
                        cost_fields = ['toll_charges', 'revised_cost',
                                       'payment_mode', 'payment_status']
                        changed_fields = form.changed_data

                        if any(x in cost_fields for x in changed_fields):
                            # cost_fields in changed_fields:
                            if 'revised_cost' in form.changed_data:
                                order_cost_data = {
                                    'order': obj,
                                    'old_value': old_order_obj.revised_cost,
                                    'new_value': obj.revised_cost,
                                    'reason': obj.adjustment_reason
                                }
                                order_cost = OrderCostHistory(**order_cost_data)
                                order_cost.save()
                            trips = Trip.objects.filter(order=obj)
                            DriverPayment.objects.filter(
                                paymenttrip__trip__in=trips).exclude(
                                status=APPROVED).update(is_stale=True)

                            obj.update_invoice_information()

                    if obj.driver:
                        if obj.status == StatusPipeline.ORDER_NEW:
                            obj.status = StatusPipeline.DRIVER_ACCEPTED

                        if old_order_obj and old_order_obj.pickup_datetime != obj.pickup_datetime:
                            # trip scheduled at should change based on
                            # order pick up time
                            Trip.objects.filter(
                                order_id=obj.id,
                                # driver_id=old_order_obj.driver,
                                status__in=[
                                    StatusPipeline.TRIP_NEW, StatusPipeline.TRIP_IN_PROGRESS,
                                    StatusPipeline.DRIVER_ACCEPTED
                                ]).update(planned_start_time=obj.pickup_datetime)

                        if not old_order_obj.driver:
                            # new order, no driver associated with order
                            # create a new trip and assigned a driver
                            create_trip_when_driver_assigned(
                                obj, request)

                        elif old_order_obj.driver != obj.driver:
                            # order has driver, contingency case ,
                            # suspend existing trip and create new trip
                            trip = Trip.objects.filter(
                                order_id=obj.id,
                                status__in=[StatusPipeline.TRIP_IN_PROGRESS,
                                            StatusPipeline.TRIP_NEW,
                                            StatusPipeline.DRIVER_ACCEPTED])

                            Trip.objects.filter(
                                order=obj, status__in=[StatusPipeline.TRIP_NEW, StatusPipeline.DRIVER_ACCEPTED,
                                                 StatusPipeline.TRIP_IN_PROGRESS]
                                                ).update(status=StatusPipeline.TRIP_SUSPENDED)

                            Stop.objects.filter(trip__in=trip,  status__in=[StatusPipeline.TRIP_NEW, StatusPipeline.DRIVER_ACCEPTED,
                                                 StatusPipeline.TRIP_IN_PROGRESS]
                                                ).update(status=StatusPipeline.TRIP_SUSPENDED, waypoint=None)

                            create_trip_when_driver_assigned(
                                obj, request, trip.first())

                # obj.save()
        super(FlashSaleAdmin, self).save_model(request, obj, form, change)


class StoreFrontAdmin(FlashSaleAdmin):
    def get_queryset(self, request):
        qs = Order.objects.filter(customer_contract__customer__api_key=settings.FLASH_SALE_API_KEY)
        qs = qs.select_related('customer', 'shipping_address')
        qs = qs.select_related('customer__user')
        qs = qs.prefetch_related('event_set')
        qs = qs.select_related('driver', 'customer_contract').distinct().order_by('-date_placed')

        return qs

class EcomOrder(Order):

    class Meta:
        proxy = True
        verbose_name = _('Ecom Order')
        verbose_name_plural = _('Ecom Order')

class EcomOrderAdmin(FSMTransitionMixin, NonEditableAdmin):
    fsm_field = ['status', ]
    list_filter = [
        'city', ('pickup_datetime', DateRangeFilter),
        'status', 'payment_status', 'payment_mode',
    ]

    list_display = (
        # 'number', 'user', 'status',
        'city', 'date_placed', 'payment_status',
        'payment_mode'
    )
    def get_txn_id(self, obj):
        transaction_ids = set()
        for x in obj.sources.all():
            for x in x.transactions.all():
                transaction_ids.add(x.reference)
        transaction_ids.update(
            list(
                obj.order_entities.exclude(
                    transaction_order__transactions__TXNID=None
                ).values_list('transaction_order__transactions__TXNID', flat=True).distinct()
            )
        )
        return list(transaction_ids)


    fieldsets = (
        ('General', {
            'fields':
                ("user", 'invoice_number',
                 'city', 'status', 'booking_type', 'date_placed',
                 'number',)
        }),
        ('Cost', {
            'fields': ('estimated_cost', 'actual_cost', 'revised_cost',
                       'adjustment_reason', 'is_pod_required',
                       'total_excl_tax', 'discount', ('tax', 'rcm',),
                       'total_incl_tax', 'toll_charges', 'total_payable',
                       'payment_status',
                       'payment_mode', 'get_txn_id',)

        }),
        ('Address Details', {
            'fields': ('hub',)
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),)


    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.filter(order_type=settings.ECOMM_ORDER_TYPE)
        qs = qs.select_related('user', 'shipping_address')
        return qs

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = [f.name for f in self.model._meta.fields]
        read_only_fields.extend(['get_txn_id'])

        return read_only_fields

# commened in actions below -> not getting used
def mark_settled(self, request, queryset):
    for line in queryset:
        if line.cost_price or 0 > 0:
            line.is_settled = True
            line.save()


def get_settlement_preview(self, request, queryset):
    amount = 0
    count = 0
    for line in queryset:
        if line.cost_price or 0 > 0:
            count += 1
            amount += float(line.cost_price)

    preview_message = 'Settlement Preview: Sum=%.2f, Count=%d' % (amount, count)
    self.message_user(request, preview_message, level=messages.INFO)

# class FlashSaleOrderSettlementViewAdmin(NonEditableAdmin):
#     action_form = helpers.ActionForm
#
#     list_display = ['order', 'partner', 'get_order_status', 'product', 'quantity', 'each_item_price',
#                     'total_item_price', 'cost_price', 'is_settled', 'get_payment_status', 'get_payment_mode' ]
#
#
#     def get_order_status(self, obj):
#         if obj.order:
#             return obj.order.status
#         return ''
#
#     def get_payment_status(self, obj):
#         if obj.order:
#             return obj.order.payment_status
#         return ''
#
#     def get_payment_mode(self, obj):
#         if obj.order:
#             return obj.order.payment_mode
#         return ''
#
#     def get_readonly_fields(self, request, obj=None):
#         read_only_fields = []
#         if obj:
#             read_only_fields += [f.name for f in self.model._meta.fields]
#             read_only_fields += ['get_order_status', 'get_payment_status', 'get_payment_mode']
#
#         return read_only_fields
#
#     actions = [get_settlement_preview, mark_settled]
#
#     def get_queryset(self, request):
#         qs = super().get_queryset(request)
#         qs = qs.filter(order__status=StatusPipeline.ORDER_DELIVERED, is_settled=False,
#                        order__customer_contract__customer__api_key=settings.FLASH_SALE_API_KEY)
#         qs = qs.select_related('order', 'order__customer_contract')
#         return qs


admin.site.register(FlashSaleOrder, FlashSaleAdmin)
admin.site.register(EcomOrder, EcomOrderAdmin)
admin.site.register(StoreFrontOrder, StoreFrontAdmin)
# admin.site.register(FlashSaleSettlementView, FlashSaleOrderSettlementViewAdmin)


