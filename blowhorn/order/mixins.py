# System and Django libraries
import logging
import pytz
import json
import re
from decimal import Decimal as D
from datetime import datetime, timedelta
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction, InternalError
from django.db.models import F
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_aware, make_aware
from django.utils.translation import ugettext_lazy as _

# 3rd Party libraries
# from blowhorn.oscar.apps.partner.models import StockRecord
from phonenumber_field.phonenumber import PhoneNumber
from rest_framework import status, exceptions
from blowhorn.oscar.core.loading import get_model, get_class as oscar_get_class
from blowhorn.oscar.core import prices
from blowhorn.oscarapi.utils.settings import overridable

# Blowhorn imports
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_SHIPMENT
from config.settings import status_pipelines as StatusPipeline
from blowhorn.order.utils import OrderNumberGenerator
from blowhorn.order.models import ShippingAddress, Labour
from blowhorn.common.helper import CommonHelper
from blowhorn.oscar.core.loading import get_model
from blowhorn.trip.models import Trip, Stop
from blowhorn.legacy.adapter import LegacyAdapter
from blowhorn.address.utils import get_city
from blowhorn.address.maps import GoogleMaps
from blowhorn.vehicle.utils import get_vehicle_class_using_preference
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL
from blowhorn.order.tasks import get_geopoint_from_delivered_addresses
from blowhorn.contract.utils import ContractUtils
from blowhorn.utils.functions import dist_between_2_points
from blowhorn.driver.payment_helpers.c2c_payment_helper import PaymentHelper
from blowhorn.contract.contract_helpers.contract_helper import ContractHelper
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.order.const import PAYMENT_STATUS_UNPAID
from blowhorn.order.models import ItemCategory
from blowhorn.coupon.mixins import CouponMixin
from blowhorn.order.utils import OrderCreator
from blowhorn.trip.mixins import TripMixin
from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED, DISCOUNT_TYPE_PERCENTAGE
from blowhorn.shopify.tasks import sync_specific_inventory

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Customer = get_model('customer', 'Customer')
SKU = get_model('customer', 'SKU')
SKUCustomerMap = get_model('customer', 'SKUCustomerMap')
Country = get_model('address', 'Country')
UserAddress = get_model('address', 'UserAddress')
Order = get_model('order', 'Order')
Dimension = get_model('order', 'Dimension')
OrderHistory = get_model('order', 'OrderHistory')
OrderLine = get_model('order', 'OrderLine')
OrderLineHistory = get_model('order', 'OrderLineHistory')
OrderLineHistoryBatch = get_model('order', 'OrderLineHistoryBatch')
Event = get_model('order', 'Event')
WayPoint = get_model('order', 'WayPoint')
ShipmentAlertConfiguration = get_model(
    'customer', 'ShipmentAlertConfiguration')
VehicleClass = get_model('vehicle', 'VehicleClass')
WhSku = get_model('wms', 'WhSku')
WhKitSku = get_model('wms', 'WhKitSku')

# Repository = oscar_get_class('shipping.repository', 'Repository')
DeliveredAddress = get_model('order', 'DeliveredAddress')
Contract = get_model('contract', 'Contract')
UTMTracking = get_model('order', 'UTMTracking')

TAX_PERCENT = 0
AVERAGE_VEHICLE_SPEED_KMPH = 15.0
LOADING_TIME = 20
UNLOADING_TIME = 10


class CustomBasket(object):
    currency = 'INR'
    has_shipping_discounts = False

    def is_shipping_required():
        """
        Test whether the basket contains physical products that require
        shipping.
        """
        return False


class OrderPlacementMixin(object):

    """
    Mixin which provides functionality for placing orders.
    Any view class which needs to place an order must use this mixin.
    """

    def get_formatted_datetime(self, _datetime, device_type = None):

        if isinstance(_datetime, str):
            if device_type == 'iPhone' or device_type == 'Android':
                try:
                    formatted_datetime = pytz.timezone(settings.TIME_ZONE).localize(
                        datetime.strptime(_datetime, '%d %b %Y %H:%M'))
                except BaseException:
                    formatted_datetime = pytz.timezone(settings.TIME_ZONE).localize(
                        datetime.strptime(_datetime, '%d %b %Y %H:%M'))
            else:
                try:
                    formatted_datetime = pytz.timezone(settings.TIME_ZONE).localize(
                        datetime.strptime(_datetime, '%Y-%m-%d %H:%M'))
                except BaseException:
                    formatted_datetime = pytz.timezone(settings.TIME_ZONE).localize(
                        datetime.strptime(_datetime, '%d-%b-%Y %H:%M'))
        else:
            formatted_datetime = _datetime
        return formatted_datetime

    def create_event(self, order, status_info, address,
                     driver, driver_location, hub_associate=None,
                     remarks=None):
        # Store time and status details in events model
        Event.objects.create(
            status=status_info,
            order=order,
            address=address,
            driver=driver,
            current_location=driver_location,
            hub_associate=hub_associate,
            remarks=remarks
        )

    def create_utm_record(self, data):
        """
        Creates UTM tracking record
        :param data: JSON
        :return:
        """
        try:
            return UTMTracking.objects.create(**data)
        except InternalError as ie:
            logger.info('Failed to save UTM data %s.\n %s' % (data, ie))
            return None

    def get_aware_datetime(self, date_str):
        ret = parse_datetime(date_str)
        if not is_aware(ret):
            ret = make_aware(ret)
        return ret

    def get_initial_order_status(self):
        return overridable(
            'OSCARAPI_INITIAL_ORDER_STATUS',
            default=StatusPipeline.ORDER_NEW
        )

    def __update_items(self, order, items):
        for item in items:
            if isinstance(item, int):
                order.item_category.add(item)

        return order

    def dimension_validation_and_creation(self, weight, volume, length, breadth, height):
        '''
            Validates and sets the wieght,volume and dimension for order/orderlines
        '''
        message = None
        dimension = None
        dimension_data = {}

        weight = weight or 0.0
        volume = volume or 0.0
        length = length or 0.0
        breadth = breadth or 0.0
        height = height or 0.0

        if weight < 0 or volume < 0 or length < 0 or breadth < 0 or height < 0:
            message = 'Weight/Volume/Dimension parameters cannot be negative'
            return message,dimension_data

        if length != 0 or breadth !=0 or height != 0:
            dimension = Dimension(length=length, breadth=breadth,
                                    height=height,uom='cubic centimeter')
            dimension.save()

        if dimension and dimension.calculate_volume() != 0:
            volume = dimension.calculate_volume()

        dimension_data = {
            'weight' : weight,
            'volume' : volume,
            'length' : length,
            'breadth' : breadth,
            'height' : height,
            'dimension' : dimension
        }

        return message,dimension_data


    def _get_cost_per_labour(self, contract, no_of_labours_requested):
        labour_config = None
        if hasattr(contract, 'labourconfiguration'):
            labour_config = contract.labourconfiguration
        if not labour_config or labour_config.max_labours < \
                no_of_labours_requested:
            return False
        return labour_config.cost_per_labour

    def place_order(self, user, order_data, **kwargs):
        """
        Writes the order out to the DB including the payment models
        """
        if 'order_number' not in order_data:
            generator = OrderNumberGenerator()
            order_data['order_number'] = generator.order_number(
                order_data.get('order_type'))

        if 'status' not in order_data:
            status_info = self.get_initial_order_status()
        else:
            status_info = StatusPipeline.ORDER_NEW

        try:
            order = Order._default_manager.get(
                number=order_data.get('order_number'))
            if order:
                message = _(
                    'There is already an order with number %s' % order_data.get(
                        'order_number'))
                _out = {
                    'status': 'FAIL',
                    'message': message
                }
                logger.error(message)
                raise exceptions.ParseError(_out)
        except Order.DoesNotExist:
            pass

        if user and user.is_authenticated:
            customer = CustomerMixin().get_customer(user)
        else:
            customer = order_data.get('customer')
        order_data['customer'] = customer

        if not order_data['customer']:
            message = _('Can\'t create order without customer')
            _out = {
                'status': 'FAIL',
                'message': message
            }
            logger.error(message)
            raise exceptions.ParseError(_out)

        expected_delivery_time = None
        expected_delivery_hours = None

        if order_data.get('expected_delivery_hours', None):
            expected_delivery_hours = order_data.get('expected_delivery_hours')
        elif customer.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL:

            if customer.expected_delivery_hours:
                expected_delivery_hours = customer.expected_delivery_hours.hour + \
                    customer.expected_delivery_hours.minute / 60

        if expected_delivery_hours:
            expected_delivery_time = timezone.now() + timedelta(
                hours=expected_delivery_hours)
        else:
            expected_delivery_time = order_data.get('expected_delivery_time',None)

        shipping_address_input = order_data.get('shipping_address')
        if shipping_address_input:
            line1 = shipping_address_input.get('line1',None)
            if line1 and len(line1) > 510:
                message = _('Delivery address length should be less than 510')
                _out = {
                    'status': 'FAIL',
                    'message': message
                }
                logger.info(message)
                raise exceptions.ParseError(_out)

        with transaction.atomic():
            dimension_parms = [order_data.get('weight',0.0),
                                order_data.get('volume',0.0),
                                order_data.get('length',0.0),
                                order_data.get('breadth',0.0),
                                order_data.get('height',0.0)]
            em,dimension_data = self.dimension_validation_and_creation(*dimension_parms)

            if em:
                _out = {
                    'status': 'FAIL',
                    'message': em
                }
                logger.error(message)
                raise exceptions.ParseError(_out)

            address_type = order_data.get('address_type')
            shipping_address = self._update_shipping_address(shipping_address_input,address_type)
            shipping_address = ShippingAddress(**shipping_address)
            shipping_address = self.create_shipping_address(
                user, shipping_address)

            order_type = order_data.get(
                'order_type', settings.DEFAULT_ORDER_TYPE)

            city = order_data.get('city', None)
            if not city:
                # pickup_hub_ = order_data.get('pickup_hub', None)
                # if pickup_hub_ and order_type == settings.SHIPMENT_ORDER_TYPE:
                #     city = pickup_hub_.city
                # else:
                city = get_city(
                    order_type=order_type,
                    geopoint=shipping_address.geopoint,
                    postcode=shipping_address.postcode,
                    customer=customer,
                    contract_id=order_data.get('customer_contract', None)
                )
            if not city:
                message = _('Blowhorn doesn\'t provide service in this area')
                _out = {
                    'status': 'FAIL',
                    'message': message
                }
                logger.error(message)
                raise exceptions.ParseError(_out)

            pickup_address = order_data.get('pickup_address')
            if pickup_address:
                pickup_address = self._update_shipping_address(pickup_address)
                pickup_address = ShippingAddress(**pickup_address)
                pickup_address = self.create_shipping_address(
                    user, pickup_address)

            total_cost_excluding_tax = order_data.get('total', 0)
            TAX_ABSOLUTE = TAX_PERCENT * total_cost_excluding_tax * 0.01
            order_total = prices.Price(
                currency=settings.OSCAR_DEFAULT_CURRENCY,
                excl_tax=total_cost_excluding_tax,
                tax=TAX_ABSOLUTE
            )

            # shipping_method = Repository().get_default_shipping_method(
            #     basket=CustomBasket,
            #     shipping_addr=shipping_address,
            #     user=user
            # )
            # shipping_charge = shipping_method.calculate(CustomBasket)

            pickup_datetime = order_data.get('pickup_datetime')
            if pickup_datetime:
                pickup_datetime = self.get_formatted_datetime(
                    pickup_datetime,
                    order_data.get('device_type')
                )
            else:
                pickup_datetime = timezone.now()
            # Ok - everything seems to be in order, let's place the order
            # Add hub for shipment type order
            hub = order_data.get('hub', None)
            pickup_hub = order_data.get('pickup_hub', None)
            pref_vehicle_class = order_data.get('vehicle_class')
            is_exclude_vehicle_class = order_data.get(
                'is_exclude_vehicle_class', False)

            vehicle_class_preference = order_data.get(
                'vehicle_class_preference', None)
            if not is_exclude_vehicle_class and pref_vehicle_class \
                    and (not vehicle_class_preference or
                         isinstance(vehicle_class_preference, str)):
                vehicle_class_preference = get_vehicle_class_using_preference(
                        pref_vehicle_class, city)
                if not vehicle_class_preference:
                    message = _('Requested vehicle class not available')
                    _out = {
                        'status': 'FAIL',
                        'message': message
                    }
                    logger.error(message)
                    raise exceptions.ParseError(_out)

            contract_id = order_data.get('customer_contract', None)
            contract = None

            if contract_id:
                contract_list = ContractHelper().get_contract_from_id(contract_id)
                if contract_list.exists():
                    contract = contract_list[0]

            if not contract_id and order_type == settings.C2C_ORDER_TYPE:
                contract_type = order_data.get('contract_type')
                contract = ContractUtils.get_contract(
                    city, vehicle_class_preference.commercial_classification,
                    contract_type=contract_type)
                if not contract:
                    message = _(
                        'Cannot create order, contract not defined for '
                        'city: {city} , vehicle class:'
                        '{vehicle_class_preference}'.format(
                            city=city,
                            vehicle_class_preference=vehicle_class_preference
                        )
                    )
                    _out = {
                        'status': 'FAIL',
                        'message': message
                    }
                    logger.error(message)
                    raise exceptions.ParseError(_out)
                order_data['customer_contract'] = contract.id

            if not expected_delivery_time and hasattr(contract, 'expected_delivery_hours'):
                if contract.expected_delivery_hours:
                    eph_timedelta = timedelta(hours=contract.expected_delivery_hours.hour,
                                        minutes=contract.expected_delivery_hours.minute,
                                        seconds=contract.expected_delivery_hours.second,
                                        microseconds=contract.expected_delivery_hours.microsecond)

                    expected_delivery_time = timezone.now() + eph_timedelta

            labour_info = order_data.get('labour_info', {}) or {}
            no_of_labours_requested = labour_info.get('no_of_labours')
            if labour_info and no_of_labours_requested > 0 and \
                    not labour_info.get('labour_cost'):
                cost_per_labour = self._get_cost_per_labour(
                    contract, no_of_labours_requested)
                if cost_per_labour:
                    _out = {
                        'status': 'FAIL',
                        'message': _('Requested number of labours not available')
                    }
                    logger.error(_out)
                    raise exceptions.ParseError(_out)
                labour_info['labour_cost'] = cost_per_labour

            coupon_code = order_data.get('coupon_code', None)
            coupon = None
            if coupon_code:
                coupon = CouponMixin().get_coupon_from_code(coupon_code)

            # price calcualtions
            is_avg_price = False
            _basket_value = D(order_data.get('total_incl_tax', 0) or order_data.get('cash_on_delivery', 0.00))
            if _basket_value == 0 and contract.avg_basket_value > 0:
                _basket_value = contract.avg_basket_value
                is_avg_price = True

            _tax = D(order_data.get('tax', 0))
            order = OrderCreator().create_order_model(
                order_number=order_data.get('order_number'),
                is_avg_price=is_avg_price,
                total_payable=order_data.get('total_payable', 0),
                total_incl_tax=_basket_value,
                tax=_tax,
                tip=order_data.get('tip', 0),
                discount=order_data.get('discount', 0),
                cash_on_delivery=order_data.get('cash_on_delivery', 0.00),
                total_excl_tax=order_data.get('total_excl_tax', 0) or (_basket_value - _tax) or 0,
                shipping_charges=order_data.get('shipping_charges', 0),
                user=user if not user.is_anonymous else customer.user,
                shipping_address=shipping_address,
                pickup_address=pickup_address,
                billing_address=None,
                total=order_total,
                pickup_datetime=pickup_datetime,
                date_placed=order_data.get('date_placed', timezone.now()),
                status=order_data.get('status', status_info),
                guest_email=order_data.get('guest_email') or '',
                reference_number=order_data.get('reference_number') or '',
                customer_reference_number=order_data.get(
                    'customer_reference_number') or '',
                order_type=order_type,
                priority=order_data.get('priority'),
                items=order_data.get('items') or [''],
                customer=customer,

                shipment_info=order_data.get('shipment_info'),
                city=city,
                source_city=order_data.get('source_city', None),
                expected_delivery_time=expected_delivery_time,
                exp_delivery_start_time=order_data.get(
                    'exp_delivery_start_time'),
                hub=hub,
                pickup_hub=pickup_hub,
                vehicle_class_preference=vehicle_class_preference,
                weight=dimension_data.get('weight', 0.00),
                volume=dimension_data.get('volume', 0.00),
                dimension=dimension_data.get('dimension',None),
                delivery_instructions=order_data.get('delivery_instructions', ''),
                customer_contract_id=order_data.get('customer_contract', None),
                device_type=order_data.get('device_type') or '',
                booking_type=order_data.get('booking_type') or '',
                auto_debit_paytm=order_data.get('auto_debit_paytm', False),
                created_by_driver=order_data.get('created_by_driver', False),
                estimated_cost=order_data.get('estimated_cost') or 0.0,
                estimated_distance_km=order_data.get(
                    'estimated_distance_km') or 0.0,
                payment_status=order_data.get('payment_status',
                                              PAYMENT_STATUS_UNPAID),
                payment_mode=order_data.get('payment_mode', None),
                coupon=coupon,
                fixed_price=order_data.get('fixed_price', 0),
                customer_category=customer.customer_category,
                floor_number=order_data.get('floor_number', -1),
                number_of_items=order_data.get('number_of_items', 1),
                estimated_time_in_mins=order_data.get('estimated_time_in_mins', 0.0),
                estimated_cost_lower=order_data.get('estimated_cost_lower', 0.0),
                estimated_cost_upper=order_data.get('estimated_cost_upper', 0.0),
                eta_in_mins=order_data.get('eta_in_mins', ''),
                is_pod_required=order_data.get('is_pod_required', False),
                is_offline_order=order_data.get('is_offline_order', False),
                has_pickup=order_data.get('has_pickup'),
                otp=order_data.get('otp'),
                bill_number=order_data.get('bill_number', None),
                bill_date=order_data.get('bill_date', None),
                order_mode=order_data.get('order_mode', None),
                store_code=order_data.get('store_code', None),
                remarks=order_data.get('remarks', None),
                return_order=order_data.get('return_order', False),
                division=order_data.get('division', None),
                source=order_data.get('source'),
                delivery_mode=order_data.get('delivery_mode', None),
                shipping_gstin=order_data.get('shipping_gstin', None),
                pickup_gstin=order_data.get('pickup_gstin', None),
                is_secondary_region=order_data.get('is_secondary_region', False),
                carrier_mode=order_data.get('carrier_mode'),
                **kwargs
            )
            logger.info('Order created:- "%s"', order)
            if order_data.get('lines', None):
                order_lines = order_data['lines']
                for line in order_lines:
                    sku = SKU.objects.get(name=line.get('sku'))
                    OrderLine.objects.create(order=order, sku=sku,
                                                weight=line.get('weight',0.0),
                                                volume=line.get('volume',0.0),
                                                dimension=line.get('dimension',0.0),
                                                quantity=line.get('quantity')
                                            )

            order = self.__update_items(order, order_data.pop('items', []))
            self.create_event(order, status_info, pickup_address, None, None)
            logger.info('Event "%s" created for order:- "%s"',
                        status_info, order)

            waypoints = order_data.get('waypoints', [])
            for waypoint in waypoints:
                self.add_waypoint(order, waypoint, user)

            sequence_id = 1
            if waypoints:
                sequence_id += len(waypoints)
            # Adding Shipping address as a Waypoint
            self.add_waypoint(order, order.shipping_address, user, sequence_id)

            estimated_distance = 0
            if order_type == settings.C2C_ORDER_TYPE:
                pickup_coordinates = order.pickup_address.geopoint

                for waypoint in order.waypoint_set.all():
                    stop_pickup_coordinates = waypoint.shipping_address.geopoint
                    # calculate distance between pickup and waypoints
                    eta_between_points = dist_between_2_points(
                        pickup_coordinates, stop_pickup_coordinates)

                    estimated_distance += eta_between_points

                    # waypoint.total_time = eta_between_points.duration_s / \
                    #     60 if eta_between_points.duration_s else 0
                    waypoint.estimated_distance = eta_between_points if eta_between_points else 0
                    waypoint.save()
                    pickup_coordinates = stop_pickup_coordinates
                if not order.estimated_distance_km:
                    order.estimated_distance_km = estimated_distance
                # if not order.estimated_cost:
                #     minutes = 0
                #     time_amt = 0
                #     dist_amt = 0
                #     fixed_amount = 0

                estimated_distance = order.estimated_distance_km
                minutes = (float(
                    estimated_distance) / AVERAGE_VEHICLE_SPEED_KMPH) * 60
                if contract_id:
                    contract = ContractHelper().get_contract_from_id(
                        contract_id)
                else:
                    contract = ContractHelper().get_contract(
                        city, vehicle_class_preference)
                    #
                    sell_rate = ContractHelper().get_sell_rate(
                        contract[0].contractterm_set.all(), timezone.now().date())
                    slabs = sell_rate.sellvehicleslab_set.all()

                    if estimated_distance:
                        dist_amt, cal = PaymentHelper()._get_amount_for_given_slabs(
                            None, slabs, estimated_distance)

                    if minutes:
                        loading_unloading_time = LOADING_TIME + UNLOADING_TIME
                        time_amt, cal = PaymentHelper()._get_amount_for_given_slabs(
                            None, slabs, minutes, loading_unloading_time)
                        fixed_amount = float(sell_rate.base_pay)

                    total_ = fixed_amount + dist_amt + time_amt
                    order.estimated_cost = total_
                order.customer_contract = contract[0]
                order.save()

                # DriverMixin().get_nearest_available_driver(
                #     order.pickup_address.geopoint, order)

                # Storing the information about labour
                if labour_info and no_of_labours_requested > 0:
                    labour_info['order'] = order
                    Labour.objects.create(**labour_info)

                # Add payment Source for the order
                # payment_type = order_data.get('payment_type')
                # PaymentMixin().create_source(order=order,
                #                              payment_type=payment_type)

            if coupon:
                coupon.increase_usage_count()
                CouponMixin().create_redeem_history(order, customer, coupon)

            # order.calculate_and_update_cost()
            # order.save()
            return order

    def get_updated_address(self, address, postal_code, lat, lng, **kwargs):
        reference_number = kwargs.get('reference_number', None)
        customer_name = kwargs.get('customer_name', None)
        customer_contact = kwargs.get('customer_contact', None)
        alternate_customer_contact = kwargs.get('alternate_customer_contact', None)
        pin_number = kwargs.get('pin_number', None)

        updated_address = {
            'line1': address,
            'postcode': postal_code,
        }

        if customer_name:
            updated_address['first_name'] = customer_name

        if customer_contact:
            updated_address['phone_number'] = customer_contact

        if alternate_customer_contact:
            updated_address['alternate_phone_number'] = alternate_customer_contact

        if pin_number:
            updated_address['pin_number'] = pin_number

        if not lat or not lng:
            success, location = GoogleMaps.get_lat_lng(address)
            if success:
                lat = location.get('lat')
                lng = location.get('lng')
            elif reference_number:
                get_geopoint_from_delivered_addresses.delay(
                    postal_code, reference_number, address)

        if lat and lng:
            geopoint = CommonHelper.get_geopoint_from_latlong(
                lat, lng)
            geopoint = {'geopoint': geopoint}
            updated_address.update(geopoint)

        return updated_address

    def create_shipping_address(self, user, shipping_address):
        """
        Create and return the shipping address for the current order.
        Compared to self.get_shipping_address(), ShippingAddress is saved and
        makes sure that appropriate UserAddress exists.
        """
        if not shipping_address:
            return None
        shipping_address.save()
        return shipping_address

    def update_address_book(self, user, addr):
        """
        Update the user's address book based on the new shipping address
        """
        try:
            user_addr = user.addresses.get(
                hash=addr.generate_hash())
        except ObjectDoesNotExist:
            # Create a new user address
            user_addr = UserAddress(user=user)
            addr.populate_alternative_model(user_addr)
        if isinstance(addr, ShippingAddress):
            user_addr.num_orders += 1
        user_addr.save()

    def _update_shipping_address(self, shipping_address, address_type=None):
        if shipping_address.get('country'):
            country = shipping_address['country']
        else:
            country = settings.COUNTRY_CODE_A2
        shipping_address['country'] = Country.objects.filter(
            iso_3166_1_a2=country).get()

        if address_type:
            shipping_address['address_type'] = address_type

        phone_number = shipping_address.get('phone_number')
        is_valid_phone_string = re.match('^\d+$',str(phone_number))
        if phone_number and is_valid_phone_string:
            shipping_address['phone_number'] = \
                str(PhoneNumber.from_string(
                    str(phone_number), settings.COUNTRY_CODE_A2))

        alternate_phone_number = shipping_address.get('alternate_phone_number')
        is_valid_alternate_phone_string = re.match('^\d+$', str(alternate_phone_number))
        if alternate_phone_number and is_valid_alternate_phone_string:
            shipping_address['alternate_phone_number'] = \
                str(PhoneNumber.from_string(
                    str(alternate_phone_number), settings.COUNTRY_CODE_A2))
        if 'geopoint' not in shipping_address:
            if 'latitude' in shipping_address and 'longitude' in shipping_address:
                shipping_address['geopoint'] = CommonHelper.get_geopoint_from_latlong(
                    shipping_address['latitude'],
                    shipping_address['longitude']
                )
                del shipping_address['latitude']
                del shipping_address['longitude']
            else:
                shipping_address['geopoint'] = \
                    CommonHelper.get_geopoint_from_location_text(
                        shipping_address)
        return shipping_address

    def add_waypoint(self, order, shipping_address, user, sequence_id=1):
        if 'ShippingAddress' != type(shipping_address).__name__:
            sequence_id = shipping_address.pop('sequence_id', sequence_id)
            shipping_address = self._update_shipping_address(shipping_address)
            shipping_address = ShippingAddress(**shipping_address)
            shipping_address = self.create_shipping_address(
                user, shipping_address)

        data = {
            'sequence_id': sequence_id,
            'order': order,
            'shipping_address': shipping_address,
        }

        waypoint = WayPoint(**data)
        waypoint.save()

        logger.info('Waypoint %d Addition Successful', waypoint.id)

    def create_trip_stops_c2c(
            self, driver,
            order):
        # function to create a new trip for driver and assigning driver to trip
        # as well as all orders

        contract = order.customer_contract

        # Create a trip and assign to driver
        trip = TripMixin().create_trip(
            driver=driver,
            order=order,
            estimated_distance_kms=order.estimated_distance_km if order else 0,
            contract=contract,
            blowhorn_contract=driver.contract,
            invoice_driver=driver
        )

        if trip:

            # c2c case
            response = self.add_c2c_order_as_trip_stop(
                order_id=order.id, trip=trip)

            if not response:
                logger.info('Trip Stop Creation Failure')
                logger.info('Trip:- "%s" Deletion Successful', trip)
                trip.delete()
                return False

        # returns json response
        trip_details = TripMixin().get_order_response(trip)

        return trip_details

    def create_trip_stops_b2c(
            self, driver=None,
            items=None,
            geopoint=None, trip_id=None):
        # function to create a new trip for driver and assigning driver to trip
        # as well as all orders
        trip = None

        if driver and not trip_id:
            """
            drivers with:- 1.NEW TRIP(after scanning items if app crashes,
                                driver won't be allowed to attempt a new trip)
                           2.TRIP IN PROGRESS won't be allowed to create a new trip
            As they are already related to a trip

            for flexibility, A driver who has cancelled a trip and there are orders
                             yet to be delivered, In that case, the SAME driver or
                             a DIFFERENT driver will be able to create a new trip
                             with those pending orders
            """
            trip = Trip.objects.filter(
                driver=driver,
                status__in=[StatusPipeline.TRIP_NEW,
                            StatusPipeline.TRIP_IN_PROGRESS,
                            StatusPipeline.DRIVER_ACCEPTED,
                            StatusPipeline.TRIP_ALL_STOPS_DONE],
                order=None
            ).first()

            if trip:
                # internet spikes
                return TripMixin().get_order_response(trip)

            # Validate Orders exists
            if not items:
                logger.error(
                    'No Orders are there to create a trip.')
                return False

        if not trip_id:
            # Create a trip and assign to driver
            trip = TripMixin().create_trip(driver=driver, blowhorn_contract=driver.contract)
        else:
            trip = Trip.objects.filter(pk=trip_id).first()

        if trip:
            response = None
            # Shipment case
            # Add each order waypoints to Trip Stop

            for item in items:
                attempted_packages = False
                order_qs = Order.objects.filter(number=item['number'])

                if order_qs.exists() and order_qs.count() < 2:
                    order = order_qs.first()

                    """
                    In case of cancellation of trip, Pending Orders status
                    will be OUT_FOR_DELIVERY,hence if new/same driver is
                    scanning the pending orders, new trip/stops will be
                    created and order will be updated with that driver

                    """
                    if order.status in [
                            StatusPipeline.ORDER_RETURNED,
                            StatusPipeline.UNABLE_TO_DELIVER
                    ]:
                        """
                        case: Before cancelling a trip, if the driver
                        has(for eg.) 3 returned/unable_to_deliver
                        orders,2 yet to be attempted/delivered orders
                        and 5 orders he has already delivered...
                        The second driver now reaches at his location
                        and scans 5 orders(2 yet to be delivered/attempted
                        and 3 returned/unable_to_deliver orders)...
                        Hence in that case, trip for second driver will be
                        generated only for those 2 yet_to_be_delivered
                        orders and the second driver will be assigned
                        to those 3 returned/unable_to_deliver orders
                        """
                        status_info = (
                            'Assigned-Order-from-Driver-%s-to-%s'
                        ) % (order.driver,
                             driver)

                        self.create_event(
                            order=order, status_info=status_info,
                            address=order.pickup_address, driver=driver,
                            driver_location=geopoint)
                        attempted_packages = True
                        response = True
                    else:
                        if order.status not in StatusPipeline.ORDER_SCANNING_STATUSES_CHECK:

                            order_status = (
                                '%s-from-%s-to-%s') % (
                                StatusPipeline.INVALID_ORDER_TRANSITION,
                                order.status,
                                StatusPipeline.OUT_FOR_DELIVERY)

                            geopoint = geopoint if geopoint else None

                            self.create_event(
                                order=order, status_info=order_status,
                                address=order.pickup_address, driver=driver,
                                driver_location=geopoint)
                            logger.info(
                                'Event "%s" created for order:- "%s"',
                                order_status, order)

                        response = self.add_b2c_order_as_trip_stop(
                            order=order, trip=trip)

                if response:
                    # order table updated with the driver scanning it
                    if trip_id:  # scanning packages in between the trip
                        if trip.status == StatusPipeline.TRIP_IN_PROGRESS:
                            order_status = StatusPipeline.OUT_FOR_DELIVERY
                        elif trip.status == StatusPipeline.TRIP_NEW:
                            order_status = StatusPipeline.DRIVER_ASSIGNED
                        order_qs.update(
                            status=order_status,
                            driver=driver
                        )
                        self.create_event(
                            order=order, status_info=order_status,
                            address=order.pickup_address, driver=driver,
                            driver_location=geopoint)
                    elif not attempted_packages:

                            order_qs.update(
                                status=StatusPipeline.DRIVER_ASSIGNED,
                                driver=driver
                            )
                            self.create_event(
                                order=order, status_info=StatusPipeline.DRIVER_ASSIGNED,
                                address=order.pickup_address, driver=driver,
                                driver_location=geopoint)

                else:
                    logger.info('Trip Stop Creation Failure')
                    logger.info('Trip:- "%s" Deletion Successful', trip)
                    trip.delete()
                    return False
            if not response:
                logger.info('Trip Stop Creation Failure')
                logger.info('Trip:- "%s" Deletion Successful', trip)
                trip.delete()
                return False

            # returns json response
            if trip.meter_reading_start:
                if not trip.stops.filter(status=StatusPipeline.TRIP_IN_PROGRESS).exists():
                    stop = Stop.objects.filter(trip=trip, status=StatusPipeline.TRIP_NEW).first()
                    stop.status = StatusPipeline.TRIP_IN_PROGRESS
                    stop.start_time = timezone.now()
                    stop.save()
            Trip.objects.filter(pk=trip.id).update(
                hub=order.hub,
                assigned=trip.stops.count(),
                customer_contract=order.customer_contract,
                trip_workflow=order.customer_contract.trip_workflow)

        trip_details = TripMixin().get_order_response(trip)

        if trip_details['trip_stops'] == []:

            logger.info(
                'Driver:- "%s" scanned all returned/unable-to-deliver '
                'orders and is returning to the hub',
                driver
            )
            logger.info('Trip:- "%s" Deletion Successful', trip)
            trip.delete()
        elif geopoint:
            # create an app event for TRIP_STARTED and capture in
            # driver_activity history also
            TripMixin().create_app_event(
                driver=driver,
                trip=trip,
                geopoint=geopoint,
                event=StatusPipeline.TRIP_NEW
            )

            logger.info(
                'App Event "%s" created for trip:- "%s" with driver:- "%s-%s"',
                StatusPipeline.TRIP_NEW, trip, driver.name,
                driver.user.national_number)

        return trip_details

    def add_b2c_order_as_trip_stop(self, trip, order):
        # Assigns each order waypoints as Trip Stops

        if order:
            # Get all waypoints for this order
            waypoint_qs = WayPoint.objects.filter(order=order).values('id')

            waypoint_len = len(waypoint_qs)

            if waypoint_len == 0:
                logger.error('No waypoints are there for order "%s"', order)
                return False
            if waypoint_len > 1:
                logger.error(
                    'Multiple waypoints are there for order "%s"', order)
                return False

            waypoint_id = waypoint_qs[0]['id']
            if not Stop.objects.filter(
                waypoint_id=waypoint_id,
                trip=trip
            ).exists():
                # single waypoint in case of SHIPMENT ORDERS
                # single/multiple waypoints in case of C2C ORDERS
                trip_stop = TripMixin().create_trip_stop(
                    trip, waypoint_id, order)
                if not trip_stop:
                    return False

                logger.info(
                    'Assigned order:- "%s", waypoint:- %d to trip_stop:- %d',
                    order, waypoint_id, trip_stop.id)

        return True

    def add_c2c_order_as_trip_stop(self, trip, order_id):
        # Assigns each order waypoints as Trip Stops
        if order_id is not None:
            order = get_object_or_404(Order, pk=order_id)

        if order:
            # Get all waypoints for this order
            waypoint_queryset = WayPoint.objects.filter(order=order)
            waypoint_queryset = waypoint_queryset.prefetch_related('stops')

            if 0 == waypoint_queryset.count():
                logger.error('No waypoints are there for order "%s"', order)
                return False

            for waypoint in waypoint_queryset:

                stops = sorted(
                    waypoint.stops.all().values('id', 'status'),
                    key=lambda x: x['id']
                )
                stop = stops[-1] if stops else None

                if stop and stop['status'] == StatusPipeline.TRIP_COMPLETED:
                    continue

                if not Stop.objects.filter(
                    waypoint=waypoint,
                    trip=trip
                ).exists():
                    # single waypoint in case of SHIPMENT ORDERS
                    # single/multiple waypoints in case of C2C ORDERS
                    trip_stop = TripMixin().create_trip_stop(
                        trip, waypoint.id, order, waypoint.sequence_id)
                    if not trip_stop:
                        return False

                    logger.info(
                        'Assigned order:- "%s", waypoint:- %d to trip_stop:- %d',
                        order, waypoint.id, trip_stop.id)

        return True

    # dont USING THIS FUNCTION
    def get_trip_for_order(self, order):
        order_waypoint = order.waypoint_set.first()
        if order_waypoint:
            trip_stop = Stop._default_manager.filter(
                waypoint=order_waypoint).first()
            if trip_stop:
                return trip_stop.trip

        return None

    def get_city(self, shipping_address):
        city = LegacyAdapter.get_city_from_geopoint(shipping_address.geopoint)
        return city

    def get_sorted_orders(self, orders, status, reversed=False):
        filtered = [x for x in orders if x.get('status') == status]
        return sorted(filtered, key=lambda x: x.get('date_sortable'), reverse=reversed)

    def get_summary(self, orders):
        total_cost = 0
        total_distance = 0
        number_trips = 0
        datewise_data = {}
        for o in orders:
            total_cost += o.total_incl_tax
            date = o.pickup_datetime.strftime('%d-%b-%Y')
            date_sortable = o.pickup_datetime.strftime('%Y-%m-%d')

            distance = 0
            for trip in o.trip_set.all():
                distance = round(trip.total_distance,
                                 1) if trip.total_distance else 0
                total_distance += distance
            number_trips += 1
            if date not in datewise_data:
                datewise_data[date] = {
                    'date': date,
                    'date_sortable': date_sortable,
                    'trips': 0,
                    'distance': 0,
                    'cost': 0,
                }

            d = datewise_data[date]
            d['trips'] = d['trips'] + 1
            d['distance'] = d['distance'] + distance
            d['cost'] = d['cost'] + o.total_incl_tax

        summary = {
            "total_cost": total_cost,
            "total_distance": total_distance,
            "number_trips": number_trips,
            "data":
                sorted(
                    datewise_data.values(),
                    key=lambda x: x.get('date_sortable'))
        }
        return summary

    def save_location(self, address, location):
        address.geopoint = CommonHelper.get_geopoint_from_latlong(
            location.get('_lat'),
            location.get('_lng')
        )
        address.save()

class ItemCategoryMixin(object):

    def get_item_categories(self):
        """
        :return: 'pk', 'category', 'is_tax_exempt'
        """
        return ItemCategory.objects.filter(is_active=True).values('pk', 'category', 'is_tax_exempt')


class OrderlineMixin():

    def get_orderlines(self, order_number):
        orderlines = OrderLine.objects.filter(order__number=order_number)
        data = []

        for obj in orderlines:
            temp = {
                'item_name': obj.sku.name if obj.sku else obj.wh_sku.name,
                'hsn_code' : obj.hsn_code,
                'item_code' : obj.item_code,
                'item_category' : obj.item_category,
                'item_quantity': obj.quantity,
                'item_price_per_each' : obj.each_item_price,
                'total_item_price' : obj.total_item_price,
                'cgst' : obj.cgst,
                'sgst' : obj.sgst,
                'igst' : obj.igst,
                'weight' : obj.weight,
                'volume' : obj.volume,
                'id' : obj.id,
                'sku' : obj.sku.name if obj.sku else obj.wh_sku.name,
                'each_item_price' : obj.each_item_price,
                'quantity': obj.quantity,
                'uom': obj.uom,
                'discount_type': obj.discount_type,
                'discounts': obj.discounts
            }
            data.append(temp)

        return data


    def dimension_validation_and_creation(self, weight, volume, length, breadth, height):
        '''
            Validates and sets the wieght,volume and dimension for order/orderlines
        '''
        dimension = None
        dimension_data = {}
        weight = weight or 0.0
        volume = volume or 0.0
        length = length or 0.0
        breadth = breadth or 0.0
        height = height or 0.0

        dimension_data = {
            'weight' : weight,
            'volume' : volume,
            'height' : height,
            'length' : length,
            'breadth' : breadth

        }

        for attribute,value in dimension_data.items():
            if value and type(value) == str:
                if not re.match('^([+-])*\d+(\.)*\d*$',value):
                    _out = {
                        'status': 'FAIL',
                        'message': '{} parameter in items should contain numeric characters only'.format(attribute.capitalize())
                    }
                    raise exceptions.ParseError(_out)
            dimension_data[attribute] = float(value)

            if dimension_data[attribute] < 0:
                _out = {
                        'status': 'FAIL',
                        'message': '{} parameter in items cannot be negative'.format(attribute.capitalize())
                }
                raise exceptions.ParseError(_out)


        dimension = Dimension(length=dimension_data['length'],
                                breadth=dimension_data['breadth'],
                                height=dimension_data['height'],
                                uom='cubic centimeter')
        dimension.save()
        dimension_data['dimension'] = dimension

        if dimension and dimension.calculate_volume() != 0:
            volume = dimension.calculate_volume()
            dimension_data['volume'] = volume

        return dimension_data

    def create_kit_skus(self, kit_sku_data, order, customer, source=None):
        kit_sku_out = []
        logging.info("This is the kit_skus data: %s", kit_sku_data)

        for data in kit_sku_data:
            if not data.get('name'):
                _out = {
                    'status': 'FAIL',
                    'message': 'Kit SKU Name is mandatory'
                }
                raise exceptions.ParseError(_out)

            if data.get('item_name') and len(data.get('item_name')) > 255:
                _out = {
                    'status': 'FAIL',
                    'message': 'Item\'s name length should be less than or equal to 255'
                }
                raise exceptions.ParseError(_out)

            if data.get('description') and len(data.get('description')) > 256:
                _out = {
                    'status': 'FAIL',
                    'message': 'Description length should be less than or equal to 256'
                }
                raise exceptions.ParseError(_out)

            if not data.get('item_details'):
                _out = {
                    'status': 'FAIL',
                    'message': 'Item details are mandatory for Kit SKUs'
                }
                raise exceptions.ParseError(_out)

            item_details = data.get('item_details')
            name = data.get('name')
            description = data.get('description', None)
            total_item_price = data.get('total_item_price', 0.0)
            quantity = data.get('item_quantity', 0)

            whkitsku = WhKitSku.objects.create(name=name, description=description, total_item_price=total_item_price,
                                               quantity=quantity)
            if not whkitsku:
                _out = {
                    'status': 'FAIL',
                    'message': 'Failed to create Kit Skus'
                }
                raise exceptions.ParseError(_out)
            item_details_out = self.create_orderlines(item_details, order, customer, source, whkitsku)
            kitsku_dict = {
                'name': whkitsku.name,
                'description': whkitsku.description,
                'item_details': item_details_out
            }
            kit_sku_out.append({
                whkitsku.id: kitsku_dict
            })
        return kit_sku_out

    def create_orderlines(self, orderline_data, order, customer, source=None, kit_sku=None):
        orderline_list = []
        orderline_out = {}
        logging.info("This is the orderline_data : %s", orderline_data)
        orderline_history_list=[]
        shopify_missing_summary = {}
        total_basket_value = 0

        for data in orderline_data:
            if not data.get('item_name'):
                _out = {
                    'status': 'FAIL',
                    'message': 'Item\'s name is mandatory'
                }
                raise exceptions.ParseError(_out)

            if data.get('item_name') and len(data.get('item_name')) > 255:
                _out = {
                    'status': 'FAIL',
                    'message': 'Item\'s name length should be less than or equal to 255'
                }
                raise exceptions.ParseError(_out)

            if not data.get('item_quantity'):
                _out = {
                    'status': 'FAIL',
                    'message': 'Item\'s quantity is mandatory'
                }
                raise exceptions.ParseError(_out)

            uom = CommonHelper.get_uom(data.get('uom', 'kg') or 'kg')
            if not uom:
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid UOM. Available options: %s' % ','.join(CommonHelper.get_uom_list())
                }
                raise exceptions.ParseError(_out)

            dim_parms = [data.get('weight',0.0),
                            data.get('volume',0.0),
                            data.get('length',0.0),
                            data.get('breadth',0.0),
                            data.get('height',0.0)]

            dimension_data = self.dimension_validation_and_creation(*dim_parms)
            quantity = data['item_quantity']
            # TODO: To be implemented at later stage
            # name = (data['item_name'].title())
            # name = ''.join(name.split(' '))
            wh_sku = None
            sku = None
            shopify_variant = data.get('shopify_variant_id', None)
            # is_whsku = data.get('is_whsku', False)

            # if is_whsku or kit_sku:
            quantity = int(data['item_quantity']) * kit_sku.quantity if kit_sku else quantity
            if shopify_variant:
                wh_sku = WhSku.objects.filter(shopify_variant_id=shopify_variant).first()

                if wh_sku and not wh_sku.requires_shipping:
                    continue

            else:
                wh_sku = WhSku.objects.filter(name=data['item_name'], client__customer=customer).first()

            # if not wh_sku:
            sku, _ = SKU.objects.get_or_create(name=data['item_name'])
            SKUCustomerMap.objects.get_or_create(sku=sku, customer=customer)

            # if shopify_variant:
            #     _info = {
            #         'shopify_variant_id' : data.get('shopify_variant_id', None),
            #         'shopify_product_id' : data.get('shopify_product_id', None)
            #     }
            #     shopify_missing_summary[wh_sku.id] = _info

            partner_id = None
            product_id = data.get("product_id", None)
            stock_id = data.get('stockrecord_id', None)

            hsn_code = data.get("hsn_code", None)
            mrp = data.get("mrp", None) or data.get("total_amount_incl_tax", None)
            if mrp:
                mrp = self._validate_float(mrp, "mrp")

            discounts = data.get("discounts", 0)
            if discounts:
                discounts = self._validate_float(discounts, "discounts")
            discount_type = data.get('discount_type', DISCOUNT_TYPE_FIXED)
            if discount_type not in [DISCOUNT_TYPE_FIXED, DISCOUNT_TYPE_PERCENTAGE]:
                _out = {
                    'status': 'FAIL',
                    'message': 'Discount Type should be fixed or percentage'
                }
                raise exceptions.ParseError(_out)
            if discount_type == DISCOUNT_TYPE_PERCENTAGE and not (0 <= float(discounts) <= 100):
                _out = {
                    'status': 'FAIL',
                    'message': 'Discount should be between 0 to 100 for percentage discount type.'
                }
                raise exceptions.ParseError(_out)

            cgst = data.get("cgst", 0)
            if cgst:
                cgst = self._validate_float(cgst, "cgst")

            sgst = data.get("sgst", 0)
            if sgst:
                sgst = self._validate_float(sgst, "sgst")

            igst = data.get("igst", 0)
            if igst:
                igst = self._validate_float(igst, "igst")

            """
            Unit Price: each_item_price,mrp (same as each_item_price)
            Discount: discounts
            Tax: cgst, igst, sgst

            Assuming -> total_item_price coming currently via diff sources includes the tax computation done
            total_item_price = total_item_price - total_tax
            """
            # get the input total price n unit price
            _total_item_price = self._validate_float(data.get('total_item_price', 0), 'total_item_price')
            _per_item_price = self._validate_float(data.get('item_price_per_each', 0), 'item_price_per_each')
            # total tax
            _total_tax = cgst + sgst + igst
            # total line amount is either (total item price or (unit_price * quantity + tax)) - discount
            _line_total_amount = (_total_item_price or ((int(data['item_quantity']) * _per_item_price) + _total_tax))

            _discount = discounts if discount_type == DISCOUNT_TYPE_FIXED else ((discounts * 100) / _line_total_amount)
            _line_total_amount = _line_total_amount - _discount

            total_basket_value += _line_total_amount

            # If the given WH Sku name is not present in our system
            if not shopify_variant and not wh_sku:
                pack_config = customer.packconfig_set.first()
                client = customer.client_set.first()
                supplier = customer.supplier_set.first()

                if not pack_config:
                    from blowhorn.wms.models import WmsConstants
                    from blowhorn.wms.submodels.sku import PackConfig
                    default_pack_config_id = WmsConstants().get('DEFAULT_PACK_CONFIG_ID', 13)
                    pack_config = PackConfig.objects.filter(id=int(default_pack_config_id)).first()
                if not client:
                    from blowhorn.wms.submodels.location import Client
                    name = 'Default Client - %s' % customer.name
                    client = Client.objects.create(name=name, customer=customer)
                if not supplier:
                    from blowhorn.wms.submodels.inbound import Supplier
                    name = 'Default Supplier - %s' % customer.name
                    supplier = Supplier.objects.create(name=name, customer=customer)

                wh_sku = WhSku.objects.create(
                    name=data['item_name'],
                    pack_config=pack_config,
                    client=client,
                    description=data.get('description', None),
                    supplier=supplier,
                    each_weight=dimension_data['weight'],
                    each_volume=dimension_data['volume'],
                    uom=uom,
                    mrp=mrp,
                    discounts=discounts,
                    discount_type=discount_type,
                    has_expiry=data.get('has_expiry', False),
                    shelf_life=data.get('shelf_life', None),
                    shelf_life_uom=data.get('shelf_life_uom', None)
                )

            orderline_list.append(OrderLine(
                order=order,
                sku=sku,
                wh_sku=wh_sku,
                wh_kit_sku=kit_sku,
                quantity=quantity,
                client_id=data.get('client_id', None),
                tracking_level=data.get('tracking_level',None),
                uom=uom,
                item_code=data.get("item_code", None),
                item_sequence_no=data.get("item_sequence_no", None),
                item_category=data.get("item_category", None),
                brand=data.get("brand", None),
                product_id=product_id,
                partner_id=partner_id,
                weight=dimension_data['weight'],
                volume=dimension_data['volume'],
                dimension=dimension_data['dimension'],
                hsn_code=hsn_code,
                # only being used in flash sale admin action n that action not getting used
                cost_price=0,
                each_item_price=_per_item_price,
                mrp=_per_item_price,
                discounts=discounts,
                cgst=cgst, sgst=sgst, igst=igst,
                total_item_price=_line_total_amount - _total_tax,
                total_amount_incl_tax=_line_total_amount,
                discount_type=discount_type
            ))

        # if shopify_missing_summary:
        #     _summary = json.dumps(shopify_missing_summary)
        #     sync_specific_inventory.delay(order.id, _summary)

        if source:
            orderlinehistory_batch = OrderLineHistoryBatch(description='Add request from %s' % str(order.customer),
                                                           number_of_entries=len(orderline_data),
                                                           request=json.dumps(orderline_data))
            orderlinehistory_batch.save()

        orderlines = OrderLine.objects.bulk_create(orderline_list)
        if total_basket_value > 0 and (order.total_incl_tax == 0 or order.is_avg_price):
            _temp = total_basket_value + order.shipping_charges + order.tip
            Order.objects.filter(id=order.id).update(total_incl_tax=_temp, is_avg_price=False)

        for obj in orderlines:
            temp = {
                'item_name': obj.sku.name if obj.sku else obj.wh_sku.name,
                'hsn_code' : obj.hsn_code,
                'item_code' : obj.item_code,
                'item_category' : obj.item_category,
                'item_quantity': obj.quantity,
                'item_price_per_each' : obj.each_item_price,
                'total_item_price' : obj.total_item_price,
                'cgst' : obj.cgst,
                'sgst' : obj.sgst,
                'igst' : obj.igst,
                'weight' : obj.weight,
                'volume' : obj.volume,
                'uom' : obj.uom.upper() if obj.uom else '',
                'discount_type': obj.discount_type,
                'discounts': obj.discounts
            }

            other_format = {
                'id' : obj.id,
                'sku' : obj.sku.name if obj.sku else obj.wh_sku.name,
                'each_item_price' : obj.each_item_price,
                'quantity': obj.quantity,
            }

            if source == 'dashboard':
                temp.update(other_format)

            if source and obj.sku:
                orderline_history_list.append(OrderLineHistory(
                    orderline_batch=orderlinehistory_batch,
                    order=obj.order,
                    sku=obj.sku,
                    quantity=obj.quantity,
                    total_item_price=obj.total_item_price,
                    each_item_price=obj.each_item_price,
                    source='%s-%s' % ('create', source)
                ))

            orderline_out[obj.id] = temp

        if source:
            OrderLineHistory.objects.bulk_create(orderline_history_list)

        return orderline_out

    def update_orderlines(self, orderline_data, order, source=None):

        for value in orderline_data:
            if not value.get('item_quantity'):
                _out = {
                    'status': 'FAIL',
                    'message': 'Item\'s quantity is mandatory'
                }
                raise exceptions.ParseError(_out)

        orderline_history_list=[]
        try:
            orderlinehistory_batch = OrderLineHistoryBatch(description='Update request from %s' % str(order.customer),
                                                           number_of_entries=len(orderline_data),
                                                           request=json.dumps(orderline_data))
            orderlinehistory_batch.save()
            orderline_to_be_updated = order.order_lines.all()

            for orderline in orderline_to_be_updated:
                orderline_history_list.append(OrderLineHistory(
                        orderline_batch=orderlinehistory_batch,
                        order=orderline.order,
                        sku=orderline.sku,
                        quantity=orderline.quantity,
                        total_item_price=orderline.total_item_price,
                        each_item_price=orderline.each_item_price,
                        source='%s-%s' % ('update', source)
                ))
            OrderLineHistory.objects.bulk_create(orderline_history_list)
            order.order_lines.all().delete()

            if source and source != 'dashboard':
                source = None
            data = self.create_orderlines(orderline_data, order, order.customer, source)

        except Exception as e:
            arg = e.args[0]
            if isinstance(arg, dict):
                arg = e.args[0]['message']
            _out = {
                'status': 'FAIL',
                'message': arg
            }

            raise exceptions.ParseError(_out)

        return data


    def delete_orderlines(self, orderline_data, order, source=None):
        existing_orderlines = list(order.order_lines.all().values_list('id', flat=True))

        if not existing_orderlines:
            _out = {
                'status': 'FAIL',
                'message': 'No existing items inside the order'
            }
            raise exceptions.ParseError(_out)

        if not orderline_data:
            _out = {
                'status': 'FAIL',
                'message': 'No order items sent for update'
            }
            raise exceptions.ParseError(_out)

        if not all(int(x) in existing_orderlines for x in orderline_data):
            _out = {
                'status': 'FAIL',
                'message': 'Item mismatch for the corresponding order'
            }
            raise exceptions.ParseError(_out)

        try:
            orderline_history_list = []
            with transaction.atomic():
                orderlinehistory_batch = OrderLineHistoryBatch(description='Delete request from %s' % str(order.customer),
                                                               number_of_entries=len(orderline_data),
                                                               request=json.dumps(orderline_data))
                orderlinehistory_batch.save()
                orderline_to_be_deleted = OrderLine.objects.filter(id__in=orderline_data)
                for orderline in orderline_to_be_deleted:
                    orderline_history_list.append(OrderLineHistory(
                            orderline_batch=orderlinehistory_batch,
                            order=orderline.order,
                            sku=orderline.sku,
                            quantity=orderline.quantity,
                            total_item_price=orderline.total_item_price,
                            each_item_price=orderline.each_item_price,
                            source='%s-%s' % ('delete', source)
                    ))
                OrderLineHistory.objects.bulk_create(orderline_history_list)
                orderline_to_be_deleted.delete()
        except Exception as e:
            _out = {
                'status': 'FAIL',
                'message': e.args[0]
            }

            return _out

        _out = {
            'status': 'PASS',
            'message': 'Items deleted successfully'
        }

        return _out

    def get_total_order_line_cost(self, order):
        """
        get sum of total order lines cost
        """

        order_lines = order.order_lines.all()

        order_cost = 0
        for order_line in order_lines:
            order_cost += (order_line.each_item_price * order_line.quantity)

        return order_cost

    def _validate_float(self, value, field):
        try:
            return float(value)
        except ValueError:
            _out = {
                'status': 'FAIL',
                'message': '%s field should contain number/float value' % field
            }
            raise exceptions.ParseError(_out)

class GMVMixin():

    def get_basket_value(self, order):
        is_avg_price = False
        _mrp = order.total_incl_tax or order.cash_on_delivery or 0

        if _mrp == 0:
            if settings.USE_READ_REPLICA:
                lines = OrderLine.objects.using('readrep1').filter(order_id=order.id)
            else:
                lines = OrderLine.objects.filter(order_id=order.id)

            _mrp = sum([line.total_item_price if line.total_item_price else line.each_item_price * line.quantity for line in lines])

        if _mrp == 0:
            _mrp = order.customer_contract.avg_basket_value
            is_avg_price = True

        return is_avg_price,_mrp

    def populate_order_mrp_based_on_orderlines(self, date='01-04-2021', is_test_run=True):
        _date = datetime.strptime(date, '%d-%m-%Y')

        _customers = Contract.objects.filter(
                        contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]).values_list(
                            'customer_id', 'customer__name').distinct('customer_id')
        for item in _customers:
            customer_id = item[0]
            print('Customer ID :: ', customer_id)
            # Process orders having order lines and no cod
            _parm = {
                'date_placed__date__gte':_date,
                'order_type':settings.SHIPMENT_ORDER_TYPE,
                'customer_id':customer_id,
                'total_incl_tax':0, 'cash_on_delivery':0,
                'order_lines__isnull':False
            }
            if settings.USE_READ_REPLICA:
                _orders3 = Order.objects.using('readrep1').filter(**_parm)

            else:
                _orders3 = Order.objects.filter(**_parm)

            for order in _orders3:
                is_avg, _mrp = self.get_basket_value(order)
                if _mrp > 0 and not is_test_run:
                    print(order,_mrp)
                    Order.objects.filter(id=order.id).update(total_incl_tax=_mrp, is_avg_price=is_avg)

    def populate_order_mrp(self, date='01-04-2021', is_test_run=True):
        _date = datetime.strptime(date, '%d-%m-%Y')

        _customers = Contract.objects.filter(contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]
                        ).values_list('customer_id', 'customer__name').distinct('customer_id')
        for item in _customers:
            customer_id = item[0]
            print('Customer ID :: ', customer_id)

            # Process orders having no order lines and no cod
            _contract = Contract.objects.filter(contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL],
                            customer_id=customer_id).first()

            if _contract and _contract.avg_basket_value > 0:
                print('Average Price -> ', _contract.avg_basket_value)
                # get the orders having no order lines and no cash on delivery
                _orders1 = Order.objects.filter(date_placed__date__gte=_date,
                            order_type=settings.SHIPMENT_ORDER_TYPE,
                            customer_id=customer_id,
                            total_incl_tax=0, order_lines__isnull=True,
                            cash_on_delivery=0)

                # update above orders with base value from excel sheet
                if _orders1.exists() and not is_test_run:
                    print('Order with no order lines and cash on delivery :: ', _orders1.count())
                    _orders1.update(is_avg_price=True, total_incl_tax=_contract.avg_basket_value)

            # Process orders having cash on delivery greater than 0
            _orders2 = Order.objects.filter(date_placed__date__gte=_date,
                        order_type=settings.SHIPMENT_ORDER_TYPE,
                        customer_id=customer_id,
                        total_incl_tax=0,
                        cash_on_delivery__gt=0
                    )

            if _orders2.exists() and not is_test_run:
                print('Orders having Cash on Delivery greater than 0 :: ', _orders2.count())
                _orders2.update(total_incl_tax=F('cash_on_delivery'))
