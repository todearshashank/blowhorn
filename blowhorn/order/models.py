# System and Django libraries
import re
import logging
import simplejson as json
from datetime import timedelta

from django.conf import settings
from django.core.validators import RegexValidator, MinLengthValidator
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db.models import Sum
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from blowhorn.customer.models import Partner as BlowhornPartner
from blowhorn.oscar.utils.models import OSCAR_IMAGE_FOLDER
from blowhorn.driver.payment_transition_conditions import is_super_user
from blowhorn.utils.functions import get_tracking_url
from blowhorn.common.middleware import current_request
from postgres_copy import CopyManager

# 3rd Party libraries
from blowhorn.oscar.apps.address.abstract_models import AbstractShippingAddress
# from blowhorn.oscar.apps.order.abstract_models import AbstractOrder
from blowhorn.oscar.core.loading import get_model
from blowhorn.oscar.models import fields as oscarfields
from blowhorn.oscar.models.fields import UppercaseCharField
from rest_framework import status
from rest_framework.response import Response
from auditlog.models import AuditlogHistoryField
from django_fsm import FSMField, transition
from django.db.models import Q
from django.db import transaction

# Blowhorn imports
from blowhorn.freshchat.whatsapp import send_whatsapp_notification
from blowhorn.vehicle.models import VehicleClass
from blowhorn.customer.models import Customer, CUSTOMER_CATEGORIES, CUSTOMER_CATEGORY_INDIVIDUAL
from blowhorn.customer.submodels.invoice_models import CustomerInvoice
from blowhorn.address.models import City, Hub, State, Slots
from blowhorn.driver.models import Driver, DriverPayment
from blowhorn.oscar.core.loading import get_class, get_model
from config.settings import status_pipelines as StatusPipeline
from blowhorn.users.models import User
from blowhorn.common.models import BaseModel
from blowhorn.company.models import Office
from blowhorn.trip.models import Trip, Stop
from blowhorn.contract.constants import GTA
from config.settings.base import FLASH_SALE_API_KEY
from config.settings.status_pipelines import (
    ORDER_NEW,
    OUT_FOR_DELIVERY,
    ORDER_RETURNED,
    ORDER_DELIVERED,
    UNABLE_TO_DELIVER,
    ORDER_RETURNED_TO_HUB,
    ORDER_LOST,
    ORDER_SENT_FOR_FULILMENT, ORDER_STATUSES)
from blowhorn.order.const import (
    PAYMENT_STATUS_UNPAID,
    PAYMENT_STATUS_PAID,
    PAY_STATUS_OPTIONS,
    PAYMENT_MODE_OPTIONS,
    ORDER_TYPE,
    SIGNATURE,
    ORDER_DOCUMENT_TYPES,
    INTERNAL_TRACKING_STATUSES,
    SHIPPING_ADDRESS_TYPES)
from blowhorn.document.file_validator import FileValidator
from .helper import generate_file_path
from .order_transition_conditions import (
    is_booking_order, is_contract_spoc_or_supervisor,
    is_driver_assigned, is_non_enterprise_manager,
    is_ongoing_trip, is_not_ongoing_trip, is_contract_supervisor,
    is_qualified_for_dummy, is_shipment_order, is_order_not_paid,
    is_qualified_for_expiry, is_ecom_order)
from blowhorn.company.helpers.tax_helper import TaxHelper
from blowhorn.driver.payment_helpers.c2c_payment_helper import PaymentHelper
from blowhorn.contract.contract_helpers.contract_helper import ContractHelper
from phonenumber_field.modelfields import PhoneNumberField
from blowhorn.common import sms_templates
from datetime import datetime
from blowhorn.utils.functions import utc_to_ist
from blowhorn.order.oscar_models import AbstractOrder, AbstractBillingAddress
from blowhorn.catalogue.models import Product
from blowhorn.wms.constants import ORDER_LINE_NEW, ORDER_LINE_STATUSES, ALLOCATION_STATUSES
from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED, DISCOUNT_TYPES

PaymentTrip = get_model('driver', 'PaymentTrip')
DriverLedger = get_model('driver', 'DriverLedger')
AppEvents = get_model('trip', 'AppEvents')
Coupon = get_model('coupon', 'Coupon')

class BillingAddress(AbstractBillingAddress):
    pass


# class Country(AbstractCountry):
#     pass


# class Product(AbstractProduct):
#     pass


class Dimension(BaseModel):
    length = models.DecimalField(_("Length"),
                                 max_digits=10, decimal_places=2, default=0,
                                 blank=True, null=True)
    breadth = models.DecimalField(_("Breadth"),
                                  max_digits=10, decimal_places=2, default=0,
                                  blank=True, null=True)
    height = models.DecimalField(_("Height"),
                                 max_digits=10, decimal_places=2, default=0,
                                 blank=True, null=True)
    uom = models.CharField(_("UOM"),
                           max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = _('Dimension')
        verbose_name_plural = _('Dimensions')

    def calculate_volume(self):
        if self.length and self.breadth and self.height:
            return self.length * self.breadth * self.height
        else:
            return 0

    def __str__(self):
        return '%sx%sx%s %s' % (self.length, self.breadth, self.height, self.uom)


class ShippingAddressManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('country')


class ShippingAddress(AbstractShippingAddress):
    postcode = UppercaseCharField(
        _("Postal Code"), max_length=64, blank=True, null=True)
    phone_number = PhoneNumberField(
        _("Phone number"), blank=True, null=True,
        help_text=_("In case we need to call you about your order"))
    alternate_phone_number = PhoneNumberField(
        _("Alternate Phone number"), blank=True, null=True,
        help_text=_("Alternate Contact Number"))
    pin_number = models.CharField(_("Pin Number"), max_length=30, null=True, blank=True)
    geopoint = models.PointField(null=True, blank=True)
    identifier = models.CharField(_("Address Identifier"), null=True, blank=True,
                                  max_length=255, help_text=_("Address Identifier"))
    floor_number = models.SmallIntegerField(
        _('Floor Number'), null=True, blank=True,
        help_text=_('Floor number to deliver'))

    is_service_lift_available = models.NullBooleanField(_("Lift Available for carrying Goods"))

    is_lift_accessible = models.NullBooleanField(_("Vehicle can reach lift"))

    address_type = models.CharField(_("Address Type"), max_length=30,
                                    null=True, blank=True,
                                    choices=SHIPPING_ADDRESS_TYPES)

    objects = ShippingAddressManager()

    def active_address_fields(self, include_salutation=True):
        """
            :return: Line1 of address
            Note: All address are fields are merged and stored in Line1
        """
        return [self.line1]

    def __str__(self):
        return self.search_text if self.search_text else self.line1


class OrderManager(models.Manager):

    def get_recent_booking_of_customer(self, customer):
        return super().get_queryset(). \
            filter(customer=customer). \
            values('number', 'city__name'). \
            order_by('-pickup_datetime').first()

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.select_related(
            'shipping_address', 'pickup_address', 'city', 'hub')
        qs = qs.select_related('customer_contract')
        qs = qs.select_related('customer')
        qs = qs.select_related('driver')
        return qs

    def prefetch_queryset(self, **kwargs):

        qs = super().get_queryset()
        query = kwargs.pop('query', None)
        neg_query = kwargs.pop('neg_query', None)
        select_related = kwargs.pop('select_related', None)
        prefetch_related = kwargs.pop('prefetch_related', None)

        if query:
            qs = qs.filter(query)
        if neg_query:
            qs = qs.exclude(neg_query)
        if select_related:
            for x in select_related:
                qs = qs.select_related(x)
        if prefetch_related:
            for x in prefetch_related:
                qs = qs.prefetch_related(x)

        return qs


class ItemCategory(models.Model):
    """
    Model to configure Items for Order
    """
    category = models.CharField(
        _("Item category"), max_length=200)

    is_tax_exempt = models.BooleanField(_("Is Tax Exempted"), default=False)

    load_preference = models.ForeignKey('driver.LoadPreference', null=True, blank=True,
                                        verbose_name='Load Preference',
                                        on_delete=models.PROTECT)

    is_active = models.BooleanField('Is Active', default=True)

    def __str__(self):
        return str(self.category)

    class Meta:
        verbose_name = _("C2C/SME Item Category")
        verbose_name_plural = _("C2C/SME Item Categories")


class OrderEventSummary(BaseModel):
    count_attempted = models.IntegerField(
        _('Number of attempts.'), default=0)

    count_rejected = models.IntegerField(
        _('Number of rejections.'), default=0)

    count_assigned = models.IntegerField(
        _('Number of assignments.'), default=0)

    count_cancelled = models.IntegerField(
        _('Number of cancellations.'), default=0)

    count_delayed = models.IntegerField(
        _('Number of delays.'), default=0)


class OrderBatch(models.Model):
    """
    Batch to aggregate bulk inserts for orders
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    number_of_entries = models.IntegerField(
        _("Num of entries"), null=True, blank=True)
    source = models.CharField(_("Source"), max_length=50, null=True, blank=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
    successfull = models.IntegerField(
        _("Successfull Order Creation Count"), null=True, blank=True)
    summary = JSONField(_("Sync Summary"), blank=True, null=True)
    file = models.FileField(_("Uploaded File"),
                            upload_to=generate_file_path,
                            max_length=256, blank=True, null=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Order Batch")
        verbose_name_plural = _("Order Batches")


class OrderApiData(models.Model):
    request_body = JSONField(_("Request Body"), blank=True, null=True)
    category = models.CharField(_("Category"), max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    reference_number = models.CharField(max_length=100, null=True, blank=True, db_index=True)
    response_status = models.CharField(max_length=30,null =True, blank=True)
    reason = models.TextField(blank=True, null=True)
    is_phone_invalid = models.BooleanField(_("Is Phone Invalid"), default=False)
    is_email_invalid = models.BooleanField(_("Is Email Invalid"), default=False)
    is_unservicebale_pincode = models.BooleanField(_("Is Unserviceable"), default=False)
    has_undefined_contract = models.BooleanField(_("Has Undefined Contract"), default=False)

    class Meta:
        verbose_name = _('Order ApiData')
        verbose_name_plural = _('Order Api Data')

    def __str__(self):
        return self.reference_number



class OrderHistory(models.Model):
    """
    Capturing the order history
    """
    order = models.ForeignKey('order.Order', on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    old_value = models.CharField(max_length=100, null=True, blank=True)
    new_value = models.CharField(max_length=100, null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Order History')
        verbose_name_plural = _('Order History')

    def __str__(self):
        return self.order.number


class Order(AbstractOrder, BaseModel):
    # Batch ID to denote bulk inserts
    batch = models.ForeignKey(OrderBatch, null=True, blank=True,
                              on_delete=models.PROTECT,
                              related_name='orders')
    number = models.CharField(
        _("Order Number"), max_length=40, db_index=True,
        unique=True, validators=[
            RegexValidator(
                regex=r'^[-,_\w]*$',
                message=_("Only AlphaNumerics and , - _ are allowed"))])

    date_placed = models.DateTimeField(_('Date Placed'), auto_now_add=True)

    updated_time = models.DateTimeField(_('Updated Date Time'),
                                        db_index=True, auto_now_add=True)
    spar_pos_pulled_on = models.DateTimeField(_('Updated Date Time'),
                                              null=True)

    delivery_mode = models.CharField(_("Delivery Mode"), max_length=10,
                                     help_text=_("air/road"),
                                     null=True, blank=True)

    currency_code = models.CharField(_("Currency Code"), max_length=3,
                                     null=True, blank=True)

    order_type = models.CharField(_("Order Type"), max_length=30, db_index=True,
                                  choices=settings.ORDER_TYPE_DICT, default=settings.DEFAULT_ORDER_TYPE)

    reference_number = oscarfields.NullCharField(
        _("Reference Number"), max_length=40, db_index=True,
        validators=[
            RegexValidator(
                regex=r'^[-_\w]*$',
                message=_("Only alpha-numerics and  - _ are allowed"))]
    )

    booking_type = models.CharField(_("Booking Type"), max_length=30,
                                    choices=ORDER_TYPE, null=True, blank=True, db_index=True)

    device_type = models.CharField(_("Device Type"), max_length=30,
                                   null=True, blank=True)

    carrier_mode = models.CharField(_("Carrier Mode"), max_length=50, null=True, blank=True)

    pickup_address = models.OneToOneField(ShippingAddress,
                                          related_name="shipping_address", null=True,
                                          verbose_name='Pickup Address', on_delete=models.PROTECT)

    pickup_gstin = models.CharField(_('GSTIN'), max_length=15,
                                      null=True, blank=True, validators=[RegexValidator(
            regex=r'^\d{2}[A-Za-z]{5}\d{4}[A-Za-z]{1}\d{1}[A-Za-z]{1}\d{1}$',
            message=_('First 2 digits, next 5 Alphabets, next 4 digits,'
                      'one Alphabet, 1 digit, 1 Alphabet, 1 digit, fixed length is 15')),
            MinLengthValidator(15)])

    pickup_datetime = models.DateTimeField(_('Pickup Date Time'),
                                           db_index=True, default=timezone.now)

    shipping_address = models.OneToOneField(ShippingAddress,
                                            related_name="drop_off_address", null=True,
                                            verbose_name='Drop Off Address', on_delete=models.PROTECT)

    shipping_gstin = models.CharField(_('GSTIN'), max_length=15,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^\d{2}[A-Za-z]{5}\d{4}[A-Za-z]{1}\d{1}[A-Za-z]{1}\d{1}$',
            message=_('First 2 digits, next 5 Alphabets, next 4 digits,'
                      'one Alphabet, 1 digit, 1 Alphabet, 1 digit, fixed length is 15')),
            MinLengthValidator(15)])

    exp_delivery_start_time = models.DateTimeField(
        _('Delivery slot start Date Time'), null=True, blank=True
    )

    expected_delivery_time = models.DateTimeField(
        _('Expected Delivery Date Time'), null=True, blank=True)

    device_info = models.CharField(
        _("Device Information related to this order"), max_length=255, null=True)

    vehicle_class_preference = models.ForeignKey(VehicleClass, null=True,
                                                 blank=True, verbose_name='Vehicle Class',
                                                 on_delete=models.PROTECT)
    items = ArrayField(models.CharField(max_length=128, blank=True, null=True,
                                        validators=[
                                            RegexValidator(
                                                regex=r'^[-_\s\w]*$',
                                                message=_(
                                                    "Only AlphaNumerics and  - _ are allowed"))]),
                       blank=True, null=True)

    item_category = models.ManyToManyField(ItemCategory,
                                           blank=True)

    # made null=True for migration dependency
    customer = models.ForeignKey(Customer, null=True, blank=False,
                                 on_delete=models.PROTECT)

    # made null=True for migration dependency
    city = models.ForeignKey(City, null=True, blank=False,
                             on_delete=models.PROTECT)

    source_city = models.ForeignKey('address.City', on_delete=models.PROTECT, verbose_name=_('Source City'),
                                    null=True, blank=True, related_name='sourcecity')

    is_in_transit = models.NullBooleanField(_("Is order in Transit?"), null=True, blank=True, default=False)

    cost_components = models.CharField(_("Cost Components"),
                                       max_length=250, blank=True, null=True)

    driver = models.ForeignKey(Driver, null=True, blank=True,
                               on_delete=models.PROTECT)

    invoice_driver = models.ForeignKey(Driver, null=True, blank=True,
                                       on_delete=models.PROTECT, related_name='invoiced_orders')

    actual_cost = models.DecimalField(_('Actual Cost'),
                                      help_text=_("Actual Service Cost on first calculation"), max_digits=10,
                                      decimal_places=2, default=0)

    revised_cost = models.DecimalField(_('Revised Amount'),
                                       help_text=_("Revised Cost (excluding tax, tolls and labour)"), max_digits=10,
                                       decimal_places=2, default=0)

    total_excl_tax = models.DecimalField(
        _("Order total (Including labour, excluding tax and tolls)"), decimal_places=2,
        max_digits=12, default=0)

    tax = models.DecimalField(_("Tax"), decimal_places=2, max_digits=12,
                              default=0)

    discount = models.DecimalField(_("Discount"), decimal_places=2,
                                   max_digits=12, null=True, blank=True,
                                   help_text=_('Discount through coupon'))

    tip = models.DecimalField(_('Tips'), max_digits=10, decimal_places=2, default=0)

    total_incl_tax = models.DecimalField(
        _("Order Total (inc.tax)"), decimal_places=2, max_digits=12,
        default=0)

    toll_charges = models.DecimalField(_('Toll Charges'),
                                       max_digits=10, decimal_places=2,
                                       default=0)

    # Below fields capture how much is payable by customer
    # if rcm is true, the tax is to be paid by the customer. We show the
    # tax on the invoice, but do not collect it. This affects the total
    # payable amount.
    rcm = models.BooleanField(_("Reverse Charge Mechanism?"), default=False)

    is_items_verified = models.BooleanField(_("Is Items Verified by Driver"), default=False)

    is_assests_captured = models.BooleanField(_("Is Asset Captured"), default=False)

    auto_debit_paytm = models.BooleanField(
        _("Auto Debit from PayTm"), default=False)

    created_by_driver = models.BooleanField(
        _("Order Created by driver"), default=False)

    total_payable = models.DecimalField(_('Total Payable by customer'),
                                        max_digits=10, decimal_places=2,
                                        default=0)

    calculated_amount = models.DecimalField(_('calculated amount after delivery'),
                                            max_digits=10, decimal_places=2,
                                            default=0)

    amount_paid_online = models.DecimalField(_('amount paid online'),
                                             max_digits=10, decimal_places=2,
                                             default=0)

    extra_charges = models.DecimalField(_('Extra Charges that has be to collected by cash'),
                                        max_digits=10, decimal_places=2,
                                        default=0)

    payable_excluding_extra_charges = models.DecimalField(_('Amount excluding extra charges'),
                                                          max_digits=10, decimal_places=2,
                                                          default=0)

    # Json data to store breakup for calculated amount
    calculation_details = models.TextField(blank=True, null=True)

    adjustment_reason = models.CharField(_('Adjustment Reason'),
                                         max_length=255, null=True, blank=True)

    shipment_info = JSONField(
        _("Shipment Info"), blank=True, null=True)

    # store code , bill number bill date are spar specific data
    store_code = models.CharField(_('Store Code'), max_length=255, null=True)

    bill_number = models.CharField(_('Bill Number'), max_length=255, null=True)

    bill_date = models.DateTimeField(_("Bill Date"), null=True)

    # online order or offline
    order_mode = models.CharField(_('Order type'), max_length=255, null=True)

    source = models.CharField(_('Source of order'), max_length=100, null=True)

    status = FSMField(default=ORDER_NEW, protected=False, db_index=True)

    allocation_status = models.CharField(_('WMS Allocation Status'), choices=ALLOCATION_STATUSES, blank=True,
                                         null=True, max_length=64)

    pickup_hub = models.ForeignKey(Hub, null=True, blank=True,
                                   on_delete=models.PROTECT, related_name='pickup_hub')

    hub = models.ForeignKey(Hub, null=True, blank=True,
                            on_delete=models.PROTECT, verbose_name="Delivery Hub")

    courier_partner = models.ForeignKey(BlowhornPartner, null=True, blank=True,
                                        on_delete=models.PROTECT, verbose_name="Delivery Partner")

    latest_hub = models.ForeignKey(Hub, null=True, blank=True,
                                   related_name='latest_hub',
                                   verbose_name='Last hub order moved to',
                                   on_delete=models.PROTECT)

    customer_contract = models.ForeignKey('contract.Contract',
                                          null=True, blank=True, on_delete=models.CASCADE)

    payment_status = models.CharField(
        _("Payment Status"), choices=PAY_STATUS_OPTIONS,
        max_length=250, default=PAYMENT_STATUS_UNPAID,
        blank=True, null=True, db_index=True)

    estimated_cost = models.DecimalField(_("Estimated Cost"),
                                         decimal_places=2,
                                         max_digits=12, default=0)

    estimated_distance_km = models.DecimalField(_("Estimated Distance (KM)"),
                                                decimal_places=2,
                                                max_digits=12, default=0)

    estimated_cost_upper = models.DecimalField(_("Estimated Distance (Upper Bound)"),
                                               decimal_places=2,
                                               max_digits=12, default=0)

    estimated_cost_lower = models.DecimalField(_("Estimated Distance (Lower Bound)"),
                                               decimal_places=2,
                                               max_digits=12, default=0)

    estimated_time_in_mins = models.DecimalField(_("Estimated Time (Mins)"),
                                                 decimal_places=2,
                                                 max_digits=12, default=0)

    eta_in_mins = models.CharField(_("ETA"), max_length=30, null=True, blank=True)

    cash_on_delivery = models.DecimalField(_("Cash On Delivery"),
                                           decimal_places=2, max_digits=12,
                                           default=0,
                                           null=True, blank=True)

    cash_collected = models.DecimalField(_("Cash Collected"),
                                         decimal_places=2, max_digits=12,
                                         default=0,
                                         null=True, blank=True)

    customer_payment_settled = models.BooleanField(_('Customer Payment Settled'), default=False)


    # Json data stored as text dump. Not using JSONField, as it encodes
    # the data upon each save, adding escape characters repeatedly
    invoice_details = models.TextField(blank=True, null=True)

    rejected_drivers = models.ManyToManyField(Driver, related_name='drivers',
                                              blank=True)

    reason_for_cancellation = models.TextField(_("Reason For Cancellation"), null=True, blank=True)

    other_cancellation_reason = models.CharField(_("Other Cancellation Reasons"), max_length=255,
                                                 null=True, blank=True)

    payment_mode = models.CharField(
        _("Payment Mode"), choices=PAYMENT_MODE_OPTIONS,
        max_length=250,
        blank=True, null=True, db_index=True)

    customer_reference_number = oscarfields.NullCharField(
        _("Customer Reference Number"), max_length=40, db_index=True
    )

    total_distance = models.FloatField(
        _("Total Distance (km)"), null=True, blank=True, default=0.0)

    event_summary = models.OneToOneField(
        OrderEventSummary, related_name='order', null=True, blank=True,
        on_delete=models.CASCADE)

    invoice_number = models.CharField(
        unique=True, max_length=20,
        null=True, blank=True, help_text="Invoice No."
    )

    is_pod_required = models.BooleanField(
        _('Pod Required'), default=False, db_index=True)

    # is True for Spar offline orders
    is_offline_order = models.BooleanField(_('Is Offline Order'), default=False)

    weight = models.FloatField(_("Weight in Kg"), default=0.0)
    volume = models.FloatField(_("Volume in cubic centimeter"), default=0.0)
    coupon = models.ForeignKey('coupon.Coupon', on_delete=models.PROTECT,
                               null=True, blank=True)
    reason_for_marking_paid = models.TextField(_('Reason for marking as Paid'),
                                               null=True, blank=True,
                                               help_text=_('Remark/reason for '
                                                           'marking order as '
                                                           'paid from admin'))

    dispatch_log = models.TextField(_('Order Dispatch log'), null=True,
                                    blank=True)

    customer_category = models.CharField(
        _("Customer Category"), max_length=25, blank=False,
        choices=CUSTOMER_CATEGORIES, default=CUSTOMER_CATEGORY_INDIVIDUAL,
        db_index=True)

    #  fixed price for kiosk order amount fixed before order is created.
    fixed_price = models.DecimalField(_('Fixed price for order'),
                                      max_digits=10, decimal_places=2,
                                      default=0)

    floor_number = models.SmallIntegerField(
        _('Floor Number'), null=True, blank=True,
        help_text=_('Floor number to deliver'))

    number_of_items = models.SmallIntegerField(
        _('Number of Items'), null=True, blank=True,
        help_text=_('Number of items to be delivered'))

    is_refunded = models.BooleanField(_("Is Refunded"), default=False)

    marked_for_contingency = models.NullBooleanField(
        _('Marked for contingency')
    )

    otp = models.CharField(_('OTP value'), max_length=4,
                           null=True, blank=True)

    has_pickup = models.NullBooleanField(verbose_name='Has Pickup')

    xdk = models.NullBooleanField(verbose_name='Is Cross Dock?')

    return_to_origin = models.NullBooleanField(verbose_name='Return to Origin')

    return_order = models.BooleanField(
        verbose_name='Reverse Pickup/Return Order', default=False)

    remarks = models.TextField(_("Remarks"), blank=True, null=True)

    delivery_instructions = models.TextField(_("Delivery Instructions"), blank=True, null=True)

    dimension = models.ForeignKey(Dimension, null=True, blank=True, on_delete=models.PROTECT)

    priority = models.IntegerField(_('Order Priority'), blank=True, null=True)

    division = models.ForeignKey('customer.CustomerDivision', on_delete=models.PROTECT, null=True, blank=True)

    is_deleted = models.NullBooleanField(_("Is Deleted"), null=True, blank=True, default=False)

    is_secondary_region = models.NullBooleanField(_("Is Extended Coverage Order"), default=False, null=True, blank=True)

    shopify_fulfillment_id = models.CharField(_("Shopify Fulfillment ID"),
                                max_length=250, blank=True, null=True, db_index=True)

    is_avg_price = models.BooleanField(_('Is Average Basket Price'), default=False)

    shipping_charges = models.DecimalField(_('Shipping Charges'),
                            max_digits=10, decimal_places=2, default=0)

    payment_source = models.TextField(_('Payment Source'), null=True,blank=True)

    objects = OrderManager()
    copy_data = CopyManager()
    def_obj = models.Manager()

    class Meta:
        app_label = "order"
        verbose_name = _("Order")
        verbose_name_plural = _("All Orders")
        unique_together = ("reference_number", "customer", "return_order")

    @property
    def order_state(self):
        """
        Returns the `order_state` for the order.
        """

        states = State.objects.filter(cities=self.city)
        if states:
            if len(states) > 1:
                # To handle scenarios when one city is falling into multiple
                # states. (Eg : Delhi in Delhi and UP.)
                office = Office.objects.filter(state__in=states).first()
                return office.state if office else None
            return states[0]
        return None

    @property
    def source_state(self):
        states = State.objects.filter(cities=self.source_city)
        if states:
            if len(states) > 1:
                # To handle scenarios when one city is falling into multiple
                # states. (Eg : Delhi in Delhi and UP.)
                office = Office.objects.filter(state__in=states).first()
                return office.state if office else None
            return states[0]
        return None

    def get_completed_trip(self):
        """
        Returns completed trip for order
        """
        trips = self.trip_set.all()
        for trip in trips:
            if trip.status == StatusPipeline.TRIP_COMPLETED:
                return trip
        return None

    @transition(field=status, source=OUT_FOR_DELIVERY,
                target=ORDER_DELIVERED,
                permission=is_contract_supervisor,
                conditions=[is_shipment_order, is_ongoing_trip], )
    def deliver(self):
        logging.info(
            "Attempting to change order status to delivered from admin")
        self.update_order_status(ORDER_DELIVERED)

    @transition(field=status, source=ORDER_SENT_FOR_FULILMENT,
                target=ORDER_DELIVERED,
                permission=is_super_user,
                conditions=[is_ecom_order], )
    def deliver(self):
        logging.info(
            "Attempting to change order status to delivered from admin")

    @transition(field=status,
                source=[StatusPipeline.ORDER_NEW, StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.DRIVER_ARRIVED_AT_PICKUP, StatusPipeline.OUT_FOR_DELIVERY,
                        StatusPipeline.DRIVER_ARRIVED_AT_DROPOFF],
                target=StatusPipeline.ORDER_CANCELLED,
                permission=is_non_enterprise_manager,
                # permission=None,
                conditions=[is_booking_order], )
    def cancel(self):
        logging.info("Attempting to cancel the order from admin")
        trips = Trip.objects.filter(
            order_id=self.id,
            status__in=[StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE]
        )

        Stop.objects.filter(trip__in=trips).update(
            status=StatusPipeline.TRIP_CANCELLED)
        trips.update(status=StatusPipeline.TRIP_CANCELLED, current_step=-1)

        from blowhorn.order.tasks import send_c2c_slack_notification, initiate_paytm_refund
        from blowhorn.driver.util import DriverRestore
        if settings.ENABLE_SLACK_NOTIFICATIONS:
            send_c2c_slack_notification.apply_async(
                (self.id,), )
        Event.objects.create(
            status=StatusPipeline.ORDER_CANCELLED,
            order=self,
            address=self.shipping_address,
            hub_associate=current_request().user,
            remarks=self.reason_for_cancellation
        )
        pickup_datetime = datetime.utcfromtimestamp(self.pickup_datetime.timestamp())
        pickup_datetime_ist = utc_to_ist(pickup_datetime)

        template_dict = sms_templates.template_booking_cancelled
        message = template_dict['text'] % \
                  (self.number, pickup_datetime_ist.strftime('%H:%M'), pickup_datetime_ist.strftime('%d %b'))
        self.customer.user.send_sms(message, template_dict)
        self.customer.reverse_order_amount(self)
        DriverRestore().restore_driver_app(self.driver)
        # Send Whatsapp notification
        msg_template_data = {"template_name": "booking_cancel",
                             "params_data": [self.customer.name, self.number,
                                             pickup_datetime_ist.strftime(
                                                 '%H:%M'),
                                             pickup_datetime_ist.strftime(
                                                 '%d %b')]}
        send_whatsapp_notification([settings.ACTIVE_COUNTRY_CODE + str(
            self.customer.user.phone_number.national_number)],
                                   msg_template_data, self)

    #     if self.payment_mode == PAYMENT_MODE_PAYTM and \
    #         self.payment_status == PAYMENT_STATUS_PAID and self.amount_paid_online > 0\
    #         and not self.is_refunded:

    #         initiate_paytm_refund.apply_async(
    #                             (self.id,),)

    @transition(field=status,
                source=[StatusPipeline.ORDER_NEW,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.DRIVER_ASSIGNED,
                        StatusPipeline.DRIVER_ARRIVED_AT_PICKUP,
                        StatusPipeline.OUT_FOR_DELIVERY,
                        StatusPipeline.DRIVER_ARRIVED_AT_DROPOFF,
                        StatusPipeline.ORDER_DELIVERED],
                target=StatusPipeline.DUMMY_STATUS,
                permission=is_contract_spoc_or_supervisor,
                conditions=[is_qualified_for_dummy], )
    def mark_dummy(self):
        with transaction.atomic():
            Trip.objects.filter(order=self).update(status=StatusPipeline.DUMMY_STATUS,
                                                   modified_by_id=self.modified_by_id,
                                                   modified_date=timezone.now())
            """
            Mark all driver payment records related to order as stale
            """
            stale_reason = '%s order marked dummy' % self.number
            trips = Trip.objects.filter(order=self)
            DriverPayment.objects.filter(
                paymenttrip__trip__in=trips).update(is_stale=True,
                                                    stale_reason=stale_reason)

            Stop.objects.filter(trip__in=trips).update(
                status=StatusPipeline.DUMMY_STATUS)
            Event.objects.create(
                status=StatusPipeline.DUMMY_STATUS,
                order=self,
                address=self.shipping_address,
                hub_associate=current_request().user,
            )
            self.customer.reverse_order_amount(self)
            order = Order.objects.filter(id=self.id)
            order.update(reference_number=None, customer_reference_number=None)

            """
            Mark invoices related to order as stale
            """

            CustomerInvoice.objects.filter(Q(invoicetrip__trip__in=trips) |
                                           Q(invoiceorder__order=self)).filter(
                status__in=StatusPipeline.INVOICE_EDIT_STATUSES).update(
                is_stale=True,
                stale_reason=stale_reason)

    @transition(field=status,
                source=StatusPipeline.ORDER_NEW,
                target=StatusPipeline.ORDER_EXPIRED,
                permission=is_contract_spoc_or_supervisor,
                conditions=[is_qualified_for_expiry], )
    def mark_expire(self):
        Event.objects.create(
            status=StatusPipeline.ORDER_EXPIRED,
            order=self,
            driver=self.driver,
            address=self.shipping_address
        )
        self.customer.reverse_order_amount(self)

    def find_last_gst_number(self, series):
        """
        Find the last GST invoice in DB
        """
        first = series
        last = '%s99' % series
        order = Order.objects.filter(invoice_number__gt=first,
                                     invoice_number__lt=last).order_by('-invoice_number')

        if order.exists():
            return order.first()

        return None

    def get_gst_invoice_series(self, invoice_date, state_code):
        """
        Same as get_invoice_year, prefixes state_code
        """
        if not state_code:
            print("State Code is mandatory for GST Invoices")
            return None

        series = '%s%d%02d' % (state_code, invoice_date.year %
                               100, invoice_date.month)

        print("GST Invoice Series: %s" % (series,))
        return series

    def generate_invoice_number(self, invoice_date, state_code):
        """
        For a given state code and date, finds the next GST invoice number
        Uses range and multiplier functions and looks up the database
        """
        print("generating invoice_number :", invoice_date)
        series = self.get_gst_invoice_series(invoice_date, state_code)
        last_invoice = self.find_last_gst_number(series)
        next_sequence = None

        if last_invoice is None:
            ''' No such GST invoices, Generate the first number '''
            next_sequence = 900000
        else:
            last_number = last_invoice.invoice_number
            m = re.match(r'(\S{6})(\d{6})', last_number)
            if m:
                series, number = m.group(1), m.group(2)
                print("Prefix:%s, Number:%s" % (series, number))
                next_sequence = int(number) + 1
            else:
                print('Invoice format invalid')

        new_invoice_number = '%s%06d' % (series, next_sequence)
        print('Invoice number: %s' % (new_invoice_number))
        return new_invoice_number

    @transition(field=status, source=StatusPipeline.ORDER_DELIVERED, target=StatusPipeline.ORDER_DELIVERED,
                permission=is_contract_spoc_or_supervisor,
                conditions=[is_booking_order, is_order_not_paid], )
    def recalculate(self):
        # when the order is completed all the fields are readonly
        # so self.driver wont be there so fetching order
        order = Order.objects.get(id=self.id)
        order.trip_set.filter(
            status__in=[
                StatusPipeline.TRIP_IN_PROGRESS,
                StatusPipeline.TRIP_NEW,
                StatusPipeline.DRIVER_ACCEPTED,
                StatusPipeline.TRIP_ALL_STOPS_DONE
            ]
        ).update(status=StatusPipeline.TRIP_COMPLETED)
        # assumption is that there wont be more than 1 completed trip
        trip = self.trip_set.filter(driver_id=order.driver_id, status=StatusPipeline.TRIP_COMPLETED).first()
        if trip:
            self.calculate_and_update_cost(trip=trip)

    @transition(field=status,
                source=[StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.DRIVER_ARRIVED_AT_PICKUP, StatusPipeline.OUT_FOR_DELIVERY,
                        StatusPipeline.DRIVER_ARRIVED_AT_DROPOFF],
                target=StatusPipeline.ORDER_DELIVERED,
                permission=is_contract_spoc_or_supervisor,
                conditions=[is_booking_order, is_driver_assigned], )
    def complete(self):
        from blowhorn.trip.mixins import TripMixin
        from blowhorn.apps.driver_app.v1.helpers.trip import (
            get_route_trace_for_stop_using_mongo,
            get_route_trace_for_trip_without_stops_using_mongo,
            get_route_trace_for_trip_with_stops_using_mongo
        )
        from django.contrib.gis.geos import MultiLineString
        logging.info(
            "Attempting to change order status to Completed from admin")
        with transaction.atomic():
            if self.driver:
                trip = Trip.objects.filter(
                    order_id=self.id, driver=self.driver,
                    status__in=[
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE
                    ]
                ).first()
                if trip:
                    stops_exist = False
                    stops = Stop.objects.filter(
                        Q(trip=trip) & ~Q(status=StatusPipeline.TRIP_COMPLETED)
                    )

                    for stop in stops:
                        stops_exist = True
                        if not stop.start_time:
                            stop.start_time = timezone.now()
                        if not stop.end_time:
                            stop.end_time = timezone.now()

                        stop.status = StatusPipeline.TRIP_COMPLETED
                        _route_trace, _distance = get_route_trace_for_stop_using_mongo(stop=stop)

                        if _distance:
                            stop._distance = _distance
                        if _route_trace:
                            stop._route_trace = _route_trace
                        stop.save()
                        TripMixin().update_route_trace_for_stop(stop)

                    gps_distance = TripMixin().update_route_trace_for_trip(trip)
                    _gps_distance = 0
                    _route_trace = None
                    if stops_exist:
                        _route_trace, _gps_distance = get_route_trace_for_trip_with_stops_using_mongo(trip)
                    else:
                        _route_trace, _gps_distance = get_route_trace_for_trip_without_stops_using_mongo(trip)
                        if _route_trace:
                            _route_trace = MultiLineString([_route_trace, ])

                    Trip.objects.filter(id=trip.id).update(
                        status=StatusPipeline.TRIP_COMPLETED,
                        gps_distance=gps_distance,
                        _gps_distance=_gps_distance,
                        _route_trace=_route_trace if _route_trace else None)

                    Event.objects.create(
                        status=StatusPipeline.ORDER_DELIVERED,
                        order=self,
                        driver=self.driver,
                        address=self.shipping_address
                    )
                    tracking_url = get_tracking_url(
                        current_request(), self.number)

                    from blowhorn.order.tasks import send_c2c_slack_notification
                    from blowhorn.order.utils import calculate_payments
                    from blowhorn.driver.util import DriverRestore
                    calculate_payments(self, trip, tracking_url)

                    if settings.ENABLE_SLACK_NOTIFICATIONS:
                        send_c2c_slack_notification.apply_async(
                            (self.id,), )

                    DriverRestore().restore_driver_app(self.driver)

    @transition(field=status, source=[StatusPipeline.DRIVER_ASSIGNED,
                                      StatusPipeline.DRIVER_ACCEPTED,
                                      ORDER_RETURNED, OUT_FOR_DELIVERY,
                                      UNABLE_TO_DELIVER],
                target=ORDER_RETURNED_TO_HUB,
                permission=is_contract_supervisor,
                conditions=[is_shipment_order, is_not_ongoing_trip], )
    def return_to_hub(self):
        logging.info("change order status to Returned to Hub from admin")

        Event.objects.create(
            driver=self.driver,
            status=ORDER_RETURNED_TO_HUB,
            order=self,
            hub_associate=current_request().user,
        )

    @transition(field=status, source=OUT_FOR_DELIVERY,
                target=ORDER_RETURNED,
                permission=is_contract_supervisor,
                conditions=[is_shipment_order, is_ongoing_trip], )
    def reject(self):
        logging.info(
            "Attempting to change order status to returned from admin")

        self.update_order_status(ORDER_RETURNED)

    @transition(field=status,
                source=StatusPipeline.ORDER_NEW,
                target=StatusPipeline.ORDER_NEW,
                permission=is_contract_spoc_or_supervisor | current_request().user.is_superuser if current_request() else False,
                conditions=[is_booking_order], )
    def dispatch(self):
        from blowhorn.order.tasks import notify
        notify.apply_async((self.id,), eta=timezone.now() + timedelta(seconds=5))

    @transition(field=status, source=OUT_FOR_DELIVERY,
                target=UNABLE_TO_DELIVER,
                permission=is_contract_supervisor,
                conditions=[is_shipment_order, is_ongoing_trip], )
    def unable_to_deliver(self):
        logging.info(
            "Attempting to change order status to unable-to-deliver from admin")

        self.update_order_status(UNABLE_TO_DELIVER)

    @transition(field=status, source=[StatusPipeline.ORDER_NEW, StatusPipeline.DRIVER_ACCEPTED,
                                      StatusPipeline.DRIVER_ASSIGNED,
                                      ORDER_RETURNED, OUT_FOR_DELIVERY,
                                      UNABLE_TO_DELIVER, ORDER_RETURNED_TO_HUB,
                                      StatusPipeline.ORDER_PICKED],
                target=ORDER_LOST,
                permission=is_contract_supervisor,
                conditions=[is_shipment_order, is_not_ongoing_trip], )
    def lost(self):
        logging.info("change order status to lost from admin")

        Event.objects.create(
            driver=self.driver,
            status=ORDER_LOST,
            order=self,
            hub_associate=current_request().user,
        )

    def __init__(self, *args, **kwargs):
        """ overriding the init method as we want to detect changes
            in important fields when saving
        """
        super().__init__(*args, **kwargs)
        self.__important_fields = [
            'expected_delivery_time'
        ]
        for field in self.__important_fields:
            setattr(self, '__original_%s' % field, getattr(self, field))

    def get_service_state(self):
        """
        Returns the service state of the order.
        """

        return self.customer_contract.service_state

    def get_invoice_state(self):
        """
        Returns the invoice state of the order.
        """

        return State.objects.filter(cities=self.city).first()

    def is_valid_future_pickup_datetime(self, pickup_datetime):
        """
        :param pickup_datetime: instance of datetime
        :return: True if given time falls within max-allowed future date limit
        """
        current_date = utc_to_ist(timezone.now())
        upper_limit_date = current_date.date() + timedelta(
            days=settings.ALLOWED_FUTURE_DAYS)

        if pickup_datetime.date() > upper_limit_date:
            return False, _('Please select pickup date time within %s days '
                            'from today' % settings.ALLOWED_FUTURE_DAYS)
        return True, ''

    def is_valid_past_pickup_datetime(self, pickup_datetime):
        """
        :param pickup_datetime: instance of datetime
        :return: True if given time falls within min-allowed past date limit
        """
        current_date = utc_to_ist(timezone.now())
        lower_limit_date = current_date.date() - timedelta(
            days=settings.MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME)

        if pickup_datetime.date() < lower_limit_date:
            return False, _('Pickup date time should be within past %d days '
                            'from today' %
                            settings.MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME)
        return True, ''

    def is_pickup_time_in_valid_date_range(self, pickup_datetime):

        is_after_threshold_pickup_time, message = \
            self.is_valid_future_pickup_datetime(pickup_datetime)
        if not is_after_threshold_pickup_time:
            return is_after_threshold_pickup_time, message

        return self.is_valid_past_pickup_datetime(pickup_datetime)

    def save(self, *args, **kwargs):
        if self and self.id:
            # save is called twice and total payable is becaming zero
            # when completing from admin so fetching latest obj based
            # on modified time
            old_obj = Order.objects.get(id=self.id)
            if old_obj.modified_date > self.modified_date:
                self = old_obj
            if self.customer_contract and self.customer_contract.customer and \
                self.customer_contract.customer.api_key == FLASH_SALE_API_KEY \
                and self.status == ORDER_DELIVERED:
                self.invoice_number = self.generate_invoice_number(
                    self.pickup_datetime, self.order_state.code)

        if self and self.driver_id and not self.order_type == settings.SHIPMENT_ORDER_TYPE:
            order = Order.objects.filter(id=self.id).first()
            # spot order will be added from admin (new order)
            if order and (order.driver_id != self.driver_id):
                from blowhorn.order.tasks import alert_driver_prior_to_trip
                # from pytz import timezone as localtime
                # pickup_date = self.pickup_datetime.astimezone(localtime(settings.TIME_ZONE))
                time_ = self.pickup_datetime - timedelta(
                    hours=OrderConstants().get('ALERT_DRIVER_PRIOR_TO_TRIP', 2))
                if time_ <= timezone.now():
                    time_ = timezone.now()
                alert_driver_prior_to_trip.apply_async((self.number, None,),
                                                       eta=time_)

        # if order and order.modified_date > self.modified_date:
        #     raise ValidationError('Order Edited from other Window Reload the page to Continue Editing...')

        if self.order_type == settings.SHIPMENT_ORDER_TYPE:
            if not self.invoice_driver:
                self.invoice_driver = self.driver

            if self._get_pk_val():
                old = self.__class__.objects.get(pk=self._get_pk_val())
                old_value = old.driver
                current_value = self.driver
                if old_value != current_value:
                    self.invoice_driver_id = current_value

            order_history_list = []
            for field in self.__important_fields:
                orig = '__original_%s' % field
                if getattr(self, orig) != getattr(self, field):
                    _dict = {
                        'order': self,
                        'field': field,
                        'old_value': getattr(self, orig),
                        'new_value': getattr(self, field)
                    }
                    order_history_list.append(OrderHistory(**_dict))
            if order_history_list:
                OrderHistory.objects.bulk_create(order_history_list)

        super().save(*args, **kwargs)

    def serialize_hook(self, hook):
        shipping_address = self.shipping_address
        shipment_info = None
        try:
            if self.shipment_info:
                shipment_info = json.loads(self.shipment_info)
        except:
            pass

        data = {
            "WaybillNo": self.number,
            "eventType": "updateOrderStatus",
            "OrderStatus": ORDER_STATUSES.get(self.status, "Pending"),
            "CourierComment": "",
            'StatusDateTime': self.updated_time.isoformat(),
            'ToMobileNumber': str(shipping_address.phone_number) if shipping_address.phone_number else '',
            'ToEmail': shipment_info.get('customer_email', '') if shipment_info and type(shipment_info) == dict else '',
            'ToName': shipping_address.first_name,
            'Client': str(self.customer),
            'StatusCode': ORDER_STATUSES.get(self.status, ORDER_NEW),
            'EDD': self.expected_delivery_time.isoformat() if self.expected_delivery_time else '',
            "order_no": self.reference_number,
            "order_status": ORDER_STATUSES.get(self.status, "Pending")

        }

        return data

    def update_order_status(self, order_status):
        from blowhorn.order.tasks import send_status_notification

        TripMixin = get_class('trip.mixins', 'TripMixin')

        trip = Trip.objects.get(
            driver=self.driver,
            status=StatusPipeline.TRIP_IN_PROGRESS,
            order=None
        )

        waypoint = WayPoint.objects.get(order=self)
        stop = Stop.objects.get(waypoint=waypoint, trip=trip)

        if stop.status == StatusPipeline.TRIP_IN_PROGRESS:

            next_stop = Stop.objects.filter(
                trip=trip,
                status=StatusPipeline.TRIP_NEW
            ).order_by(
                'waypoint__order__expected_delivery_time'
            ).first()

            # UPDATE next stops status to INPROGRESS
            if next_stop:
                Stop.objects.filter(id=next_stop.id).update(
                    status=StatusPipeline.TRIP_IN_PROGRESS
                )
            else:
                trip.status = StatusPipeline.TRIP_COMPLETED
                trip.actual_end_time = timezone.now()

        Stop.objects.filter(id=stop.id).update(
            status=StatusPipeline.TRIP_COMPLETED
        )

        if order_status == ORDER_DELIVERED:
            trip.delivered += 1
        elif order_status == ORDER_RETURNED:
            trip.rejected += 1
        elif order_status == UNABLE_TO_DELIVER:
            trip.returned += 1

        trip.save()

        TripMixin().update_route_trace_for_stop(stop)

        Event.objects.create(
            status=order_status,
            order=self,
            address=self.shipping_address,
            hub_associate=current_request().user
        )

        AppEvents.objects.create(
            driver=self.driver,
            event=order_status,
            order=self,
            hub_associate=current_request().user,
            trip=trip,
            stop=stop
        )

        send_status_notification.delay(
            self.id,
            self.number,
            order_status,
            self.reference_number,
            self.customer.notification_url,
            self.customer.ecode,
            self.customer.user.name
        )

    def get_labour_cost(self):
        labour_cost = 0
        if hasattr(self, 'labour'):
            labour_cost = self.labour.no_of_labours * self.labour.labour_cost
        return labour_cost

    def calculate_and_update_cost(self, trip=None, planned_start_time=None,
                                  show_cost_component=False, labour_charge=0):
        if self.fixed_price and not self.revised_cost:
            amount, calculation = self.fixed_price, {}
        else:
            contract = self.customer_contract
            if planned_start_time is None:
                planned_start_time = trip.planned_start_time.date() if trip else self.pickup_datetime.date()

            terms = contract.contractterm_set.all()
            terms = terms.select_related('sell_rate')
            terms = terms.prefetch_related("sell_rate__sellvasslab_set")
            terms = terms.prefetch_related("sell_rate__sellvehicleslab_set",
                                           "sell_rate__sellvasslab_set__vas_attribute")

            sell_rate = ContractHelper().get_sell_rate(
                terms, planned_start_time)

            amount, calculation = PaymentHelper().get_total_amount(
                sell_rate, self, trip=trip, labour_charge=labour_charge)

        self.actual_cost = amount
        if self.coupon:
            # Calculate discount on initial vehicle fare
            self.discount, amount = PaymentHelper().calculate_discount(self)

        self.revised_cost = amount
        self.invoice_details = calculation
        self.calculate_extra_charges()
        self.update_invoice_information(trip=trip, show_cost_component=show_cost_component)

        # return self

    def calculate_extra_charges(self):
        trips = self.trip_set.all()
        extra_amount = 0
        for trip in trips:
            extra_amount += float(trip.toll_charges + trip.parking_charges)
        self.extra_charges = extra_amount

    def is_order_exempt_from_tax(self):
        if not self.id:
            return False
        tax_exempted_list = list(self.item_category.values_list('is_tax_exempt', flat=True))

        if not tax_exempted_list or False in list(tax_exempted_list):
            return False
        return True

    def update_invoice_information(self, revised_cost=None, trip=None,
                                   show_cost_component=True):
        show_cost_component = True
        """ revised_cost - Service charges excluding tolls and labour """
        if revised_cost is not None:
            self.revised_cost = revised_cost

        labour_cost = self.get_labour_cost()
        taxable_adjustment = self.orderadjustment_set.filter(category__is_tax_applicable=True
                                                             ).aggregate(Sum('total_amount'))

        non_taxable_adjustment = self.orderadjustment_set.filter(category__is_tax_applicable=False
                                                                 ).aggregate(Sum('total_amount'))

        taxable_amount = float(self.revised_cost) + float(labour_cost) + \
                         float(taxable_adjustment.get('total_amount__sum', 0) or 0)

        self.total_excl_tax = taxable_amount + float(self.tip) + \
                              float(non_taxable_adjustment.get('total_amount__sum', 0) or 0)

        tax_exempt = self.is_order_exempt_from_tax()
        tax_info = None
        if self.customer:
            tax_info = self.customer.get_gst_info(state=self.order_state)

        invoice_state = tax_info.state if tax_info else None
        service_tax_category = self.customer.service_tax_category \
            if self.customer and self.customer.service_tax_category else GTA

        tax, distribution, rcm = TaxHelper.calculate_tax_amount(
            taxable_amount,
            order=self,
            tax_exempt=tax_exempt,
            service_state=self.order_state,
            invoice_state=invoice_state,
            service_type=service_tax_category,
            customer=self.customer
        )
        self.rcm = rcm
        self.tax = tax
        self.total_incl_tax = float(self.total_excl_tax) + float(self.tax)

        if not self.invoice_number and trip:
            self.invoice_number = self.generate_invoice_number(
                self.pickup_datetime, self.order_state.code)

        if isinstance(self.invoice_details, str):
            try:
                invoice_details = json.loads(self.invoice_details)
            except:
                invoice_details = {}
        elif not isinstance(self.invoice_details, dict):
            invoice_details = {}
        else:
            invoice_details = self.invoice_details

        invoice_details['amount'] = float(self.revised_cost)
        invoice_details['labour'] = labour_cost
        invoice_details['tax_amount'] = tax
        invoice_details['tax_distribution'] = distribution
        invoice_details['rcm'] = rcm
        invoice_details['amount_excl_tax'] = float(self.total_excl_tax)
        invoice_details['amount_incl_tax'] = float(self.total_incl_tax)
        #        order.invoice_details = json.dumps(calculation)
        if self.rcm:
            self.total_payable = self.total_excl_tax + float(self.toll_charges)
        else:
            self.total_payable = self.total_incl_tax + float(self.toll_charges)
        # calculation happening while placing order
        # so no extra charges will be there total payable and
        # payable without extra charge will be same
        self.payable_excluding_extra_charges = self.total_payable
        self.total_payable = float(self.total_payable) + float(self.extra_charges)

        print(invoice_details)
        self.invoice_details = json.dumps(invoice_details)

        self.get_cost_components(show_cost_component=show_cost_component)

    def get_cost_components(self, show_cost_component=False):
        cost_components = []
        calculation = {}
        # if not show_cost_component:
        #     return calculation

        if self.invoice_details:
            calculation = json.loads(self.invoice_details)

        # Add the basic vehicle Fare
        cost_components.append({
            'name': 'Vehicle Fare',
            'value': calculation.get('fixed') if calculation else 0
        })

        discount = self.discount
        if discount:
            cost_components.append({
                'name': 'Discount',
                'value': discount
            })

        # Show the labour amount
        if hasattr(self, 'labour') and self.labour.labour_cost:
            labour_cost = self.labour.labour_cost
            no_of_labour = self.labour.no_of_labours
            cost_components.append({
                'name': 'Labour 400 x ' +
                        str(no_of_labour),
                'value': float(no_of_labour * labour_cost)
            })

        time_amount = 0
        time_cal = calculation.get('time')
        if time_cal:
            for time in time_cal:
                time_amount += time.get('amount')

        if time_amount:
            cost_components.append({
                'name': 'Time Amount',
                'value': time_amount if time_amount else 0
            })

        dist_amount = 0
        dist_cal = calculation.get('distance')
        if dist_cal:
            for dist in dist_cal:
                dist_amount += dist.get('amount')

        if dist_amount:
            cost_components.append({
                'name': 'Distance Amount',
                'value': dist_amount if dist_amount else 0
            })

        vas_floor_amount = 0
        vas_floor_calc = calculation.get('vas_floor', {})
        if vas_floor_calc:
            for slab in vas_floor_calc:
                vas_floor_amount += slab.get('amount')

        if vas_floor_amount:
            cost_components.append({
                'name': 'VAS Floor',
                'value': vas_floor_amount
            })

        vas_item_amount = 0
        vas_item_calc = calculation.get('vas_item', {})
        if vas_item_calc:
            for slab in vas_item_calc:
                vas_item_amount += slab.get('amount')

        if vas_item_amount:
            cost_components.append({
                'name': 'VAS Items',
                'value': vas_item_amount
            })

        cgst_percent = 0
        cgst_amount = 0
        sgst_percent = 0
        sgst_amount = 0
        igst_percent = 0

        tax_dist = calculation['tax_distribution']
        if tax_dist:
            cgst = tax_dist.get('CGST', 0)
            if cgst:
                cgst_percent = cgst['percentage']
                cgst_amount = cgst['amount']
            sgst = tax_dist.get('SGST', 0)
            if sgst:
                sgst_percent = sgst['percentage']
                sgst_amount = sgst['amount']
            igst = tax_dist.get('IGST', 0)
            if igst:
                igst_percent = igst['percentage']
                igst_amount = igst['amount']

        if cgst_percent:
            cost_components.append({
                'name': 'CGST' + '(' + str(cgst_percent) + '%)',
                'value': cgst_amount
            })

        if sgst_percent:
            cost_components.append({
                'name': 'SGST' + '(' + str(sgst_percent) + '%)',
                'value': sgst_amount
            })

        if igst_percent:
            cost_components.append({
                'name': 'IGST' + '(' + str(igst_percent) + '%)',
                'value': igst_amount
            })

        self.cost_components = json.dumps(cost_components)

    # if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
    #     def index_doc(self):
    #         from blowhorn.order.documents import OrderDocument as OrderDoc
    #         doc_dict = {"meta": {"id": self.id}, "id": self.id, "number": self.number,
    #                        "reference_number": self.reference_number,
    #                        "customer_reference_number": self.customer_reference_number,
    #                        "customer_contract": {"name": self.customer_contract.name if self.customer_contract else None,
    #                        "pk": self.customer_contract.id if self.customer_contract else None,
    #                                              "city": {"name": self.city.name, "pk": self.city.id}},
    #                        "customer": {"name": self.customer.name if self.customer else None,
    #                        "pk": self.customer.id, },
    #                        "hub": {"name": self.hub.name,
    #                        "pk": self.hub.id,},
    #                        "pickup_hub": {"name": self.pickup_hub.name if self.pickup_hub else None,
    #                        "pk": self.pickup_hub.id if self.pickup_hub else None},
    #                        "driver": {"name": self.driver.name if self.driver else None,
    #                        "driver_vehicle": self.driver.driver_vehicle if self.driver else None,
    #                        "pk": self.driver.id if self.driver else None}}
    #         doc = OrderDoc(**doc_dict)
    #         return doc.to_dict(include_meta=True)

    def __str__(self):
        return self.number


class OrderCommunication(models.Model):
    # This model will be used for sending sms to customers after the driver is assigned for
    # orders from cosmos
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    contact_name = models.CharField(_("Communicator Name"), max_length=100, null=True,
                                    blank=True)

    contact_phone_number = PhoneNumberField(
        _("Communicator Phone Number"), blank=True, null=True)

    def __str__(self):
        return self.contact_name


class OrderInternalTracking(models.Model):
    status = models.CharField(_("Internal Track Status"), max_length=30,
                              choices=INTERNAL_TRACKING_STATUSES)
    associate = models.ForeignKey(User, on_delete=models.PROTECT,
                                  limit_choices_to={'is_staff': True})
    time = models.DateTimeField(_("Created time"), default=timezone.now)
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    remarks = models.CharField(_("Remarks"),
                               max_length=350, blank=True, null=True)

    class Meta:
        unique_together = ("order", "status")
        verbose_name = _("Internal Tracking")
        verbose_name_plural = _("Internal Tracking")

    def __str__(self):
        return str(self.id)


class OrderDocument(BaseModel):
    order = models.ForeignKey(Order, related_name='documents',
                              on_delete=models.PROTECT)

    file = models.FileField(upload_to=generate_file_path,
                            validators=[FileValidator()],
                            max_length=256,
                            null=True,
                            blank=True)

    document_type = models.CharField(
        _("Document Type"),
        max_length=30,
        choices=ORDER_DOCUMENT_TYPES,
        default=SIGNATURE)

    stop = models.ForeignKey('trip.Stop',
                             related_name='stop_documents',
                             on_delete=models.PROTECT, null=True,
                             blank=True)

    event = models.ForeignKey('order.Event', null=True, blank=True,
                              on_delete=models.PROTECT)

    is_document_uploaded = models.BooleanField(_('Is document sent to customer via Webhooks'), default=False)

    class Meta:
        verbose_name = _('Order document')
        verbose_name_plural = _('Order documents')

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s" % self.file

    def to_json(self, include_event=False):
        trip_id = self.stop and self.stop.trip_id
        address = ''
        if self.stop and self.stop.stop_address:
            address = self.stop.stop_address
        else:
            address = '%s' % self.order.shipping_address

        _dict = {
            'id': self.id,
            'order': '%s' % self.order,
            'trip_id': trip_id,
            'stop_id': self.stop_id,
            'address': '%s' % address,
            'file': self.file.url if self.file else '',
            'document_type': '%s' % self.document_type,
            'created_date': self.created_date.isoformat()
        }
        if include_event and self.event:
            _dict['event'] = self.event.to_json()
        return _dict


class OrderLineHistoryBatch(models.Model):
    """
    Batch to aggregate bulk inserts for orders
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    description = models.CharField(_("Description"), max_length=100)
    number_of_entries = models.IntegerField(
        _("Num of entries"), null=True, blank=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    request = JSONField(_("request"), blank=True, null=True)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("OrderLineHistory Batch")
        verbose_name_plural = _("OrderLineHistory Batches")


class OrderLine(BaseModel):
    order = models.ForeignKey(
        Order, related_name='order_lines', on_delete=models.PROTECT)
    partner = models.ForeignKey('customer.Partner', on_delete=models.PROTECT, null=True, blank=True)
    sku = models.ForeignKey('customer.SKU', on_delete=models.PROTECT, null=True, blank=True)
    # supplier for WMS SKUs
    supplier = models.ForeignKey('wms.Supplier', null=True, blank=True, on_delete=models.PROTECT)
    client = models.ForeignKey('wms.Client', null=True, blank=True, on_delete=models.PROTECT)
    wh_sku = models.ForeignKey('wms.WhSku', on_delete=models.PROTECT, null=True, blank=True)
    wh_kit_sku = models.ForeignKey('wms.WhKitSku', on_delete=models.PROTECT, null=True, blank=True)
    pack_config = models.ForeignKey('wms.PackConfig', on_delete=models.PROTECT, null=True, blank=True)
    shipping_address = models.ForeignKey(ShippingAddress, on_delete=models.PROTECT, null=True, blank=True)
    quantity = models.IntegerField(_('Sku Quantity'), default=0)
    status = models.CharField(_('Status'), choices=ORDER_LINE_STATUSES, default=ORDER_LINE_NEW,
                              null=True, blank=True, max_length=64)
    allocation_status = models.CharField(_('WMS Allocation Status'), choices=ALLOCATION_STATUSES, blank=True,
                                         null=True, max_length=64)
    reference_number = models.CharField(_('Reference Number'), null=True, blank=True, max_length=64)
    dimension = models.ForeignKey(Dimension, null=True, blank=True, on_delete=models.PROTECT)
    weight = models.FloatField(_("Weight in Kg"), default=0.0)
    volume = models.FloatField(_("Volume in cubic centimeter"), default=0.0)
    product = models.ForeignKey(Product, null=True, blank=True,
                                on_delete=models.PROTECT,
                                related_name='product_sku')
    delivered = models.IntegerField(_('Delivered Units'), default=0)
    qty_delivered = models.IntegerField(_('Quantity Delivered'), default=0)
    qty_removed = models.IntegerField(_('Quantity not picked by driver'),
                                      default=0)
    qty_allocated = models.IntegerField(_('Qty Allocated'), default = 0)
    total_item_price = models.DecimalField(_('Total price for item'),
                                           max_digits=10,
                                           decimal_places=2, default=0)
    each_item_price = models.DecimalField(_('Price for each item'),
                                          max_digits=10,
                                          decimal_places=2, default=0)
    uom = models.CharField(_("UOM"),
                           max_length=255, blank=True, null=True)

    item_code = models.CharField(_("Item Code"),
                                 max_length=255, blank=True, null=True)

    item_sequence_no = models.IntegerField(_("Item sequence number"), null=True,
                                           blank=True)
    item_category = models.CharField(_("Item Category"), max_length=255, null=True, blank=True)

    hsn_code = models.CharField(_("HSN Code"), null=True, blank=True, max_length=10)

    brand = models.CharField(_("Brand"), max_length=255, null=True, blank=True)

    cost_price = models.DecimalField(_("Cost Price"), decimal_places=2,
                                     max_digits=12, null=True, blank=True,
                                     help_text=_('Cost Price'))

    mrp = models.DecimalField(_("MRP"), decimal_places=2,
                              max_digits=12, null=True, blank=True,
                              help_text=_('Cost Price'))

    discounts = models.DecimalField(_("Discounts"), decimal_places=2,
                                     max_digits=12, null=True, blank=True)

    cgst = models.DecimalField(_("CGST"), decimal_places=2,
                                    max_digits=12, null=True, blank=True)

    sgst = models.DecimalField(_("SGST"), decimal_places=2,
                                    max_digits=12, null=True, blank=True)

    igst = models.DecimalField(_("IGST"), decimal_places=2,
                               max_digits=12, null=True, blank=True)

    total_amount_incl_tax = models.DecimalField(_("Total Amount Incl. Tax"), decimal_places=2,
                                     max_digits=12, null=True, blank=True,
                                     help_text=_('Total Amount Incl. Tax'))

    is_settled = models.BooleanField(_('Is settled'), default=False)

    tracking_level = models.CharField(_('Tracking Level'), null=True, blank=True, max_length=128)

    discount_type = models.CharField(_('Discount Type'), max_length=128, default=DISCOUNT_TYPE_FIXED,
                                     choices=DISCOUNT_TYPES, null=True, blank=True)

    copy_data = CopyManager()
    objects = models.Manager()

    @property
    def cgst_amount(self):
        if self.each_item_price and self.each_item_price > 0 and self.cgst and self.cgst > 0:
            return self.each_item_price * self.quantity * self.cgst/100
        return None

    @property
    def sgst_amount(self):
        if self.each_item_price and self.each_item_price > 0 and self.sgst and self.sgst > 0:
            return self.each_item_price * self.quantity * self.sgst/100
        return None

    @property
    def igst_amount(self):
        if self.each_item_price and self.each_item_price > 0 and self.igst and self.igst > 0:
            return self.each_item_price * self.quantity * self.igst / 100
        return None

    @property
    def taxable_value(self):
        if self.each_item_price and self.each_item_price > 0:
            return self.each_item_price * self.quantity
        elif self.total_item_price and self.total_item_price > 0:
            return self.total_item_price
        return None

    @property
    def total_amount(self):
        if self.total_amount_incl_tax and self.total_amount_incl_tax > 0:
            return self.total_amount_incl_tax
        elif self.taxable_value and self.taxable_value > 0:
            if self.cgst_amount and self.sgst_amount and self.cgst_amount > 0 and self.sgst_amount > 0:
                return self.taxable_value + self.cgst_amount + self.sgst_amount
            elif self.igst_amount and self.igst_amount > 0:
                return self.taxable_value + self.igst_amount
            else:
                return self.taxable_value
        return None

    def save(self, *args, **kwargs):
        if not self.created_date:
            self.created_date = timezone.now()
        self.modified_date = timezone.now()
        return super(OrderLine, self).save()

    def __str__(self):
        return str(self.sku if self.sku else self.wh_sku)

    class Meta:
        verbose_name = _("Order Line")
        verbose_name_plural = _("Order Line")


ADJUSTMENT_REASONS = (
    ('Discount', 'Discount'),
    ('others', 'Others')
)


class OrderLineHistory(BaseModel):
    order = models.ForeignKey(
        Order, related_name='order_linehistory', on_delete=models.PROTECT)

    sku = models.ForeignKey('customer.SKU', on_delete=models.PROTECT)

    quantity = models.IntegerField(_('Sku Quantity'), default=0)

    orderline_batch = models.ForeignKey(OrderLineHistoryBatch, on_delete=models.PROTECT)

    total_item_price = models.DecimalField(_('Total price for item'),
                                           max_digits=10,
                                           decimal_places=2, default=0)

    each_item_price = models.DecimalField(_('Price for each item'),
                                          max_digits=10,
                                          decimal_places=2, default=0)

    source = models.CharField(_("Source"),
                                 max_length=50, blank=True, null=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('Order Line History')


class OrderCostHistory(BaseModel):
    """
    Model to store cost history
    """
    order = models.ForeignKey(
        Order, related_name='ordercosthistory', on_delete=models.PROTECT)

    reason = models.CharField(_("Adjustment Reason"),
                              max_length=350)

    old_value = models.DecimalField(_("Value Before Adjusment"),
                                    decimal_places=2, max_digits=12)

    new_value = models.DecimalField(_("Value After Adjusment"),
                                    decimal_places=2, max_digits=12)

    class Meta:
        verbose_name = _('Order Cost History')


class WayPointManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('shipping_address')


class WayPoint(models.Model):
    sequence_id = models.PositiveIntegerField(_("Sequence ID"),
                                              null=True, blank=True, db_index=True)
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    shipping_address = models.OneToOneField(ShippingAddress,
                                            on_delete=models.PROTECT)
    order_line = models.ForeignKey(OrderLine, null=True, blank=True,
                                   on_delete=models.PROTECT)

    estimated_distance = models.FloatField(
        _("Estimated Distance (km)"), null=True, blank=True)

    estimated_time = models.FloatField(
        _("Estimated Time (min)"), null=True)

    objects = WayPointManager()

    def __str__(self):
        return str(self.id)

    def get_order_for_waypoint(waypoint_id):

        try:
            waypoint = WayPoint.objects.get(pk=waypoint_id)
        except waypoint.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return waypoint.order

    class Meta:
        verbose_name = _("Order WayPoint")
        verbose_name_plural = _("Order WayPoints")


class Labour(models.Model):
    """
    If a order involves a labour like helper, capture them here
    """
    labour_cost = models.DecimalField(
        _("Cost of one labourer for the order"), max_digits=5, decimal_places=2)

    no_of_labours = models.PositiveSmallIntegerField(
        _("No of labourers needed for the order"))

    order = models.OneToOneField(Order, related_name='labour', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Order Labours")
        verbose_name_plural = _("Order Labours")


class OrderCancellationReason(models.Model):
    reason = models.TextField()
    seq_no = models.PositiveIntegerField(_("Sequence ID"), null=True,
                                         blank=True)

    class Meta:
        verbose_name = _("Order Cancellation Reason")
        verbose_name_plural = _("Order Cancellation Reasons")


class Rating(models.Model):
    """
    If a order related ratings for both driver and customer
    """
    driver_rating = models.FloatField(
        _("Rating given by Customer for the Driver"))

    customer_rating = models.FloatField(
        _("Rating given by Driver for the Customer"))

    order = models.OneToOneField(Order, on_delete=models.CASCADE)


class ProductRating(models.Model):
    """
    A review of a product.
    """
    product = models.ForeignKey(
        Product, related_name='productreviews', null=True,
        on_delete=models.CASCADE)

    line = models.ForeignKey('order.OrderLine', on_delete=models.PROTECT,
                             related_name='productline', null=True, blank=True)

    rating = models.FloatField(_("Rating"), default=0.0)

    title = models.CharField(_("Product review title"),
                             max_length=255, null=True, blank=True)

    body = models.TextField(_("Body"), null=True, blank=True)

    # User information.
    user = models.ForeignKey('users.User',
                             blank=True,
                             null=True,
                             on_delete=models.CASCADE,
                             related_name='rating')

    FOR_MODERATION, APPROVED, REJECTED = 0, 1, 2
    STATUS_CHOICES = (
        (FOR_MODERATION, _("Requires moderation")),
        (APPROVED, _("Approved")),
        (REJECTED, _("Rejected")),
    )

    status = models.SmallIntegerField(_("Status"), choices=STATUS_CHOICES)

    # Denormalised vote totals
    total_votes = models.IntegerField(
        _("Total Votes"), default=0)  # upvotes + down votes
    delta_votes = models.IntegerField(
        _("Delta Votes"), default=0, db_index=True)  # upvotes - down votes

    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('product', 'user', 'line'),)
        verbose_name = _('Product Rating')
        verbose_name_plural = _('Product Rating')

    def save(self, *args, **kwargs):
        super(ProductRating, self).save(*args, **kwargs)
        self.product.update_rating()

    def delete(self, *args, **kwargs):
        super(ProductRating, self).delete(*args, **kwargs)
        if self.product is not None:
            self.product.update_rating()


class LrNumber(BaseModel):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    lr_number = models.CharField(
        unique=True, max_length=20,
        null=True, blank=True, help_text="LR No."
    )

    class Meta:
        verbose_name = _("LR Number")
        verbose_name_plural = _("LR Number")


class OrderConstants(models.Model):
    """
    Contains the constants for order
    """
    history = AuditlogHistoryField()
    name = models.CharField(max_length=50)
    value = models.FloatField()
    description = models.CharField(max_length=200, blank=True, null=True)

    def get(self, name, default):
        try:
            constant = OrderConstants._default_manager.get(name=name)
        except OrderConstants.DoesNotExist:
            return default

        return constant.value

    class Meta:
        verbose_name = _('Configuration')
        verbose_name_plural = _('Configurations')


class EventManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('address')


class Event(BaseModel):
    status = models.CharField(
        _("Status"), max_length=100, blank=True, db_index=True)
    time = models.DateTimeField(_("Created time"), default=timezone.now)
    address = models.ForeignKey(ShippingAddress, null=True, blank=True,
                                on_delete=models.PROTECT)
    hub = models.ForeignKey(Hub, null=True, blank=True,
                                on_delete=models.PROTECT)
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    driver = models.ForeignKey(Driver, null=True, blank=True,
                               on_delete=models.PROTECT)
    current_location = models.PointField(null=True, blank=True)
    current_address = JSONField(null=True, blank=True)
    hub_associate = models.ForeignKey(User, null=True, blank=True,
                                      on_delete=models.PROTECT)
    remarks = models.CharField(_("Remarks"),
                               max_length=350, blank=True, null=True)
    objects = EventManager()

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')

    def to_json(self):
        return {
            'status': self.status,
            'time': self.time.isoformat(),
            'order_id': self.order_id,
            'driver_id': self.driver_id,
            'remarks': self.remarks
        }

    def serialize_hook(self, hook):
        order = self.order
        shipping_address = order.shipping_address
        shipment_info = None
        try:
            if order.shipment_info:
                shipment_info = json.loads(order.shipment_info)
        except:
            pass

        data = {
            "WaybillNo": order.customer_reference_number or order.reference_number or order.number,
            "OrderStatus": ORDER_STATUSES.get(order.status, "Pending"),
            "CourierComment": "",
            'StatusDateTime': order.updated_time.isoformat(),
            'ToMobileNumber': str(shipping_address.phone_number) if shipping_address.phone_number else '',
            'ToEmail': shipment_info.get('customer_email', '') if shipment_info and type(shipment_info) == dict else '',
            'ToName': shipping_address.first_name,
            'Client': str(order.customer),
            'StatusCode': ORDER_STATUSES.get(order.status, ORDER_NEW),
            'EDD': order.expected_delivery_time.isoformat() if order.expected_delivery_time else '',
            "order_no": order.reference_number,
            "order_status": ORDER_STATUSES.get(order.status, "Pending")

        }

        return data


class DeliveredAddress(models.Model):
    geopoint = models.PointField(null=False, blank=False)
    postcode = models.CharField(
        _("Post/Zip-code"), max_length=64, null=False, blank=False)
    address = models.CharField(
        _("Address"), null=False, max_length=500, blank=False)

    class Meta:
        verbose_name = _('Delivered Address')
        verbose_name_plural = _('Delivered Addresses')


class OrderAdjustmentCategory(models.Model):
    name = models.CharField(
        _('Adjustment Category'), max_length=60)

    description = models.CharField(
        _('Adjustment Description'), max_length=250)

    is_tax_applicable = models.BooleanField(
        _('Tax applicable'), default=False, db_index=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _("Order Adjustment Category")
        verbose_name_plural = _("Order Adjustment Categories")


class OrderAdjustment(BaseModel):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    category = models.ForeignKey(OrderAdjustmentCategory, null=True, on_delete=models.CASCADE)

    comments = models.CharField(
        _('Comments (Optional)'), max_length=100, null=True, blank=True)

    total_amount = models.DecimalField(
        _("Total Amount"), max_digits=30, decimal_places=2, default=0)


class OrderDispatch(BaseModel):
    """
    Model to store dispatch status and time
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)

    status = models.CharField(
        _("Status"), max_length=100)

    geopoint = models.PointField(null=True)

    response_point = models.PointField(null=True)

    distance = models.FloatField(default=0.0)

    mqtt_response = models.BooleanField(default=False)

    fcm_response = models.BooleanField(default=False)


class OrderPaymentHistory(models.Model):
    """
    Model to store payment status updates for booking orders
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    old_payment_status = models.CharField(
        _("Old Payment Status"), max_length=250,
        default=PAYMENT_STATUS_UNPAID, blank=True, null=True)

    new_payment_status = models.CharField(
        _("New Payment Status"), max_length=250,
        default=PAYMENT_STATUS_PAID, blank=True, null=True)

    modified_date = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.order)

    class Meta:
        verbose_name = _("Booking order Payment History")
        verbose_name_plural = _("Booking order Payment History")


class OrderRequest(models.Model):
    """ Order Request data made by users."""
    driver_id = models.IntegerField(_("Driver Id"), null=True, blank=True)

    contract_id = models.IntegerField(_("Contract Id"))

    user = models.ForeignKey(
        'users.User', on_delete=models.PROTECT)

    is_created = models.BooleanField(_("Is Order Created"), default=False)

    hub_id = models.IntegerField(_("Hub Id"), null=True, blank=True)

    order_communication_id = models.IntegerField(_("Order Communication Id"), null=True, blank=True)

    pickup_datetime = models.CharField(
        _("Requested Pickup Date and time"), max_length=50, blank=True, null=True)

    error_message = models.CharField(
        _("Error Message"), max_length=500, blank=True, null=True)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s-%s" % (self.user, self.contract_id)

    class Meta:
        verbose_name = _('Order Request')
        verbose_name_plural = _('Order Requests')


class OrderRating(BaseModel):
    """
    rating provided by driver for each order
    """
    driver = models.ForeignKey(
        'driver.Driver', on_delete=models.PROTECT)

    order = models.OneToOneField(
        'order.Order', on_delete=models.PROTECT)

    rating = models.DecimalField(
        _("Aggregated rating for the driver"),
        max_digits=3, decimal_places=2, default=0)

    remark = models.ForeignKey(
        'driver.RatingCategory', on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s-%s" % (self.driver, self.order)

    class Meta:
        verbose_name = _('Order Rating')
        verbose_name_plural = _('Order Ratings')


class Container(BaseModel):
    """
    Container details
    """
    number = models.CharField(_('Container number'), null=False, blank=False,
                              max_length=50)

    customer = models.ForeignKey('customer.Customer', null=True, blank=True,
                                 on_delete=models.PROTECT)

    container_type = models.ForeignKey('customer.ContainerType', null=True,
                                       blank=False, on_delete=models.PROTECT)

    status = models.CharField(_('Container Status'), max_length=50, null=True, blank=True)

    class Meta:
        verbose_name = _('Container')

    def __str__(self):
        return self.number


class OrderContainer(BaseModel):
    """
    Order containers details
    """
    order = models.ForeignKey(Order, on_delete=models.PROTECT)

    container_number = models.CharField(_('Container number'), null=True,
                                        blank=True, max_length=50)

    container = models.ForeignKey(Container, on_delete=models.PROTECT)

    trip = models.ForeignKey('trip.Trip', on_delete=models.PROTECT)

    stop = models.ForeignKey('trip.Stop', on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Order Container')

    def __str__(self):
        return '%s-%s' % (self.order, self.container)


class OrderContainerHistory(BaseModel):
    """
    Order containers details
    """
    order = models.ForeignKey(Order, null=True, blank=True,
                              on_delete=models.PROTECT)

    customer = models.ForeignKey('customer.Customer', null=True, blank=True,
                                 on_delete=models.PROTECT)

    container_number = models.CharField(_('Container number'), null=True,
                                        blank=True, max_length=50)

    container_type = models.CharField(_('Container Type'), null=True,
                                      blank=True, max_length=50)

    trip = models.ForeignKey('trip.Trip', null=True, blank=True,
                             on_delete=models.PROTECT)

    stop = models.ForeignKey('trip.Stop', null=True, blank=True,
                             on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Order Container History')

    def __str__(self):
        return '%s-%s' % (self.order, self.container)


class OrderMessage(models.Model):
    """
    Order Message
    """
    order = models.ForeignKey(Order, null=True, blank=True,
                              on_delete=models.PROTECT)

    msg_event = models.CharField(_("Message Event"), max_length=50, null=True,
                                 blank=True)

    trip = models.ForeignKey('trip.Trip', null=True, blank=True,
                             on_delete=models.PROTECT)

    time = models.DateTimeField(_("Time"), auto_now_add=True)

    message = models.TextField(_("Message Text"), null=True, blank=True)

    class Meta:
        verbose_name = _('Order Message')

    def __str__(self):
        return '%s-%s' % (self.order, self.msg_event)


class UTMTracking(BaseModel):
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    utm_source = models.CharField(_('UTM Source'),
                                  max_length=255, db_index=True)
    utm_medium = models.CharField(_('UTM Medium'), max_length=255)
    utm_campaign = models.CharField(_('UTM Campaign'), max_length=255)
    extra_data = models.TextField(blank=True, null=True)

    copy_data = CopyManager()
    objects = models.Manager()

    class Meta:
        verbose_name = _('UTM Tracking')
        verbose_name_plural = _('UTM Tracking')

    def __str__(self):
        return '%s | %s | %s' % (
            self.utm_source, self.utm_medium, self.utm_campaign)


class FlashSaleBanner(BaseModel):
    is_active = models.BooleanField(_("Is Active"), default=False)

    banner = models.FileField(upload_to=OSCAR_IMAGE_FOLDER,
                              validators=[FileValidator()],
                              max_length=256,
                              null=True,
                              blank=True)

    city = models.ForeignKey(City, null=True, blank=False,
                             on_delete=models.PROTECT)

    product = models.ManyToManyField(Product, blank=True,
                                     related_name='product_banner')

    copy_data = CopyManager()
    objects = models.Manager()

    class Meta:
        verbose_name = _('FlashSale Banner')
        verbose_name_plural = _('FlashSale Banner')

    def __str__(self):
        return str(self.city)


class CallMaskingLog(BaseModel):
    order = models.ForeignKey('order.Order', on_delete=models.CASCADE)

    driver = models.ForeignKey('driver.Driver', on_delete=models.CASCADE, null=True, blank=True)

    from_number = models.CharField(_('From'), max_length=12)

    to_number = models.CharField(_('To'), max_length=12)

    caller_id = models.CharField(_('Caller Id'), max_length=12)

    duration = models.FloatField(_("Total Duration (sec)"), default=0.0)

    sid = models.CharField(unique=True, max_length=100)

    price = models.DecimalField(_("Cost"),
                                max_digits=10, decimal_places=2, default=0)

    response = models.TextField(null=True, blank=True)


class OrderAwbSequence(models.Model):
    """
    Advance AWB Sequence tracker
    """
    customer = models.ForeignKey('customer.Customer', null=True, blank=True,
                                 on_delete=models.PROTECT)

    hub_code = models.CharField(_("Hub Code"), null=True, blank=True, max_length=10)

    awb_count = models.PositiveIntegerField(_("Count of AWB numbers"))

    seq_start = models.PositiveIntegerField(_("Sequence Start"))

    seq_end = models.PositiveIntegerField(_("Sequence End"))

    seq_used = models.IntegerField(_("Number of sequences used"), null=True,
                                   blank=True)

    created_date = models.DateTimeField(_("AWB generation date"),
                                        auto_now_add=True)

    class Meta:
        verbose_name = _('Advance AWB Sequence')

    def __str__(self):
        return '%s-%s' % (self.customer, self.awb_count)

class ShippingLabelRequest(BaseModel):
    """
    Shipping Label Request
    """
    customer = models.ForeignKey('customer.Customer', null=True, blank=True,
                                 on_delete=models.PROTECT)

    file = models.FileField(_("Uploaded File"),
                            upload_to=generate_file_path,
                            max_length=256, blank=True, null=True)

    user = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    class Meta:
        verbose_name = _('Shipping Label Request')


class ShipmentOrderMView(models.Model):
    company_name = models.CharField(max_length=200, null=True)
    pickup_hub_name = models.CharField(max_length=200, null=True)
    delivery_hub_name = models.CharField(max_length=200, null=True)
    customer_reference_number = models.CharField(max_length=50, null=True)
    reference_number = models.CharField(max_length=50, null=True)
    status = models.CharField(max_length=20, null=True)
    date_placed = models.CharField(max_length=20, null=True)
    customer_name = models.CharField(max_length=200, null=True)
    customer_address = models.TextField(null=True, blank=True)
    pincode = models.CharField(max_length=10, null=True)
    latitude = models.CharField(max_length=100, null=True)
    longitude = models.CharField(max_length=100, null=True)
    customer_phone_number = models.CharField(max_length=15, null=True)
    pickup_name = models.CharField(max_length=100, null=True)
    pick_up_address = models.TextField(null=True, blank=True)
    pickup_pincode = models.CharField(max_length=10, null=True)
    pickup_phone_number = models.CharField(max_length=15, null=True)
    cash_on_delivery = models.CharField(max_length=1)
    number = models.CharField(max_length=128, db_index=True, unique=True, blank=True)
    driver = models.CharField(max_length=50, null=True)
    driver_phone_number = models.CharField(max_length=15, null=True)
    expected_delivery_time = models.CharField(max_length=45, null=True)
    packed_at = models.CharField(max_length=45, null=True)
    out_on_road_at = models.CharField(max_length=45, null=True)
    first_attempted_at = models.CharField(max_length=45, null=True)
    last_attempted_at = models.CharField(max_length=45, null=True)
    delivered_at = models.CharField(max_length=45, null=True)
    remarks = models.CharField(max_length=250, null=True)
    state = models.CharField(max_length=50, null=True)
    city = models.CharField(max_length=100, null=True)
    sku = models.TextField(null=True, blank=True)
    total_sku_qty = models.CharField(max_length=20, null=True)
    picked_time = models.CharField(max_length=20, null=True)
    bill_date = models.CharField(max_length=45, null=True)
    pod_files = models.TextField(null=True, blank=True)
    weight = models.CharField(max_length=20, null=True)
    volume = models.CharField(max_length=20, null=True)

    copy_data = CopyManager()

    class Meta:
        managed = False
        db_table = 'order_shipmentordermview'


# from blowhorn.oscar.apps.order.models import *  # noqa
