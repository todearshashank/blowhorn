# System and Django libraries
from django.conf import settings
from django.db.models import Q
from datetime import datetime, timedelta
from django.utils import timezone

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model

# Blowhorn imports
from blowhorn.contract.permissions import is_spoc_or_supervisor, is_supervisor
from blowhorn.trip.models import Trip
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.models import APPROVED
from blowhorn.customer.constants import REJECTED, CANCELLED, CREATED, INVOICE_EDITABLE_STATUS
from blowhorn.trip.models import Stop
from blowhorn.customer.models import InvoiceOrder, InvoiceTrip
from blowhorn.order.const import PAYMENT_MODE_RAZORPAY, PAYMENT_STATUS_PAID
from blowhorn.contract.constants import CONTRACT_TYPE_SME
from blowhorn.company.models import Company

PaymentTrip = get_model('driver', 'PaymentTrip')


def is_booking_order(instance):
    """ Condition to check whether the order type is booking or not """
    if instance.order_type in [settings.DEFAULT_ORDER_TYPE, CONTRACT_TYPE_SME]:
        return True
    return False

def is_order_not_paid(instance):
    """ Condition to check whether the order paid or not """
    if instance.payment_status == PAYMENT_STATUS_PAID:
        return False
    return True


def is_qualified_for_expiry(instance):
    """
    If all the trips of the order is in No-show status,
    and order is in New status, older than DAYS_OLDER_EXPIRE_ORDER(default 3) days, then order is eligible to be marked as Expired
    """
    from .models import OrderConstants

    print('check if order is eligible to goto Expired status')
    trip_status = StatusPipeline.TRIP_NO_SHOW
    now = timezone.now().date()
    days_to_expire = OrderConstants().get('DAYS_OLDER_EXPIRE_ORDER', 3)
    older = now - timedelta(days=days_to_expire)

    other_status_trips = Trip.objects.filter(order=instance). \
                                        exclude(status=trip_status).exists()
    if not other_status_trips:
        expire_order = Trip.objects.filter(order=instance,
                            planned_start_time__lte=older).exists()

        if expire_order:
            print('order can be expired')
            return True
    return False


def isnot_booking_order(instance):
    """ Condition to check whether the order type is booking or not """
    if instance.order_type == settings.DEFAULT_ORDER_TYPE:
        return False
    return True


def is_not_shipment_order(instance):
    """ Condition to check whether the order type is shipment and return False"""
    if instance.order_type == settings.SHIPMENT_ORDER_TYPE:
        return False
    return True


def is_shipment_order(order):
    """ Condition to check whether the order type is shipment"""
    return not is_not_shipment_order(order)

def is_ecom_order(order):
    return True if order.order_type == settings.ECOMM_ORDER_TYPE else False


def is_contract_spoc_or_supervisor(instance, user):
    return is_spoc_or_supervisor(instance.customer_contract, user)


def is_non_enterprise_manager(instance, user):
    return Company.objects.filter(non_enterprise_manager=user).exists()


def is_contract_supervisor(instance, user):
    return is_supervisor(instance.customer_contract, user)


def is_driver_assigned(instance):
    trip = Trip.objects.filter(
        order_id=instance.id,
        status__in=[
            StatusPipeline.TRIP_IN_PROGRESS,
            StatusPipeline.TRIP_NEW,
            StatusPipeline.DRIVER_ACCEPTED,
            StatusPipeline.TRIP_ALL_STOPS_DONE
        ]).first()
    if trip and (not trip.actual_start_time or not trip.actual_end_time):
        return False

    if not trip:
        return False

    if instance.driver:
        return True
    return False


def is_payment_unapproved(order):
    # If there are pending payment records

    if order.driver:

        return not PaymentTrip.objects.filter(
            trip__order=order,
            payment__status=APPROVED
        ).exists()

    return True


def is_ongoing_trip(order):
    stop = Stop.objects.filter(
        Q(waypoint__order=order) & ~Q(status=StatusPipeline.TRIP_CANCELLED)).first()
    if stop:
        return Trip.objects.filter(
            Q(driver=order.driver, pk=stop.trip.id) & ~Q(
                status__in=StatusPipeline.TRIP_END_STATUSES)).exists()
    return False


def is_not_ongoing_trip(order):
    return not is_ongoing_trip(order)


def is_qualified_for_dummy(instance):
    """
    If order or trip has already been invoiced, do not allow to mark dummy
    """
    order_invoiced = InvoiceOrder.objects.filter(Q(order=instance) & ~Q(invoice__status__in=INVOICE_EDITABLE_STATUS)).exists()
    trip_invoiced = InvoiceTrip.objects.filter(Q(trip__in=instance.trip_set.all()) & ~Q(invoice__status__in=INVOICE_EDITABLE_STATUS)).exists()

    if order_invoiced or trip_invoiced:
        return False

    if instance.order_type == settings.SHIPMENT_ORDER_TYPE:
        query = Q(waypoint__order=instance) | Q(order=instance)
        return not Stop.objects.filter(query).exclude(status=StatusPipeline.TRIP_CANCELLED).exists()
    else:
        return not (instance.payment_status == PAYMENT_STATUS_PAID) and is_payment_unapproved(instance)
