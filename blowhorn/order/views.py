# System and Django libraries
import os
import json
import logging
import traceback

from django.core.exceptions import ValidationError

import phonenumbers
import operator
import math
import requests
import pytz
import re
from io import BytesIO
from ipaddress import ip_address, IPv4Network

from functools import reduce
from datetime import datetime, timedelta, time
from random import randint
from dateutil.parser import parse
from django.db import transaction
from django.db.models import Q, Prefetch, ExpressionWrapper, Func, \
                    FloatField, F, DateTimeField, Value, CharField
from django.db import transaction
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.http import HttpResponse
from django.core.files import File
from django.contrib.auth import authenticate
from django.template.response import TemplateResponse

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model
from blowhorn.common.utils import TimeUtils
from blowhorn.common import sms_templates
from rest_condition import Or
from rest_framework import generics, status, views
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser

# Blowhorn imports
from blowhorn.order.utils import OrderUtil, get_skus_for_the_customer, \
    get_divisions_for_the_customer, get_hubs_for_the_customer, \
    get_advance_awb_numbers
from blowhorn.order.services.label import ShippingLabel
from blowhorn.utils.datetime_function import DateTime
from blowhorn.oscar.core.loading import get_model
from config.settings import status_pipelines as StatusPipeline
from blowhorn.order.serializers.OrderSerializer import OrderSerializer, \
    OrderListSerializer
from blowhorn.order.serializers.shipment import OrderShipmentSerializer, \
    OrderDetailsSerializer
from blowhorn.order.serializers.shipment import OrderStatusHistorySerializer, OrderEventHistoryBulkSerializer
from blowhorn.driver.serializers import DriverActivitySerializer
from blowhorn.common.helper import CommonHelper
from blowhorn.customer.views import get_customer_from_api_key, OrderCreate
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, CustomerDivision, \
    get_partner_from_api_key
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.customer.serializers import FleetSearchFormVehicleTypeSerializer, \
    FleetSearchFormCitySerializer
from blowhorn.order.mixins import OrderPlacementMixin, OrderlineMixin
from blowhorn.utils.mail import Email
from blowhorn.trip.serializers import AppEventsSerializer
from blowhorn.trip.mixins import TripMixin
from blowhorn.utils.functions import get_tracking_url
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.address.utils import within_city_working_hours, \
    get_city_from_geopoint, get_city_from_pincode, get_city_from_latlong
from blowhorn.utils.functions import utc_to_ist, minutes_to_dhms, get_base_url
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_HYPERLOCAL, \
    CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_FLASH_SALE
from blowhorn.address.serializer import ShippingAddressSerializer, ContactNumberSerializer
from blowhorn.company.models import Office
from blowhorn.common.serializers import BaseSerializer
from blowhorn.company.serializers import OfficeSerializer
from blowhorn.trip.views import TripTrackingView
from blowhorn.order.const import SIGNATURE, PROOF_OF_DELIVERY, PROOF_OF_RTO, PROOF_OF_PICKUP,\
    PROOF_OF_NON_DELIVERY, PROOF_OF_REJECT, POD_PDF, DEVICE_TYPE_API, \
    OTP_SIGN, ORDER_TYPE_NOW, PAYMENT_MODE_PAYTM, \
    PAYMENT_STATUS_PAID, BLOWHORN_WALLET, PAYMENT_MODE_CASH
from blowhorn.order.tasks import initiate_paytm_refund
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.address.models import City, Hub
from blowhorn.vehicle.models import VehicleClass
from blowhorn.customer.constants import SHIPMENT_DATA_TO_BE_IMPORTED, ORDERLINE_COLUMNS_FOR_EXPORT, COMPANY_ID
from blowhorn.common.utils import export_to_spreadsheet, generate_pdf
from blowhorn.utils.html_to_pdf import render_to_pdf, render_to_pdfstream
from blowhorn.order.services.shipment_order_import import ShipmentOrderImport, \
    ShipmentOrderlineImport
from blowhorn.order.services.unicom_shipment_import import UnicomShipmentOrder
# from blowhorn.order.services.orderskus import OrderSkuService
from blowhorn.common.tasks import send_sms
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.utils import CustomerUtils, decrypt_account_number

from blowhorn.order.helper import ShipmentOrderValidation, get_order_authentication_details
from blowhorn.order.models import ShippingAddress, OrderConstants, OrderApiData
from blowhorn.order.utils import OrderNumberGenerator
from blowhorn.apps.kiosk.views import CreateOrder
from blowhorn.contract.contract_helpers.contract_helper import ContractHelper
from blowhorn.users.utils import UserUtils
from blowhorn.utils.functions import ist_to_utc
from blowhorn.customer.tasks import broadcast_order_details_to_dashboard
from django.db.models import Count, Avg, Case, When, IntegerField, Sum
from blowhorn.route_optimiser.models import OptimalRoute

Order = get_model('order', 'Order')
WayPoint = get_model('order', 'WayPoint')
Event = get_model('order', 'Event')
# OrderNote = get_model('order', 'OrderNote')
OrderDocument = get_model('order', 'OrderDocument')
Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
DriverVehicleMap = get_model('driver', 'DriverVehicleMap')
Driver = get_model('driver', 'Driver')
DriverActivity = get_model('driver', 'DriverActivity')
DriverActivityHistory = get_model('driver', 'DriverActivityHistory')
Customer = get_model('customer', 'Customer')
PartnerCustomer = get_model('customer', 'PartnerCustomer')
CustomerInvoice = get_model('customer','CustomerInvoice')
Contract = get_model('contract', 'Contract')
Country = get_model('address', 'Country')
CustomerBankDetails = get_model('customer', 'CustomerBankDetails')
ShopifyLog = get_model('shopify', 'ShopifyLog')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class OrderList(generics.CreateAPIView,
                OrderPlacementMixin, CustomerMixin):
    permission_classes = (AllowAny,)
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get(self, request):

        results = self.get_queryset() or []
        _dict = self.serializer_class().get_orders(
            request=request, results=results)

        headers = settings.HEADER
        return Response(status=status.HTTP_200_OK, data=_dict, headers=headers)

    def post(self, request):
        context = {'request': request}
        request_vars = request.data
        order_action = request_vars.get('action', False)
        order_data = request_vars.get('data', False)
        if request.META.get('HTTP_API_KEY', None):
            order_data = request_vars
            order_action = 'create'

        trip_id = request_vars.get('trip_id')

        if order_data:  # c2c case
            if isinstance(order_data, str):
                order_data = json.loads(order_data)
            # Order data has been sent. Try to create a order with this data.
            if 'create' == order_action:
                return self.create_c2c_order(request, data=order_data,
                                             context=context)

        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_(
                    'Download Blowhorn Partner app from Play Store'
                )
            )
            location = request_vars.get('location_details', '[]')
            # verify if the reference number is sent to get order details from
            # some client.
            item_ids = request_vars.get('item_ids') or None
            if isinstance(item_ids, str):
                try:
                    items = json.loads(item_ids)
                except ValueError:
                    logger.error('Invalid format to send the Items.')
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data="Invalid format to send the Items.")
            else:
                items = item_ids

            if not isinstance(item_ids, dict) and not items:
                logger.error('Items list is empty.')
                return Response(status=status.HTTP_400_BAD_REQUEST, data="Items list is empty")

            geopoint = None
            if location:
                try:
                    location = json.loads(location)
                except BaseException:
                    # logger.error('Invalid Location json sent')
                    return Response(status=status.HTTP_200_OK,
                                    data="Invalid Location json sent")

                latitude = location.get('mLatitude')
                longitude = location.get('mLongitude')

                geopoint = CommonHelper.get_geopoint_from_latlong(
                    latitude, longitude)
            return self._assign_orders_to_driver(
                items=items, request=request,
                context=context, geopoint=geopoint, trip_id=trip_id)

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Couldn\'t process this request. Try providing '
                             'enough arguments.')

    def create_c2c_order(self, request, data, context):
        user = request.user
        if user and user.is_authenticated and user.is_staff:
            _data = {
                "status": "FAIL",
                "message": _("You're not a valid customer")
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_data)

        customer = None
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = get_customer_from_api_key(api_key)
            if not customer:
                _out = {
                    'status': 'FAIL',
                    'message': _('Unauthorized')
                }
                return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

            pickup_time = data.get('pickup_time')
            now_or_later = pickup_time.get('type')
            if now_or_later == "later" and not pickup_time.get('value'):
                _data = {
                    "status": "FAIL",
                    "message": _("`value` for `pickup_time` is mandatory for "
                                 "advanced booking")
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_data)

            pickup_datetime = DateTime().convert_iso_string_to_datetime(
                pickup_time.get('value'))
            if now_or_later == "later" and timezone.now() > pickup_datetime:
                _data = {
                    "status": "FAIL",
                    "message": _("Pickup date time should be in future for "
                                 "`later` booking")
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_data)

        else:
            # Guest User case
            phone_number = request.META.get('HTTP_MOBILE')
            if user and not user.is_authenticated and phone_number:
                customer, user = CustomerUtils().get_customer_from_phone_number(
                    phone_number)

            elif user.is_authenticated:
                customer = self.get_customer(request.user)

            if not customer:
                _data = {
                    "status": "FAIL",
                    "message": _("You're not a valid customer")
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_data)

        geopoint = data.get('pickup').get('geopoint') or \
                   data.get('pickup').get('_geopoint')
        data = OrderUtil().get_formatted_order_data(data)

        credit_amount_eligibility, pending_amount = customer.get_credit_amount_eligibility()
        credit_period_eligibility = customer.get_credit_days_eligibility()
        proceed = credit_amount_eligibility and credit_period_eligibility
        if not proceed:
            from blowhorn.customer.tasks import send_credit_limit_slack_notification
            if credit_amount_eligibility:
                pending_amount = 0
            send_credit_limit_slack_notification.delay(customer.id,
                                                       pending_amount,
                                                       credit_period_eligibility)

            _data = {
                "status": "FAIL",
                "message": _('Please complete payment for previous trips '
                             'to proceed with booking.')
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_data)

        lat = geopoint.get('lat', 0) or geopoint.get('_lat', 0) or 0
        lng = geopoint.get('lng', 0) or geopoint.get('_lng', 0) or 0
        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=data['pickup_datetime'],
            lat=lat,
            lon=lng
        )
        if not is_valid_booking_time:
            _data = {
                "status": "FAIL",
                "message": _('Service is unavailable at this time. '
                             'Please schedule for later.')
            }
            return Response(_data, status=status.HTTP_400_BAD_REQUEST)

        context['customer'] = customer

        if data.get('payment_mode') == BLOWHORN_WALLET:
            cust_balance = customer.get_customer_balance()
            if cust_balance < data.get('estimated_cost'):
                response = {
                    'status': 'FAIL',
                    'message': _('InSufficent Funds...')
                }
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=response)

        order_serializer = OrderSerializer(context=context, data=data)
        order = order_serializer.save()

        if data.get('payment_mode') == BLOWHORN_WALLET:
            debit_status = customer.put_order_amount_on_hold(order)
            if not debit_status:
                # if not enough funds change the payment mode
                order.payment_mode = PAYMENT_MODE_CASH

        if order:
            order_number = order.number
            tracking_url = get_tracking_url(context['request'], order_number)
            message = {
                'tracking_url': tracking_url,
            }
            if data.get('device_type', None) == DEVICE_TYPE_API:
                extra_info = {
                    'order_number': order_number,
                    'estimated_distance': '%.2f' % order.estimated_distance_km,
                    'estimated_cost': '%.2f' % order.estimated_cost
                }
            else:
                extra_info = {
                    'booking_type': 'advanced',
                    'friendly_id': order_number,
                }

            message.update(extra_info)
            response = {
                'status': 'PASS',
                'message': message
            }
            OrderCreate().post_save(order)
            return Response(status=status.HTTP_200_OK, data=response)

        response = {
            'status': 'FAIL',
            'message': _('Unexpected Error')
        }
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                        data=response)

    def _assign_orders_to_driver(self, request, context,
                                 items, geopoint=None, trip_id=None):
        order_serializer = OrderSerializer(context=context, data=items)
        response = order_serializer.assign_to_driver(
            geopoint=geopoint, trip_id=trip_id)
        if not response:
            logger.error('Driver Assigning Failed')
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Driver Assigning Failed')

        return Response(status=status.HTTP_200_OK, data=response)

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        order_qs = Order._default_manager.filter(customer=customer).exclude(
            status=StatusPipeline.ORDER_CANCELLED)

        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )
        return list(order_qs)

    def _fetch_site_order_details(self, reference_number):
        order_type = settings.SHIPMENT_ORDER_TYPE
        order_data = {
            'total': 540,
            'guest_email': 'test@test.com',
            'order_type': order_type,
            'reference_number': reference_number,
            'shipping_address': {
                'title': 'Mr',
                'first_name': 'XYZ',
                'last_name': 'Test',
                'line1': 'Test Line 1',
                'line2': 'Test Line 2',
                'line3': 'Test Line 3',
                'line4': 'City',
                'state': 'Karnataka',
                'postcode': '560102',
                'phone_number': '+918888999888',
            },
            'pickup_address': {
                'title': 'Mr',
                'first_name': 'XYZ',
                'last_name': 'Test',
                'line1': 'Test Line 1',
                'line2': 'Test Line 2',
                'line3': 'Test Line 3',
                'line4': 'City',
                'state': 'Karnataka',
                'postcode': '560102',
                'phone_number': '+918888999888',
                'notes': ''
            }
        }
        return order_data


class OrderDetail(generics.UpdateAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication,)
    permission_classes = (AllowAny,)
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get(self, request, pk=None):
        order = get_object_or_404(Order, pk=pk)

    def put(self, request, pk=None):
        context = {'request': request}
        request_vars = request.data
        order_queryset = self.get_queryset().filter(id=pk)
        update_data = request_vars.get('data', False)

        if not update_data:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Update data doesn\'t exists')

        if 0 == order_queryset.count():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Order does not exists')

        update_data = json.loads(update_data)
        order_serializer = OrderSerializer(
            order_queryset.get(), data=order_queryset.values().first(), context=context)

        if isinstance(update_data, dict):
            update_response = order_serializer.update_order(update_data)
            if update_response:
                return Response(status=status.HTTP_200_OK, data='successfully updated.')

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Error updating order.')

    def post(self, request, pk=None):
        data = request.data
        reason = data.get('reason', None)

        if not reason:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Please, provide a reason for cancellation')

        try:
            order = Order.objects.get(pk=pk)
        except BaseException:
            return Response(status=status.HTTP_404_NOT_FOUND, data='Order not found')

        logger.info('Customer cancelling the order: %s %s' %
                    (order.number, order.status))
        if order.status == StatusPipeline.ORDER_NEW:
            order.status = StatusPipeline.ORDER_CANCELLED
            order.reason_for_cancellation = reason
            order.save()
            if order.payment_mode == PAYMENT_MODE_PAYTM and \
                order.payment_status == PAYMENT_STATUS_PAID and order.amount_paid_online > 0 \
                and not order.is_refunded:
                initiate_paytm_refund.apply_async(
                    (order.id,), )

            event = Event.objects.create(
                status=StatusPipeline.ORDER_CANCELLED,
                order=order,
                remarks=reason,
                created_by=request.user
            )
            if order.payment_mode == BLOWHORN_WALLET:
                order.customer.reverse_order_amount(order)
            return Response(status=status.HTTP_200_OK,
                            data='Your order has been cancelled successfully')

        logger.info('Failed to cancel order. order status: %s' % order.status)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Failed to cancel the order')

    def get_queryset(self):
        queryset = self.queryset
        if not self.request.user.is_staff:
            queryset = queryset.filter(user_id=self.request.user.pk)
        return queryset


class OrderTrackingView(generics.RetrieveAPIView, generics.DestroyAPIView, OrderPlacementMixin):
    permission_classes = (AllowAny,)
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request, order_id=None):
        result = self.get_queryset().first()

        if result:
            response = OrderUtil().get_booking_details_for_customer(
                request, result, route_info=True)

            return Response(status=status.HTTP_200_OK, data=response,
                            content_type='text/html; charset=utf-8')

        response = {
            'status': 'FAIL',
            'message': 'Order doesn\'t exist'
        }

        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')

    def post(self, request, order_id=None):
        if not request.user.is_authenticated or not request.user.is_staff:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Not an authorized user',
                            content_type='text/html; charset=utf-8')

        data = request.data
        pickup = data.get('pickup', '')
        dropoff = data.get('dropoff', '')
        stops = data.get('stops', '')

        order_qs = self.get_queryset()
        order = order_qs.first()
        if not order:
            return Response(status=status.HTTP_404_NOT_FOUND, data='Order not found',
                            content_type='text/html; charset=utf-8')

        self.save_location(order.pickup_address, pickup)
        self.save_location(order.shipping_address, dropoff)

        waypoints = sorted(order.waypoint_set.all(),
                           key=lambda x: x.sequence_id)

        for i, s in enumerate(stops):
            _location = s.get('_location')
            self.save_location(waypoints[i].shipping_address, _location)

        order_qs.update(modified_by=request.user.pk, modified_date=timezone.now())
        return Response(status=status.HTTP_200_OK, data='Order updated successfully',
                        content_type='text/html; charset=utf-8')

    class Meta:
        lookup_field = 'order__number'
        lookup_url_kwarg = 'order__number'

    def get_queryset(self):
        order_id = self.kwargs.pop('order_id')
        order_qs = Order._default_manager.filter(number=order_id)

        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        return order_qs

    def delete(self, request, order_id):
        order = Order.objects.get(pk=order_id)
        if order.status == StatusPipeline.ORDER_NEW:
            order.status = StatusPipeline.ORDER_CANCELLED
            order.save()

            Event.objects.create(
                status=StatusPipeline.ORDER_CANCELLED,
                order=order,
                driver=order.driver
            )
            return Response(status=status.HTTP_200_OK, data="Success")
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="Order cannot be cancelled")


class OrderPriorRouteView(views.APIView):
    permission_classes = (AllowAny,)
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def _route_info_to_dict_array(self, route_info):
        return [x.__dict__ for x in route_info]

    def get(self, request, order_id=None):
        try:
            order = Order.objects.get(number=order_id)
        except BaseException:
            data = {
                'status': 'pass',
                'route': [],
            }
            return Response(status=status.HTTP_200_OK, data=json.dumps(data))

        lat_lng_list_dict = []
        results_filtered = []
        ist_time_arrived = None
        if isinstance(order.time_arrived_at_pickup, datetime.datetime):
            ist_time_arrived = utc_to_ist(order.time_arrived_at_pickup)

        results = DriverActivity.objects.filter(order_id=order.id)
        results_sorted = sorted(results, key=lambda x: x.gps_timestamp)
        results_filtered = []
        if ist_time_arrived:  # TODO: also check if status > driver_accepted
            results_filtered = [r for r in results_sorted
                                if (r.location_accuracy_meters < 30 and
                                    r.gps_timestamp > ist_time_arrived)]

        if not results_filtered and results_sorted:
            results_filtered = results_sorted[-1:]

        lat_lng_list = [x.geopoint for x in results_filtered]
        lat_lng_list_dict = self._route_info_to_dict_array(lat_lng_list)

        route_data = {
            'status': 'pass',
            'route': lat_lng_list_dict,
        }
        return Response(status=status.HTTP_200_OK, data=json.dumps(route_data))


class OrderStatus(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request, order_id=None):
        order_detail = get_order_authentication_details(request, order_id)

        if not order_detail.get('ret_status') == status.HTTP_200_OK:
            return Response(status=order_detail['ret_status'], data=order_detail['response'])

        order_data = order_detail['response'].pop('order')
        order = order_data.first()

        current_status = StatusPipeline.ORDER_STATUSES.get(order.status)
        delivered_to = ''
        if order.status == StatusPipeline.ORDER_DELIVERED:
           event = Event.objects.filter(order=order, status=StatusPipeline.ORDER_DELIVERED).first()
           delivered_to = event.remarks if event else str(order.customer)
        response = {
            'status': 'PASS',
            'message': {
                'awb_number': order.number,
                'order_status': current_status,
                'delivered_to': delivered_to,
                'is_return_order': order.return_order or False
            }
        }
        logging.info(response)
        return Response(status=status.HTTP_200_OK, data=response)


class CancelOrder(views.APIView):
    permission_classes = (AllowAny,)

    def put(self, request, order_id=None):

        order_detail = get_order_authentication_details(request, order_id)

        if not order_detail.get('ret_status') == status.HTTP_200_OK:
            return Response(status=order_detail['ret_status'], data=order_detail['response'])

        order_data = order_detail['response'].pop('order')
        customer = order_detail['response'].pop('customer')
        order = order_data.first()
        user = request.user

        if not order.status == StatusPipeline.ORDER_NEW:
            response = {
                'status': 'FAIL',
                'message': 'The order can\'t be cancelled now'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        if not (request.user and request.user.is_authenticated):
            user = customer.user

        try:
            order_data.update(status=StatusPipeline.ORDER_CANCELLED,
                                reference_number=None,
                                customer_reference_number=None
                            )

            Event.objects.create(
                status=StatusPipeline.ORDER_CANCELLED,
                order=order,
                address=order.shipping_address,
                hub_associate=user
            )
        except Exception as e:
            logging.info("Error while updating status : %s", e.args[0])
            response = {
                'status': 'FAIL',
                'message': 'Something went wrong while updating status'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        response = {
            'status': 'PASS',
            'message': 'Order Cancelled Successfully'
        }
        logging.info(response)
        return Response(status=status.HTTP_200_OK, data=response)


class OrderStatusHistory(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = OrderStatusHistorySerializer

    def get(self, request, order_id=None):
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()
            if customer:
                if not customer:
                    response = {
                        'status': 'FAIL',
                        'message': 'Unauthorized'
                    }
                    return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)
        else:
            response = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        try:
            order = Order.objects.get(number=order_id)
        except BaseException:
            response = {
                'status': 'FAIL',
                'message': 'Order doesn\'t exist'
            }
            return Response(status=status.HTTP_404_NOT_FOUND, data=response)

        context = {'request': request}
        query = self.get_queryset(order_id=order.id)
        serializer = OrderStatusHistorySerializer(
            query, context=context, many=True)
        response = {
            'status': 'PASS',
            'message': serializer.data
        }
        return Response(status=status.HTTP_200_OK, data=response)

    def get_queryset(self, order_id=None):
        queryset = Event.objects.filter(order_id=order_id).order_by('time')
        return queryset

    class Meta:
        lookup_field = 'order__number'
        lookup_url_kwarg = 'order__number'


class OrderStatusBulkHistory(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = OrderEventHistoryBulkSerializer

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()
            if customer:
                if not customer:
                    response = {
                        'status': 'FAIL',
                        'message': 'Unauthorized'
                    }
                    return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)
        else:
            response = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        try:
            order_ids = request.GET.dict().get('order_ids')
            order_ids = order_ids.split(',')

        except BaseException as be:
            logger.info('Error parsing order ids: %s - (Order bulk status history)' % be)
            response = {
                'status': 'FAIL',
                'message': 'Please pass the awb numbers as a comma separated string'
            }
            return Response(status=status.HTTP_404_NOT_FOUND, data=response)

        context = {'request': request}
        query = self.get_queryset(order_ids=order_ids)
        serializer = OrderEventHistoryBulkSerializer(
            query, context=context, many=True)

        response = {
            'status': 'PASS',
            'message': serializer.data
        }
        return Response(status=status.HTTP_200_OK, data=response)

    def get_queryset(self, order_ids=None):
        queryset = Order.objects.filter(number__in=order_ids)
        return queryset

    class Meta:
        lookup_field = 'order__number'
        lookup_url_kwarg = 'order__number'


class StoreFront(generics.ListCreateAPIView, CustomerMixin):
    def post(self, request):
        data = request.data
        item_details = data.pop('item_details')


class OrderShipment(generics.ListCreateAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)
    serializer_class = OrderShipmentSerializer
    queryset = serializer_class.queryset

    def get(self, request):
        """
        Gets all the NEW and PACKED shipment orders
        """

        customers_list = request.query_params.get('ids').split(',')

        orders = Order.objects.filter(
            customer__id__in=customers_list).exclude(
            status__in=StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES).values(
            'customer_id', 'reference_number', 'number')

        if not orders:
            response = 'No Orders Found'
            return Response(status=status.HTTP_404_NOT_FOUND, data=response)

        response = {
            'status': 'PASS',
            'order_list': orders,
            'message': 'Synchronisation done. Scan the orders'
        }
        return Response(status=status.HTTP_200_OK, data=response)

    def post(self, request):
        request.POST._mutable = True
        customer = None
        item_details = []
        kit_skus = []
        error_msg = None
        api_key = request.META.get('HTTP_API_KEY', None)
        customer_info = None
        if not api_key == settings.FLASH_SALE_API_KEY and 'customer_id' in request.data:
            _out = {
                'status': 'FAIL',
                'message': 'customer_id is a invalid parameter'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        if request.data.get('customer_id', None):
            customer_info = Customer.objects.get(pk=request.data.get('customer_id'))
            data = request.data
            payment_mode = data.get('payment_mode', None)
            if payment_mode and not payment_mode == PAYMENT_MODE_CASH:
                request.data.update({'status': StatusPipeline.ORDER_PENDING})
        if not customer:
            customer = get_customer_from_api_key(api_key)
        if not customer:
            customer = self.get_customer(request.user)
        if not customer:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        try:
            api_data = OrderApiData(request_body=request.data, customer=customer,
                                    reference_number=request.data.get('reference_number'))
            api_data.save()

        except Exception as e:
            api_data = None
            logger.info('Order API data capture failed - %s' % e.args[0])

        is_hyperlocal = request.data.get('is_hyperlocal', False)
        if isinstance(is_hyperlocal, str):
            is_hyperlocal = is_hyperlocal == 'true'

        _params = {
            'customer': customer,
            'contract_type': CONTRACT_TYPE_HYPERLOCAL if is_hyperlocal else CONTRACT_TYPE_SHIPMENT,
            'status': CONTRACT_STATUS_ACTIVE
        }

        postal_code = request.data.get('delivery_postal_code')
        pickup_postal_code = request.data.get('pickup_postal_code')
        if postal_code and not re.match('^\d+$', str(postal_code)):
            _out = {
                'status': 'FAIL',
                'message': 'Invalid Pincode {}- only numeric characters allowed in pincode'.format(postal_code)
            }
            if api_data:
                OrderApiData.objects.filter(id=api_data.id).update(
                        response_status='400', reason=_out['message'])

            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if pickup_postal_code and not re.match('^\d+$', str(pickup_postal_code)):
            _out = {
                'status': 'FAIL',
                'message': 'Invalid Pincode {}- only numeric characters allowed in pincode'.format(pickup_postal_code)
            }
            if api_data:
                OrderApiData.objects.filter(id=api_data.id).update(
                        response_status='400', reason=_out['message'])

            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        request.data.update({'shipment_info': json.dumps(request.data)})
        dropoff_hub_address = request.data.get('delivery_hub')
        pickup_hub_address = request.data.get('pickup_hub')
        delivery_hub_ = None
        pickup_hub_ = None
        if dropoff_hub_address:
            delivery_hub_ = Hub.objects.filter(name=dropoff_hub_address).first()
            if delivery_hub_:
                delivery_geopoint = delivery_hub_.address.geopoint.coords
                request.data.update({
                    'delivery_address': delivery_hub_.address.line1,
                    'delivery_postal_code': delivery_hub_.address.postcode,
                })
                if delivery_geopoint:
                    request.data.update({
                        'delivery_lat': delivery_geopoint[1],
                        'delivery_lon': delivery_geopoint[0]
                    })
        if pickup_hub_address:
            pickup_hub_ = Hub.objects.filter(name=pickup_hub_address).first()
            if not pickup_hub_:
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid pickup hub'
                }
                if api_data:
                    OrderApiData.objects.filter(id=api_data.id).update(
                            response_status='400', reason=_out['message'])

                return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)
            if pickup_hub_:
                pickup_hub_geopoint = pickup_hub_.address.geopoint.coords if pickup_hub_.address.geopoint else None
                request.data.update({
                    'pickup_address': pickup_hub_.address.line1,
                    'pickup_postal_code': pickup_hub_.address.postcode,
                })
                if pickup_hub_geopoint:
                    request.data.update({
                        'pickup_lat': pickup_hub_geopoint[1],
                        'pickup_lon': pickup_hub_geopoint[0]
                    })
        request.data.update({'pickup_hub_': pickup_hub_,
                             'delivery_hub_': delivery_hub_})

        city = get_city_from_pincode(postal_code)
        if not city:
            city = get_city_from_latlong(latitude=request.data.get('delivery_lat'),
                                         longitude=request.data.get('delivery_lon'), field='shipment_coverage')
        source_city = get_city_from_pincode(pickup_postal_code)
        if not source_city:
            source_city = get_city_from_latlong(latitude=request.data.get('pickup_lat'),
                                                longitude=request.data.get('pickup_lon'), field='shipment_coverage')

        if city:
            _params['city'] = city
            request.data.update({'city': city})

        if source_city and source_city != city:
            _params['source_city'] = source_city
            _params['is_inter_city_contract'] = True
            request.data.update({'source_city': source_city})

        contract_queryset = Contract.objects.filter(**_params)

        if contract_queryset.exists():
            contract = contract_queryset.first()
        else:
            _out = {
                'status': 'FAIL',
                'message': 'Contract not defined, please contact Blowhorn sales team'
            }
            if api_data:
                OrderApiData.objects.filter(id=api_data.id).update(
                        response_status='400', reason=_out['message'],
                        has_undefined_contract=True)

            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        otp = None
        pick_orders = False
        if contract.trip_workflow:
            if contract.trip_workflow.otp_required:
                otp = randint(1001, 9999)
            if contract.trip_workflow.pick_orders:
                pick_orders = True

        if request.data.get('pickup_address', None):
            request.data.update({
                'pickedup_address': request.data.get('pickup_address'),
                'has_pickup': pick_orders
            })

        if 'item_details' in request.data:
            item_details = request.data.pop('item_details')

        if 'kit_skus' in request.data:
            kit_skus = request.data.pop('kit_skus')

        items = request.data.get('items')
        if request.data.get('csv_upload'):
            request.data.update({'items': items})

        try:
            serializer_class = request.data.pop('serializer_class')
        except:
            serializer_class = self.serializer_class

        request.data.update({'customer': customer_info or customer})
        request.data.update({'customer_contract': contract.id})
        request.data.update({'otp': otp})
        request.data.update({'contract_type': contract.contract_type})

        if request.data.get('division', None):
            division = CustomerDivision.objects.filter(customer=customer,
                                                       name=request.data.get('division')).first()
            request.data.update({
                'division': division
            })

        request.data.update({'use_customer_hub': contract.use_customer_hub})

        items = request.data.get('items')
        if request.data.get('csv_upload'):
            request.data.update({'items': items})
        try:
            with transaction.atomic():

                serializer = serializer_class(
                    data=request.data,
                    context={'request': request}
                )

                if serializer.is_valid(raise_exception=True):
                    order = serializer.save()
                    message = {'awb_number': order.number}
                    if order.order_type == settings.SHIPMENT_ORDER_TYPE:
                        broadcast_order_details_to_dashboard.apply_async(
                            (order.id,), )

                    if item_details:
                        orderline = OrderlineMixin().create_orderlines(
                            item_details, order, customer, 'shipment-api')
                        message['item_details'] = orderline

                    if kit_skus:
                        kitskus = OrderlineMixin().create_kit_skus(kit_skus, order, customer, 'shipment-api')
                        message['kit_skus'] = kitskus
                    _out = {
                        'status': 'PASS',
                        'message': message
                    }
                    if api_data:
                        OrderApiData.objects.filter(id=api_data.id).update(
                            response_status='200', reason=_out['message'])

                    # basket_id = request.data.get('basket_id', None)
                    # if basket_id:
                    #     Line.objects.filter(basket_id=basket_id).delete()
                    #     Basket.objects.filter(id=basket_id).delete()
                    return Response(status=status.HTTP_200_OK, data=_out)


            error_response = {
                'status': 'FAIL',
                'message': 'Unhandled Exceptions'
            }
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            data=error_response)

        except Exception as e:
            error_msg = e.args[0]
            if api_data:
                update_parm = {
                    "response_status": '400', "reason": error_msg
                }
                error_message = error_msg.get('message', None) if isinstance(error_msg, dict) else error_msg
                update_parm['is_email_invalid'] = "Invalid customer email" in error_message
                update_parm['has_undefined_contract'] = "Contract not defined" in error_message
                update_parm['is_phone_invalid'] = "Invalid customer mobile number" in error_message
                update_parm['is_unservicebale_pincode'] = "Blowhorn doesn't provide service in this area" \
                                                                  in error_message
                OrderApiData.objects.filter(id=api_data.id).update(**update_parm)

            return Response(status=status.HTTP_400_BAD_REQUEST, data=error_msg)


class CustomerAuthTokenView(views.APIView, CustomerMixin):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        email = data.get('email', None)
        password = data.get('password', None)
        partner = get_partner_from_api_key(api_key)
        status_code = status.HTTP_401_UNAUTHORIZED
        response = {
            'status': 'FAIL',
            'message': 'Unauthorized'
        }

        if partner and email and password:
            user = authenticate(email=email, password=password)
            if user:
                customer = self.get_customer(user)
                if customer and customer.api_key:
                    status_code = status.HTTP_200_OK
                    response = {
                        'status': 'PASS',
                        'token': customer.api_key
                    }
            else:
                status_code = status.HTTP_400_BAD_REQUEST
                response = {
                    'status': 'FAIL',
                    'message': 'Invalid credentials passed'
                }

        return Response(status=status_code, data=response)


class CustomerAuthToken(views.APIView, CustomerMixin):
    permission_classes = (AllowAny,)

    def post(self, request):
        origin_ip = request.META.get('HTTP_X_REAL_IP', None)
        response = {
                "status" : "FAILED",
                "message" : "INVALID_CREDENTIALS"
            }

        status_code = status.HTTP_401_UNAUTHORIZED
        username = request.data.get('username')
        password = request.data.get('password')

        _ip2 = None
        forwarded_ip = request.META.get('HTTP_X_FORWARDED_FOR', None)
        if forwarded_ip:
            origin_ip_2 = forwarded_ip.split(',')[0]
            _ip2 = ip_address(origin_ip_2)

        if username and password and origin_ip:
            user = authenticate(username=username, password=password)

            if user:
                is_allowed = False
                customer = self.get_customer(user)
                _ip = ip_address(origin_ip)
                partnership = PartnerCustomer.objects.filter(customer=customer).first()

                if partnership:
                    allowed_list = partnership.partner.ip_address.split(',')
                    for ip in  allowed_list:
                        net = IPv4Network(ip)

                        if _ip2:
                            is_allowed = (_ip in net) or (_ip2 in net)
                        else:
                            is_allowed = _ip in net

                        if is_allowed:
                            break

                if is_allowed and customer and customer.api_key:
                    status_code =  status.HTTP_200_OK
                    response = {
                        "status" : "SUCCESS",
                        "token" : customer.api_key
                    }
                else:
                    msg = 'Unicommerce unknown/new ip incoming:' + str(origin_ip) + ': ' + str(request.META)
                    logger.error(msg);

        return Response(status=status_code, data=response)


class UnicomOrderShipment(generics.ListCreateAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request):
        """
        Get the status of orders
        """
        api_key = request.META.get('HTTP_AUTHORIZATION', None)
        customer = None
        if api_key:
            customer = self.get_customer_with_api_key(api_key)

        if not customer:
            response = {'Status': 'FAILED', 'message': 'INVALID_CREDENTIALS'}
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        response = {'Status': 'FAILED', 'message': 'No orders found'}

        waybills = request.query_params.get('waybills')
        if not waybills:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        waybills_list = waybills.split(',')
        order_qs = Order.objects.filter(number__in=waybills_list, customer=customer)

        if not order_qs:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        orders = order_qs.annotate(
                    statusDate = Func(F('updated_time'),Value("dd-MON-yyyy HH:mm:ss"),
                    function='to_char',output_field=CharField()
                    ),).extra(
                    select={'currentStatus' : 'status' , 'waybill' : 'number'}).values(
                    'currentStatus', 'statusDate', 'waybill')

        for order in orders:
            order['currentStatus'] = StatusPipeline.ORDER_STATUSES.get(order['currentStatus'])

        response = {'Status': 'SUCCESS', 'waybillDetails': orders}
        return Response(status=status.HTTP_200_OK, data=response)

    def post(self, request):
        """
            Create order for unicommerce
        """
        api_key = request.META.get('HTTP_AUTHORIZATION', None)
        customer = None
        if api_key:
            customer = self.get_customer_with_api_key(api_key)

        if not customer:
            response = {'status': 'FAILED', 'message': 'INVALID_CREDENTIALS'}
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        # create log
        ShopifyLog.objects.create(
            reference_number='Unicommerce',
            request_body=json.dumps(request.data),
            customer=customer,
            category='Api',
            source = 'Order'
        )

        # format the data for our shipment api
        order_data = UnicomShipmentOrder().process_order_data(request.data)
        order_failed = order_data.get('status', None)
        if order_failed:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=order_data)

        # create order if all validations passed
        result = UnicomShipmentOrder().create_order(order_data, api_key)
        return Response(**result)

class OrderShipmentDetails(views.APIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def get_order_detail(self, request, ref_id=None):
        """
        Get the order's instance if exist for the particular id
        """

        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()
        else:
            customer = self.get_customer(request.user)

        if not customer:
            response = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return {'ret_status': status.HTTP_401_UNAUTHORIZED, 'response': response}
        self.customer = customer

        if ref_id[:3] == 'SH-':
            # spar sends reference id other customer sends order number
            query = Q(number=ref_id, customer=customer)
        else:
            query = Q(reference_number=ref_id, customer=customer)

        order = Order.objects.filter(query).select_related(
            'shipping_address', 'pickup_address', 'hub', 'customer', 'city')
        order = order.prefetch_related('order_lines')

        if not order:
            response = {
                'status': 'FAIL',
                'message': 'Order doesn\'t exist'
            }
            return {'ret_status': status.HTTP_404_NOT_FOUND, 'response': response}

        response = {
            'status': 'PASS',
            'message': {},
            'order': order
        }
        logging.info(response)
        return {'ret_status': status.HTTP_200_OK, 'response': response}

    def get(self, request, order_id=None):
        """
        Get the shipment order detail of the corresponding id
        """

        order_detail = self.get_order_detail(request, order_id)

        if not order_detail.get('ret_status') == status.HTTP_200_OK:
            return Response(status=order_detail['ret_status'], data=order_detail['response'])

        order = order_detail['response'].pop('order')
        order = order.first()

        alternate_customer_phone = order.shipping_address.alternate_phone_number
        if alternate_customer_phone:
            alternate_customer_phone = order.shipping_address.alternate_phone_number.national_number
        else:
            alternate_customer_phone = None

        if order.hub.delivery_hub_sn:
            hub_location = order.hub.delivery_hub_sn
        else:
            hub_location = order.hub.name

        pickup_customer_name = order.pickup_address.first_name if order.pickup_address else None
        pickup_customer_mobile = order.pickup_address.phone_number.national_number \
            if order.pickup_address and order.pickup_address.phone_number else None

        order_detail['response']['message'] = {
            'order_details': {
                'order_id': order.id,
                'reference_number': order.reference_number,
                'awb_number': order.number,
                'status': StatusPipeline.ORDER_STATUSES.get(order.status),
                'delivery_hub_sn': hub_location,
                'delivery_address': order.shipping_address.line1,
                'delivery_postal_code': order.shipping_address.postcode,
                'pickup_address': order.pickup_address.line1,
                'pickup_postal_code': order.pickup_address.postcode,
                'customer_name': order.shipping_address.first_name,
                'customer_mobile': order.shipping_address.phone_number.national_number if order.shipping_address.phone_number else "",
                'alternate_customer_mobile': alternate_customer_phone,
                'pickup_customer_name': pickup_customer_name,
                'pickup_customer_mobile': pickup_customer_mobile,
                'item_details': {
                    obj.id: {
                        'item_name': obj.sku.name if obj.sku else obj.wh_sku.name,
                        'item_quantity': obj.quantity,
                        'item_qty_picked': obj.quantity - obj.qty_removed if order.status not in [
                            StatusPipeline.ORDER_NEW,
                            StatusPipeline.ORDER_OUT_FOR_PICKUP] else 0,
                        'item_qty_delivered': obj.qty_delivered
                    }
                    for obj in order.order_lines.all()
                }
            }
        }

        if order.cash_on_delivery:
            order_detail['response']['message'][
                'order_details']['cash_on_delivery'] = str(order.cash_on_delivery)

        return Response(status=order_detail['ret_status'], data=order_detail['response'])

    def put(self, request, order_id=None):
        """
        Update the existing order
        """

        order_detail = self.get_order_detail(request, order_id)

        if not order_detail.get('ret_status') == status.HTTP_200_OK:
            return Response(status=order_detail['ret_status'], data=order_detail['response'])

        order = order_detail['response'].get('order')
        order = order.first()

        if not order.status == StatusPipeline.ORDER_NEW:
            response = {
                'status': 'FAIL',
                'message': 'The order can\'t be updated now'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        item_details = []
        if 'item_details' in request.data:
            item_details = request.data.pop('item_details')

        if request.data.get('pickup_address', None):
            request.data.update({'pickedup_address': request.data.pop('pickup_address')})

        validated_data = ShipmentOrderValidation().validate(request, is_update=True, order=order)

        try:
            with transaction.atomic():
                if validated_data.get('shipping_address'):
                    shipping_address_data = validated_data.pop('shipping_address')
                    shipping_serializer = ShippingAddressSerializer(instance=order.shipping_address,
                                                                    data=shipping_address_data, partial=True)
                    if shipping_serializer.is_valid():
                        sh = shipping_serializer.save()
                        validated_data['shipping_address'] = sh.id
                    else:
                        logging.info("Shipping address update raised error : %s", shipping_serializer.errors)
                        raise Exception("Something went wrong while updating order")

                if validated_data.get('pickup_address'):
                    pickup_address_data = validated_data.pop('pickup_address')
                    pickup_serializer = ShippingAddressSerializer(instance=order.pickup_address,
                                                                  data=pickup_address_data, partial=True)
                    if pickup_serializer.is_valid():
                        pka = pickup_serializer.save()
                        validated_data['pickup_address'] = pka.id
                    else:
                        logging.info("Pickup address update raised error : %s", pickup_serializer.errors)
                        raise Exception("Something went wrong while updating order")

                if item_details:
                    orderline = OrderlineMixin().update_orderlines(item_details, order, 'shipment-api')

                order_serializer = OrderDetailsSerializer(instance=order, data=validated_data,
                                                          partial=True)
                if order_serializer.is_valid():
                    updated_order = order_serializer.save()
                    _out = {
                        'status': 'PASS',
                        'message': {
                            'awb_number': updated_order.number,
                            "item_details": orderline
                        }
                    }
                    return Response(status=status.HTTP_200_OK, data=_out)
                else:
                    logging.info("Order update raised error : %s", order_serializer.errors)
                    raise Exception("Something went wrong while updating order")

        except Exception as e:
            logging.info("This is the error while updating : %s", e.args[0])

            error_response = {
                'status': 'FAIL',
                'message': e.args[0]
            }
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=error_response)

class AddOrderlines(views.APIView):
    permission_classes = (AllowAny,)

    def put(self, request, order_id=None):
        """
        Adds new orderlines to the existing order without deleting existing
        """

        order_detail = get_order_authentication_details(request, order_id)

        if not order_detail.get('ret_status') == status.HTTP_200_OK:
            return Response(status=order_detail['ret_status'], data=order_detail['response'])

        order_data = order_detail['response'].pop('order')
        customer = order_detail['response'].pop('customer')
        order = order_data.first()
        valid_status_list = [StatusPipeline.ORDER_NEW, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]

        if order.status not in valid_status_list:
            response = {
                'status': 'FAIL',
                'message': 'The order can\'t be updated now'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        if not request.data.get('item_details'):
            _out = {
                'status': 'FAIL',
                'message': 'Please provide the list of new items to be added'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            item_details = request.data.pop('item_details')
            orderline = OrderlineMixin().create_orderlines(item_details, order, customer, 'api')
            _out = {
                'status': 'PASS',
                'message': {
                    'awb_number': order.number,
                    'item_details': orderline
                }
            }
            return Response(status=status.HTTP_200_OK, data=_out)

        except Exception as e:
            logging.info("This is the error while adding new inline : ", e.args[0])
            arg = e.args[0]
            if isinstance(arg, dict):
                arg = e.args[0]['message']
            error_response = {
                'status': 'FAIL',
                'message': arg
            }
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=error_response)

class UpdateOrderlines(views.APIView):
    permission_classes = (AllowAny,)

    def put(self, request, order_id=None):
        """
        Deletes existing orderlines and adds new orderlines to the existing order
        """

        order_detail = get_order_authentication_details(request, order_id)

        if not order_detail.get('ret_status') == status.HTTP_200_OK:
            return Response(status=order_detail['ret_status'], data=order_detail['response'])

        order_data = order_detail['response'].pop('order')
        customer = order_detail['response'].pop('customer')
        order = order_data.first()
        valid_status_list = [StatusPipeline.ORDER_NEW, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]

        if order.status not in valid_status_list:
            response = {
                'status': 'FAIL',
                'message': 'The order can\'t be updated now'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        if not request.data.get('item_details'):
            _out = {
                'status': 'FAIL',
                'message': 'Please provide the list of new items to be added'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            item_details = request.data.pop('item_details')
            orderline = OrderlineMixin().update_orderlines(item_details, order, 'api')
            _out = {
                'status': 'PASS',
                'message': {
                    'awb_number': order.number,
                    'item_details': orderline
                }
            }
            return Response(status=status.HTTP_200_OK, data=_out)

        except Exception as e:
            logging.info("This is the error while adding new inline : ", e.args[0])
            arg = e.args[0]
            if isinstance(arg, dict):
                arg = e.args[0]['message']
            error_response = {
                'status': 'FAIL',
                'message': arg
            }
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=error_response)


class DeleteOrderlines(views.APIView):
    permission_classes = (AllowAny,)

    def put(self, request, order_id=None):
        """
        Delete the existing orderline for the given order
        """

        order_detail = get_order_authentication_details(request, order_id)

        if not order_detail.get('ret_status') == status.HTTP_200_OK:
            return Response(status=order_detail['ret_status'], data=order_detail['response'])

        order_data = order_detail['response'].pop('order')
        order = order_data.first()
        valid_status_list = [StatusPipeline.ORDER_NEW, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]

        if order.status not in valid_status_list:
            response = {
                'status': 'FAIL',
                'message': 'The order can\'t be updated now'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=response)

        if not request.data.get('item_list'):
            _out = {
                'status': 'FAIL',
                'message': 'Please provide the list of items to be deleted'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            items_to_be_deleted = request.data.pop('item_list')
            orderline = OrderlineMixin().delete_orderlines(items_to_be_deleted, order, 'api')
            _out = {
                'status': 'PASS',
                'message': 'Items deleted for AWB Number : ' + str(order.number)
            }
            return Response(status=status.HTTP_200_OK, data=_out)

        except Exception as e:
            logging.info("This is the error while deleting inline : ", e.args[0])
            error_response = {
                'status': 'FAIL',
                'message': e.args[0]
            }
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=error_response)


class OrderShipmentCancel(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = OrderSerializer
    queryset = serializer_class.queryset

    def post(self, request):

        data = request.data

        request.POST._mutable = True
        customer = None
        api_key = request.META.get('HTTP_API_KEY', None)
        if not api_key and data.get('customer_id'):
            customer = Customer.objects.get(pk=data.get('customer_id'))
        if not customer:
            customer = get_customer_from_api_key(api_key)
        if not customer:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        number = data.get('awb_number')
        order = Order.objects.filter(number=number).first()

        if order.driver:
            _out = {
                'status': 'FAIL',
                'message': 'Product is out for delivery'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not order:
            _out = {
                'status': 'FAIL',
                'message': 'Invalid Awb Number'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        # TODO need to revisit again Add Picked up or not condition
        Order.objects.filter(number=number).update(
            customer_reference_number=F('reference_number'),
            reference_number=None,
            status='Cancelled'
        )

        _out = {
            'status': 'PASS',
            'message': {
                'awb_number': order.number
            }
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class ShipmentTracking(generics.RetrieveAPIView):
    serializer = DriverActivitySerializer

    def get(self, request, order_id, driver_id):
        driver = self.serializer.queryset.filter(
            driver_id=driver_id, order_id=order_id).first()
        result = self.serializer(driver)

        return Response(status=status.HTTP_200_OK, data=result.data)


class OrderPackReturn(generics.CreateAPIView):
    serializer_class = OrderSerializer
    queryset = serializer_class.queryset

    def post(self, request):

        context = {'request': request}
        data = request.POST.dict()
        hub_associate = request.user
        order_status = None
        # get the current geopoint and add in Order Events and Trip_appEvents
        location = data.get('location_details', None)
        action = data.get('action')
        if action == 'pack':
            order_status = StatusPipeline.ORDER_PACKED
        elif action == 'return':
            order_status = StatusPipeline.ORDER_RETURNED_TO_HUB

        if not order_status:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Send correct action from the app')
        geopoint = None

        if location:
            try:
                location = json.loads(location)
            except BaseException:

                # logger.error('Invalid Location json sent')
                return Response(status=status.HTTP_200_OK,
                                data="Invalid Location json sent")

            latitude = location.get('mLatitude')
            longitude = location.get('mLongitude')

            geopoint = CommonHelper.get_geopoint_from_latlong(
                latitude, longitude)

        item_ids = data.get('item_ids') or None

        try:
            items = json.loads(item_ids)
        except ValueError:

            logger.error('Invalid format to send the Items.')
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="Invalid format to send the Items.")

        for item in items:

            order_qs = Order.objects.filter(
                customer_id=item['customer_id'],
                reference_number=str(item['reference_number'])
            )

            if order_qs.exists():
                order = order_qs.first()
                order_serializer = OrderSerializer(
                    order, context=context)

                order_serializer.update_status(
                    order_status, geopoint, order, hub_associate)

                app_event_data = {}
                app_event_data['order'] = order
                app_event_data['geopoint'] = geopoint
                app_event_data['hub_associate'] = hub_associate
                app_event_data['event'] = order_status

                app_event_serializer = AppEventsSerializer(
                    data=app_event_data, context=context)
                app_event_serializer._create_event()

                logger.info(
                    'App Event "%s" created for order:- "%s" by hub_associate:- "%s"',
                    order_status, order, request.user)
            else:
                logger.error(
                    'Shipments does not belong to the current customer')

                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data="Some of the shipments does not belong"
                                     " to the current customer."
                                     " Please check the Shipment Alerts on panel")

        return Response(status=status.HTTP_200_OK, data={'status': True})


class OrderBreakdown(generics.CreateAPIView):
    serializer_class = OrderSerializer
    queryset = serializer_class.queryset

    def post(self, request):

        context = {'request': request}

        data = request.POST.dict()

        # get the current geopoint and add in Order Events and Trip_appEvents
        location = data.get('location_details', None)
        geopoint = None

        if location:
            try:
                location = json.loads(location)
            except BaseException:
                # logger.error('Invalid Location json sent')
                return Response(
                    status=status.HTTP_200_OK,
                    data="Invalid Location json sent"
                )

            latitude = location.get('mLatitude')
            longitude = location.get('mLongitude')

            geopoint = CommonHelper.get_geopoint_from_latlong(
                latitude, longitude)

        driver = get_object_or_404(Driver, user=request.user)

        trip_queryset = Trip.objects.filter(
            driver=driver,
            status__in=[
                StatusPipeline.TRIP_NEW,
                StatusPipeline.TRIP_IN_PROGRESS,
                StatusPipeline.DRIVER_ACCEPTED,
                StatusPipeline.TRIP_ALL_STOPS_DONE
            ],
            order=None
        )

        if trip_queryset.count() > 1:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_("Driver is fulfilling two trips at a time.")
            )

        if trip_queryset.exists():
            trip = trip_queryset.first()

            stop_queryset = Stop.objects.filter(trip=trip).exclude(
                status=StatusPipeline.TRIP_COMPLETED)

            for stop in stop_queryset:
                TripMixin().update_stop_status(
                    stop,
                    StatusPipeline.TRIP_CANCELLED
                )

            TripMixin().update_trip_status(trip, StatusPipeline.TRIP_CANCELLED)

            app_event_data = {}
            app_event_data['driver'] = driver
            app_event_data['trip'] = trip
            app_event_data['geopoint'] = geopoint
            app_event_data['event'] = StatusPipeline.TRIP_CANCELLED

            app_event_serializer = AppEventsSerializer(
                data=app_event_data,
                context=context
            )
            app_event_serializer._create_event()

            logger.info(
                'App Event "%s" created for trip:- "%s" from '
                'driver:- "%s-%s"',
                StatusPipeline.TRIP_CANCELLED,
                trip,
                driver.name,
                driver.user.national_number
            )
        return Response(status=status.HTTP_200_OK, data={'status': True})


class OrderSummary(generics.RetrieveAPIView, OrderPlacementMixin, CustomerMixin):
    """ API to get summary of all orders of a particular customer
    """
    serializer_class = OrderSerializer

    def get(self, request):
        results = self.get_queryset()
        summary = self.get_summary(results)
        headers = settings.HEADER
        return Response(status=status.HTTP_200_OK, data=summary, headers=headers)

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        if not customer:
            return []

        order_qs = Order._default_manager.filter(
            customer=customer, status=StatusPipeline.ORDER_DELIVERED)
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                )
            )
        )
        order_qs = order_qs.order_by('-pickup_datetime')
        return list(order_qs)


class OrderInvoice(generics.CreateAPIView, OrderPlacementMixin):
    serializer_class = OrderSerializer
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request, order_id):
        email_recipients = []
        try:
            email_recipients += json.loads(request.body)
        except:
            logger.info('OrderInvoice: Invalid content in body')

        order = Order.objects.get(id=order_id)
        trip = TripTrackingView.as_view()(
            request._request, number=order.number).data

        request._request.META['return_json'] = True
        pdf_data = OrderInvoiceAPIView.as_view()(
            request._request, order.number).data
        try:
            Email().send_receipt('Cash', order, trip, email_recipients, pdf_data)
            return Response(status=status.HTTP_200_OK,
                            data=_('Invoice sent successfully'))
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Unable to send invoice'))


class QuickBook(generics.CreateAPIView):
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        request_vars = request.data
        from_location = request_vars.get('from')
        to_location = request_vars.get('to')
        truck_type = request_vars.get('truck_type')
        coordinates = from_location.get('_geopoint')
        device_type = 'Website'

        if not from_location or \
            not to_location or not truck_type:
            response = {
                'status': 'FAIL',
                'message': 'Unable to place order'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        geopoint = CommonHelper.get_geopoint_from_latlong(
            coordinates.get('_lat'), coordinates.get('_lng'))

        city = get_city_from_geopoint(geopoint, field='coverage_pickup')
        now = utc_to_ist(timezone.now())
        # Check if outside of working hours.
        if not within_city_working_hours(city, now.time()):
            response = {
                'status': 'FAIL',
                'message': 'No Vehicles available now'
            }
            return Response(status=status.HTTP_200_OK, data=response)

        formatted_data = OrderUtil().format_quickbook_data(
            from_location, to_location, truck_type, device_type=device_type)

        context = {'request': request}
        return OrderList().create_c2c_order(request, formatted_data, context)


class OrderFlyIntegration(views.APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        context = {'request': request}

        timeframe = data.get('timeframe')
        pickup_address = data.get('pickup')
        shipping_address = data.get('dropoff')
        items_pruned = [i for i in data.get('items') if i.strip()]

        now_or_later = timeframe.get('now_or_later')
        if now_or_later == "now":
            pickup_time = timezone.now()
        else:
            pickup_time = timeframe.get('later_value')

        order_data = {
            'order_type': 'dummy',
            'total': 0,
            'items': items_pruned,
            'pickup_datetime': pickup_time,
            'pickup_address': OrderUtil().get_formatted_address(pickup_address),
            'shipping_address': OrderUtil().get_formatted_address(shipping_address),
            'timeframe': now_or_later,
            'customer': Customer.objects.filter(name='Pratik_test').first()
        }

        order_serializer = OrderSerializer(context=context, data=order_data)
        order = order_serializer.save()
        if order:
            order_number = order.number
            response = {
                'status': 'SUCCESS',
                'message': {
                    'booking_type': 'advanced',
                    'friendly_id': order_number,
                }
            }

            return Response(status=status.HTTP_200_OK, data=response)
        response = {
            'status': 'FAIL',
            'message': ''
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response)


class OrderInvoiceAPIView(views.APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, number):
        return self.get_result(request, number)

    def post(self, request, number):
        return self.get_result(request, number)

    def __get_office_contact_number(self, order):
        if order.city:
            return ContactNumberSerializer().to_representation(
                order.city.contact)
        return ''

    def get_result(self, request, number):
        order = self.get_queryset(number)
        customer, tax_info, legal_name, name, phone_number, is_business_user = \
            order.customer, None, None, '', '', False
        contract = order.customer_contract
        if customer:
            tax_info, legal_name = customer.gst_info, customer.legalname
            user = customer.user
            name = user.name
            if user.phone_number:
                phone_number = user.national_number
            if customer.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL:
                is_business_user = True

        trip_data = TripTrackingView.as_view()(
            request._request, number=number).data
        static_base = settings.ROOT_DIR.path('website/static')
        data = {
            'trip': trip_data,
            'shipments': [i.category for i in order.item_category.all()],
            'customer': {
                'gstin': getattr(tax_info, "gstin", None),
                'is_business_user': is_business_user,
                'legal_name': legal_name,
                'name': name,
                'mobile': phone_number,
            },
            'currency': {
                'url': '%s/website/images/%s/currency/currency128x128.png' %
                       (static_base, settings.WEBSITE_BUILD),
                'symbol': contract.currency_symbol
            }
        }

        consignor_serializer = ShippingAddressSerializer(
            order.pickup_address,
            remove_fields=["id", "geopoint", "notes", "search_text", "country"])
        consignee_serializer = ShippingAddressSerializer(
            order.shipping_address,
            remove_fields=["id", "geopoint", "notes", "search_text", "country"])

        gst_state = order.get_invoice_state()
        office = Office.objects.select_related('company')
        if gst_state:
            try:
                office = office.get(state=gst_state)
            except:
                # Consider Head office address if branch office address
                # is not defined
                office = office.get(is_head=True)
        else:
            office = office.get(is_head=True)

        office_serializer = OfficeSerializer(
            office, fields=['company', 'address',
                            'gst_identification_no', 'cin', 'pan']
        )

        extra_serialized_data = BaseSerializer.combine(
            False,
            [consignee_serializer, 'consignee'],
            [consignor_serializer, 'consignor'],
            [office_serializer, 'office'],
        )
        data.update(extra_serialized_data)
        data['office']['phone_number'] = self.__get_office_contact_number(order)

        output = request.GET.get('output', None)
        download = request.GET.get('download', None)
        return_json = request.GET.get('return_json', False)
        if output == 'pdf' or output == 'email':
            logo_url = '%s/website/images/%s/logo/BlackLogo.png' % (
                static_base, settings.WEBSITE_BUILD
            )
            content = {
                'payable_label': 'Payable To %s' % settings.BRAND_NAME
            }
            website_details = {
                'url': get_base_url(request),
                'support_email': settings.COMPANY_SUPPORT_EMAIL,
                'terms_and_condition_url': settings.TERMS_OF_SERVICE_URL
            }

            data.update({
                'path': static_base,
                'logo_url': logo_url,
                'booking_datetime': parse(data['trip']['booking_datetime']),
                'trip_date': parse(data['trip']['date']),
                'pickup_date': parse(data['trip']['pickup']['date']),
                'dropoff_date': parse(data['trip']['dropoff']['date']),
                'content': content,
                'website_details': website_details,
                'show_seal': settings.WEBSITE_BUILD == 'blowhorn',
                'markers': {
                    'pickup': '%s/website/images/%s/markers/Blue-Marker@2x.png' % (static_base, settings.WEBSITE_BUILD),
                    'dropoff': '%s/website/images/%s/markers/Purple-Marker@2x.png' % (static_base, settings.WEBSITE_BUILD),
                }
            })
            if return_json:
                return data

            pdf = render_to_pdf('website/invoice_c2c/invoice.html',
                                {'data': data})
            if pdf and output == 'pdf':
                response = HttpResponse(pdf, content_type='application/pdf')
                filename = "invoice_%s.pdf" % number
                if download == 'true':
                    content = "attachment; filename=%s" % filename
                else:
                    content = "inline; filename=%s" % filename
                response['Content-Disposition'] = content
                return response
            elif pdf and output == 'email':
                return Response(status=status.HTTP_200_OK,
                                data=render_to_pdfstream(
                                    'website/invoice_c2c/invoice.html',
                                    {'data': data}))

            return HttpResponse("Not Found")

        return Response(data, status=status.HTTP_200_OK)

    def get_queryset(self, order_number):
        return Order.objects.select_related(
            'customer', 'customer__user',
            'city', 'pickup_address', 'shipping_address'
        ).prefetch_related(
            'item_category'
        ).get(number=order_number)


class OrderDocumentUpload(views.APIView):
    permission_classes = (IsAuthenticated,)

    def _create_pod_pdf(self, order, document, pdf_stream=False):
        order_event = order.event_set.filter(status=StatusPipeline.ORDER_DELIVERED).last()
        static_base = settings.ROOT_DIR.path('website/static')
        logo_url = '%s/website/images/%s/logo/BlackLogo.png' % (
            static_base, settings.WEBSITE_BUILD
        )
        pdf_data = {
            'logo_url': logo_url,
            'awb_number': order.number,
            'reference_number': order.reference_number,
            'shipping_address': order.shipping_address.line1,
            'status': order.status if not order.otp else 'Delivered via OTP',
            'delivered_time': order_event.time.strftime('%d %B, %Y'),
            'remarks': order_event.remarks or 'NA',
            'coordinates': {
                'lat': order_event.current_location.y if order_event.current_location else '',
                'lng': order_event.current_location.x if order_event.current_location else ''
            },
            'consignee_name': order.shipping_address.first_name,
            'signature_file': (document and document.file and document.file.url) or '',
            'time': timezone.now().strftime('%d %B, %Y'),
            'brand_name': settings.BRAND_NAME,
            'height': '32px' if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD else '60px',
            'width': '130px' if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD else '60px',
            'sign_height': '120px',
            'sign_width': '90px'
        }
        response = render_to_pdf('website/proofofdelivery/pod.html',
                                 {'data': pdf_data})
        filename = 'pod_%s.pdf' % order.number
        content = "attachment; filename=%s" % filename
        response['Content-Disposition'] = content
        if pdf_stream:
            return response
        return File(BytesIO(response.content), filename)

    def get(self, request, order_number):
        # NOTE: Used for testing PDF generation
        try:
            order = Order.objects.get(number=order_number)
        except BaseException:
            return HttpResponse('Order not found')

        return self._create_pod_pdf(order, None, pdf_stream=request.GET.get('pdf_stream', False))

    def post(self, request, order_number):
        data = request.data
        orders_list = json.loads(data.get('order_list', None)) if data.get(
            'order_list') else None

        try:
            order = Order.objects.get(number=order_number)
        except BaseException:
            logger.error(traceback.format_exc())
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid Order Number')

        stop_id = data.get('stop_id', None)
        action = data.get('action', None)
        sign_file = data.get('sign_image', None)
        sign_otp_file = data.get('sign_otp', None)
        print('action - ', action)
        stop = None

        has_sign_otp_file = False
        has_sign_file = False
        has_pod_files = False
        if stop_id:
            stop = Stop.objects.filter(pk=stop_id).first()

        if sign_otp_file:
            if OrderDocument.objects.filter(
                    order=order, document_type=OTP_SIGN).exists():
                return Response(status=status.HTTP_200_OK, data={"order_number": order_number})

            has_sign_otp_file = True
            document = OrderDocument.objects.create(
                order=order,
                document_type=OTP_SIGN,
                file=sign_otp_file,
                stop=stop,
                is_document_uploaded=False
            )

        if sign_file:
            if OrderDocument.objects.filter(
                    order=order, document_type=SIGNATURE).exists():
                return Response(status=status.HTTP_200_OK, data={"order_number": order_number})

            has_sign_file = True
            document = OrderDocument.objects.create(
                order=order,
                document_type=SIGNATURE,
                file=sign_file,
                stop=stop,
                is_document_uploaded=False
            )
            if order.status == StatusPipeline.ORDER_DELIVERED and order.order_type == settings.SHIPMENT_ORDER_TYPE:
                pdf = self._create_pod_pdf(order, document)
                pdf_doc = OrderDocument.objects.create(
                    order=order,
                    document_type=POD_PDF,
                    file=pdf,
                    stop=stop,
                    is_document_uploaded=False
                )

        pod_count = int(data.get('pod_count', 0))
        event = Event.objects.filter(order_id=order.id, status=action).last()

        document_type = PROOF_OF_DELIVERY
        if action == StatusPipeline.ORDER_DELIVERED:
            document_type = PROOF_OF_DELIVERY
        elif action == StatusPipeline.UNABLE_TO_DELIVER:
            document_type = PROOF_OF_NON_DELIVERY
        elif action == StatusPipeline.ORDER_RETURNED:
            document_type = PROOF_OF_REJECT
        elif action == StatusPipeline.ORDER_PICKED:
            document_type = PROOF_OF_PICKUP
        elif action == StatusPipeline.ORDER_RETURNED_RTO:
            document_type = PROOF_OF_RTO

        if pod_count > 0:
            has_pod_files = True
            for i in range(pod_count):
                file = data.get('pod_image%s' % i, None)
                if file:
                    document = OrderDocument.objects.create(
                        order=order,
                        document_type=document_type,
                        file=file,
                        stop=stop,
                        event=event,
                        is_document_uploaded=False
                    )

        if orders_list:
            orders = Order.objects.filter(
                Q(number__in=orders_list) | Q(reference_number__in=orders_list)).exclude(
                number=order_number)
            sign_otp_file = OrderDocument.objects.filter(order=order,
                                                         document_type=OTP_SIGN).first() \
                if has_sign_otp_file else None
            sign_file = OrderDocument.objects.filter(order=order,
                                                     document_type=SIGNATURE).first() \
                if has_sign_file else None
            pod_files = OrderDocument.objects.filter(order=order,
                                                     document_type=document_type) \
                if has_pod_files else None

            for order in orders.iterator():
                if sign_otp_file:
                    if OrderDocument.objects.filter(
                            order=order, document_type=OTP_SIGN).exists():
                        return Response(status=status.HTTP_200_OK,
                                        data={"order_number": order_number})

                    document = OrderDocument.objects.create(
                        order=order,
                        document_type=OTP_SIGN,
                        file=sign_otp_file.file,
                        stop=stop,
                        is_document_uploaded=False
                    )

                if sign_file:
                    if OrderDocument.objects.filter(
                            order=order, document_type=SIGNATURE).exists():
                        return Response(status=status.HTTP_200_OK,
                                        data={"order_number": order_number})

                    document = OrderDocument.objects.create(
                        order=order,
                        document_type=SIGNATURE,
                        file=sign_file.file,
                        stop=stop,
                        is_document_uploaded=False
                    )
                    if order.status == StatusPipeline.ORDER_DELIVERED and\
                            order.order_type == settings.SHIPMENT_ORDER_TYPE:
                        pdf = self._create_pod_pdf(order, document)
                        pdf_doc = OrderDocument.objects.create(
                            order=order,
                            document_type=POD_PDF,
                            file=pdf,
                            stop=stop,
                            is_document_uploaded=False
                        )

                document_type = PROOF_OF_DELIVERY
                if action == StatusPipeline.ORDER_DELIVERED:
                    document_type = PROOF_OF_DELIVERY
                elif action == StatusPipeline.UNABLE_TO_DELIVER:
                    document_type = PROOF_OF_NON_DELIVERY
                elif action == StatusPipeline.ORDER_RETURNED:
                    document_type = PROOF_OF_REJECT
                elif action == StatusPipeline.ORDER_PICKED:
                    document_type = PROOF_OF_PICKUP
                elif action == StatusPipeline.ORDER_RETURNED_RTO:
                    document_type = PROOF_OF_RTO

                event = Event.objects.filter(order_id=order.id, status=action).last()
                if pod_files.exists():
                    for i in pod_files.iterator():
                        document = OrderDocument.objects.create(
                            order=order,
                            document_type=document_type,
                            file=i.file,
                            stop=stop,
                            event=event,
                            is_document_uploaded=False
                        )

        return Response(status=status.HTTP_200_OK, data={"order_number": order_number})


class OrderHeatmapAPIView(generics.ListAPIView):
    """
        API for getting the order heat map.
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        return Order.objects.filter(query)

    def get_query(self, params):
        query = [Q(pickup_address__geopoint__isnull=False),
                 Q(order_type='booking')]
        order_status = params.get('status', None)
        city_id = params.get('city_id', None)
        hub_id = params.get('hub_id', None)
        vehicle_class_id = params.get('vehicle_class_id', None)
        customer_category = params.get('customer_category', None)
        start_date = params.get('start_date', None)
        end_date = params.get('end_date', None)

        if order_status:
            query.append(Q(status=order_status))
        if city_id:
            query.append(Q(city__id=city_id))
        if hub_id:
            query.append(Q(hub__id=hub_id))
        if vehicle_class_id:
            query.append(Q(vehicle_class_preference__id=vehicle_class_id))
        if customer_category:
            query.append(Q(customer__customer_category=customer_category))
        if start_date and end_date:
            query.append(Q(date_placed__date__range=(start_date, end_date)))

        return reduce(operator.and_, query) if query else None

    def get(self, request):
        order_qs = self.get_queryset()
        orders = order_qs.annotate(
            pickup_lat=ExpressionWrapper(
                Func('pickup_address__geopoint', function='st_y'),
                output_field=FloatField()),
            pickup_lng=ExpressionWrapper(
                Func('pickup_address__geopoint', function='st_x'),
                output_field=FloatField()),
            dropoff_lat=ExpressionWrapper(
                Func('shipping_address__geopoint', function='st_y'),
                output_field=FloatField()),
            dropoff_lng=ExpressionWrapper(
                Func('shipping_address__geopoint', function='st_x'),
                output_field=FloatField())
        ).values('pickup_lat', 'pickup_lng', 'dropoff_lat', 'dropoff_lng',
                 'status', 'reason_for_cancellation')
        return Response(data=list(orders), status=status.HTTP_200_OK)


class OrderHeatmapFilterAPIView(views.APIView, CustomerMixin):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,  Or(IsBlowhornStaff, IsNonIndividualCustomer),)

    """
    Returns list of all search choices.
    """

    def get(self, request):
        user = request.user
        customer = None
        is_staff = user.is_staff
        if not is_staff:
            customer = self.get_customer(user)

        operating_cities = City.objects.all().select_related('address')

        if is_staff:
            hub_queryset = Hub.objects.all()
        else:
            hub_queryset = Hub.objects.filter(customer=customer)

        operating_cities = operating_cities.prefetch_related(
            Prefetch(
                "hub_set",
                queryset=hub_queryset,
                to_attr="hubs"
            )
        )

        cities_serializer = FleetSearchFormCitySerializer(
            operating_cities, many=True)
        if is_staff:
            vehicle_classes = VehicleClass.objects.only(
                'commercial_classification')
            vehicle_classes_serializer = FleetSearchFormVehicleTypeSerializer(
                vehicle_classes, many=True)
            combined_serialized_data = BaseSerializer.combine(
                False,
                [cities_serializer, 'cities'],
                [vehicle_classes_serializer, 'vehicle_classes']
            )
            combined_serialized_data["cancellation_reasons"] = OrderUtil(
                ).fetch_order_cancellation_reasons(is_dictlist_format=True)
            combined_serialized_data["statuses"] = list(
                StatusPipeline.ORDER_STATUS_PIPELINE[settings.C2C_ORDER_TYPE])
        else:
            combined_serialized_data = BaseSerializer.combine(
                False,
                [cities_serializer, 'cities']
            )
            combined_serialized_data['order_types'] = settings.ORDER_TYPE_DICT
            combined_serialized_data['statuses'] = list(StatusPipeline.SHIPMENT_ORDER_STATUSES)

        return Response(combined_serialized_data, status.HTTP_200_OK)


class ShipmentExportFileFormatAPIView(views.APIView, CustomerMixin):
    """
         Description     : Get the file Excel file format
         (*Sheet with only column header)
         Request type    : GET
         Params          : Customer ID
         Response        : Excel file.
    """

    permission_classes = (IsAuthenticated, Or(
        IsBlowhornStaff, IsNonIndividualCustomer),)

    def get_filename(self, user_name, category='orders'):
        file = 'template_shipment' + '_' + category + '_' + str(user_name) + '_' + \
               str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        return filename

    def get(self, request):
        customer = None
        params = request.query_params
        rowwise_flag = params.get('is_rowwise',False)
        order_or_orderline_category = params.get('type', False)
        is_rowwise = True if rowwise_flag == 'true' else False

        if request.user.is_staff:
            response = {'data': '', 'errors': ''}
            customer = Customer.objects.filter(
                id=params.get('customer_id', None)).first()

            if not customer:
                response['errors'] = 'No Customer Found'
                status_code = status.HTTP_400_BAD_REQUEST
                return Response(response, status_code)
        else:
            customer = self.get_customer(request.user)

        if order_or_orderline_category and (order_or_orderline_category == 'Orderline'):
            filename = self.get_filename(customer.name, 'orderlines')
            input_data = {
                'data' : [],
                'filename' : filename,
                'data_mapping_constant' : ORDERLINE_COLUMNS_FOR_EXPORT,
                'only_format' : True
            }
        else:
            sku_names = get_skus_for_the_customer(customer)
            hub_names = get_hubs_for_the_customer(customer)
            division_names = get_divisions_for_the_customer(customer)

            row_data = {
                            'expected_delivery_time' : 'YYYY-MM-DD HH24:MI',
                            'pickup_time' : 'YYYY-MM-DD HH24:MI',
                            'division' : json.dumps(division_names),
                            'hub_name' : json.dumps(hub_names)
                        }

            if request.user.is_staff:
                row_data['customer_id'] = customer.id

            data_mapping = []
            if sku_names:
                sku_names = sku_names[:10]

            if not is_rowwise:
                for val in sku_names:
                    dict_ = {
                        'name': val,
                        'label': val,
                        'type': 'string'
                    }
                    data_mapping.append(dict_)

            if is_rowwise:
                dict_ = {
                        'name': 'SKU Details',
                        'label': 'sku_details',
                        'type': 'string'
                    }
                data_mapping.append(dict_)

                if not sku_names:
                    sku_names = ['item_name1','item_name2']
                sku_dict = {x:y for x,y in zip(sku_names,[0]*len(sku_names))}
                row_data['sku_details'] = json.dumps(sku_dict)

            if not request.user.is_staff:
                for e in SHIPMENT_DATA_TO_BE_IMPORTED:
                    if e['name'] == COMPANY_ID:
                        SHIPMENT_DATA_TO_BE_IMPORTED.remove(e)
                        break

            data_mapping.extend(SHIPMENT_DATA_TO_BE_IMPORTED)
            filename = self.get_filename(customer.name)
            input_data = {
                'data' : [row_data],
                'filename' : filename,
                'data_mapping_constant' : data_mapping,
                'only_format' : False
            }

        file_path = export_to_spreadsheet(**input_data)

        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            response['errors'] = 'No file generated'
            status_code = status.HTTP_400_BAD_REQUEST
        return Response(response, status_code)


class ShipmentImportFileAPIView(views.APIView, ShipmentOrderImport):
    """
         Description     : Import the data from the file provided
         Request type    : POST
         Params          : Excel File
         Response        : Raise error if any issues else OK
     """

    permission_classes = (IsAuthenticated,
                          Or(IsBlowhornStaff, IsNonIndividualCustomer),)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):
        data = request.data.dict()
        self._initialize_attributes(data)

        if not self.values_list:
            if len(self.response['errors']) == 0:
                self.response['errors'] = _('No data found in file')
                self.status_code = status.HTTP_400_BAD_REQUEST
        else:
            self.generate_order_from_data(
                self.values_list, self.file_name, self.no_of_rows - 1)

        return Response(self.response, self.status_code)


class FleetOrderStatus(views.APIView):
    # permission_classes = (AllowAny,)
    # authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request):
        data = request.GET
        pickup_date = data.get('pickupDate')
        pickup_date = datetime.strptime(pickup_date, "%Y-%m-%d").date()
        end_date = timezone.now().date()
        customer = CustomerMixin().get_customer(request.user)

        # avoiding case statement which takes time when compared to direct query
        total_order_count = Order.objects.filter(
            customer=customer,
            pickup_datetime__date__gte=pickup_date,
            pickup_datetime__date__lte= end_date,
            order_type=settings.SHIPMENT_ORDER_TYPE
        ).exclude(
            status=StatusPipeline.DUMMY_STATUS
        ).count()

        shipment_lost_count = Order.objects.filter(
            customer=customer,
            pickup_datetime__date__gte=pickup_date,
            pickup_datetime__date__lte= end_date,
            status=StatusPipeline.ORDER_LOST,
            order_type=settings.SHIPMENT_ORDER_TYPE
        ).exclude(
            status=StatusPipeline.DUMMY_STATUS
        ).count()

        shipment_athub_count = Order.objects.filter(
            customer=customer,
            pickup_datetime__date__gte=pickup_date,
            pickup_datetime__date__lte= end_date,
            status=StatusPipeline.REACHED_AT_HUB,
            order_type=settings.SHIPMENT_ORDER_TYPE
        ).exclude(
            status=StatusPipeline.DUMMY_STATUS
        ).count()

        response = {
            'total_orders': total_order_count,
            'shipment_lost': shipment_lost_count,
            'shipments_hub': shipment_athub_count

        }
        return Response(status=status.HTTP_200_OK, data=response)


class CODOrderStatus(views.APIView):
    # permission_classes = (AllowAny,)
    # authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request):
        data = request.GET
        pickup_date = data.get('pickupDate')
        pickup_date = datetime.strptime(pickup_date, "%Y-%m-%d").date()
        end_date = timezone.now().date()
        customer = CustomerMixin().get_customer(request.user)

        total_order = Order.objects.filter(customer=customer, pickup_datetime__date__gte=pickup_date,
                                           pickup_datetime__date__lte= end_date,
                                           status=StatusPipeline.ORDER_DELIVERED, cash_collected__gt=0,
                                           order_type=settings.SHIPMENT_ORDER_TYPE).values('id', 'cash_collected')

        response = {
            'total_orders': total_order.count(),
            'cash_collected': total_order.aggregate(total_cash=Sum('cash_collected')).get('total_cash', 0)

        }
        return Response(status=status.HTTP_200_OK, data=response)


def get_response(total_order):
    response = {
        '12 AM - 4 AM': 0,
        '4 AM - 8 AM': 0,
        '8 AM - 12 PM': 0,
        '12 PM - 4 PM': 0,
        '4 PM - 8 PM': 0,
        '8 PM - 12 AM': 0
    }
    for x in total_order:
        hour = x.get('hour', None)
        if hour <= 4:
            response.update({'12 AM - 4 AM': response.get('12 AM - 4 AM') + x.get('total_count')})
        elif hour <= 8:
            response.update({'4 AM - 8 AM': response.get('4 AM - 8 AM') + x.get('total_count')})
        elif hour <= 12:
            response.update({'8 AM - 12 PM': response.get('8 AM - 12 PM') + x.get('total_count')})
        elif hour <= 16:
            response.update({'12 PM - 4 PM': response.get('12 PM - 4 PM') + x.get('total_count')})
        elif hour <= 20:
            response.update({'4 PM - 8 PM': response.get('4 PM - 8 PM') + x.get('total_count')})
        elif hour <= 24:
            response.update({'8 PM - 12 AM': response.get('8 PM - 12 AM') + x.get('total_count')})
    return response


class HubwiseData(views.APIView):

    def get(self, request):
        data = request.GET
        pickup_date = data.get('pickupDate')
        pickup_date = datetime.strptime(pickup_date, "%Y-%m-%d").date()
        end_date = timezone.now().date()
        customer = CustomerMixin().get_customer(request.user)

        total_order = Order.objects.filter(customer=customer, date_placed__date__gte=pickup_date,
                                           date_placed__date__lte=end_date, status=StatusPipeline.ORDER_DELIVERED) \
            .annotate(delivered_datetime=Case(When(event__status=StatusPipeline.ORDER_DELIVERED, then=F('event__time')),
                                              output_field=DateTimeField(), )).values('id', 'hub__name',
                                                                                      'expected_delivery_time',
                                                                                      'delivered_datetime',
                                                                                      'exp_delivery_start_time') \
            .exclude(delivered_datetime__isnull=True).distinct('id')

        # hrs taken to deliver order is calculated from pickup event for albl and spar

        picked_order = dict(Order.objects.filter(customer=customer, date_placed__date__gte=pickup_date,
                                                 date_placed__date__lte=end_date, status=StatusPipeline.ORDER_DELIVERED) \
                            .annotate(
            picked_datetime=Case(When(event__status=StatusPipeline.ORDER_PICKED, then=F('event__time')),
                                 output_field=DateTimeField(), )).values_list('id', 'picked_datetime') \
                            .exclude(picked_datetime__isnull=True))

        response = {}
        for x in total_order:
            print(x)
            hub_name = x.get('hub__name', None)
            expected_delivery_time = x.get('exp_delivery_start_time') or picked_order.get(x.get('id'), None) or x.get(
                'expected_delivery_time')

            if not expected_delivery_time or (expected_delivery_time and
                                              x.get('delivered_datetime') <= expected_delivery_time):
                hour = 0
            else:
                hour = math.ceil(((x.get('delivered_datetime') - expected_delivery_time).total_seconds())/3600)

            if hub_name:
                if hub_name not in response.keys():
                    response[hub_name] = {
                        '0 - 4 hr': 0,
                        '4 - 8 hr': 0,
                        '8 - 12 hr': 0,
                        '12 - 16 hr': 0,
                        '16 - 20 hr': 0,
                        '20 - 24 hr': 0,
                    }

                # hr = int(x.get('hour'))
                # time_range = time(hour=hr).strftime('%I:%M%p') + ' - ' + time(hour=hr + 1).strftime('%I:%M%p')
            data = response.get(hub_name)
            if data:
                if hour <= 4:
                    data.update({'0 - 4 hr': data.get('0 - 4 hr') + 1})
                elif hour <= 8:
                    data.update({'4 - 8 hr': data.get('4 - 8 hr') + 1})
                elif hour <= 12:
                    data.update({'8 - 12 hr': data.get('8 - 12 hr') + 1})
                elif hour <= 16:
                    data.update({'12 - 16 hr': data.get('12 - 16 hr') + 1})
                elif hour <= 20:
                    data.update({'16 - 20 hr': data.get('16 - 20 hr') + 1})
                elif hour > 20:
                    # dont want to show we missed deadline by more than 24 hrs
                    data.update({'20 - 24 hr': data.get('20 - 24 hr') + 1})
                response[hub_name] = data

        return Response(status=status.HTTP_200_OK, data=response)


class OrderCreatedTime(views.APIView):

    def get(self, request):
        data = request.GET
        pickup_date = data.get('pickupDate')
        pickup_date = datetime.strptime(pickup_date, "%Y-%m-%d").date()
        end_date = timezone.now().date()
        customer = CustomerMixin().get_customer(request.user)

        total_order = Order.objects.filter(customer=customer, pickup_datetime__date__gte=pickup_date,
            pickup_datetime__date__lte=end_date, status=StatusPipeline.ORDER_DELIVERED) \
            .extra({
            "hour": "date_part(\'hour\', \"pickup_datetime\")"
        }).values('customer', 'hour').annotate(total_count=Count("id")).order_by('hour')

        response = get_response(total_order)

        return Response(status=status.HTTP_200_OK, data=response)


class ShipmentDelivery(views.APIView):

    def get(self, request):
        data = request.GET
        pickup_date = data.get('pickupDate')
        pickup_date = datetime.strptime(pickup_date, "%Y-%m-%d").date()
        end_date = timezone.now().date()
        customer = CustomerMixin().get_customer(request.user)

        total_order = Order.objects.filter(customer_contract__customer=customer,
                                          pickup_datetime__date__gte=pickup_date,
                                          pickup_datetime__date__lte=end_date
                                          # status=StatusPipeline.ORDER_DELIVERED
                                          ).values('customer_contract__customer', 'status')

        response = {
            'shipment_delivery': 0
        }

        delivered_order, total = 0,0

        for order in total_order:
            if order.get('status') == StatusPipeline.ORDER_DELIVERED:
                delivered_order += 1
            total += 1

        if total:
            response = {
                'total': total,
                'delivered_order': delivered_order,
                'shipment_delivery': round((delivered_order / total) * 100, 2)
            }

        return Response(status=status.HTTP_200_OK, data=response)


class OrderReport(views.APIView):

    def get_maximum_order_interval(self, customer, pickup_date, end_date, interval):

        # return Order.objects.filter(customer=customer, pickup_datetime__date__range=(pickup_date, end_date),
        #                             status=StatusPipeline.ORDER_DELIVERED) \
        #     .extra(select={'date': "FLOOR (EXTRACT (EPOCH FROM pickup_datetime) / " +
        #                            str(interval) + ")"}) \
        #     .values('date') \
        #     .annotate(total_count=Count('id')).order_by('-total_count')

        return Order.objects.filter(customer=customer, pickup_datetime__date__gte=pickup_date,
            pickup_datetime__date__lte=end_date,
                                    status=StatusPipeline.ORDER_DELIVERED) \
            .extra({
            "hour": "date_part(\'hour\', \"pickup_datetime\")"
        }).values('customer', 'hour').annotate(total_count=Count("id")).order_by('-total_count')

    def get_attempted_count(self, customer, pickup_date, end_date):

        op_list = Order.objects.filter(customer=customer, pickup_datetime__date__gte=pickup_date,
            pickup_datetime__date__lte=end_date,status=StatusPipeline.ORDER_DELIVERED).values('id').annotate(
            total_event_count=Count(Case(
                When(event__status=StatusPipeline.OUT_FOR_DELIVERY, then=1),
                output_field=IntegerField(),
            )))

        first_attempt, second_attempt, third_attempt = 0, 0, 0

        for x in op_list:
            if x.get('total_event_count', 0) == 1:
                first_attempt += 1
            elif x.get('total_event_count', 0) == 2:
                second_attempt += 1
            elif x.get('total_event_count', 0) == 3:
                third_attempt += 1

        reattempted_orders = (second_attempt + third_attempt) or 0
        total_attempts = (first_attempt + second_attempt + third_attempt)

        avg_completion_time = Order.objects.filter(stops__trip__customer_contract__customer=customer,
            pickup_datetime__date__gte=pickup_date,
            pickup_datetime__date__lte=end_date,
                                       status=StatusPipeline.ORDER_DELIVERED).values(
                'customer_contract__customer').aggregate(minutes=Avg(F('stops__end_time') - F('stops__start_time')))['minutes'] or 0

        if not isinstance(avg_completion_time,int):
            avg_completion_time = avg_completion_time.total_seconds()/60

        attempts = {
            'first_attempt': first_attempt,
            'second_attempt': second_attempt,
            'third_attempt': third_attempt,
            'reattempted_orders': round(reattempted_orders/(total_attempts or 1)* 100,2),
            'total_attempts': total_attempts,
            'avg_completion_time': minutes_to_dhms(avg_completion_time)
        }

        return attempts

    def get(self, request):
        data = request.GET
        interval = OrderConstants().get('CUSTOMER_REPORTING_INTERVAL', 60) * 60
        pickup_date = data.get('pickupDate')
        pickup_date = datetime.strptime(pickup_date, "%Y-%m-%d").date()

        customer = CustomerMixin().get_customer(request.user)
        end_date = timezone.now().date()
        max_data = self.get_maximum_order_interval(customer, pickup_date, end_date, interval)

        response = {
            'max_order': 0,
            'time_range': '-'
        }
        if max_data.exists():
            response = {
                'max_order': (max_data[0].get('total_count') or 0),
                'time_range': (time(hour=int(max_data[0].get('hour'))).strftime('%I:%M%p') + ' - ' +
                              time(hour=int(max_data[0].get('hour') + 1)).strftime('%I:%M%p'))

            }

        attempts = self.get_attempted_count(customer, pickup_date, end_date)
        response.update(attempts)

        return Response(status=status.HTTP_200_OK, data=response)


class ShipmentOrderlineImportFileAPIView(views.APIView):
    """
         Description     : Import the orderline data from the file provided
         Request type    : POST
         Params          : Excel File
         Response        : Raise error if any issues else OK
     """

    permission_classes = (IsAuthenticated,
                          Or(IsBlowhornStaff, IsNonIndividualCustomer),)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):
        data = request.data.dict()
        mixin = ShipmentOrderlineImport()
        is_valid, response = mixin.prepare_data_to_create(data, request.user)
        if is_valid is not False:
            _status, response = mixin.upload_orderlines(response)
            if _status:
                return Response(
                    status=status.HTTP_200_OK,
                    data=response
                )
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=dict(errors=response))


class OrderFlashSale(views.APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get_formatted_data(self, data):

        pickup_datetime = data.get('requested_timeslot')
        pickup_datetime = datetime.strptime(pickup_datetime, '%d-%b-%Y %H:%M')
        cutoff_time_ist = datetime.now().replace(hour=15, minute=0)
        if utc_to_ist(datetime.now()) >= cutoff_time_ist:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Booking slots are closed.')

        phonenumber = settings.ACTIVE_COUNTRY_CODE + data.get('customer_number')
        order_type = 'booking'
        shipping_address = data.get('shipping_address')
        shipping_address.update({'geopoint': CommonHelper.get_geopoint_from_latlong(
            shipping_address.get('geopoint').get('latitude') or 12.9538477,
            shipping_address.get('geopoint').get('longitude') or 77.3507366)})

        name = data.get('customer_name', '')
        shipping_address.update({'phone_number': settings.ACTIVE_COUNTRY_CODE + shipping_address.get('phone_number')})
        alternate_phone_number = shipping_address.get('alternate_phone_number')
        if alternate_phone_number:
            shipping_address.update({'alternate_phone_number': settings.ACTIVE_COUNTRY_CODE +
                                     alternate_phone_number})
        email = data.get('customer_email', '') or phonenumber + '@' + settings.DEFAULT_DOMAINS.get('customer')
        email = email.lower()
        customer, user = CreateOrder().get_or_create_customer(phonenumber, email, name)
        country_code = settings.COUNTRY_CODE_A2
        shipping_address['country'] = Country.objects.filter(
            iso_3166_1_a2=country_code).get()
        pickup_address = shipping_address
        dropoff_address = ShippingAddress(**shipping_address)
        dropoff_address.save()
        pickup_address = ShippingAddress(**pickup_address)
        pickup_address.save()
        contract = Contract.objects.filter(
            contract_type=CONTRACT_TYPE_FLASH_SALE).first()
        sell_rate = ContractHelper().get_sell_rate(
            contract.contractterm_set.all(), timezone.now().date())
        order_data = {
            'customer': customer,
            'city_id': 1,
            'booking_type': ORDER_TYPE_NOW,
            'order_type': order_type,
            'number': 'FS-' + OrderNumberGenerator().order_number(order_type),
            'shipping_address': dropoff_address,
            'pickup_address': pickup_address,
            'vehicle_class_preference': VehicleClass.objects.first(),
            'customer_contract': contract,
            'pickup_datetime': ist_to_utc(pickup_datetime)
        }

        order = Order(**order_data)
        order.save()
        order.update_invoice_information(sell_rate.base_pay)
        order.actual_cost = order.revised_cost
        order.save()
        return order

    def post(self, request):
        data = request.data
        email = data.get('customer_email', '')
        if UserUtils().is_staff_email(email):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Staff email is not allowed to create order')

        order = self.get_formatted_data(data)
        if order:
            data = {
                'sequence_id': 1,
                'order': order,
                'shipping_address': order.shipping_address,
            }
            waypoint = WayPoint(**data)
            waypoint.save()
            response = {
                'pk': order.pk,
                'number': order.number,
                'total_payable': order.total_payable,
            }
            return Response(status=status.HTTP_200_OK, data=response)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Order Creation Failed')

    def put(self, request):
        data = request.data
        email = data.get('customer_email')
        if UserUtils().is_staff_email(email):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Staff email is not allowed to create order')

        pickup_datetime = data.get('requested_timeslot')
        pickup_datetime = datetime.strptime(pickup_datetime, '%d-%b-%Y %H:%M')

        orders = Order.objects.filter(number=data.get('number')) \
            .select_related('customer', 'shipping_address')
        order = orders.first()

        orders.update(pickup_datetime=ist_to_utc(pickup_datetime))
        shipping_address = data.get('shipping_address')
        shipping_address.update({'geopoint': CommonHelper.get_geopoint_from_latlong(
            shipping_address.get('geopoint').get('latitude') or 12.9538477,
            shipping_address.get('geopoint').get('longitude') or 77.3507366)})

        shipping_address.update({
            'phone_number': settings.ACTIVE_COUNTRY_CODE + shipping_address.get('phone_number')
        })
        alternate_phone_number = shipping_address.get('alternate_phone_number')
        if alternate_phone_number:
            shipping_address.update({
                'alternate_phone_number': settings.ACTIVE_COUNTRY_CODE + shipping_address.get('alternate_phone_number')
            })
        with transaction.atomic():
            try:
                ShippingAddress.objects.filter(
                    pk=order.shipping_address_id).update(**shipping_address)
            except:
                logger.info('Flash Sale: Failed to update shipping address: %s' % data.get('number'))
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='Failed to update customer details')

            try:
                customer = order.customer
                name = data.get('customer_name', '')
                customer.name = name
                user = customer.user
                user.name = name
                user.email = data.get('customer_email', '')
                user.phone_number = settings.ACTIVE_COUNTRY_CODE + data.get('customer_number')
                user.save()
            except:
                logger.info('Flash Sale: Failed to update customer details: %s' % data.get('number'))
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='Failed to update customer details')

            return Response(status=status.HTTP_200_OK,
                            data='Order updated successfully')

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Failed to update order')


class OrderOtpCreate(views.APIView):
    # permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data.dict()
        phone_number = data.get('phone_number')

        try:
            phonenumbers.parse(phone_number, settings.COUNTRY_CODE_A2)
        except:
            return Response('Invalid Phone Number !', status.HTTP_400_BAD_REQUEST)

        try:
            otp = randint(1001, 9999)
            response = {'otp': otp}
            template_dict = sms_templates.delivery_otp_verification
            message = template_dict['text'] % otp
            template_json = json.dumps(template_dict)
            send_sms.apply_async(args=[phone_number, message, template_json])

        except Exception as e:
            print(traceback.format_exc())
            return Response(e, status.HTTP_400_BAD_REQUEST)

        return Response(response, status.HTTP_200_OK)


class OrderListView(generics.RetrieveAPIView):
    """Show Previous Settlement amount of driver and next settlement."""

    permission_classes = (IsBlowhornStaff,)
    serializer_class = OrderListSerializer

    def get(self, request):
        """GET method for getting order data."""
        status_code = status.HTTP_200_OK
        response_data = []

        orders = self.get_queryset()
        response_data = self.serializer_class(orders, many=True).data

        return Response(status=status_code, data=response_data)

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        return Order.objects.filter(query).order_by('-pickup_datetime')

    def get_query(self, params):
        customer_id = params.get('customer_id', None)
        _status = params.get('status', StatusPipeline.ORDER_NEW)
        _order_type = params.get('order_type', settings.SPOT_ORDER_TYPE)
        contract_id = params.get('contract_id', None)
        created_by = params.get('created_by', None)

        if type(created_by) != int:
            created_by = self.request.user

        query = []
        if _status:
            statuses = _status.split(",")
            status_query = []
            if statuses:
                status_query.append(Q(status__in=statuses))
            query.append(reduce(operator.or_, status_query))
        if _order_type:
            order_types = _order_type.split(",")
            order_type_query = []
            if order_types:
                order_type_query.append(Q(order_type__in=order_types))
            query.append(reduce(operator.or_, order_type_query))
        if customer_id:
            query.append(Q(customer_id=customer_id))
        if contract_id:
            query.append(Q(customer_contract_id=contract_id))
        if created_by:
            query.append(Q(created_by=created_by))

        return reduce(operator.and_, query)


class HubwiseOrderDetailView(views.APIView):
    permission_classes = (AllowAny, )

    def get(self, request):
        response_data = {}
        reference_number = request.query_params.get('reference_number')
        customer_ids = request.query_params.get('customer_id')
        customer_id_list = None
        is_reference_no_valid = False

        if reference_number:
            if re.match('^[#-_\w]+[ ]*$', str(reference_number)):
                is_reference_no_valid = True

        if not is_reference_no_valid:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_(
                "Order Reference number %s is invalid" % reference_number))

        if customer_ids:
            customer_id_list = customer_ids.split(',')

        if customer_id_list:
            """
            This call is from driver app scan orders
            """
            _params = (Q(reference_number=reference_number) | Q(
                number=reference_number))
            _params &= Q(customer_id__in=customer_id_list)
            _params &= ~Q(status__in=StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES)
        else:
            """
            This call is from cosmos app
            """
            _params = Q(reference_number=reference_number) & ~Q(
                status__in=StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES)

        _params &= Q(order_type=settings.SHIPMENT_ORDER_TYPE)

        order = Order.def_obj.filter(_params).select_related(
            'customer', 'hub', 'shipping_address', 'driver',
            'driver__user').only('number', 'status',
                                 'expected_delivery_time',
                                 'customer__name', 'driver__name',
                                 'driver__user__phone_number',
                                 'driver__user__is_staff',
                                 'hub__name', 'hub__pincodes',
                                 'shipping_address__postcode').first()

        if not order:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_(
                "Order not found for the given reference number!"))

        driver_name = None
        phone_number = None
        driver = order.driver
        if driver:
            driver_name = order.driver.name
            phone_number = str(order.driver.user.phone_number.national_number)

        response_data = {
            'driver_name': driver_name,
            'phone_number': phone_number,
            'status': order.status,
            'customer_id': order.customer.id,
            'customer': order.customer.name,
            'hub': order.hub.name,
            'pincode': order.shipping_address.postcode,
            'order_number': order.number
        }
        return Response(status=status.HTTP_200_OK, data=response_data)


class DriverRating(generics.RetrieveAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, number):

        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()
        else:
            customer = self.get_customer(request.user)

        if not customer:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Unauthorized')

        if number[:3] == 'SH-':
            query = Q(number=number, customer=customer)
        else:
            query = Q(reference_number=number, customer=customer)

        order = Order.objects.filter(query).first()
        if not order:
            query = Q(number=number, customer=customer)
            order = Order.objects.filter(query).first()

        if order:
            driver = order.driver
            if driver:
                rating, count = driver.get_rating()
                response = {
                    "driver_name": driver.name,
                    "phone_number": str(driver.user.phone_number.national_number),
                    "registration_number" : driver.driver_vehicle,
                    "rating": round(rating or 0, 1),
                    "no_of_trips": count
                }
                return Response(status=status.HTTP_200_OK, data=response)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Delivery Partner will be assigned shortly')
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order number')


class EstimateTimeOfArrival(generics.RetrieveAPIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, number):

        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()
        else:
            customer = self.get_customer(request.user)

        if not customer:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Unauthorized')

        if number[:3] == 'SH-':
            query = Q(number=number, customer=customer)
        else:
            query = Q(reference_number=number, customer=customer)

        order = Order.objects.filter(query).first()
        if not order:
            query = Q(number=number, customer=customer)
            order = Order.objects.filter(query).first()
        if order:
            if not order.driver:
                return Response(status=status.HTTP_200_OK, data='We are finding the delivery partner for you, '
                                                                'try again in sometime')
            driver = order.driver
            activity = DriverActivity.objects.filter(driver=order.driver).first()
            if not activity:
                return Response(status=status.HTTP_200_OK, data='we are unable to track the delivery partner')
            driver_location = str(activity.geopoint.y) + ',' + str(activity.geopoint.x)
            pickup_geopoint = order.pickup_address.geopoint
            pickup = str(pickup_geopoint.y) + ',' + str(pickup_geopoint.x)

            url = settings.GOOGLE_MAP_DISTANCE_MATRIX_API % (driver_location, pickup, settings.DISTANCE_MATRIX_KEY)
            response = requests.get(url)
            response_data = json.loads(response.content)
            try:
                duration = response_data['rows'][0]['elements'][0]['duration']
            except:
                duration = 0

            rating, count = driver.get_rating()

            result = {
                "driver_name": driver.name,
                "phone_number": str(driver.user.phone_number.national_number),
                "registration_number": driver.driver_vehicle,
                "rating": rating,
                "no_of_trips": count,
                "duration": duration
            }

            return Response(status=status.HTTP_200_OK, data=result)

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order number')


class GetOrderCount(views.APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key != settings.BLOWHORN_API_KEY:
            return Response(status=status.HTTP_400_BAD_REQUEST,  data='API Key mismatch')

        start_date = data.get('start_date')
        end_date = data.get('end_date')
        if not start_date or not end_date:
            return Response(status=status.HTTP_400_BAD_REQUEST,  data='Start_date and end_date is required')

        date_start = TimeUtils.date_to_datetime_range(start_date)[0]
        date_end = TimeUtils.date_to_datetime_range(end_date)[1]

        start_datetime = date_start.astimezone(tz=pytz.utc)
        end_datetime = date_end.astimezone(tz=pytz.utc)

        orders_count = Order.objects.filter(pickup_datetime__gte=start_datetime,
                                            order_type='booking',
                                            pickup_datetime__lte=end_datetime,
                                            status=StatusPipeline.ORDER_DELIVERED).values(
                                            'number', 'total_payable', 'pickup_datetime')

        return Response(status=status.HTTP_200_OK, data=orders_count)


class BlanketTripShipmentOrder(views.APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        est_dist = (data["route"]["route_distance"])/(1000)
        if "driver" not in data or "id" not in data["driver"]:
            #response = {
            #    "trip_id": "Select Driver Please",
            #    "response_type": "TRIP_NOT_CREATED"
            #}
            #return Response(response, status.HTTP_400_BAD_REQUEST)
            driver_object = None
        else:
            trips = Trip.objects.filter(
                driver_id=data["driver"]["id"],
                status__in=[
                    StatusPipeline.TRIP_NEW,
                    StatusPipeline.TRIP_IN_PROGRESS,
                    StatusPipeline.DRIVER_ACCEPTED,
                    StatusPipeline.TRIP_ALL_STOPS_DONE
                ]
            )
            if trips.exists():
                _create_trip = False
                for trip in trips:
                    if trip.status == StatusPipeline.TRIP_NEW and trip.planned_start_time >= timezone.now() + timedelta(hours=6):
                        _create_trip = True
                    else:
                        _create_trip = False
                        break
                if not _create_trip:
                    response = {
                        "trip_id": "Selected Driver has an upcoming/ongoing Trip! Please select another driver.",
                        "response_type": "TRIP_NOT_CREATED"
                    }
                    return Response(response, status.HTTP_200_OK)

            driver_object = Driver.objects.get(id=data["driver"]["id"])
        stop_detailed_list = []
        first_order = Order.objects.get(number=data["route"]["elements"][1]["order_id"])
        with transaction.atomic():
            for stop in data["route"]["elements"]:
                if(stop["order_id"]!="Start/End"):
                    filter_key = stop["order_id"]
                    stop_order = Order.objects.get(number=filter_key)
                    if stop_order.status is StatusPipeline.ORDER_NEW:
                        if stop_order.has_pickup:
                            response = {
                                "trip_id": "Some Orders are New and yet to be picked up! Please exclude them.",
                                "response_type": "TRIP_NOT_CREATED"
                            }
                            return Response(response, status.HTTP_200_OK)
                    ongoing_stops = stop_order.stops.filter(trip__status__in=[StatusPipeline.TRIP_NEW,
                                                            StatusPipeline.TRIP_IN_PROGRESS,
                                                            StatusPipeline.DRIVER_ACCEPTED]).first()
                    if stop_order.status in StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES:
                        response = {
                            "trip_id": "Please Make sure that all orders are ready for last mile(Filter by Status). Some orders are in end status.",
                            "response_type": "TRIP_NOT_CREATED"
                        }
                        return Response(response, status.HTTP_200_OK)
                    elif ongoing_stops and ongoing_stops.status in [
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS]:
                        response = {
                            "trip_id": "Please Make sure that all orders are ready for last mile(Filter by Status). Some orders are fulfilled by other ongoing trip.",
                            "response_type": "TRIP_NOT_CREATED"
                        }
                        return Response(response, status.HTTP_200_OK)
                    stop_detail = {
                        "order": stop_order,
                        "type": stop["type"],
                        "sequence": int(stop["stop_sequence"])-1
                    }
                    stop_order.driver = driver_object
                    stop_order.invoice_driver= driver_object
                    stop_order.save()
                    stop_detailed_list.append(stop_detail)
        trip = TripMixin().create_trip(
            driver=driver_object,
            estimated_distance_kms=est_dist,
            contract=first_order.customer_contract,
            hub=first_order.hub,
            blowhorn_contract=driver_object.contract if driver_object is not None else None
        )
        stop_object_list = TripMixin().create_multiple_stops_trip(trip=trip,  stop_details=stop_detailed_list)
        if trip:
            OptimalRoute.objects.filter(route_id=data["route"]["abs_id"]).update(trip=trip, is_trip_created=True)
            if driver_object:
                trip_message = "Trip Successfully created with trip id: "+str(trip.trip_number)+". The driver for the trip is: "+str(driver_object.name)
            else:
                trip_message = "Trip Successfully created with trip id: "+str(trip.trip_number)+". Driver will be allocated by someone from Blowhorn Team very soon!"
            response = {
                "trip_id": trip_message,
                "response_type": "TRIP_CREATED"
            }
        else:
            response = {
                "trip_id": "Trip could not be created due to some error. Please Try Again Later",
                "response_type": "TRIP_NOT_CREATED"
            }
        return Response(response, status.HTTP_200_OK)


class OrderDocumentsList(views.APIView, CustomerMixin):

    permission_classes = (AllowAny, )
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, number):

        customer = None
        api_key = request.META.get('HTTP_API_KEY', None)
        if not api_key == settings.FLASH_SALE_API_KEY and 'customer_id' in request.data:
            _out = {
                'status': 'FAIL',
                'message': 'customer_id is a invalid parameter'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        if not customer:
            customer = get_customer_from_api_key(api_key)
        if not customer:
            customer = self.get_customer(request.user)
        if not customer:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        order = Order.objects.filter(number=number, customer=customer).last()
        if not order:
            order = Order.objects.filter(reference_number=number, customer=customer).last()
        if not order:
            _out = {
                'status': 'FAIL',
                'message': 'Order number does not exist.'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        order_docs = OrderDocument.objects.filter(order=order, order__customer=customer)
        if not order_docs.exists():
            _out = {
                'status': 'FAIL',
                'message': 'No documents exist for the given order number'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        data = []
        for doc in order_docs.iterator():
            _dict = {
                'document_type': doc.document_type,
                'document_link': doc.file.url,
                'uploaded_on': doc.created_date.astimezone(
                            pytz.timezone(settings.IST_TIME_ZONE)).strftime('%d %b %Y, %H:%M')
            }
            data.append(_dict)
        _out = {
            'status': 'PASS',
            'message': data
        }

        return Response(status=status.HTTP_200_OK, data=_out)

class OrderChallan(views.APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, order_number):
        from blowhorn.wms.utils.order import get_delivery_challan_data, format_challan_data
        order = Order.objects.get(number=order_number)
        temp = get_delivery_challan_data(order)
        errors, data = format_challan_data(temp)
        error_template = "pdf/challan/error.html"
        order_template = "pdf/challan/challan.html"
        if errors :
            response = TemplateResponse(request, error_template, {'errors': errors})
        else:
            genbytes = generate_pdf(order_template, data)
            response = HttpResponse(genbytes, content_type='application/pdf')
            filename = "%s_challan.pdf" % order.number
            content = "attachment; filename=%s" % filename
            # content = "inline; filename=%s" % filename
            response['Content-Disposition'] = content
        return response


class AdvanceAwb(views.APIView, CustomerMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        try:
            awb_count = request.data.get('awb_count', 0) or 0
            hub_id = request.data.get('hub_id', 0)

            if not awb_count:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='Parameter {awb_count} is missing')
            hub_code = None
            if hub_id:
                _hub = Hub.objects.filter(id=hub_id).first()
                hub_code = _hub.awb_code

            user = request.user
            customer = CustomerMixin().get_customer(user)
            if not customer:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data="Not a valid customer")

            awb_data = get_advance_awb_numbers(customer, awb_count, hub_code)
            if not awb_data.get('success'):
                return Response(status=status.HTTP_400_BAD_REQUEST, data=awb_data.get('msg'))
            return Response(status=status.HTTP_200_OK, data=awb_data.get('data'))

        except Exception as e:
            print(e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Please try again after sometime')


class OrderLinesDashboard(views.APIView, CustomerMixin):
    # permission_classes = (IsCustomer,)
    permission_classes = (AllowAny,)

    def get(self, request):
        order_number = request.GET.get('order_number', None)

        if not order_number:
            response = {
                'status': 'FAIL',
                'message': 'Order number missing'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        order = Order.objects.filter(number=order_number).first()
        if not order:
            response = {
                'status': 'FAIL',
                'message': 'Invalid order request'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        _ol = OrderlineMixin().get_orderlines(order_number)
        data = {
            'uoms' : CommonHelper.get_uom_list(),
            'item_details' : _ol
        }

        return Response(status=status.HTTP_200_OK, data=data)

    def post(self, request):
        try:
            invalid_status = [StatusPipeline.ORDER_NEW, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]
            customer = CustomerMixin().get_customer(request.user)
            item_details = request.data.get('item_details', None)
            order_number = request.data.get('order_number', None)
            option = request.data.get('option', None)
            print(customer, order_number, item_details)

            if not item_details or  not (order_number and len(order_number) != 0):
                response = {
                    'status': 'FAIL',
                    'message': 'Please provide all the mandatory order details'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

            order = Order.objects.filter(number=order_number).first()
            if order.status not in invalid_status:
                response = {
                    'status': 'FAIL',
                    'message': 'The skus can only be edited before pickup'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

            if option == 'add':
                orderline = OrderlineMixin().create_orderlines(item_details, order, customer, 'dashboard')
            elif option == 'delete':
                orderline = OrderlineMixin().delete_orderlines(item_details, order, 'dashboard')
            else:
                orderline = OrderlineMixin().update_orderlines(item_details, order, 'dashboard')

            _out = {
                'status': 'PASS',
                'message': {
                    'item_details': orderline
                }
            }
            return Response(status=status.HTTP_200_OK, data=_out)

        except Exception as e:
            _out = {
                    'status': 'FAIL',
                    'message': 'Please try again after sometime'
            }
            if len(e.args) != 0:
                _out = e.args[0]
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

class ShippingLabelView(views.APIView):
    '''
        Generate the shipping label

        GET: Used by front end to place request to generate the shipping label
        POST: API exposed for end customers for integrations

        INPUT: ids[] or order_numbers[] or json below

        JSON format ::
        {
            "awb_number": "SH-XXXXXXX",
            "order_id": XXXX,
            "date_placed": "XX MON YYYY",
            "weight": XX,
            "amount_to_collect": XX,
            "dimensions": {
                "length": X,
                "breadth": Y,
                "height": Z,
                "uom" : "xx"
            },
            "primary_mobile_no" : XXXXXXXXXX,
            "customer_name": "ABC DEF",
            "shipping_address" : "XXX, 2nd cross XXXXXXXXX",
            "delivery_pincode": XXXXXX,
            "return_address": "XXX, 10th cross XXXXXXXXX, XXXXXX"
        }

    '''

    permission_classes = (AllowAny,)

    def get(self, request):
        _out = {
            'status': 'FAIL',
        }
        try:
            api_key = request.META.get('HTTP_API_KEY', None)
            customer = CustomerMixin().get_customer(request.user)
            if not customer and api_key:
                customer = get_customer_from_api_key(api_key)
            if not customer:
                _out['message'] = 'Unauthorized'
                return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

            _url = ShippingLabel(customer.id, request.user, request.GET).create()
            if _url:
                _out = {
                        'status': 'SUCCESS',
                        'url': _url
                }
                return Response(status=status.HTTP_200_OK, data=_out)

        except ValidationError as ve:
            _out['message'] = ve.message

        except Exception as e:
            logger.error('Shipping Label Get error :: %s' % e)
            _out['message'] = 'Please try again after some time'

        return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

    def post(self, request):
        _out = {
            'status': 'FAIL',
        }
        try:
            api_key = request.META.get('HTTP_API_KEY', None)
            customer = CustomerMixin().get_customer(request.user)
            if not customer and api_key:
                customer = get_customer_from_api_key(api_key)
            if not customer:
                _out['message'] = 'Unauthorized'
                return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

            _url = ShippingLabel(customer.id, request.user, request.data).create()
            if _url:
                _out = {
                        'status': 'SUCCESS',
                        'url': _url
                }
                return Response(status=status.HTTP_200_OK, data=_out)

        except ValidationError as ve:
            _out['message'] = ve.message

        except Exception as e:
            logger.error('Shipping Label Post error :: %s' % e)
            _out['message'] = 'Please try again after some time'

        return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

class OrderPaymentData(views.APIView, CustomerMixin):
    # permission_classes = (IsCustomer,)
    permission_classes = (AllowAny,)

    def get(self, request, number):
        order = Order.objects.filter(number=number).first()
        if not order:
            _msg = {
                "message": "Invalid order number"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_msg)

        customer_account = CustomerBankDetails.objects.filter(customer=order.customer, is_active=True).first()

        if not customer_account:
            _msg = {
                "message": "Merchant bank details not available"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_msg)

        # initialize_data = {"demoAppKey": "3379b7e0-5172-4aa3-8305-a6c163f18684",
        #                    "prodAppKey": "3379b7e0-5172-4aa3-8305-a6c163f18684",
        #                    "merchantName": "Blowhorn", "userName": "9108143838", "currencyCode": "INR",
        #                    "appMode": "Demo",
        #                    "captureSignature": "true", "prepareDevice": "false"}

        payment_data = {
            'amount': float(order.cash_on_delivery),
            'options': {
                'customer': {
                    'mobileNo': str(order.shipping_address.phone_number.national_number),
                    'email': None,
                    'name': order.shipping_address.name
                },
                'references': {
                    'reference1': order.number,
                    'reference2': str(order.id),
                    'externalRefNumber4': order.customer.name,
                    'externalRefNumber5': decrypt_account_number(customer_account.bank_account_number),
                    'externalRefNumber6': customer_account.ifsc_code
                }
            }
        }
        return Response(status=status.HTTP_200_OK, data=payment_data)

