import graphene
import json
from graphene_django.types import DjangoObjectType
from django.conf import settings
from graphene_django.converter import convert_django_field
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.gis.db.models.fields import PointField
from django.contrib.gis.db.models import PolygonField
from recurrence.fields import RecurrenceField
from graphene_django.filter import DjangoFilterConnectionField

from django.db.models import DateTimeField, OuterRef, Subquery, F, Count

from config.settings import status_pipelines as StatusPipeline
from . import models
from blowhorn.address.models import City, Hub
from blowhorn.vehicle.models import VehicleClass
from blowhorn.customer.models import Customer, ShipmentAlertConfiguration
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.contract.models import Contract
from blowhorn.common.helper import CommonHelper
from blowhorn.order.models import Event, Order


class GeoJSON(graphene.Scalar):

    @classmethod
    def serialize(cls, value):
        return json.loads(value.geojson)


@convert_django_field.register(RecurrenceField)
def convert_phone_number_to_string(field, registry=None):
    return graphene.String(description=field.help_text, required=not field.null)


@convert_django_field.register(PhoneNumberField)
def convert_phone_number_to_string(field, registry=None):
    return graphene.String(description=field.help_text, required=not field.null)


@convert_django_field.register(PointField)
def convert_field_to_geojson(field, registry=None):
    return graphene.Field(
        GeoJSON,
        description=field.help_text,
        required=not field.null)


@convert_django_field.register(PolygonField)
def convert_field_to_geojson(field, registry=None):
    return graphene.Field(
        GeoJSON,
        description=field.help_text,
        required=not field.null)


class ContractType(DjangoObjectType):
    class Meta:
        model = Contract


class CustomerType(DjangoObjectType):
    class Meta:
        model = Customer


class VehicleClassType(DjangoObjectType):
    class Meta:
        model = VehicleClass


class CityType(DjangoObjectType):
    class Meta:
        model = City


class HubType(DjangoObjectType):
    class Meta:
        model = Hub


class BatchType(DjangoObjectType):
    class Meta:
        model = models.OrderBatch


class OrderType(DjangoObjectType):
    class Meta:
        model = models.Order


class StatusReport(graphene.ObjectType):
    status = graphene.String()
    count = graphene.Int()


class SlotReport(graphene.ObjectType):
    status = graphene.String()
    key = graphene.String()
    value = graphene.Int()
    sortable = graphene.Int()


class ShippingAddressType(DjangoObjectType):
    class Meta:
        model = models.ShippingAddress


class EventType(DjangoObjectType):
    class Meta:
        model = Event
        filter_fields = {'order__number': ['exact']}
        interfaces = (graphene.Node,)


class AddressInput(graphene.InputObjectType):
    id = graphene.Int()
    lat = graphene.Float()
    lng = graphene.Float()


class UpdateAddresses(graphene.Mutation):
    message = graphene.String()

    class Input:
        addresses = graphene.List(AddressInput)

    def mutate(self, info, addresses):
        for address in addresses:
            geopoint = CommonHelper.get_geopoint_from_latlong(address.get('lat'), address.get('lng'))
            shipping_address = models.ShippingAddress.objects.get(id=address.id)
            shipping_address.geopoint = geopoint
            shipping_address.save()

        return UpdateAddresses(
            message='All location addresses updated.'
        )


class Query(graphene.AbstractType):
    unassigned_orders = graphene.List(OrderType)
    shipment_order_statuses = DjangoFilterConnectionField(EventType)
    slotwise_reports = graphene.List(SlotReport, pickup_date=graphene.types.datetime.Date())
    statuswise_reports = graphene.List(StatusReport, pickup_date=graphene.types.datetime.Date())

    def resolve_unassigned_orders(self, info):

        return models.Order.objects.filter(
            order_type=settings.SHIPMENT_ORDER_TYPE,
            status=StatusPipeline.ORDER_NEW).select_related(
            'pickup_address', 'shipping_address', 'batch',
            'city', 'customer'
        )

    def resolve_shipment_order_statuses(self, info, **args):
        return Event.objects.filter(
            status__in=StatusPipeline.DASHBOARD_ORDER_STATUSES
        ).select_related('order').order_by('-time')

    def resolve_slotwise_reports(self, info, pickup_date=None, **args):
        count = {}
        prev = 0
        customer = CustomerMixin().get_customer(info.context.user)
        qs_delivered = Order.objects.filter(customer=customer, status=StatusPipeline.ORDER_DELIVERED,
                                            pickup_datetime__gte=pickup_date).annotate(
            del_time=Subquery(
                Event.objects.filter(order=OuterRef('pk'), status=StatusPipeline.ORDER_DELIVERED).values('time')[
                :1])).annotate(
            picked_time=Subquery(
                Event.objects.filter(order=OuterRef('pk'), status=StatusPipeline.ORDER_PICKED).values('time')[
                :1])).annotate(
            pickup_time=F('pickup_datetime')).values('del_time', 'picked_time', 'pickup_time')
        alerts = ShipmentAlertConfiguration.objects.filter(customer=customer).values(
            'time_since_creation')
        for a in alerts:
            for i in qs_delivered:
                if i.get('picked_time') is None:
                    diff = i.get('del_time') - i.get('pickup_time')
                else:
                    diff = i.get('del_time') - i.get('picked_time')
                if prev <= (diff.total_seconds() / 60) < a.get('time_since_creation'):
                    try:
                        key = '%s-%s' % (int(prev / 60), int(a.get('time_since_creation') / 60))
                        count[key] = {
                            'value': count[key]['value'] + 1,
                            'sortable': int(prev / 60)
                        }
                    except:
                        count[key] = {
                            'value': 1,
                            'sortable': int(prev / 60)
                        }
            prev = a.get('time_since_creation')

        prev = 0
        for i in alerts:
            key1 = '%s-%s' % (int(prev / 60), int(i.get('time_since_creation') / 60))
            if key1 not in count.keys():
                count[key1] = {
                    'value':  0,
                    'sortable': int(prev / 60)
                }
            prev = i.get('time_since_creation')

        n = [SlotReport(status='Delivered', key=k, value=v['value'],  sortable=v['sortable']) for k, v in count.items()]
        n = sorted(n, key=lambda x: x.sortable)

        prev = 0
        count = {}
        qs_inprogress = Order.objects.filter(customer=customer, pickup_datetime__gte=pickup_date).exclude(
            status__in=[StatusPipeline.ORDER_DELIVERED, StatusPipeline.ORDER_NEW]).annotate(
            inprogress_time=Subquery(
                Event.objects.filter(order=OuterRef('pk')).values('time').order_by('-time')[
                :1])).annotate(
            picked_time=Subquery(
                Event.objects.filter(order=OuterRef('pk'), status=StatusPipeline.ORDER_PICKED).values('time')[
                :1])).annotate(
            pickup_time=F('pickup_datetime')).values('inprogress_time', 'picked_time', 'pickup_time')

        for a in alerts:
            for i in qs_inprogress:
                if i.get('picked_time') is None:
                    diff = i.get('inprogress_time') - i.get('pickup_time')
                else:
                    diff = i.get('inprogress_time') - i.get('picked_time')
                if prev <= (diff.total_seconds() / 60) < a.get('time_since_creation'):
                    try:
                        key = '%s-%s' % (int(prev / 60), int(a.get('time_since_creation') / 60))
                        count[key] = {
                            'value': count[key]['value'] + 1,
                            'sortable': int(prev / 60)
                        }
                    except:
                        count[key] = {
                            'value': 1,
                            'sortable': int(prev / 60)
                        }
            prev = a.get('time_since_creation')

        prev = 0
        for i in alerts:
            key1 = '%s-%s' % (int(prev / 60), int(i.get('time_since_creation') / 60))
            if key1 not in count.keys():
                count[key1] = {
                    'value': 0,
                    'sortable': int(prev / 60)
                }
            prev = i.get('time_since_creation')

        m = [SlotReport(status='Inprogress', key=k, value=v['value'], sortable=v['sortable']) for k, v in count.items()]
        m = sorted(m, key=lambda x: x.sortable)
        return n + m

    def resolve_statuswise_reports(self, info,  pickup_date=None, **args):
        customer = CustomerMixin().get_customer(info.context.user)
        values = dict(Order.objects.filter(customer=customer, pickup_datetime__gte=pickup_date,
                                           status__in=[StatusPipeline.ORDER_DELIVERED, StatusPipeline.DRIVER_ASSIGNED,
                                                       StatusPipeline.ORDER_OUT_FOR_PICKUP, StatusPipeline.ORDER_PICKED,
                                                       StatusPipeline.OUT_FOR_DELIVERY, StatusPipeline.ORDER_NEW,
                                                       StatusPipeline.ORDER_RETURNED,
                                                       StatusPipeline.RESCHEDULE_DELIVERY]).values_list(
            'status').annotate(count=Count('status')))
        return [StatusReport(status=k, count=v) for k, v in values.items()]


class Mutation(graphene.ObjectType):
    update_addresses = UpdateAddresses.Field()
