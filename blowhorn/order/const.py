from django.utils.translation import ugettext_lazy as _
from config.settings import status_pipelines

from blowhorn.contract.constants import (
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKED,
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_RTO,
    SHIPMENT_SERVICE_ATTRIBUTE_OUT_FOR_DELIVERY
)

ORDER_POD_WEBHOOK_DATE_RANGE = 5

FlashSale_URL = '/media/FlashSale'

ADV_AWB_SEQ_PREFIX = 'AWB'
ADV_AWB_DEF_HUBCODE = 'BH'
ADV_AWB_START_SEQ = 100001

# Notification types
NEW_BOOKING = 'new_booking'
STATUS_UPDATE = 'status_update'

ORDER_UPDATES_FIREBASE_PATH = "orders/v1/%s"

PAYMENT_STATUS_UNPAID = u'Un-Paid'
PAYMENT_STATUS_PAID = u'Paid'
PAYMENT_STATUS_PARTIALLY_PAID = u'Partially-Paid'

PAY_STATUS_OPTIONS = (
    (PAYMENT_STATUS_UNPAID, _('Un-Paid')),
    (PAYMENT_STATUS_PAID, _('Paid')),
    (PAYMENT_STATUS_PARTIALLY_PAID, u'Partially-Paid')
)

SIGNATURE = u'Signature'
PROOF_OF_DELIVERY = u'POD'
PROOF_OF_NON_DELIVERY = u'POND'
PROOF_OF_REJECT = u'POR'
PROOF_OF_RTO = u'PORTO'
PROOF_OF_PICKUP = u'POP'
OTP_SIGN = u'OTP sign'
POD_PDF = u'e-POD'

ORDER_DOCUMENT_TYPES = (
    (SIGNATURE, _('Signature')),
    (PROOF_OF_DELIVERY, _('POD')),
    (PROOF_OF_NON_DELIVERY, _('POND')),
    (PROOF_OF_REJECT, _('POR')),
    (PROOF_OF_PICKUP, _('POP')),
    (PROOF_OF_RTO, _('PORTO')),
    (OTP_SIGN, _('OTP sign')),
    (POD_PDF, _('e-POD'))
)

PAYMENT_MODE_RAZORPAY = u'Online'
PAYMENT_MODE_ONLINE = u'Online'
PAYMENT_MODE_CASH = u'Cash'
PAYMENT_MODE_CASHFREE = u'Cashfree'
PAYMENT_MODE_EZETAP = u'Ezetap'
PAYMENT_MODE_BANK_TRANSFER = u'Bank Transfer'
PAYMENT_MODE_CARD = u'Debit/Credit Card'
PAYMENT_MODE_PAYTM = u'PayTm'
BLOWHORN_WALLET = u'Wallet'
PAYMENT_MODE_CHEQUE = u'Cheque'

BLOWHORN_AIR = 'Blowhorn Air'
BLOWHORN_SURFACE = 'Blowhorn Surface'
BLOWHORN_HEAVY = 'Blowhorn Heavy'

CARRIER_MODE = [BLOWHORN_AIR , BLOWHORN_SURFACE, BLOWHORN_HEAVY]

PAYMENT_MODE_OPTIONS = (
    (PAYMENT_MODE_RAZORPAY, _('Online')),
    (PAYMENT_MODE_CASH, _('Cash')),
    (PAYMENT_MODE_CASHFREE, _('CashFree')),
    (PAYMENT_MODE_EZETAP, _('Ezetap')),
    (PAYMENT_MODE_CHEQUE, _('Cheque')),
    (PAYMENT_MODE_PAYTM, _('PayTm')),
    (BLOWHORN_WALLET, _('Wallet')),
    (PAYMENT_MODE_BANK_TRANSFER, _('Bank Transfer')),
    (PAYMENT_MODE_CARD, _('Debit/Credit Card')),
)

ORDER_TYPE_NOW = u'now'
ORDER_TYPE_LATER = u'later'

ORDER_TYPE = (
    (ORDER_TYPE_NOW, _('now')),
    (ORDER_TYPE_LATER, _('later')),
)

CURRENT_BOOKING = 'Current'
ADVANCED_BOOKING = 'Advanced'
DEVICE_TYPE_API = 'api'

LABOUR_COST_HEAVY = 400
LIVE_STATUSES_TRIP = [status_pipelines.TRIP_NEW,
                      status_pipelines.TRIP_IN_PROGRESS,
                      status_pipelines.TRIP_COMPLETED]

STOP_STATUS_DONE = 'done'
STOP_STATUS_PENDING = 'pending'

ORDER_STATUS_CODE = {
    status_pipelines.ORDER_NEW: 'status-new',
    status_pipelines.ORDER_DELIVERED: 'status-delivered',
    status_pipelines.ORDER_CANCELLED: 'status-cancelled',
    'inprogress': 'status-inprogress'
}

IT_STATUS_NEW = 'New'
IT_STATUS_FOLLOWUP = 'Follow-Up'
IT_STATUS_CONFIRMED = 'Confirmed'
IT_STATUS_CANCELLED = 'Cancelled'

INTERNAL_TRACKING_STATUSES = (
    (IT_STATUS_NEW, _("New")),
    (IT_STATUS_FOLLOWUP, _("Follow-Up")),
    (IT_STATUS_CONFIRMED, _("Confirmed")),
    (IT_STATUS_CANCELLED, _("Cancelled")),
)

#Address types
RESIDENTIAL_ADDRESS = 'Residential'
COMMERCIAL_ADDRESS = 'Commercial'

SHIPPING_ADDRESS_TYPES = (
    (RESIDENTIAL_ADDRESS,_('Residential')),
    (COMMERCIAL_ADDRESS,_('Commercial'))

)

# Shipment Attribute to Order Event status fields mapping.
STATUS_ORDER_EVENT_FIELD_MAPPING = {
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED: ["Unable-To-Deliver"],
    SHIPMENT_SERVICE_ATTRIBUTE_PICKED: ["Picked"],
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED: ["Returned"],
    SHIPMENT_SERVICE_ATTRIBUTE_RTO: ["Returned-To-Origin"],
    SHIPMENT_SERVICE_ATTRIBUTE_OUT_FOR_DELIVERY: ["Out-For-Delivery"],
}

# ORDER_INVOICING_FIELDS = "number, updated_time, order_type, pickup_datetime, customer, city, driver, \
#                           invoice_driver, toll_charges, status, hub, customer_contract, cash_collected, \
#                           total_distance, invoice_number, weight,volume, id, pickup_address, shipping_address"

ORDER_INVOICING_FIELDS = ['number', 'updated_time', 'order_type', 'pickup_datetime',
                            'customer', 'city', 'driver', 'invoice_driver',
                            'toll_charges', 'status', 'hub', 'customer_contract', 'cash_collected',
                            'total_distance', 'invoice_number', 'weight', 'volume', 'id', 'pickup_address',
                            'shipping_address'
                            ]

ORDER_STATUS_FILTER_CONFIG = {
    'title': 'status',
    'parameter_name': 'status__in',
    'lookup_choices': status_pipelines.ORDER_STATUSES
}

SHIPMENT_ADMIN_EXPORT_FIELDS = [
    'customer__name', 'pickup_hub__name', 'hub__name', 'customer_reference_number',
    'reference_number', 'status', 'formatted_date_placed',
    'shipping_address__first_name', 'shipping_address__line1',
    'shipping_address__postcode', 'latitude', 'longitude',
    'shipping_address__phone_number',
    'pickup_address__first_name', 'pickup_address__line1',
    'pickup_address__postcode',
    'pickup_address__phone_number', 'cash_on_delivery',
    'number', 'driver__name', 'driver__user__phone_number', 'expected_time',
    'last_packed_at', 'last_out_on_road_at', 'first_attempted_at', 'last_attempted_at',
    'delivered_at',  '_remarks', 'state', 'city__name', 'sku', 'total_sku_qty',
    'picked_time', 'bill_date', 'bill_number', 'pod_docs', 'weight', 'volume'
]

SHIPMENT_ADMIN_EXPORT_COLUMN_HEADERS = [
    'Company Name', 'Pickup Hub', 'Delivery Hub', 'Customer Reference Number',
    'Reference Number', 'Status', 'Date Placed', 'Customer Name',
    'Address', 'Pin Code', 'Latitude', 'Longitude',
    'Customer Phone Number', 'Pickup Name', 'Pick up Address',
    'Pick up Pin Code', 'Pick up Phone Number', 'Cash On Delivery',
    'Number', 'Driver', 'Driver Phone Number', 'Expected Delivery Time',
    'Packed At', 'Out on Road At', 'First Attempted At', 'Last Attempted At',
    'Delivered At', 'Remarks', 'State', 'City', 'SKU',
    'Total SKU Qty', 'Picked Time', 'Bill Date', 'Bill Number',
    'POD Files', 'Weight', 'Volume'
]

SHIPMENT_ORDER_EXPORT_FIELDS = ['reference_number', 'status','number']

SHIPMENT_ORDER_EXPORT_COLUMN_HEADERS = [
    'Company Name', 'Reference Number', 'Status', 'Date Placed', 'Customer Name', 'Address', 'Pin Code', 'Number',
    'Remarks', 'City', 'POD Files']

unicom_currency_code_list = """
    AED, AFN, ALL, AMD, ANG, AOA, ARS, AUD, AWG, AZN,
    BAM, BBD, BDT, BGN, BHD, BIF, BMD, BND, BOB, BRL,
    BSD, BTN, BWP, BYR, BZD, CAD, CDF, CHF, CLP, CNY,
    COP, CRC, CUC, CVE, CYP, CZK, DJF, DKK, DOP, DZD,
    EEK, EGP, ERN, ETB, EUR, FJD, FKP, GBP, GEL, GHC,
    GIP, GMD, GNF, GTQ, GYD, HKD, HNL, HRK, HTG, HUF,
    IDR, ILS, INR, IQD, IRR, ISK, JMD, JOD, JPY, KES,
    KGS, KHR, KMF, KPW, KRW, KWD, KYD, KZT, LAK, LBP,
    LKR, LRD, LSL, LTL, LVL, LYD, MAD, MDL, MGA, MKD,
    MMK, MNT, MOP, MRO, MTL, MUR, MVR, MWK, MXN, MYR,
    MZN, NAD, NGN, NIO, NOK, NPR, NZD, OMR, PAB, PEN,
    PGK, PHP, PKR, PLN, PYG, QAR, RON, RSD, RUB, RWF,
    SAR, SBD, SCR, SDD, SEK, SGD, SHP, SKK, SLL, SOS,
    SRD, STD, SYP, SZL, THB, TJS, TMM, TND, TOP, TRY,
    TTD, TWD, TZS, UAH, UGX, USD, UYU, UZS, VEB, VND,
    VUV, WST, XAF, XCD, XOF, XPF, YER, ZAR, ZMK, ZWD
"""


ADMIN_CUSTOMER_TAB_DATA = """
<table style="
    border-collapse: collapse;
">
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;"></td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Pickup Details</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Delivery Details</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Customer Name</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Customer Address</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Pincode</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Mobile Number</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Alternate Mobile Number</td>
    <td style="border: 1px solid #ddd; padding: 13px;"></td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Hub</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
</table>

"""

ADMIN_GENERAL_TAB_DATA = """
<table style="
    border-collapse: collapse;
">
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Reference Number</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Customer Reference Number</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Hub</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Latest hub</td>
    <td style="border: 1px solid #ddd; padding: 13px; color:red;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Drop Off Address</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Dropoff Address Type</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Date Placed</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">OTP</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Exp Delivery Start Time</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Expected Delivery Time</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Driver</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Driver Mobile</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Customer Name</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Customer Mobile</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
<td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Customer</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
<td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;"> Alternate Customer Mobile</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Customer Contract</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Contract Type</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">City</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Weight in Kg</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Volume in cubic centimeter</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Priority</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Dimension (l*b*h)</td>
    <td style="border: 1px solid #ddd; padding: 13px; ">%s * %s * %s %s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Distance Travelled</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Batch Id</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Pickup Address</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Special Delivery Instructions</td>
    <td style="border: 1px solid #ddd; padding: 13px; color:red;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Division</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Updated Time</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Status</td>
    <td style="border: 1px solid #ddd; padding: 13px; color:red;">%s</td>
</tr>
</table>

"""

ADMIN_PAYMENT_TAB_DATA = """
<table style="
    border-collapse: collapse;
">
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Payment Status</td>
    <td style="border: 1px solid #ddd; padding: 13px; color:red;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Payment Mode</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Cash On Delivery</td>
    <td style="border: 1px solid #ddd; padding: 13px; color:red;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Shipping Charges</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Tax</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Discount</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Cash Collected</td>
    <td style="border: 1px solid #ddd; padding: 13px; color:green;">%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">Payment Source</td>
    <td style="border: 1px solid #ddd; padding: 13px;">%s</td>
</tr>
<tr>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;" >Is Avg Basket Price</td>
    <td style="border: 1px solid #ddd; padding: 13px;" >%s</td>
    <td style="background-color: #f2f2f2; border: 1px solid #ddd; padding: 13px;">GMV</td>
    <td style="border: 1px solid #ddd; padding: 13px; color:blue;">%s</td>
</tr>
</table>

"""