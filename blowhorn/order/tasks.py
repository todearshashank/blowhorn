# System and Django imports
from __future__ import absolute_import, unicode_literals
import simplejson as json
import requests
import logging
import traceback
import time
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
from django.shortcuts import get_object_or_404
import pytz
from django.db.models import Prefetch
from django.http import HttpRequest

# 3rd party libraries
from celery import shared_task
# from celery.decorators import periodic_task
from celery.schedules import crontab
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from rest_framework import status

from blowhorn import celery_app
from blowhorn.address.models import Slots
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.firebase_operations import firebase_add_update_database
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.oscar.core.loading import get_model
from blowhorn.order.const import ORDER_UPDATES_FIREBASE_PATH, ORDER_POD_WEBHOOK_DATE_RANGE
from blowhorn.order.utils import OrderUtil
from blowhorn.order.expire import expire_order
from blowhorn.order.search import bulk_index_orders
from blowhorn.trip.models import Trip, Stop
from blowhorn.common.notification import Notification
from blowhorn.order.models import OrderInternalTracking, OrderDispatch, OrderConstants, CallMaskingLog
from blowhorn.driver.mixins import DriverMixin
from blowhorn.common.decorators import redis_batch_lock
import paho.mqtt.publish as publish
from blowhorn.company.models import find_and_fire_hook
from blowhorn.order.signals import new_order_notification
# from config.settings.base import celery_app

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Order = get_model('order', 'Order')
Driver = get_model('driver', 'Driver')
Event = get_model('order', 'Event')
DeliveredAddress = get_model('order', 'DeliveredAddress')
try:
    NOTIFICATION_WAIT_TIME = OrderConstants().get('DISPATCH_TIMER_SECONDS', 30)
except:
    NOTIFICATION_WAIT_TIME  = 30


# @shared_task
@celery_app.task
def send_status_notification(order_id, order_number, status,
                             order_reference_number, notification_url,
                             ecode, customer):
    order_event = Event.objects.filter(
        order=order_id, status=status).first()
    if order_event:
        sign_time = order_event.time
    else:
        sign_time = timezone.now()

    # change local timezone here
    local_tz = pytz.timezone(settings.IST_TIME_ZONE)
    sign_time_ist = sign_time.astimezone(
        local_tz).strftime('%Y-%m-%d %H:%M:%S')

    data = {
        'expressno': order_number,
        'waybillno': order_reference_number,
        'sign_time': sign_time_ist,
        'ecode': ecode
    }

    response = requests.post(url=notification_url, json=data)

    if not response and not response.text:
        logger.error(
            'Response Invalid for Order %s with customer %s',
            order_number, customer)
        return

    logger.info(response)

    text = json.loads(response.text)

    if text and text.get('error_code') == 0:

        if status == StatusPipeline.ORDER_DELIVERED:
            status = StatusPipeline.DELIVERY_ACK
        elif status == StatusPipeline.ORDER_RETURNED:

            status = StatusPipeline.RETURNED_ACK
        elif status == StatusPipeline.UNABLE_TO_DELIVER:

            status = StatusPipeline.UNABLE_TO_DELIVER_ACK

        Order.objects.filter(
            pk=order_id
        ).update(
            status=status,
        )

        try:
            address = order_event.address
            driver = order_event.driver
            current_location = order_event.current_location
            hub_associate = order_event.hub_associate
        except BaseException:
            address = None
            driver = None
            current_location = None
            hub_associate = None

        Event.objects.create(
            status=status,
            order_id=order_id,
            address=address,
            driver=driver,
            current_location=current_location,
            hub_associate=hub_associate
        )

        logger.info(
            'Order %s status acknowledged by customer %s',
            order_number, customer)
    else:
        logger.info(
            'Order %s status acknowledged by customer %s not acknowledged',
            order_number, customer)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def send_restore_notification_to_driver_app(self, order_id, driver_id=None):
    order = get_object_or_404(Order, id=order_id)

    pickup_time = order.pickup_datetime

    before_pickup, after_pickup = order.customer_contract.get_start_and_end_time_to_restore_driver

    # if driver has in-progress trips or new trip has
    # actual start time and no actual end time then don't restore the app
    existing_trip = Trip.objects.filter(driver=order.driver,
                                        actual_start_time__lte=pickup_time,
                                        actual_end_time__isnull=True).exclude(
        status__in=[StatusPipeline.TRIP_END_STATUSES], order=order)

    if not existing_trip.exists() and before_pickup < pickup_time < after_pickup:
        if driver_id:
            driver = get_object_or_404(Driver, id=driver_id)
        else:
            driver = order.driver

        if driver:
            dict_ = {
                'username': driver.id,
                'action': 'restore'
            }
            user = driver.user
            notification_status = Notification.send_action_to_driver(user, dict_)


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def send_kiosk_terms_of_service(self, order_id):
    from blowhorn.utils.mail import Email
    order = Order.objects.select_related('customer', 'customer__user').get(
        id=order_id)
    Email().send_terms_of_service(order)

@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def create_order_on_ceat_system(self, order_number):
    order = Order.objects.filter(number=order_number)
    order = order.prefetch_related('lines').first()
    if order:
        from blowhorn.apps.integrations.v1.tyre.ceat.ceat import create_ceat_order
        create_ceat_order(order)


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def notify(self, order_id):
    from blowhorn.driver.views import DriverAvailability
    order = get_object_or_404(Order, id=order_id)
    output_log = ['order number ' + order.number + ' is dispatching']
    if order.status != StatusPipeline.ORDER_NEW:
        dispatch_log = str(order.dispatch_log) + 'Order status is ' + order.status + "stopping dispatch"
        Order.objects.filter(id=order.id).update(dispatch_log=dispatch_log)
        return
    slots = Slots.objects.filter(customer=order.customer, hub=order.pickup_hub, is_dispatch_required=True).exists()
    if slots:
        available_drivers = DriverAvailability().get_available_drivers(customer=order.customer, order=order)
    else:
        available_drivers = DriverMixin().get_nearest_available_driver(order.pickup_address.geopoint, order)
    output_log.append('Available Driver list  ' + str(available_drivers))
    dispatched_drivers = []
    start_time = timezone.now()
    threshold = timezone.now() + timedelta(minutes=OrderConstants().get('DISPATCH_THRESHOLD_MIN', 15))

    if available_drivers.exists():
        for activity in available_drivers:
            if timezone.now() <= threshold:
                if timezone.now() <= start_time + timedelta(minutes=OrderConstants().get('DISPATCH_QUERY_REFRESH_MIN', 5)):
                    dispatched_drivers.append(activity.driver)
                    distance = round(activity.distance.km, 1)
                    geopoint = activity.geopoint
                    dispatch = OrderDispatch.objects.create(order=order, driver=activity.driver, distance=distance,
                                                            geopoint=geopoint, status=StatusPipeline.DRIVER_DISPATCHED)
                    output_log.append('Dispatching to driver ' + str(activity.driver))
                    # response = Notification.get_mqtt_response(order, dispatch_id=dispatch.id)
                    notification_response = Notification.send_accept_reject_notification(order, activity.driver,
                                                                                         dispatch_id=dispatch.id)
                    output_log.append(str(notification_response))
                    # publish.single('message/driver/'+ str(activity.driver_id), str(response), qos=0, retain=False,
                    #               hostname=settings.MQTT_BROKER_HOST, port=1883, keepalive=35)

                    time.sleep(NOTIFICATION_WAIT_TIME)
                    order_status = Order.objects.get(id=order_id).status
                    if order_status != StatusPipeline.ORDER_NEW:
                        output_log.append('order ' + order.number + ' has Invalid status exiting ...' + order_status)
                        dispatch_log = str(order.dispatch_log) + json.dumps(output_log)
                        Order.objects.filter(id=order.id).update(dispatch_log=dispatch_log)
                        return
                    dispatch_log = str(order.dispatch_log) + json.dumps(output_log)
                    Order.objects.filter(id=order.id).update(dispatch_log=dispatch_log)
                else:
                    output_log.append('Refreshing Driver queryset')
                    if slots:
                        available_drivers = DriverAvailability().get_available_drivers(customer=order.customer,
                                                                                       order=order)
                    else:
                        available_drivers = DriverMixin().get_nearest_available_driver(order.pickup_address.geopoint,
                                                                                       order)
                    available_drivers = available_drivers.exclude(driver__in=dispatched_drivers)
                    output_log.append('Available drivers ' + str(available_drivers))
                    start_time = timezone.now()
                    dispatch_log = str(order.dispatch_log) + json.dumps(output_log)
                    Order.objects.filter(id=order.id).update(dispatch_log=dispatch_log)
            else:
                output_log.append('Order Dispatch TimeOut Exiting ..')
                dispatch_log = str(order.dispatch_log) + json.dumps(output_log)
                Order.objects.filter(id=order.id).update(dispatch_log=dispatch_log)
                return
    else:
        output_log.append('Not able to find the driver')
    dispatch_log = str(order.dispatch_log) + json.dumps(output_log)
    Order.objects.filter(id=order.id).update(dispatch_log=dispatch_log)

@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def send_mail(self, order_id):
    from blowhorn.order.views import OrderInvoice
    from django.http import HttpRequest

    request = HttpRequest()
    request.method = 'POST'
    request.META = {}
    OrderInvoice.as_view()(request, order_id)


@celery_app.task
def get_geopoint_from_delivered_addresses(postcode, reference_number, address):
    delivered_address_queryset = DeliveredAddress.objects.filter(
        postcode=postcode)
    if delivered_address_queryset.exists():

        limit = settings.DELIVERED_ADDRESS_GEOPOINT_FETCH_LIMIT
        if delivered_address_queryset.count() < limit:
            limit = delivered_address_queryset.count()

        delivered_address_dict = {}
        delivered_address_list = []

        for delivered_address in delivered_address_queryset:
            delivered_address_dict[
                delivered_address.address] = delivered_address.geopoint
            delivered_address_list.append(delivered_address.address)

        y = process.extract(address, delivered_address_list,
                            scorer=fuzz.token_set_ratio, limit=limit)

        delivered_address_list = []

        for i in range(limit):

            if y[i][1] > settings.MINIMUM_ADDRESS_STRING_MATCHING_PERCENTAGE:
                delivered_address_list.append(y[i][0])

        if delivered_address_list:
            x = process.extractOne(address, delivered_address_list,
                                   scorer=fuzz.token_sort_ratio)

            geopoint = delivered_address_dict.get(x[0][0])
            order = Order.objects.filter(reference_number=reference_number)
            if order:
                order[0].shipping_address.geopoint = geopoint
                order[0].shipping_address.save()


def broadcast_to_firebase(_dict, channel_name, message_type):
    _dict['message_type'] = message_type
    firebase_add_update_database(json.dumps(_dict), channel_name)

@celery_app.task(base=TransactionAwareTask, bind=True)
def broadcast_booking_updates_admin(self, order_id, channel_name, message_type):
    qs = Order.objects.select_related('driver')
    qs = qs.select_related('customer')
    qs = qs.select_related('labour')
    qs = qs.select_related('city')
    qs = qs.select_related('vehicle_class_preference')
    qs = qs.prefetch_related('event_set')
    qs = qs.prefetch_related('waypoint_set')
    qs = qs.prefetch_related('trip_set')
    qs = qs.prefetch_related(Prefetch('orderinternaltracking_set',
                                      queryset=OrderInternalTracking.objects.all().order_by('-time')))
    order = qs.get(pk=order_id)
    _dict = OrderUtil().get_bookings_details_for_admin(order)
    broadcast_to_firebase(_dict, channel_name, message_type)


@celery_app.task(base=TransactionAwareTask, bind=True)
def broadcast_spot_order_updates_admin(self, order_id, channel_name, message_type):
    qs = Order.objects.select_related('driver')
    qs = qs.select_related('customer')
    qs = qs.select_related('city')
    qs = qs.select_related('vehicle_class_preference')
    qs = qs.prefetch_related('trip_set')
    qs = qs.prefetch_related(Prefetch('orderinternaltracking_set',
                                      queryset=OrderInternalTracking.objects.all().order_by('-time')))
    order = qs.get(pk=order_id)
    _dict = OrderUtil().get_spot_order_details_for_admin(order)
    broadcast_to_firebase(_dict, channel_name, message_type)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def broadcast_booking_updates_customer(self, order_id, channel_name, message_type):
    qs = Order.objects.select_related('driver')
    qs = qs.select_related('labour')
    qs = qs.select_related('customer')
    qs = qs.select_related('vehicle_class_preference')
    qs = qs.prefetch_related('event_set')
    qs = qs.prefetch_related('waypoint_set')
    qs = qs.prefetch_related(
        Prefetch(
            'trip_set',
            queryset=Trip.objects.filter(
                status__in=[
                    StatusPipeline.TRIP_NEW,
                    StatusPipeline.TRIP_IN_PROGRESS,
                    StatusPipeline.TRIP_COMPLETED,
                    StatusPipeline.DRIVER_ACCEPTED,
                    StatusPipeline.TRIP_ALL_STOPS_DONE
                ]
            ).select_related(
                'driver__user',
                'vehicle__vehicle_model__vehicle_class', 'hub'
            ).prefetch_related(
                Prefetch(
                    'stops',
                    queryset=Stop.objects.select_related('waypoint'))
            ),
            to_attr='trips'
        )
    )
    order = qs.get(pk=order_id)
    channel_name = ORDER_UPDATES_FIREBASE_PATH % order.number
    _dict = OrderUtil().get_booking_details_for_customer(
        '', order, route_info=True)
    broadcast_to_firebase(_dict, channel_name, message_type)

    Notification.push_status_to_customer(order.customer.user, _dict)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def send_slack_notification(self, order_id):
    # web hook became inactive so using c2c webhook
    # one webhook can be  using for all channels
    SLACK_URL = settings.C2C_BOOKING_ORDER_SLACK_URL
    SLACK_CHANNEL_DEFAULT = settings.SHIPMENT_ORDER_SLACK_CHANNEL

    order = get_object_or_404(Order, id=order_id)
    slack_message = OrderUtil()._get_slack_message(order)
    slack_channel = SLACK_CHANNEL_DEFAULT

    slack_message.update({'channel': slack_channel})
    try:
        requests.post(url=SLACK_URL, json=slack_message)
    except:
        logger.error(traceback.format_exc())


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def initiate_paytm_refund(self, order_id):
    from blowhorn.payment.views import PayTmRefund
    data = {
      'id': order_id
    }

    request = HttpRequest()
    request.method = 'POST'
    request.data = json.dumps(data)
    PayTmRefund.as_view()(request)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def send_c2c_slack_notification(self, order_id):
    order = get_object_or_404(Order, id=order_id)

    if settings.ENABLE_SLACK_NOTIFICATIONS:
        SLACK_URL = settings.C2C_BOOKING_ORDER_SLACK_URL
        SLACK_CHANNEL_DEFAULT = '#bookings-%s' % (str(order.city).lower().replace(" ", ""))
    else:
        SLACK_URL = getattr(settings, 'SLACK_URL', None)
        SLACK_CHANNEL_DEFAULT = getattr(settings, 'SLACK_CHANNELS', {}).get('PAYMENTS', None)

    slack_message = OrderUtil()._get_c2c_slack_message(order)
    slack_channel = SLACK_CHANNEL_DEFAULT

    slack_message.update({'channel': slack_channel})
    try:
        requests.post(url=SLACK_URL, json=slack_message)
    except:
        logger.error(traceback.format_exc())


@celery_app.task
def send_c2c_payment_notification(order_id, amount, txn_id):
    from blowhorn.payment.paytm.helper import get_razorpay_slack_message

    SLACK_URL = settings.C2C_BOOKING_ORDER_SLACK_URL
    SLACK_CHANNEL_DEFAULT = settings.C2C_PAYMENT_SLACK_CHANNEL

    order = get_object_or_404(Order, id=order_id)
    slack_message = get_razorpay_slack_message(order, amount, txn_id)
    slack_channel = SLACK_CHANNEL_DEFAULT

    slack_message.update({'channel': slack_channel})
    try:
        requests.post(url=SLACK_URL, json=slack_message)
    except:
        logger.error(traceback.format_exc())


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def push_customer_feedback_to_slack(self, content):
    slack_url = settings.C2C_CUSTOMER_FEEDBACK_SLACK_URL
    slack_channel = settings.C2C_CUSTOMER_FEEDBACK_SLACK_CHANNEL

    if settings.DEBUG:
        slack_channel = settings.TESTING_SLACK_CHANNEL_DEFAULT
        slack_url = settings.TESTING_SLACK_URL

    content.update({'channel': slack_channel})
    try:
        requests.post(url=slack_url, json=content)
    except BaseException as e:
        logger.error(traceback.format_exc())


@celery_app.task
# @periodic_task(run_every=(crontab(hour="21", minute="30")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def update_order_to_expired():
    """
    Daily job at night 3:00 AM to updated eligible orders
    to expired status
    """
    if settings.DEBUG:
        return
    logger.info('Task for expiring orders')
    expire_order()


@celery_app.task
def withdraw_amount_from_paytm(order_number, order_amount):
    from blowhorn.payment.views import PayTmWithdrawal
    from django.http import HttpRequest

    withdraw_data = {
        'number': order_number,
        'amount': order_amount,
        'aut_debit': True
    }
    withdraw_request = HttpRequest()
    withdraw_request.method = 'POST'
    withdraw_request.data = json.dumps(withdraw_data)
    PayTmWithdrawal.as_view()(withdraw_request)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def alert_driver_prior_to_trip(self, order_number, trip_number):

    if not order_number:
        logger.info('Invalid data to send.. Aborting..')
        return

    if order_number:
        order = Order.objects.select_related('customer', 'hub', 'hub__address')
        order = order.get(number=order_number)
        if order.pickup_address:
            address = order.pickup_address.line1
        elif order.hub:
            address = order.hub.address.line1
        customer = order.customer
        start_time = order.pickup_datetime
        order_id = order.id
        trip_id = None
        driver = order.driver
    elif trip_number:

        trip = Trip.objects.select_related('hub', 'hub__address', 'customer_contract')
        trip = trip.select_related('customer_contract__customer').get(trip_number=trip_number)
        customer = trip.customer_contract.customer
        trip_id = trip.id
        order_id = None
        start_time = trip.planned_start_time
        address = trip.hub.address.line1
        driver = trip.driver
    else:
        return

    response = {}
    response['data'] = {
        "name": customer.name or customer.legalname,
        "address": address,
        "start_time": start_time.strftime('%a, %d %b %Y %I:%M %p'),
        "trip_id": trip_id,
        "order_id": order_id
     }
    response['action'] = 'trip_alert'

    publish.single('message/driver/' + str(driver.id), str(response), qos=0, retain=False,
                   hostname=settings.MQTT_BROKER_HOST, port=1883, keepalive=35)
    Notification.push_to_fcm_device(driver.user, {'blowhorn_notification': response})


@celery_app.task
# @periodic_task(run_every=(
# crontab(hour="1, 3, 5, 7, 9, 11, 13, 15 ,17, 19, 21, 23", minute="30")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def index_orders():
    """
    Task to index recently modified orders
    """
    bulk_index_orders()


@celery_app.task
# @periodic_task(run_every=(crontab(hour="21", minute="30")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def update_call_logs():
    """
    Daily job at night 3:00 AM to updated eligible orders
    to expired status
    """
    if settings.DEBUG:
        return
    callLogs = CallMaskingLog.objects.filter(created_date__gte=timezone.now()- timedelta(days=1))
    for log in callLogs:
        url = settings.EXOTEL_CALL_DETAILS % (
        settings.EXOTEL_API_KEY, settings.EXOTEL_API_TOKEN, settings.EXOTEL_API_SID, log.sid)
        res = requests.get(url)
        if res.status_code == status.HTTP_200_OK:
            response_data = json.loads(res.content)
            call_info = response_data["Call"]
            log.duration = call_info.get('Duration', 0) or 0
            log.price = call_info.get('Price', 0) or 0
            log.response=response_data
            log.save()


@celery_app.task
@redis_batch_lock(period=10800, expire_on_completion=True)
def send_order_pod_webhooks():
    """
        Daily job to send trigger webhooks for orders whose POD was not sent to customer
    """
    start_date = timezone.now() - timedelta(days=ORDER_POD_WEBHOOK_DATE_RANGE)

    orders = Order.objects.filter(status__in=[StatusPipeline.ORDER_DELIVERED, StatusPipeline.ORDER_PICKED],
                                  order_type=settings.SHIPMENT_ORDER_TYPE,
                                  user__webhooks__is_active=True, pickup_datetime__gte=start_date,
                                  user__webhooks__send_failed_pods=True)

    logger.info('Webhook cronjob: %s. Orders(%s): %s ' % (timezone.now().date(), orders.count(),
                                                          list(orders.values_list('number', flat=True))))

    for order in orders:
        find_and_fire_hook('order.added', order)
