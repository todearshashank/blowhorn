# System and Django libraries
import re
from django.forms.models import model_to_dict
from django.contrib.admin import AdminSite
import copy
import logging
import operator
import simplejson as json
from datetime import datetime, timedelta, date
from functools import reduce
from django.conf import settings
from django.contrib.sites.models import Site
from django.contrib.admin.filters import FieldListFilter
from django.contrib.gis.geos import Point
from django.db import models, transaction
from django.db.models import Q, Sum
from django.utils import timezone
from django.utils.translation import ugettext as _
from blowhorn.common.middleware import current_request

# 3rd Party libraries
from rest_framework import status, exceptions
from blowhorn.oscar.core.loading import get_model

# Blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.address.utils import get_full_address_str, AddressUtil
from blowhorn.common.helper import CommonHelper
from blowhorn.driver.models import DriverActivityHistory
from blowhorn.address.models import City
from blowhorn.order.const import (
    LIVE_STATUSES_TRIP, CURRENT_BOOKING,
    ADVANCED_BOOKING, ORDER_TYPE_NOW, STOP_STATUS_DONE, STOP_STATUS_PENDING,
    ORDER_STATUS_CODE, PAYMENT_STATUS_UNPAID, PAYMENT_STATUS_PAID,
    DEVICE_TYPE_API, ADV_AWB_SEQ_PREFIX, ADV_AWB_START_SEQ, ADV_AWB_DEF_HUBCODE
)
from blowhorn.order.models import OrderCancellationReason
from blowhorn.utils.functions import (
    ist_to_utc, utc_to_ist, timedelta_to_str,
    get_tracking_url,
    smoothen_geopoints, geopoints_to_km, splice_route_info,
    dist_between_2_points, log_db_queries, lat_lon_to_km
)
from blowhorn.vehicle.constants import DEFAULT_VEHICLE_CLASS
from blowhorn.vehicle.utils import get_vehicle_info
from blowhorn.common import sms_templates
from blowhorn.customer.constants import MINIMUM_INVOICE_DISTANCE
from blowhorn.contract.constants import GTA, BSS, \
    SERVICE_TAX_CATEGORIES_CODE_MAP
from blowhorn.contract.constants import CONTRACT_BILLING_TYPE_MAPPING, \
    BILLING_TYPE_HOURLY
from blowhorn.contract.constants import (
    CONTRACT_TYPE_KIOSK
)
from blowhorn.order.const import PAYMENT_MODE_PAYTM, PAYMENT_MODE_CASH
from blowhorn.utils.datetime_function import DateTime

TIME_FORMAT = '%I:%M %p'
DATE_FORMAT = '%A, %d %B, %Y'
DATE_TIME_FORMAT = '%A, %d %B, %Y, %I:%M %p'
DISTANCE_MULTIPLIER = 1.3
AVERAGE_VEHICLE_SPEED_KMPH = 15.0

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
WayPoint = get_model('order', 'WayPoint')
Labour = get_model('order', 'Labour')
Trip = get_model('trip', 'Trip')
DriverActivity = get_model('driver', 'DriverActivity')
DriverRatingDetails = get_model('driver', 'DriverRatingDetails')
ContractTerm = get_model('contract', 'ContractTerm')
OrderBatch = get_model('order', 'OrderBatch')
ShippingAddress = get_model('order', 'ShippingAddress')
Customer = get_model('customer', 'Customer')
Contract = get_model('contract', 'Contract')
SKUCustomerMap = get_model('customer', 'SKUCustomerMap')
SKU = get_model('customer', 'SKU')
# Country = get_model('address', 'Country')
Hub = get_model('address', 'Hub')
CustomerDivision = get_model('customer', 'CustomerDivision')
OrderAwbSequence = get_model('order', 'OrderAwbSequence')


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class OrderNumberGenerator(object):

    def order_number(self, order_type=None):
        prefix = None
        if isinstance(order_type, str) and 0 < len(order_type) and order_type != settings.C2C_ORDER_TYPE:
            prefix = order_type[0:2]

        unique_string = CommonHelper().get_unique_friendly_id(prefix)

        """
        @todo: When record set is huge, we can't afford to query the random order number
        and then generate the number as a recursive function. Need to relook into this.
        """
        if Order.objects.filter(number=unique_string).exists():
            return self.order_number(order_type=order_type)
        else:
            return unique_string


class OrderCreator(object):

    """
    Places the order by writing out the various models
    """

    def create_order_model(
            self, user, shipping_address, billing_address, total,
            order_number, status, request=None, **extra_order_fields):
        """Create an order model."""
        order_data = {'number': order_number, 'currency': total.currency, 'total_incl_tax': total.incl_tax,
                      'total_excl_tax': total.excl_tax, 'shipping_address': shipping_address,
                      'billing_address': billing_address}

        if user and user.is_authenticated:
            order_data['user_id'] = user.id
        order_data['status'] = status
        if extra_order_fields:
            order_data.update(extra_order_fields)

        if 'site' not in order_data:
            order_data['site'] = Site._default_manager.get_current(request)
        order = Order(**order_data)
        try:
            order.save()
        except Exception as e:
            _out = {
                'status': 'FAIL',
                'message': _(str(e))
            }
            logger.info("Order Generation failed error is", _out)
            raise exceptions.ValidationError(_("Order not created."))
        return order


def get_cost_breakdown(cost_components):
    if not cost_components:
        return {}
    components = json.loads(cost_components)
    s_ = 0
    for k, v in components.items():
        s_ += v
    return components


def create_trip_when_driver_assigned(order, request, prev_trip=None):
    from blowhorn.order.serializers.OrderSerializer import OrderSerializer
    if order:
        order_serializer = OrderSerializer(order)
        trip = order_serializer.create_c2c_trip_for_order(
            order=order, driver=order.driver)
        trip = Trip.objects.filter(Q(order_id=order.id) & ~Q(status__in=[
            StatusPipeline.TRIP_COMPLETED,
            StatusPipeline.TRIP_CANCELLED,
            StatusPipeline.TRIP_SUSPENDED],))
        trip = trip[0] if trip else ''

        if trip:
            vehicle_preference = order.driver.vehicles.all()[0]
            trip.driver = order.driver
            trip.vehicle = vehicle_preference
            trip.status = StatusPipeline.TRIP_NEW
            trip.is_contingency = True if prev_trip else False
            trip.invoice_driver = order.driver if not trip.is_contingency else prev_trip.driver
            trip.planned_start_time = order.pickup_datetime
            trip.trip_acceptence_time = order.pickup_datetime
            trip.save()

            vehicle_class = vehicle_preference.vehicle_model.vehicle_class
            template_dict = sms_templates.template_driver_acceptance
            message = template_dict['text'] % \
                (order.number, order.driver.name, str(order.driver.user.phone_number),
                 str(vehicle_class), trip.vehicle.registration_certificate_number,
                 str(get_tracking_url(request, order.number)))

            order.customer.user.send_sms(message, template_dict)


class OrderUtil(object):

    def get_formatted_address(self, addr_obj):
        sequence_id = addr_obj.get('sequence_id', None)
        addr = addr_obj.get('address') or addr_obj.get('_address')
        contact = addr_obj.get('contact') or addr_obj.get('_contact')
        geopoint = addr_obj.get('geopoint') or addr_obj.get('_geopoint')
        identifier = addr_obj.get('name') or ''

        line1 = addr.get('full_address') or addr.get('_fullAddress')
        first_name = ''
        phone_number = ''
        if contact:
            first_name = contact.get('name') or contact.get('_name')
            phone_number = contact.get('mobile', '') or contact.get('_mobile', '')

        postcode = addr.get('postal_code', '') or addr.get('_postalCode', '')
        if not postcode:
            postcode = AddressUtil().get_postcode_from_address_str(line1)

        _dict = {
            'title': '',
            'identifier': identifier,
            'first_name': first_name or '',
            'last_name': '',
            'line1': line1,
            'line2': '',
            'line3': addr.get('area') or addr.get('_area') or '',
            'line4': addr.get('city') or addr.get('_city') or '',
            'state': addr.get('_state') or '',
            'postcode': postcode,
            'country': addr_obj.get('country') or '',
            #'postal_code': postcode,
            'phone_number': phone_number,
            'notes': 'NA',
            'latitude': geopoint.get('lat') or geopoint.get('_lat'),
            'longitude': geopoint.get('lng') or geopoint.get('_lng'),
        }
        if sequence_id:
            _dict.update({'sequence_id': sequence_id})

        return _dict

    def get_labour_cost(self, category):
        labour_dict_ = {
            'light': settings.LABOUR_COST_LIGHT,
            'medium': settings.LABOUR_COST_MEDIUM,
            'heavy': settings.LABOUR_COST_HEAVY,
        }
        return labour_dict_.get(category, 0)

    def format_quickbook_data(self, _from, _to, truck_type, device_type=None):
        return {
            'pickup': _from,
            'dropoff': _to,
            'waypoints': [],
            'timeframe': {'now_or_later': 'now'},
            'items': [],
            'labour_option': None,
            'vehicle_class': truck_type,
            'device_type': device_type,
            'booking_type': 'now'
        }

    def fetch_order_cancellation_reasons(self, is_dictlist_format=False):
        if is_dictlist_format:
            _reasons = OrderCancellationReason.objects.values_list('id','reason')
            return [{'id' : item[0], 'name' : item[1]} for item in _reasons]
        else:
            return list(OrderCancellationReason.objects.values_list('reason', flat=True).order_by('seq_no'))

    def get_cancellation_reason(self, pk):
        return OrderCancellationReason.objects.get(pk=pk)

    def get_formatted_order_data(self, data):
        timeframe = data.get('timeframe') or data.get('pickup_time')
        pickup_address = data.get('pickup')
        shipping_address = data.get('dropoff')
        now_or_later = timeframe.get('now_or_later') or timeframe.get('type')
        if now_or_later == "now":
            pickup_time = timezone.now()
        elif now_or_later == "later":
            later_value = timeframe.get('later_value')
            if later_value:
                # datetime_object = datetime.strptime(later_value, '%d-%b-%Y %H:%M')
                pickup_time = later_value
            else:
                pickup_time = timeframe.get('value')

            pickup_time = DateTime().convert_iso_string_to_datetime(
                    pickup_time)

        waypoints = [self.get_formatted_address(w)
                     for w in data.get('waypoints')]

        billing_type = data.get('price_type', BILLING_TYPE_HOURLY)
        contract_type = CONTRACT_BILLING_TYPE_MAPPING.get(billing_type)

        selected_labour = data.get('labour_option', {}) or {}
        number_of_labours = data.get('number_of_labours', 0)
        if not number_of_labours:
            number_of_labours = selected_labour.get('num_of_labours', 0)

        labour_info = {
            'labour_cost': selected_labour.get('cost_per_labour', 0),
            'no_of_labours': number_of_labours
        }

        if data.get('estimated_distance') is None:
            data['estimated_distance'] = 0
        else:
            data['estimated_distance'] = data.get(
                'estimated_distance', 0) / 1000

        device_type = data.get('device_type',
                               DEVICE_TYPE_API) or DEVICE_TYPE_API
        _dict = {
            'order_type': 'booking',
            'customer_name': data.get('customer_name', ''),
            'total': 0,
            'items': data.get('items', []),
            'pickup_datetime': pickup_time,
            'pickup_address': self.get_formatted_address(pickup_address),
            'shipping_address': self.get_formatted_address(shipping_address),
            'timeframe': now_or_later,
            'vehicle_class': data.get('vehicle_class'),
            'customer_contract': data.get('customer_contract'),
            'contract_type': contract_type,
            'reference_number': '',
            'device_type': device_type,
            'booking_type': data.get('booking_type',
                                     now_or_later) or now_or_later,
            'waypoints': waypoints,
            'labour_info': labour_info,
            'estimated_distance_km': data.get('estimated_distance'),
            'estimated_time_in_mins': data.get('estimated_time', 0),
            'estimated_cost': data.get('estimated_cost_floor', 0),
            'estimated_cost_lower': data.get('estimated_cost_floor', 0),
            'estimated_cost_upper': data.get('estimated_cost_ceil', 0),
            'coupon_code': data.get('coupon_code', None),
            'payment_mode': data.get('payment_mode', PAYMENT_MODE_CASH),
            'is_pod_required': data.get('is_pod_required', False),
            'utm_data': data.get('utm_data', None),
            'tip': data.get('tip_for_partner', 0.0)
        }
        return _dict

    def get_prior_route(self, order=None, trip=None, start_time=None):
        """ If start time is not given then returns all points
            for booking
        """
        query_params = None

        if order:
            query_params = Q(order=order)
        if trip:
            query_params = query_params & Q(
                trip=trip) if query_params else Q(trip=trip)

        if start_time and isinstance(start_time, datetime):
            query_params = query_params & Q(created_time__gte=start_time)

        results = DriverActivityHistory.objects.filter(query_params).only(
            'gps_timestamp', 'location_accuracy_meters', 'geopoint',
            'vehicle_speed_kmph', 'created_time')
        results_filtered = [
            r for r in results if r.location_accuracy_meters < 140
        ]

        sorted_points = sorted(results_filtered, key=lambda x: x.gps_timestamp)
        smoothened_points = smoothen_geopoints(sorted_points)
        return smoothened_points

    def get_travel_time(self, start_time=None, end_time=None):
        try:
            return timedelta_to_str(end_time - start_time)
        except:
            return ''

    def annotate_times_and_distance(self, stops, driver_location,
                                    requested_pickup_time, status):
        p1 = stops[0]
        first_pending_found = False

        for i, p in enumerate(stops):
            start_location = None
            if not first_pending_found:
                if p.get('status') == 'done' and p.get('time_obj'):
                    _datetime = utc_to_ist(p.get('time_obj'))
                    p['time'] = _datetime.strftime(TIME_FORMAT)
                    p['date'] = str(_datetime.date())
                    p['remaining_distance'] = 0
                    p['iso_format'] = _datetime.isoformat()
                    continue
                else:
                    first_pending_found = True
                    start_location = driver_location and \
                        driver_location or p1.get('location')

            start_time = p1.get('time_obj')
            if not start_time:
                start_time = timezone.now()

            if i == 0 and status == 'upcoming':
                if timezone.now() > requested_pickup_time:
                    p['time_obj'] = start_time
                else:
                    p['time_obj'] = requested_pickup_time
            else:
                if p1.get('location') and p.get('location'):
                    total_distance = DISTANCE_MULTIPLIER * \
                        dist_between_2_points(
                            p1.get('location'), p.get('location'))

                    remaining_distance = DISTANCE_MULTIPLIER * dist_between_2_points(
                        start_location or p1.get('location'), p.get('location'))

                    remaining_time_mins = (
                        remaining_distance / AVERAGE_VEHICLE_SPEED_KMPH) * 60

                    p['time_obj'] = start_time + timedelta(
                        minutes=remaining_time_mins)
                    p['estimated_distance'] = total_distance
                    p['remaining_distance'] = remaining_distance
                    _datetime = utc_to_ist(p.get('time_obj'))
                    p['time'] = _datetime.strftime(TIME_FORMAT)
                    p['date'] = str(_datetime.date())
                    p['iso_format'] = _datetime.isoformat()
                else:
                    p['estimated_distance'] = 0
                    p['remaining_distance'] = 0
                    p['time'] = ''
                    p['date'] = ''
            p1 = p

        for p in stops:
            p['location'] = {
                'lat': p.get('location').y if p.get('location') else 0,
                'lng': p.get('location').x if p.get('location') else 0,
                'lon': p.get('location').x if p.get('location') else 0,
                'longitude': p.get('location').x if p.get('location') else 0,
                'latitude': p.get('location').y if p.get('location') else 0
            }

            del p['time_obj']
        return stops

    def _get_address_dict_for_customer(
            self, _name='', address=None,
            _time=None, _status=None, distance=0,
            travel_time=0, extras=None,
            convert_geopoint=False):
        location = None
        if convert_geopoint and isinstance(address.geopoint, Point):
            location = AddressUtil().get_latlng_from_geopoint(address.geopoint)
        else:
            location = address.geopoint

        return {
            'name': _name,
            'address': get_full_address_str(address),
            'area': address.line3,
            'postal_code': address.postcode if address.postcode else '',
            'country': address.country_id if address.country_id else
                settings.COUNTRY_CODE_A2,
            'location': location,
            'time_obj': _time,
            'status': _status,
            'wait_duration': '',
            'estimated_distance': 0,
            'distance': distance,
            'travel_time': travel_time if travel_time else '',
            'contact': AddressUtil().get_contact_from_address(address),
            'extras': extras if extras else '',
            'time': utc_to_ist(_time).strftime('%I:%M %p') if _time else '',
            'iso_time': _time.isoformat() if _time else ''
        }

    def _get_latest_trip_for_order(self, order):
        trips = [s.trip for s in order.stops.all()]
        sorted_trips = sorted(trips, key=lambda x: x.created_date, reverse=True)
        return sorted_trips[0] if len(sorted_trips) > 0 else None

    @log_db_queries
    def get_shipment_order_details_for_customer(self, order, route_info=False):
        last_modified = order.modified_date and order.modified_date.strftime(
            '%Y%m%d%H%M%S')
        display_payment = False
        driver_position = None
        trip = self._get_latest_trip_for_order(order)
        if (order.status in [StatusPipeline.ORDER_NEW]) or \
                (order.status == StatusPipeline.DRIVER_ACCEPTED):
            status = 'upcoming'
        elif order.status == StatusPipeline.ORDER_DELIVERED:
            status = 'done'
        else:
            status = 'ongoing'

        pickup_address = order.pickup_address
        pickup = self._get_address_dict_for_customer(
            _time=None,
            _name=pickup_address.identifier,
            _status=STOP_STATUS_DONE,
            address=pickup_address,
            distance=0.0,
            convert_geopoint=True
        )

        dropoff_address = order.shipping_address
        try:
            driver = trip.driver
            vehicle = trip.vehicle
            phone_number = AddressUtil().get_national_number_from_user_phone(
                driver.user)
            driver_id = driver.id
            driver_name = driver.name
            driver_mobile = phone_number
            vehicle_number = vehicle.registration_certificate_number
            vehicle_model = vehicle.vehicle_model.model_name
            if driver.photo:
                media_url = CommonHelper().get_media_url_prefix()
                driver_pic = media_url + str(driver.photo)
            else:
                driver_pic = ''

        except BaseException:
            driver_id = 0
            driver_name = ''
            driver_mobile = ''
            driver_pic = ''
            vehicle_number = ''
            vehicle_model = ''

        truck_url = ''
        truck_url_detail = ''
        vehicle_class = ''
        vehicle_alias_name = ''
        vehicle_classification = ''
        vehicle_class_preference = order.vehicle_class_preference
        if vehicle_class_preference:
            vehicle_class = vehicle_class_preference.commercial_classification
            vehicle_classification = vehicle_class
            vehicle_alias_name = [
                x.alias_name for x in order.vehicle_class_preference.cityvehicleclassalias_set.all() if x.city == order.city]
            vehicle_alias_name = ', '.join(
                [clss for clss in vehicle_alias_name if clss])
            if vehicle_alias_name:
                vehicle_class = vehicle_alias_name
            try:
                truck_url = vehicle_class_preference.image_selected.url
                truck_url_detail = vehicle_class_preference.image_detail.url
            except:
                pass

        if not truck_url:
            # Display default vehicle images if images are not available
            vehicle = get_vehicle_info(DEFAULT_VEHICLE_CLASS)
            truck_url = vehicle.get('image_url_selected')
            truck_url_detail = vehicle.get('image_url_detail')

        detail_status = StatusPipeline.ORDER_DETAIL_STATUS.get(
            order.status, None)
        stop_status = 'pending'
        if StatusPipeline.ORDER_DELIVERED == order.status:
            stop_status = 'done'
        dropoff = self._get_address_dict_for_customer(
            _time=None,
            _name=dropoff_address.identifier,
            _status=stop_status,
            address=dropoff_address,
            distance=0,
            travel_time=0,
            convert_geopoint=True
        )

        HEADER_ADDRESS_LENGTH = 40
        if pickup_address.line3:
            header_from = pickup_address.line3
        elif len(pickup_address.line1) <= HEADER_ADDRESS_LENGTH:
            header_from = pickup_address.line1
        else:
            header_from = pickup_address.line1[
                0:HEADER_ADDRESS_LENGTH - 3] + '...'

        if dropoff_address.line3:
            header_to = dropoff_address.line3
        elif len(dropoff_address.line1) <= HEADER_ADDRESS_LENGTH:
            header_to = dropoff_address.line1
        else:
            header_to = dropoff_address.line1[
                0:HEADER_ADDRESS_LENGTH - 3] + '...'

        header_from = header_from.lower().title()
        header_to = header_to.lower().title()

        city = order.city
        driver_rating = DriverRatingDetails.objects.filter(order=order).first()
        trip_workflow = order.customer_contract.trip_workflow
        call_masking_required = trip_workflow and trip_workflow.is_call_masking

        status_event = Event.objects.filter(order_id=order.id, status=order.status).order_by('-created_date').first()
        event_date, event_remarks = None, None
        if status_event:
            event_date = utc_to_ist(status_event.time).strftime('%B %d, %Y, %I:%M %p')
            event_remarks = status_event.remarks
        _dict = {
            'invoice_number': order.number,
            'order_id': order.number,
            'order_real_id': str(order.id),
            'order_key': order.number,
            'order_type': order.order_type,
            'status': status,
            'detail_status': detail_status or order.status,
            'last_modified': last_modified,
            'date': utc_to_ist(order.pickup_datetime).strftime(DATE_FORMAT),
            'header_to': header_to,
            'city': city and city.name,
            'city_id': city and city.id,
            'truck_type': vehicle_class,
            'truck_url': truck_url,
            'truck_url_detail': truck_url_detail,
            'driver_id': driver_id,
            'driver_name': driver_name,
            'driver_mobile': driver_mobile,
            'driver_pic': driver_pic,
            'vehicle_number': vehicle_number,
            'vehicle_model': vehicle_model,
            'pickup': pickup,
            'dropoff': dropoff,
            'display_payment': display_payment,
            'booking_datetime': order.date_placed.isoformat(),
            'driver_rating': driver_rating.rating if driver_rating else None,
            'call_masking_required': call_masking_required,
            'customer_name': '%s' % order.customer,
            'event_date': event_date,
            'event_remarks': event_remarks
        }
        logger.info('Order Data: ', _dict)
        return _dict

    # @todo remove app parameter and use same dates and time format
    # in both app and website
    @log_db_queries
    def get_booking_details_for_customer(self, request, order, route_info=False,
                                         app=False, is_utc_time_format=False):

        from blowhorn.activity.mixins import ActivityMixin
        payment_status = 'unpaid'
        DATE_TIME_FORMAT = '%Y%m%d%H%M%S'
        DATE_FORMAT = '%d-%b-%Y'

        now = timezone.now()
        route_trace_list_dict = []
        trip = order.trip_set.filter(
            status__in=[
                StatusPipeline.TRIP_NEW,
                StatusPipeline.TRIP_IN_PROGRESS
            ]).first()
        driver_position = None
        finished_distance = 0
        pickup_distance = 0
        pickup_distance_done = 0
        remaining_distance = 0
        remaining_time_to_pickup_mins = 0
        total_duration = 0
        total_distance = 0
        route_trace = []

        time_arrived_at_pickup = None
        time_completed = None
        time_driver_accepted = None
        last_modified = order.modified_date and order.modified_date.strftime(
            '%Y%m%d%H%M%S')
        customer_contract = order.customer_contract
        display_payment = False
        show_rebook = True
        if order.order_type == 'booking':
            display_payment = True
            if customer_contract.contract_type == CONTRACT_TYPE_KIOSK:
                show_rebook = False

        if (order.status in [StatusPipeline.ORDER_NEW]) or \
                (order.status == StatusPipeline.DRIVER_ACCEPTED
                 and trip and trip.status == StatusPipeline.TRIP_NEW):
            status = 'upcoming'
        elif order.status == StatusPipeline.ORDER_DELIVERED:
            status = 'done'
            trip = order.trips[0] if order.trips else None
            if not trip:
                return False
            total_distance = trip.total_distance or trip.gps_distance or 0
            finished_distance = total_distance
            time_driver_accepted = trip.trip_acceptence_time
            time_arrived_at_pickup = trip.actual_start_time
            time_completed = trip.actual_end_time

            if trip.route_trace:
                route_trace_list_dict = AddressUtil(
                ).get_route_trace_from_multilinestring(trip.route_trace)

            try:
                total_duration = trip.actual_end_time - trip.actual_start_time
                total_duration = timedelta_to_str(total_duration)
            except BaseException:
                pass
        else:
            status = 'ongoing'
            trip = order.trips[0] if order.trips else None
            if not trip:
                return False

            total_distance = max(float(order.estimated_distance_km),trip.total_distance or trip.gps_distance or 0)
            time_driver_accepted = trip.trip_acceptence_time
            time_arrived_at_pickup = trip.actual_start_time

            if route_info and not time_arrived_at_pickup:
                route_trace = ActivityMixin().get_prior_route(trip=trip)
                if len(route_trace) > 1:
                    finished_distance = lat_lon_to_km(
                        [x for x in route_trace])

            route_trace_list_dict = [{'lat': x.y,
                                      'lng': x.x,
                                      'latitude': x.y,
                                      'longitude': x.x} for x in route_trace]

            last_geopoint = None
            if route_trace and route_trace[-1]:
                last_geopoint = CommonHelper.get_geopoint_from_latlong(
                    route_trace[-1][1], route_trace[-1][0])
            driver_position = last_geopoint
            last_modified = trip.modified_date and trip.modified_date.strftime(
                '%Y%m%d%H%M%S')

            if time_arrived_at_pickup:
                # Pickup has been reached
                post_pickup_trace = ActivityMixin().get_prior_route(
                    trip=trip, start_time=time_arrived_at_pickup)

                route_trace_list_dict = [{
                    'lng': p.x,
                    'lat': p.y,
                    'latitude': p.y,
                    'longitude': p.x
                } for p in post_pickup_trace]

                pickup_distance = geopoints_to_km(
                    [x.geopoint for x in splice_route_info(
                        route_trace, end_time=time_arrived_at_pickup)])

            else:
                # On the way to pickup.
                # Get the remaining distance and time to pickup

                pickup_distance_done = finished_distance
                if len(route_trace) > 0:
                    remaining_distance = DISTANCE_MULTIPLIER * dist_between_2_points(
                        last_geopoint, order.pickup_address.geopoint)

                pickup_distance = pickup_distance_done + remaining_distance
                remaining_time_to_pickup_mins = \
                    (remaining_distance / AVERAGE_VEHICLE_SPEED_KMPH) * 60

        try:
            pickup_time_duration = timedelta_to_str(
                time_arrived_at_pickup - time_driver_accepted)
        except:
            pickup_time_duration = ''

        if time_arrived_at_pickup:
            pickup_status = STOP_STATUS_DONE
        else:
            # Pickup is not complete. Calculate expected arrival time
            time_arrived_at_pickup = (max(order.pickup_datetime, now + timedelta(
                minutes=remaining_time_to_pickup_mins)))
            pickup_status = STOP_STATUS_PENDING

        dropoff_status = STOP_STATUS_DONE if time_completed else STOP_STATUS_PENDING

        waypoints = sorted(order.waypoint_set.all(),
                           key=lambda x: x.sequence_id)
        previous_time = time_arrived_at_pickup
        shipping_address_str = ','.join(
            order.shipping_address.active_address_fields())

        stops = []
        for i, wpoint in enumerate(waypoints[:-1]):
            w_address = wpoint.shipping_address
            wpoint_str = ','.join(w_address.active_address_fields())
            stop = wpoint.stops.all() and wpoint.stops.all()[0]
            w_distance = 0
            _time = None
            if stop and stop.status == StatusPipeline.TRIP_COMPLETED:
                _time = stop.end_time
                w_distance = stop.distance
                w_status = STOP_STATUS_DONE
            else:
                w_status = STOP_STATUS_PENDING

#            if shipping_address_str != wpoint_str:
            travel_time = \
                self.get_travel_time(
                    start_time=previous_time, end_time=_time)

            _stop = self._get_address_dict_for_customer(
                _time=_time,
                _status=w_status,
                _name=w_address.identifier,
                address=w_address,
                distance=w_distance,
                travel_time=travel_time
            )
            stops.append(_stop)
            previous_time = _time
#            else:
#                shipping_waypoint = wpoint

        pickup_address = order.pickup_address
        pickup = self._get_address_dict_for_customer(
            _time=time_arrived_at_pickup,
            _name=pickup_address.identifier,
            _status=pickup_status,
            address=pickup_address,
            distance=pickup_distance
        )

        # Dropoff address
        if order.shipping_address:
            dropoff_address = order.shipping_address
        else:
            dropoff_address = pickup_address

#        dropoff_point = shipping_waypoint.stops.all() and \
#            shipping_waypoint.stops.all()[0]
        dropoff_distance = 0
        if dropoff_status == STOP_STATUS_DONE:
            dropoff_distance = waypoints[-1].stops.all(
            )[0].distance if waypoints else total_distance
#        if dropoff_point and \
#                dropoff_point.status == StatusPipeline.TRIP_COMPLETED:
#            dropoff_distance = dropoff_point.distance

        travel_time = self.get_travel_time(
            start_time=previous_time, end_time=time_completed)
        dropoff = self._get_address_dict_for_customer(
            _time=time_completed,
            _name=dropoff_address.identifier,
            _status=dropoff_status,
            address=dropoff_address,
            distance=dropoff_distance,
            travel_time=travel_time,
        )

        pickup_stops_drop = [pickup] + stops + [dropoff]
        pickup_stops_drop = self.annotate_times_and_distance(
            pickup_stops_drop, driver_position,
            order.pickup_datetime, status
        )

        remaining_distance_stops = sum(
            [x.get('remaining_distance', 0) for x in pickup_stops_drop])
        estimated_distance_stops = sum(
            [x.get('estimated_distance', 0) for x in pickup_stops_drop])

        estimated_distance = finished_distance + remaining_distance_stops
        if estimated_distance < estimated_distance_stops:
            estimated_distance = estimated_distance_stops

        pickup = pickup_stops_drop[0]
        stops = pickup_stops_drop[1:-1]
        dropoff = pickup_stops_drop[-1]

        try:
            driver = trip.driver
            vehicle = trip.vehicle
            phone_number = AddressUtil().get_national_number_from_user_phone(
                driver.user)
            driver_id = driver.id
            driver_name = driver.name
            driver_mobile = phone_number
            vehicle_number = vehicle.registration_certificate_number
            vehicle_model = vehicle.vehicle_model.model_name
            if driver.photo:
                media_url = CommonHelper().get_media_url_prefix()
                driver_pic = media_url + str(driver.photo)
            else:
                driver_pic = ''  # driver.photo
        except BaseException:
            driver_id = 0
            driver_name = ''
            driver_mobile = ''
            driver_pic = ''
            vehicle_number = ''
            vehicle_model = ''

        vehicle_class = ''
        truck_url = ''
        truck_url_detail = ''
        vehicle_alias_name = ''
        vehicle_classification = ''
        vehicle_app_icon_name = ''
        vehicle_class_preference = order.vehicle_class_preference
        if vehicle_class_preference:
            vehicle_class = vehicle_class_preference.commercial_classification
            if vehicle_class_preference.app_icon:
                vehicle_app_icon_name = vehicle_class_preference.app_icon
            vehicle_classification = vehicle_class
            vehicle_alias_name = [
                x.alias_name for x in order.vehicle_class_preference.cityvehicleclassalias_set.all() if x.city == order.city]
            vehicle_alias_name = ', '.join(
                [clss for clss in vehicle_alias_name if clss])
            if vehicle_alias_name:
                vehicle_class = vehicle_alias_name
            try:
                truck_url = vehicle_class_preference.image_selected.url
                truck_url_detail = vehicle_class_preference.image_detail.url
            except:
                pass
        if not truck_url:
            # Display default vehicle images if images are not available
            vehicle = get_vehicle_info(DEFAULT_VEHICLE_CLASS)
            truck_url = vehicle.get('image_url_selected')
            truck_url_detail = vehicle.get('image_url_detail')

        total_cost = float(order.total_incl_tax) if order.total_incl_tax else 0
        if order.invoice_details:
            fare_breakup = json.loads(order.invoice_details)
        else:
            fare_breakup = {}

        total_payable = float(order.total_payable)
        amount_paid_online = float(order.amount_paid_online)
        balance_payable = float(order.total_payable) - float(amount_paid_online)

        from blowhorn.utils.mail import Email

        fare = Email().get_cost_beakup(order)

        fare_breakup['time'] = fare.get('time_amount', 0)
        fare_breakup['night_charges'] = fare.get('night_charges', 0)
        fare_breakup['fixed'] = fare.get('base_fare', 0)
        fare_breakup['distance'] = fare.get('dist_amount', 0)
        fare_breakup['cgst'] = fare.get('cgst_amount', 0)
        fare_breakup['sgst'] = fare.get('sgst_amount', 0)
        fare_breakup['vat'] = fare.get('vat_amount', 0)
        fare_breakup['igst'] = fare.get('igst_amount', 0)
        fare_breakup['labour'] = fare_breakup.get('labour', 0)
        fare_breakup['toll_charges'] = float(order.toll_charges)
        fare_breakup['vas_charges'] = fare.get('vas', 0)

        adjustment_record = order.orderadjustment_set.values(
            'category__name').annotate(total_amount=Sum('total_amount'))

        total_amount = fare.get('time_amount', 0) + fare.get('base_fare', 0) \
            + fare.get('dist_amount', 0) + fare_breakup.get('labour', 0)\
            + float(order.toll_charges)

        if not order.rcm:
            total_amount += fare.get('cgst_amount', 0) + \
                fare.get('sgst_amount', 0) + fare.get('igst_amount', 0) + fare.get('vat_amount')

        no_of_labours = 0
        labour_cost = 0
        if hasattr(order, 'labour') and order.labour.labour_cost:
            labour_cost = order.labour.labour_cost
            no_of_labours = order.labour.no_of_labours
        else:
            labour = Labour.objects.filter(order=order).first()
            if labour:
                labour_cost = labour.labour_cost
                no_of_labours = labour.no_of_labours

        city = order.city
        user_message_dict = StatusPipeline.get_status_info(order.status)
        user_message = user_message_dict.get(
            'user_message') if user_message_dict else ''

        tracking_url = ''
        if request:
            tracking_url = get_tracking_url(request, order.number)

        total_distance = float(
            total_distance) if total_distance else finished_distance

        if app:
            if order.payment_status == PAYMENT_STATUS_UNPAID:
                payment_status = 'unpaid'
            elif order.payment_status == PAYMENT_STATUS_PAID:
                payment_status = 'paid'
        else:
            payment_status = order.payment_status.lower() \
                if order.payment_status else PAYMENT_STATUS_UNPAID.lower()

        if total_payable <= 0 and status == 'done':
            payment_status = PAYMENT_STATUS_PAID.lower()

        fare_breakup_details = [{
            'name': 'Base Fare',
            'value': fare_breakup['fixed']
        }, {
            'name': 'Time Charges',
            'value': fare_breakup['time']
        }, {
            'name': 'Distance Charges',
            'value': fare_breakup['distance']
        }, {
            'name': 'Labour Charges',
            'value': fare_breakup['labour']
        }, {
            'name': 'VAS Charges',
            'value': fare_breakup['vas_charges']
        }]

        total_adjustment_amount = 0.0
        for record in adjustment_record:
            total_adjustment_amount += float(record.get('total_amount'))
            fare_breakup_details.append({
                'name': record.get('category__name'),
                'value': float(record.get('total_amount'))
            })

        adjustment = round(float(order.total_payable) - (float(total_amount) + float(order.tip) + float(
                total_adjustment_amount) + fare_breakup['vas_charges'] + float(order.extra_charges) + \
                                                   fare_breakup['night_charges']),2)

        if adjustment > 0:
            fare_breakup['adjustment'] = adjustment
        elif adjustment < 0:
            fare_breakup['discount'] = adjustment

        if order.tip > 0:
            fare_breakup['tip'] = float(order.tip)
            fare_breakup_details.append({
                'name': 'Tip',
                'value': float(order.tip)
            })


        if order.extra_charges > 0:
            fare_breakup['Toll Charges / Parking Charges'] = float(order.extra_charges)

        if fare_breakup['night_charges'] >0:
            fare_breakup_details.append({
                'name': 'Night Charges',
                'value': fare_breakup['night_charges']
            })

        if adjustment > 0:
            fare_breakup_details.append({
                'name': 'Handling Charges',
                'value': adjustment
            })

        # fare_breakup_details.append({
        #     'name': 'Toll Charges',
        #     'value': float(order.toll_charges)
        # })
        fare_breakup_details.append({
            'name': 'Toll Charges / Parking Charges',
            'value': float(order.extra_charges)
        })

        if adjustment < 0:
            fare_breakup_details.append({
                'name': 'Discount',
                'value': adjustment
            })

        fare_breakup_details.append({
            'name': str(fare.get('cgst_percent')),
            'value': fare.get('cgst_amount')
        })

        fare_breakup_details.append({
            'name': str(fare.get('sgst_percent')),
            'value': fare.get('sgst_amount')
        })

        fare_breakup_details.append({
            'name': str(fare.get('igst_percent')),
            'value': fare.get('igst_amount')
        })

        fare_breakup_details.append({
            'name': str(fare.get('vat_percent')),
            'value': fare.get('vat_amount')
        })

        amount_payable = [{
            'name': 'Amount Payable to %s' % settings.BRAND_NAME,
            'value': total_payable
        }]

        if order.tax > 0 and order.rcm:
            amount_payable.append({
                'name': 'Tax Payable to Govt.',
                'value': float(order.tax)
            })

        if route_info:
            HEADER_ADDRESS_LENGTH = 40
        else:
            HEADER_ADDRESS_LENGTH = 24

        if pickup_address.line3:
            header_from = pickup_address.line3
        elif len(pickup_address.line1) <= HEADER_ADDRESS_LENGTH:
            header_from = pickup_address.line1
        else:
            header_from = pickup_address.line1[
                0:HEADER_ADDRESS_LENGTH - 3] + '...'

        if dropoff_address.line3:
            header_to = dropoff_address.line3
        elif len(dropoff_address.line1) <= HEADER_ADDRESS_LENGTH:
            header_to = dropoff_address.line1
        else:
            header_to = dropoff_address.line1[
                0:HEADER_ADDRESS_LENGTH - 3] + '...'

        header_from = header_from.lower().title()
        header_to = header_to.lower().title()

        if trip and trip.actual_start_time:
            time = utc_to_ist(trip.actual_start_time).strftime(TIME_FORMAT)
        else:
            time = utc_to_ist(order.pickup_datetime).strftime(TIME_FORMAT)

        detail_status = StatusPipeline.ORDER_DETAIL_STATUS.get(
            order.status, None)
        if total_distance < MINIMUM_INVOICE_DISTANCE and order.estimated_distance_km:
            total_distance = float(order.estimated_distance_km)

        contract_term = ContractTerm.objects.filter(
            contract=order.customer_contract, wef__lte=date.today()
        ).order_by('-wef').first()
        contract_base_pay = str(
            contract_term.sell_rate.base_pay).replace('.00', '') + " *"

        driver_rating = DriverRatingDetails.objects.filter(order=order).first()
        pickupdate_iso = order.pickup_datetime.date().isoformat()
        if is_utc_time_format:
            orderpickupdate = order.pickup_datetime.date().isoformat()
        else:
            orderpickupdate = utc_to_ist(order.pickup_datetime).strftime(DATE_FORMAT)

        dict_ = {
            'invoice_number': order.invoice_number or order.number,
            'order_id': order.number,
            'contract_id': order.customer_contract_id,
            'contract_base_pay': contract_base_pay,
            'order_real_id': str(order.id),
            'order_key': order.number,
            'status': status,
            'detail_status': detail_status or order.status,
            'detail_status_new': order.status,
            'user_message': user_message,
            'last_modified': last_modified,
            'date_sortable': order.pickup_datetime.strftime(DATE_TIME_FORMAT),
            'date': orderpickupdate,
            'iso_date': pickupdate_iso,
            'time': time,
            'pickup_datetime': order.pickup_datetime.isoformat(),
            'header_from': header_from,
            'header_to': header_to,
            'city': city and city.name,
            'city_id': city and city.id,
            'items': order.items,
            'item_category': list(
                order.item_category.values_list('id', flat=True)) if order.id else [],
            'labour_count': no_of_labours,
            'labour_cph': float(labour_cost) if labour_cost else 0,
            'truck_type': vehicle_class,
            'tracking_url': tracking_url,
            'truck_url': truck_url,
            'truck_url_detail': truck_url_detail,
            'driver_id': driver_id,
            'driver_name': driver_name,
            'driver_mobile': driver_mobile,
            'driver_pic': driver_pic,
            'vehicle_number': vehicle_number,
            'vehicle_model': vehicle_model,
            'pickup_distance': pickup_distance,
            'pickup_distance_done': pickup_distance_done,
            'pickup_time_duration': pickup_time_duration,
            'finished_distance': finished_distance,
            'estimated_distance': estimated_distance if estimated_distance else 0,
            'remaining_distance': remaining_distance_stops,
            'total_duration': total_duration,
            'total_distance': total_distance,
            'estimate': '-',
            'price': total_payable,
            'total_payable': total_payable,
            'amount_paid_online': amount_paid_online,
            'balance_payable': balance_payable,
            'fare_breakup': fare_breakup,
            'pickup': pickup,
            'dropoff': dropoff,
            'stops': stops,
            'display_payment': display_payment,
            'show_rebook': show_rebook,
            'payment_status': payment_status,
            'payment_mode': order.payment_mode and order.payment_mode.lower(),
            'route_info': route_trace_list_dict,
            'fare_breakup_details': fare_breakup_details,
            'amount_payable': amount_payable,
            'rcm': order.rcm,
            'booking_datetime': order.date_placed.isoformat() if order.date_placed else '',
            'booked_from': order.device_type,
            'vehicle_classification': vehicle_classification,
            'vehicle_app_icon_name': vehicle_app_icon_name,
            'auto_debit_paytm': order.auto_debit_paytm,
            'driver_rating': driver_rating.rating if driver_rating else 0.0,
            'pod_list': [x.file.url for x in order.documents.all() if x.file],
            'is_pod_required': order.is_pod_required,
            "tip": float(order.tip)
        }
        if order.rcm:
            dict_['rcm_message'] = order.order_state.rcm_message
        if order.customer.service_tax_category == GTA:
            dict_['HSN_CODE'] = SERVICE_TAX_CATEGORIES_CODE_MAP.get(
                GTA).get('code')
            dict_['gst_category'] = SERVICE_TAX_CATEGORIES_CODE_MAP.get(
                GTA).get('name')
        else:
            dict_['HSN_CODE'] = SERVICE_TAX_CATEGORIES_CODE_MAP.get(
                BSS).get('code')
            dict_['gst_category'] = SERVICE_TAX_CATEGORIES_CODE_MAP.get(
                BSS).get('name')
        return dict_

    def get_bookings_details_for_admin(self, order):
        """
        :param order: instance of Order
        :return: Object containing order details
        """
        from django.contrib.admin import AdminSite
        from blowhorn.order.admin import BookingOrderAdmin

        order_number = order.number
        total_distance = 0
        total_time = 0
        vehicle = '-'

        trips = order.trip_set.all()
        trips = [x for x in trips if x.status in LIVE_STATUSES_TRIP]
        if len(trips) > 0:
            total_distance = trips[0].gps_distance if trips[
                0].gps_distance else 0
            total_time = trips[0].total_time if trips[0].total_time else 0
            vehicle = trips[0].vehicle.registration_certificate_number

        # Customer Details
        city = order.city
        customer = order.customer
        user = customer.user
        mobile_number = user.national_number

        v_class = ''
        if order.vehicle_class_preference:
            vehicles = order.vehicle_class_preference.cityvehicleclassalias_set.all()
            v_class = [x.alias_name for x in vehicles if x.city == order.city]
            if not v_class:
                v_class = order.vehicle_class_preference

        if isinstance(v_class, list):
            v_class = ', '.join([clss for clss in v_class if clss])

        vehicle_class_preference = str(v_class)

        # Address Details
        pickup_address_fields = order.pickup_address.active_address_fields()
        pickup_address_str = u", ".join(pickup_address_fields)

        shipping_address_fields = \
            order.shipping_address.active_address_fields()
        shipping_address_str = u", ".join(shipping_address_fields)

        # Time Related Information
        pickup_datetime = utc_to_ist(order.pickup_datetime).strftime(
            settings.ADMIN_DATETIME_FORMAT)
        pickup_datetime_compare = utc_to_ist(order.pickup_datetime).strftime(
            settings.COMPARE_DATE_FORMAT)
        date_placed = utc_to_ist(order.date_placed).strftime(
            settings.ADMIN_DATETIME_FORMAT)

        updated_time = 'NA'
        if order.updated_time:
            updated_time = utc_to_ist(order.updated_time).strftime(
                settings.ADMIN_DATETIME_FORMAT)

        expected_delivery_time = 'NA'
        if order.expected_delivery_time:
            expected_delivery_time = \
                utc_to_ist(order.expected_delivery_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
        estimated_distance_km = order.estimated_distance_km

        # Driver Related Information
        driver_name = '-'
        driver = order.driver
        if driver:
            driver_name = driver.name

        num_of_labours = 0
        cost_per_labour = 0
        if hasattr(order, 'labour'):
            num_of_labours = order.labour.no_of_labours
            cost_per_labour = order.labour.labour_cost

        # Cost Related Information
        contract = order.customer_contract.name
        total_incl_tax = float(order.total_incl_tax)
        total_excl_tax = float(order.total_excl_tax)

        booking_type = CURRENT_BOOKING \
            if order.booking_type == ORDER_TYPE_NOW else ADVANCED_BOOKING

        color_code = ORDER_STATUS_CODE.get(order.status, 'inprogress')
        order_status = BookingOrderAdmin(Order,
                                         admin_site=AdminSite).get_status(order)
        tracking_status = BookingOrderAdmin(Order, admin_site=AdminSite). \
            tracking_status(order)

        _dict = {
            # General Info
            'pk': order.pk,
            'order_number': order_number,
            'order_type': order.order_type,
            'reference_number': order.reference_number,
            'items': order.items,
            #            'status': order.status,
            'status': order_status,
            'tracking_status': tracking_status,
            'device_type': order.device_type,
            'booking_type': booking_type,
            'color_code': color_code,

            # Customer Info
            'customer': customer.name,
            'customer_category': customer.customer_category,
            'mobile_number': mobile_number,
            'city': city and city.name,
            'city_id': city and city.id,
            'vehicle_class_preference': vehicle_class_preference,


            # Address Details
            'pickup_address': pickup_address_str,
            'shipping_address': shipping_address_str,
            'pickup_area': BookingOrderAdmin(Order, admin_site=AdminSite).get_pickup_address(order),
            'dropoff_area': BookingOrderAdmin(Order, admin_site=AdminSite).get_shipment_address(order),
            'estimated_distance_km': float(estimated_distance_km),

            # Time related
            'pickup_datetime': pickup_datetime,
            'pickup_datetime_compare': pickup_datetime_compare,
            'date_placed': date_placed,
            'updated_time': updated_time,
            'expected_delivery_time': expected_delivery_time,

            # Driver Information
            'driver_name': driver_name,
            'device_info': order.device_info,
            'vehicle': vehicle,
            'num_of_labours': num_of_labours,
            'cost_per_labour': float(cost_per_labour),
            'labour': BookingOrderAdmin(Order, admin_site=AdminSite).labour_details(order) or '-',

            # Cost Details
            'customer_contract': contract,
            'payment_status': order.payment_status,
            'cost_components': {},
            'total_excl_tax': total_excl_tax,
            'total_incl_tax': total_incl_tax,
            'invoice_details': order.invoice_details,
            'total_distance': BookingOrderAdmin(Order, admin_site=AdminSite).get_total_distance(order),
            'total_time': BookingOrderAdmin(Order, admin_site=AdminSite).get_total_time(order),
        }
        return _dict

    def get_spot_order_details_for_admin(self, order):
        """
        :param order: instance of Order
        :return: Object containing order details
        """
        from django.contrib.admin import AdminSite
        from blowhorn.order.admin import SpotOrderAdmin

        order_number = order.number
        vehicle_class = SpotOrderAdmin(Order, admin_site=AdminSite).get_trip_vehicle_class(order)
        trip_status = SpotOrderAdmin(Order, admin_site=AdminSite).get_trip_status(order)
        tracking_status = SpotOrderAdmin(Order, admin_site=AdminSite).tracking_status(order)

        date_placed = utc_to_ist(order.date_placed).strftime(
            settings.ADMIN_DATETIME_FORMAT)
        pickup_datetime = utc_to_ist(order.pickup_datetime).strftime(
            settings.ADMIN_DATETIME_FORMAT)

        _dict = {
            # General Info
            'pk': order.pk,
            'order_number': order_number,
            'order_type': order.order_type,
            'status': order.status,
            'date_placed': date_placed,
            'pickup_datetime': pickup_datetime,

            # Customer Details
            'customer': str(order.customer),
            'customer_category': order.customer_category,
            'hub': str(order.hub),
            'city': order.city.name,

            'driver': str(order.driver) if order.driver else '-',
            'vehicle_class': str(vehicle_class),
            'trip_status': trip_status,
            'tracking_status': tracking_status,
        }

        return _dict

    def get_event_history_dict(self, event, order):
        _dict = {}
        _dict['status'] = event.status
        _dict['event_time'] = utc_to_ist(event.time).strftime(
            settings.ADMIN_DATETIME_FORMAT)
        _dict['location'] = {}
        location = event.current_location or order.hub.address.geopoint
        if location:
            _dict['location']['lat'] = location.y
            _dict['location']['lon'] = location.x
        _dict['associate_name'] = ''
        if event.driver:
            _dict['associate_name'] = event.driver.name
        elif event.hub_associate:
            _dict['associate_name'] = event.hub_associate.name

        return _dict

    def _get_slack_message(self, order):
        # Now let's send this message to slack
        title = "%s --> %s" % (
            order.pickup_address.search_text,
            order.shipping_address.search_text,
        )
        track_url = '%s/%s/change/' % (
            settings.SHIPMENT_ORDER_TRACK_URL,
            order.id)

        expected_delivery_time = ''
        if order.expected_delivery_time:
            expected_delivery_time = utc_to_ist(
                order.expected_delivery_time).strftime(
                settings.ADMIN_DATETIME_FORMAT)
        slack_message = {
            "text": '%s %s in hub %s city %s' % (
                order.status, order.number, order.hub, order.city),
            "attachments": [
                {
                    "fallback": title,
                    "author_name": '%s (%s)' % (
                        order.shipping_address.first_name,
                        order.shipping_address.phone_number),
                    "title": title,
                    "title_link": track_url,
                    "fields": [
                        {
                            # "title": "Project",
                            "value": 'Client\nExpected Time\nReference Number',
                            "short": True
                        },
                        {
                            # "title": "Environment",
                            "value": '%s\n%s\n%s' % (
                                order.customer,
                                expected_delivery_time,
                                order.reference_number),
                            "short": True
                        }
                    ],
                    "color": "#F35A00"
                }
            ]
        }

        return slack_message

    def _get_c2c_slack_message(self, order):
        # Now let's send this message to slack
        title = "%s --> %s" % (
            order.pickup_address.line3,
            order.shipping_address.line3,
        )
        track_url = '%s/%s/change/' % (
            settings.C2C_ORDER_TRACK_URL,
            order.id)
        slack_message = {
            "text": '%s %s in %s city' % (
                order.status, order.number, order.city),
            "attachments": [
                {
                    "fallback": title,
                    "author_name": '%s (%s)' % (
                        order.customer,
                        order.customer.user.phone_number),
                    "title": title,
                    "title_link": track_url,
                    "fields": [
                        {
                            # "title": "Project",
                            "value": 'Customer\nPickup Date\nVehicle Type',
                            "short": True
                        },
                        {
                            # "title": "Environment",
                            "value": '%s\n%s\n%s' % (
                                order.customer,
                                utc_to_ist(
                                    order.pickup_datetime).strftime(
                                    settings.ADMIN_DATETIME_FORMAT),
                                order.vehicle_class_preference),
                            "short": True
                        }
                    ],
                    "color": "#F35A00"
                }
            ]
        }

        return slack_message

    """
    Get the order from order number
    """

    def get_order_from_friendly_id(self, number):
        order = Order.objects.filter(number=number).first()
        return order


class DateFieldFilterTomorrow(FieldListFilter):

    """
    Date time filter added tomorrow
    """

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.field_generic = '%s__' % field_path
        self.date_params = dict([(k, v) for k, v in params.items()
                                 if k.startswith(self.field_generic)])

        now = timezone.now()
        # When time zone support is enabled, convert "now" to the user's time
        if timezone.is_aware(now):
            now = timezone.localtime(now)

        if isinstance(field, models.DateTimeField):
            today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        else:       # field is a models.DateField
            today = now.date()
        tomorrow = today + timedelta(days=1)

        self.lookup_kwarg_since = '%s__gte' % field_path
        self.lookup_kwarg_until = '%s__lt' % field_path
        self.links = (
            (_('Any date'), {}),
            (_('Today'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('Tomorrow'), {
                self.lookup_kwarg_since: str(today + timedelta(days=1)),
                self.lookup_kwarg_until: str(tomorrow + timedelta(days=1)),
            }),
            (_('Past 7 days'), {
                self.lookup_kwarg_since: str(today - timedelta(days=7)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('This month'), {
                self.lookup_kwarg_since: str(today.replace(day=1)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('This year'), {
                self.lookup_kwarg_since: str(today.replace(month=1, day=1)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
        )
        super(DateFieldFilterTomorrow, self).__init__(
            field, request, params, model, model_admin, field_path)

    def expected_parameters(self):
        return [self.lookup_kwarg_since, self.lookup_kwarg_until]

    def choices(self, cl):
        for title, param_dict in self.links:
            yield {
                'selected': self.date_params == param_dict,
                'query_string': cl.get_query_string(
                    param_dict, [self.field_generic]),
                'display': title,
            }


FieldListFilter.register(
    lambda f: isinstance(f, models.DateField), DateFieldFilterTomorrow)


def get_trip(order):
    """
    get trip from order
    """
    trips = order.trip_set.all()
    trip_ = None
    for trip in trips:
        if trip.status in [StatusPipeline.TRIP_COMPLETED,
                           StatusPipeline.TRIP_CANCELLED,
                           StatusPipeline.TRIP_NEW]:
            trip_ = trip
            break
    return trip_


def get_invoice_url(order):
    if order and order.number:
        host_ = order.request.META.get('HTTP_HOST', 'blowhorn.com')

        return host_ + "/invoice/" + order.number
    return None


def calculate_payments(order, trip, tracking_url):
    from blowhorn.order.tasks import send_mail
    order.calculate_and_update_cost(trip=trip)
    order.customer.debit_order_amount(order)
    order.save()

    order.request = current_request()
    invoice_url = get_invoice_url(order)

    template_dict = sms_templates.prepaid_template_booking_completion
    message = template_dict['text'] % (order.number, invoice_url)
    order.customer.user.send_sms(message, template_dict)

    send_mail.apply_async((order.id,),)


def get_skus_for_the_customer(customer):
    sku_names = []
    if customer:
        sku_ids = SKUCustomerMap.objects.filter(
            customer=customer).values_list('sku_id', flat=True)
        sku_names = SKU.objects.filter(
            id__in=sku_ids).values_list('name', flat=True)

    return sku_names

def get_hubs_for_the_customer(customer):
    hub_names = []
    if customer:
        hub_names = list(Hub.objects.filter(customer =
                                customer).values_list('name', flat=True))

    return hub_names

def get_divisions_for_the_customer(customer):
    division_names = []
    if customer:
        division_names = list(CustomerDivision.objects.filter(
                                customer=customer).values_list('name', flat=True))

    return division_names


def get_contract_permission_filter(user):
    """ Common filter to apply for filtering out contarcts
        based on access level
    """
    if not user.is_superuser:
        cities = City.objects.get_cities_for_managerial_user(user)
        contracts = Contract.objects.filter(Q(spocs=user) |
                                            Q(supervisors=user) |
                                            Q(city__in=cities)).distinct()
        return contracts
    return None


class OrderExportClass(object):

    def get_query(self, order_type, params, **kwargs):
        query_params = []
        date_placed__gte = None
        date_placed__lte = None
        updated_time__lte = None
        updated_time__gte = None

        if params.get('date_placed__gte', None):
            splitted_date = params['date_placed__gte'].split(' ')
            if splitted_date:
                date_placed__gte = datetime.strptime(
                    splitted_date[0], '%d-%m-%Y')

        if params.get('date_placed__lte', None):
            splitted_date = params['date_placed__lte'].split(' ')
            if splitted_date:
                date_placed__lte = datetime.strptime(
                    splitted_date[0], '%d-%m-%Y')

        if params.get('updated_time__gte', None):
            splitted_date = params['updated_time__gte'].split(' ')
            if splitted_date:
                updated_time__gte = datetime.strptime(
                    splitted_date[0], '%d-%m-%Y')

        if params.get('updated_time__lte', None):
            splitted_date = params['updated_time__lte'].split(' ')
            if splitted_date:
                updated_time__lte = datetime.strptime(
                    splitted_date[0], '%d-%m-%Y')

        pickup_datetime__gte = datetime.strptime(
            params['pickup_datetime__gte'], '%d-%m-%Y') if params.get(
            'pickup_datetime__gte', None) else None

        pickup_datetime__lte = datetime.strptime(
            params['pickup_datetime__lte'], '%d-%m-%Y') if params.get(
            'pickup_datetime__lte', None) else None

        query_params.append(Q(order_type=order_type))

        if order_type == settings.SHIPMENT_ORDER_TYPE and params.get('q', None):
            split_params = params['q'].split()
            query_params.append(
                (Q(number__in=split_params) |
                Q(reference_number__in=split_params)))
        elif params.get('q', None):
            query_params.append(
                Q(invoice_number__icontains=params['q']) |
                Q(number__icontains=params['q']) |
                Q(status__icontains=params['q']) |
                Q(user__phone_number__icontains=params['q']) |
                Q(customer__user__name__icontains=params['q']) |
                Q(customer__user__phone_number__icontains=params['q']) |
                Q(driver__user__name__icontains=params['q']))

        if order_type == settings.SHIPMENT_ORDER_TYPE and kwargs.get('user',
                                                                     None):
            contracts = get_contract_permission_filter(kwargs['user'])
            if contracts:
                query_params.append(Q(customer_contract__in=contracts))

        if params.get('status'):
            query_params.append(Q(status=params['status']))

        if params.get('status__in'):
            splitted_status = params['status__in'].split(',')
            query_params.append(Q(status__in=splitted_status))

        if params.get('PayTm Transaction Orders'):
            if params['PayTm Transaction Orders'] == 'Successful':
                query_params.append(Q(payment_mode=PAYMENT_MODE_PAYTM) & Q(
                    status=StatusPipeline.ORDER_DELIVERED))
            else:
                query_params.append(Q(auto_debit_paytm=True) &
                                    Q(status=StatusPipeline.ORDER_DELIVERED) &
                                    ~Q(payment_mode=PAYMENT_MODE_PAYTM))

        if params.get('city__id__exact'):
            query_params.append(
                Q(city__id__exact=params[
                    'city__id__exact']))

        if params.get('hub__id__exact'):
            query_params.append(
                Q(hub__id__exact=params[
                    'hub__id__exact']))

        if params.get('pickup_hub__id__exact'):
            query_params.append(
                Q(pickup_hub__id__exact=params[
                    'pickup_hub__id__exact']))

        if params.get('payment_mode__exact'):
            query_params.append(
                Q(payment_mode__exact=params['payment_mode__exact']))

        if params.get('driver__id__exact'):
            query_params.append(
                Q(driver__id__exact=params['driver__id__exact']))

        if params.get('customer__id__exact'):
            query_params.append(
                Q(customer__id__exact=params[
                    'customer__id__exact']))

        if params.get('customer__customer_category__exact'):
            query_params.append(
                Q(customer__customer_category__exact=params[
                    'customer__customer_category__exact']))

        if params.get('payment_status__exact'):
            query_params.append(
                Q(payment_status__exact=params['payment_status__exact']))

        if date_placed__gte and date_placed__lte:
            query_params.append(
                Q(date_placed__date__gte=date_placed__gte) &
                Q(date_placed__date__lte=date_placed__lte))

        if pickup_datetime__gte and pickup_datetime__lte:
            query_params.append(
                Q(pickup_datetime__date__range=(
                    pickup_datetime__gte, pickup_datetime__lte)))

        if updated_time__gte and updated_time__lte:
            query_params.append(
                Q(updated_time__date__range=(
                    updated_time__gte, updated_time__lte)))

        if query_params:
            return reduce(operator.and_, query_params)
        return query_params


def get_advance_awb_numbers(customer, awb_count, hub_code=None):
    ret_data = {'success': False, 'data': None, 'msg': 'None'}

    if not customer:
        ret_data['msg'] = 'Not a customer'
        return ret_data

    if awb_count > 50000:
        ret_data[
            'msg'] = 'A maximum of 50000 advance AWB numbers can be generated in last 24 hours'
        return ret_data

    awb_last_24_hr = OrderAwbSequence.objects.filter(customer=customer,
                                                     created_date__gte=timezone.now() - timedelta(hours=24)
                                                     ).aggregate(seq_count=Sum('awb_count'))

    if awb_last_24_hr.get('seq_count') and (
            awb_count + awb_last_24_hr['seq_count']) > 50000:
        ret_data[
            'msg'] = 'A maximum of 50000 advance AWB numbers can be generated in last 24 hours'
        return ret_data

    parm = {'customer' : customer}
    if hub_code:
        last_awb_seq_obj = OrderAwbSequence.objects.filter(
                                customer=customer, hub_code=hub_code
                            ).order_by('-created_date').first()
    else:
        last_awb_seq_obj = OrderAwbSequence.objects.filter(
                                Q(hub_code=ADV_AWB_DEF_HUBCODE)| Q(hub_code__isnull=True)
                            ).order_by('-created_date').first()

    prefix = ADV_AWB_SEQ_PREFIX

    if customer.awb_code:
        prefix += customer.awb_code

    if hub_code:
        prefix += hub_code
    else:
        prefix += ADV_AWB_DEF_HUBCODE

    if not last_awb_seq_obj:
        seq_start = ADV_AWB_START_SEQ
        seq_end = seq_start + awb_count - 1
    else:
        seq_start = last_awb_seq_obj.seq_end + 1
        seq_end = last_awb_seq_obj.seq_end + awb_count

    awb_list = []

    for seq in range(seq_start, seq_end+1):
        awb_list.append(prefix + str(seq))

    OrderAwbSequence.objects.create(
        customer=customer,
        hub_code=hub_code,
        awb_count=awb_count,
        seq_start=seq_start,
        seq_end=seq_end
    )
    ret_data['success'] = True
    ret_data['data'] = awb_list
    return ret_data

