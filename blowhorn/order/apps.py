# from django.conf.urls import url
# from django.apps import AppConfig
#
#
# class OrderConfig(AppConfig):
#
#     label = 'order'
#     name = 'blowhorn.order'
#     verbose_name = 'Order'
#
#     namespace = 'order'
#
#     def ready(self):
#         from . import receivers  # noqa
#         import blowhorn.order.signals  # noqa
#
#     def get_urls(self):
#         from .views import (
#             OrderList,
#             OrderListView,
#             OrderDetail,
#             FleetOrderStatus,
#             OrderStatusHistory,
#             OrderShipment,
#             UnicomOrderShipment,
#             OrderPackReturn,
#             OrderBreakdown, StoreFront, DriverRating, EstimateTimeOfArrival
#         )
#         from .views import (
#             CustomerAuthTokenView,
#             CustomerAuthToken,
#             OrderTrackingView,
#             OrderPriorRouteView,
#             ShipmentTracking,
#             OrderSummary,
#             OrderInvoice,
#             QuickBook,
#             OrderFlyIntegration,
#             OrderInvoiceAPIView,
#             OrderDocumentUpload,
#             OrderHeatmapAPIView,
#             OrderHeatmapFilterAPIView,
#             ShipmentExportFileFormatAPIView,
#             ShipmentImportFileAPIView,
#             OrderOtpCreate,
#             OrderShipmentCancel,
#             OrderShipmentDetails,
#             OrderFlashSale,
#             CancelOrder,
#             AddOrderlines,
#             DeleteOrderlines,
#             ShipmentOrderlineImportFileAPIView, OrderReport,
#             OrderStatus, CODOrderStatus, HubwiseData, ShipmentDelivery,
#             OrderCreatedTime, HubwiseOrderDetailView, GetOrderCount, BlanketTripShipmentOrder,
#             OrderDocumentsList,
#             AdvanceAwb
#         )
#         urlpatterns = [
#             url(r'^order/flash-sale/$',
#                 OrderFlashSale.as_view(), name='order-flash-sale'),
#             url(r'^orders$', OrderList.as_view(), name='order-list'),
#             url(r'^orders/booking$', OrderList.as_view(),
#                 name='order-list-booking'),
#             url(r'^orders/list$', OrderListView.as_view(), name='order-list'),
#             url(r'^orders/(?P<pk>[0-9]+)$',
#                 OrderDetail.as_view(), name='order-detail'),
#             url(r'^orders/(?P<order_id>[\w-]+)/priorroute$',
#                 OrderPriorRouteView.as_view(), name='prior-route'),
#             url(r'^orders/(?P<order_id>[\w-]+)/status/history$',
#                 OrderStatusHistory.as_view(), name='order-status-history'),
#             url(r'^orders/(?P<order_id>[\w-]+)/status$',
#                 OrderStatus.as_view(), name='order-status'),
#             url(r'^orders/cancel/(?P<order_id>[\w-]+)$',
#                 CancelOrder.as_view(), name='order-cancel'),
#             url(r'^orders/add/items/(?P<order_id>[\w-]+)$',
#                 AddOrderlines.as_view(), name='order-cancel'),
#             url(r'^orders/delete/items/(?P<order_id>[\w-]+)$',
#                 DeleteOrderlines.as_view(), name='order-cancel'),
#             url(r'^orders/shipment$', OrderShipment.as_view(),
#                 name='order-shipment'),
#             url(r'^orders/storefront', StoreFront.as_view(),
#                 name='store-front'),
#             url(r'^orders/shipment/cancel$', OrderShipmentCancel.as_view(),
#                 name='order-shipment-cancel'),
#             url(r'^orders/shipment/(?P<order_id>[\w-]+)$',
#                 OrderShipmentDetails.as_view(), name='shipment-order-detail'),
#             url(r'^orders/shipment/(?P<order_id>[\w-]+)/track/(?P<driver_id>[0-9]+)$',
#                 ShipmentTracking.as_view(), name='shipment-track'),
#             url(r'^orders/packandreturn$', OrderPackReturn.as_view(),
#                 name='order-pack-and-return'),
#             url(r'^orders/breakdown$', OrderBreakdown.as_view(),
#                 name='order-breakdown'),
#             url(r'^summary$', OrderSummary.as_view(),
#                 name='order-summary'),
#             # This should be moved to payment app
#             url(r'^invoice/(?P<order_id>[\w-]+)',
#                 OrderInvoice.as_view(), name='order-invoice'),
#             url(r'^order/(?P<order_id>[\w-]+)/$',
#                 OrderTrackingView.as_view(), name='track-order'),
#             url(r'^quickbook$', QuickBook.as_view(), name='quickbook'),
#             url(r'^orders/orderintegration$',
#                 OrderFlyIntegration.as_view(), name='order-fly-integration'),
#             url(r'^order/(?P<number>[\w-]+)/invoice/$',
#                 OrderInvoiceAPIView.as_view(), name='order-invoice-view'),
#             url(r'^order/document/(?P<order_number>[\w-]+)/upload$',
#                 OrderDocumentUpload.as_view(), name='order-document-upload'),
#             # added a duplicate endpoint as app calls this one for upload
#             # need to fix on app side in next release
#             url(r'^orders/documents/(?P<order_number>[\w-]+)/upload$',
#                 OrderDocumentUpload.as_view(), name='order-documents-upload'),
#             url(r'^order/heatmap$',
#                 OrderHeatmapAPIView.as_view(), name='order-heatmap'),
#             url(r'^order/heatmap-filters$',
#                 OrderHeatmapFilterAPIView.as_view(), name='order-heatmap-filter'),
#             url(r'^order/export$',
#                 ShipmentExportFileFormatAPIView.as_view(), name='shipment-order-export'),
#             url(r'^order/import$',
#                 ShipmentImportFileAPIView.as_view(), name='shipment-order-import'),
#             url(r'^order/otp/create$',
#                 OrderOtpCreate.as_view(), name='order-otp-create'),
#             url(r'^order/lineitems/import$',
#                 ShipmentOrderlineImportFileAPIView.as_view(),
#                 name='shipment-orderline-import'),
#             url(r'^fleet/report/orderreports$',
#                 OrderReport.as_view(), name='order-reports'),
#             url(r'^fleet/report/statuswisecount',
#                 FleetOrderStatus.as_view(), name='status-count'),
#             url(r'^fleet/report/codorderstatus',
#                 CODOrderStatus.as_view(), name='cod-order'),
#             url(r'^fleet/report/hubdata',
#                 HubwiseData.as_view(), name='cod-order'),
#             url(r'^fleet/report/shipmentdelivery',
#                 ShipmentDelivery.as_view(), name='shipment_delivery'),
#             url(r'^fleet/report/ordercreatedtime',
#                 OrderCreatedTime.as_view(), name='order-createdtime'),
#             url(r'^order/shipment/shipment-info$',
#                 HubwiseOrderDetailView.as_view(), name='order-hubdetails'),
#             url(r'^orders/partners/getordercount/$',
#                 GetOrderCount.as_view(), name='get-ordercount'),
#             url(r'^orders/shipmentorder/createblankettrip/$',
#                 BlanketTripShipmentOrder.as_view(), name='create-blankettrip'),
#             url(r'^order/(?P<number>[\w-]+)/get-driver-details$',
#                 DriverRating.as_view(), name='get-rating'),
#             url(r'^shipment/order/(?P<number>[\w-]+)/get-estimated-arrival$',
#                 EstimateTimeOfArrival.as_view(), name='get-estimate-time'),
#             url(r'^orders/shipment/(?P<number>[\w-]+)/get-pods',
#                 OrderDocumentsList.as_view(), name='get-order-documents'),
#             url(r'orders/customer/authtoken',
#                 CustomerAuthTokenView.as_view(), name='customer-auth-token'),
#             #Unicommerce
#             url(r'^orders/unicom/authToken', CustomerAuthToken.as_view(),
#                 name='customer-token-unicom'),
#             url(r'^orders/unicom/waybill$', UnicomOrderShipment.as_view(),
#                 name='order-shipment-unicom'),
#             url(r'^orders/unicom/waybillDetails', UnicomOrderShipment.as_view(),
#                 name='order-shipment-unicom-status'),
#
#             #customer dash board - Advance AWB generation
#             url(r'^orders/advanceawb/$',
#                 AdvanceAwb.as_view(), name='generate-advance-awb'),
#         ]
#         return self.post_process_urls(urlpatterns)


from django.apps import AppConfig


class OrderConfig(AppConfig):
    name = 'blowhorn.order'
    verbose_name = "Order"

    def ready(self):
        from blowhorn.order import signals
