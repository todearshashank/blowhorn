# System and Django libraries
import logging
from datetime import datetime, timedelta

from django.conf import settings
from django.db.models import Count
from django.utils import timezone

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model

# Blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from .order_transition_conditions import is_qualified_for_expiry
from .models import OrderConstants
from blowhorn.trip.models import Trip
from blowhorn.users.models import User

Order = get_model('order', 'Order')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def expire_order():
    """
    this fucntion is used to set orders to expired status if a order
    is in new status and only no-show trips exist
    for the order which are older than DAYS_OLDER_EXPIRE_ORDER(default 7) days
    """
    logger.info('Start expire_order()')

    trip_status_to_expire = [StatusPipeline.TRIP_NO_SHOW, StatusPipeline.TRIP_NEW,
                             StatusPipeline.TRIP_SCHEDULED_OFF,
                             StatusPipeline.TRIP_CANCELLED, StatusPipeline.TRIP_SUSPENDED]
    valid_trip_status = [StatusPipeline.TRIP_COMPLETED, StatusPipeline.DRIVER_ACCEPTED,
                         StatusPipeline.TRIP_IN_PROGRESS
                         ]

    now = timezone.now().date()
    days_to_expire = OrderConstants().get('DAYS_OLDER_EXPIRE_ORDER', 2)
    end = now - timedelta(days=days_to_expire)
    start = end - timedelta(days=2)

    ids = Order.objects.filter(pickup_datetime__range=(start, end),
                               trip__status__in=valid_trip_status). \
        values_list('id', flat=True)

    """
    Expire order having trips only in status New, No-Show, Suspended, Cancelled, Scheduled-Off
    """
    Order.objects.filter(pickup_datetime__range=(start, end),
                         trip__status__in=trip_status_to_expire).exclude(id__in=ids). \
        update(status=StatusPipeline.ORDER_EXPIRED,
               modified_date=timezone.now(),
               modified_by=User.objects.filter(
                   email=settings.CRON_JOB_DEFAULT_USER
               ).first())

    logger.info('Completed expire_order()')
