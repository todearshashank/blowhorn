# from django.conf import settings
#
# from elasticsearch_dsl import analyzer, tokenizer
# from django_elasticsearch_dsl import (
#     Document,
#     fields,
#     Index,
# )
# from django_elasticsearch_dsl.registries import registry
#
# from .models import Order
# from blowhorn.customer.models import Customer
# from blowhorn.driver.models import Driver
#
# # INDEX = Index(settings.ELASTICSEARCH_INDEX_NAMES[__name__])
# #
# # # See Elasticsearch Indices API reference for available settings
# # INDEX.settings(
# #     number_of_shards=1,
# #     number_of_replicas=0,
# #     max_ngram_diff=20
# # )
#
# text_strip = analyzer(
#     'text_strip',
#     tokenizer=tokenizer('ngram', type='ngram', min_gram=4, max_gram=20),
#     filter=["lowercase", "stop", "snowball"],
# )
#
# if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
#     @registry.register_document
#     # @INDEX.doc_type
#     class OrderDocument(Document):
#         """City Elasticsearch document.
#
#         This document has been created purely for testing out complex fields.
#         """
#
#         # ID
#         id = fields.IntegerField(attr='id')
#
#         # ********************************************************************
#         # ********************** Main data fields for search *****************
#         # ********************************************************************
#
#         reference_number = fields.TextField()
#
#         customer_reference_number = fields.TextField()
#
#         # ********************************************************************
#         # ************** Nested fields for search and filtering **************
#         # ********************************************************************
#
#         # Contract object
#         customer_contract = fields.ObjectField(properties={
#             'name': fields.TextField(analyzer=text_strip),
#             'pk': fields.IntegerField(),
#             'city': fields.ObjectField(properties={
#                 'name': fields.TextField(analyzer=text_strip),
#                 'pk': fields.IntegerField(),
#             }),
#         })
#
#         customer = fields.ObjectField(properties={
#             'name': fields.TextField(analyzer=text_strip),
#             'pk': fields.IntegerField(),
#         })
#
#         hub = fields.ObjectField(properties={
#             'name': fields.TextField(analyzer=text_strip),
#             'pk': fields.IntegerField(),
#         })
#
#         pickup_hub = fields.ObjectField(properties={
#             'name': fields.TextField(analyzer=text_strip),
#             'pk': fields.IntegerField(),
#         })
#
#         driver = fields.ObjectField(properties={
#             'name': fields.TextField(analyzer=text_strip),
#             'driver_vehicle': fields.TextField(analyzer=text_strip),
#             'pk': fields.IntegerField(),
#         })
#
#         # location = fields.GeoPointField(attr='location_field_indexing')
#
#         # ********************************************************************
#         # ********** Other complex fields for search and filtering ***********
#         # ********************************************************************
#
#         # boolean_list = fields.ListField(
#         #     StringField(attr='boolean_list_indexing')
#         # )
#         #
#         # datetime_list = fields.ListField(
#         #     StringField(attr='datetime_list_indexing')
#         # )
#         # float_list = fields.ListField(
#         #     StringField(attr='float_list_indexing')
#         # )
#         # integer_list = fields.ListField(
#         #     StringField(attr='integer_list_indexing')
#         # )
#
#         class Index:
#             # Name of the Elasticsearch index
#             name = 'order_' + settings.ELASTICSEARCH_INDEX_POSTFIX
#             # See Elasticsearch Indices API reference for available settings
#             settings = {'number_of_shards': 1,
#                         'number_of_replicas': 0,
#                         'index.max_ngram_diff': 20}
#
#         class Django:
#             """Meta options."""
#
#             model = Order  # The model associate with this Document
#             parallel_indexing = True
#             fields = [
#                 'number',
#                 # 'reference_number',
#                 # 'status',
#                 # 'customer_reference_number',
#             ]
#             # related_models = [Customer, Driver]  # Optional: to ensure the Car will be re-saved when Manufacturer or Ad is updated
#
#
#         # def get_queryset(self):
#         #     """Not mandatory but to improve performance we can select related in one sql request"""
#         #     return super(ContractDocument, self).get_queryset().select_related(
#         #         'manufacturer'
#         #     )
#         #
#         def get_instances_from_related(self, related_instance):
#             """If related_models is set, define how to retrieve the order instance(s) from the related model.
#             The related_models option should be used with caution because it can lead in the index
#             to the updating of a lot of items.
#             """
#             if isinstance(related_instance, Customer):
#                 return related_instance.order_set.all()
#             elif isinstance(related_instance, Driver):
#                 return related_instance.order_set.all()
