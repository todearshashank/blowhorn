from rest_framework import serializers

from blowhorn.order.models import OrderLine


class OrderLineSerializer(serializers.ModelSerializer):
    sku = serializers.SerializerMethodField(read_only=True)
    kit_sku_name = serializers.SerializerMethodField(read_only=True)
    kit_sku_qty = serializers.SerializerMethodField(read_only=True)
    order_number = serializers.SerializerMethodField()
    reference_number = serializers.SerializerMethodField()
    manifest_status = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(OrderLineSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    def get_sku(self, obj):
        if obj.sku:
            return obj.sku.name
        elif obj.wh_sku:
            return obj.wh_sku.name
        return None

    def get_kit_sku_name(self, obj):
        return obj.wh_kit_sku.name if obj.wh_kit_sku else None

    def get_kit_sku_qty(self, obj):
        return obj.wh_kit_sku.quantity if obj.wh_kit_sku else None

    def get_order_number(self, obj):
        return '%s' % obj.order

    def get_reference_number(self, obj):
        return obj.order.reference_number if obj.order else ''

    def get_total_item_price(self, obj):
        return float(obj.total_item_price)

    def get_each_item_price(self, obj):
        return float(obj.each_item_price)

    def get_manifest_status(self, obj):
        if obj.wmsmanifestline_set.exists():
            manifest_line = obj.wmsmanifestline_set.first()
            return manifest_line and manifest_line.manifest.status

        return ''

    class Meta:
        model = OrderLine
