from rest_framework import serializers
from blowhorn.order.models import Labour


class LabourSerializer(serializers.ModelSerializer):
    class Meta:
        model = Labour
        fields = '__all__'
