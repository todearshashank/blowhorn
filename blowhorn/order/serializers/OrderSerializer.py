from blowhorn.order.serializers.order_line import OrderLineSerializer
import os
import logging
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils import timezone
from config.settings import status_pipelines as StatusPipeline

from rest_framework import serializers, status
from rest_framework.response import Response
from blowhorn.oscarapi.serializers.utils import OscarModelSerializer
from blowhorn.oscar.core.loading import get_model

from blowhorn.freshchat.whatsapp import send_whatsapp_notification
from blowhorn.order.serializers.ShippingAddressSerializer import (
    ShippingAddressSerializer
)
from blowhorn.common import sms_templates
from blowhorn.oscar.core.loading import get_model
from blowhorn.contract.constants import CONTRACT_TYPE_KIOSK
from blowhorn.order.mixins import OrderPlacementMixin
from blowhorn.order.utils import OrderUtil
from blowhorn.order.tasks import (send_status_notification, send_c2c_slack_notification,
    broadcast_booking_updates_admin, broadcast_booking_updates_customer)
from blowhorn.order.const import STATUS_UPDATE, ORDER_UPDATES_FIREBASE_PATH
from blowhorn.customer.serializers import FleetShipmentOrderListSerializer
from blowhorn.customer.utils import CustomerUtils
from blowhorn.utils.functions import get_tracking_url, utc_to_ist
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Driver = get_model('driver', 'Driver')
Labour = get_model('order', 'Labour')
DriverActivity = get_model('driver', 'DriverActivity')
User = get_user_model()


class OrderSerializer(OscarModelSerializer, OrderPlacementMixin):
    queryset = Order.objects.all()
    shipping_address = ShippingAddressSerializer(
        many=False, required=False)
    pickup_address = ShippingAddressSerializer(
        many=False, required=False)
    lines = serializers.HyperlinkedIdentityField(
        view_name='order-lines-list')

    def save(self):

        request = self.context['request']
        customer = self.context.get('customer')
        user = request.user

        # for kiosk order request.user will be our staff user
        # so populating user from data
        if self.initial_data.get('user'):
            user = self.initial_data.get('user')

        if not customer:
            phone_number = request.META.get('HTTP_MOBILE')
            if user and not user.is_authenticated and phone_number:
                customer, user = CustomerUtils().get_customer_from_phone_number(
                    phone_number)
                if not customer:
                    return False
                customer.update_name(self.initial_data.get('customer_name'))
        else:
            self.initial_data['customer'] = customer
            user = customer.user
            if self.initial_data.get('customer_name', None):
                customer.update_name(self.initial_data.get('customer_name'))

        order = self.place_order(user=user, order_data=self.initial_data)
        if order.order_type == settings.C2C_ORDER_TYPE and \
                order.status == StatusPipeline.ORDER_NEW:
            track_url = '%s/track/%s' % (
                os.environ.get('HTTP_HOST', 'blowhorn.com'), order.number)
            msg_template_data = {"template_name": "booking_update",
                                 "params_data": [order.customer.name,
                                                 order.number, track_url]}
            send_whatsapp_notification([settings.ACTIVE_COUNTRY_CODE + str(order.customer.user.phone_number.national_number)],
                                       msg_template_data, order)
            if settings.ENABLE_SLACK_NOTIFICATIONS and \
                    order.customer_contract.contract_type != CONTRACT_TYPE_KIOSK:
                template_dict = sms_templates.template_booking_acceptance
                message = template_dict['text'] % (
                    order.number, get_tracking_url(request, order.number))
                user.send_sms(message, template_dict)
                # track_url = '%s/track/%s' % (
                # os.environ.get('HTTP_HOST', 'blowhorn.com'), order.number)
                # msg_template_data = {"template_name": "booking_update",
                #                      "params_data": [order.customer.name,
                #                                      order.number, track_url]}
                # send_whatsapp_notification([order.customer.user.phone_number],
                #                            msg_template_data, order)
                if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
                    """
                    COVID-19 specific sms, remove in future or make it configurable
                    """
                    template_dict = sms_templates.covid_19_declaration_customer
                    user.send_sms(template_dict['text'] % order.number, template_dict)
                send_c2c_slack_notification.apply_async((order.id,),)

            # Store UTM params
            utm_data = self.initial_data.pop('utm_data', None)
            if utm_data:
                utm_data['order'] = order
                utm_instance = self.create_utm_record(utm_data)

        return order

    def update_order(self, update_data):
        if self.is_valid():
            updatable_fields = self.Meta.updatable_fields
            for field in updatable_fields:
                if field in update_data:
                    self.validated_data[field] = update_data[field]

            if self.is_valid() and self.save():
                return True
        return False

    def create_c2c_trip_for_order(self, order, driver):
        response = self.create_trip_stops_c2c(
            order=order, driver=driver)

        return response

    def assign_to_driver(self, geopoint=None, trip_id=None):
        request = self.context['request']

        driver = Driver._default_manager.filter(user=request.user).first()
        if not driver:
            logger.error('Driver Credentials not found, LogIn Again')
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Driver Credentials not found, LogIn Again')

        return self.create_trip_stops_b2c(
            driver=driver,
            items=self.initial_data,
            geopoint=geopoint,
            trip_id=trip_id
        )

    # verify allowed statuses and then update order status
    def update_status(self, order_status, geopoint,
                      order, hub_associate=None, driver=None,
                      delivered_to=None, reason=None):

        current_status = order.status
        order_type = order.order_type
        address = None
        # if order is in one of end statuses, add dropoff address
        if order_status in StatusPipeline.ORDER_END_STATUSES:
            address = order.shipping_address
        elif order_status in [StatusPipeline.ORDER_NEW,
                              StatusPipeline.ORDER_PACKED]:
            address = order.pickup_address

        if order.status != order_status:

            if order_type == settings.C2C_ORDER_TYPE:
                driver = order.driver or driver

                OrderPlacementMixin().create_event(
                    order,
                    order_status,
                    address,
                    driver,
                    geopoint
                )
                if settings.ENABLE_SLACK_NOTIFICATIONS:
                    send_c2c_slack_notification.apply_async(
                        (order.id,),)

            elif order_type == settings.SHIPMENT_ORDER_TYPE:
                remarks = None
                if delivered_to:
                    remarks = delivered_to
                elif reason:
                    remarks = reason

                OrderPlacementMixin().create_event(
                    order,
                    order_status,
                    address,
                    order.driver if order_status != StatusPipeline.ORDER_PACKED else None,
                    geopoint,
                    hub_associate,
                    remarks
                )

        if not driver:
            driver = order.driver

        Order.objects.filter(id=order.id).update(
            status=order_status,
            updated_time=timezone.now(),
            driver=driver)

        if order.order_type == settings.C2C_ORDER_TYPE and \
            order.customer_contract.contract_type in [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]:
            broadcast_booking_updates_admin.apply_async(
                args=[order.pk, settings.FIREBASE_ADMIN_NEW_BOOKING_CHANNEL,
                      STATUS_UPDATE])

            channel_name = ORDER_UPDATES_FIREBASE_PATH % order.number
            broadcast_booking_updates_customer.apply_async(
                args=[order.pk, channel_name, STATUS_UPDATE])


        '''
        Send notification only for specific customers,
        if the order status is return or delivered
        and order type is shipment then send notification.
        '''
        if (order.customer.is_shipment_servicetype and
                order.customer.notification_url and order.customer.ecode):
            if (order_status == StatusPipeline.ORDER_DELIVERED and
                    order_type == settings.SHIPMENT_ORDER_TYPE):

                logger.info('Sending Order %s Status notification to %s',
                            order.number, order.customer.user.name)

                send_status_notification.delay(
                    order.id,
                    order.number,
                    order_status,
                    order.reference_number,
                    order.customer.notification_url,
                    order.customer.ecode,
                    order.customer.user.name
                )

        logger.info('Order "%s" State Transition from "%s" to "%s" SUCCESSFUL',
                    order, current_status, order_status)

    def get_orders(self, request, results, app=False, is_utc_time_format=False):
        """
        Order select_related Driver
        Order prefetch_related Trip
        filter_by Customer
        order_by pickup_datetime DESC
        """
        results_json = []
        for order in results:
            # @todo remove app parameter and use same dates and time format
            # in both app and website
            response_dict = OrderUtil().get_booking_details_for_customer(
                request=request,
                order=order,
                app=True,
                is_utc_time_format=is_utc_time_format
            )
            if response_dict:
                results_json.append(
                    response_dict
                )

        current = OrderPlacementMixin().get_sorted_orders(
            results_json, 'ongoing'
        )
        past = OrderPlacementMixin().get_sorted_orders(
            results_json, 'done', True
        )
        upcoming = OrderPlacementMixin().get_sorted_orders(
            results_json, 'upcoming'
        )
        _dict = {
            'current': current,
            'past': past,
            'upcoming': upcoming,
        }
        return _dict

    class Meta:
        model = Order
        fields = '__all__'
        read_only_fields = ('number', 'reference_number', 'customer')
        updatable_fields = ('status',)


class OrderCreatetionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = '__all__'


class OrderListSerializer(serializers.ModelSerializer):

    contract_id = serializers.SerializerMethodField(read_only=True)
    contract_name = serializers.SerializerMethodField(read_only=True)
    driver_id = serializers.SerializerMethodField(read_only=True)
    pickup_datetime = serializers.SerializerMethodField(read_only=True)
    date_placed = serializers.SerializerMethodField(read_only=True)
    hub_id = serializers.SerializerMethodField(read_only=True)
    hub_name = serializers.SerializerMethodField(read_only=True)

    def get_pickup_datetime(self, order):
        return utc_to_ist(order.pickup_datetime).strftime('%d-%m-%Y %H:%M') if order.pickup_datetime else ''

    def get_date_placed(self, order):
        return utc_to_ist(order.date_placed).strftime('%d-%m-%Y %H:%M')

    def get_contract_id(self, order):
        return order.customer_contract_id

    def get_contract_name(self, order):
        return str(order.customer_contract)

    def get_driver_id(self, order):
        return order.driver_id if order.driver_id else 0

    def get_hub_id(self, order):
        return order.hub_id

    def get_hub_name(self, order):
        return order.hub.name

    class Meta:
        model = Order
        fields = ('id', 'number', 'contract_id', 'contract_name', 'driver_id',
                  'hub_id', 'hub_name', 'pickup_datetime', 'date_placed')


class WmsOrderSerializer(FleetShipmentOrderListSerializer):
    shipping_address = serializers.SerializerMethodField(read_only=True)

    def get_orderlines(self, order):
        order_lines = sorted(
            order.order_lines.all(),
            key=lambda x: x.item_sequence_no or 0)
        return OrderLineSerializer(order_lines, many=True).data

    def get_shipping_address(self, obj):
        address = {
            'line1': obj.shipping_address.line1,
            'line3': obj.shipping_address.line3,
            'line4': obj.shipping_address.line4,
            'postcode': obj.shipping_address.postcode
        }
        contact = {
            'mobile': str(obj.shipping_address.phone_number),
            'name': obj.shipping_address.first_name
        }
        geopoint = {
            'latitude': obj.shipping_address.geopoint.x if obj.shipping_address.geopoint else None,
            'longitude': obj.shipping_address.geopoint.y if obj.shipping_address.geopoint else None
        }
        shipping_details = {
            'address': address,
            'contact': contact,
            'geopoint': geopoint
        }
        return shipping_details

    class Meta:
        model = Order
        fields = [
            'id', 'number', 'reference_number', 'customer_reference_number',
            'driver', 'date_placed', 'delivered_time', 'return_order',
            'is_offline_order', 'pod_files', 'city_name',
            'cash_on_delivery', 'customer', 'trip_number',
            'expected_delivery_time_start', 'expected_delivery_time_end', 'otp',
            'expected_pickup_time', 'shipping_address',
            'batch', 'num_of_attempts', 'payment_mode', 'amount_paid_online',
            'cash_collected', 'orderlines', 'picked_time', 'driver_rating',
            'allow_assign_driver', 'is_valid_lineedit_status',
            'allocation_status', 'status',
        ]


class ManifestOrderSerializer(serializers.ModelSerializer):

    orderline_data = serializers.SerializerMethodField()

    def get_orderline_data(self, obj):
        inv_details = []
        sku_details = []
        for line in obj.order_lines.all():
            line_data = {
                'manifest_id': line.wmsmanifestline_set.first().manifest.id
                if line.wmsmanifestline_set.exists() else None,
                'manifest_status': line.wmsmanifestline_set.first().manifest.status
                if line.wmsmanifestline_set.exists() else None,
                'site_id': line.wmsmanifestline_set.first().manifest.site.id
                if line.wmsmanifestline_set.exists() else None,
                'site_name': line.wmsmanifestline_set.first().manifest.site.name
                if line.wmsmanifestline_set.exists() else None,
                'item_name': line.wh_sku.name if line.wh_sku else '',
                'tracking_level': line.tracking_level,
                'qty': line.quantity}
            inv_details.append(line_data)
        sku_details.append({'inv_details': inv_details})

        address = {
            'line1': obj.shipping_address.line1,
            'line3': obj.shipping_address.line3,
            'line4': obj.shipping_address.line4,
            'postcode': obj.shipping_address.postcode
        }
        contact = {
            'mobile': str(obj.shipping_address.phone_number),
            'name': obj.shipping_address.first_name
        }
        geopoint = {
            'latitude': obj.shipping_address.geopoint.x if obj.shipping_address.geopoint else None,
            'longitude': obj.shipping_address.geopoint.y if obj.shipping_address.geopoint else None
        }
        shipping_details = {
            'address': address,
            'contact': contact,
            'geopoint': geopoint
        }

        data = {'sku_details': sku_details, 'shipping_details': shipping_details, 'order_id': obj.id,
                'number': obj.number, 'reference_number': obj.reference_number,
                'date_placed': obj.date_placed.isoformat(), 'payment_mode': obj.payment_mode,
                'cash_on_delivery': obj.cash_on_delivery, 'delivery_instructions': obj.delivery_instructions}

        return data

    class Meta:
        model = Order
        fields = ('orderline_data', )
