import re
import json
from math import floor

import phonenumbers
from django.utils import timezone
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.translation import gettext as _

from rest_framework import serializers, exceptions

from blowhorn.common.helper import CommonHelper
from blowhorn.address.models import Hub, Slots
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL
from blowhorn.order.const import PAYMENT_MODE_CASH, \
    RESIDENTIAL_ADDRESS, COMMERCIAL_ADDRESS, CARRIER_MODE
from blowhorn.order.services.geofencing import get_radial_distance
from blowhorn.vehicle.models import VehicleClass
from config.settings import status_pipelines as StatusPipeline
from blowhorn.oscarapi.serializers.checkout import BillingAddressSerializer
from blowhorn.oscar.core.loading import get_model
from blowhorn.order.serializers.ShippingAddressSerializer import (
    ShippingAddressSerializer
)
from blowhorn.order.mixins import OrderPlacementMixin
from blowhorn.address.utils import get_hub_from_pincode, get_hub_from_store_code
from blowhorn.oscarapi.utils.settings import overridable
from phonenumber_field.phonenumber import PhoneNumber
from blowhorn.order.tasks import send_slack_notification
from datetime import timedelta
from config.settings.status_pipelines import ORDER_STATUSES, ORDER_NEW, DRIVER_ASSIGNED, REACHED_AT_HUB, DRIVER_ACCEPTED

ShippingAddress = get_model('order', 'ShippingAddress')
BillingAddress = get_model('order', 'BillingAddress')
Order = get_model('order', 'Order')
# OrderLine = get_model('order', 'Line')
# OrderLineAttribute = get_model('order', 'LineAttribute')
# Basket = get_model('basket', 'Basket')
# Country = get_model('address', 'Country')
# Repository = get_class('shipping.repository', 'Repository')
Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Driver = get_model('driver', 'Driver')
Labour = get_model('order', 'Labour')
User = get_user_model()


class OrderShipmentSerializer(serializers.Serializer, OrderPlacementMixin):

    queryset = Order.objects.filter(
        order_type=settings.SHIPMENT_ORDER_TYPE
    ).exclude(
        status=StatusPipeline.ORDER_DELIVERED
    )

    total = serializers.DecimalField(
        decimal_places=2, max_digits=12, required=False)
    shipping_address = ShippingAddressSerializer(many=False, required=False)
    billing_address = BillingAddressSerializer(many=False, required=False)
    # pickup_address = ShippingAddressSerializer(
    #     many=False, required=False)
    lines = serializers.HyperlinkedIdentityField(
        view_name='order-lines-list')

    def validate(self, attrs):
        request = self.context['request']
        data = request.data
        valid_parameters = ['awb_number', 'customer', 'customer_email',
                            'customer_mobile', 'alternate_customer_mobile',
                            'customer_name', 'delivery_address',
                            "total_payable", "customer_id", "source_city",
                            'delivery_lat', 'delivery_lon', 'store_code',
                            'basket_id', 'division', 'pickup_hub_', 'delivery_hub_',
                            'delivery_postal_code', 'expected_delivery_time',
                            'slot_start_date_time', 'slot_end_date_time',
                            'cash_on_delivery', 'pickup_customer_name', 'pickup_customer_mobile',
                            'order_creation_time', 'reference_number', "city",
                            'is_offline_order', "pickup_hub", "delivery_hub",
                            'shipment_info', 'csv_upload', 'pickedup_address',
                            'pickup_lat', 'pickup_lon', 'pickup_postal_code',
                            'items', 'customer_contract', 'alternate_mobile',
                            'is_cod', 'pickup_address', 'otp', 'has_pickup',
                            "bill_number", "bill_date", "order_mode", "status",
                            'use_customer_hub', "remarks", "pickup_datetime",
                            "payment_mode", "device_type", "status", "commercial_class",
                            "weight", "volume", "delivery_instructions",
                            "length","breadth","height","priority", "is_commercial_address",
                            "is_return_order", "customer_reference_number", "source",
                            "delivery_mode", "currency_code", "shipping_gstin",
                            "pickup_gstin", "is_secondary_region", "carrier_mode",
                            "shipping_charges", "tax", "total_tax_inclusive_amount",
                            "total_tax_exclusive_amount", "discount", "tip", "pin_number",
                            "is_hyperlocal", "contract_type"
                            ]

        # Raise exception if any extra params present in data
        for param in data:
            if param not in valid_parameters:
                message = _('%s is not a valid parameter' % param)
                _out = {
                    'status': 'FAIL',
                    'message': message
                }
                raise exceptions.ParseError(_out)

        is_commercial_address = data.get('is_commercial_address',False)
        if is_commercial_address and is_commercial_address not in ['True','False']:
            message = _('The value for is_commercial_address paramter should be True/False '
                        'or blank for defualt residential address type'
                    )
            _out = {
                'status': 'FAIL',
                'message': message
            }
            raise exceptions.ParseError(_out)

        address_type = RESIDENTIAL_ADDRESS
        if is_commercial_address and is_commercial_address == 'True':
            address_type = COMMERCIAL_ADDRESS

        if data.get('is_cod', False):
            try:
                if not float(data.get('cash_on_delivery', None)):
                    _out = {
                        'status': 'FAIL',
                        'message': _('Enter cod amount greater than 0')
                    }
                    raise exceptions.ParseError(_out)

            except (ValueError, TypeError):
                _out = {
                    'status': 'FAIL',
                    'message': _('Enter the valid amount to be collected at '
                                 'the time of delivery')
                }
                raise exceptions.ParseError(_out)

        return_order = data.get('is_return_order', False)
        customer = data.get('customer', None)
        use_customer_hub = data.get('use_customer_hub', False)
        reference_number = data.get('reference_number', None)
        contract_type = data.pop('contract_type', None)
        if not reference_number:
            _out = {
                'status': 'FAIL',
                'message': _('Shipment Reference Number is required')
            }
            raise exceptions.ParseError(_out)

        regex = re.compile("[@!$%^&*()<>?+=_/|}{~`':;\n ]")
        if regex.search(reference_number):
            _out = {
                'status': 'FAIL',
                'message': "Invalid reference number. Only alpha numeric, #, - are  allowed"
            }
            raise exceptions.ParseError(_out)
        order_qs = Order.objects.filter(
            customer=customer,
            reference_number=reference_number
        )
        if return_order:
            order_qs = order_qs.exclude(
                Q(status__in=StatusPipeline.ORDER_END_STATUSES) & ~Q(
                    return_order=True))

        if order_qs.exists():
            _out = {
                'status': 'FAIL',
                'message': {
                    'awb_number': order_qs.first().number
                }
            }
            raise exceptions.ParseError(_out)

        shipment_info = {}
        customer_name = data.get('customer_name', None)
        pickup_customer_name = data.get('pickup_customer_name', None)
        if customer_name:
            shipment_info['customer_name'] = customer_name

        # customer mobile validation
        customer_mobile = data.get('customer_mobile', None)
        if not customer_mobile:
            _out = {
                'status': 'FAIL',
                'message': _('Customer mobile number is mandatory')
            }
            raise exceptions.ParseError(_out)

        try:
            customer_phone = PhoneNumber.from_string(
                customer_mobile, settings.COUNTRY_CODE_A2)
            if not customer_phone.is_valid():
                _out = {
                    'status': 'FAIL',
                    'message': _('Invalid customer mobile number')
                }
                raise exceptions.ParseError(_out)
        except phonenumbers.NumberParseException:
            _out = {
                'status': 'FAIL',
                'message': _('Invalid customer mobile number')
            }
            raise exceptions.ParseError(_out)
        shipment_info['customer_mobile'] = customer_mobile

        # alternate customer mobile validation
        alternate_customer_mobile = data.get('alternate_customer_mobile', None)
        alternate_customer_phone = None
        if alternate_customer_mobile:
            try:
                alternate_customer_phone = PhoneNumber.from_string(
                    alternate_customer_mobile, settings.COUNTRY_CODE_A2)
                if not alternate_customer_phone.is_valid():
                    _out = {
                        'status': 'FAIL',
                        'message': _('Invalid customer alternate mobile number')
                    }
                    raise exceptions.ParseError(_out)
            except phonenumbers.NumberParseException:
                _out = {
                    'status': 'FAIL',
                    'message': _('Invalid alternate customer mobile number')
                }
                raise exceptions.ParseError(_out)
            shipment_info['alternate_customer_mobile'] = alternate_customer_mobile

        # pickup customer mobile validation
        pickup_customer_mobile = data.get('pickup_customer_mobile', None)
        pickup_customer_phone = None
        if pickup_customer_mobile:
            try:
                pickup_customer_phone = PhoneNumber.from_string(
                    pickup_customer_mobile, settings.COUNTRY_CODE_A2)
                if not pickup_customer_phone.is_valid():
                    _out = {
                        'status': 'FAIL',
                        'message': _('Invalid pickup customer mobile number')
                    }
                    raise exceptions.ParseError(_out)
            except phonenumbers.NumberParseException:
                _out = {
                    'status': 'FAIL',
                    'message': _('Invalid pickup customer mobile number')
                }
                raise exceptions.ParseError(_out)

        pin_number = data.get('pin_number', None)
        if customer.has_call_masking_pin and not pin_number:
            _out = {
                'status': 'FAIL',
                'message': _('pin_number is mandatory')
            }
            raise exceptions.ParseError(_out)
        if pin_number:
            if not re.search(r'^[0-9]*$', str(pin_number)):
                _out = {
                    'status': 'FAIL',
                    'message': _('pin_number should only contain 0-9 digits')
                }
                raise exceptions.ParseError(_out)
        shipment_info['pin_number'] = pin_number
        # customer email
        customer_email = data.get('customer_email', None)
        if customer_email:
            customer_email = customer_email.strip()
            if not re.search(settings.EMAIL_REGEX, customer_email):
                _out = {
                    'status': 'FAIL',
                    'message': _('Invalid customer email')
                }
                raise exceptions.ParseError(_out)
            shipment_info['customer_email'] = customer_email

        shipping_address = data.get('delivery_address')
        shipping_postal_code = data.get('delivery_postal_code')
        if not shipping_address or not shipping_postal_code:
            _out = {
                'status': 'FAIL',
                'message': _('Delivery address / delivery postal code is'
                             'missing')
            }
            raise exceptions.ParseError(_out)

        country_code = data.get('country_code', settings.COUNTRY_CODE_A2)
        carrier_mode = data.get('carrier_mode', None)
        if carrier_mode and carrier_mode not in CARRIER_MODE:
            _out = {
                 'status': 'FAIL',
                 'message': _('The value for carrier_mode parameter should be Blowhorn Surface or Blowhorn Air or Blowhorn Heavy')
            }
            raise exceptions.ParseError(_out)

        country_code = data.get(
            'country_code', settings.COUNTRY_CODE_A2)
        POSTAL_CODE_REGEX = ShippingAddress.POSTCODES_REGEX.get(country_code)
        if not re.search(POSTAL_CODE_REGEX, shipping_postal_code):
            _out = {
                'status': 'FAIL',
                'message': _('Invalid delivery postal code')
            }
            raise exceptions.ParseError(_out)

        shipping_lat = data.get('delivery_lat')
        shipping_lon = data.get('delivery_lon')
        store_code = data.get('store_code')
        hub = None
        if store_code:
            hub = get_hub_from_store_code(store_code, customer=customer,
                                          pincode=shipping_postal_code)

        # 1. COVERAGE CHECK
        pickup_hub_ = data.get('pickup_hub_')
        is_secondary_region = data.get('is_secondary_region', False)

        # Check for coverage iff pickup-hub && location points are present
        if pickup_hub_ and shipping_lat and shipping_lon:
            shipping_geopt = CommonHelper.get_geopoint_from_latlong(shipping_lat, shipping_lon)
            """
            Throw error iff,
               1. Coverage is defined for hub
               2. Shipping location is out of polygon
            """
            if pickup_hub_ and pickup_hub_.coverage and not is_secondary_region and \
                not pickup_hub_.coverage.contains(shipping_geopt):
                _out = {
                    'status': 'FAIL',
                    'message': _('{} doesn\'t provide service in this area'.format(settings.BRAND_NAME))
                }
                raise exceptions.ParseError(_out)

            if pickup_hub_ and pickup_hub_.extended_coverage and is_secondary_region and \
                not pickup_hub_.extended_coverage.contains(shipping_geopt):
                _out = {
                    'status': 'FAIL',
                    'message': _('{} doesn\'t provide service in this area'.format(settings.BRAND_NAME))
                }
                raise exceptions.ParseError(_out)

            hub = pickup_hub_ if not pickup_hub_.is_wms_site else None
            if pickup_hub_.is_wms_site:
                attrs['has_pickup'] = True

        slot = Slots.objects.filter(hub=pickup_hub_).first()
        # 2. RADIUS CHECK
        if slot and slot.radius_based_order:
            distance = get_radial_distance(data.get('pickup_lat'), data.get('pickup_lon'),
                                           data.get('delivery_lat'), data.get('delivery_lon'))

            if slot.radius >= floor(distance):
                hub = pickup_hub_
                data.update({"allow_order_creation": True if hub else False})

        # 3. PINCODE CHECK
        if not slot or (slot and not slot.radius_based_order):
            if not store_code and not hub:
                if use_customer_hub:
                    hub = get_hub_from_pincode(shipping_postal_code, customer=customer)
                else:
                    hub = get_hub_from_pincode(shipping_postal_code)

        if not hub:
            _out = {
                'status': 'FAIL',
                'message': _('{} doesn\'t provide service in this area'.format(settings.BRAND_NAME))
            }
            raise exceptions.ParseError(_out)

        exp_delivery_start_time = data.get('slot_start_date_time')
        if exp_delivery_start_time:
            try:
                exp_delivery_start_time = self.get_aware_datetime(
                    exp_delivery_start_time)
            except:
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid slot start date time format'
                }
                raise exceptions.ParseError(_out)

            if exp_delivery_start_time < timezone.now():
                _out = {
                    'status': 'FAIL',
                    'message': 'slot start time cannot be a past date time'
                }
                raise exceptions.ParseError(_out)

        if (data.get('slot_start_date_time') and not
            data.get('slot_end_date_time')) or \
            (not data.get('slot_start_date_time') and
             data.get('slot_end_date_time')):
            _out = {
                'status': 'FAIL',
                'message': 'Value for both slot_start_date_time and '
                           'slot_end_date_time should be given'
                }
            raise exceptions.ParseError(_out)

        expected_delivery_time = data.get('slot_end_date_time') or \
                                 data.get('expected_delivery_time')
        if expected_delivery_time:
            try:
                expected_delivery_time = self.get_aware_datetime(
                    expected_delivery_time)
            except:
                if data.get('slot_end_date_time'):
                    _out = {
                        'status': 'FAIL',
                        'message': 'Invalid slot end date time format'
                    }
                else:
                    _out = {
                        'status': 'FAIL',
                        'message': 'Invalid expected delivery date time format'
                    }
                raise exceptions.ParseError(_out)

            if expected_delivery_time < timezone.now():
                if data.get('slot_end_date_time'):
                    _out = {
                        'status': 'FAIL',
                        'message': 'slot end time cannot be a past date time'
                    }
                else:
                    _out = {
                        'status': 'FAIL',
                        'message': 'Expected delivery time cannot be a past '
                                   'date time'
                    }
                raise exceptions.ParseError(_out)

        if exp_delivery_start_time and expected_delivery_time and \
                exp_delivery_start_time >= expected_delivery_time:
            _out = {
                'status': 'FAIL',
                'message': 'slot_start_date_time cannot be greater than or '
                           'equal to slot_end_date_time'
            }
            raise exceptions.ParseError(_out)

        pickup_datetime = data.get('pickup_datetime')
        if pickup_datetime:
            try:
                pickup_datetime = self.get_aware_datetime(
                    pickup_datetime)
            except:
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid order creation date time format'
                }
                raise exceptions.ParseError(_out)

            time_diff = (timezone.now() - pickup_datetime).total_seconds()

            if time_diff > 0:
                _out = {
                    'status': 'FAIL',
                    'message': 'Pickup date cannot be a past date time'
                }
                raise exceptions.ParseError(_out)

        order_creation_time = data.get('order_creation_time')
        if order_creation_time:
            try:
                order_creation_time = self.get_aware_datetime(
                    order_creation_time)
            except:
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid order creation date time format'
                }
                raise exceptions.ParseError(_out)

            time_diff = (timezone.now() - order_creation_time).total_seconds()
            if time_diff > settings.ORDER_CREATION_PAST_TIME_TOLERANCE:
                _out = {
                    'status': 'FAIL',
                    'message': 'Order creation time cannot be a past date time'
                }
                raise exceptions.ParseError(_out)

            if time_diff < 0:
                _out = {
                    'status': 'FAIL',
                    'message': 'Order creation time cannot be a future date time'
                }
                raise exceptions.ParseError(_out)

        attrs['status'] = data.get('status', StatusPipeline.ORDER_NEW)
        vehicle_class = VehicleClass.objects.filter(commercial_classification=data.get('commercial_class')).first()
        if vehicle_class:
            attrs['vehicle_class_preference'] = vehicle_class
        attrs['total_payable'] = data.get('total_payable', 0)
        attrs['source_city'] = data.get('source_city', None)
        attrs['shipping_address'] = self.get_updated_address(
            shipping_address,
            shipping_postal_code,
            shipping_lat,
            shipping_lon,
            reference_number=reference_number,
            customer_name=customer_name,
            customer_contact=customer_phone,
            alternate_customer_contact=alternate_customer_phone,
            pin_number=str(pin_number)
        )

        pickup_hub = None

        if data.get('has_pickup', None):
            attrs['has_pickup'] = data.get('has_pickup')

        if data.get('division', None):
            attrs['division'] = data.get('division')

        if data.get('csv_upload'):
            attrs['items'] = data.get('items').split(',')
            if not data.get('pickup_address') or data.get('pickup_postal_code'):
                attrs['pickup_address'] = {
                    'line1': hub.address.line1,
                    'postcode': hub.address.postcode,
                }
            else:
                attrs['pickup_address'] = {
                    'line1': data.get('pickup_address'),
                    'postcode': data.get('pickup_postal_code'),
                }
            attrs['waybill_number'] = data.get('waybill_number')
            attrs['shipment_info'] = json.dumps(data.get('shipment_info'))
        else:
            pickup_address = data.get('pickedup_address', None)
            pickup_postal_code = data.get('pickup_postal_code')

            if (pickup_address and not pickup_postal_code) or \
                (not pickup_address and pickup_postal_code):
                _out = {
                    'status': 'FAIL',
                    'message': 'Pickup address / pickup postal code is missing'
                }
                raise exceptions.ParseError(_out)

            if pickup_address and pickup_postal_code:
                if not re.search(POSTAL_CODE_REGEX, pickup_postal_code):
                    _out = {
                        'status': 'FAIL',
                        'message': 'Invalid pickup postal code'
                    }
                    raise exceptions.ParseError(_out)

                pickup_hub = get_hub_from_pincode(pickup_postal_code,
                                                  customer=customer) if pickup_postal_code else None
                pickup_hub = get_hub_from_pincode(pickup_postal_code) if not pickup_hub else pickup_hub

                if data.get('has_pickup') and not data.get('pickup_hub_'):
                    if contract_type == CONTRACT_TYPE_HYPERLOCAL:
                        if use_customer_hub:
                            pickup_hub = get_hub_from_pincode(
                                pickup_postal_code, customer=customer)
                        else:
                            pickup_hub = get_hub_from_pincode(pickup_postal_code)
                    else:
                        pickup_hub = get_hub_from_pincode(pickup_postal_code, customer=customer)
                        pickup_hub = get_hub_from_pincode(pickup_postal_code) if not pickup_hub else pickup_hub

                    if not pickup_hub:
                        _out = {
                            'status': 'FAIL',
                            'message': '{} doesn\'t provide service in'
                                       ' this area'.format(settings.BRAND_NAME)
                        }
                        raise exceptions.ParseError(_out)

                pickup_lat = data.get('pickup_lat')
                pickup_lon = data.get('pickup_lon')

                alternate_pickup_phone = None
                if return_order and alternate_customer_mobile:
                    alternate_pickup_phone = PhoneNumber.from_string(
                                        alternate_customer_mobile, settings.COUNTRY_CODE_A2)

                attrs['pickup_address'] = self.get_updated_address(
                    pickup_address,
                    pickup_postal_code,
                    pickup_lat,
                    pickup_lon,
                    customer_name=pickup_customer_name,
                    customer_contact=pickup_customer_phone,
                    alternate_customer_contact=alternate_pickup_phone,
                    pin_number=str(pin_number)
                )

            else:

                attrs['shipment_info'] = shipment_info
                attrs['pickup_address'] = {
                    'line1': hub.address.line1,
                    'postcode': hub.address.postcode,
                }

                if pickup_customer_name:
                    attrs['pickup_address']['first_name'] = pickup_customer_name

                if pickup_customer_phone:
                    attrs['pickup_address']['phone_number'] = pickup_customer_phone

                if hub.address.geopoint:
                    coords = hub.address.geopoint.coords
                    attrs['pickup_address'].update({
                        'latitude': coords[1],
                        'longitude': coords[0]
                    })

        number = data.get('awb_number')
        if number:
            attrs['order_number'] = number

        weight = data.get('weight', 0.00) or 0.00
        volume = data.get('volume', 0.00) or 0.00
        length = data.get('length', 0.00) or 0.00
        breadth = data.get('breadth', 0.00) or 0.00
        height = data.get('height', 0.00) or 0.00
        priority = data.get('priority', None) or None

        if priority:
            priority = self.number_validation('priority', priority, int)

        dimension_data = self.dimension_validation(weight, volume, length, breadth, height)
        attrs.update(dimension_data)

        attrs['hub'] = data.get('delivery_hub_') or hub
        attrs['address_type'] = address_type
        attrs['payment_mode'] = data.get('payment_mode', PAYMENT_MODE_CASH)
        attrs['device_type'] = data.get('device_type', None)
        attrs['pickup_hub'] = data.get('pickup_hub_') or pickup_hub
        attrs['bill_number'] = data.get('bill_number', None)
        attrs['remarks'] = data.get('remarks', None)
        attrs['is_offline_order'] = data.get('is_offline_order', False)
        attrs['bill_date'] = data.get('bill_date', None)
        attrs['store_code'] = data.get('store_code', None)
        attrs['order_mode'] = data.get('order_mode', None)
        attrs['source'] = data.get('source', 'api')
        attrs['reference_number'] = reference_number
        attrs['customer_reference_number'] = data.get('customer_reference_number')
        attrs['order_type'] = settings.SHIPMENT_ORDER_TYPE
        attrs['customer'] = customer
        attrs['customer_contract'] = data.get('customer_contract')
        attrs['expected_delivery_time'] = expected_delivery_time or None
        attrs['exp_delivery_start_time'] = exp_delivery_start_time
        attrs['pickup_datetime'] = pickup_datetime if pickup_datetime else None
        attrs['date_placed'] = order_creation_time or timezone.now()
        attrs['cash_on_delivery'] = data['cash_on_delivery'] if data.get(
            'cash_on_delivery', None) else 0.00
        attrs['otp'] = data.get('otp')
        attrs['delivery_instructions'] = data.get('delivery_instructions','')
        attrs['priority'] = priority
        attrs['use_customer_hub'] = data.get('use_customer_hub', False)
        attrs['return_order'] = return_order
        attrs['delivery_mode'] = data.get('delivery_mode')
        attrs['currency_code'] = data.get('currency_code')
        attrs['shipping_gstin'] = data.get('shipping_gstin', None)
        attrs['pickup_gstin'] = data.get('pickup_gstin', None)
        attrs['carrier_mode'] = carrier_mode
        attrs['shipping_charges'] = data.get('shipping_charges', 0)
        attrs['tax'] = data.get('tax', 0)
        attrs['discount'] = data.get('discount', 0)
        attrs['total_excl_tax'] = data.get('total_tax_exclusive_amount', 0)
        attrs['total_incl_tax'] = data.get('total_tax_inclusive_amount', 0)
        attrs['tip'] = data.get('tip', 0)

        return attrs

    def number_validation(self, attribute, data, type_conversion):
        final_value = data

        if type_conversion == int:
            validation_re = '^([+-])*\d+$'
        elif type_conversion == float:
            validation_re = '^([+-])*\d+(\.)*\d*$'

        if data and type(data) == str:
            if not re.match(validation_re, data):
                if type_conversion == float:
                    _out = {
                        'status': 'FAIL',
                        'message': '{} parameter should contain numeric characters only'.format(attribute.capitalize())
                    }
                elif type_conversion == int:
                    _out = {
                        'status': 'FAIL',
                        'message': '{} parameter should be valid integer 1,2,3'.format(attribute.capitalize())
                    }

                raise exceptions.ParseError(_out)

        final_value = type_conversion(data)

        if final_value < 0:
            _out = {
                    'status': 'FAIL',
                    'message': '{} parameter cannot be negative'.format(attribute.capitalize())
            }
            raise exceptions.ParseError(_out)

        return final_value

    def dimension_validation(self, weight, volume, length, breadth, height):
        dimension_data = {
            'weight' : weight,
            'volume' : volume,
            'height' : height,
            'length' : length,
            'breadth' : breadth

        }

        for attribute,value in dimension_data.items():
            dimension_data[attribute] = self.number_validation(attribute,value,float)

        return dimension_data

    def create(self, validated_data):
        try:
            request = self.context['request']
            response = self.place_order(
                user=request.user,
                order_data=validated_data,
            )
            if settings.ENABLE_SLACK_NOTIFICATIONS:
                send_slack_notification.apply_async(
                    (response.id,),
                    eta=timezone.now() + timedelta(
                        minutes=0.3
                    ))

            return response
        except ValueError as e:
            raise exceptions.ParseError(str(e))

    class Meta:
        model = Order
        fields = overridable('OSCARAPI_ORDER_FIELD', default=('pickup_hub',
                                                              'number', 'owner', 'billing_address',
                                                              'currency', 'total_incl_tax',
                                                              'total_excl_tax', 'shipping_incl_tax',
                                                              'shipping_excl_tax',
                                                              'shipping_address', 'status',
                                                              'date_placed', 'bill_number')
                             )


class OrderStatusSerializer(serializers.Serializer):
    status = serializers.CharField(max_length=64)


class EventAddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShippingAddress
        exclude = ('id', 'search_text', 'phone_number', 'notes', 'geopoint')


class OrderStatusHistorySerializer(serializers.ModelSerializer):
    location = serializers.SerializerMethodField(read_only=True)

    status = serializers.SerializerMethodField(read_only=True)

    address = serializers.SerializerMethodField(read_only=True)

    # def to_representation(self, instance):
    #     ret = super(OrderStatusHistorySerializer,
    #                 self).to_representation(instance)
    #
    #     address_ret = {}
    #     if instance.address is not None:
    #         address_serializer = EventAddressSerializer(
    #             instance.address, context=self.context
    #         )
    #         address_ret = address_serializer.to_representation(
    #             instance.address)
    #
    #     address_str = ''
    #     if instance.status in StatusPipeline.SHIPMENT_ORDER_STATUSES:
    #
    #         address_details = []
    #         for field in address_ret:
    #             field_value = address_ret[field]
    #             if field_value:
    #                 address_details.append(field_value)
    #         address_str = ', '.join(address_details)
    #         ret['status'] = StatusPipeline.SHIPMENT_ORDER_STATUSES[instance.status]
    #     ret['location'] = {
    #         "lat": '',
    #         "lon": ''
    #     }
    #     return ret

    class Meta:
        model = Event
        fields = ('status', 'time', 'location', 'address')

    def get_status(self, obj):
        return ORDER_STATUSES.get(obj.status, ORDER_NEW)

    def get_location(self, obj):
        if obj.current_location:
            return {
                "lat": obj.current_location.y,
                "lon": obj.current_location.x,
            }
        return {}

    def get_address(self, obj):
        if obj.current_address:
            address = json.loads(obj.current_address)
            if address:
                return address[0].get('formatted_address', None)
        return ""


class OrderDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class EventSerializer(serializers.ModelSerializer):
    location = serializers.SerializerMethodField(read_only=True)

    status = serializers.SerializerMethodField(read_only=True)

    address = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Event
        fields = ('status', 'time', 'location', 'address')

    def get_status(self, obj):
        return ORDER_STATUSES.get(obj.status, ORDER_NEW)

    def get_location(self, obj):
        if obj.current_location:
            return {
                "lat": obj.current_location.y,
                "lon": obj.current_location.x,
            }
        return {}

    def get_address(self, obj):
        if obj.current_address:
            address = json.loads(obj.current_address)
            if address:
                return address[0].get('formatted_address', None)

        elif obj.status == REACHED_AT_HUB:
            hub_name = obj.remarks
            if hub_name:
                hub = Hub.objects.filter(name=hub_name).first()
                if hub:
                    hub_address = hub.address.line1
                    return hub_address

        elif obj.status in [ORDER_NEW, DRIVER_ACCEPTED, DRIVER_ASSIGNED]:
            if obj.order.pickup_address:
                pickup_address = obj.order.pickup_address.line1
                return pickup_address
            return ""

        return ""


class OrderEventHistoryBulkSerializer(serializers.ModelSerializer):
    awb_number = serializers.CharField(source='number')
    events = EventSerializer(many=True, read_only=True, source='event_set')

    class Meta:
        model = Order
        fields = ('awb_number', 'events')

