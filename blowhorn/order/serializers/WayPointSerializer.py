from blowhorn.oscarapi.serializers.utils import OscarModelSerializer
from blowhorn.order.models import WayPoint


class WayPointSerializer(OscarModelSerializer):
    queryset = WayPoint.objects.all()

    def get_order_waypoints(self, order_id):
        queryset = WayPoint.objects.filter(order=order_id)
        return queryset.values()

    class Meta:
        model = WayPoint
        fields = '__all__'
