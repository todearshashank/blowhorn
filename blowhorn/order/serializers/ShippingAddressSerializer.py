from rest_framework import serializers
from blowhorn.oscarapi.serializers.utils import OscarModelSerializer
from blowhorn.order.models import ShippingAddress
from blowhorn.oscar.core.loading import get_model
from django.contrib.gis.geos import GEOSGeometry

Country = get_model('address', 'Country')

class ShippingAddressSerializer(OscarModelSerializer):
    queryset = ShippingAddress.objects.all()

    country = serializers.HyperlinkedRelatedField(
        view_name='country-detail', queryset=Country.objects)

    class Meta:
        model = ShippingAddress
        fields = '__all__'
