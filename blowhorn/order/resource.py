from import_export.widgets import ForeignKeyWidget
from config.settings import status_pipelines as StatusPipeline
from django.conf import settings
from blowhorn.oscar.core.loading import get_model
from import_export import resources, fields

import json
from blowhorn.common.helper import CommonHelper
from blowhorn.utils.functions import utc_to_ist
from .utils import get_trip

Order = get_model('order', 'Order')
OrderBatch = get_model('order', 'OrderBatch')
Customer = get_model('customer', 'Customer')
ShippingAddress = get_model('order', 'ShippingAddress')
Contract = get_model('contract', 'Contract')
Hub = get_model('address', 'Hub')


class ShipmentOrderResource(resources.ModelResource):

    customer_id = fields.Field(attribute="customer",
                               column_name="COMPANY ID",
                               widget=ForeignKeyWidget(Customer, 'id'))

    date_placed = fields.Field(
        column_name="DATE PLACED")

    state = fields.Field(
        column_name="State")

    expected_time = fields.Field(
        column_name="EXPECTED DELIVERY TIME")

    hub_name = fields.Field(attribute="hub",
                            column_name="HUB",
                            widget=ForeignKeyWidget(Hub, 'name'))

    customer_reference_number = fields.Field(attribute="customer_reference_number",
                                             column_name="CUSTOMER REFERENCE NUMBER",)

    reference_number = fields.Field(attribute="reference_number",
                                    column_name="REFERENCE NUMBER",)

    customer_name = fields.Field(attribute="shipping_address",
                                 column_name="CUSTOMER NAME",
                                 widget=ForeignKeyWidget(ShippingAddress,
                                                         'first_name'))

    address_line1 = fields.Field(attribute="shipping_address",
                                 column_name="ADDRESS LINE1(Flat/House No./Floor/Building)",
                                 widget=ForeignKeyWidget(ShippingAddress,
                                                         'line1'))

    address_line2 = fields.Field(column_name="ADDRESS LINE2(Colony, Street/ Locality)",
                                 default='')

    postal_code = fields.Field(attribute="shipping_address",
                               column_name="PIN CODE",
                               widget=ForeignKeyWidget(ShippingAddress,
                                                       'postcode'))

    customer_phone = fields.Field(column_name="CUSTOMER PHONE NUMBER",
                                  default='')

    alternate_customer_phone = fields.Field(column_name="ALTERNATE CUSTOMER PHONE NUMBER",
                                            default='')

    customer_email = fields.Field(column_name="CUSTOMER EMAIL", default='')

    cash_on_delivery = fields.Field(attribute="cash_on_delivery",
                                    column_name="CASH ON DELIVERY")

    latitude = fields.Field(column_name="LATITUDE(if any)", default='')

    longitude = fields.Field(column_name="LONGITUDE(if any)", default='')

    city_name = fields.Field(column_name="City")

    driver_name = fields.Field(column_name="Associate Name")

    driver_mobile = fields.Field(column_name="Associate Mobile")

    pod_files = fields.Field(column_name="POD files")

    remarks = fields.Field(column_name="Remarks")

    kitting_duration = fields.Field(column_name="Kitting Duration (min)")

    preoutbound_duration = fields.Field(column_name="PreOutbound Duration (min)")

    attempt_duration = fields.Field(column_name="Attempt Duration (min)")

    pack_time = fields.Field(column_name="Packed At")

    out_on_road_time = fields.Field(column_name="Out-On-Road At")

    attempted_time = fields.Field(column_name="Attempted At")

    company_name = fields.Field(column_name="COMPANY NAME")

    def dehydrate_kitting_duration(self, order):
        self.kitting_duration = None
        self.preoutbound_duration = None
        self.attempted_time = None
        self.pack_time = None
        self.out_time = None
        self.remarks = None

        events = order.event_set.all()
        for event in events:
            if event.status == StatusPipeline.ORDER_PACKED:
                self.pack_time = event.time
                kt = self.pack_time - order.date_placed

                self.kitting_duration = kt.seconds / 60
            elif event.status == StatusPipeline.OUT_FOR_DELIVERY:
                self.out_time = event.time
                if self.pack_time:
                    preout = self.out_time - self.pack_time
                    self.preoutbound_duration = preout.seconds / 60
            elif event.status in [
                    StatusPipeline.ORDER_DELIVERED,
                    StatusPipeline.ORDER_RETURNED,
                    StatusPipeline.UNABLE_TO_DELIVER
            ]:
                self.remarks = event.remarks
                if self.out_time:
                    self.attempted_time = event.time
                    dt = self.attempted_time - self.out_time
                    self.attempt_duration = dt.seconds / 60

        if self.kitting_duration:
            return '%0.2f' % self.kitting_duration

        return ''

    def dehydrate_company_name(self, order):
        if order.customer:
            return order.customer.name
        return ''

    def dehydrate_remarks(self, order):
        if hasattr(self, 'remarks'):
            if self.remarks:
                return self.remarks
        return ''

    def dehydrate_preoutbound_duration(self, order):
        if hasattr(self, 'preoutbound_duration'):
            if self.preoutbound_duration:
                return '%0.2f' % self.preoutbound_duration
        return ''

    def dehydrate_attempt_duration(self, order):
        if hasattr(self, 'attempt_duration'):
            if self.attempt_duration:
                return '%0.2f' % self.attempt_duration
        return ''

    def dehydrate_pack_time(self, order):

        if hasattr(self, 'pack_time'):
            if self.pack_time:
                return utc_to_ist(self.pack_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
        return ''

    def dehydrate_attempted_time(self, order):
        if hasattr(self, 'attempted_time'):
            if self.attempted_time:
                return utc_to_ist(self.attempted_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
        return ''

    def dehydrate_out_on_road_time(self, order):
        if hasattr(self, 'out_time'):
            if self.out_time:
                return utc_to_ist(self.out_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
        return ''

    def dehydrate_driver_name(self, order):
        if order.driver:
            return order.driver.name
        return ''

    def dehydrate_driver_mobile(self, order):
        if order.driver:
            return str(order.driver.user.phone_number)
        return ''

    def dehydrate_pod_files(self, order):
        pod_files = []
        media_url = CommonHelper().get_media_url_prefix()

        documents = order.documents.all()
        for document in documents:
            file = document.file
            pod_files.append(media_url + str(file))

        if pod_files:
            return pod_files
        return ''

    def dehydrate_date_placed(self, order):
        if order and order.date_placed:
            date_placed = utc_to_ist(order.date_placed).strftime(
                settings.ADMIN_DATETIME_FORMAT)
            return date_placed
        return ''

    def dehydrate_state(self, order):
        if order.city:
            state = order.city.state_set.all()[0] if order.city.state_set.all() else None
            return state.name if state else ''

        return ''

    def dehydrate_expected_time(self, order):
        if order and order.expected_delivery_time:
            expected_delivery_time = utc_to_ist(
                order.expected_delivery_time).strftime(
                settings.ADMIN_DATETIME_FORMAT)
            return expected_delivery_time
        return ''

    def dehydrate_latitude(self, order):
        if order.shipping_address:
            if order.shipping_address.geopoint:
                return order.shipping_address.geopoint.y
        return ''

    def dehydrate_address_line2(self, order):
        if order.shipping_address:
            return order.shipping_address.line2
        return ''

    def dehydrate_customer_phone(self, order):
        if order.shipping_address:
            return str(order.shipping_address.phone_number)
        return ''

    def dehydrate_longitude(self, order):
        if order.shipping_address:
            if order.shipping_address.geopoint:
                return order.shipping_address.geopoint.x
        return ''

    def dehydrate_customer_email(self, order):

        try:
            shipment_info = json.loads(order.shipment_info)
            return shipment_info.get('customer_email')
        except BaseException:
            return ''

    def dehydrate_city_name(self, order):
        if order.city:
            return order.city.name
        return ''

    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    class Meta:
        model = Order
        skip_unchanged = True
        report_skipped = False
        fields = ('number', 'status', 'batch')
        export_order = (
            'company_name', 'hub_name', 'customer_reference_number', 'reference_number', 'status',
            'date_placed', 'expected_time',
            'customer_name', 'address_line1', 'address_line2',
            'postal_code', 'customer_phone',
            'customer_email', 'cash_on_delivery', 'number',
            'latitude', 'longitude', 'city_name',
            'pod_files', 'driver_name',
            'driver_mobile',
            'kitting_duration', 'preoutbound_duration', 'attempt_duration',
            'pack_time', 'out_on_road_time', 'attempted_time', 'remarks', 'state')

from django.db import connection

class BookingOrderResource(resources.ModelResource):

    customer_name = fields.Field(column_name="Customer Name")

    customer_mobile = fields.Field(column_name="Customer Mobile")

    customer_email = fields.Field(column_name="Customer Email")

    pickup_address = fields.Field(column_name="Pickup Address")

    pickup_area = fields.Field(column_name="Pickup Area")

    dropoff_area = fields.Field(column_name="DropOff Area")

    dropoff_address = fields.Field(column_name="Dropoff Address")

    order_number = fields.Field(column_name="Order Number")

    customer_category = fields.Field(column_name="Customer Category")

    distance = fields.Field(column_name="Total Distance")

    booked_from = fields.Field(column_name="Booked From")

    driver_name = fields.Field(column_name="Driver Name")

    driver_vehicle = fields.Field(column_name="Vehicle No.")

    pickup_time = fields.Field(column_name="Pick up Time")

    pickup_date = fields.Field(column_name="Pick up Date")

    total_incl_tax = fields.Field(column_name="Total Incl Tax")

    rcm = fields.Field(column_name="RCM")

    estimated_distance_km = fields.Field(column_name="Estimated distance")

    payment_status = fields.Field(column_name="Payment Status")

    payment_mode = fields.Field(column_name="Payment Mode")

    actual_end_time = fields.Field(column_name="Ended At")

    actual_start_time = fields.Field(column_name="Started At")

    vehicle_class = fields.Field(column_name="Vehicle Class")

    labour_cost = fields.Field(column_name="Labour Cost")

    reason_for_cancellation = fields.Field(column_name="Cancellation Reason")

    def dehydrate_order_number(self, order):
        return order.number

    def dehydrate_reason_for_cancellation(self, order):
        if order.reason_for_cancellation:
            return order.reason_for_cancellation
        return ''

    def dehydrate_labour_cost(self, order):
        if hasattr(order, 'labour'):
            return order.labour.no_of_labours * order.labour.labour_cost
        return 0

    def dehydrate_rcm(self, order):
        if order.rcm:
            return 'Yes'
        return 'No'

    def dehydrate_customer_name(self, order):
        if order.customer:
            return order.customer.name
        return ''

    def dehydrate_customer_mobile(self, order):
        if order.customer:
            return str(order.customer.user.phone_number)
        return ''

    def dehydrate_city(self, order):
        if order.city:
            return order.city.name
        return ''

    def dehydrate_customer_email(self, order):
        if order.customer:
            return order.customer.user.email
        return ''

    def dehydrate_pickup_address(self, order):
        return order.pickup_address.line1

    def dehydrate_dropoff_address(self, order):
        return order.shipping_address.line1

    def dehydrate_customer_category(self, order):
        if order.customer:
            return order.customer.customer_category
        return ''

    def dehydrate_distance(self, order):
        if order and order.status == StatusPipeline.ORDER_DELIVERED:
            trip_ = get_trip(order)
            distance = trip_.gps_distance if trip_ and trip_.gps_distance else 0
            return '%0.1f' % distance
        return ''

    def dehydrate_booked_from(self, order):
        return order.device_type

    def dehydrate_driver_name(self, order):
        if order.driver:
            return order.driver.name
        return ''

    def dehydrate_driver_vehicle(self, order):
        if order.driver:
            return order.driver.driver_vehicle
        return ''

    def dehydrate_pickup_date(self, order):
        if order.pickup_datetime:
            return utc_to_ist(order.pickup_datetime).date()
        return ''

    def dehydrate_estimated_distance_km(self, order):
        return order.estimated_distance_km

    def dehydrate_total_incl_tax(self, order):
        return order.total_incl_tax

    def dehydrate_payment_status(self, order):
        return order.payment_status

    def dehydrate_payment_mode(self, order):
        if order.payment_mode:
            return order.payment_mode
        return ''

    def dehydrate_actual_start_time(self, order):
        trip_ = get_trip(order)
        if trip_ and trip_.actual_start_time:
            return utc_to_ist(trip_.actual_start_time).strftime(
                settings.ADMIN_DATETIME_FORMAT)
        return ''

    def dehydrate_actual_end_time(self, order):
        trip_ = get_trip(order)
        if trip_ and trip_.actual_end_time:
            return utc_to_ist(trip_.actual_end_time).strftime(
                settings.ADMIN_DATETIME_FORMAT)
        return ''

    def dehydrate_pickup_area(self, order):
        pickup_area = order.pickup_address and (
            order.pickup_address.line3 or order.pickup_address.line1)
        return pickup_area if pickup_area else ''

    def dehydrate_dropoff_area(self, order):
        dropoff_area = order.shipping_address and (
            order.shipping_address.line3 or order.shipping_address.line1)
        return dropoff_area if dropoff_area else ''

    def dehydrate_pickup_time(self, order):
        return utc_to_ist(order.pickup_datetime).strftime('%H:%M %p')

    def dehydrate_vehicle_class(self, order):
        if order.vehicle_class_preference:
            return str(order.vehicle_class_preference)
        return ''

    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    class Meta:
        model = Order
        skip_unchanged = True
        report_skipped = False
        fields = ('customer_name', 'customer_mobile' ,'city', 'status', 'pickup_datetime',
            'total_incl_tax','toll_charges', 'total_payable','rcm','estimated_distance_km',)
        # import_id_fields = ('id', 'number')
        export_order = ( 'customer_name' , 'customer_mobile', 'order_number','city',
            'status', 'customer_email', 'pickup_address', 'dropoff_address', 'customer_category',
            'estimated_distance_km','distance', 'booked_from', 'driver_name', 'driver_vehicle',
            'vehicle_class', 'pickup_date', 'pickup_time', 'pickup_area', 'dropoff_area',
            'actual_start_time', 'actual_end_time','total_incl_tax',
            'labour_cost', 'toll_charges', 'total_payable', 'rcm','payment_status','payment_mode')

class OrderResource(resources.ModelResource):
    trip = fields.Field(attribute="Trip",
                        column_name="Trip",)
    trip_status = fields.Field(attribute="Trip Status",
                               column_name="Trip Status",)

    created_by = fields.Field(column_name="Created by")

    modified_by = fields.Field(column_name="Modified by")

    def dehydrate_customer(self, order):
        if order.customer:
            return order.customer.name
        return ''

    def dehydrate_city(self, order):
        if order.city:
            return order.city.name
        return ''

    def dehydrate_hub(self, order):
        if order.hub:
            return order.hub.name
        return ''

    def dehydrate_driver(self, order):
        if order.driver:
            return str(order.driver)
        return ''

    def dehydrate_customer_contract(self, order):
        if order.customer_contract:
            return order.customer_contract.name
        return ''

    def dehydrate_vehicle_class_preference(self, order):
        if order.customer_contract:
            return ', '.join([str(vc) for vc in order.customer_contract.vehicle_classes.all()])
        return ''

    def dehydrate_trip(self, order):
        if order:
            return ', '.join([str(trip.trip_number) for trip in order.trip_set.all()])
        else:
            return ''

    def dehydrate_trip_status(self, order):
        if order:
            return ', '.join([str(trip.status) for trip in order.trip_set.all()])
        else:
            return ''

    def dehydrate_pickup_datetime(self, order):
        if order.pickup_datetime:
            return utc_to_ist(order.pickup_datetime).strftime(
                settings.ADMIN_DATETIME_FORMAT)
        return ''

    def dehydrate_date_placed(self, order):
        if order.date_placed:
            return utc_to_ist(order.date_placed).strftime(
                settings.ADMIN_DATETIME_FORMAT)
        return ''

    def dehydrate_created_by(self, order):
        return str(order.created_by)

    def dehydrate_modified_by(self, order):
        return str(order.modified_by)

    class Meta:
        model = Order
        skip_unchanged = True
        report_skipped = False
        fields = ('number', 'order_type', 'status',
                  'customer', 'customer_contract',  'hub', 'driver',
                  'vehicle_class_preference', 'city', 'date_placed',
                  'pickup_datetime', 'trip', 'trip_status',)
        export_order = ('number', 'order_type', 'status',
                        'customer', 'customer_contract',  'hub', 'driver',
                        'vehicle_class_preference', 'city', 'date_placed',
                        'pickup_datetime', 'trip', 'trip_status', 'created_by', 'modified_by')
