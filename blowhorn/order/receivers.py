from django.dispatch import receiver
from django.db.models.signals import pre_save

from rest_framework import status
from rest_framework.response import Response
from blowhorn.oscar.core.loading import get_model

from blowhorn.common.notification import Notification
from blowhorn.driver.mixins import DriverMixin
from blowhorn.order.const import NEW_BOOKING, STATUS_UPDATE
from blowhorn.oscar.core.loading import get_model
from blowhorn.order.utils import OrderUtil

from .signals import *

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')


@receiver(new_order_notification)
def send_new_order_notification(sender, order, **kwargs):
    pickup_address_geopoint = order.pickup_address.geopoint

    driver = kwargs.get('driver', None)

    available_drivers_activity_queryset = \
        DriverMixin().get_nearest_available_driver(
            pickup_address_geopoint, order, driver=driver)

    if available_drivers_activity_queryset:
        # Send the order to Driver POS and send Admin notification
        for driver_activity in available_drivers_activity_queryset:
            driver = driver_activity.driver
            notification_status = \
                Notification.send_notification(order, driver)
            # waiting for x seconds based on the config/database
            # If the order is not being accepted, send order to another driver
            return Response(
                status=status.HTTP_200_OK, data=notification_status)

    print('Not able to find driver')
    # Return error if no driver found and trigger any action as a co-sequence
    return Response(status=status.HTTP_400_BAD_REQUEST,
                    data='Not able to find any driver nearby')


@receiver(pre_save, sender=Order)
def init_order_values(sender, instance, **kwargs):
    if not instance.pk:
        instance.message_type = NEW_BOOKING
    else:
        instance.message_type = STATUS_UPDATE
    return instance
