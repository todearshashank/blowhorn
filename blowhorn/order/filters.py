# System and Django libraries
from datetime import timedelta
from django.contrib.admin.filters import FieldListFilter
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _


class DateFieldFilter(FieldListFilter):
    """
    Date filter for Order
    """

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.field_generic = '%s__' % field_path
        self.date_params = dict([(k, v) for k, v in params.items()
                                 if k.startswith(self.field_generic)])

        now = timezone.now()
        # When time zone support is enabled, convert "now" to the user's time
        if timezone.is_aware(now):
            now = timezone.localtime(now)

        if isinstance(field, models.DateTimeField):
            today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        else:       # field is a models.DateField
            today = now.date()
        tomorrow = today + timedelta(days=1)

        self.lookup_kwarg_since = '%s__gte' % field_path
        self.lookup_kwarg_until = '%s__lt' % field_path
        self.links = (
            (_('Any date'), {}),
            (_('Today'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('Tomorrow'), {
                self.lookup_kwarg_since: str(today + timedelta(days=1)),
                self.lookup_kwarg_until: str(tomorrow + timedelta(days=1)),
            }),
            (_('Future'), {
                self.lookup_kwarg_since: str(today + timedelta(days=1)),
            }),
            (_('Past Week'), {
                self.lookup_kwarg_since: str(today - timedelta(days=7)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('This Month'), {
                self.lookup_kwarg_since: str(today.replace(day=1)),
                self.lookup_kwarg_until: str(today),
            }),
            (_('Last 60 days'), {
                self.lookup_kwarg_since: str(today - timedelta(days=60)),
                self.lookup_kwarg_until: str(today),
            }),
        )
        super(DateFieldFilter, self).__init__(
            field, request, params, model, model_admin, field_path)

    def expected_parameters(self):
        return [self.lookup_kwarg_since, self.lookup_kwarg_until]

    def choices(self, cl):
        for title, param_dict in self.links:
            yield {
                'selected': self.date_params == param_dict,
                'query_string': cl.get_query_string(
                    param_dict, [self.field_generic]),
                'display': title,
            }


FieldListFilter.register(
    lambda f: isinstance(f, models.DateField), DateFieldFilter)
