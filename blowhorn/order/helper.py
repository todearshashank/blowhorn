import re
import logging
import json
import traceback
import phonenumbers
from random import randint
from datetime import date
from django.utils import timezone
from django.utils.translation import gettext as _
from django.conf import settings
from rest_framework import status
from rest_framework import exceptions
from phonenumber_field.phonenumber import PhoneNumber
from blowhorn.oscar.core.loading import get_model

from blowhorn.common.helper import CommonHelper
from blowhorn.customer.models import Customer
from blowhorn.order import const as order_const

from config.settings import status_pipelines as StatusPipeline


MEDIA_UPLOAD_STRUCTURE = getattr(
    settings, "MEDIA_UPLOAD_STRUCTURE", ""
)

doc_code = 'customer_pod'


def generate_file_path(instance, filename):
    """
    Returns the file path as per the defined directory structure.
    """
    module_name = instance._meta.app_label + "s"

    if hasattr(instance, 'order'):
        doc_code = 'customer_pod'
        instance_label = instance.order.order_type
        instance_handle = instance_label + "_" + str(instance.order.number)
    else:
        instance_handle = str(instance.id)
        doc_code = 'shipment'

    file_name = str(date.today()) + "/" + \
        CommonHelper().get_unique_friendly_id() + "/" + filename.upper()

    return MEDIA_UPLOAD_STRUCTURE.format(
        module_name=module_name,
        instance_handle=instance_handle,
        doc_code=doc_code,
        file_name=file_name
    ).replace("//", "/")

def path_for_supporting_document(instance, filename):
    """
    Returns the file path as per the defined directory structure.
    """
    module_name = instance._meta.app_label + "s"
    instance_label = str(instance.invoice) if hasattr(instance, 'invoice') else str(instance)
    instance_handle = instance_label + "_" + str(instance.invoice) if hasattr(instance, 'invoice') else str(instance)
    file_name = str(date.today()) + "/" + \
        CommonHelper().get_unique_friendly_id() + "/" + filename.upper()

    return MEDIA_UPLOAD_STRUCTURE.format(
        module_name=module_name,
        instance_handle=instance_handle,
        doc_code=doc_code,
        file_name=file_name
    ).replace("//", "/")

class ShipmentOrderValidation():

    def validate(self, request, **kwargs):
        from blowhorn.order.mixins import OrderPlacementMixin
        from blowhorn.address.utils import get_hub_from_pincode
        from blowhorn.order.models import ShippingAddress
        from blowhorn.order.models import Order

        attrs = {}
        data = request.data
        is_update = kwargs.get('is_update', False)

        if is_update:
            order = kwargs.get('order')

        valid_parameters = ['awb_number', 'customer', 'customer_email', 'customer_mobile',
                            'customer_name', 'delivery_address',
                            'delivery_lat', 'delivery_lon', 'store_code',
                            'delivery_postal_code', 'expected_delivery_time',
                            'cash_on_delivery', 'order_creation_time', "total_payable","customer_id",
                            'reference_number', 'shipment_info', 'is_offline_order',
                            'csv_upload', 'pickedup_address', 'pickup_lat', 'pickup_lon',
                            'pickup_postal_code', 'items', 'customer_contract', 'alternate_customer_mobile',
                            'item_details', 'is_cod',"bill_number", "bill_date", "order_mode", "remarks",
                            "payment_mode", "device_type",
                            ]

        # Raise exception if any extra params present in data
        for param in data:
            if param not in valid_parameters:
                message = _('%s is not a valid parameter' % param)
                _out = {
                    'status': 'FAIL',
                    'message': message
                }
                raise exceptions.ParseError(_out)

        if data.get('is_cod', False):
            try:
                if not int(data.get('cash_on_delivery', None)):
                    _out = {
                        'status': 'FAIL',
                        'message': 'Enter cod amount greater than 0'
                    }
                    raise exceptions.ParseError(_out)

            except (ValueError, TypeError):
                _out = {
                    'status': 'FAIL',
                    'message': 'Enter the valid amount to be collected at the time of delivery'
                }
                raise exceptions.ParseError(_out)
        else:
            if data.get('cash_on_delivery', None):
                _out = {
                    'status': 'FAIL',
                    'message': 'Cash on delivery amount is needed only when is_cod is True'
                }
                raise exceptions.ParseError(_out)

        # TODO: At later stage, can be used for validation during order creation
        if not is_update:
            reference_number = data.get('reference_number', None)
            if not reference_number:
                _out = {
                    'status': 'FAIL',
                    'message': 'Shipment Reference Number is required'
                }
                raise exceptions.ParseError(_out)

            order_qs = Order.objects.filter(
                customer=data.get('customer', None),
                reference_number=reference_number
            )
            if order_qs.exists():
                _out = {
                    'status': 'PASS',
                    'message': {
                        'awb_number': order_qs.first().number
                    }
                }
                raise exceptions.ParseError(_out)
        else:
            reference_number = order.reference_number

        shipment_info = {}
        customer_name = data.get('customer_name', None)
        if customer_name:
            shipment_info['customer_name'] = customer_name

        customer_mobile = data.get('customer_mobile', None)
        if not customer_mobile:
            _out = {
                'status': 'FAIL',
                'message': 'Customer mobile number is mandatory'
            }
            raise exceptions.ParseError(_out)

        try:
            customer_phone = PhoneNumber.from_string(
                customer_mobile, settings.COUNTRY_CODE_A2)
            if not customer_phone.is_valid():
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid customer mobile number'
                }
                raise exceptions.ParseError(_out)
        except phonenumbers.NumberParseException:
            _out = {
                'status': 'FAIL',
                'message': 'Invalid customer mobile number'
            }
            raise exceptions.ParseError(_out)
        shipment_info['customer_mobile'] = customer_mobile

        alternate_customer_mobile = data.get('alternate_customer_mobile', None)
        if alternate_customer_mobile:
            try:
                alternate_customer_phone = PhoneNumber.from_string(
                    alternate_customer_mobile, settings.COUNTRY_CODE_A2)
                if not alternate_customer_phone.is_valid():
                    _out = {
                        'status': 'FAIL',
                        'message': 'Invalid alternate customer mobile number'
                    }
                    raise exceptions.ParseError(_out)
            except phonenumbers.NumberParseException:
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid alternate customer mobile number'
                }
                raise exceptions.ParseError(_out)
        shipment_info['alternate_customer_mobile'] = alternate_customer_mobile

        customer_email = data.get('customer_email', None)
        if customer_email:
            if not re.search(settings.EMAIL_REGEX, customer_email):
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid customer email id'
                }
                raise exceptions.ParseError(_out)
            shipment_info['customer_email'] = customer_email

        shipping_address = data.get('delivery_address')
        shipping_postal_code = data.get('delivery_postal_code')
        if not shipping_address or not shipping_postal_code:
            _out = {
                'status': 'FAIL',
                'message': 'Delivery address / delivery postal code is missing'
            }
            raise exceptions.ParseError(_out)

        country_code = data.get(
            'country_code', settings.COUNTRY_CODE_A2)
        POSTAL_CODE_REGEX = ShippingAddress.POSTCODES_REGEX.get(country_code)
        if not re.search(POSTAL_CODE_REGEX, shipping_postal_code):
            _out = {
                'status': 'FAIL',
                'message': 'Invalid delivery postal code'
            }
            raise exceptions.ParseError(_out)

        hub = get_hub_from_pincode(shipping_postal_code)
        if not hub and not data.get('allow_order_creation', False):
            _out = {
                'status': 'FAIL',
                'message': 'Blowhorn doesn\'t provide service in this area'
            }
            raise exceptions.ParseError(_out)

        # TODO: At later stage, can be used for validation during order creation
        if not is_update:
            expected_delivery_time = data.get('expected_delivery_time')
            if expected_delivery_time:
                try:
                    expected_delivery_time = OrderPlacementMixin().get_aware_datetime(
                        expected_delivery_time)
                except:
                    _out = {
                        'status': 'FAIL',
                        'message': 'Invalid expected delivery date time format'
                    }
                    raise exceptions.ParseError(_out)

                if expected_delivery_time < timezone.now():
                    _out = {
                        'status': 'FAIL',
                        'message': 'Expected delivery time cannot be a past date time'
                    }
                    raise exceptions.ParseError(_out)

            order_creation_time = data.get('order_creation_time')
            if order_creation_time:
                try:
                    order_creation_time = OrderPlacementMixin().get_aware_datetime(
                        order_creation_time)
                except:
                    _out = {
                        'status': 'FAIL',
                        'message': 'Invalid order creation date time format'
                    }
                    raise exceptions.ParseError(_out)

                time_diff = (timezone.now() -
                             order_creation_time).total_seconds()
                if time_diff > settings.ORDER_CREATION_PAST_TIME_TOLERANCE:
                    _out = {
                        'status': 'FAIL',
                        'message': 'Order creation time cannot be a past date time'
                    }
                    raise exceptions.ParseError(_out)

                if time_diff < 0:
                    _out = {
                        'status': 'FAIL',
                        'message': 'Order creation time cannot be a future date time'
                    }
                    raise exceptions.ParseError(_out)

        shipping_lat = data.get('delivery_lat')
        shipping_lon = data.get('delivery_lon')
        attrs['shipping_address'] = OrderPlacementMixin().get_updated_address(
            shipping_address,
            shipping_postal_code,
            shipping_lat,
            shipping_lon,
            reference_number=reference_number,
        )

        if customer_mobile:
            attrs['shipping_address']['phone_number'] = customer_phone

        if customer_name:
            attrs['shipping_address']['first_name'] = customer_name

        # TODO: At later stage, can be used for validation during order creation
        if data.get('csv_upload'):
            attrs['items'] = data.get('items').split(',')
            if not data.get('pickup_address') or data.get('pickup_postal_code'):
                attrs['pickup_address'] = {
                    'line1': hub.address.line1,
                    'postcode': hub.address.postcode,
                }
            else:
                attrs['pickup_address'] = {
                    'line1': data.get('pickup_address'),
                    'postcode': data.get('pickup_postal_code'),
                }
            attrs['waybill_number'] = data.get('waybill_number')
            attrs['shipment_info'] = json.dumps(data.get('shipment_info'))
        else:
            pickup_address = data.get('pickedup_address', None)
            pickup_postal_code = data.get('pickup_postal_code')

            if (pickup_address and not pickup_postal_code) or (not pickup_address and pickup_postal_code):
                _out = {
                    'status': 'FAIL',
                    'message': 'Pickup address / pickup postal code is missing'
                }
                raise exceptions.ParseError(_out)

            if pickup_address and pickup_postal_code:
                if not re.search(POSTAL_CODE_REGEX, pickup_postal_code):
                    _out = {
                        'status': 'FAIL',
                        'message': 'Invalid pickup postal code'
                    }
                    raise exceptions.ParseError(_out)

                if not data.get('allow_order_creation', False) and not get_hub_from_pincode(pickup_postal_code):
                    _out = {
                        'status': 'FAIL',
                        'message': 'Blowhorn doesn\'t provide service in this area'
                    }
                    raise exceptions.ParseError(_out)

                pickup_lat = data.get('pickup_lat')
                pickup_lon = data.get('pickup_lon')
                attrs['pickup_address'] = OrderPlacementMixin().get_updated_address(
                    pickup_address,
                    pickup_postal_code,
                    pickup_lat,
                    pickup_lon,
                )
            else:
                attrs['shipment_info'] = shipment_info
                attrs['pickup_address'] = {
                    'line1': hub.address.line1,
                    'postcode': hub.address.postcode,
                }
                if hub.address.geopoint:
                    coords = hub.address.geopoint.coords
                    attrs['pickup_address'].update({
                        'latitude': coords[1],
                        'longitude': coords[0]
                    })

        number = data.get('awb_number')
        if number:
            attrs['order_number'] = number

        attrs['hub'] = hub if not is_update else hub.id
        attrs['reference_number'] = reference_number
        if data.get('bill_number', None):
            attrs['bill_number'] = data['bill_number']
        if data.get('bill_date', None):
            attrs['bill_date'] = data['bill_date']
        if data.get('order_mode', None):
            attrs['order_mode'] = data['order_mode']
        attrs['cash_on_delivery'] = data['cash_on_delivery'] if data.get(
            'cash_on_delivery', None) else 0.00

        return attrs


def get_order_authentication_details(request, order_id):
    from blowhorn.order.models import Order

    api_key = request.META.get('HTTP_API_KEY', None)
    if api_key:
        customer = Customer.objects.filter(api_key=api_key).first()
        if not customer:
            response = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return {'ret_status': status.HTTP_401_UNAUTHORIZED, 'response': response}
    else:
        response = {
            'status': 'FAIL',
            'message': 'Unauthorized'
        }
        return {'ret_status': status.HTTP_401_UNAUTHORIZED, 'response': response}

    order = Order.objects.filter(number=order_id).select_related(
        'shipping_address', 'pickup_address', 'hub', 'customer')

    if not order:
        response = {
            'status': 'FAIL',
            'message': 'Order doesn\'t exist'
        }
        return {'ret_status': status.HTTP_404_NOT_FOUND, 'response': response}

    response = {
        'status': 'PASS',
        'message': {},
        'order': order,
        'customer': customer
    }
    logging.info(response)
    return {'ret_status': status.HTTP_200_OK, 'response': response}

