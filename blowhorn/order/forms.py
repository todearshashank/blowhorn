# System and Django imports
from datetime import datetime, timedelta
import json
import pytz
from django import forms
from django.conf import settings
from django.contrib.admin.helpers import ActionForm
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.forms.models import BaseInlineFormSet
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import widgets

# 3rd party imports
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.address.utils import get_hub_from_pincode
from blowhorn.order.mixins import OrderPlacementMixin
from blowhorn.oscar.core.loading import get_model
from blowhorn.trip.admin import TripConstants
from blowhorn.trip.constants import METER_READING_CAP
from blowhorn.trip.models import Trip
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.models import DriverVehicleMap
from blowhorn.utils.functions import utc_to_ist
from blowhorn.common.helper import CommonHelper
from blowhorn.order.const import PAYMENT_STATUS_PAID, PAYMENT_MODE_RAZORPAY, \
    PAYMENT_STATUS_UNPAID, PAYMENT_MODE_PAYTM, PAYMENT_MODE_CASH
from blowhorn.contract.constants import (
    CONTRACT_STATUS_ACTIVE,
    CONTRACT_TYPE_SPOT,
    CONTRACT_TYPE_C2C,
    CONTRACT_TYPE_SME,
    CONTRACT_TYPE_KIOSK, DEAFCOM_MAX_DAYS_ALLOWED, DEAFCOM_MIN_DAYS_ALLOWED, CONTRACT_TYPE_DEAFCOM
)
from blowhorn.address.models import City
from blowhorn.coupon.models import Coupon
from blowhorn.coupon.mixins import CouponMixin
from blowhorn.trip.mixins import TripMixin
from website import constants

Labour = get_model('order', 'Labour')
WayPoint = get_model('order', 'WayPoint')
Order = get_model('order', 'Order')
Hub = get_model('address', 'Hub')
Contract = get_model('contract', 'Contract')
ShippingAddress = get_model('order', 'ShippingAddress')
OrderConstants = get_model('order', 'OrderConstants')
Event = get_model('order', 'Event')
ShipmentAlertConfiguration = get_model(
    'customer', 'ShipmentAlertConfiguration')
Customer = get_model('customer', 'Customer')
InvoiceOrder = get_model('customer', 'InvoiceOrder')
Driver = get_model('driver', 'Driver')


class OrderInternalTrackingFormset(forms.models.BaseInlineFormSet):
    def save_new(self, form, commit=True):
        obj = super().save_new(form, commit=False)
        obj.associate = self.request.user
        obj.time = timezone.now()
        if commit:
            obj.save()
        return obj


class PictureWidget(forms.widgets.Widget):

    def render(self, name, value, attrs=None, ref=None, file=None):
        media_url = CommonHelper().get_media_url_prefix()

        if self.ref:
            image_path = '/media/uploads/barcode/' + self.ref + '.png'

        return mark_safe(
            '<a href=' + image_path + ' target="_blank">' + ' <img src=' + image_path + ' height="500">' + '</a>')

    def __init__(self, ref=None, file=None):
        super(PictureWidget, self).__init__()
        self.ref = ref
        self.file = file


class ShipmentOrderEditForm(forms.ModelForm):
    barcode = forms.ImageField(required=False)

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:

            shipment_info = kwargs['instance'].shipment_info
            if shipment_info:
                try:
                    shipment_info = json.loads(shipment_info)
                except BaseException:
                    pass

            initial = {'shipment_info': shipment_info}
            kwargs['initial'] = initial
            super(ShipmentOrderEditForm, self).__init__(*args, **kwargs)

            reference_number = kwargs['instance'].reference_number
            self.fields['barcode'].widget = PictureWidget(ref=reference_number)

            if self.fields.get('invoice_driver', None):
                order = kwargs.get('instance', None)
                self.fields['invoice_driver'].autocomplete = False
                invoice_ids = InvoiceOrder.objects.filter(order=order).values_list('invoice_id', flat=True)
                filter_params = Q(order__invoiceorder__invoice_id__in=invoice_ids)
                if order.driver:
                    filter_params = filter_params | Q(id=order.driver.id)
                qs = Driver.objects.filter(filter_params).distinct('id')
                self.fields['invoice_driver'].queryset = qs

    def clean(self):

        super(ShipmentOrderEditForm, self).clean()

        pickup_address = self.cleaned_data.get('pickup_address')

        if pickup_address:

            hub = get_hub_from_pincode(pickup_address.postcode)
            if not hub:
                raise ValidationError(
                    _('Blowhorn services is not available'
                      ' for selected Pickup Address ')
                )

            if self.cleaned_data.get('hub') and \
                hub != self.cleaned_data.get('hub'):
                raise ValidationError(
                    _('Hub for selected Pickup Address and the Hub'
                      ' that you have chosen are different'))

            if pickup_address.geopoint is not None:

                city = OrderPlacementMixin.get_city(self, pickup_address)
                if city is None:
                    raise ValidationError(
                        _('Blowhorn services is not available'
                          ' for selected Pickup Address '))
                elif city != self.cleaned_data.get('city'):
                    raise ValidationError(
                        _('City for selected Pickup Address and the City'
                          'that you have chosen are different'))
            elif hub:
                if hub.city != self.cleaned_data.get('city'):
                    raise ValidationError(
                        _('Pickup Address is in a different city'))

            self.cleaned_data['hub'] = hub
            city = self.cleaned_data.get('city')

    class Meta:
        model = Order
        fields = '__all__'


class ShipmentOrderAddForm(forms.ModelForm):

    def clean(self):
        super(ShipmentOrderAddForm, self).clean()
        pickup_address = self.cleaned_data.get('pickup_address')
        if pickup_address:
            hub = get_hub_from_pincode(pickup_address.postcode)
            if not hub:
                raise ValidationError(
                    _('Blowhorn services is not available'
                      ' for selected Pickup Address ')
                )

            if self.cleaned_data.get('hub') and \
                hub != self.cleaned_data.get('hub'):
                raise ValidationError(
                    _('Hub for selected Pickup Address and the Hub'
                      ' that you have chosen are different'))

            if pickup_address.geopoint is not None:
                city = OrderPlacementMixin.get_city(self, pickup_address)
                if city is None:
                    raise ValidationError(
                        _('Blowhorn services is not available'
                          ' for selected Pickup Address '))
                elif city != self.cleaned_data.get('city'):
                    raise ValidationError(
                        _('City for selected Pickup Address and the City'
                          'that you have chosen are different'))
            elif hub:
                if hub.city != self.cleaned_data.get('city'):
                    raise ValidationError(
                        _('Pickup Address is in a different city'))

            self.cleaned_data['hub'] = hub
            city = self.cleaned_data.get('city')

    class Meta:
        model = Order
        exclude = ()


def apply_contract_permission_filter(user, qs):
    """ Common filter to apply for filtering out contarcts
        based on access level
    """
    from blowhorn.order.admin import FixedOrder, SpotOrder, ShipmentOrder
    if not user.is_superuser:
        cities = City.objects.get_cities_for_managerial_user(user)
        contracts = Contract.objects.filter(Q(spocs=user) |
                                            Q(supervisors=user) |
                                            Q(city__in=cities)).distinct()
        if qs.model in [ShipmentOrder, FixedOrder, SpotOrder]:
            qs = qs.filter(customer_contract__in=contracts)
            # qs = qs.filter(city__in=cities)
        elif qs.model in [Contract]:
            qs = qs.filter(id__in=contracts)
    #        qs = qs.distinct()
    return qs


def AddSpotOrder(request):
    class SpotOrderForm(forms.ModelForm):

        num_of_orders = forms.IntegerField(
            min_value=1, max_value=100, initial=1,
            label=_("Number of Orders to be created"),
            help_text=_("Enter value greater than 1 for bulk order creation"))

        def __init__(self, *args, **kwargs):
            super(SpotOrderForm, self).__init__(*args, **kwargs)

            # if self.fields.get('customer_contract', None):
            #     self.fields['customer_contract'].autocomplete = False
            #     contract_qs = Contract.objects.filter(
            #         status=CONTRACT_STATUS_ACTIVE,
            #         contract_type=CONTRACT_TYPE_SPOT,
            #     )
            #     if request and request.user:
            #         contract_qs = apply_contract_permission_filter(request.user, contract_qs)
            #     self.fields['customer_contract'].queryset = contract_qs
            # if self.fields.get('driver', None):
            #     self.fields['driver'].autocomplete = False

        def clean(self, *args, **kwargs):
            if self.cleaned_data:
                print('self.cleaned_data-->', self.cleaned_data)
                hub = self.cleaned_data.get('hub')
                customer_contract = self.cleaned_data.get('customer_contract')
                driver = self.cleaned_data.get('driver')
                pickup_datetime = self.cleaned_data.get('pickup_datetime')
                current_date = utc_to_ist(timezone.now())
                allowed_future_pickup_datetime_range = current_date.date() + \
                                                       timedelta(days=settings.ALLOWED_FUTURE_DAYS)
                allowed_past_pickup_datetime_range = current_date.date() - timedelta(
                    days=settings.MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME)
                num_of_orders = self.cleaned_data.get('num_of_orders')

                # If Driver is given for bulk order
                if num_of_orders and num_of_orders > 1 and driver:
                    raise ValidationError({
                        'driver': 'Driver should NOT be specified for bulk '
                                  'order creation'
                    })

                if pickup_datetime:
                    is_valid, message = Order().is_pickup_time_in_valid_date_range(
                        pickup_datetime)
                    if not is_valid:
                        raise ValidationError(message)

                # If customer contract is not given
                if not customer_contract:
                    raise ValidationError({
                        'customer_contract': 'Customer Contract is mandatory'
                    })

                if not hub:
                    raise ValidationError({
                        'hub': 'Hub is mandatory'
                    })

                if not driver and 'driver' in self.changed_data:
                    raise ValidationError({
                        'driver': 'Driver is mandatory'
                    })

        class Meta:
            model = Order
            fields = '__all__'

    return SpotOrderForm


class SpotOrderEditForm(forms.ModelForm):
    suspension_trip_reason = forms.CharField(
        widget=forms.HiddenInput(),
        max_length=100,
        required=False)

    trip_status = forms.CharField(
        widget=forms.HiddenInput(),
        max_length=100,
        required=False)

    def __init__(self, *args, **kwargs):
        super(SpotOrderEditForm, self).__init__(*args, **kwargs)
        # if 'instance' in kwargs:
        #     contract = kwargs['instance'].customer_contract
        #     status = kwargs['instance'].status
        #     if status not in [StatusPipeline.ORDER_DELIVERED,
        #                       StatusPipeline.ORDER_CANCELLED, StatusPipeline.DUMMY_STATUS]:
        #         if self.fields.get('driver', None):
        #             self.fields['driver'].autocomplete = False
        #             self.fields['driver'].queryset = Driver.objects.filter(
        #                 status=StatusPipeline.ACTIVE,
        #                 vehicles__vehicle_model__vehicle_class__in=contract.vehicle_classes.all(),
        #                 vehicles__body_type__in=contract.body_types.all()
        #             ).distinct('id')

    def clean(self, *args, **kwargs):
        if self.cleaned_data and self.instance.status not in StatusPipeline.ORDER_END_STATUSES:
            driver = self.cleaned_data.get('driver')

            if not driver and 'driver' in self.changed_data:
                raise ValidationError({
                    'driver': 'Driver is mandatory'
                })

    class Meta:
        model = Order
        fields = '__all__'


class DeafcomOrderEditForm(forms.ModelForm):
    suspension_trip_reason = forms.CharField(
        widget=forms.HiddenInput(),
        max_length=100,
        required=False)

    trip_status = forms.CharField(
        widget=forms.HiddenInput(),
        max_length=100,
        required=False)

    def __init__(self, *args, **kwargs):
        super(DeafcomOrderEditForm, self).__init__(*args, **kwargs)
        # if 'instance' in kwargs:
        #     contract = kwargs['instance'].customer_contract
        #     status = kwargs['instance'].status
        #     if status not in [StatusPipeline.ORDER_DELIVERED,
        #                       StatusPipeline.ORDER_CANCELLED, StatusPipeline.DUMMY_STATUS]:
        #         if self.fields.get('driver', None):
        #             self.fields['driver'].autocomplete = False
        #             self.fields['driver'].queryset = Driver.objects.filter(
        #                 status=StatusPipeline.ACTIVE,
        #                 vehicles__vehicle_model__vehicle_class__in=contract.vehicle_classes.all(),
        #                 vehicles__body_type__in=contract.body_types.all()
        #             ).distinct('id')

    def clean(self, *args, **kwargs):
        if self.cleaned_data and self.instance.status not in StatusPipeline.ORDER_END_STATUSES:
            driver = self.cleaned_data.get('driver')

            if not driver and 'driver' in self.changed_data:
                raise ValidationError({
                    'driver': 'Driver is mandatory'
                })

    class Meta:
        model = Order
        fields = '__all__'


def AddDeafcomOrder(request):
    class DeafcomOrderForm(forms.ModelForm):

        actual_start_time = forms.SplitDateTimeField(
            widget=widgets.AdminSplitDateTime(),
            label=_("Trip Started At"),
            required=True)

        actual_end_time = forms.SplitDateTimeField(
            widget=widgets.AdminSplitDateTime(),
            label=_("Trip Ended At"),
            required=True)

        meter_reading_start = forms.IntegerField(
            initial=1, required=False,
            label=_("MR Start")
        )

        meter_reading_end = forms.IntegerField(
            initial=1, required=False,
            label=_("MR End")
        )

        trip_base_pay = forms.DecimalField(
            initial=1, required= False,
            label=_("Agreed base pay")
        )

        def __init__(self, *args, **kwargs):
            super(DeafcomOrderForm, self).__init__(*args, **kwargs)

            if self.fields.get('customer_contract', None):
                self.fields['customer_contract'].autocomplete = False
                contract_qs = Contract.objects.filter(
                    status=CONTRACT_STATUS_ACTIVE,
                    contract_type=CONTRACT_TYPE_DEAFCOM,
                )
                if request and request.user:
                    contract_qs = apply_contract_permission_filter(request.user, contract_qs)
                self.fields['customer_contract'].queryset = contract_qs

            if self.fields.get('driver', None):
                self.fields['driver'].autocomplete = False
                driver_qs = Driver.objects.filter(status=StatusPipeline.ACTIVE, is_defcom_driver=True)
                self.fields['driver'].queryset = driver_qs

            if self.fields.get('hub', None):
                self.fields['hub'].autocomplete = False
                customer_ids = Contract.objects.filter(contract_type=CONTRACT_TYPE_DEAFCOM,
                                                       status=CONTRACT_STATUS_ACTIVE).values_list('customer_id',
                                                                                                  flat=True)
                hub_qs = Hub.objects.filter(customer_id__in=customer_ids)
                self.fields['hub'].queryset = hub_qs

        def clean(self, *args, **kwargs):
            if self.cleaned_data:
                hub = self.cleaned_data.get('hub')
                customer_contract = self.cleaned_data.get('customer_contract')
                driver = self.cleaned_data.get('driver')
                current_date = utc_to_ist(timezone.now())
                start_time = self.cleaned_data.get('actual_start_time')
                end_time = self.cleaned_data.get('actual_end_time')
                mr_start = self.cleaned_data.get('meter_reading_start')
                mr_end = self.cleaned_data.get('meter_reading_end')
                base_pay=self.cleaned_data.get('trip_base_pay')

                deafcom_max_days_allowed = OrderConstants.objects.filter(name=DEAFCOM_MAX_DAYS_ALLOWED).values_list(
                    'value', flat=True)
                deafcom_min_days_allowed = OrderConstants.objects.filter(name=DEAFCOM_MIN_DAYS_ALLOWED).values_list(
                    'value', flat=True)
                deafcom_start_days = int(list(deafcom_max_days_allowed)[
                                             0]) if deafcom_max_days_allowed.exists() else settings.DEAFCOM_ALLOWED_FUTURE_DAYS

                deafcom_end_days = int(list(deafcom_min_days_allowed)[
                                           0]) if deafcom_min_days_allowed.exists() else settings.DEAFCOM_MAX_PAST_DAYS_ALLOWED_FOR_PICKUP_TIME

                allowed_future_date = current_date + \
                                      timedelta(days=deafcom_start_days)
                allowed_past_pickup_date = current_date - timedelta(
                    days=deafcom_end_days)

                if start_time and start_time< allowed_past_pickup_date:
                    raise ValidationError({
                        'actual_start_time': 'Date should not be smaller than %s' % allowed_past_pickup_date
                    })

                if end_time and end_time > allowed_future_date:
                    raise ValidationError({
                        'actual_end_time': 'Date should not be greater than %s' % allowed_future_date
                    })

                """
                 Start and end of trip can be same
                """
                if (start_time and end_time) and \
                    start_time == end_time:
                    raise ValidationError({
                        'Started at and Ended at cannot be same'
                    })
                """
                Ended at cannot be less than Started at
                """
                if start_time and end_time:
                    if start_time > end_time:
                        raise ValidationError(
                            'Ended At cannot be less than started at'
                        )

                if mr_end and not mr_start:
                    raise ValidationError(
                        'Enter Start Meter Reading'
                    )
                if mr_start and mr_end:
                    if mr_end <= mr_start:
                        raise ValidationError(
                            'End Meter Reading should be greater than Start Meter Reading'
                        )
                # num_of_orders = self.cleaned_data.get('num_of_orders')
                # If Driver is given for bulk order
                # if num_of_orders and num_of_orders > 1 and driver:
                #     raise ValidationError({
                #         'driver': 'Driver should NOT be specified for bulk '
                #                   'order creation'
                #     })

                # If customer contract is not given
                if not customer_contract:
                    raise ValidationError({
                        'customer_contract': 'Customer Contract is mandatory'
                    })

                if not hub:
                    raise ValidationError({
                        'hub': 'Hub is mandatory'
                    })

                if base_pay and base_pay < 0:
                    raise ValidationError({
                        'trip_base_pay': 'Agreed base pay should not be less than 0'
                    })

                if not driver:
                    raise ValidationError({
                        'driver': 'Driver is mandatory'
                    })

                is_valid_driver = Driver.objects.filter(
                    id=driver.id,
                    status=StatusPipeline.ACTIVE,
                    vehicles__vehicle_model__vehicle_class__in=customer_contract.vehicle_classes.all(),
                    vehicles__body_type__in=customer_contract.body_types.all()
                ).exists()

                if not is_valid_driver:
                    raise ValidationError({
                        'driver': 'Driver vehicle class does not match contract vehicle class/body type'
                    })

                vehicle_class_limit = driver.vehicles.first().vehicle_model.vehicle_class.per_day_limit if driver \
                    else None
                if vehicle_class_limit and float(base_pay) > float(vehicle_class_limit):
                    raise ValidationError({
                       'trip_base_pay': 'Cannot enter supply cost greater than the daily approval limit'})

        class Meta:
            model = Order
            fields = '__all__'

    return DeafcomOrderForm


class FixedOrderEditForm(forms.ModelForm):
    suspension_trip_reason = forms.CharField(
        widget=forms.HiddenInput(),
        max_length=100,
        required=False)

    trip_status = forms.CharField(
        widget=forms.HiddenInput(),
        max_length=100,
        required=False)

    def __init__(self, *args, **kwargs):
        super(FixedOrderEditForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            contract = kwargs['instance'].customer_contract
            status = kwargs['instance'].status
            if status not in [StatusPipeline.ORDER_DELIVERED,
                              StatusPipeline.ORDER_CANCELLED, StatusPipeline.DUMMY_STATUS]:
                if self.fields.get('driver', None):
                    self.fields['driver'].autocomplete = False
                    self.fields['driver'].queryset = Driver.objects.filter(
                        status=StatusPipeline.ACTIVE,
                        vehicles__vehicle_model__vehicle_class__in=contract.vehicle_classes.all(),
                        vehicles__body_type__in=contract.body_types.all()
                    ).distinct('id')

    def clean(self, *args, **kwargs):
        if self.cleaned_data:
            driver = self.cleaned_data.get('driver')

            if not driver and self.instance.status not in StatusPipeline.ORDER_END_STATUSES and 'driver' in self.changed_data:
                raise ValidationError({
                    'driver': 'Driver is mandatory'
                })

    class Meta:
        model = Order
        fields = '__all__'


class ShippingAddressForm(forms.ModelForm):
    line1 = forms.CharField(label='Full Address', widget=forms.Textarea())
    line3 = forms.CharField(label='Area', required=False)
    postcode = forms.CharField(label='Post/Zip-code')
    phone_number = forms.CharField(required=False, max_length=10, label='Phone Number')

    def __init__(self, *args, **kwargs):

        if kwargs.get('instance', None):
            phone_number_obj = kwargs['instance'].phone_number
            if phone_number_obj:
                phone_number = phone_number_obj.__dict__.get('national_number')
                if phone_number:
                    initial = kwargs.get('initial', {})
                    initial['phone_number'] = str(phone_number).zfill(10)
                    kwargs['initial'] = initial
        super(ShippingAddressForm, self).__init__(*args, **kwargs)

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get('phone_number')

        if phone_number:
            phone_number = '%s%s' % (settings.ACTIVE_COUNTRY_CODE, phone_number)

        return phone_number


class ContingencyTripInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ContingencyTripInlineForm, self).__init__(*args, **kwargs)
        self.fields['planned_start_time'].initial = timezone.now()


class ContingencyTripInlineFormSet(BaseInlineFormSet):

    def clean(self, *args, **kwargs):

        if len(self.forms) > 0:
            ''' Get the count of New/In-progress trip'''
            new_inprogress_trip_count = Trip.objects.filter(
                order=self.instance.id,
                status__in=[
                    StatusPipeline.TRIP_NEW,
                    StatusPipeline.TRIP_IN_PROGRESS,
                    StatusPipeline.DRIVER_ACCEPTED,
                    StatusPipeline.TRIP_ALL_STOPS_DONE]).count()

            is_trip_completed = Trip.objects.filter(
                order=self.instance.id,
                status=StatusPipeline.TRIP_COMPLETED
            ).count()

            driver = self.forms[0].cleaned_data.get('driver')
            planned_start_time = self.forms[
                0].cleaned_data.get('planned_start_time')
            order = self.forms[0].cleaned_data.get('order')

            '''If driver is not selected'''
            if not driver:
                raise ValidationError(
                    'Please select Driver'
                )

            '''If planned start time is not selected'''
            if not planned_start_time:
                raise ValidationError(
                    'Please select Planned Date and Time'
                )

            '''Planned at should be same day'''
            if not planned_start_time.date() == timezone.now().date():
                raise ValidationError(
                    'Cannot add Contingency trip, Scheduled at should be current day'
                )

            '''Planned at should not be less than order pickup datetime'''
            if planned_start_time < order.pickup_datetime:
                raise ValidationError(
                    'Contingency trip planned time cannot be before the actual order pick up time'
                )

            '''Planned at should be within same day'''
            if not planned_start_time.date() == order.pickup_datetime.date():
                raise ValidationError(
                    'Contingency trip can be planned only for same day as order pick up date'
                )

            ''' If order already delivered dont allow to add new trip'''
            if self.instance.status == StatusPipeline.TRIP_COMPLETED:
                raise ValidationError(
                    'Order already completed'
                )

            ''' If already have completed trip dont allow to add new trip'''
            if is_trip_completed > 0:
                raise ValidationError(
                    'Trip already completed'
                )

            ''' If order already have 'New' or 'In-progress'
                dont allow to add new trip
            '''
            if new_inprogress_trip_count > 0:
                raise ValidationError(
                    'Please update current trip as Suspended'
                )

    def save_new(self, form, commit=True):
        """
        This is called when a new instance is being created.
        """
        obj = super(ContingencyTripInlineFormSet, self).save_new(
            form, commit=False)
        driver = self.cleaned_data[0].get('driver')
        driver_vehicle_map = DriverVehicleMap._default_manager.filter(
            driver=driver).first()

        # Overriding the object values with updated values
        contract = obj.order.customer_contract
        obj.driver = driver
        obj.customer_contract = contract
        obj.order = self.cleaned_data[0].get('order')
        obj.vehicle = driver_vehicle_map.vehicle
        obj.is_contingency = True
        obj.planned_start_time = self.forms[
            0].cleaned_data.get('planned_start_time')

        ''' update driver for order '''
        Order.objects.filter(id=obj.order.id).update(driver=driver)

        if commit:
            ''' save Trip'''
            obj.save()
        return obj


def EditBookingOrder(city, vehicle_class):
    class BookingOrderEditForm(forms.ModelForm):

        suspension_trip_reason = forms.CharField(
            widget=forms.HiddenInput(),
            max_length=100,
            required=False)

        last_modified = forms.DateTimeField(
            widget=forms.HiddenInput(),
            required=False)

        trip_status = forms.CharField(
            widget=forms.HiddenInput(),
            max_length=100,
            required=False)

        def __init__(self, *args, **kwargs):
            customer_contract = self.base_fields.get("customer_contract")

            if customer_contract:
                customer_contract.widget.can_add_related = False
                customer_contract.widget.can_change_related = False
                customer_contract.widget.can_delete_related = False

            super(BookingOrderEditForm, self).__init__(*args, **kwargs)

            if self.fields.get('last_modified', None):
                self.fields['last_modified'].initial = self.instance.modified_date
            if self.fields.get('driver', None):
                self.fields['driver'].autocomplete = False
                self.fields['driver'].queryset = Driver.objects.filter(
                    status=StatusPipeline.ACTIVE,
                )
                # Use below code to restrict showing drivers who already are in a c2c trip
                # .exclude(
                #     id__in=Trip.objects.filter(
                #         order__order_type=settings.C2C_ORDER_TYPE,
                #         status__in=[
                #             StatusPipeline.TRIP_IN_PROGRESS,
                #             StatusPipeline.DRIVER_ACCEPTED,
                #             StatusPipeline.TRIP_ALL_STOPS_DONE,
                #             StatusPipeline.TRIP_NEW
                #         ]
                #     ).exclude(
                #         driver=kwargs.get('instance').driver if kwargs.get('instance') else None
                #     ).values_list('driver', flat=True)
                # )
            if self.fields.get('customer_contract', None):
                self.fields['customer_contract'].autocomplete = False
                self.fields['customer_contract'].queryset = Contract.objects.filter(
                    status=CONTRACT_STATUS_ACTIVE,
                    contract_type__in=(CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME),
                    city=city,
                    vehicle_classes=vehicle_class
                )

            if 'instance' in kwargs:
                if self.fields.get('shipping_address'):
                    self.fields['shipping_address'].autocomplete = False
                    self.fields['shipping_address'].queryset = \
                        ShippingAddress.objects.filter(
                            id=self.instance.shipping_address.id)

                if self.fields.get('pickup_address'):
                    self.fields['pickup_address'].autocomplete = False
                    self.fields['pickup_address'].queryset = \
                        ShippingAddress.objects.filter(
                            id=self.instance.pickup_address.id)

                if self.fields.get('customer_contract'):
                    self.fields['customer_contract'].empty_label = None

                if self.instance.status != StatusPipeline.ORDER_DELIVERED and \
                    self.fields.get('coupon', None):
                    self.fields['coupon'].autocomplete = False
                    self.fields['coupon'].queryset = Coupon.objects.filter(
                        status=StatusPipeline.ACTIVE,
                        start_time__lte=timezone.now(),
                        end_time__gte=timezone.now(),
                        cities=self.instance.city,
                    )

        def clean(self, *args, **kwargs):
            if self.cleaned_data:
                # Temporarily enabled pickup-datetime
                modified_date = self.instance.modified_date

                # ord_pickup_datetime = utc_to_ist(
                #     self.instance.pickup_datetime).strftime("%Y-%m-%d %H:%M")
                # pickup_datetime = self.cleaned_data.get('pickup_datetime')
                # current_time = utc_to_ist(
                #     timezone.now()).strftime("%Y-%m-%d %H:%M")
                #
                # if pickup_datetime:
                #     pickup_datetime = pickup_datetime.strftime("%Y-%m-%d %H:%M")
                #
                #     # Pickup date time should be future date/time
                #     if ord_pickup_datetime != pickup_datetime and \
                #             pickup_datetime < current_time:
                #         raise ValidationError({
                #             'pickup_datetime':
                #                 'Pick up time cannot be past Date/Time'
                #         })
                error_msg = []

                old_order = Order.objects.get(id=self.instance.id)
                if old_order.modified_date > self.cleaned_data.get('last_modified'):
                    raise ValidationError('Order Edited from Different Window Refresh the page to Continue Editing...')
                if old_order:
                    payment_status = self.cleaned_data.get('payment_status')
                    payment_mode = self.cleaned_data.get('payment_mode')
                    revised_cost = self.cleaned_data.get('revised_cost')
                    adjustment_reason = self.cleaned_data.get('adjustment_reason')
                    status = self.instance.status

                    if status != StatusPipeline.ORDER_DELIVERED and payment_status == PAYMENT_STATUS_PAID:
                        error_msg.append({
                            'payment_status':
                                'Complete order to choose payment status as Paid'
                        })
                    if payment_status == PAYMENT_STATUS_PAID and payment_mode != PAYMENT_MODE_CASH and \
                        payment_status != old_order.payment_status:
                        error_msg.append({
                            'payment_mode':
                                'can only choose cash when marking paid from admin'
                        })

                    coupon = self.cleaned_data.get('coupon', None)
                    if coupon:
                        stored_pickup_datetime = utc_to_ist(
                            self.instance.pickup_datetime).strftime(
                            "%Y-%m-%d %H:%M")
                        new_pickup_datetime = self.cleaned_data.get(
                            'pickup_datetime', None)

                        if not self.instance.coupon_id:
                            is_available, message = CouponMixin().validate_coupon_usage(
                                coupon, self.instance, new_pickup_datetime)
                            if not is_available:
                                raise ValidationError(_(message))

                        if stored_pickup_datetime != new_pickup_datetime and \
                            not coupon.is_active(new_pickup_datetime):
                            raise ValidationError(_(
                                'Coupon will not be valid for selected pickup '
                                'datetime. Remove the coupon before changing '
                                'pickup datetime.')
                            )

                    # if payment_status == PAYMENT_STATUS_PAID and not payment_mode:
                    #     error_msg.append({
                    #         'payment_mode':
                    #             'Please enter payment mode'
                    #     })
                    #
                    # if payment_status != old_order.payment_status:
                    #     error_msg.append({
                    #         'payment_status':
                    #             "You can't change payment status"
                    #     })
                    # if not \
                    #     self.cleaned_data.get('reason_for_marking_paid', None):
                    #     error_msg.append({
                    #         'reason_for_marking_paid':
                    #             'Please, provide a reason for marking order as '
                    #             'paid from admin'
                    #     })

                    if old_order.payment_status == PAYMENT_STATUS_PAID and old_order.payment_mode in [
                        PAYMENT_MODE_RAZORPAY, PAYMENT_MODE_PAYTM]:
                        # if payment_status == PAYMENT_STATUS_UNPAID:
                        #     error_msg.append({
                        #         'payment_status':
                        #             "You can't change payment status when paid Online"
                        #     })
                        if revised_cost and revised_cost != old_order.revised_cost:
                            error_msg.append({
                                'revised_cost':
                                    "You can't change cost when paid online"
                            })

                    if revised_cost and old_order.revised_cost != revised_cost and not adjustment_reason:
                        error_msg.append({
                            'adjustment_reason':
                                'Please Enter reason for adjustment'
                        })

                    # if old_order.payment_mode != payment_mode:
                    #     if payment_mode == PAYMENT_MODE_RAZORPAY:
                    #         error_msg.append({
                    #             'payment_mode':
                    #                 "You can't choose payment mode as Online"
                    #         })
                    #     if old_order.payment_status == PAYMENT_STATUS_PAID and \
                    #         old_order.payment_mode in [PAYMENT_MODE_RAZORPAY, PAYMENT_MODE_PAYTM]:
                    #         error_msg.append({
                    #             'payment_mode':
                    #                 "You can't change payment mode when Paid Online"
                    #         })

                if error_msg:
                    raise ValidationError(error_msg)

                customer_contract = self.cleaned_data.get('customer_contract')
                if customer_contract:
                    self.instance.trip_set.filter(
                        ~Q(customer_contract=customer_contract) & Q(
                            status__in=[StatusPipeline.TRIP_IN_PROGRESS,
                                        StatusPipeline.TRIP_NEW,
                                        StatusPipeline.DRIVER_ACCEPTED,
                                        StatusPipeline.TRIP_ALL_STOPS_DONE])
                    ).update(customer_contract=customer_contract)

    return BookingOrderEditForm


class TripInlineForm(forms.ModelForm):

    def clean(self, *args, **kwargs):
        actual_end_time = self.cleaned_data.get('actual_end_time')
        actual_start_time = self.cleaned_data.get('actual_start_time')
        driver = self.instance.driver
        if actual_end_time and actual_start_time:
            if actual_end_time < actual_start_time:
                raise ValidationError(
                    "End time should be greater than start time")

        local_tz = pytz.timezone(settings.ACT_TIME_ZONE)
        current_time = timezone.now().astimezone(
            local_tz)

        important_fields = ['actual_end_time', 'actual_start_time']

        if actual_end_time and actual_end_time > current_time:
            raise ValidationError(
                'You cant choose Future time for Ended At time'
            )

        is_important_field_changed = len([i for i in important_fields if i in self.changed_data]) > 0

        if actual_start_time and actual_end_time and is_important_field_changed:
            query = Q(driver=driver) & (Q(actual_start_time__range=(actual_start_time, actual_end_time))
                                        | Q(actual_end_time__range=(actual_start_time, actual_end_time))
                                        | Q(actual_start_time__lte=actual_start_time,
                                            actual_end_time__gte=actual_end_time)
                                        | Q(actual_start_time__gte=actual_start_time,
                                            actual_end_time__lte=actual_end_time)
                                        | Q(actual_start_time__gte=actual_start_time,
                                            actual_start_time__lte=actual_end_time)
                                        | Q(actual_start_time__lte=actual_start_time, actual_end_time__isnull=True))

            existing_trip = Trip.objects.filter(query).exclude(id=self.instance.id).exclude(
                status__in=[StatusPipeline.TRIP_NO_SHOW, StatusPipeline.DUMMY_STATUS, StatusPipeline.TRIP_SUSPENDED,
                            StatusPipeline.TRIP_SCHEDULED_OFF, StatusPipeline.TRIP_CANCELLED])

            existing_trip = existing_trip.select_related('order', 'order__customer')

            if existing_trip.exists():
                trip = Trip.objects.get(id=self.instance.id)
                error_trip = trip.validate_trip_conflict(existing_trip)
                raise ValidationError(
                    'Trip Actual start time / Actual End time conflicting with some other Trips'
                    ' or Driver has In-Progress Trip check %s' % error_trip
                )
        # if self.instance.status == StatusPipeline.TRIP_COMPLETED:
        #     if not actual_start_time or not actual_end_time:
        #         raise ValidationError(
        #             'Trip Actual start time / Actual End time is Mandatory for completed Trips'
        #         )


class IkeaOrderForm(forms.ModelForm):
    customer_mobile = forms.CharField(required=True, max_length=10)
    customer_name = forms.CharField(required=True, max_length=100)
    labour_amount = forms.IntegerField(required=True, initial=0)

    def __init__(self, *args, **kwargs):
        super(IkeaOrderForm, self).__init__(*args, **kwargs)

        last_id = ShippingAddress.objects.all().order_by('-id')[15].id
        address = ShippingAddress.objects.filter(id__gte=last_id)
        if self.fields.get('shipping_address'):
            self.fields['pickup_address'].autocomplete = False
            self.fields['shipping_address'].queryset = address

        if self.fields.get('pickup_address'):
            self.fields['pickup_address'].autocomplete = False
            self.fields['pickup_address'].queryset = address

        if self.fields.get('customer_contract'):
            self.fields['customer_contract'].autocomplete = False
            self.fields['customer_contract'].queryset = Contract.objects.filter(
                contract_type=CONTRACT_TYPE_KIOSK)

        if self.fields.get('driver'):
            self.fields['driver'].autocomplete = False
            self.fields['driver'].queryset = Driver.objects.filter(operating_city_id=3, status=StatusPipeline.ACTIVE)

    def clean(self):

        super(IkeaOrderForm, self).clean()

        contract = self.cleaned_data.get('customer_contract')
        if not contract:
            raise ValidationError({'customer_contract': 'contract is mandatory'})

    class Meta:
        model = Order
        fields = '__all__'


class IkeaEditOrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(IkeaEditOrderForm, self).__init__(*args, **kwargs)

        if self.fields.get('driver'):
            self.fields['driver'].autocomplete = False
            self.fields['driver'].queryset = Driver.objects.filter(operating_city=self.instance.city,
                                                                   status=StatusPipeline.ACTIVE)

        shipping_address = ShippingAddress.objects.filter(id=self.instance.shipping_address_id)
        pickup_address = ShippingAddress.objects.filter(id=self.instance.pickup_address_id)
        if self.fields.get('shipping_address'):
            self.fields['shipping_address'].autocomplete = False
            self.fields['shipping_address'].queryset = shipping_address

        if self.fields.get('pickup_address'):
            self.fields['pickup_address'].autocomplete = False
            self.fields['pickup_address'].queryset = pickup_address

            if self.fields.get('customer_contract'):
                self.fields['customer_contract'].autocomplete = False
                self.fields['customer_contract'].queryset = Contract.objects.filter(
                    contract_type=CONTRACT_TYPE_KIOSK)

    def clean(self):

        super(IkeaEditOrderForm, self).clean()

        contract = self.cleaned_data.get('customer_contract')
        revised_cost = self.cleaned_data.get('revised_cost')
        adjustment_reason = self.cleaned_data.get('adjustment_reason')
        if not contract:
            raise ValidationError({'customer_contract': 'contract is mandatory'})
        old_order = Order.objects.get(id=self.instance.id)

        if revised_cost and old_order.revised_cost != revised_cost and not adjustment_reason:
            raise ValidationError({
                'adjustment_reason':
                    'Please Enter reason for adjustment'
            })

    class Meta:
        model = Order
        fields = '__all__'


class BookingOrderActionForm(ActionForm):
    reason_for_marking_paid = forms.CharField(widget=forms.Textarea(
        attrs={'placeholder': 'Reason for marking order as paid (required)',
               'cols': 25, 'rows': 2}),
        label='', required=False
    )
