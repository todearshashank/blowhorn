"""Post save and Pre save signals to be executed here on order object save."""
from datetime import datetime, timedelta
from django.utils import timezone
import os
import logging
from django.conf import settings
from django.dispatch import Signal, receiver
from django.db import InternalError
from django.db.models.signals import post_save

from blowhorn.address.models import Slots
from blowhorn.oscar.core.loading import get_model
from blowhorn.common import sms_templates
from config.settings.status_pipelines import (
    DRIVER_ARRIVED_AT_DROPOFF,
    DRIVER_ACCEPTED,
    ORDER_DELIVERED
)
from blowhorn.freshchat.whatsapp import send_whatsapp_notification
from blowhorn.shopify.tasks import create_shopify_event
from blowhorn.order.const import STATUS_UPDATE, ORDER_UPDATES_FIREBASE_PATH, NEW_BOOKING
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME
from blowhorn.order.const import PAYMENT_STATUS_UNPAID
from blowhorn.order.models import OrderConstants

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
UTMTracking = get_model('order', 'UTMTracking')

new_order_notification = Signal(
    providing_args=["order", "request", "response"])

order_placed = Signal(providing_args=["order", "data"])


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

@receiver(post_save, sender=Event)
def event_created(sender, instance, created, update_fields=None, **kwargs):
    logger.info("Order event trigger for oid %s & event %s" % (instance.order.id, instance.status))
    if created and instance.order.source == 'shopify' and instance.order.shopify_fulfillment_id:
        create_shopify_event.apply_async(args=[instance.order.id])

def dispatch_order(order):
    """
    Slot based order dispatch.
    """
    from blowhorn.order.mixins import OrderPlacementMixin
    from blowhorn.order.tasks import notify
    pickup_datetime = OrderPlacementMixin().get_formatted_datetime(
        order.pickup_datetime, order.device_type)
    time_difference = pickup_datetime.replace(
        tzinfo=None) - datetime.now()
    if not order.vehicle_class_preference:
        return

    for i in range(0, settings.DISPATCH_COUNT):
        if (time_difference.total_seconds() / 3600) <= OrderConstants().get('PRE_NOTIFICATION_HOURS', 1):
            # Send Signal to transmit this order to a nearby driver.
            notify.apply_async((order.id,), eta=timezone.now()+timedelta(minutes=(i*settings.DISPATCH_INTERVAL)))

        else:
            interval = OrderConstants().get('PRE_NOTIFICATION_HOURS', 1) * 60 - timedelta(minutes=(i*settings.DISPATCH_INTERVAL))
            if interval >= 0:
                notify.apply_async(
                    (order.id,), eta=pickup_datetime - timedelta(minutes=interval))

@receiver(post_save, sender=Order)
def broadcast_bookings_updates(sender, instance, created, update_fields=None, **kwargs):
    """Post save signal on order.save()."""
    from blowhorn.order.tasks import (
        broadcast_booking_updates_admin,
        broadcast_booking_updates_customer,
        send_mail, send_status_notification,
        send_c2c_slack_notification,
        broadcast_spot_order_updates_admin
    )

    logger.info('Order post save signal received..')
    instance.message_type = STATUS_UPDATE
    if created:
        instance.message_type = NEW_BOOKING
        slots = Slots.objects.filter(customer=instance.customer, is_dispatch_required=True, hub=instance.pickup_hub).exists()
        if slots:
            # from ..customer.views import OrderCreate
            dispatch_order(instance)

    if hasattr(instance, 'event_data'):
        track_url = '%s/track/%s' % (os.environ.get('HTTP_HOST', 'blowhorn.com'), instance.number)
        invoice_url = '%s/invoice/%s' % (os.environ.get('HTTP_HOST', 'blowhorn.com'), instance.number)

        if instance.status == DRIVER_ARRIVED_AT_DROPOFF and instance.order_type == settings.C2C_ORDER_TYPE:
            message = sms_templates.template_truck_arrived_at_dropoff['text']
            instance.customer.user.send_sms(message, sms_templates.template_truck_arrived_at_dropoff)
            msg_template_data = {"template_name": "driver_arrival_at_drop",
                                 "params_data": [instance.customer.name,
                                                 instance.number,
                                                 instance.driver.driver_vehicle]}
            send_whatsapp_notification([settings.ACTIVE_COUNTRY_CODE + str(
                instance.customer.user.phone_number.national_number)],
                                       msg_template_data, instance)

        if instance.status == ORDER_DELIVERED and \
                instance.order_type == settings.C2C_ORDER_TYPE:

            if instance.payment_status == PAYMENT_STATUS_UNPAID:
                # this is a temporary fix will be removed
                if settings.BRAND_NAME == 'MzansiGo':
                    template_dict = sms_templates.template_order_completion
                    message = template_dict['text'] % (
                        settings.BRAND_NAME,
                        instance.number, track_url
                    )

                else:
                    template_dict = sms_templates.template_booking_completion
                    message = template_dict['text'] % (
                        instance.number, (float(instance.total_payable) - float(instance.amount_paid_online)), track_url)
                
                bill_amount = float(instance.total_payable) - float(instance.amount_paid_online)
                msg_template_data = {"template_name": "booking_end",
                                     "params_data": [instance.customer.name,
                                                     instance.number,
                                                     bill_amount,
                                                     track_url]}
                send_whatsapp_notification([settings.ACTIVE_COUNTRY_CODE + str(
                    instance.customer.user.phone_number.national_number)],
                                           msg_template_data, instance)
            else:
                template_dict = sms_templates.prepaid_template_booking_completion
                message = template_dict['text'] % (instance.number, invoice_url)
            instance.customer.user.send_sms(message, template_dict)
            send_mail.apply_async((instance.id,), )

        if instance.status == DRIVER_ACCEPTED and \
                instance.order_type == settings.C2C_ORDER_TYPE:
            driver = instance.driver
            vehicle_class = instance.vehicle_class
            vehicle_rc = instance.vehicle_rc
            template_dict = sms_templates.template_driver_acceptance
            message = template_dict['text'] % (
                instance.number, driver.name, str(driver.user.phone_number),
                str(vehicle_class), vehicle_rc, track_url)
            instance.customer.user.send_sms(message, template_dict)

        event_data = instance.event_data
        event_data['order'] = instance
        # @todo use serializer to store data
        event = Event(**event_data)
        event.save()

        if instance.order_type == settings.C2C_ORDER_TYPE \
            and instance.customer_contract.contract_type in \
                [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]:
            send_c2c_slack_notification.apply_async((instance.id, ), )

            # To Customer
            channel_name = ORDER_UPDATES_FIREBASE_PATH % instance.number
            broadcast_booking_updates_customer.apply_async(args=[
                instance.pk, channel_name, STATUS_UPDATE
            ])

    if hasattr(instance, '_send_status_notification'):
        logger.info('Sending Order %s Status notification to %s',
                    instance.number, instance.customer.user.name)

        send_status_notification.delay(
            instance.id,
            instance.number,
            instance.status,
            instance.reference_number,
            instance.customer.notification_url,
            instance.customer.ecode,
            instance.customer.user.name
        )

    if instance.order_type == settings.C2C_ORDER_TYPE:
        # To Admin
        channel_name = settings.FIREBASE_ADMIN_NEW_BOOKING_CHANNEL
        broadcast_booking_updates_admin.apply_async(
            args=[instance.pk, channel_name, instance.message_type])

    if instance.order_type == settings.SPOT_ORDER_TYPE:
        # To Admin
        channel_name = settings.FIREBASE_ADMIN_NEW_SPOT_ORDER_CHANNEL
        broadcast_spot_order_updates_admin.apply_async(
            args=[instance.pk, channel_name, instance.message_type])


@receiver(order_placed)
def order_created(sender, instance, data, **kwargs):
    utm_data = data.get('utm_data', {})
    utm_data['order'] = instance
    try:
        UTMTracking.objects.create(**utm_data)
    except InternalError as ie:
        logger.info('Failed to save UTM data. %s' % ie)
