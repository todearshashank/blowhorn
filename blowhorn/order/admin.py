# System and Django Libraries
import os
import csv
import logging
import simplejson as json
from django import forms
from django.conf import settings
from django.contrib import messages, admin
from django.contrib.admin.helpers import ActionForm
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe

from django.utils.html import format_html
import pytz
from datetime import datetime
from django.db.models import (
    ExpressionWrapper, F, Q, Prefetch, Sum, CharField, Subquery,
    PositiveIntegerField, Case, When, Value, Min, Max, OuterRef, Func
)
from datetime import timedelta
from django.http import HttpResponse
from django.contrib.postgres.aggregates import StringAgg

# 3rd Party Libraries
from blowhorn.oscar.core.loading import get_model
from array_tags import widgets
from mapwidgets.widgets import GooglePointFieldWidget

# Blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.admin import BaseAdmin
from blowhorn.oscar.core.loading import get_model
from blowhorn.trip.models import Trip, Stop, MIDDLE_MILE, FIRST_MILE, LAST_MILE
from blowhorn.order.filters import DateFieldFilter
from blowhorn.driver.models import Driver, PaymentTrip, APPROVED, DriverPayment, \
    DriverRatingDetails, RatingConfig, DriverVehicleMap
from blowhorn.contract.models import Contract
from blowhorn.address.utils import AddressUtil
from blowhorn.address.models import City, Hub
from blowhorn.contract.constants import (
    CONTRACT_TYPE_SHIPMENT,
    CONTRACT_STATUS_DRAFT,
    CONTRACT_TYPE_KIOSK,
    CONTRACT_TYPE_C2C,
    CONTRACT_TYPE_SME,
)

from blowhorn.order.const import ADMIN_PAYMENT_TAB_DATA, NEW_BOOKING, ORDER_TYPE_NOW, \
    CURRENT_BOOKING, ADVANCED_BOOKING, ORDER_STATUS_CODE, PAYMENT_STATUS_PAID, \
    ORDER_STATUS_FILTER_CONFIG, \
    SHIPMENT_ADMIN_EXPORT_FIELDS, SHIPMENT_ADMIN_EXPORT_COLUMN_HEADERS, \
    ADMIN_CUSTOMER_TAB_DATA, ADMIN_GENERAL_TAB_DATA, SHIPMENT_ORDER_EXPORT_COLUMN_HEADERS, SHIPMENT_ORDER_EXPORT_FIELDS
from config.settings.status_pipelines import TRIP_IN_PROGRESS
from .resource import ShipmentOrderResource, OrderResource
from blowhorn.order.tasks import send_restore_notification_to_driver_app
from .forms import (
    ShipmentOrderEditForm, ShipmentOrderAddForm, TripInlineForm,
    AddSpotOrder, ShippingAddressForm, EditBookingOrder,
    SpotOrderEditForm, FixedOrderEditForm, DeafcomOrderEditForm, AddDeafcomOrder,
    OrderInternalTrackingFormset, IkeaEditOrderForm, IkeaOrderForm,
    BookingOrderActionForm,
)
from blowhorn.utils.functions import utc_to_ist, timedelta_to_hmsstr, \
    minutes_to_dhms
from .models import OrderCostHistory, ItemCategory, OrderCancellationReason, CallMaskingLog
from blowhorn.trip.utils import TripService
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from .utils import OrderUtil, get_trip
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, Partner
from blowhorn.order.const import IT_STATUS_NEW, IT_STATUS_FOLLOWUP, IT_STATUS_CONFIRMED, \
    IT_STATUS_CANCELLED, PAYMENT_MODE_PAYTM, PAYMENT_MODE_CASH, PAYMENT_MODE_BANK_TRANSFER

from blowhorn.driver.tasks import restore_driver_app
from blowhorn.order.models import OrderInternalTracking, OrderAdjustment, \
    OrderAdjustmentCategory, OrderDispatch, OrderPaymentHistory
from .order_transition_conditions import (
    is_ongoing_trip
)
from blowhorn.utils.datetimepicker import DateTimePicker
from blowhorn.document.widgets import ImagePreviewWidget
from blowhorn.order.utils import OrderExportClass
from blowhorn.address.models import State
from blowhorn.utils.filters import multiple_choice_list_filter
from blowhorn.utils.datetime_function import DateTime
from blowhorn.trip.mixins import TripMixin
from ..apps.mixins import CreateModelMixin
from ..common.fields import CustomModelChoiceField
from ..trip.serializers import TripSerializer

Labour = get_model('order', 'Labour')
LrNumber = get_model('order', 'LrNumber')
WayPoint = get_model('order', 'WayPoint')
Dimension = get_model('order', 'Dimension')
Order = get_model('order', 'Order')
OrderLineHistory = get_model('order', 'OrderLineHistory')
OrderHistory = get_model('order', 'OrderHistory')
OrderLineHistoryBatch = get_model('order', 'OrderLineHistoryBatch')
OrderLine = get_model('order', 'OrderLine')
OrderDocument = get_model('order', 'OrderDocument')
ShippingAddress = get_model('order', 'ShippingAddress')
OrderConstants = get_model('order', 'OrderConstants')
Event = get_model('order', 'Event')
ShipmentAlertConfiguration = get_model(
    'customer', 'ShipmentAlertConfiguration')
Customer = get_model('customer', 'Customer')
Vehicle = get_model('vehicle', 'Vehicle')
Source = get_model('payment', 'Source')
OrderBatch = get_model('order', 'OrderBatch')
Container = get_model('order', 'Container')
OrderContainer = get_model('order', 'OrderContainer')
OrderContainerHistory = get_model('order', 'OrderContainerHistory')
OrderMessage = get_model('order', 'OrderMessage')
OrderAwbSequence = get_model('order', 'OrderAwbSequence')
OrderApiData = get_model('order', 'OrderApiData')
ShippingLabelRequest = get_model('order', 'ShippingLabelRequest')

tz_diff = DateTime.get_timezone_offset()

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DocumentsInline(admin.TabularInline):
    formfield_overrides = {
        models.DateField: {
            "widget": DateTimePicker(options={"format": "DD-MM-YYYY"})
        },
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    model = OrderDocument
    exclude = ('stop', 'event')
    extra = 0
    can_delete = False


class OrderLineInline(admin.TabularInline):
    formfield_overrides = {
        models.DateField: {
            "widget": DateTimePicker(options={"format": "DD-MM-YYYY"})
        }
    }
    model = OrderLine
    extra = 0
    can_delete = False

    def get_dimension(self, obj):
        dim = None
        if obj.dimension:
            dim = '{}*{}*{} {}'.format(obj.dimension.length,
                                       obj.dimension.breadth,
                                       obj.dimension.height,
                                       obj.dimension.uom)
        return dim

    get_dimension.short_description = 'Dimension'

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = [f.name for f in self.model._meta.fields]
        read_only_fields.append('get_dimension')
        return read_only_fields


@admin.register(OrderBatch)
class OrderBatchAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'source', 'start_time', 'description', 'number_of_entries', 'end_time')

    fields = ('start_time', 'description', 'number_of_entries', 'successfull',
                'file', 'end_time', 'source' , 'customer',  'summary')

    readonly_fields = ('start_time', 'description', 'number_of_entries', 'successfull',
                            'file', 'end_time', 'source' , 'customer',  'summary')

    search_fields = ['id']
    list_filter = [('start_time', DateRangeFilter), 'source']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    actions = None


class SpotOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('Spot Order')
        verbose_name_plural = _('Spot Orders')


class DefcomOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('Defcom Order')
        verbose_name_plural = _('Defcom Orders')


class OndemandOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('Ondemand Order')
        verbose_name_plural = _('Ondemand Orders')


class KioskOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('Kiosk Order')
        verbose_name_plural = _('Kiosk Orders')


class FixedOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('Fixed Order')
        verbose_name_plural = _('Fixed Orders')


class BookingOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('C2C/SME Booking')
        verbose_name_plural = _('C2C/SME Bookings')


class ShipmentOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('Shipment Order')
        verbose_name_plural = _('Shipment Orders')


class NewShipmentOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('New Shipment Order')
        verbose_name_plural = _('New Shipment Orders')


class InterCityOrder(Order):
    class Meta:
        proxy = True
        verbose_name = _('Inter City Order')
        verbose_name_plural = _('Inter City Orders')


def apply_contract_permission_filter(user, qs):
    """ Common filter to apply for filtering out contracts
        based on access level
    """
    if not user.is_superuser:
        cities = City.objects.get_cities_for_managerial_user(user)
        contracts = Contract.objects.filter(Q(spocs=user) |
                                            Q(supervisors=user) |
                                            Q(city__in=cities)).distinct()
        if qs.model in [ShipmentOrder, FixedOrder, SpotOrder, OndemandOrder, DefcomOrder]:
            qs = qs.filter(customer_contract__in=contracts)
            # qs = qs.filter(city__in=cities)
        elif qs.model in [Contract]:
            qs = qs.filter(id__in=contracts)
    #        qs = qs.distinct()
    return qs


class WaypointAdminInline(admin.TabularInline):
    verbose_name = _('Waypoint')
    verbose_name_plural = _('Waypoints')
    model = WayPoint
    fields = ('sequence_id', 'contact_phone_number', 'contact_name', 'shipping_address',)
    extra = 0
    max_num = 0
    obj = None

    def contact_phone_number(self, obj):
        return obj.shipping_address.phone_number if obj.shipping_address else ''

    def contact_name(self, obj):
        return obj.shipping_address.first_name if obj.shipping_address else ''

    contact_phone_number.short_description = 'Contact Phone Number'
    contact_name.short_description = 'Contact Name'

    def get_readonly_fields(self, request, obj=None):
        self.obj = obj

        readonly_fields = ['contact_phone_number', 'contact_name', 'sequence_id']
        if obj and obj.status in StatusPipeline.ORDER_END_STATUSES:
            readonly_fields += ['shipping_address']
        return readonly_fields

    def get_formset(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        return super().get_formset(request, obj, **kwargs)

    def get_queryset(self, request):
        # Exclude the waypoint which points to the dropoff address
        # already present in the order
        qs = super().get_queryset(request)
        # qs = qs.exclude(order__shipping_address=F('shipping_address'))
        return qs.order_by('sequence_id')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'shipping_address':
            qs = ShippingAddress.objects.filter(
                waypoint__order=self.parent_obj
            ).select_related('country')
            kwargs['queryset'] = qs

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class LabourInline(admin.TabularInline):
    verbose_name = _('Labour')
    verbose_name_plural = _('Labours')
    model = Labour
    extra = 0

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.status in [StatusPipeline.ORDER_DELIVERED, StatusPipeline.ORDER_CANCELLED]:
            return ['labour_cost', 'no_of_labours']
        else:
            return []


class OrderInternalTrackingInline(admin.TabularInline):
    model = OrderInternalTracking
    formset = OrderInternalTrackingFormset
    extra = 0
    can_delete = False
    readonly_fields = ('associate', 'time')

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        formset.request = request
        return formset


class TripInline(admin.TabularInline):
    model = Trip
    fields = (
        'link_to_trip', 'status', 'driver', 'vehicle',
        'planned_start_time', 'actual_start_time',
        'actual_end_time', 'reason_for_suspension')
    extra = 0
    can_delete = False
    readonly_fields = (
        'link_to_trip', 'status', 'driver', 'vehicle',
        'planned_start_time', 'actual_start_time',
        'actual_end_time', 'reason_for_suspension')

    verbose_name = _('Trip')
    verbose_name_plural = _('Trips')

    def link_to_trip(self, obj):
        if obj:
            link = reverse("admin:trip_trip_change", args=[obj.id])
            return format_html("<a href='{}?_changelist_filters=q%{}%'>{}</a>", link,
                               obj.trip_number, obj.trip_number)
        else:
            return None

    link_to_trip.allow_tags = True
    link_to_trip.short_description = 'Trip'

    # To disable the Add button
    def has_add_permission(self, request):
        return False


class LrNumberAdminInline(admin.TabularInline):
    model = LrNumber
    extra = 0


class HistoryAdminInline(admin.TabularInline):
    model = OrderHistory
    extra = 0
    can_delete = False
    readonly_fields = [
        'order', 'field', 'old_value', 'new_value', 'user', 'modified_time'
    ]

    def has_add_permission(self, request):
        return False


class EventAdminInline(admin.TabularInline):
    model = Event
    extra = 0
    readonly_fields = [
        'hub_associate', 'status', 'created_date', 'created_by', 'time',
        'driver', 'get_current_location', 'remarks', 'address', 'hub'
    ]

    exclude = ['current_location']

    can_delete = False

    def get_fieldsets(self, request, obj=None):
        if obj and obj.order_type == settings.C2C_ORDER_TYPE:
            fieldsets = (
                ('General', {
                    'fields': (
                        ('status', 'created_date', 'created_by',
                         'driver', 'get_current_location', 'address', 'remarks'),
                    )
                }),
            )
        else:
            fieldsets = (
                ('General', {
                    'fields': (
                        ('hub_associate', 'status', 'time', 'hub',
                         'driver', 'get_current_location', 'remarks'),
                    )
                }),
            )
        return fieldsets

    def get_current_location(self, obj):
        if obj.current_location:
            return format_html("""
                <a href="http://maps.google.com/maps?q=%s,%s" target="_blank">
                %s %s
                </a>
                """ % (
                obj.current_location.y, obj.current_location.x,
                obj.current_location.y, obj.current_location.x
            )
                               )
        return None

    get_current_location.short_description = 'Current Location'

    # To disable the Add button on admin
    def has_add_permission(self, request, obj):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver', 'created_by', )
        return qs


class DriverRatingInline(admin.TabularInline):
    model = DriverRatingDetails
    extra = 0
    readonly_fields = ('rating', 'remark', 'customer_remark', 'driver')
    fields = ('rating', 'remark', 'customer_remark', 'driver')
    can_delete = False

    # To disable the Add button on admin
    def has_add_permission(self, request, obj):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver', 'remark', )
        return qs


class ShipmentAlertFilter(admin.SimpleListFilter):
    title = _('Shipment Alert')
    parameter_name = 'Shipment Alert'

    def lookups(self, request, model_admin):

        choices = ['Yes', 'No']
        return [(c, c) for c in choices]

    def queryset(self, request, queryset):

        if self.value():
            if self.value() == 'Yes':
                return queryset.exclude(
                    status__in=[
                        StatusPipeline.ORDER_DELIVERED,
                        StatusPipeline.DELIVERY_ACK],
                    updated_time__lte=F('expected_delivery_time'))

        return queryset


class ShipmentContractFilter(admin.SimpleListFilter):
    title = _('contract')
    parameter_name = 'customer_contract_id'

    def lookups(self, request, model_admin):
        contracts = Contract.objects.exclude(
            status=CONTRACT_STATUS_DRAFT
        ).filter(contract_type=CONTRACT_TYPE_SHIPMENT)

        contracts = apply_contract_permission_filter(request.user, contracts)
        return [(c.id, c.name) for c in contracts]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(customer_contract_id=self.value())

        return queryset


def assign_driver(queryset, ids, driver, request, trip=None,
                  is_kiosk_order=False, return_json=False):
    from blowhorn.apps.driver_app.v3.services.order.scan_orders import ScanOrderMixin
    proceed = True
    records_with_end_statuses = []
    records_with_ongoing_trip = []  # orders fulfilled by other drivers in an
    # ongoing trip

    # queryset = queryset.prefetch_related('stops', 'stops__trip')
    order_numbers = []
    wms_orders = None
    if not isinstance(queryset, list):
        wms_orders = queryset.filter(customer_contract__is_wms_contract=True).values_list('number', flat=True)

    if wms_orders and wms_orders.exists():
        message = "Use WMS app to assign WMS orders: %s" % list(wms_orders)
        if return_json:
            return {
                'status': False,
                'message': message
            }
        messages.error(request, message)
        return False

    for order in queryset:
        order_numbers.append(order.number)
        if order.status in StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES:
            proceed = False
            records_with_end_statuses.append(order.number)

        elif order.status in [StatusPipeline.OUT_FOR_DELIVERY, StatusPipeline.DRIVER_ASSIGNED,
                              StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                              StatusPipeline.ORDER_MOVING_TO_HUB, StatusPipeline.ORDER_MOVING_TO_HUB_RTO,
                              StatusPipeline.ORDER_OUT_FOR_RTO] or \
            (order.status == StatusPipeline.ORDER_NEW and
             order.driver_id is not None):

            dict_ = {}
            stop = order.stops.exclude(trip__status__in=StatusPipeline.TRIP_END_STATUSES).first()

            if stop:
                dict_['Order'] = order.number
                dict_['Trip'] = stop.trip.trip_number
                proceed = False
                records_with_ongoing_trip.append(dict_)

    if not proceed:
        if records_with_ongoing_trip:
            message = "Orders fulfilled by other ongoing trip: %s" % \
                      records_with_ongoing_trip
            if return_json:
                return {
                    'status': False,
                    'message': message
                }
            messages.error(request, message)

        if records_with_end_statuses:
            messages.error(
                request,
                "Orders are in end statuses, Cannot create Trip: "
                "%s" % records_with_end_statuses
            )

        if return_json:
            message = "Orders fulfilled by other ongoing trip: %s" % \
                      records_with_ongoing_trip or records_with_end_statuses
            return {
                'status': False,
                'message': message
            }
        return False

    try:
        trip = ScanOrderMixin(
            context={'request': request},
            trip_id=trip.id if trip else None,
            hub_associate=request.user
        ).create_trip_stops_for_shipment_orders(
            driver=driver,
            trip_type=FIRST_MILE if order.source_city == driver.operating_city else LAST_MILE,
            order_numbers=order_numbers,
            **{'is_kiosk_order': is_kiosk_order}
        )
    except BaseException as e:
        logging.error('Driver Assigning Failed %s' % e)
        # logging.error(traceback.format_exc())
        if return_json:
            return {
                'status': False,
            }
        return False

    if trip:
        restore_driver_app(driver.id)
        if return_json:
            return {
                'status': True,
                'trip': trip
            }
        return True

    if return_json:
        return {
            'status': False
        }
    return False


def create_and_export_file(queryset, field_names, column_headers):
    """
    Fetches given set of fields from db and creates excel file
    :param queryset: order queryset (filtered)
    :param field_names: db field names (list)
    :param column_headers: header text in excel (list)
    :return: excel file as response
    """
    file_path = 'shipment_orders'
    pack_time = Event.objects.filter(
        order=OuterRef('pk'),
        status=StatusPipeline.ORDER_PACKED
    )
    picked_time = Event.objects.filter(
        order=OuterRef('pk'), status=StatusPipeline.ORDER_PICKED)
    out_on_road_time = Event.objects.filter(
        order=OuterRef('pk'),
        status=StatusPipeline.OUT_FOR_DELIVERY
    ).order_by('-time')
    picked_time = Subquery(picked_time.values('time')[:1])
    if not picked_time:
        picked_time = Subquery(out_on_road_time.values('time')[:1])

    attempted_time = Event.objects.filter(
        order=OuterRef('pk'),
        status__in=[StatusPipeline.ORDER_DELIVERED,
                    StatusPipeline.ORDER_RETURNED,
                    StatusPipeline.UNABLE_TO_DELIVER])

    delivered_time = Event.objects.filter(
        order=OuterRef('pk'),
        status__in=[StatusPipeline.ORDER_DELIVERED])

    state_query = State.objects.filter(cities=OuterRef('city_id'))
    docs = OrderDocument.objects.filter(order=OuterRef('pk'))

    queryset.select_related(
        'customer', 'customer__user', 'city', 'driver', 'driver__user',
        'shipping_address'
    ).prefetch_related(
        'trip_set', 'order_lines', 'order_lines__sku', 'documents__file'
    ).annotate(
        expected_time=Func(
            F('expected_delivery_time') + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        last_packed_at=Func(
            (Subquery(pack_time.values('time')[:1])) + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        last_out_on_road_at=Func(
            (Subquery(out_on_road_time.values('time')[:1])) + timedelta(
                hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        last_attempted_at=Func(
            (Subquery(attempted_time.order_by('time').reverse().values('time')[:1])) + timedelta(
                hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        first_attempted_at=Func(
            (Subquery(attempted_time.order_by('time').values('time')[:1])) + timedelta(
                hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        delivered_at=Func(
            (Subquery(delivered_time.values('time')[:1])) + timedelta(
                hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        _remarks=ExpressionWrapper(
            (Subquery(attempted_time.values('remarks')[:1])),
            output_field=CharField()
        ),
        # attempted_count = Count('event', filter=Q(event__status__in=[
        #        StatusPipeline.ORDER_DELIVERED,
        #        StatusPipeline.ORDER_RETURNED,
        #        StatusPipeline.UNABLE_TO_DELIVER])
        # ),
        state=ExpressionWrapper(
            (Subquery(state_query.values('cities__name')[0:1])),
            output_field=CharField()
        ),
        sku=StringAgg('order_lines__sku__name', delimiter=','),
        total_sku_qty=Sum('order_lines__quantity'),
        formatted_date_placed=Func(
            F('date_placed') + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        picked_time=Func(
            picked_time + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()),
        latitude=Func('shipping_address__geopoint', function='ST_Y'),
        longitude=Func('shipping_address__geopoint', function='ST_X'),
        pod_docs=Subquery(docs.values('file')[:1])
    ).to_csv(file_path, *field_names)

    modified_file = '%s_%s.csv' % (
        utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT),
        os.path.splitext(file_path)[0])

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)
        w.writerow(column_headers)
        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        os.remove(file_path)

    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = \
                'inline; filename=%s' % os.path.basename(modified_file)
            os.remove(modified_file)
            return response

    return HttpResponse(400, _('Failed to export file'))


def export_shipment_order_data(modeladmin, request, queryset):
    # get all records if nothing selected else only selected rows
    # if queryset.exists():
    #     order_ids = queryset.values_list('id', flat=True)
    #     queryset = Order.copy_data.filter(id__in=order_ids)
    # else:
    ids = queryset.values_list('id', flat=True)
    query = OrderExportClass().get_query(settings.SHIPMENT_ORDER_TYPE,
                                         request.GET.dict(), user=request.user)
    if 'RDS_READ_REP_1' in os.environ:
        queryset = Order.copy_data.using('readrep1').filter(query, id__in=ids)
    else:
        queryset = Order.copy_data.filter(query, id__in=ids)

    return create_and_export_file(
        queryset,
        SHIPMENT_ADMIN_EXPORT_FIELDS,
        SHIPMENT_ADMIN_EXPORT_COLUMN_HEADERS
    )


def export_shipment_data_with_documents(modeladmin, request, queryset):
    file_path = 'shipment_orders'
    ids = queryset.values_list('id', flat=True)
    query = OrderExportClass().get_query(settings.SHIPMENT_ORDER_TYPE,
                                         request.GET.dict(), user=request.user)
    if 'RDS_READ_REP_1' in os.environ:
        queryset = Order.copy_data.using('readrep1').filter(query, id__in=ids)
    else:
        queryset = Order.copy_data.filter(query, id__in=ids)

    queryset = queryset.annotate(
        formatted_date_placed=Func(
            F('date_placed') + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),
        shipping_customer_name=F('shipping_address__first_name'),
        shipping_address_line1=F('shipping_address__line1'),
        shipping_address_postcode=F('shipping_address__postcode'),
        city_name=F('city__name'),
        customer_name=F('customer__name'),
    ).only(*SHIPMENT_ORDER_EXPORT_FIELDS)

    output = []
    y = {}
    order_docs = OrderDocument.objects.filter(order_id__in=ids).only('order_id', 'file')
    for z in order_docs:
        if not z.order_id in y.keys():
            y[z.order_id] = z.file.url if z.file else ''
        else:
            y[z.order_id] = y[z.order_id] + "," + z.file.url if z.file else ''

    output.append(SHIPMENT_ORDER_EXPORT_COLUMN_HEADERS)
    for order in queryset:
        output.append([order.customer_name, order.reference_number, order.status, order.formatted_date_placed,
                       order.shipping_customer_name, order.shipping_address_line1, order.shipping_address_postcode,
                       order.number, order.remarks, order.city_name, y.get(order.id, '')])

    modified_file = '%s_%s.csv' % (
        utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT),
        os.path.splitext(file_path)[0])

    with open(modified_file, 'w', newline='') as f:
        writer = csv.writer(f)
        # writer.writerow(SHIPMENT_ORDER_EXPORT_COLUMN_HEADERS)
        writer.writerows(output)

    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = \
                'inline; filename=%s' % os.path.basename(modified_file)
            os.remove(modified_file)
            return response

    return HttpResponse(400, _('Failed to export file'))


def mark_for_rto(modeladmin, request, queryset):
    invalid_orders = []
    orders_for_rto = []
    order_events_list = []

    queryset = queryset.prefetch_related('customer_contract__supervisors')

    for order in queryset:
        if order.has_pickup and order.status in [StatusPipeline.UNABLE_TO_DELIVER,
                                                 StatusPipeline.REACHED_AT_HUB,
                                                 StatusPipeline.ORDER_RETURNED_TO_HUB] \
            and not order.return_to_origin:
            orders_for_rto.append(order.id)
            _dict = {
                'order': order,
                'status': order.status,
                'hub_associate': request.user,
                'remarks': 'Marked for RTO'
            }
            order_events_list.append(Event(**_dict))
        else:
            invalid_orders.append(order.number)

    if invalid_orders:
        messages.error(request,
                       _("Cannot be marked for RTO - %s" % invalid_orders)
                       )
        return

    with transaction.atomic():
        try:
            Order.objects.filter(id__in=orders_for_rto).update(return_to_origin=True)
            Event.objects.bulk_create(order_events_list)
        except:
            messages.error(request, _("Failed to update records"))


mark_for_rto.short_description = 'Process for RTO'


def update_status_rto(modeladmin, request, queryset):
    invalid_orders = []
    orders_for_rto = []
    order_events_list = []

    queryset = queryset.prefetch_related('customer_contract__supervisors')

    for order in queryset:
        if order.status in [StatusPipeline.UNABLE_TO_DELIVER,
                            StatusPipeline.REACHED_AT_HUB,
                            StatusPipeline.ORDER_RETURNED_TO_HUB,
                            StatusPipeline.ORDER_RETURNED] \
            and not order.return_to_origin \
            and request.user in order.customer_contract.supervisors.all():
            orders_for_rto.append(order.id)
            _dict = {
                'order': order,
                'status': StatusPipeline.ORDER_RETURNED_RTO,
                'hub_associate': request.user,
                'remarks': 'Status updated to RTO'
            }
            order_events_list.append(Event(**_dict))
        else:
            invalid_orders.append(order.number)

    if invalid_orders:
        messages.error(request,
                       _("Cannot change status to RTO - %s" % invalid_orders)
                       )
        return

    with transaction.atomic():
        try:
            Order.objects.filter(id__in=orders_for_rto).update(
                status=StatusPipeline.ORDER_RETURNED_RTO)
            Event.objects.bulk_create(order_events_list)
        except:
            messages.error(request, _("Failed to update records"))


update_status_rto.short_description = 'Change status to RTO'


def export_booking_order_data(modeladmin, request, queryset):
    if queryset.exists():
        order_ids = queryset.values_list('id', flat=True)
        if 'RDS_READ_REP_1' in os.environ:
            queryset = Order.copy_data.using('readrep1').filter(id__in=order_ids)
        else:
            queryset = Order.copy_data.filter(id__in=order_ids)
    else:
        query = OrderExportClass().get_query(settings.C2C_ORDER_TYPE,
                                             request.GET.dict())
        if 'RDS_READ_REP_1' in os.environ:
            queryset = Order.copy_data.using('readrep1').filter(query)
        else:
            queryset = Order.copy_data.filter(query)

    file_path = 'booking_orders'
    queryset = queryset.order_by('-pickup_datetime')
    cancelled_time = Event.objects.filter(order_id=OuterRef('pk'),
                                          status=StatusPipeline.ORDER_CANCELLED)
    queryset.select_related(
        'customer', 'customer__user', 'city', 'driver', 'driver__user'
    ).prefetch_related('trip_set').annotate(
        pickup_date=Func(F('pickup_datetime') + timedelta(hours=tz_diff),
                         Value("DD/MM/YYYY"),
                         function='to_char',
                         output_field=CharField()),
        pickup_time=Func(F('pickup_datetime') + timedelta(hours=tz_diff),
                         Value("HH24:MI"),
                         function='to_char',
                         output_field=CharField()),

        booking_date=Func(F('date_placed') + timedelta(hours=tz_diff),
                          Value("DD/MM/YYYY"),
                          function='to_char',
                          output_field=CharField()),
        booking_time=Func(F('date_placed') + timedelta(hours=tz_diff),
                          Value("HH24:MI"),
                          function='to_char',
                          output_field=CharField()),

        cancellation_date=Func(
            (Subquery(cancelled_time.values('created_date')[:1])) + timedelta(
                hours=tz_diff),
            Value("DD/MM/YYYY"),
            function='to_char',
            output_field=CharField()),
        cancellation_time=Func(
            (Subquery(cancelled_time.values('created_date')[:1])) + timedelta(
                hours=tz_diff),
            Value("HH24:MI"),
            function='to_char',
            output_field=CharField()),
        # confirmation_date=Case(When(orderinternaltracking__status=IT_STATUS_CONFIRMED,
        #                             then=Func(F('orderinternaltracking__time') + timedelta(hours=tz_diff),
        #                                       Value("DD/MM/YYYY"),
        #                                       function='to_char',
        #                                       output_field=CharField()))),
        #
        # confirmation_time=Case(When(orderinternaltracking__status=IT_STATUS_CONFIRMED,
        #                             then=Func(F('orderinternaltracking__time') + timedelta(hours=tz_diff),
        #                                       Value("HH24:MI"),
        #                                       function='to_char',
        #                                       output_field=CharField()))),

        order_booking_type=Case(
            When(booking_type=ORDER_TYPE_NOW, then=Value('Current')),
            default=Value('Advanced'),
            output_field=CharField()),

        started_at_date=Func(
            Min(F('trip__actual_start_time')) + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY"),
            function='to_char',
            output_field=CharField()
        ),
        started_at_time=Func(
            Min(F('trip__actual_start_time')) + timedelta(hours=tz_diff),
            Value("HH24:MI"),
            function='to_char',
            output_field=CharField()
        ),
        ended_at_date=Func(
            Max(F('trip__actual_end_time')) + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY"),
            function='to_char',
            output_field=CharField()
        ),
        ended_at_time=Func(
            Max(F('trip__actual_end_time')) + timedelta(hours=tz_diff),
            Value("HH24:MI"),
            function='to_char',
            output_field=CharField()
        ),
        labour_cost=ExpressionWrapper(F('labour__labour_cost') * F('labour__no_of_labours'),
                                      output_field=PositiveIntegerField()),
        rcm_modified=Case(
            When(rcm=True, then=Value('Yes')),
            default=Value('No'),
            output_field=CharField()
        )
    ).to_csv(
        file_path,
        'number', 'customer__name', 'invoice_number', 'customer__user__phone_number', 'customer__customer_category',
        'customer__user__email', 'city__name', 'status', 'order_booking_type', 'pickup_address__line1',
        'pickup_address__line3', 'shipping_address__line1', 'shipping_address__line3',
        'estimated_distance_km', 'estimated_cost', 'estimated_cost_upper', 'estimated_cost_lower',
        'estimated_time_in_mins',
        'eta_in_mins', 'total_distance', 'device_type', 'driver__name',
        'driver__user__phone_number', 'driver__driver_vehicle', 'vehicle_class_preference__commercial_classification',
        'total_incl_tax', 'payment_status', 'payment_mode', 'toll_charges', 'total_payable',
        'rcm_modified', 'reason_for_cancellation', 'labour_cost', 'booking_date', 'booking_time',
        'pickup_date', 'pickup_time', 'started_at_date',
        'started_at_time', 'ended_at_date', 'ended_at_time', 'cancellation_date', 'cancellation_time',

    )
    modified_file = '%s_' % str(utc_to_ist(timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'Number', 'Customer Name', 'Invoice Number', 'Customer Mobile', 'Customer Category',
            'Customer Email', 'City', 'Status', 'Booking Type', 'Pickup Address',
            'Pickup Area', 'Drop Off Address', 'Drop Off Area',
            'Estimated Distance (KM)', 'Estimated Cost', 'Estimated Cost High', 'Estimated Cost Low',
            'Estimated Time (Mins)', 'ETA',
            'Total Distance', 'Booked From', 'Driver Name',
            'Driver Mobile', 'Vehicle No.', 'Vehicle Class',
            'Total Incl Tax', 'Payment Status', 'Payment Mode', 'Toll Charges', 'Total Payable',
            'RCM', 'Cancellation Reason', 'Labour Cost', 'Booking Date', 'Booking Time',
            'PickUp Date',
            'Pickup Time', 'Started At Date', 'Started At Time', 'Ended At Date',
            'Ended At Time', 'Cancellation Date', 'Cancellation Time'
        ])
        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1

        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(modified_file)
            os.remove(modified_file)
            return response


def mark_orders_cash_paid(modeladmin, request, queryset):
    """
    update selected orders to paid with payment mode cash
    """
    mark_orders_paid(request, queryset, PAYMENT_MODE_CASH)


def mark_orders_bank_transfer(modeladmin, request, queryset):
    """
    update selected orders to paid with payment mode bank transfer
    """
    mark_orders_paid(request, queryset, PAYMENT_MODE_BANK_TRANSFER)


def mark_orders_paid(request, queryset, payment_mode):
    """
    Mark orders as paid
    """
    reason = request.POST.get('reason_for_marking_paid', None)
    if not reason:
        messages.error(request,
                       "Can you provide a reason for marking order as paid from"
                       " admin?")
        return False

    if queryset.filter(~Q(status=StatusPipeline.ORDER_DELIVERED) |
                       Q(payment_status=PAYMENT_STATUS_PAID)).exists():
        messages.error(request, "Select only Delivered/unpaid Orders")
        return False

    ids = queryset.values_list('id', flat=True)
    if Order.objects.filter(id__in=ids).distinct('customer_id').count() > 1:
        messages.error(request, "Select orders for one customer only at a time")
        return False

    payment_status_history = []
    curr_time = timezone.now()
    with transaction.atomic():
        for order in queryset:
            payment_status_history.append(OrderPaymentHistory(
                order=order, old_payment_status=order.payment_status,
                new_payment_status=PAYMENT_STATUS_PAID,
                modified_date=curr_time, modified_by=request.user
            ))

        OrderPaymentHistory.objects.bulk_create(payment_status_history)
        queryset.update(payment_status=PAYMENT_STATUS_PAID,
                        payment_mode=payment_mode, modified_date=curr_time,
                        modified_by=request.user,
                        reason_for_marking_paid=reason)


def create_new_trip_for_orders(modeladmin, request, queryset):
    """
    Custom Action Button for assigning shipment orders to a driver and create a
    new trip
    """

    from .subadmins.flashsale_admin import FlashSaleOrder
    is_kiosk_order = modeladmin.model == KioskOrder
    driver_id = request.POST.get('driver')
    if not driver_id:
        messages.error(request, "Driver not selected")
        return False

    ids = queryset.values_list('id', flat=True)
    queryset = queryset.prefetch_related(
        'waypoint_set',
        'waypoint_set__stops',
    )
    trips = Trip.objects.filter(
        driver_id=driver_id,
        status__in=[
            StatusPipeline.TRIP_NEW,
            StatusPipeline.TRIP_IN_PROGRESS,
            StatusPipeline.DRIVER_ACCEPTED,
            StatusPipeline.TRIP_ALL_STOPS_DONE
        ]
    )

    if trips.exists() and modeladmin.model != FlashSaleOrder:
        _create_trip = False
        for trip in trips:
            if trip.status == StatusPipeline.TRIP_NEW and trip.planned_start_time >= timezone.now() + timedelta(
                hours=6):
                _create_trip = True
            else:
                _create_trip = False
                break
        if not _create_trip:
            messages.error(
                request,
                "Selected driver is already has an ongoing trip: %s" % \
                ', '.join([i.trip_number for i in trips])
            )
            return False

    driver = Driver.objects.only('id', 'driver_vehicle').get(pk=driver_id)
    if assign_driver(queryset, ids, driver, request,
                     is_kiosk_order=is_kiosk_order):
        messages.success(
            request,
            _('Order assigned to Driver and Trip Created successfully')
        )
    else:
        messages.error(request, "Order Assignment Failed")


create_new_trip_for_orders.short_description = _('Create New Trip')


def assign_orders_in_trip(modeladmin, request, queryset):
    """
    Custom Action Button for assigning shipment orders to a driver in between
    the trip
    """

    driver_id = request.POST.get('driver')
    if not driver_id:
        messages.error(request, "Driver not selected")
        return False

    ids = queryset.values_list('id', flat=True)

    queryset.prefetch_related('waypoint_set')
    queryset.prefetch_related('waypoint_set__stops')
    driver = Driver.objects.filter(pk=driver_id).first()

    trips = Trip.objects.filter(
        driver=driver, status__in=[
            StatusPipeline.TRIP_NEW,
            StatusPipeline.TRIP_IN_PROGRESS,
            StatusPipeline.DRIVER_ACCEPTED,
            StatusPipeline.TRIP_ALL_STOPS_DONE],
        order__isnull=True
    )

    if not trips:
        messages.error(request, "No existing Trip for Driver %s" % (driver))
        return False

    if trips.count() > 1:
        messages.error(request,
                       "More than one shipment trip exists for driver %s" % driver)
        return False
    trip = trips[0]

    if assign_driver(queryset, ids, driver, request, trip):

        messages.success(
            request, "Orders assigned in between Trip %s" % (trip))
    else:
        messages.error(
            request,
            "Order Assignment Failed"
        )


assign_orders_in_trip.short_description = _('Assign Orders in Trip')


def create_middle_mile_trip(**kwargs):
    trip_data = {}
    for attr in kwargs:
        trip_data[attr] = kwargs.get(attr, None)

    trip_data['planned_start_time'] = timezone.now()
    trip_data['trip_acceptence_time'] = timezone.now()
    trip_data['actual_start_time'] = timezone.now()

    trip = CreateModelMixin().create(data=trip_data, serializer_class=TripSerializer)
    if not trip:
        logger.error('Trip creation Failed')
    else:
        logger.info('Trip:- "%s" created successfully', trip)
    return trip


def mark_orders_as_in_transit(modeladmin, request, queryset):
    items = queryset.values_list('id', flat=True)
    partner = request.POST.get('partner', None)
    if not partner:
        messages.error(
            request, "Please choose the courier partner")
        return
    driver = Driver.objects.filter(partner_id=partner).first()
    orders = Order.objects.filter(id__in=items).exclude(
        status__in=[StatusPipeline.REACHED_AT_HUB, StatusPipeline.ORDER_NEW],
        customer_contract__is_inter_city_contract=True).values_list('number', flat=True)

    if orders:
        messages.error(
            request, "Failed to mark Orders as in Transit check %s" % list(orders))
        return

    order = Order.objects.filter(id__in=items).first()

    dv_map = DriverVehicleMap.objects.filter(driver=driver).first()

    if not driver:
        messages.error(
            request, "Associate partner with driver")
        return
    if not dv_map:
        messages.error(
            request, "Driver Vehicle Mapping is Missing")
        return
    else:
        event_list = []
        count = 0
        for item in items:
            _event_data = {'order_id': item, 'status': StatusPipeline.IN_TRANSIT, 'remarks': 'admin status change',
                           'hub_associate': request.user}
            event_list.append(Event(**_event_data))
            count += 1

        with transaction.atomic():
            trip = create_middle_mile_trip(driver=driver.id, assigned=count, order_ids=list(items),
                                           blowhorn_contract=driver.contract_id if driver.contract else None,
                                           customer_contract=order.customer_contract_id,
                                           invoice_driver=driver.id, vehicle=dv_map.vehicle_id,
                                           trip_workflow=order.customer_contract.trip_workflow_id,
                                           trip_acceptence_time=timezone.now(),
                                           status=TRIP_IN_PROGRESS,
                                           hub=order.hub_id,
                                           trip_type=MIDDLE_MILE)
            Order.objects.filter(id__in=items, status__in=[StatusPipeline.REACHED_AT_HUB, StatusPipeline.ORDER_NEW],
                                 customer_contract__is_inter_city_contract=True).update(
                status=StatusPipeline.IN_TRANSIT, courier_partner_id=partner)
            Event.objects.bulk_create(event_list)
        messages.success(
            request, "Orders marked as In-Transit and trip %s is created" % (trip))


mark_orders_as_in_transit.short_description = _('Mark Orders as in transit')


def findMissingElement(a, b):
    s = dict()
    for i in range(len(b)):
        s[b[i]] = 1

    for i in range(len(a)):
        if a[i] not in s.keys():
            return True
    return False


def receive_orders(modeladmin, request, queryset):
    items = [x.id for x in queryset] #queryset.values_list('id', flat=True)
    hub = request.POST.get('hub', None)
    if not hub:
        messages.error(
            request, "Please choose the receiving hub")
        return

    trip = Trip.objects.filter(order_ids__contains=[items[0]]).first()
    is_elem_missing = findMissingElement(trip.order_ids, items)

    if is_elem_missing:
        numbers = Order.objects.filter(id__in=trip.order_ids).values_list('number', flat=True)
        messages.error(
            request, "Receive all the items for this trip %s" % list(numbers))
        return

    event_list = []
    for item in items:
        _event_data = {'order_id': item, 'status': StatusPipeline.REACHED_AT_HUB, 'remarks': 'admin status change',
                       'hub_associate': request.user, 'hub_id': hub}
        event_list.append(Event(**_event_data))
    with transaction.atomic():
        Order.objects.filter(id__in=items, status=StatusPipeline.IN_TRANSIT,
                             customer_contract__is_inter_city_contract=True).update(
            status=StatusPipeline.REACHED_AT_HUB,
            latest_hub_id=hub)
        Event.objects.bulk_create(event_list)
        if trip:
            Trip.objects.filter(id=trip.id).update(actual_end_time=timezone.now(), status=StatusPipeline.TRIP_COMPLETED,
                                                   delivered=F('assigned'))

    messages.success(
        request, "Orders Received")


receive_orders.short_description = _('Receive Orders')


def return_to_hub(modeladmin, request, queryset):
    """
    Custom Action Button returning the shipment orders to hub
    """

    records_with_ongoing_trip = []
    records_with_invalid_statuses = []
    validated_orders = []
    event_objs = []

    for order in queryset:
        proceed = True
        if order.status not in [StatusPipeline.ORDER_RETURNED,
                                StatusPipeline.OUT_FOR_DELIVERY,
                                StatusPipeline.UNABLE_TO_DELIVER]:
            proceed = False
            records_with_invalid_statuses.append(order.number)
        if is_ongoing_trip(order):
            proceed = False
            records_with_ongoing_trip.append(order.number)
        if proceed:
            validated_orders.append(order.number)
            event_objs.append(
                Event(
                    driver=order.driver,
                    status=StatusPipeline.ORDER_RETURNED_TO_HUB,
                    order=order,
                    hub_associate=request.user,
                )
            )

    Order.objects.filter(
        number__in=validated_orders
    ).update(status=StatusPipeline.ORDER_RETURNED_TO_HUB)

    if event_objs:
        Event.objects.bulk_create(event_objs)

    if records_with_ongoing_trip:
        messages.error(
            request,
            "Orders fulfilled by other ongoing trip: "
            "%s" % records_with_ongoing_trip
        )
    if records_with_invalid_statuses:
        messages.error(
            request,
            "Orders with invalid statuses: "
            "%s" % records_with_invalid_statuses
        )

    if validated_orders:
        messages.success(
            request,
            "Orders successfully returned to hub: "
            "%s" % validated_orders
        )


# make the changes in return_to_hub_confirmation.js if making changes in short description here
return_to_hub.short_description = _('Return To Hub')


def shipment_order_status_update(modeladmin, request, queryset):
    '''
    Custom Action Button for changing status ofshipment orders
    '''
    records_with_ongoing_trip = []
    records_with_shopify_fulfillment = []
    records_with_invalid_statuses = []
    validated_orders = []
    event_objs = []
    new_status = request.POST.get('new_status')

    if not new_status:
        messages.error(request, "Please select the new status")
        return False

    if not request.user.is_superuser:
        messages.error(request, "Superuser only can update the status")
        return False

    for order in queryset:
        proceed = True

        if is_ongoing_trip(order):
            proceed = False
            records_with_ongoing_trip.append(order.number)

        if order.source == 'shopify' and order.shopify_fulfillment_id:
            proceed = False
            records_with_shopify_fulfillment.append(order.number)

        if proceed:
            validated_orders.append(order.number)
            status_text = 'Superuser Status Update from {} to {}'.format(
                order.status, new_status)
            event_objs.append(
                Event(
                    driver=order.driver,
                    status=new_status,
                    order=order,
                    hub_associate=request.user,
                    remarks=status_text
                )
            )

    with transaction.atomic():
        try:
            order_params = {'status': new_status}
            if new_status == StatusPipeline.DUMMY_STATUS:
                order_params['reference_number'] = None
                order_params['customer_reference_number'] = None

            Order.objects.filter(number__in=validated_orders).update(**order_params)

            if event_objs:
                Event.objects.bulk_create(event_objs)

            if records_with_ongoing_trip:
                messages.error(
                    request,
                    "Update not allowed for order with ongoing trip: "
                    "%s" % records_with_ongoing_trip
                )

            if records_with_shopify_fulfillment:
                messages.error(
                    request,
                    "Update not allowed for order with shopify fulfillment: "
                    "%s" % records_with_shopify_fulfillment
                )

            if records_with_invalid_statuses:
                messages.error(
                    request,
                    "Orders with invalid statuses: "
                    "%s" % records_with_invalid_statuses
                )

            if validated_orders:
                messages.success(
                    request,
                    "Order status successfully updated: "
                    "%s" % validated_orders
                )

        except Exception as e:
            messages.error(request, _("Failed to update records"))


shipment_order_status_update.short_description = _('Change order status')


class AssignDriverActionForm(ActionForm):
    driver = forms.ModelChoiceField(
        queryset=Driver.objects.filter(
            status=StatusPipeline.ACTIVE,
            driver_vehicle__isnull=False
        ).select_related('user'),
        required=False
    )


class AssignDriverActionFormAlt(ActionForm):
    driver = forms.IntegerField(required=False)
    new_status = forms.CharField(required=False)


class OrderCancellationReasonAdmin(admin.ModelAdmin):
    list_display = ('reason', 'seq_no')


class OrderDispatchAdminInline(admin.TabularInline):
    model = OrderDispatch
    extra = 0
    fields = ('driver', 'get_driver_mobile', 'status', 'get_geopoint', 'distance',
              'get_response_point', 'get_created_date', 'get_modified_date', 'mqtt_response', 'fcm_response')
    readonly_fields = ['driver', 'status', 'get_created_date', 'get_modified_date',
                       'distance', 'get_geopoint', 'get_response_point', 'get_driver_mobile',
                       'mqtt_response', 'fcm_response']

    def get_local_time(self, date_):
        tz_zone = pytz.timezone(settings.IST_TIME_ZONE)
        local_datetime = date_.astimezone(tz_zone)
        return datetime.strptime((str(local_datetime).split('.')[0]), '%Y-%m-%d %H:%M:%S')

    def get_modified_date(self, obj):
        return self.get_local_time(obj.modified_date)

    def get_created_date(self, obj):
        return self.get_local_time(obj.created_date)

    get_created_date.short_description = 'Dispatch date'
    get_modified_date.short_description = 'Response date'

    def get_driver_mobile(self, obj):
        return obj.driver.user.phone_number

    get_driver_mobile.short_description = 'Mobile Number'

    def get_geopoint(self, obj):
        if obj and obj.geopoint:
            lat = obj.geopoint.coords[1]
            lon = obj.geopoint.coords[0]
            latlon = '%s,%s' % (lat, lon)
            return format_html('<a href="http://maps.google.com/maps?q={}" target="_blank">{}</a>', latlon, latlon)
        return ''

    get_geopoint.short_description = 'Location'

    def get_response_point(self, obj):
        if obj and obj.response_point:
            lat = obj.response_point.coords[1]
            lon = obj.response_point.coords[0]
            latlon = '%s,%s' % (lat, lon)
            return format_html('<a href="http://maps.google.com/maps?q={}" target="_blank">{}</a>', latlon, latlon)
        return ''

    get_response_point.short_description = 'Response Location'

    def has_delete_permission(self, request, obj=None):
        return False


class CallMaskingLogInline(admin.TabularInline):
    model = CallMaskingLog
    extra = 0

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(CallMaskingLog)
class CallMaskingLogAdmin(admin.ModelAdmin):
    model = CallMaskingLog

    list_display = ('sid', 'order', 'driver', 'from_number',
                    'to_number', 'caller_id', 'duration',
                    'price')

    search_fields = ('order__number', 'from_number', 'to_number')
    list_filter = [('driver', admin.RelatedOnlyFieldListFilter), ]

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class AssignPartnerActionForm(ActionForm):
    partner = CustomModelChoiceField(
        label=_('Partner'),
        queryset=Partner.objects.filter(is_shipping_partner=True),
        required=False,
        field_names=['pk', 'legalname']
    )

    hub = CustomModelChoiceField(
        label=_('Hub'),
        queryset=Hub.objects.all(),
        required=False,
        field_names=['pk', 'name']
    )


class ShipmentOrderAdmin(FSMTransitionMixin, BaseAdmin):
    action_form = AssignDriverActionFormAlt
    actions = [create_new_trip_for_orders, assign_orders_in_trip,
               shipment_order_status_update, return_to_hub,
               export_shipment_order_data, mark_for_rto, update_status_rto, export_shipment_data_with_documents]
    fsm_field = ['status', ]

    change_form_template = 'admin/order/shipmentorder/change_form.html'

    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    show_full_result_count = False
    list_display = (
        'business_customer_name', 'number', 'get_customer_reference_number',
        'get_reference_number', 'contract_type', 'date_placed', 'pickup_hub_name', 'hub_name', 'city_name', 'source_city_name',
        'status', 'shopify_fulfillment_id',
        'pickup_datetime',
        'get_slot_start_time', 'get_slot_end_time',
        'return_order', 'get_attempted_time',
        'get_attempted_count', 'updated_time', 'customer_name',
        'customer_phone', 'driver_name', 'get_driver_number',
        'shipping_postal_code', 'payment_status', 'payment_mode',
        'source'
    )

    list_display_links = ('number',)
    inlines = [EventAdminInline, DocumentsInline, OrderLineInline,
               LrNumberAdminInline, DriverRatingInline, OrderDispatchAdminInline,
               CallMaskingLogInline]
    list_filter = [ShipmentAlertFilter, 'source', 'customer_contract__contract_type',
                   ('city', admin.RelatedOnlyFieldListFilter),
                   multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
                   ('customer', admin.RelatedOnlyFieldListFilter),
                   ('hub', admin.RelatedOnlyFieldListFilter),
                   ('pickup_hub', admin.RelatedOnlyFieldListFilter),
                   ('driver', admin.RelatedOnlyFieldListFilter),
                   ('date_placed', DateRangeFilter),
                   ('pickup_datetime', DateRangeFilter),
                   ('updated_time', DateRangeFilter),
                    'return_order']

    def pickup_hub_name(self, obj):
        return obj.pickup_hub_name

    pickup_hub_name.short_description = _('pickup hub')

    def hub_name(self, obj):
        return obj.hub_name

    hub_name.short_description = _('hub')

    def contract_type(self, obj):
        return obj.customer_contract.contract_type

    contract_type.short_description = _('contract type')

    def driver_name(self, obj):
        return "%s | %s" % (obj.driver_name, obj.driver_vehicle)

    driver_name.short_description = _('driver')


    def business_customer_name(self, obj):
        return obj.business_customer_name

    business_customer_name.short_description = _('Customer')


    def city_name(self, obj):
        return obj.city_name

    city_name.short_description = _('City')

    def source_city_name(self, obj):
        return obj.source_city_name

    source_city_name.short_description = _('Source City')

    search_fields = (
        'number',
        'customer_reference_number',
        'reference_number',
        'batch__id',
        'batch__description', 'lrnumber__lr_number'
    )
    multi_search_fields = ('number', 'reference_number',
                           'customer_reference_number',)

    def get_actions(self, request):
        """
        Remove `Delete` and Add `Generate Payments`
        """
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']

        if not request.user.is_superuser:
            if 'shipment_order_status_update' in actions:
                del actions['shipment_order_status_update']

        return actions

    def get_slot_start_time(self, obj):
        return obj.exp_delivery_start_time

    get_slot_start_time.short_description = _('exp. delivery slot start time')

    def get_slot_end_time(self, obj):
        return obj.expected_delivery_time

    get_slot_end_time.short_description = _('exp. delivery slot end time')

    def get_reference_number(self, obj):
        if obj:
            return format_html("""
             <a title='Click here to view barcode'
             onClick='showBarCode("%s", "%s")'>%s</a>
             """ % (obj.reference_number, obj.number, obj.reference_number))
        return '--NA--'

    get_reference_number.short_description = _('reference number')

    def get_customer_reference_number(self, obj):
        if obj:
            return format_html("""
             <a title='Click here to view barcode' onClick='showBarCode("%s")'>
             %s</a>
             """ % (obj.customer_reference_number,
                    obj.customer_reference_number))
        return '--NA--'

    get_customer_reference_number.short_description = \
        _('customer reference number')

    def get_kitting_time(self, order):
        self.kitting_time = None
        self.preoutbound_time = None
        self.delivery_time = None
        pack_time = None
        out_time = None

        events = order.event_set.all()
        for event in events:
            if event.status == StatusPipeline.ORDER_PACKED:
                pack_time = event.time
                kt = pack_time - order.date_placed

                self.kitting_time = kt.seconds / 60
            elif event.status == StatusPipeline.OUT_FOR_DELIVERY:
                out_time = event.time
                if pack_time:
                    preout = out_time - pack_time
                    self.preoutbound_time = preout.seconds / 60
            elif event.status in [
                StatusPipeline.ORDER_DELIVERED,
                StatusPipeline.ORDER_RETURNED,
                StatusPipeline.UNABLE_TO_DELIVER
            ]:
                if out_time:
                    dt = event.time - out_time
                    self.delivery_time = dt.seconds / 60

        if self.kitting_time:
            return minutes_to_dhms(self.kitting_time)
        return None

    get_kitting_time.short_description = 'Kitting Duration'

    def get_batch(self, obj):
        """ Return the batch id linked with the order
            change link """
        if obj.batch:
            url = reverse(
                'admin:order_orderbatch_change', args=[obj.batch.id])
            return format_html('<a target="_blank" href="{}">{}</a>', url, obj.batch)
        return obj.batch

    get_batch.short_description = 'Batch'

    def get_preoutbound_time(self, obj):
        if self.preoutbound_time:
            return minutes_to_dhms(self.preoutbound_time)
        return None

    get_preoutbound_time.short_description = 'PreOutbound Duration'

    def get_delivery_time(self, obj):
        if self.delivery_time:
            return minutes_to_dhms(self.delivery_time)
        return None

    get_delivery_time.short_description = 'Delivery Duration'

    def get_driver_number(self, obj):
        return obj.driver_number

    get_driver_number.short_description = 'Driver Number'

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        qs = qs.prefetch_related('event_set', 'documents', 'city__state_set')
        qs = qs.select_related('customer', 'driver', 'driver__user', 'city',
                               'shipping_address', 'hub', 'batch')
        return list(qs)

    def customer_phone(self, obj):
        if obj.shipping_address:
            self.ship_postcode = obj.shipping_address.postcode
            return obj.shipping_address.phone_number

    def alternate_customer_phone(self, obj):
        if obj.shipping_address:
            self.ship_postcode = obj.shipping_address.postcode
            return obj.shipping_address.alternate_phone_number

    def shipping_postal_code(self, obj):
        if obj.shipping_address:
            return str(self.ship_postcode)
        return None

    def get_attempted_time(self, order):
        """
        Gets the first attempt time
        """
        events = order.event_set.all()
        attempted_events = [
            StatusPipeline.ORDER_DELIVERED,
            StatusPipeline.ORDER_RETURNED,
            StatusPipeline.UNABLE_TO_DELIVER]

        for event in events:
            if event.status in attempted_events:
                return event.time

        return None

    get_attempted_time.short_description = _('First Attempt Time')

    def get_attempted_count(self, order):
        """
        Gets the attempted count
        """
        events = order.event_set.all()
        if events.exists():
            attempted_events = [
                StatusPipeline.ORDER_DELIVERED,
                StatusPipeline.ORDER_RETURNED,
                StatusPipeline.UNABLE_TO_DELIVER]

            return len(
                [o for o in events if o.status in attempted_events])

        return None

    get_attempted_count.short_description = _('Attempt Count')

    def get_remaining_time(self, order):
        if order.order_type == settings.SHIPMENT_ORDER_TYPE:

            if not order.expected_delivery_time:
                return 0

            if order.status in StatusPipeline.ORDER_END_STATUSES:
                updated_time = order.updated_time
            else:
                updated_time = timezone.now()

            delivery_time = order.expected_delivery_time - order.date_placed
            delivery_time = delivery_time.total_seconds() / 60
            order_duration_in_mins = (
                                         updated_time - order.date_placed).total_seconds() / 60
            remaining_time = delivery_time - order_duration_in_mins

            if remaining_time < 0:
                remaining_time = 0

            return minutes_to_dhms(int(round(remaining_time)))
        else:
            return None

    get_remaining_time.short_description = 'Remaining Time'

    def get_critical_status(self, order):
        """
        Gets the critical status of the event
        1. Looks through the shipment alert configuration for the customer
        2. Finds the time since creation of the order
        3. Loops through the configuration slabs to find the critical status
        4. Record formatting based on color coding
        """
        success = False
        if order.status in StatusPipeline.ORDER_END_STATUSES:
            updated_time = order.updated_time
            success = True
        else:
            updated_time = timezone.now()

        event = Event.objects.filter(order_id=order.id, status=StatusPipeline.ORDER_PICKED).first()

        if not event or event.time is None:
            return '-'

        timedelta = updated_time - event.time

        minutes_since_order_placed = timedelta.total_seconds() / 60
        config_list = order.customer.shipment_configuration

        if not config_list:
            return None

        for config in config_list:
            if config.time_since_creation > minutes_since_order_placed:
                if not success:
                    return mark_safe('<b style="color:{};">{}</b>'.format(
                        config.alert_colours, config.critical_status))
                return mark_safe('<b style="color:{};">{}</b>'.format(
                    'BLUE', StatusPipeline.SUCCESS))

        # If it has come here, it has breached max SLA,
        # set to breached SLA, failure
        if not success:
            return mark_safe('<b style="color:{};">{}</b>'.format(
                'black', StatusPipeline.FAILURE))
        else:
            return mark_safe('<b style="color:{};">{}</b>'.format(
                'red', StatusPipeline.DELAYED))

    get_critical_status.short_description = _('Alert Level')

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    ''' change_view and add_view are used here
        to disable save and continue button
    '''

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        # extra_context['show_save_and_add_another'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    def changelist_view(self, request, extra_context=None):
        if 'action' in request.POST and request.POST['action'] == 'export_shipment_order_data':
            if not request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                post.update({admin.ACTION_CHECKBOX_NAME: str(Order.objects.all().first().id)})
                request._set_post(post)
        return super(ShipmentOrderAdmin, self).changelist_view(request, extra_context)

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields': (
                    ('dump_data',),
                )
            }),
            ('Customer Details', {
                'fields': (
                    ('dump_customer_data',),
                )
            }),
            ('Payment Details', {
                'fields': (
                    ('dump_payment_data',),
                )
            }),
            ('dispatch_log', {
                'fields': ('dispatch_log',)
            }),

        )

        return fieldsets

    def dump_customer_data(self, obj):
        return format_html(ADMIN_CUSTOMER_TAB_DATA % (
            obj.pickup_address.first_name, obj.shipping_address.first_name,
            obj.pickup_address.line1, obj.shipping_address.line1,
            obj.pickup_address.postcode, obj.shipping_address.postcode,
            obj.pickup_address.phone_number, obj.shipping_address.phone_number,
            obj.shipping_address.alternate_phone_number, obj.pickup_hub_name, obj.hub_name
        ))

    def dump_payment_data(self, obj):
        return format_html(ADMIN_PAYMENT_TAB_DATA % (
            obj.payment_status, obj.payment_mode,
            obj.cash_on_delivery, obj.shipping_charges,
            obj.tax, obj.discount,
            obj.cash_collected, obj.payment_source or '',
            'Yes' if obj.is_avg_price else 'No', obj.total_incl_tax or 0
        ))

    def dump_data(self, obj):
        return format_html(ADMIN_GENERAL_TAB_DATA % (
            obj.reference_number, obj.customer_reference_number, obj.hub, obj.latest_hub,
            obj.shipping_address.line1, obj.shipping_address.address_type,
            utc_to_ist(obj.date_placed).strftime(
                settings.ADMIN_DATETIME_FORMAT), obj.otp, utc_to_ist(
                obj.exp_delivery_start_time).strftime(
                settings.ADMIN_DATETIME_FORMAT) if obj.exp_delivery_start_time else None, utc_to_ist(
                obj.expected_delivery_time).strftime(
                settings.ADMIN_DATETIME_FORMAT) if obj.expected_delivery_time else None,
            obj.driver,
            obj.driver.user.phone_number if obj.driver else None,
            obj.shipping_address.first_name,
            obj.shipping_address.phone_number, obj.business_customer_name,
            obj.shipping_address.alternate_phone_number,
            obj.customer_contract, obj.customer_contract.contract_type,
            obj.city_name,
            obj.weight, obj.volume, obj.priority,
            obj.dimension.length if obj.dimension else 0,
            obj.dimension.breadth if obj.dimension else 0,
            obj.dimension.height if obj.dimension else 0,
            obj.dimension.uom if obj.dimension else '',
            '%0.2f' % obj.total_distance if obj.total_distance else None, obj.batch,
            obj.pickup_address.line1, obj.delivery_instructions,
            obj.division if obj.division else None,
            utc_to_ist(obj.updated_time).strftime(
                settings.ADMIN_DATETIME_FORMAT) if obj.updated_time else None, obj.status
        )
                           )

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = ShipmentOrderEditForm
        else:
            self.form = ShipmentOrderAddForm
        return super(ShipmentOrderAdmin, self).get_form(request, obj, **kwargs)

    # not getting used so commenting
    # def shipment_info_prettified(self, instance):
    #     """ Function to display pretty JSON """
    #     # Convert the data to sorted, indented JSON
    #     response = json.dumps(instance.data, sort_keys=True, indent=2)

    #     # Truncate the data. Alter as needed
    #     response = response[:5000]

    #     # Get the Pygments formatter
    #     formatter = HtmlFormatter(style='colorful')

    #     # Highlight the data
    #     response = highlight(response, JsonLexer(), formatter)

    #     # Get the stylesheet
    #     style = "<style>" + formatter.get_style_defs() + "</style><br>"

    #     # Safe the output
    #     return mark_safe(style + response)

    # shipment_info_prettified.short_description = _('Shipment Details')

    def get_readonly_fields(self, request, obj=None):
        if obj:  # edit
            return ['order_type', 'number', 'reference_number', 'dispatch_log',
                    'driver', 'customer_contract', 'status', 'business_customer_name',
                    'city__name', 'hub', 'shipping_address',
                    'expected_delivery_time', 'dump_customer_data', 'dump_payment_data',
                    'shipment_info', 'cash_on_delivery',
                    'date_placed', 'dump_data', 'bill_number', 'bill_date']
        else:
            return ['order_type', 'number', 'total_incl_tax',
                    'driver', 'customer_contract', 'status',
                    'dump_data', 'dump_customer_data', 'dump_payment_data']

    def get_pickup_datetime(self, obj):
        if obj:
            return obj.pickup_datetime
        return None

    def customer_name(self, obj):
        if obj and obj.shipping_address:
            return obj.shipping_address.first_name
        return None

    dump_data.short_description = 'Data'
    dump_customer_data.short_description = 'Data'
    dump_payment_data.short_description = 'Data'
    get_pickup_datetime.short_description = "Pick Up Time"
    get_pickup_datetime.admin_order_field = 'Pick Up Time'

    def get_queryset(self, request):
        order_qs = super().get_queryset(request)
        order_qs = order_qs.filter(order_type=settings.SHIPMENT_ORDER_TYPE)
        if not request.GET.get('pickup_datetime__gte') and not request.GET.get(
            'updated_time__gte') and not request.GET.get('_changelist_filters'):
            end = timezone.now() + timedelta(days=10)
            start = end - timedelta(days=55)
            order_qs = order_qs.filter(pickup_datetime__range=(start, end))
        order_qs = apply_contract_permission_filter(request.user, order_qs)
        order_qs = order_qs.annotate(business_customer_name=F('customer__name'), pickup_hub_name=F('pickup_hub__name'),
                                     hub_name=F('hub__name'), city_name=F('city__name'), driver_name=F('driver__name'),
                                     source_city_name=F('source_city__name'), driver_vehicle=F('driver__driver_vehicle'),
                                     driver_number=F('driver__user__phone_number'))
        order_qs = order_qs.prefetch_related('event_set')
        return order_qs

    # def get_search_results(self, request, queryset, search_term):
    #     from blowhorn.order.documents import OrderDocument as OrderDoc
    #     from elasticsearch_dsl import Q
    #     from elasticsearch import Elasticsearch
    #     from elasticsearch_dsl import Search
    #     print('search_term - ', search_term)
    #     if not search_term:
    #         queryset, use_distinct = super().get_search_results(request,
    #                                                             queryset,
    #                                                             search_term)
    #         return queryset, use_distinct
    #
    #     client = Elasticsearch([{'host': '18.221.137.53', 'port': '9200'}])
    #     index = 'order_' + settings.ELASTICSEARCH_INDEX_POSTFIX
    #     s = Search(using=client, index=index)[0:1000]
    #     # s = OrderDoc.search().query("match", description="search_term")
    #     """
    #     #works
    #     client = Elasticsearch([{'host': '18.221.137.53', 'port': '9200'}])
    #     s = Search(using=client, index='order_test')
    #     s = s.query('match', order_contract__contract_type="Shipment")
    #     s = s.query("multi_match", query='Bengaluru', fields=['number', 'customer_contract.contract_type', 'customer_contract.city.name'])
    #     s = s.query("multi_match", query=search_term)
    #
    #     Bulk indexing -
    #     from blowhorn.order.models import *
    #     from blowhorn.order.documents import *
    #     from elasticsearch import Elasticsearch
    #     qs = Order.objects.filter(order_type='shipment', customer_id=30647)
    #     docs = []
    #     for s in qs:
    #         docs.append(s.index_doc())
    #     from elasticsearch.helpers import bulk
    #     bulk(client, docs)
    #     """
    #     s = s.query("multi_match", query=search_term)
    #     try:
    #         _response = s.execute()
    #         print('_response - ', _response)
    #         ids_list = []
    #         for hit in _response:
    #             ids_list.append(hit['id'])
    #
    #         queryset = queryset.filter(pk__in=ids_list)
    #         return queryset, False
    #     except:
    #         queryset, use_distinct = super().get_search_results(request,
    #                                                             queryset,
    #                                                             search_term)
    #         return queryset, use_distinct

    # To disable the Delete button on admin
    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        if object_id:
            order = Order.objects.get(pk=object_id)
            extra_context['order_number'] = order.number
        try:
            return super().change_view(
                request, object_id, extra_context=extra_context)

        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            'website/js/lib/jquery-ui.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            '/static/website/js/lib/jsBarcode.js',
            '/static/website/js/lib/barcode_model.js',
            '/static/admin/js/shipment_order_confirmation.js',
            '/static/admin/js/order/return_to_hub_confirmation.js',
            '/static/admin/js/order/challan_display.js'
        )

    class Meta:
        model = Order
        fields = '__all__'


admin.site.register(ShipmentOrder, ShipmentOrderAdmin)


class InterCityOrderAdmin(BaseAdmin):
    action_form = AssignPartnerActionForm
    actions = [mark_orders_as_in_transit, receive_orders]
    inlines = [EventAdminInline]
    # list_display_links = None
    list_display = (
        'customer', 'number', 'customer_reference_number',
        'reference_number', 'date_placed', 'pickup_hub', 'hub', 'city', 'source_city',
        'status',
        'pickup_datetime', 'driver', 'payment_status', 'payment_mode',
        'source'
    )

    list_filter = ['source',
                   ('city', admin.RelatedOnlyFieldListFilter),
                   ('source_city', admin.RelatedOnlyFieldListFilter),
                   multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
                   ('customer', admin.RelatedOnlyFieldListFilter),
                   ('hub', admin.RelatedOnlyFieldListFilter),
                   ('pickup_hub', admin.RelatedOnlyFieldListFilter),
                   ('driver', admin.RelatedOnlyFieldListFilter),
                   ('date_placed', DateRangeFilter),
                   ('pickup_datetime', DateRangeFilter),
                   ('updated_time', DateRangeFilter),
                   'return_order']

    list_select_related = (
        'customer',
        'driver', 'customer_contract', 'city', 'source_city'
    )

    search_fields = (
        'number',
        'customer_reference_number',
        'reference_number', 'lrnumber__lr_number'
    )
    multi_search_fields = ('number', 'reference_number',
                           'customer_reference_number',)

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields] + ['rejected_drivers', 'item_category']

    def get_actions(self, request):
        """
        Remove `Delete` and Add `Generate Payments`
        """
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']

        if not request.user.is_superuser:
            if 'shipment_order_status_update' in actions:
                del actions['shipment_order_status_update']

        return actions

    def get_queryset(self, request):
        order_qs = super().get_queryset(request)
        order_qs = order_qs.filter(order_type=settings.SHIPMENT_ORDER_TYPE,
                                   customer_contract__is_inter_city_contract=True, source_city__isnull=False,
                                   status__in=[StatusPipeline.IN_TRANSIT, StatusPipeline.ORDER_NEW,
                                               StatusPipeline.REACHED_AT_HUB])
        order_qs = order_qs.select_related(
            'customer', 'pickup_hub', 'source_city',
            'hub', 'city', 'driver', 'customer_contract', 'customer_contract__customer',

        )
        order_qs = apply_contract_permission_filter(request.user, order_qs)
        return order_qs

        # To disable the Delete button on admin

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


admin.site.register(InterCityOrder, InterCityOrderAdmin)


class NewShipmentOrderAdmin(FSMTransitionMixin, BaseAdmin):
    resource_class = ShipmentOrderResource
    action_form = AssignDriverActionFormAlt
    actions = [create_new_trip_for_orders, assign_orders_in_trip, export_shipment_order_data]
    fsm_field = ['status', ]

    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    show_full_result_count = False
    list_display = (
        'customer', 'number', 'get_customer_reference_number',
        'get_reference_number', 'date_placed', 'pickup_hub', 'hub', 'city',
        'status', 'pickup_datetime',
        'return_order', 'updated_time', 'customer_name', 'driver', 'source'
    )

    list_display_links = ('number', 'driver',)
    inlines = [EventAdminInline, DocumentsInline, OrderLineInline]
    list_filter = [('city', admin.RelatedOnlyFieldListFilter),
                   multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
                   ('customer', admin.RelatedOnlyFieldListFilter),
                   ('hub', admin.RelatedOnlyFieldListFilter),
                   ('updated_time', DateRangeFilter),
                   'batch']

    list_select_related = (
        'customer', 'pickup_address', 'shipping_address',
        'driver', 'customer_contract', 'city',
    )

    search_fields = (
        'number',
        'reference_number'
    )
    multi_search_fields = ('number', 'reference_number')

    def get_actions(self, request):
        """
        Remove `Delete` and Add `Generate Payments`
        """
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']

        if not request.user.is_superuser:
            if 'shipment_order_status_update' in actions:
                del actions['shipment_order_status_update']

        return actions

    def get_reference_number(self, obj):
        if obj:
            return format_html("""
             <a title='Click here to view barcode'
             onClick='showBarCode("%s", "%s")'>%s</a>
             """ % (obj.reference_number, obj.number, obj.reference_number))
        return '--NA--'

    get_reference_number.short_description = _('reference number')

    def get_customer_reference_number(self, obj):
        if obj:
            return format_html("""
             <a title='Click here to view barcode' onClick='showBarCode("%s")'>
             %s</a>
             """ % (obj.customer_reference_number,
                    obj.customer_reference_number))
        return '--NA--'

    get_customer_reference_number.short_description = \
        _('customer reference number')

    def get_batch(self, obj):
        """ Return the batch id linked with the order
            change link """
        if obj.batch:
            url = reverse(
                'admin:order_orderbatch_change', args=[obj.batch.id])
            return format_html('<a target="_blank" href="{}">{}</a>', url, obj.batch)
        return obj.batch

    get_batch.short_description = 'Batch'

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    ''' change_view and add_view are used here
        to disable save and continue button
    '''

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        # extra_context['show_save_and_add_another'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    def changelist_view(self, request, extra_context=None):
        if 'action' in request.POST and request.POST['action'] == 'export_shipment_order_data':
            if not request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                post.update({admin.ACTION_CHECKBOX_NAME: str(Order.objects.all().first().id)})
                request._set_post(post)
        return super(NewShipmentOrderAdmin, self).changelist_view(request, extra_context)

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields': (
                    ('dump_data',),
                )
            }),
            ('Customer Details', {
                'fields': (
                    ('dump_customer_data',),
                )
            }),
            ('Payment Details', {
                'fields': (
                    ('dump_payment_data',),
                )
            }),
            ('dispatch_log', {
                'fields': ('dispatch_log',)
            }),

        )

        return fieldsets

    def dump_customer_data(self, obj):
        return format_html(ADMIN_CUSTOMER_TAB_DATA % (
            obj.pickup_address.first_name, obj.shipping_address.first_name,
            obj.pickup_address.line1, obj.shipping_address.line1,
            obj.pickup_address.postcode, obj.shipping_address.postcode,
            obj.pickup_address.phone_number, obj.shipping_address.phone_number,
            obj.shipping_address.alternate_phone_number,
            obj.pickup_hub, obj.hub
        ))

    def dump_payment_data(self, obj):
        return format_html(ADMIN_PAYMENT_TAB_DATA % (
            obj.payment_status, obj.payment_mode,
            obj.cash_on_delivery, obj.shipping_charges,
            obj.cash_collected
            # obj.tax, obj.discount
        ))

    def dump_data(self, obj):

        return format_html(ADMIN_GENERAL_TAB_DATA % (
            obj.reference_number, obj.customer_reference_number, obj.hub, obj.latest_hub,
            obj.shipping_address.line1, obj.shipping_address.address_type,
            utc_to_ist(obj.date_placed).strftime(
                settings.ADMIN_DATETIME_FORMAT), obj.otp, utc_to_ist(
                obj.exp_delivery_start_time).strftime(
                settings.ADMIN_DATETIME_FORMAT) if obj.exp_delivery_start_time else None, utc_to_ist(
                obj.expected_delivery_time).strftime(
                settings.ADMIN_DATETIME_FORMAT) if obj.expected_delivery_time else None,
            obj.driver,
            obj.driver.user.phone_number if obj.driver else None,
            obj.shipping_address.first_name,
            obj.shipping_address.phone_number, obj.customer,
            obj.shipping_address.alternate_phone_number,
            obj.customer_contract, obj.cash_on_delivery,
            obj.city,
            obj.weight, obj.volume, obj.priority,
            obj.dimension.length if obj.dimension else 0,
            obj.dimension.breadth if obj.dimension else 0,
            obj.dimension.height if obj.dimension else 0,
            obj.dimension.uom if obj.dimension else '',
            '%0.2f' % obj.total_distance if obj.total_distance else None, obj.batch,
            obj.pickup_address.line1, obj.delivery_instructions,
            obj.division if obj.division else None,
            utc_to_ist(obj.updated_time).strftime(
                settings.ADMIN_DATETIME_FORMAT) if obj.updated_time else None, obj.status
        )
                           )

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = ShipmentOrderEditForm
        else:
            self.form = ShipmentOrderAddForm
        return super(NewShipmentOrderAdmin, self).get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj:  # edit
            return ['order_type', 'number', 'reference_number', 'dispatch_log',
                    'driver', 'customer_contract', 'status', 'customer',
                    'city', 'hub', 'shipping_address', 'dump_payment_data',
                    'expected_delivery_time', 'dump_customer_data',
                    'shipment_info', 'cash_on_delivery',
                    'date_placed', 'dump_data', 'bill_number', 'bill_date']
        else:
            return ['order_type', 'number', 'total_incl_tax',
                    'driver', 'customer_contract', 'status',
                    'dump_data', 'dump_customer_data', 'dump_payment_data']

    def get_pickup_datetime(self, obj):
        if obj:
            return obj.pickup_datetime
        return None

    def customer_name(self, obj):
        if obj and obj.shipping_address:
            return obj.shipping_address.first_name
        return None

    dump_data.short_description = 'Data'
    dump_customer_data.short_description = 'Data'
    dump_payment_data.short_Destcription = 'Data'
    get_pickup_datetime.short_description = "Pick Up Time"
    get_pickup_datetime.admin_order_field = 'Pick Up Time'

    def get_queryset(self, request):
        order_qs = super().get_queryset(request)
        order_qs = order_qs.exclude(customer_contract__customer__api_key=settings.FLASH_SALE_API_KEY)
        if not request.GET.get('pickup_datetime__gte') and not request.GET.get(
            'updated_time__gte') and not request.GET.get(
            '_changelist_filters'):
            end = timezone.now() + timedelta(days=10)
            start = end - timedelta(days=55)
            order_qs = order_qs.filter(pickup_datetime__range=(start, end))
        order_qs = apply_contract_permission_filter(request.user, order_qs)
        order_qs = order_qs.filter(order_type=settings.SHIPMENT_ORDER_TYPE)
        order_qs = order_qs.select_related(
            'pickup_address', 'driver__user',
            'shipping_address', 'customer',
            'hub', 'city', 'driver', 'customer_contract', 'batch'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'event_set',
                queryset=Event.objects.all().order_by('time'),
            )
        )
        return order_qs

    # def get_search_results(self, request, queryset, search_term):
    #     from blowhorn.order.documents import OrderDocument as OrderDoc
    #     from elasticsearch_dsl import Q
    #     from elasticsearch import Elasticsearch
    #     from elasticsearch_dsl import Search
    #     print('search_term - ', search_term)
    #     if not search_term:
    #         queryset, use_distinct = super().get_search_results(request,
    #                                                             queryset,
    #                                                             search_term)
    #         return queryset, use_distinct
    #
    #     client = Elasticsearch([{'host': '18.221.137.53', 'port': '9200'}])
    #     index = 'order_' + settings.ELASTICSEARCH_INDEX_POSTFIX
    #     s = Search(using=client, index=index)[0:1000]
    #     # s = OrderDoc.search().query("match", description="search_term")
    #     """
    #     #works
    #     client = Elasticsearch([{'host': '18.221.137.53', 'port': '9200'}])
    #     s = Search(using=client, index='order_test')
    #     s = s.query('match', order_contract__contract_type="Shipment")
    #     s = s.query("multi_match", query='Bengaluru', fields=['number', 'customer_contract.contract_type', 'customer_contract.city.name'])
    #     s = s.query("multi_match", query=search_term)
    #
    #     Bulk indexing -
    #     from blowhorn.order.models import *
    #     from blowhorn.order.documents import *
    #     from elasticsearch import Elasticsearch
    #     qs = Order.objects.filter(order_type='shipment', customer_id=30647)
    #     docs = []
    #     for s in qs:
    #         docs.append(s.index_doc())
    #     from elasticsearch.helpers import bulk
    #     bulk(client, docs)
    #     """
    #     s = s.query("multi_match", query=search_term)
    #     try:
    #         _response = s.execute()
    #         print('_response - ', _response)
    #         ids_list = []
    #         for hit in _response:
    #             ids_list.append(hit['id'])
    #
    #         queryset = queryset.filter(pk__in=ids_list)
    #         return queryset, False
    #     except:
    #         queryset, use_distinct = super().get_search_results(request,
    #                                                             queryset,
    #                                                             search_term)
    #         return queryset, use_distinct

    # To disable the Delete button on admin
    def has_delete_permission(self, request, obj=None):
        return False

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            'website/js/lib/jquery-ui.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            '/static/website/js/lib/jsBarcode.js',
            '/static/website/js/lib/barcode_model.js',
            '/static/admin/js/shipment_order_confirmation.js',
            '/static/admin/js/order/return_to_hub_confirmation.js'
        )

    class Meta:
        model = Order
        fields = '__all__'


admin.site.register(NewShipmentOrder, NewShipmentOrderAdmin)


class OrderAdjustmentInline(admin.TabularInline):
    model = OrderAdjustment
    extra = 0

    def get_readonly_fields(self, request, obj=None):
        if obj.status == StatusPipeline.ORDER_DELIVERED:
            fields = ('category', 'total_amount', 'comments')

            return fields
        return []

    def has_delete_permission(self, request, obj=None):
        if obj.status == StatusPipeline.ORDER_DELIVERED:
            return False
        return True


class OrderCostHistoryAdminInline(admin.TabularInline):
    verbose_name = _('Cost History')
    verbose_name_plural = _('Cost History')
    model = OrderCostHistory
    extra = 0
    readonly_fields = [
        'old_value', 'new_value', 'reason'
    ]

    can_delete = False

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields': (
                    ('old_value', 'new_value',
                     'reason'),
                )
            }),
        )
        return fieldsets

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.order_by('-id')
        return qs

    # To disable the Add button on admin
    def has_add_permission(self, request, obj):
        return False


class TripAdminInline(admin.TabularInline):
    extra = 0
    max_num = 0
    can_delete = False
    can_add = False
    # show_change_link = True
    form = TripInlineForm

    def has_add_permission(self, request, obj):
        return False

    verbose_name_plural = 'Trips'
    model = Trip
    readonly_fields = ('link_to_trip', 'status', 'driver', 'reason_for_suspension')
    fields = ('link_to_trip', 'driver', 'actual_start_time',
              'actual_end_time', 'status', 'reason_for_suspension')

    def link_to_trip(self, obj):
        if obj:
            link = reverse("admin:trip_trip_change", args=[obj.id])
            return format_html("<a href='{}?_changelist_filters=q%{}%'>{}</a>", link,
                               obj.trip_number, obj.trip_number)
        else:
            return None

    link_to_trip.allow_tags = True
    link_to_trip.short_description = 'Trip'

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.status == StatusPipeline.ORDER_DELIVERED:
            return ['link_to_trip', 'driver', 'actual_start_time',
                    'actual_end_time', 'status', 'reason_for_suspension']
        return ['link_to_trip', 'trip_number', 'status', 'driver', 'reason_for_suspension']


class IsPayTmSuccessFilter(admin.SimpleListFilter):
    title = _('PayTm Transaction Orders')
    parameter_name = 'PayTm Transaction Orders'

    def lookups(self, request, model_admin):

        choices = ['Successful', 'Failed']
        return [(c, c) for c in choices]

    def queryset(self, request, queryset):

        if self.value():
            if self.value() == 'Successful':
                return queryset.filter(payment_mode=PAYMENT_MODE_PAYTM, status=StatusPipeline.ORDER_DELIVERED)
            else:
                return queryset.filter(auto_debit_paytm=True, status=StatusPipeline.ORDER_DELIVERED).exclude(
                    payment_mode=PAYMENT_MODE_PAYTM)
        return queryset


class DriverRatingFilter(admin.SimpleListFilter):
    title = _('Driver Rating')
    parameter_name = 'Driver Rating'

    def lookups(self, request, model_admin):
        self.without_rating = 'Without Rating'
        self.with_rating = 'With Rating'
        choices = list(RatingConfig.objects.all().values_list('rating', flat=True))
        choices.append(self.without_rating)
        choices.append(self.with_rating)

        return [(c, c) for c in choices]

    def queryset(self, request, queryset):

        if self.value():
            if self.value() == self.without_rating:
                return queryset.exclude(
                    id__in=DriverRatingDetails.objects.all().values_list('order_id', flat=True))
            elif self.value() == self.with_rating:
                return queryset.filter(
                    id__in=DriverRatingDetails.objects.all().values_list('order_id', flat=True))
            else:
                orders_with_rating = DriverRatingDetails.objects.filter(
                    rating=self.value()).values_list('order_id', flat=True)
                return queryset.filter(id__in=orders_with_rating)
        return queryset


class BookingOrderAdmin(FSMTransitionMixin, BaseAdmin):
    # resource_class = BookingOrderResource
    change_list_template = 'admin/order/bookingorder/change_list.html'
    change_form_template = 'admin/order/bookingorder/change_form.html'

    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget}
    }
    fsm_field = ['status', ]

    list_display = (
        'number', 'invoice_number', 'tracking_status', 'get_status', 'city',
        'get_customer', 'customer_category', 'get_driver_rating',
        'get_customer_phone_number',
        #        'device_type', 'get_booking_type',
        'pickup_datetime', 'get_pickup_address', 'get_shipment_address',
        'estimated_distance_km', 'get_vehicle_class_preference',
        'get_total_distance', 'get_total_time',
        'get_amount_excluding_tax', 'get_gst_amount', 'get_order_total',
        'discount', 'is_pod_required',
        'link_to_driver', 'get_vehicle',
        'labour_details', 'payment_status', 'payment_mode', 'is_refunded', 'date_placed',)

    # Add WaypointAdminInline after checking performance implications
    inlines = [
        LabourInline, EventAdminInline, DocumentsInline,
        OrderCostHistoryAdminInline, TripAdminInline,
        OrderInternalTrackingInline, OrderAdjustmentInline,
        OrderDispatchAdminInline, DriverRatingInline
    ]

    readonly_fields = ['order_type', 'number', 'actual_cost',
                       'get_tracking_url']

    # Adding date range filter instead of presets
    # ('pickup_datetime', DateFieldFilter),
    _list_filter = [
        'city',
        ('pickup_datetime', DateRangeFilter),
        'customer__customer_category',
        ('driver', admin.RelatedOnlyFieldListFilter),
        ('date_placed', DateFieldFilter),
        'payment_status', 'payment_mode', 'device_type',
        'coupon__code', DriverRatingFilter,
        multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
    ]
    if settings.WEBSITE_BUILD in ['blowhorn']:
        _list_filter.append(IsPayTmSuccessFilter)

    list_filter = _list_filter

    list_select_related = (
        'customer', 'pickup_address', 'shipping_address',
        'driver', 'city', 'vehicle_class_preference'
    )

    search_fields = ('invoice_number', 'number', 'status', 'user__phone_number',
                     'customer__user__name', 'customer__user__phone_number',
                     'driver__user__name',)
    multi_search_fields = ('number',)

    fieldsets = (
        ('General', {
            'fields':
                (('get_customer', 'driver'), 'get_customer_phone_number',
                 'customer_category',
                 'get_driver_rating', 'invoice_number',
                 'city', 'status', 'booking_type', 'date_placed',
                 'pickup_datetime', 'get_vehicle_class_preference',
                 'get_vehicle', 'get_invoice_url',
                 'customer_contract', 'reason_for_cancellation',
                 'other_cancellation_reason', 'suspension_trip_reason', 'last_modified',
                 'trip_status', 'number', 'coupon')
        }),
        ('Cost', {
            'fields': ('estimated_cost', 'actual_cost', 'revised_cost',
                       'adjustment_reason', 'is_pod_required',
                       ('labour_cost', 'labour_details'),
                       ('get_taxable_adjustment_amount',
                        'get_non_taxable_adjustment_amount'), 'tip',
                       'total_excl_tax', 'discount', ('tax', 'rcm',),
                       'total_incl_tax', 'toll_charges', 'amount_paid_online',
                       'extra_charges', 'payable_excluding_extra_charges', 'total_payable', 'is_refunded',
                       'payment_status', 'reason_for_marking_paid', ''
                                                                    'payment_mode', 'get_txn_id', 'get_payment_id',
                       'get_payment_status', 'is_payment_stale')

        }),
        ('Others', {
            'fields': ('items', 'estimated_distance_km', 'get_total_time',
                       'get_total_distance', 'device_type', 'item_category')
        }),
        ('Address Details', {
            'fields': (('pickup_contact_phone_number', 'pickup_contact_name',
                        'pickup_address'),
                       ('pickup_floor_number', 'pickup_service_lift_available', 'pickup_lift_accessible'),
                       ('shipping_contact_phone_number', 'shipping_contact_name', 'shipping_address'),
                       ('shipping_floor_number', 'shipping_is_service_lift_available', 'shipping_is_lift_accessible',))
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
        ('Dispatch Log', {
            'fields': ('dispatch_log',),
        })

    )

    action_form = BookingOrderActionForm
    actions = [export_booking_order_data, mark_orders_cash_paid,
               mark_orders_bank_transfer]

    def get_actions(self, request):
        """
        Remove `Delete` and Add `Generate Payments`
        """
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            if actions.get('mark_orders_cash_paid'):
                del actions['mark_orders_cash_paid']
            if actions.get('mark_orders_bank_transfer'):
                del actions['mark_orders_bank_transfer']
        return actions

    def shipping_contact_phone_number(self, obj):
        return obj.shipping_address.phone_number if obj.shipping_address else ''

    def pickup_service_lift_available(self, obj):
        return obj.pickup_address.is_service_lift_available if obj.pickup_address else ''

    def pickup_lift_accessible(self, obj):
        return obj.pickup_address.is_lift_accessible if obj.pickup_address else ''

    def pickup_floor_number(self, obj):
        return obj.pickup_address.floor_number if obj.pickup_address else ''

    def shipping_floor_number(self, obj):
        return obj.shipping_address.floor_number if obj.shipping_address else ''

    def shipping_is_service_lift_available(self, obj):
        return obj.shipping_address.is_service_lift_available if obj.shipping_address else ''

    def shipping_is_lift_accessible(self, obj):
        return obj.shipping_address.is_lift_accessible if obj.shipping_address else ''

    def shipping_contact_name(self, obj):
        return obj.shipping_address.first_name if obj.shipping_address else ''

    def pickup_contact_phone_number(self, obj):
        return obj.pickup_address.phone_number if obj.pickup_address else ''

    def pickup_contact_name(self, obj):
        return obj.pickup_address.first_name if obj.pickup_address else ''

    shipping_contact_phone_number.short_description = 'Drop Off Contact Number'
    shipping_contact_name.short_description = 'Drop Off Contact Name'
    pickup_contact_phone_number.short_description = 'Pickup Contact Number'
    pickup_contact_name.short_description = 'Pickup Contact Name'
    pickup_floor_number.short_description = 'Pickup Floor #'
    pickup_service_lift_available.short_description = 'Is Lift Available for carrying Goods '
    pickup_lift_accessible.short_description = 'Is Lift Accessible by Vehicle'
    shipping_floor_number.short_description = 'Shipping Floor #'
    shipping_is_service_lift_available.short_description = 'Is Lift Available for carrying Goods '
    shipping_is_lift_accessible.short_description = 'Is Lift Accessible by Vehicle'

    def tracking_status(self, obj):
        tracking_set = obj.orderinternaltracking_set.all()
        if len(tracking_set) > 0:
            if tracking_set[0].status == IT_STATUS_NEW:
                color_code = 'yellow'
            elif tracking_set[0].status == IT_STATUS_FOLLOWUP:
                color_code = 'red'
            elif tracking_set[0].status == IT_STATUS_CONFIRMED:
                color_code = 'green'
            elif tracking_set[0].status == IT_STATUS_CANCELLED:
                color_code = 'orange'
            else:
                color_code = 'black'
            return format_html('<b style="background:{};">{}</b>',
                               color_code,
                               tracking_set[0].status)
        else:
            return '-'

    def get_vehicle_class_preference(self, obj):
        v_class = None
        if obj.vehicle_class_preference:
            v_class = [x.alias_name for x in obj.vehicle_class_preference.cityvehicleclassalias_set.all(
            ) if x.city == obj.city]

        if not v_class:
            v_class = obj.vehicle_class_preference

        if isinstance(v_class, list):
            return ', '.join([clss for clss in v_class if clss])

        return v_class

    get_vehicle_class_preference.short_description = 'Vehicle Class'

    def get_booking_type(self, obj):
        if obj.booking_type == ORDER_TYPE_NOW:
            return CURRENT_BOOKING
        else:
            return ADVANCED_BOOKING

    get_booking_type.short_description = 'Booking Type'

    def labour_cost(self, obj):
        if hasattr(obj, 'labour'):
            return obj.labour.no_of_labours * obj.labour.labour_cost
        return 0

    labour_cost.short_description = 'Labour Cost'

    def labour_details(self, obj):
        if hasattr(obj, 'labour'):
            if obj.labour.no_of_labours:
                currency = settings.CURRENCY_WITHOUT_SYMBOL.get(settings.OSCAR_DEFAULT_CURRENCY)
                return '%d x %s%.0f' % (obj.labour.no_of_labours, currency, obj.labour.labour_cost)
        return None

    labour_details.short_description = 'Labour'

    def get_gst_amount(self, obj):
        return obj.tax

    get_gst_amount.short_description = 'Tax Amount'

    def get_amount_excluding_tax(self, obj):
        return obj.total_excl_tax

    get_amount_excluding_tax.short_description = 'Total Excl Tax'

    def get_pickup_address(self, obj):
        preview = obj.pickup_address and (
            obj.pickup_address.line3 or obj.pickup_address.line1)
        full_address = obj.pickup_address and obj.pickup_address.line1
        preview = (preview or '')[:15]
        return format_html("""<a href='#' title="%s">%s</a>""" % (full_address, preview))

    get_pickup_address.short_description = 'Pickup'

    def get_shipment_address(self, obj):
        preview = obj.shipping_address and (
            obj.shipping_address.line3 or obj.shipping_address.line1)
        full_address = obj.shipping_address and obj.shipping_address.line1
        preview = (preview or '')[:15]
        return format_html("""<a href='#' title="%s">%s</a>""" % (full_address, preview))

    get_shipment_address.short_description = 'Dropoff'

    def get_form(self, request, obj=None, **kwargs):
        self.form = EditBookingOrder(obj.city if obj else None, obj.vehicle_class_preference if obj else None)
        return super(BookingOrderAdmin, self).get_form(request, obj, **kwargs)

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

        extra_context['firebase_apikey'] = settings.FIREBASE_APIKEY
        extra_context['firebase_authdomain'] = settings.FIREBASE_AUTHDOMAIN
        extra_context[
            'firebase_messagingsenderid'] = settings.FIREBASE_MESSAGINGSENDERID
        extra_context[
            'firebase_storagebucket'] = settings.FIREBASE_STORAGEBUCKET
        extra_context['firebase_database_url'] = settings.FIREBASE_DATABASE_URL
        extra_context['order_type_booking'] = settings.C2C_ORDER_TYPE
        extra_context['message_type_new'] = NEW_BOOKING

        if 'action' in request.POST and request.POST['action'] == 'export_booking_order_data':
            if not request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                post.update({admin.ACTION_CHECKBOX_NAME: str(Order.objects.all().first().id)})
                request._set_post(post)
        return super(BookingOrderAdmin, self).changelist_view(request, extra_context)

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        if object_id:
            order = Order.objects.get(pk=object_id)
            extra_context['order_number'] = order.number
            extra_context['cancellation_reasons'] = list(OrderCancellationReason
                                                         .objects.values_list
                                                         ('reason', flat=True))
            if order.status in [StatusPipeline.ORDER_CANCELLED,
                                StatusPipeline.DUMMY_STATUS]:
                extra_context['show_save'] = False
                extra_context['show_save_and_continue'] = False
            else:
                extra_context['show_save'] = True

        try:
            return super().change_view(
                request, object_id, extra_context=extra_context)

        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def get_readonly_fields(self, request, obj=None):
        payment_trip = PaymentTrip.objects.filter(trip__order_id=obj.id,
                                                  payment__status=APPROVED,
                                                  payment__payment_type=DriverPayment.PAYMENT_CHOICES[0][0]).exists()
        fields = ['invoice_number', 'number', 'discount', 'dispatch_log']
        if payment_trip:
            fields += ['revised_cost', 'toll_charges', 'payment_status',
                       'payment_mode', 'adjustment_reason', 'is_payment_stale']

        if obj and obj.status in [
            StatusPipeline.ORDER_CANCELLED,
            StatusPipeline.DUMMY_STATUS]:
            fields += ['get_customer', 'city', 'get_vehicle_class_preference', 'items',
                       'pickup_address', 'pickup_datetime', 'shipping_address', 'get_tracking_url',
                       'driver', 'estimated_cost', 'total_incl_tax', 'status', 'actual_cost',
                       'get_invoice_url', 'booking_type', 'date_placed', 'revised_cost',
                       'get_total_time', 'get_total_distance', 'get_vehicle', 'tip',
                       'total_excl_tax', 'tax', 'total_payable', 'rcm', 'number',
                       'labour_cost', 'labour_details', 'other_cancellation_reason',
                       'adjustment_reason', 'toll_charges', 'payment_status', 'payment_mode', 'is_payment_stale',
                       'get_customer_phone_number', 'estimated_distance_km', 'item_category',
                       'customer_category', 'get_driver_rating', 'device_type', 'created_date', 'created_by',
                       'modified_date',
                       'modified_by', 'customer_contract', 'get_txn_id', 'reason_for_cancellation',
                       'coupon', 'reason_for_marking_paid']

        elif obj and obj.status == StatusPipeline.ORDER_DELIVERED:
            fields += ['get_customer', 'city', 'get_vehicle_class_preference', 'items',
                       'pickup_address', 'pickup_datetime', 'shipping_address', 'get_tracking_url',
                       'driver', 'estimated_cost', 'total_incl_tax', 'status', 'actual_cost',
                       'total_excl_tax', 'tax', 'total_payable', 'rcm',
                       'labour_cost', 'labour_details', 'other_cancellation_reason',
                       'get_total_time', 'get_total_distance', 'get_vehicle', 'tip',
                       'get_invoice_url', 'booking_type', 'date_placed', 'number',
                       'get_customer_phone_number', 'estimated_distance_km', 'item_category',
                       'customer_category', 'get_driver_rating', 'device_type',
                       'created_date', 'created_by', 'modified_date',
                       'modified_by', 'customer_contract', 'get_txn_id', 'coupon', ]
        else:
            if obj:  # edit
                fields += [
                    'get_customer', 'order_type', 'number', 'status', 'city', 'get_tracking_url',
                    'estimated_cost', 'total_incl_tax', 'status', 'actual_cost',
                    'total_excl_tax', 'tax', 'total_payable', 'rcm',
                    'labour_cost', 'labour_details', 'other_cancellation_reason',
                    'get_total_time', 'get_total_distance', 'get_vehicle',
                    'get_invoice_url', 'date_placed', 'get_customer_phone_number',
                    'estimated_distance_km', 'device_type', 'customer_category', 'get_driver_rating',
                    'booking_type', 'created_date', 'created_by', 'modified_date',
                    'modified_by', 'get_vehicle_class_preference', 'get_txn_id']
                if obj.customer.customer_category == CUSTOMER_CATEGORY_INDIVIDUAL:
                    fields += ['customer_contract']
            else:
                fields += [
                    'get_customer', 'order_type', 'number', 'total_incl_tax',
                    'status', 'actual_cost', 'get_total_time',
                    'get_total_distance', 'get_vehicle', 'estimated_cost',
                    'get_tracking_url', 'get_invoice_url', 'revised_cost',
                    'payment_status', 'total_excl_tax', 'tax', 'total_payable',
                    'rcm', 'labour_cost', 'labour_details',
                    'other_cancellation_reason', 'payment_mode', 'date_placed',
                    'get_customer_phone_number', 'estimated_distance_km',
                    'device_type', 'customer_category', 'created_date',
                    'created_by', 'modified_date', 'modified_by', 'get_txn_id',
                    'get_driver_rating']

        if obj and obj.payment_status == PAYMENT_STATUS_PAID:
            fields += ['revised_cost', 'toll_charges', 'adjustment_reason',
                       'payment_status', 'payment_mode', 'reason_for_marking_paid']

        # if obj.status == StatusPipeline.ORDER_NEW:
        #     if obj.booking_type == ORDER_TYPE_NOW and obj.pickup_datetime + timedelta(minutes=5) > timezone.now():
        #         # now booking block for 5 minutes
        #         fields += ['driver']
        #     elif obj.booking_type == ORDER_TYPE_LATER:
        #         if (obj.pickup_datetime - timezone.now()).total_seconds() > 3300 and \
        #                 (obj.date_placed - obj.pickup_datetime).total_seconds() > 3600:
        #             #later booking pickup time greater then 1hr interval, block for
        #             #5 minutes from dispatch start time
        #             fields += ['driver']
        #         elif (obj.date_placed - obj.pickup_datetime).total_seconds() <= 3600 and \
        #                 (timezone.now()- obj.date_placed).total_seconds() < 300:
        #             # later booking pickup interval less then 1 hr block for
        #             # 5 minutes
        #             fields += ['driver']
        tz_zone = pytz.timezone(settings.IST_TIME_ZONE)
        local_datetime = obj.pickup_datetime.astimezone(tz_zone)
        time_diff = (obj.pickup_datetime - obj.date_placed).total_seconds() / 60
        pickup_time_diff = (obj.pickup_datetime - timezone.now()).total_seconds() / 60

        if abs(time_diff) >= 60 and obj.pickup_datetime >= obj.date_placed and pickup_time_diff > \
            OrderConstants().get('LATER_BOOKING_DRIVER_ASSIGN_TIME', 20):
            fields += ['driver']
        elif abs(time_diff < 60) and local_datetime + timedelta(minutes=OrderConstants().get(
            'CURRENT_BOOKING_DRIVER_ASSIGN_TIME', 5)) > timezone.now():
            fields += ['driver']

        fields += ['get_payment_status', 'get_payment_id', 'is_payment_stale', 'toll_charges',
                   'get_taxable_adjustment_amount', 'get_non_taxable_adjustment_amount',
                   'shipping_contact_phone_number', 'shipping_contact_name',
                   'pickup_floor_number', 'pickup_service_lift_available', 'pickup_lift_accessible',
                   'shipping_floor_number', 'shipping_is_service_lift_available', 'shipping_is_lift_accessible',
                   'pickup_contact_phone_number', 'pickup_contact_name', 'is_refunded',
                   'amount_paid_online', 'extra_charges', 'payable_excluding_extra_charges']
        if not settings.ENABLE_REVISED_COST:
            fields += ['revised_cost', 'adjustment_reason']

        return fields

    def get_inline_instances(self, request, obj=None):
        inline_instances = []
        new_inlines = self.inlines
        if obj.waypoint_set.count() > 1:
            new_inlines = [WaypointAdminInline] + self.inlines

        for inline_class in new_inlines:
            inline = inline_class(self.model, self.admin_site)
            if request:
                if not (inline.has_add_permission(request, obj) or
                        inline.has_change_permission(request, obj) or
                        inline.has_delete_permission(request, obj)):
                    continue
                if not inline.has_add_permission(request, obj) or \
                    obj and obj.status in StatusPipeline.ORDER_END_STATUSES:
                    inline.max_num = 0
                    inline.can_delete = False
                    inline.can_add = False
            inline_instances.append(inline)

        return inline_instances

    def get_txn_id(self, obj):
        transaction_ids = set()
        for x in obj.sources.all():
            for y in x.transactions.all():
                transaction_ids.add(y.reference)
        transaction_ids.update(
            list(
                obj.order_entities.exclude(
                    transaction_order__transactions__TXNID=None
                ).values_list('transaction_order__transactions__TXNID', flat=True).distinct()
            )
        )
        return list(transaction_ids)

    get_txn_id.short_description = 'Payment Reference '

    def get_customer_phone_number(self, obj):
        if obj and obj.customer and obj.customer.user:
            return obj.customer.user.phone_number

        return None

    # def get_customer_category(self, obj):
    #     if obj.customer:
    #         return obj.customer.customer_category
    #     return None

    # get_customer_category.short_description = 'Category'

    def get_driver_rating(self, obj):
        driver_rating = DriverRatingDetails.objects.filter(order=obj).last()
        if driver_rating:
            return round(driver_rating.rating) if driver_rating.rating else '-'
        return ''

    get_driver_rating.short_description = 'Driver Rating'

    def get_vehicle(self, obj):
        trip_ = get_trip(obj)  # obj is order object
        return trip_.vehicle if trip_ and trip_.vehicle else None

    get_vehicle.short_description = 'Vehicle'

    def get_taxable_adjustment_amount(self, obj):
        taxable_adjustment = obj.orderadjustment_set.filter(category__is_tax_applicable=True
                                                            ).aggregate(Sum('total_amount'))
        return taxable_adjustment.get('total_amount__sum', 0)

    get_taxable_adjustment_amount.short_description = 'Taxable Adjustment'

    def get_non_taxable_adjustment_amount(self, obj):
        non_taxable_adjustment = obj.orderadjustment_set.filter(category__is_tax_applicable=False
                                                                ).aggregate(Sum('total_amount'))
        return non_taxable_adjustment.get('total_amount__sum', 0)

    get_non_taxable_adjustment_amount.short_description = 'Non Taxable Adjustment'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.filter(
            order_type=settings.C2C_ORDER_TYPE,
            customer_contract__contract_type__in=[CONTRACT_TYPE_SME, CONTRACT_TYPE_C2C])
        qs = qs.select_related('customer')
        qs = qs.select_related('customer__user')
        qs = qs.select_related('driver', 'customer_contract')
        qs = qs.select_related('vehicle_class_preference')

        qs = qs.select_related('shipping_address')
        qs = qs.select_related('pickup_address')
        qs = qs.select_related('shipping_address__country')
        qs = qs.select_related('pickup_address__country')

        qs = qs.select_related('city')
        qs = qs.prefetch_related('sources')
        qs = qs.prefetch_related('sources__transactions')
        qs = qs.select_related('labour', 'coupon')

        qs = qs.prefetch_related(Prefetch('orderinternaltracking_set',
                                          queryset=OrderInternalTracking.objects.all().order_by('-time')))

        qs = qs.prefetch_related('orderadjustment_set')

        #        qs = qs.prefetch_related('waypoint_set')
        #        qs = qs.prefetch_related('waypoint_set__shipping_address')
        #        qs = qs.prefetch_related('waypoint_set__shipping_address__country')
        qs = qs.prefetch_related('trip_set')
        qs = qs.prefetch_related('trip_set__vehicle')
        qs = qs.prefetch_related('vehicle_class_preference__vehiclemodel_set')
        qs = qs.prefetch_related(
            'vehicle_class_preference__cityvehicleclassalias_set')
        qs = qs.prefetch_related(
            'ordercosthistory').order_by('-pickup_datetime')
        self.request = request
        return qs

    # def get_export_queryset(self, request):
    #     qs = super().get_export_queryset(request)
    #     return list(qs)

    def get_status(self, obj):
        if obj and obj.status:
            color_code = ORDER_STATUS_CODE.get(obj.status, 'inprogress')
            return format_html("""
                <span class='%s' id='%s'
                style='cursor:pointer; text-decoration:underline;'
                onClick='openTracking("%s")' title='Click to track
                order'>%s</span>
            """ % (color_code, obj.number, obj.number, obj.status))
        return None

    get_status.short_description = 'Status'
    get_status.admin_order_field = 'status'

    def get_order_total(self, obj):
        if obj.total_payable:
            return obj.total_payable
        return None

    get_order_total.short_description = "Total Cost"
    get_order_total.admin_order_field = 'total_payable'

    def link_to_driver(self, obj):
        if obj.driver_id:
            link = reverse("admin:driver_driver_change", args=[obj.driver_id])
            return format_html('<a href="%s">%s</a>' % (link, obj.driver))
        else:
            return None

    # prevent django admin from escaping html returned above
    link_to_driver.allow_tags = True
    link_to_driver.short_description = 'Driver'

    get_customer_phone_number.short_description = "Mobile Number"

    def get_total_distance(self, obj):
        trip = obj.get_completed_trip()
        if trip:
            distance = trip.gps_distance if trip.gps_distance else 0
            return '%0.1f' % distance
        else:
            return '-'

    def get_tracking_url(self, obj):
        if obj and obj.number:
            host_ = self.request.META.get('HTTP_HOST', 'blowhorn.com')

            return format_html("""
                <a href="http://""" + host_ + """/track/""" + obj.number + """ " target="_blank">
                %s
                </a>
                """ % (obj.number)
                               )

        return None

    get_tracking_url.short_description = 'Track Order'

    def get_invoice_url(self, obj):
        if obj.number and obj.status == StatusPipeline.ORDER_DELIVERED:
            host_ = self.request.META.get('HTTP_HOST', 'blowhorn.com')

            return format_html("""
                <a href="http://""" + host_ + """/invoice/""" + obj.number + """ " target="_blank">
                %s
                </a>
                """ % (obj.number)
                               )

        return '-'

    get_invoice_url.short_description = 'View Invoice'

    def get_payment_id(self, obj):
        trip = get_trip(obj)
        payment_trip = trip.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                # C2c & SME order will have one trip
                url = reverse(
                    'admin:driver_driverpayment_change', args=[trip.payment.id])
                return format_html("<a href='{}'>{}</a>", url, trip.payment.id)
        return ''

    get_payment_id.short_description = 'Driver Payment Id'

    def is_payment_stale(self, obj):
        trip = get_trip(obj)
        payment_trip = trip.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                # C2c & SME order will have one trip
                return trip.payment.is_stale
        return 'No'

    get_payment_id.short_description = 'Driver Payment Id'

    def get_payment_status(self, obj):
        trip = get_trip(obj)
        payment_trip = trip.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                # C2c & SME order will have one trip
                return trip.payment.status
        return ''

    get_payment_status.short_description = 'Driver Payment Status'

    def get_total_time(self, obj):
        trip = obj.get_completed_trip()
        if trip and trip.actual_end_time and trip.actual_start_time:
            duration = trip.actual_end_time - trip.actual_start_time
            return timedelta_to_hmsstr(duration)
        else:
            return '-'

    get_total_time.short_description = 'Total Time'
    get_total_distance.short_description = 'Actual Distance'

    def get_customer(self, obj):
        if obj.customer:
            link = reverse("admin:customer_customer_change",
                           args=[obj.customer_id])
            return format_html(
                '<a href="%s">%s</a>' % (link, obj.customer))
        else:
            return None

    get_customer.allow_tags = True
    get_customer.short_description = 'Customer'

    def save_model(self, request, obj, form, change):
        from blowhorn.order.utils import create_trip_when_driver_assigned
        with transaction.atomic():
            if form.is_valid():
                obj.order_type = settings.C2C_ORDER_TYPE
                if change:
                    old_order_obj = Order.objects.get(id=obj.id)
                    if obj.status == StatusPipeline.ORDER_DELIVERED:
                        cost_fields = ['toll_charges', 'revised_cost', 'payment_mode', 'payment_status']
                        changed_fields = form.changed_data

                        if any(x in cost_fields for x in changed_fields):
                            # cost_fields in changed_fields:
                            if 'revised_cost' in form.changed_data:
                                order_cost_data = {
                                    'order': obj,
                                    'old_value': old_order_obj.revised_cost,
                                    'new_value': obj.revised_cost,
                                    'reason': obj.adjustment_reason
                                }
                                order_cost = OrderCostHistory(**order_cost_data)
                                order_cost.save()
                                if settings.ENABLE_REVISED_COST:
                                    obj.update_invoice_information(revised_cost=obj.revised_cost)
                            trips = Trip.objects.filter(order=obj)
                            stale_reason = '%s Order cost field changes' % (obj.number)
                            DriverPayment.objects.filter(paymenttrip__trip__in=trips).exclude(
                                status=APPROVED).update(is_stale=True,
                                                        stale_reason=stale_reason)

                            obj.update_invoice_information()

                    if obj.driver:
                        if obj.status == StatusPipeline.ORDER_NEW:
                            obj.status = StatusPipeline.DRIVER_ACCEPTED
                            Event.objects.create(status=StatusPipeline.DRIVER_ACCEPTED,
                                                 created_date=timezone.now(),
                                                 created_by=request.user,
                                                 driver=obj.driver,
                                                 order_id=obj.id)

                        if not old_order_obj.driver:
                            # new order, no driver associated with order
                            # create a new trip and assigned a driver
                            old_order_obj.pickup_datetime = obj.pickup_datetime
                            create_trip_when_driver_assigned(
                                obj, request)

                            send_restore_notification_to_driver_app.apply_async((obj.id,), )

                        elif old_order_obj.driver != obj.driver:
                            suspension_trip_reason = form.cleaned_data[
                                'suspension_trip_reason']
                            old_order_obj.pickup_datetime = obj.pickup_datetime
                            # order has driver, contingency case ,
                            # suspend existing trip and create new trip
                            trip = Trip.objects.filter(
                                order_id=obj.id,
                                status__in=[StatusPipeline.TRIP_IN_PROGRESS,
                                            StatusPipeline.TRIP_NEW,
                                            StatusPipeline.DRIVER_ACCEPTED,
                                            StatusPipeline.TRIP_ALL_STOPS_DONE])

                            Trip.objects.filter(
                                order=obj, status__in=[StatusPipeline.TRIP_IN_PROGRESS,
                                                       StatusPipeline.TRIP_NEW,
                                                       StatusPipeline.DRIVER_ACCEPTED,
                                                       StatusPipeline.TRIP_ALL_STOPS_DONE]).update(
                                status=StatusPipeline.TRIP_SUSPENDED,
                                reason_for_suspension=suspension_trip_reason,
                                current_step=-1)

                            Stop.objects.filter(trip__in=trip,
                                                status__in=[StatusPipeline.TRIP_NEW, StatusPipeline.DRIVER_ACCEPTED,
                                                            StatusPipeline.TRIP_IN_PROGRESS]).update(
                                status=StatusPipeline.TRIP_SUSPENDED, waypoint=None)

                            create_trip_when_driver_assigned(
                                obj, request, trip.filter(driver=obj.driver).first())

                            send_restore_notification_to_driver_app.apply_async((obj.id,), )
                            send_restore_notification_to_driver_app.apply_async((obj.id, old_order_obj.driver.id,), )

                        elif old_order_obj.pickup_datetime != obj.pickup_datetime:
                            # trip scheduled at accepted at should change based on
                            # order pick up time
                            trip = Trip.objects.filter(
                                order_id=obj.id,
                                driver_id=old_order_obj.driver,
                                status__in=[
                                    StatusPipeline.TRIP_NEW, StatusPipeline.TRIP_IN_PROGRESS,
                                    StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.TRIP_ALL_STOPS_DONE
                                ]).first()

                            if trip:
                                trip.planned_start_time = obj.pickup_datetime
                                trip.trip_acceptence_time = obj.pickup_datetime
                                trip.save()

                else:
                    obj.device_type = 'Admin'
        super(BookingOrderAdmin, self).save_model(request, obj, form, change)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    class Meta:
        model = Order
        exclude = ['']

    class Media:
        css = {
            'all': ('website/css/toastr.css',
                    '/static/website/css/bootstrap.css',
                    'admin/css/bookings.css',
                    '/static/admin/css/order.css',
                    '/static/admin/css/exotel_call.css',
                    'admin/css/suspension_reason.css',)
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            'admin/js/order/track-frame.js',
            '/static/admin/js/order/c2c_order_cancelation_reason.js',
            '/static/admin/js/order/dummy_order_confirmation.js',
            '/static/admin/js/suspension_reason_modal_bookingorder.js',
            '/static/admin/js/booking_order_call.js',
        )


admin.site.register(BookingOrder, BookingOrderAdmin)


class OrderConstantAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'description')


class GMapAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }
    list_display = ('search_text',)


#
from blowhorn.contract.utils import SubscriptionService


class SpotOrderAdmin(FSMTransitionMixin, admin.ModelAdmin):
    resource_class = OrderResource
    change_list_template = 'admin/order/spotorder/change_list.html'
    fsm_field = ['status', ]
    actions = [export_shipment_order_data]
    list_display = ('number', 'status', 'tracking_status','customer', 'customer_category',
                    'hub', 'city', 'driver', 'date_placed',
                    'pickup_datetime', 'get_trip_vehicle_class',
                    'get_trip_status')

    fieldsets = (
        ('General', {
            'fields': ('num_of_orders', 'number', 'customer_contract',
                       'hub', 'driver', 'pickup_datetime')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        })
    )

    list_filter = [
        ('customer', admin.RelatedOnlyFieldListFilter),
        ('customer_contract', admin.RelatedOnlyFieldListFilter),
        ('hub', admin.RelatedOnlyFieldListFilter),
        ('driver', admin.RelatedOnlyFieldListFilter),
        multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
        ('pickup_datetime', DateRangeFilter),
        'city', 'device_type'
    ]

    list_select_related = (
        'customer', 'driver', 'hub', 'customer_contract'
    )

    search_fields = (
        'number', 'status', 'customer__name', 'hub__name', 'lrnumber__lr_number',)

    def get_inline_instances(self, request, obj=None):
        inline_instances = []
        new_inlines = self.inlines
        if obj:
            new_inlines = [EventAdminInline, WaypointAdminInline, TripInline, LrNumberAdminInline,
                           HistoryAdminInline,OrderInternalTrackingInline] + self.inlines

        for inline_class in new_inlines:
            inline = inline_class(self.model, self.admin_site)
            if request:
                if not (inline._has_add_permission(request, obj) or
                        inline.has_change_permission(request, obj) or
                        inline.has_delete_permission(request, obj)):
                    continue
                if not inline._has_add_permission(request, obj) or \
                    obj and obj.status in StatusPipeline.ORDER_END_STATUSES:
                    inline.max_num = 0
                    inline.can_delete = False
                    inline.can_add = False
            inline_instances.append(inline)

        return inline_instances

    def get_fieldsets(self, request, obj=None):
        if obj:
            fieldsets = (
                ('General', {
                    'fields': ('number', 'status', 'customer_contract', 'city',
                               'hub', 'driver', 'pickup_datetime', 'suspension_trip_reason', 'trip_status')
                }),
                ('Audit Log', {
                    'classes': ('collapse',),
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                })
            )
        else:
            fieldsets = (
                ('General', {
                    'fields': ('num_of_orders', 'number', 'customer_contract',
                               'hub', 'driver', 'pickup_datetime',)
                }),
                ('Audit Log', {
                    'classes': ('collapse',),
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                })
            )

        return fieldsets

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

        extra_context['firebase_apikey'] = settings.FIREBASE_APIKEY
        extra_context['firebase_authdomain'] = settings.FIREBASE_AUTHDOMAIN
        extra_context[
            'firebase_messagingsenderid'] = settings.FIREBASE_MESSAGINGSENDERID
        extra_context[
            'firebase_storagebucket'] = settings.FIREBASE_STORAGEBUCKET
        extra_context['firebase_database_url'] = settings.FIREBASE_DATABASE_URL
        extra_context['order_type_spot'] = settings.SPOT_ORDER_TYPE
        extra_context['message_type_new'] = NEW_BOOKING

        return super(SpotOrderAdmin, self).changelist_view(request, extra_context)

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',
                    'website/css/toastr.css',
                    'admin/css/spot_order.css',
                    '/static/admin/css/order.css',
                    'admin/css/suspension_reason.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/suspension_reason_modal.js',
              '/static/admin/js/order/dummy_order_confirmation.js',
              '/static/admin/js/driver_filter.js',
              '/static/admin/js/hub_filter.js',)

    def save_model(self, request, obj, form, change):
        if form.cleaned_data:
            contract = obj.customer_contract
            hub = obj.hub
            driver = obj.driver
            pickup_datetime = obj.pickup_datetime

            if not change:
                num_of_orders = form.cleaned_data.get('num_of_orders', 1)
                if num_of_orders == 1:
                    """Create single spot order"""
                    SubscriptionService().create_spot_order(
                        contract=contract,
                        hub=hub,
                        driver=driver,
                        invoice_driver=driver,
                        pickup_datetime=pickup_datetime,
                        created_by=request.user)
                elif num_of_orders > 1:
                    """Create bulk spot orders"""
                    SubscriptionService().create_bulk_spot_order(
                        contract=contract,
                        hub=hub,
                        pickup_datetime=pickup_datetime,
                        created_by=request.user,
                        count=num_of_orders)
            else:
                suspension_trip_reason = form.cleaned_data[
                    'suspension_trip_reason']

                trip_status = form.cleaned_data.get('trip_status', None)
                print('spot order test - ', trip_status)

                order = Order.objects.filter(id=obj.id).select_related(
                    'vehicle_class_preference')
                order = order.prefetch_related('trip_set')
                order = order.prefetch_related('driver')
                order = order.prefetch_related('driver__vehicles')[0]
                if (order.driver and order.driver != driver) or (
                    not order.driver and driver and order.trip_set.filter(driver__isnull=False).exists()):
                    TripService().create_contingency_trip(
                        order.driver, obj, suspension_trip_reason, trip_status)
                elif driver and not order.driver:
                    TripMixin().update_driver_for_trip(driver, obj, obj.driver.contract)

                obj.customer_contract = contract
                obj.hub = hub
                obj.driver = driver
                obj.pickup_datetime = pickup_datetime
                obj.modified_by = request.user
                obj.modified_date = timezone.now()
                super(SpotOrderAdmin, self).save_model(request, obj, form, change)

    # Dont call super().save_model again, order already saved above

    ''' change_view and add_view are used here
        to disable save and continue button
    '''

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        # extra_context['show_save_and_add_another'] = False
        # extra_context['show_save'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    def tracking_status(self, obj):
        tracking_set = obj.orderinternaltracking_set.all()
        if len(tracking_set) > 0:
            if tracking_set[0].status == IT_STATUS_NEW:
                color_code = 'yellow'
            elif tracking_set[0].status == IT_STATUS_FOLLOWUP:
                color_code = 'red'
            elif tracking_set[0].status == IT_STATUS_CONFIRMED:
                color_code = 'green'
            elif tracking_set[0].status == IT_STATUS_CANCELLED:
                color_code = 'orange'
            else:
                color_code = 'black'
            return format_html('<b style="background:{};">{}</b>',
                               color_code,
                               tracking_set[0].status)
        else:
            return '-'


    def add_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        try:
            return super().add_view(request, extra_context=extra_context)
        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(request.user, qs)
        qs = qs.filter(order_type=settings.SPOT_ORDER_TYPE)
        qs = qs.select_related('customer', 'driver', 'hub__customer')
        qs = qs.select_related('created_by', 'modified_by')
        qs = qs.prefetch_related('customer_contract__vehicle_classes')
        qs = qs.prefetch_related('trip_set')
        qs = qs.prefetch_related('trip_set__vehicle')
        qs = qs.prefetch_related(
            'trip_set__vehicle__vehicle_model__vehicle_class')
        qs = qs.select_related('city')
        return qs

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_export_queryset(self, request):
        # Convert the queryset to list as the export function uses
        # an iterator and hence prefetch related doesn't work. Without
        # prefetch_related the number of queries explodes impacting performance
        # and function not working at all for large volumes
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_trip_vehicle_class(self, obj):
        trip_ = get_trip(obj)  # obj is order object
        return trip_.vehicle.vehicle_model.vehicle_class if trip_ and trip_.vehicle else '-'

    def get_trip_status(self, obj):
        trips = obj.trip_set.all()
        if trips:
            trips_sorted = sorted(trips, key=lambda x: x.trip_creation_time)
            return trips_sorted[-1].status
        else:
            return '-'

    get_trip_vehicle_class.short_description = 'Vehicle Class'
    get_trip_status.short_description = 'Trip Status'

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions and not request.user.is_superuser:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    # astimezonelocal_tz = pytz.timezone(settings.IST_TIME_ZONE)
    # todays_date = timezone.now().astimezone(local_tz).date()

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            if obj.status not in [StatusPipeline.ORDER_DELIVERED,
                                  StatusPipeline.ORDER_CANCELLED,
                                  StatusPipeline.DUMMY_STATUS,
                                  StatusPipeline.ORDER_EXPIRED]:
                fields = ['status', 'number', 'customer_contract', 'hub',
                          'pickup_datetime', 'created_date',
                          'created_by', 'modified_date',
                          'modified_by', 'city']
            else:
                fields = ['status', 'number', 'customer_contract', 'hub',
                          'pickup_datetime', 'created_date',
                          'created_by', 'modified_date',
                          'modified_by', 'driver', 'city']
        else:
            fields = ['status', 'number', 'created_date', 'created_by', 'modified_date',
                      'modified_by']
        # if obj and obj.pickup_datetime + timedelta(hours=OrderConstants().get(
        #     'DRIVER_REASSIGNMENT_ALLOWED_HOURS', 24)) <= timezone.now():
        #     fields += ['driver']
        return fields + ['driver']

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = SpotOrderEditForm
        else:
            self.form = AddSpotOrder(request)

        return super().get_form(request, obj, **kwargs)

    class Meta:
        model = Order
        fields = '__all__'


admin.site.register(SpotOrder, SpotOrderAdmin)


class DefcomOrderAdmin(FSMTransitionMixin, admin.ModelAdmin):
    resource_class = OrderResource
    fsm_field = ['status', ]
    actions = [export_shipment_order_data]
    list_display = ('number', 'status', 'customer', 'customer_category',
                    'hub', 'city', 'driver', 'date_placed',
                    'pickup_datetime', 'get_trip_vehicle_class',
                    'get_trip_status')

    fieldsets = (
        ('General', {
            'fields': ('customer_contract','meter_reading_start','meter_reading_end',
                       'hub', 'driver', 'pickup_datetime')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        })
    )

    list_filter = [
        ('customer', admin.RelatedOnlyFieldListFilter),
        ('customer_contract', admin.RelatedOnlyFieldListFilter),
        ('hub', admin.RelatedOnlyFieldListFilter),
        ('driver', admin.RelatedOnlyFieldListFilter),
        multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
        ('pickup_datetime', DateRangeFilter),
        'city', 'device_type'
    ]

    list_select_related = (
        'customer', 'driver', 'hub', 'customer_contract'
    )

    search_fields = (
        'number', 'status', 'customer__name', 'hub__name', 'lrnumber__lr_number',)

    def get_inline_instances(self, request, obj=None):
        inline_instances = []
        new_inlines = self.inlines
        if obj:
            new_inlines = [EventAdminInline, WaypointAdminInline, TripInline, LrNumberAdminInline,
                           HistoryAdminInline] + self.inlines

        for inline_class in new_inlines:
            inline = inline_class(self.model, self.admin_site)
            if request:
                if not (inline._has_add_permission(request, obj) or
                        inline.has_change_permission(request, obj) or
                        inline.has_delete_permission(request, obj)):
                    continue
                if not inline._has_add_permission(request, obj) or \
                    obj and obj.status in StatusPipeline.ORDER_END_STATUSES:
                    inline.max_num = 0
                    inline.can_delete = False
                    inline.can_add = False
            inline_instances.append(inline)

        return inline_instances

    def get_fieldsets(self, request, obj=None):
        if obj:
            fieldsets = (
                ('General', {
                    'fields': ('status', 'customer_contract', 'city',
                               'hub', 'driver', 'pickup_datetime', 'suspension_trip_reason')
                }),
                ('Audit Log', {
                    'classes': ('collapse',),
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                })
            )
        else:
            fieldsets = (
                ('General', {
                    'fields': ('customer_contract', 'meter_reading_start', 'meter_reading_end','actual_start_time','actual_end_time',
                               'hub', 'driver','trip_base_pay')
                }),
                # ('Audit Log', {
                #     'classes': ('collapse',),
                #     'fields': ('created_date', 'created_by', 'modified_date'),
                # })
            )

        return fieldsets

    # class Media:
    #     css = {
    #         'all': ('/static/website/css/bootstrap.css',
    #                 '/static/admin/css/order.css',
    #                 'admin/css/suspension_reason.css',)
    #     }
    #     js = ('/static/website/js/lib/jquery.min.js',
    #           '/static/website/js/lib/bootstrap.min.js',
    #           '/static/admin/js/suspension_reason_modal.js',
    #           '/static/admin/js/order/dummy_order_confirmation.js',
    #           '/static/admin/js/driver_filter.js',
    #           # '/static/admin/js/hub_filter.js'
    #           )

    def save_model(self, request, obj, form, change):
        if form.cleaned_data:
            contract = obj.customer_contract
            hub = obj.hub
            driver = obj.driver
            pickup_datetime = obj.pickup_datetime
            mr_start = form.cleaned_data.get('meter_reading_start')
            mr_end = form.cleaned_data.get('meter_reading_end')
            start_time = form.cleaned_data.get('actual_start_time')
            end_time = form.cleaned_data.get('actual_end_time')
            pickup_datetime=start_time
            base_pay=form.cleaned_data.get('trip_base_pay')

            if not change:
                SubscriptionService().create_deafcom_order(
                    contract=contract,
                    hub=hub,
                    driver=driver,
                    invoice_driver=driver,
                    pickup_datetime=pickup_datetime,
                    created_by=request.user,
                    meter_reading_start=mr_start,
                    meter_reading_end=mr_end,
                    actual_start_time=start_time,
                    actual_end_time=end_time,
                    trip_base_pay =base_pay,
                )
            else:
                suspension_trip_reason = form.cleaned_data[
                    'suspeactual_end_timension_trip_reason']

                trip_status = form.cleaned_data.get('trip_status', None)

                order = Order.objects.filter(id=obj.id).select_related(
                    'vehicle_class_preference')
                order = order.prefetch_related('trip_set')
                order = order.prefetch_related('driver')
                order = order.prefetch_related('driver__vehicles')[0]
                if (order.driver and order.driver != driver) or (
                    not order.driver and driver and order.trip_set.filter(driver__isnull=False).exists()):
                    TripService().create_contingency_trip(
                        order.driver, obj, suspension_trip_reason, trip_status)
                elif driver and not order.driver:
                    TripMixin().update_driver_for_trip(driver, obj, obj.driver.contract)

                obj.customer_contract = contract
                obj.hub = hub
                obj.driver = driver
                obj.pickup_datetime = pickup_datetime
                obj.modified_by = request.user
                obj.modified_date = timezone.now()
                super(DefcomOrderAdmin, self)

    # Dont call super().save_model again, order already saved above

    ''' change_view and add_view are used here
        to disable save and continue button
    '''

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        # extra_context['show_save_and_add_another'] = False
        # extra_context['show_save'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    def add_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        try:
            return super().add_view(request, extra_context=extra_context)
        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(request.user, qs)
        qs = qs.filter(order_type=settings.DEAFCOM_ORDER_TYPE)
        qs = qs.select_related('customer', 'driver', 'hub__customer')
        qs = qs.select_related('created_by', 'modified_by')
        qs = qs.prefetch_related('customer_contract__vehicle_classes')
        qs = qs.prefetch_related('trip_set')
        qs = qs.prefetch_related('trip_set__vehicle')
        qs = qs.prefetch_related(
            'trip_set__vehicle__vehicle_model__vehicle_class')
        qs = qs.select_related('city')
        qs = qs.select_related('customer_contract')
        return qs

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_export_queryset(self, request):
        # Convert the queryset to list as the export function uses
        # an iterator and hence prefetch related doesn't work. Without
        # prefetch_related the number of queries explodes impacting performance
        # and function not working at all for large volumes
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_trip_vehicle_class(self, obj):
        trip_ = get_trip(obj)  # obj is order object
        return trip_.vehicle.vehicle_model.vehicle_class if trip_ and trip_.vehicle else None

    def get_trip_status(self, obj):
        trips = obj.trip_set.all()
        if trips:
            trips_sorted = sorted(trips, key=lambda x: x.trip_creation_time)
            return trips_sorted[-1].status
        else:
            return None

    get_trip_vehicle_class.short_description = 'Vehicle Class'
    get_trip_status.short_description = 'Trip Status'

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions and not request.user.is_superuser:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            if obj.status not in [StatusPipeline.ORDER_DELIVERED,
                                  StatusPipeline.ORDER_CANCELLED,
                                  StatusPipeline.DUMMY_STATUS,
                                  StatusPipeline.ORDER_EXPIRED]:
                fields = ['status', 'number', 'customer_contract', 'hub',
                          'pickup_datetime', 'created_date',
                          'created_by', 'modified_date','driver','modified_by']
            else:
                fields = ['status', 'number', 'customer_contract', 'hub',
                          'pickup_datetime', 'created_date',
                          'created_by', 'modified_date',
                          'modified_by', 'city']
        else:
            fields = ['status', 'number', 'created_date', 'created_by', 'modified_date',
                      'modified_by']
        # if obj and obj.pickup_datetime + timedelta(hours=OrderConstants().get(
        #     'DRIVER_REASSIGNMENT_ALLOWED_HOURS', 24)) <= timezone.now():
        #     fields += ['driver']
        return fields + ['city']

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = DeafcomOrderEditForm
        else:
            self.form = AddDeafcomOrder(request)

        return super().get_form(request, obj, **kwargs)

    class Meta:
        model = Order
        fields = '__all__'


admin.site.register(DefcomOrder, DefcomOrderAdmin)


class OndemandOrderAdmin(FSMTransitionMixin, admin.ModelAdmin):
    resource_class = OrderResource
    fsm_field = ['status', ]
    actions = [export_shipment_order_data]
    list_display = ('number', 'status', 'customer', 'customer_category',
                    'hub', 'city', 'driver', 'date_placed',
                    'pickup_datetime', 'get_trip_vehicle_class',
                    'get_trip_status')

    fieldsets = (
        ('General', {
            'fields': ('num_of_orders', 'number', 'customer_contract',
                       'hub', 'driver', 'pickup_datetime')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        })
    )

    list_filter = [
        ('customer', admin.RelatedOnlyFieldListFilter),
        ('customer_contract', admin.RelatedOnlyFieldListFilter),
        ('hub', admin.RelatedOnlyFieldListFilter),
        ('driver', admin.RelatedOnlyFieldListFilter),
        multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
        ('pickup_datetime', DateRangeFilter),
        'city', 'device_type'
    ]

    list_select_related = (
        'customer', 'driver', 'hub', 'customer_contract'
    )

    search_fields = (
        'number', 'status', 'customer__name', 'hub__name', 'lrnumber__lr_number',)

    def get_inline_instances(self, request, obj=None):
        inline_instances = []
        new_inlines = self.inlines
        if obj:
            new_inlines = [EventAdminInline, WaypointAdminInline, TripInline, LrNumberAdminInline,
                           HistoryAdminInline] + self.inlines

        for inline_class in new_inlines:
            inline = inline_class(self.model, self.admin_site)
            if request:
                if not (inline._has_add_permission(request, obj) or
                        inline.has_change_permission(request, obj) or
                        inline.has_delete_permission(request, obj)):
                    continue
                if not inline._has_add_permission(request, obj) or \
                    obj and obj.status in StatusPipeline.ORDER_END_STATUSES:
                    inline.max_num = 0
                    inline.can_delete = False
                    inline.can_add = False
            inline_instances.append(inline)

        return inline_instances

    def get_fieldsets(self, request, obj=None):
        if obj:
            fieldsets = (
                ('General', {
                    'fields': ('status', 'customer_contract', 'city',
                               'hub', 'pickup_datetime', 'suspension_trip_reason', 'trip_status','driver')
                }),
                ('Audit Log', {
                    'classes': ('collapse',),
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                })
            )
        else:
            fieldsets = (
                ('General', {
                    'fields': ('num_of_orders', 'customer_contract',
                               'hub','pickup_datetime',)
                }),
                ('Audit Log', {
                    'classes': ('collapse',),
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                })
            )

        return fieldsets

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',
                    '/static/admin/css/order.css',
                    'admin/css/suspension_reason.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/suspension_reason_modal.js',
              '/static/admin/js/order/dummy_order_confirmation.js',
              '/static/admin/js/driver_filter.js',
              '/static/admin/js/hub_filter.js',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(request.user, qs)
        qs = qs.filter(order_type=settings.ONDEMAND_ORDER_TYPE)
        qs = qs.select_related('customer', 'driver', 'hub__customer')
        qs = qs.select_related('created_by', 'modified_by')
        qs = qs.prefetch_related('customer_contract__vehicle_classes')
        qs = qs.prefetch_related('trip_set')
        qs = qs.prefetch_related('trip_set__vehicle')
        qs = qs.prefetch_related('trip_set__vehicle__vehicle_model__vehicle_class')
        qs = qs.select_related('city')
        return qs

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    def add_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        try:
            return super().add_view(request, extra_context=extra_context)
        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_export_queryset(self, request):
        # Convert the queryset to list as the export function uses
        # an iterator and hence prefetch related doesn't work. Without
        # prefetch_related the number of queries explodes impacting performance
        # and function not working at all for large volumes
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_trip_vehicle_class(self, obj):
        trip_ = get_trip(obj)  # obj is order object
        return trip_.vehicle.vehicle_model.vehicle_class if trip_ and trip_.vehicle else None

    def get_trip_status(self, obj):
        trips = obj.trip_set.all()
        if trips:
            trips_sorted = sorted(trips, key=lambda x: x.trip_creation_time)
            return trips_sorted[-1].status
        else:
            return None

    get_trip_vehicle_class.short_description = 'Vehicle Class'
    get_trip_status.short_description = 'Trip Status'

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions and not request.user.is_superuser:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            if obj.status not in [StatusPipeline.ORDER_DELIVERED,
                                  StatusPipeline.ORDER_CANCELLED,
                                  StatusPipeline.DUMMY_STATUS,
                                  StatusPipeline.ORDER_EXPIRED]:
                fields = ['status', 'number', 'customer_contract', 'hub',
                          'pickup_datetime', 'created_date',
                          'created_by', 'modified_date',
                          'modified_by', 'city']
            else:
                fields = ['status', 'number', 'customer_contract', 'hub',
                          'pickup_datetime', 'created_date',
                          'created_by', 'modified_date',
                          'modified_by', 'driver', 'city']
        else:
            fields = ['status', 'number', 'created_date', 'created_by', 'modified_date',
                      'modified_by']
        # if obj and obj.pickup_datetime + timedelta(hours=OrderConstants().get(
        #     'DRIVER_REASSIGNMENT_ALLOWED_HOURS', 24)) <= timezone.now():
        #     fields += ['driver']
        return fields + ['driver']

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = SpotOrderEditForm
        else:
            self.form = AddSpotOrder(request)

        return super().get_form(request, obj, **kwargs)

    class Meta:
        model = Order
        fields = '__all__'


admin.site.register(OndemandOrder, OndemandOrderAdmin)


class FixedOrderAdmin(FSMTransitionMixin, admin.ModelAdmin):
    resource_class = OrderResource
    show_full_result_count = False

    fsm_field = ['status', ]
    actions = [export_shipment_order_data]
    list_display = ('number', 'status', 'customer', 'customer_category', 'hub',
                    'city', 'driver', 'pickup_datetime',
                    'get_trip_vehicle_class', 'get_trip_status', 'get_batch_id')

    fieldsets = (
        ('General', {
            'fields': ('number', 'status', 'customer', 'customer_contract', 'city',
                       'hub', 'suspension_trip_reason', 'trip_status', 'get_batch_id')
        }),
        ('Driver Info', {
            'fields': ('driver',)
        }),
        ('Date Related Info', {
            'fields': ('pickup_datetime', 'date_placed')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        })
    )

    readonly_fields = ()

    list_filter = [
        ('customer', admin.RelatedOnlyFieldListFilter),
        ('customer_contract', admin.RelatedOnlyFieldListFilter),
        ('hub', admin.RelatedOnlyFieldListFilter),
        ('driver', admin.RelatedOnlyFieldListFilter),
        multiple_choice_list_filter(**ORDER_STATUS_FILTER_CONFIG),
        ('pickup_datetime', DateRangeFilter),
        'city', 'batch_id',
    ]

    list_select_related = (
        'customer', 'driver', 'hub', 'customer_contract'
    )

    search_fields = ('number', 'status', 'customer__name', 'hub__name',
                        # 'customer_contract__name', 'driver__name',
                        'batch__description', 'lrnumber__lr_number')

    inlines = [WaypointAdminInline, EventAdminInline, TripInline,
               LrNumberAdminInline]

    def get_batch_id(self, obj):
        batch = obj.batch
        if batch:
            url = reverse(
                'admin:order_orderbatch_change', args=[batch.id])
            return format_html('<a target="_blank" href="{}">{}</a>', url, batch)
        return obj.batch

    get_batch_id.short_description = 'Order Batch'

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.status not in [StatusPipeline.ORDER_DELIVERED,
                                      StatusPipeline.ORDER_CANCELLED, StatusPipeline.DUMMY_STATUS]:
            fields = ['number', 'customer', 'customer_contract', 'city', 'hub',
                      'status', 'pickup_address', 'pickup_datetime',
                      'date_placed', 'created_date', 'created_by', 'modified_date',
                      'modified_by', 'get_batch_id']
        else:
            fields = ['number', 'customer', 'customer_contract', 'city', 'hub',
                      'status', 'pickup_address', 'pickup_datetime',
                      'date_placed', 'created_date', 'created_by', 'modified_date',
                      'modified_by', 'driver', 'get_batch_id']

        fields += ['driver']

        return fields

    def has_add_permission(self, request):
        """
        Subscription Orders are auto-generated, if needed spot orders can be
        placed via Spot Orders
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Subscription Orders are auto-generated, if needed spot orders can be
        placed via Spot Orders
        """
        if request.user.is_superuser:
            return True
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(request.user, qs)
        qs = qs.filter(order_type=settings.SUBSCRIPTION_ORDER_TYPE)
        if not request.GET.get('pickup_datetime__gte') and \
            not request.GET.get('_changelist_filters'):
            end = timezone.now() + timedelta(days=10)
            start = end - timedelta(days=55)
            qs = qs.filter(pickup_datetime__range=(start, end))
        qs = qs.select_related('customer', 'driver', 'hub__customer')
        qs = qs.select_related('created_by', 'modified_by', 'batch')
        qs = qs.prefetch_related('customer_contract__vehicle_classes')
        qs = qs.prefetch_related('trip_set')
        qs = qs.prefetch_related('trip_set__vehicle')
        qs = qs.prefetch_related(
            'trip_set__vehicle__vehicle_model__vehicle_class')
        qs = qs.select_related('city')
        return qs

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        return list(qs)

    def get_trip_vehicle_class(self, obj):
        trip_ = get_trip(obj)  # obj is order object
        return trip_.vehicle.vehicle_model.vehicle_class if trip_ and trip_.vehicle else None

    def get_trip_status(self, obj):
        trips = obj.trip_set.all()
        if trips:
            trips_sorted = sorted(trips, key=lambda x: x.trip_creation_time)
            return trips_sorted[-1].status
        else:
            return None

    get_trip_vehicle_class.short_description = 'Vehicle Class'
    get_trip_status.short_description = 'Trip Status'

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions and not request.user.is_superuser:
            del actions['delete_selected']
        return actions

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = FixedOrderEditForm

        return super().get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        if form.is_valid():

            if change:
                order = Order.objects.filter(id=obj.id).select_related(
                    'vehicle_class_preference')
                order = order.prefetch_related('trip_set')
                order = order.prefetch_related('driver')
                order = order.prefetch_related('driver__vehicles')

                suspension_trip_reason = form.cleaned_data[
                    'suspension_trip_reason']

                trip_status = form.cleaned_data.get('trip_status', None)
                if (order[0].driver != obj.driver and order[0].driver) or (
                    not order[0].driver and obj.driver and order[0].trip_set.filter(driver__isnull=False).exists()):
                    TripService().create_contingency_trip(
                        order[0].driver, obj, suspension_trip_reason, trip_status)
                elif obj.driver and not order[0].driver:
                    TripMixin().update_driver_for_trip(obj.driver, obj, obj.driver.contract)
                obj.modified_by = request.user

        super().save_model(request, obj, form, change)

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',
                    '/static/admin/css/order.css',
                    'admin/css/suspension_reason.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/order/dummy_order_confirmation.js',
              '/static/admin/js/suspension_reason_modal_fixedorder.js',)

    class Meta:
        model = Order
        fields = '__all__'


admin.site.register(FixedOrder, FixedOrderAdmin)


class ItemCategoryAdmin(admin.ModelAdmin):
    list_display = ('category', 'is_active', 'is_tax_exempt', 'load_preference')


class ShippingAddressAdmin(admin.ModelAdmin):
    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'line1'},
        'GooglePointFieldWidget'
    )
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget(settings=PointFieldWidgetSettings)},
    }

    form = ShippingAddressForm
    fieldsets = (
        (None, {
            'fields': (
                ('first_name', 'last_name',), 'phone_number', 'alternate_phone_number',
                'line1', 'line3', ('line4', 'state',), 'postcode', 'geopoint', 'country')
        }),
    )


class KioskOrderAdmin(admin.ModelAdmin):
    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget}
    }
    actions = [create_new_trip_for_orders, ]
    action_form = AssignDriverActionForm

    list_display = (
        'number', 'get_customer', 'get_customer_phone_number', 'status',
        'city', 'pickup_datetime', 'estimated_distance_km', 'payment_status',
        'payment_mode', 'date_placed', 'invoice_number',
    )
    # Add WaypointAdminInline after checking performance implications

    list_filter = [
        'city', IsPayTmSuccessFilter,
        ('pickup_datetime', DateRangeFilter),
        'customer__customer_category',
        ('driver', admin.RelatedOnlyFieldListFilter), 'status',
        ('date_placed', DateFieldFilter),
        'payment_status', 'payment_mode',
    ]

    list_select_related = (
        'customer', 'pickup_address', 'shipping_address', 'driver', 'city',
        'vehicle_class_preference',
    )

    search_fields = ('invoice_number', 'number', 'status',
                     'user__phone_number', 'customer__user__name',
                     'customer__user__phone_number', 'driver__user__name',)

    def get_fieldsets(self, request, obj=None):
        if obj:
            fieldsets = (
                ('General', {
                    'fields': (('customer', 'driver'), 'customer_contract',
                               'get_customer_phone_number', 'number', 'city',
                               'invoice_number', 'status', 'date_placed',
                               'pickup_datetime', 'shipping_address',
                               'pickup_address', 'is_pod_required')
                }),
                ('Cost', {
                    'fields': (
                        'actual_cost', 'revised_cost', 'adjustment_reason',
                        'fixed_price', 'total_excl_tax', ('tax', 'rcm',),
                        'total_incl_tax', 'toll_charges', 'total_payable',
                        'payment_status', 'payment_mode', 'get_txn_id',
                        'get_payment_id', 'get_payment_status',
                        'is_payment_stale')
                }),
                ('Others', {
                    'fields': ('estimated_distance_km', 'floor_number',
                               'number_of_items')
                }),
            )
        else:
            fieldsets = (
                ('General', {
                    'fields': (('customer_mobile', 'driver'), 'customer_name',
                               'customer_contract', 'items', 'shipping_address',
                               'pickup_address', 'labour_amount',
                               'payment_status', 'payment_mode',)
                }),
            )

        return fieldsets

    def get_form(self, request, obj=None, **kwargs):
        if not obj:
            self.form = IkeaOrderForm
        else:
            self.form = IkeaEditOrderForm

        return super(KioskOrderAdmin, self).get_form(request, obj, **kwargs)

    def get_payment_id(self, obj):
        trip = get_trip(obj)
        payment_trip = trip.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                # C2c & SME order will have one trip
                url = reverse(
                    'admin:driver_driverpayment_change', args=[trip.payment.id])
                return format_html("<a href='{}'>{}</a>", url, trip.payment.id)
        return ''

    get_payment_id.short_description = 'Driver Payment Id'

    def is_payment_stale(self, obj):
        trip = get_trip(obj)
        payment_trip = trip.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                # C2c & SME order will have one trip
                return trip.payment.is_stale
        return 'No'

    is_payment_stale.short_description = 'Is Payment Stale'

    def get_payment_status(self, obj):
        trip = get_trip(obj)
        payment_trip = trip.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                # C2c & SME order will have one trip
                return trip.payment.status
        return ''

    get_payment_status.short_description = 'Driver Payment Status'

    def labour_cost(self, obj):
        if hasattr(obj, 'labour'):
            return obj.labour.no_of_labours * obj.labour.labour_cost
        return 0

    labour_cost.short_description = 'Labour Cost'

    def labour_details(self, obj):
        if hasattr(obj, 'labour'):
            if obj.labour.no_of_labours:
                return '%d x ₹%.0f' % (obj.labour.no_of_labours,
                                       obj.labour.labour_cost)
        return None

    labour_details.short_description = 'Labour'

    def get_txn_id(self, obj):
        transaction_ids = set()
        for x in obj.sources.all():
            for y in x.transactions.all():
                transaction_ids.add(y.reference)
        transaction_ids.update(
            list(
                obj.order_entities.exclude(
                    transaction_order__transactions__TXNID=None
                ).values_list('transaction_order__transactions__TXNID',
                              flat=True).distinct()
            )
        )
        return list(transaction_ids)

    get_txn_id.short_description = 'Payment Reference '

    def get_customer_phone_number(self, obj):
        if obj and obj.customer and obj.customer.user:
            return obj.customer.user.phone_number
        return None

    def get_customer(self, obj):
        if obj.customer:
            link = reverse("admin:customer_customer_change",
                           args=[obj.customer_id])
            return format_html('<a href="%s">%s</a>' % (link, obj.customer))
        return None

    get_customer.allow_tags = True
    get_customer.short_description = 'Customer'

    get_customer_phone_number.short_description = 'Customer Mobile'

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):

        fields = ['invoice_number', 'number', 'date_placed', 'get_txn_id', 'driver',
                  'rcm', 'fixed_price', 'labour_cost', 'labour_details',
                  'get_customer_phone_number', 'status', 'city', 'actual_cost',
                  'total_incl_tax', 'tax', 'total_payable', 'total_excl_tax',
                  'customer', 'get_payment_id', 'get_payment_status',
                  'is_payment_stale', 'estimated_distance_km']
        if obj.status == StatusPipeline.ORDER_DELIVERED:
            fields += ['pickup_address', 'shipping_address',
                       'pickup_datetime', 'customer_contract']

        payment_trip = PaymentTrip.objects.filter(
            trip__order_id=obj.id, payment__status=APPROVED,
            payment__payment_type=DriverPayment.PAYMENT_CHOICES[0][0]).exists()
        if payment_trip:
            fields += ['revised_cost', 'toll_charges', 'payment_status',
                       'payment_mode', 'adjustment_reason', ]
        return fields

    inlines = [LabourInline, EventAdminInline, OrderCostHistoryAdminInline,
               TripAdminInline]

    def get_queryset(self, request):
        # Exclude the waypoint which points to the dropoff address
        # already present in the order
        qs = super().get_queryset(request)
        qs = qs.filter(customer_contract__contract_type=CONTRACT_TYPE_KIOSK)
        qs = qs.select_related('customer')
        qs = qs.select_related('customer__user')
        qs = qs.select_related('driver', 'customer_contract')
        qs = qs.select_related('vehicle_class_preference')

        qs = qs.select_related('shipping_address')
        qs = qs.select_related('pickup_address')
        qs = qs.select_related('shipping_address__country')
        qs = qs.select_related('pickup_address__country')

        qs = qs.select_related('city')
        qs = qs.prefetch_related('sources')
        qs = qs.prefetch_related('sources__transactions')
        qs = qs.select_related('labour', 'coupon')
        qs = qs.prefetch_related('trip_set')
        qs = qs.prefetch_related('trip_set__vehicle')
        return qs.order_by('-pickup_datetime')

    def save_model(self, request, obj, form, change):
        from blowhorn.order.utils import create_trip_when_driver_assigned
        with transaction.atomic():
            if form.is_valid():
                if change:
                    old_order_obj = Order.objects.get(id=obj.id)
                    if obj.status == StatusPipeline.ORDER_DELIVERED:
                        cost_fields = ['toll_charges', 'revised_cost',
                                       'payment_mode', 'payment_status']
                        changed_fields = form.changed_data

                        if any(x in cost_fields for x in changed_fields):
                            # cost_fields in changed_fields:
                            if 'revised_cost' in form.changed_data:
                                order_cost_data = {
                                    'order': obj,
                                    'old_value': old_order_obj.revised_cost,
                                    'new_value': obj.revised_cost,
                                    'reason': obj.adjustment_reason
                                }
                                order_cost = OrderCostHistory(**order_cost_data)
                                order_cost.save()
                            trips = Trip.objects.filter(order=obj)
                            DriverPayment.objects.filter(
                                paymenttrip__trip__in=trips).exclude(
                                status=APPROVED).update(is_stale=True)

                            obj.update_invoice_information()

                    if obj.driver:
                        if obj.status == StatusPipeline.ORDER_NEW:
                            obj.status = StatusPipeline.DRIVER_ACCEPTED

                        if old_order_obj and old_order_obj.pickup_datetime != obj.pickup_datetime:
                            # trip scheduled at should change based on
                            # order pick up time
                            Trip.objects.filter(
                                order_id=obj.id,
                                driver_id=old_order_obj.driver,
                                status__in=[
                                    StatusPipeline.TRIP_NEW, StatusPipeline.TRIP_IN_PROGRESS,
                                    StatusPipeline.DRIVER_ACCEPTED
                                ]).update(planned_start_time=obj.pickup_datetime)

                        if not old_order_obj.driver:
                            # new order, no driver associated with order
                            # create a new trip and assigned a driver
                            create_trip_when_driver_assigned(
                                obj, request)

                        elif old_order_obj.driver != obj.driver:
                            # order has driver, contingency case ,
                            # suspend existing trip and create new trip
                            trip = Trip.objects.filter(
                                order_id=obj.id, driver=old_order_obj.driver,
                                status__in=[StatusPipeline.TRIP_IN_PROGRESS,
                                            StatusPipeline.TRIP_NEW,
                                            StatusPipeline.DRIVER_ACCEPTED,
                                            StatusPipeline.TRIP_ALL_STOPS_DONE]).first()

                            Trip.objects.filter(
                                order=obj, driver=old_order_obj.driver, status__in=[StatusPipeline.TRIP_NEW,
                                                                                    StatusPipeline.TRIP_IN_PROGRESS]
                            ).update(status=StatusPipeline.TRIP_SUSPENDED,
                                     current_step=-1)

                            Stop.objects.filter(trip=trip, status__in=[StatusPipeline.TRIP_NEW,
                                                                       StatusPipeline.TRIP_IN_PROGRESS]
                                                ).update(status=StatusPipeline.TRIP_SUSPENDED, waypoint=None)

                            create_trip_when_driver_assigned(
                                obj, request, trip)

                            send_restore_notification_to_driver_app.apply_async((obj.id,), )
                            send_restore_notification_to_driver_app.apply_async((obj.id, old_order_obj.driver.id,), )
                obj.save()


class OrderPaymentHistoryAdmin(admin.ModelAdmin):
    list_display = ('get_order', 'get_customer', 'old_payment_status',
                    'new_payment_status', 'modified_date', 'modified_by',
                    'get_reason_for_marking_paid')

    search_fields = ('order__number', 'order__customer__name',
                     'order__user__email',)

    list_select_related = ('order', 'order__customer', 'order__user',
                           'modified_by')

    actions = None
    list_display_links = None

    def get_customer(self, obj):
        return obj.order.customer

    get_customer.short_description = 'Customer'

    def get_order(self, obj=None):
        link = reverse("admin:order_bookingorder_change", args=[obj.order_id])
        return format_html('<a href="%s" target="_blank">%s</a>' % (link, obj.order))

    get_order.allow_tags = True
    get_order.short_description = _('Order')

    def get_reason_for_marking_paid(self, obj=None):
        return obj.order.reason_for_marking_paid

    get_reason_for_marking_paid.short_description = _('Remarks')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('order').filter(order__order_type=settings.C2C_ORDER_TYPE)
        return qs


class ContainerAdmin(admin.ModelAdmin):
    list_display = ['number', 'container_type', 'customer']


class OrderContainerAdmin(admin.ModelAdmin):
    list_display = ['order', 'container', 'trip']


class OrderContainerHistoryAdmin(admin.ModelAdmin):
    list_display = ['order', 'container_number', 'container_type', 'trip']


class OrderLineHistoryAdmin(admin.ModelAdmin):
    list_display = ['order', 'source', 'sku', 'quantity', 'orderline_batch',
                    'total_item_price', 'each_item_price']
    readonly_fields = list_display


class OrderLineHistoryBatchAdmin(admin.ModelAdmin):
    list_display = ['description', 'start_time', 'end_time', 'number_of_entries']
    list_filter = ('start_time', 'end_time',)
    readonly_fields = list_display + ['get_request']
    exclude = ('request',)

    def get_request(self, obj):
        logs = obj.request
        if isinstance(logs, str):
            logs = json.loads(logs)
            if isinstance(logs, str):
                logs = json.loads(logs)
        processed = ''
        for k, v in logs.items():
            processed += '%s: %s\n' % (k.replace('_', ' ').title(), v)
        return processed

    get_request.short_description = _('Log')


class OrderHistoryAdmin(admin.ModelAdmin):
    list_display = ['order', 'field', 'old_value', 'new_value']

    readonly_fields = (
        'order', 'field', 'old_value', 'new_value', 'user', 'modified_time')


class OrderApiDataAdmin(admin.ModelAdmin):
    list_display = ['reference_number', 'customer', 'response_status', 'reason', 'created_at']

    readonly_fields = (
        'reference_number', 'response_status', 'category',
        'reason', 'created_at', 'customer', 'request_body',
        'is_phone_invalid', 'is_email_invalid', 'is_unservicebale_pincode',
        'has_undefined_contract'
    )

    search_fields = ['reference_number']

    list_filter = (('customer',admin.RelatedOnlyFieldListFilter),
                   ('created_at', DateRangeFilter),
                   'category', 'response_status',
                   'is_phone_invalid', 'is_email_invalid',
                   'is_unservicebale_pincode', 'has_undefined_contract'
                )


class OrderMessageAdmin(admin.ModelAdmin):
    list_display = ['order', 'msg_event', 'time', 'trip']

    readonly_fields = ('order', 'msg_event', 'time', 'message', 'trip')

    list_filter = (('order__customer', admin.RelatedOnlyFieldListFilter),
                   ('time', DateRangeFilter))

    search_fields = ['trip__trip_number', 'order__number']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('order', 'trip')
        return qs

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class AdvanceAWBSequenceAdmin(admin.ModelAdmin):
    list_display = ['customer', 'awb_count', 'created_date']

    readonly_fields = (
        'customer', 'awb_count', 'seq_start', 'seq_end',
        'seq_used', 'created_date')

    list_filter = (('customer', admin.RelatedOnlyFieldListFilter),
                   ('created_date', DateRangeFilter))

    search_fields = ['customer__name']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer')
        return qs

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class ShippingLabelRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'customer', 'file', 'user']


from .subadmins.flashsale_admin import *
from .subadmins.analytics import *

admin.site.register(ShippingLabelRequest, ShippingLabelRequestAdmin)
admin.site.register(KioskOrder, KioskOrderAdmin)
admin.site.register(OrderConstants, OrderConstantAdmin)
admin.site.register(ShippingAddress, ShippingAddressAdmin)
admin.site.register(OrderAdjustmentCategory)
admin.site.register(ItemCategory, ItemCategoryAdmin)
admin.site.register(OrderPaymentHistory, OrderPaymentHistoryAdmin)
admin.site.register(OrderCancellationReason, OrderCancellationReasonAdmin)
admin.site.register(Container, ContainerAdmin)
admin.site.register(OrderContainer, OrderContainerAdmin)
admin.site.register(OrderContainerHistory, OrderContainerHistoryAdmin)
admin.site.register(OrderLineHistory, OrderLineHistoryAdmin)
admin.site.register(OrderLineHistoryBatch, OrderLineHistoryBatchAdmin)
admin.site.register(OrderHistory, OrderHistoryAdmin)
admin.site.register(OrderMessage, OrderMessageAdmin)
admin.site.register(OrderApiData, OrderApiDataAdmin)
admin.site.register(OrderAwbSequence, AdvanceAWBSequenceAdmin)
