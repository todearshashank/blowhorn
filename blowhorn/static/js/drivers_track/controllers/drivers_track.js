var s;
angular.module('driversTrackApp').controller('driversTrackController', function ($scope, $compile, $document, $window, $http, $filter, $timeout, driversTrackService) {
    s = $scope;
    var map;
    var markerCluster = null;
    var driver_dict = {};
    var markers_drivers = [];
    $scope.filenamePrefixDriverActivity = "Driver_activity";

    var directionsDisplay;
    var directionDisplayPath = [];
    var polylinePath = [];
    var pickUpMarker = [];
    var dropOffMarker =[];
    var waypointMarkers = [];
    var info_window_open = true;
    const MIN_DISTANCE_DELTA_METERS = 10;
    const DRIVER_STATUS_OCCUPIED = 'occupied';
    const BOOKING_TYPE_REGULAR = 'booking';
    const DRIVER_ACCEPTED = 'driver_accepted';

    $scope.params = null;
    $scope.map = null;
    $scope.drivers = null;
    $scope.drivers_array = [];
    $scope.driver = {};
    $scope.statuses = [];
    $scope.cities = [];
    $scope.vclasses = {};
    $scope.vehicle_classes = {};
    $scope.driver_names = [];
    $scope.driver_details = {};
    $scope.showVehicleClass = {};
    $scope.showVehicleClass1 = {};
    $scope.showStatus = {};
    $scope.vclass_count = {};
    $scope.vclass_obj = {};
    $scope.showCity = {};
    $scope.all_cities = null;
    $scope.toggleSidebar = false;
    $scope.showSidebar = false;

    $scope.showVehicleClasses = [];
    $scope.showCities = [];
    $scope.showCustomer = '';

    function setCity(state) {
        for (i = 0; i < $scope.cities.length; i++) {
            $scope.showCity[$scope.cities[i]] = state;
        }
    }

    function getDriverLocations() {
        driversTrackService.getDriverLocations().then(function (data) {
            console.log('Driver locations: ', data);
            $scope.drivers = data;
            $scope.driver_names = [];

            for (var driver_id in $scope.drivers) {
                var driver = $scope.drivers[driver_id];
                $scope.drivers_array.push(driver);
            }
            $scope.all_cities = true;
            getRealTimeUpdates();
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
    }

    function getParameters() {
        driversTrackService.fetchParams().then(function (data) {
            console.log(data);
            var vclasses = angular.copy(data.icons);
            $scope.vehicle_classes = angular.copy(data.icons);
            $scope.statuses = angular.copy(data.statuses);
            $scope.cities = angular.copy(data.cities);

            $scope.statuses.map(function(status_) {
                $scope.showStatus[status_] = true;
            });

            for (var vclass in vclasses) {
                //$scope.vehicle_classes.push(vclass);
                $scope.showVehicleClass1[vclass] = true;
                var vclass_icons = vclasses[vclass];
                for (var type in vclass_icons) {
                    var vclass_status = vclass + ' ' + type;
                    $scope.vclasses[vclass_status] = vclass_icons[type];
                    $scope.vclass_count[vclass_status] = 0;
                    $scope.showVehicleClass[vclass_status] = false;
                    $scope.vclass_obj[vclass_status] = false;
                }
            }
            setCity(true);
            getDriverLocations();
            $scope.params = true;
        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
    }

    // Ralated to driver activity are temporarily disabled
    /*$scope.drivers_list = [];
    driversTrackService.searchDrivers().then(function (data) {
        $scope.drivers_list = data;
    }).catch(function () {
        toastr.error("Driver search will not be available", "Failed");
    });*/

    $scope.getDataandShowMap = function () {
        map = initMap(document.getElementById("google-map"));
        $scope.map = map;
        markerCluster = new MarkerClusterer(map);
        var fit = document.getElementById('fit-zoom').cloneNode();
        var iw = document.getElementById('toggle-infowindow').cloneNode();
        var reload = document.getElementById('reload').cloneNode();
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push($compile(fit)($scope)[0]);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push($compile(iw)($scope)[0]);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push($compile(reload)($scope)[0]);
        getParameters();
    };
    $scope.getDataandShowMap();

    $scope.selectDriver = function (driver) {
        $("#gcards").hide();
        driver.marker.setVisible(true);
        driver.marker.setMap($scope.map);
        $scope.map.setCenter(driver.marker.getPosition());
        google.maps.event.trigger(driver.marker, 'click');
        $scope.driver_searchText = angular.copy(driver.profile.name);
        $('#driver-search').val($scope.driver_searchText);
        $scope.$applyAsync();
    };

    $scope.clearSearch = function () {
        $('#driver-search').val('');
        $scope.driver_searchText = "";
        $scope.$applyAsync();
        $scope.fitZoom();
    };

    $scope.selectCustomer = function (customer_id) {
        $scope.customer_searchText = customer_id;
        $scope.showCustomer = customer_id;
    };

    $scope.getUniqueCustomers = function (drivers_array) {
        //console.log(drivers_array);
        var all_customers = drivers_array.map(function (driver) {
            if (!driver.customer_id) {
                return;
            }
            return driver.customer_id;
        });
        //console.log(all_customers);

        var valid_customers = all_customers.filter(function (customer) {
            return (customer != null);
        });
        //console.log(valid_customers);

        var unique_customers = valid_customers.filter(function(customer, i) {
            return (valid_customers.indexOf(customer) == i);
        });
        //console.log(unique_customers);
        return unique_customers;
    }

    // Watch all_cities variable and set the value for
    // individual cities to this value
    $scope.$watch('all_cities', function (value) {
        //console.log('All cities', value);
        for (var city in $scope.showCity) {
            $scope.showCity[city] = value;
        }
        if (value == true) {
            $scope.fitZoom();
        }
    });

    // Whenever a city view is toggled, do a fitZoom on the map
    $scope.$watch('showCity', function (n) {
        setTimeout(function() {
            $scope.fitZoom();
        }, 100);
    }, true);

    /*
    $scope.$watch('showCity', function (n, o) {
        setTimeout(function() {
            console.log('All cities', n);
            var do_fitzoom = false;
            for (var city in n) {
                ov = o[city];
                nv = n[city];

                console.log(city, ov, nv);

                if (ov != nv) {
                    count = $scope.getVisibleCityCount(city);
                    console.log(count);
                    do_fitzoom = (count > 0);
                }
            }
            if (do_fitzoom == true) {
                $scope.fitZoom();
            }
        }, 100)
    }, true);
    */

    $scope.$watchGroup(['map', 'drivers'], function (n, o) {
        if (n[0] && n[1] != null && o[1] == null) {
            $scope.fitZoom();
        }
    });

    function clearMarkerInfoWindow() {
        $scope.drivers_array.map(function (driver) {
            driver.marker.infobox.close();
        });
    }

    $scope.clearDirectionsDisplay = function () {
        function setNull(list) {
            list.map(function(item) {item.setMap(null)});
        }
        setNull(directionDisplayPath);
        setNull(pickUpMarker);
        setNull(dropOffMarker);
        setNull(waypointMarkers);
        setNull(polylinePath);
    };

    $scope.showBookingDetails = function (driver) {
        clearMarkerInfoWindow();
        $scope.clearDirectionsDisplay();
        var marker = driver.marker;

        //Get booking info by booking Id
        if (driver.status === DRIVER_STATUS_OCCUPIED) {
            var booking_data = {
                "booking_id": driver.booking_id,
                "booking_key": driver.booking_key
            };
            map.setCenter(marker.getPosition());
            driversTrackService.fetchBooking(driver.driver_id).then(function (data) {
                var result = angular.copy(data);
                bookingInfo = result["message"][0];
                console.log(bookingInfo);

                //Refresh driver current status
                driver.location.driver_current_status = bookingInfo && bookingInfo["current_status"];
                showBookingDialog(driver, bookingInfo);

                //Clear Bindings
                if (directionsDisplay) {
                    directionsDisplay.setMap(null);
                    directionsDisplayPost.setMap(null);
                    srcMarker.setMap(null);
                    destMarker.setMap(null);
                    waypointMarkers.map(function (marker) {
                        marker.setMap(null)}
                    );
                    waypointMarkers = [];
                }

                if (driver.booking_type === BOOKING_TYPE_REGULAR) {
                    var srcLatLng = new google.maps.LatLng(bookingInfo["pickup_geopt"].lat, bookingInfo["pickup_geopt"].lon);
                    var desLatLng = new google.maps.LatLng(bookingInfo["dropoff_geopt"].lat, bookingInfo["dropoff_geopt"].lon);
                    var waypoints = bookingInfo["waypoints"].map(function (point) {
                        return new google.maps.LatLng(point.lat, point.lon);
                    });
                    var current_waypoint = bookingInfo["current_waypoint"];

                    //Add direction path with status
                    drawCurrentToDestination(map, driver, srcLatLng, desLatLng, waypoints, current_waypoint);

                    //Add source and destination markers
                    var srcMarkerOptions = {
                        position: srcLatLng,
                        draggable: false
                    };
                    srcMarker = initMarker(map, srcMarkerOptions);
                    pickUpMarker.push(srcMarker);
                    var destMarkerOptions = {
                        position: desLatLng,
                        draggable: false
                    };
                    destMarker = initMarker(map, destMarkerOptions, 'dropoff');
                    dropOffMarker.push(destMarker);

                    waypointMarkers = waypoints.map(function (point) {
                        var waypointMarkerOptions = {
                            position: point
                        };
                        return marker = initMarker(map, waypointMarkerOptions, 'waypoint');
                    });
                } else {
                    var desLatLng = new google.maps.LatLng(bookingInfo["dropoff_geopt"].lat, bookingInfo["dropoff_geopt"].lon);
                    drawCurrentToDestination(map, driver, null, desLatLng, [], 0);
                    var destMarkerOptions = {
                        position: desLatLng,
                        draggable: false
                    };
                    destMarker = initMarker(map, destMarkerOptions, 'dropoff');
                    dropOffMarker.push(destMarker);
                }
            }).catch(function (data) {
            console.log('Booking details not found for driver', 'Failed');
            });
        } else {
            driver.marker.infobox.open(map, marker);
        }
        currentMark = this;
    };

    function drawCurrentToDestination(map, driver, srcLatLng, desLatLng, waypoints, current_waypoint) {
        var directionsService = initDirectionsService();
        var directionsDisplay = initDirectionsRenderer();
        var directionOptions = {
            polylineOptions: {
                geodesic: true,
                strokeColor: '#58B79D',
                strokeOpacity: 1.0,
                strokeWeight: 4
            }
        };
        var directionsDisplayPost = initDirectionsRenderer(directionOptions);
        directionDisplayPath.push(directionsDisplay);
        directionDisplayPath.push(directionsDisplayPost);
        var request;
        var polyline = null;
        var polyline_coordinates = [];
        var myLatlng = new google.maps.LatLng(driver.location.lat, driver.location.lon);
        var wps = [];
        var etaDisplaySection = $('#etas');
        etaDisplaySection.children().remove();
        var etaDisplayRow = $('#eta-row');

        function showEtaText(legs, names) {
            legs.map(function (leg, i) {
                var nrow = etaDisplayRow.clone();
                nrow.find('.target').text(names[i]);
                nrow.find('.eta-distance').text(leg.distance.text);
                nrow.find('.eta-time').text(leg.duration.text);
                nrow.appendTo(etaDisplaySection);
            });
        }
        //Check status
        if (driver.location.driver_current_status === DRIVER_ACCEPTED &&
            driver.booking_type === BOOKING_TYPE_REGULAR) {
            //driver location -> pickup -> waypoints -> dropoff
            //Show directions from driver location to Pickup
            request = {
                origin: myLatlng,
                destination: srcLatLng,
                waypoints: [],
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            var status_names_route1 = ['Pickup'];
            directionsDisplay.setMap(map);
            directionsService.route(request, function (response, status) {
                console.log(response);
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    showEtaText(response.routes[0].legs, status_names_route1);
                }
            });

            //Show directions from Pickup -> waypoints -> dropoff
            var status_names_route2 = [];
            wps = waypoints.map(function (point, i) {
                status_names_route2.push('Waypoint-' + (i+1));
                return {location: point};
            });
            status_names_route2.push('Dropoff');
            request = {
                origin: srcLatLng,
                destination: desLatLng,
                waypoints: wps,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsDisplayPost.setMap(map);
            directionsService.route(request, function (response, status) {
                console.log(response);
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplayPost.setDirections(response);
                    showEtaText(response.routes[0].legs, status_names_route2);
                }
            });
        } else {
            //pickup -> driver location -> waypoints -> dropoff
            polyline = new google.maps.Polyline({
                path: polyline_coordinates,
            });
            polyline.setMap(map);
            polylinePath.push(polyline);

            /*driversTrackService.getPriorRoute(driver.booking_id).then(function (data) {
                var path = polyline.getPath();
                var prior_path = data.route.reverse().map(function (point) {
                    var latlng = new google.maps.LatLng(point.lat, point.lon);
                    path.insertAt(0, latlng);
                    return latlng;
                });
            }).catch(function (data) {
                toastr.error(data.responseText, "Failed");
            });*/

            var wps = [];
            request = {
                origin: myLatlng,
                destination: desLatLng,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };

            //Show directions from driver -> waypoints -> dropoff
            var status_names_route3 = [];
            if (current_waypoint < waypoints.length) {
                var offset = 0;
                if (driver.location.driver_current_status === "waypoint") {
                    offset = current_waypoint;
                }
                var n_waypoints = waypoints.slice(current_waypoint);
                wps = n_waypoints.map(function (point, i) {
                    status_names_route3.push("Waypoint-" + (i + 1 + offset));
                    return {location: point};
                });
            }
            request = {
                origin: myLatlng,
                destination: desLatLng,
                waypoints: wps,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            status_names_route3.push('Dropoff');

            directionsDisplayPost.setMap(map);
            directionsService.route(request, function (response, status) {
                console.log(response);
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplayPost.setDirections(response);
                    showEtaText(response.routes[0].legs, status_names_route3);
                }
            });
        }
    }

    function driverActions(driver_id, action, timealarmdata) {
        var data = null;
        if (driver_id === '') {
            data = timealarmdata;
        } else {
            data = {
                'driver_id': driver_id,
                'action': action
            };
        }
        driversTrackService.driverActions(data).then(function (data) {
            toastr.success("Action sent to driver app");
        }).catch(function (data) {
            console.log(data);
            toastr.error(data, "Failed");
        });
    }

    $scope.showDriverActions = function (driver) {
        console.log(driver);
        // Remove this during unified app release
        if (driver.booking_type === "shipment") {
            console.log('Actions are not supported by Stage Transporter');
            return;
        }
        //var driver = $scope.drivers[driver_id];
        var driver_info = angular.copy(driver.profile);
        var driver_id = driver.driver_id;
        $scope.driver.driver_id = driver.driver_id;
        $scope.driver.driver_name = driver_info.name;
        $scope.driver.regno = driver_info.registration_number;
        $scope.driver.licno = driver_info.licence_number;
        $scope.driver.vehicle_model = driver_info.vehicle_model;
        $scope.driver.app_ver = driver_info.device_details.app_version_name;
        $scope.driver.device = driver_info.device_details.device_name;
        $scope.driver.os = driver_info.device_details.os_version;
        $scope.driver.network = driver_info.device_details.sim_network;
        $scope.driver.sim = driver_info.device_details.sim_sr_number;

        $scope.logout = function () {
            driverActions(driver_id, 'logout');
        };

        $scope.clear = function () {
            driverActions(driver_id, 'clear');
        };

        $scope.request_location = function () {
            driverActions(driver_id, 'request_location');
        };

        $scope.restore = function () {
            driverActions(driver_id, 'restore');
        };

        $scope.remove_idle = function () {
            driverActions(driver_id, 'remove_idle');
        };

        $scope.duty_on = function () {
            driverActions(driver_id, 'duty_on');
        };

        $scope.duty_off = function () {
            driverActions(driver_id, 'duty_off');
        };

        $scope.log_driver = function () {
            driversTrackService.logDriver(driver_id, driver_info.mapped_to);
        };

        $scope.updateLocationTime = function () {
            $('#update_location_time_form')[0].reset();
            var location_time_form = document.forms['update_location_time_form'];
            location_time_form.elements["driver_id"].value = driver_id;
            location_time_form.elements["driver_name"].value = driver_info.name;

            $("#update_location_time_form").off();
            $("#update_location_time_form").on('submit', function () {
                data = $('#update_location_time_form').serializeArray();
                driverActions('', '', data);
            });
            $('#update_location_time_modal').modal({ backdrop: 'static', keyboard: true });
            $('#update_location_time_modal').modal('show');
        };
        $('#gcardsright').css("display", "table-cell");
    };

    /*
    window.setInterval(function () {
        for (var driver_id in $scope.drivers) {
            driver = $scope.drivers[driver_id]
            setInfoWindowContent(driver);
        }
    }, 60000);
    */


    $scope.getDriverFromSidebar = function (id) {
        var driver = $scope.drivers[id];
        map.setCenter(driver.marker.getPosition());
        new google.maps.event.trigger(driver.marker, 'click');
    };

    function addDriversToSidebar(driver) {
        var driver_name_mobile = driver.profile.name + " - " + driver.profile.mobile;
        var found = $scope.driver_names.indexOf(driver_name_mobile);
        if (found === -1) {
            $scope.driver_names.push(driver_name_mobile);
        } else {
            $scope.driver_names.splice(found, 1);
        }
        $scope.driver_details[driver.driver_id] = {'name': driver.profile.name, 'status': driver.status};
    }

    $scope.fitZoom = function () {
        var marker;
        var newBounds = new google.maps.LatLngBounds();
        $scope.drivers_array.forEach(function (driver) {
            console.log('Zooming driver', driver)

            marker = driver.marker;
            console.log(driver.marker);
            if (marker) {
                if (marker.getMap() && marker.getVisible()) {
                    newBounds.extend(marker.getPosition());
                }
            }
        });
        $scope.map.fitBounds(newBounds);
    };

    $scope.toggleInfowindow = function () {
        for (var i = 0, marker; marker = markers_drivers[i]; i++) {
            map_ = marker.getMap();
            if (map_ !== null) {
                if (info_window_open) {
                    marker.infowindow.close();
                } else {
                    marker.infowindow.open(map, marker);
                }
            }
        }
        info_window_open = !info_window_open;
    };

    $scope.openDriverListSidebar = function () {
        $scope.toggleSidebar = !$scope.toggleSidebar;
        $scope.showSidebar = $scope.toggleSidebar;
    };

    $scope.processNewLocation = function (data) {
        console.log(data);
        if (!data.driver_id) {
            console.log('No driver_id: ', data);
            return;
        }

        if (data.hasOwnProperty('mLatitude') && data.hasOwnProperty('mLongitude')) {
            data['location'] = {
                'lat': data['mLatitude'],
                'lon': data['mLongitude'],
                'accuracy_meters': data['mAccuracy'],
                'speed_kmph': data['mSpeed'],
                'status': data['event'],
                'timestamp': data['mTime'],
                'gpstimestamp': data['mTime']
            };
            data['status'] = 'occupied';
        }
        var driver_id = data.driver_id;
        var status = data.status;
        var region = data.region;
        var driver = {};
        var newer_location = true;
        var new_driver = false;
        if ($scope.drivers.hasOwnProperty(driver_id)) {
            // Driver Info already present
            //console.log(driver_id, 'Driver Profile already present');
            driver = $scope.drivers[driver_id];
            driver.rdata = data;
            $scope.$apply();
        } else {
            // Driver Info not present. This is due to driver info directly coming
            // from firebase because he became online only now
            new_driver = true;
            //console.log(driver_id, 'Driver Profile not present. Fetching!!');

            // TODO: Disabling the driver profile api call since old system drivers
            // are getting synchronized in enterprise and causing deluge of profile calls
            /*
            driversTrackService.getDriverProfile(driver_id).then(function (profile_data) {
                console.log(profile_data);
                driver.driver_id = driver_id;
                driver.status = data.status;
                driver.region = data.region;
                driver.location = data.location;
                driver.profile = profile_data.message;
                $scope.drivers[driver_id] = driver;
                $scope.drivers_array.push(driver);
            }).catch(function (data) {
                console.log('Unknown Driver', driver_id);
            });
            */
        }
    };

    function getRealTimeUpdates() {
        var count = -1;
        driversTrackService.getRealTimeUpdates().then(function (data) {
            var firebase_auth_token = data.firebase_auth_token;
            var isSuccessful = true;
            console.log('Getting real-time updates');
            firebase.auth().signInWithCustomToken(firebase_auth_token).then(function (data) {
                if (isSuccessful) {
                    /*firebase.database().ref("admin-bookings").on('child_changed', function (snapshot) {
                        var current_booking = JSON.parse(snapshot.val());
                        showIncomingBookingFlash(current_booking);
                    }, function (err) {
                        console.log(err)
                    });
                    firebase.database().ref("view-user-profile").on('child_changed', function (snapshot) {
                        var result = JSON.parse(snapshot.val());
                        showCustomerViewFlash(result);
                    }, function (err) {
                        console.log(err)
                    });*/
                    toastr.success('Registering for realtime updates');
                    firebase.database().ref("active_drivers").on('child_changed', function (snapshot) {
                        var message = JSON.parse(snapshot.val());
                        console.log(message);
                        $scope.processNewLocation(message);
                    }, function (err) {
                        console.log(err)
                    });
                    firebase.database().ref("active_drivers").on('child_added', function (snapshot) {
                        var message = JSON.parse(snapshot.val());
                        $scope.processNewLocation(message);
                    }, function (err) {
                        console.log(err)
                    });
                }
            }).catch(function (error) {
                var errorMessage = error.message;
                isSuccessful = false;
                console.log(errorMessage);
            });

        }).catch(function (data) {
            toastr.error(data.responseText, "Failed");
        });
    }

    function showIncomingBookingFlash(result) {
        toastr.success(
            '<b>Booking ID</b> : ' + result.friendly_id +
            '<br><b>Pickup Address</b> : ' + result.pickup_address +
            '<br><b>Dropoff Address</b> : ' + result.dropoff_address, "Incoming Booking");
    }

    function showCustomerViewFlash(result) {
        toastr.success(
            '<b>Name</b> : ' + result.name +
            '<br><b>Email</b> : ' + result.email, "Profile Viewed by");
        // var lat_lng = new google.maps.LatLng(result.lat, result.lon);
        // var markerOptions = {
        //     position: lat_lng,
        //     icon: '/images/eye.png'
        // };
        // var marker = initMarker(map, markerOptions);
        // setTimeout(function() {marker.setMap(null);}, 5000);
    }

    function showBookingDialog(driver, bookingDetail) {
        //Driver details
        $scope.driver_last_update = driver.timestamp || '--';
        $scope.driver_mobile = driver.profile.mobile || '--';
        $scope.driver_battery_level = driver.location.battery_percent || '--';
        $scope.driver_current_status = driver.location.driver_current_status || '--';
        var distance_line = "";
        if (driver.distance_km) {
            try {
                distance_line = ('Since ' + driver.location.start_time_disp + ' : ' + parseFloat(driver.distance_km).toFixed(1) + ' km');
            } catch(err) {
            }
            $scope.driver_distance_km = distance_line;
        }

        //Booking details
        $("#driver-img").attr("src", "/static/website/images/dummy_profile.png");
        $scope.booking_start_time = bookingDetail.pickup_time_or_rpt || '--';
        $scope.pickup_date = bookingDetail.pickup_date_or_rpt || '--';
        $scope.booking_end_time = bookingDetail.dropoff_time || '--';
        $scope.pickup_address = bookingDetail.pickup_address || '--';
        $scope.dropoff_address = bookingDetail.dropoff_address || '--';
        $scope.booking_id = bookingDetail.friendly_id || '--';
        $scope.booking_date = bookingDetail.booking_date || '--';
        $scope.booking_time = bookingDetail.booking_time || '--';
        $scope.labour_request = bookingDetail.labour_requested || '--';
        $scope.assigned_driver = bookingDetail.driver_name || driver.name;
        if (bookingDetail.items_moved) {
            var itemsList = '';
            $.each(bookingDetail["items_moved"], function (i, items) {
                itemsList += items + (i !== (bookingDetail["items_moved"].length - 1) ? ', ' : '');
            });
            $scope.items_moved = itemsList;
        }
        if (bookingDetail.driver_picture) {
            $("#driver-img").attr("src", bookingDetail.driver_picture);
        }
        $('#gcards').css("display", "table-cell");
    }

    $scope.closeDialog = function () {
        $scope.clearDirectionsDisplay();
        $("#gcards").hide();
        setMapResize();
    };

    $scope.closeDialogRight = function () {
        $("#gcardsright").hide();
        setMapResize();
    };

    function setMapResize() {
        $timeout(function () {
            google.maps.event.trigger(map, 'resize');
        }, 200);
    }

    google.maps.event.addListener(map, 'drag', setMapResize);
    google.maps.event.addListener(map, 'zoom_changed', setMapResize);

    $scope.getVisibleCount = function (vclass) {
        return $scope.getAvailable(vclass).filter(function (driver) {
            return (driver.marker && driver.marker.getVisible() && driver.marker.getMap());
        }).length;
    };

    $scope.getAvailable = function (vclass) {
        //console.log(vclass);
        return $scope.drivers_array.filter(function (driver) {
            return (driver.profile.vehicle_class === vclass);
        });
    };

    $scope.getVisibleStatusCount = function (status_) {
        return $scope.drivers_array.filter(function (driver) {
            return (driver.marker &&
                driver.status === status_ &&
                driver.marker.getVisible() &&
                driver.marker.getMap()
            );
        }).length;
    };

    $scope.getVisibleCityCount = function (city) {
        return $scope.drivers_array.filter(function (driver) {
            return (driver.marker &&
                driver.region === city &&
                driver.marker.getVisible() &&
                driver.marker.getMap()
            );
        }).length;
    };

    $scope.visibleDriversFilter = function (driver, i, _) {
        //console.log(driver.marker, i, _);
        if (!driver.marker) {
            return true;
        }
        return (driver.marker.getVisible() && driver.marker.getMap());
    };

    function getDriverActivity() {
        // Fetch the driver activity
        driversTrackService.getDriverActivity($scope.filterParams).then(function (data) {
            $scope.filterParams.cursor = data.cursor;
            $scope.driverActivities = $scope.driverActivities.concat(data.results);
            $scope.displayedActivities = $scope.displayedActivities.concat(data.results);
            if (data.more) {
                getDriverActivity();
            } else {
                // enable fetch button
                $scope.filter.loading = false;
            }
            $scope.$applyAsync();
        }).catch(function (data) {
            $scope.filter.loading = false;
            toastr.error(data.responseText, 'Failed');
        });
    }

    $scope.filter = {
        startDate: '',
        endDate: '',
        cursor: ''
    };
    $scope.displayedActivities = [];
    $scope.driverActivities = [];
    $scope.fetchActivity = function () {
        console.log($scope.filter)
        if (!$scope.filter.startDate || !$scope.filter.endDate) {
            toastr.warning('Select both; start and end dates');
            return;
        }
        $scope.displayedActivities = [];
        $scope.driverActivities = [];
        $scope.filterParams = {
            'start_date': $scope.filter.startDate,
            'end_date': $scope.filter.endDate,
            'driver_id': $scope.filter.driver_id,
            'cursor': ''
        };
        $scope.filter.loading = true;
        getDriverActivity();
    };
});
