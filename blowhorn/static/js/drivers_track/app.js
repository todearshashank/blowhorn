angular.module('driversTrackApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate']);

angular.module('driversTrackApp').config(function ($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: '/static/html/drivers_track/drivers_track.html',
        controller: 'driversTrackController'
    }).
    otherwise({
        redirectTo: '/'
    });
});
