angular.module("driversTrackApp").directive("updateTimeIntervalAlarm", function () {
    return {
        restrict:"E",
        templateUrl:"/static/html/drivers_track/update_time_interval_alarm.html"
    };
});

// Temporarily disabled
/*angular.module("driversTrackApp").directive("driverActivity", function () {
    return {
        restrict:"E",
        templateUrl:"/static/html/drivers_track/drivers_activity.html"
    };
});
*/
angular.module("driversTrackApp").directive("pickdate", [function () {
    return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                $(element).datepicker({
                    format: "dd-mm-yyyy",
                    autoclose: true,
                    orientation: "top",
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                    }
                });
            }
        }
}]);

angular.module("driversTrackApp").directive('customSelect2', function () {
    return {
        restrict: 'EA',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.select2({
                placeholder: "Select Driver",
                allowClear: true,
                minimumResultsForSearch: 10
            });
            /*element.change(function () {
                ngModelCtrl.$setViewValue(element.val());
            });*/
        }
    }
});


// Min distance to consider a location upate as relevant
// below this it is mostly noise
const MIN_DISTANCE_DELTA_METERS = 10;

// maximum time in mins until which location update is needed
// If location is stale for more than this it is indicated in Red
const MAX_UPDATE_LAG_MINS = 2;

function Distance(loc1, loc2) {
    return google.maps.geometry.spherical.computeDistanceBetween(loc1, loc2);
}

function isNewLocationNewer(old_location, new_location) {
    var old_ = new Date(old_location.gpstimestamp);
    var new_ = new Date(new_location.gpstimestamp);
    console.log('New location', new_ > old_);
    return (new_ > old_);
}

function updateDriverWithNewData(new_driver, newer_location, driver, data) {
    var distance_meters = 0;
    var new_latlng = new google.maps.LatLng(data.location.lat, data.location.lon);
    var old_latlng = new google.maps.LatLng(driver.location.lat, driver.location.lon);
    if (new_driver) {
        driver.latlng = latlng;
        driver.distance_km = 0;
    }

    //console.log(driver);
    if (newer_location) {
        if (!driver.marker) {
            return;
        }
        distance_meters = Distance(new_latlng, driver.marker.getPosition());
        driver.booking_id = data.booking_id;
        driver.booking_type = data.booking_type;
        driver.booking_key = data.booking_key;
        driver.status = data.status;
        driver.timestamp = data.timestamp;
        //console.log('Updating Location. New vs Old', data.location, driver.location);
        // If the distance between previous location is < 10m we might as well ignore it
        // as it can be GPS noise
        if (distance_meters > MIN_DISTANCE_DELTA_METERS || new_driver) {
            driver.distance_km += distance_meters / 1000;
            driver.location = data.location;
            driver.marker.setPosition(new_latlng);
            driver.circle.setCenter(new_latlng);
            driver.circle.setRadius(data.location.accuracy_meters);
        }
    } else {
        //console.log('Stale Location. New vs Old', data, driver);
    }
}

angular.module("driversTrackApp").directive("marker", ["driversTrackService", function (service) {
    return {
        restrict:"E",
        scope : {
            driver : "=driver",
            rdata : "=rdata",
            map : "=map",
            showDriverActions : "=showDriverActions",
            vclasses : "=vclasses",
            showCustomer : "=showCustomer",
            showCity : "=showCity",
            showVehicleClass : "=showVehicleClass",
            showStatus : "=showStatus",
            showBookingDetails : "=showBookingDetails",
            clearDirectionsDisplay : "=clearDirectionsDisplay",
        },
        link: function (scope, element, attrs, model) {
            var marker = null;
            var infobox = null;
            //var infowindow = null;
            marker = service.createMarker(scope.map, scope.driver, scope.vclasses);
            // console.log('Creating marker for driver', scope.driver, marker);
            scope.driver.marker = marker;
            var infowindowOptions = {
                pixelOffset: new google.maps.Size(0, 6),
                disableAutoPan: true,
                maxWidth: 200,
            };
            infobox = service.createInfobox(scope, scope.map, marker);
            scope.driver.circle = service.getCircle(scope.map,
                scope.driver.location.accuracy_meters, scope.driver.marker.position);

            scope.driver.last_update = scope.driver.timestamp;

            marker.addListener('click', function () {
                var timestamp = new Date(scope.driver.timestamp);
                var now = new Date();
                var time_delta_mins = (now - timestamp) / (1000 * 60);
                scope.driver.showRed = (time_delta_mins > MAX_UPDATE_LAG_MINS);
                this.infobox.open(scope.map, marker);
                scope.$applyAsync();
            });

            marker.addListener('dblclick', function () {
                scope.showBookingDetails(scope.driver);
            });

            marker.addListener('rightclick', function () {
                scope.clearDirectionsDisplay();
            });

            marker.addListener('closeclick', function () {
                //TODO: Check for driver status
                if (directionsDisplay) {
                    directionsDisplay.setMap(null);
                    srcMarker.setMap(null);
                    destMarker.setMap(null);
                }
            });

            scope.$watch('driver.status', function (new_status) {
                marker.setIcon(service.getIcon(scope.driver.profile.vehicle_class, new_status, scope.vclasses));
            });

            scope.$watch('driver.rdata', function (data) {
                if (data == null) { return; }
                var newer_location = isNewLocationNewer(scope.driver.location, data.location);
                updateDriverWithNewData(false, newer_location, scope.driver, data);
            }, true);

            scope.$watchGroup(['showCity', 'showVehicleClass', 'showStatus', 'showCustomer'], function (data, _) {
                if (data[0] && data[1] && data[2] && (data[3] === '' || data[3] == null || data[3] === scope.driver.customer_id)) {
                    marker.setMap(scope.map);
                    scope.driver.circle.setMap(scope.map);
                } else {
                    marker.setMap(null);
                    scope.driver.circle.setMap(null);
                }
            });
        }
    };
}]);

angular.module("driversTrackApp").directive('infobox', function() {
    return {
        restrict: 'E',
        scope: {
            driver: '=driver',
            showDriverActions: '=showDriverActions',
        },
        link : function (scope) {
        },
        templateUrl: '/static/html/drivers_track/infobox.html'
    };
});
