angular.module('driversTrackApp').factory('driversTrackService', function ($q, $http, $rootScope, $compile, $window) {

    var factory = {};
    function getCookieValue(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        var csrf = $("input[name='csrfmiddlewaretoken']").val();
        return cookieValue || csrf;
    }


    factory.fetchParams = function (data) {
        var defer = $q.defer();
        $http({
            url: 'api/livetrackingparams',
            method: 'GET',
            dataType: 'json',
        }).success(function (data, textStatus, xhr) {
            defer.resolve(data);
        }).error(function (data, textStatus, xhr) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.getDriverLocations = function (data) {
        var defer = $q.defer();
        $http({
            url: 'api/driverlocations',
            method: 'GET',
            dataType: 'json',
        }).success(function (data, textStatus, xhr) {
            defer.resolve(data);
        }).error(function (data, textStatus, xhr) {
            defer.reject(data);
        });
        return defer.promise;
    };

    // @TODO: Need to implement this API
    factory.searchDrivers = function (keyword) {
        var defer = $q.defer();
        $.ajax({
            url: "/admin/drivers/s/search",
            type: "GET",
            data: {keyword : keyword},
            dataType: "json",
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.driverActions = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: 'api/action',
            type: "POST",
            data: data,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });

        return defer.promise;
    };

    factory.fetchBooking = function (driver_id) {
        var defer = $q.defer();
        $.ajax({
            url: 'api/driverbookingdetails/' + driver_id,
            method: 'POST',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                defer.resolve(data);
            },
            error: function (jqXHR, textStatus, exception) {
                defer.reject(jqXHR);
            }
        });
        return defer.promise;
    };

    factory.getPriorRoute = function (data) {
        var defer = $q.defer();
        $http({
            url: 'api/priorroute?booking_id=' + data,
            method: 'GET',
            dataType: 'json',
        }).success(function (data, textStatus, xhr) {
            defer.resolve(data);
        }).error(function (data, textStatus, xhr) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.getDriverProfile = function (data) {
        var defer = $q.defer();
        $http({
            url: 'api/profile/' + data,
            method: 'GET',
            dataType: 'json',
        }).success(function (data, textStatus, xhr) {
            defer.resolve(data);
        }).error(function (data, textStatus, xhr) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.getRealTimeUpdates = function (data) {
        var defer = $q.defer();
        $http({
            url: 'api/firebasetoken/' + ADMIN_DRIVER_TRACKING,
            method: 'GET',
            dataType: 'json',
        }).success(function (data, textStatus, xhr) {
            defer.resolve(data);
        }).error(function (data, textStatus, xhr) {
            defer.reject(data);
        });
        return defer.promise;
    };

    factory.getDriverActivity = function (data) {
        var defer = $q.defer();
        $.ajax({
            url: '/admin/drivers/s/activity/report',
            type: 'GET',
            dataType: 'json',
            data: data,
            success: function (data, textStatus, xhr) {
                defer.resolve(data);
            },
            error: function (data, textStatus, xhr) {
                defer.reject(data);
            }
        });
        return defer.promise;
    };

    factory.logDriver = function (id, mapped_to) {
        $window.open('api/drivers/log/' + id + '/' + mapped_to + '/today');
    };

    factory.getIcon = function (vehicle_class, status_, vclasses) {
        var vclass_status = vehicle_class + ' ' + status_;
        return vclasses[vclass_status] || vclasses['idle'];
    };

    factory.createMarker = function (map, driver, vclasses) {
        var profile = driver.profile;
        var icon = factory.getIcon(profile.vehicle_class, driver.status, vclasses);
        var position = new google.maps.LatLng(driver.location.lat, driver.location.lon);
        var markerOptions = {
            draggable: false,
            position : position,
        };
        var marker = initMarker(map, markerOptions);
        driver.marker = marker;
        marker.setIcon(icon);
        return marker;
    };

    factory.getCircle = function(map, accuracy, center) {
        if (accuracy == null) {
            console.log('Accuracy is null. App needs to be updated');
            accuracy = 0;
        }
        var circleOptions = {
            radius: accuracy,
            center: center
        };
        return initCircle(map, circleOptions);
    };

    factory.setInfoWindowContent = function (driver, $scope) {
        console.log(driver);
        infowindow = driver.infowindow;
        var timestamp = new Date(driver.timestamp);
        var now = new Date();
        var time_delta_mins = (now - timestamp)/(1000*60);
        var showRed = 0;
        if (time_delta_mins > 4) {
            showRed = 1;
        }
        var gps_lag_string = driver.location.gps_lag && ('Gps lag: <a style="color: red">' + driver.location.gps_lag.trim() +'</a><br>');
        var speed_kmph = Math.round(driver.location.speed_kmph * 100) / 100;
        var speed_line = speed_kmph != null? (speed_kmph +' kmph') : '';
        var template = `
        <div style="width: 200px">
        <table class="table table-bordered" style="margin-bottom: 2px"><tr>
        <td><img height="20px" src="%(img_src)s"> %(battery_percent)s%%</td>
        <td>%(speed)s</td>
        <td>%(distance_km)s km</td>
        </tr>
        <tr><td colspan="3">Name:  %(name)s</td></tr>
        <tr><td colspan="3">Mobile:  <u><a ng-click=showDriverActions('%(driverid)s') class="point">%(mobile)s</a></u></td></tr>
        <tr><td colspan="3">Last Update: <span style="color:%(color)s"> %(last_update)s </span></td><tr>
        %(gps_lag_string)s
        <table>
        </div>
        `;
        var color = (showRed === 1) ? 'red': 'black';
        //var img_src = getImgSrcForBatteryPercent(driver.battery_percent);
        var img_src = "/static/website/images/battery.png";
        var values = {
            name            : driver.profile.name,
            mobile          : driver.profile.mobile,
            color           : color,
            last_update     : timestamp.toLocaleTimeString(),
            gps_lag_string  : gps_lag_string,
            distance_km     : driver.distance_km.toFixed(1),
            //distance_line   : distance_line,
            speed           : speed_line,
            speed_kmph      : speed_kmph,
            battery_percent : driver.location.battery_percent,
            img_src         : img_src,
            driverid        : driver.driver_id,
        }
        var marker_text = sprintf(template, values);
        var compiled_content = $compile(marker_text)($scope);
        infowindow.setContent(compiled_content[0]);
        google.maps.event.addListener(infowindow, 'domready', function() {
            // Reference to the DIV that wraps the bottom of infowindow
            var iwOuter = $('.gm-style-iw');

            /* Since this div is in a position prior to .gm-div style-iw.
             * We use jQuery and create a iwBackground variable,
             * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
             */
            var iwBackground = iwOuter.prev();

            // Removes background shadow DIV
            iwBackground.children(':nth-child(2)').css({'display' : 'none'});

            // Removes white background DIV
            iwBackground.children(':nth-child(4)').css({'display' : 'none'});

            // Moves the infowindow 115px to the right.
            iwOuter.parent().parent().css({left: '40px'});

            // Moves the shadow of the arrow 76px to the left margin.
            iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

            // Moves the arrow 76px to the left margin.
            iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

            // Changes the desired tail shadow color.
            iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

            // Reference to the div that groups the close button elements.
            var iwCloseBtn = iwOuter.next();

            // Apply the desired effect to the close button
            //iwCloseBtn.css({opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});
            iwCloseBtn.css({opacity: '1', right: '55px', top: '22px'});

            // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
            if($('.iw-content').height() < 140) {
                $('.iw-bottom-gradient').css({display: 'none'});
            }

            // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
            iwCloseBtn.mouseout(function () {
                $(this).css({opacity: '1'});
            });
        });
    }

    factory.getInfoboxContent = function (scope) {
        var content = '<infobox driver="driver" show-driver-actions="showDriverActions"></infobox>';
        var compiled = $compile(content)(scope);
        return compiled[0];
    };

    factory.createInfobox = function (scope, map, marker) {
        var boxText = factory.getInfoboxContent(scope);
        var myOptions = {
            content: boxText,
            alignBottom: true,
            disableAutoPan: false,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-116, -42),
            zIndex: null,
            boxStyle: {
                //background: "url('tipbox.gif') no-repeat",
                //padding: "10px 10px 10px 10px",
                //opacity: 0.75,
                width: "224px",
                height: "136px",
                "background-color": "#FFFFFF",
                "box-shadow" : "0 2px 4px 0 rgba(0,0,0,0.5)"
            },
            closeBoxMargin: "0px",
            //closeBoxURL: "/static/img/close.png",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false,
        };

        var ib = new InfoBox(myOptions);
        ib.openIfClosed = function() {
            if (this.getMap() == null) {
                this.open(map, marker);
            }
        };
        marker.infobox = ib;
        //ib.openIfClosed();
    };

    return factory;
});
