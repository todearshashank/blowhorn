(function ($) {
    $(document).ready(function () {
        const ICON_WAYPOINT_MARKER = "/static/website/images/stop-marker.svg";
        const ICON_PICKUP_MARKER = "/static/website/images/Blue-Marker@2x.png";
        const ICON_DROPOFF_MARKER = "/static/website/images/Purple-Marker@2x.png";
        mapWidgetOptions.mapWrapperElem.style.display = 'block';
        mapWidgetOptions.mapMessageElem.style.display = 'none';

        function getStopIcon(title) {
            return 'data:image/svg+xml;utf-8,' + encodeURIComponent(' \
                <svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> \
                    <title>RedMarkerWithNumber@1x</title> \
                    <desc>Created with Sketch.</desc> \
                    <defs></defs> \
                    <g id="Assets" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> \
                        <g id="Assets-Common" transform="translate(-336.000000, -417.000000)"> \
                            <g id="RedMarkerWithNumber" transform="translate(336.000000, 417.000000)"> \
                                <g id="Group" transform="translate(4.000000, 1.000000)"> \
                                    <g id="Shadow" transform="translate(6.600000, 28.826087)" fill="#C9D2D6"> \
                                        <path d="M19.8,4.844634 C19.8,7.51914323 15.3682617,9.68926799 9.90141408,9.68926799 C4.43173832,9.68926799 0,7.51914323 0,4.844634 C0,2.1680055 4.43173832,0 9.90141408,0 C15.3682617,0 19.8,2.1680055 19.8,4.844634" id="Fill-1"></path> \
                                    </g> \
                                    <path d="M10.7047569,32.8376585 L16.5,37.3043478 L22.2952431,32.8376585 C28.5491089,30.4263894 33,24.2256175 33,16.9565217 C33,7.59169337 25.6126984,0 16.5,0 C7.38730163,0 0,7.59169337 0,16.9565217 C0,24.2256175 4.4508911,30.4263894 10.7047569,32.8376585 Z" id="Combined-Shape" fill="#141515"></path> \
                                    <rect id="Rectangle" fill="#D8D8D8" x="6.6" y="6.7826087" width="6.6" height="6.7826087"></rect> \
                                    <ellipse id="Oval-2" fill="#FC3446" cx="16.5" cy="16.9565217" rx="14.85" ry="15.2608696"></ellipse> \
                                </g> \
                                <text id="2" font-family="Heebo,Heebo-Bold" font-size="24" font-weight="bold" letter-spacing="-0.300000012" fill="#FFFFFF"> \
                                    <tspan x="13.2476563" y="26">' + title + '</tspan> \
                                </text> \
                                <rect id="bounds" stroke="#979797" stroke-width="0.01" x="0.005" y="0.005" width="39.99" height="39.99"></rect> \
                            </g> \
                        </g> \
                    </g> \
                </svg>'
            );
        }

        console.log(mapWidgetOptions.locationFieldValue);
        if (mapWidgetOptions.locationFieldValue) {
            var data = JSON.parse(mapWidgetOptions.locationFieldValue);
            data = data.coordinates;
            var totalStops = data.length;
            if (!totalStops) {
                console.error('No stops found', totalStops);
                return
            }
            var mapCenter = mapWidgetOptions.mapCenterLocation ?
            mapWidgetOptions.mapCenterLocation : [12.2323, 17.5557];

            var map = initMap(mapWidgetOptions.mapElement, {
                center: new google.maps.LatLng(mapCenter[0], mapCenter[1]),
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT
                },
                zoomControl: true,
                gestureHandling: 'cooperative',
                zoom: mapWidgetOptions.zoom
            });
            var routeTraceData = [];
            bounds = new google.maps.LatLngBounds();
            for (var i = 0 ; i < totalStops; i++) {
                var coords = data[i];
                var total_geopoints = coords.length;
                for (var j = 0; j < total_geopoints; j++) {
                    var markerPosition = {
                        'lat': coords[j][1],
                        'lng': coords[j][0]
                    };
                    routeTraceData.push(markerPosition);

                    var _location = getGoogleLatLngObject(markerPosition.lat, markerPosition.lng);
                    bounds.extend(_location);

                    var markerOptions = {
                        draggable: false,
                        optimized: false,
                        position: _location,
                        icon: {},
                        map: map
                    };

                    if (i === 0 && j === 0) {
                        // Marker for Source
                        markerOptions.icon.url = ICON_PICKUP_MARKER;
                        data[i].marker = initMarker(map, markerOptions);
                    } else if ((i === (totalStops - 1)) &&
                        (j === (total_geopoints - 1))) {
                        // Marker for Destination
                        markerOptions.icon.url = ICON_DROPOFF_MARKER;
                        data[i].marker = initMarker(map, markerOptions);
                    } else if (j === (total_geopoints - 1)) {
                        // Marker for each stops
                        markerOptions.icon.url = getStopIcon(i + 1);
                        data[i].marker = initMarker(map, markerOptions);
                    }
                }
            }

            // Draw route on map
            var path = new google.maps.Polyline({
                path: routeTraceData,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            path.setMap(map);
            map.fitBounds(bounds);
            google.maps.event.trigger(map, 'resize');

            // Need to refresh when map is inside tab.
            $('.changeform-tabs-item, .changeform-tabs-item-link').on('click', function (e) {
                e.stopPropagation();
                var refresh = function () {
                    center = map.getCenter();
                    google.maps.event.trigger(map, 'resize');
                    map.setCenter(center);
                    map.setZoom(15);
                };
                setTimeout(refresh, 100);
            });
        } else {
            mapWidgetOptions.mapWrapperElem.style.display = 'none';
            mapWidgetOptions.mapMessageElem.style.display = 'block';
        }
    });
})(jQuery || django.jQuery);
