var selectedStatus = 'published';
var productId = null;
var storedData = {};
var $element = null;
var $select = null;

$(document).ready(function () {
    $element = $('#response');
    $select = (window.outerWidth > 769) ?
            $('.hidden-xs #products')
            : $('.visible-xs #products');
    var productList = [];
    for (var i = 0; i < products.length; i++) {
        productList.push({
            id: products[i].pk,
            text: products[i].name
        });
    }
    $('#no-data').hide();
    $('#loader').show();
    if (!products.length) {
        $('#error-message').html('No data found');
        $('#no-data').show();
        $('#loader').hide();
    } else {
        selectStatus('published');
        $select.select2({
            data: productList
        });
    }

    $select.on("select2:select", function (e) {
        productId = $(e.currentTarget).val();
        if (productId === 'All products') {
            productId = null;
        }
        fetchData();
    });
});

function selectStatus(status) {
    selectedStatus = status;
    var $currentElem = (window.outerWidth > 769) ?
        $('.hidden-xs #' + selectedStatus)
        : $('.visible-xs #' + selectedStatus);
    $currentElem.addClass('highlight');
    $currentElem.siblings('div').removeClass('highlight');
    fetchData();
}

function fetchData() {
    var data = {
        status: selectedStatus
    }
    if (productId) {
        data['product_id'] = productId;
    }
    $('#no-data').hide();
    // Disabling local assignment for now.
    // if (productId && storedData.hasOwnProperty(productId) && storedData[productId].hasOwnProperty(selectedStatus)) {
    //     $element.html(storedData[productId][selectedStatus]);
    //     return;
    // }
    $('#loader').show();
    $.ajax({
        type: 'GET',
        url: '/api/changelog',
        data: data,
        success: function (response) {
            $element.html(response);
            $('#loader').hide();
        },
        error: function (response) {
            switch (response.status) {
                case 404:
                    $('#error-message').html(JSON.parse(response.responseText));
                    break;

                default:
                    $('#error-message').html(response);
                    break;
            }
            $element.html('');
            $('#no-data').show();
            $('#loader').hide();
        }
    });
}
