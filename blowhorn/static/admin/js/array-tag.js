(function() {
    'use strict';

    var ArrayTag = function(el) {
        this.el = (typeof el === 'string') ? document.querySelector(el) : el;
        this.el.style.display = 'none';

        var mainElemId = this.el.id;
        const PINCODE = /pincode/;
        var isPincode = mainElemId.search(PINCODE) > 0;

        this.delim = this.el.dataset.delim || ',';
        this.values = new Set(this.split_tags(this.el.value));

        this.el.closest('form').addEventListener('submit', ev => {
            this.el.style.display = undefined;
            this.el.value = Array.from(this.values).join(this.delim + ' ');
        });

        var div = document.createElement('div');
        div.classList.add('tag-input');

        this.inp = document.createElement('input');
        this.inp.setAttribute('id', 'tag-box');
        this.inp.setAttribute('type', 'text');
        div.appendChild(this.inp);

        this.inp.addEventListener('keyup', function (e) {
          document.getElementById('tag-box').removeAttribute('class', 'invalid-pincode');
        });

        this.button = document.createElement('a');
        this.button.innerText = 'Add';
        this.button.classList.add('button', 'add-tag');

        var pincodeRegex = /^[0-9]{4,6}$/g;
        this.button.addEventListener('click', ev => {
          this.tags = this.split_tags(this.inp.value)
          console.log(this.tags)
          var i;
          for(i=0; i<this.tags.length; i++){
              if (isPincode && !this.tags[i].match(pincodeRegex)) {
                document.getElementById('tag-box').setAttribute('class', 'invalid-pincode');
                break;
              };
              this.values.add(this.tags[i]);
              };
          this.inp.value = '';
          this.render_tags();
        });
        div.appendChild(this.button);

        this.tagList = document.createElement('p');
        this.tagList.classList.add('tags');
        this.tagList.addEventListener('click', ev => {
          if(!ev.target.matches('.tags a')) return;
          this.values.delete(ev.target.parentNode.innerText.trim());
          this.render_tags();
        });
        div.appendChild(this.tagList);

        this.el.parentElement.insertBefore(div, this.el);

        this.render_tags();
    };

    ArrayTag.prototype.render_tags = function () {
        this.tagList.innerHTML = Array.from(this.values)
            .sort()
            .map(val => '<span>' + val + '<a class="deletelink"></a></span>')
            .join(' ');
    };

    ArrayTag.prototype.split_tags = function (value) {
      return value.split(this.delim).map(x => x.trim()).filter(Boolean);
    };

    window.ArrayTag = ArrayTag;

    document.addEventListener('DOMContentLoaded', () => {
        Array.from(document.querySelectorAll('.array-tag')).forEach(el => new ArrayTag(el));
    });
    // $('formset:added', (ev, row) => {
    //     Array.from(row.querySelectorAll('.array-tag')).forEach(el => new ArrayTag(el));
    // });
})();
