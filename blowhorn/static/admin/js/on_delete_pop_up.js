(function($) {$(function() {

    var destination = $('#trip_form > div > div.submit-row > input.default.transition-suspend');

    var html =  '<div id="modalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '<h4> Reason for Suspension</h4>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<form id="suspension_reason">';
    html += '<input type="radio" name="reason_of_suspension" id="reason_1" value="Vehicle breakdown" checked> Vehicle breakdown<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_2" value="Driver denied duty"> Driver denied Duty<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_3" value="Sick"> Sick<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_4" value="Family Issues"> Family Issues<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_5" value="Traffic violation"> Traffic Violation<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_6" value="No Fuel"> No Fuel<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_7" value="Accident"> Accident<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_8" value="Customer Cancelled"> Customer Canceled<br>';
    html += '<input type="radio" name="reason_of_suspension" id="reason_9" value="Trip Interrupted"> Trip Interrupted';
    html += '</form>';
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<input class="default transition-suspend" name="_fsmtransition-status-suspend" type="submit" value="Submit">';
    html += '</div>';  // footer
    html += '</div>';  // modalWindow
    $("#trip_form").append(html);

    var reason_for_suspension_block = jQuery('.field-reason_for_suspension');

    var reason_for_suspension_block_after_submit = jQuery('.field-reason_for_suspension > div > p');

    if(reason_for_suspension_block_after_submit.length > 0)
        reason_for_suspension_block.show();
    else
        reason_for_suspension_block.hide();

    $(document).ready(function(){
        destination.addClass("dummy_class")
        destination.removeClass('transition-suspend');
        if(destination[0]) {
            destination[0].removeAttribute('name');
            destination[0].setAttribute('type', 'button');
        }
        $(".dummy_class").click(function() {
            jQuery("#modalWindow").modal('show');
            $('#id_reason_for_suspension').val($('input[name="reason_of_suspension"]:checked', '#suspension_reason').val()).trigger('change');
        });

        $('#suspension_reason input').on('change', function() {
            $('#id_reason_for_suspension').val($('input[name="reason_of_suspension"]:checked', '#suspension_reason').val()).trigger('change');
        });

        $("#suspension_reason").on('submit', function() {
            $("#trip_form").submit();
            $('.modal.in').modal('hide');
        });
    });    

});
})(django.jQuery);
