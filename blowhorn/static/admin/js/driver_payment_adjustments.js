(function($) {$(function() {

    /* Get the inline tables*/
    var tables = $('#driverpaymentadjustment_set-group > div > fieldset > table > tbody');
    /* Get all the rows*/
    var rows = tables[0].getElementsByTagName("tr");

    /* Loop through the rows and check status
    2 default rows are appended so (-2) */
    function setFieldsNonEditable() {
        for(var row_number=0; row_number<=rows.length-2; row_number++) {
            var status = $('#id_driverpaymentadjustment_set-'+row_number+'-status');
            /* status is always readonly */
            if (!status.val()) {
                $('#id_driverpaymentadjustment_set-'+row_number+'-status').val('New')
            }
            $('#id_driverpaymentadjustment_set-'+row_number+'-status').prop('readonly',true);

            if (status.val() != 'New') {
                $('#id_driverpaymentadjustment_set-'+row_number+'-category').prop('readonly',true);
                $('#id_driverpaymentadjustment_set-'+row_number+'-category').prop('disabled',true);
                $('#id_driverpaymentadjustment_set-'+row_number+'-description').prop('readonly',true);
                $('#id_driverpaymentadjustment_set-'+row_number+'-adjustment_amount').prop('readonly',true);
                $('#id_driverpaymentadjustment_set-'+row_number+'-adjustment_datetime').prop('readonly',true);
                $('#id_driverpaymentadjustment_set-'+row_number+'-adjustment_datetime').prop('disabled',true);

                /* Disable the icon field */
                var icon = $('#id_driverpaymentadjustment_set-'+row_number+'-adjustment_datetime')[0];
                if (typeof icon != 'undefined') {
                    icon.nextSibling.style.display='none';
                /*icon.nextElementSibling.addEventListener("click", function(e){
                e.preventDefault()
            })*/
        }

            /*  For select2 fields disablied will set the value as blank
            Get the category value and pass it to hidden field */
            var category = $('#id_driverpaymentadjustment_set-'+row_number+'-category').val();
            
            var input = document.createElement("input");
            input.setAttribute("type", "hidden");
            input.setAttribute("name", 'driverpaymentadjustment_set-'+row_number+'-category');
            input.setAttribute("value", category);
            $('#id_driverpaymentadjustment_set-'+row_number+'-category').append(input);

        }
    }
}
setFieldsNonEditable();
$(".add-row td a").click(function() {
    setFieldsNonEditable();
});
});
})(django.jQuery);