(function($) {
    $(function () {

    var driverElemId = 'input[name="driver"]';
    var statusElementId = 'input[name="new_status"]';
    var selectedOrder = null;

    var deactivate_button = jQuery('.transition-deactivate');
    var activate_button = jQuery('.transition-activate');

    var unable_to_deliver_button = jQuery('.transition-unable_to_deliver');
    var reject_button = jQuery('.transition-reject');
    var deliver_button = jQuery('.transition-deliver');

    var html =  '<div id="modalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog" style="width:450px;">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '<h4> Unable to deliver shipment order?</h4>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<p id="shipment_order_state">';
    html += 'Click Confirm to continue or Cancel to abort<br>';
    html += '</p>';
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    html += '<input id="submitButton" class="default transition-unable_to_deliver pull-right" name="_fsmtransition-status-unable_to_deliver" type="submit" value="Confirm">';
    html += '</div>';  // footer
    html += '</div>';  // modalWindow
    $("#shipmentorder_form").append(html);

    function initDriverAutocompleteInput(create_trip, assign_orders) {
        jQuery('#driver-autocomplete').autocomplete({
            appendTo: '.driver-autocomplete-wrapper',
            source: function (request, response) {
                $.ajax({
                    method: "GET",
                    url: "/api/driver/search/",
                    dataType: "json",
                    data: {
                        term: request.term,
                        order_id: selectedOrder,
                        create_trip: create_trip,
                        assign_orders: assign_orders
                    },
                    success: function (data) {
                        var processedData = [];
                        data.forEach(item => {
                            processedData.push({
                                id: item[0],
                                label: `${item[1]} | ${item[2]}`,
                                value: item[0]
                            });
                        });
                        response(processedData);
                        $('span[role="status"]').removeClass('ui-helper-hidden-accessible');
                    }
                });
            },
            minLength: 4,
            select: function (event, ui) {
                $(driverElemId).val(ui.item.value);
                $('#selected-driver').html(ui.item.label);
                $('span[role="status"]').addClass('ui-helper-hidden-accessible');
                $(this).val('');
                return false;
            }
        });
    }

    function openDriverAutoComplete(create_trip, assign_orders) {
        if (!selectedOrder) {
            $('select[name="action"]').val(null).trigger('change');
            alert('Please, select orders to continue..');
            return;
        }

        initDriverAutocompleteInput(create_trip, assign_orders);
        jQuery('#modal-autocomplete-drivers').modal('show');
    }

    function openStatusChangeModal() {
        if (!selectedOrder) {
            $('select[name="action"]').val(null).trigger('change');
            alert('Please, select orders to continue..');
            return;
        }
        $("#selected-status").change(function() {
            var ns = $( "#selected-status").val();
            $(statusElementId).val(ns)
        });

        jQuery('#modal-statuschange-orders').modal('show');
    }

    $(document).ready(function () {
        $(driverElemId).parent().hide();
        $(driverElemId).hide();
        $(statusElementId).parent().hide();
        $(statusElementId).hide();

        $("body").append('<div id="id-driver-autocomplete-wrap"></div>');
        $("#id-driver-autocomplete-wrap").load("/static/admin/driver/html/driver_autocomplete.html");
        $("body").append('<div id="id-order-statuschnage-wrap"></div>');
        $("#id-order-statuschnage-wrap").load("/static/admin/driver/html/order_statuschange.html");

        $('input[name="_selected_action"], #action-toggle').on('click', function () {
            var inputs = $('input[name="_selected_action"]');
            selectedOrder = null;
            for (var i = 0; i < inputs.length; i++) {
                var currentInput = $(inputs[i])[0];
                if (currentInput.checked) {
                    selectedOrder = currentInput.value;
                    console.log('selectedOrder: ', selectedOrder);
                    break;
                }
            }
        });
        $('select[name="action"]').on('change', function () {
            var action = $('select[name="action"]').val();
            console.log('action: ', action);
            $(driverElemId).val('');
            switch (action) {
                case 'create_new_trip_for_orders':
                    openDriverAutoComplete(create_trip=true, assign_orders=false);
                    break;
                case 'assign_orders_in_trip':
                    openDriverAutoComplete(create_trip=false, assign_orders=true);
                    break;

                case 'shipment_order_status_update':
                    openStatusChangeModal();
                    break;
                default:
                    console.log('other action');
            }
        });
        $('#modal-autocomplete-drivers').on('hidden.bs.modal', function () {
            $('#selected-driver').html('No driver selected');
            $('select[name="action"]').val(null).trigger('change');
        });

        unable_to_deliver_button.addClass("unable_to_deliver_class").removeClass('transition-unable_to_deliver');
        reject_button.addClass("reject_class").removeClass('transition-reject');
        deliver_button.addClass("deliver_class").removeClass('transition-deliver');

        if(unable_to_deliver_button[0]) {
            unable_to_deliver_button[0].removeAttribute('name');
            unable_to_deliver_button[0].setAttribute('type', 'button');
        }

        if(reject_button[0]) {
            reject_button[0].removeAttribute('name');
            reject_button[0].setAttribute('type', 'button');
        }

        if(deliver_button[0]) {
            deliver_button[0].removeAttribute('name');
            deliver_button[0].setAttribute('type', 'button');
        }

        $(".unable_to_deliver_class").click(function() {
            jQuery("#modalWindow").modal('show');
            $('h4').text('Unable to deliver shipment order?');
            $('#submitButton').removeClass('transition-deactivate').addClass('transition-unable_to_deliver');
            $('#submitButton')[0].setAttribute('name', '_fsmtransition-status-unable_to_deliver');
        });

        $(".reject_class").click(function() {
            jQuery("#modalWindow").modal('show');
            $('h4').text('Reject shipment order?');
            $('#submitButton').removeClass('transition-deactivate').addClass('transition-reject');
            $('#submitButton')[0].setAttribute('name', '_fsmtransition-status-reject');
        });

        $(".deliver_class").click(function() {
            jQuery("#modalWindow").modal('show');
            $('h4').text('Deliver shipment order?');
            $('#submitButton').removeClass('transition-deactivate').addClass('transition-deliver');
            $('#submitButton')[0].setAttribute('name', '_fsmtransition-status-deliver');
        });

        $("#shipment_order_state").on('submit', function() {
            $("#shipmentorder_form").submit();
            $('.modal.in').modal('hide');
        });
    });

    });
})(django.jQuery);
