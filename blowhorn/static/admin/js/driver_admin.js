(function($) {
    /* Function to show / hide reason and description
          while selecting the driver status
          if selected status is inactive show both the fields
          else hide.
      */
    $(function() {
        var selectField = $("#id_status"),
            reason_for_inactive = $(".field-reason_for_inactive");
        description = $(".field-description");
        var is_blowhorn_driver = $("#id_is_blowhorn_driver");
        contract = $(".field-contract");

        function toggleContract(value) {
            console.log(value);
            if (value) {
                contract.show();
            } else {
                contract.hide();
            }
        }

        function toggleReason(value) {
            value == "inactive" || value == "blacklisted" ?
                reason_for_inactive.show() :
                reason_for_inactive.hide();
            value == "inactive" || value == "blacklisted" ? description.show() : description.hide();
        }

        // show / hide on load based on pervious value of selectField
        toggleReason(selectField.val());

        // show / hide on change
        selectField.change(function() {
            toggleReason($(this).val());
        });

        if (typeof is_blowhorn_driver[0] != 'undefined')  {
            // show / hide on load based on pervious value of is_blowhorn_driver
            toggleContract(is_blowhorn_driver[0].checked);

            // show / hide on change
            is_blowhorn_driver.change(function () {
                toggleContract($(this)[0].checked);
            })
        }

        var is_address_same = $("div.form-row.field-is_address_same > div > label"),
            permanent_address = $("div.form-row.field-permanent_address");

        function markCurrentAddressAsPermanent() {
            value = $("#id_is_address_same").prop("checked");
            value ? permanent_address.hide() : permanent_address.show();
        }

        markCurrentAddressAsPermanent();

        // show / hide on click
        $("#id_is_address_same").on("change", function(e) {
            markCurrentAddressAsPermanent();
        });

        var is_owner = $("div.form-row.field-own_vehicle > div > label");
        var owner_detail = $(
            "div.form-row.field-owner_details.field-bank_account.field-get_owner_bank_details > div.field-box.field-owner_details"
        );
        var bank_detail = $(
            "div.form-row.field-owner_details.field-bank_account.field-get_owner_bank_details > div.field-box.field-bank_account"
        );
        var owner_bank_detail = $(
            "div.form-row.field-owner_details.field-bank_account.field-get_owner_bank_details > div.field-box.field-get_owner_bank_details"
        );
        // $("[name='own_vehicle']").bootstrapSwitch();

        function showOwnerDetail() {
            console.log("Here I am in show details functionality");
            value = $("#id_own_vehicle").prop("checked");
            if (value) {
                owner_detail.hide();
                owner_bank_detail.hide();
                bank_detail.show();
            } else {
                owner_detail.show();
                owner_bank_detail.show();
                bank_detail.hide();
            }
            // value ? owner_detail.hide(): owner_detail.show()
        }
        showOwnerDetail();

        // show / hide on click
        $("#id_own_vehicle").on("change", function(e) {
            console.log("here I am in change functionality");
            showOwnerDetail();
        });
    });

    $(document).ready(function() {
        $("body").append('<div id="id-card-wrap"></div>');
        $("#id-card-wrap").load("/static/admin/html/drivers/id_card.html");
    });

})(django.jQuery);

$ = jQuery;
var cardIdNamePrefix = "DriverIdCard";
// Driver Id Card
function saveAs(uri, filename) {
    var link = document.createElement("a");
    if (typeof link.download === "string") {
        link.href = uri;
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    } else {
        window.open(uri);
    }
}

function base64MimeType(encoded) {
  var result = 'png';

  if (typeof encoded !== 'string') {
      return result;
  }

  var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
  if (mime && mime.length) {
      var ext = mime[1];
      ext_array = ext.split('/');
      if (ext_array.length > 1) {
          result = ext_array[1];
      }
  }
  return result;
}

function downloadCard(id) {
    var date_repr = new Date().toISOString().replace(/-/g, '_').replace(/:/g,'_').replace(/ /g, '');
    var filename = [cardIdNamePrefix, id, date_repr].join("_");
    domtoimage.toJpeg(document.getElementById(id), { quality: 1.0 })
    .then(function (dataUrl) {
        saveAs(dataUrl, [filename, base64MimeType(dataUrl)].join('.'));
    });
}

function showDriverIdCard(
    name,
    avatar_url,
    phone_number,
    registration_number,
    qr_value,
    build,
    brand_name,
    pk
) {
    $("#id-card-message").hide();
    $("#footer-btn-wrap").show();
    if (!name || !avatar_url || !phone_number || !registration_number) {
        $("#footer-btn-wrap").hide();
        $("#id-card-message").show();
    }
    cardIdNamePrefix = phone_number;

    // curl -I -H "Origin: <origin>" -H "Access-Control-Request-Method: <method>" <media-url>
    // $("#cardPic").attr("CrossOrigin", "anonymous");
    $("#cardPic").attr("src", avatar_url);
    $("#cardId").html(registration_number);

    // To make Name as TitleCase
    var modName = name
        .replace(/\./g, " ")
        .toLowerCase()
        .replace(/([^ .-])([^ .-]*)/gi, function(v, v1, v2) {
            return v1.toUpperCase() + v2;
        });

    // get the website build
    var build = build

    // get company name
    var brandName = brand_name

    $("#frontCover").css("background-image", `url(/static/img/${build}/BACK_WHITEBG.jpg)`)
    $("#backCover").css("background-image", `url(/static/img/${build}/FRONT_WHITEBG.png)`)

    $('.disclaimer-body').html(`Driver ID card is not valid for any other purposes other than at ${brandName} for driver identification.`)

    $("#cardName").html(modName);
    $("#cardPhone").html(phone_number);

    $("#cardQr").html("");
    $("#cardQr").qrcode(qr_value);
    $("#cardIdBack").html(registration_number);

    $("#modal-gen-driver-card").modal({ backdrop: "static", keyboard: false });
    $("#modal-gen-driver-card").modal("show");
}
