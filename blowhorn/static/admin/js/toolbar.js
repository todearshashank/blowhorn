(function ($) {
    function filterBar($changelist) {
        var filterName;
        var $toolbar = $changelist.find('#toolbar');

         /* If toolbar not created because of no search options.
            So create it manually */
        if ($toolbar.length === 0) {
            $toolbar = $('<div>').attr('id', 'toolbar');
            $('#changelist').prepend($toolbar);
        }

        $changelist.find('#changelist-filter').children().each(function() {
            var $element = $(this);

            if ($element.prop('tagName') === 'H3') {
                filterName = $element.text();
            } else if ($element.prop('tagName') === 'UL') {
                var $select = $('<select>');
                var $items = $element.find('li');

                $.each($element.prop('attributes'), function() {
                    $select.attr(this.name, this.value);
                });

                $select.addClass('changelist-filter-select');

                if ($items.filter('.selected').length > 1) {
                    $select.attr('multiple', true);
                }

                $items.each(function(i) {
                    var $item = $(this);
                    var $link = $item.find('a');
                    var $option = $('<option>')
                        .text($link.text())
                        .attr('data-url', $link.attr('href'))
                        .attr('selected', $item.hasClass('selected'));

                    if (i === 0 ) {
                        if (filterName != null) {
                            $option.text(filterName)
                        }

                        var $separator = $('<option>')
                            .attr('disabled', true)
                            .text('---');

                        $option = $option.add($separator);
                    }

                    $select.append($option);
                });

                var $wrapper = $('<span>')
                    .addClass('changelist-filter-select-wrapper')
                    .append($select);
                $toolbar.append($wrapper);

                filterName = null;
            } else if ($element.hasClass('changelist-filter-popup')) {
                var $toggle = $element.find('.changelist-filter-popup-toggle');
                var $content = $element.find('.changelist-filter-popup-content');
                var $wrapper = $('<span>')
                    .addClass('changelist-filter-select-wrapper')
                    .append($element);

                $toolbar.append($wrapper);

                $toggle.on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $content.toggleClass('visible');
                });

                $content.on('click', function(e) {
                    e.stopPropagation();
                });

                $(document.body).on('click', function() {
                    $content.removeClass('visible');
                });
            }
        });

        $changelist.find('#changelist-filter').remove();
    }

    $(document).ready(function () {
        $('#changelist').each(function () {
            filterBar($(this));
        });

        // Todo: have to make the change to disable the submit button to avoid multiple clicks

        // Prevent form being submitted twice
        // var submitPressed = false;
        // $('input[type="submit"]').on('click', function (e) {
        //     if (submitPressed) {
        //         e.preventDefault();
        //         $('input[type="submit"]').prop('disabled', true);
        //         $('input[type="submit"]').prop('value', 'Wait...');
        //     }
        //     submitPressed = true;
        // });
    });
})(django.jQuery);
