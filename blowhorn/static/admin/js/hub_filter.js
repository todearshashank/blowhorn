(function($) {$(function() {

    var contract_key = $('#id_customer_contract');
    var hub_key = $('#id_hub');
    var contract_name_key = $('div.form-row.field-customer_contract p');

    function driver_filter_ajax(contract_param) {
        hub_key.empty().trigger('change');
        var option = new Option("---------", "", true, true);
        hub_key.append(option).trigger('change');
        // console.log(contract_param)
        $.ajax({
           type: 'GET',
           url: '/api/hubs/filters/',
           data: contract_param,
           contentType: "application/json; charset=utf-8",
           dataType: 'json',
           responseType: 'arraybuffer',
           success: function(response) {
                $.each(response, function(key, value) {
                    // console.log(value['id'] + ' ' + value['driver']);
                    var option = new Option(value.name, value.id, false, false);
                    hub_key.append(option).trigger('change');
                });
            }
        });
    }


    $(document).ready(function () {
        hub_key.prop('disabled', true)
        contract_key.on('change', function () {
            sp = $('#select2-id_driver-container');
            sp.text('---------');
            var params = {"contract_id": contract_key.val()}
            driver_filter_ajax(params);
            hub_key.prop('disabled', false);
        });

        if (contract_key.val()) {
            hub_key.prop('disabled', false);
        }
        if (contract_name_key.length) {
            hub_key.prop('disabled', false)
        }
    });
});
})(django.jQuery);


