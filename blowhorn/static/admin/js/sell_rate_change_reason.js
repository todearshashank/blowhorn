(function ($) {
    $(function () {
        var saveBtn = $('#sellrate_form input[name="_save"]');
        var saveAndContinueBtn = $('#sellrate_form input[name="_continue"]');
        var saveAndAddAnotherBtn = $('#sellrate_form input[name="_addanother"]');

        var html = '<div id="modalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
        html += '<div class="modal-dialog">';
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<a class="close" data-dismiss="modal">×</a>';
        html += '<h4> Reason for Change in Sell Rate</h4>';
        html += '</div>';
        html += '<div class="modal-body">';
        html += '<form id="rate_change">';
        html += '<textarea id="change_reason" name="reason_for_change" rows="2" cols="50" maxlength="100"></textarea><br>';
        html +='<div id="info"></div>'
        html +='<p id="message"></p>'
        html += '</form>';
        html += '</div>';
        html += '<div class="modal-footer">';
        html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="button" value="Cancel">';
        html += '<input id="submitButton" class="default" name="_save" type="submit" value="Submit">';
        html += '</div>';  // footer
        html += '</div>';  // modalWindow        
        $("#sellrate_form").append(html);

        var change = false;

        $(document).ready(function () {

            function getUrlParameters(parameter, staticURL){
                var currLocation = (staticURL.length)? staticURL : document.URL;
                var parser = document.createElement('a');
                parser.href = currLocation;
                if (parser.pathname.match(parameter)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            var is_obj_change = getUrlParameters("/change", document.URL);

            if (saveBtn.length) {
                saveBtn[0].setAttribute('type', 'button');
            }
            if (saveAndContinueBtn.length) {
                saveAndContinueBtn[0].setAttribute('type', 'button');
            }
            if (saveAndAddAnotherBtn.length) {
                saveAndAddAnotherBtn[0].setAttribute('type', 'button');
            }

            $(":input").change(function() {
                change = true;

            });

            $("#info").text("Characters left: 100");

            $(saveBtn).click(function () {
                $('#submitButton').addClass('default');
                $('#submitButton')[0].setAttribute('name', '_save');
                if (change && is_obj_change) {
                    jQuery("#modalWindow").modal('show');
                }
                else {
                    saveBtn[0].setAttribute('type', 'submit');
                    saveBtn[0].setAttribute('name', '_save');
                    saveBtn.addClass('default');
                }
            });
            $(saveAndContinueBtn).click(function () {
                $('#submitButton').addClass('default');
                $('#submitButton')[0].setAttribute('name', '_continue');
                if (change && is_obj_change) {
                    jQuery("#modalWindow").modal('show');
                }
                else {
                    saveAndContinueBtn[0].setAttribute('type', 'submit');
                    saveAndContinueBtn[0].setAttribute('name', '_continue');
                    saveAndContinueBtn.addClass('default');
                }
            });

            $(saveAndAddAnotherBtn).click(function () {
                $('#submitButton').addClass('default');
                $('#submitButton')[0].setAttribute('name', '_addanother');
                if (change && is_obj_change) {
                    jQuery("#modalWindow").modal('show');
                }
                else {
                    saveAndAddAnotherBtn[0].setAttribute('type', 'submit');
                    saveAndAddAnotherBtn[0].setAttribute('name', '_addanother');
                    saveAndAddAnotherBtn.addClass('default');
                }
            });

            $("#change_reason").on("keyup paste", function() {
                $("#info").text("Characters left: " + (100 - $(this).val().length));
                $('#id_reason_for_change').val($(this).val()).trigger('change');
            });

            $("#submitButton").on("click", function(){
                var message, reason;
                message = document.getElementById("message");
                message.innerHTML = "";
                reason = document.getElementById("change_reason").value;
                try { 
                    if($.trim(reason) == "") throw "is needed";
                }
                catch(err) {
                    message.innerHTML = "Reason for change " + err;
                    return false;
                }
                return true
            });

            $("#rate_change").on('submit', function () {
                $("#sellrate_form").submit();
                $('.modal.in').modal('hide');
            });
        });
    });

})(django.jQuery);
