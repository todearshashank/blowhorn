(function ($) {
    $(function () {
        function displayText() {
            //  Display RateCard usage in BuyRate and SellRate Admin at footer
            var contract_usage = $('#contractterm_set-group fieldset tbody tr .field-get_contract p');
            var contract_names = [];
            for (var i = 0; i < contract_usage.length; i++) {
                if (contract_usage[i].innerHTML === '-') {
                    continue;
                }
                contract_names.push(contract_usage[i].innerHTML);
            }
            console.log('contract_names', contract_names)
            var usage_header = 'This rate card is used in ' + contract_names.length + ' contract(s)';
            var usage_content = '<p>' + contract_names.join('<br>') + '</p>';
            $('#usage-header').text(usage_header);
            $('#usage-content').append(usage_content);
        }
        $(document).ready(function () {
            $("#content-main").append('<div id="contract-usage-info-block" class="aligned hints-block"></div>');
            $("#contract-usage-info-block").load("/static/admin/html/contract/contract_usage_info.html", displayText);
        });
    });
})(django.jQuery);
