(function ($) {

    $(function() {
        var tables = $("#result_list > tbody");
        var rows = tables[0].getElementsByTagName("tr");
        var change_list_footer = $("#changelist-form > div.changelist-footer");

        var total_amount = 0;
        var count = rows.length;

        var balance_summation = document.createElement('p');
        balance_summation.setAttribute('id', 'BalanceSummation');
        balance_summation.setAttribute('style','font-weight:bold; padding:0px;');
        balance_summation.innerHTML = 'Grand Total of above Opening Balances : ' + total_amount;
        change_list_footer.append(balance_summation);

        for (var row_number = 0; row_number < count; row_number++) {
            var opening_balance_data = rows[row_number].getElementsByClassName("field-amount");
            if (opening_balance_data.length) {
                opening_balance = parseFloat(opening_balance_data[0].innerHTML);

                if (!isNaN(opening_balance)) {
                    total_amount = total_amount + opening_balance;
                }

            }
        }
        balance_summation.innerHTML = 'Grand Total of above Opening Balances : ' + total_amount;
});

})(django.jQuery);
