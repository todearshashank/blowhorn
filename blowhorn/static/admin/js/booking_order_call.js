(function($) {$(function() {
    const PAYMENT_STATUS_PAID = 'Paid';
    var driver_key = $('#id_driver');
    var driver_class = document.getElementsByClassName("field-box field-driver");
    var phone_class = $("div.form-row.field-get_customer_phone_number > div > p");
    var order_number = $("div.form-row.field-number > div > p").text();
    var city = $("div.form-row.field-city > div > p").text();

    var driver_button = document.createElement('button');
    driver_button.setAttribute('class', 'driver_btn');
    driver_button.setAttribute('id', 'DriverCall');
    driver_button.innerHTML = '<i class="fa fa-phone"></i>';

    var customer_button = document.createElement('button');
    customer_button.setAttribute('class', 'customer_btn');
    customer_button.setAttribute('type', 'button');
    customer_button.setAttribute('id', 'CustomerCall');
    customer_button.innerHTML = '<i class="fa fa-phone"></i>';

    var driver_whitelist_button = document.createElement('button');
    driver_whitelist_button.setAttribute('class', 'whitelist_btn');
    driver_whitelist_button.setAttribute('type', 'button');
    driver_whitelist_button.setAttribute('id', 'DriverWhitelist');
    driver_whitelist_button.innerHTML = '<i class="fa fa-plus" ></i> Whitelist Number';

    var customer_whitelist_button = document.createElement('button');
    customer_whitelist_button.setAttribute('class', 'whitelist_btn');
    customer_whitelist_button.setAttribute('type', 'button');
    customer_whitelist_button.setAttribute('id', 'CustomerWhitelist');
    customer_whitelist_button.innerHTML = '<i class="fa fa-plus" ></i> Whitelist Number';

    if (driver_class.length) {
        driver_class[0].append(driver_button);
        driver_class[0].append(driver_whitelist_button);
    }
    phone_class.append(customer_button);
    phone_class.append(customer_whitelist_button);

    $("#CustomerWhitelist").hide();
    $("#DriverWhitelist").hide();

    $.fn.ignore = function(sel){
        return this.clone().find(sel||">*").remove().end();
    };

    function exotel_call_method(call_params) {
        $.ajax({
        	type: 'post',
            url: '/api/exotel/call',
            data: JSON.stringify(call_params),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(response){
            	console.log(response);
            },
            error: function(response){
            	console.log(response['responseJSON']);
                if ('show_whis_btn' in response['responseJSON'] && response['responseJSON']['show_whis_btn']) {
                    if (call_params['is_driver']) {
                        $("#DriverWhitelist").show();
                    }

                    else {
                        $("#CustomerWhitelist").show();
                    }
                }
                alert(response['responseJSON']['message']);
            }

        });
    };

    function exotel_whitelist_method(whitelist_params) {
        $.ajax({
            type: 'post',
            url: '/api/exotel/whitelist',
            data: JSON.stringify(whitelist_params),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(response){
                console.log(response)
                // alert(response['responseJSON'])
                if (whitelist_params['is_driver']) {
                    $("#DriverWhitelist").hide();
                }
                else {
                    $("#CustomerWhitelist").hide();
                }
            },
            error: function(response){
                console.log(response['responseJSON']);
                alert(response['responseJSON']['message']);
            }
        });
    };

    $("#CustomerCall").on("click", function(e){
        e.preventDefault();
        var driver_key = $('#id_driver');
        var driver_id = driver_key.val();
        var customer_phone_number = $("div.form-row.field-get_customer_phone_number > div > p").ignore("button").text();

        var params = {
            "is_driver": false,
            "phone_number": customer_phone_number,
            "order_number": order_number,
            "obj_id": "",
            "city": city
        }
        console.log("Calling parameters",params);
        exotel_call_method(params);
    });

    $("#DriverCall").on("click", function(e){
        e.preventDefault();
        var driver_key = $('#id_driver');
        var driver_id = driver_key.val();

        var params = {
            "is_driver": true,
            "obj_id": driver_id,
            "order_number": order_number,
            "phone_number": "",
            "city": city
        }
        console.log("Calling parameters",params)
        exotel_call_method(params);
        // driver_key.prop('disabled', false);
    });

    $("#CustomerWhitelist").on("click", function(e){
        e.preventDefault();
        var customer_phone_number = $("div.form-row.field-get_customer_phone_number > div > p").ignore("button").text();
        var params = {
            "is_driver": false,
            "phone_number": customer_phone_number,
            "obj_id": "",
            "city": city
        }
        console.log("Whitelisting parameters",params);
        exotel_whitelist_method(params);
    });

    $("#DriverWhitelist").on("click", function(e){
        e.preventDefault();
        var driver_key = $('#id_driver');
        var driver_id = driver_key.val()
        // var customer_phone_number = $("div.form-row.field-get_customer_phone_number > div > p").text()
        var params = {
            "is_driver": true,
            "phone_number": "",
            "obj_id": driver_id,
            "city": city
        }
        console.log("Whitelisting parameters",params);
        exotel_whitelist_method(params);
    });

    function handleMarkingOrderAsPaidField() {
        // Capture reason for marking order as paid from admin
        $('.field-reason_for_marking_paid').hide();
        var payment_status = $('select[name="payment_status"]').val();
        var payment_status_readonly = $('.field-payment_status p').text();
        if (payment_status === PAYMENT_STATUS_PAID ||
            payment_status_readonly === PAYMENT_STATUS_PAID) {
            $('.field-reason_for_marking_paid').show();
        }

        if (payment_status_readonly) {
            $('textarea[name="reason_for_marking_paid"').prop('readonly', true);
        }
    }

    $(document).ready(function () {
        handleMarkingOrderAsPaidField();
        var initial_payment_status = $('select[name="payment_status"]').val();
        $('select[name="payment_status"]').on('change', function () {
            var reason_paid_elem = $('textarea[name="reason_for_marking_paid"');
            var reason_fieldset = $('.field-reason_for_marking_paid');
            if ($(this).val() === 'Paid') {
                reason_fieldset.show();
                reason_paid_elem.prop('placeholder', 'Reason for marking as paid (required)');
                reason_paid_elem.prop('required', true);
            } else {
                reason_fieldset.hide();
                reason_paid_elem.prop('required', false);
            }
        });

        $('input[name="_save"], input[name="_continue"]').on('click', function (e) {
            if (initial_payment_status !== 'Paid' &&
                $('select[name="payment_status"]').val() === 'Paid' &&
                !$('textarea[name="reason_for_marking_paid"').val()) {
                alert('Can you provide a reason for marking order as paid from admin?');
                e.preventDefault();
            }
        });
    });
});
})(django.jQuery);
