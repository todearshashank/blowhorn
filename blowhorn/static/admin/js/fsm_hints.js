(function ($) {
    $(function () {
        // To place FSM hints block after submit-rows
        var $submitRows = $('.submit-row');
        var $hintBlock = $('.hints-block');
        var index = (save_on_top === 'True') ? 1 : 0;
        $hintBlock.insertAfter($submitRows[index]);
    });
})(django.jQuery);
