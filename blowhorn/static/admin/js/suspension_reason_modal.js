(function ($) {
    $(function () {
        var saveBtn = $('#spotorder_form input[name="_save"]');
        var saveAndAddBtn = $('#spotorder_form input[name="_addanother"]');

        var html = '<div id="modalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
        html += '<div class="modal-dialog">';
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<a class="close" data-dismiss="modal">×</a>';
        html += '<h4> Reason for Changing of Driver</h4>';
        html += '</div>';
        html += '<div class="modal-body">';
        html += '<form id="suspension_reason">';
        html += '<div class="wrapper">';
        html += '<div class="trip-status">';
        html += '<div class="input-radio"><input type="radio" name="trip_status" id="status_2" value="Suspended" checked> Suspend<br></div>';
        html += '<div class="input-radio"><input type="radio" name="trip_status" id="status_1" value="No-Show"> No Show<br></div>';
        html += '</div>';

        html += '<div class="splitter">';

        html += '<div class="suspend-reason">';
        html += '<div class="suspend-items"><input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_1" value="Vehicle breakdown" checked> Vehicle breakdown<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_2" value="Driver denied Duty"> Driver denied Duty<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_3" value="Sick"> Sick<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_4" value="Family Issues"> Family Issues<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_5" value="Traffic violation"> Traffic violation<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_6" value="No Fuel"> No Fuel<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_7" value="Accident"> Accident<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_8" value="Customer cancelled"> Customer cancelled<br>';
        html += '<input class="suspend-item" type="radio" name="reason_of_suspension" id="reason_9" value="Trip Interrupted"> Trip Interrupted';
        html += '</div></div>';
        html += '<div class="no-show-reason">';
        // The reasons should be fetched from backend in context
        html += '<div class="no-show-items"><input class="no-show-item" type="radio" name="reason_of_suspension" id="reason_10" value="Vehicle breakdown" checked> Vehicle breakdown<br>';
        html += '<input class="no-show-item" type="radio" name="reason_of_suspension" id="reason_11" value="Sick"> Sick<br>';
        html += '<input class="no-show-item" type="radio" name="reason_of_suspension" id="reason_12" value="Family Issues"> Family Issues<br>';
        html += '</div></div>';
        html += '</div>';
        html += '</div>';
        html += '</form>';
        html += '</div>';
        html += '<div class="modal-footer">';
        html += '<input id="submitButton" class="default" name="_save" type="submit" value="Submit">';
        html += '</div>';  // footer
        html += '</div>';  // modalWindow

        $("#spotorder_form").append(html);

        var reason_for_suspension_block = jQuery('.field-reason_for_suspension');
        var reason_for_suspension_block_after_submit = jQuery('.field-reason_for_suspension > div > p');

        if (reason_for_suspension_block_after_submit.length > 0) {
            reason_for_suspension_block.show();
        }

        $(".suspend-items").show();
        $(".no-show-items").hide();
        $('#id_trip_status').val($('input[name="trip_status"]:checked', '#suspension_reason').val()).trigger('change');
        $("#reason_1").prop('checked', true)
        $("#reason_10").prop('checked', false)

        $(document).ready(function () {
            var initDriverVal = $('#id_driver').val();

            $('.input-radio').change(function() {
                $('#id_trip_status').val($('input[name="trip_status"]:checked', '#suspension_reason').val()).trigger('change');
                if (jQuery('input[name="trip_status"]:checked', '#suspension_reason').val() == "No-Show") {

                    $(".suspend-items").hide();
                    $(".no-show-items").show();
                    $("#reason_1").prop('checked', false)
                    $("#reason_10").prop('checked', true)
                }
                else if (jQuery('input[name="trip_status"]:checked', '#suspension_reason').val() == "Suspended") {

                    $(".suspend-items").show();
                    $(".no-show-items").hide();
                    $("#reason_1").prop('checked', true)
                    $("#reason_10").prop('checked', false)
                }
            });

            function showOptions(value) {
                console.log(value);
            }
            if (saveBtn.length) {
                saveBtn[0].setAttribute('type', 'button');
            }
            if (saveAndAddBtn.length) {
                saveAndAddBtn[0].setAttribute('type', 'button');
            }

            $(saveBtn).click(function () {
                var newDriverVal = $('#id_driver').val();
                $('#submitButton').removeClass("").addClass('default');
                $('#submitButton')[0].setAttribute('name', '_save');
                if ($('#id_suspension_trip_reason').length > 0 && initDriverVal && newDriverVal !== initDriverVal) {
                    if (saveBtn.length) {
                        saveBtn[0].setAttribute('type', 'button');
                    }
                    if (saveAndAddBtn.length) {
                        saveAndAddBtn[0].setAttribute('type', 'button');
                    }
                    jQuery("#modalWindow").modal('show');
                    $('#id_suspension_trip_reason').val($('input[name="reason_of_suspension"]:checked', '#suspension_reason').val()).trigger('change');
                } else {
                    saveBtn[0].setAttribute('type', 'submit');
                    saveBtn[0].setAttribute('name', '_save');
                    saveBtn.addClass('default');
                }
            });


            $(saveAndAddBtn).click(function () {
                var newDriverVal = $('#id_driver').val();
                $('#submitButton').removeClass('default').addClass("");
                $('#submitButton')[0].setAttribute('name', '_addanother');
                if ($('#id_suspension_trip_reason').length > 0 && initDriverVal && newDriverVal !== initDriverVal) {
                    if (saveBtn.length) {
                        saveBtn[0].setAttribute('type', 'button');
                    }
                    if (saveAndAddBtn.length) {
                        saveAndAddBtn[0].setAttribute('type', 'button');
                    }
                    jQuery("#modalWindow").modal('show');
                    $('#id_suspension_trip_reason').val($('input[name="reason_of_suspension"]:checked', '#suspension_reason').val()).trigger('change');
                } else {
                    saveAndAddBtn[0].setAttribute('type', 'submit');
                    saveAndAddBtn[0].setAttribute('name', '_addanother');
                    saveAndAddBtn.addClass('default');
                }
            });
            $('#suspension_reason input').on('change', function () {
                $('#id_suspension_trip_reason').val($('input[name="reason_of_suspension"]:checked', '#suspension_reason').val()).trigger('change');
            });

            $("#suspension_reason").on('submit', function () {
                $("#spotorder_form").submit();
                $('.modal.in').modal('hide');
            });
        });
    });

})(django.jQuery);