function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

(function ($) {
    const TIMEOUT_MS = 5000;
    toastr.options.closeButton = true;
    toastr.options.closeDuration = 300;
    toastr.options.closeEasing = 'swing';
    toastr.options.progressBar = true;
    var bookingTemplate = '<tr class="row1" id=%(pk)s>' +
        '<td class="field-number">' +
        '<a href="/admin/order/bookingorder/%(pk)s/change/">' +
        ' %(order_number)s</td>' +
				'<td class="field-tracking_status">%(tracking_status)s</td>' +
        '<td class="field-get_status">%(status)s</td>' +
        '<td class="field-city nowrap">%(city)s</td>' +
        '<td class="field-customer nowrap">%(customer)s</td>' +
        '<td class="field-get_customer_category">%(customer_category)s</td>' +
        '<td class="field-get_customer_phone_number">%(mobile_number)s</td>' +
        '<td class="field-get_pickup_datetime nowrap">%(pickup_datetime)s</td>' +
        '<td class="field-get_pickup_area">%(pickup_area)s</td>' +
        '<td class="field-get_dropoff_area">%(dropoff_area)s</td>' +
        '<td class="field-estimated_distance_km">%(estimated_distance_km)s</td>' +
        '<td class="field-vehicle_class_preference nowrap">%(vehicle_class_preference)s</td>' +
        '<td class="field-get_total_distance">%(total_distance)s</td>' +
        '<td class="field-get_total_time">%(total_time)s</td>' +
        '<td class="field-get_order_total">%(total_incl_tax)s</td>' +
        '<td class="field-link_to_driver">%(driver_name)s</td>' +
        '<td class="field-field-get_vehicle nowrap">%(vehicle)s</td>' +
        '<td class="field-labour_details">%(labour)s</td>' +
        '<td class="field-payment_status">%(payment_status)s</td>' +
        '<td class="field-get_date_placed nowrap">%(date_placed)s</td>' +
        '</tr>';

    var toastHeadTemplate = '<span class="notification-type">%(order_number)s</span>';
    var toastContentTemplate = '<table id="toast-content">' +
        '<tr><th>City:</th><td>%(city)s</td></tr>' +
        '<tr><th>Customer Name:</th><td>%(customer)s</td></tr>' +
        '<tr><th>Mobile:</th><td>%(mobile_number)s</td></tr>' +
        '</table>';

    function updateTable(result) {
        var $tableBody = $('#result_list').find('tbody');
        var rows = $tableBody.find('tr');
        var recordFound = false;
        for (var i = 0; i < rows.length; i++) {
            var orderNumber = rows[i].firstChild;
            if (orderNumber.innerText.trim() === result.order_number.trim()) {
                rows[i].remove();
                recordFound = true;
                break;
            }
        }
        addNewRecord(result, 'flash-new-record');
        if (result.message_type == 'new_booking')
            jQuery.playSound('/static/sound/new_booking.mp3');
        else
            jQuery.playSound('/static/sound/status_update.mp3');
    }

    function addNewRecord(result, flashRowClass) {
        var $tableBody = $('#result_list').find('tbody');
        var $form = $('#changelist-form');
        if ($tableBody.length === 0) {
            var $table = '<div class="results">' +
                '<table id="result_list">' +
                '<thead>' +
                '   <tr>' +
                '      <th scope="col" class="sortable column-number">' +
                '         <div class="text"><a href="?o=0">Order Number</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-status">' +
                '         <div class="text"><a href="?o=1">Status</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-city">' +
                '         <div class="text"><a href="?o=2">City</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-customer">' +
                '         <div class="text"><a href="?o=3">Customer</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '       <th scope="col" class="column-get_customer_category">' +
                '           <div class="text"><span>Customer category</span></div>' +
                '           <div class="clear"></div>' +
                '       </th>' +
                '      <th scope="col" class="sortable column-get_customer_phone_number">' +
                '         <div class="text"><a href="?o=4">Mobile Number</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-device_type">' +
                '         <div class="text"><a href="?o=6">Device Type</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_booking_type">' +
                '          <div class="text"><span>Booking Type</span></div>' +
                '          <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-pickup_address">' +
                '         <div class="text"><a href="?o=5">Pickup Address</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-shipping_address">' +
                '         <div class="text"><a href="?o=6">Drop Off Address</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-get_pickup_datetime">' +
                '         <div class="text"><a href="?o=7">Pick Up Date Time</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_total_distance">' +
                '         <div class="text"><span>Total Distance (in Kms.)</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_total_time">' +
                '         <div class="text"><span>Total Time (Mins.)</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-get_order_total">' +
                '         <div class="text"><a href="?o=10">Order Total(INR)</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-estimated_distance_km">' +
                '       <div class="text"><a href="?o=14">Estimated Distance</a></div>' +
                '       <div class="clear"></div>' +
                '      </th>' +
                '       <th scope="col" class="column-get_pickup_area">' +
                '           <div class="text"><span>Pickup Area</span></div>' +
                '           <div class="clear"></div>' +
                '       </th>' +
                '       <th scope="col" class="column-get_dropoff_area">' +
                '           <div class="text"><span>Dropoff Area</span></div>' +
                '           <div class="clear"></div>' +
                '       </th>' +
                '      <th scope="col" class="sortable column-get_date_placed">' +
                '         <div class="text"><a href="?o=11">Date Placed</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-vehicle_class_preference">' +
                '         <div class="text"><a href="?o=12">Vehicle Class Preference</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_vehicle">' +
                '         <div class="text"><span>Vehicle</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-link_to_driver">' +
                '         <div class="text"><a href="?o=13">Driver</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_cph">' +
                '         <div class="text"><span>CPH</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_labour_count">' +
                '         <div class="text"><span>Labour Count</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '   </tr>' +
                '</thead><tbody></tbody></table>' +
                '</div>';
            $form.prepend($table);
        }
        var $newBooking = sprintf(bookingTemplate, result);
        var $parsedHtml = $.parseHTML($newBooking);
        $tableBody.prepend($parsedHtml);

        var toastHead = sprintf(toastHeadTemplate, result);
        var toastContent = sprintf(toastContentTemplate, result);
        toastr.info(toastContent, toastHead);

        $('#' + result.pk).addClass(flashRowClass);
        setTimeout(function () {
            $('#' + result.pk).removeClass(flashRowClass);
        }, TIMEOUT_MS);
    }

    // function updateTable(result) {
    //     if (result.message_type === messageTypeNew) {
    //         addNewRecord(result, 'flash-new-record');
    //     } else {
    //         updateRecord(result);
    //     }
    // }

    function activateFirebaseListener() {
        firebase.database().ref('admin-bookings').on('child_changed', function (snapshot) {
            var current_booking = snapshot.val();
			console.log(current_booking);
            var isBookingOrderPage = $('body.model-bookingorder').length > 0;
            if (isBookingOrderPage && current_booking) {
                var result = JSON.parse(current_booking.v1);
                console.log('RESULT: ', result);
                city_filter_applied = getParameterByName('city__id__exact');

                // DateRange filter has the date in 20-02-2018 format.
                // Got to reverse it to convert to (YYYY-MM-DD) for comparison
                pickup_gt_filter_applied = getParameterByName('pickup_datetime__gt');
                if (pickup_gt_filter_applied) {
                    pickup_gt_filter_applied = pickup_gt_filter_applied.split('-').reverse().join('-');
                }
                pickup_gte_filter_applied = getParameterByName('pickup_datetime__gte');
                if (pickup_gte_filter_applied) {
                    pickup_gte_filter_applied = pickup_gte_filter_applied.split('-').reverse().join('-');
                }
                pickup_lt_filter_applied = getParameterByName('pickup_datetime__lt');
                if (pickup_lt_filter_applied) {
                    pickup_lt_filter_applied = pickup_lt_filter_applied.split('-').reverse().join('-');
                }
                pickup_lte_filter_applied = getParameterByName('pickup_datetime__lte');
                if (pickup_lte_filter_applied) {
                    pickup_lte_filter_applied = pickup_lte_filter_applied.split('-').reverse().join('-');
                }

                console.log(city_filter_applied);
                console.log(pickup_gte_filter_applied, pickup_lte_filter_applied);
                console.log(pickup_gt_filter_applied, pickup_lt_filter_applied);

                // check if any filters have been applied, the incoming data matches it.
                // result.pickup_datetime_compare is in the format 2018-02-10 (YYYY-MM-DD)
                if (result.order_type === orderTypeBooking &&
                    (!city_filter_applied || city_filter_applied == result.city_id) &&
                    (!pickup_gte_filter_applied || result.pickup_datetime_compare >= pickup_gte_filter_applied) &&
                    (!pickup_gt_filter_applied || result.pickup_datetime_compare > pickup_gt_filter_applied) &&
                    (!pickup_lt_filter_applied || result.pickup_datetime_compare < pickup_lt_filter_applied) &&
                    (!pickup_lte_filter_applied || result.pickup_datetime_compare <= pickup_lte_filter_applied)
                ) {
                    updateTable(result);
                }
            }
        }, function (err) {
            console.log(err);
        });
    }

    function firebaseSignIn(firebase_auth_token) {
        toastr.info("Registering for real-time updates", "Registering");
        firebase.auth().signInWithCustomToken(firebase_auth_token).then(function () {
            toastr.remove();
            toastr.success("Real-time updates are active", "Success");
            activateFirebaseListener();
        }).catch(function (error) {
            console.log(error.message);
            toastr.success("Failed to activate real-time updates." +
                "<br/>Refresh the page again.", "Success");
        });
    }

    function manageLabelNodeList(lableNodes, displayType) {
        if (!lableNodes) {
            return;
        }
        for (var i = 0; i < lableNodes.length; i++) {
            lableNodes[i].style.display = displayType;
        }
    }

    $(document).ready(function () {
        $.ajax({
            type: 'GET',
            url: firebaseAuthTokenUrl,
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                firebaseSignIn(data.firebase_auth_token);
            },
            error: function (data, textStatus, xhr) {
                console.log(data);
            }
        });
        $('.field-pickup_address, .field-shipping_address').removeClass('nowrap');

        $('.actions textarea[name="reason_for_marking_paid"]').hide();
        manageLabelNodeList($('textarea[name="reason_for_marking_paid"').prop('labels'), 'none');
        $('select[name="action"]').on('change', function () {
            var action = $('select[name="action"]').val();
            if (action === 'mark_orders_cash_paid' ||
                action === 'mark_orders_bank_transfer') {
                    $('.actions textarea[name="reason_for_marking_paid"]').show();
                    $('.actions textarea[name="reason_for_marking_paid"]').prop('required', true);
                    manageLabelNodeList($('textarea[name="reason_for_marking_paid"').prop('labels'), 'inline-block');
            } else {
                $('.actions textarea[name="reason_for_marking_paid"]').hide();
                $('.actions textarea[name="reason_for_marking_paid"]').prop('required', false);
                manageLabelNodeList($('textarea[name="reason_for_marking_paid"').prop('labels'), 'none');
            }
        });

        $('#changelist-form').on('submit', function (e) {
            var action = $('select[name="action"]');
            if (action && (action === 'mark_orders_cash_paid' ||
                action === 'mark_orders_bank_transfer') &&
                !$('.actions textarea[name="reason_for_marking_paid"]').val()
            ) {
                alert('Can you provide a reason for marking order as paid from admin?');
                e.preventDefault();
                e.stopPropagation();
            } else {
                $('#changelist-form').submit();
            }
        });
    });

})(django.jQuery);
