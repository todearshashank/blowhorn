const ROUTE_TRACE_MODAL_CONTAINER_ID = 'route-trace-modal-container';
const ROUTE_TRACE_MODAL_ID = 'modal-route-trace';
const TRACE_TRIP = 'trip';
const TRACE_MONGO = 'mongo';
const POLYLINE_COLORS = {
    trip: 'red',
    mongo: 'blue',
};
var pk = null;
var map = null;
var polyline = null;
var routeTrace = {
    mongo: [],
    trip: []
};
var loading = false;

(function ($) {
    $(function () {
        function loadCss(url) {
            var head  = document.getElementsByTagName('head')[0];
            var link  = document.createElement('link');
            link.rel  = 'stylesheet';
            link.type = 'text/css';
            link.media = 'all';
            link.href = url;
            head.appendChild(link);
        }

        function loadScript(url, callback) {
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = url;
            if (callback) {
                script.onreadystatechange = callback;
                script.onload = callback;
            }
            head.appendChild(script);
        }

        function extractPkValue() {
            let pkString = window.location.pathname.replace(/[\D]/g, '');
            try {
                return parseInt(pkString)
            } catch (e) {
                console.error(e, 'Failed to parse pk value from url');
                return null
            }
        }

        function addPolyline(data, options) {
            polyline = L.polyline(data, options)
            if (!map.hasLayer(polyline)) {
                polyline.addTo(map);
            } else {
                polyline.setLatLngs(data);
                polyline.redraw()
            }
            map.fitBounds(polyline.getBounds());
        }

        function buildMap(centerLat, centerLng)  {
            document.getElementById('map-container').innerHTML = "<div id='map' style='width: 100%; height: 100%;'></div>";
            var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                osmAttribution = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,' +
                                ' <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                osmLayer = new L.TileLayer(osmUrl, {attribution: osmAttribution});
            map = new L.Map('map');
            map.setView(new L.LatLng(centerLat, centerLng), 9)
            map.addLayer(osmLayer);
        }

        function processData(data, traceType) {
            console.log('processData', data, traceType);
            switch (traceType) {
                case TRACE_TRIP:
                    for (let i = 0; i < data.length; i++) {
                        routeTrace.trip.push(data[i].map(i => [i.lat, i.lng]));
                    }
                    console.log(routeTrace.trip);
                    break;
                
                case TRACE_MONGO:
                    var latlngs = data.map(i => [i.locationInfo.mLatitude, i.locationInfo.mLongitude])
                    routeTrace.mongo = [latlngs];
                    break;
                
                default:
                    console.error('Unknown trace type: ', traceType);
            }
            addPolyline(routeTrace[traceType], {color: POLYLINE_COLORS[traceType], weight: 5});
        }

        function fetchData(traceType, pk) {
            if (routeTrace[traceType].length) {
                addPolyline(routeTrace[traceType], {color: POLYLINE_COLORS[traceType]});
                return
            }
            if (loading) {
                return;
            }
            loading = true;
            jQuery('.message').hide()
            jQuery('.loader').show();
            jQuery('#map-container').hide()
            let url = traceType === TRACE_TRIP ? `/api/trip/route-trace?trip_id=${pk}` : `/api/trip/route-trace/mongo?trip_id=${pk}`;
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                    loading = false;
                    console.log(data)
                    jQuery('.loader').hide();
                    if (data.length) {
                        jQuery('#map-container').show()
                        processData(data, traceType)
                    } else {
                        jQuery('.message').show()
                    }
                },
                error: function (data, textStatus, xhr) {
                    loading = false;
                    console.log(data, textStatus, xhr);
                }
            });
        }

        function initModalListener() {
            jQuery(`#${ROUTE_TRACE_MODAL_ID}`).on('shown.bs.modal', function () {
                map = null;
                polyline = null;
                routeTrace = {
                    mongo: [],
                    trip: []
                };
                console.log('modal opened..');
                buildMap(12.5666, 77.5666);
                $('#routeTraceType1').attr('checked', 'checked');
                if (pk) {
                    fetchData(TRACE_TRIP, pk);
                }
                $('input[type=radio][name=routeTraceTypes]').on('change', function(e) {
                    console.log(e.currentTarget.value)
                    switch (e.currentTarget.value) {
                        case TRACE_TRIP:
                            fetchData(TRACE_TRIP, pk);
                            break;

                        case TRACE_MONGO:
                            fetchData(TRACE_MONGO, pk);
                            break;
                    }
                });
            })
        }

        $('#content-main #changelist-form, #content-main > form').append(`<div id="${ROUTE_TRACE_MODAL_CONTAINER_ID}"></div>`);
        $(`#${ROUTE_TRACE_MODAL_CONTAINER_ID}`).load('/static/admin/html/trip/route_trace.html', initModalListener);

        $(document).on('ready', function () {
            pk = extractPkValue();
            if (!pk || pk === NaN) {
                console.error('PK value not present in url.');
                return;
            }
            loadCss('https://unpkg.com/leaflet@1.7.1/dist/leaflet.css')
            loadScript('https://unpkg.com/leaflet@1.7.1/dist/leaflet.js')
            li = document.createElement('li');
            li.id = 'btn-track';
            input = document.createElement('input');
            input.id = 'btn-track-live';
            input.type = 'button';
            input.classList.add('default');
            input.value = 'Route Trace';
            li.append(input);
            let objectTools = $('.object-tools')
            if (objectTools.length) {
                objectTools[0].prepend(li);
            }

            input.addEventListener('click', function () {
                jQuery('#' + ROUTE_TRACE_MODAL_ID).modal('show');
            });
        })
    });
})(django.jQuery);
