$ = django.jQuery;
$(document).ready(function () {
    $('select[name="contract"]').prop('disabled', 'disabled');
    $('select[name="action"]').on('change', function () {
        var action = $('select[name="action"]').val();
        switch (action) {
            case 'mark_trip_as_new':
                $('select[name="contract"]').prop('disabled', 'disabled');
                break;

            case 'update_trip_detail':
                $('select[name="contract"]').prop('disabled', false);
                break;

            default:
                console.log('No actions');
        }
    });
});
