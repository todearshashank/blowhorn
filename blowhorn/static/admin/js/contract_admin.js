/*
Contains custom javascript function codes used in contract admin
*/

(function($) {
    $(function() {
        var url = window.location.href;
        service_schedule = $('.field-service_schedule');
        next_30_day_occurrences = $('.field-next_30_day_occurrences'); 

        function toggleHide(value) {
            if (value == 'Fixed') {
                service_schedule.show() && next_30_day_occurrences.show()
            } else if (value == 'Spot') {
                service_schedule.hide() && next_30_day_occurrences.hide()
            } else if (value == 'Shipment') {
                service_schedule.show() && next_30_day_occurrences.show()
            }
        }

        // for edit page id is not present for contract type
        var spil = url.search("change");
        selectField = $('.field-contract_type > div > p');

        if (spil != -1 && selectField.length > 0) {
            value = selectField[0].innerHTML;
        } else {
            selectField = $('#id_contract_type');
            value = selectField.val();
        }

        // show / hide on load based on previous value of selectField
        toggleHide(value);

        // show / hide on change
        selectField.change(function () {
            toggleHide($(this).val());
        })

        // Contract Term Inline
        var original_inlines = $('.has_original');
        var total_inlines = original_inlines.length;
        if (total_inlines) {
            var latest_inline_id = original_inlines[total_inlines - 1].id;
            var buy_rate_pk = $('#' + latest_inline_id + ' > .field-buy_rate input:hidden').val();
            if (buy_rate_pk) {
                $('#contractterm_set-empty > .field-buy_rate input:hidden').val(buy_rate_pk);
                $('#contractterm_set-empty > .field-buy_rate a').html(buy_rate_pk);
            }
            var sell_rate_pk = $('#' + latest_inline_id + ' > .field-sell_rate input:hidden').val();
            if (sell_rate_pk) {
                $('#contractterm_set-empty > .field-sell_rate input:hidden').val(sell_rate_pk);
                $('#contractterm_set-empty > .field-sell_rate a').html(sell_rate_pk);
            }
        }
    });
})(django.jQuery);
