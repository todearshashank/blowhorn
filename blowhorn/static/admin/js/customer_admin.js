/*
contains all the  custom javascript functionality functions
*/

(function($) {$(function() {
    var service_tax_category= $('.field-service_tax_category')

        function enableTaxInfo() {
            value= $('#id_is_tax_registered').prop('checked')
            !value ? service_tax_category.hide(): service_tax_category.show()

        }

        enableTaxInfo()

        // show / hide on click
        $('#id_is_tax_registered').on('change', function(e) {
            enableTaxInfo()
        })
})
})(django.jQuery);
