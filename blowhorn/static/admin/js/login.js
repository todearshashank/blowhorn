document.addEventListener("DOMContentLoaded", function(event) {
    $username = document.getElementById("id_username");
    $username.className = 'form-control';
    $password = document.getElementById("id_password");
    $password.className = 'form-control';

    var currentSite = document.querySelector("meta[property='og:site_name']").getAttribute("content");
    $logo = document.getElementById('logo');
    $logo.src = `/static/website/images/${currentSite}/logo/logo.png`;
});
