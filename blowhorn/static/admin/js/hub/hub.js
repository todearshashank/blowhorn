$ = jQuery;
var qrCodeFilenamePrefix = "qr_code";
var filename = qrCodeFilenamePrefix;

(function($) {
    $(document).ready(function() {
        $("body").append('<div id="qr-card-wrap"></div>');
        $("#qr-card-wrap").load("/static/admin/html/hub/qr_code.html");
    });
})(django.jQuery);


function saveAs(uri, filename) {
    var link = document.createElement("a");
    if (typeof link.download === "string") {
        link.href = uri;
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    } else {
        window.open(uri);
    }
}

function base64MimeType(encoded) {
  var result = 'png';

  if (typeof encoded !== 'string') {
      return result;
  }

  var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
  if (mime && mime.length) {
      var ext = mime[1];
      ext_array = ext.split('/');
      if (ext_array.length > 1) {
          result = ext_array[1];
      }
  }
  return result;
}

function downloadCard() {
    domtoimage.toJpeg(document.getElementById('qr-code-wrapper'), { quality: 0.95 })
    .then(function (dataUrl) {
        saveAs(dataUrl, [filename, base64MimeType(dataUrl)].join('.'));
    });
}

function showQrCode(qr_code_str, hub_name, city_name, customer_name) {
    $("#cardQr").html("");
    $("#cardQr").qrcode(qr_code_str);

    $("#customer-name").html(customer_name);
    $("#hub-name").html(hub_name);
    $("#city-name").html(city_name);

    // QR code file
    var date_repr = new Date().toISOString().replace(/-/g, '_').replace(/:/g,'_').replace(/ /g, '');
    filename += [hub_name, customer_name, city_name, date_repr].join('_');

    $("#modal-qr-code").modal({ backdrop: "static", keyboard: false });
    $("#modal-qr-code").modal("show");
}
