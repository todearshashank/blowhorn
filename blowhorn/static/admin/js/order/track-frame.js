function openTracking(order_number) {
    // If order number is present, assign that
    // if not (when clicked from detailed view) orderNumber is already set to the
    // desired value
    if (order_number) {
        orderNumber = order_number;
    }
    window.open(getTrackingUrl(), '_blank');
    // jQuery("#track-modal").modal('show');
}

function getTrackingUrl() {
    return "/track/" + orderNumber;
}

(function ($) {
    var ifrm = document.createElement("iframe");


    function initIframe() {
        ifrm.setAttribute("src", getTrackingUrl());
        // ifrm.setAttribute("height", $(window).height() * 0.7);
        // ifrm.style.width = "98%";
        // ifrm.style.height = $(window).height();
        ifrm.frameBorder = "0";
        // $('.field-get_tracking_url').append(ifrm);
        $('#tracking-modal').append(ifrm);
    }

    $(document).ready(function () {
        var html =  '<div id="track-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
        html += '<div class="modal-dialog" style="width:100%;">';
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<a class="close" data-dismiss="modal">×</a>';
        html += '</div>';
        html += '<div class="modal-body" id="tracking-modal"></div>';
        html += '</div>';
        // $('#content-main').append(html);
        $('body').append(html);

        /*
        var $status = $('.field-get_status');
        for (var i = 0; i < $status.length; i++) {
            $status[i].addEventListener('click', function () {
                orderNumber = this.previousSibling.innerText;
                openTracking();
            });
        }
        */

        if (window.location.pathname !== "/admin/order/bookingorder/") {
            li = document.createElement("li");
            li.id = 'btn-track';
            input = document.createElement("input");
            input.id = 'btn-track-live';
            input.type = 'button';
            input.classList.add('btn-info');
            input.value = 'Track Live';
            li.append(input);
            $(".object-tools")[0].prepend(li);

            input.addEventListener('click', function () {
                window.open(getTrackingUrl(), '_blank');
            });
        }

        jQuery("#track-modal").on('show.bs.modal', function () {
            initIframe();
        });
    });
})(django.jQuery);
