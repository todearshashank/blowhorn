(function ($) {
    $(function () {
        let cancellationReasons = [];
        try {
            cancellationReasons = JSON.parse(cancelReasons.replace(/\'/g, '"'));
        } catch {
            cancellationReasons = [];
        }

        var cancleBtn = $('#bookingorder_form > div > div.submit-row > input.default.transition-cancel');
        var html = '<div id="modalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
        html += '<div class="modal-dialog">';
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<a class="close" data-dismiss="modal">×</a>';
        html += '<h4> Reason for Cancellation</h4>';
        html += '</div>';
        html += '<div class="modal-body">';
        html += '<form id="cancellation_reason">';
        for (let i = 0; i < cancellationReasons.length; i++) {
            html += '<input type="radio" name="reason_of_cancellation" value="' + cancellationReasons[i] + '"> ' + cancellationReasons[i] + '<br>';
        }
        html += '</form>';
        html += '</div>';
        html += '<div class="modal-footer">';
        html += '<input class="default transition-cancel" name="_fsmtransition-status-cancel" type="submit" value="Submit">';
        html += '</div>';  // footer
        html += '</div>';  // modalWindow
        $("#bookingorder_form").append(html);

        var reason_for_cancellation_block = jQuery('.field-reason_for_cancellation');
        var reason_for_cancellation_block_after_submit = jQuery('.field-reason_for_cancellation > div > p');
        if (reason_for_cancellation_block_after_submit.length > 0) {
            reason_for_cancellation_block.show();
        } else {
            reason_for_cancellation_block.hide();
        }

        $(document).ready(function () {
            cancleBtn.addClass("dummy_class")
            cancleBtn.removeClass('transition-cancel');
            if (cancleBtn[0]) {
                cancleBtn[0].removeAttribute('name');
                cancleBtn[0].setAttribute('type', 'button');
            }
            $(".dummy_class").click(function () {
                jQuery("#modalWindow").modal('show');
                $('#id_reason_for_cancellation').val($('input[name="reason_of_cancellation"]:checked', '#cancellation_reason').val()).trigger('change');
            });

            $('#cancellation_reason input').on('change', function () {
                $('#id_reason_for_cancellation').val($('input[name="reason_of_cancellation"]:checked', '#cancellation_reason').val()).trigger('change');
            });

            $("#cancellation_reason").on('submit', function () {
                $("#bookingorder_form").submit();
                $('.modal.in').modal('hide');
            });
        });
    });
})(django.jQuery);
