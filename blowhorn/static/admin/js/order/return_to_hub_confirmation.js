(function($) {$(function() {

    var return_to_hub_button = jQuery('.transition-return_to_hub');
    var driver_action = jQuery('#changelist-form > div.changelist-footer.fixed > div.actions.initialized > label:nth-child(3)')
    var action = jQuery('#changelist-form > div.changelist-footer.fixed > div.actions.initialized > label:nth-child(1)');
    var action_selected = jQuery('#changelist-form > div.changelist-footer.fixed > div.actions.initialized > label:nth-child(1) > span > span.selection > span');

    var html =  '<div id="modalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog" style="width:450px;">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '<h4> Return to Hub shipment order??</h4>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<p id="shipment_order_state">';
    html += 'Click Confirm to continue or Cancel to abort<br>';
    html += '</p>';
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    html += '<input id="submitButton" class="default transition-return_to_hub pull-right" name="_fsmtransition-status-return_to_hub" type="submit" value="Confirm">';
    html += '</div>';  // footer
    html += '</div>';  // modalWindow

    $("#shipmentorder_form").append(html);

    $(document).ready(function(){
        return_to_hub_button.addClass("return_to_hub_class").removeClass('transition-return_to_hub');

        if(return_to_hub_button[0]) {
            return_to_hub_button[0].removeAttribute('name');
            return_to_hub_button[0].setAttribute('type', 'button');
        }

        $(".return_to_hub_class").click(function() {
            jQuery("#modalWindow").modal('show');
            $('h4').text('Return to Hub shipment order?');
            $('#submitButton').removeClass('transition-deactivate').addClass('transition-return_to_hub');
            $('#submitButton')[0].setAttribute('name', '_fsmtransition-status-return_to_hub');
        });
        
        $("#shipment_order_state").on('submit', function() {
            $("#shipmentorder_form").submit();
            $('.modal.in').modal('hide');
        });
    });    

});
})(django.jQuery);