(function($) {$(function() {

    var make_dummy_button = jQuery('.transition-mark_dummy');


    var bookingorder_form = jQuery('#bookingorder_form');
    var spotorder_form = jQuery('#spotorder_form');
    var fixedorder_form = jQuery('#fixedorder_form');

    if (bookingorder_form.length) {
        working_form = '#bookingorder_form'
    }
    else if (spotorder_form.length) {
        working_form = '#spotorder_form'
    }
    else {
        working_form = '#fixedorder_form'
    }

    var html =  '<div id="dummyModalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog" style="width:450px;">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '<h4>Make the order Dummy?</h4>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<p id="shipment_order_state">';
    html += 'Click Confirm to continue or Cancel to abort<br>';
    html += '</p>';
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    html += '<input id="submitButton" class="default transition-mark_dummy pull-right" name="_fsmtransition-status-mark_dummy" type="submit" value="Confirm">';
    html += '</div>';  // footer
    html += '</div>';  // modalWindow


    $(working_form).append(html);

    $(document).ready(function(){
        make_dummy_button.addClass("make_dummy_class").removeClass('transition-mark_dummy');

        if(make_dummy_button.length) {
            make_dummy_button[0].removeAttribute('name');
            make_dummy_button[0].setAttribute('type', 'button');
        }

        $(".make_dummy_class").click(function() {
            jQuery("#dummyModalWindow").modal('show');
        });
        
        $("#shipment_order_state").on('submit', function() {
            $(working_form).submit();
            $('.modal.in').modal('hide');
        });
    });    

});
})(django.jQuery);