(function($) {$(function() {

    var contract_key = $('#id_customer_contract');
    var city_key = $('.field-city > div > p');
    var vehicle_class_preference = $('#id_vehicle_class_preference');

    function contract_filter_ajax(contract_param, first_value) {
        contract_key.empty().trigger('change');
        var option = new Option("---------", "", true, true);
        contract_key.append(option).trigger('change');

        $.ajax({
           type: 'GET',
           url: '/api/contract/filter',
           data: contract_param,
           contentType: "application/json; charset=utf-8",
           dataType: 'json',
           responseType: 'arraybuffer',
           success: function(response) {
                $.each(response, function(key, value) {
                    if (key == 0 & first_value) {
                      first_value_name = value.name
                      sp = $('#select2-id_customer_contract-container');
                      sp.text(first_value_name);
                    }
                    var option = new Option(value.name, value.id, false, false);
                    contract_key.append(option).trigger('change');
                });
                return first_value_name
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                sp = $('#select2-id_customer_contract-container');
                sp.text('-------');
            }
        });
    }

    $(document).ready(function () {

        if (typeof city_key != undefined) {
          var params = {
            "city_name": city_key.text(),
            "order_type": 'booking',
            "vehicle_class_preference": vehicle_class_preference.val()
          }
          contract_filter_ajax(params, false);

          vehicle_class_preference.on('change', function () {
            var params = {
              "city_name": city_key.text(),
              "order_type": 'booking',
              "vehicle_class_preference": vehicle_class_preference.val()
            }
            response = contract_filter_ajax(params, true);
          });
        }
    });
});
})(django.jQuery);
