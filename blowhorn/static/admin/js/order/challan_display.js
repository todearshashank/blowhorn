$(document).ready(function () {
            li = document.createElement("li");
            li.id = 'btn-challan';
            input = document.createElement("input");
            input.id = 'btn-challan-display';
            input.type = 'button';
            input.classList.add('btn-info');
            input.value = 'Download Challan';
            li.append(input);
            $(".object-tools")[0].prepend(li);
            input.addEventListener('click', function () {
                window.open(`/api/order/${orderNumber}/challan`, '_blank');
            });
})