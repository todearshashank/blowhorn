const PREFIX_EMPTY_ROW = "__prefix__";
const PREFIX_OPTION_ID = "#option_";
const ATTRIBUTE_TYPE = "type";
const ATTRIBUTE_TEXT = "text";
const ATTRIBUTE_DISABLED = "disabled";
const DATE_INPUT_CLASS = "bDate";
const DATE_FORMAT_REPLACE_STR = "$3-$2-$1";

var DateTimeShortcuts = {
    dateRegex: /^(\d{2})[./](\d{2})[./](\d{4})$/,
    dateTimeRegex: /^(\d{2})[./](\d{2})[./](\d{4}) [0-9]{2}\:[0-9]{2}$/,
    timeRegex: /^[0-9]{2}\:[0-9]{2}$/,
    init: function() {
        var inputs = document.getElementsByClassName(DATE_INPUT_CLASS);
        for (var i = 0; i < inputs.length; i++) {
            var inp = inputs[i];
            var inputId = inp.id;

            if (inp.getAttribute(ATTRIBUTE_TYPE) === ATTRIBUTE_TEXT
                && !inp.hasAttribute(ATTRIBUTE_DISABLED) &&
                inputId.indexOf(PREFIX_EMPTY_ROW) === -1) {

                var options = JSON.parse($(PREFIX_OPTION_ID + inputId)[0].innerHTML);
                for (var k in options) {
                    var op = options[k];
                    if (DateTimeShortcuts.dateRegex.test(op) ||
                        DateTimeShortcuts.dateTimeRegex.test(op) ||
                        DateTimeShortcuts.timeRegex.test(op)) {
                        // Date string will not work in Firefox. So need to replace.
                        op = op.replace(DateTimeShortcuts.dateRegex, DATE_FORMAT_REPLACE_STR);
                        options[k] = new Date(op);
                    }
                }
                // ID will be 'id_<field_name>'
                var picker_class = inp.id.slice(3, inp.id.length);
                $('.field-' + picker_class).css({'overflow': 'inherit'});
                $(inp).datetimepicker(options);
            }
        }
    }
};

function loadScript(url, callback) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    if (callback) {
        script.onreadystatechange = callback;
        script.onload = callback;
    }
    head.appendChild(script);
}

window.addEventListener('load', function () {
    loadScript('/static/website/js/lib/bootstrap-datetimepicker.min.js', DateTimeShortcuts.init);
});

window.DateTimeShortcuts = DateTimeShortcuts;
