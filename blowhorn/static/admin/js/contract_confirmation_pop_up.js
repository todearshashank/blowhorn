$$ = jQuery;
(function($) {
    $(document).ready(function () {
        var btn_fsm_activate = $$('.transition-activate');
        var btn_fsm_deactivate = $$('.transition-deactivate');
        var total_buttons = 0;
        var buttons = null;

        // Load modal template
        $("#content-main > form").append('<div id="confirmation-modal-container"></div>');
        $("#confirmation-modal-container").load("/static/admin/html/contract/confirm_status.html");

        if (btn_fsm_activate.length) {
            btn_fsm_activate.addClass("activate-contract")
                .removeClass('transition-activate');
            total_buttons = btn_fsm_activate.length;
            buttons = btn_fsm_activate;
        } else {
            btn_fsm_deactivate.addClass("deactivate-contract")
                .removeClass('transition-deactivate');
            total_buttons = btn_fsm_deactivate.length;
            buttons = btn_fsm_deactivate;
        }

        for (var count = 0; count < total_buttons; count++) {
            buttons[count].removeAttribute('name');
            buttons[count].setAttribute('type', 'button');
        }

        function modifyBtnAttr(fsm_action) {
            $$("#fsm-action").text(fsm_action);
            $$('#btn-fsm-submit').addClass('transition-' + fsm_action);
            $$('#btn-fsm-submit').attr('name', '_fsmtransition-status-' + fsm_action);
        }

        $(".deactivate-contract").click(function() {
            $$("#modal-fsm-confirm").modal('show');
            modifyBtnAttr('deactivate')
        });

        $(".activate-contract").click(function() {
            $$("#modal-fsm-confirm").modal('show');
            modifyBtnAttr('activate')
        });
    });
})(django.jQuery);
