(function($){
    
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = "https://d3js.org/d3.v4.js";
        head.appendChild(script);
    
    $(document).ready(function() {
        $("body").append('<div id="graphone"></div>');
        $("#graphone").load("/static/admin/html/contract/marginGraph.html",initCustomListener);
        
        
    });
})(django.jQuery);
    
var contractId = null;

function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}

function initCustomListener(){
    
    $("#num_trips").keyup(function(){
        $("#filter-inputs").html('');
        for (num = 1; num <= $("#num_trips").val(); num++) {
            console.log(num);
            var distInputId = "dist" + num;
            var timeInputId = "time" + num;
            $("#filter-inputs").append('<div style="border: 2px solid #205f62;background-color: #b3f8f7;" id="box'+ num +
                '"><center><input value="0" type="range" min="0" max="150" name="'+
                 distInputId +'" placeholder="distance in km per day"><p>Dist in <span></span>km</p> <input type="range" value="0" min="0" max="1440" name="'+
                 timeInputId +'" placeholder="time in minutes per day"><p>Time in <span></span>minutes</p></center></div>');
            initDistListener(distInputId);
            initTimeListener(timeInputId);
        };
        if($("#number_of_trips")!=null){
            $("#filter-inputs").append('<div id="leave_box"><center><input type="range" id="leave_num" name="leave_num" placeholder="leave days per month" min="0" max="30"><p>Leave of <span></span>days per month</p></center></div>');
            $('input[name=leave_num]').on('change', function(){
                $('input[name=leave_num]+p>span').text(this.value.toString());
            });
        }
        
    });
    $("#num_trips").keypress(function(key){
        if(key.charCode < 49 || key.charCode > 51) return false;
        
    });
    $("#fetch_data").on('click',function(){
        formdata = $('#filterForm').serializeArray();
        console.log("Data Fetching Done");
        $.ajax({
            url: "/api/margin-calc",
            type: 'POST',
            data: formdata,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
            },
            success: function (data, textStatus, jqXHR) {
                $('#result_margin>span').text(data);
                dist_per_month = 0;
                time_per_month = 0;
                dist_per_day = 0;
                time_per_day = 0;
                for(var i=0;i<formdata.length;i++){
                    if(formdata[i].name.startsWith('dist')){
                        dist_per_month+=parseInt(formdata[i].value);
                    };
                };
                for(var i=0;i<formdata.length;i++){
                    if(formdata[i].name.startsWith('dist')){
                        dist_per_day += parseInt(formdata[i].value);
                    };
                };
                for(var i=0;i<formdata.length;i++){
                    if(formdata[i].name.startsWith('time')){
                        time_per_month+=parseInt(formdata[i].value);
                    };
                };
                for(var i=0;i<formdata.length;i++){
                    if(formdata[i].name.startsWith('time')){
                        time_per_day += parseInt(formdata[i].value);
                    };
                };
                $('#distance_month>span').text(dist_per_month*(30-$('input[name=leave_num]').val()));
                $('#time_month>span').text(time_per_month*(30-$('input[name=leave_num]').val())/60.0);
                $('#distance_day>span').text(dist_per_day);
                $('#time_day>span').text(roundToTwo(time_per_day/60.0));
            },
            error: function (jqXHR, textStatus, exception) {
                alert(jqXHR.responseText);
            }
        });
    });
}

function initDistListener(id) {
    $('input[name='+ id +']').on('change', function ()  {
        console.log('this', this.value, $('input[name='+id+']+p>span'));
        $('input[name='+id+']+p>span').text(this.value.toString());
    });
}

function initTimeListener(id){
    $('input[name='+ id +']').on('change', function(){
        console.log('this', this.value, $('input[name='+id+']+p>span'));
        $('input[name='+ id +']+p>span').text(this.value.toString());
    });
}

function toShowGraph(contract_id){
    $("#contract_id").val(contract_id);
    console.log('contract_id: ', contract_id);
    $("#graphdisplay").modal({ backdrop: "static", keyboard: false });
    $("#graphdisplay").modal("show");
}
function getCookieValue(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    var csrf = $("input[name='csrfmiddlewaretoken']").val();
    return cookieValue || csrf;
}

