(function($){
      
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = "https://d3js.org/d3.v4.js";
        head.appendChild(script);
        
    $(document).ready(function() {
        $("body").append('<div id="graphtwo"></div>');
        $("#graphtwo").load("/static/admin/html/contract/margin_dataset_graph.html", initListener);   
    });
})(django.jQuery);

function initListener() {
    $("#dataset_form #fetch_data").on('click',function(){
        console.log("response from the on_click function");
        fetchData(contract_id);
    });
}
function getCookieValue(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    var csrf = $("input[name='csrfmiddlewaretoken']").val();
    return cookieValue || csrf;
}

function datasetGraph(contract_id){

    console.log('contract_id: ', contract_id);
    $("#dataset_form #contract_id").val(contract_id);
    console.log("contract id is ",contract_id);
    
    fetchData(contract_id);
}

function fetchData(contract_id){
    data = $('#dataset_form').serializeArray();
    console.log("Data Fetching init", data);
    $.ajax({
        url: "/api/margin-data-calc",
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", getCookieValue('csrftoken'));
        },
        success: function (data, textStatus, jqXHR) {
            // draw graph
            showGraph(data);
            $("#datasetgraphdisplay").modal({ backdrop: "static", keyboard: false });
            $("#datasetgraphdisplay").modal("show");

        },
        error: function (jqXHR, textStatus, exception) {
            alert(jqXHR.responseText);
        }
    });
}



function showGraph(
        data
    ){
        var time = data['time'];
        var onetripmargin = data['onetripmargin']
        var twotripmargin = data['twotripmargins']
        var threetripmargin = data['threetripmargins']
        $('#for_graph').html('');
        console.log(time, onetripmargin, twotripmargin, threetripmargin);
        var margin = {
            top: 30,
            right: 50,
            bottom: 50,
            left: 50
        },
        width = 500 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;
        // append the svg object to the body of the page
        var svg = d3.select("#for_graph")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

    // Initialise a X axis on margin graph:
    var x = d3.scaleLinear().range([0,width]);
    var xAxis = d3.axisBottom().scale(x);
    svg.append("text")
      //.attr("transform", "translate(0," + height + ")")
      .attr("transform", "translate(90,460)")
      .attr("class","myXaxis")
      .text("Time with interval of 30 minutes");

    // Initialize an Y axis on margin graph
    var y = d3.scaleLinear().range([height, 0]);
    var yAxis = d3.axisLeft().scale(y);
    svg.append("text")
      .attr("class","myYaxis")
      .text("Margin per Month");

    data = [];
    for (i = 0; i < time.length; i++) {
        data.push({dist: time[i], margin: onetripmargin[i]});
    }
    var x = d3.scaleLinear()
    .domain([d3.min(time), d3.max(time) + 1])
    //.domain([0,120])
    .range([ 0, width ]);

    svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

    var y = d3.scaleLinear()
    .domain([-50,70])
    .range([ height, 0 ]);

    svg.append("g")
    .call(d3.axisLeft(y));

    var bisect = d3.bisector(function (d) {
        return d.dist;
    }).left;

    var focus = svg
    .append('g')
    .append('circle')
    .style("fill", "none")
    .attr("stroke", "black")
    .attr('r', 8.5)
    .style("opacity", 0)

    var focusText = svg
    .append('g')
    .append('text')
    .style("opacity", 0)
    .attr("text-anchor", "left")
    .attr("alignment-baseline", "middle")

    // Create a rect on top of the svg area: this rectangle recovers mouse position
    svg
    .append('rect')
    .style("fill", "none")
    .style("pointer-events", "all")
    .attr('width', width)
    .attr('height', height)
    .on('mouseover', mouseover)
    .on('mousemove', mousemove)
    .on('mouseout', mouseout);
   
    svg
    .append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-width", 1.5)
    .attr(
        "d", d3.line()
        .x(function (d) { return x(d.dist) })
        .y(function (d) { return y(d.margin) })
    )

    function mouseover() {
        focus.style("opacity", 1)
        focusText.style("opacity",1)
        }
    function mouseout() {
        focus.style("opacity", 0)
        focusText.style("opacity", 0)
        }
    function mousemove() {
        // recover coordinate we need
        var x0 = x.invert(d3.mouse(this)[0]);
        var i = bisect(data, x0, 1);
        selectedData = data[i];
        focus
            .attr("cx", x(selectedData.dist))
            .attr("cy", y(selectedData.margin))
        focusText
            .html("time:" + selectedData.dist + "  -  " + "margin:" + selectedData.margin)
            .attr("x", x(selectedData.dist)+15)
            .attr("y", y(selectedData.margin))
        }
                                
        
    }
