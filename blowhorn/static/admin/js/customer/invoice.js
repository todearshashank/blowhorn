
var recipients = [];
function removeAdditionalRecipients(e) {
    var $parentElem = e.target.parentElement;
    recipients.splice(recipients.indexOf($parentElem.id.trim()), 1);
    $parentElem.remove();
    $('input[name="cc_email"]').val(recipients.join(','));
}

function appendHTML(parentElemId, sourceArray) {
    var parentElem = $('#' + parentElemId);
    parentElem.html('');
    for (var i in sourceArray) {
        var currentRow = "<div class='row'>" + sourceArray[i] + "</div>";
        parentElem.append(currentRow);
    }
}

function askConfirmation() {
    $('#email-form-container').fadeOut();
    $('#confirmation-block').fadeIn();
    var rec_contacts = [];
    $("#id-to_email_to option").each(function () {
        var val = $(this).val();
        if (rec_contacts.indexOf(val) === -1) {
            rec_contacts.push(val);
        }
    });
    var recipient_str = rec_contacts.join(',');
    $('input[name="to_email"]').val(recipient_str);
    $('input[name="email"]').val(recipient_str);

    // Display in confirmation screen
    appendHTML('confirm-recipients', rec_contacts);
    appendHTML('confirm-cc-emails', recipients);
}

function cancelConfirmation() {
    $('#confirmation-block').fadeOut();
    $('#email-form-container').fadeIn();
}

(function ($) {
    $(function () {

    function loadCss(url) {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.media = 'all';
        link.href = url;
        head.appendChild(link);
    }

    function loadScript(url, callback) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        if (callback) {
            script.onreadystatechange = callback;
            script.onload = callback;
        }
        head.appendChild(script);
    }

    var initialDateInputValues = {};
    var regex_fsmtransition = /_fsmtransition/;
    var submitButtons = $('input[type="submit"]');
    var sent_to_customer = jQuery('.transition-sent_to_customer');
    var approve = jQuery('.transition-approve');
    var reject = jQuery('.transition-reject');
    var unapprove = jQuery('.transition-unapprove');
    var confirmed = jQuery('.transition-customer_confirmed');
    var disputed = jQuery('.transition-customer_disputed');
    var regenerate = jQuery('.transition-regenerate');
    var send_invoice_mail = jQuery('.transition-send_invoice');
    var resend_invoice_mail = jQuery('.transition-resend');
    var send_for_approval = jQuery('.transition-send_for_approval');
    var reassign = jQuery('.transition-reassign');
    var customer_acknowledged = jQuery('.transition-customer_acknowledged');
    var regenerate_disputed_invoice = jQuery('.transition-regenerate_disputed_invoice');
    var generate_credit_debit_note = jQuery('.transition-generate_credit_debit_note');
    var sales_reject = jQuery('.transition-sales_reject');
    var action_go = jQuery('.actions button');

    const BTN_TEXT_SEND_TO_CUSTOMER = 'send to customer';
    const BTN_TEXT_RESEND_TO_CUSTOMER = 'resend to customer';
    var btnSendInvoice = jQuery('.transition-send_invoice');
    var btnResendInvoice = jQuery('.transition-resend');
    var selectedInvoice = null;

    function storeDatetimeInputs() {
        var dateInputs = $('.hasDatepicker');
        for (var i in dateInputs) {
            if (dateInputs[i].id) {
                initialDateInputValues[dateInputs[i].id] = dateInputs[i].value;
            }
        }
    }

    function disableFsmButtons() {
        var submitButtons = $('input[type="submit"]')
        for (var index in submitButtons) {
            var btnName = submitButtons[index].name;
            if (btnName == '_fsmtransition-status-customer_confirmed' || btnName == '_fsmtransition-status-unapprove' ||
                btnName == '_fsmtransition-status-send_invoice' || btnName == '_fsmtransition-status-customer_disputed' ||
                btnName == '_fsmtransition-status-approve' || btnName == '_fsmtransition-status-send_for_approval' ||
                btnName == '_fsmtransition-status-reject' || btnName == '_fsmtransition-status-reassign' ||
                btnName == '_fsmtransition-status-customer_acknowledged' ||
                btnName == '_fsmtransition-status-regenerate_disputed_invoice' ||
                btnName == '_fsmtransition-status-generate_credit_debit_note' ||
                btnName == '_fsmtransition-status-sales_reject' ||
                btnName == '_fsmtransition-status-resend'){
                continue;
            }

            if (btnName && btnName.includes('_fsmtransition')||
                regex_fsmtransition.test(btnName)) {
                $('input[name="' + btnName + '"]').prop('disabled', true);
            }
        }
    }

    function ValidateDate(dueDate, e) {
        var CurrentDate = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate());
        var splitDate = dueDate.split("-");
        var choosenDueDate = new Date(splitDate[2], splitDate[1] - 1, splitDate[0]);
        if (CurrentDate && choosenDueDate && CurrentDate.getTime() > choosenDueDate.getTime()){
            alert('Due Date cannot be past..');
            e.preventDefault();
        }
    }

    function valueChanged() {
        var dateValuesDuringSubmit = {};
        var dateInputs = $('.hasDatepicker');
        for (var i in dateInputs) {
            if (initialDateInputValues[dateInputs[i].id] !== dateInputs[i].value) {
                return true;
            }
        }
        return false;
    }

    function getUrlParameters(parameter, staticURL) {
        var currLocation = (staticURL.length) ? staticURL : document.URL;
        var parser = document.createElement('a');
        parser.href = currLocation;
        if (parser.pathname.match(parameter)) {
            return true;
        }
        else {
            return false;
        }
    }

    function extractUrlValue(key, url) {
        if (typeof(url) === 'undefined') {
            url = window.location.href;
        }
        var match = url.match('[?&]' + key + '=([^&#]+)');
        return match ? match[1] : false;
    }

    var html = '<div id="ConfirmationmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog" style="width:450px;">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">File upload form</h4> </div>';
    html += '<div class="modal-body">';
    html += '<p id="document">';
    html += 'Select file :<input type="file" name="supporting_doc" id="id_supporting_doc">'
    html += '<br>';
    html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    html += '<input id="btn-upload" class="default transition-customer_confirmed_class pull-right" name="_fsmtransition-status-customer_confirmed" type="submit" value="Upload" >';
    html += '</p>';
    html += '<br>';
    html += '<div class="modal-footer">';
    html += '</div>';  // footer
    html += '</div>';  // ConfirmationmodalWindow

    var unapprove_html = '<div id="UnapprovemodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    unapprove_html += '<div class="modal-dialog" style="width:450px;">';
    unapprove_html += '<div class="modal-content">';
    unapprove_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> UnApprove Form</h4></div>';
    unapprove_html += '<div class="modal-body">';
    unapprove_html += '<p id="document">';
    unapprove_html += '<textarea rows="5" cols="40" name="reason" id="id_reason"></textarea>'
    unapprove_html += '<br>';
    unapprove_html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    unapprove_html += '<input id="save-reason" class="default transition-unapprove_class pull-right" name="_fsmtransition-status-unapprove" type="submit" value="submit" >';
    unapprove_html += '</p>';
    unapprove_html += '<br>';
    unapprove_html += '<div class="modal-footer">';
    unapprove_html += '</div>';  // footer
    unapprove_html += '</div>';  // ConfirmationmodalWindow

    var reject_html = '<div id="RejectmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    reject_html += '<div class="modal-dialog" style="width:450px;">';
    reject_html += '<div class="modal-content">';
    reject_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> UnApprove Form</h4></div>';
    reject_html += '<div class="modal-body">';
    reject_html += '<p id="document">';
    reject_html += '<textarea rows="5" cols="40" name="reason" id="id_reject_reason"></textarea>'
    reject_html += '<br>';
    reject_html += `<div class="row">
                        <div class="col-offset-3 col-md-6">
                            <div class="form-group">
                                <label>Due Date:</label>
                                <input type='text' class="form-control" id="id-reject_date" autocomplete="off"/>
                            </div>
                        </div>
                    </div>`;
    reject_html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    reject_html += '<input id="id_reject" class="default transition-reject_class pull-right" name="_fsmtransition-status-reject" type="submit" value="submit" >';
    reject_html += '</p>';
    reject_html += '<br>';
    reject_html += '<div class="modal-footer">';
    reject_html += '</div>';  // footer
    reject_html += '</div>';  // ConfirmationmodalWindow

    var sales_reject_html = '<div id="SalesRejectmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    sales_reject_html += '<div class="modal-dialog" style="width:450px;">';
    sales_reject_html += '<div class="modal-content">';
    sales_reject_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> UnApprove Form</h4></div>';
    sales_reject_html += '<div class="modal-body">';
    sales_reject_html += '<p id="document">';
    sales_reject_html += '<textarea rows="5" cols="40" name="sales_reason" id="id_sales_reason"></textarea>'
    sales_reject_html += '<br>';
    sales_reject_html += `<div class="row">
                        <div class="col-offset-3 col-md-6">
                            <div class="form-group">
                                <label>Due Date:</label>
                                <input type='text' class="form-control" id="id-sales_reject_date" autocomplete="off"/>
                            </div>
                        </div>
                    </div>`;
    sales_reject_html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    sales_reject_html += '<input id="id_sales_reject" class="default transition-sales_reject_class pull-right" name="_fsmtransition-status-sales_reject" type="submit" value="submit" >';
    sales_reject_html += '</p>';
    sales_reject_html += '<br>';
    sales_reject_html += '<div class="modal-footer">';
    sales_reject_html += '</div>';  // footer
    sales_reject_html += '</div>';  // ConfirmationmodalWindow

    var disputed_html = '<div id="disputedmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    disputed_html += '<div class="modal-dialog" style="width:450px;">';
    disputed_html += '<div class="modal-content">';
    disputed_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> Disputed Form</h4></div>';
    disputed_html += '<div class="modal-body">';
    disputed_html += '<p id="document">';
    disputed_html += '<textarea rows="5" cols="40" name="disputed_reason" id="id_disputed_reason"></textarea>'
    disputed_html += '<br>';
    disputed_html += `<div class="row">
                        <div class="col-offset-3 col-md-6">
                            <div class="form-group">
                                <label>Due Date:</label>
                                <input type='text' class="form-control" id="id-disputed_date" autocomplete="off"/>
                            </div>
                        </div>
                    </div>`;
    disputed_html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    disputed_html += '<input id="id_save_disputed_reason" class="default transition-customer_disputed_class pull-right" name="_fsmtransition-status-customer_disputed" type="submit" value="submit" >';
    disputed_html += '</p>';
    disputed_html += '<br>';
    disputed_html += '<div class="modal-footer">';
    disputed_html += '</div>';  // footer
    disputed_html += '</div>';

    var send_mail = '<div id="SendMailModalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    send_mail += '<div class="modal-dialog modal-lg">';
    send_mail += '<div class="modal-content">';
    send_mail += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> Send Mail Form</h4></div>';
    send_mail += '<div class="modal-body">';
    send_mail += '<p id="send_mail">';
    send_mail += '<div class="form-group" style="max-width:350px;">';
    send_mail += '<div class="supporting-file"> Select file :<input type="file" name="supporting_doc" id="id_unapprove_doc"></div>';
    send_mail += '<div class="row" style="margin: 0;text-align: right;font-size: 12px;color: red;"><p>* Attach hard copy</p></div></div>';
    send_mail += '<div class="row" style="margin: 0;">';
    send_mail += `<div class="row" style="margin-bottom: 10px;border-bottom: 1px dashed #ccc;">
                        <div class="col-offset-3 col-md-6">
                            <label> Customer Contacts: </label><br>
                            <div id="loading-msg" class="row text-center">Loading Contacts... Be patient..</div>
                            <select id="id-to_email" data-style="" class="selectpicker form-control" multiple data-max-options="2"></select>
                        </div>
                    </div>`;
    send_mail += '<div class="row" style="margin: 0;font-size: 12px;font-weight:bold;"><p>Additional Emails (CC):</p></div>';
    send_mail += '<input type="email" name="email_prompt" id="email_prompt" placeholder="Enter the email"> &nbsp;';
    send_mail += '<button type="button" id="add-email" class="button">Add</button><br><br>';
    send_mail += '<div class="row" style="display:none;">&nbsp;&nbsp;&nbsp;&nbsp;<textarea name="email" id="id_email" rows="3" readonly></textarea></div>';
    send_mail += '<div class="row" id="email-id-container" style="padding: 5px;max-width: 350px;background:#ccc;border: 1px dashed #fff;margin:8px 0;max-height: 25vh;overflow: auto;"></div>';
    send_mail += '</div><hr>';
    send_mail += '<div class="row" style="margin: 0;font-size: 12px;"><p><b>NOTE: </b><br> 1. Mail will be sent to company invoice mail and <i><b>to YOU</b></i> by default.<br>';
    send_mail += '<p>2. Emails in Additional Emails and company invoice mail will be added to CC.</p></div>';
    send_mail += '<div class="modal-footer">';
    send_mail += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    send_mail += '<input id="send_invoice" class="default transition-send_invoice pull-right" name="_fsmtransition-status-send_invoice" type="submit" value="submit">';
    send_mail += '</div>';
    send_mail += '</div>';  // ConfirmationmodalWindow

    var reassign_html = '<div id="reassignmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    reassign_html += '<div class="modal-dialog" style="width:450px;">';
    reassign_html += '<div class="modal-content">';
    reassign_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> Send Mail Form</h4></div>';
    reassign_html += '<div class="modal-body">';
    reassign_html += '<p id="reassign-spoc">';
    reassign_html += '<div class="soft-copy-div" style="padding: 0px 20px;">';
    reassign_html += `<div class="row" style="margin-bottom: 10px;">
                                    <div class="col-offset-3 col-md-6">
                                        <label> Spoc: </label><br>
                                        <select id="id-spoc" class="livesearch"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-offset-3 col-md-6">
                                        <div class="form-group">
                                            <label>Due Date:</label>
                                            <input type='text' class="form-control" id="id-spoc_date" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>`;
    reassign_html += '</p>';
    reassign_html += '<div class="modal-footer">';
    reassign_html += '<div><input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    reassign_html += '<input id="id-reassign" class="default transition-reassign_class pull-right" name="_fsmtransition-status-reassign" type="submit" value="submit" >';
    reassign_html += '</div>';

    var send_for_approval_html = '<div id="sendforapprovalmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    send_for_approval_html += '<div class="modal-dialog" style="width:450px;">';
    send_for_approval_html += '<div class="modal-content">';
    send_for_approval_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> Send Mail Form</h4></div>';
    send_for_approval_html += '<div class="modal-body">';
    send_for_approval_html += '<p id="id_send_to_supervisor">';
    send_for_approval_html += '<div class="soft-copy-div" style="padding: 0px 20px;"><b>Upload MIS Document :</b><input type="file" name="supporting_doc" id="id_supporting_mis_doc"></div>'
    send_for_approval_html += '<br>';
    send_for_approval_html += '<div class="soft-copy-div" style="padding: 0px 20px;">';
    send_for_approval_html += `<div class="row" style="margin-bottom: 10px;">
                                    <div class="col-offset-3 col-md-6">
                                        <label> Supervisor: </label><br>
                                        <select id="id-supervisor" class="livesearch"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-offset-3 col-md-6">
                                        <div class="form-group">
                                            <label>Due Date:</label>
                                            <input type='text' class="form-control" id="id-sfa_date" autocomplete="off"/>
                                        </div>
                                    </div>
                                </div>`;
    send_for_approval_html += '</p>';
    send_for_approval_html += '<div class="modal-footer">';
    send_for_approval_html += '<div><input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    send_for_approval_html += '<input id="id-sendforapprove" class="default transition-send_for_approval_class pull-right" name="_fsmtransition-status-send_for_approval" type="submit" value="submit" >';
    send_for_approval_html += '</div>';

    var approve_html = '<div id="approvemodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="approval-modal" aria-hidden="true">';
    approve_html += '<div class="modal-dialog" style="width:450px;">';
    approve_html += '<div class="modal-content">';
    approve_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> Send Mail Form</h4></div>';
    approve_html += '<div class="modal-body">';
    approve_html += '<p id="asign-representative">';
    approve_html += '<div class="soft-copy-div" style="padding: 0px 20px;">';
    approve_html += `
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-offset-3 col-md-6">
                <label> Representative: </label><br>
                <select id="id-representative" class="livesearch"></select>
            </div>
        </div>
        <div class="row">
            <div class="col-offset-3 col-md-6">
                <div class="form-group">
                    <label>Due Date:</label>
                    <input type='text' class="form-control" id="id-date" autocomplete="off"/>
                </div>
            </div>
        </div>`;
    approve_html += '</p>';
    approve_html += '<div class="modal-footer">';
    approve_html += '<div><input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    approve_html += '<input id="id-approve" class="default transition-approve_class pull-right" name="_fsmtransition-status-approve" type="submit" value="submit" >';
    approve_html += '</div>';

    var customer_acknowledged_html = '<div id="AcknowledgednmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    customer_acknowledged_html += '<div class="modal-dialog" style="width:450px;">';
    customer_acknowledged_html += '<div class="modal-content">';
    customer_acknowledged_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">File upload form</h4> </div>';
    customer_acknowledged_html += '<div class="modal-body">';
    customer_acknowledged_html += '<p id="id_acknowledged_document">';
    customer_acknowledged_html += 'Select file :<input type="file" name="supporting_doc" id="id_supporting_doc">'
    customer_acknowledged_html += '<br>';
    customer_acknowledged_html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    customer_acknowledged_html += '<input id="id_ack_upload" class="default transition-customer_acknowledged_class pull-right" name="_fsmtransition-status-customer_acknowledged" type="submit" value="Upload" >';
    customer_acknowledged_html += '</p>';
    customer_acknowledged_html += '<br>';
    customer_acknowledged_html += '<div class="modal-footer">';
    customer_acknowledged_html += '</div>';  // footer
    customer_acknowledged_html += '</div>';  // ConfirmationmodalWindow

    $("#customerinvoice_form").append(html);
    $("#customerinvoice_form").append(unapprove_html);
    // $("#customerinvoice_form").append(send_mail);
    $("#customerinvoice_form").append(disputed_html);
    $("#customerinvoice_form").append(reject_html);
    $("#customerinvoice_form").append(approve_html);
    $("#customerinvoice_form").append(send_for_approval_html);
    $("#customerinvoice_form").append(reassign_html);
    $("#customerinvoice_form").append(customer_acknowledged_html);
    $("#customerinvoice_form").append(sales_reject_html);
    // $("#changelist-form").append(send_mail);

    var is_obj_change = getUrlParameters("/change", document.URL);
    var is_regenerated = extractUrlValue("is_regenerated");
    var inv_num = extractUrlValue("against");

    function loadSelectFilter() {
        loadScript("/static/admin/js/SelectFilter2.js");
    }
    loadScript("/static/admin/js/SelectBox.js", loadSelectFilter);

    function manageLabelNodeList(labelNodes, displayType) {
        if (!labelNodes) {
            return;
        }
        for (var i = 0; i < labelNodes.length; i++) {
            labelNodes[i].style.display = displayType;
            // labelNodes[i].textContent = label;
        }
    }

    function setCustomerContacts(is_stacked) {
        console.log('calling api/..')
        var fieldId = $('#id-to_email').attr('id');
        var selectElem = fieldId ? $('#id-to_email') : $('#id-to_email_from');
        selectElem.find('option').remove();
        $('.selector').hide();
        $('#loading-msg').text('Loading Contacts...');
        $('#loading-msg').show();
        $.ajax({
            type: 'GET',
            url: '/api/customer/businessuser/contacts/' + selectedInvoice,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(data) {
                contacts = data.contacts;
                if (!contacts.length) {
                    $('#loading-msg').hide();
                    $('.selector').show();
                }
                for (var i = 0; i < contacts.length; i++) {
                    selectElem.append($("<option value=" + contacts[i] + ">" + contacts[i] + "</option>"));
                }
                if (fieldId) {
                    SelectFilter.init(fieldId, 'Contacts', is_stacked);
                }
                $('#loading-msg').hide();
                $('.selector').show();
                $('#id-to_email_to').prop('name', 'recipients');
                staff = data.staff
                for (var i=0; i < staff.length; i++){
                    addToRecipient(null, staff[i])
                }

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                selectElem.val('');
                $('#loading-msg').text('No customer contacts found. Kindly add in business customer page.');
            }
        });
    }

    function addToRecipient(e, email) {
        console.log('Adding new email..', email);
        var emailId = '';
        if (email) {
            emailId = email;
        } else {
            emailId = $('#email_prompt').val();
        }
        var mail_ids = emailId.trim().split(",");
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        for (i = 0; i < mail_ids.length; i++) {
            var mail_id = mail_ids[i].trim().toLowerCase();
            if (re.test(String(mail_id))) {
                if (recipients.indexOf(mail_id) !== -1) {
                    alert(mail_id + ' already present');
                    return;
                }
                $('#email_prompt').val('');
            } else {
                if (e){
                    e.preventDefault();
                    alert('Please, enter correct email..');
                    return;
                }
            }
            $('#email-id-container').show();
            $('#email_prompt').val('');
            recipients.push(mail_id);
            var email_row = "<div class='row' id='" + mail_id + "'>"
            + mail_id + "<i onclick='removeAdditionalRecipients(event)'>&times;</i>"
            + "</div>";
            $('#email-id-container').append(email_row);
            $('input[name="cc_email"]').val(recipients.join(','));
        }
    }

    $("#content-main #changelist-form, #content-main > form").append('<div id="send-to-customer-modal-container"></div>');
    $("#send-to-customer-modal-container").load("/static/admin/html/customer/invoice/send_to_customer.html",
        initModalListener);

    function initModalListener() {
        jQuery('#modal-send-to-customer').on('shown.bs.modal', function () {
            console.log('modal-send-to-customer modal opened..');
            $('#confirmation-block').hide();
            $('#email-form-container').show();

            recipients = [];
            $('#email-id-container').html('');
            $('#loading-msg').hide();
            var $el = $("#id-to_email");
            var data = $el.data();
            var is_stacked = data ? parseInt(data.isStacked, 10) : 2;
            var invoice_id_under_edit = $('.field-id p').text();
            if (invoice_id_under_edit) {
                selectedInvoice = parseInt(invoice_id_under_edit);
            }
            if (selectedInvoice) {
                setCustomerContacts(is_stacked);
            }
            $('#email-id-container').hide();
        });

        // autocomplete additional emails (staff)
        jQuery('#email_prompt').autocomplete({
            appendTo: '.customer-contacts-block',
            source: function (request, response) {
                $.ajax({
                    method: 'GET',
                    url: "/api/customer/invoice/autocomplete/email",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        var processedData = [];
                        data.forEach(item => {
                            processedData.push({
                                id: item,
                                label: item,
                                value: item
                            });
                        });
                        response(processedData);
                    }
                });
            },
            minLength: 4,
            select: function (event, ui) {
                addToRecipient(event, ui.item.value);
                $(this).val("");
                return false;
            }
        });
    }

    $(document).ready(function () {
        $('input[name="_selected_action"], #action-toggle').on('click', function () {
            var inputs = $('input[name="_selected_action"]');
            selectedInvoice = null;
            for (var i = 0; i < inputs.length; i++) {
                var currentInput = $(inputs[i])[0];
                if (currentInput.checked) {
                    selectedInvoice = currentInput.value;
                    console.log('selectedInvoice: ', selectedInvoice);
                    break;
                }
            }
        });

        btnSendInvoice.removeClass('transition-send_invoice').addClass("modal-btn-send-invoice");
        btnResendInvoice.removeClass('transition-resend').addClass("modal-btn-resend-invoice");

        function modifyBtnAttr(buttons, btnText) {
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].removeAttribute('name');
                buttons[i].setAttribute('type', 'button');
                buttons[i].setAttribute('value', btnText);
            }
        }

        if (btnSendInvoice.length) {
            modifyBtnAttr(btnSendInvoice, BTN_TEXT_SEND_TO_CUSTOMER);
        }

        if (btnResendInvoice.length) {
            modifyBtnAttr(btnResendInvoice, BTN_TEXT_RESEND_TO_CUSTOMER);
        }

        function handleInvoiceModalSubmit(newAttr, resend) {
            $('#id_to_email').val($('#id-to_email').val());
            $('#send_invoice').prop('name', newAttr);
            if (resend) {
                $('#send_invoice')
                .removeClass('transition-resend_class transition-send_invoice_class transition-send_invoice')
                .addClass('transition-resend');
            }
            jQuery('#modal-send-to-customer').modal('show');
        }

        $(".modal-btn-send-invoice").click(function (e) {
            e.preventDefault();
            handleInvoiceModalSubmit('_fsmtransition-status-send_invoice', false);
        });

        $(".modal-btn-resend-invoice").click(function (e) {
            e.preventDefault();
            handleInvoiceModalSubmit('_fsmtransition-status-resend', true);
        });

        $('.additional-email-block button').on('click', function (e) {
            addToRecipient(e, null);
        });

        $('input[name="_selected_action"]').on('click', function () {
            selected_invoice = $(this).prop('checked') ? $(this).val() : null;
        });

        $('#changelist-form').prop("enctype", "multipart/form-data");
        $('.actions input[name="proof_document"]').hide();
        manageLabelNodeList($('input[name="proof_document"').prop('labels'), 'none');
        $('.field-supporting_doc').remove();
        $('.field-reason').remove();
        $('.field-disputed-reason').remove();
        $('.field-sales-reason').remove();

        var representatives = $('#id_representative').val() ? $('#id_representative').val().split(',') : new Array();

        if ($('#id_executive').val()) {
            var supervisors = $('#id_executive').val().split(',');
        } else if ($('#id_supervisor').val()) {
            var supervisors = $('#id_supervisor').val().split(',');
        } else {
            var supervisors = new Array()
        }

        var spocs = $('#id_spoc').val() ? $('#id_spoc').val().split(',') : new Array();

        $('.field-representative').hide();
        $('.field-supervisor').hide();
        $('.field-spoc').hide();
        $('.field-executive').hide();
        $('.field-duedate').hide();
        $("#customerinvoice_form > div:nth-child(2) > fieldset.module.aligned.module_0.initialized.selected > div.form-row.field-id").hide();
        $('.field-is_reassigning').hide();
        $('.field-to_email').hide();
        $('.field-cc_email').hide();

        $('.field-email').remove();
        for (var rep in representatives) {
            $("#id-representative").append($("<option value="+representatives[rep]+">"+representatives[rep]+"</option>"))
        }

        for (var rep in supervisors) {
            $("#id-supervisor").append($("<option value="+supervisors[rep]+">"+supervisors[rep]+"</option>"))
        }

        for (var rep in spocs) {
            $("#id-spoc").append($("<option value="+spocs[rep]+">"+spocs[rep]+"</option>"))
        }

        sent_to_customer.addClass("transition-sent_to_customer_class").removeClass('transition-sent_to_customer');
        unapprove.addClass("transition-unapprove_class").removeClass('transition-unapprove');
        reject.addClass("transition-reject_class").removeClass('transition-reject');
        approve.addClass("transition-approve_class").removeClass('transition-approve');
        send_for_approval.addClass("transition-send_for_approval_class").removeClass('transition-send_for_approval');
        // send_invoice_mail.addClass("transition-send_invoice_class").removeClass('transition-send_invoice');
        // resend_invoice_mail.addClass("transition-resend_class").removeClass('transition-resend');
        confirmed.addClass('transition-customer_confirmed_class').removeClass('transition-customer_confirmed');
        disputed.addClass('transition-customer_disputed_class').removeClass('transition-customer_disputed');
        regenerate.addClass('transition-customer_regenerate');
        reassign.addClass("transition-reassign_class").removeClass('transition-reassign');
        regenerate_disputed_invoice.addClass('transition-regenerate_disputed_invoice_class').remove('transition-regenerate_disputed_invoice')
        generate_credit_debit_note.addClass('transition-generate_credit_debit_note_class').remove('transition-generate_credit_debit_note')
        customer_acknowledged.addClass("transition-customer_acknowledged_class").removeClass('transition-customer_acknowledged');
        sales_reject.addClass("transition-sales_reject_class").removeClass('transition-sales_reject');

        if (confirmed[0]){
            confirmed[0].removeAttribute('name');
            confirmed[0].setAttribute('value' ,'customer confirmed');
            confirmed[0].setAttribute('type', 'button');
        }
        if (disputed[0]){
            disputed[0].removeAttribute('name');
            disputed[0].setAttribute('value' ,'customer disputed');
            disputed[0].setAttribute('type', 'button');
        }
        if (send_for_approval[0]){
            send_for_approval[0].removeAttribute('name');
            send_for_approval[0].setAttribute('value' ,'send for approval');
            send_for_approval[0].setAttribute('type', 'button');
        }
        if (reassign[0]){
            reassign[0].removeAttribute('name');
            reassign[0].setAttribute('value' ,'reassign spoc');
            reassign[0].setAttribute('type', 'button');
        }
        if (customer_acknowledged[0]){
            customer_acknowledged[0].removeAttribute('name');
            customer_acknowledged[0].setAttribute('value' ,'invoice acknowleged by customer');
            customer_acknowledged[0].setAttribute('type', 'button');
        }
        if (regenerate_disputed_invoice[0]){
            regenerate_disputed_invoice[0].removeAttribute('name');
            regenerate_disputed_invoice[0].setAttribute('value' ,'regenerate disputed invoice');
            regenerate_disputed_invoice[0].setAttribute('type', 'button');
        }
        if (generate_credit_debit_note[0]){
            generate_credit_debit_note[0].removeAttribute('name');
            generate_credit_debit_note[0].setAttribute('value' ,'generate credit debit note');
            generate_credit_debit_note[0].setAttribute('type', 'button');
        }
        if (regenerate[0]){
            regenerate[0].setAttribute('value' ,'regenerate');
        }
        if (approve[0]) {
            approve[0].removeAttribute('name');
            approve[0].setAttribute('value' ,'approve invoice');
            approve[0].setAttribute('type', 'button');
        }

        if (reject[0]) {
            reject[0].removeAttribute('name');
            reject[0].setAttribute('value' ,'reject invoice');
            reject[0].setAttribute('type', 'button');
        }

        if (sent_to_customer[0]) {
            sent_to_customer[0].removeAttribute('name');
            sent_to_customer[0].setAttribute('value' ,'sent to customer');
            sent_to_customer[0].setAttribute('type', 'button');
        }
        if (action_go[0]) {
            action_go[0].setAttribute('type', 'button');
            action_go[0].setAttribute('id', 'sub_button');
            action_go[0].removeAttribute('name');
        }
        if (unapprove[0]) {
            unapprove[0].removeAttribute('name');
            unapprove[0].setAttribute('value' ,'unapprove invoice');
            unapprove[0].setAttribute('type', 'button');
        }

        if (sales_reject[0]) {
            sales_reject[0].removeAttribute('name');
            sales_reject[0].setAttribute('value' ,'reject invoice');
            sales_reject[0].setAttribute('type', 'button');
        }

        if (!is_obj_change && !is_regenerated) {
            var temp_invoice_type = $('#id_invoice_type').val();

            if (temp_invoice_type == 'Tax Invoice') {
                $("#customerinvoice_form .field-related_invoice").hide();
                $("#customerinvoice_form .field-credit_debit_note_reason").hide();
                $("#customerinvoice_form .field-customer").show();
                $("#customerinvoice_form .field-service_period_start").show();
                $("#customerinvoice_form .field-service_period_end").show();
                $("#customerinvoice_form .field-bh_source_state").show();
                $("#customerinvoice_form .field-invoice_state").show();
                $("#customerinvoice_form .field-service_tax_category").show();
            } else {
                $("#customerinvoice_form .field-credit_debit_note_reason").show()
                $("#customerinvoice_form .field-customer").hide();
                $("#customerinvoice_form .field-service_period_start").hide();
                $("#customerinvoice_form .field-service_period_end").hide();
                $("#customerinvoice_form .field-bh_source_state").hide();
                $("#customerinvoice_form .field-invoice_state").hide();
                $("#customerinvoice_form .field-service_tax_category").hide();
            }
        }

        $('input[type="submit"]').on('click', function (e) {
            var period_start = $('#id_service_period_start').val();
            var invoice_state = $('#id_bh_source_state').val();

            if (!period_start) {
                $('#id_service_period_start').val('19-06-2000');
                $('#id_service_period_end').val('19-06-2000');
            }
            if (!invoice_state) {
                $('#id_bh_source_state').val(1);
                $('#id_invoice_state').val(1);
            }
        });

        $("#id_invoice_type", this).change(function () {
            var temp_invoice_type = $('#id_invoice_type').val();
            if (temp_invoice_type == 'Tax Invoice') {
                $("#customerinvoice_form .field-related_invoice").hide();
                $('#id_credit_debit_note_reason').val('').trigger('change');
                $('#select2-id_credit_debit_note_reason-container').text('---------').trigger('change');
                $("#customerinvoice_form .field-credit_debit_note_reason").hide();
                $("#customerinvoice_form .field-customer").show();
                $("#customerinvoice_form .field-service_period_start").show();
                $("#customerinvoice_form .field-bh_source_state").show();
                $("#customerinvoice_form .field-invoice_state").show();
                $("#customerinvoice_form .field-service_period_end").show();
                $("#customerinvoice_form .field-service_tax_category").show();
            } else {
                $("#help_txt").remove();
                $("#customerinvoice_form .field-credit_debit_note_reason").show()
                $("#customerinvoice_form .field-customer").hide();
                $("#customerinvoice_form .field-service_period_start").hide();
                $("#customerinvoice_form .field-service_period_end").hide();
                $("#customerinvoice_form .field-bh_source_state").hide();
                $("#customerinvoice_form .field-invoice_state").hide();
                $("#customerinvoice_form .field-service_tax_category").hide();
                $("#customerinvoice_form .field-related_invoice").show();
                $("#customerinvoice_form .field-related_invoice").append('<span id="help_txt">"<strong>Customer Confirmed Invoices will be listed here.</strong>"</span>');
            }
        });

        $(".transition-sent_to_customer_class").click(function () {
            $('h4').html('Upload Supporting Document');
            $('#btn-upload')[0].setAttribute('name', '_fsmtransition-status-sent_to_customer');
            jQuery("#ConfirmationmodalWindow").modal('show');
        });
         $(".transition-send_for_approval_class").click(function (e) {
            $('#id_supervisor').val($('#id-supervisor').val());
            var dueDate = $('#id-sfa_date').val();
            ValidateDate(dueDate, e);
            $('#id_duedate').val(dueDate);

            $('h4').html('Choose the Supervisor for Approval');
            $('#id-sendforapprove')[0].setAttribute('name', '_fsmtransition-status-send_for_approval');
            jQuery("#sendforapprovalmodalWindow").modal('show');
        });
         $(".transition-reassign_class").click(function (e) {
            $('#id_spoc').val($('#id-spoc').val());
            var dueDate = $('#id-spoc_date').val();
            ValidateDate(dueDate, e);
            $('#id_duedate').val(dueDate);

            $('h4').html('Choose the Spoc for Reassigning');
            $('#id-reassign')[0].setAttribute('name', '_fsmtransition-status-reassign');
            jQuery("#reassignmodalWindow").modal('show');
        });
         $(".transition-regenerate_disputed_invoice_class").click(function () {
            var inv = jQuery(".form-row.field-id > div > p")
            var invoice_number = inv.text()
            window.open('/admin/customer/invoicerequest/add', '_blank')
            regenerate_disputed_invoice.addClass('transition-regenerate_disputed_invoice')
            regenerate_disputed_invoice[0].setAttribute('name', '_fsmtransition-status-regenerate_disputed_invoice')
            regenerate_disputed_invoice[0].setAttribute('type', 'submit')

        });

         $(".transition-generate_credit_debit_note_class").click(function () {
            var inv = jQuery(".form-row.field-id > div > p")
            var invoice_number = inv.text()
            window.open('/admin/customer/customerinvoice/add?is_regenerated=true&against='.concat(invoice_number), '_blank')
            generate_credit_debit_note.addClass('transition-generate_credit_debit_note')
            generate_credit_debit_note[0].setAttribute('name', '_fsmtransition-status-generate_credit_debit_note')
            generate_credit_debit_note[0].setAttribute('type', 'submit')
        });

         $(".transition-customer_confirmed_class").click(function () {
            $('h4').html('Upload Supporting Document');
            $('#btn-upload')[0].setAttribute('name', '_fsmtransition-status-customer_confirmed');
            jQuery("#ConfirmationmodalWindow").modal('show');
        });

         $(".transition-customer_acknowledged_class").click(function () {
            $('h4').html('Upload Supporting Document of Acknowledgement');
            $('#id_ack_upload')[0].setAttribute('name', '_fsmtransition-status-customer_acknowledged');
            jQuery("#AcknowledgednmodalWindow").modal('show');
        });


        $(".transition-unapprove_class").click(function () {
            $('h4').html('Provide Reason for Un-Approve');
            $('#save-reason')[0].setAttribute('name', '_fsmtransition-status-unapprove');
            jQuery("#UnapprovemodalWindow").modal('show');
        });

        $(".transition-approve_class").click(function (e) {
            $('#id_representative').val($('#id-representative').val());
            var dueDate = $('#id-date').val();
            ValidateDate(dueDate, e);
            $('#id_duedate').val(dueDate);

            $('h4').html('Assign Collection Representative with Due date');
            $('#id-approve')[0].setAttribute('name', '_fsmtransition-status-approve');
            jQuery("#approvemodalWindow").modal('show');
        });

       jQuery("#approvemodalWindow").on('shown.bs.modal', function () {
            jQuery('#id-date').datetimepicker({
                "format": "DD-MM-YYYY",
                "useCurrent": false,
                "ignoreReadonly": true,
                "minDate": moment().add(1),
                "maxDate": moment().add(20,'days')
            });
       });

       jQuery("#sendforapprovalmodalWindow").on('shown.bs.modal', function () {
            jQuery('#id-sfa_date').datetimepicker({
                "format": "DD-MM-YYYY",
                "useCurrent": false,
                "ignoreReadonly": true,
                "minDate": moment().add(1),
                "maxDate": moment().add(20,'days')
            });
       });

       jQuery("#reassignmodalWindow").on('shown.bs.modal', function () {
            jQuery('#id-spoc_date').datetimepicker({
                "format": "DD-MM-YYYY",
                "useCurrent": false,
                "ignoreReadonly": true,
                "minDate": moment().add(1),
                "maxDate": moment().add(20,'days')
            });
       });

       jQuery("#RejectmodalWindow").on('shown.bs.modal', function () {
            jQuery('#id-reject_date').datetimepicker({
                "format": "DD-MM-YYYY",
                "useCurrent": false,
                "ignoreReadonly": true,
                "minDate": moment().add(1),
                "maxDate": moment().add(20,'days')
            });
       });

       jQuery("#disputedmodalWindow").on('shown.bs.modal', function () {
            jQuery('#id-disputed_date').datetimepicker({
                "format": "DD-MM-YYYY",
                "useCurrent": false,
                "ignoreReadonly": true,
                "minDate": moment().add(1),
                "maxDate": moment().add(20,'days')
            });
       });

       jQuery("#SalesRejectmodalWindow").on('shown.bs.modal', function () {
            jQuery('#id-sales_reject_date').datetimepicker({
                "format": "DD-MM-YYYY",
                "useCurrent": false,
                "ignoreReadonly": true,
                "minDate": moment().add(1),
                "maxDate": moment().add(20,'days')
            });
       });

        $(".transition-reject_class").click(function (e) {
            var dueDate = $('#id-reject_date').val();
            ValidateDate(dueDate, e);
            $('#id_duedate').val(dueDate);
            $('h4').html('Provide Reason for Rejection');
            $('#id_reject')[0].setAttribute('name', '_fsmtransition-status-reject');
            jQuery("#RejectmodalWindow").modal('show');
        });

        $(".transition-sales_reject_class").click(function (e) {
            var dueDate = $('#id-sales_reject_date').val();
            ValidateDate(dueDate, e);
            $('#id_duedate').val(dueDate);
            $('h4').html('Provide Reason for Rejection');
            $('#id_sales_reject')[0].setAttribute('name', '_fsmtransition-status-sales_reject');
            jQuery("#SalesRejectmodalWindow").modal('show');
        });

        $(".transition-customer_disputed_class").click(function (e) {
            var dueDate = $('#id-disputed_date').val();
            ValidateDate(dueDate, e);
            $('#id_duedate').val(dueDate);
            $('h4').html('Provide Reason for Dispute.');
            $('#id_save_disputed_reason')[0].setAttribute('name', '_fsmtransition-status-customer_disputed');
            jQuery("#disputedmodalWindow").modal('show');
        });

        $('#sub_button').click(function () {
            var action = $('select[name="action"]').val();
            switch (action) {
                case 'send_invoice':
                    jQuery("#modal-send-to-customer").modal('show');
                    break;

                case 'mark_as_customer_confirmed':
                case 'mark_as_customer_acknowledged':
                    if (!$('input[name="proof_document"').val()) {
                        alert('Proof is mandatory');
                        return;
                    }
                    $('#changelist-form').submit();
                 break;

                default:
                    $('#changelist-form').submit();
            }
        });

        $('select[name="action"]').on('change', function () {
            var action = $('select[name="action"]').val();
            switch (action) {
                case 'mark_as_customer_confirmed':
                    $('.actions input[name="proof_document"]').show();
                    manageLabelNodeList($('input[name="proof_document"').prop('labels'), 'inline-block');
                    break;

                case 'mark_as_customer_acknowledged':
                    $('.actions input[name="proof_document"]').show();
                    manageLabelNodeList($('input[name="proof_document"').prop('labels'), 'inline-block');
                    break;

                default:
                    $('.actions input[name="proof_document"]').hide();
                    manageLabelNodeList($('input[name="proof_document"').prop('labels'), 'none');
            }
        });

        $('#save-reason').on('click', function (e) {
            if (!$('#id_reason').val().trim()) {
                e.preventDefault();
                alert('Provide a reason');
            }
        });

        $('#soft-copy').on('click', function (e) {
            $(".supporting-file").hide();
            $(".soft-copy-div").show();
        });
        $('#hard-copy').on('click', function (e) {
            $(".supporting-file").show();
            $(".soft-copy-div").hide();
            $('#id_email').val('');
        });
        $('#both').on('click', function (e) {
            $(".supporting-file").show();
            $(".soft-copy-div").show();
        });

        // Keep the below commented code, will be needed in future

        $('#btn-upload').on('click', function (e) {
            if (!$('#document > #id_supporting_doc').val()
             && (document.getElementById("btn-upload").className == "default transition-customer_confirmed_class pull-right")) {
                e.preventDefault();
                alert('Upload a file..');
            }
        });

        $('#id_ack_upload').on('click', function (e) {
            if (!$('#id_acknowledged_document > #id_supporting_doc').val()
             && (document.getElementById("id_ack_upload").className == "default transition-customer_acknowledged_class pull-right")) {
                e.preventDefault();
                alert('Upload a file..');
            }
        });

        $('#id-approve').on('click', function (e) {
            if (!$('#id-representative').val() || !$('#id-representative').val().trim()) {
                e.preventDefault();
                alert('Choose the assignee..');
            }
            if (!$('#id-date').val().trim()) {
                e.preventDefault();
                alert('Choose the due date..');
            }
        });

        $('#id-sendforapprove').on('click', function (e) {
            if (!$('#id-supervisor').val() || !$('#id-supervisor').val().trim()) {
                e.preventDefault();
                alert('Choose the supervisor..');
            }
            if (!$('#id-sfa_date').val().trim()) {
                e.preventDefault();
                alert('Choose the due date..');
            }
            if ($('#id_supporting_mis_doc')[0].files.length === 0){
                e.preventDefault();
                alert('Please upload the MIS document')
            }
        });

        $('#id-reassign').on('click', function (e) {
            if (!$('#id-spoc').val() || !$('#id-spoc').val().trim()) {
                e.preventDefault();
                alert('Choose the spoc..');
            }
            if (!$('#id-spoc_date').val().trim()) {
                e.preventDefault();
                alert('Choose the due date..');
            }
            jQuery('#id_is_reassigning').prop('checked', true)
        });

        $('#id_save_disputed_reason').on('click', function (e) {
            if (!$('#id_disputed_reason').val().trim()) {
                e.preventDefault();
                alert('Provide a reason');
            }
            if (!$('#id-disputed_date').val().trim()) {
                e.preventDefault();
                alert('Choose the due date..');
            }
        });

        $('#id_reject').on('click', function (e) {
            if (!$('#id_reject_reason').val().trim()) {
                e.preventDefault();
                alert('Provide a reason');
            }
            if (!$('#id-reject_date').val().trim()) {
                e.preventDefault();
                alert('Choose the due date..');
            }
        });

         $('#id_sales_reject').on('click', function (e) {
            if (!$('#id_sales_reason').val().trim()) {
                e.preventDefault();
                alert('Provide a rejection reason');
            }
            if (!$('#id-sales_reject_date').val().trim()) {
                e.preventDefault();
                alert('Choose the due date..');
            }
        });

        // Disable FSM on form field changes
        storeDatetimeInputs();
        $("#content-main form :input", this).change(function () {
            disableFsmButtons();
        });

        var submitButtons = $('input[type="submit"]');
        for (var index in submitButtons) {
            var btnName = submitButtons[index].name;
            if (btnName && btnName.includes('_fsmtransition') ||
                regex_fsmtransition.test(btnName)) {
                $('input[name="' + btnName + '"]').on('click', function (e) {
                    if (valueChanged()) {
                        e.preventDefault();
                        disableFsmButtons();
                    }
                });
            }
        }
    });
});
})(django.jQuery);
