(function ($) {
    function addTableFooter() {
        var total_amount = parseFloat($(".field-total_amount > div > p").text());
        var suggested_amount = parseFloat($(".field-suggested_value > div > p").text());
        var suggested_tds = parseFloat($(".field-suggested_tds > div > p").text());
        var credit_amount = $("div.form-row.field-get_credit_amount > div > p").text();
        var tables = $('#invoicepayment_set-group > div > fieldset > table > tbody');
        var rows = tables[0].getElementsByTagName("tr");
        console.log(total_amount)
        if (credit_amount != "None") {
            total_amount += parseFloat(credit_amount);
        }

        var amount_received = 0.0;
        $.each(rows, function (index, value) {
            var children = $(value).length ? $(value)[0].children : [];
            $.each(children, function (cIndex, cVal) {
                var clsList = cVal.classList;
                var amount = 0
                if (clsList.value == 'field-actual_amount' | clsList.value == 'field-tds')   {
                    var tdChildren = $(this).children();

                    if (tdChildren.length) {
                        amount = tdChildren[0].innerText;
                        if (!amount) {
                            amount = tdChildren[0].value;
                        }
                    }
                    if (parseFloat(amount)) {
                        amount_received += parseFloat(amount);
                    }
                }
            });
        });

        if (amount_received) {
            total_amount -= amount_received;
        }
        total_amount = total_amount.toFixed(2);

        var tbl = document.createElement('table');
        tbl.style.backgroundColor = '#F6F6F6';
        // tbl.style.margin-left = '20px';
        var tbdy = document.createElement('tbody');
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');

        tr.appendChild(td1);
        tr.appendChild(td2);
        tbdy.appendChild(tr);
        tbl.appendChild(tbdy);

        var row_1 = tables[0].insertRow(rows.length - 1);
        var row_2 = tables[0].insertRow(rows.length - 1);

        var col_html_1 = '<b>Remaining Payable Amount: ' + total_amount + '</br>' + 'Suggested amount: ' + suggested_amount;
        var col_html_2 = '<b>Discount via Credit Note: ' + credit_amount + '</br>' + 'Suggested TDS: ' + suggested_tds;

        var col_row_1 = row_1.insertCell(0);
        var col_row_2 = row_1.insertCell(1);
        var col_row_4 = row_2.insertCell(0);

        col_row_1.innerHTML = col_html_1;
        col_row_2.innerHTML = col_html_2;

        col_row_1.height = 50;
        col_row_2.height = 50;

        td1.appendChild(col_row_1);
        td2.appendChild(col_row_2);

        col_row_4.appendChild(tbl);
        col_row_4.colSpan = 6;
    }
    $(document).ready(function () {
        addTableFooter();
    });

})(django.jQuery);
