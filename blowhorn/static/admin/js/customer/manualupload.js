var manual_reasons = [];
function set_reason_value() {
  $('#id_manualupload_reasons').val($('#reasons_choice').val());
}

(function ($) {
  $(function () {
    const BTN_TEXT_MANUAL_UPLOAD = 'Manual Upload';
    var btnSendInvoice = jQuery('.transition-manual_upload');
    function getUrlParameters(parameter, staticURL) {
      var currLocation = (staticURL.length) ? staticURL : document.URL;
      var parser = document.createElement('a');
      parser.href = currLocation;
      return parser.pathname.match(parameter)
    }
    function extractUrlValue(key, url) {
      if (typeof(url) === 'undefined') {
        url = window.location.href;
      }
      var match = url.match('[?&]' + key + '=([^&#]+)');
      return match ? match[1] : false;
    }
    function manageLabelNodeList(labelNodes, displayType) {
      if (!labelNodes) {
        return;
      }
      for (var i = 0; i < labelNodes.length; i++) {
        labelNodes[i].style.display = displayType;
      }
    }
    $("#content-main #changelist-form, #content-main > form").append('<div id="manual-upload-modal-container"></div>');
    $("#manual-upload-modal-container").load("/static/admin/html/customer/invoice/manual_upload.html", initModalListener);
    function initModalListener() {
      $('#manual_upload').prop('disabled', true)
      $('#formFile, #reasons_choice').on('change', (e) => {
        $('#manual_upload').prop('disabled', !$('#reasons_choice').val())
      })
      jQuery('#modal-manual-upload').on('shown.bs.modal', function () {
        selectElem = $('#reasons_choice')
        $('#reasons_choice')
          .empty()
          .append('<option selected="selected" value="">Select reason</option>')
        for (var i = 0; i < manual_reasons.length; i++) {
              var value = manual_reasons[i].id
              selectElem.append($("<option value=" + value + ">" + manual_reasons[i].name + "</option>"));
        }
      });
    }
    $(document).ready(function () {
        if ($('#id_manualupload_reasons').val()) {
            manual_reasons = JSON.parse($('#id_manualupload_reasons').val());
        } else {
            manual_reasons = new Array()
        }
        
        $('#id_manualupload_reasons').val('')
        $('.field-manualupload_reasons').hide();
        btnSendInvoice.removeClass('transition-manual_upload').addClass("modal-btn-manual-upload");
        function modifyBtnAttr(buttons, btnText) {
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].removeAttribute('name');
                buttons[i].setAttribute('type', 'button');
                buttons[i].setAttribute('value', btnText);
            }
        }
        if (btnSendInvoice.length) {
            modifyBtnAttr(btnSendInvoice, BTN_TEXT_MANUAL_UPLOAD);
        }
        function handleInvoiceModalSubmit(newAttr) {
            $('#manual_upload').prop('name', newAttr);
            jQuery('#modal-manual-upload').modal('show');
        }
        $(".modal-btn-manual-upload").click(function (e) {
            e.preventDefault();
            handleInvoiceModalSubmit('_fsmtransition-status-manual_upload');
        });
    });
  });
})(django.jQuery);
