
var loaded_statuses = [];
var selected_status_index = '';
$(document).ready(function () {
    var $btnDownload = '<div class="btn btn-info btn-flat btn-download" title="Download as CSV">Download</div>';
    $('#toolbar').append($btnDownload);
    $('.nav-tabs a:first').tab('show');
    $('.nav-tabs li:first').addClass('active');
    $("ul.nav-tabs li").on('click', function () {
        var tab = $(this).addClass('active');
        $(this).siblings("li").removeClass('active');
        $(this).children('a').tab('show');
    });
    $('.invoice_tab_list ul li:first').click();
    $('.btn-download').on('click', function () {
        $('#content-' + selected_status_index + ' table').trigger('outputTable');
    });
});

function getExportFilename(status) {
    var dtStr = new Date().toLocaleString().replace(/[\:\/\, ]/g, "_");
    var exportFileNamePrefix = ['invoice', status, dtStr].join('_');
    return exportFileNamePrefix + '.csv';
}

function getUrlParams() {
    var query_params = {};
    var param_list = location.search.split('?');
    if (param_list.length > 1) {
        var params = param_list[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var splitted_params = params[i].split('=');
            query_params[splitted_params[0]] = splitted_params[1];
        }
    }
    return query_params;
}

function setTabContent($element) {
    $element.addClass('active');
    $element.siblings('.tab-pane').removeClass('active');
}

function fetchInvoices(status, statusIndex) {
    var $element = $('#content-' + statusIndex);
    setTabContent($element);
    selected_status_index = statusIndex;
    if (loaded_statuses.indexOf(statusIndex) != -1) {
        return;
    }
    document.getElementById('loading').style.display = 'block';
    var data = getUrlParams();
    data['status'] = status;
    $.ajax({
        type: 'GET',
        url: '/api/summary/invoice/list',
        data: data,
        success: function(response) {
            loaded_statuses.push(statusIndex);
            document.getElementById('loading').style.display = 'none';
            $element.html(response);
            var tableContainerId = "#content-" + statusIndex;
            // hide child rows
            $(tableContainerId + " table").tablesorter({
                theme: 'materialize',
                cssChildRow : "tablesorter-childRow",
                widgets : ['uitheme', 'cssStickyHeaders', 'filter', 'math', 'output'],
                widgetOptions: {
                    math_data     : 'math',
                    math_none     : '',
                    math_complete : function($cell, wo, result, value, arry) {
                        var txt = (value === wo.math_none ? '' : '₹') + result;
                        $(tableContainerId + " .table-responsive .summary-head .status-invoice-total").text(txt);
                        return txt;
                    },
                    math_event: 'recalculate',
                    // Export config
                    output_separator     : ',',
                    output_hiddenColumns : false,
                    output_includeHeader : true,
                    output_headerRows    : false,
                    output_delivery      : 'd',
                    output_saveRows      : 'f',
                    output_replaceQuote  : '\u201c;',
                    output_saveFileName  : getExportFilename(status),
                    output_encoding      : 'data:application/octet-stream;charset=utf8,',
                    output_formatContent : function(config, widgetOptions, data) {
                        if (!data.$cell.parent().hasClass('tr-collapsible')) {
                            return data.content;
                        }
                    }
                },
                headers: {
                    '.col-id': {
                        sorter: false
                    },
                }
            });
            $('.tablesorter-childRow td').hide();
            $('.tablesorter').delegate('.toggle', 'click' ,function() {
                $(this).closest('tr').nextUntil('tr:not(.tablesorter-childRow)').find('td').toggle(500, 'linear');
                return false;
            });
        }
    });
}

function fetchHistory(invoice_id) {
    $element = $('#invoice-history' + invoice_id);
    $('#btn-history' + invoice_id).hide();
    $.ajax({
        type: 'GET',
        url: '/api/summary/invoice/' + invoice_id + '/history',
        success: function(response) {
            $element.html(response);
        }
    });
}
