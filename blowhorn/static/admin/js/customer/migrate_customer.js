$ = django.jQuery;
$(document).ready(function () {
    $('select[name="customer"]').prop('disabled', 'disabled');
    $('select[name="action"]').on('change', function () {
        var action = $('select[name="action"]').val();
        switch (action) {
            case 'merge_customer':
                $('select[name="customer"]').prop('disabled', false);
                break;

            default:
                console.log('No actions');
        }
    });
});
