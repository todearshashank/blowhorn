(function($) {
    function toggleAsset() {
        if ($('#id_purpose').val() === 'asset' ||
        ($('.field-status div p').length && $('.field-status div p')[0].innerText === 'Approved')) {
            $('.field-asset').show();
        } else {
            $('.field-asset').hide();
        }
    }

    $(document).ready(function() {
        toggleAsset();
        $('#id_purpose').on('change', function() {
            toggleAsset();
        });
    });
})(django.jQuery);
