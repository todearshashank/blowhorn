$$ = jQuery;
var fsmButtonName = null;
function submitForm() {
    $(`input[name="${fsmButtonName}"]`).click();
}
(function ($) {
    $(document).ready(function () {
        var btn_fsm_close = $$('.transition-close');
        var btn_fsm_approve = $$('.transition-approve');
        var total_close_buttons = btn_fsm_close.length;
        var total_approve_buttons = btn_fsm_close.length;

        // Load modal template
        $("#content-main > form").append('<div id="action-confirmation-modal-container"></div>');
        $("#action-confirmation-modal-container").load("/static/admin/html/drivers/action_confirmation_modal.html");

        $("#content-main > form").append('<div id="confirmation-modal-container"></div>');
        $("#confirmation-modal-container").load("/static/admin/html/drivers/close_driver_payment.html");

        if (total_close_buttons) {
            btn_fsm_close.addClass('close-dp').removeClass('transition-close');
        }

        if (total_approve_buttons) {
            btn_fsm_approve.addClass('transition-approve_class').removeClass('transition-approve');
        }

        function alterAttr(buttons, btnCount, name) {
            for (var i = 0; i < btnCount; i++) {
                btn = buttons[i];
                if (!btn) {
                    return;
                }
                btn.setAttribute('name', name);
                btn.setAttribute('type', 'button');
            }
        }

        function modifyBtnAttr(fsm_action) {
            $$("#fsm-action").text(fsm_action);
            $$('#btn-fsm-submit').addClass(`transition-${fsm_action}`);
            $$('#btn-fsm-submit').attr('name', `_fsmtransition-status-${fsm_action}`);
        }

        function modifyConfirmBtnAttr(fsm_action) {
            fsmButtonName = `_fsmtransition-status-${fsm_action}`;
            var btn = `<input type="submit" class="transition-${fsm_action}" value="Yes" name="${fsmButtonName}">`;
            $("#content-main > form").append(btn);
        }

        $(".close-dp").click(function () {
            $$("#modal-fsm-close").modal('show');
            modifyBtnAttr('close');
        });

        $('.transition-approve_class').on('click', function (e) {
            $$("#modal-confirm").modal('show');
            modifyConfirmBtnAttr('approve');
        });

        alterAttr(btn_fsm_close, total_close_buttons, 'btn-close');
        alterAttr(btn_fsm_approve, total_approve_buttons, 'btn-approve');
    });
})(django.jQuery);
