(function ($) { $(function () {

    var reject = jQuery('.transition-reject');
    var inactive = jQuery('.transition-inactivate');
    var submitButtons = $('input[type="submit"]');

    var reject_html = '<div id="RejectmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    reject_html += '<div class="modal-dialog" style="width:450px;">';
    reject_html += '<div class="modal-content">';
    reject_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> UnApprove Form</h4></div>';

    reject_html += '<div class="modal-body">';
    reject_html += '<p id="document">';
    reject_html += '<textarea rows="5" cols="40" name="reason" id="id_reason"></textarea>'
    reject_html += '<br>';
    reject_html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    reject_html += '<input id="id_reject" class="default transition-reject_class pull-right" name="_fsmtransition-status-reject" type="submit" value="submit" >';
    reject_html += '</p>';
    reject_html += '<br>';
    reject_html += '<div class="modal-footer">';

    reject_html += '</div>';  // footer
    reject_html += '</div>';


    var inactive_html = '<div id="inactivemodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    inactive_html += '<div class="modal-dialog" style="width:450px;">';
    inactive_html += '<div class="modal-content">';
    inactive_html += '<div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"> UnApprove Form</h4></div>';

    inactive_html += '<div class="modal-body">';
    inactive_html += '<p id="document">';
    inactive_html += '<textarea rows="5" cols="40" name="inactive_reason" id="id_reasoninactive"></textarea>'
    inactive_html += '<br>';
    inactive_html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    inactive_html += '<input id="id_rejects" class="default transition-inactive_class pull-right" name="_fsmtransition-status-inactivate" type="submit" value="submit" >';
    inactive_html += '</p>';
    inactive_html += '<br>';
    inactive_html += '<div class="modal-footer">';

    inactive_html += '</div>';  // footer
    inactive_html += '</div>';

    $("#documentevent_form").append(reject_html);
    $("#documentevent_form").append(inactive_html);

    $(document).ready(function () {
        $('.field-reason').remove();
        $('.field-reasoninactive').remove();

        reject.addClass("transition-reject_class").removeClass('transition-reject');
        inactive.addClass("transition-inactive_class").removeClass('transition-inactivate');

        if (reject[0]) {
                reject[0].removeAttribute('name');
                reject[0].setAttribute('value' ,'reject document');
                reject[0].setAttribute('type', 'button');
            }
        if (inactive[0]) {
                inactive[0].removeAttribute('name');
                inactive[0].setAttribute('value' ,'inactive document');
                inactive[0].setAttribute('type', 'button');
            }

        $(".transition-reject_class").click(function () {
            $('h4').html('Provide Reason for Rejection.');
            $('#id_reject')[0].setAttribute('name', '_fsmtransition-status-reject');
            jQuery("#RejectmodalWindow").modal('show');
        });

         $(".transition-inactive_class").click(function () {
            $('h4').html('Provide Reason for inactive.');
            $('#id_rejects')[0].setAttribute('name', '_fsmtransition-status-inactivate');
            jQuery("#inactivemodalWindow").modal('show');
        });

        $('#id_rejects').on('click', function (e) {
            $('#id_reason').val($('#id_reasoninactive').val());
            if (!$('#id_reason').val().trim()) {
                e.preventDefault();
                alert('Provide a reason');
            }
        });

         $('#id_reject').on('click', function (e) {
            if (!$('#id_reason').val().trim()) {
                e.preventDefault();
                alert('Provide a reason');
            }
        });



    });
});
})(django.jQuery);
