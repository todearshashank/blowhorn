function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

(function ($) {
    const TIMEOUT_MS = 5000;
    toastr.options.closeButton = true;
    toastr.options.closeDuration = 300;
    toastr.options.closeEasing = 'swing';
    toastr.options.progressBar = true;

//        '<td class="field-device_type">%(device_type)s</td>' +
//        '<td class="field-get_booking_type">%(booking_type)s</td>' +
    var spotTemplate = '<tr class="row1" id=%(pk)s>' +
        '<td class="field-number">' +
        '<a href="/admin/order/spotorder/%(pk)s/change/">' +
        ' %(order_number)s</td>' +
				'<td class="field-status">%(status)s</td>' +
        '<td class="field-tracking_status">%(tracking_status)s</td>' +
        '<td class="field-customer nowrap">%(customer)s</td>' +
        '<td class="field-customer_category">%(customer_category)s</td>' +
        '<td class="field-hub nowrap">%(hub)s</td>' +
        '<td class="field-city nowrap">%(city)s</td>' +
        '<td class="field-driver nowrap">%(driver)s</td>' +
        '<td class="field-date_placed nowrap">%(date_placed)s</td>' +
        '<td class="field-pickup_datetime">%(pickup_datetime)s</td>' +
        '<td class="field-get_trip_vehicle_class">%(vehicle_class)s</td>' +
        '<td class="field-get_trip_status">%(trip_status)s</td>' +
        '</tr>';

    var toastHeadTemplate = '<span class="notification-type">%(order_number)s</span>';
    var toastContentTemplate = '<table id="toast-content">' +
        '<tr><th>City:</th><td>%(city)s</td></tr>' +
        '<tr><th>Customer:</th><td>%(customer)s</td></tr>' +
        '<tr><th>Hub:</th><td>%(hub)s</td></tr>' +
        '</table>';

    function updateTable(result) {
        var $tableBody = $('#result_list').find('tbody');
        var rows = $tableBody.find('tr');
        var recordFound = false;
        for (var i = 0; i < rows.length; i++) {
            var orderNumber = rows[i].firstChild;
            if (orderNumber.innerText.trim() === result.order_number.trim()) {
                rows[i].remove();
                recordFound = true;
                break;
            }
        }
        addNewRecord(result, 'flash-new-record');
        if (result.message_type == 'new_booking')
            jQuery.playSound('/static/sound/new_booking.mp3');
        else
            jQuery.playSound('/static/sound/status_update.mp3');
    }

    function addNewRecord(result, flashRowClass) {
        var $tableBody = $('#result_list').find('tbody');
        var $form = $('#changelist-form');
        if ($tableBody.length === 0) {
            var $table = '<div class="results">' +
                '<table id="result_list">' +
                '<thead>' +
                '   <tr>' +
                '      <th scope="col" class="sortable column-number">' +
                '         <div class="text"><a href="?o=0">Order Number</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-status">' +
                '         <div class="text"><a href="?o=1">Status</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-tracking_status">' +
                '         <div class="text"><span>Tracking Status</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-customer">' +
                '         <div class="text"><a href="?o=3">Customer</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-customer_category">' +
                '         <div class="text"><a href="?o=4">Customer Category</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-hub">' +
                '         <div class="text"><a href="?o=5">Delivery Hub</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-city">' +
                '         <div class="text"><a href="?o=6">City</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-driver">' +
                '          <div class="text"><a href="?o=7">Driver</a></div>' +
                '          <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-date_placed">' +
                '         <div class="text"><a href="?o=8">Date Placed</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="sortable column-pickup_datetime">' +
                '         <div class="text"><a href="?o=9">Pickup Datetime</a></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_trip_vehicle_class">' +
                '         <div class="text"><span>Vehicle Class</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '      <th scope="col" class="column-get_trip_status">' +
                '         <div class="text"><span>Trip Status</span></div>' +
                '         <div class="clear"></div>' +
                '      </th>' +
                '   </tr>' +
                '</thead><tbody></tbody></table>' +
                '</div>';
            $form.prepend($table);
        }
        var $newOrder = sprintf(spotTemplate, result);
        var $parsedHtml = $.parseHTML($newOrder);
        $tableBody.prepend($parsedHtml);

        var toastHead = sprintf(toastHeadTemplate, result);
        var toastContent = sprintf(toastContentTemplate, result);
        toastr.info(toastContent, toastHead);

        $('#' + result.pk).addClass(flashRowClass);
        setTimeout(function () {
            $('#' + result.pk).removeClass(flashRowClass);
        }, TIMEOUT_MS);
    }

    // function updateTable(result) {
    //     if (result.message_type === messageTypeNew) {
    //         addNewRecord(result, 'flash-new-record');
    //     } else {
    //         updateRecord(result);
    //     }
    // }

    function activateFirebaseListener() {
        firebase.database().ref('admin-spot').on('child_changed', function (snapshot) {
            var current_order = snapshot.val();
			console.log(current_order);
            var isSpotOrderPage = $('body.model-spotorder').length > 0;
            if (isSpotOrderPage && current_order) {
                var result = JSON.parse(current_order.v1);
                console.log('RESULT: ', result);
                city_filter_applied = getParameterByName('city__id__exact');

                // DateRange filter has the date in 20-02-2018 format.
                // Got to reverse it to convert to (YYYY-MM-DD) for comparison
                pickup_gt_filter_applied = getParameterByName('pickup_datetime__gt');
                if (pickup_gt_filter_applied) {
                    pickup_gt_filter_applied = pickup_gt_filter_applied.split('-').reverse().join('-');
                }
                pickup_gte_filter_applied = getParameterByName('pickup_datetime__gte');
                if (pickup_gte_filter_applied) {
                    pickup_gte_filter_applied = pickup_gte_filter_applied.split('-').reverse().join('-');
                }
                pickup_lt_filter_applied = getParameterByName('pickup_datetime__lt');
                if (pickup_lt_filter_applied) {
                    pickup_lt_filter_applied = pickup_lt_filter_applied.split('-').reverse().join('-');
                }
                pickup_lte_filter_applied = getParameterByName('pickup_datetime__lte');
                if (pickup_lte_filter_applied) {
                    pickup_lte_filter_applied = pickup_lte_filter_applied.split('-').reverse().join('-');
                }

                console.log(city_filter_applied);
                console.log(pickup_gte_filter_applied, pickup_lte_filter_applied);
                console.log(pickup_gt_filter_applied, pickup_lt_filter_applied);

                // check if any filters have been applied, the incoming data matches it.
                // result.pickup_datetime_compare is in the format 2018-02-10 (YYYY-MM-DD)
                if (result.order_type === orderTypeSpot &&
                    (!city_filter_applied || city_filter_applied == result.city_id) &&
                    (!pickup_gte_filter_applied || result.pickup_datetime_compare >= pickup_gte_filter_applied) &&
                    (!pickup_gt_filter_applied || result.pickup_datetime_compare > pickup_gt_filter_applied) &&
                    (!pickup_lt_filter_applied || result.pickup_datetime_compare < pickup_lt_filter_applied) &&
                    (!pickup_lte_filter_applied || result.pickup_datetime_compare <= pickup_lte_filter_applied)
                ) {
                    updateTable(result);
                }
            }
        }, function (err) {
            console.log(err);
        });
    }

    function firebaseSignIn(firebase_auth_token) {
        toastr.info("Registering for real-time updates", "Registering");
        firebase.auth().signInWithCustomToken(firebase_auth_token).then(function () {
            toastr.remove();
            toastr.success("Real-time updates are active", "Success");
            activateFirebaseListener();
        }).catch(function (error) {
            console.log(error.message);
            toastr.success("Failed to activate real-time updates." +
                "<br/>Refresh the page again.", "Success");
        });
    }

    function manageLabelNodeList(lableNodes, displayType) {
        if (!lableNodes) {
            return;
        }
        for (var i = 0; i < lableNodes.length; i++) {
            lableNodes[i].style.display = displayType;
        }
    }

    $(document).ready(function () {
        $.ajax({
            type: 'GET',
            url: firebaseAuthTokenUrl,
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                firebaseSignIn(data.firebase_auth_token);
            },
            error: function (data, textStatus, xhr) {
                console.log(data);
            }
        });
    });

})(django.jQuery);
