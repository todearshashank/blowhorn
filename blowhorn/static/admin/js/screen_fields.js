/*
Contains custom javascript function codes used in contract admin
*/

(function($) {
    $(function() {
        var url = window.location.href;
        parameters = $('.field-parameters');
        data_type = $('.field-data_type');

        function toggleHide(field_type) {
            console.log(field_type)
            if (field_type == 1) {
                data_type.show() && parameters.hide()
            }
            else if (field_type == 2) {
                data_type.hide() && parameters.show()
            }
        }

        // for edit page id is not present for contract type
        var spil = url.search("change");
        selectField = $('.field-field_type > div > p');

        if (spil != -1 && selectField.length > 0) {
            field_type = selectField[0].innerHTML;
            console.log(selectField)
        } else {
            selectField = $('#id_field_type');
            field_type = selectField.val();
            console.log("else",field_type)
        }

        // show / hide on load based on pervious value of selectField
        toggleHide(field_type);

        // show / hide on change
        selectField.change(function () {
            toggleHide($(this).val());
        })
    });
})(django.jQuery);