(function($) {$(function() {

    var existing_customer = $("#id_existing_customer");
    var existing_customer_block = $("div.form-row.field-existing_customer")
    var order_number = $("div.form-row.field-order_number");
    var call_type = $("div.form-row.field-call_type > div > p")

    function showCheckbox() {
        value = call_type.text();
        console.log(value)
        if (value == 'Incoming') {

            existing_customer_block.show();
        }
        else {
            existing_customer_block.hide();
        }
    }

    showCheckbox();

    function askOrderNumber() {
        value = existing_customer.prop("checked");
        value ? order_number.show() : order_number.hide();
    }

    askOrderNumber();

    // show / hide on click
    $("#id_existing_customer").on("change", function(e) {
        askOrderNumber();
    });

});
})(django.jQuery);