(function($) {$(function() {

    var schedule_off_button = jQuery('.transition-scheduled_off');
    var no_show_button = jQuery('.transition-no_show');
    var completed_button = jQuery('.transition-complete');

    var html =  '<div id="ConfirmationmodalWindow" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog" style="width:450px;">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '<h4> Are you sure you want to mark this trip as <b>no show</b> ?</h4>';
    html += '</div>';
    html += '<div class="modal-body">';
    html += '<p id="trip_status">';
    html += 'Click Confirm to continue or Cancel to abort<br>';
    html += '</p>';
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<input class="close pull-left" data-dismiss="modal" style="opacity:1; background-color: #ff3333; color: #fff;" type="submit" value="Cancel">';
    html += '<input id="submitButton" class="default transition-no_show pull-right" name="_fsmtransition-status-no_show" type="submit" value="Confirm">';
    html += '</div>';  // footer
    html += '</div>';  // ConfirmationmodalWindow

    $("#trip_form").append(html);

    $(document).ready(function(){

        schedule_off_button.addClass("schedule_off_class").removeClass('transition-scheduled_off');
        no_show_button.addClass("no_show_class").removeClass('transition-no_show');
        completed_button.addClass("complete_class").removeClass('transition-complete');

        if(schedule_off_button[0]) {
            schedule_off_button[0].removeAttribute('name');
            schedule_off_button[0].setAttribute('type', 'button');
        }

        if(no_show_button[0]) {
            no_show_button[0].removeAttribute('name');
            no_show_button[0].setAttribute('type', 'button');
        }

        if(completed_button[0]) {
            completed_button[0].removeAttribute('name');
            completed_button[0].setAttribute('type', 'button');
        }

        $(".no_show_class").click(function() {
            $('h4').html('Are you sure you want to mark trip as <b>No Show</b> ?');
            $('#submitButton').removeClass('transition-scheduled_off transition-complete').addClass('transition-no_show');
            $('#submitButton')[0].setAttribute('name', '_fsmtransition-status-no_show');
            jQuery("#ConfirmationmodalWindow").modal('show');
        });

        $(".schedule_off_class").click(function() {
            $('h4').html('Are you sure you want to mark trip as <b>Schedule off</b> ?');
            $('#submitButton').removeClass('transition-no_show transition-complete').addClass('transition-scheduled_off');
            $('#submitButton')[0].setAttribute('name', '_fsmtransition-status-scheduled_off');
            jQuery("#ConfirmationmodalWindow").modal('show');
            
        });
        
        $(".complete_class").click(function() {
            $('h4').html('Are you sure you want to mark trip as <b>Complete</b> ?');
            $('#submitButton').removeClass('transition-no_show transition-scheduled_off').addClass('transition-complete');
            $('#submitButton')[0].setAttribute('name', '_fsmtransition-status-complete');
            jQuery("#ConfirmationmodalWindow").modal('show');
            
        });

        $("#trip_status").on('submit', function() {
            $("#trip_form").submit();
            $('.modal.in').modal('hide');
        });
    });    

});
})(django.jQuery);