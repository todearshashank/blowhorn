(function ($) {
    $(function () {
        var tables = $('#driver_document-group > div > fieldset > table > tbody');
        var count = tables[0].childElementCount;
        var rows = tables[0].getElementsByTagName("tr");
        var no_of_rows = 0;
        for (var row_number = 0; row_number <= count - 2; row_number++) {
            var table_data = rows[row_number].getElementsByTagName("td")[1];
            if (table_data.className == 'field-file' &&
                table_data.getElementsByTagName("label").length > 0) {
                no_of_rows = no_of_rows + 1;
            }
        }
        var row = tables[0].insertRow(rows.length - 1);
        var col_html = '<b>Total number of documents uploaded: ' + no_of_rows;
        var col_row = row.insertCell(0);
        col_row.innerHTML = col_html;
        col_row.colSpan = 6;
    });
})(django.jQuery);
