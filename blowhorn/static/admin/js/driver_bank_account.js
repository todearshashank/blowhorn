/*
    Function to disable pasting into driver bank account
    confirmation field
*/

(function($) {
    $(function() {
        $('#id_account_number2').on("cut copy paste drop",function(e) {
            e.preventDefault();
            $(this).addClass('no_copy');
        });

        $('#id_account_number2').on("blur keydown",function(e) {
            $(this).removeClass('no_copy');
        });
    });
})(django.jQuery);