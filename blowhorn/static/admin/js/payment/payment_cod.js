var MODAL_COD_ID = 'modal-handed_over_to_cms';
var selectedAction = null;

function manageLabelNodeList(labelNodes, displayType) {
    if (!labelNodes) {
        return;
    }
    for (var i = 0; i < labelNodes.length; i++) {
        labelNodes[i].style.display = displayType;
    }
}

(function ($) {
    $(function () {
        var btnActionGo = jQuery('.actions button');
        manageLabelNodeList($('input[name="ack_proof_document"').prop('labels'), 'none');

        $("#content-main #changelist-form, #content-main > form").append('<div id="handed_over_to_cms-modal-container"></div>');
        $("#handed_over_to_cms-modal-container").load("/static/admin/html/payment/payment_cod.html", initModalListener);

        function initModalListener() {
            jQuery('#' + MODAL_COD_ID).on('show.bs.modal', function (event) {
                $('#handed_over_to_cms-title').text('Settlement');
                $('label[for="id_ack_number"]').text('UTR Number');
                $('#ack-proof-document-container').hide();
                if (selectedAction === 'handed_over_to_cms') {
                    $('#handed_over_to_cms-title').text('Handed over to CMS');
                    $('label[for="id_ack_number"]').text('Acknowledgement Number');
                    $('#ack-proof-document-container').show();
                }
            });
        }


        $(document).ready(function () {
            $('#changelist-form').prop("enctype", "multipart/form-data");
            if (btnActionGo.length) {
                btnActionGo[0].setAttribute('type', 'button');
                btnActionGo[0].setAttribute('id', 'submit-action');
                btnActionGo[0].removeAttribute('name');
            }

            $('#submit-action').click(function () {
                var action = $('select[name="action"]').val();
                selectedAction = action;
                switch (action) {
                    case 'handed_over_to_cms':
                        jQuery(`#${MODAL_COD_ID}`).modal('show');
                        break;

                    case 'mark_as_settled':
                        jQuery(`#${MODAL_COD_ID}`).modal('show');
                        break

                    default:
                        $('#changelist-form').submit();
                }
            });
        });
    });
})(django.jQuery);