$$ = jQuery;
(function($) {
    $(document).ready(function () {
        var btnFsmReject = $$('.transition-reject');
        var total_buttons = 0;
        var buttons = null;

        // Load modal template
        $("#content-main > form").append('<div id="confirmation-modal-container"></div>');
        $("#confirmation-modal-container").load('/static/admin/driver/html/payment_adjustment_fsm_remark.html', initListener);

        if (btnFsmReject.length) {
          btnFsmReject.addClass('reject-adjustment').removeClass('transition-reject');
          total_buttons = btnFsmReject.length;
          buttons = btnFsmReject;
        }

        for (var i = 0; i < total_buttons; i++) {
          buttons[i].removeAttribute('name');
          buttons[i].setAttribute('type', 'button');
        }

        function modifyBtnAttr(fsm_action) {
          $$("#fsm-action").text(fsm_action);
          var btnSubmit = $$('#btn-fsm-submit');
          btnSubmit.addClass('transition-' + fsm_action);
          btnSubmit.attr('name', '_fsmtransition-status-' + fsm_action);
        }

        $('.reject-adjustment').click(function () {
          $$('#modal-fsm-action').modal('show');
          modifyBtnAttr('reject');
        });

        function initListener() {
          $('#reject-reason').on('keyup paste', function () {
            $('#id_remarks').val($(this).val()).trigger('change');
          });

          $$('#modal-fsm-action').on('show.bs.modal', function (e) {
            $$('#reject-reason').val('');
          });

          $$('#btn-fsm-submit').on('click', function (e) {
            if (!$('#id_remarks').val()) {
              e.preventDefault();
              e.stopPropagation();
              alert('Provide the reason for rejection.');
            }
          });
        }
    });
})(django.jQuery);
