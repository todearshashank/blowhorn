from django.db import models
from django.contrib.auth import get_user_model
from blowhorn.oscar.core.loading import get_model
from django.contrib.gis.db.models import PointField
import re
import sys, os
from django import db
from django.utils import timezone
import datetime
from django.conf import settings
import phonenumbers
from dateutil.parser import parse
import pytz

User = get_user_model()
Driver = get_model('driver', 'Driver')
Vehicle = get_model('vehicle', 'Vehicle')
VehicleClass = get_model('vehicle', 'VehicleClass')
VehicleModel = get_model('vehicle', 'VehicleModel')
BodyType = get_model('vehicle', 'BodyType')
BankAccount = get_model('driver', 'BankAccount')
DriverRating = get_model('driver', 'DriverRating')
DriverAddress = get_model('driver', 'DriverAddress')
DriverDocument = get_model('driver', 'DriverDocument')
AlternateContact = get_model('driver', 'AlternateContact')
# Country = get_model('address', 'Country')
City = get_model('address', 'City')
Order = get_model('order', 'Order')
Trip = get_model('trip', 'Trip')

cntry = Country.objects.get(
    iso_3166_1_a2='IN',
    iso_3166_1_a3='IND',
    iso_3166_1_numeric='356',
    printable_name='India',
    name='India',
    display_order=0,
    is_shipping_country=True)

dummy_date = datetime.date(1900, 1, 1)
dummy_datetime = datetime.datetime(1900, 1, 1, 0, 0, 0, tzinfo=pytz.UTC)
dummy_phone = phonenumbers.parse('0000000000', settings.DEFAULT_COUNTRY.get('country_code'))
dummy_email = '%s@%s' % (dummy_phone.national_number, settings.DEFAULT_DOMAINS.get('driver'))
dummy_user = 'DUMMY_USER'
dummy_user = User.objects.get_or_create(
                phone_number=dummy_phone,
                email=dummy_email,
                date_joined=dummy_datetime,
                is_active=False,
                name=dummy_user)


def _load_driver(driver, verbose):
    """
    Loads the Driver JSON to a set of RDBMS tables in new backend
    """
    try:
        with db.transaction.atomic():

            phone_number = phonenumbers.parse(driver['__key__']['name'],
                                              settings.DEFAULT_COUNTRY.get('country_code'))
            email = '%s@%s' % (phone_number.national_number,
                               settings.DEFAULT_DOMAINS.get('driver'))

            if verbose:
                print("email %s" % (email))

            doj = driver.get('date_of_joining', None)
            if not doj:
                doj = dummy_datetime
            else:
                doj = parse(doj)

            pan = re.sub('[\s+|/-]', '', driver.get('pan_card', 'XXXXX0000X')).upper()

            driver_name = driver.get('name', None)

            u, created = User.objects.get_or_create(
                phone_number=phone_number,
                email=email,
                date_joined=doj,
                is_active=True,
                name=driver_name)

            if verbose:
                print("User %s created" % (u))

            vehicleclass = driver['vehicle'].get('vehicle_class', 'DUMMY_VC')

            vc, created = VehicleClass.objects.get_or_create(
                commercial_classification=vehicleclass,
                licence_category_code='LCV',
                licence_category_desc='Light Commercial Vehicle')

            if verbose:
                print("Vehicle Class created %s" % (vehicleclass))

            vehiclemodel = driver['vehicle'].get('vehicle_model', 'DUMMY_VM')

            vm, created = VehicleModel.objects.get_or_create(
                model_name=vehiclemodel,
                capacity='',
                dimensions='',
                vehicle_class=vc)

            if verbose:
                print("Vehicle Model %s created" % (vehiclemodel))
            # Default body_type to DUMMY if not available at source
            b, created = BodyType.objects.get_or_create(
                body_type=driver['vehicle'].get('body_type', 'DUMMY_BT'))

            if verbose:
                print("Body Type %s created"  % (b))

            v, created = Vehicle.objects.get_or_create(
                registration_certificate_number=re.sub('[\s+|/-]', '', driver['vehicle']['registration_number']).upper(),
                body_type=b,
                vehicle_model=vm)

            if verbose:
                print("Vehicle %s created" % (v))

            # DUMMY bank account if not present at source
            if driver.get('bank_account', 'empty') == 'empty':
                account_name = 'DUMMY_ACCOUNT'
                account_number = 'DUMMY_NUMBER'
                ifsc_code = 'DUMMY_CODE'
            else:
                account_name = driver['bank_account']['name']
                account_number = driver['bank_account']['number']
                ifsc_code = driver['bank_account']['bank_code']

            ba, created = BankAccount.objects.get_or_create(
                account_name=account_name,
                account_number=account_number,
                ifsc_code=ifsc_code)

            if verbose:
                print("Bank Account created %s" % (ba))

            dr, created = DriverRating.objects.get_or_create(
                cumulative_rating=driver['rating']['value'],
                num_of_feedbacks=driver['rating']['count'])

            if verbose:
                print("Driver rating %s created" % (dr))

            city, created = City.objects.get_or_create(name=driver['operating_city'])

            if verbose:
                print("City %s created" % (city))

            da1, created = DriverAddress.objects.get_or_create(
                title='Mr',
                first_name=driver['name'],
                last_name='',
                line1=driver['permanent_address']['line'],
                line2=driver['permanent_address']['area'],
                line3=driver['permanent_address']['landmark'],
                line4=driver['permanent_address']['city'],
                state=driver['permanent_address']['state'],
                postcode=driver['permanent_address']['postal_code'],
                country=cntry)

            if verbose:
                print("Driver Address %s Created" % (da1))

            da2, created = DriverAddress.objects.get_or_create(
                title='Mr',
                first_name=driver['name'],
                last_name='',
                line1=driver['address']['line'],
                line2=driver['address']['area'],
                line3=driver['address']['landmark'],
                line4=driver['address']['city'],
                state=driver['address']['state'],
                postcode=driver['address']['postal_code'],
                country=cntry)

            ref1, created = AlternateContact.objects.get_or_create(
                name=driver['reference1']['contact_name'],
                phone_number=driver['reference1']['contact_no'],
                relationship=driver['reference1']['relationship'])

            ref2, created = AlternateContact.objects.get_or_create(
                name=driver['reference2']['contact_name'],
                phone_number=driver['reference2']['contact_no'],
                relationship=driver['reference2']['relationship'])

            ec1, created = AlternateContact.objects.get_or_create(
                name=driver['emergency_contact1']['contact_name'],
                phone_number=driver['emergency_contact1']['contact_no'],
                relationship=driver['emergency_contact1']['relationship'])

            ec2, created = AlternateContact.objects.get_or_create(
                name=driver['emergency_contact2']['contact_name'],
                phone_number=driver['emergency_contact2']['contact_no'],
                relationship=driver['emergency_contact2']['relationship'])

            if verbose:
                print("Driver Address %s Created" % (da2))

            # Default DOB to 1900 if not present at source
            dob = driver.get('date_of_birth', None)
            if not dob:
                dob = dummy_date
            else:
                dob = parse(dob)

            changed_by = driver.get('changed_by', None)
            if not changed_by:
                changed_by_user = dummy_user
            else:
                changed_by_user, created = User.objects.get_or_create(email=changed_by)

            if verbose:
                print("changed_by_user %s formed" % (changed_by))

            created_by = driver.get('created_by', None)
            if not created_by:
                created_by_user = dummy_user
            else:
                created_by_user, created = User.objects.get_or_create(email=created_by)

            if verbose:
                print("created_by %s formed" % (created_by))


            changed_on = driver.get('changed_on', None)
            if not changed_on:
                changed_on_date = dummy_datetime
            else:
                changed_on_date = parse(changed_on)

            if verbose:
                print("changed_on %s formed" % (changed_on_date))

            balance = driver.get('balance', 0)

            created_on = driver.get('created_on', None)
            if not created_on:
                created_on_date = dummy_datetime
            else:
                created_on_date = parse(created_on)

            if verbose:
                print("created_on %s formed" %(created_on_date))

            d, created = Driver.objects.get_or_create(
                user=u,
                source=driver.get('source', 'DUMMY'),
                date_of_birth=dob,
                owner_contact_no=driver['owner_contact_no'],
                driving_license_number=re.sub('[\s+|/-]', '', driver['licence_number']).upper(),
                operating_city=city,
                marital_status=driver['marital_status'],
                status=driver['status'],
                bank_account=ba,
                driver_rating=dr,
                permanent_address=da1,
                current_address=da2,
                created_date=created_on_date,
                #created_by=created_by_user,
                modified_date=changed_on_date,
                #modified_by=changed_by_user,
                comments=driver['comments'],
                contract_type=driver.get('contract_type', 'DUMMY'),
                referred_by=driver.get('referred_by', 'DUMMY_REFERER'),
                pan=pan,
                balance=balance,
                emergency_contact1=ec1,
                emergency_contact2=ec2,
                reference_contact1=ref1,
                reference_contact2=ref2)

            if verbose:
                print("Driver %s addeded" % (d))

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

def _load_customer(customer, verbose):
    try:
        if not customer['linked_account'].get('__key__', None):
            return
        print(customer['name'], customer['mobile'], customer['category']['pricing'], customer['creation_time'], customer['id_type'], customer['__key__']['name'])
        print(customer['address']['line'], customer['linked_account'], customer['rating']['count'])
    except KeyError as e:
        print(e)

def load_drivers(drivers, verbose):
    for driver in drivers:
        _load_driver(driver, verbose)
        # Do the Driver-Vehicle mapping only once driver and vehicle are saved

def load_customers(customers, verbose):
    for customer in customers:
        _load_customer(customer, verbose)

def load_orders(orders, verbose):
    print(orders)

