import argparse
from django.core.management.base import BaseCommand, CommandError
from blowhorn.oscar.core.loading import get_model
from blowhorn.gaemigrations import gaeutils
import json

Driver = get_model('driver', 'Driver')


class Command(BaseCommand):
    help = """One-Off Migration Utility from GAE Datastore."""

    def add_arguments(self, parser):
        parser.add_argument('--module', action='store', dest='module',
            help='module to migrate [driver|customer|orders]')
        parser.add_argument('--verbose', action='store_true', default=False,
                            help='Verbose')

    def _process_customers(self, verbose):
        """ Expects `customers.json` containing the JSON data from BigQuery GAE"""
        with open('customers.json') as f:
            customers = json.loads(f.read())

        gaeutils.load_customers(customers, verbose)

    def _process_orders(self, verbose):
        """ Expects `orders.json` containing the JSON data from BigQuery GAE"""
        with open('orders.json') as f:
            orders = json.loads(f.read())

        gaeutils.load_orders(orders, verbose)

    def _process_drivers(self, verbose):
        """Expects `drivers.json` file containing the data from BigQuery GAE """
        # Loop through the JSON file
        with open('drivers.json') as f:
            drivers = json.loads(f.read())
        gaeutils.load_drivers(drivers, verbose)

    def handle(self, *args, **options):
        verbose = False
        if options['verbose']:
            verbose=True
        module = options['module']
        if module == 'driver':
            self._process_drivers(verbose)
        elif module == 'customer':
            self._process_customers(verbose)
        elif module == 'order':
            self._process_orders(verbose)
        else:
            print("invalid option", options['module'])
