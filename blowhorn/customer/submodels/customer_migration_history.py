import json
from django.conf import settings
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.models import BaseModel


class CustomerMigrationHistory(BaseModel):
    CATEGORY_MERGE_CUSTOMER = 'merge_customer'
    CATEGORY_DATA_DELETE_REQUEST = 'data_delete_request'

    customer = models.CharField(_('Customer'), max_length=255)
    category = models.CharField(max_length=255, null=True, blank=True,
        default=CATEGORY_MERGE_CUSTOMER)
    number_of_customers = models.IntegerField(_('No. of records'),
                                    help_text=_('No. of records affected'),
                                              default=0)
    log_dump = JSONField(_('Log'), help_text=_('Log'))

    class Meta:
        verbose_name = _('Customer Data Log')
        verbose_name_plural = _('Customer Data Logs')

    @property
    def log_dump_json(self):
        if isinstance(self.log_dump, str):
            self.log_dump = json.loads(self.log_dump)
            if isinstance(self.log_dump, str):
                return json.loads(self.log_dump)
        return self.log_dump
