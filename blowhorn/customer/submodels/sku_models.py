from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.models import BaseModel
from blowhorn.customer.models import Customer


class SKU(BaseModel):
    """
    Template for the product / SKU
    """
    name = models.CharField(
        _("SKU"), max_length=255, unique=True, db_index=True,
        validators=[
            RegexValidator(
                regex=r'^[-,_\w]*$',
                message=_("Only AlphaNumerics and , - _ are allowed"))])

    include_in_sellshipmentslab = models.BooleanField(default=False)
    objects = models.Manager()

    class Meta:
        verbose_name = _("SKU")
        verbose_name_plural = _("SKUs")

    def __str__(self):
        return self.name


class SKUCustomerMap(BaseModel):
    """
    SKU to Customer Mapping
    """
    sku = models.ForeignKey(SKU, on_delete=models.PROTECT)

    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)

    class Meta:
        unique_together = ('sku', 'customer')
        verbose_name = _("SKU Customer Map")
        verbose_name_plural = _("SKU Customer Maps")

    def __str__(self):
        return '%s-%s' % (self.sku.name, self.customer.name)


class ContainerType(BaseModel):
    """
    Define customer container type
    """

    customer = models.ForeignKey(Customer, related_name='asset_types',
                                 on_delete=models.PROTECT, blank=False,
                                 null=False)

    name = models.CharField(_('Container Type'), null=False, blank=False,
                            max_length=50)

    return_asset = models.BooleanField(_('Return to customer'), default=True)

    class Meta:
        verbose_name = _('Customer Container Type')
        unique_together = ("customer", "name")

    def __str__(self):
        return self.name
