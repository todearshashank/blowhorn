import copy
import re
import pytz
from datetime import date, datetime, timedelta, time
from collections import defaultdict
from functools import reduce

from django.conf import settings
from django.utils import timezone
from django.contrib.humanize.templatetags.humanize import intcomma
from django.contrib.postgres.fields import JSONField
from django.db import models, transaction
from django.db.models import Sum, Q
from django.core.files.base import ContentFile
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _
from postgres_copy import CopyManager
from django.contrib.postgres.indexes import BrinIndex
from django.core.exceptions import ValidationError
import calendar
import decimal

import simplejson as json
from audit_log.models.fields import CreatingUserField
from num2words import num2words
from sortedm2m.fields import SortedManyToManyField
from django_fsm import FSMField, transition

from blowhorn.oscar.core.validators import non_python_keyword
from recurrence.fields import RecurrenceField

from blowhorn.common.models import BaseModel
from blowhorn.common.utils import ChoiceEnum
from blowhorn.common.middleware import current_request
from blowhorn.document.utils import generate_file_path
from blowhorn.document.file_validator import FileValidator
from blowhorn.customer.constants import (CUSTOMER, CONSIGNEE, CREATED,
     APPROVED, READY_TO_BE_SENT, SENT, SHIPMENT,
     VEHICLE, UNAPPROVED,
     LABOUR, TOLL_CHARGES, INVOICE_ADJUSTMENT, INVOICE_REQUEST_STATUSES,
     PENDING, REJECTED, INVOICE_TYPE, TAX_INVOICE,
     CUSTOMER_CONFIRMED, CUSTOMER_DISPUTED, PENDING_CUSTOMER_CONFIRMATION,
     PAYMENT_MODE_OPTIONS,
     CANCELLED, PAYMENT_MODE_BANK_TRANSFER, INVOICE_EXTRA_PAGES,
     SENT_FOR_APPROVAL, REASSIGNED, INVOICE_CONFIG_STATUSES,
     PENDING_CUSTOMER_ACKNOWLEDGEMENT, REGENERATED, CREATE_CREDIT_DEBIT_NOTE,
     SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED, PAYMENT_DONE, CREDIT_NOTE,
     PARTIALLY_PAID, INVOICE_CONFIG_STATUSES, INVOICE_CONFIG_ACTIVE, INVOICE_CONFIG_INACTIVE, INVOICE_EDITABLE_STATUS,
     UPLOADED_TO_GST_PORTAL, EXPIRED)
from blowhorn.customer.invoice_transition_conditions import has_reject_and_send_permissions, \
    has_approval_permissions, has_unapproval_permissions, can_approve, \
    can_regenerate, has_reassigning_permissions, is_city_sales_manager, is_qualifiy_for_unapprove, \
    is_collection_representative, can_mark_payment_done, within_dispute_threshold_date, gst_filing_required, \
    check_gst_filing_required, within_cancel_threshold_date, has_payment_receive_permission
from blowhorn.contract.permissions import is_city_manager, is_finance_user, \
    is_city_manager_or_supervisor, is_invoice_spoc, is_invoice_supervisor, \
    has_approval_permission
from blowhorn.company.constants import CGST, SGST, IGST, VAT, COMPANY_HEADER_FOOTER_DATA
from blowhorn.customer.models import CustomerTaxInformation
from blowhorn.contract.constants import (
    CONTRACT_TYPE_FIXED,
    CONTRACT_TYPES,
    SERVICE_TAX_CATEGORIES,
    EXEMPT,
    GTA, BSS,
    SERVICE_TAX_CATEGORIES_CODE_MAP
)
from blowhorn.common.utils import generate_pdf
from blowhorn.users.models import User

NEW_FINANCIAL_YEAR_MONTH = getattr(settings, 'NEW_FINANCIAL_YEAR_MONTH', 4)
INVOICE_MULTIPLIER = getattr(settings, 'NEW_FINANCIAL_YEAR_MONTH', 1000000)


def get_invoice_history_dict(old_status, new_status, invoice, action_owner,
                             due_date, reason=None, created_by=None):
    invoice_history_data = {
        'old_status': old_status,
        'new_status': new_status,
        'invoice': invoice,
        'action_owner': action_owner,
        'due_date': due_date
    }
    if reason:
        invoice_history_data['reason'] = reason
    if created_by:
        invoice_history_data['created_by'] = created_by
    return invoice_history_data


def generate_invoice(invoice):
    if invoice.is_credit_debit_note:
        CreditDebitNote.generate_credit_debit_notes(invoice=invoice)
    else:
        # Regenerate pdf invoice.
        from blowhorn.customer.helpers.invoice_helper import InvoiceHelper
        from blowhorn.customer.admin import CustomerInvoiceAdmin
        from django.contrib.admin import AdminSite
        aggregate_levels = CustomerInvoiceAdmin(invoice, admin_site=AdminSite)._get_aggregate_levels(invoice)

        if invoice.contract_invoices.all().exists():
            contract_ids = invoice.contract_invoices.values_list('contract', flat=True)
        else:
            contract_ids = []

        InvoiceHelper().generate_invoice(
            invoice.customer, invoice.service_period_start, invoice.service_period_end, aggregate_levels, contract_ids,
            configs=invoice.invoice_configs, invoice=invoice)


def create_invoice_supporting_data_bulk(support_data):
    return InvoiceSupportingDocument.objects.bulk_create(support_data)


def create_invoice_history_bulk(history_data):
    return InvoiceHistory.objects.bulk_create(history_data)


def create_invoice_history(old_status, new_status, invoice, action_owner,
                           due_date, reason=None, created_by=None):

    reason = "%s-%s" % (reason,invoice.invoice_number)
    history_dict = get_invoice_history_dict(old_status, new_status, invoice,
                                            action_owner, due_date,
                                            reason=reason,
                                            created_by=created_by)
    return InvoiceHistory.objects.create(**history_dict)


def get_invoice_upload_path(invoice, file_name):
    """
    Returns the upload path for invoice.
    """

    from blowhorn.customer.helpers.invoice_helper import InvoiceHelper
    return InvoiceHelper.generate_file_path(invoice)


class MISDocument(BaseModel):
    from blowhorn.order.helper import path_for_supporting_document
    """
    Model to save MIS document
    """
    invoice = models.ForeignKey('customer.CustomerInvoice', on_delete=models.CASCADE)

    mis_doc = models.FileField(upload_to=path_for_supporting_document,
                               max_length=256, null=True, blank=True)

    objects = models.Manager()

class InvoiceQuerySet(models.QuerySet):

    def update(self, *args, **kwargs):
        invoice_number = kwargs.get('invoice_number', None)
        if 'invoice_number' in kwargs:
            for ci in self:
                ci_old = CustomerInvoice.objects.filter(id=ci.id).first()
                if (ci_old.invoice_number and
                        (not invoice_number or len(invoice_number)==0)):
                    raise ValidationError('Invoice number cannot be empty')

        super().update(*args, **kwargs)

class InvoiceManager(models.Manager):

    def get_queryset(self):
        return InvoiceQuerySet(self.model,
                using=self._db).select_related('customer', 'batch',
                                                 'bh_source_state',
                                                 'invoice_state',
                                                 'invoice_configs',
                                                 'related_invoice')

    def prefetch_queryset(self, **kwargs):
        qs = super().get_queryset()
        query = kwargs.pop('query', None)
        select_related = kwargs.pop('select_related', None)
        prefetch_related = kwargs.pop('prefetch_related', None)

        if query:
            if isinstance(query, dict):
                qs = qs.filter(**query)
            else:
                qs = qs.filter(query)

        if select_related:
            qs = qs.select_related(*select_related)

        if prefetch_related:
            qs = qs.prefetch_related(*prefetch_related)

        return qs

    def update(self, *args, **kwargs):
        if self.id:
            ci_old = CustomerInvoice.objects.filter(id=self.id).first()
            if (ci_old.invoice_number and
                    (not self.invoice_number or len(self.invoice_number)==0)):
                raise ValidationError('Invoice number cannot be empty')

        super().update(*args, **kwargs)


class CustomerInvoice(BaseModel):

    invoice_number = models.CharField(
        unique=True, max_length=20,
        null=True, blank=True, help_text="Invoice No."
    )

    contract = models.ForeignKey(
        'contract.Contract', null=True, on_delete=models.CASCADE
    )

    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.CASCADE)

    batch = models.ForeignKey('customer.InvoiceBatch', null=True, blank=True, on_delete=models.CASCADE)

    accounting_status=models.CharField(_("Accounting status"), max_length=40, null=True, blank=True)

    invoice_date = models.DateField(default=date.today, db_index=True)

    revenue_booked_on = models.DateField(_("Revenue Booked Month"), null=True, blank=True, db_index=True)

    service_period_start = models.DateField(_("Period start"), db_index=True)

    service_period_end = models.DateField(_("Period end"), db_index=True)

    status = FSMField(default=CREATED, protected=False, db_index=True)

    basic_per_day = models.DecimalField(
        _("Base Pay per day"), max_digits=10, decimal_places=2, default=0)

    charges_basic = models.DecimalField(
        _("Base Pay (A)"), max_digits=10, decimal_places=2, default=0)

    days_counted = models.IntegerField(
        _("Days counted"), default=0,
        help_text="No. of days in month considered for base pay."
    )

    basic_pay_calculation = JSONField(
        _("Basic Pay Calculation"), null=True, blank=True)

    calculation_level = models.CharField(
        _('Calculation Level'), max_length=20, null=True, blank=True)

    timestamp_approved = models.DateTimeField(null=True, blank=True)

    user_approved = models.ForeignKey(
        'users.User',
        null=True, blank=True, on_delete=models.PROTECT
    )

    service_tax_category = models.CharField(
        _('Tax Category'), max_length=12, choices=SERVICE_TAX_CATEGORIES,
        null=True, blank=True)

    hs_code = models.ForeignKey('contract.ServiceHSNCodeMap', null=True, blank=True,
                                on_delete=models.PROTECT,
                                verbose_name=_("HSN Code"))

    invoice_type = models.CharField(_("Invoice Type"), max_length=60,
                                    choices=INVOICE_TYPE, default=TAX_INVOICE)

    bh_source_state = models.ForeignKey(
        'address.State',
        related_name='sourced_invoices',
        verbose_name='SOURCE - Blowhorn Service Invoice State',
        help_text='This is the State where Blowhorn services - has to be present in company',
        on_delete=models.PROTECT,
        null=True
    )

    invoice_state = models.ForeignKey(
        'address.State',
        related_name='targeted_invoices',
        verbose_name='TARGET - Customer Invoice State',
        help_text='This is the State where Customer would like to be invoiced',
        on_delete=models.PROTECT,
        null=True
    )

    invoice_configs = models.ForeignKey('customer.CustomerInvoiceConfig', on_delete=models.PROTECT,
                                        null=True, blank=True)

    charges_service = models.DecimalField(
        _("Slab Charges (B)"), max_digits=10, decimal_places=2, default=0)

    # These value population is to be done.
    taxable_amount = models.DecimalField(
        _("Taxable Amount (A+B)"), max_digits=10, decimal_places=2, default=0)

    cgst_percentage = models.DecimalField(
        _("CGST Percentage"), max_digits=4, decimal_places=2, default=0)

    cgst_amount = models.DecimalField(
        _("CGST Amount"), max_digits=10, decimal_places=2, default=0)

    sgst_percentage = models.DecimalField(
        _("SGST Percentage"), max_digits=4, decimal_places=2, default=0)

    sgst_amount = models.DecimalField(
        _("SGST Amount"), max_digits=10, decimal_places=2, default=0)

    igst_percentage = models.DecimalField(
        _("IGST Percentage"), max_digits=4, decimal_places=2, default=0)

    igst_amount = models.DecimalField(
        _("IGST Amount"), max_digits=10, decimal_places=2, default=0)

    vat_percentage = models.DecimalField(
        _("SGST Percentage"), max_digits=4, decimal_places=2, default=0)

    vat_amount = models.DecimalField(
        _("SGST Amount"), max_digits=10, decimal_places=2, default=0)

    discount = models.DecimalField(
        _("Discount"), max_digits=10, decimal_places=2, default=0)

    total_amount = models.DecimalField(
        _("Total Amount"), max_digits=10, decimal_places=2, default=0)

    total_rcm_amount = models.DecimalField(
        _("RCM Amount"), max_digits=10, decimal_places=2, default=0)

    total_toll_charges = models.DecimalField(
        _("Total Toll Charges"), max_digits=10, decimal_places=2, default=0)

    total_parking_charges = models.DecimalField(
        _("Total Parking Charges"), max_digits=10, decimal_places=2, default=0)

    total_call_charges = models.DecimalField(
        _("Total Call Charges"), max_digits=10, decimal_places=2, default=0)

    file = models.FileField(upload_to=get_invoice_upload_path, validators=[
        FileValidator(allowed_extensions=["pdf"])], max_length=256)

    total_by_service_type = models.TextField(blank=True, null=True)

    comments = models.TextField(_("Comments (Customer)"), null=True, blank=True,
                                help_text=_(
                                    'Notes for customer. Reflects in PDF'))

    remarks = models.TextField(_("Remarks (Admin)"), null=True, blank=True,
                               help_text=_(
                                   'Remarks/description for internal reference'))

    is_stale = models.BooleanField(
        _("Is Invoice Stale"), default=False, db_index=True)

    is_e_invoice_filed = models.NullBooleanField(
        _("Is EInvoice Filed"), null=True, blank=True)

    stale_reason = models.CharField(_('Reason For Stale'), max_length=256,
                                    null=True, blank=True)

    is_credit_debit_note = models.BooleanField(_("Is Credit / Debit Note"),
                                               default=False)

    historical_invoice_dump = JSONField(blank=True, null=True)

    related_invoice = models.ForeignKey('self',
                                        verbose_name='Against',
                                        null =True, blank=True,
                                        on_delete=models.CASCADE)

    expiry_batch = models.ForeignKey('customer.InvoiceExpirationBatch',
                                     null=True, blank=True,
                                     on_delete=models.CASCADE)

    current_owner = models.ForeignKey(User, null=True, blank=True,
                                      on_delete=models.PROTECT,
                                      related_name='current_owner')

    due_date = models.DateField(_("Due Date"), blank=True, null=True)

    assigned_date = models.DateField(_("Assigned Date"), blank=True, null=True)

    rb_job_date = models.DateTimeField(_("Revenue Booked Job Date"), blank=True,
                                       null=True)

    non_taxable_adjustment = models.DecimalField(
            _("Non Taxable Adjustment"), max_digits=10, decimal_places=2, default=0)

    taxable_adjustment = models.DecimalField(
            _("Taxable Adjustment"), max_digits=10, decimal_places=2, default=0)

    total_service_charge = models.DecimalField(
            _("Total Service Charge"), max_digits=10, decimal_places=2, default=0)

    is_processed = models.NullBooleanField(
        _("Is Invoice Processed"), default=False, db_index=True,
                            blank=True, null=True)

    is_manual_upload = models.NullBooleanField(
        _("Is Manual Upload"), default=False, db_index=True,
                            blank=True, null=True)

    is_processed_timestamp = models.DateTimeField(
        _("Is Invoice Processed Timestamp"), blank=True, null=True)

    context = models.TextField(null=True, blank=True)

    upload_to_gst_portal_failed_reason = models.TextField(null=True, blank=True)

    manual_upload_reason = models.ForeignKey('customer.ManualUploadReason',
                                             null=True, blank=True,
                                             on_delete=models.PROTECT)

    credit_debit_note_reason = models.ForeignKey('customer.CreditDebitReason', null=True, blank=True,
                                                 on_delete=models.PROTECT)
    # spoc_assigned_to = models.ForeignKey(User, null=True, blank=True,
    #     on_delete=models.PROTECT,
    #     related_name='spoc_assigned_to')

    is_deleted = models.NullBooleanField(_("Is Deleted?"), null=True, blank=True)

    copy_data = CopyManager()
    objects = InvoiceManager()

    def __str__(self):
        return '%s' % (self.invoice_number or self.id)

    class Meta:
        verbose_name = 'Customer Invoice'
        verbose_name_plural = 'Customer Invoices'

    def save(self, *args, **kwargs):
        if self.id:
            ci_old = CustomerInvoice.objects.filter(id=self.id).first()
            if (ci_old.invoice_number and
                    (not self.invoice_number or len(self.invoice_number)==0)):
                raise ValidationError('Invoice number cannot be empty')

        super(CustomerInvoice, self).save(*args, **kwargs)

    @property
    def invoice_config(self):
        return self.invoice_configs

    @property
    def basic_pay_calculation_json(self):
        if isinstance(self.basic_pay_calculation, str):
            self.basic_pay_calculation = json.loads(self.basic_pay_calculation)
            if isinstance(self.basic_pay_calculation, str):
                return json.loads(self.basic_pay_calculation)
        return self.basic_pay_calculation

    @property
    def count_distinct_drivers(self):
        return self.basic_pay_calculation_json.get("count_distinct_drivers", 0)

    @property
    def count_all_working_days(self):
        print(self.basic_pay_calculation_json)
        return self.basic_pay_calculation_json.get("count_all_working_days", 0)

    @property
    def service_state(self):
        return self.bh_source_state

    # @property
    # def non_taxable_adjustment(self):
    #     toll_and_parking_charges = self.total_toll_charges + self.total_parking_charges
    #     if self.total_rcm_amount != 0:
    #         non_taxable_adjustment = self.total_amount - (self.taxable_amount + toll_and_parking_charges)
    #     else:
    #         non_taxable_adjustment = self.total_amount - (self.taxable_amount + self.cgst_amount + \
    #                                              self.sgst_amount + self.igst_amount + toll_and_parking_charges)
    #     return non_taxable_adjustment

    # @property
    # def taxable_adjustment(self):
    #     return decimal.Decimal(self.taxable_amount) - (self.charges_service + self.charges_basic)
    #
    # @property
    # def total_service_charge(self):
    #     return self.charges_basic + self.charges_service + self.taxable_adjustment + \
    #                   decimal.Decimal(self.non_taxable_adjustment)

    @property
    def is_blanket_invoice(self):
        if self.invoice_type == TAX_INVOICE and self.is_credit_debit_note:
            return True
        return False

    # @property
    # def total_with_tax(self):
    #     toll_parking_charges = self.total_parking_charges + self.total_toll_charges
    #     if self.total_rcm_amount:
    #         return self.taxable_amount + self.non_taxable_adjustment + toll_parking_charges
    #     elif self.taxable_amount:
    #         return self.taxable_amount + self.cgst_amount + self.sgst_amount + self.igst_amount \
    #                + self.non_taxable_adjustment + toll_parking_charges
    #     else:
    #         return 0

    @property
    def invoice_gst(self):
        if not self.total_rcm_amount:
            return self.cgst_amount + self.sgst_amount + self.igst_amount
        else:
            return 0

    @property
    def debit_amount(self):
        total_amount_aggr = CustomerInvoice.objects.filter(
            related_invoice=self,
            status__in=[CUSTOMER_CONFIRMED, PAYMENT_DONE]
        ).aggregate(Sum('total_amount'))
        return total_amount_aggr.get('total_amount__sum', 0) or 0

    @property
    def suggested_value(self):
        amount = decimal.Decimal(self.taxable_amount) * decimal.Decimal(0.98) + decimal.Decimal(self.non_taxable_adjustment) \
               + decimal.Decimal(self.debit_amount) + decimal.Decimal(self.invoice_gst)
        return round(amount,2)

    @property
    def credit_amount(self):

        credit_note_amount = CustomerInvoice.objects.filter(
            invoice_type=CREDIT_NOTE, related_invoice=self).exclude(
            status__in=INVOICE_EDITABLE_STATUS).values_list('total_amount',
                                                            flat=True)

        if credit_note_amount:
            amount_sum = reduce((lambda x, y: x + y), credit_note_amount)
            return amount_sum

        return 0

    @property
    def suggested_tds(self):
        amount = decimal.Decimal(self.taxable_amount) * decimal.Decimal(0.02)
        return round(amount, 2)

    @property
    def revenue_booked_month(self):
        return str(calendar.month_abbr[self.revenue_booked_on.month]) + '-' + str(self.revenue_booked_on.year)

    @property
    def distribution_days(self):
        return self.basic_pay_calculation_json.get("distribution_days", {})

    def get_gst_invoice_series(self, invoice_date, state_code):
        """
        Same as get_invoice_year, prefixes state_code
        """
        if not state_code:
            print("State Code is mandatory for GST Invoices")
            return None

        series = '%s%d%02d' % (state_code, invoice_date.year % 100, invoice_date.month)
        print("GST Invoice Series: %s" % (series,))
        return series

    # def generate_invoice_number(self, invoice_date, state_code):
    #     print("generating invoice_number :", invoice_date)
    #     series = self.get_gst_invoice_series(invoice_date, state_code)

    #     with connection.cursor() as cursor:

    #         cursor.execute("""SELECT nextval('customer_customerinvoice_id_seq')""")
    #         invoice_number = '%s%d' % (series, cursor.fetchone()[0])
    #         print('Invoice number: %s' % (invoice_number))
    #         return invoice_number


    def generate_invoice_number(self, invoice_date, state_code):
        """
        For a given state code and date, finds the next GST invoice number
        Uses range and multiplier functions and looks up the database
        """
        print("generating invoice_number :", invoice_date)

        series = self.get_gst_invoice_series(invoice_date, state_code)
        last_invoice = self.find_last_gst_number(series)
        next_sequence = None

        if last_invoice is None:
            ''' No such GST invoices, Generate the first number '''
            next_sequence = 1
        else:
            last_number = last_invoice.invoice_number
            m = re.match(r'(\S{6})(\d{6})', last_number)
            if m:
                series, number = m.group(1), m.group(2)
                print("Prefix:%s, Number:%s" % (series, number))
                # number = 200 if int(number) < 200 else number
                next_sequence = int(number) + 1
            else:
                print('Invoice format invalid')

        new_invoice_number = '%s%06d' % (series, next_sequence)
        print('Invoice number: %s' % (new_invoice_number))

        return new_invoice_number

    def find_last_gst_number(self, series):
        """
        Find the last GST invoice in DB
        """
        # Unfortunately sort and get the first record on descending sort
        first = series
        last = '%s9' % series
        invoice = CustomerInvoice.objects.filter(
            invoice_number__gt=first,
            invoice_number__lt=last).order_by('-invoice_number')

        if invoice.exists():
            return invoice.first()

    def mark_stale(self):
        """
        Mark the invoice as `stale` and saves it. Expected to be called if a regeneration of invoice is required.
        Example : Change to any vertical such as Sell rate, Trip, Days count.
        """

        self.is_stale = True
        self.stale_reason = 'Mark Stale'
        self.save()

    def is_invoice_no_update_required(self):
        """
        Check if invoice number updation is required. Depending on invoice date logic.
        """

        if self.invoice_number:
            year, month = self.invoice_number[2:4], self.invoice_number[4:6]
            if not self.invoice_date:
                self.invoice_date = self.service_period_end
            invoice_year, invoice_month = str(self.invoice_date.year)[2:], "%02d" % (self.invoice_date.month,)

            if (invoice_year != year) or (invoice_month != month):
                return True
            return False
        else:
            return True

    def get_aggregate_attributes(self):
        attributes = []
        if self.batch:
            attributes = self.batch.aggregates.all()
        return attributes

    def get_aggregate_values(self):
        categories = []
        if self.batch:
            for aggregate in self.get_aggregate_attributes():
                categories.append(str(getattr(self.aggregation, aggregate.name)))
        return categories

    ''' FSM transitions for invoice status
    Either superuser of the defined supervisors of the contract own the permissions
    to approve/send/adjust the invoice
    @TODO: Check with operations and write the implementation like-wise (placeholder code)
    '''

    @transition(field=status, source=[CREATED, REJECTED], target=SENT_FOR_APPROVAL,
                conditions=[can_approve], permission=is_invoice_spoc)
    def send_for_approval(self):
        from blowhorn.utils.mail import Email
        from blowhorn.apps.operation_app.v5.tasks import send_invoice_action_notification
        from blowhorn.customer.utils import is_valid_gstin, remove_watermark

        gstin_present, is_valid, gstin_required = is_valid_gstin(self)

        if not gstin_present and gstin_required:
            raise ValidationError('GSTIN is mandatory. Please check/update in Business Customer')
        if gstin_present and gstin_required and not is_valid:
            raise ValidationError('Invalid Customer GSTIN. Please check/update in Business Customer')
        if not gstin_required:
            _bytes = remove_watermark(self)
            if _bytes:
                content = ContentFile(_bytes)
                self.file.save(str(self.invoice_number) + ".pdf", content, save=True)
            else:
                raise ValidationError('Invoice Watermark not removed')

        action_owner = User.objects.filter(email=self.action_email).first()

        notification_details = {
            'assignor_email':self.requested_user,
            'invoice': self.invoice_number,
            'invoice': self,
            'assignor_name': self.requested_user.name,
            'assignee_email': [self.action_email],
            'status': SENT_FOR_APPROVAL,
            'due_date': self.due_date,

        }
        history = create_invoice_history(self.status, SENT_FOR_APPROVAL, self, action_owner, self.due_date)
        self.current_owner = action_owner
        self.assigned_date = date.today()
        if hasattr(self, 'document'):
            self.document.supporting_doc = None
            self.mis.save()

            self.document.invoice_history = history
            self.document.save()
            notification_details['supporting_doc'] = self.mis.mis_doc
        Email().email_invoice_assignment_notification(notification_details)
        send_invoice_action_notification.apply_async((SENT_FOR_APPROVAL, self.id,
                                                      [action_owner.email],
                                                      self.requested_user.email,), )

    @transition(field=status, source=[SENT_FOR_APPROVAL, UNAPPROVED], target=APPROVED,
                conditions=[can_approve], permission=has_approval_permission)
    def approve(self):
        from blowhorn.utils.mail import Email
        action_owner = User.objects.filter(email=self.action_email).first()

        notification_details = {
            'assignor_email': self.requested_user,
            'invoice': self.invoice_number,
            'invoice': self,
            'assignor_name': self.requested_user.name,
            'assignee_email': [self.action_email],
            'status': APPROVED,
            'due_date': self.due_date
        }

        create_invoice_history(self.status, APPROVED, self, action_owner, self.due_date)
        self.current_owner = action_owner
        self.assigned_date = date.today()
        # Approving the invoice
        self.current_owner=action_owner
        tz = pytz.timezone(settings.ACT_TIME_ZONE)
        # invoice date to be of next day if approved post 5 PM
        cutoff_time = time(17,0,0)
        current_datetime = timezone.now().astimezone(tz)
        current_time = current_datetime.time()

        self.invoice_date = current_datetime.date()
        if current_time > cutoff_time:
            self.invoice_date = current_datetime.date() + timedelta(days=1)

        self.timestamp_approved = self.invoice_date
        Email().email_invoice_assignment_notification(notification_details)

        if self.is_invoice_no_update_required():
            self.invoice_number = self.generate_invoice_number(
            self.timestamp_approved, self.bh_source_state.code)

        if self.is_credit_debit_note:
            CreditDebitNote.generate_credit_debit_notes(invoice=self)
        else:
            from blowhorn.customer.utils import get_invoice_data, generate_invoice_pdf
            data = get_invoice_data(self)
            static_base = settings.ROOT_DIR.path('website/static')
            logo_folder_path = '%s/website/images/%s/logo/' % (static_base, settings.WEBSITE_BUILD)
            data["logo"] = logo_folder_path + "logo_rect.png"
            data.update({"einvoicing": {
                'ack_no': '',
                'ack_date': '',
                'reverse_charge': "No" if self.service_tax_category == BSS else "Yes",
                "irn": '',
                "qr_code": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAMI0lEQVR4Xu2dwXYjSQ4Du///oz1vT3sZV/RTNCZTVsyVAxIJEskq2Vb//vr6+vrVfymQAv+qwO8M0mSkwPcKZJCmIwUeFMggjUcKZJBmIAVeU6AN8ppuoT5EgQzyIY3umK8pkEFe0y3UhyiQQT6k0R3zNQUyyGu6hfoQBTLIhzS6Y76mQAZ5TbdQH6JABvmQRnfM1xTIIK/pFupDFMggH9LojvmaAhnkNd1CfYgC2iC/f//+0VKt/1yG9KP6a/y7N5f0o/NlEFDICowNgAuG6meQZ4VJP+yP/YMpahARuD1uBabzkX5Uf40n/rfHST/i3wZpg9CMvHU8g4zbZwUmeusNQPypPvG/PU7nJ/5tkDYIzchbxzPIuH1WYKJHNzjVX+OJ/+1x0o/4t0HaIDQjbx2/3iCW4Lo7t9/ApN+aP9Vf94fy2/Nj/vXHvD9dYGoQNYDipB/VX+OJ/zpuz0/85o9Y1CAiuI5bgQlv+ZN+VH+Nt+ezeHt+qp9Bxj/JpgZQfD3g6wGj89n4mn8GySCPM0oGtQNu8RnEKgh4KzDhLX0aUKq/xtvzWbw9P9Vvg7RB2iAPCmSQDJJBbjYIrUhagRS//RHj3c//7vxpfo5vkJ8uMDZg/Adn6wvip/cvg8hHLDIAxd99wN6dP/bn9E/Sf7rA2IA2iHoHovmhDYr9ySDPf1NvBcYGZJAM8qQA3QA0YBSnAaf6hKf6FKf6hKc48af6Fk/8KG7rE57q9w7SO8j0BqcBpDgNuDU41c8gGSSD9HOQ7xWwNxDh6YayN+Q6/5rfaf5Uvw0iN0gG2X5xoDUo4TPI19ejBjTgJDDhqQG351/zW+tD/Kl+G6QN0jtI7yC9g3ynAG1AuoEJTzc0xW19wlP9NkgbpA3SBmmDtEFoV/x7vA3SBmmDtEF2G4TuJXpGt8/IVJ/ilh/hqT7FSR+qT3iq3waRGwQFHuen+hS3A0Z4qk9xGnCqT3iqn0HGA7xuIDWY4pYf4ak+xWnAqT7hqX4GySC9g/QO0jtIn2LRruhTrH9VYL6ixxvqtbb/H2XPT3jLjx6RqD7hiV+PWOMBXjeQGkxxy4/wVJ/iNOBUn/BUP4NkkN5Bbn4HIQev4/YGsng6H+UnPN2glJ/wVH8dX/M/vkHWAlJ+K7DFW36EpwFf8yd+Nr7mn0HkI9a8QfJbTzLI898DkUEzSAZR7yA0YOv4/II6/b1YawEpvxXY4i0/wrdB2iA0I49xO+AWT+QpP+EzSAahGckgTx9jykdMJf5fANMFQhcEUegdRA7IvEG9pE8vuOMGIQK3x+kGsga5HX97f4gf9Y/w8w1CBG6Pk8C3D7jlf3t/iB+dn/AZBBQigTMIjdjZOPWP2GWQDEIz8tbxDDJuHwncBhk3QKan/lH6NkgbhGbkreMZZNw+ErgNMm6ATE/9o/RtkDYIzchbx48b5K3V+w/I2w1DFNf5qf5Pj+sN8tMFsudbD/A6vz3/u+MzyLiD6wFe5x/Lc336DDJu0XqA1/nH8lyfPoOMW7Qe4HX+sTzXp88g4xatB3idfyzP9ekzyLhF6wFe5x/Lc336DDJu0XqA1/nH8lyfXhuEGnRaAfpB0Zo/1Sd9iB/l/3Q86UvxDCL/Yo8EpgEm/KcPuD0/6UvxDJJBHmfEDuhpPBmA4hkkg2SQBwUySAbJIBnkewXoEYBWMMV7B/n9KBHpQ/0hPPWH4m2QNkgbpA3SBvlOAbqB7Q1+Gk8bguJ6g2AB+cVslH8dpwZTfRpAwtu45W/rE97qQ+fT+e2XV5MA6wNQfRsn/pTfNojyU9zyp/w2bvWh8+n8GeS5xdQAGhDbIMpPccuf8tu41YfOp/NnkAxih9zg9QCPH+F7B4Hu0g1Fw2EHgPJT3PKn/DZu9aHz6fxtkDaIHXKD1wPcBjHyeyzdUFTBDgDlp7jlT/lt3OpD59P52yBtEDvkBq8H+N03CIlHNwDh1wLb+nQ+4k/40/yoPsXX56P6FJ+/pCMB+aseNGCn69MAEH/C0/lsfsJTfYqvz0f1KZ5BxgalAaABJDw12OYnPNWn+Pp8VJ/iGSSDPM5IBlkrABY9fYOs61N+kp/wdAPa/ISn+hRfn4/qU7wN0gZpgzwokEEySAbJIN8rsF7xlJ8eYQhPjwg2P+GpPsXX56P6FNcbhA5IAq/xJIDlR/kpTvUJb/Wz+Qlv46TP/Pz2J+mW4BpPDbINoPwUp/qEt/rZ/IS3cdJnfv4M8vXYQ2rAegAoP/GjAbP5CW/jxH9+/gySQZ6GmAbQGoDwGUT+spltoG0ANZjiVJ/wdP51fuJn48R/fv42SBukDfK9An2K9ZVBMkgG+VYBu8LXjxCUf/6IIX+QSvwpbvtDeKqvNwgWGL9jaAHGA0D87ICv8dRfOh/hb49nkAyiZjSDKPl+/VrfcLZBxE8e/xfxo/qn8XR+4kf42+NtkDaImtEMouRrg9AAtUHkgI3hbZA2iBoxugBU8gvAGSSDqDHMIEq+HrFogHrEkgM2hs83yJj/8fQ04GuCZMB1fXv+0/xJnwxCCkHcDogsjx8j2/yEt+fPIKTwm8ftgNjjnx4we/7T/En/Nggp1AZ5VCCDyAH66XA7IFaf0zewPf9p/qR/G4QUaoO0QeSMfDTc3qBWvNM3sD3/af6kfxuEFGqDtEHMjNgbxNT+L7B0w9nzn86/1pD0secnvD2f3iAkgCV4Gk8NsOc/nX+tL+ljz094e74MAgpSA2gAqEGn8xM/Gyd97PkJr/mvv9XEEjyNpwbQABD/0/mJn42TPvb8hNf8M8izhNQAGgBq0On8xM/GSR97fsJr/hkkg9ghesJnkPHfUyyb9ye56YaiAaAap/MTPxsnfez5Ca/5t0HaIHaI2iAPCtgbYtmcP8l9O3/i9ydnfPp/7A285nf8fOsNYhtgBSI8Nfg0f+JH56O4Pd+aH/GnuD5fBvn9qLEVmBpI8fUA2vOt+ZE+FNfnyyAZhIbMvGOY3H8Dm0GkinQDWoElPfxmSpvfno/0s/wsXp+vDdIGMUOYQUA9Esg62DTvT7C38yd+f3LGPsV6XaX5LytmkNeb8z9kBnH62fk7bpDTA0D1SeA1nsZjzY/q0/kJb/nb/ITPIIf/gZ/TA0YDigMkf9WI6q/1wfOdfkm3AtABbQNO40+fj+rb/ll9iR/lJ3wbpA1CM/IYzyDjT7GswNRdukGo/mn86fNRfdKP8FZfm5/wbZA2CM1IG8QoRDfI+oYg7rb+afzp81F96j/hrb42P+HbIG0QmpE2iFGIbpD1DUHcbX2Lt/wIv45Tf9f1Sf91/TbIeINQA08PAPHLILJDJCClJzw1kOK2vsVbfoRfx9f9If6kP+FtvA3SBpm+Y9gBzSDyVxWoASQw3ZAWb/kRfh0nfdb1Sf91/TZIG6QN8qBABskgGSSDfK8APULQiic8PQJQfsKv4/Z8lt9pfdoghzeIHaA13g4oGczmX58/g2SQxxmzA5xBLh8warBtIOHXN9w6P+lH9Ukfm5/q23gb5HKD2wZbvB3gDHL5gFGDbQMJbwf0NJ70I36kj81P9W28DXK5wW2DLd4OcAa5fMCowbaBhLcDehpP+hE/0sfmp/o23ga53OC2wRZvBziDyAGzDbR420CLJ/6Un/AUJwNQfYu3/Ahv48c3iD2Axa8HgAaI+BM/wlOc+FF9i7f8CG/jGURuQDtA1EDKT3iK2wG3eMuP8DaeQTLI4wyRQTMIWNAKaB1u8Za/xRN/yk94itsBt3jLj/A23gZpg7RBHhTIIBkkg2SQ7xWgRxj7CEF4egQgfoSnOPGj+hZv+RHexucbxBI8jbcDQHh7vtMDTPzt+e35iB/FMwgoRA0+3sDxIyINEMVJP8If13f974OQALfHqcHHG5hBpiPUBmmDTAeMLhgqfvwCaoM8t4gafLyBbRDymIq3QdogaoAITBcM4Y9fQG2QNggNqYlnkPFXh5rm/A0sNfj4Ddcj1t9o87c59CPWlF3JU+CwAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Ahnk7v7E7rACGeRwAyp/twIZ5O7+xO6wAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Ahnk7v7E7rACGeRwAyp/twIZ5O7+xO6wAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Av8Ai8TmbD3x6tAAAAAASUVORK5CYII="

            }})
            data['invoice']['general']['invoice_no'] = self.invoice_number
            data['invoice']['general']['invoice_date'] = self.invoice_date
            self.context = json.dumps(data, indent=4, sort_keys=True, default=str)
            generate_invoice_pdf(self, data, APPROVED)


    @transition(field=status, source=SENT_FOR_APPROVAL, target=REJECTED, permission=is_invoice_supervisor)
    def reject(self):
        from blowhorn.utils.mail import Email
        from blowhorn.apps.operation_app.v5.tasks import send_invoice_action_notification
        spoc = None
        invoice_history = InvoiceHistory.objects.filter(invoice=self,
                                                        old_status=CREATED).latest(
            'created_time')
        if invoice_history:
            spoc = invoice_history.created_by

        notification_details = {
            'assignor_email':self.requested_user,
            'invoice': self.invoice_number,
            'invoice': self,
            'assignor_name': self.requested_user.name,
            'assignee_email': [str(spoc)] if spoc else None,
            'status': REJECTED,
            'due_date': self.due_date,
            'reason': self.reason
        }

        history = create_invoice_history(self.status, REJECTED, self, spoc, self.due_date, reason=self.reason)
        self.current_owner = spoc
        self.assigned_date = date.today()
        if hasattr(self, 'document'):
            self.document.invoice_history = history
            self.document.save()
        Email().email_invoice_assignment_notification(notification_details)
        send_invoice_action_notification.apply_async((REJECTED, self.invoice_number,
                                                      [spoc.email],
                                                      self.requested_user.email,), )

    @transition(field=status, source=[APPROVED,], target=UPLOADED_TO_GST_PORTAL,
                permission=is_finance_user, conditions=[gst_filing_required],)
    def upload_to_gst_portal(self):
        from blowhorn.customer.utils import upload_to_gst_portal_method

        is_success, resp = upload_to_gst_portal_method(self)
        if not is_success:
            raise ValidationError(resp)

    @transition(field=status, source=[APPROVED,], target=UPLOADED_TO_GST_PORTAL,
                permission=is_finance_user, conditions=[gst_filing_required],)
    def manual_upload(self):
        from blowhorn.customer.helpers.invoice_helper import InvoiceHelper
        from blowhorn.customer.utils import remove_watermark
        action_owner = User.objects.filter(email=self.action_email).first()

        _bytes = remove_watermark(self)
        if _bytes:
            self.is_manual_upload = True
            self.is_e_invoice_filed = True
            _reason = "Manual Upload Success"
            content = ContentFile(_bytes)
            self.file.save(str(self.invoice_number) + ".pdf", content, save=True)
            history = create_invoice_history(self.status, UPLOADED_TO_GST_PORTAL,
                                                self, action_owner, None,
                                                reason=_reason)

            if hasattr(self, 'document'):
                self.document.invoice_history = history
                self.document.save()
                mis_data = {
                    'invoice': self,
                    'mis_doc': self.document.supporting_doc
                }
                MISDocument.objects.create(**mis_data)
        else:
            raise ValidationError('No file generated')

    @transition(field=status, source=[APPROVED, UPLOADED_TO_GST_PORTAL], target=SENT_TO_CUSTOMER,
                permission=is_city_sales_manager, conditions=[check_gst_filing_required])
    def send_invoice(self):
        from blowhorn.utils.mail import Email
        due_date = date.today() + timedelta(days=10)

        if not self.email:
            raise ValueError('No email selected from Customer Contacts.')

        history = create_invoice_history(self.status, SENT_TO_CUSTOMER, self,
                                         self.current_owner, due_date)
        if hasattr(self, 'document'):
            self.document.invoice_history = history
            self.document.save()
            mis_data = {
                'invoice': self,
                'mis_doc': self.document.supporting_doc
            }
            MISDocument.objects.create(**mis_data)
        self.due_date = due_date
        self.assigned_date = date.today()

        Email().send_invoice(self, self.email, self.service_period_start,
                             self.service_period_end,
                             requesting_user=self.requested_user,
                             cc=self.cc_email)

    @transition(field=status, source=SENT_TO_CUSTOMER, target=CUSTOMER_ACKNOWLEDGED,
                permission=is_city_sales_manager)
    def customer_acknowledged(self):
        from blowhorn.apps.operation_app.v5.tasks import send_invoice_action_notification
        due_date = date.today() + timedelta(days=10)
        history = create_invoice_history(self.status, CUSTOMER_ACKNOWLEDGED,
                                         self, self.current_owner, due_date)
        if hasattr(self, 'document'):
            self.document.invoice_history = history
            self.document.save()

        self.due_date = due_date
        self.assigned_date = date.today()
        send_invoice_action_notification.apply_async((CUSTOMER_ACKNOWLEDGED, self.invoice_number,
                                                      [self.current_owner.email],
                                                      self.requested_user.email,), )

    @transition(field=status, source=SENT_TO_CUSTOMER, target=SENT_TO_CUSTOMER,
                permission=is_city_sales_manager)
    def resend(self):
        from blowhorn.utils.mail import Email

        if not self.email:
            raise ValueError('No email selected from Customer Contacts.')

        history = create_invoice_history(self.status, SENT_TO_CUSTOMER, self,
                                         self.current_owner, self.due_date)
        if hasattr(self, 'document'):
            self.document.invoice_history = history
            self.document.save()
            mis_data = {
                'invoice': self,
                'mis_doc': self.document.supporting_doc
            }
            MISDocument.objects.create(**mis_data)

        Email().send_invoice(self, self.email, self.service_period_start,
                             self.service_period_end,
                             requesting_user=self.requested_user,
                             cc=self.cc_email)

    @transition(field=status, source=[UPLOADED_TO_GST_PORTAL], target=CANCELLED,
                permission=is_finance_user, conditions=[within_cancel_threshold_date],)
    def cancel(self):
        inv_history = InvoiceHistory.objects.filter(invoice=self, old_status=APPROVED,
                                                    new_status=UPLOADED_TO_GST_PORTAL).first()
        if not inv_history:
            raise ValidationError("Cannot Cancel invoice, not uploaded to GST")
        if inv_history.created_time + timedelta(hours=23) < timezone.now():
            raise ValidationError("Invoice cannot be canelled after 23 hrs")
        from blowhorn.company.models import OfficeGSTInformation
        from blowhorn.customer.utils import generate_auth_token, cancel_irn
        gst_info = OfficeGSTInformation.objects.filter(office__state=self.service_state
                                                       ).select_related('office', 'office__company').first()
        if not gst_info:
            gst_info = OfficeGSTInformation.objects.select_related('office', 'office__company').first()
        generate_auth_token(gst_info)
        is_sucess, resp = cancel_irn(self, gst_info)
        if not is_sucess:
            raise ValidationError(resp)
        create_invoice_history(self.status, CANCELLED, self, None, None)

    @transition(field=status, source=CUSTOMER_ACKNOWLEDGED, target=CUSTOMER_CONFIRMED,
                permission=is_city_sales_manager)
    def customer_confirmed(self):
        from blowhorn.apps.operation_app.v5.tasks import send_invoice_action_notification
        # due_date = date.today() + timedelta(days=10)

        history = create_invoice_history(self.status, CUSTOMER_CONFIRMED, self, None, None)
        if hasattr(self, 'document'):
            self.document.invoice_history = history
            self.document.save()
        self.due_date = None
        self.assigned_date = date.today()
        send_invoice_action_notification.apply_async((CUSTOMER_CONFIRMED, self.invoice_number,
                                                      [self.current_owner.email],
                                                      self.requested_user.email,), )

    # @transition(field=status, source=CUSTOMER_ACKNOWLEDGED,
    #             target=CUSTOMER_DISPUTED,
    #             conditions=[within_dispute_threshold_date],
    #             permission=is_city_sales_manager)
    # def customer_disputed(self):
    #     from blowhorn.apps.operation_app.v5.tasks import send_invoice_action_notification
    #     from blowhorn.utils.mail import Email

    #     spoc = None
    #     invoice_history = InvoiceHistory.objects.filter(invoice=self,
    #                                                     old_status=CREATED).latest(
    #         'created_time')
    #     if invoice_history:
    #         spoc = invoice_history.created_by

    #     notification_details = {
    #         'assignor_email': self.requested_user,
    #         'invoice': self,
    #         'assignor_name': self.requested_user.name,
    #         'assignee_email': [str(spoc)] if spoc else None,
    #         'status': CUSTOMER_DISPUTED,
    #         'due_date': self.due_date,
    #         'reason': self.reason
    #     }

    #     history = create_invoice_history(self.status, CUSTOMER_DISPUTED, self, spoc, self.due_date, reason=self.reason)
    #     if hasattr(self, 'document'):
    #         self.document.invoice_history = history
    #         self.document.save()

    #     self.assigned_date = date.today()

    #     Email().email_invoice_assignment_notification(notification_details)
    #     send_invoice_action_notification.apply_async((CUSTOMER_DISPUTED, self.invoice_number,
    #                                                   [self.current_owner.email],
    #                                                   self.requested_user.email,), )

    def regenerate_invoice(self):

        if self.is_credit_debit_note or (hasattr(self, 'invoicetrip_set') and self.invoicetrip_set.exists()):
            generate_invoice(self)
        else:
            # shipment order invoice add it to task
            from blowhorn.customer.tasks import regenerate_customer_invoice
            regenerate_customer_invoice.apply_async((self.id,), )

    @transition(field=status, source=CREATED, target=CREATED, conditions=[can_regenerate])
    def regenerate(self):
        self.regenerate_invoice()

    @transition(field=status, source=UNAPPROVED, target=UNAPPROVED, conditions=[can_regenerate])
    def Regenerate(self):
        self.regenerate_invoice()

    @transition(field=status, source=REJECTED, target=REJECTED, conditions=[can_regenerate])
    def Regenerate(self):
        self.regenerate_invoice()

    # @transition(field=status, source=[APPROVED,CUSTOMER_DISPUTED], target=UNAPPROVED,
    #             permission=is_city_manager_or_supervisor)
    #
    # def unapprove(self):
    #     from blowhorn.common.tasks import send_async_mail
    #     from blowhorn.address.models import State
    #
    #     history = create_invoice_history(self.status, UNAPPROVED, self, None, None)
    #     if hasattr(self, 'document'):
    #         self.document.invoice_history = history
    #         self.document.save()
    #
    #     cities = State.objects.filter(id=self.bh_source_state_id).first().cities.all()
    #     url = current_request().META.get('HTTP_HOST', 'blowhorn.com')
    #     url = url + '/admin/customer/customerinvoice/%s/change/' % (self.id)
    #     subject_format = str(self) + '- Invoice UnApproved'
    #     content_format = str(current_request().user) +' has unapproved the Invoice ' + str(self) + '.\n\n\n' + url
    #
    #     for city in cities:
    #         send_async_mail.delay([manager.email for manager in city.managers.all()],
    #             subject_format,
    #             content_format,
    #         )
    #
    #     print("Unapproving invoice")

    # @transition(field=status, source=APPROVED, target=READY_TO_BE_SENT,
    #             permission=has_reject_and_send_permissions)
    # def send(self):
    #     create_invoice_history(self.status, READY_TO_BE_SENT, self, None, None)
    #     self.status=READY_TO_BE_SENT
    #     print("send invoice to configured emails")

    @transition(field=status, source=CREATED, target=CREATED,
        permission=has_reassigning_permissions)
    def reassign(self):
        from blowhorn.utils.mail import Email
        action_owner = User.objects.filter(email=self.action_email).first()

        notification_details = {
            'assignor_email':self.requested_user,
            'invoice': self.invoice_number,
            'invoice': self,
            'assignor_name': self.requested_user.name,
            'assignee_email': [self.action_email],
            'status': CREATED,
            'due_date': self.due_date
        }

        create_invoice_history(self.status, REASSIGNED, self, action_owner, self.due_date)
        self.current_owner=action_owner
        self.assigned_date = date.today()
        print("Reassigned to the user in created state")
        Email().email_invoice_assignment_notification(notification_details)

    @transition(field=status, source=CUSTOMER_DISPUTED,
                target=CUSTOMER_DISPUTED, permission=is_invoice_spoc)
    def regenerate_disputed_invoice(self):
        spoc = None
        invoice_history = InvoiceHistory.objects.filter(invoice=self,
                                                        old_status=CREATED).latest(
            'created_time')
        if invoice_history:
            spoc = invoice_history.created_by

        create_invoice_history(self.status, REGENERATED, self, spoc, None)
        print("Providing the link to add the new invoice")

    @transition(field=status, source=CUSTOMER_CONFIRMED,
                target=CUSTOMER_CONFIRMED, permission=is_city_sales_manager)
    def generate_credit_debit_note(self):
        spoc = None
        invoice_history = InvoiceHistory.objects.filter(
            invoice=self, old_status=CREATED).latest('created_time')
        if invoice_history:
            spoc = invoice_history.created_by

        create_invoice_history(self.status, CREATE_CREDIT_DEBIT_NOTE, self,
                               spoc, None)
        print("Providing the link to add the new invoice")

    @transition(field=status,
                source=[SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED,
                        CUSTOMER_CONFIRMED, PARTIALLY_PAID, UPLOADED_TO_GST_PORTAL],
                target=PAYMENT_DONE,
                conditions=[can_mark_payment_done],
                permission=has_payment_receive_permission)
    def payment_done(self):
        create_invoice_history(self.status, PAYMENT_DONE, self, None, None)
        print("Payment done for the invoice")

    class Meta:
        indexes = (
            BrinIndex(fields=['modified_date']),
        )


class InvoiceSummary(CustomerInvoice):
    class Meta:
        proxy = True
        verbose_name = 'Invoice Summary'
        verbose_name_plural = 'Invoice Summary'

class CollectionsSummary(CustomerInvoice):
    class Meta:
        proxy = True
        verbose_name = 'Collections Summary'
        verbose_name_plural = 'Collections Summary'


class EInvoicing(BaseModel):
    from blowhorn.order.helper import path_for_supporting_document

    invoice = models.OneToOneField(CustomerInvoice, on_delete=models.PROTECT)

    irn = models.CharField(_('IRN'), max_length=155)

    ack_no = models.CharField(_('Ack Number'), max_length=100)

    ack_date = models.DateTimeField(_("Ack Date"), null=True, blank=True)

    status = models.CharField(_('Status'), max_length=25)

    file = models.FileField(upload_to=path_for_supporting_document, max_length=256, null=True, blank=True)

    response = models.TextField('Response', null=True)

    def __str__(self):
        return self.irn


class EInvoicingEvent(models.Model):
    from blowhorn.order.helper import path_for_supporting_document

    e_invoice = models.ForeignKey(EInvoicing, on_delete=models.PROTECT)

    created_time = models.DateTimeField(auto_now_add=True)

    status = models.CharField(_('Status'), max_length=25)

    file = models.FileField(upload_to=path_for_supporting_document,
        max_length=256, null=True, blank=True)

    response = models.TextField('Response', null=True)

    def __str__(self):
        return str(self.e_invoice)


class CustomerLedger(models.Model):
    id = models.BigIntegerField(primary_key=True)

    record_number = models.IntegerField(_('Record Number'))

    customer = models.ForeignKey('customer.Customer',
        null=True, blank=True, on_delete=models.DO_NOTHING)

    transaction_date = models.DateField()

    description = models.CharField(
        _('Description'), max_length=255
    )

    debit = models.DecimalField(
        _('Debit'), max_digits=20, decimal_places=2
    )

    credit = models.DecimalField(
        _('Credit'), max_digits=20, decimal_places=2
    )

    balance = models.DecimalField(
        _('Balance'), max_digits=20, decimal_places=2
    )

    objects = models.Manager()

    copy_data = CopyManager()

    class Meta:
        managed = False
        db_table = 'customer_ledger'
        ordering = ['customer_id', '-record_number']


class InvoiceSupportingDocument(BaseModel):
    from blowhorn.order.helper import path_for_supporting_document
    """
    Model to Capture invoice supporting document and reason's
    """
    invoice = models.ForeignKey(CustomerInvoice, on_delete=models.CASCADE)

    invoice_history = models.OneToOneField('customer.InvoiceHistory', on_delete=models.CASCADE)

    supporting_doc = models.FileField(upload_to=path_for_supporting_document,
        max_length=256, null=True, blank=True)


    reason = models.CharField(null=True, blank=True, max_length=256)


class InvoiceHistory(models.Model):
    invoice = models.ForeignKey(CustomerInvoice, on_delete=models.CASCADE)

    old_status = models.CharField(_('Old Status'), max_length=160)

    new_status = models.CharField(_('New Status'), max_length=160)

    created_by = CreatingUserField(related_name='%(class)s_created_by')

    action_owner = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    created_time = models.DateTimeField(auto_now_add=True)

    due_date = models.DateField(blank=True, null=True)

    remarks = models.CharField(null=True, blank=True, max_length=256)

    reason = models.CharField(null=True, blank=True, max_length=256)

    field = models.CharField(null=True, blank=True, max_length=256)

    def __str__(self):
        return (self.old_status) + ' --> ' + str(self.new_status)

    class Meta:
        verbose_name = _("Invoice History")
        verbose_name_plural = _("Invoice History")


class InvoiceAdjustmentCategory(BaseModel):
    name = models.CharField(
        _('Adjustment Category'), max_length=60)

    description = models.CharField(
        _('Adjustment Description'), max_length=250)

    is_tax_applicable = models.BooleanField(
            _('Tax applicable'), default=False, db_index=True)

    hsn_code = models.ForeignKey('contract.ServiceHSNCodeMap', null=True, blank=True,
                                 on_delete=models.PROTECT, verbose_name=_("HSN Code"))

    status = models.CharField(
        _('Status'), max_length=32, choices=INVOICE_CONFIG_STATUSES, default=INVOICE_CONFIG_ACTIVE)

    def __str__(self):
        return '%s-%s-%s' % (self.name, self.hsn_code,
                                'Taxable' if self.is_tax_applicable else 'NonTaxable')

    class Meta:
        verbose_name = _("Invoice Adjustment Category")
        verbose_name_plural = _("Invoice Adjustment Categories")


class CustomerInvoiceAdjustment(BaseModel):

    invoice = models.ForeignKey(CustomerInvoice, on_delete=models.CASCADE)

    category = models.ForeignKey(InvoiceAdjustmentCategory, null=True, on_delete=models.CASCADE)

    comments = models.CharField(_('Comments'), max_length=100, null=True, blank=True)

    total_amount = models.DecimalField(
        _("Total Amount"), max_digits=30, decimal_places=2, default=0)

    def __str__(self):
        return str(self.id)+ str(self.total_amount)

    def save(self, *args, **kwargs):
        self.invoice.is_stale = True
        self.invoice.stale_reason = 'Invoice Adjustment'
        self.invoice.save()
        super().save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        self.invoice.is_stale = True
        self.invoice.stale_reason = 'Invoice Adjustment delete'
        self.invoice.save()
        super().delete()


class UserType(ChoiceEnum):
    CUSTOMER = CUSTOMER
    CONSIGNEE = CONSIGNEE


class InvoiceHeader(models.Model):

    name = models.CharField(
        max_length=100, null=True, blank=True
    )

    address = models.TextField()

    state = models.ForeignKey(
        'address.State', on_delete=models.CASCADE
    )

    gst_identification_number = models.CharField(
        max_length=20, null=True, blank=True,
        help_text="GST Identification Number (GSTIN) of Customer."
    )

    type = models.CharField(
        choices=UserType.choices(),
        max_length=10
    )

    invoice = models.ForeignKey(CustomerInvoice, related_name='user_entities', on_delete=models.CASCADE)


class InlineGroup(ChoiceEnum):
    VEHICLE = VEHICLE
    SHIPMENT = SHIPMENT


class InlineAggregate(models.Model):

    contract = models.ForeignKey(
        'contract.Contract', null=True, blank=True, on_delete=models.CASCADE
    )

    customer = models.ForeignKey(
        'customer', null=True, blank=True, on_delete=models.CASCADE
    )

    contract_type = models.CharField(
        _('Contract Type'), max_length=15, choices=CONTRACT_TYPES,
        null=True, blank=True)

    division = models.ForeignKey(
        'customer.CustomerDivision', null=True, blank=True, on_delete=models.CASCADE
    )

    city = models.ForeignKey(
        'address.City', null=True, blank=True, on_delete=models.CASCADE
    )

    hub = models.ForeignKey(
        'address.Hub', null=True, blank=True, on_delete=models.CASCADE
    )

    driver = models.ForeignKey(
        'driver.Driver', null=True, blank=True, on_delete=models.CASCADE
    )

    vehicle = models.ForeignKey(
        'vehicle.Vehicle', null=True, blank=True, on_delete=models.CASCADE
    )

    vehicle_class = models.ForeignKey(
        'vehicle.VehicleClass', null=True, blank=True, on_delete=models.CASCADE
    )

    class Meta:
        abstract = True

    @property
    def aggregate_level_category(self):
        aggregate_level_category = []
        if self.contract:
            aggregate_level_category.append("Contract")
        if self.division:
            aggregate_level_category.append("Division")
        if self.city:
            aggregate_level_category.append("City")
        if self.hub:
            aggregate_level_category.append("Hub")
        if self.driver:
            aggregate_level_category.append("Driver")
        if self.vehicle_class:
            aggregate_level_category.append("Vehicle Class")
        return ",".join(aggregate_level_category)

    @property
    def aggregate_level_value(self):
        return self.contract or self.division or self.city or self.hub or self.driver or self.vehicle_class


class InvoiceInline(InlineAggregate):

    group = models.CharField(
        choices=InlineGroup.choices(), max_length=15, default=VEHICLE
    )

    attribute = models.CharField(
        _("Attribute"), max_length=15
    )

    value = models.IntegerField(
        _("Value"), default=0
    )

    extra_value = models.IntegerField(
        _("Extra Value"), default=0
    )

    rate = models.DecimalField(
        _("Rate"), max_digits=10, decimal_places=2,
        null=True, blank=True)

    amount = models.DecimalField(
        _("Amount"), max_digits=10, decimal_places=2,
        null=True, blank=True)

    distribution = JSONField(null=True, blank=True)

    invoice = models.ForeignKey(CustomerInvoice, related_name='inlines', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.invoice_id)

    @property
    def distribution_json(self):
        """
        Returns the `distribution` in json format.
        """

        # This method is to be updated if we have fixed postgres JSON field related issue.
        if isinstance(self.distribution, str):
            self.distribution = json.loads(self.distribution)
            if isinstance(self.distribution, str):
                return json.loads(self.distribution)
        return self.distribution


# New invoice models.
class InvoiceAggregateAttribute(BaseModel):

    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class InvoiceBatch(BaseModel):

    aggregates = SortedManyToManyField(
        InvoiceAggregateAttribute, _("Invoice Aggregate"),
        blank=True
    )

    requested_by = models.ForeignKey('users.User', on_delete=models.CASCADE)

    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.CASCADE)

    service_period_start = models.DateField(_("Period start"), db_index=True)

    service_period_end = models.DateField(_("Period end"), db_index=True)

    invoice_request = models.ForeignKey('customer.InvoiceRequest', null=True,
                                        blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Invoice Batch")
        verbose_name_plural = _("Invoice Batches")


class InvoiceAggregation(InlineAggregate):

    invoice = models.OneToOneField(CustomerInvoice, related_name='aggregation', on_delete=models.CASCADE)


class ContractInvoiceFragment(BaseModel):

    contract = models.ForeignKey('contract.Contract', on_delete=models.CASCADE)

    charges_basic = models.DecimalField(
            _("Base Pay (A)"), max_digits=10, decimal_places=2, default=0)

    charges_service = models.DecimalField(
            _("Slab Charges (B)"), max_digits=10, decimal_places=2, default=0)

    basic_per_day = models.DecimalField(
        _("Base Pay per day"), max_digits=10, decimal_places=2, default=0)

    days_counted = models.IntegerField(
        _("Days counted"), default=0, help_text="No. of days in month considered for base pay."
    )

    invoice = models.ForeignKey(CustomerInvoice, related_name='contract_invoices', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class InvoiceEntity(InlineAggregate):

    contract_invoice = models.ForeignKey(
        ContractInvoiceFragment, related_name='entities', null=True, blank=True,
        on_delete=models.CASCADE)

    # Applicable only for slabs with `daily` interval.
    date = models.DateField(null=True, blank=True)

    attribute = models.CharField(
        _("Attribute"), max_length=250
    )

    value = models.DecimalField(
        _("Value"), default=0.0, max_digits=20, decimal_places=2,
    )

    extra_value = models.DecimalField(
        _("Extra Value"), default=0.0, max_digits=10, decimal_places=2,
    )

    rate = models.DecimalField(
        _("Rate"), max_digits=10, decimal_places=2,
        null=True, blank=True)

    slab_fixed_amount_kilometers = models.DecimalField(
        _("Slab Fixed Amount (Kilometers)"), max_digits=10, decimal_places=2,
        null=True, blank=True)

    slab_fixed_amount_hours = models.DecimalField(
        _("Slab Fixed Amount (Hours)"), max_digits=10, decimal_places=2,
        default=0)

    amount = models.DecimalField(
        _("Amount"), max_digits=10, decimal_places=2,
        default=0)

    sell_rate = models.ForeignKey('contract.sellrate', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Invoice Entity")
        verbose_name_plural = _("Invoice Entities")


# This model would be deprecated going forward and `InvoiceRequest` would be used instead.
class InvoiceCreationRequest(BaseModel):

    email = models.EmailField()

    aggregates = SortedManyToManyField(
        InvoiceAggregateAttribute, _("Invoice Aggregate"),
        blank=True
    )

    customer = models.ForeignKey('customer.Customer', on_delete=models.CASCADE)

    service_period_start = models.DateField(_("Period start"))

    service_period_end = models.DateField(_("Period end"))

    status = models.IntegerField(choices=INVOICE_REQUEST_STATUSES, default=PENDING)

    output_log = models.TextField(null=True, blank=True)

    requested_time = models.DateTimeField(_('Requested Time'), auto_now_add=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Invoice Creation Request")
        verbose_name_plural = _("Invoice Creation Requests")


class CustomerReceivableManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('customer')

class CustomerReceivable(BaseModel):
    customer = models.ForeignKey('customer.Customer', on_delete=models.CASCADE)

    bank_account = models.ForeignKey('company.CompanyBankAccount',
                                     on_delete=models.PROTECT, null=True,
                                     verbose_name=_('Blowhorn Account'))

    status = FSMField(default=CREATED, protected=False, db_index=True)

    transaction_time = models.DateTimeField(auto_now_add=True)

    payment_date = models.DateField(_("Received On"), db_index=True)

    deposit_date = models.DateField(_("Deposited On"), db_index=True)

    payment_mode = models.CharField(_("Payment Mode"), choices=PAYMENT_MODE_OPTIONS, default=PAYMENT_MODE_BANK_TRANSFER,
                                    max_length=255)

    amount = models.FloatField(_('Amount'), default=0.0)

    description = models.CharField(
        _("Cheque / Deposit Slip Reference / UTR Number"), max_length=250,
        null=True, blank=True)

    payment_copy = models.FileField(_("Cheque/ payment copy"), null=True,
                                    blank=True, upload_to=generate_file_path,
                                    max_length=256)

    unallocated_amount = models.FloatField(_('Amount'), default=0.0)
    objects = CustomerReceivableManager()
    copy_data = CopyManager()

    def __str__(self):
        return str(self.customer) + ' / ' + str(self.id) + ' (' + str(self.payment_date) + ')' + ' / ' + \
               str(self.amount) + ' / (' + str(self.unallocated_amount) + ')'

    class Meta:
        verbose_name = _('Customer Collection')
        verbose_name_plural = _('Customer Collections')

    def save(self, *args, **kwargs):
        if self.status != APPROVED:
            self.unallocated_amount = self.amount
        super().save(*args, **kwargs)

    @transition(field=status, source=[CREATED, UNAPPROVED], target=APPROVED, permission=is_finance_user)
    def approve(self):
        # print('Approving Receivables')
        print('Approving Collections')

    @transition(field=status, source=[CREATED, UNAPPROVED], target=REJECTED, permission=is_finance_user)
    def reject(self):
        # print('Rejecting Receivables')
        print('Rejecting Collections')

    @transition(field=status, source=APPROVED, target=UNAPPROVED,
                permission=is_finance_user, conditions=[is_qualifiy_for_unapprove])
    def unapprove(self):
        print('Unapproving Collections')


class CustomerInvoiceConfig(BaseModel):
    # Assumption that id for default bank account
    DEFAULT_BANK_ACCOUNT = 1

    name = models.CharField(
        _("Config Name"), max_length=100, db_index=True,
        validators=[
            RegexValidator(
                regex=r'^[a-zA-Z0-9_]+$',
                message=_("Special characters not allowed")
            ), non_python_keyword
        ], unique=True, null=True)

    customer = models.ForeignKey('customer.Customer', on_delete=models.PROTECT,
                                 related_name='invoice_configs')

    aggregates = SortedManyToManyField(
        InvoiceAggregateAttribute, _("Invoice Aggregate"),
        blank= True
    )

    contracts = SortedManyToManyField('contract.Contract',
                                      _("Customer Contract"),
                                      related_name='invoice_configs')

    invoice_state = models.ForeignKey(
        'address.State',
        verbose_name='TARGET - Customer Invoice State',
        related_name='invoice_configs',
        help_text='This is the State where Customer would like to be invoiced',
        on_delete=models.PROTECT, null=True)

    invoice_cycle = models.ForeignKey(
        'contract.PaymentCycle', on_delete=models.PROTECT,
        related_name='invoice_cycle',
        help_text=_("Invoice cycle for generating invoices"))

    extra_pages = models.IntegerField(
        choices=INVOICE_EXTRA_PAGES, default=1,
        help_text=_("Extra pages expected in invoice PDF.")
    )

    status = models.CharField(_('Status'), max_length=50,
                                   choices=INVOICE_CONFIG_STATUSES,
                                   null=True, blank=True)

    company_bank_account = models.ForeignKey('company.CompanyBankAccount',
        on_delete=models.PROTECT,
        verbose_name='Company Bank Account',
        default=DEFAULT_BANK_ACCOUNT)

    payment_schedule = RecurrenceField(
        _('Payment Schedule'), null=True, blank=True,
        help_text=_(
            "Customer would typically pay us on this day"))

    def __str__(self):
        return '%s - %s' % (self.customer.name, self.name)

    class Meta:
        verbose_name = _('Customer Invoice Config')
        verbose_name_plural = _('Customer Invoice Configs')

    @property
    def service_state(self):
        any_contract = self.contracts.first()
        return any_contract.service_state if any_contract else None


class InvoiceTrip(models.Model):
    """
    Trips associated with each invoice.
    """

    invoice = models.ForeignKey(CustomerInvoice, on_delete=models.CASCADE)

    trip = models.ForeignKey('trip.Trip', on_delete=models.PROTECT)

    invoice_driver = models.ForeignKey('driver.Driver', on_delete=models.PROTECT, null=True)

    def __str__(self):
        return u'%s-%s' % (self.invoice, self.trip)

    class Meta:
        verbose_name = _('Invoice Trip')
        verbose_name_plural = _('Invoice Trips')
        unique_together = ('invoice', 'trip')


class InvoiceOrder(models.Model):
    """
    Orders associated with each invoice
    """

    invoice = models.ForeignKey(CustomerInvoice, on_delete=models.CASCADE)

    order = models.ForeignKey('order.Order', on_delete=models.PROTECT)

    invoice_driver = models.ForeignKey('driver.Driver', on_delete=models.PROTECT, null=True)

    def __str__(self):
        return u'%s-%s' % (self.invoice, self.order)

    class Meta:
        verbose_name = _('Invoice Order')
        verbose_name_plural = _('Invoice Orders')
        unique_together = ('invoice', 'order')


class InvoiceRequest(BaseModel):

    invoice_config = models.ForeignKey(CustomerInvoiceConfig, on_delete=models.CASCADE)

    status = models.IntegerField(choices=INVOICE_REQUEST_STATUSES, default=PENDING)

    output_log = models.TextField(null=True, blank=True)

    invoice_period = models.DateField(_("Invoice Period"), null=True)

    def __str__(self):
        return str(self.id) + " - " + str(self.invoice_config)


class ReceivablelineItem(BaseModel):

    customer_receivable = models.ForeignKey(CustomerReceivable, on_delete=models.CASCADE)

    invoice = models.ForeignKey(CustomerInvoice, null=True, blank=True, on_delete=models.CASCADE)

    order = models.ForeignKey('order.Order', null=True, blank=True, on_delete=models.CASCADE)

    amount = models.FloatField(_("Amount Received"), default=0.0)

    tds = models.FloatField(_("TDS Amount"), default=0.0)
    tds_certificate = models.FileField(_("TDS Certificate"), null=True,
                                       blank=True, upload_to=generate_file_path,
                                       max_length=256)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _('Collection Line Item')
        verbose_name_plural = _('Collection Line Items')


class CreditDebitNote:

    def generate_credit_debit_notes(invoice):
        old_invoice = invoice.related_invoice
        from blowhorn.company.models import Office
        from blowhorn.company.helpers.tax_helper import TaxHelper
        from blowhorn.company.helpers.company_helper import get_company_bank_account_details

        description = []
        tax_exempt = False
        amount = invoice.charges_basic
        hsn_code = None
        division = None
        if old_invoice:
            try:
                total_by_service_type = json.loads(old_invoice.total_by_service_type)
                bss_tax_category = total_by_service_type.get("BSS")
            except:
                bss_tax_category = None

            service_type = GTA
            if bss_tax_category:
                service_type = BSS

            if old_invoice.service_tax_category:
                service_type = old_invoice.service_tax_category

            service_state = old_invoice.bh_source_state
            customer=old_invoice.customer
            invoice_state = old_invoice.invoice_state
            service_period_start = old_invoice.service_period_start
            service_period_end = old_invoice.service_period_end
            division = old_invoice.aggregation.division if hasattr(old_invoice, 'aggregation') else None
            invoice.service_tax_category = service_type
            fragment = old_invoice.contract_invoices.first()
            # assumption is all contract from fragment will have same hsn code
            if invoice.hs_code:
                hsn_code = invoice.hs_code.hsn_code
            else:
                hsn_code = fragment.contract.hsn_code.hsn_code \
                    if fragment and fragment.contract and fragment.contract.hsn_code else None
        else:
            if invoice.hs_code:
                hsn_code = invoice.hs_code.hsn_code
            service_state = invoice.bh_source_state
            service_type = invoice.service_tax_category  # change it
            customer = invoice.customer
            invoice_state = invoice.invoice_state
            service_period_start = invoice.service_period_start
            service_period_end = invoice.service_period_end

        if service_type == EXEMPT:
            tax_exempt = True

        config = old_invoice.invoice_configs if old_invoice else None

        service_desc = SERVICE_TAX_CATEGORIES_CODE_MAP.get(service_type, {}).get('name')
        hs_heading = SERVICE_TAX_CATEGORIES_CODE_MAP.get(service_type, {}).get('code')

        company_office = Office.objects.filter(state_id=service_state).first()
        if not company_office:
            company_office = Office.objects.first()
        head_office = Office.objects.filter(is_head=True).first()

        tax_charges, tax_cal, tax_rcm = TaxHelper.calculate_tax_amount(
            taxable_amount=amount, customer=customer, service_type=service_type,
            invoice_state=invoice_state, service_state=service_state, is_adjustment=False,
            tax_exempt=tax_exempt
        )
        cgst_rate = tax_cal.get(CGST, {}).get('percentage', 0)
        sgst_rate = tax_cal.get(SGST, {}).get('percentage', 0)
        igst_rate = tax_cal.get(IGST, {}).get('percentage', 0)
        vat_rate = tax_cal.get(VAT, {}).get('percentage', 0)

        cgst_amount = tax_cal.get(CGST, {}).get('amount', 0)
        sgst_amount = tax_cal.get(SGST, {}).get('amount', 0)
        igst_amount = tax_cal.get(IGST, {}).get('amount', 0)
        vat_amount = tax_cal.get(VAT, {}).get('amount', 0)

        total_amount = float(amount)
        amount_payable = float(amount)
        total_rcm_amount = 0
        if tax_rcm:
            total_rcm_amount = float(cgst_amount + sgst_amount + igst_amount + vat_amount)
        else:
            amount_payable = total_amount + float(
                                cgst_amount + sgst_amount + igst_amount + vat_amount)

        description.append(
            {
                "description": service_desc,
                "hs_heading": hsn_code or hs_heading,
                "value_of_service": intcomma(round(abs(amount), 2)),
                "discount": '0',
                "taxable_value": intcomma(abs(amount)),
                "cgst_rate": cgst_rate,
                "cgst_amount": intcomma(round(abs(cgst_amount), 2)),
                "sgst_rate": sgst_rate,
                "sgst_amount": intcomma(round(abs(sgst_amount), 2)),
                "igst_rate": igst_rate,
                "igst_amount":  intcomma(round(abs(igst_amount), 2)),
                "vat_rate": vat_rate,
                "vat_amount":  intcomma(round(abs(vat_amount), 2)),

            }
        )

        total_by_service_type = defaultdict(int)
        total_by_service_type[service_type] = amount

        invoice.bh_source_state = service_state
        invoice.invoice_state = invoice_state
        invoice.taxable_amount = amount
        invoice.total_amount = amount_payable
        invoice.total_rcm_amount = total_rcm_amount
        invoice.cgst_amount = cgst_amount
        invoice.sgst_amount = sgst_amount
        invoice.igst_amount = igst_amount
        invoice.vat_amount = vat_amount
        invoice.is_credit_debit_note = True
        invoice.is_stale=False
        invoice.service_period_start = service_period_start
        invoice.service_period_end = service_period_end
        invoice.customer=customer
        invoice.total_by_service_type = json.dumps(total_by_service_type)
        invoice.cgst_percentage = cgst_rate
        invoice.sgst_percentage = sgst_rate
        invoice.igst_percentage = igst_rate
        invoice.vat_percentage = vat_rate

        CreditDebitNote().calculate_adjustment(invoice, description, invoice_state, service_state, tax_exempt,
                                               service_type)

        customer_tax_info = CustomerTaxInformation.objects.filter(
            customer=invoice.customer, state=invoice_state, division=division).first()

        if not customer_tax_info:
            query = Q(customer=invoice.customer)
            query &= Q(division=division) if division else Q(state=invoice_state)
            customer_tax_info = CustomerTaxInformation.objects.filter(query).first()

        # customer_tax_info = CustomerTaxInformation.objects.filter(
        #     customer=customer, state=invoice_state).first()

        # if not customer_tax_info:
        #     customer_tax_info = CustomerTaxInformation.objects.filter(customer=customer).first()
        tax_legalname = None
        if customer_tax_info:
            customer_address = customer_tax_info.invoice_address
            state_name = str(customer_tax_info.state.name) if customer_tax_info else customer_address.state
            state_code = str(customer_tax_info.state.gst_code) if customer_tax_info else ""
            cust_gstin = str(customer_tax_info.gstin)
            tax_legalname = customer_tax_info.legalname
        elif customer:
            customer_address = customer.head_office_address
            state_name = customer.head_office_address.state if customer.head_office_address else ''
            state_code = ''
            cust_gstin =''

        static_base = settings.ROOT_DIR.path('website/static')
        logo_folder_path = '%s/website/images/%s/logo/' % (static_base, settings.WEBSITE_BUILD)
        company_extra_data = COMPANY_HEADER_FOOTER_DATA.get(settings.WEBSITE_BUILD)

        context = {
            "logo": logo_folder_path + "logo_rect.png"
            if invoice.timestamp_approved else logo_folder_path + "logo_alt.png",

            "company": {
                "name": company_office.company.name if company_office else '',
                "address": company_office.address if company_office else '',
                "gstin": company_office.gst_identification_no if company_office else '',
                "state": company_office.state.name,
                "bank_details": get_company_bank_account_details(config),
                "cin": company_office.company.cin,
                "pan": company_office.company.pan,
                "registered_address" : head_office.address,
                "telephone_number" : settings.BLOWHORN_SUPPORT_NUMBER,
                "website" : settings.HOST,
                "email" : company_extra_data.get('email'),


            },
            "customer": {
                "name": tax_legalname or invoice.customer.legalname,
                "address": str(customer_address.line1) if customer_address else '',
                "state": state_name,
                "state_code": state_code,
                "gstin": cust_gstin
            },

            "invoice": {
                "general": {
                    "invoice_no": invoice.invoice_number if invoice else '',
                    "invoice_date": invoice.invoice_date if invoice else '',
                    "service_period_start": service_period_start,
                    "service_period_end": service_period_end,
                    "aggregate_values": '',
                    "comments": invoice.comments if invoice else '',
                    "invoice_type": invoice.invoice_type
                },
                "description": description,
                "description_total": {
                    "value_in_figure": intcomma(round(abs(invoice.total_amount))),
                    "value_in_words": num2words(round(abs(invoice.total_amount)), lang='en_IN').title(),
                    "taxable_value": abs(invoice.taxable_amount),
                    "cgst_amount": float(abs(invoice.cgst_amount)),
                    "sgst_amount": float(abs(invoice.sgst_amount)),
                    "igst_amount": float(abs(invoice.igst_amount)),
                    "vat_amount": float(abs(invoice.vat_amount)),
                    "rcm_amount": float(abs(invoice.total_rcm_amount))
                }
            },
            "einvoicing": {
                'ack_no': '',
                'ack_date': '',
                'reverse_charge': "No" if service_type == BSS else "Yes",
                "irn": '',
                "qr_code": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAMI0lEQVR4Xu2dwXYjSQ4Du///oz1vT3sZV/RTNCZTVsyVAxIJEskq2Vb//vr6+vrVfymQAv+qwO8M0mSkwPcKZJCmIwUeFMggjUcKZJBmIAVeU6AN8ppuoT5EgQzyIY3umK8pkEFe0y3UhyiQQT6k0R3zNQUyyGu6hfoQBTLIhzS6Y76mQAZ5TbdQH6JABvmQRnfM1xTIIK/pFupDFMggH9LojvmaAhnkNd1CfYgC2iC/f//+0VKt/1yG9KP6a/y7N5f0o/NlEFDICowNgAuG6meQZ4VJP+yP/YMpahARuD1uBabzkX5Uf40n/rfHST/i3wZpg9CMvHU8g4zbZwUmeusNQPypPvG/PU7nJ/5tkDYIzchbxzPIuH1WYKJHNzjVX+OJ/+1x0o/4t0HaIDQjbx2/3iCW4Lo7t9/ApN+aP9Vf94fy2/Nj/vXHvD9dYGoQNYDipB/VX+OJ/zpuz0/85o9Y1CAiuI5bgQlv+ZN+VH+Nt+ezeHt+qp9Bxj/JpgZQfD3g6wGj89n4mn8GySCPM0oGtQNu8RnEKgh4KzDhLX0aUKq/xtvzWbw9P9Vvg7RB2iAPCmSQDJJBbjYIrUhagRS//RHj3c//7vxpfo5vkJ8uMDZg/Adn6wvip/cvg8hHLDIAxd99wN6dP/bn9E/Sf7rA2IA2iHoHovmhDYr9ySDPf1NvBcYGZJAM8qQA3QA0YBSnAaf6hKf6FKf6hKc48af6Fk/8KG7rE57q9w7SO8j0BqcBpDgNuDU41c8gGSSD9HOQ7xWwNxDh6YayN+Q6/5rfaf5Uvw0iN0gG2X5xoDUo4TPI19ejBjTgJDDhqQG351/zW+tD/Kl+G6QN0jtI7yC9g3ynAG1AuoEJTzc0xW19wlP9NkgbpA3SBmmDtEFoV/x7vA3SBmmDtEF2G4TuJXpGt8/IVJ/ilh/hqT7FSR+qT3iq3waRGwQFHuen+hS3A0Z4qk9xGnCqT3iqn0HGA7xuIDWY4pYf4ak+xWnAqT7hqX4GySC9g/QO0jtIn2LRruhTrH9VYL6ixxvqtbb/H2XPT3jLjx6RqD7hiV+PWOMBXjeQGkxxy4/wVJ/iNOBUn/BUP4NkkN5Bbn4HIQev4/YGsng6H+UnPN2glJ/wVH8dX/M/vkHWAlJ+K7DFW36EpwFf8yd+Nr7mn0HkI9a8QfJbTzLI898DkUEzSAZR7yA0YOv4/II6/b1YawEpvxXY4i0/wrdB2iA0I49xO+AWT+QpP+EzSAahGckgTx9jykdMJf5fANMFQhcEUegdRA7IvEG9pE8vuOMGIQK3x+kGsga5HX97f4gf9Y/w8w1CBG6Pk8C3D7jlf3t/iB+dn/AZBBQigTMIjdjZOPWP2GWQDEIz8tbxDDJuHwncBhk3QKan/lH6NkgbhGbkreMZZNw+ErgNMm6ATE/9o/RtkDYIzchbx48b5K3V+w/I2w1DFNf5qf5Pj+sN8tMFsudbD/A6vz3/u+MzyLiD6wFe5x/Lc336DDJu0XqA1/nH8lyfPoOMW7Qe4HX+sTzXp88g4xatB3idfyzP9ekzyLhF6wFe5x/Lc336DDJu0XqA1/nH8lyfXhuEGnRaAfpB0Zo/1Sd9iB/l/3Q86UvxDCL/Yo8EpgEm/KcPuD0/6UvxDJJBHmfEDuhpPBmA4hkkg2SQBwUySAbJIBnkewXoEYBWMMV7B/n9KBHpQ/0hPPWH4m2QNkgbpA3SBvlOAbqB7Q1+Gk8bguJ6g2AB+cVslH8dpwZTfRpAwtu45W/rE97qQ+fT+e2XV5MA6wNQfRsn/pTfNojyU9zyp/w2bvWh8+n8GeS5xdQAGhDbIMpPccuf8tu41YfOp/NnkAxih9zg9QCPH+F7B4Hu0g1Fw2EHgPJT3PKn/DZu9aHz6fxtkDaIHXKD1wPcBjHyeyzdUFTBDgDlp7jlT/lt3OpD59P52yBtEDvkBq8H+N03CIlHNwDh1wLb+nQ+4k/40/yoPsXX56P6FJ+/pCMB+aseNGCn69MAEH/C0/lsfsJTfYqvz0f1KZ5BxgalAaABJDw12OYnPNWn+Pp8VJ/iGSSDPM5IBlkrABY9fYOs61N+kp/wdAPa/ISn+hRfn4/qU7wN0gZpgzwokEEySAbJIN8rsF7xlJ8eYQhPjwg2P+GpPsXX56P6FNcbhA5IAq/xJIDlR/kpTvUJb/Wz+Qlv46TP/Pz2J+mW4BpPDbINoPwUp/qEt/rZ/IS3cdJnfv4M8vXYQ2rAegAoP/GjAbP5CW/jxH9+/gySQZ6GmAbQGoDwGUT+spltoG0ANZjiVJ/wdP51fuJn48R/fv42SBukDfK9An2K9ZVBMkgG+VYBu8LXjxCUf/6IIX+QSvwpbvtDeKqvNwgWGL9jaAHGA0D87ICv8dRfOh/hb49nkAyiZjSDKPl+/VrfcLZBxE8e/xfxo/qn8XR+4kf42+NtkDaImtEMouRrg9AAtUHkgI3hbZA2iBoxugBU8gvAGSSDqDHMIEq+HrFogHrEkgM2hs83yJj/8fQ04GuCZMB1fXv+0/xJnwxCCkHcDogsjx8j2/yEt+fPIKTwm8ftgNjjnx4we/7T/En/Nggp1AZ5VCCDyAH66XA7IFaf0zewPf9p/qR/G4QUaoO0QeSMfDTc3qBWvNM3sD3/af6kfxuEFGqDtEHMjNgbxNT+L7B0w9nzn86/1pD0secnvD2f3iAkgCV4Gk8NsOc/nX+tL+ljz094e74MAgpSA2gAqEGn8xM/Gyd97PkJr/mvv9XEEjyNpwbQABD/0/mJn42TPvb8hNf8M8izhNQAGgBq0On8xM/GSR97fsJr/hkkg9ghesJnkPHfUyyb9ye56YaiAaAap/MTPxsnfez5Ca/5t0HaIHaI2iAPCtgbYtmcP8l9O3/i9ydnfPp/7A285nf8fOsNYhtgBSI8Nfg0f+JH56O4Pd+aH/GnuD5fBvn9qLEVmBpI8fUA2vOt+ZE+FNfnyyAZhIbMvGOY3H8Dm0GkinQDWoElPfxmSpvfno/0s/wsXp+vDdIGMUOYQUA9Esg62DTvT7C38yd+f3LGPsV6XaX5LytmkNeb8z9kBnH62fk7bpDTA0D1SeA1nsZjzY/q0/kJb/nb/ITPIIf/gZ/TA0YDigMkf9WI6q/1wfOdfkm3AtABbQNO40+fj+rb/ll9iR/lJ3wbpA1CM/IYzyDjT7GswNRdukGo/mn86fNRfdKP8FZfm5/wbZA2CM1IG8QoRDfI+oYg7rb+afzp81F96j/hrb42P+HbIG0QmpE2iFGIbpD1DUHcbX2Lt/wIv45Tf9f1Sf91/TbIeINQA08PAPHLILJDJCClJzw1kOK2vsVbfoRfx9f9If6kP+FtvA3SBpm+Y9gBzSDyVxWoASQw3ZAWb/kRfh0nfdb1Sf91/TZIG6QN8qBABskgGSSDfK8APULQiic8PQJQfsKv4/Z8lt9pfdoghzeIHaA13g4oGczmX58/g2SQxxmzA5xBLh8warBtIOHXN9w6P+lH9Ukfm5/q23gb5HKD2wZbvB3gDHL5gFGDbQMJbwf0NJ70I36kj81P9W28DXK5wW2DLd4OcAa5fMCowbaBhLcDehpP+hE/0sfmp/o23ga53OC2wRZvBziDyAGzDbR420CLJ/6Un/AUJwNQfYu3/Ahv48c3iD2Axa8HgAaI+BM/wlOc+FF9i7f8CG/jGURuQDtA1EDKT3iK2wG3eMuP8DaeQTLI4wyRQTMIWNAKaB1u8Za/xRN/yk94itsBt3jLj/A23gZpg7RBHhTIIBkkg2SQ7xWgRxj7CEF4egQgfoSnOPGj+hZv+RHexucbxBI8jbcDQHh7vtMDTPzt+e35iB/FMwgoRA0+3sDxIyINEMVJP8If13f974OQALfHqcHHG5hBpiPUBmmDTAeMLhgqfvwCaoM8t4gafLyBbRDymIq3QdogaoAITBcM4Y9fQG2QNggNqYlnkPFXh5rm/A0sNfj4Ddcj1t9o87c59CPWlF3JU+CwAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Ahnk7v7E7rACGeRwAyp/twIZ5O7+xO6wAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Ahnk7v7E7rACGeRwAyp/twIZ5O7+xO6wAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Av8Ai8TmbD3x6tAAAAAASUVORK5CYII="

            }
        }

        invoice_template = "pdf/invoices/%s/invoice.html" % (settings.WEBSITE_BUILD)
        header = {"template": "pdf/invoices/b2b_v2/header.html", "context": context}
        footer = {"template": "pdf/invoices/b2b_v2/footer.html", "context": context}
        context_copy = copy.deepcopy(context)
        context_copy.update({'einvoicing': {}})
        invoice.context = json.dumps(context_copy, indent=4, sort_keys=True, default=str)
        invoice.save()

        bytes = generate_pdf(invoice_template, context, header=header, footer=footer)

        if bytes:
            file_name = str(invoice.invoice_number) + ".pdf" if invoice.invoice_number else str(
                invoice.id) + ".pdf"
            content = ContentFile(bytes)
            invoice.file.save(file_name, content, save=True)

    def calculate_adjustment(self, invoice, description, invoice_state, service_state, tax_exempt, service_type):

        from blowhorn.contract.constants import GTA, BSS
        from blowhorn.company.helpers.tax_helper import TaxHelper

        adjustment_record = invoice.customerinvoiceadjustment_set.values(
            'category__name', 'category__is_tax_applicable', 'comments').annotate(total_amount=Sum('total_amount'))
        total_amount = float(invoice.total_amount)
        taxable_amount = float(invoice.taxable_amount)

        total_taxable_amount, adjustment_amount, taxable_adj, non_tax_adj = 0, 0, 0, 0
        total_cgst_amount, total_sgst_amount, \
                total_igst_amount, total_rcm_amount, total_vat_amount = 0, 0, 0, 0, 0

        for record in adjustment_record:
            cgst_rate, sgst_rate, igst_rate, vat_rate,\
                sgst_amount, cgst_amount, igst_amount, vat_amount = [0] * 8
            amount_ = record.get('total_amount')
            adjustment_amount += amount_

            if not service_type and record.get('category__name') in [GTA, BSS]:
                service_type = record.get('category__name')
            if record.get('category__is_tax_applicable') and service_type:

                is_adjustment = False
                taxable_adj += float(amount_)
                if (float(invoice.taxable_amount) + float(amount_)) > 750:
                    is_adjustment = True
                tax_charges, tax_cal, tax_rcm = TaxHelper.calculate_tax_amount(
                    taxable_amount=amount_, customer=invoice.customer, service_type=service_type,
                    invoice_state=invoice_state, service_state=service_state, is_adjustment=is_adjustment,
                    tax_exempt=tax_exempt
                )
                cgst_rate = tax_cal.get(CGST, {}).get('percentage', 0)
                sgst_rate = tax_cal.get(SGST, {}).get('percentage', 0)
                igst_rate = tax_cal.get(IGST, {}).get('percentage', 0)
                vat_rate = tax_cal.get(VAT, {}).get('percentage', 0)

                cgst_amount = tax_cal.get(CGST, {}).get('amount', 0)
                sgst_amount = tax_cal.get(SGST, {}).get('amount', 0)
                igst_amount = tax_cal.get(IGST, {}).get('amount', 0)
                vat_amount = tax_cal.get(VAT, {}).get('amount', 0)

                total_cgst_amount += cgst_amount
                total_sgst_amount += sgst_amount
                total_igst_amount += igst_amount
                total_vat_amount += vat_amount

                total_amount = float(total_amount) + \
                                    float(cgst_amount + sgst_amount + igst_amount + vat_amount)
                if tax_rcm:
                    total_rcm_amount = float(total_rcm_amount) + \
                                            float(cgst_amount + sgst_amount + igst_amount + vat_amount)

            elif not record.get('category__is_tax_applicable'):
                non_tax_adj += amount_

            display_text = record.get('category__name')
            if record['comments']:
                display_text += "(" + record['comments'] + ")"
            description.append(
                {
                    "description": display_text,
                    "hs_heading": '',
                    "value_of_service": round(record.get('total_amount'), 2),
                    "discount": '0',
                    "taxable_value": round(record.get('total_amount'), 2),
                    "cgst_rate": cgst_rate,
                    "cgst_amount": intcomma(round(cgst_amount, 2)),
                    "sgst_rate": sgst_rate,
                    "sgst_amount": intcomma(round(sgst_amount, 2)),
                    "igst_rate": igst_rate,
                    "igst_amount": intcomma(round(igst_amount, 2)),
                    "vat_rate": vat_rate,
                    "vat_amount": intcomma(round(vat_amount, 2)),
                }
            )

        total_amount = total_amount + float(adjustment_amount) - float(total_rcm_amount)
        invoice.non_taxable_adjustment = non_tax_adj
        invoice.taxable_adjustment = taxable_adj
        invoice.total_service_charge = float(invoice.charges_basic) + float(invoice.charges_service) + \
                                       float(invoice.non_taxable_adjustment) + float(invoice.taxable_adjustment)

        invoice.cgst_amount = float(invoice.cgst_amount) + total_cgst_amount
        invoice.sgst_amount = float(invoice.sgst_amount) + total_sgst_amount
        invoice.igst_amount = float(invoice.igst_amount) + total_igst_amount
        invoice.vat_amount = float(invoice.vat_amount) + total_vat_amount
        invoice.total_rcm_amount = float(invoice.total_rcm_amount) + total_rcm_amount
        invoice.taxable_amount = float(invoice.taxable_amount) + float(adjustment_amount)
        invoice.total_amount = float(total_amount)
        invoice.save()



t_base = """Base Amount (A):\t\t\t\t\t\t%10.2f\n"""

s_charges = """Slab Charges (B):\t\t\t\t\t\t%10.2f\n"""

t_adjustment = """Taxable Adjustment (C):\t\t\t\t\t\t%10.2f\n"""

ruler = """----------------------------------------------------------------------------\n"""

taxable_amount = """\n\n\nTaxable Amount (F=A+B+C):\t\t\t\t\t%10.2f\n"""

non_taxable_amount = """Non Taxable Adjustment (D):\t\t\t\t\t%10.2f\n"""

ser_charge = """Total Service Charges (E=A+B+C+D):\t\t\t\t%10.2f\n"""

gst = """%s@ %s\t\t\t\t\t\t%10.2f\n"""

tot_incl_tax = """Total with Tax (L=E+G+H+I+J+K):\t\t\t\t\t%10.2f\n"""

payable_amount = """Total Payable :\t\t\t\t\t\t\t%10.2f\n"""

rcm_amount = """Tax Payable under RCM :\t\t\t\t\t\t%10.2f\n"""

toll_charges = """Toll Charges (J):\t\t\t\t\t\t%10.2f\n"""

parking_charges = """Parking Charges (K):\t\t\t\t\t\t%10.2f\n"""

call_charges = """Call Charges (L):\t\t\t\t\t\t%10.2f\n"""


def calculation_formatter(invoice):
    r = t_base % (invoice.charges_basic)
    r += s_charges % (invoice.charges_service)

    r += t_adjustment % (invoice.taxable_adjustment)
    r += non_taxable_amount % (invoice.non_taxable_adjustment)
    r += ruler

    r += ser_charge % (invoice.total_service_charge)
    r += taxable_amount % (invoice.taxable_amount)

    r += gst %('CGST', str(invoice.cgst_percentage) + '% (G):' , invoice.cgst_amount)
    r += gst %('SGST', str(invoice.sgst_percentage) + '% (H):' , invoice.sgst_amount)
    r += gst %('IGST', str(invoice.igst_percentage) + '% (I):' , invoice.igst_amount)
    r += gst %('VAT ', str(invoice.vat_percentage) + '% (J):' , invoice.vat_amount)
    r += toll_charges % (invoice.total_toll_charges)
    r += parking_charges % (invoice.total_parking_charges)
    r += call_charges % (invoice.total_call_charges)

    # r += ruler
    # r += tot_incl_tax % (invoice.total_with_tax)

    r += ruler
    r += payable_amount % (invoice.total_amount)
    r += rcm_amount % (invoice.total_rcm_amount)

    return r


class InvoiceExpirationBatch(models.Model):
    """
    Batch to aggregate the invoices which are marked as expired
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    description = models.CharField(_("Description"), max_length=100)
    number_of_entries = models.IntegerField(
        _("Num of entries"), null=True, blank=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Invoice Expiration Batch")
        verbose_name_plural = _("Invoice Expiration Batches")


class InvoicePaymentManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('collection')


class InvoicePayment(models.Model):
    """
    Payment made against each invoice
    """
    invoice = models.ForeignKey(CustomerInvoice, on_delete=models.PROTECT)
    collection = models.ForeignKey(CustomerReceivable, on_delete=models.PROTECT,
                                   null=True)
    amount = models.FloatField(_('Amount Paid'), default=0.0)
    tds = models.FloatField(_('TDS Deducted'), default=0.0)
    actual_amount = models.FloatField(_('Amount Credited to Bank'), null=True,
                                      blank=True)
    debit = models.FloatField(_('Debit'), null=True, default=0.0)
    paid_on = models.DateField(_("Payment Made On"), default=date.today)
    created_by = CreatingUserField(related_name='%(class)s_created_by')
    created_time = models.DateTimeField(auto_now_add=True)

    objects = InvoicePaymentManager()
    copy_data = CopyManager()

    def __str__(self):
        return '%s' % self.collection

    def get_debit_amount(self):
        total_amount_aggr = CustomerInvoice.objects.filter(
            related_invoice=self.invoice, invoice_type=CREDIT_NOTE,
            status__in=(APPROVED, UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED,
                        CUSTOMER_CONFIRMED, PARTIALLY_PAID, PAYMENT_DONE)
        ).aggregate(Sum('total_amount'))
        return abs(total_amount_aggr.get('total_amount__sum', None) or 0)

    def save(self, *args, **kwargs):
        self.amount = self.actual_amount
        _request = current_request()
        current_user = _request.user if _request else ''
        payments_list = []

        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')
                verbose_name = field.__dict__.get('verbose_name')

                if field_name in ['tds', 'actual_amount']:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                    if old_value != current_value:
                        payments_list.append(
                            InvoicePaymentHistory(invoice_payment=old, field=field_name,
                                                  verbose_name=verbose_name,
                                                  old_value=str(old_value),
                                                  new_value=current_value, user=current_user))

            InvoicePaymentHistory.objects.bulk_create(payments_list)

        if self.tds:
            self.amount = self.amount + self.tds

        self.debit = self.get_debit_amount()
        super(InvoicePayment, self).save(*args, **kwargs)

        if self.invoice.status == APPROVED:
            raise ValidationError('Approved state invoice cannot be marked as PARTIALLY PAID')

        if self.invoice.status in [SENT_TO_CUSTOMER, UPLOADED_TO_GST_PORTAL,
                                   CUSTOMER_ACKNOWLEDGED, CUSTOMER_CONFIRMED]:
            create_invoice_history(self.invoice.status, PARTIALLY_PAID,
                                   self.invoice, self.invoice.current_owner,
                                   self.invoice.due_date)
            CustomerInvoice.objects.filter(pk=self.invoice.pk).update(
                status=PARTIALLY_PAID)

    def delete(self, using=None, keep_parents=False):
        from blowhorn.customer.signals import calculate_unallocated_amount
        calculate_unallocated_amount(InvoicePayment, self, False, source=self.id)
        super(InvoicePayment, self).delete()
        payment = InvoicePayment.objects.filter(invoice=self.invoice).exclude(id=self.id).aggregate(Sum('actual_amount'))
        payment_sum = payment.get('actual_amount__sum', 0) or 0

        invoice = self.invoice
        if payment_sum < 10:
            invoice.status = CUSTOMER_CONFIRMED
        elif 10 < payment_sum < self.invoice.total_amount:
            invoice.status = PARTIALLY_PAID
        invoice.save()


class InvoicePaymentHistory(models.Model):
    invoice_payment = models.ForeignKey(InvoicePayment, on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    verbose_name = models.CharField(_('Field Changed'), max_length=250)
    old_value = models.CharField(max_length=100, null=True, blank=True)
    new_value = models.CharField(max_length=100, null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.invoice_payment.invoice.invoice_number

class ManualUploadReason(BaseModel):
    name = models.CharField(_('Reason'), max_length=60)
    description = models.CharField(_('Description'), max_length=250, null=True, blank=True)
    is_active = models.BooleanField(_('Is Active'), default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Invoice Manual Upload Reason")
        verbose_name_plural = _("Invoice Manual Upload Reasons")


class InvoiceUploadBatch(models.Model):
    """
    Invoice Batch uploaded to GST Portal
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    description = models.CharField(_("Description"), max_length=100)
    number_of_entries = models.IntegerField(
        _("Num of Invoices"), null=True, blank=True)
    successful_uploads = models.IntegerField(
        _("Successfully Uploaded to GST Portal"), null=True, blank=True)
    failed_uploads = models.IntegerField(
        _("Failed to Upload to GST Portal"), null=True, blank=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    file = models.FileField(upload_to=generate_file_path, validators=[
        FileValidator(allowed_extensions=["pdf"])], max_length=256, blank=True, null=True)
    message = models.TextField(null=True, blank=True)

    objects = models.Manager()

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Invoice GST Portal Upload Batch")
        verbose_name_plural = _("Invoice GST Portal Upload Batches")


class SendInvoiceBatch(models.Model):
    """
    Invoice Batch send to customer
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    description = models.CharField(_("Description"), max_length=100)
    number_of_entries = models.IntegerField(
        _("Num of Invoices"), null=True, blank=True)
    sent_to_customers = models.IntegerField(
        _("Successfully Sent to Customers"), null=True, blank=True)
    failed_to_send = models.IntegerField(
        _("Failed to Send to Customers"), null=True, blank=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    file = models.FileField(upload_to=generate_file_path, max_length=256, blank=True, null=True)
    message = models.TextField(null=True, blank=True)

    objects = models.Manager()

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Invoices Sent to Customer Batch")
        verbose_name_plural = _("Invoices Sent to Customer Batches")


class CreditDebitReason(BaseModel):
    name = models.CharField(_('Reason'), max_length=60)
    description = models.CharField(_('Description'), max_length=250, null=True, blank=True)
    is_active = models.BooleanField(_('Is Active'), default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Credit Debit Note Reason")
        verbose_name_plural = _("Credit Debit Note Reasons")
