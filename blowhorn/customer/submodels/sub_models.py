from blowhorn.common.models import BaseModel
from django.db import models
from django.utils.translation import ugettext_lazy as _
from blowhorn.customer.constants import CUST_LEDGER_STATUS


class CustomerLedgerStatement(BaseModel):
    """
    Customer Ledger with all his transactions
    """

    customer = models.ForeignKey('customer.Customer', on_delete=models.PROTECT)

    transaction_time = models.DateTimeField(auto_now_add=True)

    amount = models.DecimalField(_('Amount'), max_digits=10, decimal_places=2)

    balance = models.DecimalField(
        _('Balance'), max_digits=10, decimal_places=2)

    description = models.CharField(_("Description"), max_length=250)

    source = models.ForeignKey('payment.Source', on_delete=models.PROTECT, null=True, blank=True)

    order = models.ForeignKey('order.Order', on_delete=models.PROTECT, null=True, blank=True)

    wallet_source = models.ForeignKey('payment.TransactionOrder', on_delete=models.PROTECT, null=True, blank=True)

    response = models.TextField('Payment response', null=True)

    status = models.CharField(
        _('Status'), max_length=50,
        choices=CUST_LEDGER_STATUS, null=True, blank=True)


class CustomerBalance(CustomerLedgerStatement):

    class Meta:
        proxy = True
        verbose_name = _('Customer Balance')
        verbose_name_plural = _('Customer Balances')



