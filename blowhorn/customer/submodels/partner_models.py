from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _
from blowhorn.common.models import BaseModel
from blowhorn.oscar.apps.order.models import Line


class PartnerAccessKey(models.Model):
    # from blowhorn.customer.models import Partner

    partner = models.ForeignKey('customer.Partner', on_delete=models.DO_NOTHING)
    response_time = models.CharField(_("Response Time"), max_length=25)
    access_token = models.CharField(_("Access Token"), max_length=250)
    refresh_token = models.CharField(_("Refresh Token"), max_length=250)
    response = models.TextField('Client response', null=True)

    def __str__(self):
        return str(self.response_time)


class ContactCreation(BaseModel):

    user = models.ForeignKey('users.User', on_delete=models.DO_NOTHING)

    contact_reference = models.CharField(_("Response Time"), max_length=100)

    response = models.TextField('Client response', null=True)


class PartnerLineMap(BaseModel):

    order = models.ForeignKey('order.Order', on_delete=models.DO_NOTHING, null=True, blank=True)

    line = models.ForeignKey(Line, on_delete=models.DO_NOTHING, null=True, blank=True)

    reference_id = models.CharField(_("Refresh Token"), max_length=250)


class CeatOrderPush(BaseModel):

    order = models.ForeignKey('order.Order', on_delete=models.DO_NOTHING)

    reference_id = models.CharField(_("Refresh Token"), max_length=250)
