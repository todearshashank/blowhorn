import json
import logging
import phonenumbers
from django.conf import settings
from django.contrib.sites.models import Site
from django.db import transaction, IntegrityError
from django.db.models import F, Q
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework.response import Response
from organizations.backends import invitation_backend, registration_backend
from phonenumber_field.phonenumber import PhoneNumber

from blowhorn.common.redis_helper import StrictRedisHelper
from blowhorn.address.models import Hub, Slots
from blowhorn.contract.models import Contract
from blowhorn.customer.models import (InvoiceBatch,
    InvoiceCreationRequest, get_partner_from_api_key,
    CustomerInvoiceConfig, CustomerOTP, Route, OpeningBalance,
    CustomerFeedback, CustomerTaxInformation, CustomerDivision,
    CustomerContact, CustomerReceivable, CustomerInvoice, CustomerEmailLog,
    PartnerCustomer, CustomerRating, Customer,
    CUSTOMER_CATEGORY_INDIVIDUAL, AccountUser, PermissionLevelStore,
    UserModule, CustomerBankDetails)
from blowhorn.customer.submodels.customer_migration_history import \
    CustomerMigrationHistory
from blowhorn.driver.models import NonOperationPayment
from blowhorn.order.models import Order
from blowhorn.payment.models import PayTmUserDetails
from blowhorn.trip.models import Trip
from blowhorn.users.models import User


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CustomerRatingMixin(object):
    def create_rating(self, data):
        customer_rating_data = {
            'num_of_feedbacks': 1,
            'cumulative_rating': float(data.get('rating')),
        }

        customer_rating = CustomerRating(**customer_rating_data)
        customer_rating.save()
        customer = data.get('customer')
        Customer.objects.filter(pk=customer.id).update(
            customer_rating=customer_rating)

        return Response(status=status.HTTP_200_OK,
                        data=_('Customer Rating saved successfully'))

    def update_rating(self, data):
        num_of_feedbacks = data.get('num_of_feedbacks')
        cumulative_rating = data.get('cumulative_rating')

        new_cumulative_rating = (float(cumulative_rating) * num_of_feedbacks +
                                 float(data.get('rating'))) / (
                                    num_of_feedbacks + 1)
        customer = data.get('customer')

        CustomerRating.objects.filter(pk=customer.customer_rating_id) \
            .update(cumulative_rating=new_cumulative_rating,
                    num_of_feedbacks=num_of_feedbacks + 1)

        return Response(status=status.HTTP_200_OK,
                        data=_('Customer Rating updated successfully'))


class CustomerTaxInformationMixin(object):

    def get_state_code_from_gstin(self, gstin):
        """
        :param gstin: GSTIN String
        :return: State code
        """
        return gstin[0:2]


class CustomerMixin(object):

    def get_response_message(self, message, data=None):
        return {
            'message': _(message),
            'data': data
        }

    def send_invitation(self, name, email, phone_number, organization,
                        request=None, resend=False):
        """
        :param name: String
        :param email: String
        :param phone_number: String
        :param organization: instance of Organization (Customer)
        :param request: request
        :param resend: boolean
        :return: instance of user
        @todo Send invite link through sms
        """
        site = Site.objects.get(pk=settings.SITE_ID)
        user, created = invitation_backend().invite_by_email(
            email,
            phone_number=phone_number,
            sender=request.user if request else None,
            **{'name': name,
               'organization': organization,
               'domain': site,
               'base_url': settings.HOST,
               'created_required': True,
               'resend': resend
            })

        return user, created

    def send_registration_invitation(self, customer):
        """
        Sends an invite to organization owner to set password
        :param organization: instance of Organization (Customer)
        :return: instance of user
        """
        logger.info('Sending invite to organization %s' % customer)
        return registration_backend().invite_owner_by_email(customer)

    def is_mobile_number_changed(self, user, phone_number, mobile):
        """
        :param user: instance of User
        :param phone_number: phone_number as PhoneNumber class
        :param mobile: String
        :return: Boolean
        """
        if user.phone_number != phone_number or not user.is_mobile_verified:
            user.send_otp(mobile)
            return True
        return False

    def has_tax_info_edit_permission(self, user, customer):
        """
        :param user: instance of User
        :param customer: instance of Customer (Organization)
        :return: Boolean

        User is allowed to edit organization information only
        if he/she's owner or admin of that organization.
        Normal members will have view permission.
        """
        can_edit = True
        if customer.customer_category is not CUSTOMER_CATEGORY_INDIVIDUAL:
            can_edit = customer.is_admin(user) or customer.is_owner(user)

        return can_edit

    def get_customer(self, user, api_key=None):
        """
        :param user: instance of User
        :param api_key: API key of customer
        :return: instance of Customer
        """
        if api_key:
            return Customer.objects.filter(api_key=api_key).first()

        if user.is_anonymous:
            return None

        if user.is_customer():
            return user.customer

        # One user will be in single organization
        customer = Customer.objects.filter(users=user).first()
        if customer:
            return customer

        return None

    def get_customer_with_api_key(self, api_key):
        """
        :param api_key: api_key of the customer
        :return: instance of Customer
        """
        customer = Customer.objects.filter(api_key=api_key).first()
        if customer:
            return customer

        return None

    def get_partner_with_api_key(self, api_key):
        """
        :param api_key: api_key of the partner
        :return: instance of Partner
        """
        partner = get_partner_from_api_key(api_key)
        if partner:
            return partner

        return None

    def validate_phonenumber(self, phone_number):
        """
        :param phone_number: String
        :return: Boolean
        """
        try:
            phone_number = PhoneNumber.from_string(
                phone_number, settings.COUNTRY_CODE_A2)

            if not phone_number.is_valid():
                return False

        except phonenumbers.NumberParseException:
            return False

        return True

    def __delete_customers(self, customer_pks):
        deleted_customers_count = 0
        log_dump = ''
        try:
            deleted_customers = Customer.objects.filter(
                id__in=customer_pks).delete()
            if deleted_customers:
                deleted_customers_count = deleted_customers[1].get('customer.Customer')
                log_dump = '%s customers are deleted: %s, %s' % (
                    deleted_customers_count, customer_pks, deleted_customers)
        except BaseException as be:
            log_dump = '%s' % be

        return deleted_customers_count, log_dump

    def __do_write(self, org, customer_pks):
        log_dump = []
        try:
            with transaction.atomic():
                orders = Order.objects.filter(customer_id__in=customer_pks). \
                    update(customer=org)
                if orders:
                    log_dump.append('No. of Order: %s' % orders)

                contracts = Contract.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if contracts:
                    log_dump.append('No. of Contract: %s' % contracts)

                hubs = Hub.objects.filter(customer_id__in=customer_pks). \
                    update(customer=org)
                if hubs:
                    log_dump.append('No. of Hub: %s' % hubs)

                invoice_batches = InvoiceBatch.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if invoice_batches:
                    log_dump.append('No. of InvoiceBatch: %s' % invoice_batches)

                invoices = CustomerInvoice.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if invoices:
                    log_dump.append('No. of CustomerInvoice: %s' % invoices)

                invoice_requests = InvoiceCreationRequest.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if invoice_requests:
                    log_dump.append(
                        'No. of InvoiceCreationRequest: %s' % invoice_requests)

                invoice_configs = CustomerInvoiceConfig.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if invoice_configs:
                    log_dump.append(
                        'No. of CustomerInvoiceConfig: %s' % invoice_configs)

                email_logs = CustomerEmailLog.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if email_logs:
                    log_dump.append(
                        'No. of Email logs: %s' % email_logs
                    )

                nonops_payments = NonOperationPayment.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if nonops_payments:
                    log_dump.append(
                        'No. of NonOperationPayment: %s' % nonops_payments)

                otps = CustomerOTP.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if otps:
                    log_dump.append('No. of CustomerOTP: %s' % otps)

                routes = Route.objects.filter(customer_id__in=customer_pks). \
                    update(customer=org)
                if routes:
                    log_dump.append('No. of Route: %s' % routes)

                op = OpeningBalance.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if op:
                    log_dump.append('No. of OpeningBalance: %s' % op)

                feedback = CustomerFeedback.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if feedback:
                    log_dump.append('No. of CustomerFeedback: %s' % feedback)

                division = CustomerDivision.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if division:
                    log_dump.append('No. of CustomerDivision: %s' % division)

                contacts = CustomerContact.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if contacts:
                    log_dump.append('No. of CustomerContact: %s' % contacts)

                receivable = CustomerReceivable.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if receivable:
                    log_dump.append(
                        'No. of CustomerReceivable: %s' % receivable)

                paytm_users = PayTmUserDetails.objects.filter(
                    customer_id__in=customer_pks).update(customer=org)
                if paytm_users:
                    log_dump.append('No. of PayTmUserDetails: %s' % paytm_users)

                acc_users = AccountUser.objects.filter(organization_id__in=customer_pks)
                num_org_users = acc_users.update(organization=org)
                if num_org_users:
                    log_dump.append('Account users: %s' % acc_users)
                    log_dump.append('No. of org. users: %s' % num_org_users)

        except IntegrityError as ie:
            log_dump.append('%s' % ie)
        except BaseException as be:
            log_dump.append('%s' % be)

        return log_dump

    def delete_partner_customer(self, destination, source_list):
        PartnerCustomer.objects.filter(customer__in=source_list).update(customer=destination)

    def merge_customer_data(self, customer_pk, merging_customers_list):
        """
        :param customer_pk: pk of Customer (Target Customer)
        :param merging_customers_list (pk, email)
            - To migrate / delete relative info about given user-id.
            - When an existing user has been invited to an organization,
            - Link Order, Contract and all related data to organization of
                invitee.
            - NOTE: This task will be executed only when user accepts invitation
                and joined to new organization.

        """
        org = Customer.objects.get(pk=customer_pk)
        migration_history = {
            'customer': ''.join([org.name[:250], '(', str(org.pk), ')']),
            'category': CustomerMigrationHistory.CATEGORY_MERGE_CUSTOMER
        }
        ''' Since merging doesn't contain more than 5 users,
            below logic is used to capture history in detail.
        '''
        customer_pks = [x.get('pk') for x in merging_customers_list]
        migration_history['log_dump'] = self.__do_write(org, customer_pks)

        failed_tax_info = []
        for cust in merging_customers_list:
            user_email = cust.get('user__email', '')
            try:
                tax_info = CustomerTaxInformation.objects.filter(
                    customer_id=cust.get('pk')).update(customer=org)
                if tax_info:
                    migration_history['log_dump'].append(
                        'Tax info updated %s: %s' % (user_email, tax_info))

            except IntegrityError as ie:
                migration_history['log_dump'].append('%s' % ie)
                failed_tax_info.append(user_email)
            except BaseException as be:
                migration_history['log_dump'].append('%s' % be)
                failed_tax_info.append(user_email)

        self.delete_partner_customer(org, customer_pks)
        deleted_customers, log = self.__delete_customers(customer_pks)
        migration_history['number_of_customers'] = deleted_customers
        migration_history['log_dump'].append(log)
        migration_history['log_dump'].append(
            '%s' % [x.get('user__email') for x in merging_customers_list])
        logger.info(migration_history)
        self.__create_customer_data_log(migration_history)
        return deleted_customers, failed_tax_info

    def __create_customer_data_log(self, data, performed_by=None):
        try:
            logger.info('Saving customer migration log..')
            instance = CustomerMigrationHistory.objects.create(**data)
            if performed_by is not None:
                instance.created_by = performed_by
                instance.save()

        except BaseException as be:
            logger.info('%s' % be)
            logger.error('ALERT!! Customer migration history creation failed.')

    def get_permission_from_cache(self, user, reset_cache=False):
        conn = StrictRedisHelper().get_connection()
        key_map = "user_permission" + str(user.id)
        cache_data = conn.get(key_map)
        if cache_data and reset_cache:
            conn.delete(key_map)

        if not cache_data or reset_cache:
            dict_ = self.get_permission_dict(user)
            conn.set(key_map, dict_)
            cache_data = json.loads(dict_)
        return cache_data

    def delete_permission_from_cache(self, user):
        conn = StrictRedisHelper().get_connection()
        key_map = "user_permission" + str(user.id)
        cache_data = conn.get(key_map)
        if cache_data:
            """
            delete the permission from redis , when the user is marked inactive
            """
            conn.delete(key_map)

    def get_permission_dict(self, user):
        dict_ = {}
        if settings.USE_READ_REPLICA:
            modules = UserModule.objects.using('readrep1').only('code')
        else:
            modules = UserModule.objects.only('code')

        for i in range(0, 2):
            module_dict = {}
            val = 'TMS' if i == 0 else 'WMS'
            filter_dict = {
                'accountuser__user': user,
                'permission_level__system__code': val
            }
            if settings.USE_READ_REPLICA:
                permission_store = PermissionLevelStore.objects.using('readrep1').filter(**filter_dict)
            else:
                permission_store = PermissionLevelStore.objects.filter(**filter_dict)

            permission_store = permission_store.only(
                'value'
            ).annotate(
                module_code=F('module__code'),
                permission_name=F('permission_level__name')
            )

            for x in modules:
                """
                Dynamically populate the module defined as key
                """
                module_dict.update({
                    x.code.lower(): {}
                })

            for x in permission_store:
                """
                Construct the permission as dict
                """
                key = module_dict.get(x.module_code.lower(), {})
                if not x.permission_name in key.keys():
                    module_dict[x.module_code.lower()][x.permission_name.lower()] = x.value
                else:
                    module_dict[x.module_code.lower()].update({
                        x.permission_name.lower(): module_dict[x.module_code.lower()][x.permission_name.lower()] + x.value
                    })
            dict_[val] = module_dict

        return json.dumps(dict_)

    def get_module_permissions(self, module, user, system='TMS', reset_cache=False):
        use_redis = False #not settings.DEBUG or (not settings.DEBUG and not settings.ENABLE_SLACK_NOTIFICATIONS)
        if use_redis:
            permissions = self.get_permission_from_cache(user, reset_cache=reset_cache)
        else:
            permissions = self.get_permission_dict(user)

        if not isinstance(permissions, dict):
            permissions = json.loads(permissions)

        return permissions[system][module]

    def get_permission_level_values(self, permission_levels, level_name):
        for key, val in permission_levels.items():
            if key.lower() == level_name.lower():
                return val

        return []

    def get_permission_query(self, user, module, model, reset_cache=False, system='TMS', fieldname_map=None):
        use_redis = False #not settings.DEBUG or (not settings.DEBUG and not settings.ENABLE_SLACK_NOTIFICATIONS)
        if use_redis:
            permission = self.get_permission_from_cache(user, reset_cache=reset_cache)
        else:
            permission = self.get_permission_dict(user)

        if not isinstance(permission, dict):
            permission = json.loads(permission)

        query = Q()
        account_user = AccountUser.objects.filter(user=user).only('is_admin').first()
        if account_user and not account_user.is_admin:
            module_permissions = permission.get(system, {}).get(module, {})
            if not module_permissions:
                return False
            else:
                for key, val in module_permissions.items():
                    query_field = fieldname_map.get(key, None) or None
                    if query_field:
                        query |= Q(**{query_field.lower() + "__in": val})

        return query

    def get_available_user_modules(self, query_params={}, fields=[], ordering=[]):
        """
        query_params: system_id, code, etc. (dict, optional)
        fields: field names of UserModule (list of str)
        ordering: field names for ordering (list of str)
        """
        if settings.USE_READ_REPLICA:
            qs = UserModule.objects.using('readrep1').filter(**query_params)
        else:
            qs = UserModule.objects.filter(**query_params)

        if fields:
            qs = qs.values(*fields)

        if ordering:
            qs = qs.order_by(*ordering)

        return qs

    def soft_delete_customer_data(self, performed_by_id):
        """
        Mark customer related data as deleted
        :param customer_pk: pk of Customer
        :param request: Django request
        """
        performed_by = User.objects.get(pk=performed_by_id)
        customer = performed_by.customer
        customer_pk = customer.pk
        log_data = {
            'customer': customer.name[:250],
            'category': CustomerMigrationHistory.CATEGORY_DATA_DELETE_REQUEST
        }
        query_model_mapping = {
            'Order': {
                'customer_id': customer_pk,
                'is_deleted': False,
            },
            'Trip': {
                'customer_contract__customer_id': customer_pk,
                'is_deleted': False,
            },
            'CustomerInvoice': {
                'customer_id': customer_pk,
                'is_deleted': False,
            },
        }
        if customer.is_non_individual():
            query_model_mapping.update({
                'AccountUser': {
                    'organization_id': customer_pk,
                    'is_deleted': False,
                },
                'Slots': {
                    'customer_id': customer_pk,
                    'is_deleted': False,
                },
                'Route': {
                    'customer_id': customer_pk,
                    'is_deleted': False,
                },
                'CustomerBankDetails': {
                    'customer_id': customer_pk,
                    'is_deleted': False,
                }
            })
        log_dump = []
        num_of_records_affected = 0
        for k in query_model_mapping:
            logger.info('Updating flag for %s' % k)
            updated = eval(k).objects.filter(**query_model_mapping[k]).update(is_deleted=True)
            if updated:
                num_of_records_affected += updated
                log_dump.append('# of %s: %s' % (k, updated))

        log_data['log_dump'] = log_dump
        log_data['number_of_customers'] = num_of_records_affected
        self.__create_customer_data_log(log_data, performed_by=performed_by)
