from datetime import datetime
from django import template
from django.utils import timezone

register = template.Library()


@register.filter()
def divide(n1, n2):
    try:
        return n1 / n2
    except ZeroDivisionError:
        return None


@register.filter()
def percentof(amount, total):
    try:
        return '{:.1f}%'.format(amount / total * 100)
    except ZeroDivisionError:
        return None


@register.filter(name='sum_of_column')
def sum_of_column(source_array, column_name):
    # new_grades_list = [g.getattr(column_name, 0) for g in source_array]
    processed_list = [getattr(o, column_name, 0) for o in source_array]
    try:
        return sum(processed_list)
    except BaseException:
        return 0


@register.filter(name='date_delta')
def date_delta(end_date, start_date):
    if not end_date or not start_date:
        return 100

    now = timezone.now().date()
    if end_date <= now:
        return 0

    try:
        delta_start_and_end = (end_date - start_date).total_seconds()
        delta_now_and_end = (end_date - now).total_seconds()
        return (delta_now_and_end / delta_start_and_end) * 100
    except BaseException:
        return 0


class SetVarNode(template.Node):

    def __init__(self, var_name, var_value):
        self.var_name = var_name
        self.var_value = var_value

    def render(self, context):
        try:
            value = template.Variable(self.var_value).resolve(context)
        except template.VariableDoesNotExist:
            value = ""
        context[self.var_name] = value

        return u""


@register.tag(name='assign_val')
def set_var(parser, token):
    """
    {% set some_var = '123' %}
    """
    parts = token.split_contents()
    if len(parts) < 4:
        raise template.TemplateSyntaxError("'assign_val' tag must be of the "
                                           "form: {% assign_val <var_name> = <var_value> %}")

    return SetVarNode(parts[1], parts[3])


@register.filter
def next(source_list, current_index):
    """
    Returns the next element of the list using the current index if it exists.
    Otherwise returns an empty string.
    """
    try:
        return source_list[int(current_index) + 1]
    except:
        return ''


@register.filter
def previous(source_list, current_index):
    """
    Returns the previous element of the list using the current index if it exists.
    Otherwise returns an empty string.
    """
    try:
        return source_list[int(current_index) - 1]
    except:
        return ''
