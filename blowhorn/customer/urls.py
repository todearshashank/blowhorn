from django.conf.urls import url, include

from blowhorn.customer.subviews.customer_assets import CustomerContainerView
from blowhorn.customer.subviews.ondemand_order import OndemandOrderView, OndemandOrderContractDataView
from blowhorn.customer.subviews.order_items import OrderItemsView
from blowhorn.customer.subviews.profile import CustomerDataRequestView
from blowhorn.customer.subviews.proof_of_delivery import ProofOfDeliveryView
from blowhorn.customer.subviews.shipment_slots import ShipmentSlotFilterView, ShipmentSlotView, \
    ShipmentSlotDriversDetailView, ShipmentSlotAssignDriver, ShipmentSlotCoverageView
from blowhorn.customer.subviews.slot_drivers import ShipmentSlotDriverListView
from blowhorn.customer.subviews.track_parcel import TrackParcelView
from blowhorn.customer.views import *
from blowhorn.customer.subviews.login import  SendOTPLoginView
from blowhorn.customer.subviews.permission import *
from blowhorn.customer.subviews.organization import *
from blowhorn.customer.subviews.cod_payments import FleetCodPaymentsListView, FleetCodPaymentsSummaryStatusListView, \
    FleetCodPaymentsExportStatusListView
from .views import CustomerApiKeyView, WmsCustomer


urlpatterns = [
    # Invoice summary partials
    url(r'^summary/invoice/list/$', invoice_summary_list, name='invoice-summary-list'),
    url(r'^summary/invoice/(?P<invoice_pk>[0-9]+)/history$', invoice_history, name='invoice-summary-history'),

    url(r'^profile/$', CustomerProfile.as_view(), name='profile'),
    url(r'^otp/$', Otp.as_view(), name='otp'),
    url(r'^verifyotp/$', VerifyOtp.as_view(), name='verify-otp'),
    url(r'^verifyotpbooking/$', VerifyOtpBooking.as_view(), name='verify-otp-booking'),
    url(r'^location$', SaveUserAddress.as_view(), name='location'),
    url(r'^customer/login/$', SendOTPLoginView.as_view(), name='customer-otp-login'),
    url(r'^customer/auth$', CustomerApiKeyView.as_view(), name='customer-api-key-basicauth'),
    url(r'^customer/data$', CustomerDataRequestView.as_view(),
        name='customer-data-request'),
    url(r'customer/wms-status', WmsCustomer.as_view(), name='customer-wms-status'),

    # My Fleet
    url(r'^fleet/search/load-choices/$', FleetSearchChoices.as_view(), name='fleet-search-choices'),
    url(r'^fleet/trips/$', FleetTripList.as_view(), name='fleet-list-trips'),
    url(r'^fleet/trips/(?P<pk>[0-9]+)/$', FleetTripDetail.as_view(), name='fleet-trip-detail'),
    url(r'^fleet/search/shipments-choices$', FleetShipmentsSearchChoices.as_view(), name='fleet-search-shipments-choices'),
    url(r'^fleet/shipments$', FleetShipmentOrderList.as_view(), name='fleet-list-orders'),
    url(r'^fleet/shipments/order-batches$', FleetOrderBatchList.as_view(), name='fleet-list-order-batches'),
    url(r'^fleet/drivers/(?P<driver_id>[0-9]+)/status/$', FleetDriverStatus.as_view(), name='fleet-driver-status'),
    url(r'^fleet/shipments/export$', FleetShipmentExportList.as_view(), name='fleet-export-orders-list'),
    url(r'^fleet/shipment/slots/drivers$', ShipmentSlotDriverListView.as_view(), name='shipment-slots-available-drivers'),
    url(r'^fleet/shipment/assets$', CustomerContainerView.as_view(), name='order-containers'),
    url(r'^fleet/trips/(?P<trip_pk>[0-9]+)/pod$', ProofOfDeliveryView.as_view(), name='stop-documents'),
    # Organization Management
    url(r'^fleet/members$', OrganizationMemberList.as_view(), name='org-members'),
    url(r'^fleet/member/invite$', OrganizationInviteMember.as_view(), name='org-invite-member'),
    url(r'^fleet/member/supportdata$', PermissionSupportDataView.as_view(), name='org-members-supportdata'),
    url(r'^fleet/member/permissionlevel$', PermissionLevelView.as_view(), name='org-members-permissionlevel'),
    url(r'^fleet/member/(?P<pk>[0-9]+)$', OrganizationDetailsView.as_view(), name='org-members-details'),
    url(r'^fleet/member/(?P<user_pk>[0-9]+)/permissions$', UserPermissionView.as_view(), name='org-user-permissions'),
    url(r'^fleet/member/groups$', UserGroupListView.as_view(), name='org-groups-view'),
    url(r'^fleet/member/group$', UserGroupView.as_view(), name='org-add-group-view'),
    url(r'^fleet/member/group/(?P<pk>[0-9]+)$', UserGroupView.as_view(), name='org-group-view'),

    # Ondemand vehicle request
    url(r'^fleet/order/request/$', OndemandOrderView.as_view(), name='ondemand-order-request'),
    url(r'^fleet/order/request/contracts$', OndemandOrderContractDataView.as_view(), name='ondemand-order-request-contracts'),

    # Shipment Slots
    url(r'^fleet/slots/load-filter$', ShipmentSlotFilterView.as_view(), name='shipment-slot-filter'),
    url(r'^fleet/slots$', ShipmentSlotView.as_view(), name='shipment-slots'),
    url(r'^fleet/slots/(?P<pk>[0-9]+)/coverage$', ShipmentSlotCoverageView.as_view(), name='shipment-slots-coverage'),
    url(r'^hub/coverage$', ShipmentSlotCoverageView.as_view(), name='shipment-hub-coverage'),
    url(r'^fleet/slots/(?P<pk>[0-9]+)/drivers$', ShipmentSlotDriversDetailView.as_view(), name='shipment-slots-drivers'),
    url(r'^fleet/shipment/driver/assign$', ShipmentSlotAssignDriver.as_view(), name='shipment-slots-assign-driver'),
    url(r'^fleet/shipment/items', OrderItemsView.as_view(), name='order-containers'),

    url(r'^mytrips/trips/$', MyTripsTripList.as_view(), name='mytrips-list-trips'),
    url(r'^mytrips/trips/export$', MyTripsTripListExport.as_view(), name='mytrips-list-trips-export'),

    url(r'^customers/shipment$', ShipmentCustomerList.as_view(), name='shipment-customer-list'),
    url(r'^customer/route$', RouteView.as_view(), name='customer-route'),
    url(r'^customer/orderline/information', OrderLineInfoView.as_view()),
    url(r'^customer/invoice/autocomplete/email$', ContactAutocompleteView.as_view(), name='contact-autocomplete'),

    url(r'^customer/orderline/information', OrderLineInfoView.as_view(), name='order-line-info-pos'),
    url(r'^customer/hubdetails', CustomerHubDetails.as_view(), name='customer-hub-details'),

    url(r'^customer/partners/getinvoicedetails/$', GetInvoiceDetails.as_view(), name='get-partner-customerinvoice-details'),
    url(r'^customer/partners/updateinvoiceflag/$', UpdateInvoiceProcessedFlag.as_view(), name='put-invoice-isprocessed-flag'),
    url(r'^customer/mask-call', CallMasking.as_view(), name='customer-mask-call'),

    # Channel Integration
    url(r'^customer/channel/$', ChannelIntegration.as_view(), name='channel-integration-request'),
    url(r'^customer/key/$', ChannelKey.as_view(), name='channel-key'),
    url(r'^customer/syncorder/$', ChannelSyncOrder.as_view(), name='channel-key'),
    # Reporting
    url(r'^customer/report/schedule/$', CustomerReportScheduleView.as_view(), name='get-customer-report-schedule'),
    url(r'^customer/report/schedule/add/$', CustomerReportScheduleView.as_view(), name='save-customer-report-schedule'),
    url(r'^customer/report/schedule/delete/(?P<rid>[0-9]+)$', CustomerReportScheduleView.as_view(), name='delete-customer-report-schedule'),
    url(r'^customer/report/schedule/update/$', CustomerReportScheduleView.as_view(), name='update-customer-report-schedule'),
    url(r'^customer/sendsms$', SendSms.as_view(), name='send-test-sms'),
    url(r'^customer/', include('blowhorn.customer.api_urls')),
    url(r'^sms/status$', SmsResponse.as_view(), name='sms-status'),
    url(r'^track/shipment/(?P<ref_number>[\w-]+)$', TrackParcelView.as_view(), name='track-shipment'),
    url(r'^customer/bank-accounts$', CustomerBankAccountsView.as_view(), name='customer-bank-list'),
    url(r'^customer/bank-accounts/(?P<pk>[0-9]+)$', CustomerBankAccountsView.as_view(), name='customer-bank-details'),
    url(r'^customer/change-password$', ChangePasswordView.as_view(), name='validate-customer-password'),

    url(r'^shipment/failed-orders$', CustomerFailedOrderListView.as_view(), name='failed-customers-orders'),
    url(r'^fleet/cod-payments$', FleetCodPaymentsListView.as_view(), name='fleet-cod-payments'),
    url(r'^fleet/cod-payments/summary$', FleetCodPaymentsSummaryStatusListView.as_view(), name='fleet-cod-payments-summary-status'),
    url(r'^fleet/cod-payments/export$', FleetCodPaymentsExportStatusListView.as_view(), name='fleet-cod-payments-export')
]
