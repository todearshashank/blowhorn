from rest_framework import permissions

from blowhorn.customer.mixins import CustomerMixin


class IsNonIndividualCustomer(permissions.BasePermission, CustomerMixin):
    """
    Permission to allow access for SME users only.
    """

    def has_permission(self, request, view):
        user = request.user
        if user and user.is_authenticated:
            customer = self.get_customer(user)
            return customer and customer.is_non_individual()

        return False

class IsCustomer(permissions.BasePermission, CustomerMixin):
    """
    Permission to allow access for Individuals and SME users only.
    """

    def has_permission(self, request, view):
        user = request.user
        return user and user.is_authenticated and (self.get_customer(user) != None)


class IsOrgAdmin(permissions.BasePermission, CustomerMixin):
    """
    Permission to restrict API access only to Organization Admin
    """

    def has_permission(self, request, view):
        user = request.user
        if user.is_customer():
            return True
        customer = self.get_customer(user)
        return customer and customer.is_admin(user)


class IsOrgOwner(permissions.BasePermission, CustomerMixin):
    """
    Permission to restrict API access only to Organization Owner
    """

    def has_permission(self, request, view):
        user = request.user
        if user.is_customer():
            return True
        customer = self.get_customer(user)
        return customer and customer.is_owner(user)


class IsOrgMember(permissions.BasePermission, CustomerMixin):
    """
    Permission to restrict API access only to Organization Owner
    """

    def has_permission(self, request, view):
        user = request.user
        if user.is_customer():
            return True
        return self.get_customer(user)


class ReadonlyAccess(permissions.BasePermission, CustomerMixin):
    """
    Readonly mode
    """

    def has_permission(self, request, view):
        user = request.user
        if user.is_customer():
            return True

        customer = self.get_customer(user)
        if customer and request.method in permissions.SAFE_METHODS:
            return True

        return False


class IsValidAPIKeyHolder(permissions.BasePermission, CustomerMixin):
    """
    Permission to allow access customer with api key
    """

    def has_permission(self, request, view):
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = self.get_customer_with_api_key(api_key)
            if customer:
                return True

        return False

class IsValidPartner(permissions.BasePermission, CustomerMixin):
    """
    Permission to allow access partner with api key
    """

    def has_permission(self, request, view):
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = self.get_partner_with_api_key(api_key)
            if customer:
                return True

        return False
