# System and Django
import decimal
import logging
import os
import zipfile
import csv

import pytz
import requests
from django.http import HttpResponse
from django.template import loader
import pandas as pd
import openpyxl
import json
import calendar
from Crypto.Cipher import AES
from django.conf import settings
from django.contrib.admin import SimpleListFilter
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.core.exceptions import ValidationError
from django.db.models import Value, CharField, Func, F, Count, TextField, Sum, \
    ExpressionWrapper, FloatField, Case, When, Q, DateField, IntegerField, \
    DurationField, Window, Avg, Max
from django.db.models.functions import Cast, Coalesce
from django.utils.safestring import mark_safe
from django.utils import timezone, dateformat
from datetime import datetime, timedelta, date
from itertools import groupby
from openpyxl import load_workbook
from openpyxl.styles import Font, PatternFill
from babel.numbers import format_currency

from phonenumber_field.phonenumber import PhoneNumber
from django.core.files.base import ContentFile
from collections import defaultdict

from blowhorn.utils.functions import utc_to_ist, minutes_to_hmstr
from blowhorn.contract.models import CONTRACT_STATUS_ACTIVE, Contract
from blowhorn.customer.constants import CUSTOMER_CREDIT_LIMIT_PERCENTAGE_RATIO, APPROVED, \
    SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED, CREATED, SENT_FOR_APPROVAL, \
    CUSTOMER_CONFIRMED, PARTIALLY_PAID, PAYMENT_DONE, CLOSED_WITH_CN, TAX_INVOICE, CREDIT_NOTE, DEBIT_NOTE, \
    COLLECTION_STATUS_CLOSED, COLLECTION_STATUS_PARTIALLY_CLOSED, COLLECTION_STATUS_OPEN, \
    CREDIT_BREACH_5_DAYS_PRIOR_BODY, CREDIT_BREACH_1_DAY_PRIOR_BODY, CREDIT_BREACH_DAY_BODY, \
    CREDIT_BREACH_1_DAY_AFTER_BODY, CREDIT_BREACH_5_DAYS_PRIOR_SUBJECT, CREDIT_BREACH_1_DAY_PRIOR_SUBJECT, \
    CREDIT_BREACH_DAY_SUBJECT, CREDIT_BREACH_1_DAY_AFTER_SUBJECT, DETAIL_PAGE, SUMMARY_DETAIL_PAGE, \
    UPLOADED_TO_GST_PORTAL, EMAIL_TEMPLATE_AGEING_MAPPING, TRANSACTION_STATUS_UNPAID,\
    TRANSACTION_STATUS_PARTIALLY_PAID, TRANSACTION_STATUS_CLOSED_WITH_CN, MAX_INVOICE_REMAINING_AMOUNT, \
    SEND_TO_CUSTOMER_AUTOMATION_MAIL_LIST, SEND_TO_CUSTOMER_AUTOMATION_CC_LIST
from blowhorn.company.models import Company, OfficeGSTInformation
from blowhorn.customer.models import CollectionsSummary, Customer, CustomerEmailLog, Partner
from blowhorn.oscar.core.loading import get_model
from .submodels.invoice_models import EInvoicing, EInvoicingEvent, CustomerInvoiceAdjustment, create_invoice_history
from rest_framework import status
from blowhorn.contract.constants import SERVICE_TAX_CATEGORIES_CODE_MAP, EXEMPT, GTA, BSS, BASE_PAYMENT_CYCLE_MONTH
from blowhorn.common.utils import generate_pdf
from blowhorn.utils.mail import Email
from blowhorn.utils.html_to_pdf import render_to_pdfstream

INVOICE_DATE_FILTER = '2019-04-01'

invoice_type = {
    'Tax Invoice': 'INV',
    'Credit Note': 'CRN',
    'Debit Note': 'DBN',
}

SellRate = get_model('contract', 'SellRate')
SellVehicleSlab = get_model('contract', 'SellVehicleSlab')
SellShipmentSlab = get_model('contract', 'SellShipmentSlab')
SellDimensionSlab = get_model('contract', 'SellDimensionSlab')
SellVASSlab = get_model('contract', 'SellVASSlab')
SellNTSlab = get_model('contract', 'SellNTSlab')
Contract = get_model('contract', 'Contract')
PaymentInterval = get_model('contract', 'PaymentInterval')
ContractTerm = get_model('contract', 'ContractTerm')
CustomerInvoice = get_model('customer', 'CustomerInvoice')
InvoicePayment = get_model('customer', 'InvoicePayment')
InvoiceHistory = get_model('customer', 'InvoiceHistory')
CustomerTaxInformation = get_model('customer', 'CustomerTaxInformation')
Trip = get_model('trip', 'Trip')
ResourceAllocation = get_model('contract', 'ResourceAllocation')
User = get_model('users', 'User')
Customer = get_model('customer', 'Customer')
CustomerBankDetails = get_model('customer', 'CustomerBankDetails')
SendInvoiceBatch = get_model('customer', 'SendInvoiceBatch')
MISDocument = get_model('customer', 'MISDocument')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

headers = {
    "Content-Type": "application/json"
}


def expected_margin(expected_margin):
    if expected_margin is None:
        return ''
    color = 'BLACK'

    if expected_margin <= 0:
        color = 'RED'

    elif 0 < expected_margin < 5:
        color = 'YELLOW'

    elif 5 <= expected_margin < 10:
        color = 'ORANGE'

    elif 10 <= expected_margin < 15:
        color = 'DARKORANGE'

    elif expected_margin > 15:
        color = 'GREEN'

    return mark_safe('<b style="color:{};">{}</b>'.format(
        color, expected_margin))


class MyfleetHelper(object):

    def get_trip_dict(self, trip):
        """
            param: instance of Trip
            returns: custom dict of trip
        """
        actual_start_time = None
        if trip.actual_start_time:
            actual_start_time = utc_to_ist(
                trip.actual_start_time).strftime('%d-%b-%Y %H:%M')

        actual_end_time = None
        if trip.actual_end_time:
            actual_end_time = utc_to_ist(
                trip.actual_end_time).strftime('%d-%b-%Y %H:%M')
        _dict = {
            'trip_number': trip.trip_number,
            'creation_date': utc_to_ist(trip.planned_start_time).strftime('%d-%b-%Y'),
            'driver_name': trip.driver.name,
            'vehicle_number': str(trip.vehicle),
            'start_time': actual_start_time,
            'end_time': actual_end_time,
            'estimated_drop_time': trip.estimated_drop_time,
            'status': trip.status,
            'total_distance': trip.total_distance or 0,
            'total_time': minutes_to_hmstr(trip.total_time),
        }
        return _dict

    def fetch_active_customer_contracts(self, customer):
        """ @params: customer
            @returns: All contracts mapped to given customer
        """
        Contract = get_model('contract', 'Contract')

        contracts = Contract.objects.filter(customer=customer,
                                            status=CONTRACT_STATUS_ACTIVE)
        return contracts

    def fetch_active_drivers_mapped_to_contracts(self, contract_ids):
        """ @params: List of contract ids
            @returns: A list of active drivers mapped to contracts
        """
        drivers = []
        resources = ResourceAllocation.objects.filter(
            contract_id__in=contract_ids)
        for r in resources:
            for d in r.drivers.all():
                if d.status == 'active':
                    drivers.append(d)
        return drivers

    def fetch_mapped_drivers_to_enterprise_customer(self, customer_id):
        """ @params: customer_id
            @returns: All drivers mapped to contracts of given customer
        """

        contracts = self.fetch_active_customer_contracts(customer_id)
        if contracts:
            contract_ids = [c.id for c in contracts]
            drivers = self.fetch_active_drivers_mapped_to_contracts(
                contract_ids)
            return drivers

        return []

    def is_business_user(self, customer):
        return Contract.objects.filter(customer=customer).exists()

    def get_cities_list(self):
        from blowhorn.address.models import City
        from blowhorn.address.serializer import CitySerializer

        cities_list = []
        city_queryset = City.objects.all()
        city_serializer = CitySerializer(city_queryset, many=True)
        features = city_serializer.data.get('features')
        for f in features:
            properties = f.get('properties')
            city_name = properties.get('name')
            cities_list.append(city_name)
        return cities_list

    def get_customer_tax_information(self, customer):
        """
        :param customer: instance of Customer
        :return: List of tax information about given customer
        """
        tax_info = []
        tax_qs = CustomerTaxInformation.objects.filter(customer=customer)
        for t_info in tax_qs:
            state = t_info.state_id or None
            invoice_address = t_info.invoice_address.search_text \
                if t_info.invoice_address else None
            tax_info.append({
                'gstin': t_info.gstin,
                'state': state,
                'invoice_address': invoice_address
            })
        return tax_info


class CustomerUtils(object):

    def get_customer_from_phone_number(self, phone_number):
        """
        :param phone_number: String 10-digit phone-number
        :return: instance of Customer
        """
        from blowhorn.users.models import User
        from blowhorn.customer.mixins import CustomerMixin

        if settings.ACTIVE_COUNTRY_CODE not in str(phone_number):
            phone_number = PhoneNumber(
                settings.ACTIVE_COUNTRY_CODE,
                phone_number
            )
        user = User.objects.filter(
            phone_number=phone_number,
            # is_mobile_verified=True,
            customer__isnull=False,
            # last_otp_sent_at__isnull=False
        ).order_by('-last_otp_sent_at').first()

        if not user:
            email = '%s@%s' % (
                phone_number, settings.DEFAULT_DOMAINS.get('customer'))
            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                logger.info('User not found for phone number: %s' %
                            phone_number)
                return None, None

        if not user.is_anonymous:
            return CustomerMixin().get_customer(user), user
        return None, None

    def get_supervisors_and_spos_list(self, spoc_and_supervisor):
        spoc_users = []
        supervisor_users = []

        for val in spoc_and_supervisor:
            if val.get('supervisors__email'):
                supervisor_users.append(val['supervisors__email'])
            if val.get('spocs__email'):
                spoc_users.append(val['spocs__email'])

        return list(sorted(set(spoc_users))), list(
            sorted(set(supervisor_users)))

    def get_unique_sorted_superusers(self):
        return list(sorted(set(User.objects.filter(is_active=True,
                                                   is_superuser=True).values_list('email', flat=True))))

    def get_supervisors_and_spocs(self, invoice=None):
        spoc_and_supervisor = Contract.objects.filter(
            Q(contractinvoicefragment__invoice=invoice) |
            Q(contractinvoicefragment__invoice=invoice.related_invoice,
              contractinvoicefragment__invoice__isnull=False
              )).values('supervisors__email', 'spocs__email')

        return spoc_and_supervisor

    def get_company_executives(self):
        return Company.objects.exclude(executives__email__isnull=True
                                       ).values_list('executives__email', flat=True)

    def get_staff_emails(self, invoice, show_spoc=False):
        if not invoice.is_blanket_invoice:
            if not show_spoc:
                return [], []
            spoc_and_supervisor = self.get_supervisors_and_spocs(invoice)

            if not spoc_and_supervisor:
                if invoice.customer:
                    contracts = invoice.customer.contract_set.filter(
                        city__state__in=[invoice.bh_source_state, invoice.invoice_state])
                    spoc_and_supervisor = contracts.values(
                        'supervisors__email', 'spocs__email')

            if not spoc_and_supervisor:
                supervisor_users = self.get_unique_sorted_superusers()
                spoc_users = supervisor_users
            else:
                spoc_users, supervisor_users = \
                    self.get_supervisors_and_spos_list(spoc_and_supervisor)
        else:
            supervisor_users = self.get_company_executives()
            spoc_users = supervisor_users

        return spoc_users, supervisor_users

    def is_default_invoice_type(self, invoice):
        return not invoice.is_blanket_invoice and not (
            invoice.invoice_type in [CREDIT_NOTE, DEBIT_NOTE] and
            invoice.related_invoice is None)

    def get_sales_representatives(self, invoice):
        from blowhorn.address.models import City
        if self.is_default_invoice_type(invoice):
            return City.objects.filter(
                state__in=[invoice.bh_source_state, invoice.invoice_state]
            ).values_list('sales_representatives__email', flat=True).exclude(
                sales_representatives__email=None).distinct()
        else:
            return City.objects.all().values_list(
                'sales_representatives__email', flat=True).exclude(
                sales_representatives__email=None).distinct()


def get_vehicle_class_availability(driver_availability_data):
    data = json.loads(json.dumps(driver_availability_data))
    available_vehicles = defaultdict(list)
    for available in data:
        vehicle_class = available['vehicle_class'][0].get('vehicle_class_name',
                                                          '')
        if vehicle_class:
            available.pop('vehicle_class')
            available_vehicles[vehicle_class].append(available)

    return available_vehicles


abbr_to_num = {name: num for num,
                             name in enumerate(calendar.month_abbr) if num}


def CustomMonthFilter(display_name, field):
    class DateFilter(SimpleListFilter):
        title = _(display_name)
        parameter_name = field

        def lookups(self, request, model_admin):
            cal = []
            invoices = CustomerInvoice.objects.extra(select={"year": "EXTRACT(YEAR  FROM " + field + ")",
                                                             "month": "EXTRACT(MONTH  FROM " + field + ")"}
                                                     ).distinct().values_list("year", "month").order_by("-year",
                                                                                                        "-month")

            for i in invoices:
                if all(i):
                    # i[1] is month and i[0] is year
                    cal.append((calendar.month_abbr[int(i[1])] + '-' + str(int(i[0])),
                                calendar.month_abbr[int(i[1])] + '-' + str(int(i[0]))))

            return cal

        def queryset(self, request, queryset):
            if self.value():
                month, year = self.value().split('-')
                if field == 'revenue_booked_on':
                    query = Q(revenue_booked_on__month=abbr_to_num.get(
                        month, None), revenue_booked_on__year=year)
                else:
                    query = Q(service_period_end__month=abbr_to_num.get(
                        month, None), service_period_end__year=year)

                return queryset.filter(query)

    return DateFilter


def get_revenue_book_on(booked_on):
    month, year = booked_on.split('-')
    date_ = str(year) + '-' + str(abbr_to_num.get(month)) + '-01'

    return date_


def CustomYesNoFilter(display_name, field):
    class CustomerFilter(SimpleListFilter):
        title = _(display_name)
        parameter_name = field

        def lookups(self, request, model_admin):
            choice = [True, False]
            return [(c, c) for c in choice]

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(invoice_type=TAX_INVOICE, is_credit_debit_note=self.value())
            return queryset

    return CustomerFilter


class InvoicePaidFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Paid')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'paid'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('True', _('True')),
            ('False', _('False')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == 'True':
            return queryset.filter(paid=True)
        elif self.value() == 'False':
            return queryset.filter(paid=False)
        else:
            return queryset


def customer_exceeding_credit_limit(customer):
    user_to_be_notified = []
    cust = {}
    cust_last_ledger = customer.customerledger_set.latest('record_number')

    if cust_last_ledger:
        balance = cust_last_ledger.balance
        credit_amount = customer.credit_amount
        cust['name'] = customer.name
        cust['legal_name'] = customer.legalname
        cust['credit_limit'] = customer.credit_amount

        if balance and balance > credit_amount * CUSTOMER_CREDIT_LIMIT_PERCENTAGE_RATIO:
            users = customer.contract_set.filter(
                        status = 'Active').values('city__managers__email',
                                                  'supervisors__email')

            for user in users:
                user_to_be_notified.append(
                      user.get('city__managers__email'))
                user_to_be_notified.append(user.get('supervisors__email'))

        user_to_be_notified = set(user_to_be_notified)
        cust['limit_percntg'] = int(
                        CUSTOMER_CREDIT_LIMIT_PERCENTAGE_RATIO * 100)
        cust['user_to_be_notified'] = list(user_to_be_notified)

    return cust


class AddBalanceFilter(SimpleListFilter):
    title = _('Money Added')
    parameter_name = 'Money Added'

    def lookups(self, request, model_admin):
        choice = ['Money Added', 'Paytm Transaction', 'RazorPay Transaction', '-']
        return [(c, c) for c in choice]

    def queryset(self, request, queryset):
        if self.value() == 'Money Added':
            return queryset.filter(Q(source__isnull=False)|Q(wallet_source__isnull=False))
        if self.value() == 'Paytm Transaction':
            return queryset.filter(wallet_source__isnull=False)
        if self.value() == 'RazorPay Transaction':
            return queryset.filter(source__isnull=False)

        return queryset


def _filter_by_ageing(invoices_list):
    if invoices_list.get('ageing_in_days') is not None:
        return True
    else:
        return False


def get_invoice_ledger_file_name(customer):
    now = datetime.now().strftime("%d_%m_%Y_%H_%M")
    filename = '%s_ledger_%s.xlsx' % (customer, now)
    return filename


def email_credit_breached_customers(customer_pk, ageing):
    customer_email = Customer.objects.filter(id=customer_pk).values_list('credit_email_recipients', flat=True).last()
    logger.info('Customer email recipients for credit breach mail are - %s' % customer_email)
    customer = Customer.objects.filter(id=customer_pk).first()
    email_template_obj = EMAIL_TEMPLATE_AGEING_MAPPING.get(str(ageing), None)
    email_subject = email_template_obj.get('subject', None) % settings.BRAND_NAME if email_template_obj else None
    email_template = email_template_obj.get('body', None) % settings.BRAND_NAME if email_template_obj else None
    last_email_sent_at = CustomerEmailLog.objects.filter(customer=customer).last()

    if not (customer.enterprise_credit_applicable or customer_email):
        return

    # email_recipients = __clean_recipients(customer_email)

    context = {
        "body": email_template,
        "brand_name": settings.BRAND_NAME,
        "invoice_link": '%s/dashboard/enterpriseinvoice/pending' % settings.HOST
    }

    email_template_name = 'emailtemplates_v2/credit_breach.html'
    html_content = loader.render_to_string(email_template_name, context)

    # if last_email_sent_at is None or (timezone.now() - last_email_sent_at.email_sent_at).days == 4:
    if email_template_obj is not None and customer_email:
        if settings.ENABLE_SLACK_NOTIFICATIONS:
            mail = EmailMessage(
                email_subject,
                html_content,
                # to=email_recipients,
                from_email=settings.DEFAULT_INVOICE_MAIL
            )
            filename = get_invoice_ledger_file_name(customer.name)
            document = open('/tmp/invoice_payments.xlsx', 'rb')
            _file = document.read()
            mail.attach(filename, _file)
            # mail.content_subtype = 'html'
            mail.send()
            email_log_obj = CustomerEmailLog.objects.create(customer=customer, ageing=ageing)
            file = ContentFile(_file)
            email_log_obj.file.save(filename, file)
            email_log_obj.save()
    os.remove('/tmp/invoice_payments.xlsx')


def credit_breached_customers():
    if settings.BRAND_NAME == 'Blowhorn':
        return

    qs = CollectionsSummary._default_manager.filter(invoice_date__gte=INVOICE_DATE_FILTER,
                                                    customer__enterprise_credit_applicable=True,
                                                    status__in=[APPROVED, UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER,
                                                                CUSTOMER_ACKNOWLEDGED,
                                                                CUSTOMER_CONFIRMED, PARTIALLY_PAID])
    qs = qs.annotate(
        customer_name=F('customer__name'),
        customer_type=F('customer__customer_category'),
        transaction_type=F('invoice_type'),
        invoice_identifier=F('invoice_number'),
        cn_against_invoice=F('related_invoice__invoice_number'),
        transaction_date=F('invoice_date'),
        service_start=F('service_period_start'),
        service_end=F('service_period_end'),
        source_state=F('bh_source_state__name'),
        total_recvd_or_receivable=F('total_amount'),
        allocated_bank_credit=-F('invoicepayment__actual_amount'),
        tds_allocation=-F('invoicepayment__tds'),
        cn_allocation=-F('invoicepayment__debit'),
        cum_bank_credit=Window(
            expression=Sum('invoicepayment__actual_amount'),
            partition_by=[F('invoice_number')]
        ),
        cum_tds=Window(
            expression=Sum('invoicepayment__tds'),
            partition_by=[F('invoice_number')]
        ),
        cum_cn=Window(
            expression=Sum('invoicepayment__debit'),
            partition_by=[F('invoice_number')]
        ),
        balance_amount=Case(
            When(transaction_type=TAX_INVOICE, then=ExpressionWrapper(
                Coalesce(F('total_recvd_or_receivable'), Value(0)) - Coalesce(F('cum_bank_credit'),
                                                                              Value(0)) \
                - Coalesce(F('cum_tds'), Value(0)) - Coalesce(F('cum_cn'), Value(0)),
                output_field=FloatField())),
            default=Value(None)
        ),
        transaction_status=Case(
            When(Q(transaction_type=TAX_INVOICE) & Q(balance_amount__lt=10),
                 then=Value(COLLECTION_STATUS_CLOSED)),
            When(Q(transaction_type=TAX_INVOICE) & Q(balance_amount__gt=10) & Q(
                allocated_bank_credit__gt=0),
                 then=Value(COLLECTION_STATUS_PARTIALLY_CLOSED)),
            When(Q(transaction_type=CREDIT_NOTE), then=Value(COLLECTION_STATUS_CLOSED)),
            default=Value(COLLECTION_STATUS_OPEN),
            output_field=CharField(),
        ),
        collection_id=F('invoicepayment__collection__id'),
        credit_days=F('customer__enterprise_credit_period'),
        due_by=Case(
            When(Q(transaction_type=TAX_INVOICE) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                 then=ExpressionWrapper(F('transaction_date') + F('credit_days'),
                                        output_field=DateField())),
            default=Value(None, output_field=DateField()),
            output_field=DateField()
        ),
        ageing_in_days=Case(
            When(Q(transaction_type=TAX_INVOICE) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                 then=ExpressionWrapper(
                     date.today() - F('due_by'), output_field=DurationField())),
            output_field=DurationField()
        ),
        ageing_bucket=Case(
            When(Q(ageing_in_days__lt=30), then=Value('Not Aged')),
            When(Q(ageing_in_days__gt=31) & Q(ageing_in_days__lt=60) & ~Q(
                transaction_status=COLLECTION_STATUS_CLOSED),
                 then=Value('31-60 days')),
            When(Q(ageing_in_days__gt=61) & Q(ageing_in_days__lt=90) & ~Q(
                transaction_status=COLLECTION_STATUS_CLOSED),
                 then=Value('61-90 days')),
            When(Q(ageing_in_days__gt=91) & Q(ageing_in_days__lt=180) & ~Q(
                transaction_status=COLLECTION_STATUS_CLOSED),
                 then=Value('91-180 days')),
            When(Q(ageing_in_days__gt=181) & Q(ageing_in_days__lt=360) & ~Q(
                transaction_status=COLLECTION_STATUS_CLOSED),
                 then=Value('181-360 days')),
            When(Q(ageing_in_days__gt=360) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                 then=Value('> 360 days')),
            output_field=CharField()
        ),
        sales_spoc=F('current_owner__email')
    ).values(
        'customer_id', 'invoice_number', 'total_amount', 'ageing_in_days', 'balance_amount',
        'service_start', 'service_end', 'transaction_status'
    )
    invoices_list = filter(_filter_by_ageing, list(qs))
    filtered_list = list(invoices_list)
    sorted_list = sorted(filtered_list, key=lambda x: x['customer_id'])

    for k, v in groupby(sorted_list, key=lambda x: x['customer_id']):
        ageing_list = list(v)
        max_ageing_invoice = max(ageing_list or 0, key=lambda x: x['ageing_in_days'] or 0)
        max_ageing_value = max_ageing_invoice.get('ageing_in_days')
        df = pd.DataFrame(ageing_list)
        # Order of the columns in excel file which will be sent to the customers
        cols = [3, 5, 4, 0, 7, 6, 1]
        df = df.rename(
            columns={'ageing_in_days': 'Ageing(In Days)', 'invoice_number': 'Invoice Number',
                     'total_amount': 'Total Amount', 'balance_amount': 'Balance Amount',
                     'service_start': 'Service Start', 'service_end': 'Service End',
                     'transaction_status': 'Transaction Status'})
        df = df[df.columns[cols]]
        workbook = openpyxl.Workbook()
        workbook.save('/tmp/invoice_payments.xlsx')
        df.to_excel('/tmp/invoice_payments.xlsx', index=False)

        # Add remaining payable amount at the bottom and styling it
        excel_file = load_workbook('/tmp/invoice_payments.xlsx')
        worksheet = excel_file.active
        total_balance = round(df['Balance Amount'].sum(), 2)
        if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            total_balance = format_currency(total_balance, 'INR', locale='en_IN').replace(u'\xa0',
                                                                                          u' ')
        cell = worksheet.cell(row=worksheet.max_row + 1, column=worksheet.max_column)
        cell.value = 'Remaining Payable: ' + str(total_balance)
        cell.font = Font(name='Arial', bold=True, color='000000')
        cell.fill = PatternFill(start_color='FF0000', fill_type='solid')

        # Loop to adjust cell width to fit the content
        for col in worksheet.columns:
            maxl = 0
            column = col[0].column
            for ce in col:
                try:
                    if len(str(ce.value)) > maxl:
                        maxl = len(ce.value)
                except:
                    pass
            cell_width = (maxl + 2) * 1.2
            worksheet.column_dimensions[column].width = cell_width
        excel_file.save('/tmp/invoice_payments.xlsx')
        email_credit_breached_customers(k, max_ageing_value)


def create_customer(email):
    user = User.objects.filter(email=email).first()
    with transaction.atomic():
        if not user:
            user = User.objects.create_user(email, '1234')

        customer = None
        if not user.is_staff:
            customer = Customer.objects.filter(user__email=email).first()
            if not customer:
                Customer.objects.create(user=user, name='Guest')
    return user, customer


def getfiles(request, files, file_name='tmp'):

    with zipfile.ZipFile(settings.TEMPORARY_FILE_DIR + '/' + file_name + '.zip', 'w') as myzip:
        for f in files:
            myzip.writestr(os.path.basename(f.name)
                           , f.read())

    if os.path.exists(settings.TEMPORARY_FILE_DIR + '/' + file_name + '.zip'):
        with open(settings.TEMPORARY_FILE_DIR + '/' + file_name + '.zip', 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + \
                                              os.path.basename(settings.TEMPORARY_FILE_DIR + '/' + file_name + '.zip')
            os.remove(settings.TEMPORARY_FILE_DIR + '/' + file_name + '.zip')
            return response


def get_invoice_tax_info(invoice):

    division = invoice.aggregation.division if hasattr(invoice, 'aggregation') else None
    _taxinfo = {
        "customer": invoice.customer
    }
    invoice_state = invoice.invoice_state
    if invoice_state:
        _taxinfo['state'] = invoice_state
    if division:
        _taxinfo['division'] = division

    return CustomerTaxInformation.objects.filter(**_taxinfo).first()


def generate_auth_token(gst_info):
    office = gst_info.office

    token_time = gst_info.token_expire_time
    if token_time:
        token_time = pytz.timezone(settings.ACT_TIME_ZONE).localize(
            datetime.strptime(token_time, "%Y-%m-%d %H:%M:%S"))

    if token_time and token_time >= timezone.now():
        return

    headers.update({
        "client_id": settings.E_INVOICING_CLIENT_ID,
        "client_secret": settings.E_INVOICING_CLIENT_SECRET,
        "IPAddress": settings.E_INVOICING_IPADDRESS,
    })

    request_data = {
        "action": "ACCESSTOKEN",
        "UserName": gst_info.gst_user_id,
        "Password": settings.E_INVOICING_PASSWORD,
        "Gstin": office.gst_identification_no

    }
    response = requests.post(url=settings.E_INVOICING_AUTH_URL, headers=headers, json=request_data)
    data = json.loads(response.content)
    if response.status_code == status.HTTP_200_OK and data['Message'] == 'SUCCESS':
        gst_info.token_expire_time = data['Data']['TokenExpiry']
        gst_info.save()


def construct_data(index, data_dict):

    prod_desc = data_dict.get('prod_desc')
    is_service = data_dict.get('is_service')
    hs_heading = data_dict.get('hs_heading')
    invoice_number = data_dict.get('invoice_number')
    amount = data_dict.get('amount')
    igst_percentage = data_dict.get('igst_percentage')
    sgst_percentage = data_dict.get('sgst_percentage')
    cgst_percentage = data_dict.get('cgst_percentage')
    igst_amount = data_dict.get('igst_amount')
    cgst_amount = data_dict.get('cgst_amount')
    sgst_amount = data_dict.get('sgst_amount')
    total_rcm_amount = data_dict.get('total_rcm_amount')

    return {
        "SiNo": str(index),
        "ProductDesc": prod_desc,
        "IsService": is_service,
        "HsnCode": hs_heading,
        "BarCode": invoice_number,
        "Quantity": 1.0,
        "FreeQuantity": 0.0,
        "Unit": "UNT",
        "UnitPrice": amount,
        "TotAmount": amount,
        "Discount": 0.0,
        "PreTaxableVal": 0.0,
        "OtherCharges": 0.0,
        "AssAmount": amount,
        "GstRate": igst_percentage + sgst_percentage + cgst_percentage,
        "IgstAmt": igst_amount,
        "CgstAmt": cgst_amount,
        "SgstAmt": sgst_amount,
        "CesRate": 0.0,
        "CessAmt": 0.0,
        "CesNonAdvalAmt": 0.0,
        "StateCesRate": 0.0,
        "StateCesAmt": 0.0,
        "TotItemVal": amount if total_rcm_amount else
        round(amount + ((igst_percentage * amount / 100) + (cgst_percentage * amount / 100)) +
              (sgst_percentage * amount / 100), 2),
        "OrderLineRef": str(index + 2),
        "OriginCountry": "12",
        "ProdSerialNo": invoice_number,
        "StateCesNonAdvlAmt": 0.0
    }


extra_charges = {
    0: 'Toll Charges',
    1: 'Parking Charges',
    2: 'Call Charges'
}


def generate_irn(invoice, gst_info):
    office = gst_info.office
    state = office.state
    company = office.company
    user = invoice.customer.user

    division = invoice.aggregation.division if hasattr(invoice, 'aggregation') else None

    customer_tax_info = get_invoice_tax_info(invoice)
    if not customer_tax_info:
        customer_tax_info = CustomerTaxInformation.objects.filter(customer=invoice.customer,
                                                                  division=division).first()
    tax_legalname = None
    if customer_tax_info:
        customer_address = customer_tax_info.invoice_address
        state_name = str(customer_tax_info.state.name) if customer_tax_info else customer_address.state
        state_code = str(customer_tax_info.state.gst_code) if customer_tax_info else ""
        cust_gstin = str(customer_tax_info.gstin)
        tax_legalname = customer_tax_info.legalname
    else:
        customer_address = invoice.customer.head_office_address
        state_name, state_code, cust_gstin = invoice.customer.head_office_address.state, '', ''

    headers.update({
        "client_id": settings.E_INVOICING_CLIENT_ID,
        "client_secret": settings.E_INVOICING_CLIENT_SECRET,
        "IPAddress": settings.E_INVOICING_IPADDRESS,
        "user_name": gst_info.gst_user_id,
        "Gstin": office.gst_identification_no
    })

    fragment = invoice.contract_invoices.first()
    hsn_code = None
    if not hsn_code and fragment and fragment.contract.hsn_code:
        hsn_code = invoice.hs_code.hsn_code \
            if invoice.hs_code else fragment.contract.hsn_code.hsn_code

    total_by_service_type = json.loads(invoice.total_by_service_type)
    is_tax_applicable = True
    service_desc = ''
    hs_heading = 0
    service_type = None
    amount = None
    for service_type, amount in total_by_service_type.items():
        service_desc = SERVICE_TAX_CATEGORIES_CODE_MAP.get(service_type, {}).get('name')

        if service_type == BSS:
            hs_heading = 998599
        elif service_type == GTA:
            hs_heading = 996511
        if service_desc == EXEMPT:
            service_desc = 'Service Charges'
            hs_heading = 996511

        if service_type == EXEMPT:
            is_tax_applicable = False

    taxable_amt = invoice.taxable_amount - invoice.taxable_adjustment

    adj_amt = CustomerInvoiceAdjustment.objects.filter(invoice=invoice, category__hsn_code__hsn_code=hsn_code,
                                                       category__is_tax_applicable=is_tax_applicable,
                                                       ).aggregate(Sum('total_amount')).get('total_amount__sum', 0) or 0

    taxable_amt = float(taxable_amt) + float(adj_amt) if not invoice.invoice_type == CREDIT_NOTE \
        else abs(float(taxable_amt) + float(adj_amt))

    item_details = []
    data_dict = {
        "prod_desc": service_desc,
        "is_service": "Y",
        "hs_heading": hs_heading,
        "invoice_number": invoice.invoice_number,
        "amount": round(float(taxable_amt), 2),
        "igst_percentage": round(float(invoice.igst_percentage), 2),
        "sgst_percentage": round(float(invoice.sgst_percentage), 2),
        "cgst_percentage": round(float(invoice.cgst_percentage), 2),
        "igst_amount": round(float(invoice.igst_percentage) * taxable_amt / 100, 2),
        "cgst_amount": round(float(invoice.cgst_percentage) * taxable_amt / 100, 2),
        "sgst_amount": round(float(invoice.sgst_percentage) * taxable_amt / 100, 2),
        "total_rcm_amount": round(float(invoice.total_rcm_amount), 2),
        }
    index = 1
    item_details.append(construct_data(index, data_dict))

    adjustment_record = CustomerInvoiceAdjustment.objects.filter(invoice=invoice).exclude(
        category__hsn_code__hsn_code=hsn_code, category__is_tax_applicable=is_tax_applicable).values(
        'category__hsn_code').annotate(total_amount=Sum('total_amount'), category_name=F('category__name'),
                                       is_taxable=F('category__is_tax_applicable'))

    non_taxable_adj = decimal.Decimal(0)
    for record in adjustment_record:
        if service_desc == EXEMPT:
            service_desc = 'Service Charges'
            hs_heading = 996511

        index += 1
        amount_ = float(record.get('total_amount')) if not invoice.invoice_type == CREDIT_NOTE \
            else abs(float(record.get('total_amount')))

        if not service_type and record.get('category_name') in [GTA, BSS]:
            service_type = record.get('category_name')

        cgst_amount, sgst_amount, igst_amount = 0, 0, 0
        igst_percentage, sgst_percentage, cgst_percentage = 0, 0, 0
        if not record.get('is_taxable'):
            non_taxable_adj += decimal.Decimal(amount_)
        if record.get('is_taxable') and service_type:
            if float(abs(invoice.taxable_amount)) > 750:
                cgst_amount = round(amount_ * float(invoice.cgst_percentage / 100), 2)
                sgst_amount = round(amount_ * float(invoice.sgst_percentage / 100), 2)
                igst_amount = round(amount_ * float(invoice.igst_percentage / 100), 2)
                igst_percentage = round(float(invoice.igst_percentage), 2)
                sgst_percentage = round(float(invoice.sgst_percentage), 2)
                cgst_percentage = round(float(invoice.cgst_percentage), 2)

        data_dict = {
            "prod_desc": record.get('category_name'),
            "is_service": "Y",# if not invoice.total_rcm_amount else "Y",
            "hs_heading": hs_heading,
            "invoice_number": invoice.invoice_number,
            "amount": round(float(amount_), 2),
            "igst_percentage": igst_percentage,
            "sgst_percentage": sgst_percentage,
            "cgst_percentage": cgst_percentage,
            "igst_amount": igst_amount,
            "cgst_amount": cgst_amount,
            "sgst_amount": sgst_amount,
            "total_rcm_amount": round(float(invoice.total_rcm_amount), 2),
        }
        item_details.append(construct_data(index, data_dict))

    for i in range(0, 3):
        # loop for toll , parking , call charges
        if i == 0:
            amount = round(float(invoice.total_toll_charges), 2)
        elif i == 1:
            amount = round(float(invoice.total_parking_charges), 2)
        elif i == 2:
            amount = round(float(invoice.total_call_charges), 2)

        if not amount:
            continue

        index += 1

        data_dict = {
            "prod_desc": extra_charges.get(i),
            "is_service": "Y",# if not invoice.total_rcm_amount else "Y",
            "hs_heading": hs_heading,
            "invoice_number": invoice.invoice_number,
            "amount": amount,
            "igst_percentage": 0,
            "sgst_percentage": 0,
            "cgst_percentage": 0,
            "igst_amount": 0,
            "cgst_amount": 0,
            "sgst_amount": 0,
            "total_rcm_amount": round(float(invoice.total_rcm_amount), 2),
        }
        item_details.append(construct_data(index, data_dict))

    taxable_val = invoice.taxable_amount + invoice.total_toll_charges + invoice.total_parking_charges +\
                  invoice.total_call_charges + non_taxable_adj
    if not is_tax_applicable:
        taxable_val += decimal.Decimal(adj_amt)

    if invoice.invoice_type == CREDIT_NOTE:
        taxable_val = abs(invoice.taxable_amount)

    request_data = {
        "ACTION": "INVOICE",
        "VERSION": "1.1",
        "BUSINESS_LINE_CODE": "deport-1",
        "Irn": None,
        "TPAPITRANDTLS": {
            "REVREVERSECHARGE": "Y" if invoice.total_rcm_amount else "N",
            "TYP": "B2B",
            "TAXPAYERTYPE": "GST",
            "EcomGstin": None,
            "IgstOnIntra": None
        },
        "TPAPIDOCDTLS": {
            "DOCNO": invoice.invoice_number,
            "DOCDATE": invoice.invoice_date.strftime('%d/%m/%Y'),
            "DOCTYP": invoice_type.get(invoice.invoice_type)
        },
        "TPAPISELLERDTLS": {
            "GSTINNO": gst_info.office.gst_identification_no,
            "LEGALNAME": company.name,
            "TRDNAME": company.name,
            "ADDRESS1": gst_info.address,
            "ADDRESS2": gst_info.city,
            "LOCATION": state.name,
            "PINCODE": gst_info.pin_code,
            "StateCode": state.gst_code,
            "MobileNo": gst_info.mobile_number.national_number,
            "EmailId": gst_info.email
        },
        "TPAPIREFDTLS": {
            "InvoiceRemarks": invoice.remarks[:99] if invoice.remarks else "GST Filing",
            "TpApiDocPerdDtls": {
                "INVOICESTDT": invoice.service_period_start.strftime('%d/%m/%Y'),
                "INVOICEENDDT": invoice.service_period_end.strftime('%d/%m/%Y')
            }
        },
        "TPAPIVALDTLS": {
            "TOTALTAXABLEVAL": taxable_val,
            "TOTALSGSTVAL": str(round(invoice.sgst_amount, 2)) if not invoice.invoice_type == CREDIT_NOTE
            else str(abs(round(invoice.sgst_amount, 2))),
            "TOTALCGSTVAL": str(round(invoice.cgst_amount, 2)) if not invoice.invoice_type == CREDIT_NOTE
            else str(abs(round(invoice.cgst_amount, 2))),
            "TOTALIGSTVAL": str(round(invoice.igst_amount, 2)) if not invoice.invoice_type == CREDIT_NOTE
            else str(abs(round(invoice.igst_amount, 2))),
            "TOTALCESVAL": "0",
            "TOTALSTATECESVAL": "0",
            "TOTINVOICEVAL": str(invoice.total_amount) if not invoice.invoice_type == CREDIT_NOTE else
            str(abs(invoice.total_amount)),
            "ROUNDOFAMT": "0",
            "TOTALINVVALUEFC": "0",
            "Discount": "0",
            "OthCharge": "0"
        },
        "TPAPIBUYERDTLS": {
            "GSTINNO": cust_gstin,
            "LEGALNAME": tax_legalname or invoice.customer.legalname,
            "TRDNAME": tax_legalname or invoice.customer.legalname,
            "PLACEOFSUPPLY": state_code,
            "ADDRESS1": str(customer_address.line1)[:99] if customer_address else '',
            "ADDRESS2": str(customer_address.line3 or customer_address.line4 or customer_address.line1)[:99]
            if customer_address else '',
            "LOCATION": state_name,
            "PINCODE": customer_address.postcode if customer_address else None,
            "StateCode": state_code,
            "MobileNo": str(user.phone_number.national_number),
            "EmailId": str(user.email)
        },
        "TpApiBchDtls": {
            "BatchName": str(invoice.invoice_number),
            "ExpiryDate": invoice.service_period_end.strftime('%d/%m/%Y'),
            "WarrantyDate": invoice.service_period_end.strftime('%d/%m/%Y')
        },
        "TpApiItemList": item_details
    }
    print(":data --------------", request_data)
    response = requests.post(url=settings.E_INVOICING_IRN_URL, headers=headers, json=request_data)
    resp_data = json.loads(response.content)
    print("oo------------------", resp_data)
    if resp_data['MessageId'] and resp_data['Message'] == 'SUCCESS':
        data = resp_data['Data']
        signed_qr = data['Base64QRCode']
        e_invoice_data = {
            "irn": data['Irn'],
            "ack_no": data['AckNo'],
            "ack_date": data['AckDt'],
            "status": data['Status'],
            "response": json.dumps(resp_data, indent=4, sort_keys=True, default=str)
        }
        e_invoice = EInvoicing.objects.filter(invoice=invoice).first()
        if not e_invoice:
            e_invoice_data.update({"invoice_id": invoice.id})
            EInvoicing.objects.create(**e_invoice_data)
        else:
            EInvoicing.objects.filter(invoice=invoice).update(**e_invoice_data)

        get_einvoicing_pdf(invoice, gst_info)
        invoice.is_e_invoice_filed = True
        invoice.save()
        return True, signed_qr
    return False, resp_data

def get_einvoicing_pdf(invoice, gst_info):
    office = gst_info.office
    headers.update({
        "client_id": settings.E_INVOICING_CLIENT_ID,
        "client_secret": settings.E_INVOICING_CLIENT_SECRET,
        "IPAddress": settings.E_INVOICING_IPADDRESS,
        "user_name": gst_info.gst_user_id,
        "Gstin": office.gst_identification_no
    })

    url = settings.E_INVOICING_GET_PDF % (invoice.invoice_date.strftime('%d/%m/%Y'),
                                          invoice_type.get(invoice.invoice_type),
                                          invoice.invoice_number)

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        content = ContentFile(response.content)
        file_name = str(invoice) + '_gst.pdf'
        e_invoice = EInvoicing.objects.get(invoice=invoice)
        data = {
            "e_invoice_id": e_invoice.id,
            "status": e_invoice.status,
            "response": e_invoice.response,

        }
        e_invoice.file.save(file_name, content, save=True)
        event = EInvoicingEvent.objects.create(**data)
        event.file.save(file_name, content, save=True)


def cancel_irn(invoice, gst_info):
    office = gst_info.office
    headers.update({
        "client_id": settings.E_INVOICING_CLIENT_ID,
        "client_secret": settings.E_INVOICING_CLIENT_SECRET,
        "IPAddress": settings.E_INVOICING_IPADDRESS,
        "user_name": gst_info.gst_user_id,
        "Gstin": office.gst_identification_no
    })
    einvoice = EInvoicing.objects.filter(invoice=invoice).first()
    if einvoice:
        request_data = {
            "action": "CANCEL",
            "IRNNo": einvoice.irn,
            "CancelReason": "1",
            "CancelRemarks": "Wrong Entry"
        }
        response = requests.post(url=settings.E_INVOICING_IRN_URL, headers=headers, json=request_data)
        resp_data = json.loads(response.content)
        if resp_data['MessageId'] and resp_data['Message'] == 'SUCCESS':
            e_invoice_event_data = {
                "status": 'CANCELLED',
                "e_invoice_id": einvoice.id,
                "response": json.dumps(resp_data, indent=4, sort_keys=True, default=str)
            }
            EInvoicingEvent.objects.create(**e_invoice_event_data)
            einvoice.status = 'CANCL'
            einvoice.save()
            return True, None
    return False, resp_data


def get_annexure_data(invoice):
    from blowhorn.customer.helpers.invoice_helper import InvoiceHelper
    config = invoice.invoice_configs
    if config and config.extra_pages in [DETAIL_PAGE, SUMMARY_DETAIL_PAGE]:
        detailed_data = []

        if invoice.calculation_level == "ORDER":
            contract_invoice_objects = invoice.contract_invoices.all()[:1]
        else:
            contract_invoice_objects = invoice.contract_invoices.all()
        for contract_invoice in contract_invoice_objects:
            an_invoice_entity = contract_invoice.entities.first()
            if an_invoice_entity:

                sell_rate = an_invoice_entity.sell_rate
                contract_data = {
                    'invoice': contract_invoice,
                    'days_counted': contract_invoice.days_counted if sell_rate.base_pay_interval == BASE_PAYMENT_CYCLE_MONTH else 0,
                    'base_pay_per_month': sell_rate.base_pay if sell_rate.base_pay else 0,
                    'details': []
                }

                attributes = contract_invoice.entities.values_list('attribute', flat=True).distinct()
                # Ordering to maintain the base pay followed by vehicle slabs etc.
                attributes = [attribute for attribute in InvoiceHelper().get_attribute_ordered()  if attribute in attributes]

                delivered_records = None
                for attribute in attributes:
                    attributes_mapping = {"days": ["days", "Extra Hours", "Extra Kilometers"]}
                    if attribute == 'Delivered':
                        for contract_invoice in invoice.contract_invoices.all():
                            record = contract_invoice.entities.filter(
                                attribute__in=attributes_mapping.get(attribute, [attribute])).order_by('date', 'city')
                            delivered_records = delivered_records | record if delivered_records else record
                        contract_data['details'].append(
                            {
                                'attribute_name': attribute,
                                'records': delivered_records,
                                'aggregates': delivered_records.distinct().order_by('-date', 'city').aggregate(
                                    Sum('value'), Sum('extra_value'), Sum('amount'),
                                    Sum('slab_fixed_amount_kilometers'),
                                    Sum('slab_fixed_amount_hours'))
                            }
                        )
                    else:
                        records = contract_invoice.entities.filter(
                            attribute__in=attributes_mapping.get(attribute, [attribute])).order_by('date')
                        contract_data['details'].append(
                            {
                                'attribute_name': attribute,
                                'records': records,
                                'aggregates': records.order_by('-date').aggregate(
                                    Sum('value'), Sum('extra_value'), Sum('amount'),
                                    Sum('slab_fixed_amount_kilometers'),
                                    Sum('slab_fixed_amount_hours'))
                            }
                        )

                detailed_data.append(contract_data)

        return detailed_data


def get_invoice_data(invoice):
    data = json.loads(invoice.context)
    data['invoice']['details'] = get_annexure_data(invoice)
    return data


def generate_invoice_pdf(invoice, data, status):
    invoice.status = status
    invoice_template = "pdf/invoices/%s/invoice.html" % (settings.WEBSITE_BUILD)
    header = {"template": "pdf/invoices/b2b_v2/header.html", "context": data}
    footer = {"template": "pdf/invoices/b2b_v2/footer.html", "context": data}
    _bytes = generate_pdf(invoice_template, data, header=header, footer=footer)

    if _bytes:
        file_name = str(invoice.invoice_number) + ".pdf" if invoice.invoice_number else str(
            invoice.id) + ".pdf"
        content = ContentFile(_bytes)
        invoice.file.save(file_name, content, save=True)
        invoice.save()


def upload_to_gst_portal_method(invoice):

    tz = pytz.timezone(settings.ACT_TIME_ZONE)
    current_datetime = timezone.now().astimezone(tz)

    if invoice.invoice_date != current_datetime.date():
        return False, 'Invoice date is not matching with current date.'

    gst_info = OfficeGSTInformation.objects.filter(office__state=invoice.service_state
                                                   ).select_related('office', 'office__company').first()
    if not gst_info:
        gst_info = OfficeGSTInformation.objects.select_related('office', 'office__company').first()

    generate_auth_token(gst_info)
    try:
        is_sucess, resp = generate_irn(invoice, gst_info)
    except BaseException as e:
        return False, str(e)
    if not is_sucess:
        return is_sucess, resp


    # if not signed_qr:
    e_invoice = EInvoicing.objects.filter(invoice=invoice).first()
    e_invoice_resp = json.loads(e_invoice.response)
    signed_qr = e_invoice_resp['Data']['Base64QRCode']
    data = get_invoice_data(invoice)
    data.update({"einvoicing": {
        'ack_no': e_invoice.ack_no,
        'ack_date': e_invoice.ack_date,
        'reverse_charge': "No" if invoice.service_tax_category == BSS else "Yes",
        "irn": ' '.join([e_invoice.irn[i:i + 30] for i in range(0, len(e_invoice.irn), 30)]),
        "qr_code": "data:image/png;base64," + signed_qr
    }})

    generate_invoice_pdf(invoice, data, UPLOADED_TO_GST_PORTAL)
    create_invoice_history(invoice.status, UPLOADED_TO_GST_PORTAL, invoice, None, None)

    return is_sucess, resp


def validate_customer_gst_info(customer_gstin):

    gst_info = OfficeGSTInformation.objects.select_related('office').first()
    generate_auth_token(gst_info)
    office = gst_info.office

    headers.update({
        "client_id": settings.E_INVOICING_CLIENT_ID,
        "client_secret": settings.E_INVOICING_CLIENT_SECRET,
        "IPAddress": settings.E_INVOICING_IPADDRESS,
        "user_name": gst_info.gst_user_id,
        "Gstin": office.gst_identification_no
    })

    request_data = {
        "action": "GSTINDETAILS",
        "gstin": customer_gstin
    }

    response = requests.post(url=settings.E_INVOICING_IRN_URL, headers=headers, json=request_data)
    resp_data = json.loads(response.content)
    if resp_data['MessageId'] and resp_data['Message'] == 'SUCCESS':
        return True
    return False


def is_valid_gstin(invoice):

    customer_tax_info = get_invoice_tax_info(invoice)
    if not customer_tax_info:
        return False, None, invoice.customer.is_gstin_mandatory
    if customer_tax_info.is_gstin_validated or validate_customer_gst_info(customer_tax_info.gstin):
        return True, True, invoice.customer.is_gstin_mandatory

    return True, False, invoice.customer.is_gstin_mandatory


def remove_watermark(invoice):
    from blowhorn.customer.helpers.invoice_helper import InvoiceHelper

    if invoice.context:
        data = json.loads(invoice.context)
        data['einvoicing'] = {
            "hidewatermark": "yes"
        }
        data['invoice']['details'] = get_annexure_data(invoice)

        invoice_template = "pdf/invoices/%s/invoice.html" % settings.WEBSITE_BUILD
        header = {"template": "pdf/invoices/b2b_v2/header.html", "context": data}
        footer = {"template": "pdf/invoices/b2b_v2/footer.html", "context": data}
        _bytes = generate_pdf(invoice_template, data, header=header, footer=footer)

    else:
        total_by_service_type = json.loads(invoice.total_by_service_type)
        fragment = invoice.contract_invoices.first()
        entity = fragment.entities.first()
        sell_rate = entity.sell_rate if entity else None

        _bytes = InvoiceHelper().generate_manual_uploaded_pdf(
            invoice, invoice.bh_source_state, invoice.invoice_state, total_by_service_type=total_by_service_type,
            fragment=fragment, sell_rate=sell_rate)

    return _bytes


class CreditBreach(object):

    def __clean_recipients(self, emails):
        """
        To remove duplicates and None values
        :param emails: email-ids (list/str)
        :return: unique list of email ids
        """
        if not emails:
            return []

        recipients = []
        if isinstance(emails, str):
            recipients = emails.split(',')
        else:
            recipients = emails

        recipients = list(set([r.lower() for r in recipients if r]))
        return recipients

    def get_customer_ids(self, *args, **kwargs):
        customer = kwargs.pop('customer', None)
        customer_id = kwargs.pop('customer_id', None)
        customers_list = kwargs.pop('customers_list', None)

        if customer:
            customer_ids = list(
                Customer.objects.filter(name=customer, enterprise_credit_applicable=True).values_list('id', flat=True))
        elif customer_id:
            customer_ids = list(
                Customer.objects.filter(id=customer_id, enterprise_credit_applicable=True).values_list('id', flat=True))
        elif customers_list:
            customer_ids = list(Customer.objects.filter(id__in=customers_list,
                                                        enterprise_credit_applicable=True).values_list('id', flat=True))
        else:
            customer_ids = list(Customer.objects.filter(enterprise_credit_applicable=True).values_list('id', flat=True))

        return customer_ids

    def _get_collections_data(self, invoice_numbers):
        resp = []
        payment_qs = InvoicePayment.objects.filter(invoice__invoice_number__in=invoice_numbers).distinct().order_by(
            'paid_on')
        for payment in payment_qs.iterator():
            if payment.collection:
                resp.append({
                    'transaction_date': payment.paid_on,
                    'description': 'Payment',
                    'transaction_number': payment.collection.description,
                    'debit_amount': 0,
                    'credit_amount': float(payment.actual_amount),
                    'dr_cr': 'Cr'
                })
                if payment.tds:
                    resp.append({
                        'transaction_date': payment.paid_on,
                        'description': 'TDS',
                        'transaction_number': 'TDS against the invoice %s' % payment.invoice.invoice_number,
                        'debit_amount': 0,
                        'credit_amount': float(payment.tds),
                        'dr_cr': 'Cr'
                    })
        return resp

    def get_customer_ledger(self, customer_id):
        qs = CollectionsSummary._default_manager.filter(customer_id=customer_id,
                                                        invoice_date__gte=INVOICE_DATE_FILTER,
                                                        customer__enterprise_credit_applicable=True,
                                                        status__in=[APPROVED, UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER,
                                                                    CUSTOMER_ACKNOWLEDGED,
                                                                    CUSTOMER_CONFIRMED, PARTIALLY_PAID, CLOSED_WITH_CN])
        qs = qs.annotate(
            customer_name=F('customer__name'),
            customer_type=F('customer__customer_category'),
            transaction_type=F('invoice_type'),
            invoice_identifier=F('invoice_number'),
            cn_against_invoice=F('related_invoice__invoice_number'),
            transaction_date=F('invoice_date'),
            service_start=F('service_period_start'),
            service_end=F('service_period_end'),
            source_state=F('bh_source_state__name'),
            total_recvd_or_receivable=F('total_amount'),
            allocated_bank_credit=-F('invoicepayment__actual_amount'),
            tds_allocation=-F('invoicepayment__tds'),
            cn_allocation=-F('invoicepayment__debit'),
            cum_bank_credit=Window(
                expression=Sum('invoicepayment__actual_amount'),
                partition_by=[F('invoice_number')]
            ),
            cum_tds=Window(
                expression=Sum('invoicepayment__tds'),
                partition_by=[F('invoice_number')]
            ),
            cum_cn=Window(
                expression=Max('invoicepayment__debit'),
                partition_by=[F('invoice_number')]
            ),
            balance_amount=Case(
                When(transaction_type=TAX_INVOICE, then=ExpressionWrapper(
                    Coalesce(F('total_recvd_or_receivable'), Value(0)) - Coalesce(F('cum_bank_credit'),
                                                                                  Value(0)) \
                    - Coalesce(F('cum_tds'), Value(0)) - Coalesce(F('cum_cn'), Value(0)),
                    output_field=FloatField())),
                default=Value(None)
            ),
            cn_diff_amount=ExpressionWrapper(
                Coalesce(F('total_recvd_or_receivable'), Value(0)) - Coalesce(F('cum_cn'), Value(0)),
                output_field=FloatField()
            ),
            inv_status=Case(
                When(Q(transaction_type=TAX_INVOICE) & Q(cn_diff_amount__lte=10) & Q(total_amount__gt=0),
                     then=Value(TRANSACTION_STATUS_CLOSED_WITH_CN)),
                When(Q(status=PARTIALLY_PAID), then=Value(TRANSACTION_STATUS_PARTIALLY_PAID)),
                When(Q(status=CLOSED_WITH_CN), then=Value(CLOSED_WITH_CN)),
                default=Value(TRANSACTION_STATUS_UNPAID),
                output_field=CharField(),
            ),
            transaction_status=Case(
                When(Q(transaction_type=TAX_INVOICE) & Q(balance_amount__lt=10), then=Value(COLLECTION_STATUS_CLOSED)),
                When(Q(transaction_type=TAX_INVOICE) & Q(balance_amount__gt=10) & Q(allocated_bank_credit__gt=0),
                     then=Value(COLLECTION_STATUS_PARTIALLY_CLOSED)),
                When(Q(transaction_type=CREDIT_NOTE), then=Value(COLLECTION_STATUS_CLOSED)),
                default=Value(COLLECTION_STATUS_OPEN),
                output_field=CharField(),
            ),
            collection_id=F('invoicepayment__collection__id'),
            credit_days=F('customer__enterprise_credit_period'),
            due_by=Case(
                When(Q(transaction_type=TAX_INVOICE) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                     then=ExpressionWrapper(F('transaction_date') + F('credit_days'),
                                            output_field=DateField())),
                default=Value(None, output_field=DateField()),
                output_field=DateField()
            ),
            ageing_in_days=Case(
                When(Q(transaction_type=TAX_INVOICE) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                     then=ExpressionWrapper(
                         date.today() - F('due_by'), output_field=DurationField())),
                output_field=DurationField()
            ),
            ageing_bucket=Case(
                When(Q(ageing_in_days__lt=30), then=Value('Not Aged')),
                When(Q(ageing_in_days__gt=31) & Q(ageing_in_days__lt=60) & ~Q(
                    transaction_status=COLLECTION_STATUS_CLOSED),
                     then=Value('31-60 days')),
                When(Q(ageing_in_days__gt=61) & Q(ageing_in_days__lt=90) & ~Q(
                    transaction_status=COLLECTION_STATUS_CLOSED),
                     then=Value('61-90 days')),
                When(Q(ageing_in_days__gt=91) & Q(ageing_in_days__lt=180) & ~Q(
                    transaction_status=COLLECTION_STATUS_CLOSED),
                     then=Value('91-180 days')),
                When(Q(ageing_in_days__gt=181) & Q(ageing_in_days__lt=360) & ~Q(
                    transaction_status=COLLECTION_STATUS_CLOSED),
                     then=Value('181-360 days')),
                When(Q(ageing_in_days__gt=360) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                     then=Value('> 360 days')),
                output_field=CharField()
            ),
            sales_spoc=F('current_owner__email')
        ).values(
            'customer_name', 'invoice_number', 'inv_status', 'invoice_type', 'invoice_date', 'service_start',
            'service_end', 'source_state', 'total_amount', 'cum_bank_credit', 'cum_tds', 'cum_cn', 'balance_amount',
            'ageing_in_days'
        ).distinct()
        invoices_list = filter(_filter_by_ageing, list(qs))
        filtered_list = list(invoices_list)
        sorted_list = sorted(filtered_list, key=lambda x: x['customer_name'])

        for k, v in groupby(sorted_list, key=lambda x: x['customer_name']):
            ageing_list = list(v)
            max_ageing_invoice = max(ageing_list or 0, key=lambda x: x['ageing_in_days'] or 0)
            max_ageing_value = max_ageing_invoice.get('ageing_in_days')
            df = pd.DataFrame(ageing_list)
            # Order of the columns in excel file which will be sent to the customers
            df = df.rename(
                columns={'invoice_number': 'Invoice Number', 'inv_status': 'Status',
                         'invoice_type': 'Invoice Type', 'invoice_date': 'Invoice Date',
                         'service_start': 'Service Start', 'service_end': 'Service End',
                         'source_state': 'Source State', 'total_amount': 'Total Payable',
                         'cum_bank_credit': 'Paid Amount', 'cum_tds': 'TDS Amount', 'cum_cn': 'Credit Note',
                         'balance_amount': 'Receivable Amount'
                         })
            df = df.drop('ageing_in_days', 1)
            df = df.drop('customer_name', 1)
            df.drop_duplicates(subset=['Invoice Number', 'Invoice Date'], keep='first')

            # Order of columns to be shown in the excel
            cols = [0, 1, 2, 4, 5, 6, 3, 7, 8, 9, 10, 11]
            df = df[df.columns[cols]]

            df.loc[df['Status'] == CLOSED_WITH_CN, 'Credit Note'] = df['Total Payable']
            df.loc[df['Status'] == CLOSED_WITH_CN, 'Receivable Amount'] = 0

            workbook = openpyxl.Workbook()
            workbook.save('/tmp/invoice_payments.xlsx')
            df.to_excel('/tmp/invoice_payments.xlsx', index=False)

            # Add remaining payable amount at the bottom and styling it
            excel_file = load_workbook('/tmp/invoice_payments.xlsx')
            worksheet = excel_file.active
            total_balance = round(df['Receivable Amount'].sum(), 2)
            if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
                total_balance = format_currency(total_balance, 'INR', locale='en_IN').replace(u'\xa0',
                                                                                              u' ')
            cell = worksheet.cell(row=worksheet.max_row + 1, column=worksheet.max_column)
            cell.value = 'Remaining Payable: ' + str(total_balance)
            cell.font = Font(name='Arial', bold=True, color='000000')
            cell.fill = PatternFill(start_color='FF0000', fill_type='solid')

            # Loop to adjust cell width to fit the content
            for col in worksheet.columns:
                maxl = 0
                column = col[0].column
                for ce in col:
                    try:
                        if len(str(ce.value)) > maxl:
                            maxl = len(ce.value)
                    except:
                        pass
                cell_width = (maxl + 2) * 1.2
                worksheet.column_dimensions[column].width = cell_width
            excel_file.save('/tmp/invoice_payments.xlsx')

    def _get_invoice_data(self, customer_id):
        resp = []
        invoice_numbers = []
        five_day_prior_invoices = []
        one_day_after_invoices = []

        invoice_qs = CollectionsSummary._default_manager.filter(customer_id=customer_id,
                                                                invoice_date__gte=INVOICE_DATE_FILTER,
                                                                status__in=[APPROVED, UPLOADED_TO_GST_PORTAL,
                                                                            SENT_TO_CUSTOMER,
                                                                            CUSTOMER_ACKNOWLEDGED,
                                                                            CUSTOMER_CONFIRMED, PARTIALLY_PAID,
                                                                            CLOSED_WITH_CN]).values(
            'invoice_type', 'invoice_date', 'invoice_number', 'customer__enterprise_credit_period'
        ).order_by('invoice_date')

        for data in invoice_qs:
            invoice_number = data.get('invoice_number')
            if data.get('invoice_type') == TAX_INVOICE:
                due_by = data.get('invoice_date') + timedelta(days=data.get('customer__enterprise_credit_period'))
                delta = date.today() - due_by
                if delta.days == -5:
                    five_day_prior_invoices.append(invoice_number)
                elif delta.days == 1:
                    one_day_after_invoices.append(invoice_number)
        return one_day_after_invoices, five_day_prior_invoices

    def create_and_mail_customer_ledger(self, *args, **kwargs):
        customer_ids = self.get_customer_ids(*args, **kwargs)

        logger.info('Customer IDs for enterprise credit mails - %s', customer_ids)

        for i in customer_ids:
            one_day_after_invoices, five_day_prior_invoices = self._get_invoice_data(i)
            customer = Customer.objects.filter(id=i).first()

            # collections_data = self._get_collections_data(invoice_numbers)

            if one_day_after_invoices:
                email_template_obj = EMAIL_TEMPLATE_AGEING_MAPPING.get('1', None)
                self.get_customer_ledger(i)
                self._send_credit_breach_mail(customer, email_template_obj, 1, one_day_after_invoices)

            if five_day_prior_invoices:
                email_template_obj = EMAIL_TEMPLATE_AGEING_MAPPING.get('-5', None)
                self.get_customer_ledger(i)
                self._send_credit_breach_mail(customer, email_template_obj, -5, five_day_prior_invoices)
            try:
                os.remove('/tmp/invoice_payments.xlsx')
            except FileNotFoundError as e:
                logger.info('No ledger created for customer - %s' % customer)
        return

    def _send_credit_breach_mail(self, customer, email_template_obj, ageing, invoice_numbers):
        email_subject = email_template_obj.get('subject', None)
        email_subject = email_subject.format(brand_name=settings.BRAND_NAME) if email_subject else None
        email_template = email_template_obj.get('body', None)
        invoice_numbers = ', '.join(invoice_numbers)
        email_template = email_template.format(invoice_numbers=invoice_numbers) if email_template else None

        customer_email = customer.credit_email_recipients
        logger.info('Customer email recipients for credit breach mail are - %s' % customer_email)

        if not (customer.enterprise_credit_applicable or customer_email):
            return

        email_recipients = self.__clean_recipients(customer_email)

        context = {
            "body": email_template,
            "brand_name": settings.BRAND_NAME,
            "invoice_link": '%s/dashboard/enterpriseinvoice/pending' % settings.HOST
        }

        email_template_name = 'emailtemplates_v2/credit_breach.html'
        html_content = loader.render_to_string(email_template_name, context)

        if html_content and email_recipients:
            filename = get_invoice_ledger_file_name(customer.name)
            file_path = '/tmp/invoice_payments.xlsx'
            try:
                document = open('/tmp/invoice_payments.xlsx', 'rb')
            except FileNotFoundError as e:
                logger.info('No invoices list for the customer - %s' % customer)
                return
            _file = document.read()
            Email().send_mail_with_html_content(email_recipients, email_subject, html_content, filename, _file,
                                                file_path)

            # if settings.ENABLE_SLACK_NOTIFICATIONS:
            #     mail = EmailMessage(
            #         email_subject,
            #         html_content,
            #         to=email_recipients,
            #         from_email=settings.DEFAULT_INVOICE_MAIL
            #     )
            #
            #     mail.attach(filename, _file)
            #     mail.content_subtype = 'html'
            #     mail.send()

            email_log_obj = CustomerEmailLog.objects.create(customer=customer, ageing=ageing)
            file = ContentFile(_file)
            email_log_obj.file.save(filename, file)
            email_log_obj.save()
        return


def encrypt_account_number(account_number):

    obj = AES.new(settings.AES_KEY_1.encode('utf8'), AES.MODE_CBC, settings.AES_KEY_2.encode('utf8'))
    diff = 16 - len(account_number) if len(account_number) <= 16 else 32 - len(account_number)
    account_number = account_number.ljust(diff + len(account_number), '*')

    return obj.encrypt(account_number.encode('utf-8'))


def decrypt_account_number(account_number):

    obj2 = AES.new(settings.AES_KEY_1.encode('utf-8'), AES.MODE_CBC, settings.AES_KEY_2.encode('utf-8'))
    account_number = bytes(memoryview(account_number))
    account_number = obj2.decrypt(account_number)
    account_number = account_number.decode()

    return account_number.replace('*', '')


def get_account_number(pk):
    bank_account_details = CustomerBankDetails.objects.filter(id=pk).first()
    acc_number = bank_account_details.bank_account_number
    account_number = decrypt_account_number(acc_number)
    return 'XXXX%s' % account_number[-4:]


def send_bank_details_alert(customer, account_number, action):

    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    subject = 'Blowhorn (Security Alert!) - Bank Account Details Updated'
    email_template_name = 'emailtemplates_v2/customer_bank_details.html'
    context = {
        "customer_name": customer.user.name,
        "account_number": account_number,
        "action": action,
        "time_stamp": dateformat.format((timezone.now() + timedelta(hours=5, minutes=30)), 'd-m-Y H:i:s'),
        "user": customer.user.email,
        "blowhorn_concern_mail": settings.BLOWHORN_CONCERN_MAIL,
        "brand_name": settings.BRAND_NAME,
    }
    html_content = loader.render_to_string(email_template_name, context)
    mail = EmailMultiAlternatives(
        subject, html_content, to=[customer.user.email],
        from_email=settings.DEFAULT_FROM_EMAIL)
    mail.attach_alternative(html_content, "text/html")
    mail.send()


def send_password_alert(is_valid_password, user):
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    subject = 'Blowhorn (Security Alert!) - Password change intimation'
    email_template_name = 'emailtemplates_v2/password_change_mail.html'
    context = {
        "customer_name": user.name,
        "action": 'been updated successfully' if is_valid_password else 'failed to update',
        "time_stamp": dateformat.format((timezone.now() + timedelta(hours=5, minutes=30)), 'd-m-Y H:i:s'),
        "blowhorn_concern_mail": settings.BLOWHORN_CONCERN_MAIL,
        "brand_name": settings.BRAND_NAME,
    }
    html_content = loader.render_to_string(email_template_name, context)
    mail = EmailMultiAlternatives(
        subject, html_content, to=[user.email],
        from_email=settings.DEFAULT_FROM_EMAIL)
    mail.attach_alternative(html_content, "text/html")
    mail.send()


def close_invoices_with_credit_notes():
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    invoice_list = CustomerInvoice.objects.filter(invoice_type=CREDIT_NOTE,
                                                  status__in=[APPROVED, UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER,
                                                              CUSTOMER_ACKNOWLEDGED,
                                                              CUSTOMER_CONFIRMED, PARTIALLY_PAID,
                                                              PAYMENT_DONE]).select_related(
        'related_invoice').values_list('related_invoice__invoice_number', flat=True).distinct()
    paid_invoices = []
    closed_with_cn_invoices = []
    invoice_history_list = []

    for invoice in CustomerInvoice.objects.filter(invoice_number__in=invoice_list,
                                                  status__in=[APPROVED, UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER,
                                                              CUSTOMER_ACKNOWLEDGED,
                                                              CUSTOMER_CONFIRMED, PARTIALLY_PAID]).iterator():
        total_payable = invoice.total_amount or 0
        cum_credit_amount = CustomerInvoice.objects.filter(
            invoice_type=CREDIT_NOTE, related_invoice=invoice,
            status__in=[APPROVED, UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED,
                        CUSTOMER_CONFIRMED, PARTIALLY_PAID, PAYMENT_DONE]).aggregate(
            invoice_cn_amount=Sum('total_amount')).get('invoice_cn_amount', 0) or 0
        cum_bank_credit = InvoicePayment.objects.filter(invoice=invoice).aggregate(
            cum_bank_credit=Sum('actual_amount')).get('cum_bank_credit', 0) or 0
        cum_tds = InvoicePayment.objects.filter(invoice=invoice).aggregate(cum_tds=Sum('tds')).get('cum_tds', 0) or 0

        # If the invoices are fully closed with Credit Note, No Invoice Payment will be created. So we have to consider
        # the credit notes from APPROVED status to check if the invoices are fully closed with Credit Note
        # (Total Payable - Credit Note Amount <= 10) and change the status to CLOSED WITH CN
        invoice_cn_amount = CustomerInvoice.objects.filter(
            invoice_type=CREDIT_NOTE, related_invoice=invoice,
            status__in=[APPROVED, UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER,
                        CUSTOMER_ACKNOWLEDGED,
                        CUSTOMER_CONFIRMED, PARTIALLY_PAID,
                        PAYMENT_DONE]).aggregate(invoice_cn_amount=Sum('total_amount')).get(
            'invoice_cn_amount', 0) or 0


        # Mark Partially Paid invoices as Payment Done if closed by Credit Note
        # (Total Payable - Cum. Bank Credit - Cum. Credit note amount - Cum. TDS  <= 10)
        # cum_credit_amount will be negative since it is a credit note
        if float(total_payable) - (float(cum_bank_credit) - float(cum_credit_amount) + float(
            cum_tds)) <= MAX_INVOICE_REMAINING_AMOUNT and invoice.status == PARTIALLY_PAID:
            paid_invoices.append(invoice.id)
            invoice_history_list.append(InvoiceHistory(
                old_status=invoice.status,
                new_status=PAYMENT_DONE,
                invoice_id=invoice.id,
                created_by=User.objects.get(email=settings.CRON_JOB_DEFAULT_USER),
                action_owner=invoice.current_owner,
                remarks='Marked as PAYMENT DONE as the invoice is closed by credit note'
            ))

        # For the invoices which are fully closed with Credit Note
        # Total Payable - Cum. Credit amount <= 10
        # invoice_cn_amount will be negative
        if round((float(total_payable) + float(invoice_cn_amount)), 2) <= MAX_INVOICE_REMAINING_AMOUNT:
            closed_with_cn_invoices.append(invoice.id)
            invoice_history_list.append(InvoiceHistory(
                old_status=invoice.status,
                new_status=CLOSED_WITH_CN,
                invoice_id=invoice.id,
                created_by=User.objects.get(email=settings.CRON_JOB_DEFAULT_USER),
                action_owner=invoice.current_owner,
                remarks='Marked as CLOSED WITH CREDIT NOTE as the invoice is fully closed by credit note'
            ))

    if paid_invoices:
        CustomerInvoice.objects.filter(id__in=paid_invoices).update(status=PAYMENT_DONE)
    if closed_with_cn_invoices:
        CustomerInvoice.objects.filter(id__in=closed_with_cn_invoices).update(status=CLOSED_WITH_CN)
    if invoice_history_list:
        InvoiceHistory.objects.bulk_create(invoice_history_list)

def invoice_automation_excel(automation_failed_invoices):
    field_mappings = {'invoice_number': 'Invoice Number',
                     'remarks': 'Remarks',
                      'customer': 'Customer'}
    csv_data = 'Invoice Number, Remarks, Customer\n'
    for row in automation_failed_invoices:
        csv_data += ','.join(str(row.get(field, None)) for field in field_mappings.keys()) + '\n'

    return csv_data

def get_invoice_email_list(invoice):

    cities = []
    contacts = []
    contact_email = {'to_list': [],
                     'cc_list': []}
    customer = invoice.customer
    division = invoice.aggregation.division if hasattr(invoice, 'aggregation') else None

    contracts_fragment = invoice.contract_invoices.all()
    for fragment in contracts_fragment:
        if fragment.contract:
            cities.append(fragment.contract.city)
    cities += list(invoice.invoice_state.cities.all()) if not cities else []

    if division:
        contacts = customer.customercontact_set.filter(
                    Q(division__isnull=True) | Q(division=division)
                ).values('email', 'division__name')

    if not contacts:
        contacts = customer.customercontact_set.filter(
            Q(city__isnull=True) | Q(city__in=cities)
        ).values('email', 'city__name')

    if contacts:
        contact_email['to_list'] = ['%s' % c.get('email') for c in contacts if c]
        contact_email['cc_list'] = list(
            Contract.objects.filter(customer=customer, city__in=cities, supervisors__isnull=False).distinct().values_list(
                'supervisors__email', flat=True))

    return contact_email


def send_customer_invoice(invoice, due_date, invoice_email_list):
    history = create_invoice_history(invoice.status, SENT_TO_CUSTOMER, invoice,
                                     invoice.current_owner, due_date)
    if hasattr(invoice, 'document'):
        invoice.document.invoice_history = history
        invoice.document.save()
        mis_data = {
            'invoice': invoice,
            'mis_doc': invoice.document.supporting_doc
        }
        MISDocument.objects.create(**mis_data)
    invoice.due_date = due_date
    invoice.assigned_date = date.today()
    invoice.status = SENT_TO_CUSTOMER
    invoice.save()
    Email().send_invoice(invoice, invoice_email_list.get('to_list'), invoice.service_period_start,
                         invoice.service_period_end,
                         cc=invoice_email_list.get('cc_list'))

def send_invoices_to_customer():

    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    desc_batch = "Invoices Sent to Customers %s" % (timezone.now().date().strftime('%d-%m-%Y'))

    uploaded_to_gst_invoices = CustomerInvoice.objects.filter(status=UPLOADED_TO_GST_PORTAL)

    non_gstin_invoices = CustomerInvoice.objects.filter(status=APPROVED, customer__is_gstin_mandatory=False)

    invoices_to_send = uploaded_to_gst_invoices | non_gstin_invoices

    invoices_count = invoices_to_send.count()

    subject = 'BLOWHORN - %s Reports' % desc_batch

    sent_successfully = []
    failed_to_send = []
    cc_list = SEND_TO_CUSTOMER_AUTOMATION_CC_LIST

    batch = SendInvoiceBatch.objects.create(description=desc_batch, number_of_entries=invoices_count)

    due_date = date.today() + timedelta(days=10)

    for invoice in invoices_to_send:

        try:
            invoice_email_list = get_invoice_email_list(invoice)

            if not invoice_email_list:
                failed_to_send.append({"invoice_number": invoice.invoice_number,
                                       "remarks": 'No Customer contact',
                                       "customer": invoice.customer.name})
            if invoice_email_list.get('to_list'):
                send_customer_invoice(invoice, due_date, invoice_email_list)
                sent_successfully.append({"invoice_number": invoice.invoice_number,
                                           "remarks": 'SUCCESS'})
                cc_list += invoice_email_list.get('cc_list')
            else:
                failed_to_send.append({"invoice_number": invoice.invoice_number,
                                       "remarks": 'No Email id list',
                                       "customer": invoice.customer.name})

        except BaseException as e:
            failed_to_send.append({"invoice_number": invoice.invoice_number,
                                   "remarks": e,
                                   "customer": invoice.customer.name})


    message = sent_successfully + failed_to_send

    batch.sent_to_customers = len(sent_successfully)
    batch.failed_to_send = len(failed_to_send)
    batch.message = message
    batch.end_time = timezone.now()
    batch.save()

    context = {
        "total": len(message),
        "sent_successfully": len(sent_successfully),
        "failed_to_send": len(failed_to_send),
        "brand_name": settings.BRAND_NAME
    }

    email_template_name = 'emailtemplates_v2/automated_send_customerinvoice.html'
    html_content = loader.render_to_string(email_template_name, context)

    mail = EmailMultiAlternatives(
        subject,
        html_content,
        to=SEND_TO_CUSTOMER_AUTOMATION_MAIL_LIST,
        cc=cc_list,
        from_email=settings.DEFAULT_FROM_EMAIL)
    mail.attach_alternative(html_content, "text/html")

    if failed_to_send:
        filename = "%s_Reports.csv" % desc_batch
        _file = invoice_automation_excel(failed_to_send)
        _file = _file.encode("utf-8")
        mail.attach(filename, _file, 'text/csv')
        content = ContentFile(_file)
        batch.file.save(filename, content, save=True)

    mail.send()
