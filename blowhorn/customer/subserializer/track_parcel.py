import logging
from rest_framework import serializers

from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.helper import CommonHelper

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


DISPLAY_ADDRESS_COMPONENTS = ['sublocality_level_1', 'administrative_area_level_1']


def get_message_template(status, current_status=StatusPipeline.ORDER_NEW):
        """
        Returns message template for the given status
        :param status: str
        """
        template = next(
            (x for x in StatusPipeline.TRACKING_MESSAGES if status in x.get('statuses')), {}
        )
        messages = template.get('messages', {})
        if current_status == status:
            return messages.get('present', '')

        return messages.get('past', '')

class TrackParcelEventSerializer(serializers.Serializer):
    driver = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    time = serializers.DateTimeField(read_only=True)
    remarks = serializers.DateTimeField(read_only=True)
    location = serializers.SerializerMethodField(read_only=True)
    address = serializers.SerializerMethodField(read_only=True)
    message_template = serializers.SerializerMethodField(read_only=True)
    hub = serializers.SerializerMethodField(read_only=True)

    def get_location(self, obj):
        current_location = obj.current_location
        if current_location:
            return {
                'lat': current_location.y,
                'lng': current_location.x,
            }
        return {}

    def get_address(self, obj):
        address = obj.address
        if address:
            return address.line3 or address.line1

        address = obj.current_address
        if address:
            return CommonHelper().extract_from_google_address_components(
                address,
                extract_components=DISPLAY_ADDRESS_COMPONENTS
            )

        return ''

    def get_hub(self, obj):
        hub = obj.hub and obj.hub.name
        if not hub and obj.status in [
            StatusPipeline.DRIVER_ASSIGNED,
            StatusPipeline.ORDER_MOVING_TO_HUB,
            StatusPipeline.REACHED_AT_HUB,
            StatusPipeline.ORDER_MOVING_TO_HUB_RTO,
            StatusPipeline.REACHED_AT_HUB_RTO
        ]:
            hub = obj.remarks

        return hub

    def get_driver(self, obj):
        return obj.driver and obj.driver.name

    def get_message_template(self, obj):
        """
        Returns message template for the given status
        :param order_status: str
        """
        return get_message_template(obj.status, current_status=obj.order.status)

    def get_status(self, obj):
        return StatusPipeline.PARCEL_TRACKING_STATUSES.get(obj.status, StatusPipeline.ORDER_NEW)


class TrackParcelSerializer(serializers.Serializer):
    awb_number = serializers.CharField(source='number', read_only=True)
    customer = serializers.CharField(source='customer.name', read_only=True)
    end_customer = serializers.CharField(source='shipping_address.first_name')
    pickup_address = serializers.SerializerMethodField()
    shipping_address = serializers.SerializerMethodField()
    expected_delivery_time = serializers.SerializerMethodField()
    latest_event = serializers.SerializerMethodField()
    main_message_template = serializers.SerializerMethodField()
    events = serializers.SerializerMethodField()
    show_map = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField(read_only=True)
    status_pipeline = serializers.SerializerMethodField(read_only=True)

    def get_event_for_current_status(self, order_status, events):
        sorted_events = sorted(
            [x for x in events if x.status == order_status],
            key=lambda x: x.created_date
        )
        event = sorted_events[-1] if len(sorted_events) else None
        event_date, event_remarks, hub, status = None, None, None, None
        if event:
            event_date = event.time.isoformat()
            event_remarks = event.remarks
            hub = event.hub and event.hub.name
            if not hub and status in [
                StatusPipeline.DRIVER_ASSIGNED,
                StatusPipeline.ORDER_MOVING_TO_HUB,
                StatusPipeline.REACHED_AT_HUB,
                StatusPipeline.ORDER_MOVING_TO_HUB_RTO,
                StatusPipeline.REACHED_AT_HUB_RTO
            ]:
                hub = event_remarks
            status = event.status

        return {
            'remarks': event_remarks,
            'date': event_date,
            'hub': hub,
            'status': status,
        }

    def get_short_address(self, address):
        """
        Returns short address
        :param address: instance of ShippingAddress
        """
        return address.line3 or address.line1

    def display_via_map(self, order_status):
        return order_status not in StatusPipeline.TRACKING_WITHOUT_MAP +\
            [StatusPipeline.COMPLETED, StatusPipeline.ORDER_DELIVERED]

    def get_pickup_address(self, obj):
        return self.get_short_address(obj.pickup_address)

    def get_shipping_address(self, obj):
        return self.get_short_address(obj.shipping_address)

    def get_expected_delivery_time(self, obj):
        if obj.status not in StatusPipeline.SHIPMENT_ORDER_END_STATUSES:
            return {
                'start': obj.exp_delivery_start_time and obj.exp_delivery_start_time.isoformat(),
                'end': obj.expected_delivery_time and obj.expected_delivery_time.isoformat()
            }
        return {}

    def get_latest_event(self, obj):
        return self.get_event_for_current_status(obj.status, obj.events)

    def get_main_message_template(self, obj):
        return get_message_template(obj.status, current_status=obj.status)

    def get_show_map(self, obj):
        return self.display_via_map(obj.status)

    def get_events(self, obj):
        return TrackParcelEventSerializer(obj.events, many=True).data

    def get_status(self, obj):
        return StatusPipeline.PARCEL_TRACKING_STATUSES.get(obj.status, StatusPipeline.ORDER_NEW)

    def get_status_pipeline(self, obj):
        current_status = StatusPipeline.PARCEL_TRACKING_STATUSES.get(obj.status)
        in_end_status = current_status in StatusPipeline.PARCEL_TRACKING_END_STATUSES
        master_statii = StatusPipeline.PARCEL_TRACKING_PIPELINE \
            + StatusPipeline.PARCEL_TRACKING_END_STATUSES \
            + [StatusPipeline.PARCEL_TRACKING_STATUSES[StatusPipeline.UNABLE_TO_DELIVER]]
        processed = []
        children = []
        for previous, item, nxt in CommonHelper.prev_next_in_iterator(obj.events):
            final_status = StatusPipeline.PARCEL_TRACKING_STATUSES.get(item.status)
            _dict = {
                'label': final_status,
                'completed': True,
                'message_template': get_message_template(item.status, current_status=obj.status),
                'time': item.time.isoformat(),
            }
            children.append(final_status)
            if nxt and StatusPipeline.PARCEL_TRACKING_STATUSES.get(nxt.status) in master_statii:
                _dict['children'] = children
                children = []

            if final_status in master_statii:
                processed.append(_dict)

        if not in_end_status:
            processed_statii = [x.get('label') for x in processed][-1]
            try:
                master_statii_end = StatusPipeline.PARCEL_TRACKING_PIPELINE[StatusPipeline.PARCEL_TRACKING_PIPELINE.index(processed_statii):]
            except ValueError:
                master_statii_end = [StatusPipeline.PARCEL_TRACKING_STATUSES[StatusPipeline.UNABLE_TO_DELIVER]]

            master_statii_end = master_statii_end + [StatusPipeline.PARCEL_TRACKING_STATUSES[StatusPipeline.ORDER_DELIVERED]]
            pending = [_status for _status in master_statii_end if _status not in processed_statii]
            for _status in pending:
                processed.append({
                    'label': _status,
                    'completed': False,
                })

        return processed
