from rest_framework import serializers

from blowhorn.order.models import OrderContainer, OrderContainerHistory


class CustomerContainerSerializer(serializers.ModelSerializer):
    container_type = serializers.SerializerMethodField(read_only=True)
    container_num = serializers.SerializerMethodField(read_only=True)
    order_number = serializers.SerializerMethodField(read_only=True)
    trip_number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = OrderContainer
        fields = ('container_num', 'container_type', 'order_number',
                  'trip_number')

    def get_container_type(self, obj):
        return '%s' % obj.container.container_type

    def get_container_num(self, obj):
        return obj.container.number

    def get_order_number(self, obj):
        return obj.order.number

    def get_trip_number(self, obj):
        return obj.trip.trip_number


class CustomerContainerHistorySerializer(serializers.ModelSerializer):
    container_type = serializers.SerializerMethodField(read_only=True)
    container_num = serializers.SerializerMethodField(read_only=True)
    order_number = serializers.SerializerMethodField(read_only=True)
    trip_number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = OrderContainerHistory
        fields = ('container_num', 'container_type', 'order_number',
                  'trip_number')

    def get_container_type(self, obj):
        return obj.container_type

    def get_container_num(self, obj):
        return obj.container_number

    def get_order_number(self, obj):
        return obj.order.number

    def get_trip_number(self, obj):
        return obj.trip.trip_number
