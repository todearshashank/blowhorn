import logging
from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers, status

from blowhorn.address.models import City, Hub
from blowhorn.customer.models import AccountUserGroup, \
    UserModule, UserPermission, PermissionLevel, \
    PermissionLevelStore, System, AccountUser, CustomerDivision
from blowhorn.utils.functions import log_db_queries
from blowhorn.wms.models import WhSite, Client

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class UserPermissionSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(UserPermissionSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = UserPermission


class AccountUserGroupListSerializer(serializers.ModelSerializer):

    permissions = serializers.SerializerMethodField()

    permission_levels = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(AccountUserGroupListSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        self.permissions_fields = self.context.get('permissions_fields', [])
        self.fetch_level_instances = self.context.get('fetch_level_instances', False) or False
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = AccountUserGroup

    def get_permissions(self, obj):
        return UserPermissionSerializer(
            obj.permissions.all(),
            many=True,
            context={
                'fields': self.permissions_fields
            }
        ).data

    @log_db_queries
    def get_permission_levels(self, obj):
        permission_levels = obj.permission_levels.all()
        _json = [{
            'id': p.id,
            'module_id': p.module_id,
            'permission_level_id': p.permission_level_id,
            'value': list(eval(p.permission_level.model_name).objects.filter(pk__in=p.value).values('name', 'id'))
                if self.fetch_level_instances else p.value,
            'permission_level': {
                'id': p.permission_level_id,
                'name': p.permission_level.name,
                'system': p.permission_level.system_id
            } if self.fetch_level_instances else {}
        } for p in permission_levels]
        return _json

class AccountUserGroupSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(AccountUserGroupSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        self.permissions_fields = self.context.get('permissions_fields', [])
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    def validate(self, attrs):
        name = attrs.get('name', None)
        if not name:
            raise serializers.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=_('Name is mandatory to create a group')
            )

        group_qs = AccountUserGroup.objects.filter(
            name__iexact=name,
            organization=self.context.get('customer')
        )
        if self.instance:
            group_qs = group_qs.exclude(pk=self.instance.pk)

        if group_qs.exists():
            raise serializers.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=_('Group with same name already exists')
            )

        return attrs

    def create(self, validated_data):
        permissions = validated_data.pop('permissions', [])
        permission_levels = validated_data.pop('permission_levels', [])
        try:
            with transaction.atomic():
                group = AccountUserGroup.objects.create(**validated_data)
                for permission in permissions:
                    group.permissions.add(permission)

                perm_levels = [PermissionLevelStore(**_dict) for _dict in permission_levels]
                permission_levels = PermissionLevelStore.objects.bulk_create(perm_levels)
                for level in permission_levels:
                    group.permission_levels.add(level)
                group.save()
                return group

        except BaseException as be:
            logger.warning('Failed to create group. %s' % be)
            print('be-->', be)
            raise serializers.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=_('Failed to create group')
            )

    def update(self, instance, validated_data):
        permissions = validated_data.pop('permissions', [])
        permission_levels = validated_data.pop('permission_levels', [])
        try:
            self.__delete_permissions(instance)
        except BaseException as be:
            logger.warning('Failed to update group. %s' % be)
            raise serializers.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=_('Failed to update permissions for the group')
            )

        try:
            with transaction.atomic():
                for level in permission_levels:
                    instance.permission_levels.add(level)

                instance.name = validated_data.get('name', instance.name)
                for permission in permissions:
                    instance.permissions.add(permission)

                instance.save()
                return instance

        except BaseException as be:
            logger.warning('Failed to update group. %s' % be)
            raise serializers.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=_('Failed to update group')
            )

    def __delete_permissions(self, instance):
        permission_level_qs = instance.permission_levels.all()
        for g in permission_level_qs:
            instance.permission_levels.remove(g.pk)

        permissions_qs = instance.permissions.all()
        for g in permissions_qs:
            instance.permissions.remove(g.pk)

    class Meta:
        model = AccountUserGroup


class UserModuleSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(UserModuleSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = UserModule


class PermissionLevelSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(PermissionLevelSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = PermissionLevel


class PermissionLevelStoreSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(PermissionLevelStoreSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = PermissionLevelStore

class SystemSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(SystemSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = System


class AccountUserSerializer(serializers.ModelSerializer):

    groups = serializers.SerializerMethodField()
    user_permissions = serializers.SerializerMethodField()
    permission_levels = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(AccountUserSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        self.group_fields = self.context.get('group_fields', [])
        self.permissions_fields = self.context.get('permissions_fields', [])
        self.permission_level_store_fields = self.context.get('permission_level_store_fields', [])
        self.permission_level_fields = self.context.get('permission_level_fields', [])
        self.fetch_permission_level = self.context.get('fetch_permission_level', True) or True
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = AccountUser

    def get_groups(self, obj):
        return AccountUserGroupListSerializer(
            obj.groups.all(),
            many=True,
            context={
                'fields': self.group_fields,
                'permissions_fields': self.permissions_fields,
                'fetch_permission_level': self.fetch_permission_level
            }
        ).data

    def get_user_permissions(self, obj):
        return UserPermissionSerializer(
            obj.user_permissions.all(),
            many=True,
            context={
                'fields': self.permissions_fields
            }
        ).data

    @log_db_queries
    def get_permission_levels(self, obj):
        if not self.fetch_permission_level:
            return []

        permission_levels = obj.permission_levels.all()
        _json = [{
            'id': p.id,
            'module_id': p.module_id,
            'permission_level_id': p.permission_level_id,
            'value': list(eval(p.permission_level.model_name).objects.filter(pk__in=p.value).values('name', 'id')),
            'permission_level': {
                'id': p.permission_level_id,
                'name': p.permission_level.name,
                'system': p.permission_level.system_id
            }
        } for p in permission_levels]
        return _json
