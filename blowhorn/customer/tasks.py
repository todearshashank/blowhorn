"""Customer Related celery tasks to be added here."""
import requests
import traceback
import datetime
import calendar
import logging
import random
import json
import os
from datetime import date, timedelta

from blowhorn.apps.integrations.v1.payment.cashfree.views import request_async_transfer, add_benificiary
from blowhorn.contract.constants import CONTRACT_TYPE_SHIPMENT, CONTRACT_STATUS_ACTIVE
from blowhorn.oscar.core.loading import get_model

from django.conf import settings
from django.db import IntegrityError, transaction
from django.utils import timezone
from django.db.models import Count, Sum, F
from django.db.models.functions import Greatest
from django.core.files.base import ContentFile
from django.core.mail.message import EmailMultiAlternatives
from django.template import loader

from celery import Celery
from celery.schedules import crontab

from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.common.decorators import redis_batch_lock
from blowhorn.common.notification import Notification
from blowhorn.common.redis_helper import RedisHelper
from blowhorn.contract.utils import get_payment_interval
from blowhorn.users.models import User
from blowhorn.customer.models import Customer, CashFreeBeneficiary
from blowhorn.customer.models import InvoiceRequest, generate_invoice
from blowhorn.customer.helpers.invoice_helper import InvoiceHelper, \
    mark_invoices_as_expired, invoices_revenue_booked_month
from blowhorn.payment.services.wallet_transaction import TransactionNotification
from blowhorn.customer.constants import PENDING, SUCCESS, FAILURE, \
    INVOICE_CONFIG_ACTIVE, PAYMENT_URL_BULK_WEB, APPROVED, \
    UPLOADED_TO_GST_PORTAL_AUTOMATION_MAIL_LIST, UPLOADED_TO_GST_PORTAL_AUTOMATION_CC_LIST, CREDIT_NOTE,\
    UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED, CUSTOMER_CONFIRMED, PARTIALLY_PAID, PAYMENT_DONE
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, \
    OPENING_CREDIT_LIMIT_DATE, CustomerInvoice, InvoiceUploadBatch
from blowhorn.customer.helpers.event_ingestion import GPSEventIngestionService
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.utils import upload_to_gst_portal_method, CreditBreach, close_invoices_with_credit_notes, \
    send_invoices_to_customer, invoice_automation_excel
from blowhorn.order.const import PAYMENT_STATUS_PARTIALLY_PAID, \
    PAYMENT_STATUS_UNPAID, PAYMENT_MODE_CASHFREE, PAYMENT_MODE_ONLINE, PAYMENT_STATUS_PAID
from blowhorn.users.submodels.notification import NotificationMessageTemplate
from config.settings import status_pipelines as StatusPipeline
from blowhorn.common import sms_templates
from blowhorn.utils.mail import Email
from pytz import timezone as localtime
from blowhorn.customer.helpers.dashboard_firebase_update import \
    DashboardRealtimeUpdate
from blowhorn.customer.constants import INVOICE_DEACTIVATION_PERIOD, \
    NUMBER_OF_DAYS_ELAPSED_FOR_MARKING_INVOICE_AS_EXPIRED, CREATED
from blowhorn.customer.helpers.partner_invoice_helper import PartnerInvoiceHelper
from blowhorn.report.services.scheduled_reports import ReportService
from fcm_django.models import FCMDevice
from blowhorn import celery_app
from blowhorn.utils.html_to_pdf import render_to_pdfstream
from blowhorn.shopify.services.fulfillment import ShopifyFulfillmentService
from blowhorn.common.firebase_operations import FirebaseService

Order = get_model('order', 'Order')
DATE_FORMAT = '%d %b, %Y'
app = Celery()

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def process_export_request(self, customer_id, parms, email):
    """
    Process the export request and send mail to customer
    """
    ReportService().export_batch_request(customer_id, parms, email)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def process_customer_data_delete_request(self, performed_by_id):
    """
    Soft delete the customer data
    """
    try:
        with transaction.atomic():
            CustomerMixin().soft_delete_customer_data(performed_by_id)
    except BaseException as be:
        logger.info('Failed to update customer data %s' % be)


@celery_app.task
@redis_batch_lock(period=10800, expire_on_completion=True)
def clear_dashboard_data_from_firebase():
    """
        Daily job at night 3AM to delete all shipment order updates
        from firebase
    """
    logger.info('Adding task to delete shipment orders from firebase')
    if settings.DEBUG:
        return True

    FirebaseService().delete_dashboard_updates()


@celery_app.task
@redis_batch_lock(period=3600, expire_on_completion=True)
def generate_customer_reports():
    from blowhorn.report.services.scheduled_reports import ReportService
    logger.info("generating customer reports")
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return
    ReportService().generate_subscribed_reports()

@celery_app.task
@redis_batch_lock(period=3600, expire_on_completion=True)
def mail_customer_reports():
    """
    Daily job to send reports to the customers
    """
    from blowhorn.report.services.scheduled_reports import ReportService
    logger.info("mailing customer reports")
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return
    ReportService().send_customer_reports()


@celery_app.task
# @periodic_task(run_every=(crontab(hour="23", minute="00")))
@redis_batch_lock(period=3600, expire_on_completion=True)
def generate_customer_invoices():
    """Scheduled job at 4:30 AM to generate invoices."""
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    today = date.today()
    customers = Customer.objects.exclude(
        customer_category=CUSTOMER_CATEGORY_INDIVIDUAL).prefetch_related(
        'invoice_configs')

    for customer in customers:
        for config in customer.invoice_configs.filter(status=INVOICE_CONFIG_ACTIVE):
            try:
                aggregate_values = list(
                    config.aggregates.all().values_list('name', flat=True))
                date_start, date_end = get_payment_interval(today,
                                                            config.invoice_cycle)
                contract_ids = list(
                    config.contracts.values_list('id', flat=True))
                InvoiceHelper().generate_invoice(
                    customer, date_start, date_end, aggregate_values,
                    contract_ids=contract_ids, configs=config)

                # Regenerate invoice for next 3 days after cycle completion to cover any trips ended post cycle.
                if date_end < today < date_end + timedelta(days=4):
                    reference_date = today - timedelta(
                        days=today.day)  # Last day of previous month.
                    date_start, date_end = get_payment_interval(reference_date,
                                                                config.invoice_cycle)
                    InvoiceHelper().generate_invoice(
                        customer, date_start, date_end, aggregate_values,
                        contract_ids=contract_ids, configs=config)
            except:
                pass

    # currently hard coding the customerid and sellrate id
    # if we have more partners then we can add it on admin
    if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
        start = today.replace(day=1).strftime('%Y-%m-%d')
        last_day = calendar.monthrange(today.year, today.month)[1]
        end = today.replace(day=last_day).strftime('%Y-%m-%d')
        PartnerInvoiceHelper().generate_invoice(31403, 2960, start, end, False)


# @periodic_task(run_every=(crontab(hour="22", minute="00")))
# @redis_batch_lock(period=10800, expire_on_completion=True)
# def mark_invoice_customer_confirmed():
#     date_to_confirm_invoice = (datetime.datetime.today() - timedelta(days=25))
#     CustomerInvoice.objects.filter(
#         service_period_end__lte=date_to_confirm_invoice, status__in=[APPROVED, \
#                                                                      PENDING_CUSTOMER_CONFIRMATION]).update(
#         status=CUSTOMER_CONFIRMED)


@celery_app.task
# @periodic_task(run_every=(crontab(hour="03", minute="30", day_of_week="1,5")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def push_notification_to_customer():
    if settings.DEBUG:
        return
    templates = NotificationMessageTemplate.objects.filter(is_active=True, notification_group='CUSTOMER_APP')
    fcm_devices = FCMDevice.objects.filter(user__customer__isnull=False)
    fcm_devices = fcm_devices.select_related('user', 'user__customer')
    for tmp in templates:
        message = json.loads(tmp.template)
        body = message['body']
        for device in fcm_devices:
            template = body
            user = device.user
            message_ = template % (user.customer.name or user.customer.legalname or 'Guest')
            Notification.push_to_fcm_device(user, message['data'], title=message['title'], body=message_)


@celery_app.task()
def create_customer_invoice_v2(invoice_request_id=None):
    """Created for temporary usage for generating invoices for previous month (April 2018)."""
    if not invoice_request_id:
        invoice_requests = InvoiceRequest.objects.filter(status=PENDING)
    else:
        invoice_requests = [InvoiceRequest.objects.get(id=invoice_request_id)]

    for invoice_request in invoice_requests:
        date_ = invoice_request.invoice_period
        customer_invoice_config = invoice_request.invoice_config
        aggregate_values = customer_invoice_config.aggregates.values_list(
            'name', flat=True)
        contract_ids = customer_invoice_config.contracts.values_list('id',
                                                                     flat=True)

        intervals = customer_invoice_config.invoice_cycle.paymentinterval_set.values_list(
            'start_day', 'end_day').order_by('-id')

        cal_last_day = calendar.monthrange(date_.year, date_.month)
        last_day = intervals.first()[1]
        if cal_last_day[1] < last_day:
            last_day = cal_last_day[1]

        end_day = datetime.date(date_.year, date_.month, last_day)
        output_log = ''

        day_diff = 0
        invoice_request.status = SUCCESS
        for i in intervals:
            end_day = end_day - timedelta(days=day_diff)
            date_start, date_end = get_payment_interval(end_day,
                                                        customer_invoice_config.invoice_cycle)
            day_diff = (date_end - date_start).days + 1
            invoice_helper = InvoiceHelper()

            try:
                invoice_helper.generate_invoice(
                    customer_invoice_config.customer, date_start, date_end,
                    aggregate_values, contract_ids,
                    configs=customer_invoice_config,
                    invoice_request=invoice_request)
                if invoice_helper.output_log:
                    output_log = str(output_log) + "\n".join(
                        invoice_helper.output_log)
            except Exception as e:
                if output_log and invoice_helper.output_log:
                    output_log = str(output_log) + "\n".join(
                        invoice_helper.output_log) + \
                        "\n ERROR SUMMARY : " + str(e)
                else:
                    output_log = str(output_log) + "\n".join(
                        invoice_helper.output_log) + "\n ERROR SUMMARY : " + str(
                        e)
                invoice_request.status = FAILURE

        invoice_request.output_log = str(output_log)
        invoice_request.save()


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def send_invitation(self, org_id, user_info, failed_emails):
    """
    :param org_id: pk of Customer.

    :param user_info: list user details
    :param failed_emails: list of user mail-ids which are failed to migrate
        - It will send invitation mail to successfully migrated users.
    """
    recipients = [x for x in user_info if
                  x.get('user__email') not in failed_emails]

    logger.info('recipients: %s' % recipients)
    from blowhorn.customer.models import Customer, AccountUser
    org = Customer.objects.get(pk=org_id)

    for info in recipients:
        data = {
            'user_id': info.get('user__pk'),
            'organization': org,
            'status': 'old_invited'
        }
        try:
            account = AccountUser.objects.create(**data)
            logger.info('AccountUser: %s' % account)
        except IntegrityError as ie:
            logger.info('%s' % ie)
        except BaseException as be:
            logger.info('%s' % be)

        user, created = CustomerMixin().send_invitation('',
                                                        info.get('user__email'), '', org, resend=True)
        logger.info('Invitation sent successfully: %s' % user)


@app.task
@redis_batch_lock(period=900, expire_on_completion=True)
def publish_location_data(*args):

    customer_id = args[0]

    lock_status = RedisHelper().add_or_get_lock(
        'publish_location_data' + str(customer_id), 600)

    if lock_status:
        GPSEventIngestionService(customer_id).run()

    RedisHelper().expire_lock('publish_location_data' + str(customer_id))


# @celery_app.task(base=TransactionAwareTask, bind=True,
#              max_retries=settings.CELERY_EVENT_MAX_RETRIES,
#              ignore_result=False)
# def notify_exceeding_credit_limit(self, email, notification_details):
#     """
#     Send email to city managers and the supervisors of the customer.

#     contracts for customer exceeding credit limit
#     """
#     Email().send_credit_limit_mail(email, notification_details)



@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES,
             ignore_result=False)
def regenerate_customer_invoice(self, invoice_id):
    """
    Send email to city managers and the supervisors of the customer.

    contracts for customer exceeding credit limit
    """
    invoice = CustomerInvoice.objects.get(id=invoice_id)
    generate_invoice(invoice)

def generate_cashree_token():
    header = {'x-client-id': settings.CASH_FREE_PAYOUTS_GENERATE_TOKEN_ID,
              'x-client-secret': settings.CASH_FREE_GENERATE_TOKEN_KEY
              }
    url = settings.CASH_FREE_PAYOUTS_URL
    data = {'x-client-id': settings.CASH_FREE_PAYOUTS_GENERATE_TOKEN_ID,
            'x-client-secret': settings.CASH_FREE_GENERATE_TOKEN_KEY

            }
    response = requests.request("POST", url, headers=header, data=data)
    response_data = json.loads(response.content)
    status = response_data.get('status')
    if status == 'SUCCESS':
        token_data = response_data.get('data')
        token = token_data.get('token')
        return token
    else:
        print("Failed To Generate Token")

@celery_app.task
@redis_batch_lock(period=900, expire_on_completion=True)
def add_cashfre_benificiary():
    time = timezone.now() - timedelta(days=20)
    customer_ids = CashFreeBeneficiary.objects.all().values_list('customer_id', flat=True)
    customers = Customer.objects.filter(contract__contract_type=CONTRACT_TYPE_SHIPMENT,
                                        contract__status=CONTRACT_STATUS_ACTIVE,
                                        order__customer_payment_settled=False,
                                        order__status=StatusPipeline.ORDER_DELIVERED, order__cash_on_delivery__gt=0,
                                        order__created_date__gt=time
                                        ).exclude(id__in=customer_ids).distinct()
    for customer in customers:
        beneId = customer.id
        print(beneId)
        token = generate_cashree_token()
        add_benificiary(customer, beneId, token)

@celery_app.task
@redis_batch_lock(period=900, expire_on_completion=True)
def cashfre_payout():
    time = timezone.now() - timedelta(days=20)
    customers = Customer.objects.filter(contract__contract_type=CONTRACT_TYPE_SHIPMENT,
                                        contract__status=CONTRACT_STATUS_ACTIVE,
                                        order__status=StatusPipeline.ORDER_DELIVERED, order__cash_on_delivery__gt=0,
                                        order__created_date__gt=time)

    for customer in customers:
        payout_token = generate_cashree_token()

        order = Order.objects.filter(customer_payment_settled=False,
                                     payment_mode=PAYMENT_MODE_CASHFREE,
                                     payment_status=PAYMENT_STATUS_PAID,
                                     customer=customer).aggregate(
            cash_collected=Sum('cash_collected'))
        total_amount = order.get('cash_collected')
        bene_id = customer.id
        bank_acount_no = customer.bank_account
        transfer_id = random.randint(1, 99999999999)
        request_async_transfer(customer, total_amount, bene_id, transfer_id, bank_acount_no, payout_token)

@celery_app.task
def send_credit_limit_slack_notification(customer_id, pending_amount, credit_period_eligibilty):
    """Task to send slack notification in Channel: credit-cross-customer if customers credit limit is reached."""
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    if not pending_amount and credit_period_eligibilty:
        return
    if settings.ENABLE_SLACK_NOTIFICATIONS:
        _slack_url = settings.SHIPMENT_ORDER_SLACK_URL
        _slack_channel = settings.CREDIT_CROSS_CHANNEL
    else:
        _slack_url = getattr(settings, 'SLACK_URL', None)
        _slack_channel = getattr(
            settings, 'SLACK_CHANNELS', {}).get('PAYMENTS', None)

    from blowhorn.customer.models import Customer
    customer = Customer.objects.get(pk=customer_id)
    credit_limit = customer.credit_amount
    allowed_credit_period = customer.credit_period

    title = 'Credit Limit crossed'
    if pending_amount and not credit_period_eligibilty:
        title = "Both Credit Amount and Credit Period Limit crossed"
        lhs_value = "Permitted amount\nPending Amount\nPermitted Credit Period"
        rhs_value = "%s\n%s\n%s" % (
            credit_limit, pending_amount, allowed_credit_period)
    elif pending_amount:
        title = "Credit Amount Limit crossed"
        lhs_value = "Permitted amount\nPending Amount"
        rhs_value = "%s\n%s" % (credit_limit, pending_amount)
    else:
        title = "Credit Period Limit crossed"
        lhs_value = "Permitted Credit Period"
        rhs_value = "%s" % (allowed_credit_period)

    slack_message = {
        #            "text": title,
        "attachments": [
            {
                "author_name": 'Customer: %s\n Contact No.: %s' % (customer.name, customer.user.phone_number),
                "title": title + '\n (Calculations from 1st Aug 2018)',
                "fields": [
                    {
                        # "title": "Project",
                        "value": lhs_value,
                        "short": True
                    },
                    {
                        # "title": "Environment",
                        "value": rhs_value,
                        "short": True
                    }
                ],
                "color": "good"
            }
        ]
    }

    slack_message.update({'channel': _slack_channel})
    try:
        requests.post(url=_slack_url, json=slack_message)
    except:
        logging.error(traceback.format_exc())


@celery_app.task
# @periodic_task(run_every=(crontab(minute="59", hour="20")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def expire_invoices():
    """
    Mark invoices as EXPIRED.

    Daily job at morning 2:30 A.M. to fetch all the invoices
    which are in CREATED state from past one month and mark them as EXPIRED. (60 days)
    """
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return
    expiry_date = datetime.datetime.now().date() - timedelta(days=NUMBER_OF_DAYS_ELAPSED_FOR_MARKING_INVOICE_AS_EXPIRED)
    invoices = CustomerInvoice.objects.annotate(max_date=Greatest(F('created_date'),
                                                F('service_period_end'))).filter(
                                                status=CREATED, max_date__lt=expiry_date)
    print(invoices)
    if invoices:
        mark_invoices_as_expired(invoices, NUMBER_OF_DAYS_ELAPSED_FOR_MARKING_INVOICE_AS_EXPIRED)

@celery_app.task
# @periodic_task(run_every=(crontab(hour="23", minute="00")))
@redis_batch_lock(period=84600, expire_on_completion=True)
def close_invoices():
    """
    Scheduled job at 4:30 AM to mark invoices as expired after 45 days of service period end
    :return:
    """
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return
    expiry_date = datetime.datetime.now() - timedelta(days=INVOICE_DEACTIVATION_PERIOD)
    invoices = CustomerInvoice.objects.filter(status=CREATED, created_date__lt=expiry_date)

    print(invoices)
    if invoices:
        mark_invoices_as_expired(invoices, INVOICE_DEACTIVATION_PERIOD)

@celery_app.task
# @periodic_task(run_every=(crontab(minute="25", hour="18")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def assign_revenue_month_to_invoices():
    """
    Daily job at morning 11:55 P.M. to fetch all the invoices in the
    Customer Confirmed and Customer Acknowledged state and assign the
    Revenue Booked Month if not already assigned.

    """
    invoices_revenue_booked_month(invoice_ids=None)


@celery_app.task
# @periodic_task(run_every=(crontab(hour="10", minute="0", day_of_week='sunday')))
@redis_batch_lock(period=3600, expire_on_completion=True)
def booking_order_payment_due_notifications():
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    customers = Customer.objects.filter(
        order__order_type=settings.C2C_ORDER_TYPE,
        order__payment_status__in=[PAYMENT_STATUS_UNPAID,
                                   PAYMENT_STATUS_PARTIALLY_PAID],
        order__pickup_datetime__gte=OPENING_CREDIT_LIMIT_DATE,
        order__status=StatusPipeline.ORDER_DELIVERED).annotate(
            total_order_count=Count('order__id'),
        mobile_number=F('order__customer__user__phone_number'),
        total_amount=Sum('order__total_payable')
    )

    for customer in customers:
        credit_amount_eligibility, pending_amount = customer.get_credit_amount_eligibility()
        credit_period_eligibility = customer.get_credit_days_eligibility()
        proceed = credit_amount_eligibility and credit_period_eligibility
        if not proceed:
            order_qs = Order.objects.filter(
                customer=customer,
                status=StatusPipeline.ORDER_DELIVERED,
                payment_status__in=[PAYMENT_STATUS_UNPAID,
                                    PAYMENT_STATUS_PARTIALLY_PAID],
                pickup_datetime__gte=OPENING_CREDIT_LIMIT_DATE,
                total_payable__gt=0).order_by('pickup_datetime')

            if order_qs.exists():
                customer_name = customer.name
                total_payable = sum([float(o.total_payable) for o in order_qs])
                order_count = order_qs.count()
                if order_count > 1:
                    payment_url_bulk = settings.HOST_URL + PAYMENT_URL_BULK_WEB % (
                        customer.id,
                        random.randint(100, 500)
                    )
                    template_dict = sms_templates.payment_due_multiple
                    message = template_dict['text'] % (
                        customer_name,
                        total_payable,
                        order_count,
                        payment_url_bulk
                    )
                else:
                    order = order_qs.first()
                    pickup_date = order.pickup_datetime.astimezone(
                        localtime(settings.TIME_ZONE)).strftime(DATE_FORMAT)
                    payment_url = settings.HOST_URL + '/track/' + order.number
                    template_dict = sms_templates.payment_due
                    message = template_dict['text'] % (
                        customer_name,
                        pickup_date,
                        total_payable,
                        payment_url
                    )

                logger.info('Customer payment due msg: %s' % message)
                customer.user.send_sms(message, template_dict)

@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def broadcast_order_details_to_dashboard(self, order_id):
    order = Order.objects.get(id=order_id)
    from blowhorn.company.models import find_and_fire_hook
    logger.info('Broadcasting order updates ')
    DashboardRealtimeUpdate().push_order_details_update(order_id)
    find_and_fire_hook('order.added', order)

    if (order.source == 'shopify' and order.shopify_fulfillment_id and
            order.status in [StatusPipeline.OUT_FOR_DELIVERY,
            StatusPipeline.ORDER_RETURNED]):
        ShopifyFulfillmentService().create_shipment_event(order.id, order=order)


@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def broadcast_orderline_details_to_dashboard(self, order_id):
    logger.info('Broadcasting order updates ')
    DashboardRealtimeUpdate().push_orderline_update(order_id)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def send_transaction_update_to_customer(self, _dict):
    """
    Sends transaction updates to customer
    Credit, Debit notifications
    """
    logger.info('Sending transaction updates..')
    TransactionNotification().send_wallet_notification(_dict)



@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def send_donation_email(self, recipient, txn_id):
    """
    Sends email to donator
    """
    Email().send_donation_confirmation(recipient, txn_id)


@celery_app.task
@redis_batch_lock(period=3600, expire_on_completion=True)
def upload_to_gst_portal_automation():

    if not settings.ENABLE_SLACK_NOTIFICATIONS or settings.WEBSITE_BUILD == settings.MZANSIGO:
        return

    desc_batch = "Invoice Batch Upload to GST Portal %s" % (timezone.now().date().strftime('%d-%m-%Y'))

    approved_invoices = CustomerInvoice.objects.filter(status=APPROVED, upload_to_gst_portal_failed_reason__isnull=True,
                                                       customer__is_gstin_mandatory=True)
    invoices_count = approved_invoices.count()

    subject = 'BLOWHORN - %s Reports' % desc_batch

    successful_uploads = []
    failed_uploads = []

    batch = InvoiceUploadBatch.objects.create(description=desc_batch, number_of_entries=invoices_count)

    for invoice in approved_invoices:
        if not invoice.context:
            invoice.upload_to_gst_portal_failed_reason = 'No Context. Regenerate Invoice.'
            failed_uploads.append({"invoice_number": invoice.invoice_number,
                                   "remarks": invoice.upload_to_gst_portal_failed_reason})
            invoice.save()
        else:
            is_success, resp = upload_to_gst_portal_method(invoice)
            remarks = resp if isinstance(resp, str) else resp['Message']
            if is_success:
                successful_uploads.append({"invoice_number": invoice.invoice_number,
                                           "remarks": 'SUCCESS'})
            else:
                failed_uploads.append({"invoice_number": invoice.invoice_number,
                                       "remarks": remarks})
                invoice.upload_to_gst_portal_failed_reason = remarks
                invoice.save()

    message = successful_uploads + failed_uploads

    batch.successful_uploads = len(successful_uploads)
    batch.failed_uploads = len(failed_uploads)
    batch.message = message
    batch.end_time = timezone.now()
    batch.save()

    context =  {
        "total_uploads" : len(message),
        "successful_uploads" : len(successful_uploads),
        "failed_uploads" : len(failed_uploads),
        "brand_name" : settings.BRAND_NAME
    }
    email_template_name = 'emailtemplates_v2/automated_gst_upload.html'
    html_content = loader.render_to_string(email_template_name, context)

    mail = EmailMultiAlternatives(
        subject,
        html_content,
        to=UPLOADED_TO_GST_PORTAL_AUTOMATION_MAIL_LIST,
        cc=UPLOADED_TO_GST_PORTAL_AUTOMATION_CC_LIST,
        from_email=settings.DEFAULT_FROM_EMAIL)
    mail.attach_alternative(html_content, "text/html")

    if failed_uploads:
        filename = "%s_Reports.csv" % desc_batch
        _file = invoice_automation_excel(failed_uploads)
        _file = _file.encode("utf-8")
        mail.attach(filename, _file, 'text/csv')
        content = ContentFile(_file)
        batch.file.save(filename, content, save=True)

    mail.send()


@celery_app.task
@redis_batch_lock(period=10800, expire_on_completion=True)
def send_credit_breach_mails():
    CreditBreach().create_and_mail_customer_ledger()


@celery_app.task
@redis_batch_lock(period=10800, expire_on_completion=True)
def update_invoices_status_with_credit_notes():
    """
        Updates Invoice status to Payment Done | Partially Paid based on the remaining payable
         after deducting the cumulative credit note amount from the total payable
    """
    close_invoices_with_credit_notes()


@celery_app.task
@redis_batch_lock(period=3600, expire_on_completion=True)
def send_invoices_to_customers():
    """
    Sends invoices uploaded to gst portal to customers

    """
    send_invoices_to_customer()
