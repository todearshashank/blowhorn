import logging
import json
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db.models import Sum
from django.utils import timezone
from datetime import datetime, timedelta
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from django.db.models.signals import m2m_changed

from blowhorn.customer.models import InvoiceRequest, GPSEventPublishConfig, InvoicePayment
from blowhorn.customer.tasks import create_customer_invoice_v2
from blowhorn.customer.redis_models import Customer as RedisCustomer
from .constants import PAYMENT_DONE


@receiver(post_save, sender=InvoiceRequest)
def trigger_invoice_creation_past(sender, instance, created, **kwargs):
    if created:
        create_customer_invoice_v2.apply_async((instance.id,), eta=timezone.now() + timedelta(seconds=5))


@receiver(post_save, sender=GPSEventPublishConfig)
@receiver(m2m_changed, sender=GPSEventPublishConfig.contracts.through)
def add_gps_event_config_to_cache(sender, instance, **kwargs):
    """ To avoid config to be read from database for every `interval`, we
    are storing the same in cache at customer level.
    """

    logging.info(
        "GPSEventPublishConfig : Adding config to cache for customer "
        "%s" % instance.customer)

    RedisCustomer(instance.customer_id)['gps_event_config'] = {
        'config_id': instance.id,
        'is_enabled': int(instance.enabled),
        'interval': int(instance.interval),
        'contract_ids': list(instance.contracts.values_list('id', flat=True)),
        'api_endpoint': instance.api_endpoint,
        'http_request_type': instance.http_request_type,
        'auth_config': {
            'type': instance.authentication_config.authentication_type,
            'credentials': instance.authentication_config.credentials.to_dict()
        },
        'max_records_per_driver': instance.max_records_per_driver,
        'timestamp_event_last_push': {}
    }


@receiver(post_save, sender=GPSEventPublishConfig)
def create_gps_event_publish_periodic_task(
    sender, instance, created, **kwargs):
    """ For any config created or updated, would have periodic task
    schedule updated accordingly.
    """

    def get_task_name():
        """ Returns the GPS Event Publishing task name specific to customer.
        """

        return 'Publish GPS Events for customer ' + str(instance.customer_id)

    logging.info(
        "GPSEventPublishConfig : Creating periodic task for Publish GPS "
        "Events for customer %s " % instance.customer)

    PeriodicTask.objects.filter(
        name=get_task_name()).delete()

    schedule, _ = IntervalSchedule.objects.get_or_create(
        every=instance.interval, period=IntervalSchedule.SECONDS,
    )

    PeriodicTask.objects.create(
        interval=schedule,
        name=get_task_name(),
        task='blowhorn.customer.tasks.publish_location_data',
        args=json.dumps([instance.customer_id])
    )


@receiver(post_save, sender=InvoicePayment)
def calculate_unallocated_amount(sender, instance, created, **kwargs):
    source = kwargs.pop('source', None)
    if instance.collection:
        allocated_amount_sum = InvoicePayment.objects.filter(collection=instance.collection).exclude(id=source)\
            .aggregate(Sum('actual_amount'))
        bal = instance.collection.amount - (allocated_amount_sum.get('actual_amount__sum', 0) or 0)
        instance.collection.unallocated_amount = bal
        instance.collection.save()
