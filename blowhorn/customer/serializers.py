# System and Django
import logging
from datetime import timedelta
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

# Third Party Libraries
from phonenumber_field.phonenumber import PhoneNumber
from blowhorn.oscar.core.loading import get_model
from rest_framework import serializers, exceptions
from rest_framework.response import Response

# Blowhorn Modules
from blowhorn.payment.constants import PAYMENT_COLLECTED_FROM_CUSTOMER, PAYMENT_RECEIVED_BY_ASSOCIATE, \
    PAYMENT_SETTLEMENT_INITIATED, PAYMENT_SETTLED
from blowhorn.payment.models import OrderCODPayment, PaymentBankTxn
from config.settings import status_pipelines as StatusPipeline
from blowhorn.address.models import City, Hub, State
from blowhorn.address.serializer import ShippingAddressSerializer, HubGeoFeatureSerializer
from blowhorn.address.utils import AddressUtil
from blowhorn.common.helper import CommonHelper
from blowhorn.common.response_templates import PASS, FAIL
from blowhorn.common.serializers import BaseSerializer
from blowhorn.customer.constants import RESPONSE_MESSAGES, \
    ORGANIZATION_MESSAGES, MEMBER_INVITED, MEMBER_ACTIVE, CUSTOMER_CONFIRMED, \
    CUSTOMER_DISPUTED, INVOICE_ACCEPT, INVOICE_DISPUTE, \
    CUSTOMER_CONFIRMED_MAIL_BODY, CUSTOMER_CONFIRMED_MAIL_SUBJECT, \
    DEFAULT_DUEDATE_DELTA_DAYS, CUSTOMER_TAX_TYPE_TERMS, EXPECTED_DELIVERY_HOURS_TIMEDELTA
from blowhorn.customer.mixins import CustomerRatingMixin, \
    CustomerTaxInformationMixin, CustomerMixin
from blowhorn.customer.models import CustomerRating, CustomerFeedback, \
    Route, RouteStop, CustomerTaxInformation, AccountUser,\
    ShipmentAlertConfiguration
from blowhorn.driver.models import Driver
from blowhorn.driver.serializers import DriverSerializer, \
    DriverRatingDetailSerializer
from blowhorn.order.models import Order, OrderBatch
from blowhorn.order.serializers.order_line import OrderLineSerializer
from blowhorn.trip.models import Trip
from blowhorn.trip.serializers import TripStopSerializer
from blowhorn.vehicle.models import VehicleClass
from blowhorn.users.utils import UserUtils
from blowhorn.utils.functions import utc_to_ist, validate_name
from blowhorn.vehicle.serializers import VehicleSerializer
from blowhorn.customer.submodels.invoice_models import create_invoice_history, \
    InvoiceHistory
from blowhorn.customer.backends.tokens import REGISTRATION_TIMEOUT_DAYS
from blowhorn.utils.mail import Email
from blowhorn.order.models import *

User = get_model('users', 'User')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CustomerProfileSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    name = serializers.CharField(max_length=60)
    mobile = serializers.CharField(max_length=10, min_length=10)


class RouteStopSerializer(serializers.ModelSerializer):
    class Meta:
        model = RouteStop
        fields = ['id', 'seq_no', 'line1', 'line2', 'line3', 'line4',
                  'postcode', 'state', 'country', 'contact_name',
                  'contact_mobile', 'geopoint', 'address_name']


class RouteSerializer(serializers.ModelSerializer):
    stops = RouteStopSerializer(many=True)

    def create(self, validated_data):
        stops = validated_data.pop('stops')
        route = Route.objects.create(**validated_data)
        for stop in stops:
            RouteStop.objects.create(route=route, **stop)
        return route

    def update(self, instance, validated_data):
        stops = validated_data.pop('stops')
        saved_stops = list(instance.stops.all())
        instance.name = validated_data.get('name')
        instance.city = validated_data.get('city')
        instance.distance = validated_data.get('distance')
        instance.contact_name = validated_data.get('contact_name')
        instance.contact_mobile = validated_data.get('contact_mobile')
        instance.save()
        for saved_stop in saved_stops:
            saved_stop.delete()
        for stop in stops:
            RouteStop.objects.create(route=instance, **stop)
        return instance

    class Meta:
        model = Route
        fields = ['id', 'name', 'city', 'distance', 'contact_name',
                  'contact_mobile', 'stops']


class CustomerOtpSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False)
    mobile = serializers.CharField()
    otp = serializers.CharField()
    guest_booking = serializers.BooleanField(required=False)

    def validate(self, attrs):
        mobile = attrs.get('mobile', None)
        otp = attrs.get('otp', None)

        if not mobile or not otp:
            response = {
                'status': FAIL,
                'message': _(RESPONSE_MESSAGES[FAIL]['REQUIRED'])
            }
            raise serializers.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=response
            )

        user = self.context.get('user', None)
        if not user:
            user = UserUtils().get_user_from_phone_number(mobile)
            if not user:
                response = {
                    'status': FAIL,
                    'message': _(RESPONSE_MESSAGES[FAIL]['MOBILE_NOT_FOUND'])
                }
                raise exceptions.NotFound(
                    code=status.HTTP_404_NOT_FOUND,
                    detail=response
                )

        if not user.phone_number:
            response = {
                'status': FAIL,
                'message': _(RESPONSE_MESSAGES[FAIL]['MOBILE_NOT_FOUND'])
            }
            raise exceptions.NotFound(
                code=status.HTTP_404_NOT_FOUND,
                detail=response
            )

        if str(mobile) not in [str(user.phone_number), str(user.national_number)]:
            response = {
                'status': FAIL,
                'message': _(RESPONSE_MESSAGES[FAIL]['MOBILE_NOT_FOUND'])
            }
            raise exceptions.NotFound(
                code=status.HTTP_404_NOT_FOUND,
                detail=response
            )

        stored_otp, created = User.objects.retrieve_otp(user)
        logger.info('Verifying otp. User: %s, Stored-otp: %s' %
                    (user, stored_otp))
        if created:
            response = {
                'status': FAIL,
                'message': _(RESPONSE_MESSAGES[FAIL]['NO_OTP_FOUND'])
            }
            raise exceptions.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=response
            )

        if str(otp) != str(stored_otp):
            logger.info('Otp mismatch. User: %s, Otp: %s, Stored-otp: %s' %
                        (user, otp, stored_otp))
            if settings.WEBSITE_BUILD != settings.DEFAULT_BUILD:
                logger.error('Otp mismatch. User: %s, Otp: %s, Stored-otp: %s' %
                             (user, otp, stored_otp))
            user.login_attempt = user.login_attempt + 1
            if user.login_attempt > settings.INVALID_OTP_ATTEMPTS_ALLOWED:
                user.is_active = False
            user.save()
            response = {
                'status': FAIL,
                'message': _(RESPONSE_MESSAGES[FAIL]['OTP_MISMATCH'])
            }
            raise exceptions.NotFound(
                code=status.HTTP_400_BAD_REQUEST,
                detail=response
            )

        attrs['user'] = user
        return attrs

    def update(self, instance, validated_data):
        instance.is_mobile_verified = True
        instance.is_active = True
        instance.save()
        success = True
        if not validated_data.get('guest_booking'):
            login_response = User.objects.do_login(
                request=self.context['request'], user=instance)
            success = login_response.status_code == status.HTTP_200_OK

        if success:
            response = {
                'status': PASS,
                'message': _(RESPONSE_MESSAGES[PASS])
            }
            is_staff = instance.is_staff
            if not is_staff:
                customer = CustomerMixin().get_customer(instance)
                Customer.objects.add_otp(customer=customer, otp=validated_data.get('otp'),
                                         remarks='OTP entered by customer - Website')
                response['is_staff'] = is_staff
                response['is_business_user'] = customer and customer.is_non_individual()
            return status.HTTP_200_OK, response

        response = _('Mobile number verification failed')
        return status.HTTP_400_BAD_REQUEST, response


class CustomerFeedbackSerializer(serializers.ModelSerializer):
    queryset = CustomerFeedback.objects.all()

    class Meta:
        model = CustomerFeedback
        fields = '__all__'


class CustomerRatingSerializer(serializers.ModelSerializer,
                               CustomerRatingMixin):
    queryset = CustomerRating.objects.all()

    def create(self, validated_data):
        response = self.get_validated_data()
        if response.status_code != status.HTTP_200_OK:
            return response
        return self.create_rating(data=response.data)

    def update(self, instance, validated_data):
        response = self.get_validated_data()
        if response.status_code != status.HTTP_200_OK:
            return response
        return self.update_rating(data=response.data)

    def get_validated_data(self):
        data = self.initial_data
        # Validate whether all required inputs are there or not?
        mandatory_fields = ['rating']

        for field in mandatory_fields:
            if field not in data:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_(field + ' is mandatory.'))

        # Set / Derive the model fields
        validated_data = {}
        validated_data['rating'] = data.get('rating')
        validated_data['customer'] = data.get('customer')
        validated_data['cumulative_rating'] = data.get('cumulative_rating')
        validated_data['num_of_feedbacks'] = data.get('num_of_feedbacks')
        return Response(status=status.HTTP_200_OK, data=validated_data)


class CustomerTaxInformationSerializer(serializers.Serializer,
                                       CustomerTaxInformationMixin):
    customer_id = serializers.IntegerField()
    is_business_user = serializers.BooleanField(required=False)
    gstin = serializers.CharField(required=False)
    state = serializers.IntegerField(required=False)

    def queryset(self, customer):
        if customer:
            return CustomerTaxInformation.objects.filter(customer=customer)
        else:
            return CustomerTaxInformation.objects.all()

    def validate(self, attrs):
        gstin = attrs.get('gstin', None)
        if gstin:
            if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
                state_code = self.get_state_code_from_gstin(gstin)
                state = AddressUtil().get_state_from_code(state_code)
                if not state.exists():
                    _out = {
                        'status': 'FAIL',
                        'message': _('Invalid GSTIN')
                    }
                    raise exceptions.ParseError(_out)
            else:
                state_pk = attrs.get('state', None)
                state = State.objects.filter(pk=state_pk)

            attrs['state'] = state.first()
        return attrs

    def create(self, validated_data):
        validated_data.pop('is_business_user', None)
        return CustomerTaxInformation.objects.create(**validated_data)

    def update(self, instance, validated_data):
        validated_data.pop('is_business_user', None)
        instance.gstin = validated_data.get('gstin', None)
        instance.state = validated_data.get('state', None)
        instance.save()
        return instance


class FleetSearchFormHubSerializer(serializers.ModelSerializer):
    coverage = serializers.SerializerMethodField()

    class Meta:
        model = Hub
        fields = ['id', 'name', 'coverage']

    def get_coverage(self, obj):
        if obj:
            hub_data = HubGeoFeatureSerializer(obj).data
            coverage = hub_data.get('geometry', {})
            return coverage

        return {}


class FleetSearchFormStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = ['id', 'name']


class FleetSearchFormCitySerializer(serializers.ModelSerializer):
    hubs = serializers.SerializerMethodField()
    geopoint = serializers.SerializerMethodField()

    class Meta:
        model = City
        fields = ['id', 'name', 'hubs', 'geopoint']

    def get_hubs(self, city):
        return FleetSearchFormHubSerializer(city.hubs, many=True).data

    def get_geopoint(self, city):
        center_location = city.coverage_dropoff.centroid
        return [center_location.x, center_location.y]


class FleetSearchFormVehicleTypeSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = VehicleClass
        fields = ['id', 'name']

    def get_name(self, vehicle_class):
        return vehicle_class.commercial_classification


class FleetSearchFormDriverSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(FleetSearchFormDriverSerializer, self).__init__(*args, **kwargs)
        self.data_by_driver_id = {}
        trips = self.context.get("trips", None)
        if trips:
            trips = trips.values(
                'driver_id', 'driver__name', 'hub', 'hub__city_id',
                'vehicle__vehicle_model__vehicle_class_id', 'status',
                'actual_start_time', 'driver__driveractivity__geopoint',
                'trip_number', 'vehicle__registration_certificate_number',
                'order__number')
            for trip in trips:
                self.data_by_driver_id[trip.pop("driver_id")] = trip

    name = serializers.SerializerMethodField(read_only=True)
    city = serializers.SerializerMethodField(read_only=True)
    hub = serializers.SerializerMethodField(read_only=True)
    vehicle_class = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    timestamp_arrived_at_pickup = serializers.SerializerMethodField(
        read_only=True)
    last_updated_location = serializers.SerializerMethodField(read_only=True)
    mobile = serializers.SerializerMethodField(read_only=True)
    order_number = serializers.SerializerMethodField(read_only=True)
    trip_number = serializers.SerializerMethodField(read_only=True)
    vehicle_number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Driver
        fields = [
            'id', 'name', 'city', 'hub', 'vehicle_class', 'status',
            'timestamp_arrived_at_pickup', 'last_updated_location', 'mobile',
            'order_number', 'trip_number', 'vehicle_number']

    def get_name(self, driver):
        return "%s | %s" % (self.data_by_driver_id.get(driver.id, {}).get("driver__name", None),
                            self.data_by_driver_id.get(driver.id, {}).get(
                                "vehicle__registration_certificate_number", None))

    def get_city(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get("hub__city_id",
                                                             None)

    def get_hub(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get("hub", None)

    def get_vehicle_class(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get(
            "vehicle__vehicle_model__vehicle_class_id", None)

    def get_status(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get("status", None)

    def get_timestamp_arrived_at_pickup(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get(
            "actual_start_time", None)

    def get_last_updated_location(self, driver):
        geopoint = self.data_by_driver_id.get(driver.id, {}).get(
            "driver__driveractivity__geopoint", None)
        if geopoint:
            location = AddressUtil().get_latlng_from_geopoint(geopoint)
            return location['lat'], location['lng']
        return None, None

    def get_mobile(self, driver):
        return driver.user.national_number

    def get_vehicle_number(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get(
            "vehicle__registration_certificate_number", None)

    def get_trip_number(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get("trip_number",
                                                             None)

    def get_order_number(self, driver):
        return self.data_by_driver_id.get(driver.id, {}).get("order__number",
                                                             None)


class FleetTripListSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(FleetTripListSerializer, self).__init__(*args, **kwargs)

    driver = serializers.SerializerMethodField()
    vehicle = serializers.SerializerMethodField()
    time_scheduled = serializers.SerializerMethodField()
    time_started_actual = serializers.SerializerMethodField()
    time_ended_actual = serializers.SerializerMethodField()
    total_time = serializers.SerializerMethodField()
    total_distance = serializers.SerializerMethodField()
    time_elapsed = serializers.SerializerMethodField()
    distance_elapsed = serializers.SerializerMethodField()
    order_no = serializers.SerializerMethodField()
    city_id = serializers.SerializerMethodField()
    trip_type = serializers.SerializerMethodField()
    stops_count = serializers.SerializerMethodField()
    wait_time = serializers.SerializerMethodField()
    short_status = serializers.SerializerMethodField()

    class Meta:
        model = Trip
        fields = [
            'id', 'driver', 'vehicle', 'time_scheduled', 'time_started_actual',
            'time_elapsed', 'distance_elapsed', 'trip_number', 'order_no',
            'time_ended_actual', 'total_time', 'total_distance', 'city_id',
            'trip_type', 'otp', 'stops_count', 'wait_time', 'status', 'short_status'
        ]

    def get_short_status(self, trip):
        """
        Grouping list of status into one
        """
        _status = ''
        if trip.status in StatusPipeline.TRIP_END_STATUSES:
            _status = 'completed'
        elif trip.status == StatusPipeline.TRIP_NEW:
            _status = 'upcoming'
        else:
            _status = 'current'
        return _status

    def get_driver(self, trip):
        return {
            'name': trip.driver.name if trip.driver else '',
            'phone': str(trip.driver.user.phone_number) if trip.driver else ''
        }

    def get_vehicle(self, trip):
        return {
            'name': trip.vehicle.registration_certificate_number if trip.vehicle else '',
            'type': trip.vehicle.vehicle_model.model_name if trip.vehicle else ''
        }

    def get_time_scheduled(self, trip):
        return trip.planned_start_time

    def get_wait_time(self, trip):
        return trip.total_trip_wait_time_driver if trip.total_trip_wait_time_driver else 0

    def get_time_started_actual(self, trip):
        return trip.actual_start_time

    def get_time_ended_actual(self, trip):
        return trip.actual_end_time

    def get_time_elapsed(self, trip):
        return trip.get_elapsed_time()

    def get_distance_elapsed(self, trip):
        # TODO : Expected to be retrieved at Frontend from Firebase for
        # timebeing.
        return None

    def get_total_time(self, trip):
        return trip.total_time

    def get_total_distance(self, trip):
        return trip.total_distance

    def get_order_no(self, trip):
        if trip.order:
            return trip.order.number
        return None

    def get_city_id(self, trip):
        if trip.order:
            return trip.order.city_id
        return None

    def get_trip_type(self, trip):
        customer_contract = trip.customer_contract
        if customer_contract:
            return customer_contract.contract_type
        return None

    def get_stops_count(self, trip):
        if trip.stops.exists():
            return trip.stops.count()
        return 0


class FleetShipmentOrderListSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(FleetShipmentOrderListSerializer, self).__init__(*args, **kwargs)

    driver = serializers.SerializerMethodField()
    customer = serializers.SerializerMethodField()
    date_placed = serializers.SerializerMethodField()
    picked_time = serializers.SerializerMethodField()
    delivered_time = serializers.SerializerMethodField()
    expected_delivery_time_start = serializers.SerializerMethodField()
    expected_delivery_time_end = serializers.SerializerMethodField()
    pod_files = serializers.SerializerMethodField()
    city_name = serializers.SerializerMethodField()
    reference_number = serializers.SerializerMethodField()
    customer_reference_number = serializers.SerializerMethodField()
    trip_number = serializers.SerializerMethodField()
    num_of_attempts = serializers.SerializerMethodField()
    orderlines = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    driver_rating = serializers.SerializerMethodField()
    expected_pickup_time = serializers.SerializerMethodField()
    allow_assign_driver = serializers.SerializerMethodField()
    is_valid_lineedit_status = serializers.SerializerMethodField()

    def get_allow_assign_driver(self, obj):
        return obj.status == StatusPipeline.ORDER_NEW

    def get_is_valid_lineedit_status(self, obj):
        # used to make orderlines editable in dashboard
        status_list = [StatusPipeline.ORDER_NEW, StatusPipeline.ORDER_OUT_FOR_PICKUP,
                                StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED]
        return obj.status in status_list

    def get_status(self, order):
        if order.status == UNABLE_TO_DELIVER:
            if order.events:
                filtered_events = [o for o in order.events if o.status ==
                                   StatusPipeline.UNABLE_TO_DELIVER]
                sorted_events = sorted(filtered_events, key=lambda x: x.time)
                if len(sorted_events) > 0 and sorted_events[-1].remarks\
                    and StatusPipeline.RESCHEDULE_DELIVERY\
                        in sorted_events[-1].remarks:
                    return StatusPipeline.RESCHEDULE_DELIVERY
        return order.status

    def get_orderlines(self, order):
        order_lines = sorted(
            order.order_lines.all(),
            key=lambda x: x.item_sequence_no or 0)
        return OrderLineSerializer(order_lines, many=True).data

    def get_driver_rating(self, order):
        return DriverRatingDetailSerializer(
            order.driverratingdetails_set.all(),
            many=True,
            context={
                'fields': ['rating', 'remark', 'customer_remark']
            }
        ).data

    def get_trip_number(self, order):
        if order.waypoints and order.waypoints[0].stops_list:
            return order.waypoints[0].stops_list[-1].trip.trip_number
        if order.stops_list:
            return order.stops_list[-1].trip.trip_number
        return ''

    def get_num_of_attempts(self, order):
        if order.events:
            attempted_events = [
                StatusPipeline.ORDER_DELIVERED,
                StatusPipeline.ORDER_RETURNED,
                StatusPipeline.UNABLE_TO_DELIVER]
            return len(
                [o for o in order.events if o.status in attempted_events])
        return 0

    def get_pod_files(self, order):
        pod_files = []
        documents = order.documents.all()
        for i, document in enumerate(documents):
            _dict = document.to_json(include_event=True)
            pod_files.append(_dict)
        return pod_files

    def get_date_placed(self, order):
        if order and order.date_placed:
            return order.date_placed
        return ''

    def get_picked_time(self, order):
        for event in order.events:
            if StatusPipeline.ORDER_PICKED == event.status:
                return utc_to_ist(event.time).isoformat()
        else:
            return None

    def get_delivered_time(self, order):
        if order and order.updated_time and order.status in [
            StatusPipeline.ORDER_DELIVERED,
            StatusPipeline.DELIVERY_ACK
        ]:
            delivered_time = order.updated_time
            return delivered_time
        return ''

    def get_customer(self, order):
        if order.shipping_address:
            phone_number = ''
            _phone_number = order.shipping_address.phone_number
            if _phone_number is not None:
                if isinstance(_phone_number, str):
                    phone_number = _phone_number
                else:
                    phone_number = str(
                        order.shipping_address.phone_number.national_number)
            return {
                'name': order.shipping_address.first_name,
                'phone': phone_number,
                'address': order.shipping_address.line1
            }
        return {}

    def get_driver(self, order):
        if order.driver:
            return {
                'name': order.driver.name,
                'phone': str(order.driver.user.phone_number)
            }
        return {}

    def get_city_name(self, order):
        if order.city:
            return order.city.name
        return ''

    def get_reference_number(self, order):
        if order.reference_number:
            return order.reference_number
        return ''

    def get_customer_reference_number(self, order):
        if order.customer_reference_number:
            return order.customer_reference_number
        return ''

    def get_expected_delivery_time_start(self, order):
        if order.exp_delivery_start_time:
            return order.exp_delivery_start_time
        return ''

    def get_expected_delivery_time_end(self, order):
        if order.expected_delivery_time:
            return order.expected_delivery_time
        return ''

    def get_expected_pickup_time(self, order):
        if order.pickup_datetime:
            return order.pickup_datetime

        elif order.created_date:
            expected_delivery_hours = order.customer_contract.expected_delivery_hours\
                if order.customer_contract.expected_delivery_hours else\
                order.customer_contract.customer.expected_delivery_hours
            expected_pickup_hours = expected_delivery_hours.hour \
                                    + expected_delivery_hours.minute / 60 \
                if expected_delivery_hours else EXPECTED_DELIVERY_HOURS_TIMEDELTA
            expected_pickup_time = order.created_date + timedelta(hours=expected_pickup_hours)
            return expected_pickup_time

        return ''

    class Meta:
        model = Order
        fields = [
            'id', 'number', 'driver', 'date_placed', 'delivered_time', 'return_order',
            'is_offline_order', 'pod_files', 'city_name', 'status',
            'cash_on_delivery', 'customer', 'reference_number',
            'trip_number', 'expected_delivery_time_start', 'expected_delivery_time_end',
            'otp', 'customer_reference_number', 'expected_pickup_time',
            'batch', 'num_of_attempts', 'payment_mode', 'amount_paid_online',
            'cash_collected', 'orderlines', 'picked_time', 'driver_rating',
            'allow_assign_driver', 'is_valid_lineedit_status', 'shipping_charges'
        ]


class FleetShipmentExportListSerializer(FleetShipmentOrderListSerializer):

    def __init__(self, *args, **kwargs):
        super(FleetShipmentExportListSerializer, self).__init__(*args, **kwargs)

    driver = serializers.SerializerMethodField()
    delivered_time = serializers.SerializerMethodField()
    expected_delivery_time = serializers.SerializerMethodField()
    customer_name = serializers.SerializerMethodField()
    customer_phone = serializers.SerializerMethodField()
    customer_address = serializers.SerializerMethodField()
    hub_name = serializers.SerializerMethodField()
    num_of_attempts = serializers.SerializerMethodField()
    unable_to_deliver_ack = serializers.SerializerMethodField()
    unable_to_deliver = serializers.SerializerMethodField()
    returned_ack = serializers.SerializerMethodField()
    reached_at_hub = serializers.SerializerMethodField()
    pickup_attempted = serializers.SerializerMethodField()
    out_for_delivery = serializers.SerializerMethodField()
    order_returned_to_hub = serializers.SerializerMethodField()
    order_returned = serializers.SerializerMethodField()
    order_picked = serializers.SerializerMethodField()
    order_packed = serializers.SerializerMethodField()
    order_out_for_pickup = serializers.SerializerMethodField()
    order_new = serializers.SerializerMethodField()
    order_moving_to_hub = serializers.SerializerMethodField()
    order_lost = serializers.SerializerMethodField()
    order_delivered = serializers.SerializerMethodField()
    driver_assigned = serializers.SerializerMethodField()
    driver_arrived_at_pickup = serializers.SerializerMethodField()
    driver_arrived_at_dropoff = serializers.SerializerMethodField()
    driver_accepted = serializers.SerializerMethodField()
    delivery_ack = serializers.SerializerMethodField()
    attempted_timestamps = serializers.SerializerMethodField()
    remarks = serializers.SerializerMethodField()

    def get_attempted_timestamps(self, order):
        if order.events:
            attempted_events = [
                StatusPipeline.ORDER_DELIVERED,
                StatusPipeline.ORDER_RETURNED,
                StatusPipeline.UNABLE_TO_DELIVER]
            return ','.join([
                utc_to_ist(o.time).strftime(settings.ADMIN_DATETIME_FORMAT)
                for o in order.events if o.status in attempted_events
            ])
        return ''

    def get_num_of_attempts(self, order):
        if order.events:
            attempted_events = [
                StatusPipeline.ORDER_DELIVERED,
                StatusPipeline.ORDER_RETURNED,
                StatusPipeline.UNABLE_TO_DELIVER]
            return len(
                [o for o in order.events if o.status in attempted_events])
        return 0

    def get_customer_phone(self, order):
        if order.shipping_address:
            if order.shipping_address.phone_number is not None \
                and order.shipping_address.phone_number.national_number \
                    is not None:
                phone_number = str(
                    order.shipping_address.phone_number.national_number)
                return phone_number
        return ''

    def get_customer_name(self, order):
        if order.shipping_address:
            return order.shipping_address.first_name
        return ''

    def get_customer_address(self, order):
        if order.shipping_address:
            return order.shipping_address.line1
        return ''

    def get_driver(self, order):
        if order.driver:
            return order.driver.name
        return ''

    def get_hub_name(self, order):
        if order.hub:
            return order.hub.name
        return ''

    def get_date_placed(self, order):
        if order and order.date_placed:
            date_placed = utc_to_ist(
                order.date_placed).strftime(settings.ADMIN_DATETIME_FORMAT)
            return date_placed
        return ''

    def get_delivered_time(self, order):
        if order and order.updated_time and order.status in [
            StatusPipeline.ORDER_DELIVERED,
            StatusPipeline.DELIVERY_ACK
        ]:
            delivered_time = utc_to_ist(
                order.updated_time).strftime(settings.ADMIN_DATETIME_FORMAT)
            return delivered_time
        return ''

    def get_expected_delivery_time(self, order):
        if order.expected_delivery_time:
            return utc_to_ist(order.expected_delivery_time).strftime(
                settings.ADMIN_DATETIME_FORMAT)
        return ''

    def formatted_date(self, events, elem):
        for event in events:
            if elem == event.status:
                val = utc_to_ist(event.time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
                if event.remarks:
                    val = '%s (%s)' % (val, event.remarks)
                return val
        else:
            return '--NA--'

    def get_order_new(self, order):
        if order.events:
            return self.formatted_date(order.events, ORDER_NEW)
        return ''

    def get_unable_to_deliver_ack(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.UNABLE_TO_DELIVER_ACK)
        return ''

    def get_unable_to_deliver(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.UNABLE_TO_DELIVER)
        return ''

    def get_returned_ack(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.RETURNED_ACK)
        return ''

    def get_reached_at_hub(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.REACHED_AT_HUB)
        return ''

    def get_pickup_attempted(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.PICKUP_ATTEMPTED)
        return ''

    def get_out_for_delivery(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.OUT_FOR_DELIVERY)
        return ''

    def get_order_returned_to_hub(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_RETURNED_TO_HUB)
        return ''

    def get_order_returned(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_RETURNED)
        return ''

    def get_order_picked(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_PICKED)
        return ''

    def get_order_packed(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_PACKED)
        return ''

    def get_order_out_for_pickup(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_OUT_FOR_PICKUP)
        return ''

    def get_order_moving_to_hub(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_MOVING_TO_HUB)
        return ''

    def get_order_lost(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_LOST)
        return ''

    def get_order_delivered(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.ORDER_DELIVERED)
        return ''

    def get_driver_assigned(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.DRIVER_ASSIGNED)
        return ''

    def get_driver_arrived_at_pickup(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.DRIVER_ARRIVED_AT_PICKUP)
        return ''

    def get_driver_arrived_at_dropoff(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.DRIVER_ARRIVED_AT_DROPOFF)
        return ''

    def get_driver_accepted(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.DRIVER_ACCEPTED)
        return ''

    def get_delivery_ack(self, order):
        if order.events:
            return self.formatted_date(
                order.events, StatusPipeline.DELIVERY_ACK)
        return ''

    def get_remarks(self, order):
        if order.events:
            failure_events = [StatusPipeline.ORDER_RETURNED,
                              StatusPipeline.UNABLE_TO_DELIVER]
            filtered_events = [o for o in order.events
                               if o.status in failure_events]
            if len(filtered_events) > 0:
                sorted_events = sorted(filtered_events,
                                       key=lambda x: x.time, reverse=True)
                return sorted_events[0].remarks
        return '--NA--'

    class Meta:
        model = Order
        fields = [
            'number', 'driver', 'date_placed', 'delivered_time',
            'city_name', 'status', 'hub_name', 'customer_name',
            'customer_phone', 'customer_address', 'reference_number',
            'trip_number', 'expected_delivery_time',
            'customer_reference_number', 'num_of_attempts',
            'order_new', 'unable_to_deliver_ack', 'unable_to_deliver',
            'returned_ack', 'reached_at_hub', 'pickup_attempted',
            'out_for_delivery', 'order_returned_to_hub', 'order_returned',
            'order_picked', 'order_packed', 'order_out_for_pickup',
            'order_moving_to_hub', 'order_lost', 'order_delivered',
            'driver_assigned', 'driver_arrived_at_pickup',
            'driver_arrived_at_dropoff', 'driver_accepted', 'delivery_ack',
            'attempted_timestamps', 'customer_id', 'remarks', 'shipping_charges'
        ]


class FleetTripSerializer(serializers.ModelSerializer):
    pickup = serializers.SerializerMethodField()
    drop = serializers.SerializerMethodField()
    driver = serializers.SerializerMethodField()
    vehicle = serializers.SerializerMethodField()
    stops = serializers.SerializerMethodField()
    distance_estimated = serializers.SerializerMethodField()
    distance_actual = serializers.SerializerMethodField()

    class Meta:
        model = Trip
        fields = ['id', 'pickup', 'drop', 'driver', 'vehicle',
                  'stops', 'distance_estimated', 'distance_actual',
                  'status', 'otp']

    def get_pickup(self, trip):
        if trip.order and trip.order.pickup_address:
            return {
                'address': ShippingAddressSerializer(
                    trip.order.pickup_address,
                    fields=["line1", "line2", "line3", "line4", "state",
                            "phone_number", "postcode", "geopoint",
                            "country"]).data,
                "time": trip.actual_start_time
            }
        return None

    def get_drop(self, trip):
        if trip.order and trip.order.shipping_address:
            return {
                'address': ShippingAddressSerializer(
                    trip.order.shipping_address,
                    fields=["line1", "line2", "line3", "line4", "state",
                            "phone_number", "postcode", "geopoint",
                            "country"]).data,
                "time": trip.actual_start_time
            }
        return None

    def get_driver(self, trip):
        return DriverSerializer(trip.driver,
                                fields=["id", "name", "phone_number"]).data

    def get_vehicle(self, trip):
        return VehicleSerializer(
            trip.vehicle,
            fields=["vehicle_model", "registration_certificate_number"],
            alt_name_map={"vehicle_model": "model",
                          "registration_certificate_number": "reg_no"}
        ).data

    def get_stops(self, trip):
        return TripStopSerializer(trip.stops.all(), many=True,
                                  remove_fields=["trip", "waypoint"]).data

    def get_distance_estimated(self, trip):
        return trip.estimated_distance_km

    def get_distance_actual(self, trip):
        # TODO : Expected to be retrieved at Frontend from Firebase for
        # timebeing.
        return None


class FleetShipmentsSearchFormDriverSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(FleetShipmentsSearchFormDriverSerializer,
              self).__init__(*args, **kwargs)

    class Meta:
        model = Driver
        fields = ['id', 'name', ]


class CustomerShippingAddressSerializer(BaseSerializer):
    class Meta:
        model = ShippingAddress
        fields = '__all__'


class CustomerOrderSerializer(BaseSerializer):
    class Meta:
        model = Order
        # fields = '__all__'
        exclude = ('customer', 'number', 'created_by', 'modified_by', 'site')


class WaypointSerializer(BaseSerializer):
    class Meta:
        model = WayPoint
        exclude = ('created_by', 'modified_by')


class LabourSerializer(BaseSerializer):
    class Meta:
        model = Labour
        fields = '__all__'


class FleetOrderBatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderBatch
        fields = ['id', 'description', 'number_of_entries']


class ShipmentCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ['id', 'name']


class OrganizationMemberSerializer(serializers.ModelSerializer, CustomerMixin, UserUtils):
    name = serializers.CharField(source='user.name', required=False,
                                 allow_blank=True)
    email = serializers.EmailField(source='user.email', required=False)
    phone_number = serializers.SerializerMethodField(
        source='user.national_number', required=False)
    is_active = serializers.BooleanField(source='user.is_active',
                                         required=False)
    created = serializers.DateTimeField(read_only=True)
    modified = serializers.DateTimeField(read_only=True)
    organization_id = serializers.IntegerField(required=False)
    user_id = serializers.IntegerField(required=False)
    status = serializers.SerializerMethodField()
    is_invite_link_expired = serializers.SerializerMethodField(required=False)
    roles = serializers.SerializerMethodField(required=False)

    class Meta:
        model = AccountUser
        fields = [
            'id', 'name', 'email', 'phone_number', 'is_active', 'is_admin',
            'organization_id', 'created', 'modified', 'user_id', 'status',
            'last_invite_sent_on', 'is_invite_link_expired', 'roles']

    def get_status(self, obj):
        account_status = obj.status
        if account_status.startswith('old_'):
            return MEMBER_INVITED
        return account_status

    def get_phone_number(self, obj):
        return obj.user.national_number

    def get_roles(self, obj):
        return [{'id': g.id, 'name': g.name} for g in obj.groups.all()]

    def get_is_invite_link_expired(self, obj):
        last_invite_sent_on = obj.last_invite_sent_on
        if obj.status in [ MEMBER_INVITED.lower(), 'old_%s' % MEMBER_INVITED.lower()] \
            and obj.last_invite_sent_on:
            expiry_date = timezone.now() - timedelta(days=REGISTRATION_TIMEOUT_DAYS)
            return last_invite_sent_on < expiry_date
        return False

    def queryset(self, customer, query=None):
        return self.Meta.model.objects.filter(organization=customer)

    def validate(self, attrs):
        request = self.context['request']
        email = attrs.get('email', '').lower()
        if self.is_staff_email(email):
            raise exceptions.NotAcceptable(
                self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['field']['email']['staff_email']
                )
            )

        user_associated_with_other_org = False
        if self.instance:
            if self.Meta.model.objects.filter(user__email=email)\
                    .exclude(organization=self.instance.organization).exists():
                user_associated_with_other_org = True

            if attrs.get('is_admin', None) is not None and \
                self.instance.user == request.user and \
                    self.instance.is_admin != attrs.get('is_admin'):
                raise exceptions.NotAcceptable(
                    self.get_response_message(
                        ORGANIZATION_MESSAGES['permission']['admin_status']
                    )
                )

        elif self.Meta.model.objects.filter(user__email=email).exists():
            user_associated_with_other_org = True

        if user_associated_with_other_org:
            raise exceptions.ValidationError(
                self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['integrity'].format(
                        email=email))
            )

        user = attrs.get('user', {})
        name = user.get('name', None)
        if name and not validate_name(name):
            raise exceptions.ParseError(
                self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['field']['name'])
            )

        phone_number = self.context.get('phone_number', None)
        if phone_number and not self.validate_phonenumber(str(phone_number)):
            raise exceptions.ParseError(
                self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['field']['phone_number'][
                        'invalid'])
            )

        phone_number_obj = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, phone_number)
        user_qa = User.objects.filter(phone_number=phone_number_obj)
        if self.instance:
            user_qa = user_qa.exclude(pk=self.instance.user_id)

        if user_qa.exists():
            raise exceptions.ParseError(
                self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['field']['phone_number'][
                        'duplicate'])
            )

        attrs['phone_number'] = phone_number
        status = self.context.get('status', None)
        if status:
            attrs['status'] = status
        return attrs

    def create(self, validated_data):
        return self.Meta.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        user_details = validated_data.get('user')
        user = instance.user
        user.name = user_details.get('name', '')
        user.email = user_details.get('email', '')
        phone_number = validated_data.get('phone_number', None)
        if phone_number:
            phone_number = PhoneNumber(
                settings.ACTIVE_COUNTRY_CODE,
                phone_number
            )
            user.phone_number = phone_number

        status = validated_data.get('status', None)
        if status:
            user.is_active = status == MEMBER_ACTIVE.lower()
            instance.status = status

        user.save()
        if instance.is_admin != validated_data.get('is_admin', ''):
            instance.is_admin = validated_data.get('is_admin', '')
        instance.save()
        return instance


class InvoiceHistorySerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(InvoiceHistorySerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if exclude_fields and fields:
            msg = _('Either use `fields` or `exclude_fields`')
            raise ImproperlyConfigured(msg)

        if fields:
            self.Meta.fields = fields

        elif exclude_fields:
            self.Meta.exclude = exclude_fields

        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = InvoiceHistory


class CustomerInvoiceListSerializer(serializers.ModelSerializer):
    invoice_history = serializers.SerializerMethodField()
    invoice_state = serializers.SerializerMethodField()
    invoice_type = serializers.SerializerMethodField()
    total_rcm_amount = serializers.FloatField()
    taxable_amount = serializers.FloatField()

    class Meta:
        model = CustomerInvoice
        fields = ['id', 'invoice_number', 'invoice_state', 'invoice_date',
                  'service_period_start', 'service_period_end',
                  'status', 'total_amount', 'charges_basic', 'charges_service',
                  'total_rcm_amount', 'taxable_adjustment',
                  'non_taxable_adjustment', 'total_service_charge',
                  'taxable_amount', 'cgst_percentage', 'sgst_percentage',
                  'igst_percentage', 'cgst_amount', 'sgst_amount',
                  'igst_amount', 'total_amount', 'invoice_history',
                  'invoice_type', 'total_toll_charges', 'total_call_charges',
                  'total_parking_charges']

    def get_invoice_type(self, obj):
        return CUSTOMER_TAX_TYPE_TERMS.get(obj.invoice_type, None)

    def get_invoice_state(self, obj):
        return obj.invoice_state.name

    def get_invoice_history(self, obj):
        if hasattr(obj, 'invoicehistory_set'):
            history = [h for h in obj.invoicehistory_set.all() if
                       h.new_status == obj.status]
            history = sorted(history, key=lambda x: x.created_time)
            if history:
                latest_entry = history[-1]
                created_by = latest_entry.created_by.email if latest_entry.created_by else ''
                return {'created_by': created_by,
                        'created_time': utc_to_ist(
                            latest_entry.created_time).strftime(
                            settings.APP_DATETIME_FORMAT)}
        return {}


class CustomerActionSerializer(serializers.Serializer):
    action = serializers.CharField(required=False)
    reason = serializers.CharField(required=False, allow_null=True,
                                   allow_blank=True)
    comments = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    def __get_spoc(self, invoice):
        spoc = None
        invoice_history = InvoiceHistory.objects.filter(invoice=invoice)

        if invoice_history:
            invoice_history = invoice_history.latest('created_time')
            spoc = invoice_history.created_by
        return spoc

    def __get_due_date(self):
        return (timezone.now() + timedelta(
            days=DEFAULT_DUEDATE_DELTA_DAYS)).date()

    def __send_mail(self, action, invoice, recipient, user, customer,
                    reason=None):
        email = Email()
        if action == INVOICE_ACCEPT:
            subject = CUSTOMER_CONFIRMED_MAIL_SUBJECT.format(
                customer=customer,
                invoice=invoice
            )
            content = CUSTOMER_CONFIRMED_MAIL_BODY.format(
                newline_delimiter='<br>',
                accepted_by=user,
                customer=customer,
                invoice=invoice,
                brand_name=settings.BRAND_NAME,
                link=reverse("admin:customer_customerinvoice_change",
                             args=[invoice.pk]),
            )
            recipient = '%s' % recipient if recipient else None
            if recipient:
                email.send_invoice_confirmation_mail_to_sales_representative(
                    subject, content, recipient)

        elif action == INVOICE_DISPUTE:
            mail_content = {
                'invoice': invoice,
                'status': invoice.status,
                'due_date': invoice.due_date,
                'reason': reason,
                'assignor_name': user.name,
                'assignor_email': user,
                'assignee_email': [str(recipient)] if recipient else None,
            }
            email.email_invoice_assignment_notification(mail_content)

    def validate(self, attrs):
        action = attrs.get('action', None)
        if not action or action not in [INVOICE_DISPUTE, INVOICE_ACCEPT]:
            raise exceptions.ParseError(_('Action not permitted'))

        user = self.context.get('user', None)
        if action == INVOICE_DISPUTE:
            if not attrs.get('reason', None):
                raise exceptions.ParseError(_('Please, provide a reason.'))

            attrs['status'] = CUSTOMER_DISPUTED
        elif action == INVOICE_ACCEPT:
            attrs['status'] = CUSTOMER_CONFIRMED
            attrs['customer'] = CustomerMixin().get_customer(user)

        attrs['spoc'] = self.__get_spoc(self.instance)
        attrs['user'] = user
        return attrs

    def create(self, validated_data):
        raise NotImplementedError('`create()` must be implemented.')

    def update(self, instance, validated_data):
        user = validated_data.get('user', None)
        action = validated_data.get('action', None)
        invoice_status = validated_data.get('status', None)
        spoc = validated_data.get('spoc', None)
        reason = validated_data.get('reason', '')
        comments = validated_data.get('comments', None)
        due_date = self.__get_due_date()

        if comments:
            reason = '%s | %s' % (reason, comments)
        with transaction.atomic():
            create_invoice_history(
                instance.status,
                invoice_status,
                instance,
                spoc,
                due_date,
                reason=reason,
                created_by=user
            )
            instance.status = invoice_status
            instance.assigned_date = timezone.now().date()
            instance.due_date = due_date
            if spoc:
                instance.current_owner = spoc
            instance.save()

            # Commenting because approve is not sending mail
            # and dispute is disabled from dashboard
            # self.__send_mail(action, instance, spoc, user,
            #                  validated_data.get('customer'), reason)

            message = ''
            if action == INVOICE_ACCEPT:
                message = _('Confirmed')
            elif action == INVOICE_DISPUTE:
                message = _('Submitted for dispute')
        return message


class ShipmentAlertConfigSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(ShipmentAlertConfigSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if exclude_fields and fields:
            msg = _('Either use `fields` or `exclude_fields`')
            raise ImproperlyConfigured(msg)

        if fields:
            self.Meta.fields = fields

        elif exclude_fields:
            self.Meta.exclude = exclude_fields

        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = ShipmentAlertConfiguration


class CustomerFailedOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderApiData
        fields = '__all__'


class OrderPaymentHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderPaymentHistory
        fields = '__all__'


class FleetCodPaymentsSerializer(serializers.ModelSerializer):
    order_number = serializers.SerializerMethodField()
    trip_number = serializers.SerializerMethodField()
    payment_bank_txn = serializers.SerializerMethodField()
    delivery_hub = serializers.SerializerMethodField()
    driver_name = serializers.SerializerMethodField()
    payment_mode = serializers.SerializerMethodField()
    driver_vehicle = serializers.SerializerMethodField()
    order_payment_history = serializers.SerializerMethodField()


    class Meta:
        model = OrderCODPayment
        fields = ['id', 'order_number', 'trip_number', 'driver_name', 'driver_vehicle', 'payment_bank_txn',
                  'payment_mode', 'delivery_hub', 'amount', 'created_date', 'is_settled_to_customer', 'status',
                  'file', 'ack_ref', 'utr','order_payment_history']

    def get_order_number(self, obj):
        if obj.order:
            return obj.order.number
        return ''

    def get_trip_number(self, obj):
        if obj.trip:
            return obj.trip.trip_number
        return ''

    def get_payment_bank_txn(self, obj):
        if obj.payment_bank_txn:
            return obj.payment_bank_txn.txn_id
        return ''

    def get_delivery_hub(self, obj):
        if obj.order.hub:
            return obj.order.hub.name
        return ''

    def get_driver_name(self, obj):
        if obj.order.driver:
            return obj.order.driver.name
        return ''

    def get_payment_mode(self, obj):
        if obj.order:
            return obj.order.payment_mode
        return ''

    def get_driver_vehicle(self, obj):
        if obj.order.driver:
            return obj.order.driver.driver_vehicle
        return ''

    def get_order_payment_history(self, obj):
        if obj.order:
            return OrderPaymentHistorySerializer(
                OrderPaymentHistory.objects.filter(order=obj.order),
                many=True
            ).data
        return ''
