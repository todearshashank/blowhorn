from functools import reduce

from django.utils import timezone
from datetime import timedelta
from django.conf import settings

from blowhorn.oscar.core.loading import get_model
from blowhorn.contract.permissions import is_superuser_or_supervisor, is_spoc_or_supervisor, is_spoc, is_supervisor, \
    is_invoice_supervisor, is_invoice_spoc, is_default_invoice_type, is_finance_user
from blowhorn.customer.constants import REVENUE_THRESHOLD_DATE, UPLOADED_TO_GST_PORTAL, APPROVED


def is_current_user(instance, user):
    if instance.current_owner == user:
        return True
    return False


def is_invoice_superuser_or_supervisor(instance, user):
    return is_superuser_or_supervisor(instance.contract, user)


def has_approval_permissions(instance, user):
    return is_spoc(instance.contract, user) or is_current_user(instance, user)


def has_unapproval_permissions(instance, user):
    return is_spoc_or_supervisor(instance.contract, user)


def has_reject_and_send_permissions(instance, user):
    return is_supervisor(instance.contract, user) or \
        is_current_user(instance, user)


def can_approve(instance):
    if instance.is_stale:
        return False
    return True


def can_regenerate(instance):
    return instance.is_stale


def has_reassigning_permissions(instance, user):
    spoc_or_supervisor = is_invoice_supervisor(
        instance, user) or is_invoice_spoc(instance, user)
    return is_current_user(instance, user) or spoc_or_supervisor


def has_send_for_approval_permission(instance, user):
    return is_spoc(instance, user) or is_current_user(instance, user)


def is_city_sales_manager(instance, user):
    from blowhorn.address.models import City
    if not user.is_superuser:
        if instance:
            if is_default_invoice_type(instance):
                return City.objects.filter(
                    state__in=[instance.bh_source_state,
                               instance.invoice_state],
                    sales_representatives=user).exists()
            else:
                return City.objects.filter(sales_representatives=user).exists()
        elif is_current_user(instance, user):
            return True
        else:
            return False
    return True

def is_qualifiy_for_unapprove(instance):
    from blowhorn.customer.models import InvoicePayment
    return not InvoicePayment.objects.filter(collection=instance).exists()



def is_sales_manager_or_spoc(instance, user):
    if is_city_sales_manager(instance, user) or is_spoc(instance.contract, user):
        return True
    else:
        return False


def is_collection_representative(instance, user):
    from blowhorn.company.models import Company

    return Company.objects.filter(collection_representatives=user).exists()


def is_executive(user):
    '''returns True if user is a company executive'''
    from blowhorn.company.models import Company

    return Company.objects.filter(executives=user).exists()


def can_mark_payment_done(instance):
    from blowhorn.customer.models import CustomerInvoice
    from blowhorn.customer.constants import CREDIT_NOTE, INVOICE_EDITABLE_STATUS, \
        MIN_INVOICE_REMAINING_AMOUNT, MAX_INVOICE_REMAINING_AMOUNT
    credit_amount_sum = 0.0
    amount_sum = 0.0
    if instance:
        credit_note_amount = CustomerInvoice.objects.filter(
            invoice_type=CREDIT_NOTE, related_invoice=instance).exclude(
            status__in=INVOICE_EDITABLE_STATUS).values_list('total_amount',
                                                            flat=True)

        if credit_note_amount:
            credit_amount_sum = reduce(
                (lambda x, y: x + y), credit_note_amount)

        paid_amount = instance.invoicepayment_set.all().values_list('amount',
                                                                    flat=True)
        if paid_amount:
            amount_sum = reduce((lambda x, y: x + y), paid_amount)

        remaining_amount = round(
            (float(instance.total_amount) - float(amount_sum) + float(
                credit_amount_sum)), 2)

        if remaining_amount <= MAX_INVOICE_REMAINING_AMOUNT:
            return True
    return False


def within_dispute_threshold_date(instance):
    current_date = timezone.now().date()
    threshold_date = instance.service_period_end + timedelta(
        days=REVENUE_THRESHOLD_DATE)
    if current_date > threshold_date:
        return False
    return True


def within_cancel_threshold_date(instance):
    from blowhorn.customer.submodels.invoice_models import InvoiceHistory
    inv_history = InvoiceHistory.objects.filter(invoice=instance, old_status=APPROVED,
                                                new_status=UPLOADED_TO_GST_PORTAL).first()

    if not inv_history or (inv_history and inv_history.created_time + timedelta(hours=23) < timezone.now()):
        return False
    return True


def gst_filing_required(instance):
    if not instance.customer.is_gstin_mandatory:
        return False
    if settings.GST_FILING_REQUIRED and not instance.is_e_invoice_filed:
        if not instance.context:
            return False
        return True
    return False


def check_gst_filing_required(instance):
    if not instance.customer.is_gstin_mandatory:
        return True
    return False if settings.GST_FILING_REQUIRED and not instance.is_e_invoice_filed else True


def has_payment_receive_permission(instance, user):

    if is_collection_representative(instance, user) or is_finance_user(instance, user):
        return True
    return False
