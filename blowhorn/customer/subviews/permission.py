from blowhorn.utils.functions import log_db_queries
import json
import logging
from django.conf import settings
from django.db import IntegrityError, transaction
from django.db.models import Prefetch
from django.utils.translation import ugettext_lazy as _

from rest_condition import Or
from rest_framework import views, filters, response, generics
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from blowhorn.address.models import City, Hub
from blowhorn.contract.models import Contract
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer, IsOrgAdmin, IsOrgOwner
from blowhorn.customer.models import AccountUser, System, AccountUserGroup, \
    UserPermission, PermissionLevel, UserModule, CustomerDivision, PermissionLevelStore
from blowhorn.customer.subserializer.permissions import \
    AccountUserSerializer, UserPermissionSerializer, SystemSerializer, \
    AccountUserGroupSerializer, PermissionLevelSerializer, UserModuleSerializer,\
    AccountUserGroupListSerializer
from blowhorn.wms.models import WhSite

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class PermissionSupportDataView(views.APIView, CustomerMixin):
    """
    GET fleet/member/supportdata
    Returns list of permissions available in system
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request, user_pk=None):
        data = self.__get_results(user_pk)
        return response.Response(status=200, data=data)

    def __get_results(self, user_pk):
        data = self.request.GET
        system_code = data.get('system_code', None)
        customer = self.get_customer(self.request.user)

        system_qs = None
        module_qs = None
        permission_qs = None
        if system_code:
            if settings.USE_READ_REPLICA:
                system_qs = System.objects.using('readrep1').filter(code=system_code)
                module_qs = UserModule.objects.using('readrep1').filter(system_id=system_qs.first().pk)
                permission_qs = UserPermission.objects.using('readrep1').filter(module__in=module_qs)
            else:
                system_qs = System.objects.filter(code=system_code)
                module_qs = UserModule.objects.filter(system_id=system_qs.first().pk)
                permission_qs = UserPermission.objects.filter(module__in=module_qs)
        else:
            if settings.USE_READ_REPLICA:
                system_qs = System.objects.using('readrep1').all()
                module_qs = UserModule.objects.using('readrep1').all()
                permission_qs = UserPermission.objects.using('readrep1').all()
            else:
                system_qs = System.objects.all()
                module_qs = UserModule.objects.all()
                permission_qs = UserPermission.objects.all()

        systems = SystemSerializer(
            system_qs,
            many=True,
            context={
                'fields': ('id', 'name', 'code')
            }
        ).data

        modules = UserModuleSerializer(
            module_qs,
            many=True,
            context={
                'fields': ('id', 'name', 'code', 'system', 'display_order')
            }
        ).data

        permissions = UserPermissionSerializer(
            permission_qs,
            many=True,
            context={
                'fields': ('id', 'name', 'codename', 'module')
            }
        ).data

        if settings.USE_READ_REPLICA:
            permission_level_queryset = PermissionLevel.objects.using('readrep1').all()
        else:
            permission_level_queryset = PermissionLevel.objects.all()

        permission_levels = PermissionLevelSerializer(
            permission_level_queryset,
            many=True,
            context={
                'fields': ('id', 'name', 'position', 'system')
            }
        ).data

        if settings.USE_READ_REPLICA:
            group_qs = AccountUserGroup.objects.using('readrep1').filter(organization=customer)
        else:
            group_qs = AccountUserGroup.objects.filter(organization=customer)

        groups = AccountUserGroupSerializer(
            group_qs,
            many=True,
            context={
                'fields': ('id', 'name', 'permissions'),
                'permissions_fields': ('id', 'name', 'codename', 'module')
            }
        ).data

        _dict = {
            'systems': systems,
            'modules': modules,
            'groups': groups,
            'permissions': permissions,
            'permission_levels': permission_levels,
        }
        return _dict

class UserPermissionView(views.APIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (
        IsAuthenticated, IsNonIndividualCustomer,
        Or(IsOrgOwner, IsOrgAdmin))

    def get(self, request, user_pk=None):
        """
        GET fleet/member/<user_pk>/permissions
        Returns list of permissions owned for given user
        """
        data = request.data
        fields = data.get('fields', None)
        results = self._get_results(user_pk, fields=fields)
        return response.Response(status=200, data=results)

    def _get_results(self, user_pk, fields=None):
        if settings.USE_READ_REPLICA:
            user = AccountUser.objects.using('readrep1')\
                .prefetch_related('user_permissions', 'groups', 'permission_levels')\
                .get(pk=user_pk)
        else:
            user = AccountUser.objects\
                .prefetch_related('user_permissions', 'groups', 'permission_levels')\
                .get(pk=user_pk)

        default_fields = {
            'fields': ('user_permissions', 'groups', 'permission_levels'),
            'group_fields': ('id', 'name', 'permissions', 'permission_levels'),
            'permissions_fields': ('id', 'name', 'codename', 'module'),
            'permission_level_store_fields': ('id', 'module_id', 'permission_level_id', 'value')
        }
        return AccountUserSerializer(user, context=default_fields).data


    def __update_user_groups(self, account_user, groups):
        """
        Updates groups associated with user
        :param account_user: instance of AccountUser
        :param groups: pk of groups
        """
        groups_qs = account_user.groups.all()
        initial_groups = [i.pk for i in groups_qs]
        logger.info('inital groups: %s' % initial_groups)
        groups_added = [x for x in groups if x not in initial_groups]
        logger.info('groups_added groups: %s' % groups_added)
        for group in groups_added:
            account_user.groups.add(group)

        removed_groups = [x for x in initial_groups if x not in groups]
        logger.info('removed_groups groups: %s' % removed_groups)
        for pk in removed_groups:
            account_user.groups.remove(pk)

        return True

    @log_db_queries
    def __delete_all_permissions(self, account_user, system_code):
        """
        Deletes all m2m relations; groups, permissions and permission levels
        :param account_user: Instance of Account User
        :param system_code: code of System (str)
        """
        permissions_qs = account_user.user_permissions.all()
        for g in permissions_qs:
            if g.module.system.code == system_code:
                account_user.user_permissions.remove(g.pk)

        permission_level_qs = account_user.permission_levels.all()
        for g in permission_level_qs:
            if g.permission_level.system.code == system_code:
                account_user.permission_levels.remove(g.pk)

    @log_db_queries
    def __add_user_permissions(self, account_user, levels, permissions):
        """
        Adds all m2m relations; groups, permissions and permission levels
        :param account_user: Instance of Account User
        :param levels: list of permission levels (list of dict)
        :param permissions: pk of permissions
        """
        perm_levels = [PermissionLevelStore(**_dict) for _dict in levels]
        permission_levels = PermissionLevelStore.objects.bulk_create(perm_levels)
        for level in permission_levels:
            account_user.permission_levels.add(level)

        for permission in permissions:
            account_user.user_permissions.add(permission)

        account_user.save()
        return account_user

    def put(self, request, user_pk):
        """
        PUT fleet/member/<user_pk>/permissions
        Updates permissions owned for given user
        """
        data = request.data
        account_user = AccountUser.objects\
            .prefetch_related('groups', 'permission_levels', 'user_permissions')\
            .get(pk=user_pk)
        permissions = data.get('permissions', [])
        levels = data.get('levels', [])
        groups = data.get('groups', [])
        system_code = data.get('system_code', 'TMS') or 'TMS'
        try:
            with transaction.atomic():
                self.__delete_all_permissions(account_user, system_code)

        except BaseException as be:
            logger.warning('Failed to remove permission objects. %s' % be)
            return response.Response(
                status=400,
                data=_('Failed to update user permissions')
            )

        try:
            with transaction.atomic():
                self.__add_user_permissions(account_user, levels, permissions)
                self.__update_user_groups(account_user, groups)

            return response.Response(status=200, data=_('Permissions updated successfully'))
        except BaseException as be:
            logger.warning('Failed to add permission objects. %s' % be)
            return response.Response(
                status=400,
                data=_('Failed to update user permissions')
            )


class PermissionLevelView(views.APIView, CustomerMixin):

    def get(self, request):
        """
        GET fleet/member/permissionlevel
        Returns list of values for given permission level
        """
        data = request.data or request.GET
        system_id = data.get('system_id')
        params = {
            'system_id': system_id
        }
        results = self.__get_results(params)
        return response.Response(status=200, data=results)

    def __get_results(self, params):
        from blowhorn.wms.submodels.location import Client

        customer = self.get_customer(self.request.user)
        if settings.USE_READ_REPLICA:
            levels = PermissionLevel.objects.using('readrep1').filter(**params).values('model_name', 'name')
            customer_cities = Contract.objects.using('readrep1').filter(customer=customer)\
                .values_list('city', flat=True).distinct()
        else:
            levels = PermissionLevel.objects.filter(**params).values('model_name', 'name')
            customer_cities = Contract.objects.filter(customer=customer).values_list('city', flat=True).distinct()

        resp = {}
        for level in levels:
            value_fields = ['id', 'name']
            query_params = {}
            level_name = level.get('name')
            model_name = level.get('model_name') or level.get('name')
            if level_name == 'City':
                query_params['id__in'] = customer_cities

            elif level_name == 'Hub':
                value_fields = ['id', 'name', 'city_id']
                query_params['customer'] = customer
                query_params['city_id__in'] = customer_cities

            elif level_name == 'Site':
                query_params['city_id__in'] = customer_cities

            else:
                query_params['customer'] = customer

            qs = []
            try:
                if settings.USE_READ_REPLICA:
                    qs = eval(model_name).objects.using('readrep1').filter(**query_params)
                else:
                    qs = eval(model_name).objects.filter(**query_params)

                qs = qs.values(*value_fields).order_by('name')

            except BaseException as e:
                logger.info('Error while fetching permission level: %s' % e)

            resp[level_name] = qs

        return resp


class UserGroupListView(generics.ListCreateAPIView, CustomerMixin):
    queryset = AccountUserGroup.objects.all()
    serializer_class = AccountUserGroupListSerializer
    permission_classes = [IsOrgAdmin,]

    def list(self, request):
        """
        GET /fleet/member/groups
        Returns list of groups of a customer
        """
        queryset = self.get_queryset()
        serializer = self.serializer_class(
            queryset,
            many=True,
            context={
                'fields': ('id', 'name', 'permissions', 'permission_levels'),
                'permissions_fields': ('id', 'name', 'codename', 'module')
            }
        )
        return response.Response(serializer.data)

    def get_queryset(self):
        user = self.request.user
        customer = self.get_customer(user)
        data = self.request.GET
        search_term = data.get('search_term', None)
        params = {
            'organization': customer
        }
        if search_term:
            params['name__icontains'] = search_term

        if settings.USE_READ_REPLICA:
            groups = AccountUserGroup.objects.using('readrep1').filter(**params)
        else:
            groups = AccountUserGroup.objects.filter(**params)

        return groups.prefetch_related('permissions', 'permission_levels')

    def post(self, request):
        """
        POST fleet/member/groups
        Creates the group for a customer
        """
        data = request.data
        name = data.get('name', None)
        permissions = data.get('permissions', [])
        customer = self.get_customer(request.user)
        data['organization'] = customer.pk
        serializer = AccountUserGroupSerializer(
            data=data,
            context={
                'fields': ('name', 'organization', 'permissions', 'permission_levels'),
                'customer': customer,
            }
        )
        if serializer.is_valid(raise_exception=True):
            instance = serializer.save()
            return response.Response(
                status=200,
                data={
                    'message': _('Group created successfully'),
                    'data': self.serializer_class(instance).data
                }
            )

        print(serializer.errors)
        return response.Response(
            status=400,
            data=_('Failed to create group: %s' % name)
        )


class UserGroupView(views.APIView, CustomerMixin):
    serializer_class = AccountUserGroupListSerializer

    def get(self, request, pk=None):
        """
        GET fleet/member/group/<pk>
        Returns permission details of a group
        """
        try:
            group_instance = AccountUserGroup.objects\
                .prefetch_related('permission_levels', 'permissions')\
                .get(pk=pk)
        except AccountUserGroup.DoesNotExist:
            return response.Response(
                status=404,
                data=_('Group not found')
            )

        serializer = self.serializer_class(
            group_instance,
            context={
                'fields': ('id', 'name', 'permissions', 'permission_levels'),
                'permissions_fields': ('id', 'name', 'codename', 'module'),
                'fetch_level_instances': True
            }
        )
        return response.Response(status=200, data=serializer.data)

    def __create_permission_levels(self, permission_levels):
        perm_levels = [PermissionLevelStore(**_dict) for _dict in permission_levels]
        return PermissionLevelStore.objects.bulk_create(perm_levels)

    def put(self, request, pk=None):
        """
        PUT fleet/member/group/<pk>
        Updates permission details of a group
        """
        data = request.data
        customer = self.get_customer(request.user)
        data['organization'] = customer.pk

        permission_levels = data.pop('permission_levels', None)
        if permission_levels:
            level_instances = self.__create_permission_levels(permission_levels)
            data['permission_levels'] = [p.pk for p in level_instances]

        try:
            group = AccountUserGroup.objects.get(pk=pk)
        except AccountUserGroup.DoesNotExist:
            return response.Response(
                status=404,
                data=_('Group not found')
            )

        serializer = AccountUserGroupSerializer(group, data=data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(
                status=200,
                data=_('Group updated successfully')
            )

        print(serializer.errors)
        return response.Response(
            status=400,
            data=_('Failed to update group')
        )
