from rest_framework import generics, filters, views, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.customer.subserializer.customer_asset import \
    CustomerContainerSerializer, CustomerContainerHistorySerializer
from blowhorn.order.models import OrderContainer, OrderContainerHistory


class CustomerContainerView(views.APIView):
    """
    GET fleet/shipment/containers
    Returns list of containers associated with given order/trip
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)
    serializer = CustomerContainerSerializer

    def get(self, request):
        serializer = self.serializer
        params = self.request.query_params.dict()
        order_number = params.get('order_number')
        trip_number = params.get('trip_number')
        _dict = dict()
        select_related_fields = ['stop', 'container', 'order',
                                 'container__container_type', 'trip']
        if order_number:
            _dict = dict(order__number=order_number)
        elif trip_number:
            customer = CustomerMixin().get_customer(request.user)
            _dict = dict(trip__trip_number=trip_number,
                         order__customer=customer)
            select_related_fields += ['trip']

        containers = OrderContainer.objects \
            .select_related(*select_related_fields).filter(**_dict)
        if not containers.exists():
            select_related_fields = ['stop', 'order', 'trip']
            containers = OrderContainerHistory.objects\
                .select_related(*select_related_fields)\
                .filter(**_dict)
            serializer = CustomerContainerHistorySerializer
        qs = containers.order_by('-created_date')

        results = serializer(qs, many=True).data
        return Response(status=200, data=results)
