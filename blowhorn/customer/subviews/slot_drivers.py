import json
import operator
from functools import reduce
from django.conf import settings
from django.contrib.gis.db.models import Count, OuterRef, Subquery
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from rest_framework import views, filters, response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated

from blowhorn.address.models import City, Hub, Slots
from blowhorn.address.serializer import SlotModelSerializer
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.order.models import Order
from blowhorn.trip.models import Trip
from blowhorn.vehicle.models import VehicleClass
from config.settings import status_pipelines


class ShipmentSlotDriverListView(views.APIView):
    """
    GET /fleet/shipment/slots/drivers
    Returns list of drivers for given order, and city
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request):
        user = request.user
        customer = CustomerMixin().get_customer(user)
        data = request.query_params
        order_number = data.get('order_number', None)

        order = None
        try:
            order = Order.objects.get(number=order_number)
        except Order.DoesNotExist:
            return response.Response(
                status=404,
                data=_('Order doesn\'t exist')
            )

        slots = Slots.objects.filter(
            customer=customer,
            city_id=order.city_id,
            hub_id__in=[order.hub_id, order.pickup_hub_id]
        ).values(
            'autodispatch_drivers__pk',
            'autodispatch_drivers__name',
            'autodispatch_drivers__driver_vehicle'
        ).order_by(
            'autodispatch_drivers__name'
        )
        return response.Response(status=200, data=slots)
