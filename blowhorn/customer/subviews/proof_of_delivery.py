import json
from rest_framework import views, filters, response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.contract.constants import TRIP_DATA_PROOF
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.order.serializers.order_line import OrderLineSerializer
from blowhorn.order.models import OrderLine, OrderDocument
from blowhorn.trip.models import Stop, TripDocument, Trip
from blowhorn.trip.redis_models import Trip as RedisTrip


class TripProofOfDeliveryView(views.APIView):
    """
    GET fleet/trip/<trip_pk>/pod/
    Returns list of document details at stop level for given trip pk
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request, trip_pk=None):
        documents = self.__get_results(trip_pk, request.user)
        return response.Response(status=200, data=documents)

    def __get_results(self, trip_pk, user):
        """
        """
        redis_trip = RedisTrip(trip_pk)
        documents = redis_trip.get_documents(document_types=[TRIP_DATA_PROOF])
        if not documents:
            qs = TripDocument.objects.filter(trip_id=trip_pk)
            documents = [d.to_json() for d in qs]
            RedisTrip(trip_pk).update_documents(documents)
            documents = [d for d in documents if d.get('document_type') in [TRIP_DATA_PROOF]]

        return documents


class ProofOfDeliveryView(views.APIView):
    """
    GET fleet/trip/<trip_pk>/pod/
    Returns list of document details at stop level for given trip pk
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request, trip_pk=None):
        documents = self.__get_results(trip_pk, request.user)
        return response.Response(status=200, data=documents)

    def __get_results(self, trip_pk, user):
        """
        """
        trip = Trip.objects.get(pk=trip_pk)
        order_id = trip.order_id
        _query = {}
        if order_id:
            _query['order_id'] = order_id
        else:
            _query['stop__trip_id'] = trip_pk

        qs = OrderDocument.objects.select_related(
            'stop'
        ).filter(**_query).order_by('-id')
        return [d.to_json() for d in qs]
