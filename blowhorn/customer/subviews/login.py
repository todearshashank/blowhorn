
import json
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from phonenumbers import PhoneNumber
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from blowhorn.common import sms_templates
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.common.tasks import send_sms
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import Customer
from blowhorn.users.models import User


class SendOTPLoginView(APIView, CustomerMixin):
    """
    POST api/customer/login
    1. Fetches user using mobile number,
    2. Checks if user is associated with customer
    3. Sends OTP to mobile number
    """
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        phone_number = data.get('mobile', None)
        if phone_number:
            phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, phone_number)
            customer = Customer.objects.filter(user__phone_number=phone_number).first()
            user = customer.user if customer else None
            if not user:
                return Response(
                    status=status.HTTP_401_UNAUTHORIZED,
                    data=_('Mobile number is not registered')
                )

            # customer = self.get_customer(user)
            if not customer:
                 return Response(
                    status=status.HTTP_401_UNAUTHORIZED,
                    data=_('You\'re not a valid customer')
                )

            otp, created = User.objects.retrieve_otp(user)
            template_dict = sms_templates.user_login_otp
            message = template_dict['text'] % (user.name, otp)
            template_json = json.dumps(template_dict)
            send_sms.apply_async(args=[str(user.phone_number), message, template_json])
            return Response(
                status=status.HTTP_200_OK,
                data=_('OTP has been sent to your mobile number')
            )

        return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Invalid mobile number'))
