import pytz
import itertools
import logging
from datetime import datetime
from django.conf import settings
from django.db import transaction
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework import views
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication

from blowhorn.address.models import Hub, City
from blowhorn.apps.operation_app.v5.constants import CREATED
from blowhorn.apps.operation_app.v5.services.notifications import SendNotification
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_ONDEMAND
from blowhorn.contract.models import Contract
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.contract.utils import SubscriptionService
from blowhorn.order.models import OrderBatch, OrderRequest
from blowhorn.trip.models import Trip
from blowhorn.vehicle.models import VehicleClass
from blowhorn.customer.serializers import FleetTripListSerializer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)



class OndemandOrderContractDataView(views.APIView, CustomerMixin):
    """
    POST fleet/order/request/contracts
    Returns contract data
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request):
        data = request.data
        customer = CustomerMixin().get_customer(request.user)
        contract_qs = Contract.objects.filter(
            customer=customer,
            status=CONTRACT_STATUS_ACTIVE,
            contract_type=CONTRACT_TYPE_ONDEMAND
        ).values(
            'city_id',
            'city__name',
            'vehicle_classes__id',
            'vehicle_classes__commercial_classification'
        )
        if not contract_qs.exists():
            return Response(
                status=404,
                data=_('No contract found to request vehicles on demand. \
                    Please contact %s team' % settings.BRAND_NAME)
            )

        grouped_data = {}
        for city, body in itertools.groupby(
                contract_qs,
                key=lambda x: x['city__name']
            ):
            grouped_data[city] = list(body)
        return Response(status=200, data=grouped_data)


class OndemandOrderView(views.APIView, CustomerMixin):
    """
    POST fleet/order/request
    Creates order and blanket trip.
    return: trip number
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def post(self, request):
        data = request.data
        city_pk = data.get('city', None)
        hub_pk = data.get('hub', None)
        number_of_vehicles = data.get('number_of_vehicles', 1)
        requested_pickup_time = data.get('requested_time', None)
        preferred_vehicle_class = data.get('preferred_vehicle_class', None)
        customer = CustomerMixin().get_customer(request.user)

        now = timezone.now().date().strftime('%d-%m-%Y %H:%M')
        desc_batch = "On-demand Order Creation(App) %s" % now
        batch = OrderBatch.objects.create(
            description=desc_batch,
            number_of_entries=0
        )
        orders_created = 0
        error_messages = []
        order_request_list = []
        order_pk_list = []
        response_data = {}

        pickup_datetime = pytz.timezone(
            settings.IST_TIME_ZONE
        ).localize(
            datetime.strptime(requested_pickup_time, '%d-%m-%Y %H:%M')
        ).astimezone(pytz.timezone(settings.TIME_ZONE))
        if pickup_datetime < timezone.now():
            return Response(
                status=400,
                data=_('Cannot create request for past time. Please, select a future time.')
            )

        if number_of_vehicles > 5:
            return Response(
                status=400,
                data=_('Maximum allowed vehicles are 5.')
            )

        vehicle_class = VehicleClass.objects.get(pk=preferred_vehicle_class)
        hub = Hub.objects.get(pk=hub_pk)
        city = City.objects.get(pk=city_pk)
        contract = Contract.objects.filter(
            customer=customer,
            status=CONTRACT_STATUS_ACTIVE,
            contract_type=CONTRACT_TYPE_ONDEMAND,
            city=city,
            vehicle_classes__pk=preferred_vehicle_class
        ).first()
        if contract is None:
            return Response(
                status=400,
                data=_('No active contract found for %s in %s, %s. Please, contact Blowhorn team.' %
                    (vehicle_class, hub, city))
            )

        for i in range(0, number_of_vehicles):
            error_message = ''
            order_communication = None
            try:
                with transaction.atomic():
                    is_order_created, created_order = SubscriptionService().save_enterprise_order(
                        user=contract.customer.user,
                        pickup_datetime=pickup_datetime,
                        customer=contract.customer,
                        city=contract.city,
                        hub=hub,
                        driver=None,
                        order_type=settings.ONDEMAND_ORDER_TYPE,
                        created_by=request.user,
                        contract=contract,
                        invoice_driver=None,
                        blowhorn_contract=None,
                        device_type='Web',
                        batch=batch,
                        vehicle_class=vehicle_class
                    )
                    orders_created += is_order_created
                    order_pk_list.append(created_order.pk)

                logger.info('Send notification for order %s' % created_order.number)
                SendNotification(
                    CREATED,
                    request.user,
                    created_order,
                ).notify_users()
            except BaseException as e:
                is_order_created = False
                error_messages.append(str(e))

            _dict = {
                'contract_id': contract.pk,
                'driver_id': None,
                'hub_id': hub_pk,
                'order_communication_id': order_communication.id if order_communication else None,
                'pickup_datetime': pickup_datetime,
                'user': request.user,
                'is_created': True if is_order_created else False,
                'error_message': error_message
            }
            print(_dict)
            order_request_list.append(OrderRequest(**_dict))

        if orders_created:
            batch.end_time = timezone.now()
            batch.number_of_entries = orders_created
            batch.save()
        else:
            batch.delete()

        try:
            OrderRequest.objects.bulk_create(order_request_list) if order_request_list else None
        except BaseException as e:
            logger.info(e)
            return Response(status=400, data=_('Failed to create request..! Try again after sometime.'))

        if len(order_pk_list) > 0:
            response_data['trips'] = self.__get_trip_data(order_pk_list)

        response_data['error_messages'] = error_messages
        return Response(status=200, data=response_data)

    def __get_trip_data(self, order_pk_list):
        """
        Returns newly created trip in JSON format
        :param: order_pk_list: list of order ids
        """
        trips = Trip.objects.filter(
            order_id__in=order_pk_list
        ).select_related(
            'driver', 'driver__user', 'vehicle', 'vehicle__vehicle_model',
            'order', 'customer_contract'
        )
        serializer = FleetTripListSerializer(trips, many=True)
        return serializer.data
