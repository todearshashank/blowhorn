import csv
import os
from functools import reduce
import operator
from django.utils import timezone
from datetime import datetime, timedelta
from rest_framework import status, generics, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.response import Response
from django.db.models import Case, When, Value, CharField, F, Func, Q, Sum


from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.constants import WITH_BLOWHORN
from blowhorn.customer.mixins import CustomerMixin
from django_filters.rest_framework import DjangoFilterBackend

from blowhorn.customer.serializers import FleetCodPaymentsSerializer


from blowhorn.payment.constants import PAYMENT_RECEIVED_BY_ASSOCIATE, PAYMENT_HANDEDOVER_TO_CMS, \
    PAYMENT_COLLECTED_FROM_CUSTOMER
from blowhorn.payment.models import OrderCODPayment
from blowhorn.utils.datetime_function import DateTime

from blowhorn.utils.functions import utc_to_ist


class FleetCodPaymentsListView(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = FleetCodPaymentsSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('order__number', 'trip__trip_number', 'order__hub__name', 'amount', 'created_date',
                       'is_settled_to_customer', 'status', 'order__driver__name', 'order__driver__driver_vehicle',
                       'order__payment_mode')

    search_fields = ('order__number', 'trip__trip_number', 'payment_bank_txn__txn_id', 'order__driver__name',
                     'order__driver__driver_vehicle', 'utr', 'order__payment_mode')
    filterset_fields = ('status',)

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        start = self.request.GET.get('start', None)
        end = self.request.GET.get('end', None)
        quick_filter_date_delta = self.request.GET.get('quick_filter', None) or 0
        query = [Q(order__customer=customer)]

        if not start and not end:
            end = timezone.now().date()
            start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))

        if start and end:
            query.append(
                Q(created_date__date__gte=start)
                & Q(created_date__date__lte=end)
            )

        query = reduce(operator.and_, query)
        return OrderCODPayment.objects.filter(query).select_related(
            'order', 'order__customer', 'order__driver', 'order__hub', 'trip')


class FleetCodPaymentsSummaryStatusListView(APIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        customer = self.get_customer(request.user)
        start = request.GET.get('start', None)
        end = request.GET.get('end', None)
        search = request.GET.get('search', None)
        quick_filter_date_delta = request.GET.get('quick_filter', None) or 0
        query = [Q(order__customer=customer)]
        if not start and not end:
            end = timezone.now().date()
            start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))

        if start and end:
            query.append(
                Q(created_date__date__gte=start)
                & Q(created_date__date__lte=end)
            )
        if search:
            query.append(Q(order__number=search) |
                         Q(trip__trip_number=search) |
                         Q(payment_bank_txn__txn_id=search))

        query = reduce(operator.and_, query)
        result = OrderCODPayment.objects.filter(query).values('status').annotate(count=Sum('amount'))
        final = {}
        for item in result:
            status = item.get('status')
            count = item.get('count')
            if status in [PAYMENT_RECEIVED_BY_ASSOCIATE, PAYMENT_HANDEDOVER_TO_CMS, PAYMENT_COLLECTED_FROM_CUSTOMER]:
                final[WITH_BLOWHORN] = final.get(WITH_BLOWHORN, 0) + count
            else:
                final[status] = count
        return Response(final)


class FleetCodPaymentsExportStatusListView(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        tz_diff = DateTime.get_timezone_offset()
        customer = self.get_customer(request.user)
        start = request.GET.get('start', None)
        end = request.GET.get('end', None)
        status = request.GET.get('status', None)
        quick_filter_date_delta = request.GET.get('quick_filter', None) or 0
        query = [Q(order__customer=customer)]

        if not start and not end:
            end = timezone.now().date()
            start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))

        if start and end:
            query.append(
                Q(created_date__date__gte=start)
                & Q(created_date__date__lte=end)
            )

        if status:
            query.append(Q(status=status))

        query = reduce(operator.and_, query)
        queryset = OrderCODPayment.copy_data.filter(query)
        file_path = 'Order_cod_payments'

        queryset.annotate(
            creation_datetime=Func(
                F('created_date') + timedelta(hours=tz_diff),
                Value("DD/MM/YYYY HH24: MI"),
                function='to_char',
                output_field=CharField()
            ),
            settled_to_customer=Case(
                    When(is_settled_to_customer=True, then=Value('Yes')),
                    When(is_settled_to_customer=False, then=Value('No')),
                output_field=CharField()
                )
        ).to_csv(
            file_path,
            'order__number', 'trip__trip_number', 'order__driver__name', 'order__driver__driver_vehicle',
            'payment_bank_txn__txn_id', 'order__payment_mode', 'order__hub__name', 'amount', 'creation_datetime',
            'settled_to_customer', 'status', 'utr'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow([
                'Order Number', 'Trip Number', 'Driver Name', 'Driver Vehicle', 'Payment Bank Txn',
                'Payment Mode', 'Delivery Hub', 'Amount', 'Created Date', 'Is Settled To Customer', 'Status', 'UTR'
            ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response
