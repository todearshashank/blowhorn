import logging
from django.utils.translation import ugettext_lazy as _

from rest_framework.response import Response
from rest_framework.views import APIView

from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsOrgOwner
from blowhorn.customer.tasks import process_customer_data_delete_request

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# @todo move CustomerProfile api from customer/views to here

class CustomerDataRequestView(APIView, CustomerMixin):
    permission_classes = (IsOrgOwner, )

    def delete(self, request):
        """
            Soft delete customer data
            DELETE api/customer/data
        """
        logger.info('Data delete request received %s' % request.user)
        try:
            process_customer_data_delete_request.apply_async(args=[request.user.pk,])
            return Response(
                status=200,
                data=_('Your request for deleting data has been received.')
            )
        except BaseException as be:
            logger.info('Failed to update data %s' % be)
            return Response(
                status=400,
                data=_('Failed to delete your data. Please try again after sometime')
            )
