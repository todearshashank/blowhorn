import logging
from django.db.models import Q, Prefetch
from django.utils.translation import ugettext_lazy as _

from rest_framework import views, response
from rest_framework.permissions import AllowAny

from blowhorn.customer.mixins import CustomerMixin
from blowhorn.oscar.core.loading import get_model
from blowhorn.customer.subserializer.track_parcel import TrackParcelSerializer
from config.settings import status_pipelines as StatusPipeline

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TrackParcelView(views.APIView, CustomerMixin):
    permission_classes = (AllowAny,)
    serializer_class = TrackParcelSerializer

    def get_queryset(self, query):
        return Order.objects.filter(query).select_related(
            'hub', 'pickup_address', 'shipping_address', 'customer'
        ).prefetch_related(
            Prefetch(
                'event_set',
                queryset=Event.objects.select_related(
                    'driver', 'hub_associate'
                ).filter(
                    status__in=StatusPipeline.PARCEL_TRACKING_STATUSES.keys()
                ).order_by('time'),
                to_attr='events'
            ),
            Prefetch(
                'event_set',
                queryset=Event.objects.select_related(
                    'driver', 'hub_associate'
                ).filter(
                    status__in=StatusPipeline.DASHBOARD_ORDER_STATUSES
                ).order_by('time'),
                to_attr='event_pipeline'
            )
        ).first()

    def get(self, request, ref_number=None):
        user = request.user
        query = Q(reference_number=ref_number) | Q(number=ref_number)
        order = self.get_queryset(query)
        if not order:
            return response.Response(
                status=404,
                data=_('Order with number %s not found' % ref_number)
            )

        serializer = self.serializer_class(order)
        data = serializer.data
        return response.Response(status=200, data=data)
