from blowhorn.customer.models import AccountUser
import json
import operator
import logging
from functools import reduce
from django.conf import settings
from django.contrib.gis.db.models import Count, OuterRef, Subquery
from django.core.serializers import serialize
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from blowhorn.oscar.core.loading import get_model
from rest_framework import views, filters, response, generics, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny

from blowhorn.address.models import City, Hub, Slots
from blowhorn.address.serializer import SlotModelSerializer, HubGeoFeatureSerializer
from blowhorn.order.admin import assign_driver
from blowhorn.common.helper import CommonHelper
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.customer.serializers import FleetShipmentOrderListSerializer
from blowhorn.customer.views import FleetShipmentOrderList
from blowhorn.trip.models import Trip
from blowhorn.vehicle.models import VehicleClass
from config.settings import status_pipelines

Order = get_model('order', 'Order')
Stop = get_model('trip', 'Stop')
Driver = get_model('driver', 'Driver')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ShipmentSlotFilterView(views.APIView, CustomerMixin):
    """
    GET fleet/slots/load-filter
    Returns data required for filter components
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request):
        user = request.user
        customer = self.get_customer(user)
        city_pks, hub_pks = None, None
        if not user.is_customer():
            account_user = AccountUser.objects.filter(user=user).only('is_admin').first()
            if account_user and not account_user.is_admin:
                permissions = self.get_module_permissions('resource_planning', self.request.user)
                if not permissions:
                    return response.Response(
                        status=200,
                        data={
                            'cities': [],
                            'hubs': [],
                            'vehicle_classes': []
                        })
                city_pks = self.get_permission_level_values(permissions, 'city')
                hub_pks = self.get_permission_level_values(permissions, 'hub')

        query = Q(customer=customer)
        subquery = [Q(is_deleted=False)]
        if city_pks:
            subquery.append(Q(city_id__in=city_pks))

        if hub_pks:
            subquery.append(Q(hub_id__in=hub_pks))

        if subquery:
            subquery = reduce(operator.or_, subquery)
            query &= subquery

        slots = Slots.objects\
            .filter(query)\
            .values('city_id', 'hub_id', 'vehicle_class_id').distinct()

        print('slots', slots)
        cities = City.objects.filter(
            id__in=list(set([i.get('city_id') for i in slots if i.get('city_id')]))
        ).values('id', 'name')
        hubs = Hub.objects.filter(
            id__in=list(set([i.get('hub_id') for i in slots if i.get('hub_id')]))
        ).values('id', 'name')
        vehicle_classes = VehicleClass.objects.filter(
            id__in=list(set([i.get('vehicle_class_id') for i in slots if i.get('vehicle_class_id')]))
        ).values('id', 'commercial_classification')
        data = {
            'cities': cities,
            'hubs': hubs,
            'vehicle_classes': vehicle_classes
        }
        return response.Response(status=200, data=data)


class ShipmentSlotView(generics.ListAPIView, CustomerMixin):
    """
    GET fleet/slots
    Returns slots for logged-in customer
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)
    serializer_class = SlotModelSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('hub_id', 'city_id')

    def __get_query(self, params, customer):
        user = self.request.user
        city_id = params.get('city_id', None)
        hub_id = params.get('hub_id', None)
        vehicle_class_id = params.get('vehicle_class_id', None)
        query = [Q(customer=customer)]
        city_pks, hub_pks = None, None
        if not user.is_customer():
            account_user = AccountUser.objects.filter(user=user).only('is_admin').first()
            if account_user and not account_user.is_admin:
                permissions = self.get_module_permissions('resource_planning', self.request.user)
                if not permissions:
                    return Q()

                city_pks = self.get_permission_level_values(permissions, 'city')
                hub_pks = self.get_permission_level_values(permissions, 'hub')

        subquery = []
        if city_pks:
            subquery.append(Q(city_id__in=city_pks))

        elif city_id:
            query.append(Q(city_id=city_id))

        if hub_pks:
            subquery.append(Q(hub_id__in=hub_pks))

        elif hub_id:
            query.append(Q(hub_id=hub_id))

        if subquery:
            subquery = reduce(operator.or_, subquery)

        if vehicle_class_id:
            query.append(Q(vehicle_class_id=vehicle_class_id))

        query = reduce(operator.and_, query)
        if subquery:
            query &= subquery
        return query

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        if not customer:
            return self.queryset.none()

        query = self.__get_query(self.request.query_params, customer)
        if settings.USE_READ_REPLICA:
            slots = Slots.objects.using('readrep1').filter(query)
        else:
            slots = Slots.objects.filter(query)

        slots = slots.select_related('city', 'hub', 'vehicle_class')\
            .prefetch_related('autodispatch_drivers')\
            .annotate(total_drivers=Count('autodispatch_drivers'))\
            .order_by('city_id')
        return slots


class ShipmentSlotDriversDetailView(views.APIView):
    """
    GET fleet/slots/<pk>/drivers
    Returns detailed driver info about a slot
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer)

    def get(self, request, pk=None):
        drivers_pk = Slots.objects\
            .filter(pk=pk)\
            .prefetch_related('autodispatch_drivers')\
            .values('autodispatch_drivers__pk')
        trip_data = Trip.objects\
            .filter(
                status__in=[status_pipelines.TRIP_IN_PROGRESS],
                driver__in=drivers_pk
            )\
            .select_related('driver')\
            .values('driver__name', 'driver__driver_vehicle', 'trip_number')

        return response.Response(status=200, data=trip_data)


class ShipmentSlotAssignDriver(views.APIView):
    """
    GET fleet/shipment/driver/assign
    Assign driver for the order
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def post(self, request):
        data = request.data
        order_number = data.get('order_number', None)
        driver_id = data.get('driver_pk', None)

        if not order_number or not driver_id:
            return response.Response(
                status=400,
                data=_('Please select a driver')
            )

        order = Order.objects.filter(number=order_number)
        if not order:
            return response.Response(
                status=400,
                data=_('The order doesn\'t exist')
            )

        driver = Driver.objects.filter(id=driver_id).first()
        if not driver:
            return response.Response(
                status=400,
                data=_('The driver id does not exist')
            )

        if order and driver:
            resp = {}
            try:
                resp = assign_driver(order, None, driver, request, return_json=True)
            except BaseException as e:
                logger.info('Failed to assign driver: %s' % e)
                pass

            if not resp.get('status'):
                message = resp.get('message') or 'Failed to assign driver. Please contact %s team.' % \
                    settings.BRAND_NAME
                return response.Response(
                    status=400,
                    data=_(message)
                )

            order_qs = FleetShipmentOrderList().get_results(Q(number=order_number), request)
            order_data = {}
            if order_qs.exists():
                serializer = FleetShipmentOrderListSerializer(order_qs, many=True)
                order_data = serializer.data
            trip = resp.get('trip')
            _dict = {
                'trip_number': trip.trip_number,
                'order': order_data[0]
            }
            return response.Response(
                status=200,
                data=_dict
            )

        return response.Response(
            status=400,
            data=_('Failed to assign driver. Please contact %s team.' % settings.BRAND_NAME)
        )


class ShipmentSlotCoverageView(views.APIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication,
        BasicAuthentication
    )
    serializer_class = SlotModelSerializer

    def get(self, request):
        """
        GET hub/coverage
        Returns coverage of a hub in GeoJson Format

        :header HTTP_API_KEY: (str)
        :param name: Name of the hub (str)
        :param latitude: Latitude of the delivery location (float)
        :param longitude: Longitude of the delivery location (float)
        """
        api_key = request.META.get('HTTP_API_KEY', None)
        user = request.user
        is_authenticated = user and user.is_authenticated
        if not api_key and not is_authenticated:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return response.Response(status=401, data=_out)

        customer = None
        if api_key:
            customer = self.get_customer(None, api_key=api_key)

        elif is_authenticated:
            customer = self.get_customer(user)

        if not customer:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return response.Response(status=401, data=_out)

        data = request.query_params or request.data
        pk = data.get('id', None)
        hub_name = data.get('hub', None)
        latitude = data.get('latitude', None)
        longitude = data.get('longitude', None)
        is_secondary_region = data.get('is_secondary_region', False)

        if not any([hub_name, latitude, longitude]):
           return response.Response(
                status=400,
                data=_('Hub Name or Latitude & Longitude required')
            )

        if latitude and not longitude or not latitude and longitude:
            return response.Response(
                        status=400,
                        data=_('Latitude or Longitude missing')
                    )

        params = {
            'customer': customer
        }
        if pk:
            params['pk'] = pk

        if hub_name:
            params['name'] = hub_name

        if latitude and longitude:
            geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)
            if is_secondary_region:
                params = {
                    'extended_coverage__contains': geopoint
                }
            else:
                params = {
                    'coverage__contains': geopoint
                }

        result = self.get_hub(params)
        if result:
            if is_secondary_region and not result.extended_coverage:
                response.Response(
                    status=status.HTTP_409_CONFLICT,
                    data=_('Secondary Coverage Polygon not defined.')
                )
            if not result.coverage:
                response.Response(
                    status=status.HTTP_409_CONFLICT,
                    data=_('Coverage Polygon not defined.')
                )

            hub_data = HubGeoFeatureSerializer(result).data
            _out = {
                'status': 'SUCCESS',
                'message': hub_data
            }
            return response.Response(
                status=200,
                data=_out
            )

        message = ''
        if hub_name:
            message = 'Hub with name "%s" doesn\'t exists' % hub_name
        else:
            message = 'Location is out of coverage area'

        _out = {
            'status': 'FAIL',
            'message': message
        }
        return response.Response(
            status=400,
            data=_out
        )

    def get_hub(self, params):
        return Hub.objects.filter(**params).first()

    def put(self, request, pk=None):
        """
        PUT fleet/slots/<pk>/coverage
        Update coverage area of given hub
        """
        data = request.data
        coverage = data.get('coverage', [])

        slot = self.get_queryset(pk=pk)
        if not slot:
            return response.Response(
                status=200,
                data=_('Slot doesn\'t exists')
            )
        slot.coverage = coverage
        slot.save()
        return response.Response(status=200, data=_('Coverage updated successfully'))

    def get_queryset(self, pk=None):
        try:
            return Slot.objects.get(pk=pk)
        except Slot.DoesNotExist:
            return None
