import logging
import operator
from django.conf import settings
from django.db import transaction, IntegrityError
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from functools import reduce
from  django_filters.rest_framework import DjangoFilterBackend
from phonenumbers import PhoneNumber
from blowhorn.oscar.core.loading import get_model
from rest_condition import Or
from rest_framework import status, generics, filters
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.constants import ORGANIZATION_MESSAGES
from blowhorn.customer.permissions import IsOrgAdmin, IsOrgOwner, ReadonlyAccess
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import AccountUser
from blowhorn.customer.serializers import OrganizationMemberSerializer
from blowhorn.users.utils import UserUtils

User = get_model('users', 'User')
Customer = get_model('customer', 'Customer')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class OrganizationMemberList(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,
                          Or(IsOrgOwner, IsOrgAdmin, ReadonlyAccess))

    serializer_class = OrganizationMemberSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('is_active', 'is_admin', 'pk')
    search_fields = ('user__email', 'user__phone_number', 'user__name')
    filterset_fields = ('groups__id', 'status')

    def get_query(self, customer):
        group_id = self.request.GET.get('group_id', None)
        query = [Q(organization=customer), Q(is_deleted=False)]
        if group_id:
            query.append(Q(groups__id=group_id))

        return reduce(operator.and_, query)

    def get_queryset(self):
        """
        Return list of members in an organization
        GET fleet/members
        """
        customer = self.get_customer(self.request.user)
        if not customer:
            return self.queryset.none()
        query = self.get_query(customer)

        if settings.USE_READ_REPLICA:
            users =  AccountUser.objects.using('readrep1').filter(query)
        else:
            users =  AccountUser.objects.filter(query)

        return users.select_related('user').prefetch_related('groups')

    def put(self, request):
        """
        Update details of member
        PUT fleet/members
        :param request:
        :return:
        """
        data = request.data
        logger.info('Updating member: %s' % data)
        pk = data.get('id', None)
        email = data.get('email', None)
        phone_number = data.get('phone_number', None)
        member_status = data.get('status', None)
        if not pk or not email:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['field']['email']['required']
                )
            )

        account_user = self.serializer_class.Meta.model.objects.get(pk=pk)
        serializer = self.serializer_class(
            account_user,
            data=data,
            context=dict(request=request, phone_number=phone_number, status=member_status)
        )
        if serializer.is_valid():
            try:
                account_instance = serializer.save()
                account = self.serializer_class(account_instance)

                if account_user.status == 'old_invited':
                    user, created = self.send_invitation(
                        '', email, phone_number, account_instance.organization,
                        request=request, resend=True)
                response_data = self.get_response_message(
                    '%s updated successfully' % email,
                    account.data
                )

                return Response(status=status.HTTP_200_OK,
                                data=response_data)

            except IntegrityError as ie:
                message = ORGANIZATION_MESSAGES['error']['integrity'].format(
                    email=email)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=self.get_response_message(message))

            except BaseException as be:
                logger.info('%s' % be)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=ORGANIZATION_MESSAGES['error']['common'])

        logger.info(serializer.errors)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=serializer.errors)


class OrganizationInviteMember(APIView, CustomerMixin, UserUtils):
    permission_classes = (IsAuthenticated, Or(IsOrgOwner, IsOrgAdmin))
    serializer_class = OrganizationMemberSerializer

    def post(self, request):
        """
        Invite member to an organization
        POST fleet/member/invite
        :param request: request with following data
            email: mandatory
            name, phone_number: optional
            is_admin: Boolean (default: False)
        :return: Success if org-user is created;
            otherwise error message will be sent
        """
        user = request.user
        data = request.data
        logger.info('Adding new member: %s' % data)
        email = data.get('email', None)
        name = data.get('name', None)
        phone_number = data.get('phone_number', None)
        is_admin = data.get('is_admin', False)
        customer = self.get_customer(user)

        if self.is_staff_email(email):
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['field']['email']['staff_email']
                )
            )

        if phone_number:
            if not self.validate_phonenumber(str(phone_number)):
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=self.get_response_message(
                        ORGANIZATION_MESSAGES['error']['field'][
                            'phone_number']['invalid'])
                )

            phone_number_obj = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, phone_number)
            if User.objects.filter(phone_number=phone_number_obj).exists():
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=self.get_response_message(
                        ORGANIZATION_MESSAGES['error']['field'][
                            'phone_number']['duplicate'])
                )

        if Customer.objects.filter(user__email=email).exists():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['integrity'].format(
                        email=email)
                )
            )

        if self.serializer_class.Meta.model.objects.filter(
                user__email=email).exists():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['integrity'].format(
                        email=email)
                )
            )

        with transaction.atomic():
            user, created = self.send_invitation(name, email, phone_number,
                                                 customer, request=request)

            try:
                extra_info = dict(is_admin=is_admin, created=created)
                account_user, created = customer.get_or_add_user(
                    user, **extra_info)

                account = self.serializer_class(account_user)
                if account_user.status == 'old_invited':
                    user, created = self.send_invitation(
                        '', email, phone_number, customer, request=request,
                        resend=True)

                response_data = self.get_response_message(
                    ORGANIZATION_MESSAGES['invitation']['success'].format(
                        email=email),
                    account.data
                )

                return Response(status=status.HTTP_200_OK,
                                data=response_data)

            except IntegrityError as ie:
                logger.info('%s' % ie)
                message = ORGANIZATION_MESSAGES['error']['integrity'].format(
                    email=email)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=self.get_response_message(message))

            except BaseException as be:
                logger.info('%s' % be)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=ORGANIZATION_MESSAGES['error']['common'])

    def put(self, request):
        """
        Resend invitation
        PUT fleet/member/invite
        """
        data = request.data
        email = data.get('email', None)
        logger.info('Resending invitation: %s' % data)

        customer = self.get_customer(request.user)
        try:
            user = self.send_invitation('', email, '', customer)
            AccountUser.objects.filter(user__email=email)\
                .update(last_invite_sent_on=timezone.now())
            return Response(
                status=status.HTTP_200_OK,
                data=self.get_response_message(
                    _('Invitation has been successfully sent to %s' % email)
                )
            )

        except BaseException as be:
            logger.info(be)
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_response_message(
                    _('Failed to send invitation to %s' % email)
                )
            )


class OrganizationDetailsView(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,
                          Or(IsOrgOwner, IsOrgAdmin, ReadonlyAccess))

    serializer_class = OrganizationMemberSerializer

    def get(self, request, pk=None):
        instance = None
        try:
            instance = AccountUser.objects.get(pk=pk)
        except AccountUser.DoesNotExist:
            return Response(
                status=404,
                data=_('User doesn\'t exists')
            )

        serializer = self.serializer_class(instance)
        return Response(status=status.HTTP_200_OK,
                                data=serializer.data)
