from rest_framework import generics, filters
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.order.serializers.order_line import OrderLineSerializer
from blowhorn.order.models import OrderLine
from blowhorn.trip.models import Stop


class OrderItemsView(generics.ListAPIView):
    """
    GET fleet/shipment/items
    Returns list of items associated with given order
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)
    serializer_class = OrderLineSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('-created_date',)

    def __get_results(self, params, user):
        """
        :param params: filter params JSON
        :param user: instance of User
        :return: queryset object
        """
        order_number = params.get('order_number')
        trip_number = params.get('trip_number')
        if order_number:
            _dict = dict(order__number__in=[order_number])
        elif trip_number:
            customer = CustomerMixin().get_customer(user)
            _dict = dict(trip__trip_number=trip_number,
                         order__customer=customer)
            order_numbers = Stop.objects.filter(**_dict).values_list(
                'order_id', flat=True)
            _dict = dict(order_id__in=order_numbers)

        order_lines = OrderLine.objects.filter(**_dict)\
            .select_related('order', 'created_by', 'modified_by', 'sku')
        return order_lines.order_by('item_sequence_no')

    def get_queryset(self):
        filter_param = self.request.query_params.dict()
        return self.__get_results(filter_param, self.request.user)
