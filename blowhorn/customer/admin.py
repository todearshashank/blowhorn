import os
import json
import csv
import pytz
import logging
import calendar
from datetime import datetime, timedelta, date
from functools import partial
from django.conf import settings
from django.core.exceptions import ValidationError
from django.contrib import messages, admin
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.humanize.templatetags.humanize import intcomma
from django.contrib.postgres.fields import JSONField
from django.contrib.admin.helpers import ActionForm
from django import forms
from django.urls import resolve
from django.db import connection
from django.utils import timezone
from array_tags import widgets
from django.db import transaction


from django.urls import reverse
from django.db import transaction
from django.db.models import Count, When, Case, Q, Prefetch, Sum, F, Min, Max, \
    IntegerField, CharField, BooleanField, ExpressionWrapper, Subquery, OuterRef, Value, FloatField
from django.db.models.functions import Coalesce
from django.db.models import Func
from django.forms.models import BaseInlineFormSet
from django.http import HttpResponseRedirect, HttpResponse, QueryDict
from django.utils.html import mark_safe, format_html
from django.utils.translation import ugettext_lazy as _

from mapwidgets.widgets import GooglePointFieldWidget
from blowhorn.oscar.core.loading import get_model
from prettyjson import PrettyJSONWidget
from import_export.admin import ExportMixin#, base_formats
from django.contrib.postgres.aggregates import StringAgg
from functools import reduce

from config.settings import status_pipelines as StatusPipeline
from blowhorn.utils.functions import minutes_to_dhms, get_in_ist_timezone, \
    format_datetime
from blowhorn.common.admin import (
    NonEditableAdmin, NonEditableTabularInlineAdmin, BaseAdmin
)
from blowhorn.common.utils import export_to_spreadsheet, TimeUtils
from blowhorn.common.fields import CustomModelChoiceField
from blowhorn.address.utils import AddressUtil
from blowhorn.driver.models import DriverEligibility, DriverStatus
from blowhorn.customer.models import (
    # Models
    Customer,
    CustomerDivision,
    CustomerAddress,
    ShipmentAlertConfiguration,
    CustomerTaxInformation,
    CustomerInvoice,
    InvoiceHeader,
    InvoiceInline,
    CustomerContact,
    CustomerOTP,
    CustomerSalesRepresentative,
    # Constants
    CUSTOMER_CATEGORY_INDIVIDUAL,
    CUSTOMER_CATEGORY_TRANSITION,
    CUSTOMER_CATEGORY_NON_ENTERPRISE,
    Partner,
    PartnerCustomer,
    CustomerFeedback,
    GPSEventPublishConfig,
    AuthenticationConfig,
    BasicAuthCredential,
    CustomerHistory,
    MISDocument,
    SmsConfig, InvoiceUploadBatch, SendInvoiceBatch,
    CustomerLedgerStatement,
    CustomerBalance,
    CustomerEmailLog, CUSTOMER_CATEGORY_ENTERPRISE, VendorPayout, CUSTOMER_SOURCE_ADMIN,
    CashFreePayoutHistory, CashFreeBeneficiary)
from .forms import (
    CustomerForm, CustomerEditForm, HubFormset,
    TaxInformationInlineForm, BusinessCustomerForm,
    BusinessCustomerEditForm, ShipmentAlertFormset,
    CustomerAddressForm, CustomerContactFormSet,
    CustomerInvoiceConfigFormSet, InvoiceAdjustmentCategoryForm,
    InvoiceRequestForm, ContractInvoiceFragmentForm,
    CustomerReceivableFormSet, CustomerInvoiceForm,
    CustomerInvoiceAdjustmentFormSet, InvoiceAdjustmentCategoryEditForm,
    ReceivablelineForm, CustomerActionForm, DriverRatingAggregateForm,
    RatingConfigForm, InvoicePaymentFormset, InvoicePaymentForm,
    SmsConfigForm, C2CCustomerActionForm, CustomerInvoiceConfigForm, CustomerInvoiceConfigEditForm
)
from .resource import BusinessCustomerResource, InvoiceResource, \
    ReceivableResource
from blowhorn.driver.models import Driver, DriverRatingAggregate, RatingConfig, \
    RatingCategory, DriverRatingDetails
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from blowhorn.utils.datetimerangefilter import DateRangeFilter, DateFilter
from blowhorn.customer.utils import CustomMonthFilter, get_revenue_book_on, \
    CustomYesNoFilter, AddBalanceFilter, getfiles, is_valid_gstin, CreditBreach
from blowhorn.contract.constants import CONTRACT_STATUS_ACTIVE, EXEMPT
from blowhorn.customer.constants import CREATED, UNAPPROVED, CUSTOMER_DISPUTED, \
    CUSTOMER_CONFIRMED, INVOICE_TRIPS_DATA_TO_BE_EXPORTED, UPLOADED_TO_GST_PORTAL, \
    INVOICE_ORDER_DATA_TO_BE_EXPORTED, TAX_INVOICE, INVOICE_CONFIG_ACTIVE, \
    REJECTED, SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED, APPROVED, CREDIT_NOTE, \
    PAYMENT_DONE, PARTIALLY_PAID, SENT_FOR_APPROVAL, ACCOUNTING_INVOICE_STATUS, ACCOUNTING_STATUS_UNBILLED, \
    ACCOUNTING_STATUS_BILLED, INVOICE_CONFIG_ACTIVE, INVOICE_STATUSES_FOR_GSTIN_WARNING, \
    BUSINESS_CUSTOMER_CONTACT_EDITABLE_GROUPS
from blowhorn.utils.functions import utc_to_ist
from blowhorn.customer.submodels.invoice_models import (
    InvoiceEntity, ContractInvoiceFragment, InvoiceAggregateAttribute,
    InvoiceBatch, InvoiceAggregation, CustomerInvoiceAdjustment,
    InvoiceCreationRequest, InvoiceAdjustmentCategory, CustomerInvoiceConfig,
    InvoiceTrip, InvoiceOrder, InvoiceRequest, CustomerReceivable,
    ReceivablelineItem, CreditDebitNote,
    InvoiceHistory, InvoiceSupportingDocument,
    calculation_formatter,
    create_invoice_history_bulk, InvoicePayment, InvoicePaymentHistory, EInvoicing, EInvoicingEvent,
)
from blowhorn.utils.admin_list_filter_title import \
    list_related_only_filter_title
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.tasks import send_invitation
from blowhorn.customer.invoice_transition_conditions import \
    is_city_sales_manager, is_collection_representative
from blowhorn.users.models import User
from blowhorn.apps.operation_app.v5.tasks import send_invoice_action_notification
from blowhorn.customer import constants as customer_constants
from blowhorn.utils.datetime_function import DateTime

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Contract = get_model('contract', 'Contract')
Hub = get_model('address', 'Hub')
# Email = get_model('customer', 'Email')
City = get_model('address', 'City')
CustomerDeliveryOption = get_model('customer', 'CustomerDeliveryOption')
# CommunicationEventType = get_model('customer', 'CommunicationEventType')
CustomerReportSchedule = get_model('customer', 'CustomerReportSchedule')
UserReport = get_model('customer','UserReport')
Report = get_model('customer', 'Report')

RATING_MAX_CONST = 5
BUSINESS_CUSTOMER_CHANGE_URL = "admin:customer_businesscustomer_change"

VERIFICATION_PENDING = 'Verification Pending'
VERIFIED = 'Verified'

COLOR_CODE_SENT = 'green'
COLOR_CODE_DATAREADY = 'red'

DATETIME_FORMAT = settings.ADMIN_DATETIME_FORMAT

INVOICE_EDITABLE_STATUS = [CREATED, UNAPPROVED, REJECTED]

tz_diff = DateTime.get_timezone_offset()

def generate_reports(modeladmin, request, queryset):
    ids = queryset.values_list('id', flat=True)
    from blowhorn.report.services.scheduled_reports import ReportService
    ReportService().generate_subscribed_reports(ids=ids)

def send_reports(modeladmin, request, queryset):
    ids = queryset.values_list('id', flat=True)
    from blowhorn.report.services.scheduled_reports import ReportService
    ReportService().send_customer_reports(ids=ids)

generate_reports.short_description = _('Generate Reports')
send_reports.short_description = _('Send Reports')

class CustomerReportScheduleAdmin(admin.ModelAdmin):
    model = CustomerReportSchedule

    list_display = ('customer', 'report_type', 'period','service_schedule', 'city')
    fields = ['customer', 'report_type', 'service_schedule', 'city', 'subscribed_users', 'period']
    readonly_fields = ['customer', 'report_type', 'service_schedule', 'city', 'subscribed_users','period']
    search_fields = ['customer__name', 'city']
    actions = [generate_reports,]

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class ReportAdmin(admin.ModelAdmin):
    model = Report

    list_display = ('name', 'category', 'code', 'is_active',
                        'description',  'display_inline', 'system',
                        'width',)
    fields = ['name', 'code', 'category', 'description',
                 'display_inline', 'is_active', 'width', 'system']

    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_update_permission(self, request):
        return request.user.is_superuser

class UserReportAdmin(admin.ModelAdmin):
    model = UserReport

    list_display = ('customer', 'report_type', 'period', 'city',
                        'start_date', 'end_date', 'file', 'send_status')
    fields = ['customer', 'report_type', 'period', 'city',
                    'start_date', 'end_date', 'file', 'send_status']
    readonly_fields = fields
    actions = [send_reports,]
    list_filter = ['report_type', 'send_status']

    def has_add_permission(self, request):
        return False

    search_fields = ['customer__name']

class CustomerDeliveryOptionAdmin(admin.ModelAdmin):
    model = CustomerDeliveryOption
    list_display = ('customer','status','option','is_active')
    list_filter = [('customer',admin.RelatedOnlyFieldListFilter),
                    'status']

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_update_permission(self, request):
        return request.user.is_superuser

class CustomerHistoryInline(admin.TabularInline):
    model = CustomerHistory
    readonly_fields = ['verbose_name', 'old_value', 'new_value', 'user', 'modified_time']
    fields = ['verbose_name', 'old_value', 'new_value', 'user', 'modified_time']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class InvoiceAdjustmentCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'hsn_code', 'is_tax_applicable', 'status', 'created_date', 'created_by', )
    form = InvoiceAdjustmentCategoryForm
    search_fields = ['description']
    list_filter = ['name', 'status', 'is_tax_applicable']

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        if request.user:
            return request.user.is_superuser
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['id', 'name', 'description', 'is_tax_applicable', 'created_date', 'created_by']
        return []

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = InvoiceAdjustmentCategoryEditForm
        else:
            self.form = InvoiceAdjustmentCategoryForm
        return super().get_form(request, obj, **kwargs)

@admin.register(DriverRatingDetails)
class DriverRatingDetailsAdmin(admin.ModelAdmin):
    list_display = ('id', 'driver', 'order', 'get_pickupdate',
                    'get_customer', 'rating', 'remark', 'customer_remark', )
    list_display_links = None

    list_filter = [('order__pickup_datetime', DateRangeFilter), 'rating']

    actions = None

    def get_customer(self, obj):
        return obj.user.name

    get_customer.short_description = 'Customer'

    def get_pickupdate(self, obj):
        return obj.order.pickup_datetime

    get_pickupdate.short_description = 'Service Time'
    get_pickupdate.admin_order_field = 'order__pickup_datetime'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('order')
        qs = qs.select_related('driver')
        qs = qs.select_related('user')
        qs = qs.select_related('remark')
        return qs

    search_fields = ['order__number', 'driver__name', 'driver__vehicles__registration_certificate_number']


class RatingConfigAdmin(admin.ModelAdmin):

    list_display = ('rating', 'created_date', 'created_by', 'modified_date', 'modified_by')

    fields = ('rating', 'title')

    form = RatingConfigForm

    def has_add_permission(self, request):
        if request.user:
            return request.user.is_superuser
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user:
            return request.user.is_superuser
        return False


class RatingCategoryAdmin(admin.ModelAdmin):

    list_display = ('remark', 'created_date', 'created_by', 'modified_date', 'modified_by')

    fields = ('remark', )

    def has_add_permission(self, request):
        if request.user:
            return request.user.is_superuser
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user:
            return request.user.is_superuser
        return False


class DriverRatingAggregateAdmin(admin.ModelAdmin):

    list_filter = ('order_type',)

    list_display = ('rating', 'order_type', 'created_date', 'created_by', 'modified_date', 'modified_by')

    fields = ('rating', 'remarks', 'order_type')

    form = DriverRatingAggregateForm

    def has_add_permission(self, request):
        if request.user:
            return request.user.is_superuser
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user:
            return request.user.is_superuser
        return False


class ShipmentAlertConfigurationInline(admin.TabularInline):
    extra = 0
    model = ShipmentAlertConfiguration
    formset = ShipmentAlertFormset

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer')
        return qs.order_by('time_since_creation')


class CustomerContactInline(admin.TabularInline):
    extra = 0
    model = CustomerContact
    formset = CustomerContactFormSet
    fields = ('name', 'mobile', 'email', 'division', 'city', 'customer', 'role', 'remarks')

    def get_readonly_fields(self, request, obj=None):
        return ['name', 'mobile', 'email', 'division', 'city', 'customer', 'role', 'remarks']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return request.user.is_sales_head

    def get_formset(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield,
                                               request=request, obj=obj)
        return super().get_formset(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        customer = kwargs.pop('obj', None)
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "division":
            ''' Show divisions for this customer only '''
            formfield.queryset = CustomerDivision.objects.filter(
                customer=customer)
        return formfield

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer')
        qs = qs.prefetch_related('division')
        return qs


class AddCustomerContactInline(admin.TabularInline):
    extra = 0
    model = CustomerContact
    formset = CustomerContactFormSet
    verbose_name_plural = 'Add Customer Contacts'

    def has_change_permission(self, request, obj=None):
        user = request.user
        return any(item in BUSINESS_CUSTOMER_CONTACT_EDITABLE_GROUPS for item in
                   list(user.groups.values_list('name', flat=True))) or user.is_sales_head


    def has_view_permission(self, request, obj=None):
        user = request.user
        return any(item in BUSINESS_CUSTOMER_CONTACT_EDITABLE_GROUPS for item in
                   list(user.groups.values_list('name', flat=True))) or user.is_sales_head

    def has_delete_permission(self, request, obj=None):
        return request.user.is_sales_head

    def get_formset(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield,
                                               request=request, obj=obj)
        return super().get_formset(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        customer = kwargs.pop('obj', None)
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "division":
            ''' Show divisions for this customer only '''
            formfield.queryset = CustomerDivision.objects.filter(
                customer=customer)
        return formfield

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer')
        qs = qs.prefetch_related('division')
        return qs


class CustomerOTPInline(admin.TabularInline):
    verbose_name_plural = 'OTP'
    extra = 0
    model = CustomerOTP
    fields = ('otp', 'sent_at', 'remarks',)
    readonly_fields = ('otp', 'sent_at', 'remarks',)

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.order_by('-sent_at')
        return qs


class EInvoicingEventAdmin(admin.TabularInline):
    verbose_name_plural = 'Event'
    extra = 0
    model = EInvoicingEvent
    fields = ('created_time', 'status', 'file', 'response',)
    readonly_fields = fields

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CashFreeBeneficiaryAdmin(admin.ModelAdmin):
    verbose_name_plural = 'CashFree Beneficiary'
    model = CashFreeBeneficiary
    fields = ('customer', 'bene_id', 'bank_account', 'ifsc_code', 'created_at')
    list_filter = ('customer__bank_account', 'customer__name')
    search_fields = ('customer__bank_account',)
    list_display = ['bene_id', 'customer', 'bank_account', 'ifsc_code','created_at']
    readonly_fields = fields

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CashFreePayoutHistoryAdmin(admin.ModelAdmin):
    verbose_name_plural = 'CashFree Payout History'
    model = CashFreePayoutHistory
    fields = ('customer', 'referenceId', 'beneId', 'transferId', 'amount', 'bank_account')
    list_filter = ('beneId', 'customer', 'transferId', 'referenceId')
    search_fields = ('transferId', 'beneId')
    list_display = ['beneId', 'customer', 'transferId', 'referenceId', 'amount', 'bank_account', 'response_status',
                    'created_at', 'remarks']
    readonly_fields = fields

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class HubAdminInline(admin.TabularInline):
    model = Hub
    extra = 0
    fields = ('name', 'city', 'address')

    """below code was added for performance improvement of business customer admin panel
    reference - https://blog.ionelmc.ro/2012/01/19/tweaks-for-making-django-admin-faster/"""

    def formfield_for_dbfield(self, db_field, **kwargs):
        customer = kwargs.pop('obj', None)
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "address":
            formfield.choices = formfield.choices
        if db_field.name == "city":
            formfield.choices = formfield.choices
        return formfield

    # def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
    #     if db_field.name == 'address':
    #         qs = CustomerAddress.objects.filter(customer=self.parent_obj) \
    #             .select_related('country')
    #         kwargs['queryset'] = qs
    #     return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.is_readonly:
            return self.fields
        return []

    def get_formset(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        if obj and not obj.is_readonly:
            self.formset = HubFormset
        return super().get_formset(request, obj, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('city', )
        qs = qs.select_related('address', 'address__country')
        return qs


def merge_customer(modeladmin, request, queryset):
    print('Merging customers..', request.POST)
    customer_id = request.POST.get('customer')
    if not customer_id:
        messages.error(request, 'Select a customer')
        return False

    merging_customers_list = queryset.values('pk', 'user__pk', 'user__email')
    print('Customers list: ', merging_customers_list)

    conflicting_pk = [x.get('user__email') for x in merging_customers_list if
                      int(x.get('pk')) == int(customer_id)]
    if not conflicting_pk:
        successful_migration, failed_tax_info = CustomerMixin().merge_customer_data(
            customer_id, merging_customers_list)

        if successful_migration:
            message = 'No. of customers migrated: %s' % successful_migration
            messages.success(request, message)
            messages.success(request, '')

        if failed_tax_info:
            message = 'List of failed tax detail update: %s' % failed_tax_info
            messages.error(request, message)

        if successful_migration or failed_tax_info:
            messages.info(request, 'Check Customer Migration History for detail')

            send_invitation.apply_async(
                args=[customer_id, list(merging_customers_list), failed_tax_info])

    else:
        message = 'Conflicting: %s. The target customer should not be in checked list' % \
                  conflicting_pk[0]
        messages.error(request, message)


def send_credit_breach_mail_alert(modeladmin, request, queryset):
    ids = list(queryset.values_list('id', flat=True))
    last_sent_email_qs = CustomerEmailLog.objects.filter(customer__id__in=ids,
                                                         email_sent_at__date=timezone.now().date())
    if last_sent_email_qs.exists():
        messages.error(request, 'Email has already been sent to %s today' % list(
            last_sent_email_qs.values_list('customer__name', flat=True).distinct()))
        return
    CreditBreach().create_and_mail_customer_ledger(customers_list=ids)
    messages.info(request, 'Sending alerts to the Customer(s)')


class CustomerAdmin(admin.ModelAdmin):
    save_on_top = True
    list_filter = ('customer_category',)
    search_fields = ['id', 'name', 'user__email', 'user__phone_number', ]
    list_display = ('id', 'name', 'getPhoneNumber', 'getUser',
                    'customer_category', 'getMobileStatus', 'getEmailStatus',
                    'getCustomerRating', 'getCustomerCount', 'getCreationDate')

    action_form = C2CCustomerActionForm
    actions = [merge_customer]
    inlines = [CustomerOTPInline]
    fields = (
        'customer_category', 'name', 'email', 'service_tax_category',
        'phone_number', 'legalname', 'password', 'current_address',
        'last_otp_value', 'last_otp_sent_at', 'source')
    readonly_fields = ('api_key', 'customer_rating', 'created_date',
                       'created_by', 'modified_date', 'modified_by',
                       'last_otp_value', 'last_otp_sent_at', 'source')
    exclude = ['is_shipment_servicetype',
               'api_key', 'expected_delivery_hours', ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('user')
        qs = qs.select_related('customer_rating')
        return qs

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if obj:
            self.form = CustomerEditForm
        else:
            self.form = CustomerForm
        return super(CustomerAdmin, self).get_form(request, obj, **kwargs)

    def get_actions(self, request):
        """
        Remove `Delete` and `Merge Customer` action for non-superusers
        """
        actions = super().get_actions(request)

        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']

        # if 'merge_customer' in actions:
        #     del actions['merge_customer']
        return actions

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            last_otp_value = None
            last_otp_sent_at = None
            if change:
                existing_customer = Customer.objects.filter(pk=obj.id).first()
                last_otp_value = obj.user.last_otp_value
                last_otp_sent_at = obj.user.last_otp_sent_at
            else:
                obj.source = CUSTOMER_SOURCE_ADMIN
                existing_customer = None
            if not change:
                is_mobile_verified = False
                obj.source = CUSTOMER_SOURCE_ADMIN
            else:
                is_mobile_verified = None

            obj.user = Customer.objects.upsert_user(
                email=data.get('email'),
                name=data.get('name'),
                phone_number=data.get('phone_number'),
                password=data.get('password', None),
                is_mobile_verified=is_mobile_verified,
                user_id=existing_customer.user.id if existing_customer else None,
                last_otp_value=last_otp_value,
                last_otp_sent_at=last_otp_sent_at
            )

            super().save_model(request, obj, form, change)

    # def getName(self, obj):
    #     if obj.user:
    #         return obj.user.name
    #     return None

    def getUser(self, obj):
        if obj.user:
            return obj.user.email
        return None

    def last_otp_value(self, obj):
        if obj.user:
            return obj.user.last_otp_value
        return None

    def last_otp_sent_at(self, obj):
        if obj.user and obj.user.last_otp_sent_at:
            date_ = (utc_to_ist(obj.user.last_otp_sent_at)
                     ).strftime('%d %b %Y %H:%M')
            return date_
        return None

    def getPhoneNumber(self, obj):
        if obj.user:
            return obj.user.phone_number
        return None

    def getMobileStatus(self, obj):
        if obj.user:
            if not obj.user.is_mobile_verified:
                return VERIFICATION_PENDING
            else:
                return VERIFIED
        return None

    def getEmailStatus(self, obj):
        if obj.user:
            if not obj.user.is_email_verified:
                return VERIFICATION_PENDING
            else:
                return VERIFIED
        return None

    def getCustomerRating(self, obj):
        if obj.user:
            if obj.customer_rating:
                return str(obj.customer_rating.cumulative_rating) + '/ ' + \
                       str(RATING_MAX_CONST)
            else:
                return str(0)

        return None

    def getCustomerCount(self, obj):
        if obj.user:
            if obj.customer_rating:
                return obj.customer_rating.num_of_feedbacks
            else:
                return str(0)

        return None

    def getCreationDate(self, obj):
        if obj.user:
            return obj.user.date_joined
        return None

    # on edit of other than individual type customer
    # redirect to business customer change view
    def response_change(self, request, obj, post_url_continue=None):
        if '_continue' not in request.POST and \
            '_addanother' not in request.POST and \
            obj.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL:
            return HttpResponseRedirect(reverse(
                BUSINESS_CUSTOMER_CHANGE_URL, args=[obj.id]))
        else:
            return super(CustomerAdmin, self).response_change(request, obj)

    # on addition of customer other than individual type
    # redirect to business customer change view
    def response_add(self, request, obj, post_url_continue=None):
        if '_continue' not in request.POST and \
            '_addanother' not in request.POST and \
            obj.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL:
            return HttpResponseRedirect(reverse(BUSINESS_CUSTOMER_CHANGE_URL,
                                                args=[obj.id]))
        else:
            return super(CustomerAdmin, self).response_add(request, obj,
                                                           post_url_continue)

    # name.short_description = "Name / Brand"
    # getName.admin_customer_field = 'name'
    getUser.short_description = "Email"
    getUser.admin_customer_field = 'Email'
    getPhoneNumber.short_description = "Mobile"
    getPhoneNumber.admin_customer_field = 'user.phone_number'
    getMobileStatus.short_description = 'Mobile Verification Status'
    getMobileStatus.admin_customer_field = 'user.is_mobile_verified'
    getEmailStatus.short_description = 'Email Verification Status'
    getEmailStatus.admin_customer_field = 'user.is_email_verified'
    getCustomerRating.short_description = 'Rating Value'
    getCustomerRating.admin_customer_field = \
        'customer_rating.cumulative_rating'
    getCustomerCount.short_description = 'Rating Count'
    getCustomerCount.admin_customer_field = 'customer_rating.num_of_feedbacks'
    getCreationDate.short_description = 'Creation Time'

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/admin/js/customer/migrate_customer.js'
        )


class CustomerAddressAdmin(admin.ModelAdmin):
    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'line1'},
        'GooglePointFieldWidget'
    )

    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget(
            settings=PointFieldWidgetSettings)},
    }

    form = CustomerAddressForm

    fieldsets = (
        (None, {
            'fields': ('title', 'first_name', 'last_name',
                       'line1', 'geopoint', 'line4', 'state', 'postcode',
                       'country',)
        }),
    )


class TaxInformationInline(admin.TabularInline):
    model = CustomerTaxInformation
    verbose_name = 'GST Information'
    verbose_name_plural = 'GST Information'
    extra = 0
    fields = ('state', 'gstin', 'legalname', 'invoice_address', 'division')
    form = TaxInformationInlineForm

    """below code was added for performance improvement of business customer admin panel
    reference - https://blog.ionelmc.ro/2012/01/19/tweaks-for-making-django-admin-faster/"""

    def get_formset(self, request, obj=None, **kwargs):
        kwargs['formfield_callback'] = partial(self.formfield_for_dbfield,
                                               request=request, obj=obj)
        return super().get_formset(request, obj, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        customer = kwargs.pop('obj', None)
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "invoice_address":
            formfield.choices = formfield.choices
        if db_field.name == "state":
            formfield.choices = formfield.choices
        if db_field.name == "division":
            ''' Show divisions for this customer only '''
            formfield.queryset = CustomerDivision.objects.filter(
                customer=customer)
        return formfield

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('state', 'customer', 'division')
        qs = qs.select_related('invoice_address', 'invoice_address__country')
        return qs


class DivisionInline(admin.TabularInline):
    model = CustomerDivision
    verbose_name = 'Division'
    verbose_name_plural = 'Divisions'
    extra = 0
    fields = ('name', 'created_by', 'created_date',)
    readonly_fields = ('created_by', 'created_date',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('modified_by', 'created_by', )
        return qs


class ContractsInline(admin.TabularInline):
    model = Contract
    show_change_link = False
    extra = 0
    fields = ('get_name', 'division', 'status', 'city')
    readonly_fields = ('get_name', 'division', 'status', 'city')
    ordering = ('status', 'city',)
    can_delete = False

    def get_name(self, instance):
        url = reverse('admin:contract_contract_change', args=[instance.id])
        return format_html("<a href='{url}'>" + str(instance.name) + "</a>", url=url)

    get_name.allow_tags = True
    get_name.short_description = 'Name'

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('modified_by', 'created_by', 'division')
        return qs


class BusinessCustomer(Customer):
    class Meta:
        proxy = True
        verbose_name = _('Business Customer')
        verbose_name_plural = _('Business Customers')

    def __str__(self):

        name = '%s' % self.name
        if self.legalname:
            return name + ' - ' + '%s' % self.legalname
        return name


class BusinessCustomerHubview(Customer):
    class Meta:
        proxy = True
        verbose_name = _('Business Customer Hub View')
        verbose_name_plural = _('Business Customer Hub Views')


class BusinessCustomerConfigView(Customer):
    class Meta:
        proxy = True
        verbose_name = _('Business Customer Config View')
        verbose_name_plural = _('Business Customers Config views')


class CustomerInvoiceConfigAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'customer', 'invoice_state', 'status',)
    list_filter = (('customer', admin.RelatedOnlyFieldListFilter),
                   'status')
    search_fields = ['id', 'name', 'customer__name', ]
    fields = ('name', 'invoice_state', 'customer', 'contracts', 'invoice_cycle', 'status', 'aggregates', 'extra_pages',
              'company_bank_account')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['customer']
        return []

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = CustomerInvoiceConfigEditForm
        else:
            self.form = CustomerInvoiceConfigForm
        return super(CustomerInvoiceConfigAdmin, self).get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('invoice_state',
                               'customer', 'invoice_cycle', )
        qs = qs.prefetch_related('aggregates', 'contracts')
        return qs


class CustomerInvoiceConfigInline(admin.TabularInline):
    model = CustomerInvoiceConfig
    # raw_id_fields = ('contracts',)
    extra = 0

    filter_horizontal = ['contracts']
    readonly_fields = ('name', 'invoice_state', 'invoice_cycle', 'contracts', 'status', 'aggregates', 'extra_pages', 'company_bank_account')

    def get_formset(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        if obj and not obj.is_readonly:
            self.formset = CustomerInvoiceConfigFormSet
        return super(CustomerInvoiceConfigInline, self).get_formset(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.is_readonly:
            return ('name', 'invoice_state', 'invoice_cycle', 'contracts', 'status', 'aggregates', 'extra_pages', 'company_bank_account')
        return []

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        field = super(CustomerInvoiceConfigInline, self).formfield_for_manytomany(
            db_field, request, **kwargs)

        if db_field.name == 'aggregates':
            field.queryset = field.queryset.exclude(name='vehicle')

        if db_field.name == 'contracts':
            field.queryset = field.queryset.filter(customer=self.parent_obj)

        return field

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('modified_by', 'created_by', 'invoice_state',
                               'customer', 'invoice_cycle', )
        qs = qs.prefetch_related('aggregates', 'contracts', 'contracts__supervisors', 'contracts__spocs',
                                 'contracts__payment_cycle', 'contracts__customer', 'contracts__division',
                                 'contracts__city', 'customer__customerdivision_set', )
        return qs


class BusinessCustomerAbstract(admin.ModelAdmin):
    list_display = ('id', 'name', 'legalname',
                    'customer_category',)

    list_display_links = ('id', 'name', 'legalname',)
    search_fields = ['id', 'name', 'legalname', 'customer_category']

    list_filter = ('name', 'customer_category')

    actions = None
    fieldsets = (
        ('General', {
            'fields': ('customer_category', 'name',
                       'legalname', 'entity_type',
                       'head_office_address',
                       'industry_segment', 'cin',
                       ('pan', 'tan',),
                       ('service_tax_category',),
                       'last_otp_value', 'last_otp_sent_at')

        }),
        ('Payment Terms', {
            'classes': ('collapse',),
            'fields': ('credit_period', 'credit_amount', 'advance_taken')
        }),
        ('Shipment Related', {
            'fields': (
                'is_shipment_servicetype', 'expected_delivery_hours',
                'notification_url', 'ecode')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def last_otp_value(self, obj):
        if obj.user:
            return obj.user.last_otp_value
        return None

    def last_otp_sent_at(self, obj):
        if obj.user and obj.user.last_otp_sent_at:
            date_ = (utc_to_ist(obj.user.last_otp_sent_at)
                     ).strftime('%d %b %Y %H:%M')
            return date_
        return None

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        obj.is_readonly = False
        self.customer = obj
        return super(BusinessCustomerAbstract, self).get_form(
            request, obj, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)

        qs = qs.select_related('user', 'current_address',
                               'current_address__country',
                               'head_office_address',
                               'head_office_address__country', )

        qs = qs.prefetch_related('cities', )
        qs = qs.select_related('created_by', 'modified_by', 'current_address',
                               'user__customer', )
        return qs

    def get_readonly_fields(self, request, obj=None):
        if obj:
            fields = ['last_otp_value', 'last_otp_sent_at'] + [f.name for f in
                                                             self.model._meta.fields]
            fields.remove('expected_delivery_hours')
            return fields
        return []

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class BusinessCustomerConfigViewAdmin(BusinessCustomerAbstract):
    inlines = [CustomerInvoiceConfigInline]

    class Media:
        css = {'all': ('/static/admin/css/configview.css',)}


class BusinessCustomerHubViewAdmin(BusinessCustomerAbstract):
    inlines = [HubAdminInline]


class CustomerSalesRepresentativeInline(admin.TabularInline):

    model = CustomerSalesRepresentative
    verbose_name = 'Customer Sales Representatives'
    verbose_name_plural = 'Customer Sales Representatives'
    extra = 0
    fields = ('name', 'mobile', 'email', 'division',
                        'city', 'sales_representatives')

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'sales_representatives':
            sales_reps_emails = City.objects.values_list(
                                'sales_representatives__email')
            kwargs['queryset'] = User.objects.filter(email__in=sales_reps_emails,
                is_staff=True, is_active=True
            ).exclude(email__icontains='blowhorn.net')
        return super().formfield_for_manytomany(db_field, request, **kwargs)


class BusinessCustomerAdmin(admin.ModelAdmin):
    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
    }

    list_display = ('id', 'name', 'legalname', 'awb_code',
                    'customer_category', 'active_contracts')
    list_display_links = ('id', 'name', 'legalname', 'active_contracts')
    search_fields = ['id', 'name', 'legalname', 'customer_category', ]

    action_form = CustomerActionForm
    actions = [merge_customer, 'export_customer_data', send_credit_breach_mail_alert]
    list_filter = ('name', 'customer_category')

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = (
            'api_key', 'created_date', 'created_by', 'modified_date', 'get_hub_view',
            'get_config_view', 'modified_by', 'last_otp_value', 'last_otp_sent_at')

        sales_representatives = list(City.objects.filter(
            sales_representatives__isnull=False).values_list('sales_representatives__email', flat=True))
        if request.user and request.user.email not in sales_representatives:
            readonly_fields += ('credit_period', 'credit_amount', 'advance_taken')

            if not request.user.is_superuser:
                readonly_fields += ('customer_specific_info',)

        return readonly_fields

    # actions = None
    fieldsets = (
        ('General', {
            'fields': ('customer_category', 'name', 'awb_code',
                       'legalname', 'entity_type', 'email', 'phone_number',
                       'is_active', 'head_office_address',
                       'industry_segment', 'cin',
                       ('pan', 'tan',),
                       ('service_tax_category',),
                       ('get_hub_view', 'get_config_view',),
                       'last_otp_value', 'last_otp_sent_at',
                       'customer_specific_info', 'is_gstin_mandatory',
                       'show_pod_in_dashboard', 'has_call_masking_pin')
        }),
        ('Credit Terms', {
            'classes': ('collapse',),
            'fields': ('enterprise_credit_applicable', 'enterprise_credit_period', 'enterprise_credit_amount',
                       'credit_email_recipients')
        }),
        ('Shipment Related', {
            'fields': (
                'is_shipment_servicetype', 'expected_delivery_hours',
                'notification_url', 'ecode')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def get_inline_instances(self, request, obj=None):
        inline_instances = []

        if not obj:
            return inline_instances

        new_inlines = [
            TaxInformationInline, ContractsInline,
            ShipmentAlertConfigurationInline, HubAdminInline, DivisionInline,
            AddCustomerContactInline, CustomerContactInline,
            CustomerOTPInline, CustomerHistoryInline, CustomerSalesRepresentativeInline
        ]

        if obj and obj.contract_set.all().count() > 1000:
            new_inlines.remove(ContractsInline)

        for inline_class in new_inlines:
            inline = inline_class(self.model, self.admin_site)
            inline_instances.append(inline)

        return inline_instances

    def get_actions(self, request):
        """
        Remove `Delete` and `Merge Customer` action for non-superusers
        """
        actions = super().get_actions(request)

        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions


    def get_hub_view(self, obj):
        if obj:
            url = reverse(
                'admin:customer_businesscustomerhubview_change', args=[obj.id])

            return format_html(
                "<a href='{}'><input type='button' value='{}'></a>", url,
                'Hub View')
        return None

    def get_config_view(self, obj):
        if obj:
            url = reverse(
                'admin:customer_businesscustomerconfigview_change',
                args=[obj.id])

            return format_html(
                "<a href='{}'><input type='button' value='{}'></a>", url,
                'Config View')
        return None

    def active_contracts(self, obj):
        if obj.active_contracts > 0:
            query_dictionary = QueryDict('', mutable=True)
            query_dictionary.update(
                {
                    'customer__id__exact': str(obj.id),
                    'status': CONTRACT_STATUS_ACTIVE
                }
            )

            url = '{base_url}?{querystring}'.format(
                base_url=reverse('admin:contract_contract_changelist'),
                querystring=query_dictionary.urlencode()
            )

            return format_html("<a href='{url}'>" + str(obj.active_contracts) + "</a>", url=url)
        return format_html(
            "<a href='#' style='pointer-events: none;cursor: default;color: #4E5454;'>" + str(
                obj.active_contracts) + "</a>")

    active_contracts.short_description = 'Active Contracts'

    def last_otp_value(self, obj):
        if obj.user:
            return obj.user.last_otp_value
        return None

    def last_otp_sent_at(self, obj):
        if obj.user and obj.user.last_otp_sent_at:
            date_ = (utc_to_ist(obj.user.last_otp_sent_at)
                     ).strftime('%d %b %Y %H:%M')
            return date_
        return None

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        qs = qs.annotate(active_contracts=Count(Case(
            When(contract__status=CONTRACT_STATUS_ACTIVE, then=1),
            output_field=models.CharField()))
        )
        qs = qs.select_related('user', 'current_address',
                               'current_address__country',
                               'head_office_address',
                               'head_office_address__country', )
        qs = qs.prefetch_related('cities', )
        qs = qs.select_related('created_by', 'modified_by',
                               'current_address', 'user__customer', )
        return qs

    def get_export_queryset(self, request):
        # Convert the queryset to list as the export function uses
        # an iterator and hence prefetch related doesn't work. Without
        # prefetch_related the number of queries explodes impacting performance
        # and function not working at all for large volumes

        qs = super().get_export_queryset(request)
        qs = qs.select_related('user', 'head_office_address')
        qs = qs.select_related('created_by', 'modified_by')
        qs = qs.prefetch_related(Prefetch(
            'customertaxinformation_set',
            queryset=CustomerTaxInformation.objects.select_related('state'))
        )

        qs = qs.prefetch_related(Prefetch('contract_set',
                                          queryset=Contract.objects.all()))

        qs = qs.prefetch_related(Prefetch('customerdivision_set',
                                          queryset=CustomerDivision.objects.all()))

        return list(qs)

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if obj:
            obj.is_readonly = True

        self.customer = obj
        if obj:
            self.form = BusinessCustomerEditForm
        else:
            self.form = BusinessCustomerForm
        return super(BusinessCustomerAdmin, self).get_form(
            request, obj, **kwargs)

    # def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
    #     print('db_field', db_field.__dict__)
    #     if db_field.name == 'head_office_address':
    #         kwargs['queryset'] = \
    #             CustomerAddress.objects.filter(
    #                 pk=self.customer.head_office_address_id)
    #     return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            last_otp_value = None
            last_otp_sent_at = None

            if change:
                existing_customer = Customer.objects.filter(pk=obj.id).first()
                is_mobile_verified = existing_customer.user.is_mobile_verified
                last_otp_value = obj.user.last_otp_value
                last_otp_sent_at = obj.user.last_otp_sent_at
            else:
                existing_customer = None
                obj.created_by = request.user
                is_mobile_verified = False
                obj.source = CUSTOMER_SOURCE_ADMIN

            user_id = existing_customer.user_id if existing_customer else None
            obj.user = Customer.objects.upsert_user(
                email=data.get('email'),
                name=data.get('name'),
                phone_number=data.get('phone_number'),
                password=data.get('password'),
                is_mobile_verified=is_mobile_verified,
                user_id=user_id,
                last_otp_value=last_otp_value,
                last_otp_sent_at=last_otp_sent_at
            )

            obj.modified_by = request.user
            obj.save()
            super().save_model(request, obj, form, change)

            try:
                if not change and obj.is_non_individual():
                    CustomerMixin().send_registration_invitation(obj)
            except BaseException as be:
                logging.info('Failed to send invite to customer %s' % obj)


    def export_customer_data(self, request, queryset):
        ids = queryset.values_list('id', flat=True)
        queryset = Customer.copy_data.filter(id__in=ids)

        file_path = 'business_customer_details'

        queryset.annotate(
            active_contracts=Count('contract', filter=Q(contract__status='Active'))
        ).to_csv(
            file_path,
            'id', 'name', 'legalname', 'customer_category', 'active_contracts'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow(['ID', 'Name', 'Legal Name', 'Customer Category', 'Active Contracts'
                        ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response

    export_customer_data.short_description = 'Export Customer Details in excel'

    class Media:
        css = {'all': ('/static/admin/css/business_customer.css',)}


class CustomerInvoiceAdjustmentAdmin(admin.TabularInline):
    model = CustomerInvoiceAdjustment
    extra = 0
    formset = CustomerInvoiceAdjustmentFormSet

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.status not in INVOICE_EDITABLE_STATUS:  # edit
            fields = ('category', 'total_amount')

            return fields
        return []

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):

        field = super(CustomerInvoiceAdjustmentAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

        if db_field.name == 'category':
            if request._obj_ is None:
                field.queryset = field.queryset.filter(status=INVOICE_CONFIG_ACTIVE, hsn_code__isnull=False)
            else:
                hsn_list = [EXEMPT, ]
                if request._obj_.invoice_type == CREDIT_NOTE:
                    if request._obj_.related_invoice:
                        temp = request._obj_.related_invoice.contract_invoices.filter(
                                    contract__hsn_code__isnull=False).first()
                        if temp:
                            hsn_list.append(temp.contract.hsn_code.service_tax_category)

                    field.queryset = field.queryset.filter(hsn_code__service_tax_category__in=hsn_list,
                                                           status=INVOICE_CONFIG_ACTIVE)

                else:
                    contract_invoices = request._obj_.contract_invoices.first()
                    if contract_invoices:
                        field.queryset = field.queryset.filter(hsn_code=contract_invoices.contract.hsn_code,
                                                               status=INVOICE_CONFIG_ACTIVE)
                    else:
                        field.queryset = field.queryset.filter(status=INVOICE_CONFIG_ACTIVE, hsn_code__isnull=False)
        return field

    def has_delete_permission(self, request, obj=None):
        if obj and obj.status not in INVOICE_EDITABLE_STATUS:
            return False
        return True


class InvoiceInlineAdmin(NonEditableTabularInlineAdmin):
    model = InvoiceInline
    extra = 0
    verbose_name = 'Slab Calculations'
    verbose_name_plural = 'Slab Calculations'

    fields = [
        'group', 'city', 'hub', 'driver', 'vehicle_class', 'attribute', 'value', 'amount', 'amount_distribution'
    ]

    readonly_fields = ('amount_distribution',)

    def amount_distribution(self, obj):
        if obj.distribution_json and isinstance(obj.distribution_json, list):
            html = """
            <div class="rTable">
            <div class="rTableRow">
            <div class="rTableHead"><strong>{basis}</strong></div>
            <div class="rTableHead"><strong>{attribute}</strong></div>
            <div class="rTableHead"><strong>Amount</strong></div>
            </div>
            """.format(
                basis="Date",
                # Hardcoded for now. Can be made dynamic in future based on slab
                # interval.
                attribute=obj.attribute
            )
            for distribution in obj.distribution_json:
                html += """
                <div class="rTableRow">
                <div class="rTableCell">{date}</div>
                <div class="rTableCell">{value}</div>
                <div class="rTableCell">{amount}</div>
                </div>
                """.format(
                    date=distribution[0],
                    value=distribution[1],
                    amount=distribution[2]
                )
            html += "</div>"
            return mark_safe(html)
        return ""

    amount_distribution.short_description = mark_safe("""
        <span style="padding-left:100px;padding-right:200px">Distribution</span>""")
    amount_distribution.allow_tags = True

    class Media:
        css = {
            'all': ('/static/admin/css/custom_table.css',)
        }


def send_invoice(modeladmin, request, queryset):
    """
    Custom Action Button for Sending invoices.
    """
    from blowhorn.utils.mail import Email

    error_list = []
    non_gstin_error_list = []
    message = ''
    invoice_list = []

    print('request.POST: ', request.POST)
    if CustomerInvoice.objects.filter(id__in=queryset).distinct(
            'customer').count() > 1:
        messages.error(
            request, _("Cannot Zip invoices of different customers")
        )
        return

    for invoice in queryset:

        if invoice.customer.is_gstin_mandatory and invoice.status not in [UPLOADED_TO_GST_PORTAL, SENT_TO_CUSTOMER]:
            error_list.append(invoice.id)
        elif not invoice.customer.is_gstin_mandatory and invoice.status not in [APPROVED, SENT_TO_CUSTOMER]:
            non_gstin_error_list.append(invoice.id)
        else:
            invoice_list.append(invoice)

    if error_list:
        message = "Cannot Send Invoices in state other than UPLOADED_TO_GST_PORTAL/SENT TO CUSTOMER for Gstin mandatory customers. " \
              "Check id %s.\n" % error_list
    if non_gstin_error_list:
        message += "Cannot Send Invoices in state other than APPROVED/SENT TO CUSTOMER for Non-Gstin customers. " \
              "Check id %s.\n" % non_gstin_error_list

    if message:
        messages.error(request, _(message))
        return

    recipients = request.POST.getlist('recipients', None) or None
    if not recipients:
        recipients = request.POST.get('email', None)

    if not recipients:
        messages.error(
            request,
            _("No email selected from Customer Contacts")
        )
        return

    cc_email = request.POST.get('cc_email', None)
    _date = queryset.aggregate(Min('service_period_start'),
                               Max('service_period_end'))
    start_date = _date.get('service_period_start__min')
    end_date = _date.get('service_period_end__max')

    invoice_history_list = []
    _file = request.FILES.get('supporting_doc')

    backup = None
    for query in queryset:

        old_status = query.status
        new_status = customer_constants.SENT_TO_CUSTOMER if query.status == customer_constants.APPROVED \
            else customer_constants.CUSTOMER_ACKNOWLEDGED
        invoice_history_list.append(InvoiceHistory(invoice_id=query.id, old_status=old_status,
                                                   new_status=new_status, created_by=request.user,
                                                   action_owner=query.current_owner))
        if _file:
            mis_data = {
                'invoice': query,
                'mis_doc': backup.mis_doc if backup else _file
            }
            backup = MISDocument.objects.create(**mis_data)

    if invoice_history_list:
        InvoiceHistory.objects.bulk_create(invoice_history_list)

    Email().send_invoice(queryset, recipients, start_date, end_date,
                         zip_invoice=True, requesting_user=request.user,
                         cc=cc_email)
    messages.success(request, _("Invoice Sent Successfully"))


send_invoice.short_description = _('Send Invoices To Customer')


def set_invoice_status(request, processed_invoices, due_date, history_data, updated_status):

    if updated_status not in [CUSTOMER_CONFIRMED, CUSTOMER_ACKNOWLEDGED]:
        return False, 0

    with transaction.atomic():
        if updated_status == CUSTOMER_ACKNOWLEDGED:
            updated_count = CustomerInvoice.objects.filter(
                invoice_number__in=processed_invoices).update(
                status=updated_status, due_date=due_date,
                assigned_date=date.today(),
                modified_by=request.user,
                modified_date=timezone.now())
        else:
            updated_count = CustomerInvoice.objects.filter(
                invoice_number__in=processed_invoices).update(
                status=updated_status, due_date=due_date,
                modified_by=request.user,
                modified_date=timezone.now())

        invoice_history_instances = create_invoice_history_bulk(history_data)
        document = request.FILES.get('proof_document', None)
        if document:
            first_record = None
            for history in invoice_history_instances:
                _dict = {
                    'invoice':history.invoice,
                    'invoice_history': history,
                    'supporting_doc': first_record.supporting_doc if first_record else document,
                    'created_by': request.user
                }
                instance = InvoiceSupportingDocument.objects.create(**_dict)
                if not first_record:
                    first_record = instance

        return True, updated_count
    return False, 0


def mark_as_customer_acknowledged(modeladmin, request, queryset):
    """
    Mark selected invoices as CUSTOMER ACKNOWLEDGED
    which are in SENT TO CUSTOMER.
    Otherwise, invalid invoice ids are displayed.
    """

    invalid_invoices = []
    processed_invoices = []
    history_data = []
    action_owners = []
    customer = None
    due_date = date.today() + timedelta(days=10)

    for invoice in queryset:
        if request.FILES and customer and customer != invoice.customer:
            messages.error(
                request, _("Cannot upload document for different customers."
                           " Choose invoices of same customer.")
            )
            return

        customer = invoice.customer
        if invoice.status == SENT_TO_CUSTOMER and \
                is_city_sales_manager(invoice, request.user):

            processed_invoices.append(invoice.invoice_number)
            action_owners.append(invoice.current_owner.email if invoice.current_owner else None)
            invoice_history = InvoiceHistory(
                old_status=SENT_TO_CUSTOMER,
                new_status=CUSTOMER_ACKNOWLEDGED,
                invoice=invoice,
                action_owner=invoice.current_owner,
                due_date=due_date,
                created_by=request.user,
                reason = invoice.invoice_number
            )
            history_data.append(invoice_history)

        else:
            invalid_invoices.append('%s' % invoice.pk)

    if invalid_invoices:
        messages.error(
            request,
            _("Failed to mark invoices as customer acknowledged: %s. \
                Either status is not SENT TO CUSTOMER  or "
              "you don't have permission to perform this action." %
              ', '.join(invalid_invoices))
        )

    if processed_invoices:
        success, updated_count = set_invoice_status(
            request, processed_invoices, due_date, history_data,
            updated_status=CUSTOMER_ACKNOWLEDGED)
        if success:
            action_owners = list(set(action_owners))
            send_invoice_action_notification.apply_async(
                (CUSTOMER_ACKNOWLEDGED, processed_invoices,
                 action_owners,
                 request.user.email,), )
            messages.success(
                request,
                _("{updated_count} invoices are marked as CUSTOMER ACKNOWLEDGED."
                " {processed_invoices}".format(
                    updated_count=updated_count,
                    processed_invoices=', '.join(processed_invoices)
                ))
            )


mark_as_customer_acknowledged.short_description = _(
    'Mark as Customer Acknowledged')

def download_invoice(modeladmin, request, queryset):
    urls = [rec.file for rec in queryset if rec.file]
    return getfiles(request, urls, file_name='Invoices')
download_invoice.short_description = _('Download as Zip')


def mark_as_customer_confirmed(modeladmin, request, queryset):
    """
    Mark selected invoices as CUSTOMER CONFIRMED
    which are in CUSTOMER ACKNOWLEDGED state.
    Otherwise, invalid invoice ids are displayed.
    """

    invalid_invoices = []
    processed_invoices = []
    history_data = []
    action_owners = []
    customer = None
    due_date = None

    for invoice in queryset:
        if request.FILES and customer and customer != invoice.customer:
            messages.error(
                request, _("Cannot upload document for different customers."
                           " Choose invoices of same customer.")
            )
            return

        customer = invoice.customer
        if invoice.status == CUSTOMER_ACKNOWLEDGED and \
                is_city_sales_manager(invoice, request.user):

            processed_invoices.append(invoice.invoice_number)
            action_owners.append(invoice.current_owner.email if invoice.current_owner else None)
            invoice_history = InvoiceHistory(
                old_status=CUSTOMER_ACKNOWLEDGED,
                new_status=CUSTOMER_CONFIRMED,
                invoice=invoice,
                action_owner=None,
                due_date=due_date,
                created_by=request.user,
                reason=invoice.invoice_number
            )
            history_data.append(invoice_history)

        else:
            invalid_invoices.append('%s' % invoice.pk)

    if invalid_invoices:
        messages.error(
            request,
            _("Failed to mark invoices as customer acknowledged: %s. \
                Either status is not CUSTOMER ACKNOWLEDGED or "
              "you don't have permission to perform this action." %
              ', '.join(invalid_invoices))
        )

    if processed_invoices:
        success, updated_count = set_invoice_status(
            request, processed_invoices, due_date, history_data,
            updated_status=CUSTOMER_CONFIRMED)
        if success:
            action_owners = list(set(action_owners))
            send_invoice_action_notification.apply_async(
                (CUSTOMER_CONFIRMED, processed_invoices,
                 action_owners,
                 request.user.email,), )
            messages.success(
                request,
                _("{updated_count} invoices are marked as CUSTOMER CONFIRMED. "
                "{processed_invoices}".format(
                    updated_count=updated_count,
                    processed_invoices=', '.join(processed_invoices))
                )
            )


mark_as_customer_confirmed.short_description = _(
    'Mark as Customer Confirmed')


def delete_invoices(modeladmin, request, queryset):
    """
    Custom Action Button for Deleting invoices.
    """

    error_list = []
    delete_list = []
    for query in queryset:
        if CustomerInvoice.objects.filter(
            batch=query.batch
        ).count() != queryset.filter(batch=query.batch).count():
            messages.error(
                request,
                _("Not all the Batch Record is selected. Check Batch %s") %
                    query.batch
            )
            break
        if query.status == CREATED:
            delete_list.append(query.id)
        elif query.status in StatusPipeline.INVOICE_END_STATUSES:
            error_list.append(query.id)

    if len(error_list):
        messages.error(
            request,
            _("Only Created Invoices can't be deleted check invoice with id %s") % (error_list)
        )
    elif len(delete_list):
        with transaction.atomic():
            InvoiceBatch.objects.filter(
                customerinvoice__in=delete_list).delete()
            CustomerInvoice.objects.filter(id__in=delete_list).delete()
            fragment = ContractInvoiceFragment.objects.filter(
                invoice__in=delete_list)
            InvoiceEntity.objects.filter(
                contract_invoice__in=fragment).delete()
            fragment.delete()

        messages.success(
            request, _("Successfully deleted invoices %s") % delete_list)


delete_invoices.short_description = _('Delete Invoices')


def export_invoice_payment(modeladmin, request, queryset):
    file_path = 'invoice_payment'

    customer_ledger_ids = queryset.values_list('id', flat=True)

    invoice_payment = InvoicePayment.copy_data.filter(
        id__in=customer_ledger_ids)

    invoice_payment = invoice_payment.select_related(
        'invoice', 'invoice__customer', 'collection').to_csv(
        file_path,
        'invoice__customer__legalname', 'invoice__customer__name',
        'invoice__invoice_number', 'amount', 'tds', 'actual_amount', 'collection__deposit_date',
        'collection', 'collection__payment_date', 'collection__amount', 'collection__description',
        'collection__bank_account__nick_name')

    modified_file = os.path.splitext(file_path)[0] + '_' + \
                    str(format_datetime(datetime.now(),
                                        settings.ADMIN_DATE_FORMAT)) + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'Customer Legal Name', 'Customer Name', 'Invoice Number',
            'Amount', 'TDS', 'Actual Amount', 'Paid on', 'Collection ID',
            'Payment Date', 'Bank Received Amount', 'UTR', 'Bank'
        ])

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + \
                                              os.path.basename(modified_file)
            os.remove(modified_file)
            return response


class ContractInvoiceFragmentInlineFormset(BaseInlineFormSet):

    def __init__(self, *args, **kwargs):
        super(ContractInvoiceFragmentInlineFormset,
              self).__init__(*args, **kwargs)
        self.can_delete = False

    def save_existing(self, form, instance, commit=True):
        super(ContractInvoiceFragmentInlineFormset, self).save_existing(
            form, instance, commit=commit)
        invoice = instance.invoice
        if invoice:
            invoice.mark_stale()


class ContractInvoiceFragmentInlineAdmin(admin.TabularInline):
    model = ContractInvoiceFragment
    formset = ContractInvoiceFragmentInlineFormset
    form = ContractInvoiceFragmentForm
    extra = 0
    verbose_name = 'Contract Invoices Fragment'
    verbose_name_plural = 'Contract Invoice Fragments'
    max_num = 0

    fields = ('days_counted', 'get_contract', 'charges_basic',
              'charges_service', 'basic_per_day',)

    def get_contract(self, obj):
        if obj.contract:
            url = reverse(
                'admin:contract_contract_change', args=[obj.contract.id])
            return format_html("<a href='{}'>{}</a>", url, str(obj.contract))
        return None

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = ['get_contract', 'charges_basic',
                            'charges_service', 'basic_per_day', 'invoice']
        if obj and obj.status not in INVOICE_EDITABLE_STATUS:
            read_only_fields += ['days_counted', ]
        return read_only_fields

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('contract')
        qs = qs.prefetch_related('contract__supervisors')
        return qs


class InvoiceHistoryAdminInline(admin.TabularInline):
    verbose_name_plural = _('Invoice History')
    model = InvoiceHistory
    extra = 0
    # min_num = 0
    exclude = ('remarks',)
    readonly_fields = ('field','old_status', 'new_status', 'action_owner', 'due_date', 'created_by', 'created_time', 'reason', 'get_blank' )

    def get_blank(self, obj=None):
        if obj:
            return obj.reason
        return ''

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('created_by', 'action_owner')
        return qs


class MISDocumentInline(admin.TabularInline):
    verbose_name_plural = "MIS Document"
    model = MISDocument
    extra = 0
    min_num = 0
    fields = ('get_mis_doc', 'file')
    readonly_fields = fields

    def file(self, obj):
        if obj.mis_doc:
            return os.path.basename(obj.mis_doc.name)
        return ''

    def get_mis_doc(self, obj):
        if obj.mis_doc:
            filename = obj.mis_doc.url
            return format_html("<a href='{url}'> View</a>", url=filename)
        return ''

    def has_add_permission(self, request):
        return False

    get_mis_doc.short_description = 'MIS Document'


class InvoiceSupportingDocumentInline(admin.TabularInline):
    verbose_name_plural = "Supporting Document"
    model = InvoiceSupportingDocument
    extra = 0
    min_num = 0

    fields = ('invoice_history', 'get_reason', 'get_supporting_doc',)

    def get_supporting_doc(self, obj):
        if obj.supporting_doc:
            filename = obj.supporting_doc.url
            return format_html("<a href='{url}'> View</a>", url=filename)
        return ''

    def get_reason(self, obj):
        if obj.reason:
            return obj.reason
        return '-'

    get_reason.short_description = 'Reason'

    get_supporting_doc.short_description = 'Supporting Document'

    readonly_fields = fields

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return False


class InvoiceTripAdminInline(admin.TabularInline):
    verbose_name_plural = "Trips"
    model = InvoiceTrip

    fields = ('line_number', 'get_trip_number', 'get_driver', 'invoice_driver', 'get_contract',
              'get_planned_start_time', 'get_actual_start_time', 'get_actual_end_time', 'get_total_distance',
              'get_total_time', 'get_mr_info', 'get_packages_info','get_trip_toll_charges', 'get_trip_parking_charges')
    extra = 0
    max_num = 15000
    can_delete = False
    line_numbering = 0

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('trip', 'invoice_driver')
        qs = qs.select_related('invoice')
        qs = qs.select_related('trip__customer_contract')
        qs = qs.select_related('trip__customer_contract__division')
        qs = qs.select_related('trip__driver')
        qs = qs.select_related('trip__order')
        qs = qs.select_related('trip__order__customer')
        qs = qs.order_by('trip__planned_start_time')
        return qs

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = ['line_number', 'get_trip_number', 'get_driver', 'get_contract', 'get_planned_start_time',
                            'get_actual_start_time', 'get_actual_end_time', 'get_total_distance',
                            'get_total_time', 'get_mr_info', 'get_packages_info', 'invoice_driver', 'get_trip_toll_charges', 'get_trip_parking_charges']

        return read_only_fields

    def get_formset(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        return super().get_formset(request, obj, **kwargs)

    def line_number(self, obj):
        self.line_numbering += 1
        return self.line_numbering

    line_number.short_description = '#'

    def get_trip_toll_charges(self, obj):
        return obj.trip.toll_charges

    get_trip_toll_charges.short_description = 'Toll'

    def get_trip_parking_charges(self, obj):
        return obj.trip.parking_charges

    get_trip_parking_charges.short_description = 'Parking'

    def get_trip_number(self, obj):
        if obj.trip:
            url = reverse(
                'admin:trip_trip_change', args=[obj.trip.id])
            return format_html("<a href='{}'>{}</a>", url, obj.trip.trip_number)
        return None

    def get_contract(self, obj):
        if obj.trip:
            url = reverse(
                'admin:contract_contract_change', args=[obj.trip.customer_contract_id])
            return format_html("<a href='{}'>{}</a>", url, obj.trip.customer_contract)
        return None

    get_contract.short_description = _('Contract')

    get_trip_number.short_description = _('Trip Number')

    def get_driver(self, obj):
        if obj.trip:
            return obj.trip.driver
        return None

    get_driver.short_description = _('Driver')

    def get_actual_start_time(self, obj):
        if obj.trip and obj.trip.actual_start_time:
            return get_in_ist_timezone(obj.trip.actual_start_time).strftime(DATETIME_FORMAT)
        return None

    get_actual_start_time.short_description = _('Trip Start')

    def get_planned_start_time(self, obj):
        if obj.trip:
            return get_in_ist_timezone(obj.trip.planned_start_time).strftime(DATETIME_FORMAT)
        return None

    get_planned_start_time.short_description = _('Scheduled At')

    def get_actual_end_time(self, obj):
        if obj.trip and obj.trip.actual_end_time:
            return get_in_ist_timezone(obj.trip.actual_end_time).strftime(DATETIME_FORMAT)
        return None

    get_actual_end_time.short_description = _('Trip End')

    def get_mr_info(self, obj):
        if obj.trip.meter_reading_end:
            return '%s to %s' % (obj.trip.meter_reading_start, obj.trip.meter_reading_end)
        return '-'

    get_mr_info.short_description = _('Meter Readings')

    def get_total_distance(self, obj):
        if obj.trip and obj.trip.total_distance is not None:
            return "%.2f" % obj.trip.total_distance
        return None

    get_total_distance.short_description = _('Total Distance (kms)')

    def get_total_time(self, obj):
        if obj.trip and obj.trip.total_time is not None:
            return minutes_to_dhms(obj.trip.total_time)
        return None

    get_total_time.short_description = _('Total Time')

    def get_packages_info(self, obj):
        if obj.trip.assignd or obj.trip.delivered:
            return {'assigned': obj.trip.assigned,
                    'delivered': obj.trip.delivered}
        return '-'

    get_packages_info.short_description = _('Packages')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'invoice_driver':
            qs = Driver.objects.filter(
                trip__invoicetrip__invoice=self.parent_obj).distinct('id')
            kwargs['queryset'] = qs
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class RelatedInvoiceExists(admin.SimpleListFilter):
    title = _('Related Invoice Exists')
    parameter_name = 'related_invoice'

    def lookups(self, request, model_admin):
        choices = ['Yes', 'No']
        return [(c, c) for c in choices]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'Yes':
                return queryset.filter(
                    invoice__in=Subquery(CustomerInvoice.objects.filter(
                        related_invoice=OuterRef('invoice')).values('pk'))
                )
            else:
                return queryset.exclude(
                    invoice__in=Subquery(CustomerInvoice.objects.filter(
                        related_invoice=OuterRef('invoice')).values('pk'))
                )
        return queryset


class InvoicePaymentHistoryInline(admin.TabularInline):
    model = InvoicePaymentHistory
    extra = 0
    readonly_fields = ['field', 'verbose_name', 'old_value', 'new_value', 'user', 'modified_time']
    can_delete = False

    verbose_name = _('Invoice Payment History')
    verbose_name_plural = _('Invoice Payment History')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class InvoicePaymentAdmin(admin.ModelAdmin):

    list_display = ('id', 'get_invoice_number', 'get_customer', 'get_amount',
                    'get_tds', 'get_debit', 'get_paid_on', 'get_actual_amount', 'collection', )

    search_fields = ['invoice__customer__name', 'invoice__invoice_number']
    list_filter = [('collection__deposit_date', DateFilter),
                   RelatedInvoiceExists]
    inlines = [InvoicePaymentHistoryInline]
    actions = [export_invoice_payment]

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['get_invoice_number', 'get_customer',
                           'get_actual_amount', 'get_debit', 'created_time',
                           'created_by', 'get_amount', 'get_tds', 'get_paid_on', 'get_debit',]

        if obj and (
            not is_collection_representative(obj,
                request.user)):
            # or obj.invoice.status == PAYMENT_DONE):  TODO: Uncomment this in future
            readonly_fields += ['get_amount', 'get_tds', 'get_paid_on', 'get_debit', ]
        return readonly_fields

    def get_form(self, request, obj=None, **kwargs):
        if obj is not None:
            self.customer = obj.invoice.customer
            self.paid_on = obj.paid_on
            self.created_date = obj.collection.created_date if obj.collection else obj.paid_on
        return super().get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Look through the collections for the same customer
        within -1 and +1 of received date
        """
        if db_field.name == 'collection':
            # Display Collection as `MODE | RECEIVED ON | AMOUNT`
            return CustomModelChoiceField(
                queryset=CustomerReceivable.objects.filter(
                    customer=self.customer,
                    status=APPROVED,
                    payment_date__range=[self.created_date - timedelta(1),
                                         self.created_date + timedelta(1)]),
                required=False,
                field_names=['payment_mode', 'deposit_date', 'amount']
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('invoice', 'invoice__customer', 'collection')
        return qs

    def get_actions(self, request):
        """
        Remove delete and add export action
        """
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def get_fieldsets(self, request, obj=None):

        if obj:

            fieldsets = (
                (None, {
                    'fields':
                        ('get_invoice_number', 'get_customer', 'get_amount', 'get_tds', 'get_paid_on',
              'get_debit', 'get_actual_amount', 'collection', )
                }),
                ('Audit Log', {
                    'fields': (
                        'created_by', 'created_time'),
                }),
            )
        else:
            fieldsets = (
                (None, {
                    'fields':
                        ('get_invoice_number', 'get_customer', 'amount', 'tds',
                         'get_paid_on', 'debit',
                         'actual_amount', 'collection', ),
                }),
            )
        return fieldsets

    def get_invoice_number(self, obj):
        if obj:
            link = reverse("admin:customer_customerinvoice_change", args=[obj.invoice.id])
            return format_html(u'<a href="%s">%s</a>' % (link, obj.invoice.invoice_number))
        return ''
    get_invoice_number.allow_tags = True

    get_invoice_number.short_description = 'Invoice Number'

    def get_customer(self, obj):
        if obj:
            return obj.invoice.customer
    get_customer.short_description = 'Customer'

    def get_paid_on(self, obj):
        if obj:
            return obj.collection.deposit_date
        return '-'

    def get_amount(self, obj):
        if obj and obj.amount:
            return round(obj.amount, 2)
        return ''

    get_amount.short_description = 'Amount'

    def get_tds(self, obj):
        if obj and obj.tds:
            return round(obj.tds, 2)
        return ''

    get_tds.short_description = 'TDS'

    def get_debit(self, obj):
        if obj and obj.debit:
            return round(obj.debit, 2)
        return ''

    get_debit.short_description = 'Debit'

    def get_actual_amount(self, obj):
        if obj and obj.actual_amount:
            return round(obj.actual_amount, 2)
        return ''

    get_actual_amount.short_description = 'Actual Amount'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class InvoicePaymentAdminInline(admin.TabularInline):
    model = InvoicePayment
    form = InvoicePaymentForm
    formset = InvoicePaymentFormset
    verbose_name = 'Payment Received'
    verbose_name_plural = 'Payment Received'
    extra = 0
    fields = ('collection', 'actual_amount', 'tds', 'debit',)
    parent_obj = None
    customer = None
    paid_on = None

    def get_formset(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        return super().get_formset(request, obj, **kwargs)

    def get_form(self, request, obj=None, **kwargs):
        if obj is not None:
            self.parent_obj = obj
        return super().get_form(self, request, obj=None, change=False, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['debit']
        if self.parent_obj and \
            not is_collection_representative(self, request.user) or obj.status == PAYMENT_DONE:
            readonly_fields += ['actual_amount', 'tds', 'paid_on', 'collection']
        return readonly_fields

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Look through the collections for the same customer
        within -1 and +1 of received date
        """
        if db_field.name == 'collection':
            qs = CustomerReceivable.objects.filter(
                    customer=self.parent_obj.customer, status=APPROVED)
            kwargs['queryset'] = qs
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    class Media:
        js = ('admin/js/customer/invoice_payment_inline.js',)


class InvoiceOrderAdminInline(admin.TabularInline):
    verbose_name_plural = "Orders"
    model = InvoiceOrder

    fields = (
    'line_number', 'get_order_number', 'get_driver', 'invoice_driver', 'get_total_distance', 'get_order_status',
    'get_updated_time', 'get_contract',)
    extra = 0
    can_delete = False
    line_numbering = 0
    max_num = 15000

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('order', 'invoice_driver')
        qs = qs.select_related('invoice')
        qs = qs.select_related('order__customer_contract')
        qs = qs.select_related('order__customer_contract__division')
        qs = qs.select_related('order__driver')
        qs = qs.select_related('order__customer')
        return qs

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = ['line_number', 'get_order_number', 'get_driver', 'get_contract', 'get_total_distance',
                            'invoice_driver', 'get_order_status', 'get_updated_time']

        # if obj and obj.status == APPROVED:
        #     read_only_fields += ['invoice_driver']

        return read_only_fields

    def get_formset(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        return super().get_formset(request, obj, **kwargs)

    def line_number(self, obj):
        self.line_numbering += 1
        return self.line_numbering

    line_number.short_description = '#'

    def get_order_number(self, obj):
        if obj.order and obj.order.order_type == settings.SHIPMENT_ORDER_TYPE:
            link = reverse(
                "admin:order_shipmentorder_change", args=[obj.order_id])
            return format_html("<a href='{}'>{}</a>", link, obj.order.number)
        return None

    def get_total_distance(self, obj):
        if obj.order and obj.order.total_distance:
            return '%.2f' % obj.order.total_distance
        return None

    get_total_distance.short_description = _('Total Distance (Kms)')

    def get_contract(self, obj):
        if obj.order:
            url = reverse(
                'admin:contract_contract_change', args=[obj.order.customer_contract_id])
            return format_html("<a href='{}'>{}</a>", url, obj.order.customer_contract)
        return None

    get_contract.short_description = _('Contract')

    get_order_number.short_description = _('Order Number')

    def get_driver(self, obj):
        if obj.order:
            return obj.order.driver
        return None

    get_driver.short_description = _('Driver')

    def get_order_status(self, obj):
        if obj.order:
            return obj.order.status
        return None

    get_order_status.short_description = _('Status')

    def get_updated_time(self, obj):
        if obj.order:
            return obj.order.updated_time
        return None

    get_updated_time.short_description = _('Updated On')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'invoice_driver':
            qs = Driver.objects.filter(
                order__invoiceorder__invoice=self.parent_obj).distinct('id')
            kwargs['queryset'] = qs
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


def get_filename(invoice):
    file = str(invoice.customer) + str(invoice)
    filename = file + '.xlsx'

    return filename


def export_invoice_trip(modeladmin, request, queryset):
    print('Queries Count at begin : %d' % len(connection.queries))
    if queryset.count() != 1:
        messages.error(
            request,
            _("You can Choose one Invoice at a time")
        )
        return False

    filename = get_filename(queryset[0])
    file_path = export_to_spreadsheet(
        queryset[0].invoicetrip_set.all().values().annotate(trip_id=F('trip__id'),
                                                            trip_number=F('trip__trip_number'),
                                                            planned_start_time=F('trip__planned_start_time'),
                                                            actual_start_time=F('trip__actual_start_time'),
                                                            actual_end_time=F('trip__actual_end_time'),
                                                            total_time=F('trip__total_time'),
                                                            total_distance=F('trip__total_distance'),
                                                            assigned=F('trip__assigned'),
                                                            delivered=F('trip__delivered'),
                                                            attempted=F('trip__attempted'),
                                                            rejected=F('trip__rejected'),
                                                            pickups=F('trip__pickups'),
                                                            returned=F('trip__returned'),
                                                            delivered_cash=F('trip__delivered_cash'),
                                                            delivered_mpos=F('trip__delivered_mpos'),
                                                            customer_contract=F('trip__customer_contract__name'),
                                                            invoice_driver=F('trip__invoice_driver__name'),
                                                            hub=F('trip__hub__name'),
                                                            invoice_driver_vehicle=F(
                                                                'trip__invoice_driver__driver_vehicle'),
                                                            invoice_driver_vehicle_class=F(
                                                                'trip__invoice_driver__vehicles__vehicle_model__vehicle_class__commercial_classification'),
                                                            driver=F('trip__driver__name'),
                                                            division=F('trip__customer_contract__division__name'),
                                                            time_mins=Func(F('total_time'),
                                                                           Value("FM999999990"),
                                                                           function="to_char"),
                                                            cumulative_kms=Func(Sum('total_distance'),
                                                                            template='%(expressions)s OVER (ORDER BY %(order_by)s)',
                                                                            order_by='trip_id'),
                                                            cumulative_minutes=Func(Sum('total_time'),
                                                                            template='%(expressions)s OVER (ORDER BY %(order_by)s)',
                                                                            order_by='trip_id')

                                                            ), filename, INVOICE_TRIPS_DATA_TO_BE_EXPORTED)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            logging.info(
                'This is the file path "%s" with name "%s"' % (fh, filename))

            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + \
                                              os.path.basename(
                                                  file_path)
            os.remove(file_path)

        print('Queries Count at end : %d' % len(connection.queries))

        return response


def write_to_file(queryset):
    file_path = 'invoice-export'
    queryset.select_related(
        'customer', 'batch', 'bh_source_state', 'invoice_state',
        'invoice_configs', 'related_invoice'
    ).annotate(
        paid_amount=Coalesce(
            Sum(F('invoicepayment__actual_amount')), 0),
        transaction_type=F('invoice_type'),
        total_recvd_or_receivable=F('total_amount'),
        tds_allocation=Coalesce(
            Sum(F('invoicepayment__tds')), Value(0)
        ),
        cn_allocation=Coalesce(
            Sum(F('invoicepayment__debit')), Value(0)),
        receivable_amount=Case(
            When(transaction_type=TAX_INVOICE, then=ExpressionWrapper(
                F('total_recvd_or_receivable') - F(
                    'paid_amount') - F('tds_allocation') - F('cn_allocation'),
                output_field=FloatField())),
            default=Value(None)
            )

    # ).annotate(
        # taxable_adjustment = F('taxable_amount') - (F('charges_service') + F('charges_basic')),
        # non_taxable_adjustment =Case(
        #     When(total_rcm_amount = 0, then= F('total_amount') - (F('taxable_amount') +\
        #                                      F('cgst_amount') + F('sgst_amount') + F('igst_amount'))),
        #     default=F('total_amount') - F('taxable_amount'),
        #     output_field=CharField()),
        # total_service_charge = Case(
        #     When(total_rcm_amount = 0, then=F('total_amount') - (F('cgst_amount') + F('sgst_amount') + F('igst_amount'))),
        #     default=F('total_amount'),
        #     output_field=CharField()),
        # total_with_tax=Case(
        #     When(total_rcm_amount=0, then=(F('taxable_amount') + F('cgst_amount') + F('sgst_amount') +
        #                                                         F('igst_amount'))),
        #     default=F('taxable_amount'),
        #     output_field=CharField()),
    ).to_csv(
        file_path,
         'id', 'customer__id', 'customer__name', 'customer__customer_category', 'invoice_number', 'status', 'related_invoice__invoice_number', 'revenue_booked_on',
        'invoice_type', 'invoice_date', 'service_period_start', 'service_period_end', 'bh_source_state__name', 'invoice_state__name',
        'charges_basic', 'charges_service', 'taxable_adjustment', 'non_taxable_adjustment', 'total_service_charge',
        'taxable_amount', 'cgst_amount', 'sgst_amount', 'igst_amount', 'total_amount', 'total_rcm_amount',
        'current_owner__email', 'due_date', 'paid_amount', 'tds_allocation', 'receivable_amount', 'comments','aggregation__division__name'
    )

    modified_file = '%s-' % str(os.path.splitext(file_path)[0]) + str(utc_to_ist(timezone.now()).date()) + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
           'Id', 'Customer ID', 'customer name', 'Customer Type', 'Invoice Number', 'status', 'Invoice Against', 'Revenue Date',
            'Invoice Type', 'Invoice Date', 'Service Period Start', 'service period end','Source State', 'Target State',
            'Base Amount (A)', 'Slab Charges (B)', 'Taxable Adjustment (C)', 'Non Taxable Adjustment (D)',
            'Total Service Charges (E=A+B+C+D)', 'Taxable Amount (F=A+B+C)',
            'CGST (G)', 'SGST (H)', 'IGST (I)', 'Total Payable', 'RCM Amount',
            'Current Owner', 'Due Date', 'Paid amount', 'TDS amount', 'Receivable amount', 'Comments','Customer Division'

        ])
        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        if not os.path.exists(file_path):
            return HttpResponse(400, _('File doesn\'t exists'))
        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(modified_file)
            os.remove(modified_file)
            return response

def billed_revenue(modeladmin, request, queryset):
    invalid_invoices = []
    history_data = []

    for invoice in queryset:
        if invoice.status not in ACCOUNTING_INVOICE_STATUS:
            invalid_invoices.append(str(invoice.id))
        invoice_history = InvoiceHistory(
            old_status=invoice.accounting_status or ('-') ,
            new_status=ACCOUNTING_STATUS_BILLED,
            invoice=invoice,
            created_by=request.user,
            field='accounting_status',
            reason=invoice.invoice_number
        )
        history_data.append(invoice_history)

    if invalid_invoices:
        messages.error(
            request,
            _("Invoices : %s are not in a valid status." % ','.join(invalid_invoices))
        )
        return False
    with transaction.atomic():
        queryset.filter(status__in=ACCOUNTING_INVOICE_STATUS).update(accounting_status=ACCOUNTING_STATUS_BILLED)
        messages.success(request,_("Success" ))
        invoice_history_instances = create_invoice_history_bulk(history_data)

billed_revenue.short_description = 'Mark as Billed Revenue'


def unbilled_revenue(modeladmin, request, queryset):

    invalid_invoices = []
    history_data = []

    for invoice in queryset:
        if invoice.status not in ACCOUNTING_INVOICE_STATUS:
            invalid_invoices.append(str(invoice.id))
        invoice_history = InvoiceHistory(
            old_status=invoice.accounting_status or ('-'),
            new_status=ACCOUNTING_STATUS_UNBILLED,
            invoice=invoice,
            created_by=request.user,
            field='accounting_status',
            reason=invoice.invoice_number
        )
        history_data.append(invoice_history)

    if invalid_invoices:
        messages.error(
            request,
            _("Invoices : %s are not in a valid status." % ','.join(invalid_invoices))
        )
        return False
    with transaction.atomic():
        queryset.filter(status__in=ACCOUNTING_INVOICE_STATUS).update(accounting_status=ACCOUNTING_STATUS_UNBILLED)
        messages.success(request,_("Success" ))
        invoice_history_instances = create_invoice_history_bulk(history_data)

unbilled_revenue.short_description = 'Mark as Unbilled Revenue'


def invoice_export(modeladmin, request, queryset):
    invoices = CustomerInvoice.copy_data.all()
    if not queryset.exists():
        params = request.get_full_path().split('?')
        if len(params) > 1:
            y = params[1].split('&')
            list_data = {}
            for a in y:
                if not a:
                    continue
                exp = a.split('=')
                if len(exp) >= 1:
                    if exp[0] in ['service_period_end', 'revenue_booked_on']:
                        list_data[exp[0]] = datetime.strptime('1-'+exp[1], '%d-%b-%Y')
                    elif exp[0] == 'blanket_invoice':
                        list_data['is_credit_debit_note'] = exp[1]
                        list_data['invoice_type'] = TAX_INVOICE
                    else:
                        list_data[exp[0]] = exp[1].replace('+', ' ')
            if list_data:
                invoices = invoices.filter(** list_data)
    else:
        invoices = invoices.filter(id__in=queryset)

    return write_to_file(invoices)


invoice_export.short_description = _('Export Invoice')


def export_invoice_order(modeladmin, request, queryset):
    print('Queries Count at begin : %d' % len(connection.queries))
    if queryset.count() != 1:
        messages.error(
            request,
            ("You can Choose one Invoice at a time")
        )
        return False
    date_start = TimeUtils.date_to_datetime_range(queryset[0].service_period_start)[0]
    date_end = TimeUtils.date_to_datetime_range(queryset[0].service_period_end)[1]

    start = date_start.astimezone(tz=pytz.utc)
    end = date_end.astimezone(tz=pytz.utc)

    field_names = [datadict['label'] for datadict in INVOICE_ORDER_DATA_TO_BE_EXPORTED]

    column_headers = [datadict['name'] for datadict in INVOICE_ORDER_DATA_TO_BE_EXPORTED]

    order_ids = InvoiceOrder.objects.filter(invoice=queryset[0],
                                     ).values_list('order_id', flat=True)

    filename = str(queryset[0].customer) + str(queryset[0])
    modified_file = '%s_%s.csv' % (
        utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT),
        filename)
    order_qs = Order.copy_data.filter(id__in=order_ids).filter(invoiceorder__invoice__in=queryset)

    delivered_events = Event.objects.filter(order=OuterRef('pk'),
                                            status=StatusPipeline.ORDER_DELIVERED).distinct().annotate(
        event_count=Count('id'))
    unable_to_deliver_events = Event.objects.filter(
        order=OuterRef('pk'),
        status=StatusPipeline.UNABLE_TO_DELIVER).distinct().annotate(event_count=Count('id'))
    returned_events = Event.objects.filter(
        order=OuterRef('pk'),
        status=StatusPipeline.ORDER_RETURNED).distinct().annotate(event_count=Count('id'))
    returned_to_origin_events = Event.objects.filter(
        order=OuterRef('pk'),
        status=StatusPipeline.ORDER_RETURNED_RTO).distinct().annotate(
        event_count=Count('id'))

    order = order_qs.annotate(
        order_number=F('number'),

        order_reference_number=F('reference_number'),

        cust_reference_number=F('customer_reference_number'),

        order_updated_time=Func(
            F('updated_time') + timedelta(hours=tz_diff),
            Value("DD/MM/YYYY HH24: MI"),
            function='to_char',
            output_field=CharField()
        ),

        driver_name=F('driver__name'),

        invoice_driver_name=F('invoice_driver__name'),

        hub_name=F('hub__name'),

        order_status=F('status'),

        sku_type=F('order_lines__sku__name'),

        total_count=Sum('order_lines__quantity'),

        delivered_count=Sum('order_lines__delivered'),

        delivered=Subquery(delivered_events.values('event_count'), output_field=IntegerField()),

        unable_to_deliver=Subquery(unable_to_deliver_events.values('event_count'), output_field=IntegerField()),

        returned=Subquery(returned_events.values('event_count'), output_field=IntegerField()),

        returned_to_origin=Subquery(returned_to_origin_events.values('event_count'), output_field=IntegerField()),

        pincode=F('shipping_address__postcode')

        ).to_csv(filename, *field_names)
    with open(filename, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)
        w.writerow(column_headers)
        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        os.remove(filename)

    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = \
                'inline; filename=%s' % os.path.basename(modified_file)
            os.remove(modified_file)
            print('Queries Count at end : %d' % len(connection.queries))
            return response

    # return HttpResponse(400, _('Failed to export file'))

class EmailActionForm(ActionForm):
    email = forms.CharField(required=False, widget=forms.HiddenInput(),
                            label='')
    cc_email = forms.CharField(required=False, widget=forms.HiddenInput(),
                               label='')
    proof_document = forms.FileField(required=False,
                                     label=_('Proof of Acknowledgement/Confirmation'))


class CustomerInvoiceAdmin(FSMTransitionMixin, NonEditableAdmin):
    list_display = (
        'id', 'invoice_number', 'status', 'is_manual_upload',
        'current_owner_email', 'due_date', 'assigned_date',
        'customer_name', 'get_related_invoice', 'invoice_type', 'invoice_date',
        'get_revenue_booked_on', 'service_tax_category', 'is_blanket_invoice',
        'state', 'link_to_batch_id', 'config', 'aggregate_levels',
        'aggregate_values', 'service_period_start', 'service_period_end',
        'amount_taxable', 'non_taxable_adjustment', 'taxable_adjustment', 'amount_taxes',
        'total_rcm_amount', 'get_adjustment_amount', 'amount_total', 'accounting_status',
        'amount_credit', 'file'
    )
    form = CustomerInvoiceForm

    # date_hierarchy = 'service_period_end'

    formfield_overrides = {
        JSONField: {'widget': PrettyJSONWidget}
    }
    action_form = EmailActionForm
    actions = [delete_invoices, export_invoice_trip, send_invoice,
               export_invoice_order, invoice_export, download_invoice,
               mark_as_customer_acknowledged, mark_as_customer_confirmed, billed_revenue, unbilled_revenue]

    fsm_field = ['status', ]
    # resource_class = InvoiceResource

    search_fields = ['invoice_number', 'id', 'related_invoice__invoice_number',
                     'related_invoice__id', 'current_owner__name', 'comments', 'remarks']

    multi_search_fields = ('invoice_number',)
    list_filter = ['status', 'invoice_type', 'contract_invoices__contract__division',
                   ('customer', admin.RelatedOnlyFieldListFilter), 'is_manual_upload',
                   CustomMonthFilter('service period end', 'service_period_end'),
                   CustomMonthFilter('revenue Month', 'revenue_booked_on'),
                   CustomYesNoFilter('Blanket Invoice', 'blanket_invoice'),
                   ('bh_source_state', list_related_only_filter_title('Invoice State')),
                   ('current_owner', admin.RelatedOnlyFieldListFilter)]

    def state(self, obj):
        if obj:
            return obj.bh_source_state
        return None

    def current_owner_email(self, obj):
        return obj.current_owner_email

    def get_adjustment_amount(self, obj):
        if obj:
            return obj.amount_adjustment
        return None

    def has_add_permission(self, request):
        return True

    get_adjustment_amount.short_description = 'Adjustment Amount'

    def get_revenue_booked_on(self, obj):
        if obj and obj.revenue_booked_on:
            return str(calendar.month_abbr[obj.revenue_booked_on.month]) + '-' + str(obj.revenue_booked_on.year)
        return None

    get_revenue_booked_on.short_description = 'Revenue Month'

    def link_to_batch_id(self, obj):
        if obj and obj.batch_id:
            url = reverse("admin:customer_invoicebatch_change",
                          args=[obj.batch_id])
            return format_html("<a href='{url}'>" + str(obj.batch_id) + "</a>", url=url)
        return None

    def get_related_invoice(self, obj):
        if obj and obj.related_invoice:
            url = reverse("admin:customer_customerinvoice_change",
                          args=[obj.related_invoice_id])
            return format_html("<a href='{url}'>" + str(obj.related_invoice) + "</a>", url=url)
        return None

    link_to_batch_id.short_description = 'Batch'
    get_related_invoice.short_description = 'Against'

    def config(self, obj):
        if obj:
            return obj.invoice_config

    def customer_name(self, obj):
        return obj.customer_name

    def get_calculation(self, obj):
        slip = calculation_formatter(obj)
        return format_html('<pre>%s</pre>' % slip)

    get_calculation.short_description = _('Details')

    def _get_aggregate_levels(self, obj):
        self._aggregate_levels = getattr(self, '_aggregate_values', {})
        if obj and not obj in self._aggregate_levels:
            self._aggregate_levels[obj] = []
            for attribute in obj.get_aggregate_attributes():
                self._aggregate_levels[obj].append(attribute.name)
        return self._aggregate_levels.get(obj, [])

    def aggregate_levels(self, obj):
        return self._get_aggregate_levels(obj) if obj else None

    def aggregate_values(self, obj):
        return ", ".join(obj.get_aggregate_values()) if obj else None

    def amount_taxes(self, obj):
        return intcomma(obj.cgst_amount + obj.sgst_amount + obj.igst_amount) if obj else None

    def amount_taxable(self, obj):
        return intcomma(obj.taxable_amount) if obj else None

    # def non_taxable_amount(self, obj):
    #     return obj.non_taxable_amount
        # if obj.total_rcm_amount > 0:
        #     return amount - obj.taxable_amount
        # return amount - (obj.taxable_amount + obj.cgst_amount + obj.sgst_amount + obj.igst_amount)

    # non_taxable_amount.short_description = 'Non Taxable Adjustment'

    # def taxable_adjustment(self, obj):
    #     return obj.taxable_amount - (obj.charges_service + obj.charges_basic)

    # taxable_adjustment.short_description = 'Taxable Adjustment'

    def get_form(self, request, obj=None, **kwargs):
        # just save obj reference for future processing in Inline
        request._obj_ = obj
        return super(CustomerInvoiceAdmin, self).get_form(request, obj, **kwargs)

    def amount_total(self, obj):
        return intcomma(obj.total_amount) if obj else None

    def amount_discount(self, obj):
        return intcomma(obj.discount) if obj.discount else 0

    def amount_credit(self, obj):
        if obj.amount_credit:
            return obj.amount_credit
        else:
            return '-'

    def is_blanket_invoice(self, obj):
        if obj.invoice_type == TAX_INVOICE and obj.is_credit_debit_note:
            return True
        return False

    is_blanket_invoice.short_description = 'Blanket Invoice'

    def get_credit_amount(self, obj):
        credit_note_amount = []

        if obj:
            credit_note_amount = CustomerInvoice.objects.filter(
                invoice_type=CREDIT_NOTE, related_invoice=obj).exclude(
                status__in=INVOICE_EDITABLE_STATUS).values_list('total_amount',
                                                                flat=True)

        if credit_note_amount:
            amount_sum = reduce((lambda x, y: x + y), credit_note_amount)
            return amount_sum

        return None

    get_credit_amount.short_description = _('Credit Amount')

    def get_actions(self, request):
        """
        Remove `Delete` Option on invoices`
        """
        actions = super().get_actions(request)

        if not request.user.is_superuser and 'delete_invoices' in actions:
            del actions['delete_invoices']

        if 'delete_selected' in actions:
            del actions['delete_selected']

        return actions

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(CustomerInvoiceAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'related_invoice':
            default_qs = Q(is_credit_debit_note=False,
                        status__in=[CUSTOMER_CONFIRMED, PARTIALLY_PAID, CUSTOMER_DISPUTED])
            blanket_invoice_qs = Q(invoice_type=TAX_INVOICE,
                        is_credit_debit_note=True,
                        status__in=[CUSTOMER_CONFIRMED, PARTIALLY_PAID, CUSTOMER_DISPUTED])

            field.queryset = field.queryset.filter(default_qs|blanket_invoice_qs)

        if db_field.name == 'customer':
            field.queryset = field.queryset.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        if db_field.name == 'credit_debit_note_reason':
            field.queryset = field.queryset.filter(is_active=True)

        return field

    def get_queryset(self, request):
        """ Access control for contract-supervisors and superusers
        """
        qs = super(CustomerInvoiceAdmin, self).get_queryset(request)
        # TODO : This permission is to be fixed later.
        # if not request.user.is_superuser:
        #     cities = City.objects.filter(managers=request.user)
        #     qs = qs.filter(Q(contract__spocs=request.user) | Q(
        # contract__supervisors=request.user) | Q(contract__city__in=cities))
        qs = qs.select_related('bh_source_state', 'invoice_configs__customer')
        qs = qs.select_related('aggregation')
        qs = qs.prefetch_related('aggregation__hub', 'aggregation__city',
                                 'aggregation__driver', 'aggregation__vehicle',
                                 'aggregation__contract',
                                 'aggregation__division')
        qs = qs.prefetch_related(
            'batch__aggregates')
        qs = qs.annotate(amount_adjustment=Sum(
            'customerinvoiceadjustment__total_amount'))
        qs = qs.annotate(customer_name=F('customer__name'), current_owner_email=F('current_owner__email'))

        # Get total credit note raised against this invoice
        qs = qs.annotate(amount_credit=(Subquery(
            CustomerInvoice.objects.filter(
                related_invoice_id=OuterRef('pk')).only('id').annotate(
                sum=Coalesce(Sum('total_amount'), 0)).values('sum')[:1])))

        return qs.order_by('-modified_date')

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        # Added refrences here beacuse it will impact admin queryset
        # count/timings
        qs = qs.select_related('customer', 'invoice_state', 'bh_source_state', 'batch',
                               'invoice_configs__customer')

        qs = qs.select_related('contract', 'contract__customer',
                               'contract__city', 'customer', 'contract__division', 'aggregation').distinct()
        qs = qs.prefetch_related('aggregation__hub', 'aggregation__city', 'aggregation__driver', 'aggregation__vehicle',
                                 'aggregation__contract', 'aggregation__division')
        qs = qs.prefetch_related(
            'batch__aggregates', 'customerinvoiceadjustment_set')

        qs = qs.annotate(amount_adjustment=Sum(
            'customerinvoiceadjustment__total_amount'))

        return list(qs)

    def get_inline_instances(self, request, obj=None):
        inline_instances = []

        if not obj:
            return inline_instances

        new_inlines = [CustomerInvoiceAdjustmentAdmin,
                       InvoiceHistoryAdminInline,
                       InvoiceSupportingDocumentInline, MISDocumentInline]

        if not obj.is_credit_debit_note:
            new_inlines += [ContractInvoiceFragmentInlineAdmin, InvoiceTripAdminInline]

            if obj and obj.invoicetrip_set.all().count() > 1000:
                new_inlines.remove(InvoiceTripAdminInline)

        if obj.status in [APPROVED, SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED, UPLOADED_TO_GST_PORTAL,
                          CUSTOMER_CONFIRMED, PARTIALLY_PAID, PAYMENT_DONE]:
            new_inlines.append(InvoicePaymentAdminInline)

        if obj.calculation_level == "ORDER":
            if InvoiceTripAdminInline in new_inlines:
                new_inlines.remove(InvoiceTripAdminInline)
            if obj and obj.invoiceorder_set.all().count() < 1000:
                new_inlines.append(InvoiceOrderAdminInline)

        for inline_class in new_inlines:
            inline = inline_class(self.model, self.admin_site)
            if request:
                if inline_class in [InvoiceHistoryAdminInline,
                                    InvoiceTripAdminInline,
                                    InvoiceOrderAdminInline]:
                    # inline.max_num = 15000
                    inline.can_delete = False
                    inline.can_add = False
                elif obj and inline_class == MISDocumentInline:
                    inline.can_delete = True
                    inline.can_add = False

                elif obj and inline_class == InvoicePaymentAdminInline:
                    if request.user and \
                        is_collection_representative(self, request.user):# and \
                            # not (obj.status == PAYMENT_DONE):
                        inline.can_delete = True
                        inline.can_add = True
                    else:
                        inline.can_delete = False
                        inline.can_add = False

                elif obj and obj.status not in INVOICE_EDITABLE_STATUS:
                    inline.max_num = 0
                    inline.can_delete = False
                    inline.can_add = False

            inline_instances.append(inline)

        return inline_instances

    def get_fieldsets(self, request, obj=None):

        if obj:

            fieldsets = (
                (None, {
                    'fields':
                        ('related_invoice', 'customer', 'invoice_date',
                         'get_revenue_booked_on', 'invoice_number',
                         'service_period_start', 'service_period_end', 'status',
                         'bh_source_state', 'invoice_state', 'hs_code',
                         'file', 'invoice_configs', 'aggregate_levels',
                         'aggregate_values', 'is_stale', 'stale_reason', 'manual_upload_reason',
                         'is_blanket_invoice', 'comments', 'remarks',
                         'supporting_doc', 'reason', 'email', 'representative',
                         'supervisor', 'spoc', 'executive', 'duedate',
                         'is_reassigning', 'id', 'to_email', 'cc_email',
                         'get_credit_amount', 'upload_to_gst_portal_failed_reason', 'manualupload_reasons',
                         'credit_debit_note_reason')
                }),
                ('Cost Information', {
                    'fields':
                        (
                            'charges_basic',
                            ('days_counted', 'basic_per_day',
                             'count_distinct_drivers', 'count_all_working_days',),
                            'charges_service', 'taxable_adjustment',
                            'taxable_amount', 'non_taxable_adjustment',
                            ('cgst_amount', 'sgst_amount', 'igst_amount'),
                            'discount', 'total_amount', 'suggested_value',
                            'suggested_tds'
                        ),
                }),
                ('Calculation', {
                    'fields': ('get_calculation',),
                }),
                ('Audit Log', {
                    'fields': (
                        'created_date', 'created_by', 'modified_date',
                        'modified_by', 'rb_job_date'),
                }),
                ('Base Pay Distribution', {
                    'fields':
                        ('count_distinct_drivers',
                         'count_all_working_days', 'distribution_days'),
                }),
                ('Historical Dump', {
                    'fields':
                        ('historical_invoice_dump',),
                }),
            )
        else:
            fieldsets = (
                (None, {
                    'fields':
                        ('invoice_type', 'credit_debit_note_reason', 'related_invoice', 'customer', 'invoice_date',
                         'service_period_start', 'service_period_end', 'charges_basic',
                         'service_tax_category', 'hs_code', 'status', 'bh_source_state',
                         'invoice_state', 'comments', 'remarks', 'supporting_doc', 'reason',
                         'email')
                }),)

        return fieldsets

    readonly_fields = [
        "count_all_working_days", "count_distinct_drivers", 'is_blanket_invoice', 'invoice_date',
        'non_taxable_adjustment', 'taxable_adjustment',
        "distribution_days", "aggregate_levels", "aggregate_values", "historical_invoice_dump",
        'get_calculation',
        'get_revenue_booked_on', 'get_credit_amount', 'suggested_value', 'suggested_tds']

    editable_fields = ["days_counted"]


    def count_all_working_days(self, invoice):
        return invoice.count_all_working_days

    def count_distinct_drivers(self, invoice):
        return invoice.count_distinct_drivers

    def suggested_value(self, invoice):
        return invoice.suggested_value

    def suggested_tds(self, invoice):
        return invoice.suggested_tds

    def distribution_days(self, invoice):
        driver_names_by_id = dict(Driver.objects.filter(
            id__in=invoice.distribution_days.keys()).values_list('id', 'name'))
        html = """
        <div class="rTable">
        <div class="rTableRow">
        <div class="rTableHead"><strong>Driver ID</strong></div>
        <div class="rTableHead"><strong>Driver Name</strong></div>
        <div class="rTableHead"><strong>Days worked</strong></div>
        </div>
        """
        for driver_id, days in invoice.distribution_days.items():
            html += """
            <div class="rTableRow">
            <div class="rTableCell">{driver_id}</div>
            <div class="rTableCell">{driver_name}</div>
            <div class="rTableCell">{days_count}</div>
            </div>
            """.format(
                driver_id=driver_id,
                driver_name=driver_names_by_id.get(int(driver_id), "None"),
                days_count=days,
            )

        return mark_safe(html)

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            # TODO: Remove the below commented code once the revenue month batch job data is verified
            # booked_on = form.cleaned_data.get('booked_on', None)
            # if booked_on:
            #     obj.revenue_booked_on = get_revenue_book_on(booked_on)
            supporting_doc_fields = ['supporting_doc', 'reason',
                                     'disputed_reason', 'supervisor',
                                     'representative', 'spoc', 'duedate',
                                     'sales_reason']
            recipients = ''
            stored_emails = form.cleaned_data.get('email', None)

            obj.action_email = None
            obj.requested_user = request.user
            manualupload_reasons = form.cleaned_data.get('manualupload_reasons', None)

            if manualupload_reasons:
                obj.manual_upload_reason_id = manualupload_reasons

            if stored_emails:
                recipients = stored_emails

            if form.cleaned_data.get('to_email', None):
                recipients = ','.join([recipients, form.cleaned_data.get('to_email')])

            obj.email = recipients
            obj.cc_email = form.cleaned_data.get('cc_email', None)

            if any(x in supporting_doc_fields for x in form.changed_data):
                _file = form.cleaned_data.get('supporting_doc', None)
                reason = form.cleaned_data.get('reason', None)
                disputed_reason = form.cleaned_data.get('disputed_reason', None)
                sales_reason = form.cleaned_data.get('sales_reason', None)
                supervisor = form.cleaned_data.get('supervisor', None)
                representative = form.cleaned_data.get('representative', None)
                spoc = form.cleaned_data.get('spoc', None)

                if form.cleaned_data.get('duedate', None):
                    obj.due_date = form.cleaned_data.get('duedate')

                if form.cleaned_data.get('is_reassigning', False):
                    obj.action_email = spoc or ''
                else:
                    obj.action_email = supervisor or representative or ''

                if _file:
                    data = {
                        'invoice': obj,
                        'supporting_doc': _file,
                        'reason': manualupload_reasons
                    }
                    mis_data = {
                        'invoice': obj,
                        'mis_doc': _file
                    }
                    obj.mis = MISDocument(**mis_data)
                    document = InvoiceSupportingDocument(**data)
                    obj.document = document

                if reason or disputed_reason or sales_reason:
                    obj.reason = reason or disputed_reason or sales_reason
                else:
                    obj.reason = None

            imp_field = ['comments', 'charges_basic', 'invoice_date',]
            if change and any(x in imp_field for x in form.changed_data):
                obj.is_stale = True
                obj.stale_reason = 'Invoice Important fields changed'
            if not change:
                obj.is_credit_debit_note = True
                CreditDebitNote.generate_credit_debit_notes(invoice=obj)
            # elif not obj.is_credit_debit_note:
            #     if "days_counted" in form.cleaned_data:
            #         invoice = CustomerInvoice.objects.get(id=obj.id)
                    # total_base_amount = round(
                    #     invoice.basic_per_day *
                    #     invoice.days_counted,
                    #     0)
                    # print("j----------------", total_base_amount, obj.days_counted)
                    # obj.basic_per_day = total_base_amount / obj.days_counted
                    # obj.charges_basic = obj.basic_per_day * \
                    #                     obj.count_all_working_days
            super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, obj=None):
        read_only_fields = super().get_readonly_fields(request, obj)
        if obj and obj.status in INVOICE_EDITABLE_STATUS:
            # read_only_fields.extend(self.editable_fields)
            read_only_fields.remove('comments')
            read_only_fields.remove('hs_code')
            if obj.is_credit_debit_note:
                read_only_fields.remove('taxable_amount')
        if not obj:
            read_only_fields.append('status')
        else:
            read_only_fields.extend(self.editable_fields)
            read_only_fields.remove('remarks')

        return read_only_fields

    def changelist_view(self, request, extra_context=None):
        if 'action' in request.POST and request.POST['action'] == 'invoice_export':
            if not request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                post.update({admin.ACTION_CHECKBOX_NAME: str('-1')})
                request._set_post(post)

        return super(CustomerInvoiceAdmin, self).changelist_view(request,
                                                                 extra_context)

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        if object_id:
            invoice = CustomerInvoice.objects.get(pk=object_id)
            gstin_present, is_valid, gstin_required = is_valid_gstin(invoice)
            if not gstin_present and gstin_required and invoice.status in INVOICE_STATUSES_FOR_GSTIN_WARNING:
                messages.info(request,
                              'GSTIN not present for this customer. Please check/update in Business Customer')
            if gstin_present and gstin_required and not is_valid and \
                invoice.status in INVOICE_STATUSES_FOR_GSTIN_WARNING:
                messages.info(request, 'Invalid Customer GSTIN. Please check/update in Business Customer')
        try:
            return super().change_view(
                request, object_id, extra_context=extra_context)

        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    class Media:
        css = {
            'all': (
                'website/css/bootstrap.css',
                '/static/admin/css/invoice.css',
                'website/css/bootstrap-datetimepicker.css',
            )
        }
        js = (
            'website/js/lib/jquery.min.js',
            'website/js/lib/bootstrap.min.js',
            'website/js/lib/jquery-ui.min.js',
            'website/js/lib/moment.min.js',
            'website/js/lib/bootstrap-datetimepicker.min.js',
            'admin/js/customer/invoice.js',
            'admin/js/customer/manualupload.js',
        )


class InvoiceEntityAdmin(admin.ModelAdmin):
    model = InvoiceEntity
    list_display = (
        'id', 'city', 'hub', 'driver', 'vehicle_class', 'date', 'attribute', 'value', 'extra_value', 'rate',
        'slab_fixed_amount_kilometers', 'slab_fixed_amount_hours', 'amount', 'sell_rate', 'contract_invoice',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('city', 'hub', 'driver', 'vehicle_class')
        qs = qs.select_related('contract_invoice', 'sell_rate')
        return qs


class InvoiceAggregationAdmin(admin.ModelAdmin):
    model = InvoiceAggregation
    list_display = ['id', 'contract', 'contract_type', 'division',
                    'city', 'hub', 'driver', 'vehicle_class', 'invoice']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('city', 'hub', 'driver', 'vehicle_class')
        qs = qs.select_related('contract', 'vehicle', 'invoice', 'invoice__customer')
        return qs


class CustomerLedgerStatementAdmin(NonEditableAdmin):
    model = CustomerLedgerStatement
    list_filter = (('customer', admin.RelatedOnlyFieldListFilter),('transaction_time', DateRangeFilter),
                   AddBalanceFilter)
    search_fields = ('customer__name', 'customer__legalname',)
    list_per_page = 500
    list_display = ('id', 'customer', 'order_number', 'transaction_time',
                    'credit', 'debit', 'balance', 'paytm_source', 'modified_by',)
    readonly_fields = list_display

    def razorpay_source(self, obj):
        #razorpay
        if obj.source:
            url = reverse(
                'admin:payment_source_change', args=[obj.source_id])
            return format_html("<a href='{}'>{}</a>", url, obj.source.wallet_reference)
        return '-'

    def paytm_source(self, obj):
        #paytm
        if obj.wallet_source:
            url = reverse(
                'admin:payment_transactionorder_change', args=[obj.wallet_source_id])
            return format_html("<a href='{}'>{}</a>", url, obj.wallet_source.order_number)
        return '-'

    def order_number(self, obj):
        #razorpay
        if obj.order:
            url = reverse(
                'admin:order_bookingorder_change', args=[obj.order_id])
            return format_html("<a href='{}'>{}</a>", url, obj.order.number)
        return '-'

    def credit(self, obj):
        return obj.amount if obj.amount > 0 else ''

    credit.admin_order_field = 'amount'

    def debit(self, obj):
        return obj.amount if obj.amount <= 0 else ''

    debit.admin_order_field = 'amount'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer', 'order', 'wallet_source', 'source')
        return qs

class CustomerLedgerBalanceAdmin(NonEditableAdmin):
    model = CustomerBalance
    list_per_page = 100
    list_display = ('customer',  'balance',)
    readonly_fields = list_display
    list_filter = (('customer', admin.RelatedOnlyFieldListFilter),)
    search_fields = ('customer__name', 'customer__legalname',)


    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.order_by('customer_id', '-transaction_time').distinct('customer_id')
        return qs


class InvoiceBatchAdmin(NonEditableAdmin):
    model = InvoiceBatch
    search_fields = ('id', 'aggregates__name',)
    list_filter = (('customer', admin.RelatedOnlyFieldListFilter),
                   ('service_period_start', DateRangeFilter),
                   ('service_period_end', DateRangeFilter),)
    list_display = ['id', 'get_aggregates', 'customer', 'service_period_start',
                    'service_period_end', 'requested_by']

    def get_aggregates(self, config):
        return [x for x in config.aggregates.all()]

    get_aggregates.short_description = 'Aggregations'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer', 'invoice_request')
        qs = qs.select_related('requested_by', 'created_by', 'modified_by')
        qs = qs.prefetch_related(Prefetch(
            'aggregates',
            queryset=InvoiceAggregateAttribute.objects.only('name'))
        )
        return qs


class InvoiceCreationRequestAdmin(admin.ModelAdmin):
    model = InvoiceCreationRequest
    list_display = (
        'id', 'customer', 'get_aggregates', 'service_period_start',
        'service_period_end', 'status', 'email', 'requested_time',)
    readonly_fields = ('status', 'output_log', 'requested_time')

    def get_aggregates(self, config):
        return list(config.aggregates.all().values_list('name', flat=True))

    get_aggregates.short_description = 'Aggregations'

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(InvoiceCreationRequestAdmin, self).changeform_view(request, object_id, extra_context=extra_context)


class InvoiceRequestAdmin(admin.ModelAdmin):
    model = InvoiceRequest
    form = InvoiceRequestForm
    list_display = ('id', 'get_customer', 'get_config', 'status',
                    'created_by', 'created_date')
    readonly_fields = ('status', 'output_log', 'created_by', 'created_date')
    search_fields = ('id', 'invoice_config__name', )

    def get_customer(self, _request):
        return _request.invoice_config.customer

    get_customer.short_description = 'Customer'

    def get_config(self, _request):
        return _request.invoice_config.name

    get_config.short_description = 'Config Name'

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(InvoiceRequestAdmin, self).changeform_view(request, object_id, extra_context=extra_context)

    def get_form(self, request, obj=None, **kwargs):
        """
        Override the formset function in order to remove the add and change buttons beside the foreign key pull-down
        menus in the inline.
        """
        form = super(InvoiceRequestAdmin, self).get_form(request, obj, **kwargs)
        widget = form.base_fields['invoice_config'].widget
        widget.can_add_related = False
        widget.can_change_related = False
        widget.can_delete_related = False
        return form

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'invoice_config':
            qs = CustomerInvoiceConfig.objects.filter(
                status=INVOICE_CONFIG_ACTIVE)
            kwargs['queryset'] = qs

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    class Media:
        js = (
            '/static/admin/js/customer/invoice_period.js',
        )

class CollectionLineInline(admin.TabularInline):
    model = InvoicePayment
    verbose_name = 'Invoice Allocation'
    verbose_name_plural = 'Invoice Allocation'
    extra = 0
    readonly_fields = ('amount', 'invoice', 'actual_amount', 'tds', 'paid_on', 'debit', )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ReceivablelineItemInline(admin.TabularInline):
    model = ReceivablelineItem
    min_num = 0
    extra = 0
    max_num = 1500
    formset = CustomerReceivableFormSet

    def get_formset(self, request, obj=None, **kwargs):
        self.parent_obj = obj
        return super().get_formset(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'invoice':
            customer = None
            if self.parent_obj:
                customer = self.parent_obj.customer
            qs = CustomerInvoice.objects.filter(status=CUSTOMER_CONFIRMED,
                                                customer=customer)
            kwargs['queryset'] = qs

        if db_field.name == 'order':
            customer = None
            if self.parent_obj:
                customer = self.parent_obj.customer
            qs = Order.objects.filter(status=StatusPipeline.ORDER_DELIVERED,
                                      customer=customer,
                                      order_type=settings.DEFAULT_ORDER_TYPE)
            kwargs['queryset'] = qs
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if self.parent_obj and self.parent_obj.status not in (CREATED,
                                                              UNAPPROVED):
            return ['invoice', 'order', 'amount', 'tds', 'get_tds_cert_link']
        return []

    def get_tds_cert_link(self, obj=None):
        if obj.tds_certificate:
            return u'<a href="%s" target="_blank">View</a>' % obj.tds_certificate.url
        return '--NA--'

    get_tds_cert_link.short_description = _('TDS Certificate')
    get_tds_cert_link.allow_tags = True

    def get_fields(self, request, obj=None):
        fields = ('invoice', 'order', 'amount', 'tds')
        if self.parent_obj and self.parent_obj.status not in (CREATED,
                                                              UNAPPROVED):
            return fields + ('get_tds_cert_link',)

        return fields + ('tds_certificate',)

def export_collection_data(modeladmin, request, queryset):
    if queryset:
        collections = queryset.values_list('id', flat=True)
        queryset = CustomerReceivable.copy_data.filter(id__in=collections)
    else:
        queryset = CustomerReceivable.copy_data.all()

    queryset = queryset.order_by('-id')

    file_part = 'collection'
    queryset.annotate(
        allocated_amount=Sum('invoicepayment__actual_amount'),
        allocated_invoice_count=Count('invoicepayment'),
        allocated_invoices=StringAgg('invoicepayment__invoice__invoice_number', delimiter=',')
    ).to_csv(
        file_part,
        'id',
        'customer__name',
        'amount',
        'description',
        'bank_account__account_name',
        'bank_account__nick_name',
        'status',
        'transaction_time',
        'payment_date',
        'deposit_date',
        'payment_mode',
        'unallocated_amount',
        'allocated_amount',
        'allocated_invoice_count',
        'allocated_invoices')

    file_name = '%s_' % str(utc_to_ist(timezone.now())) + os.path.splitext(file_part)[0] + ".csv"
    with open(file_part, 'r') as inFile, open(file_name, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'Collection ID',
            'Customer',
            'Amount',
            'Description',
            'Bank Account',
            'Bank Short Name',
            'Collection Entry Status',
            'Transaction Time',
            'Payment Date',
            'Deposit Date',
            'Payment Mode',
            'Unallocated Amount',
            'Allocated Amount',
            'Allocated Invoice Count',
            'Allocated Invoices'
            ])

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1

        os.remove(file_part)

    if os.path.exists(file_name):
        with open(file_name, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_name)
            os.remove(file_name)
            return response


class CustomerReceivableAdmin(FSMTransitionMixin, BaseAdmin):
    actions = [export_collection_data]
    fsm_field = ['status', ]
    model = CustomerReceivable
    form = ReceivablelineForm

    inlines = [#ReceivablelineItemInline,
                CollectionLineInline]

    search_fields = ('id', 'customer__name', 'customer__legalname',)

    list_display = ('id', 'customer', 'status', 'transaction_time',
                    'payment_date', 'payment_mode', 'amount',
                    'get_total_paid_amount', 'get_total_tds_amount',
                    'get_line_items_count', 'allocated_amount', 'get_unallocated_amount',
                    'allocated_invoice_count', 'allocated_invoices')
    list_filter = ['status', 'payment_mode',
                   ('payment_date', DateFilter),
                   ('customer', admin.RelatedOnlyFieldListFilter),
                   'bank_account', ('deposit_date', DateFilter)]

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['status', 'created_date', 'created_by',
                           'modified_date', 'modified_by']
        if obj and obj.status not in (CREATED, UNAPPROVED):
            readonly_fields += ['customer', 'transaction_time', 'payment_date',
                                'payment_mode', 'amount',
                                'get_total_paid_amount', 'amount',
                                'description', 'bank_account', 'payment_copy',
                                'deposit_date']

        return readonly_fields

    def get_fieldsets(self, request, obj=None):
        if obj:
            fieldsets = (
                (None, {
                    'fields':
                        ('customer', ('payment_date', 'payment_mode'),
                         ('deposit_date', 'payment_copy'), 'status',
                         'bank_account', 'amount', 'description',),
                }),
                ('Audit Log', {
                    'classes': ('collapse',),
                    'fields': (
                        'created_date', 'created_by', 'modified_date',
                        'modified_by'),
                }),)
        else:
            fieldsets = (
                (None, {
                    'fields':
                        ('customer', ('payment_date', 'payment_mode'),
                         ('deposit_date', 'payment_copy'), 'status',
                         'bank_account', 'amount', 'description'),
                }),)

        return fieldsets

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super(CustomerReceivableAdmin,
                                       self).get_search_results(request,
                                                                queryset,
                                                                search_term)

        if search_term:
            ''' Since, invoice field present in child model,
                need to query the child model; ReceivablelineItem
            '''
            search_params = [x.strip() for x in search_term.split()]
            queryset |= self.model.objects.filter(
                pk__in=ReceivablelineItem.objects.filter(
                    invoice__invoice_number__in=search_params).values(
                    'customer_receivable_id'))
        return queryset, use_distinct

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer')
        qs = qs.select_related('bank_account', 'created_by', 'modified_by')
        qs = qs.annotate(lines_count=Count('receivablelineitem'))
        qs = qs.annotate(total_tds_amount=Sum('receivablelineitem__tds'))
        qs = qs.annotate(total_received=Sum('receivablelineitem__amount'))
        qs = qs.annotate(allocated_amount=Sum('invoicepayment__actual_amount'))
        qs = qs.annotate(allocated_invoice_count=Count('invoicepayment'))
        qs = qs.annotate(allocated_invoices=StringAgg('invoicepayment__invoice__invoice_number', delimiter=','))
        return qs.order_by('-payment_date')

    def get_total_paid_amount(self, obj):
        if obj.total_received:
            total_received = obj.total_received
            return total_received + obj.total_tds_amount

        # If no line-items present
        return obj.amount

    get_total_paid_amount.short_description = 'Total Received (Incl. TDS)'

    def allocated_amount(self, obj):
        if obj.allocated_amount:
            return obj.allocated_amount

    allocated_amount.short_description = 'Allocated Amount'

    def allocated_invoice_count(self, obj):
        if obj.allocated_invoice_count:
            return obj.allocated_invoice_count

    allocated_invoice_count.short_description = 'Allocated Invoices Count'

    def get_unallocated_amount(self, obj):
        return obj.unallocated_amount

    get_unallocated_amount.short_description = 'Unallocated Amount'

    def allocated_invoices(self, obj):
        if obj.allocated_invoice_count and obj.allocated_invoice_count > 0:
            return obj.allocated_invoices

    allocated_invoices.short_descriptions = 'Allocated Invoices'

    def get_total_tds_amount(self, obj):
        return obj.total_tds_amount

    get_total_tds_amount.short_description = _('Total TDS Amount')

    def get_line_items_count(self, obj):
        return obj.lines_count

    get_line_items_count.short_description = 'Line Items count'

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            qs = BusinessCustomer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL).exclude(id=16478)
            kwargs['queryset'] = qs
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return False

    class Media:
        css = {'all': ('/static/admin/css/customer_receivable.css',)}


class DriverEligibilityInline(admin.TabularInline):
    model = DriverEligibility
    extra = 0
    max = 1


class PartnerAdmin(admin.ModelAdmin):
    list_display = ('legalname', 'category', 'pan', 'is_tax_registered', 'service_tax_category')
    search_fields = list_display

    readonly_fields = ['api_key']

    inlines = [DriverEligibilityInline]


class ReceivablelineItemAdmin(admin.ModelAdmin):
    actions = None
    list_display = ('customer_receivable', 'invoice', 'order', 'amount', 'tds',
                    'get_tds_cert_link')
    search_fields = ('customer_receivable__id', 'invoice__invoice_number',
                     'order__number')

    fields = ['customer_receivable', 'invoice', 'order', 'amount', 'tds',
              'get_tds_cert_link']
    readonly_fields = ['customer_receivable', 'invoice', 'order', 'amount',
                       'tds', 'get_tds_cert_link']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_tds_cert_link(self, obj=None):
        if obj.tds_certificate:
            return u'<a href="%s" target="_blank">View</a>' % obj.tds_certificate.url
        return '--NA--'

    get_tds_cert_link.short_description = _('TDS Certificate')
    get_tds_cert_link.allow_tags = True


@admin.register(CustomerFeedback)
class CustomerFeedbackAdmin(admin.ModelAdmin):
    actions = None
    search_fields = ('customer__name', 'customer__user__email',
                     'customer__user__phone_number',)
    list_display = ('customer', 'email', 'phone_number', 'feedback',
                    'created_date',)
    readonly_fields = list_display + ('extra_info',)
    exclude = ('extra_details',)
    list_filter = (('customer', admin.RelatedOnlyFieldListFilter),
                   ('created_date', DateRangeFilter),)

    def phone_number(self, obj):
        user = obj.customer.user
        if user:
            return '%s' % user.phone_number
        return '--NA--'

    def email(self, obj):
        user = obj.customer.user
        if user:
            return user.email
        return '--NA--'

    def extra_info(self, obj):
        if obj.extra_details:
            extra_details = json.loads(obj.extra_details)
            return '\n'.join([(x + ': ' + extra_details[x]) for x in extra_details])
        return '--NA--'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('customer', 'customer__user')

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(CustomerFeedbackAdmin, self).changeform_view(
            request, object_id, extra_context=extra_context)


class GPSEventPublishConfigInline(admin.TabularInline):
    model = GPSEventPublishConfig
    extra = 0

    filter_horizontal = ['contracts']

    def get_parent_object_from_request(self, request):
        """ Returns the parent object from the request or None.

        Note that this only works for Inlines, because the `parent_model`
        is not available in the regular admin.ModelAdmin as an attribute.
        """

        resolved = resolve(request.path_info)
        if resolved.args:
            return self.parent_model.objects.get(pk=resolved.args[0])
        return None

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        customer = self.get_parent_object_from_request(request)
        field = super(
            GPSEventPublishConfigInline, self).formfield_for_manytomany(
            db_field, request, **kwargs)

        if db_field.name == 'contracts':
            field.queryset = field.queryset.filter(customer=customer)

        return field


class GPSEventPublishConfigView(Customer):

    class Meta:
        proxy = True
        verbose_name = _('GPS Event Publish Config View')
        verbose_name_plural = _('GPS Event Publish Config Views')


class GPSEventPublishConfigViewAdmin(BusinessCustomerAbstract):

    inlines = [GPSEventPublishConfigInline]
    change_list_template = 'admin/customer/gpseventpublishconfig_change_list.html'

    class Media:
        css = {'all': ('/static/admin/css/configview.css',)}

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.exclude(gps_event_publish_config=None)
        return qs


class SmsConfigAdmin(admin.ModelAdmin):

    list_display = ('id', 'template_id', 'get_customer', 'event', 'is_active')

    list_filter = (('customer', admin.RelatedOnlyFieldListFilter),
                   ('contract', admin.RelatedOnlyFieldListFilter))

    form = SmsConfigForm

    fields = ('customer', 'contract', 'is_active', 'template_id',
              'event', 'stop_status', 'msg_template', 'get_msg_info')

    readonly_fields = ('get_msg_info', )

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            qs = Customer.objects.filter(
                customer_category__in=[CUSTOMER_CATEGORY_ENTERPRISE,
                                       CUSTOMER_CATEGORY_TRANSITION,
                                       CUSTOMER_CATEGORY_NON_ENTERPRISE])
            kwargs['queryset'] = qs
        if db_field.name == 'contract':
            kwargs['queryset'] = Contract.objects.filter(
                customer__customer_category__in=[CUSTOMER_CATEGORY_ENTERPRISE,
                                        CUSTOMER_CATEGORY_TRANSITION,
                                        CUSTOMER_CATEGORY_NON_ENTERPRISE],
                status=CONTRACT_STATUS_ACTIVE)
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def get_customer(self, obj):
        return list(obj.customer.all().values_list('name', flat=True))

    get_customer.short_description = 'Customers'

    def get_msg_info(self, obj):
        template = """
        <table><thead>"""
        template += """<th>Tag Name</th>
                        <th></th>"""
        template += """</thead><tbody>"""

        template += """
                    <tr>
                        <td>{customer_name}</td>
                        <td>Name of Blowhorn Customer like Swiggy.</td>
                    </tr>
                    <tr>
                        <td>{reference_number}</td>
                        <td>Reference number of order.</td>
                    </tr>
                    <tr>
                        <td>{customer_reference_number}</td>
                        <td>Customer reference number of order.</td>
                    </tr>
                    <tr>
                        <td>{order_number}</td>
                        <td>Order number.</td>
                    </tr>
                    <tr>
                        <td>{delivery_date}</td>
                        <td>To be used when order has expected delivery date time.</td>
                    </tr>
                    <tr>
                        <td>{delivery_time}</td>
                        <td>To be used when order has expected delivery date time.</td>
                    </tr>
                    <tr>
                        <td>{driver_name}</td>
                        <td>Delivery associate name.</td>
                    </tr>
                    <tr>
                        <td>{phone_number}</td>
                        <td>Delivery associate phone number.</td>
                    </tr>
                    <tr>
                        <td>{order_otp}</td>
                        <td>OTP to be shared by end customer to driver to collect the delivery(should be used only when trip workflow has option - require OTP for order delivery is enabled)</td>
                    </tr>
                    <tr>
                        <td>{tracking_link}</td>
                        <td>End customer can track their order using this link.</td>
                    </tr>
                    <tr>
                        <td>{feedback_link}</td>
                        <td>End customer can share delivery feedback on given link.</td>
                    </tr>
        """
        template += "</tbody></table>"
        return mark_safe(template)

    get_msg_info.short_description = 'Note'


class CustomerEmailLogAdmin(admin.ModelAdmin):

    list_display = ('id', 'customer', 'email_sent_at', 'ageing')
    list_filter = ('customer',)

    search_fields = ('customer',)

    fields = ('customer', 'email_sent_at', 'ageing', 'file')
    readonly_fields = ('customer', 'email_sent_at', 'ageing', 'file')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class VendorPayoutAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'vendor_name', 'percentage', 'is_active')

    search_fields = ('vendor_name',)

    # fields = ('customer', 'vendor_name', 'percentage','is_active')

    def has_delete_permission(self, request, obj=None):
        return False


class EInvoicingAdmin(admin.ModelAdmin):
    list_display = ('id', 'invoice', 'irn', 'ack_no', 'ack_date', 'status', 'file')
    readonly_fields = list_display + ('response',)

    inlines = [EInvoicingEventAdmin]

    search_fields = ('invoice__invoice_number', 'irn', 'ack_no',)
    list_filter = ('status', ('ack_date', DateFilter))

    # fields = ('customer', 'vendor_name', 'percentage','is_active')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

class PartnerCustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'partner', 'is_active', 'enable_daily_sync', 'date_created')
    search_fields = ('customer__name',)

    readonly_fields = ('customer', 'email', 'partner_token', 'store_data',
        'partner', 'store_domain', 'store_data', 'fulfillment_service_data')

    list_filter = ('partner', 'is_active')

    def get_fieldsets(self, request, obj=None):
        if obj:
            fieldsets = (
                ('General', {
                    'fields': ('customer', 'partner', 'is_active', 'is_deleted', 'enable_daily_sync',
                               'is_wms_flow', 'store_domain', 'email', 'partner_token',
                               'location_id', 'delivery_code')
                }),
                ('Store Data', {
                    'fields': ('store_data', 'fulfillment_service_data')
                })
            )
        else:
            fieldsets = (
                ('General', {
                    'fields': ('customer', 'partner', 'is_active', 'is_deleted',
                               'is_wms_flow', 'store_domain', 'email', 'partner_token')
                }),
            )

        return fieldsets

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return request.user.is_superuser


class InvoiceUploadBatchAdmin(admin.ModelAdmin):

    model = InvoiceUploadBatch

    list_display = ('id', 'start_time', 'end_time', 'number_of_entries', 'successful_uploads',
                    'failed_uploads')

    fields = ('start_time', 'end_time', 'description', 'number_of_entries',
              'successful_uploads', 'failed_uploads', 'file')

    readonly_fields = fields

    search_fields = ['id']
    list_filter = [('start_time', DateRangeFilter)]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    actions = None


class SendInvoiceBatchAdmin(admin.ModelAdmin):

    model = SendInvoiceBatch

    list_display = ('id', 'start_time', 'end_time', 'number_of_entries', 'sent_to_customers',
                    'failed_to_send')

    fields = ('start_time', 'end_time', 'description', 'number_of_entries',
              'sent_to_customers', 'failed_to_send', 'file')

    readonly_fields = fields

    search_fields = ['id']
    list_filter = [('start_time', DateRangeFilter)]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    actions = None


from .subadmins.sku_admin import *
from .subadmins.invoice_admin import *
from .subadmins.product_admin import *
from .subadmins.organization import OrganizationUserAdmin, OrganizationOwnerAdmin
from .subadmins.customer_migration_history import CustomerMigrationHistoryAdmin

admin.site.register(CustomerAddress, CustomerAddressAdmin)
admin.site.register(VendorPayout, VendorPayoutAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(BusinessCustomer, BusinessCustomerAdmin)
admin.site.register(BusinessCustomerHubview, BusinessCustomerHubViewAdmin)
admin.site.register(BusinessCustomerConfigView, BusinessCustomerConfigViewAdmin)
admin.site.register(CustomerInvoiceConfig, CustomerInvoiceConfigAdmin)
admin.site.register(GPSEventPublishConfigView, GPSEventPublishConfigViewAdmin)
admin.site.register(GPSEventPublishConfig)
admin.site.register(InvoiceHeader)
admin.site.register(CustomerInvoice, CustomerInvoiceAdmin)
admin.site.register(InvoiceEntity, InvoiceEntityAdmin)
admin.site.register(InvoiceAggregateAttribute)
admin.site.register(InvoiceAggregation, InvoiceAggregationAdmin)
admin.site.register(InvoiceBatch, InvoiceBatchAdmin)
admin.site.register(InvoiceAdjustmentCategory, InvoiceAdjustmentCategoryAdmin)
admin.site.register(InvoiceRequest, InvoiceRequestAdmin)
admin.site.register(ReceivablelineItem, ReceivablelineItemAdmin)
admin.site.register(CustomerReceivable, CustomerReceivableAdmin)
admin.site.register(AuthenticationConfig)
admin.site.register(BasicAuthCredential)
admin.site.register(DriverRatingAggregate, DriverRatingAggregateAdmin)
admin.site.register(RatingCategory, RatingCategoryAdmin)
admin.site.register(RatingConfig, RatingConfigAdmin)
admin.site.register(InvoicePayment, InvoicePaymentAdmin)
admin.site.register(Partner, PartnerAdmin)
admin.site.register(SmsConfig, SmsConfigAdmin)
admin.site.register(DriverStatus)
admin.site.register(CustomerLedgerStatement, CustomerLedgerStatementAdmin)
admin.site.register(CustomerBalance, CustomerLedgerBalanceAdmin)
admin.site.register(CustomerEmailLog, CustomerEmailLogAdmin)
admin.site.register(CustomerDeliveryOption, CustomerDeliveryOptionAdmin)
admin.site.register(CustomerReportSchedule, CustomerReportScheduleAdmin)
admin.site.register(Report, ReportAdmin)
admin.site.register(UserReport, UserReportAdmin)
admin.site.register(PartnerCustomer, PartnerCustomerAdmin)
admin.site.register(EInvoicing, EInvoicingAdmin)
admin.site.register(CashFreeBeneficiary, CashFreeBeneficiaryAdmin)
admin.site.register(CashFreePayoutHistory, CashFreePayoutHistoryAdmin)
admin.site.register(InvoiceUploadBatch, InvoiceUploadBatchAdmin)
admin.site.register(SendInvoiceBatch, SendInvoiceBatchAdmin)
