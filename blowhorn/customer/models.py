from datetime import timedelta
from django.utils import timezone
from django.conf import settings
from django.core.validators import RegexValidator, MinLengthValidator
from django.core.exceptions import ValidationError
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.db import transaction
from django.db.models import Sum, Q
from django.utils.translation import ugettext_lazy as _

import phonenumbers
from recurrence.fields import RecurrenceField
from phonenumber_field.modelfields import PhoneNumberField
from rest_framework import status
from rest_framework.response import Response
from organizations.abstract import AbstractOrganization, \
    AbstractOrganizationUser, AbstractOrganizationOwner
from organizations.fields import SlugField
from organizations.signals import user_added
from blowhorn.oscar.apps.address.abstract_models import AbstractAddress
from blowhorn.oscar.models.fields import NullCharField
from postgres_copy import CopyManager
from blowhorn.utils.datetime_function import DateTime

from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.models import BaseModel
from blowhorn.users.models import User
from blowhorn.common.helper import CommonHelper
from blowhorn.document.utils import generate_file_path
from blowhorn.contract.constants import SERVICE_TAX_CATEGORIES, EXEMPT
from blowhorn.customer.constants import MEMBER_STATUSES, MEMBER_INVITED, MEMBER_ACTIVE, \
    STATUS_HOLD, ORDER_STATUSES_REASON_MAPPING, REPORT_CATEGORIES, AGGREGATION_CATEGORIES
from blowhorn.order.const import PAYMENT_STATUS_PAID, BLOWHORN_WALLET
from blowhorn.customer.constants import HTTP_REQUEST_TYPES, \
    AUTHENTICATION_TYPES

OPENING_CREDIT_LIMIT_DATE = '2018-08-01'


def generate_report_file_path(instance, filename):
    """
    Returns the file path as per the defined directory structure.
    """
    REPORT_UPLOAD_STRUCTURE = "reports/{customer}/{city}/{period}/{duration}/{event}/{date}/report/{file_name}"

    diff = instance.end_date - instance.start_date
    if diff == timedelta(days=1):
        duration = 'daily'
    elif diff == timedelta(days=7):
        duration = 'weekly'
    else:
        duration = 'monthly'

    name = instance.report_type.description.lower().replace(' ', '-')
    showdate = instance.end_date.strftime('%-d-%B-%Y')

    file_name = str(date.today()) + "/" + \
                CommonHelper().get_unique_friendly_id() + "/" + filename.upper()

    return REPORT_UPLOAD_STRUCTURE.format(
        customer=instance.customer,
        city=instance.city,
        period=instance.period,
        duration=duration,
        event=name,
        date=showdate,
        file_name=file_name
    ).replace("//", "/")


class CustomerManager(models.Manager):

    def upsert_user(self, email, name, phone_number, is_mobile_verified=None,
                    password=1234, user_id=None, last_otp_value=None, last_otp_sent_at=None):

        now = timezone.now()
        customer_phone = phonenumbers.parse(
            phone_number,
            settings.COUNTRY_CODE_A2
        )
        email = email.lower() if email else email
        if not user_id:
            user = User(
                phone_number=customer_phone,
                email=email,
                is_active=True,
                last_login=now,
                name=name,
                last_otp_value=last_otp_value,
                last_otp_sent_at=last_otp_sent_at
            )
        else:
            user = User.objects.get(pk=user_id)
            # Update the fields which are allowed to be updated from admin
            user.name = name
            user.email = email
            user.phone_number = customer_phone

        if is_mobile_verified is not None:
            user.is_mobile_verified = is_mobile_verified
        else:
            user.is_mobile_verified = self.get_mobile_verification_status(user)

        if not user_id:
            user.set_password(password) if password else user.set_password('1234')

        user.save()
        return user

    def add_otp(self, customer, otp, remarks=None):
        CustomerOTP.objects.create(customer=customer, otp=otp, remarks=remarks)

    def get_mobile_verification_status(self, user):
        user = User.objects.get(pk=user.id)
        return user.is_mobile_verified

    def create_customer(self, email=None, phone_number='', **kwargs):
        if not phone_number and not email:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Either email or phone number is required.')

        if phone_number:
            phone_number = phonenumbers.parse(
                phone_number, settings.COUNTRY_CODE_A2)
            if not email:
                email = '%s@%s' % (phone_number.national_number,
                                   settings.DEFAULT_DOMAINS.get('customer'))

        password = kwargs.pop('password', '')
        name = kwargs.pop('name', '')
        is_active = kwargs.pop('is_active', False)
        is_email_verified = kwargs.pop('is_email_verified', False)
        source = kwargs.pop('source', None)

        user = User.objects.create_user(
            email=email.lower(),
            phone_number=phone_number,
            password=password,
            name=name,
            is_active=is_active,
            is_email_verified=is_email_verified
        )

        if user:
            customer_rating = self.upsert_customer_rating(
                num_of_feedbacks=kwargs.pop('num_of_feedbacks', 0),
                cumulative_rating=kwargs.pop('cumulative_rating', 0)
            )

            customer = Customer(
                user=user,
                customer_rating=customer_rating,
                source=source,
                slug=email
            )
            customer.save()

            response = {
                'user': user,
                'customer': customer
            }
            return Response(status=status.HTTP_200_OK, data=response)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='failed to create user.')

    def upsert_customer_rating(self, num_of_feedbacks=0, cumulative_rating=0):
        customer_rating = CustomerRating(
            num_of_feedbacks=num_of_feedbacks,
            cumulative_rating=cumulative_rating
        )
        customer_rating.save()
        return customer_rating


class CustomerReportSchedule(models.Model):
    customer = models.ForeignKey('customer.Customer', on_delete=models.CASCADE)

    city = models.ForeignKey('address.City', blank=True, null=True, on_delete=models.CASCADE)

    service_schedule = RecurrenceField(_('Service Calendar'),
                                       help_text=_("Reports Auto Generation Calendar Rule"))

    period = models.CharField(_("Aggregation Interval"), max_length=10, blank=False, default="Daily",
                                choices=AGGREGATION_CATEGORIES)

    subscribed_users = models.ManyToManyField('customer.AccountUser',
                                              related_name="subscribed_users", blank=True)

    report_type = models.ForeignKey('customer.Report', on_delete=models.CASCADE)

    def __str__(self):
        return '%s-%s' % (self.customer, self.report_type)


class UserReport(models.Model):
    customer = models.ForeignKey('customer.Customer', on_delete=models.CASCADE)
    city = models.ForeignKey('address.City', blank=True, null=True, on_delete=models.CASCADE)
    period = models.CharField(_("Aggregation Interval"), max_length=10, blank=True,
                        null=True, choices=AGGREGATION_CATEGORIES)
    start_date = models.DateField(_('Start Date'))
    end_date = models.DateField(_('End Date'))
    send_status = models.BooleanField(_('Send Status'), default=False)
    report_type = models.ForeignKey('customer.Report', on_delete=models.CASCADE)
    file = models.FileField(_("Uploaded File"),
                            upload_to=generate_report_file_path,
                            max_length=256, blank=True, null=True)

    def __str__(self):
        return '%s-%s' % (self.customer, self.report_type)

    class Meta:
        verbose_name = _('User Report')
        verbose_name_plural = _('User Reports')


class Report(models.Model):
    name = models.CharField(_("Report Name"), max_length=200,
                            help_text=_("Name display on the dashboard"))
    code = models.CharField(_("Code"), max_length=100)
    is_active = models.BooleanField('Is Active', default=True)
    description = models.CharField(_("Description"), max_length=300,
                                   help_text=_("Part of mail subject"))
    category = models.CharField(_("Category"), max_length=25, blank=False,
                                choices=REPORT_CATEGORIES)
    system = models.ForeignKey('customer.System', on_delete=models.PROTECT, null=True, blank=True)
    display_inline = models.BooleanField('Display Embed Report', default=False)
    width = models.IntegerField('Display Width', default=20)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Report Type')
        verbose_name_plural = _('Report Types')


class OpeningBalance(models.Model):
    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.CASCADE)
    date = models.DateField(db_index=True)
    amount = models.DecimalField(
        _("Opening Balance"), max_digits=10, decimal_places=2, default=0)
    comments = models.TextField(null=True, blank=True)

    objects = models.Manager()
    copy_data = CopyManager()

    def __str__(self):
        return u'%s %s' % (self.customer, self.date)

    class Meta:
        verbose_name = _('Opening Balance')
        verbose_name_plural = _('Opening Balance')


class CustomerRating(models.Model):
    """
    Aggregated rating for the customer based on driver feedback
    """
    cumulative_rating = models.DecimalField(
        _("Aggregated rating for the customer"),
        max_digits=3, decimal_places=2)

    num_of_feedbacks = models.IntegerField(
        _("Num of feedbacks received"), default=0)

    def __str__(self):
        return str(self.cumulative_rating)


class CustomerAddressManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('country')


class CustomerAddress(AbstractAddress):
    """
    Customer Address from  Oscar AbstractAddress
    """
    # customer = models.ForeignKey('customer.Customer', on_delete=models.PROTECT)
    geopoint = models.PointField(null=True, blank=True)
    objects = CustomerAddressManager()

    class Meta:
        verbose_name = _("Customer's address")
        verbose_name_plural = _("Customer addresses")


CUSTOMER_CATEGORY_INDIVIDUAL = u'Individual'
CUSTOMER_CATEGORY_NON_ENTERPRISE = u'Non Enterprise'
CUSTOMER_CATEGORY_TRANSITION = u'Transition'
CUSTOMER_CATEGORY_ENTERPRISE = u'Enterprise'
CUSTOMER_CATEGORIES = (
    (CUSTOMER_CATEGORY_INDIVIDUAL, _('Individual')),
    (CUSTOMER_CATEGORY_NON_ENTERPRISE, _('Non Enterprise')),
    (CUSTOMER_CATEGORY_TRANSITION, _('Transition')),
    (CUSTOMER_CATEGORY_ENTERPRISE, _('Enterprise')),
)

CUSTOMER_SOURCE_WEBSITE = u'Website'
CUSTOMER_SOURCE_ADMIN = u'Admin'
CUSTOMER_SOURCE_MOBILE = u'Android/iOS'

# class CustomerCategory(models.Model):
#     """
#     Customer category
#     =================
#         whether Enterprise
#         or Non-Enterprise
#         if Non-Enterprise
#             whether C2C
#             or SME
#     """
#     category = models.CharField(
#         _("Customer Category"), max_length=25, blank=False,
#         choices=customercategories, default="Individual")

# def __str__(self):
#     return self.category

ENTITY_TYPES = (
    (u'Private Limited Company', _('Private Limited Company')),
    (u'Public Limited Company', _('Public Limited Company')),
    (u'Body Corporate', _('Body Corporate')),
    (u'Partnership', _('Partnership')),
    (u'Trust/ Charities/ NGOs', _('Trust/ Charities/ NGOs')),
    (u'Financial Institution (FI)', _('Financial Institution (FI)')),
    (u'Foreign Institutional Investor (FII)', _(
        'Foreign Institutional Investor (FII)')),
    (u'Hindu Undivided Family (HUF)', _('Hindu Undivided Family (HUF)')),
    (u'Association of persons', _('Association of persons')),
    (u'Bank', _('Bank')),
    (u'Government Body', _('Government Body')),
    (u'Non-Government Organisation', _('Non-Government Organisation')),
    (u'Defence Establishment', _('Defence Establishment')),
    (u'Body of Individuals', _('Body of Individuals')),
    (u'Society', _('Society')),
    (u'Limited Liability Partnership', _('Limited Liability Partnership')),
    (u'Others', _('Others')),
)

INDUSTRY_SEGMENTS = (
    (u'Aviation', _('Aviation')),
    (u'Agriculture', _('Agriculture')),
    (u'Auto Components', _('Auto Components')),
    (u'Automobiles', _('Automobiles')),
    (u'Bio fuels', _('Bio fuels')),
    (u'Biotechnology', _('Biotechnology')),
    (u'Capital goods', _('Capital goods')),
    (u'Chemicals', _('Chemicals')),
    (u'Defence', _('Defence')),
    (u'Drugs and Pharmaceuticals', _('Drugs and Pharmaceuticals')),
    (u'E Commerce - Food Tech', _('E Commerce - Food Tech')),
    (u'E Commerce - Grocery Tech', _('E Commerce - Grocery Tech')),
    (u'E Commerce - Large goods', _('E Commerce - Large goods')),
    (u'E Commerce - Others', _('E Commerce - Others')),
    (u'E Commerce - Retail', _('E Commerce - Retail')),
    (u'Education and Training', _('Education and Training')),
    (u'Energy', _('Energy')),
    (u'Family business', _('Family business')),
    (u'Fast moving consumer goods', _('Fast moving consumer goods')),
    (u'Food processing', _('Food processing')),
    (u'Gems and Jewellary', _('Gems and Jewellary')),
    (u'Health care', _('Health care')),
    (u'Housing', _('Housing')),
    (u'Information and Communication technology', _(
        'Information and Communication technology')),
    (u'Infrastructure', _('Infrastructure')),
    (u'Innovation', _('Innovation')),
    (u'Leather and Leather products', _('Leather and Leather products')),
    (u'Logistics', _('Logistics')),
    (u'Manufacturing', _('Manufacturing')),
    (u'Media and Entertainment', _('Media and Entertainment')),
    (u'Micro, Medium and Small scale industries', _(
        'Micro, Medium and Small scale industries')),
    (u'Oil and Gas', _('Oil and Gas')),
    (u'Power', _('Power')),
    (u'Real estate', _('Real estate')),
    (u'Retail', _('Retail')),
    (u'Safety and Security', _('Safety and Security')),
    (u'Sports goods', _('Sports goods')),
    (u'Telecom & Broadband', _('Telecom & Broadband')),
    (u'Tourism and Hospitality', _('Tourism and Hospitality')),
    (u'Urban development', _('Urban development')),
    (u'Water', _('Water')),
    (u'IT & ITES', _('IT & ITES')),
    (u'Cement', _('Cement')),
    (u'Furniture and Home Furnishing', _('Furniture and Home Furnishing')),
    (u'Home Services', _('Home Services')),
)


class Customer(AbstractOrganization, BaseModel):
    """
    Main Customer Model for Blowhorn
    """
    slug = SlugField(max_length=200, blank=True, null=True, editable=True,
                     populate_from='user__email',
                     help_text=_("The name in all lowercase, suitable for URL identification"))

    source = models.CharField(max_length=50, null=True, blank=True, default=CUSTOMER_SOURCE_WEBSITE)

    awb_code = models.CharField(_("Customer Short Identifier for AWB"), max_length=10,
                                    blank=True, null=True,
                                    help_text=_("Please enter 3 letter unique code"))

    current_address = models.ForeignKey(
        CustomerAddress,
        null=True,
        blank=True,
        verbose_name='Current Address',
        on_delete=models.PROTECT,
        related_name='current_address')

    customer_category = models.CharField(
        _("Customer Category"), max_length=25, blank=False,
        choices=CUSTOMER_CATEGORIES, default=CUSTOMER_CATEGORY_INDIVIDUAL, db_index=True)

    customer_rating = models.ForeignKey(
        CustomerRating,
        null=True,
        blank=True,
        verbose_name='Customer Rating',
        on_delete=models.PROTECT,
        help_text=_("Applicable only for Individual customers"))

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        blank=False,
        verbose_name=_('User details'),
    )

    api_key = models.CharField(
        _('API Key'),
        max_length=30,
        unique=True,
        null=True,
        blank=True,
    )

    is_shipment_servicetype = models.BooleanField(
        _('Shipment ServiceType'), default=False)

    is_monthly_invoice = models.BooleanField(
        _('Monthly Invoice'), default=False, db_index=True)

    legalname = NullCharField(
        _("Legal Name"), max_length=255, blank=True, null=True,
        db_index=True)

    expected_delivery_hours = models.TimeField(
        _('Expected Delivery Hours'),
        help_text=_("Maximum Hours for Order Delivery"),
        null=True, blank=True)

    objects = CustomerManager()

    # Fields applicable for non-Individuals
    cin = models.CharField(_('CIN'), max_length=21, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{1}\d{5}[A-Za-z]{2}\d{4}[A-Za-z]{3}\d{6}$',
            message=_('First Alphabet, next 5 digits,'
                      'next 2 Alphabet, next 4 digits,'
                      'next 3 Alphabet, next 6 digits,'
                      'fixed length is 21')),
            MinLengthValidator(21)])
    pan = models.CharField(_('PAN'), max_length=10, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{5}\d{4}[A-Za-z]{1}$',
            message=_('First 5 digits, next 4 digits,'
                      'last Alphabet, fixed length is 10')),
            MinLengthValidator(10)])
    tan = models.CharField(_('TAN'), max_length=10, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{4}\d{5}[A-Za-z]{1}$',
            message=_('First 4 digits Alphabet,'
                      'next 5 digits,'
                      'last Alphabet, fixed length is 10')),
            MinLengthValidator(10)])

    # Below field will be removed. Please use the method `has_tax_registered` instead.
    is_tax_registered = models.BooleanField(
        _('Tax Registered'), default=False, db_index=True)

    service_tax_category = models.CharField(
        _('Tax Category'), max_length=12, choices=SERVICE_TAX_CATEGORIES, default=EXEMPT, db_index=True)

    entity_type = models.CharField(_('Entity Type'), max_length=60,
                                   null=True, blank=True, choices=ENTITY_TYPES,
                                   db_index=True)

    industry_segment = models.CharField(_('Industry Segment'), max_length=60,
                                        null=True, blank=True,
                                        choices=INDUSTRY_SEGMENTS,
                                        db_index=True)

    cities = models.ManyToManyField('address.City')

    head_office_address = models.ForeignKey(CustomerAddress, null=True,
                                            blank=True,
                                            related_name="head_office_address",
                                            on_delete=models.PROTECT,
                                            verbose_name='Head Office Address')

    credit_period = models.IntegerField(_("Credit Period in Days"), default=0)
    credit_amount = models.IntegerField(_("Credit Amount in Rs"), default=0)
    enterprise_credit_period = models.IntegerField(_("Credit Period for enterprise customer in Days"), null=True,
                                                   blank=True)
    enterprise_credit_amount = models.IntegerField(_("Credit Amount for enterprise customer in INR"), null=True,
                                                   blank=True)
    enterprise_credit_applicable = models.BooleanField(
        _('Whether Credit Policy Applicable?'), default=False, db_index=True)

    credit_email_recipients = ArrayField(models.CharField(max_length=255), blank=True, null=True)

    advance_taken = models.DecimalField(
        _("Advance Money Taken"), decimal_places=2, default=0, max_digits=20)

    name = models.CharField(
        _('Name / Brand'), max_length=255, null=True, blank=True, db_index=True)

    notification_url = models.CharField(_('Notification URL'), max_length=500,
                                        null=True, blank=True)

    ecode = models.CharField(_('Ecode'), max_length=500, null=True, blank=True,
                             help_text=_(
                                 "Integration DSP code - for e.g ind.bh"))

    ifsc_code = models.CharField(
        _("Branch Code/IFSC Code for the bank"), max_length=11,
        default='-',
        help_text=_("Accounts IFSC (standard IFSC format) - length 11, first four bank IFSC and 5th digit 0)"))

    bank_account = models.CharField(_('Bank Account No'), max_length=18, null=True, blank=True, db_index=True)

    customer_specific_info = models.TextField(_("Customer Specific Information"),
                                              null=True, blank=True,
                                              help_text=_("Can define process/SOP for this customer over here"))
    is_gstin_mandatory = models.BooleanField(default=True)

    show_pod_in_dashboard = models.BooleanField(default=True)

    has_call_masking_pin = models.BooleanField(default=False,
                                               help_text=_("Does customer provides call masking pin for orders?"))

    copy_data = CopyManager()

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')

    def __str__(self):
        return self.name or ''

    # @staticmethod
    # def autocomplete_search_fields():
    #     return 'name', 'legalname'

    def clean(self):
        # Dont allow to save customers who are non-individuals and don't have
        # legal name
        if self.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL \
            and not self.legalname:
            raise ValidationError(
                _('Non Individual Customers need a legal name'))

        if self.enterprise_credit_applicable and not (self.enterprise_credit_period or self.enterprise_credit_amount):
            raise ValidationError(
                _("Enterprise Customer Credit Policy Turned On, please set the required fields")
            )

    def save(self, *args, **kwargs):
        # Generate API keys for Non-Individual customers
        if not self.api_key and self.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL:
            self.api_key = CommonHelper.generate_api_key()

        from blowhorn.common.middleware import current_request
        _request = current_request()
        history_list = []
        current_user = _request.user if _request else ''

        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')
                verbose_name = field.__dict__.get('verbose_name')

                if field_name in ['credit_period', 'credit_amount', 'advance_taken', 'enterprise_credit_applicable',
                                  'enterprise_credit_period', 'enterprise_credit_amount', 'service_tax_category']:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                    if old_value != current_value:
                        history_list.append(
                            CustomerHistory(
                                field=field_name, verbose_name=verbose_name, old_value=str(old_value),
                                new_value=current_value, user=current_user, customer=old))

            CustomerHistory.objects.bulk_create(history_list)

        super(Customer, self).save(*args, **kwargs)

    def add_balance_to_wallet(self, amount=0, payment_source=None, payment_response=None,
                              payment_type='razorpay', transaction_order=None):
        from blowhorn.customer.tasks import send_transaction_update_to_customer  # Circular dependency

        trans_query = {}
        if payment_type in ['razorpay', 'payfast']:
            trans_query.update({"source_id": payment_source.id})

        elif payment_type == 'paytm':
            trans_query.update({"wallet_source_id": transaction_order.id})

        prev_record = CustomerLedgerStatement.objects.filter(**trans_query).first()
        if prev_record:
            data = {
                'payment_mode': payment_type,
                'ledger_id': 0
            }
            return data

        with transaction.atomic():
            last_transaction = CustomerLedgerStatement.objects \
                .select_for_update(nowait=True) \
                .filter(customer=self) \
                .order_by('-transaction_time') \
                .first()
            if last_transaction:
                balance = float(last_transaction.balance) + (
                    float(payment_source.amount_debited) if payment_response else amount)
            else:
                balance = float(payment_source.amount_debited) if payment_response else amount
            query = {
                "customer_id": self.id,
                "amount": float(payment_source.amount_debited) if payment_response else amount,
                "balance": balance,
                "response": payment_response if payment_response else None
            }
            if payment_type in ['razorpay', 'payfast']:
                query.update({"source_id": payment_source.id})

            elif payment_type == 'paytm':
                query.update({"wallet_source_id": transaction_order.id})

            ledger_statement = CustomerLedgerStatement.objects.create(**query)
            data = {
                'payment_mode': payment_type,
                'ledger_id': ledger_statement.id
            }
        send_transaction_update_to_customer.apply_async((data,), )

    def get_customer_balance(self):
        ledger = CustomerLedgerStatement.objects \
            .filter(customer=self) \
            .order_by('-transaction_time') \
            .first()

        return float(ledger.balance) if ledger else 0

    def get_last_transactions(self):
        transactions = []
        statements = CustomerLedgerStatement.objects \
            .filter(customer=self) \
            .order_by('-transaction_time')

        for statement in statements:
            timezone_date_time_obj = DateTime().get_locale_datetime(statement.transaction_time).isoformat()
            if statement.order:
                if statement.amount < 0:
                    desc = "Paid for Order #%s" % statement.order
                else:
                    desc = "Amount Reversed for Order #%s" % statement.order
            else:
                desc = "Money Added to Wallet"

            transactions.append({
                "number": str(statement.order or (
                    statement.source.wallet_reference if statement.source else statement.wallet_source)),
                "transaction_time": timezone_date_time_obj,
                "desc": desc,
                "amount": float(statement.amount),
                "balance": float(statement.balance),
                "id": statement.id,
                "transaction_type": "Debit" if statement.amount <= 0 else "Credit",
                "status": statement.status  # if statement else ''
            })
        return transactions

    def put_order_amount_on_hold(self, order):
        cust_balance = self.get_customer_balance()
        if cust_balance >= order.estimated_cost:
            CustomerLedgerStatement.objects.create(order=order, amount=-float(order.estimated_cost), customer=self,
                                                   balance=(cust_balance - float(order.estimated_cost)),
                                                   status=STATUS_HOLD)
            return True
        return False

    def create_customer_ledger_entry(self, order, debit, cust_balance, description):
        CustomerLedgerStatement.objects.create(order=order, amount=-debit, customer=self,
                                               balance=(cust_balance - float(debit)),
                                               description=description)
        order.payment_status = PAYMENT_STATUS_PAID
        order.amount_paid_online = order.total_payable
        order.payment_mode = BLOWHORN_WALLET
        return order

    def debit_order_amount(self, order):
        if order.payment_status == PAYMENT_STATUS_PAID:
            return order

        total_debit = 0
        with transaction.atomic():
            debit_statements = CustomerLedgerStatement.objects.filter(customer=self, order=order)
            for statement in debit_statements:
                total_debit += float(statement.amount)
                if statement.status == STATUS_HOLD:
                    statement.status = None
                    statement.save()
            order.amount_paid_online = abs(total_debit)

            if -total_debit != order.total_payable:
                debit = float(order.total_payable) + total_debit
                cust_balance = self.get_customer_balance()
                if debit < 0:
                    # refund excess debit
                    description = 'Credit Remaining Amount'
                    order = self.create_customer_ledger_entry(order, debit, cust_balance, description)
                elif debit > 0:
                    if cust_balance >= debit:
                        description = 'Paid for order %s' % (order)
                        order = self.create_customer_ledger_entry(order, debit, cust_balance, description)
                else:
                    # no debit has happened
                    if order.total_payable <= cust_balance:
                        order_amount = float(order.total_payable)
                        description = 'Paid for order %s' % (order)
                        order = self.create_customer_ledger_entry(order, order_amount, cust_balance, description)
            elif -total_debit == order.total_payable:
                order.payment_status = PAYMENT_STATUS_PAID
                order.amount_paid_online = order.total_payable
        return order

    def reverse_order_amount(self, order):
        """
        reverse the money puton hold when the order is cancelled
        :param order:
        :return: True
        """
        if order.payment_mode == BLOWHORN_WALLET:
            with transaction.atomic():
                debit_statements = CustomerLedgerStatement.objects.filter(customer=self, order=order,
                                                                          status=STATUS_HOLD)
                for statement in debit_statements:
                    cust_balance = self.get_customer_balance()
                    CustomerLedgerStatement.objects.create(order=order, amount=-float(statement.amount), customer=self,
                                                           balance=(cust_balance + float(-statement.amount)),
                                                           description='Payment Reversal')
                    statement.status = None
                    statement.save()

    def is_SME(self):
        """
        Check if customer is SME. Returns `true` on success, otherwise `false`.
        """

        return self.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL

    def is_non_individual(self):
        """
        Check if customer is not an individual. Returns `true` on success, otherwise `false`.
        """

        return self.customer_category != CUSTOMER_CATEGORY_INDIVIDUAL

    def is_individual(self):
        """
        Check if customer is an Individual. Returns `true` on success, otherwise `false`.
        """

        return self.customer_category == CUSTOMER_CATEGORY_INDIVIDUAL

    def is_owner(self, user):
        """
        Returns True is user is the organization's owner, otherwise false
        """
        return hasattr(self, 'user') and self.user == user

    def get_unpaid_orders(self):
        from blowhorn.order.models import Order
        from blowhorn.utils.functions import utc_to_ist

        query = Q(
            customer=self,
            order_type=settings.C2C_ORDER_TYPE,
            status=StatusPipeline.ORDER_DELIVERED,
            total_payable__gt=0,
            # pickup_datetime__date__gte=OPENING_CREDIT_LIMIT_DATE
        )
        neg_query = Q(
            payment_status=PAYMENT_STATUS_PAID
        )
        orders = Order.objects.prefetch_queryset(
            query=query, neg_query=neg_query).only('id', 'total_payable',
                                                   'number', 'pickup_datetime')
        if not orders:
            return {
                'pending_amount': 0,
                'unpaid_orders': [],
                'can_place_order': True
            }

        pending_amount = 0
        unpaid_orders = []
        for order in orders:
            order_data = {}
            pending_amount += float(order.total_payable)
            order_data['id'] = order.id
            order_data['number'] = order.number
            order_data['total_payable'] = float(order.total_payable)
            order_data['completed_at'] = utc_to_ist(order.pickup_datetime).strftime(settings.APP_DATE_FORMAT)
            unpaid_orders.append(order_data)

        return {
            'pending_amount': round(pending_amount, 2),
            'unpaid_orders': unpaid_orders,
            'can_place_order': self.is_eligible_for_booking()
        }

    def get_total_pending_amount(self):
        """
        Get pending amount to be paid by customer for c2c/booking orders
        """
        from blowhorn.order.models import Order
        pending_amount = Order.objects.filter(
            customer=self,
            order_type=settings.C2C_ORDER_TYPE,
            status=StatusPipeline.ORDER_DELIVERED,
            total_payable__gt=0,
            pickup_datetime__date__gte=OPENING_CREDIT_LIMIT_DATE
        ).exclude(payment_status=PAYMENT_STATUS_PAID).aggregate(
            pending_amount=Sum('total_payable'))['pending_amount']

        return float(pending_amount) if pending_amount else 0.0

    def get_credit_amount_eligibility(self):
        """
        Checks whether customer is eligible to book orders based on his credit amount configured
        """
        pending_amount = self.get_total_pending_amount()
        return pending_amount <= (self.enterprise_credit_amount or 0), pending_amount

    def get_credit_days_eligibility(self):
        """
        Checks whether customer is eligible to book orders based on his credit days configured
        """
        from blowhorn.order.models import Order
        return not Order.objects.filter(
            customer=self,
            order_type=settings.C2C_ORDER_TYPE,
            status=StatusPipeline.ORDER_DELIVERED,
            total_payable__gt=0,
            pickup_datetime__date__gte=OPENING_CREDIT_LIMIT_DATE,
            pickup_datetime__date__lte=(timezone.now() - timedelta(days=self.enterprise_credit_period or 0)).date()
        ).exclude(payment_status=PAYMENT_STATUS_PAID).exists()

    def is_eligible_for_booking(self):
        """
        Checks if a customer does not have unpaid Orders and if so check credit days
        """
        credit_amount_eligibility, pending_amount = self.get_credit_amount_eligibility()
        return credit_amount_eligibility and self.get_credit_days_eligibility()

    def get_payment_details(self, send_slack_notification=False):
        from blowhorn.order.models import Order
        total_pending_amount = Order.objects.filter(
            customer=self,
            order_type=settings.C2C_ORDER_TYPE,
            status=StatusPipeline.ORDER_DELIVERED,
            total_payable__gt=0
        ).exclude(payment_status=PAYMENT_STATUS_PAID).aggregate(pending_amount=Sum('total_payable'))['pending_amount']

        total_pending_amount = float(total_pending_amount) if total_pending_amount else 0.0

        credit_amount_eligibility, pending_amount = self.get_credit_amount_eligibility()
        credit_period_eligibility = self.get_credit_days_eligibility()
        can_place_order = credit_amount_eligibility and credit_period_eligibility

        if not can_place_order and send_slack_notification:
            from blowhorn.customer.tasks import send_credit_limit_slack_notification
            if credit_amount_eligibility:
                pending_amount = 0
            send_credit_limit_slack_notification.delay(self.id, pending_amount,
                                                       credit_period_eligibility)
        return {
            'pending_amount': total_pending_amount,
            'can_place_order': can_place_order
        }

    def _check_booking_window_time(self, booking_time=None, lat=None, lon=None,
                                   geopoint=None, city=None):
        from blowhorn.address.utils import get_city_from_geopoint
        from blowhorn.utils.functions import utc_to_ist

        if not city:
            if not geopoint:
                geopoint = CommonHelper.get_geopoint_from_latlong(lat, lon)
            city = get_city_from_geopoint(geopoint, field='coverage_pickup')

        if not booking_time:
            booking_time = timezone.now()
        booking_time = utc_to_ist(booking_time).time()

        if city:
            return city.start_time <= booking_time <= city.end_time
        return False

    def _check_booking_window_time_UTC(self, booking_time=None, lat=None, lon=None,
                                       geopoint=None, city=None):
        from blowhorn.address.utils import get_city_from_geopoint
        if not city:
            if not geopoint:
                geopoint = CommonHelper.get_geopoint_from_latlong(lat, lon)
            city = get_city_from_geopoint(geopoint, field='coverage_pickup')

        if not booking_time:
            booking_time = timezone.now()
        booking_time = booking_time.time()

        if city:
            return city.start_time <= booking_time <= city.end_time
        return False

    @property
    def gst_info(self):
        return CustomerTaxInformation.objects.filter(customer=self).first()

    def get_gst_info(self, state=None):

        query = Q(customer=self)
        query &= Q(state=state) if state else Q()
        tax_info = CustomerTaxInformation.objects.select_related('state').filter(customer=self).first()

        return tax_info

    def has_tax_registered(self):
        """
        Returns a boolean corresponds to whether customer has registered for tax.
        """

        return bool(self.gst_info) and bool(self.gst_info.gstin)

    def update_name(self, name):
        """
        :param name: String (Name of the customer)
        """
        self.name = name
        user = self.user
        user.name = name
        with transaction.atomic():
            user.save()
            self.save()

    def get_or_add_user(self, user, **kwargs):
        """
        :param user: instance of User
        :param kwargs: extra-info (like is_admin)
        :return: tuple (OrganizationUser, created)
            created: Boolean
        """
        is_admin = kwargs.pop('is_admin', False)
        new_user = kwargs.pop('created', False)

        users_count = self.users.all().count()
        defaults = {'is_admin': is_admin}
        if not new_user:
            defaults['status'] = 'old_invited'

        org_user, created = self._org_user_model.objects. \
            get_or_create(organization=self,
                          user=user,
                          defaults=defaults)
        if users_count == 0:
            self._org_owner_model.objects \
                .create(organization=self, organization_user=org_user)
        if created:
            # User added signal
            user_added.send(sender=self, user=user)
        return org_user, created


class CustomerHistory(models.Model):
    """
    Capturing the customer history
    """
    customer = models.ForeignKey('customer.Customer', on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    verbose_name = models.CharField(_('Field Changed'), max_length=250)
    old_value = models.CharField(max_length=100, null=True, blank=True)
    new_value = models.CharField(max_length=100, null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Customer History')
        verbose_name_plural = _('Customer History')


class CashFreeBeneficiary(models.Model):
    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT, blank=False, null=True)

    ifsc_code = models.CharField(
        _("Branch Code/IFSC Code for the bank"), max_length=11,
        default='-',
        help_text=_("Accounts IFSC (standard IFSC format) - length 11, first four bank IFSC and 5th digit 0)"))

    bank_account = models.CharField(_('Bank Account No'), max_length=18, null=True, db_index=True)

    bene_id = models.CharField(_('Bene Id'), max_length=9999, unique=True, null=True, db_index=True)

    created_at = models.DateTimeField(auto_now=True)

    response_status = models.CharField(max_length=30, null=True, blank=True)

    remarks = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        verbose_name = _('CashFree Beneficiary')
        verbose_name_plural = _('CashFree Beneficiary')

    def __str__(self):
        return str(self.customer)


class CashFreePayoutHistory(models.Model):
    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT, blank=False, null=True)

    beneId = models.CharField(_('BeneId'), max_length=21,
                              null=True, blank=True, unique=True)

    transferId = models.CharField(_('Transfer Id'), max_length=40, unique=True, null=True, db_index=True)

    amount = models.FloatField(_('Amount'), null=True, blank=True, default=0)

    created_at = models.DateTimeField(auto_now=True)

    ifsc_code = models.CharField(
        _("Branch Code/IFSC Code for the bank"), max_length=11,
        default='-',
        help_text=_("6 Digit Branch Code/First 4 Bank Code, then 0 and then next 6 Alphanumeric "
                    "characters(Branch Code)"))

    bank_account = models.CharField(_('Bank Account No'), max_length=18, null=True, db_index=True)

    referenceId = models.CharField(max_length=100, null=True, blank=True, db_index=True)

    response_status = models.CharField(max_length=30, null=True, blank=True)

    remarks = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        verbose_name = _('CashFree Payout History')
        verbose_name_plural = _('CashFree Payout History')

    def __str__(self):
        return str(self.amount)


class CustomerOTP(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)

    otp = models.CharField(_('OTP'), max_length=4, null=True, blank=True)

    sent_at = models.DateTimeField(_('OTP Sent At'), auto_now_add=True)

    remarks = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        verbose_name = _('User OTP')
        verbose_name_plural = _('User OTP')

    def __str__(self):
        return str(self.otp)


class CustomerFeedback(BaseModel):
    """
    Customer Feedback for Blowhorn
    """
    feedback = models.TextField(
        _("Customer feedback"), blank=True,
        help_text=_("Customer feedback about blowhorn"), default="No feedback")

    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT, blank=False, null=True)

    extra_details = models.TextField(null=True,
                                     help_text=_("Extra info like app version, code"))


class Route(models.Model):
    """
    Customer's saved route information.
    """
    name = models.CharField(
        _("Route name"), max_length=100)

    city = models.CharField(
        _("city"), max_length=100, null=True)

    distance = models.DecimalField(
        _("distance"), max_digits=15, decimal_places=2, null=True, blank=True)

    contact_name = models.CharField(
        _("Contact name"), max_length=100, null=True, blank=True)

    contact_mobile = PhoneNumberField(
        _("Mobile Number"), blank=True, null=True,
        help_text=_("Contact person's primary mobile number e.g. +91{10 digit mobile number}"))

    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT, blank=False)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), default=False, null=True, blank=True)

    class Meta:
        unique_together = ("name", "customer", "city")

    def __str__(self):
        return ('%s-%s') % (self.name, self.customer)


class RouteStop(AbstractAddress):
    """
    Customer's saved route stop information
    """
    route = models.ForeignKey(
        Route,
        on_delete=models.CASCADE, null=True, related_name="stops")

    seq_no = models.IntegerField()

    address_name = models.CharField(
        _("Address name"), max_length=100, null=True, blank=True)

    contact_name = models.CharField(
        _("Contact name"), max_length=100, null=True, blank=True)

    contact_mobile = PhoneNumberField(
        _("Mobile Number"), blank=True, null=True,
        help_text=_("Contact person's primary mobile number e.g. +91{10 digit mobile number}"))

    geopoint = models.PointField()

    title = None

    first_name = None

    last_name = None

    search_text = None


class CustomerTaxInformation(models.Model):
    """
    Customer GST Information
    """
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    state = models.ForeignKey('address.State', on_delete=models.PROTECT)
    gstin = models.CharField(_("GSTIN"), max_length=50)
    invoice_address = models.ForeignKey(CustomerAddress, null=True, blank=True,
                                        on_delete=models.PROTECT)

    legalname = models.CharField(_("Legal Name"), max_length=50, null=True, blank=True)

    division = models.ForeignKey(
        'customer.CustomerDivision', null=True, blank=True, on_delete=models.PROTECT)

    is_gstin_validated = models.BooleanField('Is GSTIN validated', default=False)

    class Meta:
        unique_together = ("customer", "state", "division")


class CustomerDivision(BaseModel):
    """
    Customer Divisions (sub-groups) that exist.
    An extra identification based on divisions that exist in
    enterprise customers where company works individually with all of them.
    """
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ("customer", "name")


class CustomerContact(models.Model):
    name = models.CharField(
        _("Contact name"), max_length=100, null=True)

    mobile = PhoneNumberField(
        _("Mobile Number"), blank=True, null=True,
        help_text=_("Contact person's primary mobile number e.g. +91{10 digit mobile number}"))

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        db_index=True
    )

    # division = models.ForeignKey(CustomerDivision, related_name='old_division', null=True, blank=True, on_delete=models.PROTECT)
    division = models.ManyToManyField(CustomerDivision, blank=True)

    # city = models.ForeignKey('address.City', related_name='old_city', null=True, blank=True, on_delete=models.PROTECT)
    city = models.ManyToManyField('address.City', blank=True)

    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)

    role = models.CharField(
        _("Organisation Role"), max_length=100, blank=True)

    remarks = models.CharField(
        _("Remarks"), max_length=255, null=True, blank=True)


class CustomerSalesRepresentative(CustomerContact):
    sales_representatives = models.ManyToManyField('users.User')


class ShipmentAlertConfiguration(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    time_since_creation = models.IntegerField(_("Mins Since Creation"),
                                              default=0)
    critical_status = models.CharField(_("Critical Status"), max_length=50,
                                       choices=StatusPipeline.SHIPMENT_ALERT_LEVELS,
                                       default=StatusPipeline.NORMAL)
    alert_colours = models.CharField(_("Colours"), max_length=50,
                                     choices=StatusPipeline.ALERT_LEVELS_COLOURS)

    def __str__(self):
        return str(self.customer)

    class Meta:
        unique_together = ("customer", "time_since_creation",
                           "critical_status", "alert_colours")
        verbose_name = _('Shipment Alert Configuration')
        verbose_name_plural = _('Shipment Alert Configuration')


class SmsConfig(BaseModel):
    customer = models.ManyToManyField(Customer)

    contract = models.ManyToManyField('contract.Contract')

    is_active = models.BooleanField('Is Active', default=False)

    event = models.CharField(_('Trigger message on order event'),
                             choices=StatusPipeline.MSG_EVENTS, max_length=50,
                             blank=False)

    stop_status = models.CharField(_('Order stop status'),
                                   choices=StatusPipeline.STOP_MSG_STATUS,
                                   max_length=50, blank=True, null=True,
                                   help_text="Stop status for order event Out-For-Delivery")

    msg_template = models.TextField(_('Message Template'), null=False,
                                    blank=False)

    template_id = models.CharField(_('DLT Template ID'),max_length=50,blank=True, null=True,
                                   help_text="Template ID should be same as updated on DLT portal")

    txn_type = models.CharField(_('Type of Transaction'), max_length=50,
                              default='TXN',
                              help_text=_('Type of Transaction'))

    def __str__(self):
        return u'%s-%s' % (self.id, self.event)

    class Meta:
        verbose_name = _('Customer SMS Config')
        verbose_name_plural = _('Customer SMS Config')


class BasicAuthCredential(BaseModel):
    username = models.CharField(max_length=100)

    password = models.CharField(max_length=100)

    def __str__(self):
        return str(self.id)

    def to_dict(self):
        """ Every Authentication type model should have a `to_dict` to return
        the credentials alone of type dict.
        """

        return {
            'username': self.username,
            'password': self.password
        }


class AuthenticationConfig(BaseModel):
    """ A generic Authentication config handling model for customers to access
    any BlowHorn service.
    """

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    authentication_type = models.IntegerField(choices=AUTHENTICATION_TYPES)

    # As of now, this is a ForeignKey to BasicAuthCredential,
    # In future this should be a GenericForeignKey if multiple auth types are supported.

    credentials = models.ForeignKey(BasicAuthCredential, on_delete=models.CASCADE)

    enabled = models.BooleanField(default=True)

    def __str__(self):
        return str(self.customer) + " - " + \
               str(self.get_authentication_type_display())


class GPSEventPublishConfig(BaseModel):
    customer = models.ForeignKey(
        Customer, related_name='gps_event_publish_config', on_delete=models.CASCADE)

    contracts = models.ManyToManyField('contract.Contract')

    api_endpoint = models.URLField()

    http_request_type = models.IntegerField(choices=HTTP_REQUEST_TYPES)

    interval = models.IntegerField(
        default=300, help_text="Interval in seconds.")

    max_records_per_driver = models.IntegerField(
        default=-1,
        help_text="Max no. of records per driver in one request. "
                  "-1 denotes no limit."
    )

    authentication_config = models.ForeignKey(
        AuthenticationConfig, null=True, blank=True, on_delete=models.CASCADE)

    comments = models.TextField(null=True, blank=True)

    enabled = models.BooleanField(
        default=False,
        help_text="Indicates whether the configuration is enabled ?"
    )

    def __str__(self):
        return str(self.customer)

    class Meta:
        verbose_name = _('GPS Event Publishing Configuration')
        verbose_name_plural = _('GPS Event Publishing Configurations')


class AccountUserGroup(BaseModel):
    """
    role; finance user / operation manager
    """
    name = models.CharField(max_length=100)

    organization = models.ForeignKey(Customer,on_delete=models.PROTECT)

    permissions = models.ManyToManyField(
        'customer.UserPermission',
        verbose_name=_('permissions'),
        blank=True,
    )

    permission_levels = models.ManyToManyField(
        'customer.PermissionLevelStore',
        verbose_name=_('Permission Level'),
        blank=True,
        help_text=_('Permissions level for this user.'),
        related_name="accountgroup_set",
        related_query_name="accountgroup",
    )

    class Meta:
        unique_together = ("organization", "name")

    def __str__(self):
        return '%s | %s' % (self.name, self.organization)

    class Meta:
        verbose_name = _('Organization Group')
        verbose_name_plural = _('Organization Groups')


class System(BaseModel):
    """
    WMS, TMS, etc
    """
    name = models.CharField(max_length=100)

    code = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('System')
        verbose_name_plural = _('Systems')

class PermissionLevel(BaseModel):
    """
    TMS: Division, City, Hub
    WMS: Hub, Client, Site
    """
    name = models.CharField(max_length=255, unique=True)

    model_name = models.CharField(max_length=250,
        null=True, blank=True,
        help_text=_('Model name to query data from.')
    )

    position = models.PositiveIntegerField(_('Position'), default=0,
        help_text=_('Position in hierarchy'))

    system = models.ForeignKey(System, on_delete=models.PROTECT)

    def __str__(self):
        return '%s | %s | %s' % (self.system, self.position, self.name)

    class Meta:
        verbose_name = _('Permission Level')
        verbose_name_plural = _('Permission Levels')
        unique_together = [['system', 'name']]
        ordering = ['system','name', 'position']

class UserModule(BaseModel):
    """
    Available modules in customer dashboard
    """
    name = models.CharField(max_length=100, help_text=_('Human readable name. e.g., Shipment Orders, Configuration'))

    code = models.CharField(max_length=100, help_text=_('Auto-generated based on name. e.g.: shipment_orders'))

    system = models.ForeignKey(System, on_delete=models.PROTECT, null=True, blank=True)

    display_order = models.PositiveSmallIntegerField(
        verbose_name=_('Display Order'),
        default=1,
        help_text=_('Order of display in dashboard')
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        #self.name = self.title()
        if not self.code:
            self.code = self.name.replace(' ', '_').lower()

        super(UserModule, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Module')
        verbose_name_plural = _('Modules')
        unique_together = [['system', 'code']]
        ordering = ['system__name', 'code']


class UserPermission(BaseModel):
    """
    === Django Permission
    """

    module = models.ForeignKey(UserModule, on_delete=models.PROTECT)

    name = models.CharField(_('name'), max_length=255)

    codename = models.CharField(_('codename'), max_length=100)

    url = models.CharField(max_length=255, null=True, blank=True, default='/dashboard',
        help_text=_('URL of the page or module. e.g.: /dashboard/overview'))

    def __str__(self):
        return '%s | %s' % (self.module, self.name)

    def save(self, *args, **kwargs):
        self.name = self.name.title()
        if not self.codename:
            self.codename = self.name.replace(' ', '_').lower()

        super(UserPermission, self).save(*args, **kwargs)

    class Meta:
        unique_together = [['module', 'codename']]
        ordering = ['module__name', 'codename']
        verbose_name = _('Module Feature')
        verbose_name_plural = _('Module Features')


class PermissionLevelStore(BaseModel):

    module = models.ForeignKey(UserModule, on_delete=models.PROTECT)

    permission_level = models.ForeignKey(PermissionLevel, on_delete=models.PROTECT)

    value = ArrayField(models.PositiveIntegerField(), blank=True, null=True, default=list)

    class Meta:
        ordering = ['permission_level__name']
        verbose_name = _('Permission Level Store')
        verbose_name_plural = _('Permission Level Store')


class AccountPermissionsMixin(models.Model):
    groups = models.ManyToManyField(
        AccountUserGroup,
        verbose_name=_('groups'),
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions '
            'granted to each of their groups.'
        ),
        related_name="accountuser_set",
        related_query_name="accountuser",
    )

    user_permissions = models.ManyToManyField(
        UserPermission,
        verbose_name=_('user permissions'),
        blank=True,
        help_text=_('Specific permissions for this user.'),
        related_name="accountuser_set",
        related_query_name="accountuser",
    )

    permission_levels = models.ManyToManyField(
        PermissionLevelStore,
        verbose_name=_('Permission Level'),
        blank=True,
        help_text=_('Permissions level for this user.'),
        related_name="accountuser_set",
        related_query_name="accountuser",
    )

    class Meta:
        abstract = True


class AccountUser(AbstractOrganizationUser, AccountPermissionsMixin):
    status = models.CharField(_('Status'), max_length=50,
                              choices=MEMBER_STATUSES,
                              default=MEMBER_INVITED.lower(),
                              help_text=_('Status of member'))

    last_invite_sent_on = models.DateTimeField(null=True, blank=True,
                                               auto_now_add=True)

    system = models.ForeignKey(System, on_delete=models.PROTECT, null=True, blank=True)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), default=False, null=True, blank=True)

    def __str__(self):
        return '%s' % self.user

    @property
    def is_active(self):
        return self.status == MEMBER_ACTIVE


class AccountOwner(AbstractOrganizationOwner):
    status = models.CharField(_('Status'), max_length=50,
                              choices=MEMBER_STATUSES,
                              default=MEMBER_INVITED.lower(),
                              help_text=_('Status of Owner'))

    def __str__(self):
        return '%s' % self.organization_user


class Partner(BaseModel):
    current_address = models.ForeignKey(
        CustomerAddress,
        null=True,
        blank=True,
        verbose_name='Partner Current Address',
        on_delete=models.PROTECT,
        related_name='partner_current_address')

    is_shipping_partner = models.BooleanField(default=False)

    category = models.CharField(
        _("Partner Category"), max_length=25, blank=False,
        choices=CUSTOMER_CATEGORIES, default=CUSTOMER_CATEGORY_INDIVIDUAL, db_index=True)

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        blank=False,
        verbose_name=_('User details'),
    )

    api_key = models.CharField(
        _('API Key'),
        max_length=30,
        unique=True,
        null=True,
        blank=True,
    )

    legalname = NullCharField(
        _("Legal Name"), max_length=255, blank=True, null=True,
        db_index=True)

    pan = models.CharField(_('PAN'), max_length=10, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{5}\d{4}[A-Za-z]{1}$',
            message=_('First 5 digits, next 4 digits,'
                      'last Alphabet, fixed length is 10')),
            MinLengthValidator(10)])
    tan = models.CharField(_('TAN'), max_length=10, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{4}\d{5}[A-Za-z]{1}$',
            message=_('First 4 digits Alphabet,'
                      'next 5 digits,'
                      'last Alphabet, fixed length is 10')),
            MinLengthValidator(10)])

    # Below field will be removed. Please use the method `has_tax_registered` instead.
    is_tax_registered = models.BooleanField(
        _('Tax Registered'), default=False, db_index=True)

    ip_address = models.TextField(_('IP Address List String'), null=True, blank=True)

    service_tax_category = models.CharField(
        _('Tax Category'), max_length=12, choices=SERVICE_TAX_CATEGORIES, default=EXEMPT, db_index=True)

    objects = models.Manager()

    def save(self, *args, **kwargs):
        # Generate API keys for Non-Individual customers
        if not self.api_key:
            self.api_key = CommonHelper.generate_api_key()

        super(Partner, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.legalname)


class CustomerEmailLog(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING)
    file = models.FileField(_("Uploaded File"),
                            upload_to=generate_file_path,
                            max_length=256, blank=True, null=True)
    email_sent_at = models.DateTimeField(auto_now_add=True)
    ageing = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.customer.name)


class PartnerCustomer(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT,
                                    null=True, blank=True)
    partner = models.ForeignKey(Partner, on_delete=models.PROTECT)
    email = models.CharField(_("Email"), max_length=250, null=True, blank=True)
    partner_token = models.CharField(_('Partner Token'), max_length=50, null=True, blank=True)
    store_domain = models.CharField(_('Store Domain'), max_length=50, null=True, blank=True)
    store_data = models.TextField(_('Store Json'), null=True, blank=True)
    fulfillment_service_data = models.TextField(_('Fulfillment Service Json'), null=True, blank=True)
    is_active = models.BooleanField(_('Is Active'), default=False)
    is_deleted = models.BooleanField(_("Is Deleted"), default=False)
    is_wms_flow = models.BooleanField(_('Is WMS Flow'), default=False)
    enable_daily_sync = models.BooleanField(_('Enable Daily Backend Sync'), default=False)
    delivery_code = models.CharField(_("Code Based Delivery"), max_length=250, null=True, blank=True)
    location_id = models.CharField(_('Location ID'), max_length=255, null=True, blank=True)
    date_created = models.DateTimeField(_("Date created"), default=timezone.now)

    def __str__(self):
        return "%s-%s-%s" % (self.customer.name, self.partner.legalname, self.is_active)

    class Meta:
        verbose_name = _('Partner Customer')
        verbose_name_plural = _('Partner Customers')


class CustomerDeliveryOption(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING,
                                 null=True, blank=True)
    status = models.CharField(_("Status"),
                              choices=ORDER_STATUSES_REASON_MAPPING,
                              max_length=250)
    option = models.CharField(_("Option"), max_length=300)
    is_active = models.BooleanField(_('Is Active'), default=True)


class PartnerTaxInformation(models.Model):
    """
    Customer GST Information
    """
    partner = models.ForeignKey('customer.partner', on_delete=models.PROTECT)
    state = models.ForeignKey('address.State', on_delete=models.PROTECT)
    gstin = models.CharField(_("GSTIN"), max_length=50)
    invoice_address = models.ForeignKey(CustomerAddress, null=True, blank=True,
                                        on_delete=models.PROTECT)

    class Meta:
        unique_together = ("partner", "state")
        verbose_name = _('Partner tax Information')
        verbose_name_plural = _('Partner tax Information')


class VendorPayout(BaseModel):
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING, null=True, blank=True)

    partner = models.OneToOneField('customer.Partner', on_delete=models.DO_NOTHING, null=True, blank=True)

    vendor_name = models.CharField(
        _('Vendor for payout, should be same on cashfree dashboard'), max_length=150, unique=True)

    percentage = models.DecimalField(
        _("His share in percentage"), max_digits=10, decimal_places=2, default=0)

    is_active = models.BooleanField(
        _('Is Active'), default=False)

    def __str__(self):
        return self.vendor_name

    class Meta:
        verbose_name = _('Vendor payout')
        verbose_name_plural = _('Vendor payout')


class CustomerBankDetails(BaseModel):

    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)

    bank_name = models.CharField(_('Bank Name'), max_length=100)

    ifsc_validator = RegexValidator(
        regex=r'^[A-Za-z]{4}[0]\w{6}$|^\d{6}$',
        message=_('6 Digit Branch Code/First 4 Alphabets(Bank Code), then 0 and next 6 '
                  'Alphanumeric characters(Branch Code)'))

    ifsc_code = models.CharField(
        _("Branch Code/IFSC Code for the bank"), max_length=11,
        validators=[ifsc_validator],
        help_text=_("6 Digit Branch Code/First 4 Bank Code, then 0 and then next 6 Alphanumeric "
                    "characters(Branch Code)"))

    branch_name = models.CharField(_('Branch Name'), max_length=100)

    account_name = models.CharField(_('Account Holder Name'), max_length=100)

    bank_account_number = models.BinaryField(_('Bank Account Number'),
                                             null=True, blank=True)

    is_active = models.BooleanField(default=False, null=True, blank=True)

    is_deleted = models.BooleanField(_("Is Deleted?"), default=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        if type(self.bank_account_number) == str:
            self.prepare_for_save()
        super().save(*args, **kwargs)

    def prepare_for_save(self):
        from blowhorn.customer.utils import encrypt_account_number
        # This is the first time this bank detail instance is being saved.
        self.bank_account_number = encrypt_account_number(self.bank_account_number)

    def to_json(self):
        from blowhorn.customer.utils import decrypt_account_number
        account_number = decrypt_account_number(self.bank_account_number)
        account_data = {
            'id': self.id,
            'account_number': 'XXXXXXXXXX%s' % account_number[-4:],
            'ifsc_code': self.ifsc_code,
            'bank_name': self.bank_name,
            'account_name': self.account_name,
            'branch_name': self.branch_name,
            'is_active': self.is_active,
            'created_by': '%s' % self.created_by,
            'created_date': self.created_date,
            'modified_by': '%s' % self.modified_by,
            'modified_date': self.modified_date
        }
        return account_data


class CustomerBankDetailsHistory(models.Model):

    customer_bank_details = models.CharField(max_length=50, null=True, blank=True)
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    reason = models.CharField(max_length=100, null=True, blank=True)
    action = models.CharField(max_length=15)
    action_owner = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
    created_time = models.DateTimeField(auto_now_add=True)


def get_partner_from_api_key(api_key):
    return Partner.objects.filter(api_key=api_key).first()


from .submodels.invoice_models import *
from .submodels.sku_models import *
from .submodels.sub_models import *
from .submodels.partner_models import *
from .submodels.customer_migration_history import CustomerMigrationHistory

"""
If written on the top of the file, then our custom model fields won't defined
in our model wont override the fields from  oscar
"""
from blowhorn.customer.submodels.sub_models import *
from blowhorn.oscar.apps.customer.models import *  # noqa
