import json
import logging
from datetime import datetime
from django.conf import settings
from django.db.models import Prefetch
from django.utils import timezone

from blowhorn.common.firebase_operations import broadcast_to_firebase
from blowhorn.customer.constants import DASHBOARD_UPDATE_URLS, \
    CD_ORDER_DETAILS, CD_ORDERLINES
from blowhorn.customer.serializers import FleetShipmentOrderListSerializer
from blowhorn.order.models import Order, WayPoint, Event, OrderLine
from blowhorn.order.serializers.order_line import OrderLineSerializer
from blowhorn.trip.models import Stop
from config.settings import status_pipelines
from blowhorn.utils.datetime_function import DateTime

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DashboardRealtimeUpdate(object):

    def get_order(self, params):
        order_qs = Order.objects.select_related(
            'driver__user', 'shipping_address', 'city', 'hub', 'driver', )
        order_qs = order_qs.prefetch_related(
            'documents'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.all().extra(
                            select={
                                'end_time_null': 'end_time is null '},
                            order_by=['end_time']),
                        to_attr='stops_list'
                    )
                ),
                to_attr='waypoints'
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'stops',
                queryset=Stop.objects.all().extra(
                    select={
                        'end_time_null': 'end_time is null '},
                    order_by=['end_time']),
                to_attr='stops_list'
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'event_set',
                queryset=Event.objects.filter(
                    status__in=[
                        status_pipelines.ORDER_DELIVERED,
                        status_pipelines.ORDER_RETURNED,
                        status_pipelines.UNABLE_TO_DELIVER]
                ), to_attr='events'
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'order_lines',
                queryset=OrderLine.objects.all().select_related('sku').order_by('item_sequence_no')
            )
        )
        return order_qs.get(**params)

    def get_orderline_qs(self, params):
        qs = OrderLine.objects.filter(**params) \
            .select_related('order', 'created_by', 'modified_by', 'sku')
        return qs.order_by('-modified_date')

    def push_order_details_update(self, order_id):
        """
        :param order_id: pk of Order
        :return:
        """
        order = self.get_order(dict(id=order_id))
        if order.order_type not in [settings.SHIPMENT_ORDER_TYPE] or \
            (order.customer_contract.customer and
             order.customer_contract.customer.api_key == settings.FLASH_SALE_API_KEY):
            return
        order_json = FleetShipmentOrderListSerializer(order).data
        time_func = DateTime()
        order_json['broadcast_time'] = timezone.now()
        for key, val in order_json.items():
            if isinstance(val, datetime):
                order_json[key] = time_func.get_locale_datetime(val).isoformat()
        url = DASHBOARD_UPDATE_URLS[CD_ORDER_DETAILS].format(
                customer_id=order.customer_id)
        broadcast_to_firebase(url, order_json)

    def push_orderline_update(self, order_id):
        """
        :param order_id: pk of Order
        :return:
        """
        qs = self.get_orderline_qs(dict(order_id=order_id))
        orderline_json = OrderLineSerializer(qs, many=True).data
        broadcast_to_firebase(
            DASHBOARD_UPDATE_URLS[CD_ORDERLINES],
            orderline_json
        )
