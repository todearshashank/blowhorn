import re
import operator
import logging
import pytz
import math
import copy
import random
from decimal import Decimal
from django.db.models import F, Q, ExpressionWrapper, DateTimeField, QuerySet
from datetime import datetime, timedelta, date as datetime_date
from django.utils import timezone
from django.contrib.humanize.templatetags.humanize import intcomma
from django.conf import settings
from django.core.files.base import ContentFile
from collections import defaultdict
from django.db import transaction
from django.db.models import Sum
from django.db.models.functions import Greatest

from blowhorn.oscar.core.loading import get_model
import simplejson as json
from num2words import num2words
from functools import reduce

from blowhorn.order.const import ORDER_INVOICING_FIELDS
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.models import Contract
from blowhorn.order.models import Order, OrderLine, Event
from blowhorn.order.helpers.order_helper import OrderSet
from blowhorn.trip.models import Trip, Stop
from blowhorn.customer.models import CustomerInvoice, InvoiceEntity, ContractInvoiceFragment, \
    InvoiceBatch, InvoiceAggregateAttribute, InvoiceAggregation, InvoiceTrip, SKU, InvoiceOrder, \
    InvoiceExpirationBatch, create_invoice_history, InvoiceHistory
from blowhorn.driver.payment_helpers.contract_helper import ContractHelper
from blowhorn.driver.payment_helpers.trips_helper import TripSet
from blowhorn.company.helpers.tax_helper import TaxHelper
from blowhorn.contract.contract_helpers.sell_rate_helper import SellRateHelper
from blowhorn.utils.functions import IST_OFFSET_HOURS, format_datetime
from blowhorn.customer.constants import (
    VEHICLE,
    SHIPMENT,
    CREATED,
    APPROVED,
    SUMMARY_PAGE,
    DETAIL_PAGE,
    SUMMARY_DETAIL_PAGE,
    EXPIRED,
    TAX_INVOICE,
    NUMBER_OF_DAYS_ELAPSED_FOR_MARKING_INVOICE_AS_EXPIRED,
    CUSTOMER_ACKNOWLEDGED,
    CUSTOMER_CONFIRMED,
    PAYMENT_DONE,
    REVENUE_THRESHOLD_DATE,
)
from blowhorn.common.utils import TimeUtils, generate_pdf
from blowhorn.company.models import Office
from blowhorn.contract.constants import (
    CONTRACT_TYPE_HYPERLOCAL,
    SERVICE_TAX_CATEGORIES_CODE_MAP,
    CONTRACT_STATUS_ACTIVE,
    GTA,
    BSS,
    SHIPMENT_SERVICE_ATTRIBUTES,
    DIMENSION_SERVICE_ATTRIBUTES,
    VEHICLE_SERVICE_ATTRIBUTES,
    CONTRACT_TYPE_SHIPMENT,
    BASE_PAYMENT_CYCLE_MONTH,
    VAS_CHARGES,
    VAS_SERVICE_ATTRIBUTE_SMS,
    VAS_SERVICE_ATTRIBUTE_CASH_HANDLING,
    VAS_SERVICE_ATTRIBUTE_CALL
)
from blowhorn.company.constants import CGST, SGST, IGST, VAT, COMPANY_HEADER_FOOTER_DATA
from blowhorn.users.models import User
from blowhorn.contract.constants import EXEMPT
from blowhorn.company.helpers.company_helper import get_company_bank_account_details

PARKING_CHARGES = 'Parking Charges'
TOLL_CHARGES = 'Toll Charges'


MEDIA_UPLOAD_STRUCTURE = getattr(
    settings, "MEDIA_UPLOAD_STRUCTURE", ""
)

DATE_FORMAT = "%d-%b-%Y"

CustomerTaxInformation = get_model('customer', 'CustomerTaxInformation')
SKU = get_model('customer', 'SKU')

shipment_attributes = ['Assigned',
                       'Delivered',
                       'Attempted',
                       'Rejected',
                       'Pickups',
                       'Returned',
                       'MPOS Packages',
                       'Cash Packages',
                       'Assigned - Attempted + Returned',
                       'Assigned - Attempted + Rejected',
                       'Assigned - Attempted',
                       'Assigned - Attempted + Returned + Rejected',
                       'Assigned -  Returned']


class InvoiceHelper(object):
    INVOICE_UPLOAD_PATH = "uploads/documents/invoices/{period}/{customer_id}/{invoice_identifier}.pdf"

    AGGREGATE_LEVEL_FIELD_MAPPING = {
        "City": "city",
        "Hub": "hub",
        "Driver": "driver",
        "Vehicle Class": "vehicle_class",
        "Vehicle": "vehicle",
        "Customer" : "customer"
    }

    SLAB_GROUP_MAPPING = {
        "contract.SellVehicleSlab": VEHICLE,
        "contract.SellShipmentSlab": SHIPMENT
    }

    expression_planned_start_time_ist = ExpressionWrapper(
        F('planned_start_time') + timedelta(hours=IST_OFFSET_HOURS), output_field=DateTimeField()
    )

    expression_date_placed_ist = ExpressionWrapper(
        F('updated_time') + timedelta(hours=IST_OFFSET_HOURS), output_field=DateTimeField()
    )

    aggregate_level_field_mapping = {
        "trip": {
            'city': 'order__city',
            'vehicle_class': 'vehicle__vehicle_model__vehicle_class',
            'hub': 'order__hub',
            'contract': 'customer_contract',
            'division': 'customer_contract__division',
            'contract_type': 'customer_contract__contract_type',
            'driver': 'invoice_driver',
            'vehicle': 'vehicle',
            'customer' : 'customer_contract__customer'
        },
        "order": {
            'city': 'city',
            'hub': 'hub',
            'contract': 'customer_contract',
            'division': 'division',
            'contract_type': 'customer_contract__contract_type',
            'driver': 'invoice_driver',
            'customer' : 'customer'
        }
    }

    aggregate_level_fields_non_relational = ['contract_type']

    DEFAULT_FILTER = {'id__gte': 0}

    aggregate_level_inline_aggregate_field_mapping = {
        "City": "city_id",
        "Hub": "hub_id",
        "Driver": "driver_id",
        "Vehicle Class": "vehicle_class_id",
        "Customer" : "customer_id"}

    def __init__(self, debug=False):
        self.debug = debug
        self.output_log = []

    def get_attribute_ordered(self):
        sku_category = SKU.objects.filter(include_in_sellshipmentslab=True).values_list('name', flat=True)
        attribute_ordered = ['days', 'trips', 'Kilometres', 'Hours', 'Minutes', PARKING_CHARGES, TOLL_CHARGES,
                                  VAS_SERVICE_ATTRIBUTE_SMS, VAS_SERVICE_ATTRIBUTE_CASH_HANDLING] + \
                                 list(dict(SHIPMENT_SERVICE_ATTRIBUTES).keys()) + list(sku_category) + \
                                 list(dict(DIMENSION_SERVICE_ATTRIBUTES).keys())
        return attribute_ordered

    def get_possible_invoice_groups(self, trips=None, orders=None, aggregate_levels=None, order_count=0,
                                    trip_count=0):
        """
        Returns various invoice groups for given `trips` at given `aggregate_levels`.
        """
        assert trip_count or order_count, "Either trips or orders should be passed."

        if not aggregate_levels or len(aggregate_levels) == 0:
            return [self.DEFAULT_FILTER]

        fields = []
        for aggregate_level in aggregate_levels:
            value_field = self.aggregate_level_field_mapping["trip" if trips else "order"].get(aggregate_level, None)
            if value_field:
                fields.append(value_field)
        if trip_count:
            return trips.values(*fields).distinct()
        if order_count:
            return orders.values(*fields).distinct()

    def _create_invoice_aggregate(self, invoice, invoice_group, calculation_level):

        if invoice_group == self.DEFAULT_FILTER:
            return InvoiceAggregation.objects.create(**{'invoice': invoice})

        inv_map = {v: k for k, v in
                   self.aggregate_level_field_mapping["trip" if calculation_level == "TRIP" else "order"].items()}

        data = {inv_map[k] + "_id": v for k, v in invoice_group.items()}

        data['invoice'] = invoice

        for field in self.aggregate_level_fields_non_relational:
            current_val = data.pop(field + "_id", None)
            if current_val:
                data[field] = current_val
        return InvoiceAggregation.objects.create(**data)

    def _get_invoice_groups(self, invoice, calculation_level):
        aggregates = invoice.batch.aggregates.values_list('name', flat=True)
        result = {}

        for aggregate in aggregates:
            key = self.aggregate_level_field_mapping["trip" if calculation_level == "TRIP" else "order"].get(aggregate)
            if aggregate in self.aggregate_level_fields_non_relational:
                value = getattr(invoice.aggregation, aggregate)
            else:
                value = getattr(invoice.aggregation, aggregate + "_id")
            if value:
                result[key] = value
        return result

    def _iprint(self, *args, sep=' ', end='\n', file=None):
        """
        Print any given statement on console and also adds it to log for later outputting if required.
        """
        print(*args, sep=' ', end='\n', file=None)
        self.output_log.append("".join([str(arg) for arg in args]))

    def _get_calculation_level(self, config):
        """
        Returns the calculation level basis the config (ORDER or TRIP).
        """

        if config.contracts.values_list('contract_type', flat=True).first() in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
            return "ORDER"
        return "TRIP"

    def __get_summary_records(self, invoice, config):
        """
        Returns the summary as various inlines of whole invoice calculation.
        Any logic for summary page changes should be handled here.
        """

        def __get_display_name(field, attribute):
            sku_category = SKU.objects.filter(include_in_sellshipmentslab=True).values_list('name', flat=True)
            if field == "type":
                if attribute in dict(SHIPMENT_SERVICE_ATTRIBUTES).keys():
                    return "Shipment"
                elif attribute in sku_category:
                    return "Sku"
                elif attribute in dict(VEHICLE_SERVICE_ATTRIBUTES).keys():
                    return "Vehicle"
                elif attribute == "days":
                    return "Service Charges"
                elif attribute == "base_pay":
                    return "Base Pay"
                elif attribute == VAS_SERVICE_ATTRIBUTE_SMS:
                    return "SMS"
                elif attribute == VAS_SERVICE_ATTRIBUTE_CASH_HANDLING:
                    return VAS_SERVICE_ATTRIBUTE_CASH_HANDLING
                elif attribute == "adjustment":
                    return "Others"
            elif field == "description":
                return attribute.title()
            return ""

        if config.extra_pages not in [SUMMARY_PAGE, SUMMARY_DETAIL_PAGE]:
            return []

        # Creating summary records for basic pay and service charges.
        records = []
        entities = InvoiceEntity.objects.filter(contract_invoice__invoice=invoice)
        entity_categories = entities.values_list('attribute', 'rate').distinct()
        for attribute, rate in entity_categories:
            entity_calc = entities.filter(attribute=attribute, rate=rate).aggregate(
                total_extra_quantity=Sum('extra_value'), total_quantity=Sum('value'), total_amount=Sum('amount'))
            records.append(
                {
                    'type': __get_display_name(field='type', attribute=attribute),
                    'description': __get_display_name(field='description', attribute=attribute),
                    'unit_price': rate,
                    'quantity': (entity_calc['total_extra_quantity'] if attribute != "days" else
                                 entity_calc['total_quantity']) or 1,
                    'total_amount': entity_calc['total_amount']
                }
            )

        total_base_pay = 0.0
        for contract_invoice in invoice.contract_invoices.all():
            total_base_pay += float(contract_invoice.charges_basic)

        if total_base_pay != 0:
            records.append(
                {
                    'type': __get_display_name(field='type', attribute='base_pay'),
                    'description': 'Total Base Pay',
                    'unit_price': '',
                    'quantity': '',
                    'total_amount': total_base_pay
                }
            )

        # Creating summary records for all invoice adjustments.
        adjustments = invoice.customerinvoiceadjustment_set.all()
        for adjustment in adjustments:
            records.append(
                {
                    'type': __get_display_name(field='type', attribute="adjustment"),
                    'description': __get_display_name(field='description', attribute=adjustment.category.name),
                    'unit_price': '-',
                    'quantity': '-',
                    'total_amount': adjustment.total_amount
                }
            )

        ordered_records = []
        preserve_order = ["Base Pay","Service Charges", "Vehicle", "Shipment", "Sku", "SMS",
                          VAS_SERVICE_ATTRIBUTE_CASH_HANDLING, "Others"]

        for category in preserve_order:
            for record in records:
                if record['type'] == category:
                    ordered_records.append(record)
        self._iprint("Summary records : ", ordered_records)
        return ordered_records

    @transaction.atomic
    def generate_invoice(
        self, customer, date_start, date_end, aggregate_levels=[], contract_ids=[], configs=None,
        invoice=None, days_count={}, invoice_request=None):
        """

        Invoice generation at any aggregate levels for any given date range.
        """
        base_pay_distribution_from_slabs = {}

        config = configs
        calculation_level = "TRIP"
        service_state, invoice_state = None, None
        random_spocs_email = None
        if config:
            calculation_level = self._get_calculation_level(config)
            if not len(contract_ids):
                contract_ids = list(configs.contracts.values_list('id', flat=True))
            service_state = config.service_state
            invoice_state = config.invoice_state

        assert service_state, "Service state is None. Aborting."
        assert invoice_state, "Invoice state is None, Aborting."
        assert contract_ids, "No contracts given. Aborting."

        self._iprint("Invoice State : %s | Service State : %s" % (invoice_state.name, service_state.name))

        self._iprint("Generate Invoice with inputs : ", customer, date_start, date_end, aggregate_levels)

        aggregates = []
        for aggregate_level in aggregate_levels:
            aggregate = InvoiceAggregateAttribute.objects.get(name=aggregate_level)
            aggregates.append(aggregate)

        date_start = TimeUtils.date_to_datetime_range(date_start)[0]
        date_end = TimeUtils.date_to_datetime_range(date_end)[1]

        datetime_start = date_start.astimezone(tz=pytz.utc)
        datetime_end = date_end.astimezone(tz=pytz.utc)

        if calculation_level == "TRIP":
            all_trips = Trip.objects.filter(
                customer_contract__customer=customer,
                status=StatusPipeline.TRIP_COMPLETED,
                planned_start_time__range=(
                    datetime_start, datetime_end),
                order__status__in=[StatusPipeline.ORDER_DELIVERED,
                                   StatusPipeline.DELIVERY_ACK],
            ).exclude(
                actual_start_time=None
            )
            total_count = all_trips.count()
            self._iprint("Total trips to be invoiced is ", total_count)
        elif calculation_level == "ORDER":
            order_ids = Event.objects.filter(order__customer=customer,
                            order__customer_contract__contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL],
                            time__range=(datetime_start, datetime_end),
                        ).exclude(status=StatusPipeline.ORDER_NEW).values_list('order_id', flat=True)
            all_orders = Order.def_obj.filter(
                id__in=order_ids,
                status__in=[StatusPipeline.ORDER_DELIVERED, StatusPipeline.DELIVERY_ACK, StatusPipeline.ORDER_RETURNED,
                            StatusPipeline.DRIVER_REJECTED, StatusPipeline.UNABLE_TO_DELIVER, StatusPipeline.ORDER_RETURNED_RTO],
            ).only(*ORDER_INVOICING_FIELDS)
            total_count = all_orders.count()

            self._iprint("Total orders to be invoiced is ", total_count)

        # Filtering out all contracts
        active_contracts_with_state = Contract.objects.filter(
            id__in=contract_ids, customer=customer, status=CONTRACT_STATUS_ACTIVE).only('id')

        user = getattr(getattr(self, 'request', None), 'user', User.objects.first())

        if not invoice:
            all_invoice_batches = InvoiceBatch.objects.filter(
                customer=customer, service_period_start=date_start, service_period_end=date_end
            )
            batch = None
            for invoice_batch in all_invoice_batches:
                if list(invoice_batch.aggregates.values_list('name', flat=True)) == list(aggregate_levels):
                    # We can't depend on aggregation level alone, as multiple config can exist with same aggregation level.
                    # So, we check if any invoice of batch have same config. if so, current batch invoices don't need to be created again.
                    a_batch_invoice = invoice_batch.customerinvoice_set.all().first()
                    if a_batch_invoice and a_batch_invoice.invoice_config == config:
                        batch = invoice_batch
                        break
            if not batch:
                batch = InvoiceBatch.objects.create(
                    customer=customer, service_period_start=date_start, service_period_end=date_end, requested_by=user,
                    invoice_request=invoice_request)
                self._iprint("Batch created with ID : ", batch.id)
            else:
                self._iprint("Batch already found with ID : ", batch.id)
        else:
            assert invoice.batch, "Invoice batch does not exist, cannot regenerate."
            batch = invoice.batch

        if aggregates:
            batch.customer = customer
            batch.service_period_start = date_start
            batch.service_period_end = date_end
            batch.save()
            batch.aggregates.add(*aggregates)

        invoices = []

        self._iprint("Invoices generating for state ", service_state, service_state.id, "\n")

        if calculation_level == "TRIP":
            trips = all_trips.filter(customer_contract__in=active_contracts_with_state)
            invoice_groups = self.get_possible_invoice_groups(trips=trips, aggregate_levels=aggregate_levels,
                                                              trip_count=total_count, order_count=0)

        elif calculation_level == "ORDER":
            orders = all_orders.filter(customer_contract__in=active_contracts_with_state).only(*ORDER_INVOICING_FIELDS)
            invoice_groups = self.get_possible_invoice_groups(orders=orders, aggregate_levels=aggregate_levels,
                                                              order_count=total_count, trip_count=0)

        if isinstance(invoice_groups, QuerySet):
            invoice_group_count = invoice_groups.count()
        else:
            invoice_group_count = len(invoice_groups)

        self._iprint(
            "Expected number of invoices to be generated for state : %s = %d" % (service_state, invoice_group_count))

        if invoice:
            invoice_groups = [self._get_invoice_groups(invoice, calculation_level)]

        hsn_code = None

        for invoice_group in invoice_groups:
            self._iprint("\n ======== INVOICE GROUP ", invoice_group, " ========")

            if not invoice:
                checklist = {}
                for k, v in invoice_group.items():
                    field_name = {v: k for k, v in self.aggregate_level_field_mapping[
                        "trip" if calculation_level == "TRIP" else "order"].items()}.get(k, None)
                    if field_name:
                        checklist[field_name] = v
                if not checklist:
                    checklist = self.DEFAULT_FILTER

                aggregate = InvoiceAggregation.objects.filter(**checklist).filter(
                    invoice__customer=customer, invoice__service_period_start=date_start.date(),
                    invoice__service_period_end=date_end.date(), invoice__status=CREATED,
                    invoice__invoice_configs=config,
                    invoice__bh_source_state=service_state
                ).last()
                if aggregate:
                    invoice = aggregate.invoice
                    self._iprint("********** Invoice already exists with ID ", invoice.id)
                else:
                    if isinstance(date_end, datetime):
                        invoice_date = date_end.date()
                    else:
                        invoice_date = date_end

                    created_by = invoice_request.created_by if invoice_request else User.objects.filter(
                        email=settings.CRON_JOB_DEFAULT_USER).first()

                    # check to see if status changed
                    updated_invoice_exists = CustomerInvoice.objects.filter(
                        invoice_date=invoice_date,
                        customer=customer, service_period_start=date_start.date(),
                        service_period_end=date_end.date(),
                        invoice_configs=config,
                        status=APPROVED).exists()

                    assert not updated_invoice_exists, "Please refresh the page as invoice status changed"

                    invoice = CustomerInvoice.objects.create(
                        invoice_date=invoice_date,
                        customer=customer, service_period_start=date_start.date(),
                        service_period_end=date_end.date(),
                        invoice_configs=config)
                    invoice.created_by = created_by
                    self._create_invoice_aggregate(invoice, invoice_group, calculation_level)
                    self._iprint("********** Invoice created with ID ", invoice.id)

            if 'customer_contract' in invoice_group:
                invoice_group.pop('customer_contract__contract_type', None)

            self._iprint(">>>> Calculation proceeding for invoice group ", invoice_group)
            if calculation_level == "TRIP":
                trips_invoice_group = trips.filter(**invoice_group)
                self._iprint("Number of trips :", trips_invoice_group.count())
                contracts_applicable = trips_invoice_group.values_list('customer_contract_id', flat=True).distinct()
            elif calculation_level == "ORDER":
                orders_invoice_group = orders.filter(**invoice_group).only(*ORDER_INVOICING_FIELDS)
                self._iprint("Number of orders :", orders_invoice_group.count())
                contracts_applicable = orders_invoice_group.values_list('customer_contract_id', flat=True).distinct()

            if contracts_applicable:
                spocs_user = Contract.objects.filter(
                    id__in=contracts_applicable,
                    spocs__is_active=True
                ).values_list('spocs__email', flat=True).first()
                if spocs_user:
                    random_spocs_email = spocs_user

            if random_spocs_email and not invoice.current_owner:
                due_date = invoice.service_period_end + timedelta(days=5)
                random_spocs_user = User.objects.get(email=random_spocs_email)
                invoice.current_owner = random_spocs_user
                invoice.due_date = due_date
                invoice.assigned_date = datetime_date.today()
                create_invoice_history('-', CREATED, invoice, random_spocs_user, due_date)

            total_basic, total_service = 0, 0

            total_by_service_type = defaultdict(int)

            # Delete all contract invoice fragments that are not applicable anymore to `invoice`.
            invoice.contract_invoices.exclude(contract_id__in=contracts_applicable).delete()

            applicable_trip_ids = []
            applicable_order_ids = []
            sell_rate = None
            invoice_fragment = None
            total_extra = 0
            for contract_id in contracts_applicable:
                extra_charges_slabs_fixed = 0
                self._iprint("Calculation for contract id ", contract_id)
                invoice_fragment, created = ContractInvoiceFragment.objects.get_or_create(
                    contract_id=contract_id, invoice=invoice)
                if not created:
                    invoice_fragment.entities.all().delete()
                    _days_count = invoice_fragment.days_counted
                else:
                    _days_count = days_count.get(invoice_fragment.id, (date_end - date_start).days + 1)

                contract = Contract.objects.get(id=contract_id)

                base_pay_extra_by_driver_kms, base_pay_extra_by_driver_hrs = 0, 0

                # Fetching the sell rate logic.
                helper_contract = ContractHelper(contract)
                sell_rate_trip_first = helper_contract.get_sell_rate(date_start.date())
                sell_rate_trip_last = helper_contract.get_sell_rate(date_end.date())
                if not sell_rate_trip_first or not sell_rate_trip_last:
                    self._iprint(
                        "Skipping contract ID as sell rate not exist for trip dates between ",
                        str(date_start), str(date_end))
                    continue
                if sell_rate_trip_first != sell_rate_trip_last:
                    self._iprint(
                        "NOTE : Multiple sell rate exists for given period. Defaulting to last applicable date for given period.")
                sell_rate = sell_rate_trip_last

                # Assumption : Picking last sell rate of the applicable period for calculation.
                helper_sell_rate = SellRateHelper(sell_rate, datetime_start, datetime_end, days_count=_days_count)

                charges_basic, charges_slab = 0, 0
                basic_calculation = {}
                if calculation_level == "TRIP":
                    trips_applicable = trips_invoice_group.filter(customer_contract_id=contract_id).annotate(
                        planned_start_time_ist=self.expression_planned_start_time_ist).order_by('actual_start_time')
                    self._iprint("Number of trips applicable for contract : ", trips_applicable.count())
                    applicable_trip_ids.extend(list(trips_applicable.values_list('id', flat=True)))
                    trip_set = TripSet(trips=trips_applicable, contract=contract)
                    charges_slab, slab_calculation, base_pay_distribution_from_slabs = \
                        helper_sell_rate.calculate_service_charges(date_start, date_end,
                                                                   trip_set=trip_set,
                                                                   base_pay_distribution_from_slabs=base_pay_distribution_from_slabs)

                    # base_pay_distribution_from_slabs = helper_sell_rate.base_pay_distribution_from_slabs

                    charges_basic, basic_calculation = helper_sell_rate.calculate_basic_pay(
                        trip_set, _days_count
                    )
                    self._iprint(
                        "CONTRACT : %s | Total trips : %s | CHARGES => BASIC : %s | SERVICE %s" %
                        (contract_id, trips_applicable.count(), charges_basic, charges_slab))

                    # Dumping basic calculation data in to database.
                    if basic_calculation:

                        base_pay_extra_by_driver = {}
                        if base_pay_distribution_from_slabs:
                            self._iprint("Extra basic from slabs `fixed` field : ",
                                         base_pay_distribution_from_slabs)
                            self._iprint("base_pay_distribution_from_slabs :", base_pay_distribution_from_slabs)
                            for slab_type, value in base_pay_distribution_from_slabs.items():
                                for (attribute_name, aggregate_level, aggregate_value,
                                     sell_rate_id), per_day_amount in value.items():
                                    field_name = self.aggregate_level_inline_aggregate_field_mapping.get(
                                        aggregate_level)
                                    if field_name == 'driver_id':
                                        if aggregate_value not in base_pay_extra_by_driver.keys():
                                            base_pay_extra_by_driver[aggregate_value] = defaultdict(int)
                                        base_pay_extra_by_driver[aggregate_value][attribute_name] += per_day_amount

                        entities = []
                        for driver_id, attrs_counted in basic_calculation['distribution_days'].items():
                            if helper_sell_rate.trip_wise_calculation:
                                attrs_counted = 1
                            base_pay_extra_by_driver_kms = float(
                                base_pay_extra_by_driver.get(driver_id, {}).get("Kilometres", 0))
                            base_pay_extra_by_driver_hrs = float(
                                base_pay_extra_by_driver.get(driver_id, {}).get("Hours", 0))

                            base_pay_extra_by_shipment_att = 0
                            for x in shipment_attributes:
                                base_pay_extra_by_shipment_att += float(
                                    base_pay_extra_by_driver.get(driver_id, {}).get(x, 0))

                            total_driver_amount = float(
                                basic_calculation['distribution_amount'].get(driver_id, 0)) + \
                                                  base_pay_extra_by_driver_kms * attrs_counted + \
                                                  base_pay_extra_by_driver_hrs * attrs_counted + \
                                                  base_pay_extra_by_shipment_att

                            if total_driver_amount > 0:
                                val = attrs_counted if base_pay_extra_by_shipment_att == 0 else 0
                                entities.append(
                                    InvoiceEntity(
                                        driver_id=driver_id,
                                        attribute=basic_calculation.get('attribute_actual', 'days'),
                                        value=math.ceil(val) if helper_sell_rate.is_ceiling_applicable else val,
                                        rate=basic_calculation['amount_per_day'],
                                        slab_fixed_amount_kilometers=base_pay_extra_by_driver_kms * attrs_counted,
                                        slab_fixed_amount_hours=base_pay_extra_by_driver_hrs * attrs_counted,
                                        amount=total_driver_amount,
                                        sell_rate=sell_rate,
                                        contract_invoice=invoice_fragment
                                    )
                                )
                            extra_charges_slabs_fixed += float(base_pay_extra_by_driver.get(driver_id, {}).get(
                                "Kilometres", 0)) * attrs_counted + float(base_pay_extra_by_driver.get(driver_id, {}).get(
                                "Hours", 0)) * attrs_counted + base_pay_extra_by_shipment_att
                        if entities:
                            InvoiceEntity.objects.bulk_create(entities)

                elif calculation_level == "ORDER":
                    orders_applicable = orders_invoice_group.filter(customer_contract_id=contract_id).annotate(
                        planned_start_time_ist=self.expression_date_placed_ist).order_by('updated_time')
                    self._iprint("Number of orders applicable for contract ", orders_applicable.count())
                    applicable_order_ids.extend(list(orders_applicable.values_list('id', flat=True)))
                    order_set = OrderSet(orders, orders_applicable=orders_applicable)
                    stops_ids = Stop.objects.filter(order__in=orders).values_list('id', flat=True)
                    trips_applicable_ids = list(
                        set(Trip.objects.filter(stops__in=stops_ids).values_list('id', flat=True)))
                    trips_applicable = Trip.objects.filter(id__in=trips_applicable_ids).annotate(
                        planned_start_time_ist=self.expression_planned_start_time_ist)
                    trip_set = TripSet(trips=trips_applicable, contract=contract)

                    charges_basic, basic_calculation = helper_sell_rate.calculate_basic_pay(
                        trip_set, _days_count
                    )
                    charges_slab, slab_calculation, calculate_service_charges, fixed_amt = \
                        helper_sell_rate.calculate_service_charges(date_start, date_end, order_set=order_set)

                    charges_basic += fixed_amt
                    invoice.fixed_amt = fixed_amt

                if slab_calculation:
                    entities = []
                    for slab_charges in slab_calculation:
                        for entry in slab_charges:
                            field_name = self.AGGREGATE_LEVEL_FIELD_MAPPING.get(entry['slab']['aggregate_level'])
                            date = entry.get('date', None)
                            if date:
                                date = datetime.strptime(date, '%d-%b-%Y')
                            sku = entry['slab'].get('sku_name', None)

                            data_dict = {
                                'attribute': sku if sku else entry['attribute'],
                                'value': math.ceil(round(entry['value'], 2))
                                if helper_sell_rate.is_ceiling_applicable else round(entry['value'], 2),
                                'date': date,
                                'extra_value': entry['value_extra'],
                                'rate': entry['slab']['rate'],
                                'amount': round(entry['amount'], 2),
                                'sell_rate': sell_rate,
                                'contract_invoice': invoice_fragment
                            }
                            if field_name not in self.aggregate_level_fields_non_relational:
                                data_dict[field_name + "_id"] = entry['aggregate_value']
                            else:
                                data_dict[field_name] = entry['aggregate_value']
                            entities.append(InvoiceEntity(**data_dict))
                    if entities:
                        InvoiceEntity.objects.bulk_create(entities)

                invoice_fragment.charges_basic = float(charges_basic) + float(extra_charges_slabs_fixed)
                invoice_fragment.charges_service = charges_slab
                invoice_fragment.days_counted = _days_count
                ## TODO: This is basis an assumption that if multiple contract terms are applicable for given invoice period, all of those base pay interval would align.
                invoice_fragment.basic_per_day = round(basic_calculation.get('amount_per_day', 0),
                                                       2) + base_pay_extra_by_driver_kms + base_pay_extra_by_driver_hrs
                invoice_fragment.save()

                total_basic += charges_basic + float(extra_charges_slabs_fixed)
                total_extra = float(total_extra) + float(extra_charges_slabs_fixed)
                total_service = float(total_service) + float(charges_slab)

                total_by_service_type[contract.service_tax_category] += float(charges_basic) + float(
                    charges_slab) + float(extra_charges_slabs_fixed)

                if invoice.hs_code:
                    hsn_code = invoice.hs_code.hsn_code
                else:
                    hsn_code = contract.hsn_code.hsn_code if contract.hsn_code else hsn_code

            invoice.charges_basic = float(total_basic)
            invoice.charges_service = total_service
            invoice.taxable_amount = float(total_basic) + float(total_service)
            invoice.bh_source_state_id = service_state
            invoice.invoice_state_id = invoice_state
            invoice.total_by_service_type = json.dumps(total_by_service_type)
            invoice.is_stale = False
            invoice.invoice_configs = config
            invoice.batch = batch
            invoice.calculation_level = calculation_level

            # check to see if status changed
            updated_invoice_exists = CustomerInvoice.objects.filter(id=invoice.id, status=APPROVED).exists()
            if updated_invoice_exists and invoice.status == CREATED:
                assert False, "Please refresh the page as invoice status changed"

            invoice.save()

            if calculation_level == "TRIP":
                # Creating trips accounted for this invoice.
                if applicable_trip_ids:
                    # Delete all unwanted trips. (Which are not considered anymore)
                    InvoiceTrip.objects.filter(invoice=invoice).exclude(trip_id__in=applicable_trip_ids).delete()

                    # Create any new Invoice trip that is required for invoice.
                    trip_ids = list(set(applicable_trip_ids) - set(
                        InvoiceTrip.objects.filter(invoice=invoice).values_list('trip_id', flat=True).distinct('id')))
                    invoice_trips = []
                    for trip in Trip.objects.filter(id__in=trip_ids):
                        invoice_trips.append(
                            InvoiceTrip(invoice=invoice, trip=trip, invoice_driver=trip.invoice_driver))

                    if invoice_trips:
                        InvoiceTrip.objects.bulk_create(invoice_trips)
            elif calculation_level == "ORDER":
                # Creating orders accounted for this invoice
                if applicable_order_ids:
                    # Delete all unwanted orders(which are not considered anymore)
                    InvoiceOrder.objects.filter(invoice=invoice).exclude(order_id__in=applicable_order_ids).delete()

                    # Create any new Invoice order which is required for invoice.
                    order_ids = list(set(applicable_order_ids) - set(
                        InvoiceOrder.objects.filter(invoice=invoice).values_list('order_id', flat=True).distinct('id')))
                    invoice_orders = []
                    for order_id, invoice_driver_id in Order.objects.filter(id__in=order_ids).values_list('id', 'invoice_driver_id'):
                        invoice_orders.append(
                            InvoiceOrder(invoice=invoice, order_id=order_id, invoice_driver_id=invoice_driver_id))

                    if invoice_orders:
                        InvoiceOrder.objects.bulk_create(invoice_orders)

            bytes = self.generate_invoice_as_pdf(
                invoice, service_state, invoice_state, total_by_service_type=total_by_service_type,
                sell_rate=sell_rate, fragment=invoice_fragment, hsn_code=hsn_code)

            if bytes:
                file_name = str(invoice.invoice_number) + ".pdf" if invoice.invoice_number else str(
                    invoice.id) + ".pdf"
                content = ContentFile(bytes)
                invoice.file.save(file_name, content, save=True)

            self._iprint("Invoice successfully updated with PDF ")
            invoices.append(invoice)
            invoice = None

    def generate_invoice_as_pdf(self, invoice, service_state, invoice_state, **kwargs):
        """
        Generate PDF for invoice. (Version 2)
        """

        total_by_service_type = kwargs.get('total_by_service_type', {})
        sell_rate = kwargs.get('sell_rate', {})
        fragment = kwargs.get('fragment', {})
        hsn_code = kwargs.get('hsn_code')
        config = invoice.invoice_configs

        summary_records = self.__get_summary_records(invoice, config) if config else None

        description = []

        total_taxable_amount = 0
        total_cgst_amount, total_sgst_amount, total_igst_amount, \
                total_vat_amount, total_rcm_amount = 0, 0, 0, 0, 0
        cgst_rate, sgst_rate, igst_rate, vat_rate = 0, 0, 0, 0

        if not hsn_code and fragment and fragment.contract.hsn_code:
            hsn_code = invoice.hs_code.hsn_code\
                if invoice.hs_code else fragment.contract.hsn_code.hsn_code

        service_type = None
        tax_exempt = False
        for service_type, amount in total_by_service_type.items():
            service_desc = SERVICE_TAX_CATEGORIES_CODE_MAP.get(service_type, {}).get('name')
            hs_heading = hsn_code or SERVICE_TAX_CATEGORIES_CODE_MAP.get(service_type, {}).get('code')

            tax_exempt = False
            if service_desc == EXEMPT:
                service_desc = 'Service Charges'
                tax_exempt = True

            self._iprint(
                "CALCULATING TAXES with invoice state %s and service state %s " % (invoice_state, service_state))
            charges_tax, tax_calculation, rcm = TaxHelper.calculate_tax_amount(
                taxable_amount=amount, customer=invoice.customer, service_type=service_type,
                invoice_state=invoice_state, service_state=service_state, tax_exempt=tax_exempt,
            )

            total_taxable_amount += amount

            cgst_rate = tax_calculation.get(CGST, {}).get('percentage', 0)
            sgst_rate = tax_calculation.get(SGST, {}).get('percentage', 0)
            igst_rate = tax_calculation.get(IGST, {}).get('percentage', 0)
            vat_rate = tax_calculation.get(VAT, {}).get('percentage', 0)

            cgst_amount = tax_calculation.get(CGST, {}).get('amount', 0)
            sgst_amount = tax_calculation.get(SGST, {}).get('amount', 0)
            igst_amount = tax_calculation.get(IGST, {}).get('amount', 0)
            vat_amount = tax_calculation.get(VAT, {}).get('amount', 0)

            # if not rcm:
            total_cgst_amount += cgst_amount
            total_sgst_amount += sgst_amount
            total_igst_amount += igst_amount
            total_vat_amount += vat_amount
            if rcm:
                total_rcm_amount += cgst_amount + sgst_amount + igst_amount + vat_amount

            description.append(
                {
                    "description": service_desc,
                    "hs_heading": hs_heading,
                    "value_of_service": intcomma(round(amount, 2)),
                    "discount": str(invoice.discount),
                    "taxable_value": intcomma(round((float(amount) - float(invoice.discount)), 2)),
                    "cgst_rate": cgst_rate,
                    "cgst_amount": intcomma(round(cgst_amount, 2)),
                    "sgst_rate": sgst_rate,
                    "sgst_amount": intcomma(round(sgst_amount, 2)),
                    "igst_rate": igst_rate,
                    "igst_amount": intcomma(round(igst_amount, 2)),
                    "vat_rate": vat_rate,
                    "vat_amount": intcomma(round(vat_amount, 2)),
                }
            )

        invoice.total_call_charges = 0
        invoice.cgst_amount = total_cgst_amount
        invoice.cgst_percentage = cgst_rate
        invoice.sgst_amount = total_sgst_amount
        invoice.sgst_percentage = sgst_rate
        invoice.igst_amount = total_igst_amount
        invoice.igst_percentage = igst_rate
        invoice.vat_amount = total_vat_amount
        invoice.vat_percentage = vat_rate
        invoice.total_rcm_amount = total_rcm_amount
        invoice.service_tax_category = service_type
        invoice.taxable_amount = total_taxable_amount
        invoice.total_amount = float(total_taxable_amount) + float(total_cgst_amount) + \
                               float(total_sgst_amount) + float(total_igst_amount) + \
                               float(total_vat_amount)- float(total_rcm_amount)

        self.calculate_toll_and_parking_charges(invoice, description, TripSet, sell_rate, fragment)
        self.calculate_call_masking_charges(invoice, description, OrderSet, sell_rate, fragment)
        invoice.save()

        bss_tax_category = total_by_service_type.get("BSS")
        if bss_tax_category:
            service_type = BSS

        company_office = Office.objects.filter(state_id=service_state).first()
        if not company_office:
            company_office = Office.objects.first()
        head_office = Office.objects.filter(is_head=True).first()

        service_start = invoice.service_period_start.strftime("%d-%b-%Y")
        service_end = invoice.service_period_end.strftime("%d-%b-%Y")

        division = invoice.aggregation.division if hasattr(invoice, 'aggregation') else None

        _taxinfo = {
            "customer" : invoice.customer
        }

        if invoice_state:
            _taxinfo['state'] = invoice_state

        if division:
            _taxinfo['division'] = division

        customer_tax_info = CustomerTaxInformation.objects.filter(**_taxinfo).first()

        tax_legalname = None
        if customer_tax_info:
            customer_address = customer_tax_info.invoice_address
            state_name = str(customer_tax_info.state.name) if customer_tax_info else customer_address.state
            state_code = str(customer_tax_info.state.gst_code) if customer_tax_info else ""
            cust_gstin = str(customer_tax_info.gstin)
            tax_legalname = customer_tax_info.legalname
        else:
            customer_address = invoice.customer.head_office_address
            state_name, state_code, cust_gstin = invoice.customer.head_office_address.state, '', ''

        adjustment_record = invoice.customerinvoiceadjustment_set.values(
            'category__name', 'category__is_tax_applicable', 'comments').annotate(total_amount=Sum('total_amount'))
        total_amount = float(invoice.total_amount)
        taxable_amount = float(invoice.taxable_amount)

        total_taxable_amount, adjustment_amount, taxable_adj, non_tax_adj = 0, 0, 0, 0
        total_cgst_amount, total_sgst_amount, \
                total_igst_amount, total_rcm_amount, total_vat_amount = 0, 0, 0, 0, 0

        for record in adjustment_record:
            cgst_rate, sgst_rate, igst_rate, \
                sgst_amount, cgst_amount, igst_amount, total_vat_amount = [0] * 7
            amount_ = record.get('total_amount')
            adjustment_amount += amount_

            if not service_type and record.get('category__name') in [GTA, BSS]:
                service_type = record.get('category__name')
            if record.get('category__is_tax_applicable') and service_type:

                is_adjustment = False
                taxable_adj += float(amount_)
                if (float(invoice.taxable_amount) + float(amount_)) > 750:
                    is_adjustment = True
                tax_charges, tax_cal, tax_rcm = TaxHelper.calculate_tax_amount(
                    taxable_amount=amount_, customer=invoice.customer, service_type=service_type,
                    invoice_state=invoice_state, service_state=service_state, is_adjustment=is_adjustment,
                    tax_exempt=tax_exempt
                )

                if tax_charges != 0:
                    total_taxable_amount += float(amount_)

                cgst_rate = tax_cal.get(CGST, {}).get('percentage', 0)
                sgst_rate = tax_cal.get(SGST, {}).get('percentage', 0)
                igst_rate = tax_cal.get(IGST, {}).get('percentage', 0)
                vat_rate = tax_cal.get(VAT, {}).get('percentage', 0)

                cgst_amount = tax_cal.get(CGST, {}).get('amount', 0)
                sgst_amount = tax_cal.get(SGST, {}).get('amount', 0)
                igst_amount = tax_cal.get(IGST, {}).get('amount', 0)
                vat_amount = tax_cal.get(VAT, {}).get('amount', 0)

                # if not tax_rcm:
                total_cgst_amount += cgst_amount
                total_sgst_amount += sgst_amount
                total_igst_amount += igst_amount
                total_vat_amount += vat_amount
                total_amount = float(total_amount) + \
                                    float(cgst_amount + sgst_amount + igst_amount + vat_amount)
                if tax_rcm:
                    total_rcm_amount = float(total_rcm_amount) + \
                                            float(cgst_amount + sgst_amount + igst_amount + vat_amount)
            elif not record.get('category__is_tax_applicable'):
                non_tax_adj += amount_

            display_text = record.get('category__name')
            if record['comments']:
                display_text += "(" + record['comments'] + ")"
            description.append(
                {
                    "description": display_text,
                    "hs_heading": '',
                    "value_of_service": '',
                    "discount": '0',
                    "taxable_value": round(record.get('total_amount'), 2),
                    "cgst_rate": cgst_rate,
                    "cgst_amount": intcomma(round(cgst_amount, 2)),
                    "sgst_rate": sgst_rate,
                    "sgst_amount": intcomma(round(sgst_amount, 2)),
                    "igst_rate": igst_rate,
                    "igst_amount": intcomma(round(igst_amount, 2)),
                    "vat_rate": vat_rate,
                    "vat_amount": intcomma(round(vat_amount, 2)),

                }
            )

        total_amount = total_amount + float(adjustment_amount) - float(total_rcm_amount)
        invoice.non_taxable_adjustment = non_tax_adj
        invoice.taxable_adjustment = taxable_adj
        invoice.total_service_charge = float(invoice.charges_basic) + float(invoice.charges_service) + \
                                       float(invoice.non_taxable_adjustment) + float(invoice.taxable_adjustment)

        invoice.cgst_amount = float(invoice.cgst_amount) + total_cgst_amount
        invoice.sgst_amount = float(invoice.sgst_amount) + total_sgst_amount
        invoice.igst_amount = float(invoice.igst_amount) + total_igst_amount
        invoice.vat_amount = float(invoice.vat_amount) + total_vat_amount
        invoice.total_rcm_amount = float(invoice.total_rcm_amount) + total_rcm_amount
        invoice.taxable_amount = float(invoice.taxable_amount) + total_taxable_amount
        toll_and_parking_amount = float(invoice.total_toll_charges) + float(invoice.total_parking_charges)
        invoice.total_amount = float(total_amount) + toll_and_parking_amount + float(invoice.total_call_charges)
        invoice.output_log = "\n".join(self.output_log)
        invoice.save()

        static_base = settings.ROOT_DIR.path('website/static')
        logo_folder_path = '%s/website/images/%s/logo/' % (static_base, settings.WEBSITE_BUILD)
        company_extra_data = COMPANY_HEADER_FOOTER_DATA.get(settings.WEBSITE_BUILD)

        context = {
            "logo": logo_folder_path + "logo_rect.png"
            if invoice.timestamp_approved else logo_folder_path + "logo_alt.png",

            "company": {
                "name": company_office.company.name if company_office else '',
                "address": company_office.address if company_office else '',
                "gstin": company_office.gst_identification_no if company_office else '',
                "state": company_office.state.name,
                "bank_details": get_company_bank_account_details(config),
                "cin": company_office.company.cin,
                "pan": company_office.company.pan,
                "registered_address" : head_office.address,
                "telephone_number" : settings.BLOWHORN_SUPPORT_NUMBER,
                "website" : settings.HOST,
                "email" : company_extra_data.get('email'),
                "build" : settings.WEBSITE_BUILD
            },
            "customer": {
                "name": tax_legalname or invoice.customer.legalname,
                "address": str(customer_address.line1) if customer_address else '',
                "state": state_name,
                "state_code": state_code,
                "gstin": cust_gstin
            },
            "invoice": {
                "general": {
                    "invoice_no": invoice.invoice_number,
                    "invoice_date": invoice.invoice_date,
                    "service_period_start": service_start,
                    "service_period_end": service_end,
                    "aggregate_values": ' - '.join(invoice.get_aggregate_values()),
                    "comments": invoice.comments,
                    "invoice_type": 'TAX INVOICE',
                    },
                "description": description,
                "description_total": {
                    "value_in_figure": intcomma(round(total_amount + toll_and_parking_amount + float(invoice.total_call_charges))),
                    "value_in_words": num2words(round(total_amount + toll_and_parking_amount + float(invoice.total_call_charges)), lang='en_IN').title(),
                    "taxable_value": intcomma(round((taxable_amount + toll_and_parking_amount + float(adjustment_amount) + float(invoice.total_call_charges)
                                                     - float(invoice.discount)), 2)),
                    "cgst_amount": intcomma(round(invoice.cgst_amount, 2)),
                    "sgst_amount": intcomma(round(invoice.sgst_amount, 2)),
                    "igst_amount": intcomma(round(invoice.igst_amount, 2)),
                    "vat_amount": intcomma(round(invoice.vat_amount, 2)),
                    "rcm_amount": intcomma(round(invoice.total_rcm_amount, 2))
                },
                "currency_unit" : company_extra_data.get('currency_unit'),
                "currency_word" : company_extra_data.get('currency_word'),
            },
            "einvoicing": {
                'ack_no': '',
                'ack_date': '',
                'reverse_charge': "No" if service_type == BSS else "Yes",
                "irn": '',
                "qr_code": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAMI0lEQVR4Xu2dwXYjSQ4Du///oz1vT3sZV/RTNCZTVsyVAxIJEskq2Vb//vr6+vrVfymQAv+qwO8M0mSkwPcKZJCmIwUeFMggjUcKZJBmIAVeU6AN8ppuoT5EgQzyIY3umK8pkEFe0y3UhyiQQT6k0R3zNQUyyGu6hfoQBTLIhzS6Y76mQAZ5TbdQH6JABvmQRnfM1xTIIK/pFupDFMggH9LojvmaAhnkNd1CfYgC2iC/f//+0VKt/1yG9KP6a/y7N5f0o/NlEFDICowNgAuG6meQZ4VJP+yP/YMpahARuD1uBabzkX5Uf40n/rfHST/i3wZpg9CMvHU8g4zbZwUmeusNQPypPvG/PU7nJ/5tkDYIzchbxzPIuH1WYKJHNzjVX+OJ/+1x0o/4t0HaIDQjbx2/3iCW4Lo7t9/ApN+aP9Vf94fy2/Nj/vXHvD9dYGoQNYDipB/VX+OJ/zpuz0/85o9Y1CAiuI5bgQlv+ZN+VH+Nt+ezeHt+qp9Bxj/JpgZQfD3g6wGj89n4mn8GySCPM0oGtQNu8RnEKgh4KzDhLX0aUKq/xtvzWbw9P9Vvg7RB2iAPCmSQDJJBbjYIrUhagRS//RHj3c//7vxpfo5vkJ8uMDZg/Adn6wvip/cvg8hHLDIAxd99wN6dP/bn9E/Sf7rA2IA2iHoHovmhDYr9ySDPf1NvBcYGZJAM8qQA3QA0YBSnAaf6hKf6FKf6hKc48af6Fk/8KG7rE57q9w7SO8j0BqcBpDgNuDU41c8gGSSD9HOQ7xWwNxDh6YayN+Q6/5rfaf5Uvw0iN0gG2X5xoDUo4TPI19ejBjTgJDDhqQG351/zW+tD/Kl+G6QN0jtI7yC9g3ynAG1AuoEJTzc0xW19wlP9NkgbpA3SBmmDtEFoV/x7vA3SBmmDtEF2G4TuJXpGt8/IVJ/ilh/hqT7FSR+qT3iq3waRGwQFHuen+hS3A0Z4qk9xGnCqT3iqn0HGA7xuIDWY4pYf4ak+xWnAqT7hqX4GySC9g/QO0jtIn2LRruhTrH9VYL6ixxvqtbb/H2XPT3jLjx6RqD7hiV+PWOMBXjeQGkxxy4/wVJ/iNOBUn/BUP4NkkN5Bbn4HIQev4/YGsng6H+UnPN2glJ/wVH8dX/M/vkHWAlJ+K7DFW36EpwFf8yd+Nr7mn0HkI9a8QfJbTzLI898DkUEzSAZR7yA0YOv4/II6/b1YawEpvxXY4i0/wrdB2iA0I49xO+AWT+QpP+EzSAahGckgTx9jykdMJf5fANMFQhcEUegdRA7IvEG9pE8vuOMGIQK3x+kGsga5HX97f4gf9Y/w8w1CBG6Pk8C3D7jlf3t/iB+dn/AZBBQigTMIjdjZOPWP2GWQDEIz8tbxDDJuHwncBhk3QKan/lH6NkgbhGbkreMZZNw+ErgNMm6ATE/9o/RtkDYIzchbx48b5K3V+w/I2w1DFNf5qf5Pj+sN8tMFsudbD/A6vz3/u+MzyLiD6wFe5x/Lc336DDJu0XqA1/nH8lyfPoOMW7Qe4HX+sTzXp88g4xatB3idfyzP9ekzyLhF6wFe5x/Lc336DDJu0XqA1/nH8lyfXhuEGnRaAfpB0Zo/1Sd9iB/l/3Q86UvxDCL/Yo8EpgEm/KcPuD0/6UvxDJJBHmfEDuhpPBmA4hkkg2SQBwUySAbJIBnkewXoEYBWMMV7B/n9KBHpQ/0hPPWH4m2QNkgbpA3SBvlOAbqB7Q1+Gk8bguJ6g2AB+cVslH8dpwZTfRpAwtu45W/rE97qQ+fT+e2XV5MA6wNQfRsn/pTfNojyU9zyp/w2bvWh8+n8GeS5xdQAGhDbIMpPccuf8tu41YfOp/NnkAxih9zg9QCPH+F7B4Hu0g1Fw2EHgPJT3PKn/DZu9aHz6fxtkDaIHXKD1wPcBjHyeyzdUFTBDgDlp7jlT/lt3OpD59P52yBtEDvkBq8H+N03CIlHNwDh1wLb+nQ+4k/40/yoPsXX56P6FJ+/pCMB+aseNGCn69MAEH/C0/lsfsJTfYqvz0f1KZ5BxgalAaABJDw12OYnPNWn+Pp8VJ/iGSSDPM5IBlkrABY9fYOs61N+kp/wdAPa/ISn+hRfn4/qU7wN0gZpgzwokEEySAbJIN8rsF7xlJ8eYQhPjwg2P+GpPsXX56P6FNcbhA5IAq/xJIDlR/kpTvUJb/Wz+Qlv46TP/Pz2J+mW4BpPDbINoPwUp/qEt/rZ/IS3cdJnfv4M8vXYQ2rAegAoP/GjAbP5CW/jxH9+/gySQZ6GmAbQGoDwGUT+spltoG0ANZjiVJ/wdP51fuJn48R/fv42SBukDfK9An2K9ZVBMkgG+VYBu8LXjxCUf/6IIX+QSvwpbvtDeKqvNwgWGL9jaAHGA0D87ICv8dRfOh/hb49nkAyiZjSDKPl+/VrfcLZBxE8e/xfxo/qn8XR+4kf42+NtkDaImtEMouRrg9AAtUHkgI3hbZA2iBoxugBU8gvAGSSDqDHMIEq+HrFogHrEkgM2hs83yJj/8fQ04GuCZMB1fXv+0/xJnwxCCkHcDogsjx8j2/yEt+fPIKTwm8ftgNjjnx4we/7T/En/Nggp1AZ5VCCDyAH66XA7IFaf0zewPf9p/qR/G4QUaoO0QeSMfDTc3qBWvNM3sD3/af6kfxuEFGqDtEHMjNgbxNT+L7B0w9nzn86/1pD0secnvD2f3iAkgCV4Gk8NsOc/nX+tL+ljz094e74MAgpSA2gAqEGn8xM/Gyd97PkJr/mvv9XEEjyNpwbQABD/0/mJn42TPvb8hNf8M8izhNQAGgBq0On8xM/GSR97fsJr/hkkg9ghesJnkPHfUyyb9ye56YaiAaAap/MTPxsnfez5Ca/5t0HaIHaI2iAPCtgbYtmcP8l9O3/i9ydnfPp/7A285nf8fOsNYhtgBSI8Nfg0f+JH56O4Pd+aH/GnuD5fBvn9qLEVmBpI8fUA2vOt+ZE+FNfnyyAZhIbMvGOY3H8Dm0GkinQDWoElPfxmSpvfno/0s/wsXp+vDdIGMUOYQUA9Esg62DTvT7C38yd+f3LGPsV6XaX5LytmkNeb8z9kBnH62fk7bpDTA0D1SeA1nsZjzY/q0/kJb/nb/ITPIIf/gZ/TA0YDigMkf9WI6q/1wfOdfkm3AtABbQNO40+fj+rb/ll9iR/lJ3wbpA1CM/IYzyDjT7GswNRdukGo/mn86fNRfdKP8FZfm5/wbZA2CM1IG8QoRDfI+oYg7rb+afzp81F96j/hrb42P+HbIG0QmpE2iFGIbpD1DUHcbX2Lt/wIv45Tf9f1Sf91/TbIeINQA08PAPHLILJDJCClJzw1kOK2vsVbfoRfx9f9If6kP+FtvA3SBpm+Y9gBzSDyVxWoASQw3ZAWb/kRfh0nfdb1Sf91/TZIG6QN8qBABskgGSSDfK8APULQiic8PQJQfsKv4/Z8lt9pfdoghzeIHaA13g4oGczmX58/g2SQxxmzA5xBLh8warBtIOHXN9w6P+lH9Ukfm5/q23gb5HKD2wZbvB3gDHL5gFGDbQMJbwf0NJ70I36kj81P9W28DXK5wW2DLd4OcAa5fMCowbaBhLcDehpP+hE/0sfmp/o23ga53OC2wRZvBziDyAGzDbR420CLJ/6Un/AUJwNQfYu3/Ahv48c3iD2Axa8HgAaI+BM/wlOc+FF9i7f8CG/jGURuQDtA1EDKT3iK2wG3eMuP8DaeQTLI4wyRQTMIWNAKaB1u8Za/xRN/yk94itsBt3jLj/A23gZpg7RBHhTIIBkkg2SQ7xWgRxj7CEF4egQgfoSnOPGj+hZv+RHexucbxBI8jbcDQHh7vtMDTPzt+e35iB/FMwgoRA0+3sDxIyINEMVJP8If13f974OQALfHqcHHG5hBpiPUBmmDTAeMLhgqfvwCaoM8t4gafLyBbRDymIq3QdogaoAITBcM4Y9fQG2QNggNqYlnkPFXh5rm/A0sNfj4Ddcj1t9o87c59CPWlF3JU+CwAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Ahnk7v7E7rACGeRwAyp/twIZ5O7+xO6wAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Ahnk7v7E7rACGeRwAyp/twIZ5O7+xO6wAhnkcAMqf7cCGeTu/sTusAIZ5HADKn+3Av8Ai8TmbD3x6tAAAAAASUVORK5CYII="

            }
        }

        if config and config.extra_pages in [DETAIL_PAGE, SUMMARY_DETAIL_PAGE]:
            detailed_data = []

            if invoice.calculation_level == "ORDER":
                contract_invoice_objects = invoice.contract_invoices.all()[:1]
            else:
                contract_invoice_objects = invoice.contract_invoices.all()
            for contract_invoice in contract_invoice_objects:
                an_invoice_entity = contract_invoice.entities.first()
                if an_invoice_entity:

                    sell_rate = an_invoice_entity.sell_rate
                    contract_data = {
                        'invoice': contract_invoice,
                        'days_counted': contract_invoice.days_counted if sell_rate.base_pay_interval == BASE_PAYMENT_CYCLE_MONTH else 0,
                        'base_pay_per_month': sell_rate.base_pay if sell_rate.base_pay else 0,
                        'details': []
                    }

                    attributes = contract_invoice.entities.values_list('attribute', flat=True).distinct()
                    # Ordering to maintain the base pay followed by vehicle slabs etc.
                    attributes = [attribute for attribute in self.get_attribute_ordered() if attribute in attributes]

                    delivered_records = None
                    for attribute in attributes:
                        attributes_mapping = {"days": ["days", "Extra Hours", "Extra Kilometers"]}
                        if attribute == 'Delivered':
                            for contract_invoice in invoice.contract_invoices.all():
                                record = contract_invoice.entities.filter(
                                    attribute__in=attributes_mapping.get(attribute, [attribute])).order_by('date', 'city')
                                delivered_records = delivered_records | record if delivered_records else record
                            contract_data['details'].append(
                                {
                                    'attribute_name': attribute,
                                    'records': delivered_records,
                                    'aggregates': delivered_records.distinct().order_by('-date', 'city').aggregate(
                                        Sum('value'), Sum('extra_value'), Sum('amount'),
                                        Sum('slab_fixed_amount_kilometers'),
                                        Sum('slab_fixed_amount_hours'))
                                }
                            )
                        else:
                            records = contract_invoice.entities.filter(
                                attribute__in=attributes_mapping.get(attribute, [attribute])).order_by('date')
                            contract_data['details'].append(
                                {
                                    'attribute_name': attribute,
                                    'records': records,
                                    'aggregates': records.order_by('-date').aggregate(
                                        Sum('value'), Sum('extra_value'), Sum('amount'), Sum('slab_fixed_amount_kilometers'),
                                        Sum('slab_fixed_amount_hours'))
                                }
                            )

                    detailed_data.append(contract_data)

            context['invoice']['details'] = detailed_data

        data = []
        for adjustment in invoice.customerinvoiceadjustment_set.all():
            data.append({
                'name': adjustment.category.name,
                'amount': adjustment.total_amount
            })

        context['invoice']['adjustment'] = data
        context['invoice']['summary'] = summary_records

        invoice_template = "pdf/invoices/%s/invoice.html" % (settings.WEBSITE_BUILD)
        header = {"template": "pdf/invoices/b2b_v2/header.html", "context": context}
        footer = {"template": "pdf/invoices/b2b_v2/footer.html", "context": context}
        context_copy = copy.deepcopy(context)
        context_copy.update({'einvoicing': {}})
        invoice.context = json.dumps(context_copy, indent=4, sort_keys=True, default=str)

        invoice.save()

        return generate_pdf(invoice_template, context, header=header, footer=footer)

    # generate context and save pdf
    def generate_manual_uploaded_pdf(self, invoice, service_state, invoice_state, **kwargs):
        """
        Generate PDF for manually uploaded invoice.
        """
        total_by_service_type = kwargs.get('total_by_service_type', {})
        sell_rate = kwargs.get('sell_rate', {})
        fragment = kwargs.get('fragment', {})
        hsn_code = kwargs.get('hsn_code')
        config = invoice.invoice_configs

        summary_records = self.__get_summary_records(invoice, config) if config else None

        description = []

        total_taxable_amount = 0
        total_cgst_amount, total_sgst_amount, total_igst_amount, \
                total_vat_amount, total_rcm_amount = 0, 0, 0, 0, 0
        cgst_rate, sgst_rate, igst_rate, vat_rate = 0, 0, 0, 0

        if not hsn_code and fragment and fragment.contract.hsn_code:
            hsn_code = invoice.hs_code.hsn_code\
                if invoice.hs_code else fragment.contract.hsn_code.hsn_code

        service_type = None
        tax_exempt = False
        for service_type, amount in total_by_service_type.items():
            service_desc = SERVICE_TAX_CATEGORIES_CODE_MAP.get(service_type, {}).get('name')
            hs_heading = hsn_code or SERVICE_TAX_CATEGORIES_CODE_MAP.get(service_type, {}).get('code')

            tax_exempt = False
            if service_desc == EXEMPT:
                service_desc = 'Service Charges'
                tax_exempt = True

            self._iprint(
                "CALCULATING TAXES with invoice state %s and service state %s " % (invoice_state, service_state))
            charges_tax, tax_calculation, rcm = TaxHelper.calculate_tax_amount(
                taxable_amount=amount, customer=invoice.customer, service_type=service_type,
                invoice_state=invoice_state, service_state=service_state, tax_exempt=tax_exempt,
            )

            total_taxable_amount += amount

            cgst_rate = tax_calculation.get(CGST, {}).get('percentage', 0)
            sgst_rate = tax_calculation.get(SGST, {}).get('percentage', 0)
            igst_rate = tax_calculation.get(IGST, {}).get('percentage', 0)
            vat_rate = tax_calculation.get(VAT, {}).get('percentage', 0)

            cgst_amount = tax_calculation.get(CGST, {}).get('amount', 0)
            sgst_amount = tax_calculation.get(SGST, {}).get('amount', 0)
            igst_amount = tax_calculation.get(IGST, {}).get('amount', 0)
            vat_amount = tax_calculation.get(VAT, {}).get('amount', 0)

            # if not rcm:
            total_cgst_amount += cgst_amount
            total_sgst_amount += sgst_amount
            total_igst_amount += igst_amount
            total_vat_amount += vat_amount
            if rcm:
                total_rcm_amount += cgst_amount + sgst_amount + igst_amount + vat_amount

            description.append(
                {
                    "description": service_desc,
                    "hs_heading": hs_heading,
                    "value_of_service": intcomma(round(amount, 2)),
                    "discount": str(invoice.discount),
                    "taxable_value": intcomma(round((float(amount) - float(invoice.discount)), 2)),
                    "cgst_rate": cgst_rate,
                    "cgst_amount": intcomma(round(cgst_amount, 2)),
                    "sgst_rate": sgst_rate,
                    "sgst_amount": intcomma(round(sgst_amount, 2)),
                    "igst_rate": igst_rate,
                    "igst_amount": intcomma(round(igst_amount, 2)),
                    "vat_rate": vat_rate,
                    "vat_amount": intcomma(round(vat_amount, 2)),
                }
            )

        bss_tax_category = total_by_service_type.get("BSS")
        if bss_tax_category:
            service_type = BSS

        company_office = Office.objects.filter(state_id=service_state).first()
        if not company_office:
            company_office = Office.objects.first()
        head_office = Office.objects.filter(is_head=True).first()

        service_start = invoice.service_period_start.strftime("%d-%b-%Y")
        service_end = invoice.service_period_end.strftime("%d-%b-%Y")

        division = invoice.aggregation.division if hasattr(invoice, 'aggregation') else None

        _taxinfo = {
            "customer" : invoice.customer
        }

        if invoice_state:
            _taxinfo['state'] = invoice_state

        if division:
            _taxinfo['division'] = division

        customer_tax_info = CustomerTaxInformation.objects.filter(**_taxinfo).first()

        tax_legalname = None
        if customer_tax_info:
            customer_address = customer_tax_info.invoice_address
            state_name = str(customer_tax_info.state.name) if customer_tax_info else customer_address.state
            state_code = str(customer_tax_info.state.gst_code) if customer_tax_info else ""
            cust_gstin = str(customer_tax_info.gstin)
            tax_legalname = customer_tax_info.legalname
        else:
            customer_address = invoice.customer.head_office_address
            state_name, state_code, cust_gstin = invoice.customer.head_office_address.state, '', ''

        adjustment_record = invoice.customerinvoiceadjustment_set.values(
            'category__name', 'category__is_tax_applicable', 'comments').annotate(total_amount=Sum('total_amount'))
        total_amount = float(invoice.total_amount)
        taxable_amount = float(invoice.taxable_amount)

        total_taxable_amount, adjustment_amount, taxable_adj, non_tax_adj = 0, 0, 0, 0
        total_cgst_amount, total_sgst_amount, \
                total_igst_amount, total_rcm_amount, total_vat_amount = 0, 0, 0, 0, 0

        for record in adjustment_record:
            cgst_rate, sgst_rate, igst_rate, \
                sgst_amount, cgst_amount, igst_amount, total_vat_amount = [0] * 7
            amount_ = record.get('total_amount')
            adjustment_amount += amount_

            if not service_type and record.get('category__name') in [GTA, BSS]:
                service_type = record.get('category__name')
            if record.get('category__is_tax_applicable') and service_type:

                is_adjustment = False
                taxable_adj += float(amount_)
                if (float(invoice.taxable_amount) + float(amount_)) > 750:
                    is_adjustment = True
                tax_charges, tax_cal, tax_rcm = TaxHelper.calculate_tax_amount(
                    taxable_amount=amount_, customer=invoice.customer, service_type=service_type,
                    invoice_state=invoice_state, service_state=service_state, is_adjustment=is_adjustment,
                    tax_exempt=tax_exempt
                )

                if tax_charges != 0:
                    total_taxable_amount += float(amount_)

                cgst_rate = tax_cal.get(CGST, {}).get('percentage', 0)
                sgst_rate = tax_cal.get(SGST, {}).get('percentage', 0)
                igst_rate = tax_cal.get(IGST, {}).get('percentage', 0)
                vat_rate = tax_cal.get(VAT, {}).get('percentage', 0)

                cgst_amount = tax_cal.get(CGST, {}).get('amount', 0)
                sgst_amount = tax_cal.get(SGST, {}).get('amount', 0)
                igst_amount = tax_cal.get(IGST, {}).get('amount', 0)
                vat_amount = tax_cal.get(VAT, {}).get('amount', 0)

                # if not tax_rcm:
                total_cgst_amount += cgst_amount
                total_sgst_amount += sgst_amount
                total_igst_amount += igst_amount
                total_vat_amount += vat_amount
                total_amount = float(total_amount) + \
                                    float(cgst_amount + sgst_amount + igst_amount + vat_amount)
                if tax_rcm:
                    total_rcm_amount = float(total_rcm_amount) + \
                                            float(cgst_amount + sgst_amount + igst_amount + vat_amount)
            elif not record.get('category__is_tax_applicable'):
                non_tax_adj += amount_

            display_text = record.get('category__name')
            if record['comments']:
                display_text += "(" + record['comments'] + ")"
            description.append(
                {
                    "description": display_text,
                    "hs_heading": '',
                    "value_of_service": '',
                    "discount": '0',
                    "taxable_value": round(record.get('total_amount'), 2),
                    "cgst_rate": cgst_rate,
                    "cgst_amount": intcomma(round(cgst_amount, 2)),
                    "sgst_rate": sgst_rate,
                    "sgst_amount": intcomma(round(sgst_amount, 2)),
                    "igst_rate": igst_rate,
                    "igst_amount": intcomma(round(igst_amount, 2)),
                    "vat_rate": vat_rate,
                    "vat_amount": intcomma(round(vat_amount, 2)),

                }
            )

        total_amount = total_amount + float(adjustment_amount) - float(total_rcm_amount)
        toll_and_parking_amount = float(invoice.total_toll_charges) + float(invoice.total_parking_charges)

        static_base = settings.ROOT_DIR.path('website/static')
        logo_folder_path = '%s/website/images/%s/logo/' % (static_base, settings.WEBSITE_BUILD)
        company_extra_data = COMPANY_HEADER_FOOTER_DATA.get(settings.WEBSITE_BUILD)

        context = {
            "logo": logo_folder_path + "logo_rect.png"
            if invoice.timestamp_approved else logo_folder_path + "logo_alt.png",

            "company": {
                "name": company_office.company.name if company_office else '',
                "address": company_office.address if company_office else '',
                "gstin": company_office.gst_identification_no if company_office else '',
                "state": company_office.state.name,
                "bank_details": get_company_bank_account_details(config),
                "cin": company_office.company.cin,
                "pan": company_office.company.pan,
                "registered_address" : head_office.address,
                "telephone_number" : settings.BLOWHORN_SUPPORT_NUMBER,
                "website" : settings.HOST,
                "email" : company_extra_data.get('email'),
                "build" : settings.WEBSITE_BUILD
            },
            "customer": {
                "name": tax_legalname or invoice.customer.legalname,
                "address": str(customer_address.line1) if customer_address else '',
                "state": state_name,
                "state_code": state_code,
                "gstin": cust_gstin
            },
            "invoice": {
                "general": {
                    "invoice_no": invoice.invoice_number,
                    "invoice_date": invoice.invoice_date,
                    "service_period_start": service_start,
                    "service_period_end": service_end,
                    "aggregate_values": ' - '.join(invoice.get_aggregate_values()),
                    "comments": invoice.comments,
                    "invoice_type": 'TAX INVOICE',
                    },
                "description": description,
                "description_total": {
                    "value_in_figure": intcomma(round(invoice.total_amount)),
                    "value_in_words": num2words(round(invoice.total_amount), lang='en_IN').title(),
                    "taxable_value": intcomma(round(invoice.taxable_amount, 2)),
                    "cgst_amount": intcomma(round(invoice.cgst_amount, 2)),
                    "sgst_amount": intcomma(round(invoice.sgst_amount, 2)),
                    "igst_amount": intcomma(round(invoice.igst_amount, 2)),
                    "vat_amount": intcomma(round(invoice.vat_amount, 2)),
                    "rcm_amount": intcomma(round(invoice.total_rcm_amount, 2))
                },
                "currency_unit" : company_extra_data.get('currency_unit'),
                "currency_word" : company_extra_data.get('currency_word'),
            },
            "einvoicing": {
                "hidewatermark": "yes"
            }
        }

        if config and config.extra_pages in [DETAIL_PAGE, SUMMARY_DETAIL_PAGE]:
            detailed_data = []

            if invoice.calculation_level == "ORDER":
                contract_invoice_objects = invoice.contract_invoices.all()[:1]
            else:
                contract_invoice_objects = invoice.contract_invoices.all()
            for contract_invoice in contract_invoice_objects:
                an_invoice_entity = contract_invoice.entities.first()
                if an_invoice_entity:

                    sell_rate = an_invoice_entity.sell_rate
                    contract_data = {
                        'invoice': contract_invoice,
                        'days_counted': contract_invoice.days_counted if sell_rate.base_pay_interval == BASE_PAYMENT_CYCLE_MONTH else 0,
                        'base_pay_per_month': sell_rate.base_pay if sell_rate.base_pay else 0,
                        'details': []
                    }

                    attributes = contract_invoice.entities.values_list('attribute', flat=True).distinct()
                    # Ordering to maintain the base pay followed by vehicle slabs etc.
                    attributes = [attribute for attribute in self.get_attribute_ordered() if attribute in attributes]

                    delivered_records = None
                    for attribute in attributes:
                        attributes_mapping = {"days": ["days", "Extra Hours", "Extra Kilometers"]}
                        if attribute == 'Delivered':
                            for contract_invoice in invoice.contract_invoices.all():
                                record = contract_invoice.entities.filter(
                                    attribute__in=attributes_mapping.get(attribute, [attribute])).order_by('date', 'city')
                                delivered_records = delivered_records | record if delivered_records else record
                            contract_data['details'].append(
                                {
                                    'attribute_name': attribute,
                                    'records': delivered_records,
                                    'aggregates': delivered_records.distinct().order_by('-date', 'city').aggregate(
                                        Sum('value'), Sum('extra_value'), Sum('amount'),
                                        Sum('slab_fixed_amount_kilometers'),
                                        Sum('slab_fixed_amount_hours'))
                                }
                            )
                        else:
                            records = contract_invoice.entities.filter(
                                attribute__in=attributes_mapping.get(attribute, [attribute])).order_by('date')
                            contract_data['details'].append(
                                {
                                    'attribute_name': attribute,
                                    'records': records,
                                    'aggregates': records.order_by('-date').aggregate(
                                        Sum('value'), Sum('extra_value'), Sum('amount'), Sum('slab_fixed_amount_kilometers'),
                                        Sum('slab_fixed_amount_hours'))
                                }
                            )

                    detailed_data.append(contract_data)

            context['invoice']['details'] = detailed_data

        data = []
        for adjustment in invoice.customerinvoiceadjustment_set.all():
            data.append({
                'name': adjustment.category.name,
                'amount': adjustment.total_amount
            })

        context['invoice']['adjustment'] = data
        context['invoice']['summary'] = summary_records

        invoice_template = "pdf/invoices/%s/invoice.html" % (settings.WEBSITE_BUILD)
        header = {"template": "pdf/invoices/b2b_v2/header.html", "context": context}
        footer = {"template": "pdf/invoices/b2b_v2/footer.html", "context": context}
        context_copy = copy.deepcopy(context)
        invoice.context = json.dumps(context_copy, indent=4, sort_keys=True, default=str)
        invoice.save()

        return generate_pdf(invoice_template, context, header=header, footer=footer)

    @staticmethod
    def generate_file_path(invoice):
        """
        Generates the remote upload file path for invoice.
        """

        invoice_identifier = invoice.invoice_number or str(invoice.id)

        prev_file_name = (re.findall(invoice_identifier + "*\w+", invoice.file.name) or [None])[-1]
        next_file_no = int(prev_file_name[-1]) + 1 \
            if prev_file_name and prev_file_name[-1].isdigit() and prev_file_name[-2] == "_" else 1

        return InvoiceHelper.INVOICE_UPLOAD_PATH.format(
            period=invoice.service_period_start.strftime("%B")[:3] + "-" + invoice.service_period_start.strftime("%Y"),
            customer_id=invoice.customer_id,
            invoice_identifier=invoice_identifier + "_" + str(next_file_no)
        )

    def create_entity_for_toll_and_parking(self, trips, att, sell_rate, fragment):
        entities = []
        for trip in trips:
            entities.append(
                InvoiceEntity(
                    driver_id=trip.driver_id,
                    attribute=att,
                    date=trip.planned_start_time_ist,
                    value=1,
                    rate=trip.parking_charges if att == PARKING_CHARGES else trip.toll_charges,
                    amount=trip.parking_charges if att == PARKING_CHARGES else trip.toll_charges,
                    sell_rate=sell_rate,
                    extra_value=1,
                    contract_invoice=fragment)
            )
        if entities:
            InvoiceEntity.objects.bulk_create(entities)

    def calculate_call_masking_charges(self, invoice, description, OrderSet, sell_rate, fragment):
        orders, call_masking_charges = OrderSet.get_call_masking_logs(invoice)
        invoice.total_call_charges = 0

        if call_masking_charges > 0:
            call_rate = 0

            if sell_rate.call_rate and sell_rate.call_rate != 0:
                call_rate = sell_rate.call_rate

            invoice.total_call_charges = call_masking_charges + \
                                            (float(call_rate) / 100.0 * call_masking_charges)
            description.append(
                {
                    "description": VAS_SERVICE_ATTRIBUTE_CALL,
                    "hs_heading": '',
                    "value_of_service": intcomma(round(invoice.total_call_charges, 2)),
                    "taxable_value": intcomma(round(float(invoice.total_call_charges), 2)),
                    "cgst_rate": 0,
                    "cgst_amount": 0,
                    "sgst_rate": 0,
                    "sgst_amount": 0,
                    "igst_rate": 0,
                    "igst_amount": 0,
                    "vat_rate": 0,
                    "vat_amount": 0,
                }
            )

            entity = InvoiceEntity(
                        attribute=VAS_SERVICE_ATTRIBUTE_CALL,
                        value=1,
                        rate=Decimal(round(invoice.total_call_charges, 2)),
                        amount=Decimal(round(invoice.total_call_charges, 2)),
                        sell_rate=sell_rate,
                        extra_value=1,
                        contract_invoice=fragment)
            entity.save()

    def calculate_toll_and_parking_charges(self, invoice, description, TripSet, sell_rate, fragment):
        trips, parking, toll = TripSet.get_toll_parking_charges(invoice)
        total = 0
        for att in VAS_CHARGES:
            if att == PARKING_CHARGES:
                amount, invoice.total_parking_charges = parking, parking
            else:
                amount, invoice.total_toll_charges = toll, toll

            total += amount
            if amount > 0:
                description.append(
                    {
                        "description": att,
                        "hs_heading": '',
                        "value_of_service": intcomma(round(amount, 2)),
                        "taxable_value": intcomma(round(float(amount), 2)),
                        "cgst_rate": 0,
                        "cgst_amount": 0,
                        "sgst_rate": 0,
                        "sgst_amount": 0,
                        "igst_rate": 0,
                        "igst_amount": 0,
                        "vat_rate": 0,
                        "vat_amount": 0,
                    }
                )
        self.create_entity_for_toll_and_parking(set([trip for trip in trips if trip.parking_charges])
                                                , PARKING_CHARGES, sell_rate, fragment)
        self.create_entity_for_toll_and_parking(set([trip for trip in trips if trip.toll_charges])
                                                , TOLL_CHARGES, sell_rate, fragment)


class CustomerLedgerExportClass(object):

    def get_filename(self):
        file = 'customer_ledger'
        return file

    def get_query(self, params):
        query_params = []
        if params.get('customer__id__exact'):
            query_params.append(
                Q(customer__id__exact=params['customer__id__exact']))

        if query_params:
            return reduce(operator.and_, query_params)
        return query_params

def mark_invoices_as_expired(invoice_qs, expiry_days):
    date_batch = timezone.now().date().strftime('%d-%m-%Y')
    desc_batch = "Invoice Expired on %s after %s days" % (date_batch, str(expiry_days))

    if invoice_qs:
        batch = InvoiceExpirationBatch.objects.create(description=desc_batch,
                                                      number_of_entries=invoice_qs.count())
        _user = User.objects.get(email=settings.CRON_JOB_DEFAULT_USER)

        objs = [
            InvoiceHistory(
                old_status=invoice.status,
                new_status=EXPIRED,
                invoice=invoice,
                action_owner=None,
                due_date=None,
                created_by=_user,
                reason="%s-%s-%s" % (invoice.invoice_number, str(expiry_days), "Expiry Job")
            )
            for invoice in invoice_qs
        ]

        invoice_qs.update(status=EXPIRED, modified_by=_user,
                        modified_date=datetime.now(), expiry_batch=batch)

        InvoiceHistory.objects.bulk_create(objs)
        batch.end_time = timezone.now()
        batch.save()


def invoices_revenue_booked_month(invoice_ids=None):
    day_of_month = timezone.now().day

    if invoice_ids:
        invoices = CustomerInvoice.objects.filter(id__in=invoice_ids,
                                                  status__in=[CUSTOMER_CONFIRMED, CUSTOMER_ACKNOWLEDGED, PAYMENT_DONE],
                                                  revenue_booked_on__isnull=True)
    else:
        invoices = CustomerInvoice.objects.filter(
            status__in=[CUSTOMER_CONFIRMED, CUSTOMER_ACKNOWLEDGED, PAYMENT_DONE],
            revenue_booked_on__isnull=True)

    logging.info("Total number of invoices to be assigned with revenue month"
                 " : %s" % invoices.count())
    logging.info("These are the invoices for which revenue booked on will be assigned : %s" % invoices)

    if invoices:
        num_of_invoices = invoices.count()
        user_updating = User.objects.get(
            email=settings.CRON_JOB_DEFAULT_USER)

        if day_of_month > REVENUE_THRESHOLD_DATE:
            booked_on_date = datetime_date.today().replace(day=1)
        else:
            booked_on_date = (datetime_date.today() - timedelta(days=(REVENUE_THRESHOLD_DATE + 1))).replace(day=1)

        invoices.update(modified_by=user_updating,
                        modified_date=timezone.now(),
                        revenue_booked_on=booked_on_date,
                        rb_job_date=timezone.now())
