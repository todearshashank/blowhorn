import requests
import json
from math import ceil
from num2words import num2words
from datetime import datetime
from itertools import chain
from django.db.models import F
from rest_framework import status

from django.conf import settings
from blowhorn.contract.models import SellRate
from blowhorn.contract.contract_helpers.sell_rate_helper import SellRateHelper
from django.forms.models import model_to_dict
from blowhorn.common.utils import generate_pdf
from django.core.files.base import ContentFile
from blowhorn.customer.models import *
from blowhorn.company.models import Office
from blowhorn.company.constants import COMPANY_HEADER_FOOTER_DATA
from blowhorn.customer.models import InvoiceOrder, CUSTOMER_CATEGORY_INDIVIDUAL
from blowhorn.company.helpers.company_helper import get_company_bank_account_details
from blowhorn.driver.models import DriverConstants

# CLIENT_URLS = [settings.MZANSIGO_URL,settings.MZANSIGO_URL_B2B 3785 3369]
headers = {'content-type': 'application/json', 'api-key': settings.BLOWHORN_API_KEY}
MZANSIGO_CLIENT_URLS = { settings.MZANSIGO_URL_C2C : 2960,
                            settings.MZANSIGO_URL_B2B : 3785
                        }

class PartnerInvoiceHelper(object):

    def __init__(self):
        self.billed_order_count = 0
        if settings.DEBUG:
            self.one_usd = 0.059
        else:
            response = requests.get(settings.CURRENCY_CONVERTER_URL)
            data = json.loads(response.content)
            self.one_usd = round(1 / data.get('quotes').get('USDZAR'), 4)
        

    def get_order_amount(self, orders, slab):

        total_amount = 0
        count = 0

        for order in orders:
            count += 1
            if slab.start < count <= slab.end:
                total_amount += order.get('total_payable')
            elif count > slab.end:
                break

        return total_amount, count

    def update_flag_for_invoice(self, invoice_list, processing_date):
        if settings.DEBUG:
            return True

        request_data = {
            'invoice_list': invoice_list,
            'pmonth' : processing_date.month,
            'pyear' : processing_date.year
        }

        response = requests.post(settings.MZANSIGO_URL_INVOICE_FLAG,
                                    json=request_data,
                                    headers=headers)
        
        return response.status_code == status.HTTP_200_OK

    def get_orders(self, start_date, end_date, client_url):
        request_data = {
            'start_date': start_date,
            'end_date': end_date,

        }

        response = requests.get(client_url, json=request_data, headers=headers)
        orders = json.loads(response.content)
        return orders

    def get_amount_in_usd(self, total_amount):
        return round(self.one_usd * total_amount, 3)

    def _get_slab_amount(self, slab, value, count):
        amount = 0
        value_extra = 0
        fixed_amount = 0
        if slab.end is None:
            print('End slab is None. Skipping slab calculation')
            return amount, value_extra, fixed_amount

        if slab.is_ceiling_applicable:
            self.is_ceiling_applicable = True
            value = ceil(value)

        if slab.start < count < slab.end: #value in range(slab.start, slab.end + 1):
            amount += float(value) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount

        return amount, count, fixed_amount

    def get_invoice_amount_in_usd(self, helper_sell_rate, sell_rate, orders):
        shipment_slabs = sell_rate.sellshipmentslab_set.all()

        amount = 0
        fixed_amount = 0
        invoice_amount = 0
        calculation = []
        billed_order_count = 0

        for slab in shipment_slabs:
            slab_display = model_to_dict(slab)
            slab_display.update({"name": slab._meta.label})
            sku = slab.sku if hasattr(slab, "sku") else None
            total_amount, count = self.get_order_amount(orders, slab)
            total_amount_in_usd = self.get_amount_in_usd(total_amount)
            slab_amount, value_extra, fixed_amount_ = self._get_slab_amount(slab, total_amount_in_usd, count)
            invoice_amount += slab_amount
            fixed_amount += fixed_amount_
            amount = float(amount) + float(slab_amount)
            if slab_amount > 0:
                calculation.append({
                    'attribute': helper_sell_rate._get_attributes_from_slab(slab, sku),
                    'slab': slab_display,
                    'value': total_amount_in_usd,
                    'amount': round(slab_amount, 2),
                    'value_extra': value_extra - billed_order_count if value_extra > billed_order_count else 0
                })
            billed_order_count += value_extra

        self.billed_order_count = billed_order_count

        return invoice_amount, calculation

    def get_context(self, invoice, customer, company_office, is_approve):
        customer_address = customer.head_office_address
        head_office = Office.objects.filter(is_head=True).first()
        company_extra_data = COMPANY_HEADER_FOOTER_DATA.get(settings.WEBSITE_BUILD)

        description = [{
            "description": 'IT implementation and Platform integration advisory services ',
            "hs_heading": '',
            "value_of_service": '',
            "discount": '0',
            "taxable_value": round(invoice.total_amount, 2),
        }]

        adjustment_record = invoice.customerinvoiceadjustment_set.values(
            'category__name', 'category__is_tax_applicable', 'comments').annotate(total_amount=Sum('total_amount'))

        total_adjustment = 0
        for record in adjustment_record:

            total_adjustment += record.get('total_amount')
            display_text = record.get('category__name')
            if record['comments']:
                display_text += "(" + record['comments'] + ")"
            description.append(
                {
                    "description": display_text,
                    "hs_heading": '',
                    "value_of_service": '',
                    "discount": '0',
                    "taxable_value": round(record.get('total_amount'), 2),

                }
            )
        
        invoice.total_amount = invoice.total_amount + float(total_adjustment)
        invoice.taxable_amount = invoice.taxable_amount + float(total_adjustment)
        invoice.taxable_adjustment = total_adjustment
        invoice.total_service_charge = invoice.total_service_charge + float(total_adjustment)

        context = {
            "logo": settings.APPS_DIR.path('static') + "img/logo.png"
            if is_approve else settings.APPS_DIR.path('static') + "img/logo_alt.png",
            "company": {
                "name": company_office.company.name if company_office else '',
                "address": company_office.address if company_office else '',
                "gstin": company_office.gst_identification_no if company_office else '',
                "state": company_office.state.name,
                "bank_details": get_company_bank_account_details(None),
                "cin": company_office.company.cin,
                "country": str(company_office.state.country),
                "pan": company_office.company.pan,
                "registered_address" : head_office.address,
                "telephone_number" : settings.BLOWHORN_SUPPORT_NUMBER,
                "website" : settings.HOST,
                "email" : company_extra_data.get('email'),
            },

            "customer": {
                "name": customer.legalname,
                "address": str(customer_address.line1) if customer_address else '',
                "state": customer_address.state,
                "country": str(customer_address.country),
                "gstin": ''
            },

            "invoice": {
                "general": {
                    "invoice_no": invoice.invoice_number if invoice.invoice_number else '',
                    "invoice_date": invoice.invoice_date.strftime("%d-%b-%Y") if invoice.invoice_date else '',
                    "service_period_start": invoice.service_period_start.strftime("%d-%b-%Y"),
                    "service_period_end": invoice.service_period_end.strftime("%d-%b-%Y"),
                    "aggregate_values": ' - ',
                    "comments": '',
                    "invoice_type": 'TAX INVOICE',
                    "usd_amount": self.one_usd,
                    "currency_date": timezone.now().date(),
                    "LUT_NO": DriverConstants().get('LUT_NO', 'AD2909190012232')
                },
                "description": description,
                "description_total": {
                    "value_in_figure": intcomma(round(invoice.total_amount, 2)),
                    "value_in_words": num2words(round(invoice.total_amount, 2)),
                    "taxable_value": intcomma(round(invoice.total_amount, 2)),
                    "total_without_adj":intcomma(round((float(invoice.total_amount) - float(invoice.taxable_adjustment)), 2)),
                }
            }
        }
        return context

    def get_invoice_number(self, invoice):
        series = '%s%d%02d' % ('ZA', invoice.invoice_date.year % 100, invoice.invoice_date.month)
        last_invoice = invoice.find_last_gst_number(series)
        if last_invoice is None:
            ''' No such GST invoices, Generate the first number '''
            next_sequence = 1
        else:
            last_number = last_invoice.invoice_number
            m = re.match(r'(\S{6})(\d{6})', last_number)
            if m:
                series, number = m.group(1), m.group(2)
                print("Prefix:%s, Number:%s" % (series, number))
                next_sequence = int(number) + 1
            else:
                print('Invoice format invalid')

        new_invoice_number = '%s%06d' % (series, next_sequence)
        print('Invoice number: %s' % (new_invoice_number))

        return new_invoice_number

    def generate_invoice(self, customer_id, sellrate_id, start_date, end_date, is_approve=False):
        if settings.WEBSITE_BUILD != settings.DEFAULT_BUILD:
            return

        customer = Customer.objects.get(id=customer_id)
        company_office = Office.objects.first()

        invoice_amount, calculation, orders, updated_invoice_list = \
                    self.get_invoice_amount_for_different_order_type(sellrate_id,
                                                        start_date, end_date)
        
        invoice, is_created = CustomerInvoice.objects.get_or_create(
            service_period_start=datetime.strptime(start_date, "%Y-%m-%d"),
            service_period_end=datetime.strptime(end_date, "%Y-%m-%d"),
            customer_id=customer_id)

        invoice.total_amount = invoice_amount
        invoice.taxable_amount = invoice_amount
        invoice.charges_basic = invoice_amount
        invoice.is_stale = False
        invoice.bh_source_state = company_office.state
        invoice.invoice_state = company_office.state
        invoice.total_service_charge = invoice_amount
        if is_approve and invoice.invoice_date and not invoice.invoice_number:
            invoice.invoice_number = self.get_invoice_number(invoice)
        invoice.historical_invoice_dump = json.dumps(orders)

        context = self.get_context(invoice, customer, company_office, is_approve)
        context['invoice']['summary'] = calculation
        invoice_template = "pdf/invoices/b2b_v2/partner_invoice.html"
        header = {"template": "pdf/invoices/b2b_v2/header.html", "context": context}
        footer = {"template": "pdf/invoices/b2b_v2/footer.html", "context": context}

        bytes = generate_pdf(invoice_template, context, header=header, footer=footer)

        file_name = str(invoice.invoice_number) + ".pdf" if invoice.invoice_number else str(
            invoice.id) + ".pdf"
        content = ContentFile(bytes)
        invoice.file.save(file_name, content, save=True)
        invoice.save()

        if len(updated_invoice_list) != 0:
            cp_date = datetime.strptime(start_date, "%Y-%m-%d")
            result = self.update_flag_for_invoice(updated_invoice_list, cp_date)

            if not result:
                invoice.is_stale = True
                invoice.save()

    def get_invoice_amount_for_different_order_type(self, sellrate_id, start_date, end_date):
        total_amount = 0
        total_calculation = []
        order_list = []
        updated_invoice_list = []

        for client_url in MZANSIGO_CLIENT_URLS:
            _sellrate_id = MZANSIGO_CLIENT_URLS[client_url]
            sell_rate = SellRate.objects.get(id=_sellrate_id)
            helper_sell_rate = SellRateHelper(sell_rate, start_date, end_date)
            orders = self.get_orders(start_date, end_date, client_url)
            invoice_amount, calculation = self.get_invoice_amount_in_usd(helper_sell_rate, sell_rate, orders)
            total_amount += invoice_amount
            total_calculation.extend(calculation)
            order_list.append(orders)

            if client_url == settings.MZANSIGO_URL_B2B:
                updated_invoice_list = [data.get('number') for data in orders]

        len_qs = len(order_list)

        if len_qs == 0:
            orders = order_list
        else:
            orders = list(chain(*order_list))

        return total_amount,total_calculation,orders,updated_invoice_list

