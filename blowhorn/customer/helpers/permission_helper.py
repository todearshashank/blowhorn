from blowhorn.customer.models import PermissionLevel, \
    UserPermission, AccountUser

from blowhorn.customer.constants import MEMBER_ACTIVE


# class PermissionClass(object):
#     def __init__(self, **kwargs):
#         self.cities = []
#         self.hub = []
#         self.division = []
#         self.modules = {}

#     def user_permissions(self, record):
#         self.cities.append({"name": record.city.name,
#             'city_id':record.city_id}) if record.city_id else ''
#         self.hub.append({"name": record.hub.name,
#                          "hub_id":record.hub_id}) if record.hub_id else ''
#         self.division.append({"name": record.division.name,
#                               "division_id": record.division_id}) if record.division_id else ''

#     def module_permissions(self, record):
#         if record.module.module not in self.modules.keys():
#             self.modules[record.module.module] = {
#                 "grant": record.grant,
#                 "submodule": [ {"title": x.title, 'grant': x.grant} for x in record.module.submodule_set.all()]
#                 # "has_write_permission": record.has_write_permission
#             }
#         if record.grant and not self.modules[record.module.module]['grant']:
#             self.modules[record.module.module]['grant'] = record.grant
#         # if record.has_write_permission and not self.modules[record.module.module]['has_write_permission']:
#         #     self.modules[record.module.module]['has_write_permission'] = record.has_write_permission

#     def get_user_permission(self, user):
#         userLevelpermissions = AccountUserPermissionLevel.objects.filter(account_user__user=user).select_related('city', 'hub', 'division')
#         userPermissions = UserPermission.objects.filter(user__user=user).select_related('module')
#         userPermissions = userPermissions.prefetch_related('module__submodule_set')
#         groupPermissions = UserGroupPermission.objects.filter(group__account_user__user=user).select_related('module')
#         groupPermissions = groupPermissions.prefetch_related('module__submodule_set')
#         [self.user_permissions(x) for x in userLevelpermissions]
#         [self.module_permissions(x) for x in userPermissions]
#         [self.module_permissions(x) for x in groupPermissions]




class PermissionBackend(object):

    def _get_accountuser_permissions(self, user_obj):
        return user_obj.user_permissions.all()

    def _get_accountgroup_permissions(self, user_obj):
        user_groups_field = AccountUser._meta.get_field('groups')
        user_groups_query = 'group__%s' % user_groups_field.related_query_name()
        return UserPermission.objects.filter(**{user_groups_query: user_obj})

    def _get_permissions(self, user_obj, obj, from_name):
        """
        Return the permissions of `user_obj` from `from_name`. `from_name` can
        be either "group" or "user" to return permissions from
        `_get_group_permissions` or `_get_user_permissions` respectively.
        """
        if hasattr(user_obj, 'customer'):
            # @todo need to return all
            return

        if not user_obj.is_active or obj is not None:
            return set()

        perm_cache_name = '_%s_perm_cache' % from_name
        if not hasattr(user_obj, perm_cache_name):
            if user_obj.is_customer():
                perms = UserPermission.objects.all()
            else:
                perms = getattr(self, '_get_%s_permissions' % from_name)(user_obj)

            perms = perms.values_list('module__title', 'codename').order_by()
            setattr(user_obj, perm_cache_name, {"%s.%s" % (ct, name) for ct, name in perms})

        return getattr(user_obj, perm_cache_name)

    def get_user_permissions(self, user_obj, obj=None):
        """
        Return a set of permission strings the user `user_obj` has from their
        `user_permissions`.
        """
        return self._get_permissions(user_obj, obj, 'accountuser')

    def get_group_permissions(self, user_obj, obj=None):
        """
        Return a set of permission strings the user `user_obj` has from the
        groups they belong.
        """
        return self._get_permissions(user_obj, obj, 'accountgroup')

    def get_all_permissions(self, user_obj, obj=None):
        if not user_obj.is_active or obj is not None:
            return set()

        if not hasattr(user_obj, '_perm_cache'):
            user_obj._perm_cache = self.get_all_permissions(user_obj)

        return user_obj._perm_cache

    def has_perm(self, user_obj, perm, obj=None):
        return user_obj.is_active and \
            self.has_perm(user_obj, perm, obj=obj)

    def get_available_permissions(self):
        return UserPermission.objects\
            .select_related('module')\
            .values('name', 'module__title')\
            .order_by('module__title')
