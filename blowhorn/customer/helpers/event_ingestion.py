import logging
import requests
import base64
import json
from datetime import datetime, timedelta

from blowhorn.customer.redis_models import Customer as RedisCustomer
from blowhorn.customer.constants import HTTP_POST, AUTH_TYPE_BASIC
from blowhorn.activity.models import DriverTripLog
from blowhorn.activity.serializers import GPSEventSerializer


class GPSEventIngestionService(object):

    def __init__(self, customer_id):

        self.customer_id = customer_id
        self.gps_event_config = RedisCustomer(customer_id)['gps_event_config']
        self.config_id = self.gps_event_config.get('config_id', None)
        self.is_enabled = self.gps_event_config.get('is_enabled', False)
        self.interval = self.gps_event_config.get('interval', 0)
        self.contract_ids = self.gps_event_config.get('contract_ids', None)
        self.api_endpoint = self.gps_event_config.get('api_endpoint', None)
        self.http_request_type = self.gps_event_config.get(
            'http_request_type', None)
        self.auth_config = self.gps_event_config.get('auth_config', None)
        self.max_records_per_driver = self.gps_event_config.get(
            'max_records_per_driver', -1)
        self.timestamp_event_last_push = self.gps_event_config.get(
            'timestamp_event_last_push', {})

        if not self.config_id or not self.contract_ids or \
            not self.api_endpoint or not self.http_request_type or not \
            self.auth_config or not self.max_records_per_driver or \
            not self.interval:
            logging.warning(
                "GPS Event Ingestion - Configuration not valid for customer "
                "%s" % self.customer_id)

    def __get_serializer(self):
        """
        Returns the corresponding serializer that suits customer's
        required format.
        For now, defaulting to only one format.
        """

        return GPSEventSerializer

    def __get_data(self):
        """
        Returns the gps event data for the customer for given time.
        Performs slicing of gps_events at driver level if required.
        """

        current_time = datetime.now()

        all_gps_events = DriverTripLog.objects.filter(
            createdTime__gte=current_time - timedelta(seconds=self.interval),
            createdTime__lte=current_time,
            contractId__in=self.contract_ids
        )

        driver_ids_active = all_gps_events.values_list('driverId', flat=True)

        gps_events_stripped = []

        for driver_id in driver_ids_active:
            gps_events_driver = all_gps_events.filter(
                driverId=driver_id)[:self.max_records_per_driver]
            gps_events_stripped.extend(list(gps_events_driver))

        logging.info(
            "GPS Event Ingestion - No. of drivers : %s | No. of records : %s"
            %(len(driver_ids_active), len(gps_events_stripped)))

        if gps_events_stripped:
            serializer = self.__get_serializer()
            return serializer(gps_events_stripped, many=True).data

        return None

    def __perform_ingestion(self, data):
        """
        Pass the given `data` to customer by the configured way of request.
        """

        if self.http_request_type == HTTP_POST:
            headers = {'Content-Type': 'application/json'}
            if self.auth_config['type'] == AUTH_TYPE_BASIC:
                username, password = \
                    self.auth_config['credentials']['username'], \
                    self.auth_config['credentials']['password']
                b64_val = base64.b64encode(
                    bytes(username + ":" + password, encoding='utf-8'))
                headers['Authorization'] = \
                    "Basic %s" % str(b64_val.decode("utf-8"))

            response = requests.post(
                self.api_endpoint, data=json.dumps(data), headers=headers)

            return response

        return None

    def run(self):
        if not self.is_enabled:
            logging.warning(
                "GPS Event Ingestion - GPS Configuration found for customer "
                "%s but not enabled." % self.customer_id)
            return

        data = self.__get_data()

        if data:
            response = self.__perform_ingestion(data)

            if response:
                logging.info(
                    "GPS Event Ingestion - for customer %s : "
                    "Response status code : %s | Response content : %s."
                    % (self.customer_id, response.status_code,
                       response.content))
