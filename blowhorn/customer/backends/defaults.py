"""
    Backend classes should provide common interface
"""

import email.utils
import inspect
import uuid

from django.conf import settings
from django.conf.urls import url
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.http import Http404, HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.template import loader
from django.utils.translation import ugettext as _
from phonenumber_field.phonenumber import PhoneNumber

from .forms import UserRegistrationForm
from .forms import org_registration_form
from .tokens import RegistrationTokenGenerator, REGISTRATION_TIMEOUT_DAYS
from organizations.compat import reverse
from organizations.utils import create_organization
from organizations.utils import default_org_model
from organizations.utils import model_field_attr

from blowhorn.customer.constants import MEMBER_ACTIVE
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.users.utils import validate_customer_password


class BaseBackend(object):
    """
    Base backend class for registering and inviting users to an organization
    """
    registration_form_template = 'organizations/register_form.html'
    activation_success_template = 'organizations/register_success.html'
    success_url = ''

    def __init__(self, org_model=None):
        self.user_model = get_user_model()
        self.org_model = org_model or default_org_model()

    def get_urls(self):
        raise NotImplementedError

    def get_success_url(self):
        """Will return the class's `success_url` attribute unless overridden"""
        return '/'

    def get_form(self, **kwargs):
        """Returns the form for registering or inviting a user"""
        if not hasattr(self, 'form_class'):
            raise AttributeError(_("You must define a form_class"))
        return self.form_class(**kwargs)

    def get_token(self, user, **kwargs):
        """Returns a unique token for the given user"""
        return RegistrationTokenGenerator().make_token(user)

    def get_username(self):
        """
        Returns a UUID-based 'random' and unique username.

        This is required data for user models with a username field.
        """
        return str(uuid.uuid4())[:model_field_attr(self.user_model,
                                                   self.user_model.USERNAME_FIELD,
                                                   'max_length')]

    def update_user_status(self, account_set, user):
        """
        :param account_set: through modal instance (AccountOwner/AccountUser)
        :param user: instance of User
        """
        for a in account_set.all():
            a.status = MEMBER_ACTIVE.lower()
            a.save()

    def activate_organizations(self, user):
        """
        Activates the related organizations for the user.

        It only activates the related organizations by model type - that is, if
        there are multiple types of organizations then only organizations in
        the provided model class are activated.
        """
        try:
            relation_name = self.org_model().user_relation_name
        except TypeError:
            # No org_model specified, raises a TypeError because NoneType is
            # not callable. This the most sensible default:
            relation_name = "organizations_organization"

        organization_set = getattr(user, relation_name)
        for org in organization_set.filter(is_active=False):
            if not org.is_active:
                org.is_active = True
                org.save()

        self.update_user_status(getattr(user, 'customer_accountuser'), user)

    def activate_view(self, request, user_id, token):
        """
        View function that activates the given User by setting `is_active` to
        true if the provided information is verified.
        """
        try:
            user = self.user_model.objects.get(id=user_id)
        except self.user_model.DoesNotExist:
            raise Http404(_("Your URL may have expired."))

        if not RegistrationTokenGenerator().check_token(user, token):
            raise Http404(_("Your URL may have expired."))

        form = self.get_form(data=request.POST or None, instance=user)
        if form.is_valid():
            form.instance.is_active = True
            is_valid, msg = validate_customer_password(form.cleaned_data['password'])
            if not is_valid:
                return HttpResponse(msg)
            user = form.save()
            user.set_password(form.cleaned_data['password'])
            user.save()
            self.activate_organizations(user)
            user = authenticate(username=form.cleaned_data[user.USERNAME_FIELD],
                                password=form.cleaned_data['password'])
            login(request, user)
            return redirect('/')

        return render(request, self.registration_form_template, {'form': form})

    def send_reminder(self, user, sender=None, **kwargs):
        """Sends a reminder email to the specified user"""
        if user.is_active:
            return False
        token = RegistrationTokenGenerator().make_token(user)
        kwargs.update({
            'token': token,
            'expiry_days': REGISTRATION_TIMEOUT_DAYS
        })
        self.email_message(user, self.reminder_subject, self.reminder_body,
                           sender, **kwargs).send()

    def email_message(self, user, subject_template, body_template, sender=None,
                      message_class=EmailMessage, **kwargs):
        """
        Returns an email message for a new user. This can be easily overridden.
        For instance, to send an HTML message, use the EmailMultiAlternatives
        message_class and attach the additional content.
        """
        if sender:
            from_email = "Blowhorn <%s>" % email.utils.parseaddr(settings.DEFAULT_FROM_EMAIL)[1]
        else:
            from_email = 'Blowhorn <%s>' % settings.DEFAULT_FROM_EMAIL

        kwargs.update({'sender': sender, 'user': user})

        subject_template = loader.get_template(subject_template)
        body_template = loader.get_template(body_template)
        subject = subject_template.render(kwargs).strip()
        body = body_template.render(kwargs)
        return message_class(subject, body, from_email, [user.email])


class RegistrationBackend(BaseBackend):
    """
    A backend for allowing new users to join the site by creating a new user
    associated with a new organization.
    """
    # NOTE this backend stands to be simplified further, as email verification
    # should be beyond the purview of this app
    activation_subject = 'organizations/email/activation_subject.txt'
    activation_body = 'organizations/email/activation_body.html'
    reminder_subject = 'organizations/email/reminder_subject.txt'
    reminder_body = 'organizations/email/reminder_body.html'
    form_class = UserRegistrationForm

    def get_success_url(self):
        return '/' #reverse('registration_success')

    def get_urls(self):
        return [
            url(r'^complete/$', view=self.success_view,
                name="registration_success"),
            url(r'^(?P<user_id>[\d]+)-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                view=self.activate_view, name="registration_register"),
            url(r'^$', view=self.create_view, name="registration_create"),
        ]

    def register_by_email(self, email, sender=None, request=None, **kwargs):
        """
        Returns a User object filled with dummy data and not active, and sends
        an invitation email.
        """
        try:
            user = self.user_model.objects.get(email=email)
        except self.user_model.DoesNotExist:
            user = self.user_model.objects.create(
                username=self.get_username(),
                email=email,
                password=self.user_model.objects.make_random_password()
            )
            user.is_active = False
            user.save()
        self.send_activation(user, sender, **kwargs)
        return user

    def invite_owner_by_email(self, customer, **kwargs):
        """
        Sends an invitation email to organization owner.
        """
        user = customer.user
        site = Site.objects.get(pk=settings.SITE_ID)
        InvitationBackend().invite_by_email(
            user.email,
            phone_number=user.phone_number,
            **{'name': user.name,
                'organization': customer,
                'domain': site,
                'base_url': settings.HOST,
                'resend': True
            })
        return user

    def send_activation(self, user, sender=None, **kwargs):
        """
        Invites a user to join the site
        """
        token = self.get_token(user)
        kwargs.update({
            'token': token,
            'expiry_days': REGISTRATION_TIMEOUT_DAYS
        })
        self.email_message(user, self.activation_subject, self.activation_body,
                           sender, **kwargs).send()

    def create_view(self, request):
        """
        Initiates the organization and user account creation process
        """
        try:
            if request.user.is_authenticated:
                return redirect('/dashboard')
        except TypeError:
            if request.user.is_authenticated:
                return redirect('/dashboard')

        form = org_registration_form(self.org_model)(request.POST or None)
        if form.is_valid():
            try:
                user = self.user_model.objects.get(
                    email=form.cleaned_data['email'])
            except self.user_model.DoesNotExist:
                user = self.user_model.objects.create(
                    username=self.get_username(),
                    email=form.cleaned_data['email'],
                    password=self.user_model.objects.make_random_password())
                user.is_active = False
                user.save()
            else:
                return redirect("organization_add")
            organization = create_organization(user, form.cleaned_data['name'],
                                               form.cleaned_data['slug'],
                                               is_active=False)
            return render(request, self.activation_success_template,
                          {'user': user, 'organization': organization})
        return render(request, self.registration_form_template, {'form': form})

    def success_view(self, request):
        return render(request, self.activation_success_template, {})


class InvitationBackend(BaseBackend):
    """
    A backend for inviting new users to join the site as members of an
    organization.
    """
    notification_subject = 'organizations/email/notification_subject.txt'
    notification_body = 'organizations/email/notification_body.html'
    invitation_subject = 'organizations/email/invitation_subject.txt'
    invitation_body = 'organizations/email/invitation_body.html'
    reminder_subject = 'organizations/email/reminder_subject.txt'
    reminder_body = 'organizations/email/reminder_body.html'
    form_class = UserRegistrationForm

    def get_success_url(self):
        # TODO get this url name from an attribute
        return redirect('/dashboard')

    def get_urls(self):
        # TODO enable naming based on a model?
        return [
            url(r'^(?P<user_id>[\d]+)-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                view=self.activate_view, name="invitations_register"),
        ]

    def invite_by_email(self, email, phone_number=None, sender=None,
                        request=None, **kwargs):
        """Creates an inactive user with the information we know and then sends
        an invitation email for that user to complete registration.

        If your project uses email in a different way then you should make to
        extend this method as it only checks the `email` attribute for Users.
        """
        created = False
        try:
            user = self.user_model.objects.get(email=email)
        except self.user_model.DoesNotExist:
            # TODO break out user creation process
            if 'name' in inspect.getargspec(self.user_model.objects.create_user).args:
                user = self.user_model.objects.create(
                    name=kwargs.pop('name'),
                    email=email,
                    password=self.user_model.objects.make_random_password())
            else:
                _data = {
                    'email': email,
                    'password': self.user_model.objects.make_random_password()
                }
                _name = kwargs.pop('name', None)
                if _name is not None:
                    _data['name'] = _name

                user = self.user_model.objects.create(**_data)

            if phone_number and settings.ACTIVE_COUNTRY_CODE not in str(phone_number):
                user.phone_number = PhoneNumber(
                    settings.ACTIVE_COUNTRY_CODE,
                    phone_number
                )
            created = True
            user.is_active = False
            user.save()
        kwargs['created'] = created
        self.send_invitation(user, sender, **kwargs)
        if kwargs.pop('created_required', None):
            return user, created

        return user

    def send_invitation(self, user, sender=None, **kwargs):
        """An intermediary function for sending an invitation email that
        selects the templates, generating the token, and ensuring that the user
        has not already joined the site.
        """
        if user.is_active and not kwargs.pop('resend', None):
            return False
        token = self.get_token(user)
        kwargs.update({
            'token': token,
            'expiry_days': REGISTRATION_TIMEOUT_DAYS
        })
        mail = self.email_message(user, self.invitation_subject, self.invitation_body,
                           sender, **kwargs)
        mail.content_subtype = 'html'
        mail.send()
        return True

    def send_notification(self, user, sender=None, **kwargs):
        """
        An intermediary function for sending an notification email informing
        a pre-existing, active user that they have been added to a new
        organization.
        """
        if not user.is_active:
            return False
        self.email_message(user, self.notification_subject,
                           self.notification_body, sender, **kwargs).send()
        return True
