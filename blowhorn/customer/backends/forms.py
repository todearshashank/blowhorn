from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.formfields import PhoneNumberField

from blowhorn.utils.widgets.phonenumber_field import PhoneNumberPrefixWidget


class UserRegistrationForm(forms.ModelForm):
    """
    Form class for completing a user's registration and activating the
    User.

    The class operates on a user model which is assumed to have the required
    fields of a BaseUserModel
    """
    name = forms.CharField(max_length=30)
    password = forms.CharField(max_length=30, widget=forms.PasswordInput)
    password_confirm = forms.CharField(max_length=30,
                                       widget=forms.PasswordInput)
    phone_number = PhoneNumberField(widget=PhoneNumberPrefixWidget(
                attrs={'placeholder': 'Contact', 'class': "form-control"},
                initial=settings.ACTIVE_COUNTRY_CODE
        )
    )

    def __init__(self, *args, **kwargs):
        super(UserRegistrationForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['name', 'email', 'phone_number', 'password',
                                'password_confirm']

    def clean(self):
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data.get("password_confirm")
        if password != password_confirm or not password:
            raise forms.ValidationError(_("Your password entries must match"))
        return super(UserRegistrationForm, self).clean()

    class Meta:
        model = get_user_model()
        fields = ['name', 'email', 'phone_number', 'password',
                  'password_confirm']


def org_registration_form(org_model):
    """
    Generates a registration ModelForm for the given organization model class
    """

    class OrganizationRegistrationForm(forms.ModelForm):
        """Form class for creating new organizations owned by new users."""
        email = forms.EmailField()

        class Meta:
            model = org_model
            exclude = ('is_active', 'users')

        def save(self, *args, **kwargs):
            self.instance.is_active = False
            super(OrganizationRegistrationForm, self).save(*args, **kwargs)

    return OrganizationRegistrationForm
