import re
import json
from django import forms
from django.forms import fields as formfields
from django.forms.models import BaseInlineFormSet
from django.contrib.admin.helpers import ActionForm
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.utils.html import format_html
from django.db.models import Q
from django.conf import settings

from blowhorn.oscar.core.loading import get_model

from blowhorn.address.models import State, City
from blowhorn.users.models import User
from blowhorn.customer.models import CUSTOMER_CATEGORY_NON_ENTERPRISE, \
    CUSTOMER_CATEGORY_TRANSITION, CUSTOMER_CATEGORY_ENTERPRISE, CustomerReceivable, InvoicePayment
from blowhorn.driver.models import DriverRatingAggregate, RatingConfig
from .models import (CustomerTaxInformation,
                     Customer, CUSTOMER_CATEGORY_INDIVIDUAL)
from blowhorn.utils.functions import validate_phone_number
from blowhorn.contract.models import Contract
from blowhorn.contract.constants import CONTRACT_TYPE_SHIPMENT, \
    CONTRACT_STATUS_ACTIVE
from blowhorn.customer.constants import CREATED, CREDIT_NOTE, \
    TAX_INVOICE, REJECTED, INVOICE_CONFIG_ACTIVE, DEBIT_NOTE
from blowhorn.utils.datetimepicker import DateTimePicker
from blowhorn.common.middleware import current_request
from blowhorn.common.fields import CustomModelChoiceField
from blowhorn.customer.utils import CustomerUtils, validate_customer_gst_info
from blowhorn.company.models import Company
from blowhorn.contract.models import ServiceHSNCodeMap
from config.settings import status_pipelines as StatusPipeline
from .submodels.invoice_models import ManualUploadReason, CustomerInvoice

Office = get_model('company', 'Office')

class CustomerInvoiceEditForm(forms.ModelForm):
    contract_type = forms.CharField(label='Contract Type', disabled=True)
    customer = forms.CharField(label='Customer Legal Name', disabled=True)
    gstin = forms.CharField(label='GSTIN', disabled=True)
    business_type = forms.CharField(label='Business Type', disabled=True)
    vehicle_classes = forms.CharField(label='Vehicle Classes', disabled=True)
    city = forms.CharField(label='City', disabled=True)
    service_state_code = forms.CharField(label='Code', disabled=True)

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            obj = kwargs['instance']
            contract = obj.contract
            tax_info = CustomerTaxInformation.objects.filter(
                customer=obj.contract.customer, state=obj.bh_source_state
            )

            gstin = ''
            service_state_code = ''
            if tax_info.exists():
                gstin = tax_info.first().gstin
                service_state_code = gstin[0:2]

            vehicle_classes = contract.vehicle_classes.all()
            vehicle_classes = ', '.join(
                [x.commercial_classification for x in vehicle_classes])

            initial = {
                'contract_type': contract.contract_type,
                'customer': contract.customer.legalname,
                'gstin': gstin,
                'business_type': contract.customer.customer_category,
                'vehicle_classes': vehicle_classes,
                'city': contract.city,
                'service_state_code': service_state_code
            }

            kwargs['initial'] = initial
        super(CustomerInvoiceEditForm, self).__init__(*args, **kwargs)


class CustomerContactFormSet(BaseInlineFormSet):
    phone_number = forms.CharField(label='Mobile Number',
                                   validators=[validate_phone_number],
                                   required=True, max_length=10)


class CustomerForm(forms.ModelForm):
    email = forms.EmailField()
    phone_number = forms.CharField(label='Mobile Number',
                                   validators=[validate_phone_number],
                                   required=True, max_length=10)

    password = forms.CharField(max_length=32, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            customer = kwargs['instance']
            user = customer.user if customer else None
            if user:
                initial = {'email': user.email,
                           'phone_number': user.phone_number}
                kwargs['initial'] = initial
        super(CustomerForm, self).__init__(*args, **kwargs)

    def clean(self):
        super(CustomerForm, self).clean()
        email = self.cleaned_data.get('email')
        user = User.objects.filter(email=email)
        name = self.cleaned_data.get('name')
        phone_number = self.cleaned_data.get('phone_number')
        if not name:
            raise ValidationError({
                'name': _("Customer Name is Mandatory")
            })
        if user:
            raise ValidationError({
                'email': _("Email already registered")
            })

    class Meta:
        model = Customer
        exclude = ['user']


class CustomerInvoiceConfigForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super(CustomerInvoiceConfigForm, self).__init__(*args, **kwargs)
        qs = None
        if 'instance' in kwargs:
            config = kwargs['instance']
            customer = config.customer if config else None
            if customer and 'contracts' in self.fields:
                qs = Contract.objects.filter(customer=customer, status=CONTRACT_STATUS_ACTIVE)
                self.fields['contracts'].autocomplete = False

        if not 'instance' in kwargs:
            self.fields['customer'].autocomplete = False
            self.fields['customer'].queryset = Customer.objects.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        if not qs:
            qs = Contract.objects.filter(status=CONTRACT_STATUS_ACTIVE)
        # self.fields['contracts'].autocomplete = False
        self.fields['contracts'].queryset = qs

    def clean(self):
        super(CustomerInvoiceConfigForm, self).clean()
        contracts = self.cleaned_data.get('contracts')
        status = self.cleaned_data.get('status')
        invoice_state = self.cleaned_data.get('invoice_state')
        customer = self.cleaned_data.get('customer')
        name = self.cleaned_data.get('name')
        selected_contract = []

        if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            if not CustomerTaxInformation.objects.filter(customer=customer,
                                                         state=invoice_state).exists():
                raise ValidationError(
                    'There is no GSTIN number defined for '
                    'the target state in the customer tax info')

        if not contracts:
            raise ValidationError(
                'Every configuration should have atleast one contract.')

        if not status:
            raise ValidationError(
                'please select a status for invoice config')

        if not contracts[0].customer.head_office_address:
            raise ValidationError(
                'Head office address is mandatory to add (or modify) invoice configuration.')

        contract_types = set()
        contract_service_types = set()
        contract_service_states = set()
        config = set()

        service_tax_category, hsn_code = contracts[0].service_tax_category, contracts[0].hsn_code \
            if contracts else None

        for contract in contracts:

            if contract.service_tax_category != service_tax_category and contract.hsn_code != hsn_code:
                raise ValidationError({
                    'contracts': 'All contracts should have same service tax category and hsn code'
                })

            if status == INVOICE_CONFIG_ACTIVE:
                selected_contract.append(contract)
            contract_types.add(contract.contract_type)

            if contract.status == CONTRACT_STATUS_ACTIVE:
                contract_service_types.add(contract.service_tax_category)

            if len(contract_service_types) > 1:
                config.add(name)
            contract_service_states.add(contract.service_state)

        if CONTRACT_TYPE_SHIPMENT in contract_types and len(contract_types) > 1:
            raise ValidationError(
                'Shipment contracts cannot be clubbed with other type contracts in same config.')

        if len(contract_service_types) > 1:
            raise ValidationError(
                "Contracts under different service tax category (GTA/BSS)"
                "cannot be clubbed together in same config. check %s" % config)

        # if len(contract_service_states) > 1:
        #     raise ValidationError(
        #         "Contracts from different states cannot be clubbed together in same config.")

        if any(selected_contract.count(x) > 1 for x in selected_contract):
            raise ValidationError(
                'Each contract should be in one configuration only.')


class CustomerInvoiceConfigEditForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CustomerInvoiceConfigEditForm, self).__init__(*args, **kwargs)
        qs = None
        if 'instance' in kwargs:
            config = kwargs['instance']
            customer = config.customer if config else None
            if customer and 'contracts' in self.fields:
                qs = Contract.objects.filter(customer=customer, status=CONTRACT_STATUS_ACTIVE)
                self.fields['contracts'].autocomplete = False
        if not 'instance' in kwargs:
            self.fields['customer'].autocomplete = False
            self.fields['customer'].queryset = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        if not qs:
            qs = Contract.objects.filter(status=CONTRACT_STATUS_ACTIVE)

        # self.fields['contracts'].autocomplete = False
        self.fields['contracts'].queryset = qs

    def clean(self):
        super(CustomerInvoiceConfigEditForm, self).clean()
        contracts = self.cleaned_data.get('contracts')
        status = self.cleaned_data.get('status')
        invoice_state = self.cleaned_data.get('invoice_state')
        customer = self.instance.customer
        selected_contract = []

        if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            if not CustomerTaxInformation.objects.filter(customer=customer,
                                                         state=invoice_state).exists():
                raise ValidationError(
                    'There is no GSTIN number defined for '
                    'the target state in the customer tax info')

        if not contracts:
            raise ValidationError(
                'Every configuration should have atleast one contract.')

        if not status:
            raise ValidationError(
                'please select a status for invoice config')

        if not contracts[0].customer.head_office_address:
            raise ValidationError(
                'Head office address is mandatory to add (or modify) invoice configuration.')

        contract_types = set()
        contract_service_types = set()
        contract_service_states = set()
        config = set()

        service_tax_category, hsn_code = contracts[0].service_tax_category, contracts[0].hsn_code \
            if contracts else None

        for contract in contracts:

            if contract.service_tax_category != service_tax_category and contract.hsn_code != hsn_code:
                raise ValidationError({
                    'contracts': 'All contracts should have same service tax category and hsn code'
                })

            if status == INVOICE_CONFIG_ACTIVE:
                selected_contract.append(contract)
            contract_types.add(contract.contract_type)

            if contract.status == CONTRACT_STATUS_ACTIVE:
                contract_service_types.add(contract.service_tax_category)

            if len(contract_service_types) > 1:
                config.add(self.instance.name)
            contract_service_states.add(contract.service_state)

        if CONTRACT_TYPE_SHIPMENT in contract_types and len(contract_types) > 1:
            raise ValidationError(
                'Shipment contracts cannot be clubbed with other type contracts in same config.')

        if len(contract_service_types) > 1:
            raise ValidationError(
                "Contracts under different service tax category (GTA/BSS)"
                "cannot be clubbed together in same config. check %s" % config)

        # if len(contract_service_states) > 1:
        #     raise ValidationError(
        #         "Contracts from different states cannot be clubbed together in same config.")

        if any(selected_contract.count(x) > 1 for x in selected_contract):
            raise ValidationError(
                'Each contract should be in one configuration only.')


class RatingConfigForm(forms.ModelForm):

    def clean(self):
        super(RatingConfigForm, self).clean()
        rating = self.cleaned_data.get('rating')

        if rating < 0 or rating > 5:
            raise ValidationError({
                'rating': _("Rating value should be between 1 and 5")
            })

    class Meta:
        model = RatingConfig
        fields = '__all__'


class DriverRatingAggregateForm(forms.ModelForm):
    # def clean(self):
    #     super(DriverRatingAggregateForm, self).clean()
    #     remarks = self.cleaned_data.get('remarks')
    #     rating = self.cleaned_data.get('rating')

    #     if remarks:
    #         current_remarks = remarks.values_list('remark', flat=True)
    #         if rating.rating == 5:
    #             invalid_remarks = DriverRatingAggregate.objects.exclude(
    #                 rating__rating=5).values_list('remarks__remark', flat=True)
    #         else:
    #             invalid_remarks = DriverRatingAggregate.objects.filter(
    #                 rating__rating=5).values_list('remarks__remark', flat=True)
    #         remark_invalid = set(invalid_remarks).intersection(current_remarks)
    #         if remark_invalid:
    #             raise ValidationError({
    #                 'remarks': _("Remarks added in Rating 5 should not belong to any other rating: %s" % remark_invalid)
    #             })

    class Meta:
        model = DriverRatingAggregate
        fields = '__all__'


class CustomerEditForm(forms.ModelForm):
    email = forms.EmailField()
    phone_number = forms.CharField(label='Mobile Number',
                                   validators=[validate_phone_number],
                                   required=True, max_length=10)
    password = ReadOnlyPasswordHashField(label=_("password"), help_text=_(
        "Raw passwords are not stored, so there is no way to see "
        "this user's password"
    ))
    last_otp_value = forms.CharField(label='last OTP value')
    last_otp_sent_at = forms.CharField(label='last OTP sent at')

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            customer = kwargs['instance']
            user = customer.user if customer else None
            if user:
                phone_number_obj = user.phone_number
                phone_number = phone_number_obj.__dict__.get(
                    'national_number') if phone_number_obj else ''
                initial = {'email': user.email,
                           'phone_number': str(phone_number) if phone_number else phone_number,
                           'password': user.password, 'last_otp_value': user.last_otp_value,
                           'last_otp_sent_at': user.last_otp_sent_at}
                kwargs['initial'] = initial
        super(CustomerEditForm, self).__init__(*args, **kwargs)

        if user:
            # No reverse url for password change. Need to construct manually
            link = reverse('admin:users_user_change', args=[user.id])
            url_components = link.split('/')[:-2]
            url_components.append('password')
            final_url = '/'.join(url_components)
            help_text = """Raw passwords are not stored, so there is no way to see "
            this user's password, but you can change the password
            using""" + u'<a href="%s" target="_blank"> this form</a>' % (
                final_url)
            self.fields['password'].help_text = help_text

    def clean_password(self):
        return self.initial["password"]

    def clean(self):
        super(CustomerEditForm, self).clean()
        email = self.cleaned_data.get('email')
        service_tax_category = self.cleaned_data.get('service_tax_category')
        customer_category = self.cleaned_data.get('customer_category')
        if customer_category == CUSTOMER_CATEGORY_NON_ENTERPRISE and not service_tax_category:
            raise ValidationError({
                'service_tax_category': 'Mandatory for Non-Enterprise Customer'
            })
        # look for user, except current user
        user = User.objects.filter(
            email=email).exclude(id=self.instance.user.id)
        if user:
            raise ValidationError({
                'email': _("Email already registered")
            })

    class Meta:
        model = Customer
        exclude = ['user']


class HubFormset(BaseInlineFormSet):

    def clean(self):
        super(HubFormset, self).clean()
        for form in self.forms:
            data = form.cleaned_data
            address = data.get('address')
            if not address:
                raise ValidationError(
                    _('Address is mandatory')
                )


class TaxInformationInlineForm(forms.ModelForm):
    gstin = forms.CharField(max_length=15)

    # def __init__(self, *args, **kwargs):
    #     super(TaxInformationInlineForm, self).__init__(*args, **kwargs)
    #     instance = self.instance
    #     self.fields['invoice_address'].autocomplete = False
    #     if instance:
    #         qs = CustomerAddress.objects.filter(id=instance.invoice_address_id)
    #         self.fields['invoice_address'].queryset = qs

    def clean(self):
        super(TaxInformationInlineForm, self).clean()
        gstin_ = self.cleaned_data.get('gstin')
        state = self.cleaned_data.get('state')
        invoice_address = self.cleaned_data.get('invoice_address')
        legalname = self.cleaned_data.get('legalname')

        if gstin_:
            if not state:
                raise ValidationError({
                    'state': 'Please Select State'
                })
            if not invoice_address:
                raise ValidationError({
                    'invoice_address': 'Please Select Invoice Address'
                })
            if not legalname:
                raise ValidationError({
                    'legalname': "Please enter Legal Name"
                })

        if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
            if gstin_:
                # get the initial two characters of gst number i.e state code
                is_valid_state_gst_code = State.objects.filter(
                    pk=state.id, gst_code=gstin_[0:2]).exists()
                if not is_valid_state_gst_code:
                    raise ValidationError(
                        "Invalid State GST Code"
                    )

            if not re.search(r'^\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}$',
                             str(gstin_)):
                raise ValidationError({'gstin': format_html(
                    '<span>Invalid GSTIN Number:<br>'
                    'GSTIN should be in uppercase '
                    '(2 digit state code, PAN Number, 1 digit, Z,'
                    ' Last digit can be one alphabet or one number)</span>')})

        if not self.instance.is_gstin_validated:
            valid_gstin = validate_customer_gst_info(gstin_)
            if not valid_gstin:
                raise ValidationError({
                    'gstin': "GSTIN not registerd on Government GST Portal"
                })
            CustomerTaxInformation.objects.filter(id=self.instance.pk).update(is_gstin_validated=True)


class BusinessCustomerForm(forms.ModelForm):
    name = forms.CharField(label='Brand', max_length=100)
    email = forms.EmailField()
    phone_number = forms.CharField(label='Mobile Number',
                                   validators=[validate_phone_number],
                                   required=True, max_length=10)

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            customer = kwargs['instance']
            user = customer.user if customer else None
            if user:
                initial = {'name': user.name, 'email': user.email,
                           'phone_number': user.phone_number}
                kwargs['initial'] = initial
        super(BusinessCustomerForm, self).__init__(*args, **kwargs)
        choices = (
            (CUSTOMER_CATEGORY_NON_ENTERPRISE,
             CUSTOMER_CATEGORY_NON_ENTERPRISE),
            (CUSTOMER_CATEGORY_TRANSITION,
             CUSTOMER_CATEGORY_TRANSITION),
            (CUSTOMER_CATEGORY_ENTERPRISE, CUSTOMER_CATEGORY_ENTERPRISE))
        self.fields['customer_category'] = forms.ChoiceField(
            choices=choices, label="Customer Category")

    def clean(self):
        super(BusinessCustomerForm, self).clean()
        email = self.cleaned_data.get('email')
        user = User.objects.filter(email=email)
        awb_code = self.cleaned_data.get('awb_code')
        phone_number = self.cleaned_data.get('phone_number')
        category = self.instance.customer_category
        shipment_service = self.cleaned_data.get('is_shipment_servicetype')
        expected_delivery_hours = self.cleaned_data.get(
            'expected_delivery_hours')
        is_tax_registered = self.cleaned_data.get('is_tax_registered')
        brand_name = self.cleaned_data.get('name')
        industry_segment = self.cleaned_data.get('industry_segment')
        customer_category = self.cleaned_data.get('customer_category')

        if not re.match(r'^[A-Za-z0-9 ]+$', brand_name):
            raise ValidationError({
                'name': ("Only AplhaNumeric Allowed")
            })

        if self.instance.awb_code and self.instance.awb_code != awb_code:
            raise ValidationError({
                'awb_code': 'The awb unique identifier cannot be changed once assigned'
            })

        if awb_code:
            _hub = Customer.objects.filter(awb_code=awb_code
                                        ).exclude(id=self.instance.pk).first()

            if _hub:
                raise ValidationError({
                    'awb_code': 'The awb unique identifier already exists'
                })

            coderex = re.match('^[A-Z]{3}$',awb_code)
            if not coderex:
                raise ValidationError({
                    'awb_code': 'The awb unique identifier should be 3 letters caps only'
                })


        existing_customer = Customer.objects.filter(name=brand_name).exclude(
            id=self.instance.id).exclude(
            customer_category=CUSTOMER_CATEGORY_INDIVIDUAL).exists()

        if existing_customer:
            raise ValidationError({'name': 'Brand name Already Exists'})
        if user:
            raise ValidationError({
                'email': _("Email already registered")
            })

        if shipment_service and category != CUSTOMER_CATEGORY_INDIVIDUAL:
            if not expected_delivery_hours:
                raise ValidationError(
                    "Please provide Maximum Hours for Order Delivery")

        if customer_category in [CUSTOMER_CATEGORY_NON_ENTERPRISE,
                                 CUSTOMER_CATEGORY_TRANSITION,
                                 CUSTOMER_CATEGORY_ENTERPRISE] \
            and industry_segment is None:
            raise ValidationError({
                'industry_segment': _("Mandatory field")
            })

    def clean_cin(self):
        return self.cleaned_data['cin'] or None

    def clean_pan(self):
        return self.cleaned_data['pan'] or None

    def clean_tan(self):
        return self.cleaned_data['tan'] or None

    class Meta:
        model = Customer
        exclude = ['user']


class BusinessCustomerEditForm(forms.ModelForm):
    name = forms.CharField(label='Brand', max_length=100)
    email = forms.EmailField()
    phone_number = forms.CharField(label='Mobile Number',
                                   validators=[validate_phone_number],
                                   required=True, max_length=10)
    last_otp_value = forms.CharField(label='last OTP value')
    last_otp_sent_at = forms.CharField(label='last OTP sent at')

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            customer = kwargs['instance']
            user = customer.user if customer else None

            if user:
                phone_number_obj = user.phone_number

                phone_number = None
                if phone_number_obj:
                    phone_number = phone_number_obj.__dict__.get(
                        'national_number')
                initial = {'name': user.name, 'email': user.email,
                           'phone_number': str(phone_number).zfill(10) if phone_number else phone_number,
                           'password': user.password, 'last_otp_value': user.last_otp_value,
                           'last_otp_sent_at': user.last_otp_sent_at}
                kwargs['initial'] = initial
        super(BusinessCustomerEditForm, self).__init__(*args, **kwargs)

        if user:
            # No reverse url for password change. Need to construct manually
            link = reverse('admin:users_user_change', args=[user.id])
            url_components = link.split('/')[:-2]
            url_components.append('password')
            final_url = '/'.join(url_components)
            help_text = """Raw passwords are not stored, so there is no way to see "
            this user's password, but you can change the password
            using""" + u'<a href="%s" target="_blank"> this form</a>' % (
                final_url)

        choices = (
            (CUSTOMER_CATEGORY_NON_ENTERPRISE,
             CUSTOMER_CATEGORY_NON_ENTERPRISE),
            (CUSTOMER_CATEGORY_TRANSITION,
             CUSTOMER_CATEGORY_TRANSITION),
            (CUSTOMER_CATEGORY_ENTERPRISE, CUSTOMER_CATEGORY_ENTERPRISE))
        self.fields['customer_category'] = forms.ChoiceField(
            choices=choices, label="Customer Category")

        # self.fields['head_office_address'].autocomplete = False
        # self.fields['head_office_address'].queryset = \
        #     CustomerAddress.objects.filter(pk=customer.head_office_address_id)

    def clean(self):
        super(BusinessCustomerEditForm, self).clean()
        email = self.cleaned_data.get('email')
        awb_code = self.cleaned_data.get('awb_code')
        category = self.instance.customer_category
        shipment_service = self.cleaned_data.get('is_shipment_servicetype')
        expected_delivery_hours = self.cleaned_data.get(
            'expected_delivery_hours')
        is_tax_registered = self.cleaned_data.get('is_tax_registered')
        brand_name = self.cleaned_data.get('name')
        industry_segment = self.cleaned_data.get('industry_segment')

        if not re.match(r'^[A-Za-z0-9 ]+$', brand_name):
            raise ValidationError({
                'name': _("Only AplhaNumeric Allowed")
            })

        if self.instance.awb_code and self.instance.awb_code != awb_code:
            raise ValidationError({
                'awb_code': 'The awb unique identifier cannot be changed once assigned'
            })

        if awb_code:
            _hub = Customer.objects.filter(awb_code=awb_code
                                        ).exclude(id=self.instance.pk).first()

            if _hub:
                raise ValidationError({
                    'awb_code': 'The awb unique identifier already exists'
                })

            coderex = re.match('^[A-Z]{3}$',awb_code)
            if not coderex:
                raise ValidationError({
                    'awb_code': 'The awb unique identifier should be 3 letters caps only'
                })

        existing_customer = Customer.objects.filter(name=brand_name).exclude(
            id=self.instance.id).exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL).exists()

        if existing_customer:
            raise ValidationError({'name': 'Brand name Already Exists'})
        # look for user, except current user
        user = User.objects.filter(
            email=email).exclude(id=self.instance.user.id)

        service_tax_category = self.cleaned_data.get('service_tax_category')
        customer_category = self.cleaned_data.get('customer_category')
        if customer_category == CUSTOMER_CATEGORY_NON_ENTERPRISE and not service_tax_category:
            raise ValidationError({
                'service_tax_category': 'Mandatory for Non-Enterprise Customer'
            })

        if user:
            raise ValidationError({
                'email': _("Email already registered")
            })
        if shipment_service and category != CUSTOMER_CATEGORY_INDIVIDUAL:
            if not expected_delivery_hours:
                raise ValidationError(
                    "Please provide Maximum Hours for Order Delivery")

        head_office_address = self.cleaned_data.get('head_office_address')
        if not head_office_address:
            raise ValidationError(
                "Head office address is mandatory for customer.")

        if customer_category in [CUSTOMER_CATEGORY_NON_ENTERPRISE,
                                 CUSTOMER_CATEGORY_TRANSITION,
                                 CUSTOMER_CATEGORY_ENTERPRISE] \
            and industry_segment is None:
            raise ValidationError({
                'industry_segment': _("Mandatory field")
            })

    def clean_cin(self):
        return self.cleaned_data['cin'] or None

    def clean_pan(self):
        return self.cleaned_data['pan'] or None

    def clean_tan(self):
        return self.cleaned_data['tan'] or None

    class Meta:
        model = Customer
        exclude = ['user']


class ShipmentAlertFormset(BaseInlineFormSet):

    def clean(self):
        super(ShipmentAlertFormset, self).clean()
        list_of_value_of_mins = []

        for form in self.forms:
            data = form.cleaned_data

            if not data.get('DELETE'):
                cur_value = data.get('time_since_creation')

                if cur_value == 0:
                    raise ValidationError(
                        _('Zero is not a valid input')
                    )
                if cur_value is not None:
                    if not str(cur_value).isdigit():
                        raise ValidationError(
                            _('Only Numbers are allowed')
                        )

                list_of_value_of_mins.append(cur_value)

        if len(list_of_value_of_mins) != len(set(list_of_value_of_mins)):
            raise ValidationError(
                _('Duplicate value for MINS SINCE CREATION field not allowed')
            )


class CustomerAddressForm(forms.ModelForm):
    line1 = forms.CharField(label='Full Address')
    postcode = forms.CharField(label='Post/Zip-code')


class CustomerInvoiceConfigFormSet(BaseInlineFormSet):

    def clean(self):
        super(CustomerInvoiceConfigFormSet, self).clean()
        selected_contract = []

        for form in self.forms:
            contracts = form.cleaned_data.get('contracts')
            status = form.cleaned_data.get('status')
            invoice_state = form.cleaned_data.get('invoice_state')
            customer = form.cleaned_data.get('customer')

            if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
                if not CustomerTaxInformation.objects.filter(customer=customer,
                                                             state=invoice_state).exists():
                    raise ValidationError(
                        'There is no GSTIN number defined for '
                        'the target state in the customer tax info')
            if not contracts:
                raise ValidationError(
                    'Every configuration should have atleast one contract.')
            if not status:
                raise ValidationError(
                    'please select a status for invoice config')

            if not contracts[0].customer.head_office_address:
                raise ValidationError(
                    'Head office address is mandatory to add (or modify) invoice configuration.')

            contract_types = set()
            contract_service_types = set()
            contract_service_states = set()
            config = set()

            for contract in contracts:
                if status == INVOICE_CONFIG_ACTIVE:
                    selected_contract.append(contract)
                contract_types.add(contract.contract_type)
                if contract.status == CONTRACT_STATUS_ACTIVE:
                    contract_service_types.add(contract.service_tax_category)

                if len(contract_service_types) > 1:
                    config.add(form.instance.name)
                contract_service_states.add(contract.service_state)

            if CONTRACT_TYPE_SHIPMENT in contract_types and len(contract_types) > 1:
                raise ValidationError(
                    'Shipment contracts cannot be clubbed with other type contracts in same config.')
            if len(contract_service_types) > 1:
                raise ValidationError(
                    "Contracts under different service tax category (GTA/BSS)"
                    "cannot be clubbed together in same config. check %s" % config)
            # if len(contract_service_states) > 1:
            #     raise ValidationError(
            #         "Contracts from different states cannot be clubbed together in same config.")

        if any(selected_contract.count(x) > 1 for x in selected_contract):
            raise ValidationError(
                'Each contract should be in one configuration only.')


# class CustomerInvoiceConfigForm(forms.ModelForm):
#     def __init__(self, *args, parent_object, **kwargs):
#         super(CustomerInvoiceConfigForm, self).__init__(*args, **kwargs)
#         if self.fields.get('contracts', None):
#             self.fields['contracts'].autocomplete = False
#             qs = Contract.objects.filter(customer=parent_object, status='Active')
#
#             self.fields['contracts'].queryset = qs


class InvoiceRequestForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(InvoiceRequestForm, self).__init__(*args, **kwargs)
        for field_name in self.fields:
            field = self.fields[field_name]
            if isinstance(field, formfields.DateField):
                field.widget = DateTimePicker(
                    options={"format": "MM-YYYY", "viewMode": "months"})


class ContractInvoiceFragmentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        fragment = kwargs.get('instance', None)
        super(ContractInvoiceFragmentForm, self).__init__(*args, **kwargs)

        # if fragment and len(fragment.entities.all()) and self.fields.get('days_counted', None):
        #     if fragment.entities.all()[0].sell_rate.base_pay_interval != BASE_PAYMENT_CYCLE_MONTH:
                # self.fields['days_counted'].widget = ReadOnlyInput()


class CustomerReceivableFormSet(BaseInlineFormSet):

    def clean(self):
        super(CustomerReceivableFormSet, self).clean()
        amount = 0
        for form in self.forms:
            amount += form.cleaned_data.get('amount', 0)
            order = form.cleaned_data.get('order')
            invoice = form.cleaned_data.get('invoice')
            if not order and not invoice:
                raise ValidationError('Invoice or Order is Mandatory')
            if order and invoice:
                raise ValidationError('Choose either invoice or order')

        if self.instance.amount < amount:
            raise ValidationError(
                'sum of invoices/orders cannot exceed the received amount')


class ReceivablelineForm(forms.ModelForm):
    def clean(self):
        super(ReceivablelineForm, self).clean()
        description = self.cleaned_data.get('description')
        if self.instance.status == CREATED and not description:
            raise ValidationError(
                {'description': 'Reference Number is Mandatory'})


class CustomerInvoiceForm(forms.ModelForm):
    supporting_doc = forms.FileField(required=False)
    reason = forms.CharField(required=False)
    sales_reason = forms.CharField(required=False)
    disputed_reason = forms.CharField(required=False)
    email = forms.CharField(required=False)
    # booked_on = forms.ChoiceField(required=False)
    representative = forms.CharField(required=False)
    supervisor = forms.CharField(required=False)
    spoc = forms.CharField(required=False)
    executive = forms.CharField(required=False)
    duedate = forms.DateField(required=False)
    is_reassigning = forms.BooleanField(initial=False, required=False)
    to_email = forms.CharField(required=False)
    cc_email = forms.CharField(required=False)
    manualupload_reasons = forms.CharField(required=False)

    id = forms.HiddenInput()

    def clean(self):
        super(CustomerInvoiceForm, self).clean()
        customer = self.cleaned_data.get('customer')
        service_tax_category = self.cleaned_data.get('service_tax_category')
        invoice = self.cleaned_data.get('related_invoice')
        invoice_type = self.cleaned_data.get('invoice_type')
        amount = self.cleaned_data.get('charges_basic')
        credit_debit_note_reason = self.cleaned_data.get('credit_debit_note_reason')

        inv_obj = CustomerInvoice.objects.filter(id=self.instance.pk)

        # invoice_date = self.cleaned_data.get('invoice_date')
        # office = Office.objects.filter(is_head=True).first()
        # company = office.company

        # if self.instance.status in INVOICE_EDITABLE_STATUS and company.revenue_locked_date >= invoice_date:
        #     raise ValidationError(
        #         {'invoice_date': 'Revenue is locked till %s.' % (company.revenue_locked_date)})

        period_end = self.cleaned_data.get(
            'service_period_end') or self.instance.service_period_end

        if self.cleaned_data.get('invoice_type') == TAX_INVOICE \
            and not current_request().user.is_superuser:
            raise ValidationError(
                {'invoice_type': 'Only SuperUser can add Tax Invoice'})

        if customer and not service_tax_category:
            raise ValidationError(
                {'service_tax_category': 'Service category is mandatory for Customer'})

        if not inv_obj and invoice_type:
            if invoice_type == TAX_INVOICE and not customer:
                raise ValidationError('Customer is mandatory for tax invoice')
            if invoice_type in [CREDIT_NOTE, DEBIT_NOTE] and not invoice:
                raise ValidationError('Linked invoice is mandatory for credit or debit note')
            if invoice_type == CREDIT_NOTE and amount and amount > 0:
                raise ValidationError(
                    {'charges_basic': 'Credit Note should have Negative amount'})
            if invoice_type in [CREDIT_NOTE, DEBIT_NOTE] and not credit_debit_note_reason:
                raise ValidationError({
                    'credit_debit_note_reason': 'Reason mandatory to raise Credit/Debit note'
                })

        # if invoice_date and period_end and period_end > invoice_date:
        #     raise ValidationError(
        #         {'invoice_date': 'Invoice date should be greater than or Equal '
        #                          'to Service Period End'})

        # if self.has_changed():
        #     if 'invoice_date' in self.changed_data:
        #         if invoice_date > date.today():
        #             raise ValidationError(
        #                 {'invoice_date': 'Future invoice date not allowed'})

    def __is_default_invoice_type(self, invoice):
        return not invoice.is_blanket_invoice and not (
            invoice.invoice_type in [CREDIT_NOTE, DEBIT_NOTE] and
            invoice.related_invoice is None)

    def __get_sales_representatives(self, invoice):
        if self.__is_default_invoice_type(invoice):
            return City.objects.filter(
                state__in=[invoice.bh_source_state, invoice.invoice_state]
            ).values_list('sales_representatives__email', flat=True).exclude(
                sales_representatives__email=None).distinct()
        else:
            return City.objects.all().values_list(
                'sales_representatives__email', flat=True).exclude(
                sales_representatives__email=None).distinct()

    def get_executive_users(self):
        return Company.objects.exclude(executives__email__isnull=True
                                       ).values_list('executives__email', flat=True)

    def __init__(self, *args, **kwargs):
        super(CustomerInvoiceForm, self).__init__(*args, **kwargs)
        invoice = kwargs.get('instance', None)

        if self.fields.get('email', None):
            if invoice and invoice.customer:
                self.fields['email'].initial = invoice.customer.user.email
                initial = {'email': invoice.customer.user.email}
                kwargs['initial'] = initial

        if invoice:
            users = []
            spoc_users = []
            supervisor_users = []
            customer_contacts = []
            executive_users = None

            if invoice.status in [CREATED, REJECTED]:
                spoc_users, supervisor_users = CustomerUtils().get_staff_emails(invoice, show_spoc=True)
                if invoice.is_credit_debit_note:
                    executive_users = CustomerUtils().get_company_executives()
            else:
                users = CustomerUtils().get_sales_representatives(invoice)

            customer = invoice.customer
            customer_contacts = None

            reason_list = ManualUploadReason.objects.filter(is_active=True).values('id', 'name')

            self.fields['manualupload_reasons'].initial = json.dumps(list(reason_list))

            if customer:
                cities = []
                contracts_fragment = invoice.contract_invoices.all()
                for fragment in contracts_fragment:
                    if fragment.contract:
                        cities.append(fragment.contract.city)
                if not cities:
                    cities = invoice.invoice_state.cities.all()
                customer_contacts = customer.customercontact_set.filter(
                    Q(city__isnull=True) | Q(city__in=cities)
                ).values('email', 'city__name')

            if users:
                self.fields['representative'].initial = ','.join(users)
            if executive_users:
                self.fields['executive'].initial = ','.join(executive_users)

            if supervisor_users:
                self.fields['supervisor'].initial = ','.join(
                    supervisor_users)  # '','.join(supervisor_users)
            if spoc_users:
                self.fields['spoc'].initial = ','.join(spoc_users)
            if customer_contacts:
                customer_contacts = [
                    '%s | %s' % (c.get('email'), c.get('city__name')) for c in
                    customer_contacts]
                self.fields['to_email'].initial = ','.join(customer_contacts)

        # TODO: Remove the below commented code once the revenue month batch
        #  job data is verified

        # if self.fields.get('booked_on', None):
        #     service_period_end = datetime.today()
        #     cal = []
        #     initial_val = ''
        #     for i in range(0, 2):
        #         end = service_period_end - relativedelta(months=i)
        #         month_year = (calendar.month_abbr[end.month] + '-' + str(end.year),
        #                       calendar.month_abbr[end.month] + '-' + str(end.year))
        #         if invoice and invoice.revenue_booked_on and end.year == invoice.revenue_booked_on.year and \
        #                 end.month == invoice.revenue_booked_on.month:
        #             initial_val = calendar.month_abbr[end.month] + \
        #                 '-' + str(end.year)
        #             # choice field displays 0th element as default value so inserting
        #             # already choosen month at 0th position
        #             cal.insert(0, month_year)
        #             self.fields['booked_on'].initial = month_year
        #         else:
        #             cal.append(month_year)
        #     self.fields['booked_on'] = forms.ChoiceField(choices=cal,
        #                                                  label='Revenue Booked Month',
        #                                                  required=False)
        #     if invoice and invoice.status in StatusPipeline.INVOICE_END_STATUSES and invoice.revenue_booked_on:
        #         initial_val = calendar.month_abbr[invoice.revenue_booked_on.month] + '-' + str(
        #             invoice.revenue_booked_on.year)
        #         self.fields['booked_on'] = forms.CharField(
        #             label='Revenue Booked Month')
        #         self.fields['booked_on'].initial = initial_val
        #         self.fields['booked_on'].widget = ReadOnlyInput()


class CustomerInvoiceAdjustmentFormSet(BaseInlineFormSet):
    def clean(self):
        super(CustomerInvoiceAdjustmentFormSet, self).clean()

        hsn_code = None
        invoice = None

        adj_sum = {}

        for form in self.forms:
            invoice = form.cleaned_data.get('invoice')
            comments = form.cleaned_data.get('comments')
            total_amount = form.cleaned_data.get('total_amount')
            category = form.cleaned_data.get('category')
            if category and not category.hsn_code:
                raise ValidationError('Adjustment not having HSN Code')

            if not comments:
                raise ValidationError('Comments is Mandatory')

            # if invoice.invoice_type == CREDIT_NOTE:
            #     is_delete = form.cleaned_data.get('DELETE', False)
            #     if not is_delete:
            #         raise ValidationError('Adjustment not allowed for Credit Note')
            #     continue
            if not invoice.is_credit_debit_note:
                if not hsn_code:
                    fragment = invoice.contract_invoices.first()
                    if invoice.hs_code:
                        hsn_code = invoice.hs_code.hsn_code
                    elif fragment.contract.hsn_code:
                        hsn_code = fragment.contract.hsn_code.hsn_code

                if category:
                    if category.hsn_code and not category.hsn_code in adj_sum.keys():
                        adj_sum[category.hsn_code.hsn_code] = 0
                    adj_sum[category.hsn_code.hsn_code] += float(total_amount)

        if hsn_code and invoice:
            if adj_sum.get(hsn_code, None):
                adj_sum[hsn_code] += float(invoice.taxable_amount) - float(invoice.taxable_adjustment)
            else:
                adj_sum[hsn_code] = float(invoice.taxable_amount) - float(invoice.taxable_adjustment)

        val1 = [{k: v} for k, v in adj_sum.items() if v < 0]
        if val1:
            raise ValidationError({'Adjustment Sum of hsn code should be positive but it is %s' % val1})


class CustomerActionForm(ActionForm):
    customer = CustomModelChoiceField(
        label=_('Target Customer'),
        queryset=Customer.objects.exclude(
            customer_category=CUSTOMER_CATEGORY_INDIVIDUAL),
        required=False,
        field_names=['pk', 'name']
    )


class C2CCustomerActionForm(ActionForm):
    customer = CustomModelChoiceField(
        label=_('Target Customer'),
        queryset=Customer.objects.filter(
            customer_category__in=[CUSTOMER_CATEGORY_INDIVIDUAL, CUSTOMER_CATEGORY_NON_ENTERPRISE]),
        required=False,
        field_names=['pk', 'name']
    )


class InvoicePaymentFormset(BaseInlineFormSet):

    def clean(self):
        super(InvoicePaymentFormset, self).clean()
        amount_paid = 0.0
        collection_data = {}
        unallocated_collection = {}
        for form in self.forms:
            data = form.cleaned_data
            actual_amount = data.get('actual_amount', 0)
            if data.get('DELETE'):
                continue

            if actual_amount and actual_amount < 0:
                raise ValidationError(_('Amount cannot be negative'))

            if 'collection' in form.changed_data and data.get('id'):
                old_instance = InvoicePayment.objects.get(pk=data.get('id').id)
                if old_instance.collection:
                    raise ValidationError(_("changing collection is not allowed"))

            if not data.get("id") and actual_amount > form.instance.collection.unallocated_amount:
                raise ValidationError("Cannot allocate more then available amount")

            if 'actual_amount' in form.changed_data:
                if data.get('id'):
                    old_instance = InvoicePayment.objects.get(pk=data.get('id').id)
                collection = data.get('collection', '')
                if collection_data.get(collection.id, ''):
                    collection_data[collection.id] += actual_amount or 0 if not data.get('id') \
                        else actual_amount - old_instance.actual_amount
                elif collection:
                    collection_data[collection.id] = actual_amount or 0 if not data.get('id') \
                        else actual_amount - old_instance.actual_amount
                    unallocated_collection[collection.id] = collection.unallocated_amount

            if not data:
                raise ValidationError(_("Invalid request"))

            if not data.get('invoice'):
                raise ValidationError(_("No invoice was linked"))

            if not data.get('DELETE'):
                if not data.get('id'):
                    if not (data.get("actual_amount") or data.get("tds")):
                        raise ValidationError('Either Amount Received or '
                                              'Amount deducted as TDS is needed',
                                              code='Forbidden')
                # else:
                #     if form.has_changed() and form.initial and form.instance.collection.unallocated_amount >=0:
                #         raise ValidationError(
                #             'You cannot change the already entered record',
                #             code='Forbidden')

                tds = data.get('tds', 0)
                if tds and tds < 0:
                    raise ValidationError(_('TDS amount cannot be negative'))

                amount_paid += data.get('actual_amount', 0.0)

        invoice = self.instance
        invoice_total_amount = float(invoice.total_amount)

        for key, val in collection_data.items():
            if unallocated_collection.get(key) < val and val > 0:
                raise ValidationError("Cannot allocate more then available amount")

        if invoice and invoice.invoice_type != CREDIT_NOTE and invoice_total_amount - amount_paid < 0:
            raise ValidationError(
                _("Paid amount is exceeding the invoice balance"))

    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)
        kwargs['parent_object'] = self.instance

        return kwargs


class InvoicePaymentForm(forms.ModelForm):

    def __init__(self, *args, parent_object, **kwargs):
        super(InvoicePaymentForm, self).__init__(*args, **kwargs)
        invoice_payment = kwargs.get('instance', None)
        if not invoice_payment and self.fields.get('collection', None):
            qs = CustomerReceivable.objects.filter(
                customer=parent_object.customer, unallocated_amount__gte=1)
            self.fields['collection'].queryset = qs


class SmsConfigForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(SmsConfigForm, self).__init__(*args, **kwargs)
        self.fields['customer'].autocomplete = False
        self.fields['contract'].autocomplete = False

    def clean(self):
        super(SmsConfigForm, self).clean()
        event = self.cleaned_data.get('event')
        stop_status = self.cleaned_data.get('stop_status')
        template_id = self.cleaned_data.get('template_id')

        if not template_id:
            raise ValidationError({'template_id': _("DLT Template ID is mandatory")})

        if event == StatusPipeline.OUT_FOR_DELIVERY and stop_status is None:
            raise ValidationError({'stop_status': _("Select stop status")})

        if event != StatusPipeline.OUT_FOR_DELIVERY and stop_status:
            raise ValidationError({'stop_status': _(
                "Stop status should be defined only for Out-For Delivery order event")})

class InvoiceAdjustmentCategoryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(InvoiceAdjustmentCategoryForm, self).__init__(*args, **kwargs)
        hsn_code = kwargs.get('instance', None)

        if not hsn_code:
            qs = ServiceHSNCodeMap.objects.filter(is_active=True)
            self.fields['hsn_code'].queryset = qs

class InvoiceAdjustmentCategoryEditForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(InvoiceAdjustmentCategoryEditForm, self).__init__(*args, **kwargs)
        hsn_code = kwargs.get('instance', None)
        qs = ServiceHSNCodeMap.objects.filter(is_active=True)
        self.fields['hsn_code'].queryset = qs
