from django.conf import settings
from django.utils.translation import ugettext_lazy as _


MAX_ALLOWED_WALLET_RECHARGE_AMOUNT = getattr(
    settings, 'MAX_ALLOWED_WALLET_RECHARGE_AMOUNT', 10000)
EXPECTED_DELIVERY_HOURS_TIMEDELTA = 24

INSTAKART_DATA_INGESTION_SOURCE = getattr(
    settings, 'INSTAKART_DATA_INGESTION_SOURCE', 'testing')
INSTAKART_DATA_INGESTION_EVENT = getattr(
    settings, 'INSTAKART_DATA_INGESTION_EVENT', 'vehicle_event')

RESPONSE_MESSAGES = {
    "FAIL": {
        "REQUIRED": "OTP is required",
        "MOBILE_NOT_FOUND": "Mobile number not found",
        "NO_OTP_FOUND": "No OTP associated with this mobile",
        "OTP_MISMATCH": "Entered OTP is not valid",
    },
    "PASS": "Your mobile has been verified successfully"
}

VERIFY_MOBILE = 'verify-mobile'
PROFILE_UPDATE_SUCCESS = 'Your profile details has been saved successfully'
PROFILE_UPDATE_FAILED = 'Failed to update the profile'


# Invoice header - User Types
CUSTOMER = u'CUSTOMER'
CONSIGNEE = u'CONSIGNEE'

# Invoice Adjustment - Types
LABOUR = u'LABOUR COST'
TOLL_CHARGES = u'TOLL CHARGES'
OTHERS = u'OTHERS'

INVOICE_ADJUSTMENT = (
    (LABOUR, _("LABOUR COST")),
    (TOLL_CHARGES, _("TOLL CHARGES")),
    (OTHERS, _("OTHERS")),
)

REJECTED = "REJECTED"
SETTLED = "SETTLED"
# Invoice - Statuses
CREATED = "CREATED"
APPROVED = "APPROVED"
UNAPPROVED = "UNAPPROVED"
READY_TO_BE_SENT = "READY TO SEND"
SENT = "SENT"
CANCELLED = "CANCELLED"
CUSTOMER_APPROVED = "CUSTOMER APPROVED"
CUSTOMER_CONFIRMED = "CUSTOMER CONFIRMED"
CUSTOMER_DISPUTED = "CUSTOMER DISPUTED"
PENDING_CUSTOMER_CONFIRMATION = "PENDING CUSTOMER CONFIRMATION"
EXPIRED = "EXPIRED"
SENT_FOR_APPROVAL = "SENT FOR APPROVAL"
REASSIGNED = "REASSIGNED"
REGENERATED = "REGENERATED"
PENDING_CUSTOMER_ACKNOWLEDGEMENT = "PENDING CUSTOMER ACKNOWLEDGEMENT"
CREATE_CREDIT_DEBIT_NOTE = "CREATE CREDIT DEBIT NOTE"
SENT_TO_CUSTOMER = "SENT TO CUSTOMER"
UPLOADED_TO_GST_PORTAL = "UPLOADED TO GST"
CUSTOMER_ACKNOWLEDGED = "CUSTOMER ACKNOWLEDGED"
PARTIALLY_PAID = "PARTIALLY PAID"
PAYMENT_DONE = "PAYMENT DONE"
CLOSED_WITH_CN = "CLOSED WITH CREDIT NOTE"

INVOICE_DEACTIVATION_PERIOD = 45

INVOICE_STATUSES_FOR_SUMMARY = [
    CREATED,
    REASSIGNED,
    SENT_FOR_APPROVAL,
    APPROVED,
    UPLOADED_TO_GST_PORTAL,
    SENT_TO_CUSTOMER,
    CUSTOMER_ACKNOWLEDGED,
    CUSTOMER_CONFIRMED,
    PARTIALLY_PAID,
    CUSTOMER_DISPUTED,
    REJECTED,
    PAYMENT_DONE
]

INVOICE_STATUSES_FOR_GSTIN_WARNING = [CREATED, REJECTED, SENT_FOR_APPROVAL]

MZASIGO_ALLOWED_STATUS_FOR_INVOICE_GENERATION = [
    CUSTOMER_APPROVED,
    APPROVED,
    SENT_TO_CUSTOMER,
    CUSTOMER_ACKNOWLEDGED,
    CUSTOMER_CONFIRMED,
    PARTIALLY_PAID,
    PAYMENT_DONE,
    PENDING_CUSTOMER_ACKNOWLEDGEMENT
]

# Invoice Inline Groups (as displayed in Invoice).
SHIPMENT = u'SHIPMENT'
VEHICLE = u'VEHICLE'

# Customer Bank account details action
ACCOUNT_ADDED = 'Account Added'
ACCOUNT_DELETED = 'Account Deleted'
ADDED = 'Added'
DELETED = 'Deleted'
ACTIVATED = 'Activated'
DEACTIVATED = 'Deactivated'

# Upload to GST Portal Automation. Finance team email ids
UPLOADED_TO_GST_PORTAL_AUTOMATION_MAIL_LIST = ['ops.bom@blowhorn.com', 'ops.blr@blowhorn.com', 'ops.maa@blowhorn.com',
                                               'ops.ncr@blowhorn.com', 'ops.hyd@blowhorn.com', 'all.sales@blowhorn.com',
                                               'ops.ccu@blowhorn.com']
UPLOADED_TO_GST_PORTAL_AUTOMATION_CC_LIST = ['santosh.desai@blowhorn.com',
                                             'aditya@blowhorn.com', 'ramprakash@blowhorn.com', 'abhay.r@blowhorn.com',
                                             'anurag@blowhorn.com', 'dollyshyam@blowhorn.com',
                                             'harikiran@blowhorn.com', 'sreekumar.tv@blowhorn.com',
                                             'kamlesh@blowhorn.com', 'vaishak@blowhorn.com', 'vivek@blowhorn.com',
                                             'siddharth.menon@blowhorn.com', 'nikhil.suresh@blowhorn.com',
                                             'adarsh@blowhorn.com', 'subhramanian@blowhorn.com',
                                             'santoshnair@blowhorn.com', 'citymanagers@blowhorn.com',
                                             'sarath@blowhorn.com', 'asha.garga@blowhorn.com', 'karthik.mn@blowhorn.com',
                                             'rohit.agrawal@blowhorn.com', 'harisha.g@blowhorn.com']

SEND_TO_CUSTOMER_AUTOMATION_MAIL_LIST = ['all.sales@blowhorn.com', 'citymanagers@blowhorn.com',
                                         'nikhil.suresh@blowhorn.com', 'subhramanian@blowhorn.com']
SEND_TO_CUSTOMER_AUTOMATION_CC_LIST = ['asrar@blowhorn.com', 'arif@blowhorn.com', 'ramprakash@blowhorn.com',
                                       'ashutosh@blowhorn.com', 'dollyshyam@blowhorn.com']

# Minimum invoice distance to be shown in Invoice / order tracking.
MINIMUM_INVOICE_DISTANCE = 0.2

"""
['number', 'trip_number', 'city_name', 'hub_name', 'status', 'num_of_attempts',
'driver', 'date_placed', 'expected_delivery', 'expected_delivery', 'delivered_time',
'reference_number', 'customer_reference_number', 'customer_name', 'customer_phone',
'customer_address', 'attempted_timestamps', 'remarks']
"""
MYFLEET_SHIPMENTS_DATA_TO_BE_EXPORTED = [
    {'name': 'Order', 'label': 'number', 'type': 'string'},
    {'name': 'Trip', 'label': 'trip_number', 'type': 'string'},
    {'name': 'City', 'label': 'city_name', 'type': 'string'},
    {'name': 'Hub', 'label': 'hub_name', 'type': 'string'},
    {'name': 'Status', 'label': 'status', 'type': 'string'},
    {'name': '# Attempts', 'label': 'num_of_attempts', 'type': 'string'},
    {'name': 'Driver', 'label': 'driver', 'type': 'string'},
    {'name': 'Date Placed', 'label': 'date_placed', 'type': 'string'},
    {'name': 'Expected Delivery', 'label': 'expected_delivery', 'type': 'string'},
    {'name': 'Delivered Time', 'label': 'delivered_time', 'type': 'string'},
    {'name': 'Reference Number', 'label': 'reference_number', 'type': 'string'},
    {'name': 'Customer Reference Number',
        'label': 'customer_reference_number', 'type': 'string'},
    {'name': 'Customer', 'label': 'customer_name', 'type': 'string'},
    {'name': 'Customer Phone', 'label': 'customer_phone', 'type': 'string'},
    {'name': 'Customer Address', 'label': 'customer_address', 'type': 'string'},
    {'name': 'Attempt Timestamps', 'label': 'attempted_timestamps', 'type': 'string'},
    {'name': 'Remarks', 'label': 'remarks', 'type': 'string'},
]

EVENT_COLUMNS_FOR_EXPORT = [
    {'name': 'Order New', 'label': 'order_new', 'type': 'string'},
    {'name': 'Driver Assigned', 'label': 'driver_assigned', 'type': 'string'},
    {'name': 'Out For Delivery', 'label': 'out_for_delivery', 'type': 'string'},
    {'name': 'Order Picked', 'label': 'order_picked', 'type': 'string'},
    {'name': 'Order Delivered', 'label': 'order_delivered', 'type': 'string'},
    {'name': 'Unable To Deliver', 'label': 'unable_to_deliver', 'type': 'string'},
    {'name': 'Order Returned To Hub', 'label': 'order_returned_to_hub',
     'type': 'string'},
    {'name': 'Order Returned', 'label': 'order_returned', 'type': 'string'},
    {'name': 'Order Lost', 'label': 'order_lost', 'type': 'string'},
    {'name': 'Delivery Ack', 'label': 'delivery_ack', 'type': 'string'},
    {'name': 'Returned Ack', 'label': 'returned_ack', 'type': 'string'},
    {'name': 'Unable To Deliver Ack', 'label': 'unable_to_deliver_ack',
     'type': 'string'},
]

COLUMN_ITEM_NAME = 'Item Name'
COLUMN_ORDER_NUMBER = 'Order Number'
COLUMN_ITEM_CODE = 'Item Code'
COLUMN_ITEM_SEQ_NO = 'Item Sequence No'
COLUMN_UOM = 'UOM(like eaches,boxes,pallets)'
COLUMN_STATUS = 'Status'
COLUMN_QTY = 'Ordered Quantity'
COLUMN_CGST = 'CGST'
COLUMN_IGST = 'IGST'
COLUMN_SGST = 'SGST'
COLUMN_HSNCODE = 'HSN Code(XXXXXXXXXX)'
COLUMN_TOTAL_AMT_INC_TAX = 'Total Amount Incl. Tax'
COLUMN_COST_PER_ITEM = 'Cost Per Item'
COLUMN_NET_AMT = 'Total Item Price'
COLUMN_WEIGHT = 'Weight'
COLUMN_VOLUME = 'Volume(in cc)'
COLUMN_MRP = 'Max Retail Price'
COLUMN_DISCOUNT_TYPE = 'Discount Type'
COLUMN_DISCOUNTS = 'Discounts'

SHIPMENT_ADMIN_IMPORT = 'Spreadsheet-Admin'
SHIPMENT_CUSTOMER_IMPORT = 'Spreadsheet-Customer'
COMPANY_ID = 'Company ID'

ORDERLINE_COLUMNS_FOR_EXPORT = [
    {'name': COLUMN_ORDER_NUMBER, 'label': 'number', 'type': 'string'},
    {'name': COLUMN_ITEM_SEQ_NO, 'label': 'item_sequence_no', 'type': 'string'},
    {'name': COLUMN_ITEM_NAME, 'label': 'sku', 'type': 'string'},
    {'name': COLUMN_ITEM_CODE, 'label': 'item_code', 'type': 'string'},
    {'name': COLUMN_HSNCODE, 'label': 'hsn_code', 'type': 'string'},
    {'name': COLUMN_STATUS, 'label': 'status', 'type': 'string'},
    {'name': COLUMN_UOM, 'label': 'uom', 'type': 'string'},
    {'name': COLUMN_QTY, 'label': 'quantity', 'type': 'string'},
    {'name': COLUMN_COST_PER_ITEM, 'label': 'each_item_price', 'type': 'string'},
    {'name': COLUMN_NET_AMT, 'label': 'total_item_price', 'type': 'string'},
    {'name': COLUMN_CGST, 'label': 'cgst', 'type': 'string'},
    {'name': COLUMN_SGST, 'label': 'sgst', 'type': 'string'},
    {'name': COLUMN_IGST, 'label': 'igst', 'type': 'string'},
    {'name': COLUMN_TOTAL_AMT_INC_TAX, 'label': 'total_amount_incl_tax', 'type': 'string'},
    {'name': COLUMN_WEIGHT, 'label': 'weight', 'type': 'string'},
    {'name': COLUMN_VOLUME, 'label': 'volume', 'type': 'string'},
    {'name': COLUMN_MRP, 'label': 'mrp', 'type': 'string'},
    {'name': COLUMN_DISCOUNT_TYPE, 'label': 'discount_type', 'type': 'string'},
    {'name': COLUMN_DISCOUNTS, 'label': 'discounts', 'type': 'string'},
]

ORDERLINE_COLUMNS_FOR_IMPORT = ORDERLINE_COLUMNS_FOR_EXPORT
ORDERLINE_LABEL_NAME_MAPPING = {
    COLUMN_ORDER_NUMBER: 'number',
    COLUMN_ITEM_NAME: 'sku',
    COLUMN_ITEM_CODE: 'item_code',
    COLUMN_ITEM_SEQ_NO: 'item_sequence_no',
    COLUMN_UOM: 'uom',
    COLUMN_QTY: 'quantity',
    COLUMN_COST_PER_ITEM: 'each_item_price',
    COLUMN_CGST: 'cgst',
    COLUMN_IGST: 'igst',
    COLUMN_SGST: 'sgst',
    COLUMN_HSNCODE: 'hsn_code',
    COLUMN_NET_AMT: 'total_item_price',
    COLUMN_TOTAL_AMT_INC_TAX: 'total_amount_incl_tax',
    COLUMN_STATUS: 'status',
    COLUMN_WEIGHT: 'weight',
    COLUMN_VOLUME: 'volume',
    COLUMN_MRP: 'mrp',
    COLUMN_DISCOUNT_TYPE: 'discount_type',
    COLUMN_DISCOUNTS: 'discounts'
}

ORDERLINE_MANDATORY_HEADERS = [COLUMN_ORDER_NUMBER, COLUMN_ITEM_NAME, COLUMN_QTY]
ORDERLINE_MANDATORY_VALUES = ['sku', 'number', 'quantity']
ORDERLINE_IMPORT_BATCH_DESCRIPTION = 'DASHBOARD_ORDERLINE_IMPORT_%s'

SHIPMENT_DATA_TO_BE_IMPORTED = [
    {'name': COMPANY_ID, 'label': 'customer_id', 'type': 'string'},
    {'name': 'Date Placed', 'label': 'date_placed', 'type': 'string'},
    {'name': 'AWB Number', 'label': 'awb_number', 'type': 'string'},
    {'name': 'Reference Number', 'label': 'reference_number', 'type': 'string'},
    {'name': 'Customer Reference Number', 'label': 'customer_reference_number', 'type': 'string'},
    {'name': 'Is HyperLocal Order (Y/N)', 'label': 'is_hyperlocal', 'type': 'string'},
    {'name': 'Is Return Order (Y/N)', 'label': 'is_return_order', 'type': 'string'},
    {'name': 'Expected Delivery DateTime YYYY-MM-DD HH24:MI', 'label': 'expected_delivery_time', 'type': 'string'},
    {'name': 'Pickup DateTime YYYY-MM-DD HH24:MI', 'label': 'pickup_time', 'type': 'string'},
    {'name': 'Pickup Customer Name', 'label': 'pickup_name', 'type': 'string'},
    {'name': 'Pickup Customer Phone', 'label': 'pickup_phone', 'type': 'string'},
    {'name': 'Pickup Address', 'label': 'pickup_address', 'type': 'string'},
    {'name': 'Pickup Pincode', 'label': 'pickup_pincode', 'type': 'string'},
    {'name': 'Pickup Latitude', 'label': 'pickup_latitude', 'type': 'string'},
    {'name': 'Pickup Longitude', 'label': 'pickup_longitude', 'type': 'string'},
    {'name': 'Weight (in kg)', 'label': 'weight', 'type': 'string'},
    {'name': 'Volume (in cc)', 'label': 'volume', 'type': 'string'},
    {'name': 'Priority', 'label': 'priority', 'type': 'string'},
    {'name': 'Cash on Delivery', 'label': 'cash_on_delivery', 'type': 'string'},
    {'name': 'Shipping Charges', 'label': 'shipping_charges', 'type': 'string'},
    {'name': 'Delivery Latitude', 'label': 'delivery_latitude', 'type': 'string'},
    {'name': 'Delivery Longitude', 'label': 'delivery_longitude', 'type': 'string'},
    {'name': 'Delivery Customer Name', 'label': 'delivery_name', 'type': 'string'},
    {'name': 'Delivery Customer Phone', 'label': 'delivery_phone', 'type': 'string'},
    {'name': 'Alternate Delivery Customer Phone', 'label': 'alternate_delivery_phone', 'type': 'string'},
    {'name': 'Delivery Customer Email', 'label': 'delivery_email', 'type': 'string'},
    {'name': 'Delivery Address', 'label': 'delivery_address', 'type': 'string'},
    {'name': 'Delivery Pincode', 'label': 'delivery_pincode', 'type': 'string'},
    {'name': 'Is Commercial Address (Y/N)', 'label': 'address_type', 'type': 'string'},
    {'name': 'Delivery Instructions', 'label': 'delivery_instructions', 'type': 'string'},
    {'name': 'Pickup Hub', 'label': 'pickup_hub_name', 'type': 'string'},
    {'name': 'Hub', 'label': 'hub_name', 'type': 'string'},
    {'name': 'Division', 'label': 'division', 'type': 'string'},
    {'name': 'Carrier Mode', 'label': 'carrier_mode', 'type': 'string'},
    ]

SHIPMENT_NOT_MANDATORY_LABEL = [
    'Weight (in kg)', 'Volume (in cc)', 'Shipping Charges',
    'Delivery Instructions', 'AWB Number',
    'Expected Delivery DateTime YYYY-MM-DD HH24:MI', 'Pickup DateTime YYYY-MM-DD HH24:MI',
    'Priority', 'Is Commercial Address (Y/N)',
    'Hub', 'Division', 'Customer Email',
    'Is Return Order (Y/N)', 'Pickup Hub',
    'Pickup Customer Name', 'Pickup Customer Phone',
    'Pickup Address', 'Pickup Pincode', 'Pickup Latitude', 'Pickup Longitude',
    'Alternate Delivery Customer Phone', 'Carrier Mode',
    'Is HyperLocal Order (Y/N)'
]


MYFLEET_TRIPS_DATA_TO_BE_EXPORTED = [
    {'name': 'Order', 'label': 'order_no', 'type': 'string'},
    {'name': 'Trip', 'label': 'trip_number', 'type': 'string'},
    {'name': 'Driver', 'label': 'driver',
        'type': 'object', 'fields': ['name']},
    {'name': 'Vehicle', 'label': 'vehicle',
        'type': 'object', 'fields': ['name', 'type']},
    {'name': 'Scheduled Time', 'label': 'time_scheduled', 'type': 'datetime'},
    {'name': 'Start Time', 'label': 'time_started_actual', 'type': 'datetime'},
    {'name': 'End Time', 'label': 'time_ended_actual', 'type': 'datetime'},
    {'name': 'Total Time', 'label': 'total_time', 'type': 'time'},
    {'name': 'Total Distance', 'label': 'total_distance', 'type': 'distance'},
]

INVOICE_TRIPS_DATA_TO_BE_EXPORTED = [
    {'name': 'Trip Number', 'label': 'trip_number', 'type': 'string'},
    {'name': 'Contract', 'label': 'customer_contract', 'type': 'string'},
    {'name': 'Actual Driver', 'label': 'driver', 'type': 'string'},
    {'name': 'Invoice Driver', 'label': 'invoice_driver', 'type': 'string'},
    {'name': 'Vehicle Number', 'label': 'invoice_driver_vehicle', 'type': 'string'},
    {'name': 'Vehicle Class', 'label': 'invoice_driver_vehicle_class', 'type': 'string'},
    {'name': 'Hub', 'label': 'hub', 'type': 'string'},
    {'name': 'Division', 'label': 'division', 'type': 'string'},
    {'name': 'Scheduled At', 'label': 'planned_start_time', 'type': 'datetime'},
    {'name': 'Actual Start Time', 'label': 'actual_start_time', 'type': 'datetime'},
    {'name': 'Actual End Time', 'label': 'actual_end_time', 'type': 'datetime'},
    {'name': 'Total Time(HH:MM)', 'label': 'total_time', 'type': 'time_hh_mm'},
    {'name': 'Total Time in Mins', 'label': 'time_mins', 'type': 'string'},
    {'name': 'Total Distance(kms)',
     'label': 'total_distance', 'type': 'string'},
    {'name': 'Mongo Distance(kms)',
     'label': 'gps_distance', 'type': 'string'},
    {'name': 'RDS Distance(kms)',
     'label': '_gps_distance', 'type': 'string'},
    {'name': 'Assigned', 'label': 'assigned', 'type': 'string'},
    {'name': 'Delivered', 'label': 'delivered', 'type': 'string'},
    {'name': 'Attempted', 'label': 'attempted', 'type': 'string'},
    {'name': 'Rejected', 'label': 'rejected', 'type': 'string'},
    {'name': 'Pickups', 'label': 'pickups', 'type': 'string'},
    {'name': 'Delivered_cash', 'label': 'delivered_cash', 'type': 'string'},
    {'name': 'Delivered_mpos', 'label': 'delivered_mpos', 'type': 'string'},
    {'name': 'Cumulative Kms', 'label': 'cumulative_kms', 'type': 'string'},
    {'name': 'Cumulative Minutes', 'label': 'cumulative_minutes', 'type': 'string'},

]

INVOICE_ORDER_DATA_TO_BE_EXPORTED = [
    {'name': 'Number', 'label': 'order_number', 'type': 'string'},
    {'name': 'Reference Number', 'label': 'order_reference_number', 'type': 'string'},
    {'name': 'Customer Reference Number',
        'label': 'cust_reference_number', 'type': 'string'},
    {'name': 'Updated Time ', 'label': 'order_updated_time', 'type': 'datetime'},
    {'name': 'Driver', 'label': 'driver_name', 'type': 'string'},
    {'name': 'Invoice Driver', 'label': 'invoice_driver_name', 'type': 'string'},
    {'name': 'Hub', 'label': 'hub_name', 'type': 'string'},
    {'name': 'Status', 'label': 'order_status', 'type': 'string'},
    {'name': 'Sku Type', 'label': 'sku_type', 'type': 'string'},
    {'name': 'Sku Total Count', 'label': 'total_count', 'type': 'string'},
    {'name': 'Sku Delivered Count', 'label': 'delivered_count', 'type': 'string'},
    {'name': 'Delivered', 'label': 'delivered', 'type': 'string'},
    {'name': 'unable_to_deliver', 'label': 'unable_to_deliver', 'type': 'string'},
    {'name': 'returned', 'label': 'returned', 'type': 'string'},
    {'name': 'returned_to_origin', 'label': 'returned_to_origin', 'type': 'string'},
    {'name': 'pincode', 'label': 'pincode', 'type': 'string'},
]

MYTRIPS_TRIPS_DATA_TO_BE_EXPORTED = [
    {'name': 'Booking #', 'label': 'order_id', 'type': 'string'},
    {'name': 'Vehicle Model', 'label': 'vehicle_model', 'type': 'string'},
    {'name': 'City', 'label': 'city', 'type': 'string'},
    {'name': 'Booked For', 'label': 'date', 'type': 'date'},
    {'name': 'Pickup Address', 'label': 'pickup',
        'type': 'object', 'fields': ['address']},
    {'name': 'Dropoff Address', 'label': 'dropoff',
        'type': 'object', 'fields': ['address']},
    {'name': 'Distance', 'label': 'total_distance', 'type': 'distance'},
    {'name': 'Total Cost', 'label': 'price', 'type': 'string'},
    {'name': 'Payment Mode', 'label': 'payment_mode', 'type': 'string'},
    {'name': 'Driver Name', 'label': 'driver_name', 'type': 'string'},
    {'name': 'Vehicle Number', 'label': 'vehicle_number', 'type': 'string'},
]

TRIP_STOPS_DATA_TO_BE_IMPORTED = [
    {'name': 'Customer Phone', 'label': 'customer_phone', 'type': 'string'},
    {'name': 'Customer Address', 'label': 'customer_address', 'type': 'string'},
    {'name': 'Pin Code', 'label': 'pincode', 'type': 'string'},
    {'name': 'Latitude', 'label': 'latitude', 'type': 'string'},
    {'name': 'Longitude', 'label': 'longitude', 'type': 'string'},
    {'name': 'Stop Type (Pickup/Drop)', 'label': 'stop_type', 'type': 'string'}
]

STOPS_MANDATORY_FIELDS = ['Latitude', 'Longitude', 'Pin Code', 'Customer Address']

EXPORT_TYPE_MINIMAL = 'minimal'
EXPORT_TYPE_WITH_EVENTS = 'with_events'
EXPORT_TYPE_ORDERLINE = 'orderline'
EXPORT_TYPE_POD = 'pod_docs'
EXPORT_TYPE_MIS_SUMMARY = 'mis_summary'
EXPORT_TYPE_MIS = 'mis_report'
EXPORT_TYPE_NDR = 'ndr_report'

REPORT_CATEGORIES = (
    ('Shipment', _('Shipment')),
    ('Fixed Order', _('Fixed Order')),
    ('Trip', _('Trip')),
    ('Inventory', _('Inventory'))

)

AGGREGATION_CATEGORIES = (
    ('Daily', _('Daily')),
    ('Weekly', _('Weekly')),
    ('Monthly', _('Monthly')),
    ('Adhoc', _('Adhoc'))
)

AGGREGATION_LEVEL = {
    "Daily" : "Daily",
    "Weekly" : "WTD",
    "Monthly" : "MTD",
    "Adhoc" : "Adhoc"

}

ALLOWED_TIME_FORMATS = [
    '%d-%b-%Y %H:%M',  # 05-Oct-2017 15:00
    '%d %b %Y %H:%M',  # 05 Oct 2017 15:00
    '%b %d, %Y %H:%M',  # Oct 13, 2017 9:00
    '%A, %d %B %Y %I%p'  # Friday, 10 October 2017 5PM
]

PICKUP_DATETIME_FORMAT = '%d %b %Y %H:%M'

NUM_DAYS_FUTURE = 14

CUSTOMER_APP_TIME_FORMAT = '%H:%M'


PENDING = -1
SUCCESS = 1
FAILURE = 0

INVOICE_REQUEST_STATUSES = (
    (PENDING, _('Pending')),
    (SUCCESS, _('Success')),
    (FAILURE, _('Failure')),
)

ACCOUNTING_INVOICE_STATUS = [CUSTOMER_ACKNOWLEDGED, CUSTOMER_CONFIRMED, PARTIALLY_PAID,PENDING_CUSTOMER_ACKNOWLEDGEMENT,
                             PAYMENT_DONE, CUSTOMER_APPROVED,SENT_TO_CUSTOMER, UPLOADED_TO_GST_PORTAL]

ACCOUNTING_STATUS_BILLED = "Billed Revenue"
ACCOUNTING_STATUS_UNBILLED = "Unbilled Revenue"


INVOICE_CONFIG_ACTIVE = u'Active'
INVOICE_CONFIG_INACTIVE = u'Inactive'

INVOICE_CONFIG_STATUSES = (
    (INVOICE_CONFIG_ACTIVE, _('Active')),
    (INVOICE_CONFIG_INACTIVE, _('Inactive')),
)

PAYMENT_MODE_CASH = u'Cash'
PAYMENT_MODE_BANK_TRANSFER = u'Bank Transfer'
PAYMENT_MODE_CHEQUE = u'Cheque'

PAYMENT_MODE_OPTIONS = (
    (PAYMENT_MODE_CASH, _('Cash')),
    (PAYMENT_MODE_CHEQUE, _('Cheque')),
    (PAYMENT_MODE_BANK_TRANSFER, _('Bank Transfer')),
)

CREDIT_NOTE = u'Credit Note'
DEBIT_NOTE = u'Debit Note'
TAX_INVOICE = u'Tax Invoice'

INVOICE_TYPE = (
    (CREDIT_NOTE, _('Credit Note')),
    (DEBIT_NOTE, _('Debit Note')),
    (TAX_INVOICE, _('Tax Invoice')),
)

# Invoice various pages

NONE = 0
DETAIL_PAGE = 1
SUMMARY_PAGE = 2
SUMMARY_DETAIL_PAGE = 3

INVOICE_EXTRA_PAGES = (
    (DETAIL_PAGE, _('Detail')),
    (NONE, _('None')),
    (SUMMARY_PAGE, _('Summary')),
    (SUMMARY_DETAIL_PAGE, _('Summary + Detail'))
)


# HTTP Request types.

HTTP_GET = 1
HTTP_POST = 2

HTTP_REQUEST_TYPES = (
    (HTTP_GET, _('GET')),
    (HTTP_POST, _('POST'))
)

# Authentication types.

AUTH_TYPE_NONE = -1
AUTH_TYPE_BASIC = 1
AUTH_TYPE_TOKEN = 2

AUTHENTICATION_TYPES = (
    (AUTH_TYPE_NONE, _('NONE')),
    (AUTH_TYPE_BASIC, _('BASIC')),
    (AUTH_TYPE_TOKEN, _('TOKEN')),
)

# Order status reason mapping
ORDER_STATUSES_REASON_MAPPING = (
    ('Delivered','ORDER_DELIVERED'),
    ('Unable-To-Deliver','ORDER_CANCELLED'),
)

# ORGANIZATION RELATED
ORGANIZATION_MESSAGES = {
    'invitation': {
        'success': 'Invitation has been sent to {email}',
        'failed': 'Failed to sent invitation to {email}'
    },
    'member': {
        'created': '{email} has been added successfully',
        'failed': 'Failed to add {email} as member'
    },
    'error': {
        'integrity': '{email} already present as individual customer.',
        'field': {
            'name': 'Name should not contain special characters & numbers.',
            'email': {
                'required': 'Email is mandatory',
                'invalid': 'Invalid email address',
                'staff_email': '%s staff email is not allowed' % settings.BRAND_NAME
            },
            'phone_number': {
                'required': 'Mobile number is required',
                'invalid': 'Invalid mobile number',
                'duplicate': 'Phone number already registered'
            },
            'common': 'Unable to process your request. Try again later.'
        }
    },
    'permission': {
        'admin_status': 'You can\'t change your own admin status'
    },
}

# MEMBER STATUSES
MEMBER_INVITED = _('Invited')
MEMBER_ACTIVE = _('Active')
MEMBER_INACTIVE = _('Inactive')
MEMBER_STATUSES = (
    ('invited', MEMBER_INVITED),
    ('active', MEMBER_ACTIVE),
    ('inactive', MEMBER_INACTIVE)
)

# GPS Ingestion constants
EVENT_INGESTION_CUSTOMER_ID_DATA_MAP = {
    3: {
        "tracking_id": "vehicle__reg_no",
        "time": "created_time",
        "latitude": "location_details__mLatitude",
        "longitude": "location_details__mLongitude",
        "altitude": "location_details__mAltitude",
        "source": INSTAKART_DATA_INGESTION_SOURCE,
        "type": INSTAKART_DATA_INGESTION_EVENT,
        "attributes": {}
    }
}

EVENT_INGESTION_DEFAULT_CUSTOMER_ID = 3

EVENT_INGESTION_DEFAULT_TIME_FORMAT = "%Y-%m-%d %H:%M:%S+0530"

CUSTOMER_CREDIT_LIMIT_PERCENTAGE_RATIO = 0.8

NUMBER_OF_DAYS_ELAPSED_FOR_MARKING_INVOICE_AS_EXPIRED = 60

INVOICE_SUMMARY_LEGEND = [
    {'color': '#8a8007', 'info': 'Blanket Invoice'},
    {'color': 'yellow', 'info': 'Check Due Date'},
    {'color': 'orange', 'info': 'Deadline Nearing'},
    {'color': 'red', 'info': 'Deadline Not Met'},
]

"""Enterprise credit period expiration email content"""

CREDIT_PERIOD_START_DATE = '2019-4-1'

# Sending to customer
CREDIT_PERIOD_MAIL_SUBJECT = """{cust} | Credit period has expired"""
CREDIT_PERIOD_MAIL_BODY = """
        Hello {customer}, {newline_delimiter}

        We understand that you might be caught up with your month end priorities, but our job is to remind you of our healthy relationship.  {newline_delimiter}

        Your credit period for {period} has expired. It would be great if you could make the payment so it doesn’t hamper our rapport. {newline_delimiter}

        Regards, {newline_delimiter}
        Team Blowhorn
"""
"""Enterprise credit amount expiration email content"""
CREDIT_AMOUNT_MAIL_SUBJECT = """{cust} | Credit period has expired"""
CREDIT_AMOUNT_MAIL_BODY = """
        Hello {customer}, {newline_delimiter}

        We understand that you might be caught up with your month end priorities, but our job is to remind you of our healthy relationship.  {newline_delimiter}

        Your credit amount of {amount} has expired. It would be great if you could make the payment so it doesn’t hamper our rapport. {newline_delimiter}

        Regards, {newline_delimiter}
        Team Blowhorn
"""

""" Invoice email content
"""
# Sending to customer
CUSTOMER_MAIL_SUBJECT = """{customer} | {invoice_state} | Invoice for the period {period_start} to {period_end}"""

"""
Constant To act as a threshold for marking the revenue month as current or the
previous month
"""
REVENUE_THRESHOLD_DATE = 20

INVOICE_EDITABLE_STATUS = [CREATED, UNAPPROVED, REJECTED, CUSTOMER_DISPUTED, EXPIRED, SENT_FOR_APPROVAL]

MIN_INVOICE_REMAINING_AMOUNT = 0
MAX_INVOICE_REMAINING_AMOUNT = 10

DEFAULT_DUEDATE_DELTA_DAYS = 2

# MYFLEET CUSTOMER
INVOICE_PENDING = 'pending'
INVOICE_ACCEPT = 'accept'
INVOICE_DISPUTE = 'dispute'
ENTERPRISE_INVOICE_STATUS_MAP = {
    'pending': SENT_TO_CUSTOMER,
    'accepted': CUSTOMER_CONFIRMED,
    'disputed': CUSTOMER_DISPUTED,
    'partially_paid': PARTIALLY_PAID,
    'paid': PAYMENT_DONE
}
ALLOWED_INVOICE_SEARCH_FIELDS = ('invoice_number__icontains',)
INVOICE_FILTER_DATE_FIELD = 'service_period_end'

DOC_INVOICE = 'invoice'
DOC_MIS = 'mis'
DOWNLOAD_DOCUMENT_TYPES = (DOC_INVOICE, DOC_MIS,)

REASONS_FOR_DISPUTING_INVOICE = (
    (0, 'Incorrect GST Information'),
    (1, 'Incorrect Invoice break up'),
    (2, 'Incorrect Address Information'),
    (3, 'Incorrect Invoice format'),
    (4, 'MIS / Annexure not provided'),
    (5, 'Inaccurate MIS / Annexure data'),
    (-1, 'Other'),
)

# Terminology for customer
INVOICE_TYPE_REGULAR = _('Regular')
CUSTOMER_TAX_TYPE_TERMS = {
    TAX_INVOICE: INVOICE_TYPE_REGULAR,
    CREDIT_NOTE: DEBIT_NOTE
}

CUSTOMER_CONFIRMED_MAIL_SUBJECT = """Invoice {invoice} has been accepted by {customer}"""
CUSTOMER_CONFIRMED_MAIL_BODY = """
    Hi, {newline_delimiter}
    {accepted_by} ({customer}) accepted the invoice with number <a href="{link}">{invoice}</a>. {newline_delimiter}{newline_delimiter}

    Regards, {newline_delimiter}
    {brand_name} Team
"""

PAYMENT_URL_SEPARATOR = 'PB'
PAYMENT_URL_BULK_WEB = '/payment/%s' + PAYMENT_URL_SEPARATOR + '%s/web'


TERMS_AND_CONDITION = ['Customer to handover [1] invoice copy of goods along with goods post billing to Blowhorn.',
                       'One order translates to delivery at one address. Multiple addresses will be as multiple orders and billing accordingly.',
                       'Deliveries only within Nehru Outer Ring Road.',
                       'The prices are for one mini truckload at the maximum. Any spillover will be treated like an additional booking.',
                       'Maximum of two pairs of helping hands will be provided for the activity.',
                       'The prices are for movements to ground floor and apartments with lifts.',
                       'In the absence of elevators or more than 3 floors, a charge of Rs.50 per floor will be extra.',
                       'If the customer is not in the premises or waiting exceeds 30 mins, Charges @150/hour will be levied additionally.',
                       'Cut off time for orders is 1800 hours on the same day.',
                       'All Orders will be prepaid.',
                       'Customers are expected to share Proper Address for delivery, contact number and recipient name.',
                       'If there are any exceptions to the above, decision will be made on mutual consultation.',
                       'No passengers allowed inside the Goods Transport Vehicles.',
                       'Please ensure the items are packed to your satisfaction after billing. Kindly paste the stickers provided on all packed items if they contain leather sofas, glass surfaces/panes, marble tops or are fragile in nature. The responsibility to paste these stickers shall lie with the purchaser of these goods.',
                       'In the rare event of damage to goods being transported, the Maximum Liability of Transporter is limited invoice value or Rs.3000 whichever is lower.',
                       '** - 50% Discount for second hour if items are less and not so bulky and there is space left behind in vehicle']


KIOSK_TERMS_OF_SERVICE_TEMPLATE = """
    <table style="background:#f4f4f4;">
        <thead style="border:1px solid #ccc;background:#ccc;">
            <tr>
                <td colspan="2" style="padding:8px;text-align:center;">Terms and Conditions for the Transport</td>
            </tr>
        </thead>
        <tbody>
            {body_content}
        </tbody>
    </table>
"""

KIOSK_TERMS_OF_SERVICE_ROW_TEMPLATE = """
<tr>
    <td style="padding:4px;" style="border:1px solid #ccc;">{current_index}.</td>
    <td style="padding:4px;" style="border:1px solid #ccc;">{row_content}</td>
<tr>
"""

TEMPLATE_SHIPMENT_ORDER_REPORT = {
    "subject": "Daily Report | Shipment Orders | {date}",
    "body": """
        Dear Sir/ Madam,
        Greetings from {brand_name}!

        Please find daily report on shipment order in attachment.

        Regards,
        Team {brand_name}
    """
}

# FIRABASE DASHBOARD UPDATE CHANNELS
CD_ORDER_DETAILS = 'order_details'
CD_ORDERLINES = 'orderlines'
DASHBOARD_UPDATE_URLS = {
    CD_ORDER_DETAILS: 'dashboard/{customer_id}/orders/shipment',
    CD_ORDERLINES: 'dashboard/orderlines'
}

# Collection Statuses
COLLECTION_STATUS_CLOSED=u'Closed'
COLLECTION_STATUS_PARTIALLY_CLOSED=u'Partially Closed'
COLLECTION_STATUS_OPEN=u'Open'

TRANSACTION_STATUS_PARTIALLY_PAID = u'Partially Paid'
TRANSACTION_STATUS_UNPAID = u'Unpaid'
TRANSACTION_STATUS_CLOSED_WITH_CN = u'Closed with Credit Note'

STATUS_HOLD = 'Hold'
CUST_LEDGER_STATUS = (
    (STATUS_HOLD, _('Hold')),
)

# SHIPMENT ORDER EXPORT DATA
SHIPMENT_ORDER_NDR_EXPORT = {
    'company_name' : 'Company Name',
    'city_name' : 'City',
    'delivery_hub' : 'Delivery Hub',
    'order_customer_reference_number' : 'Customer Reference Number',
    'order_reference_number' : 'Reference Number',
    'order_number' : 'Order Number',
    'order_status' : 'Status',
    'order_date_placed' : 'Date Placed',
    'no_of_attempts' : 'Attempt Count',
    'first_attempted_at' : 'First attempted on',
    'customer_name' : 'Customer Name',
    'phone_number' : 'Customer Phone',
    'address' : 'Address',
    'postcode' : 'Shipping Postal Code',
    '_remarks' : 'Remarks'
}

SHIPMENT_ORDER_MIS_EXPORT = {
    'company_name' : 'Company Name',
    'city_name' : 'City',
    'delivery_hub' : 'Delivery Hub',
    'order_customer_reference_number' : 'Customer Reference Number',
    'order_reference_number' : 'Reference Number',
    'order_number' : 'Order Number',
    'order_status' : 'Status',
    'order_date_placed' : 'Date Placed',
    'customer_name' : 'Customer Name',
    'phone_number' : 'Customer Phone',
    'address' : 'Address',
    'postcode' : 'Shipping Postal Code'
}

SHIPMENT_ORDER_MIS_SUMMARY_EXPORT = {
    'order_status' : 'Status of Order',
    'count' : 'Count'
}

SHIPMENT_ORDER_MINIMAL_EXPORT = {
    'number': 'Number',
    'hub__name': 'Hub',
    'customer_reference_number': 'Customer Reference Number',
    'reference_number': 'Reference Number',
    'status': 'Status',
    'formatted_date_placed': 'Date Placed',
    'shipping_address__first_name': 'Customer Name',
    'shipping_address__phone_number': 'Customer Phone Number',
    'shipping_address__line1': 'Address',
    'shipping_address__postcode': 'Pincode',
    'state': 'State',
    'city__name': 'City',
    # 'latitude': 'Latitude',
    # 'longitude': 'Longitude',
    'pickup_address__first_name': 'Pickup Name',
    'pickup_address__line1': 'Pick up Address',
    'pickup_address__postcode': 'Pickup Pin Code',
    'pickup_address__phone_number': 'Pickup Phone Number',
    'cash_on_delivery': 'Cash On Delivery',
    'driver__name': 'Driver',
    'driver__driver_vehicle': 'Vehicle Number',
    'sku': 'SKU',
    'total_sku_qty': 'Total SKU Qty',
    'bill_date': 'Bill Date',
    'bill_number': 'Bill Number',
    'expected_time': 'Expected Delivery Time',
    'picked_time': 'Picked Time',
    'last_packed_at': 'Packed At',
    'last_out_on_road_at': 'Out on Road At',
    'last_attempted_at': 'Attempted At',
    '_remarks': 'Remarks',
    'pod_docs': 'POD Files',
}

SHIPMENT_ORDERLINE_EXPORT = {
    'order_number': 'Order Number',
    'item_seq_no': 'Item Sequence No',
    'item_name': 'Item Name',
    'itemcode': 'Item Code',
    'hsn_code': 'HSN Code(XXXXXXXXXX)',
    'order_status': 'Status',
    'itemuom': 'UOM(like eaches,boxes,pallets)',
    'cost_per_item': 'Cost Per Item',
    'ordered_quantity': 'Ordered Quantity',
    'picked_quantity': 'Picked Quantity',
    'quantity_delivered': 'Delivered Quantity',
    'quantity_returned': 'Returned Quantity',
    'net_amount_for_order_delivered': 'Net Amount for Order Delivered',
    'cgst': 'CGST',
    'igst': 'IGST',
    'sgst': 'SGST',
    'total_item_price': 'Total Item Price',
    'total_amount_incl_tax': 'Total Amount Incl. Tax',
    'weight': 'Weight',
    'volume':'Volume(in cc)',
    'mrp': 'MRP',
    'order_discounts': 'discount',
    'order_discount_type': 'discount_type'
}

EVENT_COLUMNS_EXPORT = {
    'number': 'Number',
    'hub__name': 'Hub',
    'customer_reference_number': 'Customer Reference Number',
    'reference_number': 'Reference Number',
    'status': 'Status',
    'formatted_date_placed': 'Date Placed',
    'shipping_address__first_name': 'Customer Name',
    'shipping_address__phone_number': 'Customer Phone Number',
    'shipping_address__line1': 'Address',
    'shipping_address__postcode': 'Pincode',
    'state': 'State',
    'city__name': 'City',
    # 'latitude': 'Latitude',
    # 'longitude': 'Longitude',
    'pickup_address__first_name': 'Pickup Name',
    'pickup_address__line1': 'Pick up Address',
    'pickup_address__postcode': 'Pickup Pin Code',
    'pickup_address__phone_number': 'Pickup Phone Number',
    'cash_on_delivery': 'Cash On Delivery',
    'driver__name': 'Driver',
    'driver__driver_vehicle': 'Vehicle Number',
    'sku': 'SKU',
    'total_sku_qty': 'Total SKU Qty',
    'bill_date': 'Bill Date',
    'bill_number': 'Bill Number',
    'expected_time': 'Expected Delivery Time',
    '_remarks': 'Remarks',
    'pod_docs': 'POD Files',
    'number_of_attempts': 'Number of Attempts',
    'attempted_timestamps': 'Attempted Timestamps',
    'formatted_date_placed': 'Date Placed',
    'driver_assigned': 'Driver Assigned',
    'picked_time': 'Picked Time',
    'last_packed_at': 'Packed At',
    'last_out_on_road_at': 'Out on Road At',
    'last_attempted_at': 'Attempted At',
    'delivered_time': 'Order Delivered',
    'unable_to_deliver': 'Unable To Deliver',
    'order_returned_to_hub': 'Order Returned To Hub',
    'order_returned': 'Order Returned',
    'order_lost': 'Order Lost',
    'delivery_ack': 'Delivery Ack',
    'returned_ack': 'Returned Ack',
    'unable_to_deliver_ack': 'Unable To Deliver Ack',
}


# Shipment Reports Mail Body
EXPORT_TYPE_REPORT_SUBJECT = """ {aggregation_level} {report_name} | Report for the period {period_start} to {period_end}"""

EXPORT_TYPE_REPORT_BODY = """
    Dear Sir/ Madam, {newline_delimiter}
    Greetings from {brand_name}!

    Please find attached report for the service period {period_start} to {period_end}.

    For any query or clarification please get in touch with us. {newline_delimiter}

    Regards,
    {brand_name} Team
"""

# Shipment Reports Mail Body
EXPORT_TYPE_DAY_REPORT_SUBJECT = """ {aggregation_level} {report_name} | Report for {period_start}"""

EXPORT_TYPE_DAY_REPORT_BODY = """
    Dear Sir/ Madam, {newline_delimiter}
    Greetings from {brand_name}!

    Please find attached report for {period_start}.

    For any query or clarification please get in touch with us. {newline_delimiter}

    Regards,
    {brand_name} Team
"""

# shipment export request
EXPORT_TYPE_ADHOC_REPORT_SUBJECT = """ {report_name} Adhoc Request | Report for {period_start} to {period_end}"""

CREDIT_BREACH_5_DAYS_PRIOR_SUBJECT = """{brand_name} Payment Pending - Due in 5 days"""
CREDIT_BREACH_1_DAY_PRIOR_SUBJECT = """{brand_name} Payment Pending - Due in 1 day"""
CREDIT_BREACH_DAY_SUBJECT = """{brand_name} Payment Pending - Due Today"""
CREDIT_BREACH_1_DAY_AFTER_SUBJECT = """{brand_name} Payment Pending - Overdue"""


CREDIT_BREACH_1_DAY_AFTER_BODY = """
    Your credit period has ended for the invoice(s) - <b>{invoice_numbers}</b>. <br>This is a reminder for payment remittance so that services are uninterrupted.
"""

CREDIT_BREACH_DAY_BODY = """
    Your credit period has ended. This is a reminder for payment remittance so that services are uninterrupted.
"""

CREDIT_BREACH_1_DAY_PRIOR_BODY = """
    Your credit period ends in 1 day. This is a reminder for payment remittance so that services are uninterrupted.
"""

CREDIT_BREACH_5_DAYS_PRIOR_BODY = """
    Your credit period ends in 5 days for the invoice(s) - <b>{invoice_numbers}</b>. <br>This is a reminder for payment remittance so that services are uninterrupted.
"""

UPLOAD_TO_GST_PORTAL_BODY = """
    Hi Team, {newline_delimiter}
    __________________________________________________________{newline_delimiter}
    Total invoice uploads = {total_uploads}                   {newline_delimiter}
    Successful invoice uploads = {successful_uploads}         {newline_delimiter}
    Failed invoice uploads = {failed_uploads}                 {newline_delimiter}
    __________________________________________________________{newline_delimiter}
    Please find detailed information in attachment.
"""


EMAIL_TEMPLATE_AGEING_MAPPING = {
    '-5': {
        'body': CREDIT_BREACH_5_DAYS_PRIOR_BODY,
        'subject': CREDIT_BREACH_5_DAYS_PRIOR_SUBJECT
    },
    '1': {
        'body': CREDIT_BREACH_1_DAY_AFTER_BODY,
        'subject': CREDIT_BREACH_1_DAY_AFTER_SUBJECT
    }
}


SUCCESS_STATUS_CODES = ['200', '201']
BUSINESS_CUSTOMER_CONTACT_EDITABLE_GROUPS = ['Sales Associate', 'Sales Manager']

WITH_BLOWHORN = 'With blowhorn'
