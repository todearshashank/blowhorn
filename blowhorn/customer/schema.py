import datetime
import graphene
from django.conf import settings
from django.db import connection
from django.db.models import Prefetch

from graphql.language import ast
from graphene.types import Scalar
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from graphql_relay.node.node import from_global_id
from blowhorn.oscar.core.loading import get_model

from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.models import ResourceAllocation, Contract, Resource
from . import models
from blowhorn.order.models import Order, WayPoint, ShippingAddress
from blowhorn.trip.models import Trip, Stop
from blowhorn.customer.models import Route, RouteStop
from blowhorn.common.notification import Notification
from blowhorn.utils.functions import dist_between_2_points
from blowhorn.contract.mixins import ResourceAllocationMixin

# Country = get_model('address', 'Country')


class ShiftTime(Scalar):

    @staticmethod
    def serialize(dt):
        return ResourceAllocationMixin().get_ist_time(dt)


class RouteStopType(DjangoObjectType):
    class Meta:
        model = models.RouteStop


class RouteType(DjangoObjectType):
    class Meta:
        model = models.Route
        filter_fields = {'city': ['exact'], 'customer__name': ['exact']}
        interfaces = (graphene.Node,)


class ContractType(DjangoObjectType):
    class Meta:
        model = Contract
        filter_fields = {'customer__id': ['iexact']}
        interfaces = (graphene.Node,)


class ResourceType(DjangoObjectType):
    class Meta:
        model = Resource


class ResourceAllocationType(DjangoObjectType):
    shift_start_time = graphene.Field(ShiftTime)
    class Meta:
        model = ResourceAllocation
        filter_fields = {'contract__customer__name': ['iexact']}
        interfaces = (graphene.Node,)


class UpdateOrderWaypoints(graphene.Mutation):
    message = graphene.String()
    status = graphene.Int()

    class Input:
        trip_id = graphene.Int(required=True)
        route_id = graphene.String(required=True)
        order_number = graphene.String(required=True)
        replace = graphene.Boolean(default_value=True)

    def mutate(self, info, trip_id, order_number, route_id, replace):
        message = ''
        status = 200
        if trip_id is not None \
            and order_number is not None \
                and route_id is not None \
                    and replace is not None:
            _route_id = from_global_id(route_id)[1]
            route_stops = RouteStop.objects.filter(
                route_id=_route_id
            ).order_by(
                'seq_no'
            ).values()
            stop_count = len(route_stops)
            if stop_count > 0:
                country = Country.objects.filter(
                    iso_3166_1_a2=settings.COUNTRY_CODE_A2).get()
                trip = Trip.objects.filter(id=trip_id)[0]
                order = Order.objects.filter(number=order_number)[0]
                count = WayPoint.objects.filter(order=order).count()
                count_backup = count
                display_inprogess = False

                if count > 0:
                    if replace:
                        Stop.objects.filter(trip=trip).delete()
                        WayPoint.objects.filter(order=order).delete()
                        count = 1
                        count_backup = count
                        waypoint = {
                            'sequence_id': count + stop_count,
                            'order': order,
                            'shipping_address': order.shipping_address,
                            'order_line': None,
                            'estimated_distance': None,
                            'estimated_time': None
                        }
                        waypoint = WayPoint(**waypoint)
                        waypoint.save()
                        stop = {
                            'status': StatusPipeline.TRIP_NEW,
                            'trip': trip,
                            'waypoint': waypoint,
                            'sequence': count + stop_count,
                            'order': order,
                        }
                        stop = Stop(**stop)
                        stop.save()
                    else:
                        count += 1
                else:
                    display_inprogess = True
                    count = 1
                    count_backup = count
                    waypoint = {
                        'sequence_id': count + stop_count,
                        'order': order,
                        'shipping_address': order.shipping_address,
                        'order_line': None,
                        'estimated_distance': None,
                        'estimated_time': None
                    }
                    waypoint = WayPoint(**waypoint)
                    waypoint.save()
                    stop = {
                        'status': StatusPipeline.TRIP_NEW,
                        'trip': trip,
                        'waypoint': waypoint,
                        'sequence': count + stop_count,
                        'order': order
                    }
                    stop = Stop(**stop)
                    stop.save()

                for index, route_stop in enumerate(route_stops):
                    if index == stop_count - 1:
                        continue

                    address = {
                        'title': '',
                        'identifier': route_stop['address_name'],
                        'first_name': route_stop['contact_name'],
                        'last_name': '',
                        'line1': route_stop['line1'],
                        'line2': route_stop['line2'],
                        'line3': route_stop['line3'],
                        'line4': route_stop['line4'],
                        'state': route_stop['state'],
                        'postcode': route_stop['postcode'],
                        'phone_number': route_stop['contact_mobile'],
                        'notes': 'NA',
                        'country': country,
                        'geopoint': route_stop['geopoint']
                    }
                    address = ShippingAddress(**address)
                    address.save()
                    waypoint = {
                        'sequence_id': count,
                        'order': order,
                        'shipping_address': address,
                        'order_line': None,
                        'estimated_distance': None,
                        'estimated_time': None
                    }
                    waypoint = WayPoint(**waypoint)
                    waypoint.save()
                    stop = {
                        'status': StatusPipeline.TRIP_IN_PROGRESS if display_inprogess and index == 0 else StatusPipeline.TRIP_NEW,
                        'trip': trip,
                        'waypoint': waypoint,
                        'sequence': count,
                        'order': order,
                    }
                    stop = Stop(**stop)
                    stop.save()
                    count += 1

                estimated_distance = 0
                pickup_coordinates = order.pickup_address.geopoint
                waypoints = WayPoint.objects.filter(order=order).order_by(
                    'sequence_id')
                for waypoint in waypoints:
                    stop_pickup_coordinates = waypoint.shipping_address.geopoint
                    # calculate distance between pickup and waypoints
                    eta_between_points = dist_between_2_points(
                        pickup_coordinates, stop_pickup_coordinates)

                    estimated_distance += eta_between_points

                    # waypoint.total_time = eta_between_points.duration_s / \
                    #     60 if eta_between_points.duration_s else 0
                    waypoint.estimated_distance = eta_between_points if eta_between_points else 0
                    waypoint.save(update_fields=['estimated_distance'])
                    pickup_coordinates = stop_pickup_coordinates
                order.estimated_distance_km = estimated_distance
                order.save(update_fields=['estimated_distance_km'])
                if not replace:
                    dict_ = {
                        'username': order.driver_id,
                        'action': 'restore'
                    }
                    Notification.send_action_to_driver(order.driver.user, dict_)
            else:
                message = "Route doesn't have stops configured"
                status = 200
        else:
            message = 'Trip id, order number and route id are mandatory'
            status = 200

        return UpdateOrderWaypoints(
            message=message,
            status=status
        )


class UpdateResourceRoute(graphene.Mutation):
    message = graphene.String()
    status = graphene.Int()

    class Input:
        resource_id = graphene.String(required=True)
        route_id = graphene.String(required=True)

    def mutate(self, info, resource_id, route_id):
        message = ''
        status = 200
        if resource_id is not None:
            route_id = from_global_id(route_id)[1] if len(
                route_id) > 0 else None
            resources = Resource.objects.filter(id=resource_id)
            if resources.count() > 0:
                resource = resources[0]
                route = Route.objects.filter(id=route_id).first()
                resource.route = route
                resource.save(update_fields=['route'])
                message = 'Route is configured'
            else:
                message = 'Resource is not present'
                status = 400
        else:
            message = 'Driver is a mandatory field'
            status = 400
        return UpdateOrderWaypoints(
            message=message,
            status=status
        )


class Query(graphene.AbstractType):
    customer_routes = DjangoFilterConnectionField(RouteType)
    customer_resource_allocation = DjangoFilterConnectionField(
        ResourceAllocationType)

    def resolve_customer_routes(self, info, **args):
        return models.Route.objects.all().prefetch_related(
            Prefetch(
                'stops',
                queryset=models.RouteStop.objects.all()
            )
        )

    def resolve_customer_resource_allocation(self, info, **args):
        result = ResourceAllocation.objects.all().select_related(
            'contract').prefetch_related(
            Prefetch(
                'resource_set',
                queryset=Resource.objects.all()
            )
        )
        return result


class Mutation(graphene.ObjectType):
    update_order_waypoints = UpdateOrderWaypoints.Field()
    update_resource_route = UpdateResourceRoute.Field()
