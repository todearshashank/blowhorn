from django.core.management.base import BaseCommand

from blowhorn.customer.models import Customer
from blowhorn.customer.helpers.invoice_helper import InvoiceHelper


class Command(BaseCommand):
    help = 'Generates invoice for customer - at a contract level.'

    def add_arguments(self, parser):
        parser.add_argument('-debug', '--debug', nargs='?', type=int, help='Debug mode (True / False.)')
        parser.add_argument('-date_start', '--date_start', nargs='?', type=str, help='Start date (YYYY-MM-DD)')
        parser.add_argument('-date_end', '--date_end', nargs='?', type=str, help='End date (YYYY-MM-DD)')
        parser.add_argument('-aggregate_levels', '--aggregate_levels', nargs='?', type=str, help='Aggregate Levels')
        parser.add_argument('-customer', '--customer', nargs='?', type=int, help='Customer ID')
        parser.add_argument('-contracts', '--contracts', nargs='?', type=str, help='Contracts')

    def handle(self, *args, **kwargs):

        debug = bool(kwargs.get('debug', 0))
        customer = kwargs.get('customer', None)
        date_start = kwargs.get('date_start', None)
        date_end = kwargs.get('date_end', None)
        aggregate_levels = kwargs.get('aggregate_levels', '')
        contracts = kwargs.get('contracts', '')
        if aggregate_levels:
            aggregate_levels = aggregate_levels.split(',')
        else:
            aggregate_levels = []

        if contracts:
            contracts = contracts.split(',')
        else:
            contracts = []

        assert customer is not None, "`customer` is required."
        assert date_start is not None, "`date_start` is required."
        assert date_end is not None, "`date_end` is required."

        print("INPUTS", debug, date_start, date_end, customer, aggregate_levels, contracts)

        customer = Customer.objects.get(id=customer)
        InvoiceHelper(debug=debug).generate_invoice(customer, date_start, date_end, aggregate_levels, contracts)

