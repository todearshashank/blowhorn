from django.conf.urls import url

from . import views
from blowhorn.customer.invoice_views.list import InvoiceListView, \
    InvoiceSupportData, CustomerContactView
from blowhorn.customer.invoice_views.action import InvoiceActionView
from blowhorn.customer.invoice_views.download import InvoiceDownload


urlpatterns = [

    # List all rate packages (vehicle pricing).
    url(r'^rate-packages/$', views.RatePackages.as_view(),
        name='rate-packages-list'),
    url(r'^available-vehicles/$', views.AvailableVehicles.as_view(),
        name='available-vehicles-list'),

    # List/ Create Order
    url(r'^orders/$', views.OrderCreate.as_view(), name='create-order'),
    url(r'^slots/$', views.BookingSlots.as_view(), name='booking-slots'),

    # MyFleet Invoice
    url(r'^invoice/enterprise/supportdata$', InvoiceSupportData.as_view(),
        name='myfleet-invoice-supportdata'),
    url(r'^invoice/enterprise/list/$', InvoiceListView.as_view(),
        name='myfleet-invoice-list'),
    url(r'^invoice/enterprise/action/(?P<pk>[0-9]+)$',
        InvoiceActionView.as_view(),
        name='myfleet-invoice-action'),
    url(r'^invoice/enterprise/download/(?P<invoice_number>[\w-]+)/(?P<doc_type>[\w-]+)$',
        InvoiceDownload.as_view(),
        name='myfleet-invoice-download'),
    url(r'^businessuser/contacts/(?P<invoice_pk>[0-9]+)$',
        CustomerContactView.as_view(), name='businesscustomer-contact'),
]
