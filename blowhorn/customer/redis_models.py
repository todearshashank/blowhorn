from blowhorn.common.redis_ds.redis_dict import RedisDict


class RedisBase(object):

    def delete(self):
        """ Delete this object from redis"""

        raise ValueError("Cannot delete this instance from Redis.")


class Customer(RedisBase, RedisDict):
    """ BlowHorn customer specific details.
    Please note that any details found on redis will be real-time.
    """

    def __init__(self, id, **kwargs):

        RedisDict.__init__(
            self,
            id=str(id),
            fields=self.__get_model(),
            defaults=kwargs
        )

    def __get_model(self):
        """ Returns the model for customer. Any new fields if required for
        customer in Redis should be added here in below format in below
        dictionary.
        <name> : <value_type>
        Example : <name> : str"""

        return {
            'gps_event_config': dict,
        }

