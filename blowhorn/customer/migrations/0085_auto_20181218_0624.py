# Generated by Django 2.1.1 on 2018-12-18 06:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0084_auto_20181122_1814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerinvoice',
            name='comments',
            field=models.TextField(blank=True, help_text='Notes for customer. Reflects in PDF', null=True),
        ),
    ]
