# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-02-15 10:27
from __future__ import unicode_literals

from django.db import migrations, models
import oscar.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0024_auto_20180213_1117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='customer_category',
            field=models.CharField(choices=[('Individual', 'Individual'), ('Non Enterprise', 'Non Enterprise'), ('Transition', 'Transition'), ('Enterprise', 'Enterprise')], db_index=True, default='Individual', max_length=25, verbose_name='Customer Category'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='entity_type',
            field=models.CharField(blank=True, choices=[('Private Limited Company', 'Private Limited Company'), ('Public Limited Company', 'Public Limited Company'), ('Body Corporate', 'Body Corporate'), ('Partnership', 'Partnership'), ('Trust/ Charities/ NGOs', 'Trust/ Charities/ NGOs'), ('Financial Institution (FI)', 'Financial Institution (FI)'), ('Foreign Institutional Investor (FII)', 'Foreign Institutional Investor (FII)'), ('Hindu Undivided Family (HUF)', 'Hindu Undivided Family (HUF)'), ('Association of persons', 'Association of persons'), ('Bank', 'Bank'), ('Government Body', 'Government Body'), ('Non-Government Organisation', 'Non-Government Organisation'), ('Defence Establishment', 'Defence Establishment'), ('Body of Individuals', 'Body of Individuals'), ('Society', 'Society'), ('Limited Liability Partnership', 'Limited Liability Partnership'), ('Others', 'Others')], db_index=True, max_length=60, null=True, verbose_name='Entity Type'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='industry_segment',
            field=models.CharField(blank=True, choices=[('Aviation', 'Aviation'), ('Agriculture', 'Agriculture'), ('Auto Components', 'Auto Components'), ('Automobiles', 'Automobiles'), ('Bio fuels', 'Bio fuels'), ('Biotechnology', 'Biotechnology'), ('Capital goods', 'Capital goods'), ('Chemicals', 'Chemicals'), ('Defence', 'Defence'), ('Drugs and Pharmaceuticals', 'Drugs and Pharmaceuticals'), ('E Commerce - Food Tech', 'E Commerce - Food Tech'), ('E Commerce - Grocery Tech', 'E Commerce - Grocery Tech'), ('E Commerce - Large goods', 'E Commerce - Large goods'), ('E Commerce - Others', 'E Commerce - Others'), ('E Commerce - Retail', 'E Commerce - Retail'), ('Education and Training', 'Education and Training'), ('Energy', 'Energy'), ('Family business', 'Family business'), ('Fast moving consumer goods', 'Fast moving consumer goods'), ('Food processing', 'Food processing'), ('Gems and Jewellary', 'Gems and Jewellary'), ('Health care', 'Health care'), ('Housing', 'Housing'), ('Information and Communication technology', 'Information and Communication technology'), ('Infrastructure', 'Infrastructure'), ('Innovation', 'Innovation'), ('Leather and Leather products', 'Leather and Leather products'), ('Logistics', 'Logistics'), ('Manufacturing', 'Manufacturing'), ('Media and Entertainment', 'Media and Entertainment'), ('Micro, Medium and Small scale industries', 'Micro, Medium and Small scale industries'), ('Oil and Gas', 'Oil and Gas'), ('Power', 'Power'), ('Real estate', 'Real estate'), ('Retail', 'Retail'), ('Safety and Security', 'Safety and Security'), ('Sports goods', 'Sports goods'), ('Telecom & Broadband', 'Telecom & Broadband'), ('Tourism and Hospitality', 'Tourism and Hospitality'), ('Urban development', 'Urban development'), ('Water', 'Water'), ('IT & ITES', 'IT & ITES'), ('Cement', 'Cement'), ('Furniture and Home Furnishing', 'Furniture and Home Furnishing'), ('Home Services', 'Home Services')], db_index=True, max_length=60, null=True, verbose_name='Industry Segment'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='is_monthly_invoice',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Monthly Invoice'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='is_tax_registered',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Tax Registered'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='legalname',
            field=oscar.models.fields.NullCharField(db_index=True, max_length=255, unique=True, verbose_name='Legal Name'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='service_tax_category',
            field=models.CharField(blank=True, choices=[('GTA', 'Goods Transportation Agency - GTA'), ('BSS', 'Business Support Services - BSS')], db_index=True, max_length=12, null=True, verbose_name='GTA/BSS'),
        ),
    ]
