# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-06-07 07:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0051_invoicehistory'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creditdebitnotes',
            name='tax_category',
            field=models.CharField(blank=True, choices=[('GTA', 'Goods Transportation Agency - GTA'), ('BSS', 'Business Support Services - BSS'), ('TAX EXEMPT', 'Tax Exempt')], max_length=12, null=True, verbose_name='Tax Category'),
        ),
    ]
