# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-05-14 19:08
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0049_contract_identifier'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customer', '0036_auto_20180509_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerInvoiceConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('aggregates', sortedm2m.fields.SortedManyToManyField(blank=True, help_text=None, null=True, sorted='Invoice Aggregate', to='customer.InvoiceAggregateAttribute')),
                ('contracts', sortedm2m.fields.SortedManyToManyField(help_text=None, sorted='Customer Contract', to='contract.Contract')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customerinvoiceconfig_created_by', to=settings.AUTH_USER_MODEL)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='invoice_configs', to='customer.Customer')),
                ('invoice_cycle', models.ForeignKey(help_text='Invoice cycle for generating invoices', on_delete=django.db.models.deletion.PROTECT, related_name='invoice_cycle', to='contract.PaymentCycle')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customerinvoiceconfig_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Customer Invoice Config',
                'verbose_name_plural': 'Customer Invoice Configs',
            },
        ),
        migrations.AddField(
            model_name='customerinvoice',
            name='invoice_configs',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='customer.CustomerInvoiceConfig'),
        ),
    ]
