# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-07 10:14
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contract', '0070_kiosklabourconfiguration'),
        ('customer', '0065_auto_20180802_2120'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuthenticationConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('authentication_type', models.IntegerField(choices=[(-1, 'NONE'), (1, 'BASIC'), (2, 'TOKEN')])),
                ('enabled', models.BooleanField(default=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='authenticationconfig_created_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BasicAuthCredential',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('username', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='basicauthcredential_created_by', to=settings.AUTH_USER_MODEL)),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='basicauthcredential_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='GPSEventPublishConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('api_endpoint', models.URLField()),
                ('http_request_type', models.IntegerField(choices=[(1, 'GET'), (2, 'POST')])),
                ('interval', models.IntegerField(default=300, help_text='Interval in seconds.')),
                ('max_records_per_driver', models.IntegerField(default=-1, help_text='Max no. of records per driver in one request. -1 denotes no limit.')),
                ('comments', models.TextField(blank=True, null=True)),
                ('enabled', models.BooleanField(default=False, help_text='Indicates whether the configuration is enabled ?')),
                ('authentication_config', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='customer.AuthenticationConfig')),
                ('contracts', models.ManyToManyField(to='contract.Contract')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='gpseventpublishconfig_created_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'GPS Event Publishing Configuration',
                'verbose_name_plural': 'GPS Event Publishing Configurations',
            },
        ),
        migrations.CreateModel(
            name='GPSEventPublishConfigView',
            fields=[
            ],
            options={
                'verbose_name': 'GPS Event Publish Config View',
                'verbose_name_plural': 'GPS Event Publish Config Views',
                'proxy': True,
                'indexes': [],
            },
            bases=('customer.customer',),
        ),
        migrations.AddField(
            model_name='gpseventpublishconfig',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gps_event_publish_config', to='customer.Customer'),
        ),
        migrations.AddField(
            model_name='gpseventpublishconfig',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='gpseventpublishconfig_modified_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='authenticationconfig',
            name='credentials',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customer.BasicAuthCredential'),
        ),
        migrations.AddField(
            model_name='authenticationconfig',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customer.Customer'),
        ),
        migrations.AddField(
            model_name='authenticationconfig',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='authenticationconfig_modified_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
