# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-02-02 15:07
from __future__ import unicode_literals

from django.db import migrations
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0019_auto_20180131_1316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerinvoice',
            name='status',
            field=django_fsm.FSMField(default='CREATED', max_length=50),
        ),
    ]
