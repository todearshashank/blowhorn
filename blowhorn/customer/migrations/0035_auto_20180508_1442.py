# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-05-08 14:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0034_auto_20180507_1518'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerinvoiceadjustment',
            name='comments',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Comments (Optional)'),
        ),
        migrations.AddField(
            model_name='invoicecreationrequest',
            name='output_log',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='customer',
            name='service_tax_category',
            field=models.CharField(blank=True, choices=[('GTA', 'Goods Transportation Agency - GTA'), ('BSS', 'Business Support Services - BSS'), ('TAX EXEMPT', 'Tax Exempt')], db_index=True, max_length=12, null=True, verbose_name='GTA/BSS'),
        ),
    ]
