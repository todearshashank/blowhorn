# Generated by Django 2.2.17 on 2021-05-17 16:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0150_auto_20210515_1310'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerinvoice',
            name='manual_upload_reason',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='customer.ManualUploadReason'),
        ),
    ]
