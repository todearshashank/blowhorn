# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-26 07:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0006_auto_20180615_0602'),
        ('customer', '0073_customerinvoiceconfig_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerinvoiceconfig',
            name='company_bank_account',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='company.CompanyBankAccount', verbose_name='Company Bank Account'),
        ),
    ]
