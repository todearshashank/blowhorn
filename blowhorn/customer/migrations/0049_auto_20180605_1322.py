# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-06-05 13:22
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0048_auto_20180601_1036'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='invoicebatch',
            options={'verbose_name': 'Invoice Batch', 'verbose_name_plural': 'Invoice Batches'},
        ),
    ]
