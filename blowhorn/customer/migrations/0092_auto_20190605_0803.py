# Generated by Django 2.1.1 on 2019-06-05 08:03

from django.db import migrations, models
import recurrence.fields


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0091_auto_20190509_0656'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='enterprise_credit_amount',
            field=models.IntegerField(blank=True, null=True, verbose_name='Credit Amount for enterprise customer in INR'),
        ),
        migrations.AddField(
            model_name='customer',
            name='enterprise_credit_applicable',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Whether Credit Policy Applicable?'),
        ),
        migrations.AddField(
            model_name='customer',
            name='enterprise_credit_period',
            field=models.IntegerField(blank=True, null=True, verbose_name='Credit Period for enterprise customer in Days'),
        ),
        migrations.AddField(
            model_name='customerinvoiceconfig',
            name='payment_schedule',
            field=recurrence.fields.RecurrenceField(blank=True, help_text='Customer would typically pay us on this day', null=True),
        ),
    ]
