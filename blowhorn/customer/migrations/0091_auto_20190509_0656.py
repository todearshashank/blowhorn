# Generated by Django 2.1.1 on 2019-05-09 06:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0090_auto_20190425_0814'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerinvoice',
            name='non_taxable_adjustment',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Non Taxable Adjustment'),
        ),
        migrations.AddField(
            model_name='customerinvoice',
            name='taxable_adjustment',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Taxable Adjustment'),
        ),
        migrations.AddField(
            model_name='customerinvoice',
            name='total_service_charge',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Total Service Charge'),
        ),
    ]
