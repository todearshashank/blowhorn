# Generated by Django 2.2.18 on 2021-02-19 09:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0139_auto_20210202_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='sku',
            name='include_in_sellshipmentslab',
            field=models.BooleanField(default=False),
        ),
    ]
