# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-22 14:04
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import django_fsm
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0016_auto_20180119_1159'),
    ]

    operations = [
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Route name')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='customer.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='RouteStop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seq_no', models.IntegerField()),
                ('address', models.CharField(max_length=255, verbose_name='Contact address')),
                ('contact_name', models.CharField(max_length=100, null=True, verbose_name='Contact name')),
                ('contact_mobile', phonenumber_field.modelfields.PhoneNumberField(blank=True, help_text="Contact person's primary mobile number e.g. +91{10 digit mobile number}", max_length=128, null=True, verbose_name='Mobile Number')),
                ('geopoint', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('route', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='stops', to='customer.Route')),
            ],
        ),
        migrations.AlterField(
            model_name='customerinvoice',
            name='status',
            field=django_fsm.FSMField(choices=[('CREATED', 'CREATED'), ('READY_TO_BE_SENT', 'READY_TO_SEND'), ('SENT', 'SENT')], default='CREATED', max_length=50),
        ),
        migrations.AlterUniqueTogether(
            name='route',
            unique_together=set([('name', 'customer')]),
        ),
    ]
