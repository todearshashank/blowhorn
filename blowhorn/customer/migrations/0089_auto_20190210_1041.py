# Generated by Django 2.1.1 on 2019-02-10 10:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0088_auto_20190203_1443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoiceaggregation',
            name='contract_type',
            field=models.CharField(blank=True, choices=[('Spot', 'Spot'), ('Fixed', 'Fixed'), ('Shipment', 'Shipment'), ('Kiosk', 'Kiosk'), ('FlashSale', 'FlashSale')], max_length=15, null=True, verbose_name='Contract Type'),
        ),
        migrations.AlterField(
            model_name='invoiceentity',
            name='contract_type',
            field=models.CharField(blank=True, choices=[('Spot', 'Spot'), ('Fixed', 'Fixed'), ('Shipment', 'Shipment'), ('Kiosk', 'Kiosk'), ('FlashSale', 'FlashSale')], max_length=15, null=True, verbose_name='Contract Type'),
        ),
        migrations.AlterField(
            model_name='invoiceinline',
            name='contract_type',
            field=models.CharField(blank=True, choices=[('Spot', 'Spot'), ('Fixed', 'Fixed'), ('Shipment', 'Shipment'), ('Kiosk', 'Kiosk'), ('FlashSale', 'FlashSale')], max_length=15, null=True, verbose_name='Contract Type'),
        ),
    ]
