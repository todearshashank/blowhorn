# Generated by Django 2.1.1 on 2020-11-21 07:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0134_customerreportschedule_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='source',
            field=models.CharField(blank=True, default='Website', max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='customerotp',
            name='remarks',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
