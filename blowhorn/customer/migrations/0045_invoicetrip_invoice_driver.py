# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-05-26 13:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0066_auto_20180525_0819'),
        ('customer', '0044_auto_20180525_1259'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoicetrip',
            name='invoice_driver',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='driver.Driver'),
        ),
    ]
