from django import forms
from django.conf import settings
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.phonenumber import PhoneNumber

from blowhorn.customer.models import Customer, AccountUser, \
    CUSTOMER_CATEGORY_INDIVIDUAL
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.users.models import User
from blowhorn.utils.functions import validate_phone_number
from blowhorn.customer.constants import MEMBER_ACTIVE, MEMBER_INACTIVE


class AccountUserUpdateForm(forms.ModelForm):
    """
    Form class for editing OrganizationUsers *and* the linked user model.
    """
    name = forms.CharField(max_length=100)
    email = forms.EmailField()
    password = ReadOnlyPasswordHashField(label=_('Password'))
    phone_number = forms.CharField(label='Mobile Number',
                                   max_length=10,
                                   validators=[validate_phone_number])
    is_active = forms.BooleanField(required=False)
    is_admin = forms.BooleanField(required=False)

    class Meta:
        exclude = ('user',)
        model = AccountUser

    def __init__(self, *args, **kwargs):
        super(AccountUserUpdateForm, self).__init__(*args, **kwargs)
        if self.instance.pk is not None:
            user = self.instance.user
            self.fields['name'].initial = user.name
            self.fields['email'].initial = user.email
            self.fields['password'].initial = user.password
            self.fields['phone_number'].initial = user.national_number
            self.fields['is_active'].initial = user.is_active

            # No reverse url for password change. Need to construct manually
            link = reverse('admin:users_user_change', args=[user.id])
            url_components = link.split('/')[:-2]
            url_components.append('password')
            final_url = '/'.join(url_components)
            help_text = """
                Raw passwords are not stored, so there is no way to see this
                user's password, but you can change the password using""" + \
                u'<a href="%s" target="_blank"> this form</a>' % final_url
            self.fields['password'].help_text = help_text

        self.fields['organization'].autocomplete = False
        self.fields['organization'].queryset = Customer.objects.exclude(
            customer_category=CUSTOMER_CATEGORY_INDIVIDUAL
        )

    def clean(self, *args, **kwargs):
        super(AccountUserUpdateForm, self).clean(*args, **kwargs)
        email = self.cleaned_data['email']
        email = email.lower()

        account = self.Meta.model.objects.filter(user__email=email).exclude(
            pk=self.instance.pk).first()
        if account:
            raise ValidationError('%s is already in %s' % (email, account.organization))

        phone_number = self.cleaned_data.get('phone_number', None) or None
        if phone_number and settings.ACTIVE_COUNTRY_CODE not in str(phone_number):
                self.cleaned_data['phone_number'] = PhoneNumber(
                    settings.ACTIVE_COUNTRY_CODE,
                    phone_number
                )

    def save(self, *args, **kwargs):
        """
        This method saves changes to the linked user model.
        """
        User.objects.filter(pk=self.instance.user.pk).update(
            name=self.cleaned_data['name'],
            email=self.cleaned_data['email'],
            is_active=self.cleaned_data['is_active'],
            phone_number=self.cleaned_data['phone_number']
        )
        self.instance.status = MEMBER_ACTIVE.lower() if self.cleaned_data['is_active'] else MEMBER_INACTIVE.lower()
        return super(AccountUserUpdateForm, self).save(*args, **kwargs)


class AccountUserForm(forms.ModelForm):
    """
    Form class for editing OrganizationUsers *and* the linked user model.
    """
    name = forms.CharField(max_length=100)
    email = forms.EmailField()
    phone_number = forms.CharField(label='Mobile Number',
                                   max_length=10,
                                   validators=[validate_phone_number],
                                   required=False)

    class Meta:
        exclude = ('user',)
        model = AccountUser

    def __init__(self, *args, **kwargs):
        super(AccountUserForm, self).__init__(*args, **kwargs)

        self.fields['organization'].autocomplete = False
        self.fields['organization'].queryset = Customer.objects.exclude(
            customer_category=CUSTOMER_CATEGORY_INDIVIDUAL
        )

    def clean(self, *args, **kwargs):
        super(AccountUserForm, self).clean(*args, **kwargs)

        email = self.cleaned_data.get('email', '')
        email = email.lower()
        customer = Customer.objects.filter(user__email=email).first()
        if customer:
            raise ValidationError('Email is already registered %s' % customer)

        account = self.Meta.model.objects.filter(user__email=email).first()
        if account:
            raise ValidationError('%s is already in %s' % (email, account.organization))

        phone_number = self.cleaned_data.get('phone_number', None) or None
        if phone_number and settings.ACTIVE_COUNTRY_CODE not in str(phone_number):
            self.cleaned_data['phone_number'] = PhoneNumber(
                settings.ACTIVE_COUNTRY_CODE,
                phone_number
            )

    def save(self, *args, **kwargs):
        """
        This method saves changes to the linked user model.
        """
        name = self.cleaned_data.get('name', '')
        email = self.cleaned_data.get('email', '')
        phone_number = self.cleaned_data.get('phone_number', '')
        user, created = CustomerMixin().send_invitation(
            name, email, str(phone_number),
            self.cleaned_data['organization'])

        self.instance.user = user
        return super(AccountUserForm, self).save(*args, **kwargs)


class AccountUserGroupForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super(AccountUserGroupForm, self).__init__(*args, **kwargs)
        if 'organization' in self.fields:
            self.fields['organization'].autocomplete = False
            self.fields['organization'].queryset = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL
            )
