import logging
from django.utils.translation import ugettext_lazy as _


from rest_framework import status, generics
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.customer.serializers import CustomerActionSerializer
from blowhorn.customer.submodels.invoice_models import CustomerInvoice

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class InvoiceActionView(generics.UpdateAPIView, CustomerMixin):
    """
    PUT invoice/enterprise/<invoice_pk>/action
    data:
        action: accept/reject (str)
        reason: only if action is reject (str)
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)
    serializer_class = CustomerActionSerializer

    def put(self, request, pk=None):
        data = request.data
        user = request.user
        invoice = self.get_queryset(pk)
        if not invoice:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data=_('Invoice not found'))

        serializer = self.serializer_class(
            invoice,
            data=data,
            context={'user': user}
        )
        if serializer.is_valid():
            message = serializer.save()
            return Response(status=status.HTTP_200_OK, data=_(message))

        logger.warning(serializer.errors, exc_info=1)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self, invoice_pk):
        try:
            return CustomerInvoice.objects.get(pk=invoice_pk)
        except CustomerInvoice.DoesNotExist:
            return None
