from django.db.models import Q, Prefetch

from rest_framework import generics, filters, views, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.constants import ENTERPRISE_INVOICE_STATUS_MAP, \
    ALLOWED_INVOICE_SEARCH_FIELDS, INVOICE_FILTER_DATE_FIELD, \
    REASONS_FOR_DISPUTING_INVOICE, INVOICE_PENDING
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import CustomerInvoice
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.customer.serializers import CustomerInvoiceListSerializer
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.customer.models import ContractInvoiceFragment
from blowhorn.contract.models import Contract


class InvoiceListView(generics.ListAPIView):
    """
    GET invoice/enterprise/list
    Returns list of invoices (filtered)
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)
    serializer_class = CustomerInvoiceListSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('-service_period_end', '-total_amount')

    def __get_query_params(self, filter_param):
        user = self.request.user
        current_status = ENTERPRISE_INVOICE_STATUS_MAP.get(
            filter_param.pop('status', 'pending'))
        query_params = Q(customer=CustomerMixin().get_customer(user)) & Q(
            status=current_status) & ~Q(is_deleted=True)

        search_val = filter_param.pop('search', None)
        if search_val:
            search_query = None
            for field in ALLOWED_INVOICE_SEARCH_FIELDS:
                if search_query:
                    search_query |= Q(**{field: search_val})
                else:
                    search_query = Q(**{field: search_val})
            query_params &= search_query
        start_date = filter_param.get('start', None)
        end_date = filter_param.get('end', None)
        if start_date and end_date:
            start_date_param = '%s__gte' % INVOICE_FILTER_DATE_FIELD
            end_date_param = '%s__lte' % INVOICE_FILTER_DATE_FIELD
            query_params &= Q(**{start_date_param: start_date}) & Q(
                **{end_date_param: end_date})
        state = filter_param.get('state', None)
        if state:
            query_params &= Q(invoice_state_id=state)
        return query_params

    def get_queryset(self):
        filter_param = self.request.query_params.dict()
        query_params = self.__get_query_params(filter_param)
        fieldname_map = {
            'city': 'contract__city_id',
            'division': 'contract__division_id'
        }
        permission_query = CustomerMixin().get_permission_query(self.request.user, 'invoices', CustomerInvoice, fieldname_map=fieldname_map)
        if permission_query:
            query_params &= permission_query
        qs = CustomerInvoice.objects.filter(query_params).select_related(
            'invoice_state')
        if filter_param.get('status', None) not in [INVOICE_PENDING]:
            qs = qs.prefetch_related('invoicehistory_set')
        return qs.order_by(*self.ordering_fields)


class InvoiceSupportData(views.APIView):
    """
    GET invoice/enterprise/supportdata
    """

    def get(self, request):
        reasons_for_dispute = [
            {'id': r[0], 'name': r[1]} for r in REASONS_FOR_DISPUTING_INVOICE
        ]
        data = {
            'reasons_for_dispute': reasons_for_dispute
        }
        return Response(status=status.HTTP_200_OK, data=data)


class CustomerContactView(views.APIView):
    # authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request, invoice_pk=None):
        invoice = CustomerInvoice.objects.select_related(
            'customer').prefetch_related(
            Prefetch('contract_invoices',
                     queryset=ContractInvoiceFragment.objects.all().select_related(
                         'contract'
                     )
        )).get(pk=invoice_pk)
        customer = invoice.customer
        contacts = []
        cities = []
        division = invoice.aggregation.division if hasattr(invoice, 'aggregation') else None
        division_based = True

        if customer:
            contracts_fragment = invoice.contract_invoices.all()
            for fragment in contracts_fragment:
                if fragment.contract:
                    cities.append(fragment.contract.city)
            if not cities:
                cities = list(invoice.invoice_state.cities.all())
                cities = cities + list(invoice.bh_source_state.cities.all())

            if division:
                contacts = customer.customercontact_set.filter(
                    Q(division__isnull=True) | Q(division=division)
                ).values('email', 'division__name')

            if not contacts:
                division_based = False
                contacts = customer.customercontact_set.filter(
                    Q(city__isnull=True) | Q(city__in=cities)
                ).values('email', 'city__name')

        if contacts:
            if division_based:
                contact_json = [
                '%s | %s' % (c.get('email'), c.get('division__name')) for c in
                contacts if c]
            else:
                contact_json = [
                    '%s | %s' % (c.get('email'), c.get('city__name')) for c in
                    contacts if c]
            spocs = list(
                Contract.objects.filter(customer=customer, city__in=cities).values_list(
                    'spocs__email', flat=True))
            supervisors = list(
                Contract.objects.filter(customer=customer, city__in=cities).values_list(
                    'supervisors__email',
                    flat=True))
            _dict = {
                'contacts': contact_json,
                'staff': list(set(spocs + supervisors))
            }
            return Response(status=status.HTTP_200_OK, data=_dict)

        return Response(status=status.HTTP_404_NOT_FOUND)
