import logging
import zipfile
from datetime import datetime
from django.conf import settings
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _

from rest_framework import views
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from blowhorn.customer.constants import DOWNLOAD_DOCUMENT_TYPES, DOC_MIS, \
    DOC_INVOICE
from blowhorn.customer.models import CustomerInvoice, MISDocument
from blowhorn.customer.permissions import IsNonIndividualCustomer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class InvoiceDownload(views.APIView):
    """
    GET invoice/enterprise/download/<invoice_number>/<doc_type>
    Returns rendered pdf file for download to prevent exposure of storage url
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def __get_zipped_mis_documents(self, invoice):
        invoice_number = invoice.invoice_number
        documents = MISDocument.objects.filter(
            invoice=invoice)
        if documents:
            now = datetime.now().strftime("%d_%m_%Y_%H_%M")
            zip_filename = '%s.zip' % invoice_number
            atleast_one = False
            with zipfile.ZipFile(
                settings.TEMPORARY_FILE_DIR + '/' + zip_filename, 'w') \
                as myzip:
                for f in documents:
                    if f.mis_doc:
                        atleast_one = True
                        f_ext = f.mis_doc.name.split('.')[-1]
                        tmp_filename = '%s_%s.%s' % (
                            f.invoice.invoice_number, now, f_ext)
                        myzip.writestr(tmp_filename,
                                       f.mis_doc.read())

            if atleast_one:
                try:
                    zf = open(myzip.filename, 'rb')
                    _file = zf.read()
                    response = HttpResponse(_file,
                                            content_type='application/zip')
                    content = "attachment; filename='%s'.zip" % invoice_number
                    response['Content-Disposition'] = content
                    return response

                except FileNotFoundError:
                    logger.info('File is missing: %s' % zip_filename)

        return HttpResponse('No Documents Found.')

    def get(self, request, invoice_number=None, doc_type=None):
        if doc_type not in DOWNLOAD_DOCUMENT_TYPES:
            return HttpResponse(_('File Not Found'))

        try:
            invoice = CustomerInvoice.objects.get(invoice_number=invoice_number)
        except CustomerInvoice.DoesNotExist:
            invoice = None

        if invoice:
            if doc_type == DOC_INVOICE and invoice.file:
                try:
                    response = HttpResponse(invoice.file,
                                            content_type='application/pdf')
                except FileNotFoundError:
                    return HttpResponse(_('File Not Found'))

                filename = "invoice_%s.pdf" % invoice.invoice_number
                content = "attachment; filename=%s" % filename
                response['Content-Disposition'] = content
                return response

            elif doc_type == DOC_MIS:
                return self.__get_zipped_mis_documents(invoice)

        return HttpResponse(_('File Not Found'))
