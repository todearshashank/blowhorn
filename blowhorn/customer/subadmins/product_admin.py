# from django.contrib import admin
# from blowhorn.oscar.apps.catalogue.models import Product
# from blowhorn.common.admin import BaseAdmin
# from blowhorn.oscar.core.loading import get_model
#
# ProductAttributeValue = get_model('catalogue', 'ProductAttributeValue')
# ProductImage = get_model('catalogue', 'ProductImage')
# Stock = get_model('partner', 'StockRecord')
#
#
#
# class AttributeInline(admin.TabularInline):
#     model = ProductAttributeValue
#     fields = ('attribute', 'value_text',)
#     extra = 0
#
#
# class ProductImageInline(admin.TabularInline):
#     model = ProductImage
#     extra = 0
#
#
# class StockInline(admin.TabularInline):
#     model = Stock
#     extra = 0
#
#
# class ProductAdmin(BaseAdmin):
#     date_hierarchy = 'date_created'
#     list_display = ('get_title', 'upc', 'get_product_class', 'structure',
#                     'attribute_summary', 'date_created')
#     list_filter = ['structure', 'is_discountable']
#     raw_id_fields = ['parent']
#     inlines = [AttributeInline, ProductImageInline, StockInline]
#     prepopulated_fields = {"slug": ("title",)}
#     search_fields = ['upc', 'title']
#
#     def get_queryset(self, request):
#         qs = super(ProductAdmin, self).get_queryset(request)
#         return (
#             qs
#                 .select_related('product_class', 'parent')
#                 .prefetch_related(
#                 'attribute_values',
#                 'attribute_values__attribute'))
#
#
# admin.site.unregister(Product)
# admin.site.register(Product, ProductAdmin)
