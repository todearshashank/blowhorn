import os
import csv
import pytz
from datetime import datetime, timedelta, date
from django.conf import settings
from django.contrib import admin
from django.db.models import Value, CharField, Func, F, Count, TextField, Sum, \
    ExpressionWrapper, FloatField, Case, When, Q, DateField, IntegerField, \
    DurationField, Window, Avg
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
from django.utils import timezone
from django.contrib.postgres.aggregates import StringAgg
from django.db.models.functions import Cast, Coalesce, ExtractYear, Concat
from django.db.models.aggregates import Min
from django.contrib.admin import SimpleListFilter
from dateutil.rrule import rrule, YEARLY

from blowhorn.customer.constants import (INVOICE_STATUSES_FOR_SUMMARY,
                                         INVOICE_SUMMARY_LEGEND, INVOICE_EDITABLE_STATUS)
from blowhorn.customer.constants import (APPROVED, SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED,
    CUSTOMER_CONFIRMED, PARTIALLY_PAID, PAYMENT_DONE, TAX_INVOICE, CREDIT_NOTE,
    COLLECTION_STATUS_CLOSED, COLLECTION_STATUS_PARTIALLY_CLOSED, COLLECTION_STATUS_OPEN)
from blowhorn.contract.permissions import is_finance_user
from blowhorn.customer.models import InvoiceSummary, OpeningBalance, \
    CustomerLedger, InvoiceExpirationBatch, CUSTOMER_CATEGORY_INDIVIDUAL, \
    InvoiceHistory, CollectionsSummary, CustomerInvoice, Customer, \
    CustomerReceivable, ManualUploadReason, CreditDebitReason
from blowhorn.utils.datetimerangefilter import DateFilter, DateRangeFilter
from blowhorn.utils.functions import format_datetime, utc_to_ist, \
    convert_date_hierarchy_params
from blowhorn.driver.util import get_financial_year


def export_opening_balance_list(modeladmin, request, queryset):
    file_path = 'opening_balance'

    opening_balance_ids = queryset.values_list('id', flat=True)

    opening_balance = OpeningBalance.copy_data.filter(
        id__in=opening_balance_ids)

    opening_balance = opening_balance.select_related(
        'customer'
    ).annotate(
        op_date=Func(F('date') + timedelta(hours=5.5),
                     Value("DD/MM/YYYY"),
                     function='to_char',
                     output_field=CharField())

    ).to_csv(
        file_path,
        'customer__name', 'op_date', 'amount', 'comments'
    )

    modified_file = '%s_' % str(utc_to_ist(
        timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'Customer', 'Date', 'Amount', 'Comments',
        ])

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:

            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + \
                os.path.basename(modified_file)
            os.remove(modified_file)
            return response


@admin.register(OpeningBalance)
class OpeningBalanceAdmin(admin.ModelAdmin):

    actions = [export_opening_balance_list]
    list_display = (
        'id', 'customer', 'date', 'amount', )

    search_fields = ('customer__name', 'customer__legalname', )

    list_filter = [('date', DateFilter), ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer')
        return qs

    class Media:
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/opening_balances_summation.js',
            )


class InvoiceCustomersFilter(SimpleListFilter):
    """ Shows only customers who have invoices
    """
    title = _('customer')
    parameter_name = 'customer'

    def lookups(self, request, model_admin):
        return [(c.id, c.name) for c in Customer.objects.annotate(invoices_cnt=Count('customerinvoice')).filter(invoices_cnt__gt=0)]

    def queryset(self, request, queryset):
        if self.value():
            return CustomerInvoice.objects.filter(customer_id=self.value())


class ShowInvoiceFilter(SimpleListFilter):
    """ Shows all invoices
    """
    title = _('Invoice')
    parameter_name = 'cust_invoice_id'

    def lookups(self, request, model_admin):
        choice = ['All']
        return [(c, c) for c in choice]

    def queryset(self, request, queryset):
        return queryset


class FinancialYearFilter(SimpleListFilter):
    """
    Filter for getting the invoices of a given financial year
    """
    title = _('Financial Year')
    parameter_name = 'financial_year'

    def lookups(self, request, model_admin):
        start_year = 2015
        end_date = timezone.now()
        start_date = datetime(day=end_date.day, month=end_date.month, year=start_year)
        start_date = pytz.utc.localize(datetime.combine(start_date, datetime.min.time()))
        return [(get_financial_year(year), get_financial_year(year)) for year in
                rrule(YEARLY, dtstart=start_date, until=end_date)]

    def queryset(self, request, queryset):
        if self.value():
            val = [int(x) for x in self.value().split('-')]
            start_date = datetime(day=1, month=4, year=val[0])
            end_date = datetime(day=1, month=4, year=val[1])
            return CustomerInvoice.objects.filter(invoice_date__gte=start_date, invoice_date__lt=end_date)


@admin.register(CollectionsSummary)
class CollectionsSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/customer/collections/change_list.html'
    list_filter = [
        InvoiceCustomersFilter, 'bh_source_state', ShowInvoiceFilter, FinancialYearFilter
    ]
    # shoddily done, works only on invoice numbers :(
    search_fields = ('invoice_number', )

    def has_add_permission(self, request):
        return False

    def get_collections_data(self, customer_id):
        queryset = CustomerReceivable.objects.filter(customer=customer_id,
                                                     status__in=[APPROVED])
        if customer_id is None:
            queryset = CustomerReceivable.objects.filter(status__in=[APPROVED])
        queryset = queryset.annotate(
                customer_name=F('customer__name'),
                customer_type=F('customer__customer_category'),
                transaction_type=F('payment_mode'),
                invoice_identifier=Value(None, output_field=CharField()),
                cn_against_invoice=Value(None, output_field=CharField()),
                transaction_date=F('payment_date'),
                service_start=Value(None, output_field=DateField()),
                service_end=Value(None, output_field=DateField()),
                source_state=Value(None, output_field=CharField()),
                total_recvd_or_receivable=-F('amount'), # display negative for received amount in bank
                allocated_bank_credit=Sum(F('invoicepayment__actual_amount')),
                tds_allocation=Value(None, output_field=FloatField()),
                cn_allocation=Value(None, output_field=FloatField()),
                balance_amount=F('unallocated_amount'),
                transaction_status=Case(
                                    When(Q(balance_amount__lt=10), then=Value(COLLECTION_STATUS_CLOSED)),
                                    When(Q(balance_amount__gt=10) & Q(allocated_bank_credit__gt=0),
                                        then=Value(COLLECTION_STATUS_PARTIALLY_CLOSED)),
                                    default=Value(COLLECTION_STATUS_OPEN),
                                    output_field=CharField(),
                                    ),
                collection_id=F('id'),
                credit_days=Value(None, output_field=IntegerField()),
                due_by=Value(None, output_field=DateField()),
                ageing_in_days=Value(None, output_field=DurationField()),
                ageing_bucket=Value(None, output_field=CharField()),
                sales_spoc=Value(None, output_field=CharField()),
                credit_amount=Value(None, output_field=CharField()),
                year=ExtractYear('payment_date'),
                financial_year=Case(
                    When(payment_date__month__lt=4, then=Concat(F('year') - 1, Value('-'), F('year'))),
                    When(payment_date__month__gte=4, then=Concat(F('year'), Value('-'), F('year') + 1)),
                    output_field=CharField())
                ).values(
                    'customer_name',
                    'customer_type',
                    'transaction_type',
                    'invoice_identifier',
                    'cn_against_invoice',
                    'transaction_date',
                    'total_recvd_or_receivable',
                    'service_start',
                    'service_end',
                    'source_state',
                    'allocated_bank_credit',
                    'tds_allocation',
                    'cn_allocation',
                    'balance_amount',
                    'transaction_status',
                    'collection_id',
                    'credit_days',
                    'due_by',
                    'ageing_in_days',
                    'ageing_bucket',
                    'sales_spoc',
                    'credit_amount',
                    'financial_year',
                    )
        return queryset

    def get_invoice_data(self, queryset):
        queryset = queryset.annotate(
            customer_name=F('customer__name'),
            customer_type=F('customer__customer_category'),
            transaction_type=F('invoice_type'),
            invoice_identifier=F('invoice_number'),
            cn_against_invoice=F('related_invoice__invoice_number'),
            transaction_date=F('invoice_date'),
            service_start=F('service_period_start'),
            service_end=F('service_period_end'),
            source_state=F('bh_source_state__name'),
            total_recvd_or_receivable=F('total_amount'),
            # display allocated bank credit, CN and TDS as nagative amounts
            allocated_bank_credit=-F('invoicepayment__actual_amount'),
            tds_allocation=-F('invoicepayment__tds'),
            cn_allocation=-F('invoicepayment__debit'),
            cum_bank_credit=Window(
                expression=Sum('invoicepayment__actual_amount'),
                partition_by=[F('invoice_number')]
            ),
            cum_tds=Window(
                expression=Sum('invoicepayment__tds'),
                partition_by=[F('invoice_number')]
            ),
            cum_cn=Window(
                expression=Sum('invoicepayment__debit'),
                partition_by=[F('invoice_number')]
            ),
            balance_amount=Case(
                When(transaction_type=TAX_INVOICE, then=ExpressionWrapper(
                    Coalesce(F('total_recvd_or_receivable'), Value(0)) - Coalesce(F('cum_bank_credit'), Value(0)) \
                    - Coalesce(F('cum_tds'), Value(0)) - Coalesce(F('cum_cn'), Value(0)),
                output_field=FloatField())),
                default=Value(None)
                ),
            transaction_status=Case(
                                When(Q(transaction_type=TAX_INVOICE) & Q(balance_amount__lt=10), then=Value(COLLECTION_STATUS_CLOSED)),
                                When(Q(transaction_type=TAX_INVOICE) & Q(balance_amount__gt=10) & Q(allocated_bank_credit__gt=0),
                                    then=Value(COLLECTION_STATUS_PARTIALLY_CLOSED)),
                                When(Q(transaction_type=CREDIT_NOTE), then=Value(COLLECTION_STATUS_CLOSED)),
                                default=Value(COLLECTION_STATUS_OPEN),
                                output_field=CharField(),
                                ),
            collection_id=F('invoicepayment__collection__id'),
            credit_days=F('customer__enterprise_credit_period'),

            due_by=Case(
                    When(Q(transaction_type=TAX_INVOICE) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                        then=ExpressionWrapper(F('transaction_date') + F('credit_days'), output_field=DateField())),
                    default=Value(None, output_field=DateField()),
                    output_field=DateField()
                ),
            ageing_in_days=Case(
                    When(Q(transaction_type=TAX_INVOICE) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                        then=ExpressionWrapper(
                                date.today() - F('due_by'), output_field=DurationField())),
                        output_field=DurationField()
                ),
            ageing_bucket=Case(
                            When(Q(ageing_in_days__lt=30), then=Value('Not Aged')),
                            When(Q(ageing_in_days__gt=31) & Q(ageing_in_days__lt=60) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                                then=Value('31-60 days')),
                            When(Q(ageing_in_days__gt=61) & Q(ageing_in_days__lt=90) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                                then=Value('61-90 days')),
                            When(Q(ageing_in_days__gt=91) & Q(ageing_in_days__lt=180) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                                then=Value('91-180 days')),
                            When(Q(ageing_in_days__gt=181) & Q(ageing_in_days__lt=360) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED),
                                then=Value('181-360 days')),
                            When(Q(ageing_in_days__gt=360) & ~Q(transaction_status=COLLECTION_STATUS_CLOSED), then=Value('> 360 days')),
                            output_field=CharField()
                            ),
            sales_spoc=F('current_owner__email'),
            credit_amount=Window(
                expression=Sum('customerinvoice__total_amount'),
                partition_by=[F('invoice_number')]
            ),
            year=ExtractYear('invoice_date'),
            financial_year=Case(
                When(invoice_date__month__lt=4, then=Concat(F('year')-1, Value('-'), F('year'))),
                When(invoice_date__month__gte=4, then=Concat(F('year'), Value('-'), F('year') + 1)),
                output_field=CharField()
            )
            ).values(
                'customer_name',
                'customer_type',
                'transaction_type',
                'invoice_identifier',
                'cn_against_invoice',
                'transaction_date',
                'total_recvd_or_receivable',
                'service_start',
                'service_end',
                'source_state',
                'allocated_bank_credit',
                'tds_allocation',
                'cn_allocation',
                'balance_amount',
                'transaction_status',
                'collection_id',
                'credit_days',
                'due_by',
                'ageing_in_days',
                'ageing_bucket',
                'sales_spoc',
                'credit_amount',
                'financial_year',
                )

        return queryset

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context=extra_context)
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        data = request.GET.dict()
        cust_invoice_id = data.get('cust_invoice_id')
        financial_year = data.get('financial_year')
        customer_id = request.GET.dict().get('customer', None)
        bh_source_state = request.GET.dict().get('bh_source_state__id__exact', None)
        invoice_number = request.GET.dict().get('q', None)

        if not customer_id and not bh_source_state and not invoice_number and not cust_invoice_id and not financial_year:
            response.context_data['invoice_list'] = qs.none()
            return response
        # Consider invoices from APPROVED status
        qs = CollectionsSummary._default_manager.filter(
            status__in=[APPROVED, SENT_TO_CUSTOMER, CUSTOMER_ACKNOWLEDGED,
                        CUSTOMER_CONFIRMED, PARTIALLY_PAID, PAYMENT_DONE])

        if customer_id:
            qs = qs.filter(customer_id=customer_id)

        if bh_source_state:
            qs = qs.filter(bh_source_state=bh_source_state)
        if invoice_number:
            qs = qs.filter(invoice_number=invoice_number)

        qs1 = self.get_invoice_data(qs)
        qs2 = CustomerInvoice.objects.none()

        if not (invoice_number or customer_id):
            qs2 = self.get_collections_data(None)
        elif not invoice_number and customer_id:
            qs2 = self.get_collections_data(customer_id)

        if financial_year:
            qs1 = qs1.filter(financial_year=financial_year)
            qs2 = qs2.filter(financial_year=financial_year)

        response.context_data['invoice_list'] = qs1.union(qs2). \
            order_by('customer_name', 'collection_id', 'transaction_date')

        return response


@admin.register(InvoiceSummary)
class InvoiceSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/customer/invoice/change_list.html'
    date_hierarchy = 'service_period_end'
    list_filter = ('bh_source_state',)
    show_full_result_count = False
    actions_selection_counter = False

    def has_add_permission(self, request):
        return False

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context=extra_context)
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        data = request.GET.dict()
        lookup_params = convert_date_hierarchy_params('service_period_end',
                                                      data)
        if lookup_params:
            qs = InvoiceSummary._default_manager.filter(**lookup_params)
        else:
            qs = InvoiceSummary._default_manager.all()

        qs = qs.values('status').annotate(total=Count('id')).order_by('status')
        response.context_data['results'] = list(qs)
        response.context_data['statuses'] = INVOICE_STATUSES_FOR_SUMMARY
        response.context_data['legend_info'] = INVOICE_SUMMARY_LEGEND
        return response

    def get_queryset(self, request):
        return super().get_queryset(request)


def export_customer_ledger(modeladmin, request, queryset):
    file_path = 'customer_ledger'

    customer_ledger_ids = queryset.values_list('id', flat=True)

    cust_ledger = CustomerLedger.copy_data.filter(
        id__in=customer_ledger_ids)

    cust_ledger = cust_ledger.select_related(
        'customer').to_csv(
        file_path,
        'record_number', 'customer__name', 'customer__legalname',
        'transaction_date', 'description', 'debit', 'credit', 'balance')

    modified_file = os.path.splitext(file_path)[0] + '_' + \
        str(format_datetime(datetime.now(), settings.ADMIN_DATE_FORMAT)) + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'Record Number', 'Customer Name', 'Customer Legal Name',
            'Transaction Date', 'Description', 'Debit', 'Credit',
            'Balance'
        ])

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:

            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + \
                os.path.basename(modified_file)
            os.remove(modified_file)
            return response


@admin.register(CustomerLedger)
class CustomerLedgerAdmin(admin.ModelAdmin):
    list_display = (
        'record_number', 'customer', 'description',
        'transaction_date', 'debit', 'credit', 'balance'
    )

    list_display_links = None

    search_fields = ('customer__name', 'customer__legalname', 'description', )
    actions = [export_customer_ledger]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('customer')
        # qs = qs.order_by('customer_id', '-record_number')
        return qs

    def get_actions(self, request):
        actions = super(CustomerLedgerAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(InvoiceExpirationBatch)
class InvoiceExpirationBatchAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_time', 'description',
                    'number_of_entries', 'end_time')

    fields = ('start_time', 'description', 'number_of_entries', 'end_time')

    readonly_fields = ('start_time', 'description',
                       'number_of_entries', 'end_time')

    search_fields = ['id']
    list_filter = [('start_time', DateRangeFilter)]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    actions = None


@admin.register(InvoiceHistory)
class InvoiceHistoryAdmin(admin.ModelAdmin):
    list_display = ['invoice', 'old_status', 'new_status', 'created_by',
                    'action_owner', 'created_time', 'due_date', 'reason']


@admin.register(ManualUploadReason)
class ManualUploadReasonAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'is_active')
    list_filter = ['is_active',]

    search_fields = ('id', 'name')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        finance_user = is_finance_user(None, request.user)
        if finance_user or request.user.is_superuser:
            return True
        return False


@admin.register(CreditDebitReason)
class CreditDebitReasonAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'is_active')
    list_filter = ['is_active']

    search_fields = ('id', 'name')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return True
