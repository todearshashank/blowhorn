from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from blowhorn.customer.submodels.customer_migration_history import \
    CustomerMigrationHistory
from blowhorn.utils.datetimerangefilter import DateFilter


@admin.register(CustomerMigrationHistory)
class CustomerMigrationHistoryAdmin(admin.ModelAdmin):
    list_display = ('customer', 'number_of_customers', 'category', 'created_by', 'created_date')
    search_fields = ('customer', 'created_by__email', 'created_by__name',)
    list_filter = (('created_date', DateFilter), 'category')
    readonly_fields = ('customer', 'category', 'number_of_customers', 'get_log_dump',)
    exclude = ('log_dump',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_log_dump(self, obj):
        val = obj.log_dump_json
        formatted_text = ''
        for v in val:
            formatted_text += v + '\n'
        return formatted_text

    get_log_dump.short_description = _('Log')
