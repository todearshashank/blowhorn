from django.contrib import admin

from blowhorn.customer.submodels.sku_models import SKU, SKUCustomerMap, \
    ContainerType
from blowhorn.customer.models import Customer, CUSTOMER_CATEGORY_INDIVIDUAL

@admin.register(SKU)
class SKUAdmin(admin.ModelAdmin):
    list_display = ('name', 'include_in_sellshipmentslab')
    search_fields = ('name',)
    list_filter = ('include_in_sellshipmentslab',)

    def get_readonly_fields(self, request, obj=None):
        self.readonly_fields = ['created_date', 'created_by', 'modified_date',
            'modified_by']
        return self.readonly_fields

@admin.register(SKUCustomerMap)
class SKUCustomerMapAdmin(admin.ModelAdmin):
    list_display = ('sku', 'customer', )
    search_fields = ['customer__name']

@admin.register(ContainerType)
class ContainerTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            qs = Customer.objects.exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
            kwargs['queryset'] = qs

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
