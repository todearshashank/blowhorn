from django.contrib import admin, messages
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _

from organizations.models import (Organization, OrganizationUser,
                                  OrganizationOwner)

from blowhorn.common.admin import BaseAdmin
from blowhorn.customer.models import AccountUser, AccountOwner, AccountUserGroup, \
    UserModule, UserPermission, PermissionLevel, PermissionLevelStore, System
from blowhorn.customer.subforms.organization import AccountUserForm,\
    AccountUserUpdateForm, AccountUserGroupForm


@admin.register(AccountUserGroup)
class AccountUserGroupAdmin(BaseAdmin):
    list_display = ('name', 'get_organization_name',)
    search_fields = ('organization__name', 'organization__user__email')
    filter_horizontal = ('permissions',)
    form = AccountUserGroupForm

    def get_organization_name(self, obj):
        return obj.organization.name

    get_organization_name.short_description = _('Customer')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('organization')

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'permissions':
            qs = kwargs.get('queryset', db_field.remote_field.model.objects)
            kwargs['queryset'] = qs.select_related('module')
        return super().formfield_for_manytomany(db_field, request=request, **kwargs)


@admin.register(UserModule)
class UserModuleAdmin(BaseAdmin):
    list_display = ('name', 'code', 'system', 'display_order')
    search_fields = ('name', 'code')
    readonly_fields = ('code',)
    list_filter = (('system', admin.RelatedOnlyFieldListFilter),)

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (
                (None, {
                    'fields': ('name', 'code', 'system', 'display_order')
                }),
            )
        return (
            (None, {
                'fields': ('name', 'system', 'display_order')
            }),
        )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('system')


@admin.register(UserPermission)
class UserPermissionAdmin(BaseAdmin):
    list_display = ('name', 'codename', 'get_module_name', 'url')
    list_filter = (('module', admin.RelatedOnlyFieldListFilter),)
    search_fields = ('module__name', 'name', 'codename')
    readonly_fields = ('codename',)

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (
                (None, {
                    'fields': ('name', 'codename', 'module', 'url')
                }),
            )
        return (
            (None, {
                'fields': ('name', 'module', 'url')
            }),
        )

    def get_module_name(self, obj):
        return obj.module.name

    get_module_name.short_description = _('Module')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('module')


@admin.register(PermissionLevel)
class PermissionLevelAdmin(BaseAdmin):
    list_display = ('name', 'model_name', 'position', 'get_system')
    search_fields = ('name', 'model_name')
    list_filter = (('system', admin.RelatedOnlyFieldListFilter),)

    def get_system(self, obj):
        return obj.system.name

    get_system.short_description = _('System')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('system')


@admin.register(PermissionLevelStore)
class PermissionLevelStoreAdmin(BaseAdmin):
    list_display = ('get_permission_level',)
    search_fields = ('permission_level__name',)

    def get_permission_level(self, obj):
        return '%s' % obj.permission_level

    get_permission_level.short_description = _('Permission Level')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('permission_level')

class OwnerInline(admin.StackedInline):
    model = AccountOwner
    extra = 0


@admin.register(AccountUser)
class OrganizationUserAdmin(BaseAdmin):
    search_fields = ('user__name', 'user__email', 'organization__name',
        'user__phone_number')
    list_display = ['user', 'organization', 'is_admin', 'status']
    list_select_related = ['user', 'organization']
    filter_horizontal = ('user_permissions', 'groups')

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (('General', {
                    'fields': ('organization', 'name', 'email',
                               'phone_number', ('is_admin', 'is_active'))
                    }),
                    ('Permissions', {
                        'fields': ('groups', 'user_permissions', 'permission_levels')
                }),
            )
        return (('General', {
                'fields': ('organization', 'name', 'email', 'phone_number')
            }),
        )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('user', 'organization')

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = AccountUserUpdateForm
        else:
            self.form = AccountUserForm

        return super(OrganizationUserAdmin, self).get_form(request, obj,
                                                           **kwargs)


@admin.register(AccountOwner)
class OrganizationOwnerAdmin(BaseAdmin):
    search_fields = ('organization_user__name', 'organization_user__email',
                     'organization__name',)
    list_display = ['organization', 'organization_user']
    list_filter = (('organization_user', admin.RelatedOnlyFieldListFilter),)
    list_select_related = ['organization_user', 'organization']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('organization_user', 'organization')


admin.site.register(System)
admin.site.unregister(Organization)
admin.site.unregister(OrganizationUser)
admin.site.unregister(OrganizationOwner)
