from blowhorn.oscar.core.loading import get_model
from import_export import resources, fields
from import_export.widgets import ForeignKeyWidget
from django.db import connection
from django.contrib.admin import AdminSite

from blowhorn.company.models import CompanyBankAccount
from blowhorn.customer.models import CustomerAddress, CustomerReceivable

Contract = get_model('contract', 'Contract')
Customer = get_model('customer', 'Customer')
CustomerInvoice = get_model('customer', 'CustomerInvoice')

class BusinessCustomerResource(resources.ModelResource):


    head_office_address = fields.Field(attribute="head_office_address",
                                column_name="head_office_address",
                                widget=ForeignKeyWidget(CustomerAddress,
                                                        'line1'))

    mobile = fields.Field(column_name="Mobile Number")

    gstin = fields.Field(column_name="GSTIN")

    gst_state = fields.Field(column_name="GST State")

    contract = fields.Field(column_name="Contract")

    customer_division = fields.Field(column_name="Customer Division")

    brand_name = fields.Field(column_name="Name / Brand")

    def dehydrate_is_tax_registered(self, customer):
        if customer.is_tax_registered == 1:
            return 'Yes'
        return 'No'


    def dehydrate_mobile(self, customer):
        if customer.user:
            return customer.user.phone_number
        return ''

    def dehydrate_gstin(self, customer):
        gst_info = customer.customertaxinformation_set.all()
        gstin = [info.gstin for info in gst_info ]
        return ', '.join(map(str, set(gstin)))

    def dehydrate_gst_state(self, customer):
        gst_info = customer.customertaxinformation_set.all()
        state = [info.state for info in gst_info ]
        return ', '.join(map(str, set(state)))

    def dehydrate_contract(self, customer):
        contract = customer.contract_set.all()
        contract = [c.name for c in contract ]
        return ', '.join(map(str, set(contract)))

    def dehydrate_customer_division(self, customer):
        division = customer.customerdivision_set.all()
        division = [d.name for d in division ]
        return ', '.join(map(str, set(division)))

    def dehydrate_brand_name(self, customer):
        if customer.user:
            return customer.user.name
        return ''


    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    class Meta:
        model = Customer
        export_order = (
            'id', 'brand_name', 'legalname', 'customer_category',
            'head_office_address', 'mobile', 'contract', 'gstin', 'gst_state',
            'pan', 'tan', 'is_tax_registered', 'entity_type', 'industry_segment',
            'credit_period', 'credit_amount'
        )
        exclude = ('current_address', 'customer_rating', 'cities',
                    'user', 'api_key', 'is_shipment_servicetype',
                    'expected_delivery_hours', 'cin', 'ecode',
                    'advance_taken', 'notification_url',
                    'name',)


class ReceivableResource(resources.ModelResource):
    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    customer = fields.Field(attribute="customer",
                                       column_name="customer",
                                       widget=ForeignKeyWidget(Customer,
                                                               'name'))

    bank_account = fields.Field(attribute="bank_account",
                            column_name="Bank Account",
                            widget=ForeignKeyWidget(CompanyBankAccount,
                                                    'account_number'))

    account_name = fields.Field(attribute="bank_account",
                                column_name="Account Name",
                                widget=ForeignKeyWidget(CompanyBankAccount,
                                                        'account_name'))
    nick_name = fields.Field(attribute="bank_account",
                                column_name="Account Nick Name",
                                widget=ForeignKeyWidget(CompanyBankAccount,
                                                        'nick_name'))

    class Meta:
        model = CustomerReceivable
        fields = ('id', 'status', 'transaction_time', 'payment_date',
                  'deposit_date', 'payment_mode', 'amount', 'description',)
        export_order = (
            'id', 'customer', 'amount', 'description', 'account_name',
            'nick_name', 'bank_account', 'status', 'transaction_time',
            'payment_date', 'deposit_date', 'payment_mode',)


class InvoiceResource(resources.ModelResource):
    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    customer = fields.Field(column_name="Customer")

    service_state = fields.Field(column_name="Service State")

    invoice_state = fields.Field(column_name="Invoice State")

    invoice_config = fields.Field(column_name="Invoice Config")

    aggregate_levels = fields.Field(column_name="Aggregrate Levels")

    tax_amount = fields.Field(column_name="Tax Amount")

    is_stale = fields.Field(column_name="Is Stale")

    adjustment_amount = fields.Field(column_name="Adjustment Amount")

    def dehydrate_customer(self, invoice):
        return str(invoice.customer)

    def dehydrate_is_stale(self, invoice):
        return invoice.is_stale

    def dehydrate_service_state(self, invoice):
        if invoice.bh_source_state:
            return str(invoice.bh_source_state.name)
        return ''

    def dehydrate_invoice_state(self, invoice):
        if invoice.invoice_state:
            return str(invoice.invoice_state.name)
        return ''

    def dehydrate_invoice_config(self, invoice):
        return str(invoice.invoice_configs)

    def dehydrate_aggregate_levels(self, invoice):
        from blowhorn.customer.admin import CustomerInvoiceAdmin
        aggregate_levels = CustomerInvoiceAdmin(invoice, admin_site=AdminSite)._get_aggregate_levels(invoice)
        return aggregate_levels

    def dehydrate_tax_amount(self, invoice):
        return invoice.cgst_amount + invoice.sgst_amount + invoice.igst_amount

    def dehydrate_adjustment_amount(self, invoice):
        if invoice.amount_adjustment:
            return invoice.amount_adjustment
        return 0

    class Meta:
        model = CustomerInvoice
        fields = ( 'id', 'invoice_number', 'status', 'invoice_date', 'batch',
                    'aggregate_levels', 'service_period_start', 'service_period_end', 'taxable_amount',
                    'total_rcm_amount', 'total_amount', )

        export_order = ( 'id', 'customer', 'invoice_number', 'status', 'invoice_date', 'service_state',
                         'invoice_state','batch', 'aggregate_levels' ,'invoice_config',
                         'service_period_start', 'service_period_end', 'taxable_amount', 'tax_amount',
                         'total_rcm_amount', 'adjustment_amount', 'total_amount', 'is_stale',)
