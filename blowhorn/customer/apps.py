# from django.conf.urls import url, include
#
# from blowhorn.customer.subviews.customer_assets import CustomerContainerView
# from blowhorn.customer.subviews.ondemand_order import OndemandOrderView, OndemandOrderContractDataView
# from blowhorn.customer.subviews.order_items import OrderItemsView
# from blowhorn.customer.subviews.proof_of_delivery import ProofOfDeliveryView
# from blowhorn.customer.subviews.shipment_slots import ShipmentSlotFilterView, ShipmentSlotView, \
#     ShipmentSlotDriversDetailView, ShipmentSlotAssignDriver
# from blowhorn.customer.subviews.slot_drivers import ShipmentSlotDriverListView
# from blowhorn.customer.views import *
# from blowhorn.customer.subviews.login import SendOTPLoginView
#
#
# urlpatterns = [
#         # Invoice summary partials
#         url(r'^summary/invoice/list/$', invoice_summary_list, name='invoice-summary-list'),
#         url(r'^summary/invoice/(?P<invoice_pk>[0-9]+)/history$', invoice_history, name='invoice-summary-history'),
#
#         url(r'^profile/$', CustomerProfile.as_view(), name='profile'),
#         url(r'^otp/$', Otp.as_view(), name='otp'),
#         url(r'^verifyotp/$', VerifyOtp.as_view(), name='verify-otp'),
#         url(r'^verifyotpbooking/$', VerifyOtpBooking.as_view(), name='verify-otp-booking'),
#         url(r'^location$', SaveUserAddress.as_view(), name='location'),
#         url(r'^customer/login/$', SendOTPLoginView.as_view(), name='customer-otp-login'),
#
#         # My Fleet
#         url(r'^fleet/search/load-choices/$', FleetSearchChoices.as_view(), name='fleet-search-choices'),
#         url(r'^fleet/trips/$', FleetTripList.as_view(), name='fleet-list-trips'),
#         url(r'^fleet/trips/(?P<pk>[0-9]+)/$', FleetTripDetail.as_view(), name='fleet-trip-detail'),
#         url(r'^fleet/search/shipments-choices$', FleetShipmentsSearchChoices.as_view(), name='fleet-search-shipments-choices'),
#         url(r'^fleet/shipments$', FleetShipmentOrderList.as_view(), name='fleet-list-orders'),
#         url(r'^fleet/shipments/order-batches$', FleetOrderBatchList.as_view(), name='fleet-list-order-batches'),
#         url(r'^fleet/drivers/(?P<driver_id>[0-9]+)/status/$', FleetDriverStatus.as_view(), name='fleet-driver-status'),
#         url(r'^fleet/shipments/export$', FleetShipmentExportList.as_view(), name='fleet-export-orders-list'),
#         url(r'^fleet/shipment/slots/drivers$', ShipmentSlotDriverListView.as_view(), name='shipment-slots-available-drivers'),
#         url(r'^fleet/shipment/assets$', CustomerContainerView.as_view(), name='order-containers'),
#         url(r'^fleet/trips/(?P<trip_pk>[0-9]+)/pod$', ProofOfDeliveryView.as_view(), name='stop-documents'),
#         # Organization Management
#         url(r'^fleet/members$', OrganizationMemberList.as_view(), name='org-members'),
#         url(r'^fleet/member/invite$', OrganizationInviteMember.as_view(), name='org-invite-member'),
#
#         # Ondemand vehicle request
#         url(r'^fleet/order/request/$', OndemandOrderView.as_view(), name='ondemand-order-request'),
#         url(r'^fleet/order/request/contracts$', OndemandOrderContractDataView.as_view(), name='ondemand-order-request-contracts'),
#
#         # Shipment Slots
#         url(r'^fleet/slots/load-filter$', ShipmentSlotFilterView.as_view(), name='shipment-slot-filter'),
#         url(r'^fleet/slots$', ShipmentSlotView.as_view(), name='shipment-slots'),
#         url(r'^fleet/slots/(?P<pk>[0-9]+)/drivers$', ShipmentSlotDriversDetailView.as_view(), name='shipment-slots-drivers'),
#         url(r'^fleet/shipment/driver/assign$', ShipmentSlotAssignDriver.as_view(), name='shipment-slots-assign-driver'),
#         url(r'^fleet/shipment/items', OrderItemsView.as_view(), name='order-containers'),
#
#         url(r'^mytrips/trips/$', MyTripsTripList.as_view(), name='mytrips-list-trips'),
#         url(r'^mytrips/trips/export$', MyTripsTripListExport.as_view(), name='mytrips-list-trips-export'),
#
#         url(r'^customers/shipment$', ShipmentCustomerList.as_view(), name='shipment-customer-list'),
#         url(r'^customer/route$', RouteView.as_view(), name='customer-route'),
#         url(r'^customer/orderline/information', OrderLineInfoView.as_view()),
#         url(r'^customer/invoice/autocomplete/email$', ContactAutocompleteView.as_view(), name='contact-autocomplete'),
#
#         url(r'^customer/orderline/information', OrderLineInfoView.as_view(), name='order-line-info-pos'),
#         url(r'^customer/hubdetails', CustomerHubDetails.as_view(), name='customer-hub-details'),
#
#         url(r'^customer/partners/getinvoicedetails/$', GetInvoiceDetails.as_view(), name='get-partner-customerinvoice-details'),
#         url(r'^customer/partners/updateinvoiceflag/$', UpdateInvoiceProcessedFlag.as_view(), name='put-invoice-isprocessed-flag'),
#         url(r'^customer/mask-call', CallMasking.as_view(), name='customer-mask-call'),
#
#         # Reporting
#         url(r'^report/$', ReportView.as_view(), name='get-report-types'),
#         url(r'^customer/report/schedule/$', CustomerReportScheduleView.as_view(), name='get-customer-report-schedule'),
#         url(r'^customer/report/schedule/add/$', CustomerReportScheduleView.as_view(), name='save-customer-report-schedule'),
#         url(r'^customer/report/schedule/delete/(?P<rid>[0-9]+)$', CustomerReportScheduleView.as_view(), name='delete-customer-report-schedule'),
#         url(r'^customer/report/schedule/update/$', CustomerReportScheduleView.as_view(), name='update-customer-report-schedule'),
#         url(r'^customer/', include('blowhorn.customer.api_urls')),
#     ]


from django.apps import AppConfig

class CustomerConfigApp(AppConfig):
    name = 'blowhorn.customer'
    verbose_name = "Customer"

    def ready(self):
        from blowhorn.customer import signals
