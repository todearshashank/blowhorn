from django.conf.urls import url, include
from oscarapi.app import RESTApiApplication

from . import views
from blowhorn.customer.subviews.customer_assets import CustomerContainerView
from blowhorn.customer.subviews.order_items import OrderItemsView
from blowhorn.customer.subviews import organization as org_view
from blowhorn.customer.subviews import proof_of_delivery as pod_view
from blowhorn.customer.subviews import shipment_slots as slots_view
from blowhorn.customer.subviews import ondemand_order as ondemand_order_view
from blowhorn.customer.subviews import slot_drivers as slot_drivers_view


class CustomerApplication(RESTApiApplication):

    def get_urls(self):
        urlpatterns = [
            # End points of invoice summary partials
            url(r'^summary/invoice/list/$', views.invoice_summary_list,
                name='invoice-summary-list'),
            url(r'^summary/invoice/(?P<invoice_pk>[0-9]+)/history$',
                views.invoice_history,
                name='invoice-summary-history'),
            url(r'^customer/invoice/autocomplete/email$',
                views.ContactAutocompleteView.as_view(),
                name='contact-autocomplete'),

            url(r'^profile/$', views.CustomerProfile.as_view(), name='profile'),
            url(r'^otp/$', views.Otp.as_view(), name='otp'),
            url(r'^verifyotp/$', views.VerifyOtp.as_view(), name='verify-otp'),
            url(r'^verifyotpbooking/$',
                views.VerifyOtpBooking.as_view(), name='verify-otp'),
            url(r'^location$', views.SaveUserAddress.as_view(),
                name='location'),

            # New My Fleet Tracking APIs
            url(r'^fleet/search/load-choices/$',
                views.FleetSearchChoices.as_view(),
                name='fleet-search-choices'),
            url(r'^fleet/trips/$', views.FleetTripList.as_view(),
                name='fleet-list-trips'),
            url(r'^fleet/user/permission/$', views.UserPermission.as_view(), name='user-permissions'),
            url(r'^fleet/user/modules/$', views.UserModules.as_view(), name='user-modules'),
            url(r'^mytrips/trips/$', views.MyTripsTripList.as_view(),
                name='mytrips-list-trips'),
            url(r'^mytrips/trips/export$',
                views.MyTripsTripListExport.as_view(),
                name='mytrips-list-trips'),
            url(r'^fleet/trips/(?P<pk>[0-9]+)/$',
                views.FleetTripDetail.as_view(), name='fleet-trip-detail'),
            url(r'^fleet/search/shipments-choices$',
                views.FleetShipmentsSearchChoices.as_view(),
                name='fleet-search-shipments-choices'),
            url(r'^fleet/shipments$', views.FleetShipmentOrderList.as_view(),
                name='fleet-list-orders'),
            url(r'^fleet/shipments/order-batches$',
                views.FleetOrderBatchList.as_view(),
                name='fleet-list-order-batches'),
            url(r'^fleet/drivers/(?P<driver_id>[0-9]+)/status/$',
                views.FleetDriverStatus.as_view(),
                name='fleet-driver-status'),
            url(r'^fleet/shipments/export$',
                views.FleetShipmentExportList.as_view(),
                name='fleet-export-orders-list'),
            url(r'^fleet/shipment/slots/drivers$',
                slot_drivers_view.ShipmentSlotDriverListView.as_view(),
                name='shipment-slots-available-drivers'),
            url(r'^customers/shipment$', views.ShipmentCustomerList.as_view(),
                name='shipment-customer-list'),
            url(r'^customer/route$', views.RouteView.as_view(),
                name='customer-route'),

            # Organization Management
            url(r'^fleet/members$', org_view.OrganizationMemberList.as_view(),
                name='org-members'),
            url(r'^fleet/member/invite$',
                org_view.OrganizationInviteMember.as_view(),
                name='org-invite-member'),
            url(r'^customer/orderline/information', views.OrderLineInfoView.as_view()),

            url(r'^fleet/shipment/assets$',
                CustomerContainerView.as_view(),
                name='order-containers'),

            url(r'^fleet/trips/(?P<trip_pk>[0-9]+)/pod$',
                pod_view.ProofOfDeliveryView.as_view(),
                name='stop-documents'),

            # Ondemand vehicle request
            url(r'^fleet/order/request/$',
                ondemand_order_view.OndemandOrderView.as_view(),
                name='ondemand-order-request'),
            url(r'^fleet/order/request/contracts$',
                ondemand_order_view.OndemandOrderContractDataView.as_view(),
                name='ondemand-order-request-contracts'),

            # Shipment Slots
            url(r'^fleet/slots/load-filter$', slots_view.ShipmentSlotFilterView.as_view(),
                name='shipment-slot-filter'),
            url(r'^fleet/slots$', slots_view.ShipmentSlotView.as_view(),
                name='shipment-slots'),
            url(r'^fleet/slots/(?P<pk>[0-9]+)/drivers$', slots_view.ShipmentSlotDriversDetailView.as_view(),
                name='shipment-slots-drivers'),
            url(r'^fleet/shipment/driver/assign$', slots_view.ShipmentSlotAssignDriver.as_view(),
                name='shipment-slots-assign-driver'),
            url(r'^fleet/shipment/items', OrderItemsView.as_view(),
                name='order-containers'),
            url(r'^customer/orderline/information', views.OrderLineInfoView.as_view(),
                name='order-line-info-pos'),
            url(r'^customer/hubdetails', views.CustomerHubDetails.as_view(),
                name='customer-hub-details'),

            url(r'^customer/', include('blowhorn.customer.api_urls')),
            url(r'^customer/partners/getinvoicedetails/$',
                views.GetInvoiceDetails.as_view(), name='get-partner-customerinvoice-details'),
            url(r'^customer/partners/updateinvoiceflag/$',
                views.UpdateInvoiceProcessedFlag.as_view(), name='put-invoice-isprocessed-flag'),
            url(r'^customer/mask-call', views.CallMasking.as_view(),
                name='customer-mask-call'),

            #Reporting urls
            url(r'^customer/report/schedule/$',
                views.CustomerReportScheduleView.as_view(), name='get-customer-report-schedule'),
            url(r'^customer/report/schedule/add/$',
                views.CustomerReportScheduleView.as_view(), name='save-customer-report-schedule'),
            url(r'^customer/report/schedule/delete/(?P<rid>[0-9]+)$',
                views.CustomerReportScheduleView.as_view(), name='delete-customer-report-schedule'),
            url(r'^customer/report/schedule/update/$',
                views.CustomerReportScheduleView.as_view(), name='update-customer-report-schedule'),
            url(r'^report/$',
                views.ReportView.as_view(), name='get-report-types'),

        ]
        return self.post_process_urls(urlpatterns)


application = CustomerApplication()
