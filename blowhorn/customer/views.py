import os
import re
import json
import logging
import operator
import pytz
import requests
import phonenumbers
import recurrence

import base64
from decimal import Decimal
from datetime import datetime, timedelta
from django.utils.timezone import make_aware
from functools import reduce
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db import transaction, IntegrityError
from django.db.models import Q, Prefetch
from django.db.models.functions import Concat
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q, Prefetch, F, \
    Value as V, CharField
from django.contrib.auth import update_session_auth_hash

from phonenumber_field.phonenumber import PhoneNumber
from rest_condition import Or
from rest_framework import status, generics, filters
from rest_framework.authentication import SessionAuthentication,\
    BasicAuthentication
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend

from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.oscar.core.loading import  get_model
from blowhorn.common.utils import TimeUtils
from blowhorn.address.mixins import AddressMixin
from blowhorn.address.models import UserAddress, City
from blowhorn.common.helper import CommonHelper
from blowhorn.order.utils import OrderUtil
from blowhorn.common.pagination import StandardResultsSetPagination, \
    TripPagination, ShipmentSetPagination
from blowhorn.common.serializers import BaseSerializer
from blowhorn.common.communication import Communication
from blowhorn.customer.constants import VERIFY_MOBILE, PROFILE_UPDATE_SUCCESS, \
    PROFILE_UPDATE_FAILED, \
    MYFLEET_TRIPS_DATA_TO_BE_EXPORTED, EXPORT_TYPE_POD, \
    EXPORT_TYPE_MINIMAL, EXPORT_TYPE_WITH_EVENTS, \
    EXPORT_TYPE_ORDERLINE, ORGANIZATION_MESSAGES, \
    SHIPMENT_ORDER_MINIMAL_EXPORT, EVENT_COLUMNS_EXPORT, SHIPMENT_ORDERLINE_EXPORT, \
    MZASIGO_ALLOWED_STATUS_FOR_INVOICE_GENERATION, SUCCESS_STATUS_CODES
from blowhorn.customer.models import Route, CustomerTaxInformation, \
    CUSTOMER_CATEGORY_NON_ENTERPRISE, CUSTOMER_CATEGORY_INDIVIDUAL, \
    AccountUser, CustomerInvoice, InvoiceHistory, \
    ShipmentAlertConfiguration, UserModule
from blowhorn.customer.permissions import IsNonIndividualCustomer, IsCustomer, \
    IsOrgAdmin, IsOrgOwner, ReadonlyAccess
from blowhorn.customer.serializers import (
    CustomerProfileSerializer, CustomerOtpSerializer,
    CustomerTaxInformationSerializer, RouteSerializer,
    FleetSearchFormCitySerializer, FleetSearchFormVehicleTypeSerializer,
    FleetSearchFormDriverSerializer, FleetTripSerializer,
    FleetTripListSerializer, FleetShipmentOrderListSerializer,
    FleetShipmentsSearchFormDriverSerializer, FleetSearchFormStateSerializer,
    FleetOrderBatchSerializer, ShipmentCustomerSerializer,
    ShipmentAlertConfigSerializer, CustomerFailedOrderSerializer)
from blowhorn.customer.subserializer.permissions import AccountUserSerializer
from blowhorn.customer.utils import MyfleetHelper, \
    get_vehicle_class_availability, send_bank_details_alert, get_account_number, \
    send_password_alert
from blowhorn.driver.models import Driver
from blowhorn.trip.permissions import IsAllowedToRead as HasTripReadPermissions
from blowhorn.users.serializers import UserAddressSerializer
from blowhorn.utils.functions import ist_to_utc
from blowhorn.vehicle.models import VehicleClass
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, GTA, CONTRACT_TYPE_C2C, \
    CONTRACT_TYPE_SME, CONTRACT_TYPE_SHIPMENT, CONTRACT_STATUS_ACTIVE, \
    CONTRACT_TYPES
from blowhorn.contract.utils import ContractUtils
from blowhorn.address.helpers.location_helper import LocationHelper
from blowhorn.common.utils import export_to_spreadsheet, \
    export_lists_to_spreadsheet
from blowhorn.order.mixins import OrderPlacementMixin
from blowhorn.order.models import OrderConstants, OrderLine, OrderApiData
from blowhorn.order.tasks import notify
from blowhorn.address.utils import get_city_from_latlong, get_city_from_geopoint
from blowhorn.customer.serializers import CustomerShippingAddressSerializer, \
    WaypointSerializer
from blowhorn.customer.constants import ALLOWED_TIME_FORMATS, NUM_DAYS_FUTURE, \
    CUSTOMER_APP_TIME_FORMAT
from blowhorn.contract.models import Contract
from blowhorn.order.serializers.OrderSerializer import OrderSerializer
from blowhorn.order.services.shipment_export import ShipmentExport
from blowhorn.driver.constants import DRIVER_ACTIVITY_IDLE, \
    AVAILABLE_DRIVER_SEARCH_RADIUS_IN_METERS
from blowhorn.driver.serializers import AvailableDriverSerializer
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.serializers import OrganizationMemberSerializer
from blowhorn.customer.submodels.invoice_models import InvoiceSummary
from blowhorn.customer.constants import CREATED, ADDED, DELETED, ACCOUNT_ADDED, \
    ACCOUNT_DELETED, ACTIVATED, DEACTIVATED
from blowhorn.utils.functions import convert_date_hierarchy_params
from blowhorn.apps.driver_app.v3.serializers.address import HubSerializer
from blowhorn.order.models import CallMaskingLog
from blowhorn.shopify.tasks import sync_customer_orders
from blowhorn.logapp.models import SmsLog
from blowhorn.users.utils import validate_customer_password
from blowhorn.report.serializers import CustomerReportScheduleListSerializer


User = get_model('users', 'User')
Customer = get_model('customer', 'Customer')
PartnerCustomer = get_model('customer', 'PartnerCustomer')
Partner = get_model('customer', 'Partner')
CustomerReportSchedule = get_model('customer', 'CustomerReportSchedule')
Report = get_model('customer', 'Report')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
OrderBatch = get_model('order', 'OrderBatch')
WayPoint = get_model('order', 'WayPoint')
Stop = get_model('trip', 'Stop')
Hub = get_model('address', 'Hub')
DriverActivity = get_model('driver', 'DriverActivity')
DriverRatingDetails = get_model('driver', 'DriverRatingDetails')
State = get_model('address', 'State')
CustomerBankDetails = get_model('customer', 'CustomerBankDetails')
CustomerBankDetailsHistory = get_model('customer', 'CustomerBankDetailsHistory')
EzetapPayment = get_model('payment', 'EzetapPayment')

BUSINESS_FLOWS_FILE = 'blowhorn/contract/business_flow.json'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def get_customer_from_api_key(api_key=None):
    if not api_key:
        return False

    customer = Customer._default_manager.filter(api_key=api_key).first()
    if not customer:
        return False
    return customer

class ChannelKey(APIView, CustomerMixin):

    def get(self, request):
        result = {
            'key' : 'Pending',
            'is_active' : False
        }

        customer = self.get_customer(request.user)
        if customer:
            parm = {'contract_type__in' : [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]}
            result['key'] = customer.api_key
            result['is_active'] = ContractUtils.get_active_customer_contract(customer, **parm)

        return Response(status=status.HTTP_200_OK, data=result)

class ChannelSyncOrder(APIView, CustomerMixin):

    def get(self, request):
        customer = self.get_customer(request.user)
        if customer.api_key:
            sync_customer_orders.apply_async(args=[customer.id, ''])
        return Response(status=status.HTTP_200_OK)

class ChannelIntegration(APIView, CustomerMixin):

    def post(self, request):
        data = request.data
        customer = self.get_customer(request.user)
        partnername = data.get('partner')
        partner = Partner.objects.filter(legalname=partnername).first()
        pcs = PartnerCustomer.objects.filter(customer=customer, partner=partner,
                is_deleted=False)

        if partnername == 'Shopify':
            if not pcs:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Please install the blowhorn app from shopify store')

            pc = pcs.latest('date_created')
            if pc.is_active:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='%s service subscription is already active' % (partnername))
            else:
                pc.is_active = True
                pc.save()
                PartnerCustomer.objects.filter(customer=customer,
                    partner=partner).exclude(id=pc.id).update(is_deleted=True, is_active=False)

        elif partnername == 'Unicommerce':
            if not pcs:
                PartnerCustomer.objects.create(customer=customer, partner=partner, is_active=True)
            else:
                pc = pcs.latest('date_created')
                if pc.is_active:
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='%s service subscription is already active' % (partnername))
                else:
                    pc.is_active = True
                    pc.save()

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Service Subscription request is invalid')

        return Response(status=status.HTTP_200_OK, data='Service subscription activated successfully')

class CustomerReportScheduleView(APIView, CustomerMixin):

    def post(self, request):
        data = request.data
        customer = self.get_customer(request.user)
        period = data.get('period')
        weekday = data.get('weekday')
        report_pk = data.get('report_type')
        city_id = data.get('city', None)
        aggregator = data.get('aggregator','Daily')
        weekday_list = ('yearly', 'monthly', 'weekly', 'daily')

        if period not in weekday_list:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid schedule period')

        if weekday and weekday not in range(0,7):
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid weekday')

        if period == 'daily':
            rule = recurrence.Rule(recurrence.DAILY)
        else:
            rule = recurrence.Rule(recurrence.WEEKLY,byday=[recurrence.to_weekday(weekday)])

        pattern = recurrence.Recurrence(dtstart=datetime.now(), rrules=[rule,])
        report_type = Report.objects.get(pk=report_pk)
        city = None
        if city_id:
            city = City.objects.filter(id=city_id).first()

        crs = CustomerReportSchedule.objects.filter(
            customer=customer,
            report_type=report_type,
            city=city
        ).first()
        if crs:
            crs.service_schedule = pattern
            if city:
                crs.city = city

        else:
            parm = {
                'customer' : customer,
                'report_type' : report_type,
                'service_schedule' : pattern,
                'period' : aggregator
            }
            if city:
                parm['city'] = city
            crs = CustomerReportSchedule(**parm)

        crs.save()
        data = CustomerReportScheduleListSerializer(crs).data
        return Response(status=status.HTTP_200_OK, data=data)

    def put(self, request):
        data = request.data
        user_list = data.get('user_list',[])
        schedule_id = data.get('schedule_id')

        crs = CustomerReportSchedule.objects.filter(id=schedule_id).first()
        if crs:
            if user_list:
                account_users = AccountUser.objects.filter(id__in=user_list)
                crs.subscribed_users.set(account_users)
            else:
                crs.subscribed_users.clear()
            crs.save()

        return Response(status=status.HTTP_200_OK, data = 'User list successfully updated')


    def delete(self, request, rid=None):
        data = request.query_params
        if not rid:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid report id')

        CustomerReportSchedule.objects.filter(id = rid).delete()
        return Response(status=status.HTTP_200_OK, data = 'Schedule successfully deleted')


class OrderLineInfoView(APIView, CustomerMixin):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def get(self, request):
        """
            API for fetching orderline information from SPAR pos
        """
        reference_number = request.GET.get('reference_number', '')
        customer = self.get_customer(request.user)
        if reference_number:
            bill_date, out_dict = fetch_data_from_pos(reference_number=reference_number)
            if out_dict:
                response = {
                    "data": out_dict
                }
                order = Order.objects.filter(reference_number=reference_number, customer_id=customer.id).first()
                response.update({'order_number':order.number if order else ''})
                return Response(status=status.HTTP_200_OK, data=response)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Not able to Fetch Information from POS')
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Missing Reference Number')

    def post(self, request):
        data = request.data
        reference_number = data.get('reference_number')

        if reference_number:
            response, message = insert_data_into_pos(reference_number)
            if not response:
                return Response(status=status.HTTP_400_BAD_REQUEST, data=message)
            return Response(status=status.HTTP_200_OK, data=message)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Missing Reference Number')


class CustomerHubDetails(generics.RetrieveAPIView, CustomerMixin):
    def get(self, request):
        """
            API for fetching Customer Hub details
        """
        customer = self.get_customer(request.user)
        if customer:
            hubs = Hub.objects.filter(customer=customer)
            if hubs:
                hub_serializer = HubSerializer(hubs, many=True)
                return Response(status=status.HTTP_200_OK, data=hub_serializer.data)

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid Customer')


class RouteView(generics.CreateAPIView, CustomerMixin):
    """
    This class defines the APIs for customer routes.
    """
    queryset = Route.objects.all()
    serializer_class = RouteSerializer

    def delete(self, request):
        """
            API for deleting the selected customer route.
        """
        customer = self.get_customer(request.user)
        id = request.GET.get('id', '')
        if id:
            try:
                route = Route.objects.get(pk=id)
            except Route.DoesNotExist:
                return Response(status=status.HTTP_200_OK,
                            data=_('Route doesn\'t exist'))
            route.delete()
            all_routes = routes = Route.objects.filter(customer=customer)
            route_serializer = RouteSerializer(routes, many=True)
            return Response(status=status.HTTP_200_OK,
                            data=route_serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_("Select the route to be deleted"))

    def get(self, request):
        """
            API for retrieving the saved route information
            for a customer
        """
        user = request.user
        customer = self.get_customer(user)
        route_name = request.GET.get('route_name', '')
        city = request.GET.get('city', None)
        city_names = None
        if not user.is_customer():
            account_user = AccountUser.objects.filter(user=user).only('is_admin').first()
            if account_user and not account_user.is_admin:
                permissions = self.get_module_permissions('routes', user)
                if not permissions:
                    return Response([], status.HTTP_200_OK)

                city_pks = self.get_permission_level_values(permissions, 'city')
                hub_pks = self.get_permission_level_values(permissions, 'hub')
                if hub_pks:
                    city_pks = Hub.objects.filter(pk__in=hub_pks).values('city_id')

                if city_pks:
                    city_names = City.objects.filter(pk__in=city_pks).values_list('name', flat=True)

        if route_name:
            routes = Route.objects.filter(
                customer=customer,
                name=route_name,
                is_deleted=False
            )

        else:
            query = {
                'customer': customer,
                'is_deleted': False
            }
            if city_names:
                query['city__in'] = city_names

            elif city:
                query['city'] = city

            routes = Route.objects.filter(**query)

        serializer = RouteSerializer(routes, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    def post(self, request):
        """
            API for creating the customer route.
        """
        data = request.data
        customer = self.get_customer(request.user)
        data = data.get('route')
        contact_mobile = data.get('contact_mobile', '')
        if 'contact_mobile' in data and len(contact_mobile) > 0 and \
                not contact_mobile.startswith(settings.ACTIVE_COUNTRY_CODE):
            try:
                data['contact_mobile'] = settings.ACTIVE_COUNTRY_CODE + contact_mobile
                # data['contact_mobile'] = PhoneNumber(
                #     settings.ACTIVE_COUNTRY_CODE,
                #     data['contact_mobile'])
            except:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=_('Entered phone number is not valid')
                )
        try:
            data['stops'] = list(
                map(self.get_formatted_stop, data.get('stops')))
        except ValueError as error:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=str(error))

        if Route.objects.filter(customer=customer, name=data.get('name'),
                                city=data.get('city')).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_("Route name exists."))

        route_serializer = RouteSerializer(data=data)
        if route_serializer.is_valid():
            route_serializer.save(customer=customer)
            all_routes = routes = Route.objects.filter(customer=customer)
            route_serializer = RouteSerializer(routes, many=True)
            return Response(status=status.HTTP_200_OK,
                            data=route_serializer.data)
        else:
            errors = []
            for key in route_serializer.errors:
                if key in ['contact_mobile', 'contact_name']:
                    for err in route_serializer.errors[key]:
                        errors.append(str(err))

            if route_serializer.errors.get('stops'):
                for errMap in route_serializer.errors.get('stops'):
                    for key in errMap:
                        for err in errMap[key]:
                            errors.append(str(err))
            error_msg = "\n".join(set(errors))
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_(error_msg))

    def put(self, request):
        """
            API for editing the customer route.
        """

        ##
        # permission class has to be added later to give access only to customers
        ##
        data = request.data
        customer = self.get_customer(request.user)
        data = data.get('route')
        contact_mobile = data.get('contact_mobile', '')
        if contact_mobile and not contact_mobile.startswith(
            settings.ACTIVE_COUNTRY_CODE):
            try:
                data['contact_mobile'] = settings.ACTIVE_COUNTRY_CODE + contact_mobile
                # data['contact_mobile'] = ph(
                #     settings.ACTIVE_COUNTRY_CODE, contact_mobile)
            except ValueError as error:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=_('Entered phone number is not valid')
                )
        try:
            data['stops'] = list(
                map(self.get_formatted_stop, data.get('stops')))
        except ValueError as error:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=str(error))
        if Route.objects.filter(customer=customer, name=data.get('name'),
                                city=data.get('city')).exists():
            routes = Route.objects.filter(customer=customer, name=data.get('name'))
            if routes[0].id != data.get('id'):
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_("Route name exists."))
        routes = Route.objects.filter(customer=customer, id=data.get('id'))
        route_serializer = RouteSerializer(routes[0], data=data)
        if route_serializer.is_valid():
            route_serializer.save(customer=customer)
            all_routes = routes = Route.objects.filter(customer=customer)
            route_serializer = RouteSerializer(routes, many=True)
            return Response(status=status.HTTP_200_OK,
                            data=route_serializer.data)
        else:
            errors = []
            for key in route_serializer.errors:
                if key in ['contact_mobile', 'contact_name']:
                    for err in route_serializer.errors[key]:
                        errors.append(str(err))

            if route_serializer.errors.get('stops'):
                for errMap in route_serializer.errors.get('stops'):
                    for key in errMap:
                        for err in errMap[key]:
                            errors.append(str(err))
            error_msg = "\n".join(set(errors))
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_(error_msg))

    def get_formatted_stop(self, stop):
        """
            This method converts lat and lng to geopoint
        """
        if not stop.get('country'):
            country_code = settings.COUNTRY_CODE_A2
            stop['country'] = country_code
        stop['geopoint'] = CommonHelper.get_geopoint_from_latlong(
            stop.get('lat'), stop.get('lng'))
        contact_mobile = stop.get('contact_mobile')
        if contact_mobile and not contact_mobile.startswith(settings.ACTIVE_COUNTRY_CODE):
            try:
                parsed_num = phonenumbers.parse(contact_mobile, settings.COUNTRY_CODE_A2)
                stop['contact_mobile'] = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, contact_mobile)
                if not phonenumbers.is_valid_number(stop['contact_mobile']):
                    raise ValueError(
                        _('Phone number entered for stop in %s(%s) is not valid') % (stop['line3'], stop['seq_no']+1))
            except phonenumbers.NumberParseException:
                raise ValueError(
                    _('Phone number entered for stop in %s(%s) is not valid') % (stop['line3'], stop['seq_no']+1))
        del stop['lat']
        del stop['lng']
        return stop


class CustomerProfile(generics.RetrieveUpdateAPIView, CustomerMixin,
                      AddressMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = CustomerProfileSerializer

    def get_user_permissions(self, user_pk):
        user = AccountUser.objects\
            .prefetch_related('user_permissions', 'groups', 'permission_levels')\
            .filter(user_id=user_pk).first()

        context = {
            'fields': ('user_permissions', 'groups', 'permission_levels'),
            'group_fields': ('id', 'name', 'permissions', 'permission_levels'),
            'permissions_fields': ('id', 'name', 'codename', 'module', 'url'),
            'fetch_permission_level': False
        }
        return AccountUserSerializer(user, context=context).data

    def get(self, request):
        """
            Fetch customer profile info
        """
        user = request.user
        myfleet = MyfleetHelper()
        phone_number = user.national_number
        name = user.name if user.name != 'Guest' else None
        legal_name = None
        is_business_user = False
        is_tax_registered = False
        is_org_admin = False
        tax_info = []
        payment_details = {}
        organization_name = None
        wallet_available_balance = 0.0
        customer = self.get_customer(user)
        org_owner_name, org_owner_email = None, None
        show_pod_in_dashboard = False
        permissions = {}
        if customer:
            legal_name = customer.legalname
            is_org_admin = user.is_customer() or customer.is_admin(user)
            tax_info = myfleet.get_customer_tax_information(customer)
            is_business_user = customer.is_non_individual()
            is_tax_registered = customer.is_tax_registered or is_business_user
            payment_details = customer.get_payment_details()
            organization_name = customer.name
            show_pod_in_dashboard = customer.show_pod_in_dashboard
            wallet_available_balance = customer.get_customer_balance()
            org_owner_name, org_owner_email = customer.user.name, customer.user.email
            if not is_org_admin:
                permissions = self.get_user_permissions(user.pk)

        customer_domain = 'customer.' + settings.WEBSITE_BUILD + '.net'
        saved_locations = self.get_saved_user_addresses(user)
        profile = {
            'cid': getattr(customer, 'id', 0),
            'name': name,
            'email': user.email if customer_domain not in user.email else None,
            'legal_name': legal_name,
            'mobile': phone_number,
            'mobile_status': user.is_mobile_verified,
            'email_status': user.is_email_verified,
            'action_type': 'profile-view-flash',
            'saved_locations': saved_locations,
            'recent_locations': saved_locations,
            'b_myfleet_required': is_business_user,
            'is_business_user': is_tax_registered,
            'is_org_admin': is_org_admin,
            'is_staff': user.is_staff,
            'is_org_owner': user.is_customer(),
            'organization_name': organization_name,
            'organization_owner': {
                'name': org_owner_name,
                'email': org_owner_email
            },
            # Sending first info since allowing only single GSTIN from web
            'gstin': tax_info[0].get('gstin', None) if tax_info else None,
            'state': tax_info[0].get('state', None) if tax_info else None,
            'pending_amount': payment_details.get('pending_amount', 0),
            'wallet_available_balance': wallet_available_balance,
            'permissions': permissions,
            'show_pod_in_dashboard': show_pod_in_dashboard
        }
        response = {
            'status': 'PASS',
            'message': profile
        }
        return Response(status=status.HTTP_200_OK, data=response)

    def _update_tax_information(self, customer, name, is_business_user,
                                legal_name, gstin, state):
        """
        Create/update customer tax information
        """
        if is_business_user:
            if not legal_name:
                return False, _('Company name is required for business')

            customer.legalname = legal_name
            customer.is_tax_registered = is_business_user
            customer.customer_category = CUSTOMER_CATEGORY_NON_ENTERPRISE \
                if is_business_user else CUSTOMER_CATEGORY_INDIVIDUAL

            tx_info = \
                CustomerTaxInformation.objects.filter(
                    customer=customer).first()

            if tx_info and not gstin:
                # Customer removed GSTIN from profile
                tx_info.delete()

            elif gstin:
                data = {
                    'customer_id': customer.pk,
                    'gstin': gstin
                }

                if not isinstance(state,type(None)):
                    data['state'] = state

                if tx_info:
                    tax_info = CustomerTaxInformationSerializer(
                        tx_info, data=data)
                else:
                    tax_info = CustomerTaxInformationSerializer(data=data)

                if tax_info.is_valid():
                    try:
                        tax_info.save()
                        customer.service_tax_category = GTA
                    except:
                        return False, _('Error while saving GSTIN')
                else:
                    return False, _('Invalid GSTIN')
        else:
            customer.name = name

        customer.save()
        return True, ''

    def put(self, request, pk=None):
        """
            Update the customer profile
        """
        data = request.data
        user = request.user
        phone_number = None
        name = data.get('name')
        mobile = data.get('mobile')
        email = data.get('email')
        gstin = data.get('gstin', None)
        legal_name = data.get('legal_name', None)
        is_business_user = data.get('is_business_user', False)
        state = data.get('state', False)
        if mobile:
            phone_number = PhoneNumber(
                settings.ACTIVE_COUNTRY_CODE, mobile)
            is_mobile_no_taken = User.objects.filter(phone_number=phone_number, customer__isnull=False).exclude(
                id=user.id).exists()
            if is_mobile_no_taken:
                _out = {
                    'status': 'FAIL',
                    'message': 'Mobile number is associated with another account'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=_out)

        if email:
            is_email_taken = User.objects.filter(email=email).exclude(id=user.id).exists()
            if is_email_taken:
                _out = {
                    'status': 'FAIL',
                    'message': 'Email is associated with another account'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        with transaction.atomic():
            customer = self.get_customer(user)
            if customer:
                if self.has_tax_info_edit_permission(user, customer):
                    success, message = \
                        self._update_tax_information(
                            customer, name, is_business_user, legal_name, gstin, state)

                    if not success:
                        _out = {
                            'status': 'FAIL',
                            'message': message
                        }
                        return Response(status=status.HTTP_400_BAD_REQUEST,
                                        data=_out)

            mobile_num_changed = self.is_mobile_number_changed(
                user, phone_number, mobile)
            try:
                user.name = name
                user.phone_number = phone_number
                user.email = email
                user.save()
                message = VERIFY_MOBILE if mobile_num_changed \
                    else PROFILE_UPDATE_SUCCESS
                response = {
                    'status': 'PASS',
                    'message': _(message)
                }
            except BaseException:
                response = {
                    'status': 'FAIL',
                    'message': _(PROFILE_UPDATE_FAILED)
                }
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=response)
        return Response(status=status.HTTP_200_OK, data=response)


class SaveUserAddress(generics.CreateAPIView, generics.UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserAddressSerializer

    def get(self, request):
        saved_locations = AddressMixin().get_saved_user_addresses(request.user)
        return Response(status=status.HTTP_200_OK, data=saved_locations)

    def _get_address(self, query_params):
        params = reduce(operator.and_, query_params) if query_params else None
        try:
            return UserAddress.objects.get(params)
        except UserAddress.DoesNotExist:
            return False

    def post(self, request):
        data = request.data
        user = request.user
        identifier = data.get('name', '')
        if not identifier:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Please, Provide a name for address.'))

        # Check for the existing address of user
        identifier = identifier.strip()
        context = {
            'user': user,
            'request': data
        }

        serializer = self.serializer_class(data=data, context=context)
        _data = serializer.validate({})
        ua = UserAddress(**_data)
        address_hash = ua.generate_hash()
        address_present = True

        user_address = self._get_address(
            [Q(user=user), Q(identifier=identifier)])
        if user_address:
            # Update address only if address is changed
            if user_address.hash != address_hash:
                serializer = self.serializer_class(user_address, data=data,
                                                   context=context)
            else:
                message = _('Nothing to update')
                logger.info(message)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=message)

        if not user_address:
            # Try to fetch address by hash if first attempt of fetching address
            # by identifier failed
            user_address = self._get_address([Q(user=user), Q(hash=hash)])
            if user_address:
                serializer = self.serializer_class(user_address, data=data,
                                                   context=context)
            else:
                address_present = False

        if not address_present:
            # Create a new user address
            serializer = self.serializer_class(data=data, context=context)

        if serializer.is_valid():
            try:
                serializer.save()
                saved_locations = AddressMixin().get_saved_user_addresses(user)
                logger.info('User address successfully saved: %s' % user)
                return Response(status=status.HTTP_200_OK, data=saved_locations)

            except IntegrityError as ie:
                message = _('Address already exists in your address book')
                logger.info('Integrity error: %s' % ie.args[0])
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=message)

            except BaseException as be:
                message = 'Error: %s' % be.args[0]
                logger.info(message)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=message)

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=serializer.errors)


class Otp(generics.CreateAPIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        phone_number = data.get('mobile')
        name = data.get('name', 'Guest')
        user = request.user
        # Guest user case
        if settings.ACTIVE_COUNTRY_CODE not in str(phone_number):
            phone_number = PhoneNumber(
                settings.ACTIVE_COUNTRY_CODE,
                phone_number
            )
        if user and not user.is_authenticated:
            customer = Customer.objects.filter(user__phone_number=phone_number).first()
            user = customer.user if customer else None
            if not customer:
                email = '%s@%s' % (phone_number,
                                   settings.DEFAULT_DOMAINS.get('customer'))

                if not User.objects.filter(email=email).exists():
                    # Create new user and then send otp
                    with transaction.atomic():
                        user = User.objects.create_user(
                            email=email,
                            phone_number=phone_number,
                            name=name
                        )
                        Customer.objects.create(
                            user=user,
                            name=name,
                            slug=email
                        )

        user.send_otp(str(phone_number))
        return Response(status=status.HTTP_200_OK)


class VerifyOtp(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CustomerOtpSerializer

    def post(self, request):
        """
            Verification of mobile number during login/registration/updating profile.
        """
        context = {
            'request': request,
        }
        user = request.user
        if user and user.is_authenticated:
            context['user'] = user

        serializer = self.serializer_class(
            data=request.data,
            context=context
        )
        if serializer.is_valid():
            status_code, response = serializer.update(
                serializer.validated_data.get('user'),
                serializer.validated_data
            )
            return Response(status=status_code, data=response)

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=_('Something went wrong'))


# class VerifyOtpBooking(generics.CreateAPIView):
#     """
#         Verify OTP when entered by guest user upon
#         placing booking
#     """
#     authentication_classes = ()
#     permission_classes = ()
#     serializer_class = CustomerOtpSerializer
#
#     def post(self, request):
#         request.data['mobile'] = request.data.get('mobile')
#         request.data['guest_booking'] = True
#         serializer = self.serializer_class(
#             data=request.data,
#             context={'request': request}
#         )
#         if serializer.is_valid():
#             status_code, response = serializer.update(
#                 serializer.validated_data.get('user'),
#                 serializer.validated_data
#             )
#             return Response(status=status_code, data=response)
#
#         return Response(status=status.HTTP_400_BAD_REQUEST,
#                         data=_('Something went wrong'))


class VerifyOtpBooking(generics.CreateAPIView, CustomerMixin):
    """
        Verify OTP when entered by guest user upon
        placing booking
    """
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        data = request.data
        mobile = data.get('mobile', None)
        otp = data.get('otp', None)

        if not mobile or not otp:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Mobile number/OTP is required'))

        if mobile:
            phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, mobile)
            customer = Customer.objects.filter(user__phone_number=phone_number).first()
            user = customer.user if customer else None
            if user and not user.is_active and\
                    user.login_attempt > settings.INVALID_OTP_ATTEMPTS_ALLOWED:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='User is blocked, Please contact support')
            if user:
                # customer = self.get_customer(user)
                Customer.objects.add_otp(customer=customer, otp=data.get('otp'),
                                         remarks='OTP entered by customer - Website')
                login_response = User.objects.do_login(
                    request=request, user=user, mobile=mobile, otp=otp)
                if login_response.status_code == status.HTTP_200_OK:
                    user = login_response.data.get('user')

                    if not customer or user.is_staff:
                        return Response(status=status.HTTP_401_UNAUTHORIZED,
                                        data='Mobile number verification failed')

                    response = {
                        'status': 'PASS',
                        'message': 'Your mobile has been verified successfully',
                        'is_staff': user.is_staff,
                        'is_business_user': customer and customer.is_non_individual()
                    }
                    return Response(status=status.HTTP_200_OK, data=response)
                return Response(status=login_response.status_code, data=login_response.data)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=_('Something went wrong'))


# Fleet Tracking Views
class FleetSearchChoices(APIView, CustomerMixin):
    """
    View to list all choices supported in search. (Dropdown choices)

    * Requires authentication.
    * Only non-individual customers are able to access this view.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request):
        """
        Returns list of all search choices.
        """
        user = request.user
        module_name = request.GET.get('module', None)
        customer = self.get_customer(user)
        city_pks = None
        if not user.is_customer():
            account_user = AccountUser.objects.filter(user=user).only('is_admin').first()
            if account_user and not account_user.is_admin:
                permissions = self.get_module_permissions('enterprise_trips', self.request.user)
                if not permissions:
                    return Response({
                        'cities': [],
                        'vehicle_classes': [],
                        'drivers': [],
                        'states': []
                    }, status.HTTP_200_OK)

                city_pks = self.get_permission_level_values(permissions, 'city')

        if not city_pks:
            city_pks = customer.hub_set.values_list('city_id', flat=True)

        operating_cities = City.objects.filter(
            id__in=city_pks
        ).select_related('address').prefetch_related(
            Prefetch(
                "hub_set",
                queryset=customer.hub_set.all(),
                to_attr="hubs"
            )
        )

        states = State.objects.values('id', 'name')
        states_serializer = FleetSearchFormStateSerializer(
            states, many=True
        )
        cities_serializer = FleetSearchFormCitySerializer(
            operating_cities, many=True)

        vehicle_classes = VehicleClass.objects.all()
        vehicle_classes_serializer = FleetSearchFormVehicleTypeSerializer(
            vehicle_classes, many=True)

        trips = Trip.objects.filter(
            Q(
                Q(order__customer=customer) |
                Q(customer_contract__customer=customer)
            ),
            Q(status__in=[
                StatusPipeline.NEW, StatusPipeline.TRIP_IN_PROGRESS,
                StatusPipeline.DRIVER_ACCEPTED,
                StatusPipeline.TRIP_ALL_STOPS_DONE
            ])
        ).select_related('hub')

        driver_ids = trips.values_list('driver_id', flat=True)
        drivers = Driver.objects.filter(id__in=driver_ids).prefetch_related(
            "drivervehiclemap_set").select_related('user')
        driver_serializer = FleetSearchFormDriverSerializer(
            drivers, many=True, context={'trips': trips})

        combined_serialized_data = BaseSerializer.combine(
            False,
            [cities_serializer, 'cities'],
            [vehicle_classes_serializer, 'vehicle_classes'],
            [driver_serializer, 'drivers'],
            [states_serializer, 'states']
        )
        combined_serialized_data['contract_types'] = CONTRACT_TYPES
        return Response(combined_serialized_data, status.HTTP_200_OK)


class MyTripsTripListExport(generics.ListAPIView, CustomerMixin):
    """
    List trips of customer (for given status).

    * Requires authentication.
    * Only customers are able to access this view.
    """
    customer = None
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsCustomer,)

    def get(self, request, *args, **kwargs):
        user = self.request.user
        self.customer = self.get_customer(user)
        response = {}
        qs = list(self.get_queryset())
        order_results_json = []
        for order in qs:
            response_dict = OrderUtil().get_booking_details_for_customer(
                request=self.request,
                order=order
            )
            if response_dict:
                order_results_json.append(response_dict)
        order_list = self.get_order_list(order_results_json)
        customer_info = self.get_customer_info(request.user)
        filename = self.get_filename(self.customer.name)
        file_path = export_lists_to_spreadsheet(
                [customer_info, order_list], filename, [False, True])
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (
                    fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                    os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            response['errors'] = 'No file generated'
            status_code = status.HTTP_400_BAD_REQUEST
        return Response(response, status_code)

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        order_qs = Order._default_manager.filter(query)
        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'customer__user',
            'vehicle_class_preference', 'shipping_address', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )
        return order_qs

    def get_query(self, params):
        _status = params.get('status', None)

        query = [Q(customer=self.customer),
                 Q(order_type=settings.C2C_ORDER_TYPE),
                 ~Q(status=StatusPipeline.ORDER_CANCELLED)]

        if _status:
            status_query = []
            if _status == 'upcoming':
                status_query = Q(status=StatusPipeline.ORDER_NEW)
            elif _status == 'current':
                status_query = Q(status__in=[StatusPipeline.DRIVER_ACCEPTED,
                                            StatusPipeline.DRIVER_ASSIGNED,
                                            StatusPipeline.DRIVER_ARRIVED_AT_PICKUP,
                                            StatusPipeline.OUT_FOR_DELIVERY,
                                            StatusPipeline.DRIVER_ARRIVED_AT_DROPOFF
                                        ]
                )
            elif _status == 'completed':
                status_query = Q(status=StatusPipeline.ORDER_DELIVERED)
            if status_query:
                query.append(status_query)
        return reduce(operator.and_, query)

    def get_filename(self, user_name):
        file = 'my_trips_' + str(user_name) + '_' + \
            str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        return filename

    def get_customer_info(self, user):
        customer_info = []
        customer_info.append(['Customer name', user.name])
        customer_info.append(['Customer email(booked through)', user.email])
        customer_info.append(['Mobile No.', user.national_number])
        return customer_info

    def get_order_list(self, orders):
        order_list = [('Sl No', 'Booking ID', 'Vehicle Type', 'City',
                       'Booked For', 'Pickup Address', 'Dropoff Address',
                       'Distance', 'Total cost to blowhorn',
                       'Tax payable on RCM', 'Payment Mode', 'Payment Status',
                       'Driver Name', 'Vehicle Number')]
        for count, order in enumerate(orders):
            order_list.append((count + 1, order['order_id'],
                               order['vehicle_model'], order['city'],
                               order['date'], order['pickup']['address'],
                               order['dropoff']['address'], '{0:.2f}'.format(
                               order['total_distance']) + ' kms',
                               order['amount_payable'][0]['value'],
                               order['amount_payable'][1]['value'] if len(
                                   order['amount_payable']) > 1 else 0,
                               order['payment_mode'], order['payment_status'],
                               order['driver_name'], order['vehicle_number']))
        return order_list


class MyTripsTripList(generics.ListAPIView, CustomerMixin):
    """
    List trips of customer (for given status).

    * Requires authentication.
    * Only customers are able to access this view.
    """

    customer = None
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsCustomer,)
    pagination_class = TripPagination

    def get(self, request, *args, **kwargs):
        user = self.request.user
        self.customer = self.get_customer(user)
        qs = self.get_queryset()
        return self.get_paginated_response(qs)

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        order_qs = Order._default_manager.filter(query)
        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'customer__user',
            'vehicle_class_preference', 'shipping_address', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        _status = self.request.GET.get('status', None)
        if _status == 'completed':
            order_qs = order_qs.order_by('-payment_status', '-pickup_datetime')
        else:
            order_qs = order_qs.order_by('pickup_datetime')

        return self.paginate_queryset(order_qs)

    def get_query(self, params):
        _status = params.get('status', None)
        query = [
            Q(customer=self.customer),
            Q(order_type=settings.C2C_ORDER_TYPE),
            ~Q(is_deleted=True),
            ~Q(status=StatusPipeline.ORDER_CANCELLED),
        ]

        if _status:
            status_query = []
            if _status == 'upcoming':
                status_query = Q(status=StatusPipeline.ORDER_NEW)
            elif _status == 'current':
                status_query = Q(status__in=[
                    StatusPipeline.DRIVER_ACCEPTED,
                    StatusPipeline.DRIVER_ASSIGNED,
                    StatusPipeline.DRIVER_ARRIVED_AT_PICKUP,
                    StatusPipeline.OUT_FOR_DELIVERY,
                    StatusPipeline.DRIVER_ARRIVED_AT_DROPOFF
                ])
            elif _status == 'completed':
                status_query = Q(status=StatusPipeline.ORDER_DELIVERED)
            if status_query:
                query.append(status_query)
        return reduce(operator.and_, query)

class UserModules(generics.ListAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request):
        modules = UserModule.objects.prefetch_related('submodule_set')
        resp = []
        for mod in modules:
            resp.append({
                "module": mod.module,
                "submodule": [x.title for x in mod.submodule_set.all()]

            })
        return Response(resp, status.HTTP_200_OK)


class FleetTripList(generics.ListAPIView, CustomerMixin):
    """
    List trips of customer (for given status).

    * Requires authentication.
    * Only SME customers are able to access this view.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    customer = None
    serializer_class = FleetTripListSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('planned_start_time', 'id')

    def get(self, request):
        user = request.user
        self.customer = self.get_customer(user)
        self.user = request.user
        output = request.GET.get('output', None)
        response = {}

        if output == 'xlsx':
            self.pagination_class = None
            filename = self.get_filename(self.customer.name)
            file_path = export_to_spreadsheet(
                super().get(request).data,
                filename,
                MYFLEET_TRIPS_DATA_TO_BE_EXPORTED
            )
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (
                        fh, filename))

                    response = HttpResponse(
                        fh.read(),
                        content_type="application/vnd.ms-excel"
                    )
                    response['Content-Disposition'] = 'inline; filename=%s' % os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                response['errors'] = 'No file generated'
                status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)
        else:
            return super().get(request)

    def get_filename(self, user_name):
        file = 'myfleet_trips_' + str(user_name) + '_' + \
            str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        return filename

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        return Trip.objects.filter(query).select_related(
            'driver', 'driver__user', 'vehicle', 'vehicle__vehicle_model',
            'order', 'customer_contract')

    def get_query(self, params):
        _status = params.get('status', None)
        city_id = params.get('city_id', None)
        hub_id = params.get('hub_id', None)
        driver_id = params.get('driver_id', None)
        order_trip_no = params.get('order_trip_no', None)
        vehicle_class = params.get('vehicle_class', None)
        start = params.get('start', None)
        end = params.get('end', None)

        trip_type = params.get('trip_type', None)
        query = [
            Q(order__customer=self.customer) |
            Q(customer_contract__customer=self.customer),
            ~Q(is_deleted=True)
        ]

        quick_filter_date_delta = params.get('quick_filter', None) or 0
        if not start and not end:
            end = timezone.now().date()
            start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))

        status_query = []
        if _status:
            if _status == 'upcoming':
                status_query = Q(status=StatusPipeline.TRIP_NEW)
            elif _status == 'current':
                temp = StatusPipeline.TRIP_END_STATUSES + [StatusPipeline.TRIP_NEW,]
                status_query = ~Q(status__in=temp)
            elif _status == 'completed':
                temp = [StatusPipeline.TRIP_COMPLETED, StatusPipeline.TRIP_SUSPENDED,
                            StatusPipeline.TRIP_SCHEDULED_OFF, StatusPipeline.TRIP_NO_SHOW]
                status_query = Q(status__in=temp)
            if status_query:
                query.append(status_query)

        if city_id:
            query.append(Q(customer_contract__city_id__in=city_id))
        if hub_id:
            query.append(Q(order__hub_id__in=hub_id) | Q(hub_id__in=hub_id))
        if driver_id:
            query.append(Q(driver__id=driver_id))
        if order_trip_no:
            query.append(Q(order__number=order_trip_no)
                         | Q(trip_number=order_trip_no))
        if trip_type:
            query.append(Q(customer_contract__contract_type=trip_type))
        if vehicle_class:
            query.append(
                Q(vehicle__vehicle_model__vehicle_class_id=vehicle_class))
        if start and end:
            query.append(
                Q(planned_start_time__date__range=(start, end))
                | Q(created_date__date__range=(start, end))
            )

        fieldname_map = {
            'city': 'customer_contract__city__id',
            'hub': 'hub',
            'division': 'customer_contract__division_id'
        }
        permission_query = CustomerMixin().get_permission_query(self.request.user, 'enterprise_trips', Trip, fieldname_map=fieldname_map)
        if permission_query:
            query.append(permission_query)

        return reduce(operator.and_, query)


class FleetShipmentsSearchChoices(APIView, CustomerMixin):
    """
    View to list all choices supported in search. (Dropdown choices)

    * Requires authentication.
    * Only SME customers are able to access this view.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def __get_alert_level(self, customer):
        alerts = ShipmentAlertConfiguration.objects.filter(
            customer=customer).order_by('time_since_creation')
        return ShipmentAlertConfigSerializer(alerts, many=True).data

    def get(self, request):
        """
        Returns list of all search choices.
        """
        customer = self.get_customer(request.user)
        query = Q(customer=customer) & Q(
            order_type=settings.SHIPMENT_ORDER_TYPE)

        fieldname_map = {
            'city': 'city',
            'hub': 'hub',
        }
        module_name = request.GET.get('module', None)
        permission_query = CustomerMixin().get_permission_query(
            self.request.user,
            module_name or 'shipment_orders',
            Order,
            fieldname_map=fieldname_map
        )
        if permission_query:
            query &= permission_query

        orders = Order.objects.filter(query)
        operating_cities = City.objects.filter(
            id__in=orders.distinct('city').values_list(
                'city_id', flat=True)
        ).select_related('address').prefetch_related(
            Prefetch(
                "hub_set",
                queryset=Hub.objects.filter(
                    id__in=orders.distinct('hub').values_list(
                        'hub_id', flat=True)),
                to_attr="hubs"
            )
        )

        cities_serializer = FleetSearchFormCitySerializer(
            operating_cities, many=True)
        driver_ids = orders.distinct('driver').values_list(
            'driver_id', flat=True)

        drivers = Driver.objects.filter(
            id__in=driver_ids).select_related('user')
        driver_serializer = FleetShipmentsSearchFormDriverSerializer(
            drivers, many=True, context={'orders': orders})

        combined_serialized_data = BaseSerializer.combine(
            False,
            [cities_serializer, 'cities'],
            [driver_serializer, 'drivers']
        )
        combined_serialized_data["statuses"] = \
            list(StatusPipeline.SHIPMENT_ORDER_STATUSES)
        combined_serialized_data["status_legend"] = \
            StatusPipeline.SHIPMENT_STATUS_MAPPING_LEGEND
        combined_serialized_data["status_colors"] = \
            StatusPipeline.ORDER_STATUS_COLOURS
        combined_serialized_data["alert_config"] = \
            self.__get_alert_level(customer)

        return Response(combined_serialized_data, status.HTTP_200_OK)


class FleetShipmentOrderList(generics.ListAPIView, CustomerMixin):
    """
    List shipment Order of customer

    * Requires authentication.
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer)
    serializer_class = FleetShipmentOrderListSerializer
    pagination_class = ShipmentSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('date_placed',)

    def get_results(self, query, request):
        fieldname_map = {
            'city': 'city',
            'hub': 'hub',
            'division': 'customer_contract__division_id'
        }
        permission_query = CustomerMixin().get_permission_query(request.user, 'shipment_orders', Order, fieldname_map=fieldname_map)
        if permission_query:
            query = query & permission_query

        try:
            order_qs = Order.objects.filter(query)
            order_qs = order_qs.select_related(
                'driver__user', 'shipping_address', 'city', 'hub', 'driver', 'pickup_hub')
            order_qs = order_qs.prefetch_related(
                'documents'
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'waypoint_set',
                    queryset=WayPoint.objects.prefetch_related(
                        Prefetch(
                            'stops',
                            queryset=Stop.objects.all().extra(
                                select={
                                    'end_time_null': 'end_time is null '},
                                order_by=['end_time']),
                            to_attr='stops_list'
                        )
                    ),
                    to_attr='waypoints'
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'stops',
                    queryset=Stop.objects.all().extra(
                        select={
                            'end_time_null': 'end_time is null '},
                        order_by=['end_time']),
                    to_attr='stops_list'
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'event_set', queryset=Event.objects.all(), to_attr='events'
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'order_lines',
                    queryset=OrderLine.objects.all().select_related('sku').order_by('item_sequence_no')
                )
            ).prefetch_related(
                Prefetch(
                    'driverratingdetails_set',
                    queryset=DriverRatingDetails.objects.select_related(
                        'remark').only('rating', 'remark', 'customer_remark')
                )
            )
            return order_qs
        except BaseException as be:
            logger.info('Failed to query orders: %s' % be)
            return Order.objects.none()

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        return self.get_results(query, self.request)

    def get_query(self, params):
        quick_filter_date_delta = params.get('quick_filter', None) or 0
        _status = params.get('status', None)
        city_id = params.get('city_id', None)
        hub_id = params.get('delivery_hub_id', None)
        pickup_hub_id = params.get('pickup_hub_id', None)
        driver_id = params.get('driver_id', None)
        batch_id = params.get('batch_id')
        order_no = params.get('order_no', None)
        order_ref_nos = params.get('order_ref_nos', None)
        start = params.get('start', None)
        end = params.get('end', None)
        reference_number = params.get('reference_number', None)
        customer_reference_number = params.get(
            'customer_reference_number', None)

        if not start and not end:
            IST = pytz.timezone(settings.ACT_TIME_ZONE)
            end = datetime.now(IST).date()
            start = datetime.now(IST).date() - timedelta(
                days=int(quick_filter_date_delta))

        customer = self.get_customer(self.request.user)
        query = [Q(customer=customer),
            Q(order_type=settings.SHIPMENT_ORDER_TYPE),
            ~Q(is_deleted=True),
            ~Q(status=StatusPipeline.DUMMY_STATUS)
        ]
        if _status:
            statuses = _status.split(",")
            status_query = []
            if statuses:
                status_query.append(Q(status__in=statuses))
            query.append(reduce(operator.or_, status_query))
        if reference_number:
            query.append(Q(reference_number=reference_number))
        if customer_reference_number:
            query.append(
                Q(customer_reference_number=customer_reference_number))
        if city_id:
            query.append(Q(city_id=city_id))
        if hub_id:
            query.append(Q(hub_id=hub_id))
        if pickup_hub_id:
            query.append(Q(pickup_hub_id=pickup_hub_id))
        if driver_id:
            query.append(Q(driver__id=driver_id))
        if order_no:
            query.append(Q(number=order_no))
        if batch_id:
            query.append(Q(batch_id=batch_id))
        if start and end:
            query.append(
                (Q(updated_time__date__gte=start)
                & Q(updated_time__date__lte=end))
                | (Q(created_date__date__gte=start)
                & Q(created_date__date__lte=end))
                | (Q(expected_delivery_time__date__gte=start)
                & Q(expected_delivery_time__date__lte=end))
            )
        if order_ref_nos:
            query.append(
                Q(number=order_ref_nos) |
                Q(reference_number=order_ref_nos) |
                Q(customer_reference_number=order_ref_nos)
            )
        return reduce(operator.and_, query)


class FleetDriverStatus(APIView):
    """
    Retrieve trip level status of driver.

    * Requires authentication.
    * Only SME customers are able to access this view.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer,)

    def get(self, request, driver_id):

        driver_ongoing_trip = Trip.active.for_driver(driver_id).last()

        if driver_ongoing_trip:
            order = driver_ongoing_trip.order
            order_number = None
            if order is not None:
                order_number = order.number
            data = {
                "data": {
                    "trip": {
                        "id": driver_ongoing_trip.id,
                        "order_number": order_number,
                        "next_stop": driver_ongoing_trip.get_next_stop()
                    },
                },
                "message": _("success")
            }
        else:
            data = {
                "message": _("No ongoing  trips for driver."),
                "data": None
            }
        return Response(data, status.HTTP_200_OK)


class FleetTripDetail(generics.RetrieveAPIView, CustomerMixin):
    """
    Retrieve trip level detail of driver.

    * Requires authentication.
    * Only SME customers are able to access this view.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (
        IsAuthenticated, IsNonIndividualCustomer, HasTripReadPermissions)
    serializer_class = FleetTripSerializer

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return Trip.objects.filter(
            customer_contract__customer=customer
        ).select_related('order', 'driver', 'vehicle').prefetch_related(
            Prefetch('stops', queryset=Stop.objects.order_by(
                'waypoint__sequence_id'))
        )


class RatePackages(APIView, CustomerMixin):
    """
    Description     : API to get vehicle sell rate packages.
    Request type    : GET
    Params          : (lat, lng) or city
    Response        : Available Vehicle packages for given city / location.
    """

    permission_classes = (AllowAny, )

    def _get_city(self):
        params = self.request.GET
        city = params.get('city', None)
        if city:
            city = City.objects.get(id=city)
        else:
            lat, lng = params.get('lat', None), params.get('lng', None)
            if lat and lng:
                city = LocationHelper.get_city_from_lat_lng(lat, lng)
                # Passing Bangalore as default city if lat-lng didn't match
                if not city:
                    return City.objects.first()

        # Defaulting to all cities if no city was matched.
        if not city:
            return list(City.objects.all())
        return city

    def _get_contract_types(self, customer=None):
        # if customer and customer.is_SME():
        return [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]
        # else:
        #     return [CONTRACT_TYPE_C2C]

    def get(self, request):
        user = request.user
        customer = self.get_customer(user)
        city = self._get_city()
        cities = city if isinstance(city, list) else [city]
        contract_types = self._get_contract_types(customer)
        response = {'data': '', 'errors': ''}
        if city and contract_types:
            response['data'] = ContractUtils.get_sell_packages(
                cities=cities, contract_types=contract_types, customer=customer)
            status_code = status.HTTP_200_OK
        else:
            response['errors'] = _('Insufficient arguments.')
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(response, status_code)


class FleetShipmentExportList(APIView, CustomerMixin):
    """
    List shipment Order of customer

    * Requires authentication.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer)

    def get_query(self, params):
        quick_filter_date_delta = params.get('quick_filter', None) or 0
        _status = params.get('status', None)
        city_id = params.get('city_id', None)
        hub_id = params.get('hub_id', None)
        driver_id = params.get('driver_id', None)
        order_no = params.get('order_no', None)
        order_ref_nos = params.get('order_ref_nos', None)
        start = params.get('start', None)
        end = params.get('end', None)
        reference_number = params.get('reference_number', None)
        customer_reference_number = params.get(
            'customer_reference_number', None)

        if not start and not end:
            end = timezone.now().date()
            start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))

        self.customer = self.get_customer(self.request.user)
        query = [Q(customer=self.customer),
                 Q(order_type=settings.SHIPMENT_ORDER_TYPE)]
        if _status:
            statuses = _status.split(",")
            status_query = []
            if statuses:
                status_query.append(Q(status__in=statuses))
            query.append(reduce(operator.or_, status_query))
        if reference_number:
            query.append(Q(reference_number=reference_number))
        if customer_reference_number:
            query.append(
                Q(customer_reference_number=customer_reference_number))
        if city_id:
            query.append(Q(city_id=city_id))
        if hub_id:
            query.append(Q(hub_id=hub_id))
        if driver_id:
            query.append(Q(driver__id=driver_id))
        if order_no:
            query.append(Q(number=order_no))
        if start and end:
            query.append(
                Q(updated_time__date__gte=start)
                & Q(updated_time__date__lte=end)
                | (Q(created_date__date__gte=start)
                   & Q(created_date__date__lte=end))
                | (Q(expected_delivery_time__date__gte=start)
                   & Q(expected_delivery_time__date__lte=end))
            )
        if order_ref_nos:
            query.append(Q(number=order_ref_nos) |
                         Q(reference_number=order_ref_nos) |
                         Q(customer_reference_number=order_ref_nos))
        return reduce(operator.and_, query)

    def get_filename(self, prefix, user_name):
        _date = datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT)
        return "%s_%s_%s.xlsx" % (prefix, user_name, _date)

    def get_pg_copy_queryset(self, params):
        query = self.get_query(params)
        return Order.copy_data.filter(query)

    def get_order_queryset(self, query_params):
        query = self.get_query(query_params)
        order_qs = Order.copy_data.filter(query)
        order_qs = order_qs.select_related('driver__user')
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.all().extra(
                            select={
                                'end_time_null': 'end_time is null '
                            },
                            order_by=['end_time']
                        ), to_attr='stops_list'
                    )
                ), to_attr='waypoints'
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'stops',
                queryset=Stop.objects.all().extra(
                    select={
                        'end_time_null': 'end_time is null '},
                    order_by=['end_time']),
                to_attr='stops_list'
            )
        )
        return order_qs.prefetch_related(
            Prefetch(
                'event_set', queryset=Event.objects.all(), to_attr='events'
            )
        )

    def get(self, request):
        response = {}
        export_type = self.request.query_params.get('export_type')
        _start = self.request.query_params.get('start', None)
        _end = self.request.query_params.get('end', None)
        _delta = self.request.query_params.get('quick_filter', None) or 0
        _status = None

        if _start and _end:
            start = datetime.strptime(_start,'%Y-%m-%d')
            end = datetime.strptime(_end,'%Y-%m-%d')
            _delta = (end - start).days

        # place export request for time period more than 45 days or
        # event export for more then equal to 1 days
        if (int(_delta) > 45) or (export_type == EXPORT_TYPE_WITH_EVENTS and int(_delta) > 1):
            from blowhorn.customer.tasks import process_export_request
            _customer = self.get_customer(request.user)
            user_email = request.user.email
            _parms = json.dumps(request.query_params)
            process_export_request.apply_async(
                args=[_customer.id, _parms, user_email])

            return HttpResponse(_('<h1>You will receive the exported data in mail in few minutes</h1>'))

        if export_type == EXPORT_TYPE_MINIMAL:
            field_names = SHIPMENT_ORDER_MINIMAL_EXPORT.keys()
            column_names = SHIPMENT_ORDER_MINIMAL_EXPORT.values()

            _status, response = ShipmentExport(
                query_params=self.get_query(request.query_params),
                field_names=field_names,
                column_names=column_names
            ).export_minimal_details()

        elif export_type == EXPORT_TYPE_POD:
            _status, response = ShipmentExport(
                query_params=self.get_query(request.query_params),
                field_names=[],
                column_names=[]
            ).export_pod_documents()

        elif export_type == EXPORT_TYPE_WITH_EVENTS:
            field_names = EVENT_COLUMNS_EXPORT.keys()
            column_names = EVENT_COLUMNS_EXPORT.values()
            _status, response = ShipmentExport(
                query_params=self.get_query(request.query_params),
                field_names=field_names,
                column_names=column_names
            ).export_orders_with_events()

        elif export_type in [EXPORT_TYPE_ORDERLINE]:
            field_names = SHIPMENT_ORDERLINE_EXPORT.keys()
            column_names = SHIPMENT_ORDERLINE_EXPORT.values()
            _status, response = ShipmentExport(
                filename_prefix='shipments_orderlines',
                query_params=self.get_query(request.query_params),
                field_names=field_names,
                column_names=column_names
            ).export_orderline_details()

        if _status in [status.HTTP_200_OK, status.HTTP_400_BAD_REQUEST]:
            return response

        return HttpResponse(_('<h1>Unable to download the file</h1>'))

class BookingSlots(generics.ListAPIView):

    def get(self, request):
        data = request.GET.dict()
        latitude = data.get('lat')
        longitude = data.get('lon')
        city = get_city_from_latlong(latitude, longitude)
        if not city:
            city = City.objects.first()

        start_hour = city.start_time.strftime(CUSTOMER_APP_TIME_FORMAT)
        end_hour = city.end_time.strftime(CUSTOMER_APP_TIME_FORMAT)

        slots = []
        for i in range(NUM_DAYS_FUTURE):
            date = timezone.now() + timedelta(days=i)
            slots.append({
                'date': date.date(),
                'from': start_hour,
                'to': end_hour,
                'closed_hours_details': []
            })

        return Response(status=status.HTTP_200_OK, data=slots)


class OrderCreate(generics.CreateAPIView, CustomerMixin):
    permission_classes = (IsCustomer,)

    serializer_class = OrderSerializer

    def create(self, request):
        context = {'request': request}
        data = list(request.data.dict())

        try:
            data = json.loads(data[0])
        except ValueError as ve:
            print('Value Error: %s' % ve)
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=_('Data is tampered or invalid.'))

        user = request.user
        customer = self.get_customer(user)
        data['customer'] = customer
        request.POST._mutable = True

        if not data.get('device_type'):
            data['device_type'] = 'iOS'

        data_parsed = self._parse_params(data)
        if not data_parsed:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.error_message)

        address_pickup = CustomerShippingAddressSerializer(
            data=data_parsed.get('pickup_address', {}))
        address_drop_off = CustomerShippingAddressSerializer(
            data=data_parsed.get('shipping_address', {}))
        data['shipping_address'] = data_parsed.get('shipping_address', None)
        way_point_serializer = WaypointSerializer(
            data=data_parsed.get('waypoints_address', []), many=True)

        if address_pickup.is_valid(raise_exception=True) and \
            address_drop_off.is_valid(raise_exception=True) and \
                way_point_serializer.is_valid(raise_exception=True):
            order_serializer = OrderSerializer(
                context=context, data=data_parsed)
            order = order_serializer.save()
            way_point_serializer.save(order=order)
            self.post_save(order)

        background_color = '#13A893'
        dict_response = {
            "status": "PASS",
            "message": {
                'booking_id': order.number,
                'friendly_id': order.number,
                'tracking_url': 'http://%s/track/%s' % (settings.HOST, order.number),
                'booking_type': 'advanced',
                'failure_title': _('Thank You!'),
                'failure_message': _('Sorry! Unable to take your booking'),
                'success_title': _('Congratulations!'),
                'success_message': _('We have received your booking and will '
                                     'contact you shortly'),
                'background_color': background_color,
                'buttons': {
                    'done': True,
                    'call_support': False,
                }
            }
        }
        return Response(dict_response, status=status.HTTP_201_CREATED)

    def _parse_params(self, order_data):
        self.error_message = ''
        customer = order_data['customer']
        pickup = order_data.get('pickup_address')
        droppoff = order_data.get('dropoff_address')
        timeframe = order_data.get('timeframe', None)
        payment_mode = order_data.get('selectedPaymentMode', None)
        if not timeframe:
            self.error_message = 'Something went wrong!'
            return None

        now_or_later = timeframe.get('now_or_later', None)
        later_value = timeframe.get('later_value', '')
        pickup_datetime = self._clean_pickup_time(now_or_later, later_value)

        credit_amount_eligibility, pending_amount = customer.get_credit_amount_eligibility()
        credit_period_eligibility = customer.get_credit_days_eligibility()

        is_customer_eligible = credit_amount_eligibility and credit_period_eligibility
        if not is_customer_eligible:
            self.error_message = _('Please complete payment for previous '
                                   'trips to proceed with booking.')
            from blowhorn.customer.tasks import send_credit_limit_slack_notification
            if credit_amount_eligibility:
                pending_amount = 0

            send_credit_limit_slack_notification.delay(customer.id,
                                                       pending_amount,
                                                       credit_period_eligibility)
            return None

        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=pickup_datetime,
            lat=pickup['geopoint'].get('latitude', 0),
            lon=pickup['geopoint'].get('longitude', 0)
        )
        if not is_valid_booking_time:
            self.error_message = _('Service is unavailable at this time. '
                                   'Please schedule for later.')
            return None

        contract = Contract.objects.get(id=order_data.get('contract_id', None))
        # for C2C contract will have one vehicle class
        vehicle_class_preference = contract.vehicle_classes.first()

        return {
            'pickup_address': self._clean_shipping_address(pickup),
            'shipping_address': self._clean_shipping_address(droppoff),
            'items': order_data.get('items', None),
            'coupon_code': order_data.get('coupon_code', None),
            'customer_contract': order_data.get('contract_id', None),
            'vehicle_class_preference': vehicle_class_preference,
            'pickup_datetime': pickup_datetime,
            'device_info': order_data.get('device_type', 'iOS'),
            'status': StatusPipeline.ORDER_NEW,
            'waypoints': self._clean_waypoints(order_data.get('waypoints', [])),
            'order_type': settings.C2C_ORDER_TYPE,
            'labour_info': self._clean_labour(order_data.get('labour_option', '')),
            'customer': order_data.get('customer', None),
            'device_type': order_data.get('device_type', ''),
            'booking_type': now_or_later,
            'auto_debit_paytm': order_data.get('auto_debit_paytm', False),
            'estimated_cost': order_data.get('estimatePriceLow', 0.0),
            'estimated_distance_km': order_data.get('estimated_distance_km', 0.0),
            'payment_mode': payment_mode
        }

    def post_save(self, order):
        """
        Any logic post creating order is to be written here.
        """
        pickup_datetime = OrderPlacementMixin().get_formatted_datetime(
            order.pickup_datetime, order.device_type)
        time_difference = pickup_datetime.replace(
            tzinfo=None) - datetime.now()

        if (time_difference.total_seconds() / 3600) <= OrderConstants().get('PRE_NOTIFICATION_HOURS', 1):
            # Send Signal to transmit this order to a nearby driver.
            notify.apply_async((order.id,), )

        else:
            notify.apply_async(
                (order.id,), eta=pickup_datetime - timedelta(
                    hours=OrderConstants().get('PRE_NOTIFICATION_HOURS', 1)))

    # Clean methods
    def _clean_shipping_address(self, address):

        phone_no = address['address'].get('contact', {}).get('mobile', None)
        if not phone_no:
            phone_no = address['address'].get('contact', {}).get('phone_number', '')

        return {
            "title": "",
            "first_name": address.get('name', ''),
            "last_name": "",
            "line1": address['address'].get('line1', ''),
            "line2": '',
            "line3": address['address'].get('line3', ''),
            "line4": address['address'].get('line4', ''),
            "postcode": address['address'].get('postal_code', None),
            "country": address['address'].get('country', settings.COUNTRY_CODE_A2),
            "phone_number": phone_no,
            "geopoint": CommonHelper.get_geopoint_from_latlong(
                address['geopoint'].get('latitude', 0),
                address['geopoint'].get('longitude', 0))
        }

    def _clean_pickup_time(self, now_or_later, later_value=None):
        if now_or_later == 'now':
            pickup_time = timezone.now()
        else:
            m = re.match(r'(\d{2} \S{3} \d{4}) (\d:\d{2})', later_value)
            pickup_time = '%s 0%s' % (
                m.group(1), m.group(2)) if m else later_value
            for f in ALLOWED_TIME_FORMATS:
                logging.info('Matching time "%s" with format "%s"' %
                             (later_value, f))
                try:
                    pickup_time = ist_to_utc(
                        datetime.strptime(later_value, f))
                    break
                except:
                    pass

        return pickup_time

    def _clean_waypoints(self, waypoints):
        waypoints_cleaned = []
        for i, waypoint in enumerate(waypoints):
            contact = waypoint.get('contact')
            address = waypoint.get('address')
            geopoint = waypoint.get('geopoint')

            phone_no = contact.get('mobile', None) if contact else None
            if not phone_no:
                phone_no = contact.get('phone_number', '') if contact else ''

            waypoints_cleaned.append({
                "title": "",
                "first_name": contact.get('name', '') if contact else '',
                "last_name": "",
                "line1": address.get('line1', '') if address else '',
                "line2": '',
                "line3": address.get('line3', '') if address else '',
                "line4": address.get('line4', '') if address else '',
                "postcode": address.get('postal_code', None) if address else '',
                "phone_number": phone_no,
                "geopoint": CommonHelper.get_geopoint_from_latlong(geopoint.get('latitude', 0),
                                                                   geopoint.get('longitude', 0)),
                'sequence_id': i + 1
            })

        return waypoints_cleaned

    def _clean_labour(self, labour):
        count = 0
        cost = 0
        if isinstance(labour, str):
            if labour in ['light', 'heavy']:
                labour_cost = OrderConstants().get('LABOUR_COST', 400)
                LABOUR_COST_MAPPING = {
                    'light': labour_cost,
                    'medium': labour_cost,
                    'heavy': labour_cost,
                }
                count = 1
                cost = LABOUR_COST_MAPPING.get(labour, 400)

        elif isinstance(labour, dict):
            cost = labour.get('cost_per_labour', 0)
            count = labour.get('num_of_labours', 0)

        return {
            'no_of_labours': count,
            'labour_cost': cost
        }


class AvailableVehicles(APIView):
    """
    Description     : API to get vehicles which are available to serve
    Request type    : GET
    Params          : (lat, lng)
    Response        : Available Vehicles in a specified radius (for now 100 km)
    """

    permission_classes = (AllowAny, )

    def _get_location(self):
        params = self.request.GET
        lat, lng = params.get('lat', None), params.get('lng', None)
        if lat and lng:
            return LocationHelper.get_geopoint_from_latlong(lat, lng)

        return False

    def _get_available_vehicles(self, location):
        driver_available = DriverActivity.objects.filter(
            driver__status=StatusPipeline.ACTIVE,
            driver__vehicles__isnull=False,
            event=DRIVER_ACTIVITY_IDLE,
            geopoint__distance_lte=(
                location,
                AVAILABLE_DRIVER_SEARCH_RADIUS_IN_METERS
            )
        ).select_related('driver').prefetch_related('driver__vehicles')

        available_vehicle = []
        if driver_available:
            driver_available_serializer = AvailableDriverSerializer(
                driver_available, many=True)
            available_vehicle = get_vehicle_class_availability(
                driver_available_serializer.data)

        return available_vehicle

    def get(self, request):
        response = {'data': '', 'errors': ''}
        location = self._get_location()
        if not location:
            response['errors'] = _('Location geopoint missing')
            status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)

        city = get_city_from_geopoint(location, field='coverage_pickup')
        if not city:
            response['errors'] = 'Area not covered'
            status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)

        available_vehicle = self._get_available_vehicles(location)
        if available_vehicle:
            response['data'] = available_vehicle
            status_code = status.HTTP_200_OK
        else:
            status_code = status.HTTP_400_BAD_REQUEST
        return Response(response, status_code)


class FleetOrderBatchList(generics.ListAPIView, CustomerMixin):
    """
    List OrderBatch within the given range start and end date
    and for the particular customer

    * Requires authentication.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer)

    serializer_class = FleetOrderBatchSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('start_time',)

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        order_batch_qs = OrderBatch.objects.filter(query).distinct()

        return order_batch_qs

    def get_query(self, params):
        customer = self.get_customer(self.request.user)
        start_time = params.get('start_time', None)
        end_time = params.get('end_time', None)

        query = [Q(orders__customer=customer), Q(
            orders__order_type=settings.SHIPMENT_ORDER_TYPE)]

        if start_time:
            query.append(Q(start_time__date__gte=start_time))
        if end_time:
            query.append(Q(start_time__date__lte=end_time))
        return reduce(operator.and_, query)


class ShipmentCustomerList(generics.ListAPIView):
    """
        Description     : Get Customers with Shipment type contracts
                          This API is used in driver app and shipment utility
                           page in admin
        Request type    : GET
        Params          : None
        Response        : Customer Id and name for given customers.
    """

    permission_classes = (IsAuthenticated, )
    serializer_class = ShipmentCustomerSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id',)

    def get(self, request):

        query_params = Q(contract__contract_type__in = [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL])
        if hasattr(request.user, 'driver'):
            query_params = query_params & Q(
                contract__city=request.user.driver.operating_city) & Q(
                contract__status=CONTRACT_STATUS_ACTIVE)
        shipment_customers = Customer.objects.filter(query_params
                                                     ).distinct().values('id',
                                                                         'name')
        return Response(status=status.HTTP_200_OK, data=shipment_customers)


class OrganizationMemberList(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,
                          Or(IsOrgOwner, IsOrgAdmin, ReadonlyAccess))

    serializer_class = OrganizationMemberSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('is_active', 'is_admin',)

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        if not customer:
            return self.queryset.none()
        return self.serializer_class().queryset(customer=customer)

    def put(self, request):
        data = request.data
        logger.info('Updating member: %s' % data)
        pk = data.get('id', None)
        email = data.get('email', None)
        phone_number = data.get('phone_number', None)
        if not pk or not email:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=self.get_response_message(
                    ORGANIZATION_MESSAGES['error']['field']['email']['required']
                )
            )

        account_user = self.serializer_class.Meta.model.objects.get(pk=pk)
        serializer = self.serializer_class(
            account_user, data=data, context={
                'request': request,
                'phone_number': phone_number
            })
        if serializer.is_valid():
            try:
                account_instance = serializer.save()
                account = self.serializer_class(account_instance)

                if account_user.status == 'old_invited':
                    user, created = self.send_invitation(
                        '', email, phone_number,
                        account_instance.organization, request=request,
                        resend=True)
                response_data = self.get_response_message(
                    '%s updated successfully' % email,
                    account.data
                )

                return Response(status=status.HTTP_200_OK,
                                data=response_data)

            except IntegrityError as ie:
                message = ORGANIZATION_MESSAGES['error']['integrity'].format(
                    email=email)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=self.get_response_message(message))

            except BaseException as be:
                logger.info('%s' % be)
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=_(ORGANIZATION_MESSAGES['error']['common']))

        logger.info(serializer.errors)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=_(serializer.errors))


class OrganizationInviteMember(APIView, CustomerMixin):
    permission_classes = (IsAuthenticated, Or(IsOrgOwner, IsOrgAdmin))
    serializer_class = OrganizationMemberSerializer

    def post(self, request):
        """
        :param request: request with following data
            email: mandatory
            name, phone_number: optional
            is_admin: Boolean (default: False)
        :return: Success if org-user is created;
            otherwise error message will be sent
        """
        user = request.user
        data = request.data
        logger.info('Adding new member: %s' % data)
        email = data.get('email', '').lower()
        name = data.get('name', None)
        phone_number = data.get('phone_number', None)
        is_admin = data.get('is_admin', False)
        customer = self.get_customer(user)

        if phone_number and not self.validate_phonenumber(str(phone_number)):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_response_message(
                                ORGANIZATION_MESSAGES['error']['field'][
                                    'phone_number']['invalid'])
                            )

        if Customer.objects.filter(user__email=email).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_response_message(
                                ORGANIZATION_MESSAGES['error']['integrity'].
                                    format(email=email)
                            )
                        )

        if self.serializer_class.Meta.model.objects.filter(
                user__email=email).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_response_message(
                                ORGANIZATION_MESSAGES['error']['integrity'].
                                    format(email=email)
                            )
                        )

        with transaction.atomic():
            user, created = self.send_invitation(name, email, phone_number,
                                                 customer, request=request)

            try:
                extra_info = {
                    'is_admin': is_admin,
                    'created': created
                }
                account_user, created = customer.get_or_add_user(user,
                                                                 **extra_info)

                account = self.serializer_class(account_user)
                if account_user.status == 'old_invited':
                    user, created = self.send_invitation('', email,
                        phone_number, customer, request=request, resend=True)

                response_data = self.get_response_message(
                    ORGANIZATION_MESSAGES['invitation']['success'].format(
                        email=email),
                    account.data
                )

                return Response(status=status.HTTP_200_OK,
                                data=response_data)

            except IntegrityError as ie:
                logger.info('%s' % ie)
                message = ORGANIZATION_MESSAGES['error']['integrity'].format(
                    email=email)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=self.get_response_message(message))

            except BaseException as be:
                logger.info('%s' % be)
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=ORGANIZATION_MESSAGES['error']['common'])

    def put(self, request):
        """
        Resend invitation
        NOTE: not implemented at client side
        """
        data = request.data
        email = data.get('email', None)
        logger.info('Resending invitation: %s' % data)

        customer = self.get_customer(request.user)
        try:
            user = self.send_invitation('', email, '', customer)
            return Response(status=status.HTTP_200_OK,
                            data=self.get_response_message(
                                'Invitation has been sent successfully'
                            )
                        )

        except BaseException as be:
            logger.info(be)
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=self.get_response_message(
                                'Failed to send invitation to %s' % email
                            )
                        )


@login_required
@api_view(['GET'])
def invoice_summary_list(request):
    if request.method == 'GET':
        data = request.GET.dict()
        related_models = ['customer', 'bh_source_state', 'invoice_state',
                          'current_owner']
        lookup_params = convert_date_hierarchy_params('service_period_end',
                                                      data)
        if lookup_params:
            qs = InvoiceSummary._default_manager.filter(**lookup_params)
        else:
            qs = InvoiceSummary._default_manager.all()

        qs = qs.exclude(customer__customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        qs = qs.only('customer_id', 'customer', 'id', 'invoice_number',
                     'invoice_date', 'status', 'invoice_type',
                     'bh_source_state', 'invoice_state', 'current_owner',
                     'total_amount', 'due_date', 'assigned_date',
                     'invoice_type', 'is_credit_debit_note',
                     'total_rcm_amount', 'comments', 'remarks')
        qs = qs.select_related(*related_models)
        return render_to_response(
            'admin/customer/invoice/partials/main_table.html',
            {
                'invoices': qs,
                'current_status': request.GET.get('status', CREATED),
            }
        )
    return render_to_response('website/404_not_found.html', {'params': "{}"},
                              status=400)


@login_required
@api_view(['GET'])
def invoice_history(request, invoice_pk=None):
    if request.method == 'GET':
        if not invoice_pk:
            return HttpResponse(_('History not found.'), status=400)

        qs = InvoiceHistory.objects.filter(invoice_id=invoice_pk)
        qs = qs.select_related('created_by', 'action_owner').order_by(
            'created_time')
        return render_to_response(
            'admin/customer/invoice/partials/history.html', {'history_qs': qs}
        )

    return render_to_response('website/404_not_found.html',
                              {'params': "{}"},
                              status=400)


class ContactAutocompleteView(APIView):
    permission_classes = (IsBlowhornStaff,)

    def get(self, request):
        email_pattern = '''^([!#-\'*+\/-9=?A-Z^-~\\\\-]{1,64}(\.[!#-\'*+\/-9=?A-Z^-~\\\\-]{1,64})*|
        \"([\]!#-[^-~\ \t\@\\\\]|(\\[\t\ -~]))+\")@([0-9A-Z]([0-9A-Z-]{0,61}[0-9A-Za-z])?
        (\.[0-9A-Z]([0-9A-Z-]{0,61}[0-9A-Za-z])?))+$'''

        entered_term = request.GET.get('term', '')
        query_param = {
            'is_active': True,
            'is_staff': True
        }
        query_param.update({'email__icontains': entered_term})

        contacts_list = User.objects.filter(**query_param) \
            .values_list('email', flat=True)

        contacts = [item for item in contacts_list]
        otheremail = re.match(email_pattern,entered_term)

        if otheremail:
            contacts.append(entered_term)

        return Response(status=status.HTTP_200_OK, data=contacts)

class GetInvoiceDetails(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key != settings.BLOWHORN_API_KEY:
            return Response(status=status.HTTP_400_BAD_REQUEST,  data='API Key mismatch')

        start_date = data.get('start_date')
        end_date = data.get('end_date')
        if not start_date or not end_date:
            return Response(status=status.HTTP_400_BAD_REQUEST,  data='Start_date and end_date is required')

        date_start = TimeUtils.date_to_datetime_range(start_date)[0]
        date_end = TimeUtils.date_to_datetime_range(end_date)[1]

        start_datetime = date_start.astimezone(tz=pytz.utc)
        end_datetime = date_end.astimezone(tz=pytz.utc)

        unprocessed_qs = Q(is_processed=False,
                                status__in=MZASIGO_ALLOWED_STATUS_FOR_INVOICE_GENERATION)

        processed_qs = Q(is_processed=True,
                            is_processed_timestamp__date__gte=start_datetime,
                            is_processed_timestamp__date__lte=end_datetime)

        invoices = CustomerInvoice.objects.filter(
                        unprocessed_qs | processed_qs).annotate(
                        number=F('invoice_number'),
                        total_payable=F('total_amount'),
                        pickup_datetime=Concat('service_period_start', V(' to '), 'service_period_end',
                        output_field=CharField())).values(
                        'number', 'total_payable', 'pickup_datetime')

        return Response(status=status.HTTP_200_OK, data=invoices)

class UpdateInvoiceProcessedFlag(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key != settings.BLOWHORN_API_KEY:
            return Response(status=status.HTTP_400_BAD_REQUEST,  data='API Key mismatch')

        invoice_list = data.get('invoice_list')
        if not invoice_list:
            return Response(status=status.HTTP_400_BAD_REQUEST,  data='Invoice list required in input for flag update')

        cp_month = data.get('pmonth')
        cp_year = data.get('pyear')
        start_date = make_aware(datetime(cp_year, cp_month, 5))
        invoices = CustomerInvoice.objects.filter(
                        invoice_number__in = invoice_list).update(is_processed=True,
                                    is_processed_timestamp=start_date)

        return Response(status=status.HTTP_200_OK)


class SendSms(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key != "P1a03N19d91B":
            return Response(status=status.HTTP_400_BAD_REQUEST)

        template_id = data.get('template_id', None)
        message = data.get('message', None)
        phone_number = data.get('phone_number', None)
        service = data.get('service')

        if template_id and message and phone_number and service:
            template_dict = {
                'template_id' : template_id,
                'template_type' : 'TXN'
            }
            Communication.send_sms(sms_to=phone_number, message=message,
                                    template_dict=template_dict,
                                    priority='high', provider=service)

        return Response(status=status.HTTP_200_OK)


class CallMasking(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        order_id = data.get('order_id')
        url = settings.EXOTEL_MASKING % (settings.EXOTEL_API_KEY, settings.EXOTEL_API_TOKEN, settings.EXOTEL_API_SID)
        order = Order.objects.filter(id=order_id).first()
        if not order:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order id')

        dropoff = order.shipping_address
        customer_number = str(dropoff.phone_number)
        if not customer_number:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Customer number not available')

        driver = order.driver
        if not driver:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Driver number not available')

        driver_phone = str(driver.user.phone_number)
        data = {
            'From': str(customer_number),
            'To': driver_phone,
            'CallerId': '08047090952',
            "StatusCallback": settings.HOST_URL+ '/api/exotel/call/update',
            "StatusCallbackEvent": "terminal",
            "StatusCallbackContentType": "application/json"
        }

        logger.info('Callmasking data: ', data)
        res = requests.post(url, data=data)
        data = json.loads(res.content)
        response = {
            "message": "Failed to make a call"
        }
        if res.status_code == status.HTTP_200_OK:
            call_details = data['Call']
            details = {
                "to_number": call_details['To'],
                "from_number": call_details['From'],
                "sid": call_details['Sid'],
                "caller_id": call_details['PhoneNumberSid'],
                "order": order,
                "driver": order.driver,
                "price": call_details['Price'] or 0
            }
            CallMaskingLog.objects.create(**details)
            response = {
                "message": "You will get a callback soon"
            }

        return Response(status=status.HTTP_200_OK, data=response)


class CustomerApiKeyView(APIView, CustomerMixin):

    permission_classes = (AllowAny, )

    def get(self, request):
        """
        Returns Customer API Key after authenticating the user
        :param request:
        :return:
        """

        auth_str = request.META.get('HTTP_AUTHORIZATION', None)

        try:
            auth_bytes = auth_str.encode('ascii')
            b64_bytes = base64.b64decode(auth_bytes)
            user_auth_str = b64_bytes.decode('ascii')

        except BaseException as e:
            logger.info('Error parsing base64 string - %s' % e)
            response = {
                'status': 'FAIL',
                'message': _(
                    'There was an error in parsing your authtoken.'
                    ' Please make sure the credentials are encoded properly')
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        user_auth = user_auth_str.split(':')
        email = user_auth[0]
        password = user_auth[1]

        user = User.objects.filter(
            email=email,
            is_staff=False,
            driver__isnull=True,
            vendor__isnull=True
        ).first()

        if user:
            if not user.is_active:
                response = {
                    'status': 'FAIL',
                    'message': _('Your user is blocked. Please contact support.')
                }
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=response
                )

            customer = self.get_customer(user)
            login_response = User.objects.do_login(
                request=request,
                email=email,
                password=password
            )
            if login_response.status_code == status.HTTP_200_OK:
                user = login_response.data.get('user')

                if not customer or user.is_staff:
                    return Response(
                        status=status.HTTP_401_UNAUTHORIZED,
                        data=_('User not allowed to login')
                    )

                customer_apikey = customer.api_key
                apikey_bytes = customer_apikey.encode("ascii")
                base64_key_bytes = base64.b64encode(apikey_bytes)
                base64_key_string = base64_key_bytes.decode("ascii")

                response = {
                    'status': 'PASS',
                    'api_key': base64_key_string
                }
                return Response(status=status.HTTP_200_OK, data=response)

            response = {
                'status': 'FAIL',
                'message': _('Incorrect Email/Password')
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        response = {
            'status': 'FAIL',
            'message': _('User not found. Contact support')
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response)


class SmsResponse(APIView):

    permission_classes = (AllowAny,)

    def post(self, request):

        _status = status.HTTP_400_BAD_REQUEST
        try:
            sms_response = request.data
            sms_sid = sms_response.get('SmsSid', None)
            message_id = sms_response.get('id', None)
            units, price = 0, 0
            smslog = None

            # exotel processing
            if sms_sid:
                provider = 'exotel'
                _msg_status = sms_response.get('Status')
                msg_status = 'Delivered' if (_msg_status=='sent') else 'Failed'
                _description = sms_response.get('DetailedStatus')
                description = '%s - %s' % (_msg_status, _description)
                smslog = SmsLog.objects.filter(ssid=sms_sid)
                _status = status.HTTP_200_OK

            # kaleyra processing
            elif message_id:
                msg_status = sms_response.get('status')
                description = sms_response.get('description')
                units = sms_response.get('units', 0)
                price = sms_response.get('price', 0)
                smslog = SmsLog.objects.filter(ssid=message_id)
                _status = status.HTTP_200_OK

            if (_status == status.HTTP_200_OK):
                if smslog.exists():
                    smslog.update(sms_status=msg_status, detailed_status=description,
                                    units=Decimal(units), cost=Decimal(price))

        except Exception as e:
            print(e)

        return Response(status=_status)


class CustomerBankAccountsView(APIView, CustomerMixin):
    permission_classes = (IsAuthenticated, Or(IsOrgOwner, IsOrgAdmin))

    def get(self, request):
        customer = self.get_customer(request.user)
        bank_details = CustomerBankDetails.objects.filter(
            customer=customer, is_deleted=False)
        bank_account_details = [record.to_json() for record in bank_details]
        return Response(status=status.HTTP_200_OK, data=bank_account_details)

    def post(self, request):
        data = request.data
        customer = self.get_customer(request.user)
        if not customer:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid User')

        account_id = data.get('id', None)
        is_active = data.get('is_active', None)
        if is_active and CustomerBankDetails.objects.filter(customer=customer, is_active=True).exclude(
           id=account_id).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='There can be only one active bank account.')
        account_number = data.get('account_number', None)
        ifsc_code = data.get('ifsc_code', None)
        account_name = data.get('account_name', None)
        bank_name = data.get('bank_name', None)
        branch_name = data.get('branch_name', None)

        if not account_number or not ifsc_code:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Account No. and IFSC code is mandatory')

        try:
            with transaction.atomic():
                bank_details = CustomerBankDetails(customer=customer, bank_account_number=account_number,
                                                   ifsc_code=ifsc_code, account_name=account_name,
                                                   bank_name=bank_name, branch_name=branch_name)
                bank_details.save()
                account_number = 'XXXX%s' % account_number[-4:]
                CustomerBankDetailsHistory.objects.create(customer_bank_details=account_number, action=ACCOUNT_ADDED,
                                                          action_owner=request.user, customer=customer)
        except BaseException as be:
            logger.info('Failed Bank A/c addition: %s' % be)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to save bank details')

        send_bank_details_alert(customer, account_number, ADDED)

        return Response(status=status.HTTP_200_OK, data='Bank Account Details Saved')

    def put(self, request, pk=None):
        data = request.data
        customer = self.get_customer(request.user)
        if not customer:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid User')

        is_active = data.get('is_active', False)
        if is_active and CustomerBankDetails.objects.filter(
            customer=customer,
            is_active=True
        ).exclude(id=pk).exists():
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_('There can be only one active bank account.')
            )

        account_number = get_account_number(pk)
        action = ACTIVATED if is_active else DEACTIVATED
        try:
            with transaction.atomic():
                CustomerBankDetails.objects.filter(id=pk).update(is_active=is_active)
                CustomerBankDetailsHistory.objects.create(
                    customer_bank_details=account_number,
                    action=action,
                    action_owner=request.user,
                    customer=customer
                )
        except BaseException as be:
            logger.info('Failed Bank A/c update: %s' % be)
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data='Failed to update bank details'
            )

        send_bank_details_alert(customer, account_number, action)
        return Response(
            status=status.HTTP_200_OK,
            data='Bank account details updated successfully'
        )

    def delete(self, request, pk=None):
        customer = self.get_customer(request.user)
        if not customer:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid User')
        bank_account_details = CustomerBankDetails.objects.filter(id=pk)
        data = request.data
        reason = data.get('reason', None)
        if not bank_account_details.exists():
            return Response(status=status.HTTP_404_NOT_FOUND, data='Bank Details not found')

        if EzetapPayment.objects.filter(bank_details=pk).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Cannot delete account details. Kindly mark as inactive.')

        account_number = get_account_number(pk)

        try:
            with transaction.atomic():
                CustomerBankDetailsHistory.objects.create(customer_bank_details=account_number, action=ACCOUNT_DELETED,
                                                          action_owner=request.user, reason=reason, customer=customer)
                bank_account_details.delete()
        except BaseException as be:
            logger.info('Failed Bank A/c deletion: %s' % be)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to delete bank details')

        send_bank_details_alert(customer, account_number, DELETED)
        return Response(status=status.HTTP_200_OK, data='Bank Account Details Deleted')


class ChangePasswordView(APIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        data = request.data
        oldpassword = data.get('oldpassword', None)
        password1 = data.get('password1', None)
        password2 = data.get('password2', None)
        user = request.user
        customer = self.get_customer(request.user)

        if not password1 or not password2:
            _out = {
                'status': 'FAIL',
                'message': 'Password cannot be Empty'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        is_valid_password, msg = validate_customer_password(password1, password2, oldpassword, user)

        if is_valid_password:
            user.set_password(password1)
            user.save()
            update_session_auth_hash(request, user)
        _out = {
            'status': 'Success' if is_valid_password else 'Password Validation Failed',
            'message': msg
        }
        send_password_alert(is_valid_password, user)
        return Response(status=status.HTTP_200_OK if is_valid_password else status.HTTP_400_BAD_REQUEST,
                        data=_out)


class WmsCustomer(APIView):
    """
        API to retrieve WMS config status for the customer
    """

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = get_customer_from_api_key(api_key)

        is_wms_enabled = True if customer.whsite_set.first() else False

        resp = {
            'status': 'PASS',
            'is_wms_enabled': is_wms_enabled
        }
        return Response(status=status.HTTP_200_OK, data=resp)


class CustomerFailedOrderListView(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = CustomerFailedOrderSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('reference_number', 'response_status', 'reason', 'created_at')

    search_fields = ('reference_number', 'reason')
    filterset_fields = ('response_status', )

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        start = self.request.GET.get('start', None)
        end = self.request.GET.get('end', None)
        quick_filter_date_delta = self.request.GET.get('quick_filter', None) or 0
        query = [Q(customer=customer), ~Q(response_status__in=SUCCESS_STATUS_CODES)]

        if not start and not end:
            end = timezone.now().date()
            start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))

        if start and end:
            query.append(
                Q(created_at__date__gte=start)
                & Q(created_at__date__lte=end)
            )

        query = reduce(operator.and_, query)
        return OrderApiData.objects.filter(query)
