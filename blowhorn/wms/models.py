from django.utils.translation import ugettext_lazy as _
from django.db import models
from blowhorn.oscar.core.loading import get_model

from blowhorn.common.models import BaseModel


class ProductGroup(BaseModel):
    name = models.TextField(_('Name'), null=False, blank=False)

    description = models.CharField(_('Description'), null=True, blank=True,
                                   max_length=256)
    customer = models.ForeignKey('customer.Customer', on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('customer', 'name')


class WmsConstants(BaseModel):
    """
        Contains configurable constants for WMS
    """

    name = models.CharField(max_length=50)
    value = models.FloatField()
    description = models.CharField(max_length=200, blank=True, null=True)

    def get(self, name, default):
        try:
            constant = WmsConstants._default_manager.get(name=name)
        except WmsConstants.DoesNotExist:
            return default

        return constant.value

    class Meta:
        verbose_name = _('WMS Config')
        verbose_name_plural = _('WMS Configs')


from blowhorn.wms.submodels.inbound import *
from blowhorn.wms.submodels.location import *
from blowhorn.wms.submodels.sku import *
from blowhorn.wms.submodels.receiving import *
from blowhorn.wms.submodels.returnorder import *
from blowhorn.wms.submodels.tasks import *
from blowhorn.wms.submodels.inventory import *
from blowhorn.wms.submodels.putaway import *
from blowhorn.wms.submodels.query_builder import *
from blowhorn.wms.submodels.qualitycheck import *
from blowhorn.wms.submodels.yard import *
from blowhorn.wms.submodels.stockcheck import *


class QCAttributeValues(BaseModel):
    attribute = models.ForeignKey(QCAttributes, on_delete=models.PROTECT)

    qc_config = models.ForeignKey(QCConfiguration, on_delete=models.PROTECT)

    value_text = models.TextField(_('Text'), blank=True, null=True)
    value_integer = models.IntegerField(_('Integer'), blank=True, null=True, db_index=True)
    value_boolean = models.NullBooleanField(_('Boolean'), blank=True, db_index=True)
    value_float = models.FloatField(_('Float'), blank=True, null=True, db_index=True)
    value_date = models.DateField(_('Date'), blank=True, null=True, db_index=True)
    value_datetime = models.DateTimeField(_('DateTime'), blank=True, null=True, db_index=True)
    value_file = models.FileField(upload_to=generate_file_path, validators=[FileValidator()], max_length=256, null=True,
                                  blank=True)

    qc_task = models.ForeignKey(WhTask, blank=True, null=True, on_delete=models.PROTECT)

    def __str__(self):
        return '%s - %s' % (self.qc_task, self.attribute)

