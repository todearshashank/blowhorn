# Generated by Django 2.2.24 on 2021-08-09 17:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wms', '0008_qcattributes_customer'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='shopify_location_id',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Shopify Location'),
        ),
        migrations.AddField(
            model_name='whsku',
            name='shopify_description',
            field=models.TextField(blank=True, null=True, verbose_name='Shopify Description'),
        ),
        migrations.AddField(
            model_name='whsku',
            name='shopify_inventory_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Shopify Inventory ID'),
        ),
        migrations.AddField(
            model_name='whsku',
            name='shopify_product_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Shopify Product ID'),
        ),
        migrations.AddField(
            model_name='whsku',
            name='shopify_variant_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Shopify Variant ID'),
        ),
    ]
