# Generated by Django 2.2.24 on 2021-08-17 16:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wms', '0012_auto_20210812_1804'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='qcattributes',
            options={'verbose_name': 'QC Attribute', 'verbose_name_plural': 'QC Attributes'},
        ),
        migrations.AlterModelOptions(
            name='wmsmanifestline',
            options={'verbose_name': 'OrderLine', 'verbose_name_plural': 'Order Lines'},
        ),
        migrations.AddField(
            model_name='inventorysummary',
            name='inbound_qty',
            field=models.PositiveIntegerField(default=0, verbose_name='Inbound Quantity'),
        ),
        migrations.AddField(
            model_name='inventorysummary',
            name='outbound_qty',
            field=models.PositiveIntegerField(default=0, verbose_name='Outbound Quantity'),
        ),
        migrations.AlterField(
            model_name='inventory',
            name='weight',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=300, null=True, verbose_name='Weight'),
        ),
    ]
