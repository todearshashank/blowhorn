# Generated by Django 2.2.23 on 2021-09-04 16:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wms', '0017_auto_20210904_0333'),
    ]

    operations = [
        migrations.AddField(
            model_name='whsku',
            name='requires_shipping',
            field=models.BooleanField(default=True, verbose_name='Requires Shipping'),
        ),
    ]
