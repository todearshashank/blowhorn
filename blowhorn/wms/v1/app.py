from django.conf.urls import url, include

from blowhorn.wms.v1.views import wms as warehouse_views
from blowhorn.wms.views import allocate, whtask, whsku, location, inventory, qc, pallet, shipping, receiving, putaway, \
    pick, order, stockcheck, inbound

urlpatterns = [

    url(r'^login$', warehouse_views.Login.as_view(), name='wms-login-v1'),

    url(r'^inventory/import$', inventory.inventory_import, name='inventory-import'),
    url(r'^inventory/file/template$', inventory.InventoryExcelFileFormat.as_view(), name='inventory-import-file-format'),
    url(r'^inventory/file/import$', inventory.InventoryExcelImport.as_view(), name='inventory-file-import'),
    url(r'^inventory/export$', inventory.InventoryExport.as_view(), name='inventory-export'),
    url(r'^inventory/transaction/export$', inventory.InventoryTransactionExport.as_view(),
        name='inventory-transaction-export'),

    url(r'^sku/file/template$', whsku.WhSkuExcelFileFormat.as_view(), name='sku-file-format'),
    url(r'^sku/file/import$', whsku.WhSkuExcelImport.as_view(), name='sku-file-import'),
    url(r'^sku/import$', whsku.whsku_import, name='sku-import'),
    url(r'^sku/export$', whsku.WhSkuExcelExportView.as_view(), name='sku-export'),

    url(r'^supplier/file/template$', inbound.SupplierImportExcelTemplate.as_view(), name='supplier-file-template'),
    url(r'^supplier/file/import$', inbound.SupplierImportView.as_view(), name='supplier-file-import'),
    url(r'^supplier/import$', inbound.supplier_import, name='supplier-import'),
    url(r'^supplier$', inbound.SupplierView.as_view(), name='wms-supplier'),

    url(r'^product/file/template$', warehouse_views.ProductGroupImportTemplate.as_view(),
        name='product-import-file-template'),
    url(r'^product/file/import$', warehouse_views.ProductGroupImport.as_view(), name='product-group-import'),
    url(r'^import/lookups$', warehouse_views.ImportLookupsView.as_view(), name='import-lookups'),
    url(r'^product/import$', warehouse_views.product_group_import, name='product-group-import'),
    url(r'^productgroup$', whsku.ProductGroupView.as_view(), name='wms-productgroup'),

    url(r'^client/file/template$', location.ClientImportExcelTemplate.as_view(), name='client-import-template'),
    url(r'^client/file/import$', location.ClientImportView.as_view(), name='client-import'),
    url(r'^client/import$', location.client_import, name='client-import'),

    url(r'^inventory/transaction$', inventory.InventoryTransactionListView.as_view(), name='inventory-transaction-list'),
    url(r'^stock/summary$', warehouse_views.StockSummaryCountListView.as_view(), name='stock-summary-list'),
    url(r'^stock/summary/detail$', warehouse_views.StockSummaryListView.as_view(), name='stock-summary-detail'),

    url(r'^activities$', warehouse_views.Activity.as_view(), name='wms-activity-v1'),
    url(r'^blindreceiving$', receiving.BlindReceiving.as_view(), name='wms-blind-receiving'),
    url(r'drivers/search$', warehouse_views.DriverFilter.as_view(), name='driver-search'),
    url(r'^inventory/info', warehouse_views.InventoryInfo.as_view(), name='wms-inv-info'),
    url(r'^stock/info', stockcheck.StockInfo.as_view(), name='wms-stock-info'),
    url(r'^grn/list', receiving.GRNListView.as_view(), name='wms-grn-list'),
    url(r'^grn', receiving.GRN.as_view(), name='wms-grn-receiving'),
    url(r'^asn', receiving.ASNView.as_view(), name='wms-asn-receiving'),
    url(r'^putaway$', putaway.PutAway.as_view(), name='wms-putaway'),
    url(r'^putaway/start$', putaway.StartPutAway.as_view(), name='wms-putaway'),
    url(r'^location', location.LocationView.as_view(), name='wms-location'),
    url(r'^inventory$', inventory.InventoryView.as_view(), name='wms-inventory'),
    url(r'^task$', whtask.WhTaskView.as_view(), name='wms-whtask'),
    url(r'^tasks$', whtask.WhTaskListView.as_view(), name='wms-whtask-list'),
    url(r'^sku', whsku.WhSkuView.as_view(), name='wms-whsku'),
    url(r'^vendor', warehouse_views.WmsClient.as_view(), name='wms-client'),
    url(r'^tracking/info', warehouse_views.TrackingDetail.as_view(), name='wms-tracking-info'),
    url(r'^receiving', receiving.Receiving.as_view(), name='wms-tracking-info'),
    url(r'^warehouse', warehouse_views.WmsView.as_view(), name='wms-site'),
    url(r'^updatesite', warehouse_views.SiteView.as_view(), name='wms-zone'),
    url(r'^siteconfig', warehouse_views.SiteConfigView.as_view(), name='wms-siteconfig'),
    url(r'^allocate$', allocate.AllocateOrder.as_view(), name='order-allocation'),
    url(r'^pick$', pick.Pick.as_view(), name='wms-putaway'),
    url(r'^pick/start$', pick.StartPick.as_view(), name='wms-putaway'),
    url(r'^hub/onboard$', warehouse_views.HubOnboardView.as_view(), name='hub-onboard'),
    url(r'^site/list', location.WhSiteListView.as_view(), name='whsite-list-view'),
    url(r'^site/create$', location.WhSiteView.as_view(), name='create-whsite'),
    url(r'^packconfig$', whsku.PackConfigView.as_view(), name='pack-config'),
    url(r'^trackinglevel$', whsku.TrackingLevelView.as_view(), name='tracking-level'),
    url(r'^client/sku$', whsku.ClientSKUView.as_view(), name='client-skus'),
    # import wms data
    url(r'^import/skus$', whsku.SkuImport.as_view(), name='wms-sku-import'),
    url(r'^locations$', location.LocationImport.as_view(), name='wms-location-import'),
    url(r'^itls$', inventory.ITLView.as_view(), name='inventory-transaction-view'),
    url(r'^inventories$', inventory.DataImport.as_view(), name='wms-data-import'),
    url(r'^locationtypes$', location.LocationTypeImport.as_view(), name='wms-location-type-import'),
    url(r'^locationzones$', location.LocationZoneImport.as_view(), name='wms-location-zone-import'),
    url(r'^storagetypes$', location.StorageTypeImport.as_view(), name='wms-storage-type-import'),
    url(r'^clients$', location.ClientImport.as_view(), name='wms-client-import'),
    url(r'^suppliers$', receiving.ImportSupplier.as_view(), name='wms-supplier-import'),
    url(r'^purchaseorders$', receiving.ImportPurchaseOrder.as_view(), name='wms-purchase-order-import'),
    url(r'^create-asn$', receiving.ImportASNOrder.as_view(), name='wms-asn-order-import'),
    url(r'^status/asn$', receiving.ASNOrderStatus.as_view(), name='wms-get-asn-order'),
    url(r'^update-asn$', receiving.ASNOrderUpdate.as_view(), name='wms-get-asn-order'),
    url(r'^update-po$', receiving.POOrderUpdate.as_view(), name='wms-get-asn-order'),
    url(r'^status/po$', receiving.PurchaseOrderStatus.as_view(), name='wms-get-po-order'),
    url(r'^status/inventories$', inventory.InventoryStatus.as_view(), name='wms-get-inventory'),
    url(r'^inventoriestransactions$', inventory.InventoryTransactions.as_view(), name='wms-get-inventory-transaction'),

    # QC
    url(r'^qc/config$', qc.QcConfigView.as_view(), name='qc-check'),
    # url(r'^yard/activity', qc.QcConfigView.as_view(), name='qc-check'),
    url(r'^qc/attributes', qc.QcAttributesVIew.as_view(), name='qc-check'),
    url(r'^qc/task$', qc.QCTaskView.as_view(), name='qc-check'),
    url(r'^pallet$', pallet.PalletView.as_view(), name='pallet'),
    url(r'^shipping/manifest$', shipping.Manifest.as_view(), name='manifest'),
    url(r'^shipping/manifest-list$', shipping.ManifestList.as_view(), name='manifest-list'),
    url(r'^shipping/xdk', shipping.XDKManifest.as_view(), name='xdk-manifest'),
    url(r'^shipment/order$', shipping.ShipmentOrder.as_view(), name='shipment-order'),
    url(r'^hub-receiving', shipping.ReturnOrderView.as_view(), name='return-order'),
    url(r'^shipping$', shipping.Shipping.as_view(), name='shipping'),
    url(r'^orders/manifest_list', shipping.ManifestOrderList.as_view(), name='orders-to-be-manifested'),
    url(r'^manifest', shipping.ManifestView.as_view(), name='shipping-manifest'),
    url(r'^orders$', shipping.WmsOrderListView.as_view(), name='wms-order-list'),
    url(r'^orders/ship', shipping.ShippingView.as_view(), name='shipping-orders'),
    url(r'^order/detail', shipping.OrderDetail.as_view(), name='order-detail'),
    url(r'^orders/(?P<wh_id>[\w\-]+)$', allocate.OrderView.as_view(), name='wms-orders'),

    # Allocation
    url(r'allocation/config/list$', allocate.AllocationConfigListView.as_view(), name='list-allocation-config'),
    url(r'allocation/config$', allocate.AllocationConfigView.as_view(), name='allocation-config'),
    url(r'allocation/config/(?P<pk>[\w\-]+)$', allocate.AllocationConfigView.as_view(), name='allocation-config'),
    url(r'shipping/allocate$', allocate.Allocation.as_view(), name='allocation'),
    url(r'order/allocation_lines/(?P<pk>[\w\-]+)$', allocate.AllocationLineListView.as_view(), name='allocation-lines'),

    # Query Builder
    url(r'querybuilder/list$', allocate.QueryBuilderListView.as_view(), name='query-builder-list'),
    url(r'querybuilder$', allocate.QueryBuilderView.as_view(), name='query-builder'),
    url(r'querybuilder/(?P<pk>[\w\-]+)$', allocate.QueryBuilderView.as_view(), name='query-builder'),

    # import
    url(r'^import/asn$', allocate.OrderView.as_view(), name='wms-orders'),
    url(r'^order/deliveryChallan/(?P<order_id>[\w-])', order.ChallanView.as_view(), name='Order-Delivery-Challan'),
    url(r'^client$', warehouse_views.ClientView.as_view(), name='wms-client'),

    # Shipping label
    url(r'^orders/(?P<pk>[0-9]+)/shipping-label$', shipping.ShippingLabelDetailsView.as_view(), name='wms-shipping-label'),
    # url(r'^stock-check/history', stockcheck.StockCheckHistoryListView.as_view(), name='stock-check-history'),
    url(r'^stock-check/lines', stockcheck.StockCheckLinesListView.as_view(), name='stock-check-lines'),
    url(r'^stock-check/batch', stockcheck.StockCheckBatchView.as_view(), name='stock-check-batch'),
    url(r'^stock-check', stockcheck.StockCheckListView.as_view(), name='stock-check'),

    # Refactored views
    url(r'^inventory/list$', inventory.InventoryListView.as_view(), name='wms-inventory-list'),
]

