import os
import logging
import json
from datetime import timedelta, datetime

from django.db.models import Count, F, Max, Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from rest_framework import status, generics, serializers, views, filters
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from phonenumbers import PhoneNumber
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse

from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED
from blowhorn.oscar.core.loading import get_model
from config.settings import status_pipelines as StatusPipeline

from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import AccountUser
from blowhorn.driver.models import Driver
from blowhorn.users.models import User
from blowhorn.users.permission import IsActive
from blowhorn.utils.functions import utc_to_ist
from blowhorn.wms.constants import INV_TXN_STATUSES, ORDER_LINE_SHIPPED, WMS_RECEIVED, WMS_STOCK_CHECKED
from blowhorn.wms.submodels.inbound import Supplier
from blowhorn.wms.submodels.inventory import WhSite, InventoryTransaction, Inventory
from blowhorn.wms.submodels.location import Client, Location, LocationZone
from blowhorn.wms.mixins.warehouse import WhSiteMixins, ZoneMixins, SiteConfigMixins
from blowhorn.wms.mixins.inbound import ProductGroupImportMixin
from blowhorn.driver import constants as driver_constants
from blowhorn.common import serializers as common_serializer
from blowhorn.wms.submodels.sku import WhSku, TrackingLevel, PackConfig
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.wms.constants import PRODUCT_GROUP_DATA_TO_BE_IMPORTED, INV_STATUSES, ALLOCATION_STATUSES, \
    QUERY_BUILDER_FILTER_ON_FIELD_VALUES, QUERY_BUILDER_ORDER_BY_VALUES, QUERY_BUILDER_OPERATOR_VALUES, \
    QUERY_BUILDER_FIELD_FILTER_VALUES, QUERY_BUILDER_FROM_MODEL_VALUES, PROCESSES, \
    QUERY_BUILDER_WHERE_CONDITION_VALUES, QUERY_BUILDER_ORDERING_VALUES, QUERY_BUILDER_FUNCTIONS_VALUES

from blowhorn.common.utils import export_to_spreadsheet
from blowhorn.wms.admin import ProductGroupImportForm
from blowhorn.common.helper import CommonHelper
from blowhorn.address.models import GenericAddress, Country, City, State
from blowhorn.address.serializer import StateSerializer
from blowhorn.wms.serializers.inventory import InventorySerializer, InventoryTransactionSerializer, \
    StockSummaryListSerializer
from blowhorn.wms.mixins.inventory import InventorySummaryData
from blowhorn.wms.submodels.tasks import TaskType


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

ProductGroup = get_model('wms', 'ProductGroup')
Hub = get_model('address', 'Hub')


class ClientSerializer(serializers.ModelSerializer):
    customer_name = serializers.SerializerMethodField(read_only=True)

    def get_customer_name(self, client):
        if client.customer:
            return client.customer.name
        return '-'

    class Meta:
        model = Client
        exclude = ('modified_by', 'created_by', 'modified_date', 'created_date', 'description',)


def get_user_permission(user):
    customer = CustomerMixin().get_customer(user)
    ac = AccountUser.objects.filter(user=user).first()
    is_org_admin = user.is_customer() or (customer and customer.is_admin(user))
    if customer and (ac and ac.is_admin) or is_org_admin:
        # handle customer admin
        return WhSite.objects.values_list('id', flat=True)
    permission = CustomerMixin().get_permission_from_cache(user, reset_cache=True)

    if not isinstance(permission, dict):
        permission = json.loads(permission)
    # sites, wh_sites = [], []
    site_permission = permission['WMS'].get('sites', None)
    if site_permission:
        return site_permission.get('site', [])
    return []


def get_login_response(user, token):
    sites = get_user_permission(user)
    wh_sites = []
    if sites:
        wh_sites = WhSite.objects.filter(id__in=sites).values('id', 'name')

    response = {
        'name': user.name,
        'mobile': str(user.phone_number),
        'email': user.email,
        'mAuthToken': token,
        'sites': wh_sites

    }
    return response


class DriverFilter(views.APIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET.dict()
        search = data.get('search')
        site_id = data.get('site_id')
        site = WhSite.objects.filter(id=site_id).first()
        if not search:
            drivers = Driver.objects.filter(yard__hub=site.hub,
                                            driver_vehicle__isnull=False,
                                            operating_city__in=[site.city],
                                            ).annotate(phone=F('user__phone_number')).values('id', 'name',
                                                                                             'driver_vehicle',
                                                                                             'phone', 'yard')
        else:
            drivers = Driver.objects.filter(Q(user__phone_number__icontains=search) |
                                            Q(name__icontains=search) | Q(driver_vehicle__icontains=search),
                                            status=StatusPipeline.ACTIVE,
                                            driver_vehicle__isnull=False,
                                            operating_city__in=[site.city],
                                            ).annotate(phone=F('user__phone_number')).values('id', 'name',
                                                                                             'driver_vehicle', 'phone',
                                                                                             'yard').distinct('id')
        return Response(status=status.HTTP_200_OK, data=drivers)


class Activity(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        user = request.user
        data = request.GET
        site_id = data.get('site_id')
        sites = get_user_permission(user)
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        # qc_count = Case(
        #     When(qc_status=['Passed', 'Failed'], then=1),
        #     default=0,
        #     output_field=IntegerField(),
        # )
        time = timezone.now() - timedelta(days=30)
        inv_data = InventoryTransaction.objects.filter(site_id=site_id, created_by=user, created_date__gte=time).values(
            'status').annotate(total=Count('status'), latest_date=Max('created_date'))

        activities = []
        trans_data = ''
        count_ = 0
        for x in inv_data:
            if x.get('status') in ['Passed', 'Failed']:
                count_ += x.get('total', 0)
            elif x.get('status') in [ORDER_LINE_SHIPPED, WMS_RECEIVED, WMS_STOCK_CHECKED]:
                activities.append({
                    'name': x.get('status', ''),
                    'count': x.get('total', 0),
                })
                trans_data = x.get('latest_date')

        activities.append({
            'name': 'Quality Check',
            'count': count_,
        })

        resp = {
            'activities': activities,
            'last_activity': utc_to_ist(trans_data).isoformat() if trans_data else ''
        }
        return Response(status=status.HTTP_200_OK, data=resp)


class InventoryInfo(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        user = request.user
        data = request.GET
        site_id = data.get('site_id')
        tag = data.get('code')
        sites = get_user_permission(user)
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        inv = Inventory.objects.filter(tag=tag, site_id=site_id, wmsmanifestline__isnull=True).first()
        # whsku = None

        # if not inv:
        #     whsku = WhSku.objects.filter(site_id=site_id, name=tag).first()

        if not inv:
            resp = {
                "message": "Invalid Tag Name"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        whsku = inv.sku
        pack_config = whsku.pack_config

        weight = whsku.each_weight or '0' + pack_config.uom if pack_config and pack_config.uom else ''

        resp = {
            "inv_id": inv.id if inv else None,
            "sku_id": whsku.id if whsku else None,
            "sku_name": whsku.name,
            "manufacture_name": str(whsku.supplier),
            "ean": whsku.ean,
            "upc": whsku.upc,
            "uom": whsku.uom,
            "mrp": whsku.mrp or 0,
            "selling_price": whsku.mrp - (whsku.discounts if whsku.discount_type == DISCOUNT_TYPE_FIXED else (
                whsku.mrp * whsku.discounts / 100)) if whsku.mrp else 0,
            "sku_group": str(whsku.product_group),
            "attributes": [{"key": "Product Tag", "value": inv.tag},
                           {"key": "Manufacturer Name", "value": str(whsku.supplier)},
                           {"key": "SKU", "value": str(whsku)},
                           {"key": "Capacity", "value": weight}],
            'sku_image': whsku.file.url if whsku.file else '',
            "qty": inv.qty if inv else 1,

        }

        return Response(status=status.HTTP_200_OK, data=resp)


class Login(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        mobile = data.get('mobile')
        password = data.get('password')
        if not mobile or not password:
            resp = {
                "message": 'Mobile number and password is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, mobile)
        user = User.objects.filter(phone_number=phone_number, is_staff=False).first()
        if user:
            if not user.is_active and user.login_attempt > settings.INVALID_OTP_ATTEMPTS_ALLOWED:
                resp = {
                    "message": 'User is blocked, Please contact support'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

            login_response = User.objects.do_login(
                request=request, email=user.email, password=password)
            if login_response.status_code == status.HTTP_200_OK:
                user = login_response.data.get('user')
                sites = get_user_permission(user)
                if not sites:
                    resp = {
                        "message": "User doesn't have Site Permission"
                    }
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
                token = login_response.data.get('token')
                response = get_login_response(user, token)
                return Response(status=status.HTTP_200_OK, data=response)
            resp = {
                "message": login_response.data
            }
            return Response(status=login_response.status_code, data=resp)

        resp = {
            "message": 'User Not Registered'
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)


class WmsClient(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET
        vendor = data.get('vendor')
        site_id = data.get('site_id')
        customer = CustomerMixin().get_customer(request.user)
        whsite = None
        if site_id:
            whsite = WhSite.objects.filter(id=site_id).first()

        query = Q(customer=customer)
        if whsite:
            query = Q(customer=customer)

        if vendor:
            query = query & Q(name__icontains=vendor)
            client = Client.objects.filter(query).order_by('id')
        else:
            client = Client.objects.filter(query).order_by('id')

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=client,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ClientSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

class WmsView(views.APIView, WhSiteMixins):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        user = request.user
        data = request.query_params
        self._initialize_attributes(user, data)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self._get_site_map_data()
        print('self.zone_list - ', self.zone_list)
        return Response(status=self.status, data=self.response)


class TrackingDetail(views.APIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        user = request.user
        customer = CustomerMixin().get_customer(request.user)
        data = request.GET
        site_id = data.get('site_id')
        code = data.get('code')
        if not code or not site_id:
            resp = {
                "message": "Invalid Code or Site"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        wh_sku_query = Q(name=code) | Q(ean=code) | Q(upc=code)
        wh_sku = WhSku.objects.filter(wh_sku_query).first()
        if not wh_sku:
            resp = {
                "message": "Invalid Sku"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        site = WhSite.objects.filter(id=site_id).only('id').first()
        if site:
            query = Q(customer=customer)
            if user.is_staff:
                query = Q()
            suppliers = Supplier.objects.filter(query).values('id', 'name')
            pack_config = PackConfig.objects.filter(whsku=wh_sku).select_related('tracking_level').first()
            if not pack_config:
                pack_config = PackConfig.objects.filter(whsku__isnull=True).select_related('tracking_level').first()
            if not pack_config:
                resp = {
                    "message": "Tracking Level not defined"
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
            tracking_level = pack_config.tracking_level
            tracking_info = [{
                "name": tracking_level.level_one_name,
                "display": tracking_level.level_one_name,
                "value": 1}]
            if tracking_level.level_two_name:
                tracking_info.append({
                    "name": tracking_level.level_two_name,
                    "display": tracking_level.level_two_name + ' - ' + str(tracking_level.ratio_level_two_one)
                               + ' ' + tracking_level.level_one_name,
                    "value": tracking_level.ratio_level_two_one
                })
            if tracking_level.level_three_name:
                tracking_info.append({
                    "name": tracking_level.level_three_name,
                    "display": tracking_level.level_three_name + ' - ' + str(tracking_level.ratio_level_three_two \
                                                                             * tracking_level.ratio_level_two_one) + ' ' + tracking_level.level_one_name,
                    "value": tracking_level.ratio_level_three_two
                })

            resp = {
                "suppliers": suppliers,
                "tracking_info": tracking_info
            }
            return Response(status=status.HTTP_200_OK, data=resp)
        resp = {
            "message": "No Suppliers Found"
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)


class SiteView(views.APIView, ZoneMixins):
    permission_classes = (IsActive, IsAuthenticated,)

    def post(self, request):
        user = request.user
        data = request.data
        try:
            self._initialize_attributes(user, data)
        except:
            pass
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self._save_site_data()
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        return Response(status=self.status, data=self.response_data)


class SiteConfigView(views.APIView, SiteConfigMixins):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        user = request.user
        self._initialize_attributes(user)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self._get_site_config_data()

        return Response(status=status.HTTP_200_OK, data=self.site_config_data)


@permission_classes([IsAuthenticated])
def product_group_import(request):
    context = {}
    if request.method == 'POST':
        if not request.FILES.get('file', None):
            data_mapping = PRODUCT_GROUP_DATA_TO_BE_IMPORTED
            file = 'template' + '_' + 'product_group' + '_' + \
                   str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
            filename = file + '.xlsx'
            input_data = {
                'data': [],
                'filename': filename,
                'data_mapping_constant': data_mapping,
                'only_format': False
            }

            file_path = export_to_spreadsheet(**input_data)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (fh, filename))

                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                                                      os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                message = 'No file generated'

    if request.FILES.get('file', None):
        form = ProductGroupImportForm(request.POST, request.FILES)
        success, message = ProductGroupImportMixin()._process_import_data(request.FILES, request)

        context = {
            'success': success,
            'message': message,
            'alert': True
        }
    return render(request, 'website/product_import.html', context)


class ProductGroupImportTemplate(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        data_mapping = PRODUCT_GROUP_DATA_TO_BE_IMPORTED
        file = 'template' + '_' + 'product_group' + '_' + \
               str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        input_data = {
            'data': [],
            'filename': filename,
            'data_mapping_constant': data_mapping,
            'only_format': False
        }

        file_path = export_to_spreadsheet(**input_data)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='No file generated')


class ProductGroupImport(views.APIView, ProductGroupImportMixin):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        data = request.data.dict()
        success, message = self._process_import_data(data, request)
        if success:
            return Response(status=status.HTTP_200_OK, data=message)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=message)


class ImportLookupsView(views.APIView, CustomerMixin):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        customer = self.get_customer(request.user)
        if customer:
            product_groups = ProductGroup.objects.filter(customer=customer).values('id', 'name').order_by('name')
            skus = WhSku.objects.filter(client__customer=customer).values('id', 'name').order_by('name')
            suppliers = Supplier.objects.filter(customer=customer).values('id', 'name').order_by('name')
            hub_qs = Hub.objects.filter(customer=customer).values('name', 'id', 'city_id').order_by('name')
            clients = Client.objects.filter(customer=customer).values('id', 'name').order_by('name')
            locations = Location.objects.values('id', 'name').order_by('name')
            pack_configs = PackConfig.objects.filter(customer=customer).values('id', 'name').order_by('name')
            tracking_level_qs = TrackingLevel.objects.filter(customer=customer)
            task_type = TaskType.objects.values('id', 'name').order_by('name')
            tracking_levels = []
            for tracking_level in tracking_level_qs.iterator():
                tl_id = tracking_level.id
                tracking_levels.append({'id': tl_id, 'name': tracking_level.name})
                tracking_levels.append({'id': tl_id, 'name': tracking_level.level_two_name})
                tracking_levels.append({'id': tl_id, 'name': tracking_level.level_three_name})

            cities = City.objects.values('id', 'name')
            state_qs = State.objects.all()
            states = StateSerializer(state_qs, many=True).data
            sites = WhSite.objects.values('id', 'name')
            allocation_statuses = ALLOCATION_STATUSES
            query_builder_lookups = {
                'processes': PROCESSES,
                'from_model': QUERY_BUILDER_FROM_MODEL_VALUES,
                'where_condition': QUERY_BUILDER_WHERE_CONDITION_VALUES,
                'filter': QUERY_BUILDER_FILTER_ON_FIELD_VALUES,
                'field_filter': QUERY_BUILDER_FIELD_FILTER_VALUES,
                'operator': QUERY_BUILDER_OPERATOR_VALUES,
                'order_by': QUERY_BUILDER_ORDER_BY_VALUES,
                'ordering': QUERY_BUILDER_ORDERING_VALUES,
                'functions': QUERY_BUILDER_FUNCTIONS_VALUES
            }

        else:
            product_groups = ProductGroup.objects.none()
            skus = WhSku.objects.none()
            suppliers = Supplier.objects.none()
            hub_qs = Hub.objects.none()
            cities = City.objects.none()
            states = State.objects.none()
            clients = Client.objects.none()
            sites = WhSite.objects.none()
            locations = Location.objects.none()
            pack_configs = PackConfig.objects.none()
            tracking_levels = TrackingLevel.objects.none()
            allocation_statuses = None
            query_builder_lookups = None
            task_type = TaskType.objects.none()

        data = {
            'suppliers': list(suppliers),
            'product_groups': list(product_groups),
            'locations': list(locations),
            'skus': list(skus),
            'hubs': list(hub_qs),
            'cities': list(cities),
            'states': list(states),
            'clients': list(clients),
            'sites': list(sites),
            'pack_configs': list(pack_configs),
            'tracking_levels': list(tracking_levels),
            'task_types': list(task_type),
            'inv_statuses': INV_STATUSES,
            'inv_txn_statuses': INV_TXN_STATUSES,
            'allocation_statuses': allocation_statuses,
            'query_builder_lookups': query_builder_lookups
        }

        return Response(status=status.HTTP_200_OK, data=data)


class HubOnboardView(views.APIView, CustomerMixin):

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        data = request.data
        name = data.get('name', None)
        city = data.get('city', None)
        uom = data.get('uom', None)
        created_address = False
        customer = CustomerMixin().get_customer(request.user)

        if not name:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Hub name is mandatory'))

        if not city:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('City is mandatory'))

        if Hub.objects.filter(name=name).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Hub with given name already exists'))

        address = data.get('address', None)
        if address:
            line1 = address.get('line1', '')
            line2 = address.get('line2', '')
            line3 = address.get('line3', '')
            line4 = address.get('line4', '')
            state = address.get('state', '')
            postcode = address.get('postcode', '')
            country = Country.objects.filter(name='India').first()
            location = address.get('location', None)
            lat = location.get('lat', '')
            lon = location.get('lon', '')

            if not (line1 or line2 or line3):
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Address is mandatory'))

            if not location:
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Location is mandatory'))

            if not postcode:
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Postcode is mandatory'))

            geopoint = CommonHelper.get_geopoint_from_latlong(lat, lon)

            created_address = GenericAddress.objects.create(
                geopoint=geopoint,
                line1=line1,
                line2=line2,
                line3=line3,
                line4=line4,
                state=state,
                postcode=postcode,
                country=country
            )

        city_obj = City.objects.filter(name=city).first()

        if not city_obj:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Incorrect city name provided'))
        if created_address:
            hub = Hub.objects.create(
                name=name,
                city=city_obj,
                address=created_address,
                customer=customer,
                uom=uom,
                is_wms_site=True
            )
            if hub:
                site = WhSite.objects.create(hub=hub,
                                             address=created_address,
                                             name=name,
                                             uom=uom,
                                             city=city_obj,
                                             is_wms_site=True)
                zone = LocationZone.objects.create(name=name,
                                                   site=site)
                location_obj = Location.objects.create(name=name,
                                                       site=site,
                                                       zone=zone)
                return Response(status=status.HTTP_200_OK, data=_('Hub added successfully'))

        return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Hub creation failed'))

class ClientView(views.APIView, CustomerMixin):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET
        search_term = data.get('search_term', None)
        customer = self.get_customer(request.user)
        query = {
            'customer': customer
        }
        if search_term:
            query['name__icontains'] = search_term

        clients = Client.objects.filter(
            **query
        ).values(
            'id', 'name', 'description', 'created_date', 'modified_date'
        )
        _next = data.get('next')
        serialized_data = common_serializer.PaginatedSerializer(
            queryset=clients,
            num=50,
            page=_next
        )
        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def get_response_dict(self, client):
        data = {
            "id": client.id,
            "name": client.name,
            "description": client.description,
            "created_date": client.created_date.isoformat(),
            "modified_date": client.modified_date.isoformat()
        }

        return data

    def post(self, request):
        data = request.data
        customer = CustomerMixin().get_customer(request.user)
        _name = data.get('name', None)
        _description = data.get('description', None)

        if not _name:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                    data="Please provide client name")

        client_name_exists = Client.objects.filter(customer=customer,
                                 name=_name).exists()
        if client_name_exists:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                    data="Client name already exists")

        client = Client.objects.create(customer=customer, name=_name,
                    description=_description)

        data = self.get_response_dict(client)
        return Response(status=status.HTTP_200_OK, data=data)

    def put(self, request):
        data = request.data
        customer = CustomerMixin().get_customer(request.user)
        _id = data.get('id')
        _name = data.get('name', None)
        _description = data.get('description', None)

        if not _name:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                    data="Please provide client name")

        clients = Client.objects.filter(customer=customer, name=_name).exclude(id=_id)
        if clients.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                    data="Client name already exists")

        client = Client.objects.filter(id=_id).first()
        if client:
            client.name = _name
            client.description = _description
            client.modified_by = customer.user
            client.save()
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data="Record doesnt exist")

        data = self.get_response_dict(client)
        return Response(status=status.HTTP_200_OK, data=data)

class StockSummaryCountListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = StockSummaryListSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        data = self.request.query_params
        customer = CustomerMixin().get_customer(self.request.user)
        logger.info('DATA: %s' % data)
        return InventorySummaryData.get_customer_inventory_summary(customer.id, data)

class StockSummaryListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = InventorySerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('created_date', 'id', 'tag', 'mfg_date', 'site__name', 'location__name', 'qty')

    def get_queryset(self):
        data = self.request.query_params
        logger.info('DATA: %s' % data)
        return InventorySummaryData.get_customer_inventory(data)
