import json
from rest_framework import views
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from django.db.models import Q

from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.putaway import PutAwayMixins, StartPutAwayMixins
from blowhorn.users.permission import IsActive
from blowhorn.wms.serializers.putaway import PutawayTaskSerializer
from blowhorn.wms.constants import ACTIVE_WHTASKS_STATUSES, PUTAWAY_TASK, WH_TASK_COMPLETED
from blowhorn.oscar.core.loading import get_model


WhTask = get_model('wms', 'WhTask')


class PutAway(views.APIView, PutAwayMixins):
    permission_classes = (IsActive, IsAuthenticated, )

    def get(self, request):
        user = request.user
        data = request.GET.dict()
        print('data - ', data)
        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        is_directed = data.get('is_directed')

        _params = Q(task_type__name=PUTAWAY_TASK, status__in=ACTIVE_WHTASKS_STATUSES)

        if is_directed == 'true':
            _params = _params & Q(operator=user)
        else:
            _params = _params & Q(operator__isnull=True)

        print('_params - ', _params)
        putaway_task_qs = WhTask.objects.filter(_params).order_by('-priority')
        print('putaway_task_qs - ', putaway_task_qs)

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=putaway_task_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=PutawayTaskSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        user = request.user
        data = request.data
        print('test---------')
        print('data - ', data)
        req_data = json.loads(data.get('request_data'))
        self._initialize_attributes(req_data, user)
        print('error_msg 1 - ', self.error_msg)
        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        if self.wh_task.status == WH_TASK_COMPLETED:
            response_data = PutawayTaskSerializer(instance=self.wh_task).data
            return Response(status=status.HTTP_200_OK, data=response_data)

        self.do_putaway()
        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        response_data = PutawayTaskSerializer(instance=self.wh_task).data
        return Response(status=status.HTTP_200_OK, data=response_data)


class StartPutAway(views.APIView, StartPutAwayMixins):
    permission_classes = (IsActive, IsAuthenticated, )

    def post(self, request):
        user = request.user
        data = request.data
        print('data - ', data)
        req_data = json.loads(data.get('request_data'))
        print('req_data - ', req_data)
        self._initialize_attributes(req_data, user)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self._start_putaway()

        response_data = PutawayTaskSerializer(instance=self.putaway_task).data
        return Response(status=status.HTTP_200_OK, data=response_data)

