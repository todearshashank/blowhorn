from rest_framework import serializers, status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q

from blowhorn.users.permission import IsActive
from blowhorn.wms.submodels.containers import WmsPallet
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.location import LocationImportMixins
from blowhorn.wms.serializers.containers import PalletSerializer


class PalletView(views.APIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET.dict()
        site_id = data.get('site_id')

        name = data.get('name')
        _params = None
        if site_id:
            _params = Q(site_id=site_id)
        if name:
            if _params:
                _params = _params & Q(name__icontains=name)
            else:
                _params = Q(name__icontains=name)

        if _params:
            pallet = WmsPallet.objects.filter(_params)
        else:
            pallet = WmsPallet.objects.all()

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=pallet,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=PalletSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        data = request.data
        pallet_serializer = PalletSerializer(data=data)
        if pallet_serializer.is_valid():
            pallet = pallet_serializer.save()
            resp_data = {"id": pallet.id}
            return Response(status=status.HTTP_200_OK, data=resp_data)

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid pallet data')


# class PalletImport(views.APIView, PalletImportMixins):
#     permission_classes = (IsActive, IsAuthenticated, )

#     def post(self, request):
#         user = request.user
#         data = request.data
#         print('start data - ', data)
#         self._initialize_attributes(data, user)
#         if self.error_msg:
#             return Response(status=self.status, data=self.error_msg)
#         self._create_sku()

#         return Response(status=status.HTTP_200_OK, data=self.response_data)

