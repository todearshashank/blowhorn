import json
from rest_framework import views
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from django.db.models import Q

from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.qc import QcConfigMixin, QcAttributesMixin, QcTaskMixin
from blowhorn.users.permission import IsActive
from blowhorn.wms.serializers.qc import QCAttributesSerializer, QCConfigurationSerializer, QcTaskSerializer
from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.constants import QC_TASK, ACTIVE_WHTASKS_STATUSES
from blowhorn.wms.submodels.inventory import Inventory
from blowhorn.wms.submodels.sku import WhSku
from blowhorn.customer.mixins import CustomerMixin

WhTask = get_model('wms', 'WhTask')
QCAttributes = get_model('wms', 'QCAttributes')
QCConfiguration = get_model('wms', 'QCConfiguration')


class QcConfigView(views.APIView, QcConfigMixin):

    def get(self, request):
        data = request.GET.dict()
        product_group_id = data.get('product_group_id')
        sku_id = data.get('sku_id')
        inventory_id = data.get('inventory_code')
        is_web = data.get('is_web', False)
        inventory = Inventory.objects.filter(id=inventory_id).first()
        qc_attrs = None

        customer = CustomerMixin().get_customer(request.user)
        if inventory:
            if not sku_id:
                sku_id = inventory.sku_id
            if not product_group_id:
                product_group_id = inventory.sku.product_group_id
        elif sku_id:
            sku = WhSku.objects.filter(id=sku_id).first()
            product_group_id = sku.product_group_id

        qc_text = data.get('qc_text')
        _params = None

        if sku_id:
            qc_attrs = QCConfiguration.objects.filter(sku_id=sku_id, product_group__isnull=True,
                                                      is_active=True)
        if not qc_attrs and product_group_id:
            qc_attrs = QCConfiguration.objects.filter(product_group_id=product_group_id, is_active=True)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        if is_web:
            qc_attrs = QCConfiguration.objects.filter(
                Q(product_group__customer=customer) | Q(sku__client__customer=customer) |
                                                      Q(supplier__customer=customer))
            serialized_data = common_serializer.PaginatedSerializer(
                queryset=qc_attrs,
                num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
                page=next_,
                serializer_method=QCConfigurationSerializer,
                context={'request': request})
        else:
            serialized_data = QCConfigurationSerializer(qc_attrs.first())

        if not qc_attrs.exists():
            resp = {
                "message": "Qc not Configured"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        return Response(status=status.HTTP_200_OK, data=serialized_data.data)

    def post(self, request):
        data = request.data
        self._initialize_attributes(data, request)

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self._save_config_data()
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        serialized_data = QCConfigurationSerializer(self.qc_configs).data
        return Response(status=status.HTTP_200_OK, data=serialized_data)


class QcAttributesVIew(views.APIView, QcAttributesMixin):

    def get(self, request):
        data = request.GET.dict()

        qc_text = data.get('qc_text')
        _params = None
        if qc_text:
                _params = Q(name__icontains=qc_text) | Q(text__icontains=qc_text)
        if _params:
            qc_attrs = QCAttributes.objects.filter(_params)
        else:
            qc_attrs = QCAttributes.objects.all()

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=qc_attrs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=QCAttributesSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        data = request.data
        user = request.user
        self._initialize_attributes(data, user)

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self._save_config_data()

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        response_data = QCAttributesSerializer(self.qc_configs, many=True).data
        return Response(status=status.HTTP_200_OK,
                        data=response_data)


class QCTaskView(views.APIView, QcTaskMixin):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data = request.GET.dict()
        task_id = data.get('task_id')
        is_directed = data.get('is_directed')
        # if not task_id:
        #     error_msg = 'Qc Task id is missing'
        #     _status = status.HTTP_400_BAD_REQUEST
        #     return Response(status=_status, data=error_msg)

        _params = Q(task_type__name=QC_TASK) & Q(status__in=ACTIVE_WHTASKS_STATUSES)
        if task_id:
            _params &= Q(id=task_id)
        print('is_directed - ', is_directed)
        print("typeof('is_directed') - ",  type(is_directed))
        if is_directed == 'true':
            _params &= Q(operator=request.user)
        print('_params - ', _params)
        task_qs = WhTask.objects.filter(_params)
        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        qc_serializer = common_serializer.PaginatedSerializer(
            queryset=task_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=QcTaskSerializer,
            context={'request': request})
        # if not task:
        #     error_msg = 'Invalid Task id'
        #     _status = status.HTTP_400_BAD_REQUEST
        #     return Response(status=_status, data=error_msg)

        # qc_serializer = QcTaskSerializer(task_qs, many=True)
        print('qc_serializer.data - ', qc_serializer.data)
        return Response(status=status.HTTP_200_OK, data=qc_serializer.data)

    def post(self, request):
        data = request.data
        user = request.user
        try:
            data = json.loads(data.get('request_data'))
            if not data:
                raise Exception
        except:
            data = request.data
        print('data - ', data)
        self._initialize_attributes(data, user)

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self._check_save_data()

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            'message': 'Success'
        }
        return Response(status=status.HTTP_200_OK, data=_out)

