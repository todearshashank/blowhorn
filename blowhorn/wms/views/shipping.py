import logging
import json
import operator
import requests
from datetime import timedelta
from functools import reduce
from django.conf import settings
from django.db.models import F, Q, Prefetch
from django.db.models.expressions import Case, Value, When
from django.db.models.fields import CharField
from django.forms.models import model_to_dict
from django.db import transaction
from django.db.models import F, Q
from django.utils import timezone
from blowhorn.payment.payment_bank.helpers import cod_orderlevel_save
from rest_framework import status, views
from django.utils import timezone

from rest_framework import generics, status, views, filters
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from blowhorn.apps.driver_app.v3.services.order.scan_orders import ScanOrderMixin
from blowhorn.apps.operation_app.v5.views.user import DriverHandoverItems
from blowhorn.contract.models import TripWorkFlow
from blowhorn.common import serializers as common_serializer
from blowhorn.common.middleware import current_request
from blowhorn.common.pagination import ShipmentSetPagination
from blowhorn.customer.permissions import IsNonIndividualCustomer
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.driver import constants as driver_constants
from blowhorn.driver.models import CashHandover, Driver, DriverRatingDetails
from blowhorn.legacy.util import LegacyCipher
from blowhorn.order.models import Order, Event, WayPoint
from blowhorn.order.serializers.OrderSerializer import ManifestOrderSerializer, WmsOrderSerializer
from blowhorn.order.utils import OrderNumberGenerator
from blowhorn.oscar.core.loading import get_model
from blowhorn.trip.constants import HUB_DELIVERY, ORDER_DELIVERY
from blowhorn.trip.models import Stop, Trip, StopOrders
from blowhorn.trip.models import Trip, StopOrders
from blowhorn.users.permission import IsActive
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.common.middleware import current_request
from blowhorn.wms.constants import ORDER_LINE_PICKED, ORDER_LINE_MANIFESTED, WMS_INV_CREATED, \
    INV_UNLOCKED, ORDER_LINE_SHIPPED, WMS_INV_QTY_UPDATED, INV_LOCKED, WMS_RECEIVED, ALLOCATION_ALLOCATED, \
    ALLOCATION_PARTIALLY_ALLOCATED, ALLOCATION_SHORT
from blowhorn.wms.submodels.returnorder import ReturnOrder, ReturnOrderLine
from blowhorn.wms.views.receiving import Receiving
from blowhorn.wms.mixins.manifest import ManifestMixin, ShippingMixin
from blowhorn.wms.serializers.containers import ManifestSerializer, ManifestListSerializer
from blowhorn.wms.serializers.order import OrderLineSerializer, XDKOrderSerializer
from blowhorn.wms.submodels.containers import WmsContainer, WmsManifest, WmsManifestLine, WmsAllocation, \
    WmsAllocationLine
from blowhorn.wms.submodels.inventory import Inventory
from blowhorn.wms.submodels.location import WhSite, Client
from blowhorn.wms.submodels.sku import PackConfig
from blowhorn.wms.v1.views.wms import get_user_permission
from blowhorn.wms.serializers.shipping import ShippingLabelSerializer
# from blowhorn.wms.tasks import create_asnorder
from blowhorn.wms.v1.views.wms import get_user_permission
from blowhorn.wms.services.stockcheck import StockCheckService
from config.settings.status_pipelines import DRIVER_ACCEPTED, ACTIVE, TRIP_IN_PROGRESS, TRIP_NEW, REACHED_AT_HUB_RTO, \
    ORDER_PICKED
from config.settings import status_pipelines as StatusPipeline

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

OrderLine = get_model('order', 'OrderLine')


class ManifestView(views.APIView, ManifestMixin):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET.dict()

        line_ids = data.get('line_ids')
        site_id = data.get('site_id')
        if line_ids:
            order_lines = OrderLine.objects.filter(id__in=line_ids, status=ORDER_LINE_PICKED,
                                                   order__pickup_hub_id=site_id)
        else:
            order_lines = OrderLine.objects.filter(status=ORDER_LINE_PICKED, order__pickup_hub_id=site_id)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=order_lines,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=OrderLineSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        data = request.data
        self._initialize_attributes(data)
        self.manifest_orders()
        return Response(status=status.HTTP_200_OK, data='Success')


class Manifest(views.APIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get_response(self, manifest, manifest_id):
        invs = Inventory.objects.filter(wmsmanifestline__manifest=manifest).annotate(
            discount=F('wmsmanifestline__order_line__discounts'),
            sell_price=F('wmsmanifestline__order_line__total_item_price'))
        resp = {
            "manifest_id": manifest.id,
            "created_date": manifest.created_date.isoformat(),
            "message": "Manifest Created" if not manifest_id else "Manifest Updated"
        }
        inv_details = []
        for inv in invs:
            inv_details.append({
                "inv_id": inv.id,
                "qty": inv.qty,
                "tag": inv.tag,
                "tracking_level": inv.tracking_level,
                "site_id": inv.site_id,
                "sku_id": inv.sku_id,
                "mrp": inv.sell_price + inv.discount,
                "selling_price": inv.sell_price,
                "sku_name": inv.sku.name,
                "sku_image": inv.sku.file.url if inv.sku.file else ''

            })
        resp.update({"inv_details": inv_details})
        return resp

    def get_qty_in_given_tracking_level(self, inv, tracking_lvl, tracking_level):
        if inv.tracking_level == tracking_level.level_one_name:
            return inv.qty, inv.tracking_level
        if inv.tracking_level == tracking_lvl:
            return inv.qty, inv.tracking_level
        else:
            if inv.tracking_level == tracking_level.level_two_name:
                return tracking_level.ratio_level_two_one * inv.qty, tracking_lvl
            else:
                return tracking_level.ratio_level_two_one * inv.qty * tracking_level.ratio_level_three_two, tracking_lvl

    def post(self, request):
        data = request.data
        user = request.user
        site_id = data.get('site_id')
        number = data.get('order_number')
        sku_details = data.get('sku_details', [])
        sites = get_user_permission(user)
        manifest_id = data.get('manifest_id')
        device_type = request.META.get('HTTP_DEVICE_TYPE', None)
        if not device_type:
            device_type = data.get('device_type', None)

        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        site = WhSite.objects.filter(id=site_id).first()

        # TODO Change this logic to receive (customer_id along with reference number) or
        #  (order number instead of reference number) from the operator app and remove the below piece of code
        #  to fetch customer
        inv_details = sku_details[0].get('inv_details') if sku_details else []
        client_id = inv_details[0].get('client_id') if inv_details else None
        client = Client.objects.filter(id=client_id).first()
        customer = client.customer if client else None
        if not customer:
            resp = {
                "message": "Invalid client ID"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        query = Q(number=number, order_lines__wh_sku__isnull=False) | Q(reference_number=number, customer=customer,
                                                                        order_lines__wh_sku__isnull=False)
        order = Order.objects.filter(query).first()
        if not order or (order and order.status != StatusPipeline.ORDER_NEW):
            resp = {
                "message": "Invalid WMS order number / status for the site %s" % str(site)
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        if WmsManifest.objects.filter(wmsmanifestline__order_line__order__number=order).exists():
            resp = {
                "message": "Order already Manifested"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        manifest, container = None, None
        if manifest_id:
            manifest = WmsManifest.objects.filter(id=manifest_id).first()
            if not manifest or (manifest and manifest.status == ORDER_LINE_SHIPPED):
                resp = {
                    "message": "Manifest Shipped / Invalid Manifest"
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        inv_details = []
        inv_ids = []
        for sku in sku_details:
            container = sku.get('container', None)
            for inv in sku.get('inv_details', []):
                inv_ids.append(inv.get('inv_id'))
                inv.update({'container_name': container})
                inv_details.append(inv)

        invs = Inventory.objects.filter(id__in=inv_ids)
        if not invs:
            resp = {
                "message": "Invalid Inventory"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        error_ids = WmsManifestLine.objects.filter(inventory_id__in=inv_ids).values_list('inventory__tag', flat=True)
        if error_ids:
            resp = {
                "message": "Inventory Manifested already %s" % list(error_ids)
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        error = []
        query = Q(order__number=number) | Q(order__reference_number=number, order__customer=customer)
        line_dict = dict(OrderLine.objects.filter(query).values_list('wh_sku_id', 'id'))

        with transaction.atomic():
            if not manifest:
                manifest = WmsManifest.objects.create(status=ORDER_LINE_MANIFESTED, site_id=site_id)
            for inv in invs:
                for item in inv_details:
                    qty = item['physical_qty']
                    container_name = item.get('container_name', None)

                    pack_config = PackConfig.objects.filter(whsku=inv.sku).select_related('tracking_level').first()
                    if not pack_config:
                        pack_config = PackConfig.objects.filter(whsku__isnull=True).select_related(
                            'tracking_level').first()
                    if not pack_config:
                        resp = {
                            "message": "Tracking Level not defined"
                        }
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
                    tracking_level = pack_config.tracking_level
                    inv_qty, trk_lvl = self.get_qty_in_given_tracking_level(inv, item['tracking_level'],
                                                                            tracking_level)
                    if item['inv_id'] == inv.id and inv_qty >= qty:
                        container_ = None
                        if container_name:
                            container_ = WmsContainer.objects.filter(name=container_name, site_id=site_id,
                                                                     inventory__wmsmanifestline__manifest__status=ORDER_LINE_MANIFESTED).first()
                            if not container_:
                                container_ = WmsContainer.objects.create(name=container_name, site_id=site_id)
                            inv.container = container_
                        if inv_qty > qty:
                            tag = StockCheckService().get_latest_tag(inv.tag)
                            new_inv = Inventory.objects.create(tag=tag, sku_id=inv.sku_id, status=INV_UNLOCKED,
                                                               client=inv.client, qty=qty,
                                                               weight=inv.weight, tracking_level=item['tracking_level'],
                                                               location_id=inv.location_id, site_id=inv.site_id,
                                                               supplier=inv.supplier, pack_config=inv.pack_config,
                                                               container=inv.container)

                            StockCheckService().write_itl(new_inv, qty, ORDER_LINE_MANIFESTED, user,
                                                  tracking_level=item['tracking_level'], tag=tag,
                                                  container=inv.container, order=order, qty_left=inv_qty - qty)
                            WmsManifestLine.objects.create(inventory=new_inv,
                                                           inv_details=json.dumps(model_to_dict(new_inv), indent=4,
                                                                                  sort_keys=True, default=str),
                                                           order_line_id=line_dict[item['sku_id']], manifest=manifest)
                            new_inv.created_by = user
                            new_inv.modified_by = user
                            new_inv.save()
                            StockCheckService().write_itl(inv, inv_qty - qty, WMS_INV_QTY_UPDATED, user,
                                                  tracking_level=item['tracking_level'])
                            inv.qty = inv_qty - qty
                            inv.tracking_level = item['tracking_level']
                        else:
                            StockCheckService().write_itl(inv, qty, ORDER_LINE_MANIFESTED, user,
                                                  container=container_, tracking_level=item['tracking_level'],
                                                  order=order)
                            inv.container = container_
                            WmsManifestLine.objects.create(inventory=inv, manifest=manifest,
                                                           inv_details=json.dumps(model_to_dict(inv), indent=4,
                                                                                  sort_keys=True, default=str),
                                                           order_line_id=line_dict[item['sku_id']])
                        inv.modified_by = user
                        inv.save()

                    elif item['inv_id'] == inv.id:
                        error.append(item['inv_id'])
        if error:
            resp = {
                "message": "Manifest Creation Failed, Invalid Qty for Inventory %s" % str(error),
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        if device_type in ['Android', 'iOS']:
            serializer = ManifestListSerializer(manifest)
            return Response(status=status.HTTP_200_OK, data=serializer.data)
        resp = self.get_response(manifest, manifest_id)
        return Response(status=status.HTTP_200_OK, data=resp)

    def get(self, request):
        data = request.GET.dict()
        site_id = data.get('site_id')
        manifests = WmsManifest.objects.filter(status=ORDER_LINE_MANIFESTED, site_id=site_id).order_by('-modified_date')
        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=manifests,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ManifestSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)

    def put(self, request):
        data = request.data
        user = request.user
        site_id = data.get('site_id')
        sites = get_user_permission(user)
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        order_ids = data.get('order_ids')
        manifest_id = data.get('manifest_id')

        manifest_lines = WmsManifestLine.objects.filter(order_line__order_id__in=order_ids)
        total_items = WmsManifestLine.objects.filter(manifest_id=manifest_id).count()
        if total_items <= len(order_ids):
            resp = {
                "message": "Cannot remove all the orders from the manifest"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        manifest = WmsManifest.objects.filter(id=manifest_id).first()

        if not manifest or (manifest and manifest.status != ORDER_LINE_MANIFESTED):
            resp = {
                "message": "Cannot update, manifest shipped.."
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        with transaction.atomic():
            new_manifest = WmsManifest.objects.create(status=ORDER_LINE_MANIFESTED, site_id=site_id)
            manifest_line_list = []
            for line in manifest_lines:
                manifest_line_list.append(WmsManifestLine(inventory=line.inventory, inv_details=line.inv_details,
                                                          order_line_id=line.order_line_id, manifest=new_manifest))
            WmsManifestLine.objects.bulk_create(manifest_line_list)
            WmsManifestLine.objects.filter(order_line__order_id__in=order_ids).exclude(manifest=new_manifest).delete()
        resp = {
            "message": "Manifest Updated",
            "manifest_id": new_manifest.id,
            "manifest_number": 'MNST-' + str(new_manifest.id)
        }
        return Response(status=status.HTTP_201_CREATED, data=resp)


class ManifestList(views.APIView):

    def get(self, request):
        data = request.GET.dict()
        site_id = data.get('site_id')
        manifests = WmsManifest.objects.filter(status=ORDER_LINE_MANIFESTED, site_id=site_id).order_by('-modified_date')
        next_ = data.get('next') or driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=manifests,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ManifestListSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)


class XDKManifest(views.APIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def post(self, request):
        data = request.data
        user = request.user
        site_id = data.get('site_id')
        driver_id = data.get('driver_id')
        numbers = data.get('order_numbers')
        sites = get_user_permission(user)
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        driver = Driver.objects.filter(id=driver_id, status=ACTIVE).first()
        if not driver:
            resp = {
                "message": "Driver is In-Active"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        with transaction.atomic():
            try:
                trip_ = Trip.objects.filter(status__in=[TRIP_IN_PROGRESS, DRIVER_ACCEPTED, TRIP_NEW],
                                            driver_id=driver_id).first()
                trip_work_flow = TripWorkFlow.objects.filter(is_wms_flow=True).first()
                trip = ScanOrderMixin(
                    context={'request': request}, trip_id=trip_.id if trip_ else None,
                    hub_associate=request.user
                ).create_trip_stops_for_shipment_orders(
                    driver=driver,
                    order_numbers=numbers,
                    trip_workflow=trip_work_flow if trip_work_flow else None,
                    can_ship_wms_order=True
                )

            except Exception as e:
                message = str(e.args[0])
                return Response(status=status.HTTP_400_BAD_REQUEST, data={"message": message})

        resp = {
            "message": "Shipped Successfully",
            "trip_id": trip.id,
            "trip_number": trip.trip_number,
            "driver_name": driver.name,
            "vehicle": driver.driver_vehicle,
            "mobile": str(driver.user.phone_number)

        }
        return Response(status=status.HTTP_200_OK, data=resp)

    def get(self, request):
        data = request.GET.dict()
        site_id = data.get('site_id')
        site = WhSite.objects.filter(id=site_id).first()
        orders = Order.objects.filter(latest_hub=site.hub, status__in=[StatusPipeline.REACHED_AT_HUB,
                                                                                 StatusPipeline.REACHED_AT_HUB_RTO,
                                                                                 StatusPipeline.UNABLE_TO_DELIVER]
                                      ).order_by('-modified_date')
        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=orders,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=XDKOrderSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)


class Shipping(views.APIView, ManifestMixin):
    permission_classes = (IsActive, IsAuthenticated,)

    def check_for_hub_movement(self, order, hub):
        if order.hub == hub:
            return ORDER_DELIVERY
        return HUB_DELIVERY

    def post(self, request):
        data = request.data
        user = request.user
        manifest_ids = data.get('line_ids')
        driver_id = data.get('driver_id')
        driver = Driver.objects.filter(id=driver_id, status=ACTIVE).first()
        if not driver:
            resp = {
                "message": "Driver is In-Active"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        site_id = data.get('site_id')

        invs = Inventory.objects.filter(wmsmanifestline__manifest_id__in=manifest_ids, site_id=site_id,
                                        wmsmanifestline__manifest__status=ORDER_LINE_MANIFESTED)

        if not invs:
            resp = {
                "message": "Invalid Manifest ids"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        inv_ids = []

        with transaction.atomic():
            for i, inv in enumerate(invs):
                inv_ids.append(inv.id)
                StockCheckService().write_itl(inv, inv.qty, ORDER_LINE_SHIPPED, user, tracking_level=inv.tracking_level,
                                      container=inv.container)
            try:
                site = WhSite.objects.filter(id=site_id).first()
                order_numbers = Order.objects.filter(order_lines__wmsmanifestline__manifest__in=manifest_ids
                                                     ).values_list('number', flat=True)
                Order.objects.filter(order_lines__wmsmanifestline__manifest__in=manifest_ids
                                     ).update(status=ORDER_PICKED, latest_hub=site.hub)
                # create event
                trip_ = Trip.objects.filter(status__in=[TRIP_IN_PROGRESS, DRIVER_ACCEPTED, TRIP_NEW],
                                            driver_id=driver_id).first()
                trip_work_flow = TripWorkFlow.objects.filter(is_wms_flow=True).first()
                trip = ScanOrderMixin(
                    context={'request': request}, trip_id=trip_.id if trip_ else None,
                    hub_associate=request.user
                ).create_trip_stops_for_shipment_orders(
                    driver=driver,
                    order_numbers=order_numbers,
                    trip_workflow=trip_work_flow if trip_work_flow else None,
                    can_ship_wms_order=True
                )
                WmsManifest.objects.filter(id__in=manifest_ids).update(status=ORDER_LINE_SHIPPED, trip=trip)
                WmsManifestLine.objects.filter(manifest_id__in=manifest_ids).update(inventory=None)

                # scanordermixin = ScanOrderMixin()
                # scanordermixin.pickup_hub = site.hub
                # stop_type = scanordermixin.check_for_hub_movement(order, trip_work_flow)
                # if stop_type == HUB_DELIVERY:
                    # create_asnorder.apply_async((site_id, manifest_ids), eta=timezone.now() + timedelta(seconds=2))

            except Exception as e:
                message = str(e.args[0])
                return Response(status=status.HTTP_400_BAD_REQUEST, data={"message": message})
            if inv_ids:
                ReturnOrderLine.objects.filter(inventory__id__in=inv_ids).update(inventory=None)
                Inventory.objects.filter(id__in=inv_ids).delete()

        resp = {
            "message": "Shipped Successfully",
            "trip_id": trip.id,
            "trip_number": trip.trip_number,
            "driver_name": driver.name,
            "vehicle": driver.driver_vehicle,
            "mobile": str(driver.user.phone_number)

        }

        return Response(status=status.HTTP_200_OK, data=resp)


class ShippingView(views.APIView, ShippingMixin):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET.dict()

        line_ids = data.get('line_ids')
        site_id = data.get('site_id')
        if line_ids:
            order_lines = OrderLine.objects.filter(id__in=line_ids, status=ORDER_LINE_MANIFESTED,
                                                   order__pickup_hub_id=site_id)
        else:
            order_lines = OrderLine.objects.filter(status=ORDER_LINE_MANIFESTED, order__pickup_hub_id=site_id)

        # TODO: Remove
        order_lines = OrderLine.objects.filter(status=ORDER_LINE_MANIFESTED)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=order_lines,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=OrderLineSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        data = request.data

        self._initialize_attributes(data)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self.ship_orders()
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        resp_msg = {"message": "Success"}
        return Response(status=status.HTTP_200_OK, data=resp_msg)


class OrderDetail(views.APIView):
    def get(self, request):
        user = request.user
        data = request.GET.dict()
        number = data.get('code')
        site_id = data.get('site_id')
        sites = get_user_permission(user)
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        query = Q()
        if number:
            # spar sends reference id other customer sends order number
            query = Q(number=number, order_lines__wh_sku__isnull=False)

        order = Order.objects.filter(query).select_related(
            'shipping_address', 'pickup_address', 'hub', 'customer', 'city')
        order = order.prefetch_related('order_lines').first()

        if not order:
            response = {
                'message': 'WMS Order doesn\'t exist'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        manifest = WmsManifest.objects.filter(wmsmanifestline__order_line__order=order).first()

        if manifest:
            response = {
                'message': 'Order already Manifested'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        items = []
        for line in order.order_lines.all():
            items.append({
                'orderline_id': line.id,
                'sku_name': line.wh_sku.name,
                'qty': line.quantity,
                'sku_id': line.wh_sku_id if line.wh_sku else '',
                'sku_image': line.wh_sku.file.url if line.wh_sku and line.wh_sku.file else '',
                'tracking_level': line.tracking_level if line.tracking_level else ''
            })

        response = {
            'order_id': order.id,
            'reference_number': order.reference_number,
            'hub_id': order.hub_id,
            'hub_name': str(order.hub),
            'awb_number': order.number,
            'status': order.status,
            'delivery_address': order.shipping_address.line1,
            'delivery_postal_code': order.shipping_address.postcode,
            'customer_name': order.shipping_address.first_name,
            'customer_mobile': order.shipping_address.phone_number.national_number
            if order.shipping_address.phone_number else "",
            'item_details': items
        }
        return Response(status=status.HTTP_200_OK, data=response)


class ShipmentOrder(views.APIView):
    permission_classes = (IsActive, IsAuthenticated, )

    def post(self, request):
        data = request.data
        device_type = request.META.get('HTTP_DEVICE_TYPE', None)
        user = request.user
        shipping_details = data.get('shipping_details', None)
        site_id = data.get('site_id')
        customer = CustomerMixin().get_customer(user)
        is_org_admin = True if customer.user == user else False
        is_cod = data.get('is_cod', False)
        cod_amount = data.get('amount', 0)
        sites = get_user_permission(user)
        if not is_org_admin and not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        sku_details = data.get('sku_details')
        manifest_id = data.get('manifest_id')
        if not shipping_details:
            resp = {
                "message": "Shipping Details required"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        item_details = []
        item_price = 0
        for sku in sku_details:
            for inv_detail in sku.get('inv_details', []):
                item_price += inv_detail.get('selling_price', 0)
                item_details.append({
                    "item_name": inv_detail['sku_name'],
                    # "is_whsku": True,
                    "tracking_level": inv_detail['tracking_level'],
                    "client_id": inv_detail['client_id'],
                    "item_quantity": inv_detail['physical_qty'],
                    "item_price_per_each": inv_detail.get('price', 0),
                    "total_item_price": inv_detail.get('selling_price', 0),
                    "discounts": inv_detail.get('mrp', 0) - inv_detail.get('selling_price', 0),
                    "item_category": "",
                    "brand": ""
                })
        if cod_amount and item_price and item_price != cod_amount:
            resp = {
                "message": "Order Total Not matching with Sku price"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        order_number = OrderNumberGenerator().order_number(order_type=settings.SHIPMENT_ORDER_TYPE)
        site = WhSite.objects.filter(id=site_id).first()
        req_data = {
            "reference_number": order_number,
            "awb_number": order_number,
            "pickup_hub": site.hub.name,
            "customer_name": shipping_details['contact']['name'],
            "customer_mobile": shipping_details['contact']['mobile'],
            "delivery_address": shipping_details['address']['line1'],
            "delivery_postal_code": shipping_details['address']['postcode'],
            "delivery_lat": shipping_details['geopoint']['latitude'],
            "delivery_lon": shipping_details['geopoint']['longitude'],
            "item_details": item_details,
            "is_cod": is_cod,
            "cash_on_delivery": cod_amount
        }
        api_key = site.customer.api_key

        with transaction.atomic():
            headers = {'content-type': 'application/json', "api_key": api_key}
            resp = requests.post(settings.HOST_URL+'/api/orders/shipment', json=req_data, headers=headers)
            resp_data = json.loads(resp.content)
            if resp.status_code == status.HTTP_200_OK:
                awb_number = resp_data['message']['awb_number']
                manifest_request = current_request()
                manifest_data = {
                    "sku_details": sku_details,
                    "site_id": site_id,
                    "order_number": awb_number,
                    "manifest_id": manifest_id,
                    "device_type": device_type

                }
                manifest_request.data = manifest_data
                resp = Manifest().post(manifest_request)
                if resp.status_code == status.HTTP_200_OK:
                    resp_data = resp.data
                    return Response(status=status.HTTP_200_OK, data=resp_data)
                else:
                    return resp

            else:
                return Response(status=resp.status_code, data=resp_data)


class ReturnOrderView(views.APIView):
    permission_classes = (IsActive, IsAuthenticated, )

    def get_pending_amt(self, driver, trip, is_return_order):
        numbers = StopOrders.objects.filter(trip=trip).values_list('order__number', flat=True)
        if is_return_order:
            query = Q(driver=driver, stops__trip=trip, status__in=[StatusPipeline.ORDER_RETURNED,
                                                                   StatusPipeline.ORDER_MOVING_TO_HUB_RTO])

        else:
            query = Q(driver=driver, number__in=numbers, status__in=[StatusPipeline.ORDER_MOVING_TO_HUB,
                                                                     StatusPipeline.ORDER_PICKED,
                                                                     StatusPipeline.ORDER_MOVING_TO_HUB_RTO])

            query |= Q(driver=driver, stops__trip=trip, status__in=[StatusPipeline.UNABLE_TO_DELIVER,
                                                                    StatusPipeline.ORDER_PICKED])
        orders = Order.objects.filter(query).distinct('id')
        orders = orders.prefetch_related('order_lines')
        resp = {
                "trip_number": trip.trip_number if trip else '',
                "driver_name": driver.name,
                "driver_id": driver.id,
                "driver_vehicle": driver.driver_vehicle,
                "cod_amt": driver.cod_balance,
            }
        order_details = []
        for order in orders:
            shipping_address = order.shipping_address
            order_details.append({
                "order_number": order.number,
                "status": order.status,
                "reference_number": order.reference_number,
                "customer_name": order.customer.name,
                "order_amt": order.cash_on_delivery,
                "shipping_address": {
                    "address": {
                        "line1": shipping_address.line1,
                        "line3": shipping_address.line3,
                        "line4": shipping_address.state,
                        "postcode": shipping_address.postcode,
                    },
                    "contact": {
                        "mobile": str(shipping_address.phone_number),
                        "name": shipping_address.name

                    }
                },
                "order_lines": [{"line_id": line.id,
                                 "sku_name": line.wh_sku.name if line.wh_sku else line.sku.name,
                                 "sku_image": line.wh_sku.file.url if line.wh_sku and line.wh_sku.file else '',
                                 "is_wh_sku": True if line.wh_sku else False,
                                 "sku_id": line.wh_sku_id,
                                 "selling_price": line.total_item_price,
                                 "tracking_level": line.tracking_level or line.trk_level,
                                 "qty": line.quantity} for line in order.order_lines.all().annotate(
                        trk_level=F('wh_sku__pack_config__tracking_level__level_one_name'))]

            })
        resp.update({"order_details": order_details})
        if driver:
            amount = driver.cod_balance
        return resp, amount

    def create_inventory_from_orderline(self, records, user, site_id, return_order, sku_info):

        for record in records:
            manifestlines = record.wmsmanifestline_set.all()
            location = Receiving().get_or_create_location(site_id, client=record.client)

            for line in manifestlines:
                inv_details = json.loads(line.inv_details)
                tag_ = StockCheckService().get_latest_tag(inv_details['tag'])
                tracking_level = record.tracking_level
                if not tracking_level:
                    tracking_level = record.trk_level
                inv = Inventory.objects.create(tag=tag_, sku_id=inv_details['sku'], qty=sku_info[line.order_line_id][0],
                                               weight=inv_details['weight'], expiry_date=sku_info[line.order_line_id][1],
                                               location=location, site_id=site_id, tracking_level=tracking_level,
                                               pack_config_id=inv_details['pack_config'], client_id=inv_details['client'],
                                               supplier_id=inv_details['supplier'], status=INV_LOCKED)
                inv.created_by = user
                inv.modified_by = user
                inv.save()
                ReturnOrderLine.objects.create(return_order=return_order, inventory=inv, order_line=line.order_line)
                Receiving().write_itl(inv, user, tracking_level, record.pack_config_id, status=WMS_RECEIVED)

        return True

    def get(self, request):

        user = request.user
        data = request.GET.dict()
        site_id = data.get('site_id')
        is_return_order = data.get('is_return_order') in ["true", True]
        driver_id = data.get('driver_id')
        driver_mobile = data.get('driver_mobile')
        driver = None
        if not driver_mobile:
            resp = {
                "message": "Invalid Mobile Number"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        sites = get_user_permission(user)
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        try:
            if len(driver_mobile) > 10:
                information = LegacyCipher.decode(driver_mobile)
                information = json.loads(information)
                driver_mobile = str(information.get('id'))
            driver = DriverHandoverItems().get_driver(driver_mobile='+91'+driver_mobile, driver_id=driver_id)
        except:
            pass

        if not driver:
            resp = {
                "message": "Driver Not found"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        trip = Trip.objects.filter(driver=driver, status__in=[
            StatusPipeline.TRIP_IN_PROGRESS, StatusPipeline.TRIP_ALL_STOPS_DONE]).first()
        resp, amount = self.get_pending_amt(driver, trip, is_return_order)

        if not resp['trip_number'] and not amount and is_return_order:
            resp = {
                "message": "No orders or cash to be collected from driver"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        return Response(status=status.HTTP_200_OK, data=resp)

    def post(self, request):
        data = request.data
        user = request.user
        site_id = data.get('site_id')
        driver_id = data.get('driver_id')
        is_return_order = data.get('is_return_order')
        trip_number = data.get('trip_number')
        sites = get_user_permission(user)
        cod_amt = data.get('cod_amt') or 0
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        driver = Driver.objects.filter(id=driver_id).first()
        order_details = data.get('order_details')
        if is_return_order and float(driver.cod_balance) != float(cod_amt):
            resp = {
                "message": "Driver has to pay %s amount" % (float(driver.cod_balance))
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        line_ids = []
        order_numbers = []
        sku_info = {}
        for order in order_details:
            order_numbers.append(order.get('order_number'))
            for line in order.get('order_lines', []):
                if line.get('is_wh_sku'):
                    line_ids.append(line['line_id'])
                    sku_info.update({line['line_id']: [int(line['physical_qty']) or int(line['qty']),
                                                       line.get('expiry_date', None)]})
        site = WhSite.objects.filter(id=site_id).first()
        orders = Order.objects.filter(number__in=order_numbers)

        with transaction.atomic():
            event_list = []
            rto_orders = []
            at_hub_rto = []
            for order in orders:
                if is_return_order:
                    if order.pickup_hub == site.hub:
                        rto_orders.append(order.number)
                        event_status = StatusPipeline.ORDER_RETURNED_RTO
                    else:
                        at_hub_rto.append(order.number)
                        event_status = StatusPipeline.REACHED_AT_HUB_RTO
                event_list.append(Event(status=event_status if is_return_order else
                                  StatusPipeline.REACHED_AT_HUB, hub_associate=user, order=order, driver=driver,
                                  remarks=str(site.hub)))
            Event.objects.bulk_create(event_list)
            if is_return_order:
                trip = Trip.objects.filter(trip_number=trip_number).first()
                return_order = ReturnOrder.objects.create(trip=trip, site_id=site_id, amount_collected=cod_amt)
                # query = Q(number__in=order_numbers, status=StatusPipeline.ORDER_RETURNED)
                if rto_orders:
                    Order.objects.filter(number__in=rto_orders).update(status=StatusPipeline.ORDER_RETURNED_RTO
                    if is_return_order else StatusPipeline.REACHED_AT_HUB, latest_hub=site.hub)
                if at_hub_rto:
                    Order.objects.filter(number__in=at_hub_rto).update(status=REACHED_AT_HUB_RTO, latest_hub=site.hub)
            else:
                Order.objects.filter(number__in=order_numbers).update(status=StatusPipeline.REACHED_AT_HUB,
                                                                   latest_hub=site.hub)

            if is_return_order:
                if line_ids:
                    records = OrderLine.objects.filter(id__in=line_ids, order__pickup_hub=site.hub).annotate(
                        trk_level=F('wh_sku__pack_config__tracking_level__level_one_name'))
                    self.create_inventory_from_orderline(records, user, site_id, return_order, sku_info)
                if float(driver.cod_balance) == float(cod_amt):
                    # BLOW-1166
                    _trip = Trip.objects.filter(trip_number=trip_number).first()
                    cash_handover = CashHandover.objects.create(driver=driver,
                                        collected_by=user,
                                        amount=cod_amt
                                    )
                    cod_orderlevel_save(
                        driver=driver,
                        trip=_trip,
                        cash_handover=cash_handover
                    )

                    Driver.objects.filter(id=driver_id).update(cod_balance=0)
                resp = {
                    "message": "Return Received"
                }
            else:
                resp = {
                    "message": "Item Received"
                }
        return Response(status=status.HTTP_200_OK, data=resp)


class WmsOrderListView(generics.ListAPIView, CustomerMixin):
    """
    List shipment Order of customer

    * Requires authentication.
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsNonIndividualCustomer)
    serializer_class = WmsOrderSerializer
    pagination_class = ShipmentSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('date_placed',)

    def get_results(self, query):
        try:
            order_qs = Order.objects.filter(query)
            order_qs = order_qs.select_related(
                'driver__user', 'shipping_address', 'city',
                'hub', 'driver', 'pickup_hub'
            )
            order_qs = order_qs.prefetch_related('documents')
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'waypoint_set',
                    queryset=WayPoint.objects.prefetch_related(
                        Prefetch(
                            'stops',
                            queryset=Stop.objects.all().extra(
                                select={
                                    'end_time_null': 'end_time is null '},
                                order_by=['end_time']),
                            to_attr='stops_list'
                        )
                    ),
                    to_attr='waypoints'
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'stops',
                    queryset=Stop.objects.all().extra(
                        select={
                            'end_time_null': 'end_time is null '},
                        order_by=['end_time']),
                    to_attr='stops_list'
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'event_set', queryset=Event.objects.all(), to_attr='events'
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'order_lines',
                    queryset=OrderLine.objects.all().select_related(
                        'wh_sku'
                    ).order_by('item_sequence_no')
                )
            ).prefetch_related(
                Prefetch(
                    'driverratingdetails_set',
                    queryset=DriverRatingDetails.objects.select_related(
                        'remark').only('rating', 'remark', 'customer_remark')
                )
            )
            return order_qs.distinct()
        except BaseException as be:
            logger.info('Failed to query orders: %s' % be)
            return Order.objects.none()

    def get_query(self, params):
        quick_filter_date_delta = params.get('quick_filter', None) or 0
        _status = params.get('status', None)
        city_id = params.get('city_id', None)
        hub_id = params.get('delivery_hub_id', None)
        pickup_hub_id = params.get('pickup_hub_id', None)
        driver_id = params.get('driver_id', None)
        batch_id = params.get('batch_id')
        order_no = params.get('order_no', None)
        order_ref_nos = params.get('order_ref_nos', None)
        start = params.get('start', None)
        end = params.get('end', None)
        reference_number = params.get('reference_number', None)
        customer_reference_number = params.get('customer_reference_number', None)
        allocation_status = params.get('fulfillment_status', None)

        if not start and not end:
            end = timezone.now().date()
            start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))

        customer = self.get_customer(self.request.user)
        query = [
            Q(customer=customer),
            Q(order_type=settings.SHIPMENT_ORDER_TYPE),
            # Q(order_lines__wh_sku__isnull=False),
            ~Q(status=StatusPipeline.DUMMY_STATUS)
        ]
        if _status:
            statuses = _status.split(",")
            status_query = []
            if statuses:
                status_query.append(Q(status__in=statuses))
            query.append(reduce(operator.or_, status_query))

        if allocation_status:
            query.append(Q(allocation_status=allocation_status))

        if reference_number:
            query.append(Q(reference_number=reference_number))

        if customer_reference_number:
            query.append(Q(customer_reference_number=customer_reference_number))

        if city_id:
            query.append(Q(city_id=city_id))

        if hub_id:
            query.append(Q(hub_id=hub_id))

        if pickup_hub_id:
            query.append(Q(pickup_hub_id=pickup_hub_id))

        if driver_id:
            query.append(Q(driver__id=driver_id))

        if order_no:
            query.append(Q(number=order_no))

        if batch_id:
            query.append(Q(batch_id=batch_id))

        if start and end:
            query.append(
                (Q(updated_time__date__gte=start)
                & Q(updated_time__date__lte=end))
                | (Q(created_date__date__gte=start)
                & Q(created_date__date__lte=end))
                | (Q(expected_delivery_time__date__gte=start)
                & Q(expected_delivery_time__date__lte=end))
            )

        if order_ref_nos:
            query.append(
                Q(number=order_ref_nos) |
                Q(reference_number=order_ref_nos) |
                Q(customer_reference_number=order_ref_nos)
            )
        return reduce(operator.and_, query)

    def get_queryset(self):
        query = self.get_query(self.request.query_params)
        return self.get_results(query)


class ManifestOrderList(views.APIView):
    # NOTE: This API is deprecated and will be removed once WMS Order page becomes intact.
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        request_data = request.GET
        manifest_status = request_data.get('status', None)
        allocation_status = request_data.get('fulfillment_status', None)
        search_term = request_data.get('search_term', None)
        query = [Q(order_lines__wh_sku__isnull=False), Q(customer=customer)]

        if allocation_status:
            query.append(Q(allocation_status=allocation_status))

        if manifest_status == 'New':
            query.append(Q(order_lines__wmsmanifestline__isnull=True))
        if manifest_status == 'Manifested':
            query.append(Q(order_lines__wmsmanifestline__manifest__status=ORDER_LINE_MANIFESTED))
        if manifest_status == 'Shipped':
            query.append(Q(order_lines__wmsmanifestline__manifest__status=ORDER_LINE_SHIPPED))

        if search_term:
            query.append((Q(number__icontains=search_term)
                | Q(reference_number__icontains=search_term)))

        query = reduce(operator.and_, query)
        order_qs = Order.objects.select_related('shipping_address').filter(query).distinct()
        next_ = request_data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED
        serialized_data = common_serializer.PaginatedSerializer(
            queryset=order_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ManifestOrderSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)


class ShippingLabelDetailsView(views.APIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = ShippingLabelSerializer

    def get_queryset(self, params):
        return Order.objects.filter(**params)\
            .select_related('shipping_address')\
            .prefetch_related(
                Prefetch(
                    'order_lines',
                    queryset=OrderLine.objects.all().select_related('wh_sku', 'sku')
                )
            ).first()

    def get(self, request, pk=None):
        params = {
            'pk': pk
        }
        order = self.get_queryset(params)
        if not order:
            return Response(
                status=400,
                data='Order not found'
            )

        serializer = self.serializer_class(order)
        return Response(status=200, data=serializer.data)
