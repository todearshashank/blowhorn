import logging
from blowhorn.users.permission import IsActive
from rest_framework import serializers, status, views
from rest_framework.response import Response
from rest_framework.permissions import  IsAuthenticated
from rest_framework import serializers, filters, generics
from django_filters.rest_framework import DjangoFilterBackend

from django import db
from django.db import transaction

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.wms.submodels.tasks import WhTask
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.wms.serializers.whtask import WhTaskSerializer


class WhTaskView(views.APIView):
    """
    WhTask CRUD APIs
    URL:   api/wms/v1/task
    """
    permission_classes = (IsActive, IsAuthenticated,)

    def post(self, request):
        """
        not in use
        :param request:
        :return:
        """

        serializer = WhTaskSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
                logging.info("Location Type created")
                return Response(status=status.HTTP_200_OK, data='Location Type created')
            except db.IntegrityError as e:
                logging.info(
                    "Following error occurred while creating Wh task : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])

        logging.info("Error occurred in Wh Task serializer : %s ", serializer.errors)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)

    def patch(self, request, pk=None):
        """
        not in use
        :param request:
        :param pk:
        :return:
        """

        task = WhTask.objects.filter(pk=pk).first()
        if not task:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Task does not exist')

        serializer = WhTaskSerializer(task, data=request.data, partial=True)
        if serializer.is_valid():
            try:
                serializer.save()
            except db.IntegrityError as e:
                logging.info("Following error occurred while updating Wh task : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])
            return Response(status=status.HTTP_200_OK, data='Details updated')

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Details not updated')

    def delete(self, request, pk=None):

        task = WhTask.objects.filter(pk=pk).first()
        if not task:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Task does not exist')

        try:
            with transaction.atomic():
                WhTask.objects.filter(pk=pk).delete()

        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to delete task: %s' % e)

        return Response(status=status.HTTP_200_OK, data='Task deleted successfully')


class WhTaskListView(generics.ListAPIView, CustomerMixin):
    """
    WhTask get api for customer dashboard
    URL:   api/wms/v1/tasks
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = WhTaskSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('order__number', 'sku__name', 'from_loc__name', 'to_loc__name',
                       'client__name', 'qty', 'tag', 'qc_config__name', 'priority',
                       'operator__name', 'created_by__name', 'modified_by__name',
                       'supplier__name', 'site__name', 'status')
    search_fields = ('order__number', 'order__reference_number', 'from_loc__name', 'to_loc__name',
                     'sku__name', 'tag', 'operator__name', 'created_by__name',
                     'modified_by__name', 'supplier__name', 'site__name')
    filterset_fields = ('status', 'task_type', 'created_date')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return WhTask.objects.filter(client__customer=customer).select_related('order', 'sku', 'client', 'from_loc',
                                'to_loc', 'qc_config', 'operator', 'created_by', 'modified_by', 'supplier', 'site')
