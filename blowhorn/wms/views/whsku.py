import os
import logging
import operator
from datetime import datetime
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.db.models import Q
from django.utils import timezone
from functools import reduce

from rest_framework import serializers, status, views, filters, generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import permission_classes

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.users.permission import IsActive
from blowhorn.wms.models import ProductGroup
from blowhorn.wms.submodels.inventory import Inventory
from blowhorn.wms.submodels.sku import WhSku, PackConfig, TrackingLevel
from blowhorn.wms.submodels.location import Client
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.whsku import WhSkuImportMixins, WhSkuExcelImportMixin
from blowhorn.wms.subadmins.sku import WhSkuImportForm
from blowhorn.common.utils import export_to_spreadsheet
from blowhorn.wms.constants import WH_SKU_DATA_TO_BE_IMPORTED, WHSKU_DATA_TO_BE_EXPORTED
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.wms.serializers.sku import PackConfigSerializer, TrackingLevelSerializer, ProductGroupSerializer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class SkuSerializer(serializers.ModelSerializer):
    product_group_name = serializers.CharField(source='product_group.name', read_only=True)
    supplier_name = serializers.CharField(source='supplier.name', read_only=True)
    client_name = serializers.CharField(source='client.name', read_only=True)
    pack_config_name = serializers.CharField(source='pack_config.name', read_only=True)

    class Meta:
        model = WhSku
        fields = '__all__'


class WhSkuView(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        data = request.GET.dict()
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = CustomerMixin().get_customer(api_key=api_key, user=None) if api_key \
            else CustomerMixin().get_customer(request.user)

        name = data.get('name')
        product_group = data.get('product_group', None)
        site = data.get('site', None)
        client = data.get('client', None)
        supplier = data.get('supplier', None)
        query = Q(client__customer=customer)

        if name:
            query &= Q(name__icontains=name)
        if product_group:
            query &= Q(product_group__name=product_group)
        if site:
            query &= Q(inventory__site__name=site)
        if client:
            query &= Q(client__name=client)
        if supplier:
            query &= Q(supplier__name=supplier)

        skus = WhSku.objects.filter(query).order_by('-id')

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=skus,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=SkuSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        if api_key:
            customer = CustomerMixin().get_customer(api_key=api_key, user=None)
            client_name = data.get('client')
            client = Client.objects.filter(name=client_name, customer=customer).first()
            if not client:
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid Client Name'
                }
                return Response(status=status.HTTP_404_NOT_FOUND, data=_out)
            data['client'] = client.id

        # client-customer
        sku_serializer = SkuSerializer(data=data)
        if sku_serializer.is_valid():
            sku = sku_serializer.save()
            resp_data = {"id": sku.id}
            return Response(status=status.HTTP_200_OK, data=resp_data)
        logger.info('Error when creating SKU for data %s: %s' % (data, sku_serializer.errors))
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to create SKU')

    def put(self, request):
        data = request.data
        sku_id = data.get('id', None)
        sku = WhSku.objects.filter(id=sku_id).first()
        customer = CustomerMixin().get_customer(request.user)

        if not sku:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='SKU ID does not exist')

        if Inventory.objects.filter(sku=sku, sku__client__customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='SKU cannot be updated as it is already mapped to Inventories')

        sku_serializer = SkuSerializer(sku, data=data)
        if sku_serializer.is_valid():
            sku = sku_serializer.save()
            sku.modified_by = request.user
            sku.modified_date = timezone.now()
            sku.save()
            return Response(status=status.HTTP_200_OK, data='Updated the SKU details')

        logger.info('Error when updating the SKU details for customer %s: %s' % (customer, sku_serializer.errors))
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Could not update the SKU details')


class SkuImport(views.APIView, WhSkuImportMixins):
    permission_classes = (IsActive, IsAuthenticated, )

    def post(self, request):
        user = request.user
        data = request.data
        print('start data - ', data)
        self._initialize_attributes(data, user)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self._create_sku()

        return Response(status=status.HTTP_200_OK, data=self.response_data)


def get_filename(prefix):
    date_str = datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT)
    return '%s_%s.xlsx' % (prefix, date_str)


class WhSkuExcelExportView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsActive)
    queryset = WhSku.objects.all()
    serializer_class = SkuSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('name', 'id')

    def get_results(self, request, data):
        user = request.user
        product_group_id = data.get('product_group_id', None)
        search_term = data.get('search_term', None)
        query = [Q()]

        customer = CustomerMixin().get_customer(user)

        if customer:
            query.append(Q(client__customer=customer))
        else:
            return WhSku.objects.none()

        if product_group_id:
            query.append(Q(product_group__id=product_group_id))

        if search_term:
            query.append(Q(name__icontains=search_term))

        final_query = reduce(operator.and_, query)
        return WhSku.objects.filter(final_query)\
            .select_related('product_group')\
            .order_by('name')

    def get_queryset(self):
        data = self.request.data or self.request.GET
        return self.get_results(self.request, data)

    def get(self, request):
        output = request.GET.get('output', None)
        response = {}
        if output == 'xlsx':
            self.pagination_class = None
            filename = get_filename('sku_')
            file_path = export_to_spreadsheet(
                super().get(request).data,
                filename,
                WHSKU_DATA_TO_BE_EXPORTED
            )
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (
                            fh, filename))

                    response = HttpResponse(
                        fh.read(),
                        content_type="application/vnd.ms-excel"
                    )
                    response['Content-Disposition'] = 'inline; filename=%s' % os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                response['errors'] = 'No file generated'
                status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)
        else:
            return super().get(request)


@permission_classes([IsAuthenticated])
def whsku_import(request):
    context = {}
    if request.method == 'POST':
        if not request.FILES.get('file', None):
            data_mapping = WH_SKU_DATA_TO_BE_IMPORTED
            file = 'template' + '_' + 'sku' + '_' + \
                   str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
            filename = file + '.xlsx'
            input_data = {
                'data': [],
                'filename': filename,
                'data_mapping_constant': data_mapping,
                'only_format': False
            }

            file_path = export_to_spreadsheet(**input_data)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (fh, filename))

                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                                                      os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                message = 'No file generated'

    if request.FILES.get('file', None):
        form = WhSkuImportForm(request.POST, request.FILES)
        success, message = WhSkuExcelImportMixin()._process_import_data(request.FILES, request)
        context = {
            'success': success,
            'message': message,
            'alert': True
        }
    return render(request, 'website/whsku_import.html', context)


class WhSkuExcelFileFormat(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data_mapping = WH_SKU_DATA_TO_BE_IMPORTED
        file = 'template' + '_' + 'sku' + '_' + \
               str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        input_data = {
            'data': [],
            'filename': filename,
            'data_mapping_constant': data_mapping,
            'only_format': False
        }

        file_path = export_to_spreadsheet(**input_data)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='No file generated')


class WhSkuExcelImport(views.APIView, WhSkuExcelImportMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data.dict()
        success, message = self._process_import_data(data, request)
        if success:
            return Response(status=status.HTTP_200_OK, data=message)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=message)


class PackConfigView(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        data = request.GET.dict()

        pack_config_id = data.get('id', None)
        name = data.get('name', None)

        query = Q(customer=customer)
        if name:
            query &= Q(name__icontains=name)
        if pack_config_id:
            query &= Q(id=pack_config_id)

        pack_config_qs = PackConfig.objects.filter(query)
        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=pack_config_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=PackConfigSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)

    def post(self, request):
        customer = CustomerMixin().get_customer(request.user)
        data = request.data
        data['customer'] = customer.id
        name = data.get('name', None)

        if PackConfig.objects.filter(name=name, customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Name already exists')

        pack_config_serializer = PackConfigSerializer(data=data)
        if pack_config_serializer.is_valid():
            pack_config = pack_config_serializer.save()
            resp_data = {"id": pack_config.id}
            return Response(status=status.HTTP_200_OK, data=resp_data)
        logger.info('Error creating pack config for customer %s : %s' % (customer, pack_config_serializer.errors))
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Could not create Pack Config')

    def put(self, request):
        customer = CustomerMixin().get_customer(request.user)
        data = request.data
        pack_config_id = data.get('id', None)
        pack_config = PackConfig.objects.filter(id=pack_config_id).first()

        if Inventory.objects.filter(pack_config_id=pack_config_id, client__customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Pack Config cannot be updated as it is already mapped to Inventories')

        if WhSku.objects.filter(pack_config_id=pack_config_id, client__customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Pack Config cannot be updated as it is already mapped to SKUs')

        if PackConfig.objects.filter(name=data.get('name', None), customer=customer).exclude(
                id=pack_config_id).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Given name already exists')

        pack_config_serializer = PackConfigSerializer(pack_config, data=data)
        if pack_config_serializer.is_valid():
            pack_config = pack_config_serializer.save()
            pack_config.modified_by = request.user
            pack_config.modified_date = timezone.now()
            pack_config.save()
            return Response(status=status.HTTP_200_OK, data='Pack Config Updated')

        logger.info('Error updating pack config for customer %s : %s' % (customer, pack_config_serializer.errors))
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to updated Pack Config')

    def delete(self, request):
        customer = CustomerMixin().get_customer(request.user)
        data = request.GET
        pack_config_id = data.get('id', None)

        if Inventory.objects.filter(pack_config_id=pack_config_id, client__customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Pack Config cannot be deleted as it is already mapped to Inventories')

        if WhSku.objects.filter(pack_config_id=pack_config_id, client__customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Pack Config cannot be deleted as it is already mapped to SKUs')

        try:
            pack_config = PackConfig.objects.filter(id=pack_config_id).delete()
        except BaseException as e:
            logger.info('Error when deleting pack config: %s' % e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Could not delete Pack Config')

        return Response(status=status.HTTP_200_OK, data='Deleted the Pack Config')


class TrackingLevelView(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data = request.GET.dict()
        customer = CustomerMixin().get_customer(request.user)
        name = data.get('name', None)
        tracking_level_id = data.get('id', None)

        query = Q(customer=customer)
        if name:
            query &= Q(name__icontains=name)
        if tracking_level_id:
            query &= Q(id=tracking_level_id)

        tracking_level_qs = TrackingLevel.objects.filter(query)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=tracking_level_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=TrackingLevelSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)

    def post(self, request):
        data = request.data
        customer = CustomerMixin().get_customer(request.user)
        data['customer'] = customer.id
        name = data.get('name', None)
        level_one_name = data.get('level_one_name', None)
        level_two_name = data.get('level_two_name', None)
        level_three_name = data.get('level_three_name', None)

        if TrackingLevel.objects.filter(name=name, level_one_name=name, level_two_name=name, level_three_name=name,
                                        customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Name already exists')

        if TrackingLevel.objects.filter(name=level_one_name, level_one_name=level_one_name,
                                        level_two_name=level_one_name, level_three_name=level_one_name,
                                        customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Level One Name already exists')

        if TrackingLevel.objects.filter(name=level_two_name, level_one_name=level_two_name,
                                        level_two_name=level_two_name, level_three_name=level_three_name,
                                        customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Level Two Name already exists')

        if TrackingLevel.objects.filter(name=level_three_name, level_one_name=level_three_name,
                                        level_two_name=level_three_name, level_three_name=level_three_name,
                                        customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Level Three Name already exists')

        tracking_level_serializer = TrackingLevelSerializer(data=data)
        if tracking_level_serializer.is_valid():
            tracking_level = tracking_level_serializer.save()
            response = {'id': tracking_level.id}
            return Response(status=status.HTTP_200_OK, data=response)
        logger.info('Error creating Tracking Level for customer %s : %s' % (customer, tracking_level_serializer.errors))
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Could not create tracking level')

    def put(self, request):
        data = request.data
        customer = CustomerMixin().get_customer(request.user)
        tracking_level_id = data.get('id', None)
        name = data.get('name', None)
        level_one_name = data.get('level_one_name', None)
        level_two_name = data.get('level_two_name', None)
        level_three_name = data.get('level_three_name', None)

        tracking_level = TrackingLevel.objects.filter(id=tracking_level_id).first()
        if not tracking_level:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Tracking Level ID does not exist')

        if TrackingLevel.objects.filter(name=name, level_one_name=name, level_two_name=name, level_three_name=name,
                                        customer=customer).exclude(id=tracking_level_id).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Name already exists')

        if TrackingLevel.objects.filter(name=level_one_name, level_one_name=level_one_name,
                                        level_two_name=level_one_name, level_three_name=level_one_name,
                                        customer=customer).exclude(id=tracking_level_id).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Level One Name already exists')

        if TrackingLevel.objects.filter(name=level_two_name, level_one_name=level_two_name,
                                        level_two_name=level_two_name, level_three_name=level_three_name,
                                        customer=customer).exclude(id=tracking_level_id).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Level Two Name already exists')

        if TrackingLevel.objects.filter(name=level_three_name, level_one_name=level_three_name,
                                        level_two_name=level_three_name, level_three_name=level_three_name,
                                        customer=customer).exclude(id=tracking_level_id).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Level Three Name already exists')

        if Inventory.objects.filter(
            Q(tracking_level=name) | Q(tracking_level=level_one_name) | Q(tracking_level=level_two_name) | Q(
                tracking_level=level_three_name), client__customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Tracking level cannot be updated as it is already mapped to Inventories')

        tracking_level_serializer = TrackingLevelSerializer(tracking_level, data=data)
        if tracking_level_serializer.is_valid():
            tracking_level = tracking_level_serializer.save()
            tracking_level.modified_by = request.user
            tracking_level.modified_date = request.user
            tracking_level.save()
            return Response(status=status.HTTP_200_OK, data='Updated the Tracking Level')

        logger.info('Error updating Tracking Level for customer %s : %s' % (customer, tracking_level_serializer.errors))
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Could not update Tracking Level')

    def delete(self, request):
        data = request.GET
        customer = CustomerMixin().get_customer(request.user)
        tracking_level_id = data.get('id', None)
        tracking_level = TrackingLevel.objects.get(id=tracking_level_id)
        name = data.get('name', None)
        level_one_name = tracking_level.level_one_name
        level_two_name = tracking_level.level_two_name
        level_three_name = tracking_level.level_three_name

        if Inventory.objects.filter(
            Q(tracking_level=name) | Q(tracking_level=level_one_name) | Q(tracking_level=level_two_name) | Q(
                tracking_level=level_three_name), client__customer=customer).exists():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Tracking level cannot be deleted as it is already mapped to Inventories')

        try:
            tracking_level = TrackingLevel.objects.filter(id=tracking_level_id).delete()
        except BaseException as e:
            logger.info('Error when deleting Tracking Level: %s' % e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Could not delete the Tracking Level')

        return Response(status=status.HTTP_200_OK, data='Deleted the Tracking Level')


class ClientSKUView(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data = request.GET.dict()
        customer = CustomerMixin().get_customer(request.user)
        client = data.get('client', None)
        site = data.get('site', None)

        if not (client or site):
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Client/Site is required')

        query = Q(client__customer=customer)
        if client:
            query &= Q(client__name=client)
        if site:
            query &= Q(inventory__site__name=site)

        sku_qs = WhSku.objects.filter(query).distinct('id')

        if not sku_qs.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='No SKUs found for the given Client/Site')

        data = []
        for sku in sku_qs.iterator():
            tracking_levels = [sku.pack_config.tracking_level.level_one_name,
                               sku.pack_config.tracking_level.level_two_name,
                               sku.pack_config.tracking_level.level_three_name]
            data.append(
                {
                    'sku_name': sku.name,
                    'tracking_levels': tracking_levels
                }
            )

        return Response(status=status.HTTP_200_OK, data=data)


class ProductGroupView(views.APIView):
    permission_classes = (AllowAny, )

    def get(self, request):
        data = request.GET.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = CustomerMixin().get_customer(api_key=api_key, user=None) if api_key \
            else CustomerMixin().get_customer(request.user)

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED, data="Unauthorized")

        name = data.get('name', None)
        description = data.get('description, None')
        query = Q(customer=customer)

        if name:
            query &= Q(name=name)
        if description:
            query &= Q(description=description)

        product_groups = ProductGroup.objects.filter(query).order_by('-id')

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=product_groups,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ProductGroupSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = CustomerMixin().get_customer(api_key=api_key, user=None) if api_key \
            else CustomerMixin().get_customer(request.user)

        if not customer:
            response = {
                "status": "FAIL",
                "message": "Unauthorized"
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        data['customer'] = customer.id
        product_group_serializer = ProductGroupSerializer(data=data, context={'customer': customer})
        if product_group_serializer.is_valid():
            product_group = product_group_serializer.save()
            response = {
                "status": "PASS",
                "message": "Successfully created Product Group - %s" % product_group.name
            }
            return Response(status=status.HTTP_200_OK, data=response)

        logger.error('Error creating Product Group for customer %s - %s' % (customer, product_group_serializer.errors))

        response = {
            "status": "FAIL",
            "message": "Could not create Product Group"
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

    def put(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = CustomerMixin().get_customer(api_key=api_key, user=None) if api_key \
            else CustomerMixin().get_customer(request.user)

        if not customer:
            response = {
                "status": "FAIL",
                "message": "Unauthorized"
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        product_group = ProductGroup.objects.filter(id=data.get('id', None)).first()
        if not product_group:
            response = {
                "status": "FAIL",
                "message": "Invalid Product Group ID"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        product_group_serializer = ProductGroupSerializer(product_group, data=data, context={'customer': customer})
        if product_group_serializer.is_valid():
            product_group = product_group_serializer.save()
            response = {
                "status": "PASS",
                "message": "Successfully updated Product Group - %s" % product_group.name
            }
            return Response(status=status.HTTP_200_OK,
                            data=response)

        logger.error('Error updating Product Group for customer %s - %s' % (customer, product_group_serializer.errors))

        response = {
            "status": "FAIL",
            "message": "Could not update Product Group"
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response)
