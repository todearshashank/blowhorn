import os
import logging
from datetime import datetime
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from django.utils.translation import gettext_lazy as _
from django import db
from django.db import IntegrityError, transaction
from django.db.models import Count, Subquery, OuterRef, IntegerField
from django.db.models.functions import Coalesce

from rest_framework import serializers, status, views, generics, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend

from blowhorn.customer.models import Customer
from blowhorn.order.models import Dimension
from blowhorn.users.permission import IsActive
from blowhorn.wms.models import ProductGroup
from blowhorn.wms.submodels.location import Location, LocationType, StorageType, LocationZone, Client, WhSite
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.location import LocationImportMixins, LocationTypeImportMixins, ClientImportMixins, \
    StorageTypeImportMixins, LocationZoneImportMixins
from blowhorn.wms.serializers.location import LocationSerializer, LocationZoneSerializer, LocationTypeSerializer
from blowhorn.wms.submodels.location import Location, WhSite, Client
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.location import LocationImportMixins
from blowhorn.wms.serializers.location import LocationSerializer, WhSiteSerializer, ClientSerializer
from blowhorn.common.utils import export_to_spreadsheet
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.wms.constants import CLIENT_DATA_TO_BE_IMPORTED
from blowhorn.wms.mixins.location import ClientImportMixin, SiteMixin

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.wms.constants import CLIENT_DATA_TO_BE_IMPORTED, LOC_UNLOCKED, INV_UNLOCKED, INV_LOCKED
from blowhorn.wms.mixins.location import ClientImportMixin, SiteMixin, LocationZoneMixin
from blowhorn.wms.submodels.inventory import Inventory
from blowhorn.wms.subadmins.location import ClientImportForm
from blowhorn.customer.mixins import CustomerMixin


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class LocationView(views.APIView, CustomerMixin):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET.dict()
        wh_id = data.get('wh_id')

        name = data.get('name')
        _params = None
        if wh_id:
            _params = Q(site__hub_id=wh_id)
        if name:
            if _params:
                _params = _params & Q(name__icontains=name)
            else:
                _params = Q(name__icontains=name)

        if _params:
            location = Location.objects.filter(_params)
        else:
            location = Location.objects.all()

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=location,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=LocationSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):

        serializer = LocationSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
                logging.info("Location created")
                return Response(status=status.HTTP_200_OK, data='Location created')
            except db.IntegrityError as e:
                logging.info(
                    "Following error occurred while creating location : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid location data')

    def patch(self, request, pk=None):

        location = Location.objects.filter(pk=pk).first()
        if not location:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Location does not exist')

        serializer = LocationSerializer(location, data=request.data, partial=True)

        if serializer.is_valid():
            try:
                serializer.save()
            except db.IntegrityError as e:
                logging.info("Following error occurred while updating location : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])
            return Response(status=status.HTTP_200_OK, data='Details updated')

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Details not updated')

    def delete(self, request, pk=None):

        location = Location.objects.filter(pk=pk, status=LOC_UNLOCKED).exclude(is_deleted=True).first()
        if not location:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Locked location does not exist')

        inventory = Inventory.objects.filter(Q(location_id=pk) & (Q(
            status=INV_LOCKED) | Q(status=INV_UNLOCKED, qty__gt=0))).first()

        if inventory:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Cannot delete location with inventory')

        Location.objects.filter(pk=pk).update(is_deleted=True)

        return Response(status=status.HTTP_200_OK, data='Location deleted successfully')


class LocationListView(generics.ListAPIView, CustomerMixin):
    """
    Location get api for customer dashboard
    URL:   api/wms/v2/location/list
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = LocationSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('name', 'created_date', 'site__name', 'status', 'zone__name',
                       'volume', 'weight', 'modified_date')
    search_fields = ('name', 'site__name', 'created_by__name', 'modified_by__name')
    filterset_fields = ('zone_id', 'site_id', 'status')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return Location.objects.exclude(
            is_deleted=True).select_related('site', 'zone', 'created_by', 'modified_by')


class LocationImport(views.APIView, LocationImportMixins):
    permission_classes = (IsActive, IsAuthenticated, )

    def post(self, request):
        user = request.user
        data = request.data
        print('start data - ', data)
        self._initialize_attributes(data, user)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self._create_sku()

        return Response(status=status.HTTP_200_OK, data=self.response_data)


class LocationTypeImport(views.APIView, LocationTypeImportMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        self.initialize_attributes(data, customer)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self.create_location_type_data()

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class LocationZoneImport(views.APIView, LocationZoneImportMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        self.initialize_attributes(data, customer)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self.create_location_zone_data()

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class StorageTypeImport(views.APIView, StorageTypeImportMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        self.initialize_attributes(data, customer)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self.create_storage_data()

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class ClientImport(views.APIView, ClientImportMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')
        self.initialize_attributes(data, customer)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self._create_client_data()
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)
@permission_classes([IsAuthenticated])
def client_import(request):
    context = {}
    if request.method == 'POST':
        if not request.FILES.get('file', None):
            data_mapping = CLIENT_DATA_TO_BE_IMPORTED
            file = 'template' + '_' + 'client' + '_' + \
                   str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
            filename = file + '.xlsx'
            input_data = {
                'data': [],
                'filename': filename,
                'data_mapping_constant': data_mapping,
                'only_format': False
            }

            file_path = export_to_spreadsheet(**input_data)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (fh, filename))

                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                                                      os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                message = 'No file generated'

    if request.FILES.get('file', None):
        form = ClientImportForm(request.POST, request.FILES)
        success, message = ClientImportMixin()._process_import_data(request.FILES, request)

        context = {
            'success': success,
            'message': message,
            'alert': True
        }
    return render(request, 'website/client_import.html', context)


class ClientImportExcelTemplate(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data_mapping = CLIENT_DATA_TO_BE_IMPORTED
        file = 'template' + '_' + 'client' + '_' + \
               str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        input_data = {
            'data': [],
            'filename': filename,
            'data_mapping_constant': data_mapping,
            'only_format': False
        }

        file_path = export_to_spreadsheet(**input_data)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='No file generated')


class ClientImportView(views.APIView, ClientImportMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data.dict()
        success, message = self._process_import_data(data, request)
        if success:
            return Response(status=status.HTTP_200_OK, data=message)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=message)


class WhSiteListView(generics.ListAPIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data = request.GET.dict()
        search_term = data.get('search_term', None)
        query = Q()
        if search_term:
            query = (Q(name__icontains=search_term) | Q(hub__name__icontains=search_term) | Q(
                city__name__icontains=search_term) | Q(store_code__icontains=search_term))

        site_qs = WhSite.objects.filter(query)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=site_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=WhSiteSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)


class WhSiteView(views.APIView, SiteMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data
        self._initialize_attributes(data, request.user)
        if self.error_msg:
            return Response(status=self.status_code, data=self.error_msg)

        self._save_site_data()
        return Response(status=status.HTTP_200_OK, data='Site created successfully')


class ClientView(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data = request.GET.dict()
        name = data.get('name', None)
        customer = CustomerMixin().get_customer(request.user)

        if name:
            client_qs = Client.objects.filter(customer=customer, name=name)
        else:
            client_qs = Client.objects.filter(customer=customer).all()

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=client_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ClientSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)

    def post(self, request):
        data = request.data
        client_serializer = ClientSerializer(data=data)
        if client_serializer.is_valid():
            client = client_serializer.save()
            response = {'id': client.id}
            return Response(status=status.HTTP_200_OK, data=response)
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Could not create Client. %s' % client_serializer.errors)


class LocationZoneView(views.APIView):
    """
      Location Zone CRUD Apis
      URL:   api/wms/v2/location/zone
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = LocationZoneSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
                logging.info("Location Zone created")
                return Response(status=status.HTTP_200_OK, data='Zone created')
            except db.IntegrityError as e:
                logging.info(
                    "Following error occurred while creating location zone : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])

        logging.info("Error occurred in Location Zone serializer : %s ", serializer.errors)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)

    def patch(self, request, pk=None):

        location_zone = LocationZone.objects.filter(pk=pk).first()
        if not location_zone:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Zone does not exist')

        serializer = LocationZoneSerializer(location_zone, data=request.data, partial=True)
        if serializer.is_valid():
            try:
                serializer.save()
            except db.IntegrityError as e:
                logging.info("Following error occurred while updating location zone : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])
            return Response(status=status.HTTP_200_OK, data='Details updated')

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Details not updated')

    def delete(self, request, pk=None):

        location_zone = LocationZone.objects.filter(pk=pk).first()
        if not location_zone:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Zone does not exist')

        location = Location.objects.filter(zone_id=pk, status=LOC_UNLOCKED)
        if location.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Cannot delete zone for unlocked location')

        try:
            with transaction.atomic():
                LocationZoneMixin().soft_delete_zone_data(location_zone, request.user)

        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to delete location zone: %s' % e)

        return Response(status=status.HTTP_200_OK, data='Zone deleted successfully')


class LocationZoneListView(generics.ListAPIView, CustomerMixin):
    """
    Location zone get api for customer dashboard
    URL:   api/wms/v2/location/zone/list
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = LocationZoneSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('name', 'created_date', 'site__name', 'status', 'modified_date')
    search_fields = ('name', 'site__name', 'created_by__name', 'modified_by__name')
    filterset_fields = ('site_id', 'status')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return LocationZone.objects.filter(is_deleted=False).select_related('site').annotate(
            num_active_locations=Coalesce(Subquery(Location.objects.filter(
                zone_id=OuterRef('pk'), status=LOC_UNLOCKED).values('zone_id').annotate(
                count=Count('pk')).values('count'), output_field=IntegerField()), 0))


class LocationTypeView(views.APIView, CustomerMixin):
    """
         Location Type CRUD Apis
         URL:   api/wms/v2/location/type
       """
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        customer = CustomerMixin().get_customer(request.user)
        request.data['customer'] = customer.id

        serializer = LocationTypeSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
                logging.info("Location Type created")
                return Response(status=status.HTTP_200_OK, data='Location Type created')
            except db.IntegrityError as e:
                logging.info(
                    "Following error occurred while creating location type : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])

        logging.info("Error occurred in Location Type serializer : %s ", serializer.errors)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)

    def patch(self, request, pk=None):

        location_type = LocationType.objects.filter(pk=pk).first()
        if not location_type:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Zone does not exist')

        serializer = LocationTypeSerializer(location_type, data=request.data, partial=True)
        if serializer.is_valid():
            try:
                serializer.save()
            except db.IntegrityError as e:
                logging.info("Following error occurred while updating location type : %s", e)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])
            return Response(status=status.HTTP_200_OK, data='Details updated')

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Details not updated')

    def delete(self, request, pk=None):

        """
        Currently not in use
        """

        location_type = LocationType.objects.filter(pk=pk).first()
        if not location_type:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Loacation Type does not exist')

        try:
            with transaction.atomic():
                LocationType.objects.filter(pk=pk).delete()

        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Failed to delete location type: %s' % e)

        return Response(status=status.HTTP_200_OK, data='Location Type deleted successfully')


class LocationTypeListView(generics.ListAPIView, CustomerMixin):
    """
    Location type get api for customer dashboard
    URL:   api/wms/v2/location/type/list
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = LocationTypeSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('name', 'created_date', 'customer__name', 'modified_date')
    search_fields = ('name', 'customer__name', 'created_by__name', 'modified_by__name',
                     'loc_prefix_code')
    filterset_fields = ('customer',)

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return LocationType.objects.filter(customer=customer).select_related('customer')
