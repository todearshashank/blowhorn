import json
import logging
import operator
from datetime import datetime
from functools import reduce
from django_filters.rest_framework import DjangoFilterBackend
from django.db import transaction
from rest_framework import views, filters, generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from django.contrib.humanize.templatetags.humanize import intcomma

from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import Customer
from blowhorn.users.permission import IsActive
from blowhorn.driver import constants as driver_constants
from blowhorn.common import serializers as common_serializer
from blowhorn.wms.mixins.qc import Supplier
from blowhorn.wms.mixins.receiving import BlindReceivingMixins, ImportSupplierMixins, PurchaseOrderMixins, \
    ASNOrderMixins
from blowhorn.wms.submodels.location import Client
from blowhorn.wms.submodels.receiving import GRNLineItems, PurchaseOrder, ASNOrder, ASNLineItems, POLineItems
from blowhorn.wms.submodels.sku import WhSite, WhSku
from blowhorn.wms.mixins.receiving import BlindReceivingMixins
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.submodels.location import Location, Client, LocationType, LocationZone
from blowhorn.wms.submodels.receiving import PurchaseOrder, ASNOrder, ASNLineItems, POLineItems
from blowhorn.wms.serializers.receiving import PurchaseOrderSerializer
from blowhorn.shopify.tasks import adjust_inventory_on_shopify
from blowhorn.customer.models import PartnerCustomer
from blowhorn.shopify.models import ShopifyLog
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.wms.constants import ASN_EXPIRED, ASN_NEW_STATUS, \
    ASN_IN_PROGRESS, ASN_COMPLETED, ASN_END_STATUSES, \
    INV_UNLOCKED, WMS_QTY_ADD, WMS_RECEIVED
from blowhorn.wms.v1.views.wms import get_user_permission
from blowhorn.common.utils import generate_pdf
from django.core.files.base import ContentFile

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class Receiving(views.APIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def write_itl(self, inv,  user, tracking_level, pack_config_id, container=None, status=None, purchase_order=None):
        inv_trans = InventoryTransaction.objects.create(
            tag=inv.tag,
            from_loc=inv.location,
            to_loc=inv.location,
            sku_id=inv.sku_id,
            client=inv.client,
            qty=inv.qty,
            site=inv.site,
            supplier=inv.supplier,
            status=status,
            container=container,
            tracking_level=tracking_level,
            pack_config_id=pack_config_id,
            created_by=user,
            modified_by=user,
            operation=WMS_QTY_ADD,
            qty_diff = inv.qty,
            qty_left = inv.qty

        )
        inv_trans.created_by = user
        inv_trans.modified_by = user
        inv_trans.save()

        if purchase_order:
            parm = {
                "sku_id" : inv.sku_id,
                "qty" : inv.qty,
                "purchase_order" : purchase_order
            }
            grn_items = GRNLineItems.objects.filter(**parm)
            if not grn_items:
                GRNLineItems.objects.create(**parm)
            else:
                grn_item = grn_items.first()
                grn_item.qty = (grn_item.qty if grn_items.qty else 0) + inv.qty
                grn_item.save()

    def get_or_create_location(self, site_id, client=None):
        if not client:
            loc_name = 'DEFAULT-'
            client, is_created = Client.objects.get_or_create(name='DEFAULT')
        else:
            loc_name = str(client) + '-'
        loc_type, is_created = LocationType.objects.get_or_create(name='DEFAULT')
        zone, is_created = LocationZone.objects.get_or_create(name='DEFAULT-ZNE-'+str(site_id), site_id=site_id)
        location, is_created = Location.objects.get_or_create(name=loc_name + str(site_id), client=client,  zone=zone,
                                                              site_id=site_id, status=INV_UNLOCKED, loc_type=loc_type)
        return location

    def create_inventory(self, record, sku_id, user, purchase_order=None):
        tag = record.get('tag')
        expiry_date = record.get('expiry_date')
        weight = record.get('weight', 0)
        wh_sku = WhSku.objects.filter(id=sku_id).first()
        pack_config_id = wh_sku.pack_config_id
        client_id = record.get('vendor_id', None)
        qty = record.get('qty')
        site_id = record.get('site_id')
        supplier_id = record.get('supplier_id')
        tracking_level = record.get('tracking_level')
        mfg_date = record.get('mfg_date', None)
        is_shopify_synced = False

        try:
            pcm = PartnerCustomer.objects.filter(
                    customer_id=wh_sku.client.customer.id,
                    is_active=True, is_wms_flow=True).first()
            if pcm:
                if wh_sku.shopify_inventory_id:
                    is_shopify_synced = True
                    adjust_inventory_on_shopify.delay(wh_sku.shopify_inventory_id,
                            wh_sku.client.customer.id, qty)
                else:
                    ShopifyLog.objects.create(
                        reference_number=sku_id,
                        request_body=json.dumps(record),
                        reason="Sku ID %s is missing" % (sku_id),
                        customer=wh_sku.client.customer,
                        category='Variant Missing',
                        source='Inventory'
                    )

            client = None
            if client_id:
                client = Client.objects.filter(id=client_id).first()
            location = self.get_or_create_location(site_id, client=client)
            inv = Inventory.objects.create(tag=tag, sku_id=sku_id, qty=qty,
                        status=INV_UNLOCKED, is_shopify_synced=is_shopify_synced,
                        weight=weight, expiry_date=expiry_date, location=location,
                        site_id=site_id, pack_config_id=pack_config_id, client_id=client_id,
                        supplier_id=supplier_id, tracking_level=tracking_level, mfg_date=mfg_date)
            inv.created_by = user
            inv.modified_by = user
            inv.save()

            self.write_itl(inv, user, tracking_level, pack_config_id, status=WMS_RECEIVED, purchase_order=purchase_order)
            if purchase_order:
                _grn_item = {
                    "name" : wh_sku.name,
                    "size" : wh_sku.pack_config.tracking_level.level_two_name or wh_sku.pack_config.tracking_level.level_one_name,
                    "quantity" : 0,
                    "price" : float(wh_sku.mrp or 0) * float(qty),
                    "delivered_quantity" : qty
                }
                self.total_price += _grn_item['price']
                self.line_items.append(_grn_item)

        except Exception as e:
            error_msg = 'Inventory creation failed'
            logger.info('Receiving - Inventory creation failed - %s' % e.args[0])
            return False, False, error_msg
        return True, inv, "Received Successfully"

    def post(self, request):
        user = request.user
        data = request.data
        site_id = data.get('site_id')
        code = data.get('code')
        tag = data.get('tag')
        supplier_id = data.get('supplier_id', None)
        sites = get_user_permission(user)
        self.line_items = []
        self.total_price = 0

        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have WMS Permissions"
            }
            return Response(status=status.HTTP_403_FORBIDDEN, data=resp)
        if Inventory.objects.filter(tag=tag, site_id=site_id).exists():
            resp = {
                "message": "Duplicate TagId"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        sku = WhSku.objects.filter(name=code).first()

        if not sku:
            resp = {
                "message": "Invalid Sku"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        with transaction.atomic():
            purchase_order = PurchaseOrder.objects.create(
                status=ASN_NEW_STATUS, delivery_date=datetime.now(),
                po_number=datetime.now().strftime("%d%m%y%H%M%S%f"),
                site_id=site_id, supplier_id=supplier_id)

            is_created, inv, msg = self.create_inventory(data, sku.id, user, purchase_order)
            resp = {
                "message": msg,
                "inv_tag": inv.tag if inv else ''
            }

            if is_created:
                data = {}
                _supplier_name = inv.supplier.name if inv.supplier else 'Default Supplier'
                if _supplier_name == 'Default Supplier':
                    _supplier_name = inv.sku.client.customer.name

                grn_number = 'RN%s' % datetime.now().strftime("%d%m%y%H%M%S%f")
                data['grn_number'] = grn_number
                data['date'] = datetime.now().strftime('%d %b %Y %I:%M %p')
                data['items'] = self.line_items
                data['supplier_name'] = _supplier_name
                data['receiver_name'] = user.name
                data['total_price'] = intcomma(round(self.total_price,2))

                bytes = generate_pdf('pdf/grn/grn.html', data)
                purchase_order.total_price = data['total_price']
                purchase_order.receiving_number = grn_number
                if bytes:
                    filename = '%s.pdf' % grn_number
                    purchase_order.file.save(filename, ContentFile(bytes), save=True)
                purchase_order.status = ASN_COMPLETED
            else:
                purchase_order.status = ASN_EXPIRED

            purchase_order.save()
            if is_created:
                return Response(status=status.HTTP_200_OK, data=resp)

        return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

class BlindReceiving(views.APIView, BlindReceivingMixins):
    permission_classes = (IsActive, IsAuthenticated,)

    def post(self, request):
        data = request.data

        self.is_grn, self.is_asn = False, False
        self.purchase_order = PurchaseOrder.objects.create(
                status=ASN_NEW_STATUS, delivery_date=datetime.now(),
                po_number=datetime.now().strftime("%d%m%y%H%M%S%f"))

        self._initialize_attributes(data, request)
        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        for i in data:
            self._receive_inventory(i)
            print('self.error_msg - ', self.error_msg)
            if self.error_msg:
                self.purchase_order.status=ASN_EXPIRED
                self.purchase_order.save()
                return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        _file = self.generate_grn_document(request.user)
        message = {
            "file": _file,
            "message": 'Item received'
        }
        return Response(status=status.HTTP_200_OK, data=message)


class GRNListView(generics.ListAPIView):
    permission_classes = (IsActive, IsAuthenticated)
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    search_fields = ('po_number', 'receiving_number', 'invoice_number')
    ordering_fields = ('created_date', 'status', 'delivery_date')
    filterset_fields = ('site_id', 'supplier_id')
    serializer_class = PurchaseOrderSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        customer = CustomerMixin().get_customer(self.request.user)
        query = [Q(supplier__customer=customer)]
        start = self.request.GET.get('start', None)
        end = self.request.GET.get('end', None)
        if start and end:
            query.append(
                Q(delivery_date__date__gte=start)
                & Q(delivery_date__date__lte=end)
            )

        query = reduce(operator.and_, query)
        return PurchaseOrder.objects.filter(query)

class GRN(views.APIView, BlindReceivingMixins):
    permission_classes = (IsActive, IsAuthenticated)

    def post(self, request):
        data = request.data

        po_number = data[0].get('po_number')
        self.is_grn, self.is_asn = True, False
        self.purchase_order = PurchaseOrder.objects.filter(po_number=po_number,
                                                           status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS]).first()

        if not self.purchase_order:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid PO Number')

        self._initialize_attributes(data)
        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        for i in data:
            self._receive_inventory(i)
            if self.error_msg:
                return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        po_line_status = POLineItems.objects.exclude(purchase_order=self.purchase_order,
                                                     status__in=ASN_END_STATUSES).exists()
        if po_line_status:
            self.purchase_order.status = ASN_IN_PROGRESS
        else:
            self.purchase_order.status = ASN_COMPLETED

        self.purchase_order.save()
        message = {
            "message": 'Item received for po number %s' % (po_number)
        }
        return Response(status=status.HTTP_200_OK, data=message)


class ASNView(views.APIView, BlindReceivingMixins):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET.dict()
        inv_number = data.get('invoice_number')
        site_id = data.get('site_id')

        if inv_number:
            asn_order = ASNOrder.objects.filter(invoice_number__icontains=inv_number,
                                                status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS])
        else:
            asn_order = ASNOrder.objects.filter(status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS])

        _params = Q(status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS])

        if inv_number:
            _params = _params & Q(invoice_number__icontains=inv_number)
        if site_id:
            _params = _params & Q(site_id=site_id)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=asn_order,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ASNOrderSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        data = request.data
        inv_number = data[0].get('invoice_number')
        self.is_grn, self.is_asn = False, True
        self.asn_order = ASNOrder.objects.filter(invoice_number=inv_number,
                                                 status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS]
                                                 ).first()

        if not self.asn_order:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid Invoice Number')

        self._initialize_attributes(data)
        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        for i in data:
            self._receive_inventory(i)
            if self.error_msg:
                print('self.error_msg - ', self.error_msg)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        asn_line_status = ASNLineItems.objects.filter(asn_order=self.asn_order).exclude(
            status__in=ASN_END_STATUSES).exists()
        if asn_line_status:
            self.asn_order.status = ASN_IN_PROGRESS
        else:
            self.asn_order.status = ASN_COMPLETED
        self.asn_order.save()

        return Response(status=status.HTTP_200_OK, data='SUCCESS')


class ImportSupplier(views.APIView, ImportSupplierMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        self._initialize_attributes(data,customer)

        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        self._save_data()
        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class ImportPurchaseOrder(views.APIView, PurchaseOrderMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')
        self.initialize_attributes(data, customer)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self.create_po_data()
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class ImportASNOrder(views.APIView, ASNOrderMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')
        self.initialize_attributes(data, customer)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)

        self.create_asn_data()
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class ASNOrderStatus(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        data = request.GET.dict()
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        inv_number = data.get('invoice_number')
        site_id = data.get('site_id')

        if inv_number:
            asn_order = ASNOrder.objects.filter(invoice_number__icontains=inv_number,
                                                status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS])
        else:
            asn_order = ASNOrder.objects.filter(status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS])

        _params = Q(status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS])

        if inv_number:
            _params = _params & Q(invoice_number__icontains=inv_number)
        if site_id:
            _params = _params & Q(site_id=site_id)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=asn_order,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=ASNOrderSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)


class ASNOrderUpdate(views.APIView):
    permission_classes = (AllowAny,)

    def put(self, request):
        data = request.data

        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        self.trace_log = {}
        asn_lines_list = []
        asn_ = data.get('asn_order')
        if asn_:
            for i, asn in enumerate(asn_):
                invoice_number = asn.get('invoice_number')
                site_id = asn.get('site')
                inv_num = ASNOrder.objects.filter(invoice_number=invoice_number)
                site_query = WhSite.objects.filter(id=site_id)
                if not inv_num:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data='Invoice number not found')
                if not site_query:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data='Site not found')
                expiry_date = asn.get('expiry_date')
                description = asn.get('description')
                delivery_date = asn.get('delivery_date')
                total_price = asn.get('total_price')
                batch = asn.get('batch')
                serial_number = asn.get('serial_number')
                status_ = asn.get('status')
                client_name = asn.get('client')
                client = None
                if client_name:
                    client = Client.objects.filter(name=client_name).first()
                if not client_name:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data='Client not found')
                supplier_name = asn.get('supplier')
                supplier = None
                if supplier_name:
                    supplier = Supplier.objects.filter(name=supplier_name).first()
                if not supplier_name:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data='Supplier not found')

                asn_order_update = ASNOrder.objects.update(
                    invoice_number=invoice_number,
                    description=description,
                    delivery_date=delivery_date,
                    expiry_date=expiry_date,
                    site=site_id,
                    supplier=supplier,
                    total_price=total_price,
                    batch=batch,
                    serial_number=serial_number,
                    status=status_,
                    client=client,
                )
                existing_orderlines = ASNLineItems.objects.filter(
                    asn_order__id__in=inv_num
                )
                self.trace_log.update({
                    'existing_orderlines_in_asn': [
                        o.sku.__str__() for o in existing_orderlines
                    ]
                })
                if existing_orderlines.exists():
                    deleted_records = existing_orderlines.delete()
                    self.trace_log.update({
                        'orderlines_deleted': deleted_records[0]
                    })

                line_item = asn.get('line_items')
                if line_item:
                    for j, line_items in enumerate(line_item):
                        sku_name = line_items.get('sku')
                        if not sku_name:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data='Sku not found')
                        sku = WhSku.objects.filter(name=sku_name).first()
                        if not sku:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data=' Invalid Sku')

                        qty = line_items.get('qty')
                        status_ = line_items.get('status')
                        if not status_:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data='Status not found')

                        qty_received = line_items.get('qty_received')
                        price_per_qty_ = line_items.get('price_per_qty')
                        item_desc = line_items.get('item_desc')
                        asn_order = line_items.get('asn_order_id')
                        if not asn_order:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data='asn order not found')

                        lines_item_dict = {
                            'sku': sku,
                            'qty': qty,
                            'status': status_,
                            'qty_received': qty_received,
                            'price_per_qty': price_per_qty_,
                            'item_desc': item_desc,
                            'asn_order_id': asn_order_update
                        }
                        asn_lines_list.append(ASNLineItems(**lines_item_dict))

                ASNLineItems.objects.bulk_create(asn_lines_list)

        return Response(status=status.HTTP_200_OK, data='Successfully updated')


class POOrderUpdate(views.APIView):
    permission_classes = (AllowAny,)

    def put(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')
        po_lines_list = []
        self.trace_log = {}
        po_ = data.get('purchase_order')
        if po_:
            for i, po in enumerate(po_):
                site_id = po.get('site')
                site_query = WhSite.objects.filter(id=site_id)
                if not site_query:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data='Site not found')
                expiry_date = po.get('expiry_date')
                invoice_number = po.get('invoice_number')
                description = po.get('description')
                delivery_date = po.get('delivery_date')
                total_price = po.get('total_price')
                batch = po.get('batch')
                serial_number = po.get('serial_number')
                status_ = po.get('status')
                supplier_name = po.get('supplier')
                supplier = None
                if supplier_name:
                    supplier = Supplier.objects.filter(name=supplier_name).first()
                if not supplier_name:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data='Supplier not found')

                po_order_update = PurchaseOrder.objects.update(
                    invoice_number=invoice_number,
                    description=description,
                    delivery_date=delivery_date,
                    expiry_date=expiry_date,
                    site=site_id,
                    supplier=supplier,
                    total_price=total_price,
                    batch=batch,
                    serial_number=serial_number,
                    status=status_
                )
                existing_orderlines = POLineItems.objects.filter(
                    purchase_order__site__in=site_query
                )
                self.trace_log.update({
                    'existing_orderlines_in_po': [
                        o.sku.__str__() for o in existing_orderlines
                    ]
                })
                if existing_orderlines.exists():
                    deleted_records = existing_orderlines.delete()
                    self.trace_log.update({
                        'orderlines_deleted': deleted_records[0]
                    })

                line_item = po.get('line_items')
                if line_item:
                    for j, line_items in enumerate(line_item):
                        sku_name = line_items.get('sku')
                        if not sku_name:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data='Sku not found')
                        sku = WhSku.objects.filter(name=sku_name).first()
                        if not sku:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data=' Invalid Sku')

                        qty = line_items.get('qty')
                        status_ = line_items.get('status')
                        if not status_:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data='Status not found')

                        qty_received = line_items.get('qty_received')
                        price_per_qty_ = line_items.get('price_per_qty')
                        item_desc = line_items.get('item_desc')
                        purchase_order = line_items.get('purchase_order')
                        if not purchase_order:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data='purchase order not found')

                        lines_item_dict = {
                            'sku': sku,
                            'qty': qty,
                            'status': status_,
                            'qty_received': qty_received,
                            'price_per_qty': price_per_qty_,
                            'item_desc': item_desc,
                            'purchase_order_id': po_order_update
                        }
                        po_lines_list.append(POLineItems(**lines_item_dict))

            POLineItems.objects.bulk_create(po_lines_list)
        return Response(status=status.HTTP_200_OK, data='Successfully updated')


class PurchaseOrderStatus(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        data = request.GET.dict()

        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        po_number = data.get('po_number')
        site_id = data.get('site_id')

        _params = Q(status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS])

        if po_number:
            _params = _params & Q(po_number__icontains=po_number)
        if site_id:
            _params = _params & Q(site_id=site_id)

        po_order = PurchaseOrder.objects.filter(_params)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=po_order,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=PurchaseOrderSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)
        resp = {
            "message": "Asn Received"
        }
        return Response(status=status.HTTP_200_OK, data=resp)
