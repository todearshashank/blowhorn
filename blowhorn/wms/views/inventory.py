import os
import pytz
import logging
import operator
from functools import reduce
from datetime import datetime, timedelta
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from django.db.models import Q, F

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, views, generics, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from blowhorn.customer.models import Customer
from blowhorn.users.permission import IsActive
from blowhorn.wms.mixins.inventory import DataImportMixins
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.serializers.inventory import InventorySerializer, InventoryTransactionSerializer, \
    InventoryExportSerializer
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.inventory import InventoryImportMixin
from blowhorn.wms.subadmins.inventory import InventoryImportForm
from blowhorn.common.utils import export_to_spreadsheet
from blowhorn.wms.constants import INVENTORY_DATA_TO_BE_IMPORTED, INVENTORY_DATA_TO_BE_EXPORTED, \
    INVENTORY_TRANSACTION_DATA_TO_BE_EXPORTED
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.address.models import State

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class InventoryView(views.APIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        data = request.GET.dict()
        customer = CustomerMixin().get_customer(request.user)
        _parm = {
            'client__customer': customer
        }
        tag = data.get('tag', None)
        sku_id = data.get('sku', None)
        client_id = data.get('client', None)
        site_id = data.get('site', None)

        if tag:
            _parm['tag__icontains'] = tag
        if sku_id:
            _parm['sku_id'] = sku_id
        if client_id:
            _parm['client_id'] = client_id
        if site_id:
            _parm['site_id'] = site_id

        inv = Inventory.objects.filter(**_parm).order_by('-id')
        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED
        serialized_data = common_serializer.PaginatedSerializer(
            queryset=inv,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=InventorySerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)


class InventoryListView(generics.ListAPIView, CustomerMixin):
    """
    Web GET /api/wms/v1/inventory/list
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = InventorySerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('sku__name', 'tag', 'qty', 'status', 'weight', 'mfg_date', 'expiry_date',
        'created_date', 'site__name', 'tracking_level')
    search_fields = ('sku__name', 'site__name', 'supplier__name', 'tag')
    filterset_fields = ('sku_id', 'client_id', 'site_id', 'status')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        mfg_date_start = self.request.GET.get('mfg_date_start', None)
        mfg_date_end = self.request.GET.get('mfg_date_end', None)
        query = [Q(client__customer=customer)]
        if mfg_date_start and mfg_date_end:
            query.append(
                Q(mfg_date__date__gte=mfg_date_start)
                & Q(mfg_date__date__lte=mfg_date_end)
            )

        expiry_date_start = self.request.GET.get('expiry_date_start', None)
        expiry_date_end = self.request.GET.get('expiry_date_end', None)
        if expiry_date_start and expiry_date_end:
            query.append(
                Q(expiry_date__date__gte=expiry_date_start)
                & Q(expiry_date__date__lte=expiry_date_end)
            )

        query = reduce(operator.and_, query)
        return Inventory.objects.filter(query)\
            .select_related('sku', 'client', 'supplier', 'created_by', 'site')


class ITLView(views.APIView):
    permission_classes = (IsActive, IsAuthenticated, )

    def get(self, request):
        data = request.GET.dict()
        site_id = data.get('site_id')
        try:
            itl = InventoryTransaction.objects.filter(site_id=site_id)
        except Exception as e:
            _out = {
                'status': "FAILED",
                'message': e.args[0]
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=itl,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=InventoryTransactionSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)


class DataImport(views.APIView, DataImportMixins):
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        self.initialize_attributes(data, customer)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self.create_inventory_data()

        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        _out = {
            "message": "Success"
        }
        return Response(status=status.HTTP_200_OK, data=_out)


class InventoryStatus(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        data = request.GET.dict()

        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        tag = data.get('tag')
        if tag:
            inv = Inventory.objects.filter(tag__icontains=tag)
        else:
            inv = Inventory.objects.all()

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=inv,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=InventorySerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

class InventoryTransactions(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        data = request.GET.dict()

        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        tag = data.get('tag')
        if tag:
            itl = InventoryTransaction.objects.filter(tag__icontains=tag)
        else:
            itl = InventoryTransaction.objects.all()

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=itl,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=InventoryTransactionSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)




@permission_classes([IsAuthenticated])
def inventory_import(request):

    context = {}
    if request.method == 'POST':
        if not request.FILES.get('file', None):
            data_mapping = INVENTORY_DATA_TO_BE_IMPORTED
            file = 'template' + '_' + 'inventory' + '_' + \
                   str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
            filename = file + '.xlsx'
            input_data = {
                'data': [],
                'filename': filename,
                'data_mapping_constant': data_mapping,
                'only_format': False
            }

            file_path = export_to_spreadsheet(**input_data)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (fh, filename))

                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                                                      os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                message = 'No file generated'

    if request.FILES.get('file', None):
        form = InventoryImportForm(request.POST, request.FILES)
        success, message = InventoryImportMixin()._process_import_data(request.FILES, request)

        context = {
            'success': success,
            'message': message,
            'alert': True
        }
    return render(request, 'website/inventory_import.html', context)


class InventoryExcelFileFormat(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data_mapping = INVENTORY_DATA_TO_BE_IMPORTED
        file = 'template' + '_' + 'inventory' + '_' + \
               str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        input_data = {
            'data': [],
            'filename': filename,
            'data_mapping_constant': data_mapping,
            'only_format': False
        }

        file_path = export_to_spreadsheet(**input_data)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='No file generated')


class InventoryExcelImport(views.APIView, InventoryImportMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data.dict()
        success, message = self._process_import_data(data, request)
        if success:
            return Response(status=status.HTTP_200_OK, data=message)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=message)


class InventoryTransactionListView(generics.ListCreateAPIView):
    """
    Web GET /api/wms/v1/inventory/transaction
    """
    permission_classes = (IsAuthenticated,)
    queryset = InventoryTransaction.objects.all()
    serializer_class = InventoryTransactionSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend,)
    ordering_fields = ('sku__name', 'tag', 'qty', 'status', 'mfg_date',
        'created_date', 'site__name', 'tracking_level', 'client_name',
        'from_loc__name', 'supplier__name', 'pack_config__name',
        'source', 'created_by__name', 'modified_by__name')
    search_fields = ('sku__name', 'tracking_level', 'tag')
    filterset_fields = ('sku_id', 'client_id', 'site_id', 'status', 'tag')

    def get_queryset(self):
        data = self.request.data or self.request.GET
        user = self.request.user
        from_date = data.get('created_date_start', None)
        to_date = data.get('created_date_end', None)
        is_summary = data.get('is_summary', False)

        query = [Q()]
        customer = CustomerMixin().get_customer(user)
        if customer:
            query.append(
                Q(sku__client__customer=customer) | Q(client__customer=customer) | Q(
                    supplier__customer=customer
                )
            )
        else:
            return InventoryTransaction.objects.none()

        if is_summary:
            query.append(Q(operation__isnull=False))

        if from_date:
            from_date = datetime.strptime(from_date, '%d-%m-%Y') - timedelta(hours=5.5)
            from_date = from_date.astimezone(tz=pytz.utc)
            query.append(Q(created_date__gte=from_date))

        if to_date:
            to_date += ' 23:59:59'
            to_date = datetime.strptime(to_date, '%d-%m-%Y %H:%M:%S') - timedelta(hours=5.5)
            to_date = to_date.astimezone(tz=pytz.utc)
            query.append(Q(created_date__lte=to_date))

        final_query = reduce(operator.and_, query)
        return InventoryTransaction.objects\
            .select_related('sku', 'client', 'site', 'created_by', 'modified_by', 'pack_config')\
            .filter(final_query)


def get_filename(prefix):
    date_str = datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT)
    return '%s_%s.xlsx' % (prefix, date_str)


class InventoryExport(generics.ListAPIView):
    queryset = Inventory.objects.all()
    serializer_class = InventoryExportSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('-created_date',)
    permission_classes = (IsAuthenticated,)

    def get_results(self, request, data):
        state = data.get('state', None)
        city = data.get('city', None)
        site = data.get('site', None)
        _status = data.get('status', None)
        qc_status = data.get('qc_status', None)
        received_status = data.get('received_status', None)
        product_group = data.get('product_group', None)
        search_term = data.get('search_term', None)
        user = request.user

        remarks = []
        query = [Q()]

        customer = CustomerMixin().get_customer(user)

        if customer:
            query.append(Q(client__customer=customer))
        else:
            return Inventory.objects.none()

        if _status:
            query.append(Q(status=_status))

        if qc_status:
            remarks.append(qc_status)

        if received_status:
            remarks.append(received_status)

        if remarks:
            query.append(Q(remarks__in=remarks))

        if state:
            cities = State.objects.filter(pk=state).values('cities__id')
            query.append(Q(site__city_id__in=cities))

        if city:
            query.append(Q(site__city_id=city))

        if product_group:
            query.append(Q(sku__product_group__name=product_group))

        if site:
            query.append(Q(site_id=site))

        if search_term:
            query.append(Q(tag__icontains=search_term))

        final_query = reduce(operator.and_, query)
        return Inventory.objects.filter(final_query).order_by('-created_date')

    def get_queryset(self):
        data = self.request.data or self.request.GET
        return self.get_results(self.request, data)

    def get(self, request):
        user = request.user
        output = request.GET.get('output', None)
        response = {}
        if output == 'xlsx':
            self.pagination_class = None
            filename = get_filename('inventory_')
            file_path = export_to_spreadsheet(
                super().get(request).data,
                filename,
                INVENTORY_DATA_TO_BE_EXPORTED
            )
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (
                        fh, filename))

                    response = HttpResponse(
                        fh.read(),
                        content_type="application/vnd.ms-excel"
                    )
                    response['Content-Disposition'] = 'inline; filename=%s' % os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                response['errors'] = 'No file generated'
                status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)
        else:
            return super().get(request)


class InventoryTransactionExport(generics.ListAPIView):

    queryset = InventoryTransaction.objects.all()
    serializer_class = InventoryTransactionSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('-created_date', 'id')
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        data = self.request.data or self.request.GET
        user = self.request.user
        tag = data.get('tag', None)
        from_loc_id = data.get('from_loc_id', None)
        to_loc_id = data.get('to_loc_id', None)
        search_term = data.get('search_term', None)
        product_group = data.get('product_group', None)
        supplier_id = data.get('supplier_id', None)
        status = data.get('status', None)
        from_date = data.get('from_date', None)
        to_date = data.get('to_date', None)

        query = [Q()]

        customer = CustomerMixin().get_customer(user)

        if customer:
            query.append(Q(client__customer=customer))
        else:
            return Inventory.objects.none()

        if tag:
            query.append(Q(tag=tag))

        if from_loc_id:
            query.append(Q(from_loc_id=from_loc_id))

        if to_loc_id:
            query.append(Q(to_loc_id=to_loc_id))

        if status:
            query.append(Q(status=status))

        if supplier_id:
            query.append(Q(status=supplier_id))

        if search_term:
            query.append(Q(sku__name__icontains=search_term) | Q(tag__icontains=search_term))

        if product_group:
            query.append(Q(sku__product_group__name=product_group))

        if from_date:
            from_date = datetime.strptime(from_date, '%d-%m-%Y') - timedelta(hours=5.5)
            from_date = from_date.astimezone(tz=pytz.utc)
            query.append(Q(created_date__gte=from_date))

        if to_date:
            to_date += ' 23:59:59'
            to_date = datetime.strptime(to_date, '%d-%m-%Y %H:%M:%S') - timedelta(hours=5.5)
            to_date = to_date.astimezone(tz=pytz.utc)
            query.append(Q(created_date__lte=to_date))

        final_query = reduce(operator.and_, query)
        return InventoryTransaction.objects\
            .select_related('sku')\
            .filter(final_query)\
            .order_by('-created_date')

    def get(self, request):
        user = request.user
        self.customer = CustomerMixin().get_customer(user)
        output = request.GET.get('output', None)
        response = {}
        if output == 'xlsx':
            self.pagination_class = None
            filename = get_filename('inventory_transaction_')
            file_path = export_to_spreadsheet(
                super().get(request).data,
                filename,
                INVENTORY_TRANSACTION_DATA_TO_BE_EXPORTED
            )
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (
                        fh, filename))

                    response = HttpResponse(
                        fh.read(),
                        content_type="application/vnd.ms-excel"
                    )
                    response['Content-Disposition'] = 'inline; filename=%s' % os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                response['errors'] = 'No file generated'
                status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status_code)
        else:
            return super().get(request)
