from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from rest_framework import status, generics, serializers, views
from rest_framework.response import Response

from blowhorn.users.permission import IsActive
from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.utils.order import get_delivery_challan_data

Order = get_model('order', 'Order')


class ChallanView(views.APIView):

    permission_classes = (IsActive, )

    def get(self, request, order_id):
        try:
            order = Order.objects.get(pk=order_id)
        except:
            _out = {
                "message": "Invalid Order Id"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        challan_data = get_delivery_challan_data(order)
        if not challan_data:
            _out = {
                "message": "Failed to get challan data"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        return Response(status=status.HTTP_200_OK, data=challan_data)

