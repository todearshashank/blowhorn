import logging
from django.utils.translation import ugettext_lazy as _
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, generics, serializers, views, filters
from rest_framework.response import Response
from django.db.models import Q
from django.utils import timezone

from blowhorn.users.permission import IsActive
from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.common import serializers as common_serializer
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.driver import constants as driver_constants
from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.mixins.allocation import AllocateMixins
from blowhorn.wms.mixins.shipping import AllocationMixin
from blowhorn.wms.serializers.order import WmsOrderSerializer
from blowhorn.wms.submodels.containers import WmsAllocationConfig, WmsAllocationLine
from blowhorn.wms.submodels.query_builder import QueryBuilder
from blowhorn.wms.serializers.allocate import AllocationConfigSerializer, QueryBuilderSerializer, \
    WmsAllocationLineSerializer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Order = get_model('order', 'Order')


class OrderView(views.APIView):

    def get(self, request, wh_id):
        user = request.user
        data = request.GET.dict()
        reference_number = data.get('reference_number')
        ord_status = data.get('status')

        _query = Q(pickup_hub_id=wh_id)
        if reference_number:
            _query = _query & Q(reference_number=reference_number)
        if ord_status:
            _query = _query & Q(status=ord_status)

        _query = _query & (Q(hub__is_wms_site=True) | Q(pickup_hub__is_wms_site=True))

        order_qs = Order.objects.filter(_query).order_by('-pickup_datetime').prefetch_related('order_lines')

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=order_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=WmsOrderSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)


# TODO Remove this API, not in use
class AllocateOrder(views.APIView, AllocateMixins):

    permission_classes = (IsActive, IsAuthenticated, )

    def post(self, request):
        data = request.data
        self._initialize_attributes(data)
        self.run_allocation()
        print('self.error_msg - ', self.error_msg)
        return Response(status=self.status, data=self.error_msg or self.message)


class AllocationConfigListView(generics.ListAPIView, CustomerMixin):

    permission_classes = (IsAuthenticated, )
    serializer_class = AllocationConfigSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('id', 'name', 'is_active', 'product_group__name', 'supplier__name', 'client__name', 'site__name',
                       'created_date', 'query_builder__name', 'created_by__name', 'modified_by__name', 'operation')
    search_fields = ('name', 'query_builder__name', 'supplier__name', 'client__name', 'product_group__name')
    filterset_fields = ('name', 'supplier_id', 'client_id', 'site_id', 'product_group_id', 'is_active', 'operation')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return WmsAllocationConfig.objects.filter(customer=customer).select_related('product_group', 'site', 'client',
                                                                                    'supplier', 'query_builder',
                                                                                    'created_by', 'modified_by')


class AllocationConfigView(views.APIView, CustomerMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data
        customer = CustomerMixin().get_customer(request.user)
        allocation_serializer = AllocationConfigSerializer(data=data, context={'customer': customer})
        if allocation_serializer.is_valid():
            allocation_config = allocation_serializer.save()
            allocation_config.created_by = request.user
            allocation_config.save()
            return Response(status=status.HTTP_200_OK, data='Created Allocation Config')

        logger.info('Error creating an allocation config for data %s - %s' % (data, allocation_serializer.errors))
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Could not create Allocation Config')

    def put(self, request, pk):
        data = request.data
        instance = WmsAllocationConfig.objects.filter(id=pk).first()
        if not instance:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid Allocation Config ID')

        customer = CustomerMixin().get_customer(request.user)
        allocation_serializer = AllocationConfigSerializer(
            instance=instance,
            data=data,
            context={'customer': customer}
        )
        if allocation_serializer.is_valid():
            allocation_config = allocation_serializer.save()
            allocation_config.modified_by = request.user
            allocation_config.modified_date = timezone.now()
            allocation_config.save()
            return Response(status=status.HTTP_200_OK, data='Updated Allocation Config')

        logger.info('Error updating an allocation config for data %s - %s' % (data, allocation_serializer.errors))

        return Response(status=status.HTTP_400_BAD_REQUEST, data='Could not update the Allocation config')


class QueryBuilderListView(generics.ListAPIView, CustomerMixin):

    permission_classes = (IsAuthenticated, )
    serializer_class = QueryBuilderSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('id', 'sequence', 'process', 'site__id', 'created_date', 'modified_date',
                       'created_by__name', 'modified_by__name')
    search_fields = ('name', 'process', 'site__name', 'created_by__name', 'modified_by__name')
    filterset_fields = ('site__name', 'process')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return QueryBuilder.objects.filter(customer=customer).select_related(
            'site', 'created_by', 'modified_by')


class QueryBuilderView(views.APIView, CustomerMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data
        customer = self.get_customer(request.user)
        data['customer'] = customer.id
        query_builder_serializer = QueryBuilderSerializer(data=data, context={'customer': customer})
        if query_builder_serializer.is_valid():
            query_builder = query_builder_serializer.save()
            query_builder.created_by = request.user
            query_builder.save()
            return Response(status=status.HTTP_200_OK, data='Created Query Builder')

        logger.info('Could not create Query Builder for data %s - %s' % (data, query_builder_serializer.errors))

        return Response(status=status.HTTP_400_BAD_REQUEST, data=query_builder_serializer.errors)

    def put(self, request, pk):
        data = request.data
        instance = QueryBuilder.objects.filter(id=pk).first()
        customer = CustomerMixin().get_customer(request.user)
        if not instance:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid Query Builder ID')

        query_builder_serializer = QueryBuilderSerializer(instance=instance, data=data, context={'customer': customer})
        if query_builder_serializer.is_valid():
            query_builder = query_builder_serializer.save()
            query_builder.modified_by = request.user
            query_builder.modified_date = timezone.now()
            query_builder_serializer.save()
            return Response(status=status.HTTP_200_OK, data='Updated Query Builder')

        logger.info('Could not update Query Builder for data %s - %s' % (data, query_builder_serializer.errors))

        return Response(status=status.HTTP_400_BAD_REQUEST, data=query_builder_serializer.errors)


class Allocation(views.APIView, AllocationMixin):

    permission_classes = (IsActive, IsAuthenticated)

    def post(self, request):
        data = request.data
        self._initialize_attributes(data, request)

        if self.error_message:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_message)
        try:
            self._run_allocation_algorithm()
        except BaseException as e:
            logger.info('Error occurred during Allocation - %s' % e)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            data='Error occurred during Allocation. Contact Support')

        resp = self.get_response_data()

        return Response(status=status.HTTP_200_OK, data=resp)


class AllocationLineListView(generics.ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = WmsAllocationLineSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('id', )

    def get_queryset(self):
        order_id = self.kwargs.get('pk', None)
        return WmsAllocationLine.objects.filter(order_id=order_id).select_related('created_by',
                                                                                       'modified_by').prefetch_related(
            'inventories')
