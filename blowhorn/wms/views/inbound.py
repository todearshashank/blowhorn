import os
import logging
from datetime import datetime
from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from django.db.models import Q


from rest_framework import serializers, status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from blowhorn.users.permission import IsActive
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.subadmins.inventory import SupplierImportForm
from blowhorn.wms.submodels.inbound import Supplier
from blowhorn.wms.mixins.inbound import SupplierImportMixin
from blowhorn.common.utils import export_to_spreadsheet
from blowhorn.wms.constants import SUPPLIER_DATA_TO_BE_IMPORTED
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.wms.serializers.supplier import SupplierSerializer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@permission_classes([IsAuthenticated])
def supplier_import(request):
    context = {}
    if request.method == 'POST':
        if not request.FILES.get('file', None):
            data_mapping = SUPPLIER_DATA_TO_BE_IMPORTED
            file = 'template' + '_' + 'supplier' + '_' + \
                   str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
            filename = file + '.xlsx'
            input_data = {
                'data': [],
                'filename': filename,
                'data_mapping_constant': data_mapping,
                'only_format': False
            }

            file_path = export_to_spreadsheet(**input_data)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    logging.info(
                        'This is the file path "%s" with name "%s"' % (fh, filename))

                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                                                      os.path.basename(file_path)
                    os.remove(file_path)
                    return response
            else:
                message = 'No file generated'

    if request.FILES.get('file', None):
        form = SupplierImportForm(request.POST, request.FILES)
        success, message = SupplierImportMixin()._process_import_data(request.FILES, request)

        context = {
            'success': success,
            'message': message,
            'alert': True
        }
    return render(request, 'website/supplier_import.html', context)


class SupplierImportExcelTemplate(views.APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request):
        data_mapping = SUPPLIER_DATA_TO_BE_IMPORTED
        file = 'template' + '_' + 'supplier' + '_' + \
               str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        input_data = {
            'data': [],
            'filename': filename,
            'data_mapping_constant': data_mapping,
            'only_format': False
        }

        file_path = export_to_spreadsheet(**input_data)
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='No file generated')


class SupplierImportView(views.APIView, SupplierImportMixin):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        data = request.data.dict()
        success, message = self._process_import_data(data, request)
        if success:
            return Response(status=status.HTTP_200_OK, data=message)
        return Response(status=status.HTTP_400_BAD_REQUEST, data=message)


class SupplierView(views.APIView):

    def get(self, request):
        data = request.GET.dict()
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = CustomerMixin().get_customer(api_key=api_key, user=None) if api_key \
            else CustomerMixin().get_customer(request.user)
        if not customer:
            response = {
                "status": "FAIL",
                "message": "Unauthorized"
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        query = Q(customer=customer)
        name = data.get('name', None)

        if name:
            query &= Q(name=name)

        suppliers = Supplier.objects.filter(query).order_by('id')

        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=suppliers,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=SupplierSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK, data=serialized_data.data)

    def post(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = CustomerMixin().get_customer(api_key=api_key, user=None) if api_key \
            else CustomerMixin().get_customer(request.user)

        if not customer:
            response = {
                "status": "FAIL",
                "message": "Unauthorized"
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        data['customer'] = customer.id
        supplier_serializer = SupplierSerializer(data=data, context={'customer': customer})

        if supplier_serializer.is_valid():
            supplier = supplier_serializer.save()
            response = {
                "status": "PASS",
                "message": "Successfully created Supplier - %s" % supplier.name
            }
            return Response(status=status.HTTP_200_OK, data=response)

        logger.info('Error creating Supplier for customer %s - %s' % (customer, supplier_serializer.errors))

        response = {
            "status": "FAIL",
            "message": "Failed to create Supplier"
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

    def put(self, request):
        data = request.data
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = CustomerMixin().get_customer(api_key=api_key, user=None) if api_key \
            else CustomerMixin().get_customer(request.user)

        if not customer:
            response = {
                "status": "FAIL",
                "message": "Unauthorized"
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=response)

        supplier = Supplier.objects.filter(id=data.get('id', None)).first()
        if not supplier:
            response = {
                "status": "FAIL",
                "message": "Invalid Supplier ID"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        data['customer'] = customer.id
        supplier_serializer = SupplierSerializer(supplier, data=data, context={'customer': customer})

        if supplier_serializer.is_valid():
            supplier = supplier_serializer.save()
            response = {
                "status": "PASS",
                "message": "Successfully updated Supplier - %s" % supplier.name
            }
            return Response(status=status.HTTP_200_OK, data=response)

        logger.info('Error updating Supplier for customer %s - %s' % (customer, supplier_serializer.errors))

        response = {
            "status": "FAIL",
            "message": "Failed to update Supplier"
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

