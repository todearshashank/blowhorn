import json
from rest_framework import views
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from django.db.models import Q

from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as driver_constants
from blowhorn.wms.mixins.pick import PickMixins, StartPickMixins
from blowhorn.users.permission import IsActive
from blowhorn.wms.serializers.putaway import PutawayTaskSerializer
from blowhorn.wms.constants import ACTIVE_WHTASKS_STATUSES, PICK_TASK, WH_TASK_COMPLETED
from blowhorn.oscar.core.loading import get_model


WhTask = get_model('wms', 'WhTask')


class Pick(views.APIView, PickMixins):
    permission_classes = (IsActive, IsAuthenticated, )

    def get(self, request):
        user = request.user
        data = request.GET.dict()
        print('data - ', data)
        next_ = data.get('next') or driver_constants.PAGE_TO_BE_DISPLAYED

        is_directed = data.get('is_directed')

        _params = Q(task_type__name=PICK_TASK, status__in=ACTIVE_WHTASKS_STATUSES)

        if is_directed == 'true':
            _params = _params & Q(operator=user)
        else:
            _params = _params & Q(operator__isnull=True)

        pick_task_qs = WhTask.objects.filter(_params).order_by('-priority')

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=pick_task_qs,
            num=driver_constants.DRIVER_APP_NUMBER_OF_ENTRY_IN_PAGE,
            page=next_,
            serializer_method=PutawayTaskSerializer,
            context={'request': request})

        return Response(status=status.HTTP_200_OK,
                        data=serialized_data.data)

    def post(self, request):
        user = request.user
        data = request.data
        req_data = json.loads(data.get('request_data'))

        self._initialize_attributes(req_data, user)
        if self.error_msg:
            print('self.error_msg - 1 - ', self.error_msg)
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        if self.wh_task.status == WH_TASK_COMPLETED:
            response_data = PutawayTaskSerializer(instance=self.wh_task).data
            return Response(status=status.HTTP_200_OK, data=response_data)

        self.do_pick()
        self.update_pick_task()
        print('self.error_msg do pick - ', self.error_msg)
        if self.error_msg:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=self.error_msg)

        response_data = PutawayTaskSerializer(instance=self.wh_task).data
        return Response(status=status.HTTP_200_OK, data=response_data)


class StartPick(views.APIView, StartPickMixins):
    permission_classes = (IsActive, IsAuthenticated, )

    def post(self, request):
        user = request.user
        data = request.data
        print('start data - ', data)
        req_data = json.loads(data.get('request_data'))
        self._initialize_attributes(req_data, user)
        if self.error_msg:
            return Response(status=self.status, data=self.error_msg)
        self._start_pick()

        response_data = PutawayTaskSerializer(instance=self.pick_task).data
        return Response(status=status.HTTP_200_OK, data=response_data)

