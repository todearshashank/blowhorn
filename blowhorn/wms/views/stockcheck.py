
from django.core.exceptions import ValidationError
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, generics, filters
from rest_framework.permissions import IsAuthenticated

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.wms.submodels.stockcheck import StockCheck, StockCheckLines, StockCheckBatch
from blowhorn.users.permission import IsActive
from blowhorn.wms.serializers.stockcheck import StockCheckLinesSerializer, StockCheckSerializer, StockCheckBatchSerializer
from blowhorn.wms.services.stockcheck import StockCheckService


class StockInfo(generics.RetrieveAPIView):
    permission_classes = (IsActive, IsAuthenticated,)

    def get(self, request):
        try:
            _status, _data = StockCheckService().get_details(
                                request.user, request.GET)
            print(_status, _data)
            return Response(status=_status, data=_data)
        except ValidationError as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=e)

    def post(self, request):
        try:
            _status, _data = StockCheckService().create_entry(
                                request.user, request.data)
            print(_status, _data)
            return Response(status=_status, data=_data)
        except ValidationError as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=e)

    def delete(self, request):
        try:
            _status, _data = StockCheckService().delete_entry(
                                request.user, request.data)
            print(_status, _data)
            return Response(status=_status, data=_data)
        except ValidationError as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=e)


class StockCheckListView(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = StockCheckSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('sku__name', 'product_group__name',
                       'created_date', 'created_by__name')

    search_fields = ('sku__name', 'product_group__name', 'created_by__name')
    filterset_fields = ('sku__name', 'product_group__name', 'batch_id')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return StockCheck.objects.filter(sku__client__customer=customer)


class StockCheckLinesListView(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = StockCheckLinesSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('sku__name', 'tag', 'prev_qty', 'curr_qty',
                       'stock_check__created_date', 'stock_check__created_by__name')

    search_fields = ('sku__name', 'tag', 'stock_check__created_by__name')
    filterset_fields = ('sku__name', 'tag', 'stock_check__id')

    def get_queryset(self):

        customer = self.get_customer(self.request.user)
        return StockCheckLines.objects.filter(sku__client__customer=customer).select_related(
            'sku', 'stock_check')


class StockCheckBatchView(generics.ListAPIView, CustomerMixin):
    permission_classes = (IsAuthenticated,)
    serializer_class = StockCheckBatchSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, DjangoFilterBackend)
    ordering_fields = ('site__name', 'created_date', 'created_by__name')

    search_fields = ('site__name', 'created_by__name')
    filterset_fields = ('site__name', 'created_by__name')

    def get_queryset(self):
        customer = self.get_customer(self.request.user)
        return StockCheckBatch.objects.filter(stockcheck__sku__client__customer=customer)

