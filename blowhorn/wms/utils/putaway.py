from blowhorn.wms.models import WhSku


def update_whtask_location_weight_volume(wh_task, complete=True):
    """
    :param wh_task: Wh Move task
    :param complete: Is the move task created or completed
    :return:
    """
    volume = 0
    weight = 0

    # Use weight volume at SKU level if defined else fetch from pack config
    if wh_task.sku.each_weight:
        weight = wh_task.qty * wh_task.sku.each_weight
    else:
        if wh_task.sku.pack_config.use_tag_volume:
            volume = wh_task.sku.pack_config.tag_volume if wh_task.sku.pack_config.tag_volume else 0
        else:
            volume = wh_task.qty * wh_task.sku.pack_config.each_volume if wh_task.sku.pack_config.each_volume else 0

    if wh_task.sku.each_volume:
        volume = wh_task.qty * wh_task.sku.each_volume
    elif wh_task.sku.pack_config.each_weight:
        weight = wh_task.qty * wh_task.sku.pack_config.each_weight

    if not complete:
        wh_task.to_loc.allocated_volume = wh_task.to_loc.allocated_volume + float(volume)
        wh_task.to_loc.allocated_weight = wh_task.to_loc.allocated_weight + float(weight)
    else:
        # remove  allocated weight/volume
        wh_task.to_loc.allocated_volume = max(wh_task.to_loc.allocated_volume - float(volume), 0)
        wh_task.to_loc.allocated_weight = max(wh_task.to_loc.allocated_weight - float(weight), 0)

        # update actual current weight/volume
        wh_task.to_loc.current_weight = wh_task.to_loc.current_weight + float(weight)
        wh_task.to_loc.current_volume = wh_task.to_loc.current_volume + float(volume)

        # update from loc weight volume
        wh_task.from_loc.current_weight = max(wh_task.from_loc.current_weight - float(weight), 0)
        wh_task.from_loc.current_volume = max(wh_task.from_loc.current_volume - float(volume), 0)

        wh_task.from_loc.save()

    wh_task.to_loc.save()

