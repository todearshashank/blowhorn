from datetime import datetime, timedelta

from django.utils import timezone
from num2words import num2words
from django.contrib.humanize.templatetags.humanize import intcomma

from blowhorn.order.models import OrderLine
from blowhorn.wms.constants import ASN_NEW_STATUS
from blowhorn.wms.serializers.order import OrderChallanSerializer
from blowhorn.wms.submodels.receiving import ASNOrder, ASNLineItems


def get_delivery_challan_data(order):
    challan_serializer = OrderChallanSerializer(instance=order)
    return challan_serializer.data

def format_challan_data(input_data):
    errors = []
    data = {}
    consignor = {}
    consignor_data = input_data.get('pickup_address', None)
    if consignor_data:
        consignor['name'] = input_data['customer_name']
        consignor['address'] = consignor_data['line1']
        consignor['gstin'] = input_data['pickup_gstin']
        consignor['state'] = input_data['source_state']
        data['consignor'] = consignor

    if not consignor:
        errors.append('The consignor data is incomplete : name/address/gstin/state.')

    consignee = {}
    consignee_data = input_data.get('shipping_address', None)
    if consignee_data:
        consignee['name'] = consignee_data['first_name']
        consignee['address'] = consignee_data['line1']
        consignee['gstin'] = input_data['shipping_gstin']
        consignee['state'] = input_data['destination_state']
        data['consignee'] = consignee

    if not consignee:
        errors.append('The consignee data is incomplete : name/address/gstin/state.')

    data['challan'] = {
        'number' : input_data['id'],
        'date' : datetime.now().strftime('%d %b %Y')
    }

    # data['eway_number'] = input_data['number']
    data['vehicle_number'] = input_data.get('vehicle_number','')

    items = []
    is_hsn_present = True
    order_lines = input_data.get('order_lines',None)
    for item in order_lines:
        temp = {}
        temp['name'] = item.get('sku_name', '')
        temp['rate'] = item.get('each_item_price', 0)
        temp['quantity'] = item.get('quantity', 0)
        temp['taxable_value'] = item.get('taxable_value','')
        hsn = item.get('hsn_code', None)
        if not hsn:
            is_hsn_present = False
        temp['hsn'] = hsn
        
        cgst = item.get('cgst', None)
        if cgst and float(cgst) != 0:
            temp['cgst_rate'] = cgst
            temp['cgst_amount'] = item.get('cgst_amount','')

        sgst = item.get('sgst', None)
        if sgst and float(sgst) != 0:
            temp['sgst_rate'] = sgst
            temp['sgst_amount'] = item.get('sgst_amount','')

        igst = item.get('igst', None)
        if igst and float(igst) !=0:
            temp['igst_rate'] = igst
            temp['igst_amount'] = item.get('igst_amount','')
        
        items.append(temp)

    data['items'] = items
    if not items:
        errors.append('The order lines data is missing.')
    # if not is_hsn_present:
    #     errors.append('The hsn code of the product is missing.')

    amount = {}
    data['total_quantity'] = input_data['total_qty']
    amount['taxable_value'] = intcomma(input_data['taxable_value'])
    

    amount['cgst_amount'] = input_data['cgst_amount']
    amount['sgst_amount'] = input_data['sgst_amount']
    amount['igst_amount'] = input_data['igst_amount']
    amount['total_tax'] = intcomma(input_data['cgst_amount'] + input_data['sgst_amount'] + input_data['igst_amount'])
    amount['currency_unit'] = input_data['currency_symbol']
    _tot = input_data['cgst_amount'] + input_data['sgst_amount'] + input_data['igst_amount'] + input_data['taxable_value']
    tot = round(_tot,0)
    amount['value_in_figure'] = intcomma(tot)
    amount['value_in_words'] = input_data['currency_symbol'] + ' ' \
                                    + num2words(tot, lang='en_IN').title() \
                                    + ' only'
    data['amount'] = amount
    if not input_data['total_amount']:
        errors.append('The total amount is missing')

    return errors,data


def create_asn_order(site_id, manifest_ids):
    delivery_date = (timezone.now() + timedelta(days=1)).date()
    asn = ASNOrder.objects.create(invoice_number='ASN-'+str(manifest_ids[0]), site_id=site_id,
                                  delivery_date=delivery_date, status=ASN_NEW_STATUS)

    asn_lines = []

    order_lines = OrderLine.objects.filter(wmsmanifestline__manifest__in=manifest_ids)

    for line in order_lines:
        asn_lines.append(ASNLineItems(asn_order=asn,
                                      sku=line.wh_sku,
                                      status=ASN_NEW_STATUS,
                                      qty=line.quantity,
                                      site_id=site_id,
                                      price_per_qty=line.each_item_price,
                                      tracking_level=line.tracking_level))
    ASNLineItems.objects.bulk_create(asn_lines)















