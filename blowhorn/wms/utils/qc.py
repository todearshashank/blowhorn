from django.db.models import Q

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.constants import PROCESS_RECEIVING, PROCESS_PUTAWAY, \
    PROCESS_PICKING, PROCESS_SHIPPING, QC_TASK

Hub = get_model('address', 'Hub')
QCConfiguration = get_model('wms', 'QCConfiguration')
TaskType = get_model('wms', 'TaskType')
WhTask = get_model('wms', 'WhTask')


def create_qc_task(inv_obj, operation, data):
    po_line_id = data.get('po_line_id')
    asn_line_id = data.get('asn_line_id')
    supplier = data.get('supplier')

    _params = Q(sku=inv_obj.sku) | Q(supplier=inv_obj.supplier)
    if inv_obj.sku.product_group:
        _params |= Q(product_group=inv_obj.sku.product_group)
    _params |= Q(zone=inv_obj.location.zone)
    _params |= Q(location=inv_obj.location)

    if operation == PROCESS_RECEIVING:
        _params = (_params) & Q(qc_at_receivng=True)
    elif operation == PROCESS_PICKING:
        _params = (_params) & Q(qc_at_picking=True)
    elif operation == PROCESS_SHIPPING:
        _params = (_params) & Q(qc_at_shipping=True)
    qc_config = QCConfiguration.objects.filter(_params).order_by('sequence').first()

    if not qc_config:
        return None

    qc_task_type = TaskType.objects.filter(name=QC_TASK).first()

    qc_data = {
        'task_type':qc_task_type,
        'from_loc': inv_obj.location,
        'sku': inv_obj.sku,
        'client': inv_obj.client,
        'site': inv_obj.site,
        'qty': inv_obj.qty,
        'tag': inv_obj.tag,
        'qc_config': qc_config,
        'po_line_id': po_line_id,
        'asn_line_id': asn_line_id,
        'supplier': supplier
    }
    qc_task = WhTask.objects.create(**qc_data)
    print('qc_task - ', qc_task)
    return qc_task


