
from django.contrib import admin
from django.db.models import F
from django.contrib.admin.helpers import ActionForm
from django import forms


from blowhorn.utils.admin_list_filter_title import list_filter_title
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from blowhorn.wms.submodels.inventory import Inventory, InventorySummary, \
    InventoryTransaction
from django.utils.html import format_html



@admin.register(Inventory)
class InventoryAdmin(admin.ModelAdmin):
    list_filter = (('location__zone', admin.RelatedOnlyFieldListFilter),
                   ('created_date', DateRangeFilter),
                   'sku', 'status', 'remarks', 'sku__product_group'
                   )
    search_fields = ('tag', 'sku__name', 'status', 'remarks', 'wmsmanifestline__order_line__order__number')

    list_display = ('tag', 'sku', 'get_bar_code_name','site', 'location', 'client', 'qty', 'allocated_qty', 'tracking_level',
                    'status', 'weight', 'dimensions', 'mfg_date', 'expiry_date', 'get_order', 'is_shopify_synced')

    fields = ('tag', 'sku', 'site', 'location', 'client', 'qty', 'allocated_qty', 'pack_config', 'tracking_level',
              'status', 'weight', 'dimensions', 'expiry_date', 'mfg_date', 'created_by', 'modified_by', 'created_date',
              'get_order')

    readonly_fields = fields

    def get_order(self, obj):
        return obj.order_number
    get_order.short_description = 'Order'

    def get_bar_code_name(self, obj):
        if obj:
            return format_html("""
             <a title='Click here to view barcode' onClick='showBarCode("%s")'>
             %s</a>
             """ % (obj.tag + obj.site.name,
                    obj.tag))
        return '--NA--'

    get_bar_code_name.short_description = \
        'Qr Code'

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            'website/js/lib/jquery-ui.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            '/static/website/js/lib/jsBarcode.js',
            '/static/website/js/lib/barcode_model.js',
        )

    def get_queryset(self, request):
        return super(InventoryAdmin, self).get_queryset(request) \
            .select_related('sku', 'location', 'site', 'client')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(sku_name=F('sku__name'), supplier_name=F('supplier__name'))
        qs = qs.annotate(order_number=F('wmsmanifestline__order_line__order__number'))
        return qs



class InventoryImportForm(ActionForm):
    file = forms.FileField(required=True)


class SupplierImportForm(ActionForm):
    file = forms.FileField(required=True)


@admin.register(InventorySummary)
class InventorySummaryAdmin(admin.ModelAdmin):
    list_display = ('site', 'location', 'sku', 'available_qty', 'unavailable_qty', 'created_date')
    search_fields = ('sku__name',)
    list_filter = ('site__name', 'location__name', ('created_date', DateRangeFilter))
    readonly_fields = ('site', 'location', 'sku', 'available_qty', 'unavailable_qty')

    fields = ('site', 'location', 'sku', 'available_qty', 'unavailable_qty')


@admin.register(InventoryTransaction)
class InventoryTransactionAdmin(admin.ModelAdmin):
    list_filter = (
        'status', 'client', 'reference', 'site__name', ('client__customer', admin.RelatedOnlyFieldListFilter),
        ('created_date', DateRangeFilter))

    search_fields = ('sku__name',)

    list_display = ('tag', 'order', 'sku', 'qty', 'operation', 'qty_diff', 'status', 'remarks', 'client', 'qty', 'get_from_loc', 'get_to_loc', 'site',
                    'supplier', 'reference', 'pack_config', 'tracking_level', 'reference_number', 'source',
                    'created_by', 'created_date')

    fields = ('tag', 'sku', 'client', 'qty', 'get_from_loc', 'get_to_loc', 'site', 'supplier', 'status',
              'reference', 'pack_config', 'tracking_level', 'reference_number')

    def get_from_loc(self, obj):
        if obj:
            return obj.from_loc_name
        return None
    get_from_loc.short_description = 'From Hub'

    def get_to_loc(self, obj):
        if obj:
            return obj.to_loc_name
        return None

    get_to_loc.short_description = 'To Hub'

    def get_fieldsets(self, request, obj=None):
        fields = ('tag', 'sku', 'client', 'qty', 'from_loc', 'to_loc', 'site', 'supplier', 'status', 'reference',
                  'reference_number',)

        return (
            ('General', {
                'fields': fields
            }),
            ('Audit log', {
                'fields': ('created_by', 'created_date', 'modified_by', 'modified_date',)
            }),
        )

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return [f.name for f in self.model._meta.fields]
        else:
            return []

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(sku_name=F('sku__name'), from_loc_name=F('from_loc__name'), supplier_name=F('supplier__name'),
                         to_loc_name=F('to_loc__name'))
        return qs
