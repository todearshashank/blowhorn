
from django.contrib import admin

from blowhorn.oscar.core.loading import get_model

QCAttributes = get_model('wms', 'QCAttributes')
QCConfiguration = get_model('wms', 'QCConfiguration')
QCAttributeValues = get_model('wms', 'QCAttributeValues')


@admin.register(QCConfiguration)
class QCConfigurationAdmin(admin.ModelAdmin):
    list_display = ('product_group', 'sku', 'supplier', 'zone', 'location')
    list_filter = ('sku__name', 'zone__name')


    fields = ('name', 'product_group', 'sku', 'supplier', 'zone', 'location',
              'qc_attributes', 'is_active', 'qc_at_receivng', 'qc_at_picking',
              'qc_at_shipping')


@admin.register(QCAttributes)
class QCAttributesAdmin(admin.ModelAdmin):
    list_display = ('customer', 'name', 'text', 'type', 'required')
    list_filter = ('customer__name', 'is_active')

    fields = ('name', 'text', 'type', 'required', 'customer')


@admin.register(QCAttributeValues)
class QCAttributeValuesAdmin(admin.ModelAdmin):
    list_display = ('attribute', 'qc_config', 'qc_task')

    list_filter = ('qc_task__site__name', 'attribute__customer__name')

    inlines = []

    fieldsets = (
        ('General', {
            'fields': ('attribute', 'qc_config', 'qc_task')
        }),
        ('Values', {'fields': (
            'value_text', 'value_integer', 'value_boolean', 'value_float', 'value_date', 'value_datetime',
            'value_file')})
    )
