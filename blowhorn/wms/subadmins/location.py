from django.contrib import admin
from django.utils.html import format_html

from blowhorn.address.admin import regenerate_qr_code
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, Customer
from blowhorn.wms.submodels.location import StorageType, LocationType, \
    LocationZone, Location, Client, WhSite, LocationZoneHistory
from django.contrib.admin.helpers import ActionForm
from django import forms
from django.utils.translation import ugettext_lazy as _
from blowhorn.utils.datetimerangefilter import DateFilter


@admin.register(WhSite)
class SiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'get_qr_code')
    actions = [regenerate_qr_code, ]
    list_filter = (
        ('city', admin.RelatedOnlyFieldListFilter),'name')

    fields = ('name', 'city', 'length', 'breadth', 'height', 'uom', 'hub')

    def get_queryset(self, request):
        return super(SiteAdmin, self).get_queryset(request) \
            .select_related('city', 'address', 'address__country')

    def get_qr_code(self, obj):
        if obj.qr_code:
            return format_html("""
                <a title='Click here to download qr code'
                onClick='showQrCode("%s", "%s", "%s", "%s")'>View</a>
                """ % (obj.qr_code, obj.name, obj.city.name, ''))
        return '--NA--'

    get_qr_code.short_description = _('QR Code')

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/website/js/lib/jquery.qrcode.min.js',
              'website/js/lib/dom-to-img.js',
              '/static/admin/js/hub/hub.js',
              )


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'customer')
    list_filter = ('name', ('customer', admin.RelatedOnlyFieldListFilter))

    fields = ('name', 'description', 'customer')


class ClientImportForm(ActionForm):
    file = forms.FileField(required=True)


@admin.register(StorageType)
class StorageTypeAdmin(admin.ModelAdmin):

    list_display = ('name', 'description')

    fields = ('name', 'description')


@admin.register(LocationType)
class LocationTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'loc_prefix_code')
    list_filter = ('name', 'loc_prefix_code')

    fields = ('name', 'loc_prefix_code')


@admin.register(LocationZone)
class LocationZoneAdmin(admin.ModelAdmin):
    list_display = ('name', 'site')

    fields = ('name', 'site', 'sequence', 'status', 'is_receiving', 'is_putaway', 'is_packing', 'is_shipping')


@admin.register(LocationZoneHistory)
class LocationZoneHistoryAdmin(admin.ModelAdmin):
    list_display = ('zone', 'number_of_zones', 'category', 'created_by', 'created_date')
    search_fields = ('zone', 'created_by__email', 'created_by__name',)
    list_filter = (('created_date', DateFilter), 'category')
    readonly_fields = ('zone', 'category', 'number_of_zones', 'get_log_dump',)
    exclude = ('log_dump',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_log_dump(self, obj):
        val = obj.log_dump_json
        formatted_text = ''
        for v in val:
            formatted_text += v + '\n'
        return formatted_text

    get_log_dump.short_description = _('Log')


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'client', 'loc_type', 'weight', 'volume', 'site', 'customer', 'status', 'zone')

    list_filter = (('site', admin.RelatedOnlyFieldListFilter), ('client__customer', admin.RelatedOnlyFieldListFilter),
                   'zone__name', 'status',)

    fields = ('name', 'client', 'loc_type', 'site', 'status', 'zone', 'weight',
              'volume', 'current_weight', 'current_volume', 'allocated_volume',
              'allocated_weight',
              'sequence', 'is_receiving', 'is_putaway', 'is_packing', 'is_shipping')

    def customer(self, obj):
        return '-'

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields] + ['customer']

    def get_queryset(self, request):
        return super(LocationAdmin, self).get_queryset(request) \
            .select_related('client', 'loc_type', 'site', 'zone')
