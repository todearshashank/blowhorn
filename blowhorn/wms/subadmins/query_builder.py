from django.contrib import admin
from blowhorn.oscar.core.loading import get_model

# from blowhorn.wms.submodels.putaway import QueryBuilder, QueryBuilderLines

QueryBuilder = get_model('wms', 'QueryBuilder')
QueryBuilderLines = get_model('wms', 'QueryBuilderLines')


class QueryBuilderLinesInline(admin.TabularInline):
    model = QueryBuilderLines

    fields = ('sequence', 'from_model', 'where_condition', 'open_parenthesis', 'filter',
              'operator', 'field_value', 'field_filter', 'close_parenthesis',
              'order_by')


@admin.register(QueryBuilder)
class QueryBuilderAdmin(admin.ModelAdmin):
    list_display = ('name', 'process', 'sequence', 'site')
    list_filter = ('site__name', 'name')
    fields = ('name', 'process', 'site', 'sequence', 'is_active')

    inlines = [QueryBuilderLinesInline, ]
