from django.contrib import admin
from django.db.models import F
from django.urls import reverse
from django.utils.html import format_html

from blowhorn.common.admin import NonEditableTabularInlineAdmin
from blowhorn.wms.submodels.returnorder import ReturnOrder, ReturnOrderLine


class ReturnOrderLineAdmin(NonEditableTabularInlineAdmin):
    model = ReturnOrderLine
    extra = 0
    fields = ('id', 'order_line', 'inventory',)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super(ReturnOrderLineAdmin, self).get_queryset(request)
        return qs


@admin.register(ReturnOrder)
class ReturnOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'amount_collected', 'site_name', 'trip_link')
    list_filter = ('site__name', 'amount_collected')
    search_fields = 'id',

    inlines = [ReturnOrderLineAdmin, ]

    fields = list_display

    def trip_link(self, obj):
        if obj and obj.trip_id:
            link = reverse(
                "admin:trip_trip_change", args=[obj.trip_id])
            return format_html('<a href="%s">%s</a>' % (link, obj.trip_number))
        return '-'

    trip_link.short_description = 'Trip'

    def site_name(self, obj):
        return obj.site_name

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields] + ['trip_link', 'site_name']

    def get_queryset(self, request):
        qs = super(ReturnOrderAdmin, self).get_queryset(request)
        qs = qs.annotate(trip_number=F('trip__trip_number'), trip_status=F('trip__status'), site_name=F('site__name'))
        return qs
