
from django.contrib import admin
from django.db.models import F

from blowhorn.wms.submodels.tasks import TaskType, WhTask


@admin.register(TaskType)
class TaskTypeAdmin(admin.ModelAdmin):

    list_display = ('name', )


@admin.register(WhTask)
class WhTaskAdmin(admin.ModelAdmin):

    list_display = ('task_type', 'order', 'order_line_id', 'from_loc', 'to_loc', 'site', 'sku', 'supplier', 'qty', 'tag', 'status', 'operator')

    list_filter = ('created_date', 'sku','supplier','site', 'task_type','to_loc','from_loc','status')

    fields = ('task_type', 'order', 'from_loc', 'to_loc', 'site',
                    'sku', 'supplier', 'qty', 'tag', 'status', 'operator')

    # def get_queryset(self, request):
    #     return super(WhTaskAdmin, self).get_queryset(request). \
    #         select_related('task_type', 'order', 'from_loc', 'to_loc', 'site', 'sku', 'operator', 'supplier').only(
    #         'task_type__name', 'order__number', 'from_loc__name', 'to_loc__name', 'site__name', 'sku__name',
    #         'operator__name', 'supplier__name')

    # def get_queryset(self, request):
    #     return super(WhTaskAdmin, self).get_queryset(request).annotate(
    #         task_type=F('task_type__name'), order=F('order__number'),
    #         from_loc=F('from_loc'), to_loc=F('to_loc'), site=F('site__name'),
    #         sku=F('sku'), operator=F('operator__email_id'),
    #         supplier=F('supplier__name'))

