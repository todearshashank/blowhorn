from django.contrib import admin
from django.db.models import F
from django.urls import reverse
from django.utils.html import format_html

from blowhorn.common.admin import NonEditableTabularInlineAdmin
from blowhorn.wms.submodels.containers import WmsManifest, WmsManifestLine, WmsAllocation, WmsAllocationLine, \
    WmsAllocationConfig
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from blowhorn.customer.models import Customer, CUSTOMER_CATEGORY_INDIVIDUAL


class WmsManifestLineAdmin(NonEditableTabularInlineAdmin):
    model = WmsManifestLine
    extra = 0
    fields = ('id', 'order_line', 'order_number', 'inventory', 'created_date', 'created_by')

    def order_number(self, obj):
        if obj:
            return obj.order_line.order.number
        return '-'

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super(WmsManifestLineAdmin, self).get_queryset(request)
        return qs

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields] + ['order_number']


@admin.register(WmsManifest)
class WmsManifestAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'status', 'site_name', 'trip_link', 'created_by', 'get_trip_driver', 'created_date', 'modified_date')
    inlines = [WmsManifestLineAdmin, ]
    search_fields = 'id',
    list_filter = (
        'site__name', 'wmsmanifestline__order_line__client', ('trip__driver', admin.RelatedOnlyFieldListFilter),
        ('created_by', admin.RelatedOnlyFieldListFilter))
    fieldsets = (
        (None, {
            'fields':
                ('id', 'status', 'site_name', 'trip_link', 'get_trip_driver', )
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        })
    )

    def trip_link(self, obj):
        if obj and obj.trip_id:
            link = reverse(
                "admin:trip_trip_change", args=[obj.trip_id])
            return format_html('<a href="%s">%s</a>' % (link, obj.trip_number))
        return '-'

    trip_link.short_description = 'Trip'

    def site_name(self, obj):
        return obj.site_name

    def get_trip_driver(self, obj):
        if obj and obj.trip_id:
            return obj.trip.driver
        return '-'

    get_trip_driver.short_description = 'Driver'

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields] + ['trip_link', 'site_name', 'get_trip_driver']

    def get_queryset(self, request):
        qs = super(WmsManifestAdmin, self).get_queryset(request)
        qs = qs.annotate(trip_number=F('trip__trip_number'), trip_status=F('trip__status'), site_name=F('site__name'))
        return qs


class WmsAllocationLineInlineAdmin(NonEditableTabularInlineAdmin):

    model = WmsAllocationLine
    extra = 0
    fields = ('id', 'order', 'order_line', 'inventories', 'requested_qty', 'allocated_qty')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super(WmsAllocationLineInlineAdmin, self).get_queryset(request)
        return qs

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]


@admin.register(WmsAllocation)
class WmsAllocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'site', 'no_of_orders_allocated', 'allocation_time')

    inlines = [WmsAllocationLineInlineAdmin, ]
    search_fields = ('id', 'site__name')
    list_filter = ('site', )

    fieldsets = (
        (None, {
            'fields': ('site', 'no_of_orders_allocated', 'allocation_time')
        }),
        ('Audit Log', {
            'fields': ('created_by', 'created_date', 'modified_by', 'modified_date')
        })
    )

    readonly_fields = ('created_by', 'created_date', 'modified_by', 'modified_date')


@admin.register(WmsAllocationConfig)
class WmsAllocationConfigAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'is_active', 'customer', 'query_builder',
                    'product_group', 'site', 'supplier', 'client')

    search_fields = ('name', 'customer__name', 'query_builder__name', 'site__name')
    list_filter = (('customer', admin.RelatedOnlyFieldListFilter), 'is_active',
                   ('site', admin.RelatedOnlyFieldListFilter), ('product_group', admin.RelatedOnlyFieldListFilter),
                   ('supplier', admin.RelatedOnlyFieldListFilter), ('client', admin.RelatedOnlyFieldListFilter))

    fieldsets = (
        (None, {
            'fields': ('name', 'is_active', 'customer', 'query_builder', 'product_group',
                       'site', 'supplier', 'client')
        }),
        ('Audit Log', {
            'fields': ('created_by', 'created_date', 'modified_by', 'modified_date')
        })
    )
    readonly_fields = ('created_by', 'created_date', 'modified_by', 'modified_date')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = \
                Customer.objects.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)
