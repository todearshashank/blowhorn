import time

from django import forms
from django.contrib import admin
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from blowhorn.utils.datetimerangefilter import DateRangeFilter
from blowhorn.wms.submodels.stockcheck import StockCheck, StockCheckLines, \
        GainLossReason, StockCheckBatch


class StockCheckLinesInline(admin.TabularInline):

    model = StockCheckLines
    extra = 0
    fields = ('sku', 'tag', 'curr_qty', 'prev_qty', 'get_gain_loss', 'reason')
    readonly_fields = fields

    def get_gain_loss(self, obj):
        return obj.curr_qty - obj.prev_qty
    get_gain_loss.short_description = 'Gain or Loss'

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(StockCheckBatch)
class StockCheckBatchAdmin(admin.ModelAdmin):

    list_display = ('id', 'site', 'created_date', 'created_by', 'get_total_time', 'end_time')
    list_filter = ('site', ('created_date', DateRangeFilter))

    search_fields = ['created_by__name']

    fields = ('id', 'site', 'created_date', 'created_by', 'get_total_time', 'end_time', 'file')
    readonly_fields = fields

    def get_total_time(self, obj):
        if obj.end_time:
            return time.strftime("%H:%M:%S", time.gmtime((obj.end_time - obj.created_date).total_seconds()))
        return '-'
    get_total_time.short_description = 'Total Time (HH:MM:SS)'

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(StockCheck)
class StockCheckAdmin(admin.ModelAdmin):

    list_display = ('batch', 'status', 'sku', 'product_group', 'created_date', 'created_by', 'get_sku_count')
    list_filter = ('batch', 'status', 'sku', 'product_group', 'created_by', ('created_date', DateRangeFilter))

    search_fields = ['created_by__name']

    fields = ('id', 'batch', 'status', 'sku', 'product_group', 'created_date', 'created_by', 'get_sku_count')
    readonly_fields = fields

    inlines = [StockCheckLinesInline]

    can_delete = False

    def get_sku_count(self, obj):
        if obj.sku:
            return StockCheckLines.objects.filter(stock_check_id=obj.id, sku=obj.sku).distinct('sku').count()
        return None
    get_sku_count.short_description = 'SKU Count'

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


class StockCheckLinesForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(StockCheckLinesForm, self).__init__(*args, **kwargs)
        query = Q(is_active=True)
        if self.instance.stock_check:
            query &= Q(site=self.instance.stock_check.site)
        self.fields['reason'].queryset = GainLossReason.objects.filter(query)


@admin.register(StockCheckLines)
class StockCheckLinesAdmin(admin.ModelAdmin):

    form = StockCheckLinesForm

    list_display = ('stock_check', 'sku', 'tag', 'curr_qty', 'prev_qty',
                    'get_gain_loss', 'reason')
    list_filter = ('sku__name', 'tag',
                   ('stock_check__created_date', DateRangeFilter))
    fields = ('stock_check', 'sku', 'tag', 'curr_qty', 'prev_qty', 'get_gain_loss',
              'reason', 'get_created_by', 'get_created_date')

    search_fields = ['created_by__name']

    readonly_fields = ('stock_check', 'sku', 'tag', 'curr_qty', 'prev_qty', 'get_gain_loss',
                       'get_created_by', 'get_created_date')

    def get_created_by(self, obj):
        if obj.stock_check:
            return obj.stock_check.created_by
        return None
    get_created_by.short_description = 'Created By'

    def get_created_date(self, obj):
        if obj.stock_check:
            return obj.stock_check.created_date
        return None
    get_created_date.short_description = 'Created Date'

    def get_gain_loss(self, obj):
        return obj.curr_qty - obj.prev_qty
    get_gain_loss.short_description = 'Gain or Loss'

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


@admin.register(GainLossReason)
class GainLossReasonAdmin(admin.ModelAdmin):

    list_display = ('name', 'site', 'description', 'is_active')
    list_filter = ('name', 'site', 'is_active')

    fieldsets = (
        ('General', {
            'fields': ('name', 'site', 'description', 'is_active',)
        }),
        ('AuditLog', {
            'fields': ('created_by', 'created_date',
                       'modified_date', 'modified_by')
        }),
    )

    readonly_fields = ('created_by', 'created_date',
                       'modified_date', 'modified_by')

    def has_delete_permission(self, request, obj=None):
        return False
