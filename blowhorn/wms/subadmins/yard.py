from django.contrib import admin

from blowhorn.wms.submodels.yard import Yard

@admin.register(Yard)
class YardAdmin(admin.ModelAdmin):

    list_display = ('driver', 'hub',)
    list_filter = ['driver__operating_city']
    search_fields = ['user__phone_number', 'name', 'vehicles__registration_certificate_number', 'hub__name']

    fields = list_display
