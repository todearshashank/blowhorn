
from django.contrib import admin

from blowhorn.wms.submodels.receiving import GRNLineItems, \
        PurchaseOrder, POLineItems, ASNOrder, ASNLineItems


class POLineItemsInline(admin.TabularInline):
    model = POLineItems
    readonly_fields = ['qty_received']

class GRNLineItemsInline(admin.TabularInline):
    model = GRNLineItems
    readonly_fields = ['sku', 'qty']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(PurchaseOrder)
class PurchaseOrderAdmin(admin.ModelAdmin):
    list_display = ('po_number', 'invoice_number', 'supplier', 'status')
    list_filter = ('invoice_number', 'supplier__name')

    inlines = [POLineItemsInline, GRNLineItemsInline]

    def has_delete_permission(self, request, obj=None):
        return False


class ASNLineItemsInline(admin.TabularInline):
    model = ASNLineItems
    readonly_fields = ['qty_received']
    extra = 0


@admin.register(ASNOrder)
class ASNOrderAdmin(admin.ModelAdmin):
    list_display = ('invoice_number', 'supplier', 'status')
    list_filter = ('invoice_number', 'supplier','status')

    inlines = [ASNLineItemsInline]
