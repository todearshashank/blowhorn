
import json
from django.contrib import admin
from django.contrib.admin.helpers import ActionForm
from django import forms
from django.utils.translation import ugettext_lazy as _
from blowhorn.wms.forms import WhskuForm
from blowhorn.wms.submodels.sku import KitSkuMapping, TrackingLevel, PackConfig, WhSku, WhSkuChangeLog
from django.utils.html import format_html
from blowhorn.customer.models import Customer, CUSTOMER_CATEGORY_INDIVIDUAL
from blowhorn.wms.tasks import update_order_kitskus


class WhSkuChangeLogInline(admin.TabularInline):
    model = WhSkuChangeLog
    can_delete = False
    max_num = 5000
    extra = 0
    readonly_fields = ('field', 'old_value', 'new_value', 'modified_by', 'modified_date')
    verbose_name = 'Changelog'
    verbose_name_plural = 'Changelog'

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('modified_by', 'whsku')


@admin.register(TrackingLevel)
class TrackingLevelAdmin(admin.ModelAdmin):
    list_display = ('name', 'level_one_name', 'level_two_name', 'ratio_level_two_one', 'customer')
    list_filter = ('name', ('customer', admin.RelatedOnlyFieldListFilter))

    fields = (
        'name', 'level_one_name', 'level_two_name', 'level_three_name', 'ratio_level_two_one', 'ratio_level_three_two',
        'customer'
    )

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = \
                Customer.objects.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(PackConfig)
class PackConfigAdmin(admin.ModelAdmin):
    list_display = ('name', 'tracking_level', 'each_volume', 'tag_volume')
    list_filter = ('tracking_level__name',)

    fields = ('name', 'tracking_level', 'each_volume', 'tag_volume',
              'each_weight', 'customer')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = \
                Customer.objects.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(WhSku)
class WhSkuAdmin(admin.ModelAdmin):
    form = WhskuForm
    list_display = ('name', 'pack_config', 'is_kit_sku', 'get_bar_code_name', 'get_each_weight_kg', 'each_dimension')
    list_select_related = ('pack_config',)
    list_filter = (
        'client__name', 'pack_config', 'name', 'each_weight', ('client__customer', admin.RelatedOnlyFieldListFilter))

    fieldsets = (
        ('General', {
            'fields': ('name', 'description', 'pack_config', 'upc', 'ean', 'product_group',
                       'client', 'has_expiry', 'shelf_life', 'shelf_life_uom',
                       'requires_putaway', 'get_each_weight_units', 'supplier', 'uom', 'file',
                       'mrp', 'discounts', 'discount_type', 'requires_shipping',
                       'is_kit_sku')
        }),
        ('Shopify', {
            'fields': ('shopify_product_id', 'shopify_variant_id',
                       'shopify_description', 'shopify_inventory_id')
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by',
                       'modified_date', 'modified_by')
        }),
    )

    conditional_inlines = [WhSkuChangeLogInline]

    def get_inline_instances(self, request, obj=None):
        inlines = []
        if obj:
            inlines.append(WhSkuChangeLogInline(self.model, self.admin_site))
        return inlines

    def get_readonly_fields(self, request, obj=None):
        fields = [f.name for f in self.model._meta.fields] + ['get_each_weight_units',]
        fields.remove('upc')
        fields.remove('ean')
        fields.remove('is_kit_sku')
        return fields

    def get_each_weight_kg(self, obj):
        if obj:
            return '%s %s' % (obj.each_weight, obj.uom)

    get_each_weight_kg.short_description = 'Each Weight In(KGs)'

    def get_each_weight_units(self, obj):
        if obj:
            return obj.each_weight
        return '-'

    get_each_weight_units.short_description = 'Each Weight In(KGs)'

    def get_bar_code_name(self, obj):
        if obj:
            client_name = obj.client.name if obj.client else ''
            return format_html("""
             <a title='Click here to view barcode'
              onClick='showBarCode("%s")'>
             %s</a>
             """ % (obj.name + client_name,
                    obj.name))
        return '--NA--'

    get_bar_code_name.short_description = _('Qr Code')

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            'website/js/lib/jquery-ui.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            '/static/website/js/lib/jsBarcode.js',
            '/static/website/js/lib/barcode_model.js',
        )


class WhSkuImportForm(ActionForm):
    file = forms.FileField(required=True)

@admin.register(KitSkuMapping)
class KitSkuMappingAdmin(admin.ModelAdmin):
    list_display = ('parent_sku', 'child_sku', 'quantity')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'parent_sku':
            kwargs['queryset'] = \
                WhSku.objects.filter(is_kit_sku=True)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def update_order_skus(modeladmin, request, queryset):
        id_list = [entry.id for entry in queryset]
        if id_list:
            update_order_kitskus.apply_async(args=[json.dumps(id_list),])
    update_order_skus.short_description = "Update Orders"

    actions = [update_order_skus]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            del actions['update_order_skus']
        return actions
