import time
from rest_framework import serializers

from blowhorn.oscar.core.loading import get_model

StockCheckLines = get_model('wms', 'StockCheckLines')
StockCheck = get_model('wms', 'StockCheck')
StockCheckBatch = get_model('wms', 'StockCheckBatch')


class StockCheckLinesSerializer(serializers.ModelSerializer):

    sku_name = serializers.SerializerMethodField()
    lost_qty = serializers.SerializerMethodField()
    date_time = serializers.SerializerMethodField()
    performed_by = serializers.SerializerMethodField()

    def get_sku_name(self, obj):
        return obj.sku.name if obj.sku else ''

    def get_lost_qty(self, obj):
        return (obj.prev_qty - obj.curr_qty) if obj.prev_qty > obj.curr_qty else 0

    def get_date_time(self, obj):
        return obj.stock_check.created_date

    def get_performed_by(self, obj):
        return obj.stock_check.created_by.name if obj.stock_check.created_by else ''

    class Meta:
        model = StockCheckLines
        fields = '__all__'


class StockCheckSerializer(serializers.ModelSerializer):

    sku_name = serializers.SerializerMethodField()
    product_group_name = serializers.SerializerMethodField()
    performed_by = serializers.SerializerMethodField()
    total_sku = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()

    def get_total_sku(self, obj):
        return StockCheckLines.objects.filter(stock_check_id=obj.id).distinct('sku').count()

    def get_performed_by(self, obj):
        return obj.created_by.name if obj.created_by else ''

    def get_sku_name(self, obj):
        return obj.sku.name if obj.sku else ''

    def get_product_group_name(self, obj):
        return obj.product_group.name if obj.product_group else ''

    def get_site_name(self, obj):
        return obj.batch.site.name if obj.batch else ''

    class Meta:
        model = StockCheck
        fields = '__all__'


class StockCheckBatchSerializer(serializers.ModelSerializer):

    site_name = serializers.SerializerMethodField()
    performed_by = serializers.SerializerMethodField()
    total_time = serializers.SerializerMethodField()
    total_sku = serializers.SerializerMethodField()

    def get_total_sku(self, obj):
        return StockCheckLines.objects.filter(stock_check__batch_id=obj.id).distinct('sku').count()

    def get_total_time(self, obj):
        if obj.end_time:
            return time.strftime("%H:%M:%S", time.gmtime((obj.end_time - obj.created_date).total_seconds()))
        return '-'

    def get_performed_by(self, obj):
        return obj.created_by.name if obj.created_by else ''

    def get_site_name(self, obj):
        return obj.site.name

    class Meta:
        model = StockCheckBatch
        fields = '__all__'
