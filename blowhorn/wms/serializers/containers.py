from django.db.models import F
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from blowhorn.order.models import Order, OrderLine
from blowhorn.oscar.core.loading import get_model

Container = get_model('wms', 'WmsContainer')

Pallet = get_model('wms', 'WmsPallet')
Inventory = get_model('wms', 'Inventory')
Manifest = get_model('wms', 'WmsManifest')


class InventorySerializer(serializers.ModelSerializer):
    sku_name = serializers.SerializerMethodField()
    sku_image = serializers.SerializerMethodField()
    mrp = serializers.SerializerMethodField()
    selling_price = serializers.SerializerMethodField()

    def get_mrp(self, obj):
        return obj.sell_price + obj.discount

    def get_selling_price(self, obj):
        return obj.sell_price

    def get_sku_name(self, obj):
        return obj.sku.name

    def get_sku_image(self, obj):
        return obj.sku.file.url if obj.sku.file else ''

    class Meta:
        model = Inventory
        fields = ('id', 'qty', 'tag', 'tracking_level', 'site_id', 'sku_id', 'sku_name', 'sku_image', 'mrp',
                  'selling_price',)


class ContainerSerializer(serializers.ModelSerializer):
    site_name = serializers.SerializerMethodField()

    def get_site_name(self, obj):
        return obj.site.name

    class Meta:
        model = Container
        fields = '__all__'


class OrderLineSerializer(serializers.ModelSerializer):
    inv_details = serializers.SerializerMethodField()

    class Meta:
        model = OrderLine
        fields = ('inv_details',)

    def get_inv_details(self, obj):
        lines = obj.wmsmanifestline_set.all()
        if lines:
            return [{"id": line.inventory.id,
                     "qty": line.inventory.qty,
                     "tag": line.inventory.tag,
                     "tracking_level": line.inventory.tracking_level,
                     "site_id": line.inventory.site_id,
                     "sku_id": line.inventory.site_id,
                     "sku_name": line.inventory.sku.name,
                     "sku_image": line.inventory.sku.file.url if line.inventory.sku.file else '',
                     }
                    for line in lines if line.inventory]

        return []


class OrderSerializer(serializers.ModelSerializer):
    order_lines = serializers.SerializerMethodField()
    shipping_address = serializers.SerializerMethodField()

    def get_order_lines(self, obj):
        order_lines = obj.order_lines.all()
        lines = []
        for line in order_lines:
            sku_info = {
                "sku_id": line.wh_sku_id,
                "sku_name": line.wh_sku.name,
                "mrp": float(line.total_item_price or 0) + float(line.discounts or 0),
                "selling_price": float(line.total_item_price),
                "sku_image": line.wh_sku.file.url if line.wh_sku.file else '',
                "inv_details": []

            }
            for line in line.wmsmanifestline_set.all():
                if line.inventory:
                    sku_info['inv_details'].append(
                        {"id": line.inventory.id,
                         "qty": line.inventory.qty,
                         "tag": line.inventory.tag,
                         "tracking_level": line.inventory.tracking_level,
                         "site_id": line.inventory.site_id,
                         })
            lines.append(sku_info)
        return lines

    def get_shipping_address(self, obj):
        shipping_address = obj.shipping_address
        return {"address": {
            "line1": shipping_address.line1,
            "line3": shipping_address.line3,
            "line4": shipping_address.state,
            "postcode": shipping_address.postcode,
        },
            "contact": {
                "mobile": str(shipping_address.phone_number),
                "name": shipping_address.name

            }}

    class Meta:
        model = Order
        fields = ('id', 'number', 'reference_number', 'shipping_address', 'order_lines', 'cash_on_delivery', 'hub_id',)


class ManifestListSerializer(serializers.ModelSerializer):
    created_date = serializers.SerializerMethodField()
    order_details = serializers.SerializerMethodField()
    manifest_number = serializers.SerializerMethodField()
    manifest_id = serializers.SerializerMethodField()

    def get_created_date(self, obj):
        return obj.created_date.isoformat()

    def get_manifest_id(self, obj):
        return obj.id

    def get_manifest_number(self, obj):
        return 'MNST-' + str(obj.id)

    def get_order_details(self, obj):
        orders = Order.objects.filter(order_lines__wmsmanifestline__manifest=obj).distinct("id")
        return OrderSerializer(orders, many=True).data

    class Meta:
        model = Manifest
        fields = ('manifest_id', 'created_date', 'order_details', 'manifest_number',)


class ManifestSerializer(serializers.ModelSerializer):
    created_date = serializers.SerializerMethodField()
    inv_details = serializers.SerializerMethodField()
    manifest_id = serializers.SerializerMethodField()

    def get_created_date(self, obj):
        return obj.created_date.isoformat()

    def get_manifest_id(self, obj):
        return obj.id

    def get_inv_details(self, obj):
        invs = Inventory.objects.filter(wmsmanifestline__manifest=obj).annotate(
            discount=F('wmsmanifestline__order_line__discounts'),
            sell_price=F('wmsmanifestline__order_line__total_item_price'))
        return InventorySerializer(invs, many=True).data

    class Meta:
        model = Manifest
        fields = ('manifest_id', 'created_date', 'inv_details')


class PalletSerializer(serializers.ModelSerializer):
    site_name = serializers.SerializerMethodField()

    def get_site_name(self, obj):
        return obj.site.name

    class Meta:
        model = Pallet
        fields = '__all__'
