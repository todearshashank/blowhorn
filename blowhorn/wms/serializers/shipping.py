from rest_framework import serializers

from blowhorn.oscar.core.loading import get_model
from blowhorn.address.serializer import ShippingAddressSerializer
from blowhorn.wms.serializers.order import OrderLineSerializer

Order = get_model('order', 'Order')


class ShippingLabelSerializer(serializers.ModelSerializer):
    shipping_address = ShippingAddressSerializer(read_only=True)
    order_lines = serializers.SerializerMethodField(read_only=True)

    def get_order_lines(self, order):
        orderlines = order.order_lines.all()
        processed =  [{
                'id': ol.wh_sku_id or ol.sku_id,
                'name': ol.wh_sku.name if ol.wh_sku else ol.sku.name,
                'qty': ol.quantity,
                'each_item_price': ol.each_item_price,
                'total_item_price': ol.total_item_price
            } for ol in orderlines]
        return processed

    class Meta:
        model = Order
        fields = (
            'id', 'number', 'reference_number', 'shipping_address',
            'date_placed', 'weight', 'payment_mode', 'payment_status',
            'cash_on_delivery', 'delivery_instructions', 'order_lines'
        )
