from rest_framework import serializers

from blowhorn.oscar.core.loading import get_model

Order = get_model('order', 'Order')
WhTask = get_model('wms', 'WhTask')


class PutawayTaskSerializer(serializers.ModelSerializer):
    sku_name = serializers.SerializerMethodField()
    task_type_name = serializers.SerializerMethodField()
    sku_name = serializers.SerializerMethodField()
    from_loc_name = serializers.SerializerMethodField()
    to_loc_name = serializers.SerializerMethodField()
    order_number = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()

    def get_sku_name(self, obj):
        return obj.sku.name

    def get_from_loc_name(self, obj):
        return obj.from_loc.name

    def get_to_loc_name(self, obj):
        if obj.to_loc:
            return obj.to_loc.name
        return None

    def get_order_number(self, obj):
        if obj.order:
            return obj.order.number
        return None

    def get_client_name(self, obj):
        return obj.client.name

    def get_site_name(self, obj):
        return obj.site.name

    def get_task_type_name(self, obj):
        return obj.task_type.name

    class Meta:
        model = WhTask
        fields = ('id', 'task_type_name', 'qty', 'tag', 'status', 'sku_id',
                  'sku_name', 'from_loc_id',
                  'from_loc_name', 'to_loc_id', 'to_loc_name', 'priority',
                  'order_id', 'order_number', 'order_line_id', 'client_id',
                  'client_name', 'site_id', 'site_name')

