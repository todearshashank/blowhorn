from rest_framework import serializers

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.serializers.putaway import PutawayTaskSerializer

Order = get_model('order', 'Order')
QCAttributes = get_model('wms', 'QCAttributes')
QCConfiguration = get_model('wms', 'QCConfiguration')
WhTask = get_model('wms', 'WhTask')


class QCAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = QCAttributes
        exclude = ('created_by', 'modified_by', 'created_date', 'modified_date')


class QCConfigurationSerializer(serializers.ModelSerializer):
    zone_name = serializers.SerializerMethodField()
    sku_name = serializers.SerializerMethodField()
    location_name = serializers.SerializerMethodField()
    product_group_name = serializers.SerializerMethodField()
    qc_attributes = QCAttributesSerializer(many=True)

    def get_zone_name(self, obj):
        if obj.zone:
            return obj.zone.name
        return None

    def get_sku_name(self, obj):
        if obj.sku:
            return obj.sku.name
        return None

    def get_location_name(self, obj):
        if obj.location:
            return obj.location.name
        return None

    def get_product_group_name(self, obj):
        if obj.product_group:
            return obj.product_group.name
        return None

    class Meta:
        model = QCConfiguration
        fields = '__all__'


class QcTaskSerializer(serializers.ModelSerializer):
    qc_task = serializers.SerializerMethodField()
    qc_config = serializers.SerializerMethodField()

    def get_qc_task(self, obj):
        return PutawayTaskSerializer(obj).data

    def get_qc_config(self, obj):
        return QCConfigurationSerializer(obj.qc_config).data

    class Meta:
        model = WhTask
        fields = ('qc_task', 'qc_config')

