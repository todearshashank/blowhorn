from rest_framework import serializers, exceptions

from blowhorn.oscar.core.loading import get_model

WhSku = get_model('wms', 'WhSku')
PackConfig = get_model('wms', 'PackConfig')
TrackingLevel = get_model('wms', 'TrackingLevel')
ProductGroup = get_model('wms', 'ProductGroup')


class WhSkuSerializer(serializers.ModelSerializer):

    class Meta:
        model = WhSku
        fields = '__all__'


class PackConfigSerializer(serializers.ModelSerializer):
    tracking_level_dict = serializers.SerializerMethodField()

    class Meta:
        model = PackConfig
        fields = '__all__'

    def get_tracking_level_dict(self, obj):
        tracking_level = obj.tracking_level
        return {
            'name': tracking_level.name,
            'level_one_name': tracking_level.level_one_name,
            'level_two_name': tracking_level.level_two_name,
            'level_three_name': tracking_level.level_three_name,
            'ratio_level_two_one': tracking_level.ratio_level_two_one,
            'ratio_level_three_two': tracking_level.ratio_level_three_two
        } if tracking_level else ''


class TrackingLevelSerializer(serializers.ModelSerializer):

    class Meta:
        model = TrackingLevel
        fields = '__all__'


class ProductGroupSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(ProductGroupSerializer, self).__init__(*args, **kwargs)
        self.customer = self.context.get('customer', None) if self.context else None

    def validate(self, attrs):
        id_ = attrs.get('id', None)
        name = attrs.get('name', None)

        if id_ and ProductGroup.objects.filter(name=name, customer=self.customer).exclude(id=id_).exists():
            _out = {
                "status": "FAIL",
                "message": "Given Product Group Name already exists"
            }
            raise exceptions.ParseError(_out)

    class Meta:
        model = ProductGroup
        fields = '__all__'
