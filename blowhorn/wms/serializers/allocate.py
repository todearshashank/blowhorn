import logging

from django.db.models import F
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.serializers.inventory import InventorySerializer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

WmsAllocationConfig = get_model('wms', 'WmsAllocationConfig')
QueryBuilder = get_model('wms', 'QueryBuilder')
QueryBuilderLines = get_model('wms', 'QueryBuilderLines')
WmsAllocationLine = get_model('wms', 'WmsAllocationLine')


class AllocationConfigSerializer(serializers.ModelSerializer):

    created_by_name = serializers.SerializerMethodField()
    modified_by_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()
    product_group_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    query_builder_name = serializers.SerializerMethodField()

    def get_created_by_name(self, obj):
        return obj.created_by.name if obj.created_by else ''

    def get_modified_by_name(self, obj):
        return obj.modified_by.name if obj.modified_by else ''

    def get_site_name(self, obj):
        return obj.site.name if obj.site else ''

    def get_product_group_name(self, obj):
        return obj.product_group.name if obj.product_group else ''

    def get_supplier_name(self, obj):
        return obj.supplier.name if obj.supplier else ''

    def get_client_name(self, obj):
        return obj.client.name if obj.client else ''

    def get_query_builder_name(self, obj):
        return obj.query_builder.name if obj.query_builder else ''

    def validate(self, attrs):
        customer = self.context.get('customer')
        if not customer:
            raise ValidationError('You\'re not a valid customer')

        if not attrs.get('name', None):
            raise ValidationError('Please enter Name for the Allocation Config')

        attrs['customer'] = customer
        return attrs

    class Meta:
        model = WmsAllocationConfig
        fields = ('id', 'name', 'query_builder', 'query_builder_name', 'product_group',
                  'product_group_name', 'site', 'site_name', 'supplier', 'supplier_name', 'client', 'client_name',
                  'is_active', 'created_by_name', 'modified_by_name', 'created_date', 'modified_date', 'operation')


class QueryBuilderLinesSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(required=False)
    query_builder = serializers.PrimaryKeyRelatedField(queryset=QueryBuilder.objects.all(), required=False)

    class Meta:
        model = QueryBuilderLines
        fields = '__all__'


class QueryBuilderSerializer(serializers.ModelSerializer):

    query_builder_lines = serializers.SerializerMethodField()
    query_lines = QueryBuilderLinesSerializer(many=True, write_only=True)
    created_by_name = serializers.SerializerMethodField()
    modified_by_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()

    def get_created_by_name(self, obj):
        return obj.created_by.name if obj.created_by else ''

    def get_modified_by_name(self, obj):
        return obj.modified_by.name if obj.modified_by else ''

    def get_site_name(self, obj):
        return obj.site.name if obj.site else ''

    def get_query_builder_lines(self, obj):
        qs = QueryBuilderLines.objects.filter(query_builder=obj).order_by('sequence')
        return QueryBuilderLinesSerializer(qs, many=True).data

    def validate(self, attrs):
        customer = self.context.get('customer')
        if not customer:
            raise ValidationError('You\'re not a valid customer')

        if not attrs.get('name', None):
            raise ValidationError('Please enter Name for the Query Builder')

        if not attrs.get('process', None):
            raise ValidationError('Process is mandatory for Query Builder')

        attrs['customer'] = customer
        return attrs

    def create(self, validated_data):
        lines_data = validated_data.pop('query_lines', None)
        query_builder = QueryBuilder.objects.create(**validated_data)
        for line_data in lines_data:
            query_builder_line = QueryBuilderLines.objects.create(query_builder=query_builder, **line_data)
        return query_builder

    def update(self, instance, validated_data):
        lines_data = validated_data.pop('query_lines', None)
        for line_data in lines_data:
            line_instance = QueryBuilderLines.objects.filter(id=line_data.get('id', None)).first()
            line_instance.sequence = line_data.get('sequence', line_instance.sequence)
            line_instance.from_model = line_data.get('from_model', line_instance.from_model)
            line_instance.where_condition = line_data.get('where_condition', line_instance.where_condition)
            line_instance.open_parenthesis = line_data.get('open_parenthesis', line_instance.open_parenthesis)
            line_instance.filter = line_data.get('filter', line_instance.filter)
            line_instance.field_filter = line_data.get('field_filter', line_instance.field_filter)
            line_instance.operator = line_data.get('operator', line_instance.operator)
            line_instance.field_value = line_data.get('field_value', line_instance.field_value)
            line_instance.close_parenthesis = line_data.get('close_parenthesis', line_instance.close_parenthesis)
            line_instance.group_by = line_data.get('group_by', line_instance.group_by)
            line_instance.having_clause = line_data.get('having_clause', line_instance.having_clause)
            line_instance.order_by = line_data.get('order_by', line_instance.order_by)
            line_instance.functions = line_data.get('functions', line_instance.functions)
            line_instance.save()

        instance.name = validated_data.get('name', instance.name)
        instance.process = validated_data.get('process', instance.process)
        instance.site = validated_data.get('site', instance.site)
        instance.sequence = validated_data.get('sequence', instance.sequence)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.save()
        return instance

    class Meta:
        model = QueryBuilder
        fields = ('id', 'name', 'site', 'process', 'sequence', 'is_active', 'query_builder_lines', 'query_lines',
                  'site_name', 'created_by_name', 'modified_by_name', 'created_date', 'modified_date')


class WmsAllocationLineSerializer(serializers.ModelSerializer):
    created_by_name = serializers.SerializerMethodField()
    modified_by_name = serializers.SerializerMethodField()
    inventories_list = serializers.SerializerMethodField()
    sku_name = serializers.SerializerMethodField()

    def get_created_by_name(self, obj):
        return obj.created_by.name if obj.created_by else ''

    def get_modified_by_name(self, obj):
        return obj.modified_by.name if obj.modified_by else ''

    def get_inventories_list(self, obj):
        inventory_qs = obj.inventories.all()
        return InventorySerializer(inventory_qs, many=True).data

    def get_sku_name(self, obj):
        return obj.order_line.wh_sku.name if obj.order_line and obj.order_line.wh_sku else ''

    class Meta:
        model = WmsAllocationLine
        fields = ('id', 'order_line_id', 'inventories_list', 'created_by', 'created_by_name', 'modified_by',
                  'modified_by_name', 'created_date', 'modified_date', 'requested_qty', 'allocated_qty', 'sku_name')
