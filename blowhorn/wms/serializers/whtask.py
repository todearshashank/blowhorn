from rest_framework import serializers
from blowhorn.wms.submodels.tasks import WhTask


class WhTaskSerializer(serializers.ModelSerializer):

    task_type = serializers.SerializerMethodField()
    sku_name = serializers.SerializerMethodField()
    from_loc_name = serializers.SerializerMethodField()
    to_loc_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()
    operator_name = serializers.SerializerMethodField()

    def get_sku_name(self, obj):
        return obj.sku.name

    def get_from_loc_name(self, obj):
        if obj.from_loc:
            return obj.from_loc.name
        return None

    def get_to_loc_name(self, obj):
        if obj.to_loc:
            return obj.to_loc.name
        return None

    def get_client_name(self, obj):
        if obj.client:
            return obj.client.name
        return None

    def get_site_name(self, obj):
        return obj.site.name

    def get_operator_name(self, obj):
        if obj.operator:
            return obj.operator.name
        return obj.site.name

    def get_task_type(self, obj):
        if obj.task_type:
            return obj.task_type.name
        return None

    class Meta:
        model = WhTask
        fields = '__all__'
