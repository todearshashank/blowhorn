import logging
from rest_framework import serializers

from blowhorn.wms.submodels.receiving import GRNLineItems, PurchaseOrder, ASNOrder, \
    ASNLineItems, POLineItems
from blowhorn.wms.submodels.sku import PackConfig
from blowhorn.wms.submodels.receiving import PurchaseOrder, ASNOrder, ASNLineItems, POLineItems
from blowhorn.wms.submodels.sku import PackConfig
from blowhorn.wms.constants import ASN_COMPLETED, ASN_EXPIRED, ASN_NEW_STATUS, ASN_IN_PROGRESS

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class PackConfigSerializer(serializers.ModelSerializer):
    tracking_level = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PackConfig
        fields = '__all__'

    def get_tracking_level(self, pack_config):
        return pack_config.tracking_level.level_one_name if pack_config.tracking_level else None


class POLineSerializer(serializers.ModelSerializer):
    sku = serializers.SerializerMethodField(read_only=True)
    pack_config = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = POLineItems
        fields = '__all__'

    def get_sku(self, line):
        return line.sku.name

    def get_pack_config(self, line):
        sku = line.sku
        if sku and sku.pack_config:
            return PackConfigSerializer(sku.pack_config).data
        return []

class POLineSerializer(serializers.ModelSerializer):
    sku = serializers.SerializerMethodField(read_only=True)
    pack_config = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = POLineItems
        fields = '__all__'

    def get_sku(self, line):
        return line.sku.name

    def get_pack_config(self, line):
        sku = line.sku
        if sku and sku.pack_config:
            return PackConfigSerializer(sku.pack_config).data
        return []

class GRNLineItemsSerializer(serializers.ModelSerializer):
    sku = serializers.SerializerMethodField(read_only=True)
    pack_config_name = serializers.SerializerMethodField(read_only=True)
    sku_mrp = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = GRNLineItems
        fields = '__all__'

    def get_sku(self, line):
        return line.sku.name

    def get_pack_config_name(self, line):
        return line.sku.pack_config and line.sku.pack_config.name

    def get_sku_mrp(self, line):
        return line.sku.mrp


class ASNLineItemsSerializer(serializers.ModelSerializer):
    sku = serializers.SerializerMethodField(read_only=True)
    pack_config = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ASNLineItems
        fields = '__all__'

    def get_sku(self, line):
        return line.sku.name

    def get_pack_config(self, line):
        sku = line.sku
        if sku and sku.pack_config:
            return PackConfigSerializer(sku.pack_config).data
        return []


class PurchaseOrderSerializer(serializers.ModelSerializer):
    sku_lines = serializers.SerializerMethodField(read_only=True)
    grn_lines = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PurchaseOrder
        fields = '__all__'

    def get_sku_lines(self, order):
        return POLineSerializer(
                order.polineitems_set.all(),
                    many=True, required=False).data

    def get_grn_lines(self, order):
        return GRNLineItemsSerializer(
                order.grnlineitems_set.all(),
                    many=True, required=False).data


class ASNOrderSerializer(serializers.ModelSerializer):
    client_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()
    sku_lines = serializers.SerializerMethodField(read_only=True)

    def get_site_name(self, obj):
        if obj.site:
            return obj.site.name
        return None

    def get_client_name(self, obj):
        if obj.client:
            return obj.client.name
        return None

    class Meta:
        model = ASNOrder
        fields = '__all__'

    def get_sku_lines(self, order):
        return ASNLineItemsSerializer(
                order.asnlineitems_set.filter(
                    status__in=[ASN_NEW_STATUS, ASN_IN_PROGRESS]),
                    many=True, required=False).data
