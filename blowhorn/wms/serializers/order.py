from django.conf import settings
from rest_framework import serializers

from blowhorn.oscar.core.loading import get_model
from blowhorn.utils.functions import utc_to_ist
from blowhorn.wms.serializers.supplier import SupplierOrderLineSerializer
from config.settings import status_pipelines as StatusPipeline
from blowhorn.apps.driver_app.v3.serializers.order import ShippingAddressSerializer


Order = get_model('order', 'Order')
OrderLine = get_model('order', 'OrderLine')


class OrderLineSerializer(serializers.ModelSerializer):
    wh_sku_name = serializers.SerializerMethodField()
    shipping_address = serializers.SerializerMethodField()
    order_number = serializers.SerializerMethodField()

    def get_wh_sku_name(self, order_line):
        if order_line.wh_sku:
            return order_line.wh_sku.name
        return None

    def get_shipping_address(self, order_line):
        return ShippingAddressSerializer(instance=order_line.order.shipping_address).data

    def get_order_number(self, order_line):
        return order_line.order.number

    class Meta:
        model = OrderLine
        fields = (
        'id', 'wh_sku_id', 'wh_sku_name', 'quantity', 'qty_allocated', 'each_item_price', 'status', 'reference_number',
        'shipping_address', 'order_number')


class XdkOrderLineSerializer(serializers.ModelSerializer):
    sku_name = serializers.SerializerMethodField()
    sku_image = serializers.SerializerMethodField()
    qty = serializers.SerializerMethodField()

    class Meta:
        model = OrderLine
        fields = ('tracking_level', 'sku_name', 'sku_image', 'qty', )

    def get_sku_name(self, order_line):
        if order_line.wh_sku:
            return order_line.wh_sku.name
        return None

    def get_sku_image(self, order_line):
        if order_line.wh_sku and order_line.wh_sku.file:
            return order_line.wh_sku.file.url
        return None

    def get_qty(self, order_line):
        return order_line.quantity


class WmsOrderSerializer(serializers.ModelSerializer):
    order_lines = serializers.SerializerMethodField(read_only=True)
    site_name = serializers.SerializerMethodField(read_only=True)

    def get_site_name(self, obj):
        if obj.pickup_hub:
            return obj.pickup_hub.name
        return None

    def get_order_lines(self, order):
        return OrderLineSerializer(order.order_lines.all(), many=True).data

    class Meta:
        model = Order
        fields = (
        'id', 'number', 'reference_number', 'status', 'date_placed', 'pickup_datetime', 'updated_time', 'order_lines',
        'site_name')


class XDKOrderSerializer(serializers.ModelSerializer):
    customer_name = serializers.SerializerMethodField(read_only=True)
    created_date = serializers.SerializerMethodField(read_only=True)
    order_lines = serializers.SerializerMethodField(read_only=True)
    shipping_address = serializers.SerializerMethodField(read_only=True)
    is_returned_order = serializers.SerializerMethodField(read_only=True)


    class Meta:
        model = Order
        fields = ('id', 'number', 'reference_number', 'customer_name', 'created_date', 'order_lines',
                  'shipping_address', 'is_returned_order',)

    def get_customer_name(self, obj):
        return obj.customer.name

    def get_is_returned_order(self, obj):
        return obj.status == StatusPipeline.REACHED_AT_HUB_RTO

    def get_created_date(self, obj):
        return utc_to_ist(obj.created_date).isoformat()

    def get_order_lines(self, order):
        return XdkOrderLineSerializer(order.order_lines.all(), many=True).data

    def get_shipping_address(self, obj):
        shipping_address = obj.shipping_address
        return {"address": {
            "line1": shipping_address.line1,
            "line3": shipping_address.line3,
            "line4": shipping_address.state,
            "postcode": shipping_address.postcode,
        },
            "contact": {
                "mobile": str(shipping_address.phone_number),
                "name": shipping_address.name

            }}


class OrderLineChallanSerializer(serializers.ModelSerializer):
    sku_name = serializers.SerializerMethodField()
    supplier = serializers.SerializerMethodField(read_only=True)
    sku_id = serializers.SerializerMethodField()

    def get_sku_name(self, order_line):
        if order_line.wh_sku:
            return order_line.wh_sku.name
        else:
            return order_line.sku.name

    def get_sku_id(self, order_line):
        if order_line.wh_sku_id:
            return order_line.wh_sku_id
        else:
            return order_line.sku_id

    def get_supplier(self, order_line):
        if order_line.wh_sku:
            return SupplierOrderLineSerializer(order_line.supplier).data
        return None

    class Meta:
        model = OrderLine
        fields = (
            'id', 'sku_id', 'sku_name', 'quantity', 'each_item_price', 'hsn_code', 'cgst', 'sgst', 'igst',
            'cgst_amount', 'sgst_amount', 'igst_amount', 'taxable_value', 'total_amount',
            'status', 'reference_number',
            'supplier')


class OrderChallanSerializer(serializers.ModelSerializer):
    order_lines = serializers.SerializerMethodField(read_only=True)
    site_name = serializers.SerializerMethodField(read_only=True)
    customer_name = serializers.SerializerMethodField()
    shipping_address = serializers.SerializerMethodField()
    pickup_address = serializers.SerializerMethodField()
    transporter_name = serializers.SerializerMethodField()
    vehicle = serializers.SerializerMethodField()
    total_qty = serializers.SerializerMethodField()
    taxable_value = serializers.SerializerMethodField()
    cgst_amount = serializers.SerializerMethodField()
    sgst_amount = serializers.SerializerMethodField()
    igst_amount = serializers.SerializerMethodField()
    total_amount = serializers.SerializerMethodField()
    destination_state = serializers.SerializerMethodField()
    source_state = serializers.SerializerMethodField()
    currency_symbol = serializers.SerializerMethodField()

    def get_site_name(self, obj):
        if obj.pickup_hub:
            return obj.pickup_hub.name
        return None

    def get_customer_name(self, order):
        return order.customer.legalname

    def get_order_lines(self, order):
        return OrderLineChallanSerializer(order.order_lines.all(), many=True).data

    def get_shipping_address(self, order):
        return ShippingAddressSerializer(instance=order.shipping_address).data

    def get_pickup_address(self, order):
        return ShippingAddressSerializer(instance=order.pickup_address).data

    def get_transporter_name(self, order):
        return settings.BRAND_NAME

    def get_vehicle(self, order):
        if order.driver:
            return order.driver.driver_vehicle

    def get_total_qty(self, order):
        return sum([line.quantity for line in order.order_lines.all()])

    def get_taxable_value(self, order):
        return sum([line.taxable_value for line in order.order_lines.all() if line.taxable_value])

    def get_cgst_amount(self, order):
        return sum([line.cgst_amount for line in order.order_lines.all() if line.cgst_amount])

    def get_sgst_amount(self, order):
        return sum([line.sgst_amount for line in order.order_lines.all() if line.sgst_amount])

    def get_igst_amount(self, order):
        return sum([line.igst_amount for line in order.order_lines.all() if line.igst_amount])

    def get_total_amount(self, order):
        return sum([line.total_amount for line in order.order_lines.all() if line.total_amount])

    def get_destination_state(self, order):
        if order.order_state:
            return order.order_state.name
        return None

    def get_source_state(self, order):
        if order.source_state:
            return order.source_state.name
        elif order.order_state:
            return order.order_state.name
        return None

    def get_currency_symbol(self, order):
        return "Rs."

    class Meta:
        model = Order
        fields = (
        'id', 'number', 'reference_number', 'customer', 'status', 'date_placed', 'pickup_datetime', 'updated_time',
        'site_name', 'destination_state', 'pickup_address', 'source_state', 'shipping_address', 'transporter_name',
        'pickup_gstin', 'shipping_gstin', 'order_lines', 'total_qty', 'taxable_value',
        'cgst_amount', 'sgst_amount', 'igst_amount', 'total_amount', 'vehicle', 'customer_name',
        'rcm', 'currency_symbol')


