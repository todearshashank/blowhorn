from rest_framework import serializers

from blowhorn.oscar.core.loading import get_model

Inventory = get_model('wms', 'Inventory')
InventoryTransaction = get_model('wms', 'InventoryTransaction')
WhSku = get_model('wms', 'WhSku')
InventorySummary = get_model('wms', 'InventorySummary')


class InventorySerializer(serializers.ModelSerializer):

    sku_name = serializers.SerializerMethodField()
    location_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()
    uom = serializers.SerializerMethodField()

    def get_sku_name(self, obj):
        return obj.sku.name if obj.sku else ''

    def get_location_name(self, obj):
        return obj.location.name if obj.location else ''

    def get_client_name(self, obj):
        return obj.client.name if obj.client else ''

    def get_site_name(self, obj):
        return obj.site.name if obj.site else ''

    def get_uom(self, obj):
        return obj.sku.uom if obj.sku else ''

    class Meta:
        model = Inventory
        fields = '__all__'


class InventoryTransactionSerializer(serializers.ModelSerializer):

    sku_name = serializers.SerializerMethodField()
    from_loc_name = serializers.SerializerMethodField()
    to_loc_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()
    pack_config_name = serializers.SerializerMethodField()
    modified_by_name = serializers.SerializerMethodField()
    created_by_name = serializers.SerializerMethodField()
    order_number = serializers.SerializerMethodField()
    ref_number = serializers.SerializerMethodField()

    """
    NOTE: Order table won't be available in test & local environment since
            it's being ignored during backup.
    """
    def get_order_number(self, obj):
        try:
            return obj.order.number if obj.order else ''
        except BaseException as be:
            return ''

    def get_ref_number(self, obj):
        try:
            return obj.order.reference_number if obj.order else ''
        except BaseException as be:
            return ''

    def get_sku_name(self, obj):
        return obj.sku.name if obj.sku else ''

    def get_from_loc_name(self, obj):
        return obj.from_loc.name if obj.from_loc else ''

    def get_to_loc_name(self, obj):
        return obj.to_loc.name if obj.to_loc else ''

    def get_client_name(self, obj):
        return obj.client.name if obj.client else ''

    def get_site_name(self, obj):
        return obj.site.name if obj.site else ''

    def get_supplier_name(self, obj):
        return obj.supplier.name if obj.supplier else ''

    def get_pack_config_name(self, obj):
        return obj.pack_config.name if obj.pack_config else ''

    def get_modified_by_name(self, obj):
        return obj.modified_by.name if obj.modified_by else ''

    def get_created_by_name(self, obj):
        return obj.created_by.name if obj.created_by else ''

    class Meta:
        model = InventoryTransaction
        fields = '__all__'


class InventoryExportSerializer(serializers.ModelSerializer):
    queryset = Inventory.objects.all()
    sku_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()
    site_name = serializers.CharField(source='site.name')

    class Meta:
        model = Inventory
        fields = (
            'id', 'tag', 'sku_name', 'status', 'qty',
            'remarks', 'supplier_name', 'weight', 'expiry_date',
            'site_name', 'tracking_level',
            'mfg_date', 'created_date', 'modified_date', 'created_by',
            'modified_by')
        depth = 1

    def get_order_number(self, obj):
        return obj.order_number

    def get_sku_name(self, obj):
        sku = obj.sku
        if sku:
            return sku.name
        return ''

    def get_supplier_name(self, obj):
        supplier = obj.supplier
        if supplier:
            return supplier.name
        return ''


class InventoryTransactionExportSerializer(serializers.ModelSerializer):
    queryset = InventoryTransaction.objects.all()
    sku_name = serializers.SerializerMethodField()
    from_hub_name = serializers.SerializerMethodField()
    to_hub_name = serializers.SerializerMethodField()
    supplier_name = serializers.SerializerMethodField()

    class Meta:
        model = InventoryTransaction
        fields = ('id', 'tag', 'sku_name', 'from_hub_name', 'to_hub_name', 'qty', 'status', 'supplier_name',
                  'created_by', 'created_date', 'modified_by', 'modified_date')
        depth = 1

    def get_sku_name(self, obj):
        sku = obj.sku
        if sku:
            return sku.name
        return ''

    def get_from_hub_name(self, obj):
        hub = obj.from_hub
        if hub:
            return hub.name
        return ''

    def get_to_hub_name(self, obj):
        hub = obj.to_hub
        if hub:
            return hub.name
        return ''

    def get_supplier_name(self, obj):
        supplier = obj.supplier
        if supplier:
            return supplier.name
        return ''

class StockSummaryListSerializer(serializers.Serializer):
    quantity = serializers.IntegerField(read_only=True)
    sku_name = serializers.SerializerMethodField()
    sku_id = serializers.SerializerMethodField()
    product_group_name = serializers.SerializerMethodField()
    pack_config_name = serializers.SerializerMethodField()

    class Meta:
        fields = ('sku_name', 'sku_id', 'product_group_name', 'pack_config_name', 'quantity')

    def get_sku_id(self, obj):
        return obj.get('sku_id', '')

    def get_sku_name(self, obj):
        return obj.get('sku_name', '')

    def get_quantity(self, obj):
        return obj.get('quantity', '')

    def get_product_group_name(self, obj):
        return obj.get('product_group_name', '')

    def get_pack_config_name(self, obj):
        return obj.get('pack_config_name', '')
