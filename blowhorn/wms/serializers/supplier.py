import phonenumbers
from django.conf import settings
from rest_framework import serializers, exceptions

from phonenumber_field.phonenumber import PhoneNumber

from blowhorn.oscar.core.loading import get_model
from blowhorn.apps.operation_app.v5.serializers.vendor import VendorAddressSerializer

Supplier = get_model('wms', 'Supplier')


class SupplierOrderLineSerializer(serializers.ModelSerializer):
    invoice_address = serializers.SerializerMethodField()

    def get_invoice_address(self, order_line):
        return VendorAddressSerializer(instance=order_line.order.shipping_address).data

    class Meta:
        model = Supplier
        fields = (
            'id', 'name', 'phone_number', 'invoice_address', 'state', 'gstin')


class SupplierSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(SupplierSerializer, self).__init__(*args, **kwargs)
        self.customer = self.context.get('customer', None) if self.context else None

    def validate(self, attrs):
        name = attrs.get('name', None)
        id_ = attrs.get('id', None)
        if id_ and Supplier.objects.filter(name=name, customer=self.customer).exclude(id=id_).exists():
            _out = {
                "status": "FAIL",
                "message": "Given Supplier name already exists"
            }
            raise exceptions.ParseError(_out)

        phone_number = attrs.get('phone_number', None)

        try:
            phone = PhoneNumber.from_string(
                phone_number, settings.COUNTRY_CODE_A2)
            if not phone.is_valid():
                _out = {
                    'status': 'FAIL',
                    'message': 'Invalid Phone number'
                }
                raise exceptions.ParseError(_out)
        except phonenumbers.NumberParseException:
            _out = {
                'status': 'FAIL',
                'message': 'Invalid Phone number'
            }
            raise exceptions.ParseError(_out)

    class Meta:
        model = Supplier
        fields = '__all__'
