from rest_framework import serializers, status

from django.utils.translation import ugettext_lazy as _

from blowhorn.oscar.core.loading import get_model

Location = get_model('wms', 'Location')
LocationZone = get_model('wms', 'LocationZone')
LocationType = get_model('wms', 'LocationType')
WhSite = get_model('wms', 'WhSite')
Client = get_model('wms', 'Client')


class LocationZoneSerializer(serializers.ModelSerializer):

    num_active_locations = serializers.IntegerField(read_only=True)
    site_name = serializers.SerializerMethodField(read_only=True)
    created_by_name = serializers.SerializerMethodField(read_only=True)
    modified_by_name = serializers.SerializerMethodField(read_only=True)

    def validate(self, attrs):
        name = attrs.get('name', None)
        _status = attrs.get('status', None)
        site_id = attrs.get('site', None)
        if not name or not _status or not site_id:
            raise serializers.ValidationError(
                code=status.HTTP_400_BAD_REQUEST,
                detail=_('Name, Status, Site is mandatory to create a zone')
            )

        return attrs

    def get_site_name(self, obj):
        if obj:
            return obj.site.name

    def get_created_by_name(self, obj):
        return obj and obj.created_by and obj.created_by.name

    def get_modified_by_name(self, obj):
        return obj and obj.modified_by and obj.modified_by.name

    class Meta:
        model = LocationZone
        fields = '__all__'


class LocationTypeSerializer(serializers.ModelSerializer):

    customer_name = serializers.SerializerMethodField(read_only=True)
    created_by_name = serializers.SerializerMethodField(read_only=True)
    modified_by_name = serializers.SerializerMethodField(read_only=True)

    def get_customer_name(self, obj):
        if obj:
            return obj.customer.name

    def get_created_by_name(self, obj):
        return obj and obj.created_by and obj.created_by.name

    def get_modified_by_name(self, obj):
        return obj and obj.modified_by and obj.modified_by.name

    class Meta:
        model = LocationType
        fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):

    zone_name = serializers.SerializerMethodField()
    client_name = serializers.SerializerMethodField()
    site_name = serializers.SerializerMethodField()
    rack_name = serializers.SerializerMethodField()
    loc_dimensions = serializers.SerializerMethodField()
    created_by_name = serializers.SerializerMethodField(read_only=True)
    modified_by_name = serializers.SerializerMethodField(read_only=True)

    def get_loc_dimensions(self, obj):
        if obj.dimensions:
            return '%s * %s * %s cub. %s' % (
            obj.dimensions.length, obj.dimensions.breadth, obj.dimensions.height, obj.dimensions.uom)
        return None

    def get_rack_name(self, obj):
        if obj.rack:
            return obj.rack.name
        return None

    def get_zone_name(self, obj):
        return obj.zone.name

    def get_client_name(self, obj):
        if obj.client:
            return obj.client.name
        return None

    def get_site_name(self, obj):
        return obj.site.name

    def get_created_by_name(self, obj):
        return obj and obj.created_by and obj.created_by.name

    def get_modified_by_name(self, obj):
        return obj and obj.modified_by and obj.modified_by.name

    class Meta:
        model = Location
        fields = '__all__'


class WhSiteSerializer(serializers.ModelSerializer):

    hub_name = serializers.SerializerMethodField(read_only=True)
    site_address = serializers.SerializerMethodField(read_only=True)
    site_shipping_address = serializers.SerializerMethodField(read_only=True)
    city_name = serializers.SerializerMethodField(read_only=True)

    def get_city_name(self, obj):
        return obj.city.name if obj and obj.city else '-'

    def get_hub_name(self, obj):
        return obj.hub.name if obj.hub else ''

    def get_site_address(self, obj):
        address = obj.address
        if address:
            address_data = {
                'line1': address.line1,
                'line2': address.line2,
                'line3': address.line3,
                'line4': address.line4,
                'state': address.state,
                'postcode': address.postcode,
                'location': {'lat': address.geopoint.y, 'lon': address.geopoint.x} if address.geopoint else ''
            }
            return address_data
        return {}

    def get_site_shipping_address(self, obj):
        address = obj.address
        if address:
            address_data = {
                'line1': address.line1,
                'line2': address.line2,
                'line3': address.line3,
                'line4': address.line4,
                'state': address.state,
                'postcode': address.postcode,
                'location': {'lat': address.geopoint.y, 'lon': address.geopoint.x} if address.geopoint else ''
            }
            return address_data
        return {}

    class Meta:
        model = WhSite
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = '__all__'
