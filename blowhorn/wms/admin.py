
from django.contrib import admin
from django.contrib.admin.helpers import ActionForm
from django import forms

from blowhorn.wms.submodels.inbound import Supplier
from blowhorn.wms.models import ProductGroup, WmsConstants


@admin.register(ProductGroup)
class ProductGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')

    fields = ('name', 'description', 'customer')

    list_filter = ('name',)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = \
                Customer.objects.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(WmsConstants)
class WmsConstantsAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'description')

    fields = ('name', 'value', 'description')


@admin.register(Supplier)
class SupplierAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone_number', 'status', 'current_address')
    list_filter = ('status', 'customer__name')

    fields = ('name', 'phone_number', 'customer', 'status', 'photo', 'current_address', 'is_address_same',
              'bank_account', 'reason_for_inactive', 'remarks', 'tos_response_date')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'customer':
            kwargs['queryset'] = \
                Customer.objects.exclude(customer_category=CUSTOMER_CATEGORY_INDIVIDUAL)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class ProductGroupImportForm(ActionForm):
    file = forms.FileField(required=True)


from blowhorn.wms.subadmins.location import *
from blowhorn.wms.subadmins.inventory import *
from blowhorn.wms.subadmins.sku import *
from blowhorn.wms.subadmins.tasks import *
from blowhorn.wms.subadmins.receving import *
from blowhorn.wms.subadmins.query_builder import *
from blowhorn.wms.subadmins.qualitycheck import *
from blowhorn.wms.subadmins.containers import *
from blowhorn.wms.subadmins.returnorder import *
from blowhorn.wms.subadmins.yard import *
from blowhorn.wms.subadmins.stock_check import *


