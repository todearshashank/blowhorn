from django.conf.urls import url, include

urlpatterns = [
    url(r'^wms/v1/', include('blowhorn.wms.v1.app')),
    url(r'^wms/v2/', include('blowhorn.wms.v2.app')),
]
