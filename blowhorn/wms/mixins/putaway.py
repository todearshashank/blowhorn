from rest_framework import status
from django.utils import timezone

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.submodels.sku import WhSku
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.submodels.location import Location, Client
from blowhorn.wms.submodels.tasks import WhTask
from django.db import transaction
from blowhorn.wms.constants import PUTAWAY_TASK, LOC_UNLOCKED, WH_TASK_NEW, WH_TASK_ASSIGEND, WH_TASK_INPROGRESS, WH_TASK_COMPLETED

Hub = get_model('address', 'Hub')


class PutAwayMixins(object):

    def _initialize_attributes(self, data, user):
        self.user = user
        self.sku_name = data.get('sku_name')
        self.wh_task_id = data.get('id')
        self.pack_config = data.get('pack_config')
        self.qty = data.get('qty')
        self.site = data.get('site_id')
        self.client = data.get('client_name')
        self.tag = data.get('tag')
        self.inv_status = data.get('inv_status')
        # self.new_tag = data.get('new_tag')  # for partial movement, user will give tag name
        self.to_loc = data.get('to_loc_name')
        self.error_msg = None

        self._get_putaway_data()

    def _get_putaway_data(self):
        if self.client:
            self.client = Client.objects.filter(name=self.client).first()
        if not self.client:
            self.error_msg = 'Invalid Client'
            return

        self.sku = None
        if self.sku_name:
            self.sku = WhSku.objects.filter(name=self.sku_name, client_id=self.client).first()
        if not self.sku:
            self.error_msg = 'Invalid Sku id'
            return

        if self.site:
            self.site = Hub.objects.filter(pk=self.site).first()
        if not self.site:
            self.error_msg = 'Invalid Site id'
            return

        wh_task = None
        if self.wh_task_id:
            wh_task = WhTask.objects.filter(id=self.wh_task_id, task_type__name=PUTAWAY_TASK).first()
            self.from_loc = wh_task.from_loc
            self.wh_task = wh_task
        if not wh_task:
            self.error_msg = 'Invalid putAway task'
            return

        # if self.wh_task.status != PUTAWAY_INPROGRESS:
        #     self.error_msg = 'Putaway task is not in In-Progress status'
        #     self.status = status.HTTP_400_BAD_REQUEST
        #     return

        if self.wh_task.operator != self.user:
            self.error_msg = 'Putaway task is assigned to a different user'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if self.to_loc:
            self.to_loc = Location.objects.filter(name=self.to_loc).first()
            # TODO handle site for blind received tasks as site is currently not captured
            # self.to_loc = Location.objects.filter(name=self.to_loc, site=self.site).first()
            if not self.to_loc:
                self.error_msg = 'Invalid To Location'
                return
        else:
            # todo write the algorithm to find putway location
            # consider cold storage location for applicable items
            self.to_loc = self._get_putaway_location()

        if self.wh_task.to_loc.name != self.to_loc.name:
            self.error_msg = 'To loc does not match with putaway task'
            return

        self.inv = Inventory.objects.filter(tag=self.tag, sku=self.sku, client=self.client,
                                            location=self.from_loc).first()
        if not self.inv:
            self.error_msg = 'Invalid tag name or client'
            return
        if self.qty > self.inv.qty:
            self.error_msg = 'Invalid qty, qty available is %s' % (self.inv.qty)
            return

    # TODO
    def _get_putaway_location(self):
        loc = Location.objects.filter(status=LOC_UNLOCKED, is_putaway=True).first()
        return loc

    def do_putaway(self):
        with transaction.atomic():
            # not sure inventory will have one sku  or for every sku an inventory will be created
            if self.inv.sku == self.sku:
                if self.inv.qty == self.qty:
                    # full movement
                    inv = Inventory.objects.filter(tag=self.tag, client=self.client,
                                                   sku=self.wh_task.sku,
                                                   location=self.to_loc,
                                                   site=self.wh_task.site).first()
                    if inv:
                        # the destination location already has that inv, so update the qty
                        inv.qty += self.qty
                        inv.save()
                    else:
                        Inventory.objects.filter(tag=self.tag, client=self.client,
                                                 sku=self.wh_task.sku,
                                                 location=self.from_loc,
                                                 site=self.wh_task.site).update(
                            location=self.to_loc)
                    WhTask.objects.filter(pk=self.wh_task.id).update(status=WH_TASK_COMPLETED)
                    # BlindReceivingMixins._write_itl()
                    InventoryTransaction.objects.create(tag=self.tag, from_loc=self.from_loc, to_loc=self.to_loc,
                                                        sku=self.sku, client=self.client, qty=self.qty, status='Moved',
                                                        site=self.wh_task.site, reference=PUTAWAY_TASK, reference_number=self.wh_task.id,
                                                        supplier=self.wh_task.supplier)
                else:
                    # partial  movement
                    rem_qty = self.inv.qty - self.qty
                    inv = Inventory.objects.filter(tag=self.tag, client=self.client,
                                                   sku=self.wh_task.sku,
                                                   location=self.to_loc,
                                                   site=self.wh_task.site).first()
                    if inv:
                        inv.qty += self.qty
                        inv.save()
                    else:
                        Inventory.objects.filter(tag=self.tag, client=self.client,
                                                 sku=self.wh_task.sku,
                                                 location=self.from_loc,
                                                 site=self.wh_task.site).update(
                            location=self.to_loc, qty=self.qty)
                    Inventory.objects.create(tag=self.tag, sku=self.sku, client=self.client, qty=rem_qty,
                                             site=self.site, status=self.inv_status,
                                             weight=self.weight, location=self.from_loc,
                                             supplier=self.wh_task.supplier)
                    WhTask.objects.filter(pk=self.wh_task.id).update(status=WH_TASK_COMPLETED)

                    WhTask.objects.create(task_type=self.wh_task.task_type, from_loc=self.from_loc, to_loc=self.to_loc,
                                          sku=self.sku, client=self.client, qty=rem_qty, tag=self.new_tag, status=WH_TASK_NEW,
                                          site=self.site, supplier=self.wh_task.supplier)

                    InventoryTransaction.objects.create(tag=self.tag, from_loc=self.from_loc, to_loc=self.to_loc,
                                                        sku=self.sku, client=self.client, qty=self.qty, status='Moved',
                                                        site=self.site, reference=PUTAWAY_TASK, reference_number=self.wh_task.id,
                                                        supplier=self.wh_task.supplier)


class StartPutAwayMixins(object):

    def _initialize_attributes(self, data, user):
        self.user = user
        self.error_msg = None
        self.status = status.HTTP_200_OK
        putaway_task_id = data.get('id')
        if not putaway_task_id:
            self.error_msg = 'Putaway task id is not given'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        try:
            self.putaway_task = WhTask.objects.get(pk=putaway_task_id, task_type__name=PUTAWAY_TASK)
        except:
            self.error_msg = 'Invalid putaway task id'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if self.putaway_task.operator and self.putaway_task.operator != self.user:
            self.error_msg = 'Putaway task is assigned to a different user'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if self.putaway_task.status not in [WH_TASK_NEW, WH_TASK_ASSIGEND]:
            self.error_msg = 'Putaway task is not in status - %s' % self.putaway_task.status
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def _start_putaway(self):
        self.putaway_task.operator = self.user
        self.putaway_task.status = WH_TASK_INPROGRESS
        self.putaway_task.modified_date = timezone.now()
        self.putaway_task.save()

