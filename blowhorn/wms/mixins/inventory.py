import pytz
import pandas as pd
from datetime import datetime
import logging
from openpyxl import load_workbook
from django.core.files.base import ContentFile
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db.models import (Sum, F, Subquery, OuterRef,
    ExpressionWrapper, DecimalField)
from django.db import transaction
from django.conf import settings
from rest_framework import status
from blowhorn.common.helper import CommonHelper
from blowhorn.common.utils import generate_pdf
from blowhorn.address.models import Hub
from blowhorn.address.utils import get_full_address_str
from blowhorn.wms.submodels.receiving import GRNLineItems, PurchaseOrder
from blowhorn.customer.models import CustomerTaxInformation
from blowhorn.order.models import Dimension
from blowhorn.wms.submodels.inbound import Supplier
from blowhorn.wms.submodels.inventory import WhSite, \
        Inventory, InventoryTransaction, InventorySummary
from blowhorn.wms.submodels.location import Location, Client
from blowhorn.wms.submodels.sku import WhSku, TrackingLevel
from blowhorn.wms.constants import ASN_COMPLETED, ASN_EXPIRED, ASN_NEW_STATUS, \
        INVENTORY_MANDATORY_LABELS, TAG_PREFIX_CODE, WMS_QTY_ADD, \
        WMS_RECEIVED, WMS_SOURCE_IMPORT, LOC_UNLOCKED
from blowhorn.customer.mixins import CustomerMixin

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class DataImportMixins(object):
    def initialize_attributes(self, data, customer):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.data = data
        self.customer = customer
        self.inventory_list = []
        self.inventory_transaction_list = []

    def create_inventory_data(self):
        with transaction.atomic():
            inventory_import = self.data.get('inventory_data')
            if inventory_import:
                for i, inventory in enumerate(inventory_import):
                    inventory_data = {}
                    tag = inventory.get('tag')
                    if not tag:
                        self.error_msg = 'Tag not found'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                    site_name = inventory.get('site')
                    if not site_name:
                        self.error_msg = 'Site not given'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return
                    site = WhSite.objects.filter(name=site_name).first()
                    if not site:
                        self.error_msg = 'site not valid'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                    sku_name = inventory.get('sku')
                    if not sku_name:
                        self.error_msg = 'Sku is mandatory'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return
                    sku = WhSku.objects.filter(name=sku_name).first()
                    if not sku:
                        self.error_msg = 'Sku not valid'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                    location_name = inventory.get('location')
                    if not location_name:
                        self.error_msg = 'Location is mandatory'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return
                    location = Location.objects.filter(name=location_name, site=site).first()
                    if not location:
                        self.error_msg = 'Location not valid'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                    client_name = inventory.get('client')
                    client = None
                    if client_name:
                        client = Client.objects.filter(name=client_name, customer=self.customer).first()

                        if not client:
                            self.error_msg = 'Client not valid'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                    qty = inventory.get('qty')

                    allocated_qty = inventory.get('allocated_qty')

                    status_ = inventory.get('status')
                    if not status_:
                        self.error_msg = 'Status not given'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return
                    weight = inventory.get('weight')
                    dimension = inventory.get('dimensions')
                    dim = None
                    if dimension:
                        length = dimension.get('length')
                        breadth = dimension.get('breadth')
                        height = dimension.get('height')
                        uom = dimension.get('uom')
                        dimension_dict = {
                            'length': length,
                            'breadth': breadth,
                            'height': height,
                            'uom': uom

                        }
                        dim = Dimension.objects.filter(**dimension_dict).first()
                        if not dim:
                            dim = Dimension.objects.create(**dimension_dict)

                    expiry_date = inventory.get('expiry_date')

                    supplier_name = inventory.get('supplier')
                    supplier = None
                    if supplier_name:
                        supplier = Supplier.objects.filter(name=supplier_name).first()
                        if not supplier:
                            self.error_msg = 'Supplier is invalid'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                    remarks = inventory.get('remarks')

                    inventory_data['tag'] = tag
                    inventory_data['sku'] = sku
                    inventory_data['location'] = location
                    inventory_data['site'] = site
                    inventory_data['client'] = client
                    inventory_data['qty'] = qty
                    inventory_data['allocated_qty'] = allocated_qty
                    inventory_data['status'] = status_
                    inventory_data['weight'] = weight
                    inventory_data['expiry_date'] = expiry_date
                    inventory_data['supplier'] = supplier
                    inventory_data['remarks'] = remarks
                    inventory_data['dimensions'] = dim
                    inventory_data['qty_diff'] = qty
                    inventory_data['qty_left'] = qty
                    inventory_data['operation'] = WMS_QTY_ADD

                    self.inventory_list.append(Inventory(**inventory_data))

                    Itl = {'tag': tag,
                           'from_loc': location,
                           'sku': sku,
                           'site': site,
                           'client': client,
                           'qty': qty,
                           'uom': '',
                           'supplier': supplier,
                           'status': status_,
                           'reference': ''}
                    self.inventory_transaction_list.append(InventoryTransaction(**Itl))
                Inventory.objects.bulk_create(self.inventory_list)
                InventoryTransaction.objects.bulk_create(self.inventory_transaction_list)

class InventoryImportMixin(object):

    def _process_import_data(self, data, request):
        """
            This method intializes the variables and perform basic prechecks
            on input file like file name, customer id and empty file check.
            Also does the row processing of the excel sheet where each row
            is equivalent to one order.
        """
        self.file_name = data.get('file')
        self.response = {'data': '', 'errors': ''}
        self.success = False
        self.status_code = status.HTTP_200_OK
        self.row_index = 0
        self.user = request.user
        self.pdf_grn_line_dict = {}
        self.grn_line_items_dict = {}
        self.site_list = set()

        self.values_list = []
        self.inventory_data_list = []
        self.inventory_trans_data_list = []

        # Initialize error lists for further processing
        self._initialize_error_list()

        # Load the workbook and initial validation
        error_message = self._initial_validations()
        if error_message:
            return self.success, error_message

    def _initial_validations(self):
        try:
            wb_obj = load_workbook(filename=self.file_name, data_only=True)
        except Exception as e:
            error_message = 'The file may be corrupted or not in xlsx format.' \
                            ' Please download a new sample sheet.'
            return error_message

        sheet_obj = wb_obj.active
        headers = [cell.value for cell in sheet_obj[1]]
        self.no_of_rows = sheet_obj.max_row

        for i in INVENTORY_MANDATORY_LABELS + ['Sl No.']:
            if i not in headers:
                error_message = 'Incorrect file template uploaded. Please download the template again and upload'
                return error_message
        row_error = self._row_processing(sheet_obj, headers)
        if not self.values_list:
            error_message = 'File is empty'
            return error_message
        if len(row_error) != 0:
            error_message = 'Incorrect file format'
            return error_message

        created_inventory, message = self._create_inventory_from_data(self.values_list)
        self.success = True if created_inventory else False
        message = 'Uploaded Successfully' if created_inventory else message
        return message

    def _initialize_error_list(self):
        self.records_with_incorrect_hub_name = []
        self.records_with_incorrect_supplier_name = []
        self.records_with_incorrect_sku_name = []
        self.records_with_incorrect_site_name = []
        self.records_without_mandatory_fields = []
        self.records_with_duplicate_tags = []
        self.records_with_existing_tags = []
        self.site_tag_dict = {}
        self.records_without_hub_permission = []
        self.records_with_incorrect_location_name = []
        self.records_with_incorrect_date_format = []
        self.records_with_invalid_tracking_level = []

    def _row_processing(self, sheet_obj, headers):
        index = 0
        row_error = []
        for row in sheet_obj.iter_rows(min_row=2):
            inventory_data_dict = {}
            index += 1
            for key, cell in zip(headers, row):
                if cell.value:
                    value = cell.value
                    inventory_data_dict[key] = value
            if inventory_data_dict:
                self.values_list.append(inventory_data_dict)
        return row_error

    def _create_inventory_from_data(self, inventory_data):
        self.customer = CustomerMixin().get_customer(self.user)
        for data in inventory_data:
            supplier = None
            client = None
            tracking_level = None
            pack_config = None
            weight = None
            self.row_index += 1

            if not set(list(data.keys())).intersection(set(INVENTORY_MANDATORY_LABELS)) == set(
                    INVENTORY_MANDATORY_LABELS):
                self.records_without_mandatory_fields.append(self.row_index)
                continue
                # return False, 'Mandatory fields are missing %s ' % INVENTORY_MANDATORY_LABELS

            site = data['Site']
            site = WhSite.objects.filter(name=site).first()
            if not site:
                self.records_with_incorrect_site_name.append(self.row_index)
            self.hub = site.hub if site else None

            sku_name = data['SKU Name']
            sku = WhSku.objects.filter(name=sku_name).first()
            if not sku:
                self.records_with_incorrect_sku_name.append(self.row_index)
            qty = data['Quantity']
            if sku:
                weight = float(sku.each_weight) * float(qty) if sku and sku.each_weight else None
                pack_config = sku.pack_config if sku else None
                tracking_level_obj = pack_config.tracking_level
                client = sku.client if sku else None
                supplier = sku.supplier if sku else None
                tracking_level = data['Tracking Level']
                if not (tracking_level_obj.level_one_name != tracking_level or
                        tracking_level_obj.level_two_name != tracking_level or
                        tracking_level_obj.level_three_name != tracking_level):
                    self.records_with_invalid_tracking_level.append(self.row_index)

                if TrackingLevel.objects.filter(level_one_name=tracking_level, customer=self.customer).exists():
                    weight = float(sku.each_weight) * float(qty) if sku and sku.each_weight else None
                elif TrackingLevel.objects.filter(level_two_name=tracking_level, customer=self.customer).exists():
                    weight = float(sku.each_weight) * float(qty) * float(
                        tracking_level_obj.ratio_level_two_one) if sku and sku.each_weight else None
                elif TrackingLevel.objects.filter(level_three_name=tracking_level, customer=self.customer).exists():
                    weight = float(sku.each_weight) * float(qty) * float(
                        tracking_level_obj.ratio_level_two_one) * float(
                        tracking_level_obj.ratio_level_three_two) if sku and sku.each_weight else None

            location = Location.objects.first()
            if not location:
                self.records_with_incorrect_location_name.append(self.row_index)

            tag = CommonHelper().get_unique_friendly_id(TAG_PREFIX_CODE)
            if Inventory.objects.filter(tag=tag, site=site).exists():
                self.records_with_existing_tags.append(self.row_index)
            elif self.site_tag_dict.get(site) and self.site_tag_dict[site] == tag:
                self.records_with_duplicate_tags.append(self.row_index)
            else:
                self.site_tag_dict[site] = tag

            expiry_date = data.get('Expiry Date (YYYY/MM/DD)', None)

            if expiry_date and not isinstance(expiry_date, datetime):
                self.records_with_incorrect_date_format.append(self.row_index)

            inventory_dict = {
                'tag': tag,
                'sku_id': sku.id if sku else None,
                'qty': qty,
                'supplier_id': supplier.id if supplier else None,
                'site_id': site.id if site else None,
                'location_id': location.id if location else None,
                'tracking_level': tracking_level,
                'weight': weight,
                'pack_config': pack_config,
                'client': client,
                'expiry_date': expiry_date,
                'created_by': self.user
            }

            inv_trans_dict = {
                'tag': tag,
                'sku_id': sku.id if sku else None,
                'qty': qty,
                'supplier_id': supplier.id if supplier else None,
                'site_id': site.id if site else None,
                'from_loc_id': location.id if location else None,
                'to_loc_id': location.id if location else None,
                'tracking_level': tracking_level,
                'pack_config': pack_config,
                'client': client,
                'status': WMS_RECEIVED,
                'source': WMS_SOURCE_IMPORT,
                'created_by': self.user,
                'qty_diff': qty,
                'qty_left': qty,
                'operation': WMS_QTY_ADD
            }

            if sku:
                self._update_grn_line_items(site, sku, tracking_level_obj, qty)
                self.inventory_data_list.append(Inventory(**inventory_dict))
                self.inventory_trans_data_list.append(InventoryTransaction(**inv_trans_dict))

        error = self._raise_error()
        if error:
            return False, error

        created = Inventory.objects.bulk_create(self.inventory_data_list)
        InventoryTransaction.objects.bulk_create(self.inventory_trans_data_list)
        self.generate_grn_document()

        return True, len(created)

    def generate_grn_document(self):
        if not self.grn_line_items_dict:
            return

        _supplier = Supplier.objects.filter(
                            customer=self.customer,
                            name='Default Supplier'
                        ).first()

        hub_data = '%s %s' % (self.hub.name, get_full_address_str(self.hub.address))
        gstin = CustomerTaxInformation.objects.filter(customer=self.customer).values_list('gstin', flat=True).first()

        # loop through each site
        for site_id in self.site_list:
            # create purchase order for the site
            _timestamp = datetime.now().strftime("%d%m%y%H%M%S%f")
            _purchase_order = PurchaseOrder.objects.create(status=ASN_NEW_STATUS, site_id=site_id,
                    delivery_date=datetime.now(), po_number=_timestamp,
                    supplier=_supplier)

            # get the GRN line items and pdf items for site
            _pdf_li = self.pdf_grn_line_dict.get(site_id)
            if not _pdf_li:
                continue
            total_cost = sum([e.get('price', 0) for e in _pdf_li])

            _grn_li = []
            _grnli = self.grn_line_items_dict.get(site_id)
            for e in _grnli:
                e['purchase_order'] = _purchase_order
                _grn_li.append(GRNLineItems(**e))

            # create the GRN line items
            GRNLineItems.objects.bulk_create(_grn_li)
            _purchase_order.status = ASN_EXPIRED

            # set the data for rendering in pdf
            context = {}
            tz = pytz.timezone(settings.ACT_TIME_ZONE)
            grn_number = 'RN%s' % _timestamp
            context['grn_number'] = grn_number
            context['date'] = datetime.now().astimezone(tz).strftime('%d %b %Y %I:%M %p')
            context['items'] = _pdf_li
            context['supplier_name'] = self.customer.name
            context['gstin'] = gstin
            context['address'] = hub_data
            context['receiver_name'] = self.user.name
            context['total_price'] = intcomma(round(total_cost, 2))
            _purchase_order.receiving_number = grn_number
            _purchase_order.total_price = total_cost

            bytes = generate_pdf('pdf/grn/grn.html', context)
            if bytes:
                filename = '%s.pdf' % grn_number
                _purchase_order.file.save(filename, ContentFile(bytes), save=True)
                _purchase_order.status = ASN_COMPLETED

            _purchase_order.save()

    def _update_grn_line_items(self, site, sku, tracking_level, qty):
        self.site_list.add(site.id)
        _grn_pdf_li = {
            "name": sku.name,
            "size": (tracking_level.level_two_name or
                        tracking_level.level_one_name).title(),
            "quantity": 0,
            "price": float(sku.mrp or 0) * float(qty),
            "delivered_quantity": qty
        }

        _grn_li = {
                'sku_id' : sku.id,
                'qty' : qty
        }
        if site.id in self.pdf_grn_line_dict:
            _existing_pdf_list = self.pdf_grn_line_dict.get(site.id)
            _existing_pdf_list.append(_grn_pdf_li)

            _existing_grn_list = self.grn_line_items_dict.get(site.id)
            _existing_grn_list.append(_grn_li)
        else:
            self.pdf_grn_line_dict[site.id] = [_grn_pdf_li,]
            self.grn_line_items_dict[site.id] = [_grn_li,]

    def _raise_error(self):
        error_message = ''
        if self.records_with_incorrect_site_name:
            error_message = ' Incorrect site name provided in row numbers %s. ' % self.records_with_incorrect_site_name
        if self.records_with_incorrect_location_name:
            error_message += ' Could not create Inventory. Please contact support'
        if self.records_with_incorrect_sku_name:
            error_message += (' Incorrect sku name provided in row numbers %s. ' % self.records_with_incorrect_sku_name)
        if self.records_with_incorrect_supplier_name:
            error_message += (
                ' Incorrect supplier name provided in row numbers %s. ' % self.records_with_incorrect_supplier_name)
        if self.records_without_mandatory_fields:
            error_message += (
                ' Mandatory fields like Tag, Hub, SKU Name, Quantity,Supplier are missing in rows %s. '
                % self.records_without_mandatory_fields
            )
        if self.records_with_existing_tags:
            error_message += (
                ' Tags already exist for rows %s for the respective site. ' % self.records_with_existing_tags
            )
        if self.records_with_duplicate_tags:
            error_message += (
                ' Duplicate Tags given in rows %s. Tag has to be unique. ' % self.records_with_duplicate_tags
            )
        if self.records_without_hub_permission:
            error_message += (
                ' You do not have permission to add inventory for the hubs given in rows %s. '
                % self.records_without_hub_permission
            )
        if self.records_with_incorrect_date_format:
            error_message += (
                ' Incorrect date format provided in rows %s.Please provide date in YYYY/MM/DD format. '
                % self.records_with_incorrect_date_format
            )
        if self.records_with_invalid_tracking_level:
            error_message += (
                ' Invalid tracking level given in rows %s. '
                'Please select the tracking level that belongs to the '
                'SKU packconfig only' % self.records_with_invalid_tracking_level
            )
        return error_message

class InventorySummaryData:

    def create_inventory_summary():
        _data = Inventory.objects.filter(wmsmanifestline__order_line__order__isnull=True)

        _data = Inventory.objects.values('site','location','sku', 'status'
                    ).annotate(quantity = Sum(F('qty')))

        cs = ['site_id','location_id','sku_id','status','quantity']
        df = pd.DataFrame.from_records(_data.values(),columns=cs)
        ppf = pd.pivot_table(df,index=['site_id','location_id','sku_id'],
                columns='status',values='quantity',fill_value=0,
                margins=False).reset_index()

        _ppf = ppf.rename(columns={"Locked": "unavailable_qty", "UnLocked": "available_qty"})

        InventorySummary.objects.bulk_create(
            InventorySummary(**vals) for vals in _ppf.to_dict('records'))

    def get_customer_inventory(parms):
        sku_id = parms.get('sku_id', None)
        site = parms.get('site_id', None) or None
        location = parms.get('location_id', None) or None
        qs = {
            "sku_id" : sku_id,
            "status" : LOC_UNLOCKED,
            "wmsmanifestline__order_line__order__isnull" : True
        }

        if site:
            qs['site_id'] = site
        if location:
            qs['location_id'] = location

        return Inventory.objects.select_related('location', 'site').filter(**qs)

    def get_customer_inventory_summary(customer_id, params):
        site = params.get('site_id', None) or None
        location = params.get('location_id', None) or None
        ordering = params.get('ordering', None)

        qs = {
            'client__customer_id': customer_id
        }
        search_term = params.get('search_term', '')
        if search_term:
            qs['name__icontains'] = search_term

        _summary = WhSku.objects.filter(**qs)
        if not _summary:
            return []

        inventory_qs = {
            "sku" : OuterRef("id"),
            "status" : LOC_UNLOCKED,
            "wmsmanifestline__order_line__order__isnull" : True
        }

        if site:
            inventory_qs['site_id'] = site
        if location:
            inventory_qs['location_id'] = location

        summary_qs = _summary.annotate(
            quantity = ExpressionWrapper(
                    Subquery(Inventory.objects.filter(
                        **inventory_qs).values('sku').annotate(
                            tot_qty= Sum('qty')).values('tot_qty')[:1]
                ), output_field=DecimalField()),
            sku_name=F('name'),
            sku_id=F('id'),
            product_group_name = F('product_group__name'),
            pack_config_name = F('pack_config__name'),
        )
        if ordering:
            summary_qs = summary_qs.order_by(ordering)

        summary_qs = summary_qs.values('sku_id', 'sku_name', 'product_group_name',
            'pack_config_name', 'quantity')

        return summary_qs
