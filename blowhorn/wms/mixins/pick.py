from rest_framework import status
from django.utils import timezone

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.submodels.sku import WhSku
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.submodels.location import Location, Client
from blowhorn.wms.submodels.tasks import WhTask, TaskType
from django.db import transaction
from blowhorn.wms.constants import PICK_TASK, LOC_UNLOCKED, WH_TASK_NEW, WH_TASK_ASSIGEND, WH_TASK_INPROGRESS, \
    WH_TASK_COMPLETED, ORDER_LINE_PICKED, ORDER_LINE_NEW, ORDER_LINE_ALLOCATED, SHIPPING_TASK
from config.settings import status_pipelines as StatusPipeline

Hub = get_model('address', 'Hub')
Order = get_model('order', 'Order')
OrderLine = get_model('order', 'OrderLine')
WhSite = get_model('wms', 'WhSite')


class PickMixins(object):

    def _initialize_attributes(self, data, user):
        self.user = user
        self.sku_name = data.get('sku_name')
        self.wh_task_id = data.get('id')
        self.pack_config = data.get('pack_config')
        self.qty = data.get('qty')
        self.site = data.get('site_id')
        self.client = data.get('client_name')
        self.tag = data.get('tag')
        self.inv_status = data.get('inv_status', 'UnLocked')
        # TODO : generate new tag
        self.new_tag = data.get('new_tag', '%s-%s' % (self.tag, self.wh_task_id) ) # for partial movement, user will give tag name
        self.to_loc = data.get('to_loc_name')
        self.error_msg = None
        # TODO : update weight based on sku pack config
        self.weight = 0
        self.shipping_task_type = TaskType.objects.filter(name=SHIPPING_TASK).first()

        self._get_pick_data()

    def _get_pick_data(self):
        if self.client:
            self.client = Client.objects.filter(name=self.client).first()
        if not self.client:
            self.error_msg = 'Invalid Client'
            return

        self.sku = None
        if self.sku_name:
            self.sku = WhSku.objects.filter(name=self.sku_name, client_id=self.client).first()
        if not self.sku:
            self.error_msg = 'Invalid Sku id'
            return

        if self.site:
            self.site = WhSite.objects.filter(pk=self.site).first()
        if not self.site:
            self.error_msg = 'Invalid Site id'
            return
        print('self.site - ', self.site)

        wh_task = None
        if self.wh_task_id:
            wh_task = WhTask.objects.filter(id=self.wh_task_id, task_type__name=PICK_TASK).select_related('order_line',
                                                                                                          'order_line__order').first()
            self.from_loc = wh_task.from_loc
            self.wh_task = wh_task
        if not wh_task:
            self.error_msg = 'Invalid Pick task'
            return

        if self.qty != self.wh_task.qty:
            self.error_msg = 'Invalid Pick qty'

        self.order = self.wh_task.order_line.order if self.wh_task.order_line else self.wh_task.order
        print('self.order - ', self.order)

        if self.wh_task.operator != self.user:
            self.error_msg = 'Pick task is assigned to a different user'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if self.to_loc:
            self.to_loc = Location.objects.filter(name=self.to_loc, site=self.site).first()
            if not self.to_loc:
                self.error_msg = 'Invalid To Location'
                return
        else:
            # todo write the algorithm to find putway location
            # consider cold storage location for applicable items
            self.to_loc = self._get_pick_location()

        if self.wh_task.to_loc.name != self.to_loc.name:
            self.error_msg = 'To loc does not match with putaway task'
            return

        self.inv = Inventory.objects.filter(tag=self.tag, sku=self.sku, client=self.client,
                                            location=self.from_loc).first()
        if not self.inv:
            self.error_msg = 'Invalid tag name or client'
            return
        if self.qty > self.inv.qty:
            self.error_msg = 'Invalid qty, qty available is %s' % (self.inv.qty)
            return

    # TODO
    def _get_pick_location(self):
        loc = Location.objects.filter(status=LOC_UNLOCKED, is_shipping=True).first()
        return loc

    def do_pick(self):
        with transaction.atomic():
            # not sure inventory will have one sku  or for every sku an inventory will be created
            if self.inv.sku == self.sku:
                if self.inv.qty == self.qty:
                    # full movement
                    inv = Inventory.objects.filter(tag=self.tag, client=self.client, location=self.to_loc,
                                                   sku=self.sku).first()
                    if inv:
                        # the destination location already has that inv, so update the qty
                        inv.qty += self.qty
                        inv.save()
                    else:
                        Inventory.objects.filter(tag=self.tag, client=self.client, location=self.from_loc,
                                                 sku=self.sku).update(location=self.to_loc)
                    WhTask.objects.filter(pk=self.wh_task.id).update(status=WH_TASK_COMPLETED)

                else:
                    # partial  movement
                    rem_qty = self.inv.qty - self.qty
                    inv = Inventory.objects.filter(tag=self.tag, client=self.client, location=self.to_loc,
                                                   sku=self.sku).first()
                    if inv:
                        inv.qty += self.qty
                        inv.save()
                    else:
                        Inventory.objects.filter(tag=self.tag, client=self.client, location=self.from_loc, sku=self.sku).update(
                            location=self.to_loc, qty=self.qty)
                    print('self.site - 2 - ', self.site)
                    Inventory.objects.create(tag=self.tag, sku=self.sku, client=self.client, qty=rem_qty,
                                             status=self.inv_status, weight=self.weight, location=self.from_loc,
                                             site=self.site, supplier=self.inv.supplier)
                    WhTask.objects.filter(pk=self.wh_task.id).update(status=WH_TASK_COMPLETED)

                # create a shipping task
                WhTask.objects.create(task_type=self.shipping_task_type, from_loc=self.to_loc,
                                      sku=self.sku, client=self.client, qty=self.qty, tag=self.tag,
                                      status=WH_TASK_NEW, order_line=self.wh_task.order_line,
                                      order=self.wh_task.order, site=self.site)
                # BlindReceivingMixins._write_itl()
                InventoryTransaction.objects.create(tag=self.tag, from_loc=self.from_loc, to_loc=self.to_loc,
                                                    sku=self.sku, client=self.client, qty=self.qty, status='Moved',
                                                    site=self.site, reference=PICK_TASK,
                                                    reference_number=self.wh_task.id, supplier=self.inv.supplier)

                self.wh_task.order_line.status = ORDER_LINE_PICKED
                self.wh_task.order_line.save()

    def update_pick_task(self):
        lines_not_picked = OrderLine.objects.filter(
            order_id=self.order.id,
            status__in=[ORDER_LINE_NEW, ORDER_LINE_ALLOCATED]
        ).exists()

        if lines_not_picked:
            self.order.status = StatusPipeline.WMS_ORDER_PARTIALLY_PICKED
        else:
            self.order.status = StatusPipeline.WMS_ORDER_PICKED
        self.order.save()


class StartPickMixins(object):

    def _initialize_attributes(self, data, user):
        self.user = user
        self.error_msg = None
        self.status = status.HTTP_200_OK
        pick_task_id = data.get('id')
        if not pick_task_id:
            self.error_msg = 'Pick task id is not given'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        try:
            self.pick_task = WhTask.objects.get(pk=pick_task_id, task_type__name=PICK_TASK)
        except:
            self.error_msg = 'Invalid pick task id'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if self.pick_task.operator and self.pick_task.operator != self.user:
            self.error_msg = 'Pick task is assigned to a different user'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if self.pick_task.status not in [WH_TASK_NEW, WH_TASK_ASSIGEND]:
            self.error_msg = 'Pick task is not in status - %s' % self.pick_task.status
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def _start_pick(self):
        self.pick_task.operator = self.user
        self.pick_task.status = WH_TASK_INPROGRESS
        self.pick_task.modified_date = timezone.now()
        self.pick_task.save()

