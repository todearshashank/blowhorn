from blowhorn.wms.submodels.containers import WmsManifestLine
import re
import json
from blowhorn.wms.submodels.inventory import InventoryTransaction
from blowhorn.wms.constants import WMS_QTY_ADD, \
        WMS_RECEIVED, ORDER_LINE_MANIFESTED,  \
        WMS_QTY_SUBTRACT, WMS_STOCK_CHECKED

def inventory_summary_update():

    manifested_inv = InventoryTransaction.objects.filter(status=ORDER_LINE_MANIFESTED)
    for e in manifested_inv:
        e.qty_diff = e.qty
        e.operation = WMS_QTY_SUBTRACT
        e.save()
    
    received_inv = InventoryTransaction.objects.filter(status=WMS_RECEIVED)
    for e in received_inv:
        e.qty_diff = e.qty
        e.operation = WMS_QTY_ADD
        e.save()
    
    stock_inv = InventoryTransaction.objects.filter(status=WMS_STOCK_CHECKED)
    for e in stock_inv:
        x = re.findall('\d+',e.remarks)

        if x and len(x)==2:
            if 'Decreased' in e.remarks:
                e.qty_diff = int(x[0]) - int(x[1])
                e.operation = WMS_QTY_SUBTRACT
            
            elif 'Increased' in e.remarks:
                e.qty_diff = int(x[1]) - int(x[0])
                e.operation = WMS_QTY_ADD
            
            e.save()

def inventory_summary_update_order():
    ml = WmsManifestLine.objects.all()
    for e in ml:
        _data = json.loads(e.inv_details)
        tag = _data.get('tag', None) or None
        if not tag:
            continue
        print(tag)
        InventoryTransaction.objects.filter(tag=tag, status=ORDER_LINE_MANIFESTED).update(order=e.order_line.order)

