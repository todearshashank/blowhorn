import logging

from rest_framework import status
from django.db import transaction
from openpyxl import load_workbook

from blowhorn.oscar.core.loading import get_model

from blowhorn.wms.models import Rack, LocationZone, StorageType, \
    LocationType, Location, Client, WhSite, Customer, LocationZoneHistory, QCConfiguration, PutawayAlgorithm
from blowhorn.address.utils import get_wms_hub_for_customer
from blowhorn.wms.serializers.sku import WhSkuSerializer
from blowhorn.wms.constants import CLIENT_MANDATORY_LABELS
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.common.helper import CommonHelper
from blowhorn.address.models import GenericAddress

Dimension = get_model('order', 'Dimension')
Hub = get_model('address', 'Hub')
City = get_model('address', 'City')
ProductGroup = get_model('wms', 'ProductGroup')
WhSite = get_model('wms', 'WhSite')
PackConfig = get_model('wms', 'PackConfig')
Country = get_model('address', 'Country')

logger = logging.getLogger(__name__)


class LocationImportMixins(object):

    def _initialize_attributes(self, user, data):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.user = user
        self.data = data
        self.customer = None
        self.response_data = None
        self.sku_data_list = []
        try:
            self.customer = self.user.customer
        except:
            self.error_msg = 'Customer not found'
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def _create_loccation_data(self):
        for loc_data in self.data:
            client_name = loc_data.get('client_name')
            loc_name = loc_data.get('loc_name')
            description = loc_data.get('description')
            site_name = loc_data.get('site')
            zone_name = loc_data.get('zone_name')
            product_group = loc_data.get('product_group')
            volume = loc_data.get('volume')
            weight = loc_data.get('weight')
            length = loc_data.get('length')
            breadth = loc_data.get('breadth')
            height = loc_data.get('height')
            sequence = loc_data.get('sequence')
            is_receiving = loc_data.get('is_receiving')
            is_putaway = loc_data.get('is_putaway')
            is_packing = loc_data.get('is_packing')
            is_shipping = loc_data.get('is_shipping')
            status = loc_data.get('status', 'UnLocked')
            allow_sku_mixing = loc_data.get('allow_sku_mixing')

            try:
                client = Client.objects.get(name=client_name)
                site = Hub.objects.get(name=site_name)
                zone = ProductGroup.objects.get(name=zone_name)
            except Exception as e:
                self.error_msg = e.args[0]
                self.status = status.HTTP_400_BAD_REQUEST
                return

            sku_data = {
                'name': loc_name,
                'description': description,
                'site': site,
                'client': client,
                'zone': zone,
                'status': status,
                'customer': self.customer,
                'weight': weight,
                'volume': volume,
                'is_receiving': is_receiving,
                'is_putaway': is_putaway
            }
            self.sku_data_list.append(sku_data)

        sku_serializer = WhSkuSerializer(data=self.sku_data_list, many=True)
        if sku_serializer.is_valid():
            sku_serializer.save()

        self.response_data = sku_serializer.data


class LocationTypeImportMixins(object):

    def initialize_attributes(self, data, customer):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.data = data
        self.customer = customer

        self.location_type_list = []

    def create_location_type_data(self):
        loc_import = self.data.get('location_type')
        if loc_import:
            for i, location_type in enumerate(loc_import):
                location_data = {}
                name = location_type.get('name')
                if not name:
                    self.error_msg = 'Name is mandatory'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return
                description = location_type.get('description')
                loc_prefix_code = location_type.get('loc_prefix_code')

                location_data['name'] = name
                location_data['description'] = description
                location_data['loc_prefix_code'] = loc_prefix_code
                location_type['customer'] = self.customer

                self.location_type_list.append(LocationType(**location_data))

            LocationType.objects.bulk_create(self.location_type_list)


class LocationZoneImportMixins(object):

    def initialize_attributes(self, data, customer):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.customer = customer
        self.data = data
        self.location_zone_list = []

    def create_location_zone_data(self):
        loc_zone = self.data.get('location_zn')
        if loc_zone:
            for i, location_zone in enumerate(loc_zone):
                location_zone_data = {}
                name = location_zone.get('name')
                if not name:
                    self.error_msg = 'Name is mandatory'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

                sequence = location_zone.get('sequence')
                for_putaway = location_zone.get('for_putaway')

                zone_status = location_zone.get('status')
                if not zone_status:
                    self.error_msg = 'status not found'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return
                site_name = location_zone.get('site')
                if not site_name:
                    self.error_msg = 'Site not found'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return
                site = WhSite.objects.filter(name=site_name).first()
                if not site:
                    self.error_msg = 'site not valid'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

                loc_type_name = location_zone.get('loc_type')
                loc_type = None
                if loc_type_name:
                    loc_type = LocationType.objects.filter(name=loc_type_name).first()

                    if not loc_type:
                        self.error_msg = 'location invalid'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                storage_type_name = location_zone.get('storage_type')
                storage_type = None
                if storage_type_name:
                    storage_type = StorageType.objects.filter(name=storage_type_name).first()

                    if not storage_type:
                        self.error_msg = 'Storage type name not found'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                dimension = location_zone.get('dimensions')
                dim = None
                if dimension:
                    length = dimension.get('length')
                    breadth = dimension.get('breadth')
                    height = dimension.get('height')
                    uom = dimension.get('uom')
                    dimension_dict = {
                        'length': length,
                        'breadth': breadth,
                        'height': height,
                        'uom': uom

                    }
                    dim = Dimension.objects.filter(**dimension_dict).first()
                    if not dim:
                        dim = Dimension.objects.create(**dimension_dict)

                product_group_name = location_zone.get('product_group')
                product_group = None
                if product_group_name:
                    product_group = ProductGroup.objects.filter(name=product_group_name).first()
                    if not product_group:
                        self.error_msg = 'Product group invalid'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                is_receiving = location_zone.get('is_receiving')

                is_putaway = location_zone.get('is_putaway')

                is_packing = location_zone.get('is_packing')

                is_shipping = location_zone.get('is_shipping')

                location_zone_data['name'] = name
                location_zone_data['site'] = site
                location_zone_data['sequence'] = sequence
                location_zone_data['for_putaway'] = for_putaway
                location_zone_data['status'] = zone_status
                location_zone_data['loc_type'] = loc_type
                location_zone_data['storage_type'] = storage_type
                location_zone_data['product_group'] = product_group
                location_zone_data['is_receiving'] = is_receiving
                location_zone_data['is_putaway'] = is_putaway
                location_zone_data['is_packing'] = is_packing
                location_zone_data['is_shipping'] = is_shipping
                location_zone_data['dimensions'] = dim

                self.location_zone_list.append(LocationZone(**location_zone_data))

            LocationZone.objects.bulk_create(self.location_zone_list)


class LocationZoneMixin(object):

    def __create_zone_data_log(self, data, performed_by=None):
        try:
            logger.info('Saving location zone history log..')
            instance = LocationZoneHistory.objects.create(**data)
            if performed_by is not None:
                instance.created_by = performed_by
                instance.save()

        except BaseException as be:
            logger.info('%s' % be)
            logger.error('ALERT!! Location Zone history creation failed.')

    def soft_delete_zone_data(self, zone, performed_by):
        """
        Mark zone related data as deleted
        """

        log_dump = []
        num_of_records_affected = 0
        zone_pk = zone.pk

        log_data = {
            'zone': zone.name,
            'category': LocationZoneHistory.CATEGORY_DATA_DELETE_REQUEST
        }
        query_model_mapping = {
            'Location': {
                'zone_id': zone_pk,
                'is_deleted': False,
            },
            'PutawayAlgorithm': {
                'zone_id': zone_pk,
                'is_deleted': False,
            },
            'QCConfiguration': {
                'zone_id': zone_pk,
                'is_deleted': False,
            },
            'Rack': {
                'zone_id': zone_pk,
                'is_deleted': False,
            },
            'LocationZone': {
                'id': zone_pk,
                'is_deleted': False,
            },
        }

        for k in query_model_mapping:
            logger.info('Updating flag for %s' % k)
            updated = eval(k).objects.filter(**query_model_mapping[k]).update(is_deleted=True)
            if updated:
                num_of_records_affected += updated
                log_dump.append('# of %s: %s' % (k, updated))

        log_data['log_dump'] = log_dump
        log_data['number_of_zones'] = num_of_records_affected
        self.__create_zone_data_log(log_data, performed_by=performed_by)


class ClientImportMixins(object):

    def initialize_attributes(self, data, customer):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.data = data
        self.customer = customer

        self.client_type_list = []

    def _create_client_data(self):

        client_data = self.data.get('client_data')
        if client_data:
            for i, client in enumerate(client_data):
                client_type_data = {}
                name = client.get('name')
                if not name:
                    self.error_msg = 'Name is mandatory'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

                description = client.get('description')

                client_type_data['name'] = name
                client_type_data['description'] = description
                client_type_data['customer'] = self.customer

                self.client_type_list.append(Client(**client_type_data))

            Client.objects.bulk_create(self.client_type_list)


class StorageTypeImportMixins(object):

    def initialize_attributes(self, data, customer):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.customer = customer
        self.data = data
        self.storage_type_list = []

    def create_storage_data(self):

        storage_type = self.data.get('storage_type')
        if storage_type:
            for i, storage in enumerate(storage_type):
                storage_type_data = {}
                name = storage.get('name')
                if not name:
                    self.error_msg = 'Name is mandatory'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return
                description = storage.get('description')

                storage_type_data['name'] = name
                storage_type_data['description'] = description
                storage_type_data['customer'] = self.customer

                self.storage_type_list.append(StorageType(**storage_type_data))

            StorageType.objects.bulk_create(self.storage_type_list)
class ClientImportMixin(object):

    def _process_import_data(self, data, request):
        """
            This method intializes the variables and perform basic prechecks
            on input file like file name, customer id and empty file check.
            Also does the row processing of the excel sheet where each row
            is equivalent to one order.
        """
        self.file_name = data.get('file')
        self.response = {'data': '', 'errors': ''}
        self.success = False
        self.status_code = status.HTTP_200_OK
        self.row_index = 0
        self.user = request.user
        self.customer = CustomerMixin().get_customer(self.user)

        self.values_list = []
        self.client_data_list = []

        # Initialize error lists for further processing
        self._initialize_error_list()

        # Load the workbook and initial validation
        error_message = self._initial_validations()
        if error_message:
            return self.success, error_message

    def _initial_validations(self):
        try:
            wb_obj = load_workbook(filename=self.file_name, data_only=True)
        except Exception as e:
            error_message = 'The file may be corrupted or not in xlsx format.' \
                            ' Please download a new sample sheet.'
            return error_message

        sheet_obj = wb_obj.active
        headers = [cell.value for cell in sheet_obj[1]]
        self.no_of_rows = sheet_obj.max_row

        for i in CLIENT_MANDATORY_LABELS + ['Sl No.']:
            if i not in headers:
                error_message = 'Incorrect file template uploaded. Please download the template again and upload'
                return error_message
        row_error = self._row_processing(sheet_obj, headers)
        if not self.values_list:
            error_message = 'File is empty'
            return error_message
        if len(row_error) != 0:
            error_message = 'Incorrect file format'
            return error_message

        created_inventory, message = self._create_clients_from_data(self.values_list)
        self.success = True if created_inventory else False
        message = 'Uploaded Successfully' if created_inventory else message
        return message

    def _initialize_error_list(self):
        self.records_without_mandatory_fields = []
        self.records_with_duplicate_name = []
        self.records_with_existing_name = []
        self.name_set = set()

    def _row_processing(self, sheet_obj, headers):
        index = 0
        row_error = []
        for row in sheet_obj.iter_rows(min_row=2):
            inventory_data_dict = {}
            index += 1
            for key, cell in zip(headers, row):
                if cell.value:
                    value = cell.value
                    inventory_data_dict[key] = value
            if inventory_data_dict:
                self.values_list.append(inventory_data_dict)
        return row_error

    def _create_clients_from_data(self, client_data):
        print('-----------data----------', client_data)
        for data in client_data:
            self.row_index += 1

            if not set(list(data.keys())).intersection(set(CLIENT_MANDATORY_LABELS)) == set(
                    CLIENT_MANDATORY_LABELS):
                self.records_without_mandatory_fields.append(self.row_index)
                continue
                # return False, 'Mandatory fields are missing %s ' % CLIENT_MANDATORY_LABELS

            name = data['Client Name']
            if Client.objects.filter(name=name, customer=self.customer).exists():
                self.records_with_existing_name.append(self.row_index)
            elif name in self.name_set:
                self.records_with_duplicate_name.append(self.row_index)
            else:
                self.name_set.add(name)
            client_dict = {
                'name': name,
                'description': data.get('Description', None),
                'customer': self.customer
            }

            self.client_data_list.append((Client(**client_dict)))

        error = self._raise_error()
        if error:
            return False, error
        created = Client.objects.bulk_create(self.client_data_list)

        return True, len(created)

    def _raise_error(self):
        error_message = ''
        if self.records_without_mandatory_fields:
            error_message += ('Mandatory fields %s are missing for rows %s. ' % (CLIENT_MANDATORY_LABELS,
                              self.records_without_mandatory_fields)
                              )
        if self.records_with_existing_name:
            error_message += ('Client name given in rows %s already exists. ' % self.records_with_existing_name)
        if self.records_with_duplicate_name:
            error_message += ('Duplicate Client name present in rows %s. ' % self.records_with_duplicate_name)
        return error_message


class SiteMixin(object):

    def _initialize_attributes(self, data, user):
        self.site_data = {}
        name = data.get('name', None)
        city = data.get('city', None)
        hub = data.get('hub', None)
        address = data.get('address', None)
        shipping_address = data.get('shipping_address', None)
        pincodes = data.get('pincodes', None)
        self.error_msg = ''
        self.site_address = ''
        self.shipping_address = ''

        if not name:
            self.error_msg = 'Site name is required'
            self.status_code = status.HTTP_400_BAD_REQUEST
        if not hub:
            self.error_msg = 'Hub is required'
            self.status_code = status.HTTP_400_BAD_REQUEST

        customer = CustomerMixin().get_customer(user)
        if WhSite.objects.filter(name=name).exists():
            self.status_code = status.HTTP_400_BAD_REQUEST
            self.error_msg = 'Site with the given name already exists'

        city = City.objects.filter(name=city).first()
        hub = Hub.objects.filter(name=hub).first()

        if not hub:
            self.error_msg = 'Incorrect Hub provided'
            self.status_code = status.HTTP_400_BAD_REQUEST

        if not city:
            city = hub.city

        if pincodes and not isinstance(pincodes, list):
            try:
                pincodes_list = pincodes.split(',')
                pincodes = pincodes_list
            except BaseException as e:
                logging.info('Error when parsing pincodes %s' % e)
                self.error_msg = 'Pass the Pincodes as a comma separated string'
                self.status_code = status.HTTP_400_BAD_REQUEST

        self.site_data['name'] = name
        self.site_data['hub'] = hub
        self.site_data['store_code'] = data.get('store_code', None)
        self.site_data['client_billing_code'] = data.get('client_billing_code', None)
        self.site_data['shipping_code'] = data.get('shipping_code', None)
        self.site_data['length'] = data.get('length', None)
        self.site_data['breadth'] = data.get('breadth', None)
        self.site_data['height'] = data.get('height', None)
        self.site_data['uom'] = data.get('uom', None)
        self.site_data['city'] = city
        self.site_data['pincodes'] = pincodes
        self.site_data['customer'] = customer
        self.site_data['is_wms_site'] = True

        if address:
            line1 = address.get('line1', '')
            line2 = address.get('line2', '')
            line3 = address.get('line3', '')
            line4 = address.get('line4', '')
            state = address.get('state', '')
            postcode = address.get('postcode', '')
            country = Country.objects.first()
            location = address.get('location', None)
            lat = location.get('lat', '')
            lon = location.get('lon', '')

            if not (line1 or line2 or line3):
                self.error_msg = 'Address is mandatory'
                self.status_code = status.HTTP_400_BAD_REQUEST

            if not location:
                self.error_msg = 'Location is mandatory'
                self.status_code = status.HTTP_400_BAD_REQUEST

            if not postcode:
                self.status_code = status.HTTP_400_BAD_REQUEST
                self.error_msg = 'Postcode is mandatory'

            geopoint = CommonHelper.get_geopoint_from_latlong(lat, lon)

            self.site_address = {
                'geopoint': geopoint, 'line1': line1, 'line2':  line2, 'line3': line3, 'line4': line4,
                'state': state, 'postcode': postcode, 'country': country
            }

        if shipping_address:
            line1 = address.get('line1', '')
            line2 = address.get('line2', '')
            line3 = address.get('line3', '')
            line4 = address.get('line4', '')
            state = address.get('state', '')
            postcode = address.get('postcode', '')
            country = Country.objects.first()
            location = address.get('location', None)
            lat = location.get('lat', '')
            lon = location.get('lon', '')

            if not (line1 or line2 or line3):
                self.error_msg = 'Address is mandatory in Shipping Address'
                self.status_code = status.HTTP_400_BAD_REQUEST

            if not location:
                self.error_msg = 'Location is mandatory in Shipping Address'
                self.status_code = status.HTTP_400_BAD_REQUEST

            if not postcode:
                self.status_code = status.HTTP_400_BAD_REQUEST
                self.error_msg = 'Postcode is mandatory in Shipping Address'

            geopoint = CommonHelper.get_geopoint_from_latlong(lat, lon)

            self.shipping_address = {
                'geopoint': geopoint, 'line1': line1, 'line2': line2, 'line3': line3, 'line4': line4,
                'state': state, 'postcode': postcode, 'country': country
            }
        return

    def _save_site_data(self):
        with transaction.atomic():
            if self.site_address:
                site_address = GenericAddress.objects.create(**self.site_address)
                self.site_data['address'] = site_address
            if self.shipping_address:
                shipping_address = GenericAddress.objects.create(**self.shipping_address)
                self.site_data['shipping_address'] = shipping_address
            site = WhSite.objects.create(**self.site_data)

