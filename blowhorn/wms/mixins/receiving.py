import json
import pytz
import logging

import phonenumbers
from collections import Counter
from datetime import datetime
from django.conf import settings
from django.db.models import Q
from django.core.files.base import ContentFile
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db import transaction

from rest_framework import status
from phonenumber_field.phonenumber import PhoneNumber
from blowhorn.oscar.core.loading import get_model

from blowhorn.address.utils import get_full_address_str
from blowhorn.address.models import Country
from blowhorn.common.helper import CommonHelper
from blowhorn.common.utils import generate_pdf
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.shopify.tasks import adjust_inventory_on_shopify
from blowhorn.utils.datetime_function import DateTime
from blowhorn.wms.submodels.receiving import (GRNLineItems, ASNLineItems, WhSite,
    PurchaseOrder, POLineItems, ASNOrder)
from blowhorn.customer.models import CustomerTaxInformation
from blowhorn.wms.submodels.sku import WhSku, TrackingLevel
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.submodels.location import Location, Client, WhSite
from blowhorn.wms.submodels.tasks import WhTask, TaskType
from blowhorn.wms.submodels.inbound import Supplier
from blowhorn.wms.constants import ASN_EXPIRED, PUTAWAY_TASK, ASN_IN_PROGRESS, ASN_COMPLETED, PROCESS_RECEIVING, \
    WH_TASK_HOLD, WH_TASK_NEW, WMS_QTY_ADD, WMS_RECEIVED, WMS_SOURCE_IMPORT, TAG_PREFIX_CODE
from blowhorn.wms.services.putaway import FindPutawayLocation
from blowhorn.wms.utils.putaway import update_whtask_location_weight_volume
from blowhorn.wms.utils.qc import create_qc_task

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Hub = get_model('address', 'Hub')
Supplier = get_model('wms', 'Supplier')
PartnerCustomer = get_model('customer', 'PartnerCustomer')
VendorAddress = get_model('vehicle', 'VendorAddress')
ShopifyLog = get_model('shopify', 'ShopifyLog')


class BlindReceivingMixins(object):

    def _initialize_attributes(self, data, request):
        self.inv = None
        self.wh_task = None
        self.wh_task_type = None
        self.error_msg = None
        self.client = None
        self.site = None
        self.use_default_location = data[0].get('default_loc', False)
        self.source = request.headers.get('source', None)
        self.site_name = None
        self.supplier = None
        self.user = request.user
        self.customer = CustomerMixin().get_customer(request.user) if request else None
        skus = []
        from_loc = []
        to_loc = []
        tags = []
        self.grn_line_items = []
        self.total_price = 0

        if self.is_grn:
            self.site = self.purchase_order.site
            self.client = self.purchase_order.client
            self.supplier = self.purchase_order.supplier
        elif self.is_asn:
            self.site = self.asn_order.site
            self.client = self.asn_order.client
            self.supplier = self.asn_order.supplier

        for i in data:
            skus.append(i.get('sku'))
            if i.get('from_loc'):
                from_loc.append(i.get('from_loc'))
            if i.get('to_loc'):
                to_loc.append(i.get('to_loc'))
            if not i.get('tag'):
                i['tag'] = CommonHelper().get_unique_friendly_id(TAG_PREFIX_CODE)
            if i.get('tag'):
                tags.append(i.get('tag'))
            if not self.client:
                self.client = i.get('client')
            if not self.site:
                self.site_name = i.get('site')
            if not self.supplier:
                self.supplier = i.get('supplier')

        self.sku_rec = dict(WhSku.objects.filter(Q(name__in=skus) | Q(ean__in=skus) | Q(upc__in=skus)
                                                 ).values_list('name', 'id'))
        self.sku_rec_ean = dict(WhSku.objects.filter(Q(name__in=skus) | Q(ean__in=skus) | Q(upc__in=skus)
                                                 ).values_list('ean', 'id'))
        self.sku_rec_upc = dict(WhSku.objects.filter(Q(name__in=skus) | Q(ean__in=skus) | Q(upc__in=skus)
                                                     ).values_list('upc', 'id'))

        self.from_locs = dict(Location.objects.filter(name__in=from_loc).values_list('name', 'id'))
        self.from_locs_putway = dict(Location.objects.filter(name__in=from_loc).values_list('name', 'is_putaway'))
        self.to_locs = dict(Location.objects.filter(name__in=to_loc).values_list('name', 'id'))
        self.inv = dict(Inventory.objects.filter(tag__in=tags).values_list('tag', 'id'))

        # diff = list(skus - self.sku_rec.keys())
        # if diff:
        #     self.error_msg = {
        #         "message": 'Invalid sku names %s' % diff
        #     }
        #     return

        diff = from_loc - self.from_locs.keys()
        if diff:
            self.error_msg = {
                "message": 'Invalid from-locations %s' % diff
            }
            return

        diff = to_loc - self.to_locs.keys()
        if diff:
            self.error_msg = {
                "message": 'Invalid to-locations %s' % diff
            }
            return

        duplicate_tags = [k for k, v in Counter(tags).items() if v > 1]

        if duplicate_tags:
            self.error_msg = {
                "message": "Duplicate Inventory tags given - %s" % duplicate_tags
            }
            return

        if self.inv:
            self.error_msg = {
                "message": 'Inventory tags exist %s' % (list(self.inv.keys()))
            }
            return
        if not self.use_default_location:
            try:
                self.wh_task_type = TaskType.objects.get(name=PUTAWAY_TASK)
            except:
                self.error_msg = {
                    "message": 'PUTAWAY task type not found'
                }
                return

        if not self.is_grn and not self.is_asn:
            if self.client:
                try:
                    self.client = Client.objects.get(name=self.client, customer=self.customer)
                except BaseException as e:
                    logger.info('Error occurred while fetching Client - %s' % e)
                    self.error_msg = {
                        "message": 'Invalid Client'
                    }
                    return

            try:
                self.site = WhSite.objects.get(name=self.site_name)
            except BaseException as e:
                logger.info('Error occurred while fetching Site - %s' % e)
                self.error_msg = {
                    "message": 'Invalid Site'
                }
                return

            self.supplier = Supplier.objects.filter(name=self.supplier, customer=self.customer).first()

        if not self.site:
            self.error_msg = {
                "message": 'Invalid Site'
            }
            return

        self.hub = self.site.hub

    def _create_inventory(self, record):
        from blowhorn.wms.views.receiving import Receiving

        tag = record.get('tag')
        weight = record.get('weight')
        qty = record.get('qty')

        datetime_helper = DateTime()
        expiry_date = datetime_helper.str_to_datetime(
            record.get('expiry_date', None), datetime_helper.ISO_DATETIME_FORMAT)
        mfg_date = datetime_helper.str_to_datetime(
            record.get('mfg_date', None), datetime_helper.ISO_DATETIME_FORMAT)

        self.tracking_level = record.get('tracking_level', None)
        inv_status = record.get('inv_status', 'UnLocked')

        sku_id = self.sku_rec.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_ean.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_upc.get(record['sku'], None)

        self.default_loc = Receiving().get_or_create_location(self.site.id, client=self.client)
        from_loc = self.from_locs[record['from_loc']] if not self.use_default_location else self.default_loc.id

        sku = WhSku.objects.get(id=sku_id)
        if not weight:
            weight = float(qty) * float(sku.each_weight) if sku.each_weight else None
        self.pack_config_id = sku.pack_config.id if sku.pack_config else None
        tracking_level_obj = sku.pack_config.tracking_level

        if TrackingLevel.objects.filter(level_one_name=self.tracking_level, customer=self.customer).exists():
            weight = float(sku.each_weight) * float(qty) if sku and sku.each_weight else None
        elif TrackingLevel.objects.filter(level_two_name=self.tracking_level, customer=self.customer).exists():
            weight = float(sku.each_weight) * float(qty) * float(
                tracking_level_obj.ratio_level_two_one) if sku and sku.each_weight else None
        elif TrackingLevel.objects.filter(level_three_name=self.tracking_level, customer=self.customer).exists():
            weight = float(sku.each_weight) * float(qty) * float(
                tracking_level_obj.ratio_level_two_one) * float(
                tracking_level_obj.ratio_level_three_two) if sku and sku.each_weight else None

        is_shopify_synced = False
        try:
            pcm = PartnerCustomer.objects.filter(
                customer_id=sku.client.customer.id,
                is_active=True, is_wms_flow=True).first()
            if pcm:
                if sku.shopify_inventory_id:
                    is_shopify_synced = True
                    adjust_inventory_on_shopify.delay(sku.shopify_inventory_id,
                                                      sku.client.customer.id, qty)
                else:
                    ShopifyLog.objects.create(
                        reference_number=sku_id,
                        request_body=json.dumps(record),
                        reason="Sku ID %s is missing" % (sku_id),
                        customer=sku.client.customer,
                        category='Variant Missing',
                        source='Inventory'
                    )

            self.inv = Inventory.objects.create(tag=tag, sku_id=sku_id,
                                                qty=qty, status=inv_status,
                                                weight=weight, location_id=from_loc,
                                                expiry_date=expiry_date,
                                                mfg_date=mfg_date,
                                                pack_config_id=self.pack_config_id,
                                                tracking_level=self.tracking_level,
                                                site_id=self.site.id,
                                                supplier=self.supplier,
                                                client=self.client,
                                                is_shopify_synced=is_shopify_synced)

            _grn_item = {
                "name": sku.name,
                "size": (sku.pack_config.tracking_level.level_two_name or
                         sku.pack_config.tracking_level.level_one_name).title(),
                "quantity": 0,
                "price": float(sku.mrp or 0) * float(qty),
                "delivered_quantity": qty
            }
            self.total_price += _grn_item['price']
            self.grn_line_items.append(_grn_item)

        except Exception as e:
            self.error_msg = 'Inventory creation failed - %s' % e.args[0]
            return False
        return True

    def check_putaway_required(self, record):
        if self.from_locs_putway[record['from_loc']]:
            return False
        return True

    def _create_whtask(self, record, status=None):
        from_loc = self.from_locs[record['from_loc']]
        to_loc = self.to_locs.get(record.get('to_loc'))
        sku_id = self.sku_rec.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_ean.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_upc.get(record['sku'], None)
        qty = record.get('qty')
        tag = record.get('tag')
        line_id = record.get('line_id')
        try:
            putaway_task = WhTask.objects.create(
                task_type=self.wh_task_type,
                from_loc_id=from_loc,
                to_loc_id=to_loc,
                sku_id=sku_id,
                site=self.site,
                client=self.client,
                qty=qty,
                tag=tag,
                status=status if status else WH_TASK_NEW,
                supplier=self.supplier,
                asn_line_id=line_id if self.is_asn else None,
                po_line_id=line_id if self.is_grn else None
            )
        except:
            self.error_msg = 'WH Task Creation Failed'
            return False

        if not to_loc:
            to_loc = FindPutawayLocation(putaway_task).get_putaway_location()

        if to_loc:
            putaway_task.to_loc = to_loc
            putaway_task.save()

            # update alloacted location weight and volume based on putaway task
            update_whtask_location_weight_volume(putaway_task, False)

        self.wh_task = putaway_task

        return True

    def _write_itl(self, record):
        tag = record.get('tag')
        qty = record.get('qty')
        from_loc = self.from_locs[record['from_loc']] if not self.use_default_location else self.default_loc.id
        to_loc = self.to_locs.get(record.get('to_loc')) if not self.use_default_location else self.default_loc.id
        sku_id = self.sku_rec.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_ean.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_upc.get(record['sku'], None)
        if not to_loc:
            to_loc = self.wh_task.to_loc_id if self.wh_task else None
        try:
            inv_trans = InventoryTransaction.objects.create(
                tag=tag,
                from_loc_id=from_loc,
                to_loc_id=to_loc,
                sku_id=sku_id,
                client=self.client,
                qty=qty,
                site=self.site,
                supplier=self.supplier,
                status=WMS_RECEIVED,
                source=self.source or WMS_SOURCE_IMPORT,
                pack_config_id=self.pack_config_id,
                tracking_level=self.tracking_level,
                operation=WMS_QTY_ADD,
                qty_diff=qty,
                qty_left=qty
            )
            inv_trans.created_by = self.user
            inv_trans.modified_by = self.user
            inv_trans.save()

        except:
            self.error_msg = 'ITL Creation Failed'
            return False
        return True

    def generate_grn_document(self, user):
        _po = PurchaseOrder.objects.filter(id=self.purchase_order.id).first()
        file_url = None
        if self.grn_line_items:
            data = {}
            _supplier_name = self.supplier.name if self.supplier else None
            if _supplier_name == 'Default Supplier':
                _supplier_name = self.customer.name

            grn_number = 'RN%s' % datetime.now().strftime("%d%m%y%H%M%S%f")
            tz = pytz.timezone(settings.ACT_TIME_ZONE)
            data['grn_number'] = grn_number
            data['date'] = datetime.now().astimezone(tz).strftime('%d %b %Y %I:%M %p')
            data['items'] = self.grn_line_items
            data['supplier_name'] = _supplier_name
            data['receiver_name'] = user.name
            data['gstin'] = CustomerTaxInformation.objects.filter(customer=self.customer).values_list('gstin', flat=True).first()
            data['address'] = '%s %s' % (self.hub.name, get_full_address_str(self.hub.address))
            data['total_price'] = intcomma(round(self.total_price, 2))
            _po.receiving_number = grn_number
            _po.total_price = self.total_price

            bytes = generate_pdf('pdf/grn/grn.html', data)
            if bytes:
                filename = '%s.pdf' % grn_number
                _po.file.save(filename, ContentFile(bytes), save=True)
                file_url = _po.file.url

            _po.status = ASN_COMPLETED

        else:
            _po.status = ASN_EXPIRED

        _po.site = self.site
        _po.supplier = self.supplier
        _po.save()
        return file_url

    def _update_grn_line_items(self, record):
        sku_id = self.sku_rec.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_ean.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_upc.get(record['sku'], None)
        quantity = record.get('qty')
        parm = {
            "sku_id": sku_id,
            "qty": quantity,
            "purchase_order": self.purchase_order
        }
        grn_items = GRNLineItems.objects.filter(**parm)
        if not grn_items:
            GRNLineItems.objects.create(**parm)
        else:
            grn_item = grn_items.first()
            grn_item.qty = (grn_item.qty if grn_items.qty else 0) + int(quantity)
            grn_item.save()

        return True

    def _update_asn_line_items(self, record):
        line_id = record.get('line_id')
        sku_id = self.sku_rec.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_ean.get(record['sku'], None)
        if not sku_id:
            sku_id = self.sku_rec_upc.get(record['sku'], None)
        qty = record.get('qty')
        asn_item = ASNLineItems.objects.filter(
            # id=line_id,  TODO uncomment it
            sku_id=sku_id,
            asn_order=self.asn_order
        )
        if not asn_item:
            self.error_msg = 'Invalid ASN line item'
            return False

        if asn_item[0].qty >= qty:
            line_status = ASN_COMPLETED
        else:
            line_status = ASN_IN_PROGRESS
        asn_item.update(qty_received=qty, status=line_status)
        return True

    def _receive_inventory(self, record):
        if self.error_msg:
            return
        with transaction.atomic():
            # try:
            if not self._create_inventory(record):
                return
            if self.is_asn:
                self._update_asn_line_items(record)
            receiving_data = {
                'po_line_id': record.get('line_id') if self.is_grn else None,
                'asn_line_id': record.get('line_id') if self.is_asn else None,
                'supplier': self.supplier
            }
            if not self.use_default_location:
                qc_task = create_qc_task(self.inv, PROCESS_RECEIVING, receiving_data)
                putaway_status = None
                if qc_task:
                    putaway_status = WH_TASK_HOLD
                print('putaway_status - ', putaway_status)
                if self.check_putaway_required(record):
                    if not self._create_whtask(record, putaway_status):
                        pass
            if not self._write_itl(record):
                return
            self._update_grn_line_items(record)
            # except:
            #     self.error_msg = 'Receiving failed'


class ImportSupplierMixins(object):

    def _initialize_attributes(self, data, customer):
        self.customer = customer
        self.error_msg = None
        self.status = status.HTTP_200_OK
        suppliers = data.get('supplier')
        if not isinstance(suppliers, list):
            self.error_msg = {
                'status': 'FAILURE',
                'message': 'Suppliers data should be sent in a list'
            }
            self.status = status.HTTP_400_BAD_REQUEST
            return
        self.supplier_data_list = []
        self.vendor_addr_list = []
        self.invalid_records = []
        if suppliers:
            for i, supplier in enumerate(suppliers):
                supplier_data = {}

                name = supplier.get('name')
                if not name:
                    msg = 'Name missing for record - %d' % i
                    self.invalid_records.append(msg)

                phone_number = supplier.get('phone_number')
                try:
                    pickup_phone = PhoneNumber.from_string(
                        str(phone_number), settings.COUNTRY_CODE_A2)
                    if not pickup_phone.is_valid():
                        msg = 'Invalid phone number - %s' % phone_number
                        self.invalid_records.append(msg)
                except phonenumbers.NumberParseException:
                    msg = 'Invalid phone number - %s' % phone_number
                    self.invalid_records.append(msg)

                cin = supplier.get('cin')
                pan = supplier.get('pan')
                tan = supplier.get('tan')
                address = supplier.get('address')
                line1 = address.get('line1')
                postcode = address.get('postcode')
                location = address.get('location')
                geopoint = CommonHelper.get_geopoint_from_location_json(location)
                line4 = address.get('city')
                state = address.get('state')
                country = Country.objects.filter(iso_3166_1_a2__iexact=settings.COUNTRY_CODE_A2).first()

                address_dict = {
                    'line1': line1,
                    'postcode': postcode,
                    'geopoint': geopoint,
                    'line4': line4,
                    'state': state,
                    'country': country

                }
                self.vendor_addr_list.append(VendorAddress(**address_dict))

                supplier_data['name'] = name
                supplier_data['phone_number'] = phone_number
                supplier_data['cin'] = cin
                supplier_data['pan'] = pan
                supplier_data['tan'] = tan
                self.supplier_data_list.append(supplier_data)
            if self.invalid_records:
                self.error_msg = str(self.invalid_records)
                self.status = status.HTTP_400_BAD_REQUEST
                return

    def _save_data(self):
        with transaction.atomic():
            supplier_creation_list = []

            vendors_addr = VendorAddress.objects.bulk_create(self.vendor_addr_list)
            for i, supplier_data in enumerate(self.supplier_data_list):
                supplier_data['current_address'] = vendors_addr[i]
                supplier_creation_list.append(Supplier(**supplier_data))

            Supplier.objects.bulk_create(supplier_creation_list)


class PurchaseOrderMixins(object):
    def initialize_attributes(self, data, customer):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.data = data
        self.customer = customer
        self.purchase_order_list = []
        self.po_lines_list = []
        self.invalid_records = []

    def create_po_data(self):
        purchase_order = self.data.get('purchase_order')
        if purchase_order:
            for i, po in enumerate(purchase_order):
                po_number = po.get('po_number')
                try:
                    pickup_phone = PhoneNumber.from_string(
                        str(po_number), settings.COUNTRY_CODE_A2)
                    if not pickup_phone.is_valid():
                        msg = 'Invalid phone number - %s' % po_number
                        self.invalid_records.append(msg)
                except phonenumbers.NumberParseException:
                    msg = 'Invalid phone number - %s' % po_number
                    self.invalid_records.append(msg)

                invoice_number = po.get('invoice_number')
                description = po.get('description')
                delivery_date = po.get('delivery_date')
                expiry_date = po.get('expiry_date')
                site_name = po.get('site')
                site = None
                if site_name:
                    site = WhSite.objects.filter(name=site_name).first()

                    if not site:
                        self.error_msg = 'Site not found'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                supplier_name = po.get('supplier')
                supplier = None
                if supplier_name:
                    supplier = Supplier.objects.filter(name=supplier_name).first()

                    if not supplier:
                        self.error_msg = 'Supplier not found'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                total_price = po.get('total_price')
                batch = po.get('batch')
                serial_number = po.get('serial_number')
                status_ = po.get('status')
                if not status_:
                    self.error_msg = 'Status not found'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return
                po_order_create = PurchaseOrder.objects.create(
                    po_number=po_number,
                    invoice_number=invoice_number,
                    description=description,
                    delivery_date=delivery_date,
                    expiry_date=expiry_date,
                    site=site,
                    supplier=supplier,
                    total_price=total_price,
                    batch=batch,
                    serial_number=serial_number,
                    status=status_

                )
                line_item = po.get('line_items')
                if line_item:
                    for i, line_item in enumerate(line_item):
                        sku_name = line_item.get('sku')
                        if not sku_name:
                            self.error_msg = 'Sku is mandatory'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return
                        sku = WhSku.objects.filter(name=sku_name).first()
                        if not sku:
                            self.error_msg = 'Sku invalid'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                        qty = line_item.get('qty')
                        if not qty:
                            self.error_msg = 'Quantity not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return
                        status_ = line_item.get('status')
                        if not status_:
                            self.error_msg = 'Status not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return
                        qty_received = line_item.get('qty_received')
                        if not qty_received:
                            self.error_msg = 'Quantity received not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return
                        price_per_qty_ = line_item.get('price_per_qty')
                        item_desc = line_item.get('item_desc')
                        purchase_order = line_item.get('purchase_order')
                        if not purchase_order:
                            self.error_msg = 'purchase order not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                        lines_item_dict = {
                            'sku': sku,
                            'qty': qty,
                            'status': status_,
                            'qty_received': qty_received,
                            'price_per_qty': price_per_qty_,
                            'item_desc': item_desc,
                            'purchase_order': po_order_create
                        }
                        self.po_lines_list.append(POLineItems(**lines_item_dict))
            if self.invalid_records:
                self.error_msg = str(self.invalid_records)
                self.status = status.HTTP_400_BAD_REQUEST
                return

            POLineItems.objects.bulk_create(self.po_lines_list)


class ASNOrderMixins(object):
    def initialize_attributes(self, data, customer):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.data = data
        self.customer = customer
        self.asn_lines_list = []
        self.asn_order_list = []

    def create_asn_data(self):
        asn_ = self.data.get('asn_order')
        if asn_:
            for i, asn in enumerate(asn_):
                invoice_number = asn.get('invoice_number')
                if not invoice_number:
                    self.error_msg = 'Invoice number not found'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return
                description = asn.get('description')
                delivery_date = asn.get('delivery_date')
                expiry_date = asn.get('expiry_date')
                site_name = asn.get('site')
                site = None
                if site_name:
                    site = WhSite.objects.filter(name=site_name).first()

                    if not site:
                        self.error_msg = 'Site not found'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                supplier_name = asn.get('supplier')
                supplier = None
                if supplier_name:
                    supplier = Supplier.objects.filter(name=supplier_name).first()

                    if not supplier:
                        self.error_msg = 'Supplier name Invalid'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                total_price = asn.get('total_price')
                batch = asn.get('batch')
                serial_number = asn.get('serial_number')
                status_ = asn.get('status')
                client_name = asn.get('client')
                client = None
                if client_name:
                    client = Client.objects.filter(name=client_name, customer=self.customer).first()

                    if not client_name:
                        self.error_msg = 'Client not found'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                asn_order_create = ASNOrder.objects.create(
                    invoice_number=invoice_number,
                    description=description,
                    delivery_date=delivery_date,
                    expiry_date=expiry_date,
                    site=site,
                    supplier=supplier,
                    total_price=total_price,
                    batch=batch,
                    serial_number=serial_number,
                    status=status_,
                    client=client,

                )
                line_item = asn.get('line_items')
                if line_item:
                    for i, line_items in enumerate(line_item):
                        sku_name = line_items.get('sku')
                        if not sku_name:
                            self.error_msg = 'Sku not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return
                        sku = WhSku.objects.filter(name=sku_name).first()
                        if not sku:
                            self.error_msg = 'Sku not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                        qty = line_items.get('qty')
                        status_ = line_items.get('status')
                        if not status_:
                            self.error_msg = 'Status number not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return
                        qty_received = line_items.get('qty_received')
                        price_per_qty_ = line_items.get('price_per_qty')
                        item_desc = line_items.get('item_desc')
                        asn_order = line_items.get('asn_order')
                        if not asn_order:
                            self.error_msg = 'Asn order  not found'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                        lines_item_dict = {
                            'sku': sku,
                            'qty': qty,
                            'status': status_,
                            'qty_received': qty_received,
                            'price_per_qty': price_per_qty_,
                            'item_desc': item_desc,
                            'asn_order': asn_order_create
                        }
                        self.asn_lines_list.append(ASNLineItems(**lines_item_dict))

            ASNLineItems.objects.bulk_create(self.asn_lines_list)
