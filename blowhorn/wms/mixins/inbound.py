# Django and System libraries
import logging
from django.conf import settings
import math
import phonenumbers

# 3rd party libraries
from rest_framework import status, exceptions
from blowhorn.oscar.core.loading import get_model
from openpyxl import load_workbook
from phonenumbers import PhoneNumber
from phonenumber_field.phonenumber import PhoneNumber
# blowhorn imports
from blowhorn.users.models import User
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.wms.constants import SUPPLIER_MANDATORY_LABELS, PRODUCT_GROUP_MANDATORY_LABELS

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Supplier = get_model('wms', 'Supplier')
ProductGroup = get_model('wms', 'ProductGroup')


class SupplierImportMixin(object):
    def _process_import_data(self, data, request):
        """
            This method intializes the variables and perform basic prechecks
            on input file like file name, customer id and empty file check.
            Also does the row processing of the excel sheet where each row
            is equivalent to one order.
        """
        self.file_name = data.get('file')
        self.response = {'data': '', 'errors': ''}
        self.success = False
        self.status_code = status.HTTP_200_OK
        self.row_index = 0
        self.user = request.user
        self.customer = CustomerMixin().get_customer(self.user)

        self.values_list = []
        self.supplier_data_list = []

        # Initialize error lists for further processing
        self._initialize_error_list()

        # Load the workbook and initial validation
        error_message = self._initial_validations()
        if error_message:
            return self.success, error_message

    def _initial_validations(self):
        try:
            wb_obj = load_workbook(filename=self.file_name, data_only=True)
        except Exception as e:
            error_message = 'The file may be corrupted or not in xlsx format.' \
                            ' Please download a new sample sheet.'
            return error_message

        sheet_obj = wb_obj.active
        headers = [cell.value for cell in sheet_obj[1]]
        self.no_of_rows = sheet_obj.max_row

        for i in SUPPLIER_MANDATORY_LABELS + ['Sl No.']:
            if i not in headers:
                error_message = 'Incorrect file template uploaded. Please download the template again and upload'
                return error_message
        row_error = self._row_processing(sheet_obj, headers)
        if not self.values_list:
            error_message = 'File is empty'
            return error_message
        if len(row_error) != 0:
            error_message = 'Incorrect file format'
            return error_message

        created_supplier, message = self._create_supplier_from_data(self.values_list)
        self.success = True if created_supplier else False
        message = 'Uploaded Successfully' if created_supplier else message
        return message

    def _initialize_error_list(self):
        self.records_without_mandatory_fields = []
        self.records_with_duplicate_name = []
        self.recorder_with_duplicate_mobile = []
        self.records_with_existing_name = []
        self.records_with_invalid_mobile = []
        self.name_set = set()

    def _row_processing(self, sheet_obj, headers):
        index = 0
        row_error = []
        for row in sheet_obj.iter_rows(min_row=2):
            supplier_data_dict = {}
            index += 1
            for key, cell in zip(headers, row):
                if cell.value:
                    value = cell.value
                    supplier_data_dict[key] = value
            if supplier_data_dict:
                self.values_list.append(supplier_data_dict)
        return row_error

    def _create_supplier_from_data(self, supplier_data):
        print('-----------supplier data---------', supplier_data)
        for data in supplier_data:
            supplier_dict = {}
            self.row_index += 1

            if not set(list(data.keys())).intersection(set(SUPPLIER_MANDATORY_LABELS)) == set(
                    SUPPLIER_MANDATORY_LABELS):
                self.records_without_mandatory_fields.append(self.row_index)
                continue
                # return False, 'Mandatory fields are missing %s ' % SUPPLIER_MANDATORY_LABELS

            name = data['Name (Unique Identifier)']
            if Supplier.objects.filter(name=name, customer=self.customer).exists():
                self.records_with_existing_name.append(self.row_index)
            elif name in self.name_set:
                self.records_with_duplicate_name.append(self.row_index)
            else:
                self.name_set.add(name)

            phone_number = data.get('Mobile', None)

            if phone_number:
                is_phone_number, phone_number = self.input_float_validation(phone_number)
                try:
                    phone_number = PhoneNumber.from_string(str(phone_number), settings.COUNTRY_CODE_A2)
                    if not phone_number.is_valid():
                        self.records_with_invalid_mobile.append(self.row_index)
                        continue
                except phonenumbers.NumberParseException:
                    self.records_with_invalid_mobile.append(self.row_index)
                    continue
                if Supplier.objects.filter(phone_number=phone_number).exists():
                    self.recorder_with_duplicate_mobile.append(self.row_index)

            supplier_dict = {
                'name': name,
                'phone_number': phone_number,
                'customer': self.customer
            }

            self.supplier_data_list.append(Supplier(**supplier_dict))

        error = self._raise_error()
        if error:
            return False, error

        created = Supplier.objects.bulk_create(self.supplier_data_list)
        return True, len(created)

    def _raise_error(self):
        error_message = ''
        if self.records_without_mandatory_fields:
            error_message += ('Mandatory fields are missing %s in rows: %s. ' % (SUPPLIER_MANDATORY_LABELS,
                              self.records_without_mandatory_fields)
                              )
        if self.records_with_existing_name:
            error_message += ('Supplier name provided in rows %s already exist. ' % self.records_with_existing_name)

        if self.records_with_duplicate_name:
            error_message += ('Duplicate Supplier name present in rows %s. ' % self.records_with_duplicate_name)
        if self.recorder_with_duplicate_mobile:
            error_message += ('Phone numbers provided in rows %s already exist. ' % self.recorder_with_duplicate_mobile)
        return error_message

    def input_float_validation(self, data):
        is_valid = False

        data = str(data)
        temp = data.replace('.', '')

        if temp.isdigit():
            data_float = float(data)
            if math.floor(data_float) == data_float:
                is_valid = True
                data = str(int(data_float)).strip()

        return is_valid, data


class ProductGroupImportMixin(object):
    def _process_import_data(self, data, request):
        """
            This method intializes the variables and perform basic prechecks
            on input file like file name, customer id and empty file check.
            Also does the row processing of the excel sheet where each row
            is equivalent to one order.
        """
        self.file_name = data.get('file')
        self.response = {'data': '', 'errors': ''}
        self.success = False
        self.status_code = status.HTTP_200_OK
        self.row_index = 0
        self.user = request.user

        self.values_list = []
        self.product_data_list = []

        # Initialize error lists for further processing
        self._initialize_error_list()

        # Load the workbook and initial validation
        error_message = self._initial_validations()
        if error_message:
            return self.success, error_message

    def _initial_validations(self):
        try:
            wb_obj = load_workbook(filename=self.file_name, data_only=True)
        except Exception as e:
            error_message = 'The file may be corrupted or not in xlsx format.' \
                            ' Please download a new sample sheet.'
            return error_message

        sheet_obj = wb_obj.active
        headers = [cell.value for cell in sheet_obj[1]]
        self.no_of_rows = sheet_obj.max_row

        for i in PRODUCT_GROUP_MANDATORY_LABELS + ['Sl No.']:
            if i not in headers:
                error_message = 'Incorrect file template uploaded. Please download the template again and upload'
                return error_message
        row_error = self._row_processing(sheet_obj, headers)
        if not self.values_list:
            error_message = 'File is empty'
            return error_message
        if len(row_error) != 0:
            error_message = 'Incorrect file format'
            return error_message

        created_product_group, message = self._create_product_group_from_data(self.values_list)
        self.success = True if created_product_group else False
        message = 'Uploaded Successfully' if created_product_group else message
        return message

    def _initialize_error_list(self):
        self.records_without_mandatory_fields = []
        self.records_with_duplicate_name = []
        self.records_with_existing_name = []
        self.name_set = set()

    def _row_processing(self, sheet_obj, headers):
        index = 0
        row_error = []
        for row in sheet_obj.iter_rows(min_row=2):
            supplier_data_dict = {}
            index += 1
            for key, cell in zip(headers, row):
                if cell.value:
                    value = cell.value
                    supplier_data_dict[key] = value
            if supplier_data_dict:
                self.values_list.append(supplier_data_dict)
        return row_error

    def _create_product_group_from_data(self, product_data):
        print('-----------product group data---------', product_data)
        for data in product_data:
            product_dict = {}
            self.row_index += 1

            if not set(list(data.keys())).intersection(set(PRODUCT_GROUP_MANDATORY_LABELS)) == set(
                    PRODUCT_GROUP_MANDATORY_LABELS):
                self.records_without_mandatory_fields.append(self.row_index)
                continue
                # return False, 'Mandatory fields are missing %s ' % PRODUCT_GROUP_MANDATORY_LABELS

            name = data['Name (Unique Identifier)']
            customer = CustomerMixin().get_customer(self.user)

            if ProductGroup.objects.filter(name=name, customer=customer).exists():
                self.records_with_existing_name.append(self.row_index)
            elif name in self.name_set:
                self.records_with_duplicate_name.append(self.row_index)
            else:
                self.name_set.add(name)

            description = data.get('Description', None)

            product_dict = {
                'name': name,
                'description': description,
                'customer': customer
            }
            self.product_data_list.append(ProductGroup(**product_dict))

        error = self._raise_error()
        if error:
            return False, error

        created = ProductGroup.objects.bulk_create(self.product_data_list)
        return True, len(created)

    def _raise_error(self):
        error_message = ''
        if self.records_without_mandatory_fields:
            error_message += ('Mandatory fields %s are missing in rows %s. ' % (PRODUCT_GROUP_MANDATORY_LABELS,
                              self.records_without_mandatory_fields)
                              )
        if self.records_with_existing_name:
            error_message += ('Product Group name provided in rows %s already exist. ' % self.records_with_existing_name)
        if self.records_with_duplicate_name:
            error_message += ('Duplicate Product Group name present in rows %s. ' % self.records_with_duplicate_name)
        return error_message
