from rest_framework import status
from django.db import transaction
from datetime import datetime

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.constants import TYPE_FIELD_MAPPING, QC_TASK, DATE, FILE, PYTHON_DATA_FIELD_MAP, WH_TASK_COMPLETED, \
    ACTIVE_WHTASKS_STATUSES, WH_TASK_NEW, WH_TASK_HOLD, WH_TASK_REJECT, INV_LOCKED, PUTAWAY_TASK, QC_FAILED, QC_PASSED, \
    INV_UNLOCKED
from blowhorn.customer.mixins import CustomerMixin

ProductGroup = get_model('wms', 'ProductGroup')
Supplier = get_model('wms', 'Supplier')
WhSKU = get_model('wms', 'WhSku')
Location = get_model('wms', 'Location')
Zone = get_model('wms', 'LocationZone')
QCAttributes = get_model('wms', 'QCAttributes')
QCConfiguration = get_model('wms', 'QCConfiguration')
QCAttributeValues = get_model('wms', 'QCAttributeValues')
WhTask = get_model('wms', 'WhTask')
Inventory = get_model('wms', 'Inventory')
InventoryTransaction = get_model('wms', 'InventoryTransaction')


class QcConfigMixin(object):

    def _initialize_attributes(self, data, request):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        # self.qc_config_name = data.get('qc_config_name')
        self.wh_id = data.get('wh_id')
        self.product_group_id = data.get('product_group_id')
        self.product_group_name = data.get('product_group_name')
        self.sku_id = data.get('sku_id')
        self.sku_name = data.get('sku_name')
        self.supplier_id = data.get('supplier_id')
        self.supplier_name = data.get('supplier_name')
        self.zone_id = data.get('zone_id')
        self.zone_name = data.get('zone_name')
        self.loc_id = data.get('loc_id')
        self.loc_name = data.get('loc_name')
        self.qc_at_receiving = data.get('qc_at_receiving', False)
        self.qc_at_picking = data.get('qc_at_picking', False)
        self.qc_at_shipping = data.get('qc_at_picking', False)
        self.qc_attribute_ids = data.get('qc_attribute_ids', False)
        self.is_active = data.get('is_active', False)

        self.customer = CustomerMixin().get_customer(request.user)

        print('self.supplier_name - ', self.supplier_name)
        print('self.qc_at_receiving - ', self.qc_at_receiving)

        if not self.product_group_id and not self.product_group_name \
            and not self.sku_id and not self.sku_name \
            and not self.supplier_id and self.supplier_name \
            and not self.zone_id and not self.zone_name \
            and not self.loc_id and not self.loc_name:
            self.error_msg = 'At least one QC config field should have value'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if (self.loc_id or self.loc_name or self.zone_id or self.zone_name) and not self.wh_id:
            self.error_msg = 'Warehouse id is mandatory when location or zone data is given'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        self.product_group = None
        self.sku = None
        self.supplier = None
        self.zone = None
        self.loc = None
        self.qc_attributes = []
        self.qc_configs = None

        self._check_data()

    def _check_data(self):
        if self.product_group_id:
            try:
                self.product_group = ProductGroup.objects.get(pk=self.product_group_id)
            except:
                self.error_msg = 'Invalid product group id'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.product_group_name:
            try:
                self.product_group = ProductGroup.objects.get(name=self.product_group_name, customer=self.customer)
            except:
                self.error_msg = 'Invalid product group name'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.sku_id:
            try:
                self.sku = WhSKU.objects.get(pk=self.sku_id)
            except:
                self.error_msg = 'Invalid sku id'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.sku_name:
            try:
                self.sku = WhSKU.objects.get(name=self.sku_name, client__customer=self.customer)
            except:
                self.error_msg = 'Invalid sku name'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.supplier_id:
            try:
                self.supplier = Supplier.objects.get(pk=self.supplier_id)
            except:
                self.error_msg = 'Invalid Supplier id'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.supplier_name:
            try:
                self.supplier = Supplier.objects.get(name=self.supplier_name, customer=self.customer)
            except:
                self.error_msg = 'Invalid Supplier name'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.zone_id:
            try:
                self.zone = Zone.objects.get(pk=self.zone_id, site__hub_id=self.wh_id)
            except:
                self.error_msg = 'Invalid Zone id'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.zone_name:
            try:
                self.zone = Zone.objects.get(name=self.zone_name, site__hub_id=self.wh_id)
            except:
                self.error_msg = 'Invalid Zone name'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.loc_id:
            try:
                self.loc = Location.objects.get(pk=self.loc_id, site__hub_id=self.wh_id)
            except:
                self.error_msg = 'Invalid location id'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.loc_name:
            try:
                self.loc = Location.objects.get(pk=self.loc_name, site__hub_id=self.wh_id)
            except:
                self.error_msg = 'Invalid location name'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        if self.qc_attribute_ids:
            self.qc_attributes = QCAttributes.objects.filter(id__in=self.qc_attribute_ids)
            if not self.qc_attributes or len(self.qc_attributes) != len(self.qc_attribute_ids):
                self.error_msg = 'Invalid list of qc attributes'
                self.status = status.HTTP_400_BAD_REQUEST
                return

    def _save_config_data(self):

        qc_attribute_data = {
            # 'name': self.qc_config_name,
            'product_group': self.product_group if self.product_group else None,
            'sku': self.sku if self.sku else None,
            'supplier': self.supplier if self.supplier else None,
            'zone': self.zone if self.zone else None,
            'location': self.loc if self.loc else None,
            'qc_at_receivng': self.qc_at_receiving,
            'qc_at_picking': self.qc_at_picking,
            'qc_at_shipping': self.qc_at_shipping,
            'is_active': self.is_active
        }

        try:
            with transaction.atomic():
                self.qc_configs = QCConfiguration.objects.create(**qc_attribute_data)
                print('self.qc_configs - ', self.qc_configs)
                for qc_attr in self.qc_attributes:
                    self.qc_configs.qc_attributes.add(qc_attr)
        except:
            self.error_msg = 'Failed to save QC config'
            self.status = status.HTTP_400_BAD_REQUEST
            return


class QcAttributesMixin(object):

    def _initialize_attributes(self, data, user):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        invalid_fields = []
        self.attr_data = data.get('attr_data')
        self.customer = CustomerMixin().get_customer(user)
        # self.qc_attr_name = data.get('qc_attr_name')
        for attr in self.attr_data:
            value_type = attr.get('type')
            if not TYPE_FIELD_MAPPING.get(value_type):
                invalid_fields.append(value_type)
        if invalid_fields:
            self.error_msg = 'Invalid field types - %s' % invalid_fields
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def _save_config_data(self):
        try:
            with transaction.atomic():
                attr_obj_list = []
                for attr in self.attr_data:
                    attr_obj_list.append(QCAttributes(
                        name=attr.get('name'),
                        text=attr.get('text'),
                        type=attr.get('type'),
                        required=attr.get('required', False),
                        is_active=attr.get('is_active', False),
                        customer=self.customer

                    ))
                self.qc_configs = QCAttributes.objects.bulk_create(attr_obj_list)
                print('self.qc_configs - ', self.qc_configs)
        except:
            self.error_msg = 'QC question already exists'
            self.status = status.HTTP_400_BAD_REQUEST
            return


class QcAttributeValuesMixin(object):

    def _initialize_attributes(self, data):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.qc_config_id = data.get('qc_config_id')
        self.qc_text = data.get('qc_text')
        self.value_type = data.get('field_type')
        self.value_type = data.get('required')

    def _save_config_data(self):

        qc_attribute_data = {
            'name': self.qc_attr_name,
            'text': self.qc_text,
            'type': self.field_type,
            'required': self.required
        }

        try:
            with transaction.atomic():
                qc_config = QCAttributes.objects.create(qc_attribute_data)
                for qc_attr in self.qc_attributes:
                    qc_config.qc_attributes.add(qc_attr)
        except:
            self.error_msg = 'Failed to save QC attributes'
            self.status = status.HTTP_400_BAD_REQUEST
            return


class QcTaskMixin(object):

    def _initialize_attributes(self, data, user):
        self.data = data
        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.user = user
        print('data - ', self.user)
        self.task_id = data.get('task_id')
        self.qc_result = data.get('qc_result')
        self.qc_attributes = data.get('qc_attributes')
        self.sku_id = data.get('sku_id', None)
        self.inv_id = data.get('inv_id', None)
        self.inv = None
        if self.inv_id:
            self.inv = Inventory.objects.filter(id=self.inv_id).first()
        self.qc_config_id = data.get('qc_config_id', None)
        # if not self.sku_id and not self.inv_id:
        if not self.task_id and not self.sku_id and not self.inv_id:
            self.error_msg = 'Qc Task id is missing'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        try:
            self.task = WhTask.objects.filter(pk=self.task_id, task_type__name=QC_TASK,
                                              status__in=ACTIVE_WHTASKS_STATUSES).select_related('qc_config').first()
        except:
            self.error_msg = 'Invalid Task id'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if not self.qc_attributes:
            self.error_msg = 'QC attributes are missing'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if not self.qc_result:
            self.error_msg = 'qc_result is missing'
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def _check_save_data(self):

        qc_attrs = self.qc_attributes
        if not qc_attrs:
            qc_attrs = self.task.qc_config.qc_attributes.all()
        qc_check_data = {}
        qc_attr_values_list = []

        # attribute ids for the given qc task
        attr_ids = []
        print('qc_attrs - ', qc_attrs)
        for attr in qc_attrs:
            is_dict = True if isinstance(attr, dict) else False
            id_ = attr.get('id') if is_dict else attr.id
            qc_check_data[id_] = {'data_type': attr.get('type') if is_dict else attr.type,
                                      'required':  attr.get('required') if is_dict else attr.required}
            attr_ids.append(id_)
        # attribute ids passed in API call
        given_attr_ids = []
        for attr in self.qc_attributes:
            given_attr_ids.append(attr.get('id'))
            data_type = (qc_check_data.get(attr.get('id'))).get('data_type')
            required = (qc_check_data.get(attr.get('id'))).get('required')
            if not data_type:
                self.error_msg = 'Invalid qc attribute id'
                self.status = status.HTTP_400_BAD_REQUEST
                return
            value = attr.get('value')
            if required and value is None:
                self.error_msg = 'Value required for qc attribute id - %d' % attr.get('id')
                self.status = status.HTTP_400_BAD_REQUEST
                return
            if not value:
                continue

            db_field = TYPE_FIELD_MAPPING.get(data_type)
            if PYTHON_DATA_FIELD_MAP[data_type] == 'date':
                try:
                    value = datetime.strptime(value, "%Y-%m-%d").date()
                except:
                    self.error_msg = 'Invalid date format'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            # TODO: test this
            elif PYTHON_DATA_FIELD_MAP[data_type] == FILE:
                pass

            else:
                print('PYTHON_DATA_FIELD_MAP[data_type] - ', PYTHON_DATA_FIELD_MAP[data_type])
                print('type(value) - ', str(type(value)))
                if not isinstance(value, PYTHON_DATA_FIELD_MAP[data_type]):
                    self.error_msg = 'Invalid %s data for attribute id %d' % (data_type, attr.get('id'))
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            qc_attr_data = {
                'attribute_id': attr.get('id'),
                'qc_config_id': self.qc_config_id or self.task.qc_config_id,
                db_field: value,
                'qc_task_id': self.task.id if self.task else None,
                'created_by': self.user
            }
            qc_attr_values_list.append(QCAttributeValues(**qc_attr_data))

        # check if any qc task attributes are missing
        attr_ids.sort()
        given_attr_ids.sort()
        if not (attr_ids == given_attr_ids):
            self.error_msg = 'Attribute ids does not match QC task attr ids'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        inventory = None
        if self.qc_result == 'Failed' and not self.inv:
            inventory = Inventory.objects.filter(tag=self.task.tag, location=self.task.from_loc,
                                                 client=self.task.client, site=self.task.site, sku=self.task.sku,
                                                 qty=self.task.qty, supplier=self.task.supplier)

        task = None
        if self.task:
            task = WhTask.objects.filter(task_type__name=PUTAWAY_TASK,tag=self.task.tag,
                                         from_loc=self.task.from_loc, client=self.task.client,
                                         site=self.task.site, sku=self.task.sku,
                                         qty=self.task.qty, supplier=self.task.supplier,
                                         status=WH_TASK_HOLD)

        # try:
        with transaction.atomic():
            QCAttributeValues.objects.bulk_create(qc_attr_values_list)
            if self.task:
                self.task.operator = self.user
                self.task.status = WH_TASK_COMPLETED
                self.task.save()

            # Mark inventory as QC failed
            if inventory:
                inventory.update(status=INV_LOCKED, remarks='QC Failed')
                if self.task:
                    task.update(status=WH_TASK_REJECT, remarks='QC Failed')
            elif task:
                # Release On Hold Task
                task.update(status=WH_TASK_NEW)
            if self.inv:
                inv_data = {}
                if self.qc_result == QC_FAILED:
                    inv_data = {
                        "status": INV_LOCKED,
                        "remarks": 'QC Failed'
                    }
                elif self.qc_result == QC_PASSED and self.inv.status == INV_LOCKED:
                    inv_data = {
                        "status": INV_UNLOCKED,
                        "remarks": 'QC Passed'
                    }
                if inv_data:
                    Inventory.objects.filter(id=self.inv.id).update(**inv_data)

            # write ITL
            write_itl(self.task, self.inv, self.qc_result, self.user)
        # except:
        #     self.error_msg = 'Failed to save response'
        #     self.status = status.HTTP_400_BAD_REQUEST
        #     return


def write_itl(task, inv, status, user):

    invs_trans = InventoryTransaction.objects.create(
        tag=task.tag if task else inv.tag,
        from_loc_id=task.from_loc_id if task else None,
        to_loc_id=task.to_loc_id if task else None,
        sku_id=task.sku_id if task else inv.sku_id,
        client=task.client if task else inv.client,
        qty=task.qty if task else inv.qty,
        site=task.site if task else inv.site,
        supplier=task.supplier if task else inv.supplier,
        tracking_level=inv.tracking_level if inv else None,
        pack_config=inv.pack_config if inv else None,
        reference=task.task_type.name if task else None,
        reference_number=task.id if task else inv.id,
        status=status
    )
    invs_trans.created_by = user
    invs_trans.modified_by = user
    invs_trans.save()

