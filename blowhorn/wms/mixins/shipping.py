# System & Third Party Library Imports
import json

# Django Imports
from django.utils import timezone
from django.db import transaction
from django.db.models import *

# Blowhorn Imports
from blowhorn.order.models import Order, OrderLine
from blowhorn.wms.submodels.location import WhSite
from blowhorn.wms.submodels.inventory import Inventory
from blowhorn.wms.submodels.query_builder import QueryBuilder, QueryBuilderLines
from blowhorn.wms.submodels.containers import WmsContainer, WmsManifest, WmsManifestLine, WmsAllocation, \
    WmsAllocationLine
from blowhorn.wms.constants import ORDER_LINE_PICKED, ORDER_LINE_MANIFESTED, WMS_INV_CREATED, \
    INV_UNLOCKED, ORDER_LINE_SHIPPED, WMS_INV_QTY_UPDATED, INV_LOCKED, WMS_RECEIVED, ALLOCATION_ALLOCATED, \
    ALLOCATION_PARTIALLY_ALLOCATED, ALLOCATION_SHORT, PROCESS_ALLOCATION
from blowhorn.customer.mixins import CustomerMixin
from config.settings.status_pipelines import DRIVER_ACCEPTED, ACTIVE, TRIP_IN_PROGRESS, TRIP_NEW, REACHED_AT_HUB_RTO, \
    ORDER_PICKED
from config.settings import status_pipelines as StatusPipeline
from blowhorn.wms.views.stockcheck import StockInfo
from blowhorn.wms.services.allocation import OrderAllocation
from blowhorn.wms.v1.views.wms import get_user_permission
from blowhorn.wms.submodels.containers import WmsAllocationConfig
from blowhorn.wms.services.stockcheck import StockCheckService


class AllocationMixin(object):

    def _initialize_attributes(self, data, request):
        self.user = request.user
        self.order_ids = data.get('order_ids', [])
        self.orderline_ids = data.get('orderline_ids', [])
        self.customer = CustomerMixin().get_customer(self.user)
        self.allocation_config = WmsAllocationConfig.objects.filter(id=data.get('allocation_config_id')).first()
        self.error_message = ''

        if not self.allocation_config:
            self.error_message = 'Could not find the allocation config'

        if not self.allocation_config.is_active:
            self.error_message = 'Selected Allocation Config is not active'

        self.site_id = self.allocation_config.site_id
        sites = get_user_permission(self.user)

        self.query_builder = self.allocation_config.query_builder

        if not (self.order_ids or self.orderline_ids):
            self.error_message = "Order/ OrderLine IDs are required for allocation"

        if not int(self.site_id) in sites:
            self.error_message = "User doesn't have Site Permission"

        self.site = WhSite.objects.filter(id=self.site_id).first()

        self.orderline_qs = None
        self.order_qs = None

        if self.order_ids:
            self.order_qs = Order.objects.filter(id__in=self.order_ids)
            self.orderline_qs = OrderLine.objects.filter(order_id__in=self.order_ids).exclude(
                allocation_status=ALLOCATION_ALLOCATED)

        if self.orderline_ids:
            self.order_qs = Order.objects.filter(order_lines_id__in=self.orderline_ids)
            self.orderline_qs = OrderLine.objects.filter(id__in=self.orderline_ids).exclude(
                allocation_status=ALLOCATION_ALLOCATED)

        if not self.orderline_qs:
            self.error_message = 'No unallocated orders found from the selected orders'

    def get_allocation_config_qs(self, invs):
        query_ = Q()

        if self.allocation_config.product_group:
            query_ &= Q(sku__product_group=self.allocation_config.product_group)

        if self.allocation_config.supplier:
            query_ &= Q(supplier=self.allocation_config.supplier)

        if self.allocation_config.client:
            query_ &= Q(client=self.allocation_config.client)

        return invs.filter(query_)

    def get_allocated_inventory_tags(self, qty, tracking_lvl, tracking_level_obj, user, order, orderline):
        from blowhorn.wms.views.shipping import Manifest
        invs = OrderAllocation(self.allocation_config, orderline).create_query_from_builder()
        invs = self.get_allocation_config_qs(invs)
        allocated_qty = 0
        inv_ids = []
        for inv in invs:
            inv_qty, tracking_lvl_ = Manifest().get_qty_in_given_tracking_level(inv, tracking_lvl,
                                                                                tracking_level_obj)
            required_qty = qty - allocated_qty
            if inv_qty <= required_qty and allocated_qty < qty:
                allocated_qty += inv.qty
                inv.status = INV_LOCKED
                inv.modified_by = user
                inv.save()
                inv_ids.append(inv.id)
                StockCheckService().write_itl(inv, qty, ALLOCATION_ALLOCATED, user,
                                              tracking_level=tracking_lvl, tag=inv.tag,
                                              container=inv.container, order=order, qty_left=0)

            if inv_qty > required_qty and qty > allocated_qty:
                tag = StockCheckService().get_latest_tag(inv.tag)
                new_inv = Inventory.objects.create(tag=tag, sku_id=inv.sku_id, status=INV_LOCKED,
                                                   client=inv.client, qty=required_qty,
                                                   weight=inv.weight, tracking_level=tracking_lvl,
                                                   location_id=inv.location_id, site_id=inv.site_id,
                                                   supplier=inv.supplier, pack_config=inv.pack_config,
                                                   container=inv.container)
                new_inv.created_by = user
                new_inv.modified_by = user
                new_inv.save()
                StockCheckService().write_itl(new_inv, qty, ALLOCATION_ALLOCATED, user,
                                              tracking_level=tracking_lvl, tag=tag,
                                              container=inv.container, order=order)
                StockCheckService().write_itl(inv, inv_qty - qty, WMS_INV_QTY_UPDATED, user,
                                              tracking_level=tracking_lvl, qty_left=inv_qty-qty)
                allocated_qty += required_qty
                inv_ids.append(new_inv.id)
                inv.qty = inv_qty - required_qty
                inv.tracking_level = tracking_lvl
                inv.modified_by = user
                inv.save()
        return inv_ids, allocated_qty

    def _run_allocation_algorithm(self):
        with transaction.atomic():
            allocation_obj = WmsAllocation.objects.create(site=self.site, no_of_orders_allocated=len(self.order_ids),
                                                          allocation_time=timezone.now())
            allocation_obj.created_by = self.user
            allocation_obj.modified_by = self.user
            allocation_obj.save()

            for orderline in self.orderline_qs:
                whsku = orderline.wh_sku
                qty = orderline.quantity
                tracking_lvl = orderline.tracking_level
                tracking_level_obj = whsku.pack_config.tracking_level
                order = orderline.order

                inv_ids, allocated_qty = self.get_allocated_inventory_tags(qty, tracking_lvl, tracking_level_obj,
                                                                           self.user, order, orderline)

                allocation_line = WmsAllocationLine.objects.create(allocation=allocation_obj, order=order,
                                                                   order_line=orderline, requested_qty=qty,
                                                                   allocated_qty=allocated_qty
                                                                   )
                if not allocated_qty:
                    allocation_status = ALLOCATION_SHORT
                elif allocated_qty < qty:
                    allocation_status = ALLOCATION_PARTIALLY_ALLOCATED
                else:
                    allocation_status = ALLOCATION_ALLOCATED

                for inv_id in inv_ids:
                    inv = Inventory.objects.filter(id=inv_id).first()
                    allocation_line.inventories.add(inv) if inv else None

                allocation_line.created_by = self.user
                allocation_line.modified_by = self.user
                allocation_line.save()

                orderline.allocation_status = allocation_status
                orderline.qty_allocated = allocated_qty
                orderline.modified_by = self.user
                orderline.save()

            allocation_obj.no_of_orders_allocated = self.order_qs.count()
            allocation_obj.save()

            for order in self.order_qs.iterator():
                allocation_status = ALLOCATION_ALLOCATED
                if order.order_lines.filter(allocation_status=ALLOCATION_PARTIALLY_ALLOCATED).exists():
                    allocation_status = ALLOCATION_PARTIALLY_ALLOCATED
                elif order.order_lines.filter(allocation_status=ALLOCATION_SHORT).exists():
                    allocation_status = ALLOCATION_SHORT
                order.allocation_status = allocation_status
                order.modified_by = self.user
                order.save()

    def get_response_data(self):
        data = {}
        for order in self.order_qs.iterator():
            data['number'] = order.number
            data['allocation_status'] = order.allocation_status
            data['order_lines'] = {}
            for orderline in order.order_lines.iterator():
                data['order_lines']['id'] = orderline.id
                data['order_lines']['allocation_status'] = orderline.allocation_status
        return data
