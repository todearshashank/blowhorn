import math
from rest_framework import status
from django.db import transaction
from openpyxl import load_workbook

from blowhorn.oscar.core.loading import get_model

from blowhorn.wms.models import Rack, LocationZone, StorageType, \
    LocationType, Location, Client
from blowhorn.address.utils import get_wms_hub_for_customer
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.wms.serializers.sku import WhSkuSerializer
from blowhorn.wms.constants import WH_SKU_MANDATORY_LABELS, PRODUCT_GROUP_MANDATORY_LABELS
from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED, DISCOUNT_TYPE_PERCENTAGE

Dimension = get_model('order', 'Dimension')
Hub = get_model('address', 'Hub')
City = get_model('address', 'City')
ProductGroup = get_model('wms', 'ProductGroup')
PackConfig = get_model('wms', 'PackConfig')
WhSku = get_model('wms', 'WhSku')
Supplier = get_model('wms', 'Supplier')


class WhSkuImportMixins(object):

    def _initialize_attributes(self, user, data):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.user = user
        self.data = data
        self.customer = None
        self.response_data = None
        self.sku_data_list = []
        try:
            self.customer = self.user.customer
        except:
            self.error_msg = 'Customer not found'
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def _create_sku_data(self):
        for sku_data in self.data:
            client_name = sku_data.get('client_name')
            sku_name = sku_data.get('sku_name')
            description = sku_data.get('description')
            pack_config = sku_data.get('pack_config')
            product_group = sku_data.get('product_group')
            each_weight = sku_data.get('each_weight')
            each_volume = sku_data.get('each_volume')
            length = sku_data.get('length')
            breadth = sku_data.get('breadth')
            height = sku_data.get('height')
            shelf_life = sku_data.get('shelf_life')
            shelf_life_uom = sku_data.get('shelf_life_uom')
            mrp = sku_data.get('Max Retail Price') or None
            discounts = sku_data.get('Discounts') or 0
            discount_type = sku_data.get('Discount Type') or DISCOUNT_TYPE_FIXED

            try:
                client = Client.objects.get(name=client_name)
                pack_config = PackConfig.objects.get(name=pack_config)
                product_group = ProductGroup.objects.get(name=product_group)
            except Exception as e:
                self.error_msg = e.args[0]
                self.status = status.HTTP_400_BAD_REQUEST
                return

            sku_data = {
                'name': sku_name,
                'description': description,
                'product_group': product_group,
                'pack_config': pack_config,
                'client': client,
                'customer': self.customer,
                'each_weight': each_weight,
                'each_volume': each_volume,
                'shelf_life': shelf_life,
                'shelf_life_uom': shelf_life_uom,
                'mrp': mrp,
                'discounts': discounts,
                'discount_type': discount_type,
            }
            self.sku_data_list.append(sku_data)

        sku_serializer = WhSkuSerializer(data=self.sku_data_list, many=True)
        if sku_serializer.is_valid():
            sku_serializer.save()

        self.response_data = sku_serializer.data


class WhSkuExcelImportMixin(object):

    def _process_import_data(self, data, request):
        """
            This method intializes the variables and perform basic prechecks
            on input file like file name, customer id and empty file check.
            Also does the row processing of the excel sheet where each row
            is equivalent to one order.
        """
        self.file_name = data.get('file')
        self.response = {'data': '', 'errors': ''}
        self.success = False
        self.status_code = status.HTTP_200_OK
        self.row_index = 0
        self.user = request.user

        self.values_list = []
        self.sku_data_list = []

        # Initialize error lists for further processing
        self._initialize_error_list()
        # Load the workbook and initial validation
        message = self._initial_validations()
        if message:
            return self.success, message

    def _initial_validations(self):
        try:
            wb_obj = load_workbook(filename=self.file_name, data_only=True)
        except Exception as e:
            error_message = 'The file may be corrupted or not in xlsx format.' \
                            ' Please download a new sample sheet.'
            return error_message

        sheet_obj = wb_obj.active
        headers = [cell.value for cell in sheet_obj[1]]
        self.no_of_rows = sheet_obj.max_row

        for i in WH_SKU_MANDATORY_LABELS + ['Sl No.']:
            if i not in headers:
                error_message = 'Incorrect file template uploaded. Please download the template again and upload'
                return error_message
        row_error = self._row_processing(sheet_obj, headers)
        if not self.values_list:
            error_message = 'File is empty'
            return error_message
        if len(row_error) != 0:
            error_message = 'Incorrect file format'
            return error_message

        created_sku, message = self._create_sku_from_data(self.values_list)
        self.success = True if created_sku else False
        message = 'Uploaded successfully' if created_sku else message
        return message

    def _initialize_error_list(self):
        self.records_with_existing_sku_name = []
        self.records_with_incorrect_supplier_name = []
        self.records_with_incorrect_product_group_name = []
        self.records_without_mandatory_fields = []
        self.records_with_incorrect_pack_config_name = []
        self.records_with_incorrect_client_name = []
        self.records_with_duplicate_sku_name = []
        self.records_with_invalid_discount_type = []
        self.records_with_invalid_discounts = []
        self.records_with_incorrect_each_weight = []
        self.sku_name_set = set()

    def _row_processing(self, sheet_obj, headers):
        index = 0
        row_error = []
        for row in sheet_obj.iter_rows(min_row=2):
            sku_data_dict = {}
            index += 1
            for key, cell in zip(headers, row):
                if cell.value:
                    value = cell.value
                    sku_data_dict[key] = value
            if sku_data_dict:
                self.values_list.append(sku_data_dict)
        return row_error

    def _create_sku_from_data(self, sku_data):
        print(sku_data, '----data--------')
        for data in sku_data:
            whsku_dict = {}
            self.row_index += 1

            if not set(list(data.keys())).intersection(set(WH_SKU_MANDATORY_LABELS)) == set(WH_SKU_MANDATORY_LABELS):
                self.records_without_mandatory_fields.append(self.row_index)
                continue
                # return False, 'Mandatory fields are missing %s' % WH_SKU_MANDATORY_LABELS
            sku_name = data['Name (Unique Identifier)']
            if WhSku.objects.filter(name=sku_name).exists():
                self.records_with_existing_sku_name.append(self.row_index)
            elif sku_name in self.sku_name_set:
                self.records_with_duplicate_sku_name.append(self.row_index)
            else:
                self.sku_name_set.add(sku_name)

            supplier_name = data['Supplier']
            supplier = Supplier.objects.filter(name=supplier_name).first()
            if not supplier:
                self.records_with_incorrect_supplier_name.append(self.row_index)

            pack_config_name = data['Pack Config']
            pack_config = PackConfig.objects.filter(name=pack_config_name).first()
            if not pack_config:
                self.records_with_incorrect_pack_config_name.append(self.row_index)

            product_group_name = data['Product Group']
            product_group = ProductGroup.objects.filter(name=product_group_name).first()
            if not product_group:
                self.records_with_incorrect_product_group_name.append(self.row_index)

            customer = CustomerMixin().get_customer(self.user)
            client_name = data.get('Client', None)
            client = Client.objects.filter(name=client_name, customer=customer).first()
            if not client:
                self.records_with_incorrect_client_name.append(self.row_index)

            discount_type = data.get('Discount Type', DISCOUNT_TYPE_FIXED)
            discounts = data.get('Discounts', 0)
            if discount_type not in [DISCOUNT_TYPE_FIXED, DISCOUNT_TYPE_PERCENTAGE]:
                self.records_with_invalid_discount_type.append(self.row_index)
            if not self.input_float_validation(discounts)[0] or \
                (discount_type == DISCOUNT_TYPE_PERCENTAGE and not (0 <= float(discounts) <= 100)):
                self.records_with_invalid_discounts.append(self.row_index)

            each_weight = data.get('Each Weight', None)
            try:
                each_weight = float(each_weight)
            except:
                self.records_with_incorrect_each_weight.append(self.row_index)

            whsku_dict['name'] = data['Name (Unique Identifier)']
            whsku_dict['description'] = data.get('Description', None)
            whsku_dict['supplier_id'] = supplier.id if supplier else None
            whsku_dict['product_group_id'] = product_group.id if product_group else None
            # whsku_dict['customer_id'] = customer.id if customer else None
            whsku_dict['pack_config_id'] = pack_config.id if pack_config else None
            whsku_dict['each_weight'] = each_weight
            whsku_dict['each_volume'] = data.get('Each Volume', None)
            whsku_dict['client_id'] = client.id if client else None
            whsku_dict['mrp'] = data.get('Max Retail Price', None)
            whsku_dict['discounts'] = discounts
            whsku_dict['discount_type'] = discount_type
            self.sku_data_list.append(WhSku(**whsku_dict))

        error = self._raise_error()
        if error:
            return False, error
        created = WhSku.objects.bulk_create(self.sku_data_list)
        return True, len(created)

    def _raise_error(self):
        error_message = ''
        if self.records_with_existing_sku_name:
            error_message += ('SKU Name for rows %s already exist. ' % self.records_with_existing_sku_name)
        if self.records_with_duplicate_sku_name:
            error_message += (' Duplicate sku name provided in row numbers %s,'
                              ' SKU name has to be unique. ' % self.records_with_duplicate_sku_name)
        if self.records_with_incorrect_supplier_name:
            error_message += (
                ' Incorrect supplier name provided in row numbers %s. ' % self.records_with_incorrect_supplier_name)
        if self.records_without_mandatory_fields:
            error_message += (
                ' Mandatory fields %s are missing in rows %s. ' % (WH_SKU_MANDATORY_LABELS,
                                                                   self.records_without_mandatory_fields)
            )
        if self.records_with_incorrect_client_name:
            error_message += (
                ' Client name given in rows %s does not exist. ' % self.records_with_incorrect_client_name
            )
        if self.records_with_invalid_discount_type:
            error_message += (
                'Discount Type should be fixed or percentage for rows %s \n' % self.records_with_invalid_discount_type
            )
        if self.records_with_invalid_discounts:
            error_message += (
                'Invalid discount amount for rows %s \n' % self.records_with_invalid_discounts
            )
        if self.records_with_incorrect_each_weight:
            error_message += (
                'weight should be in Float/Integer for rows %s \n' % self.records_with_incorrect_each_weight
            )
        return error_message

    def input_float_validation(self, data):
        is_valid = False
        data = str(data)
        temp = data.replace('.', '')

        if temp.isdigit():
            data_float = float(data)
            if math.floor(data_float) == data_float:
                is_valid = True
                data = str(int(data_float)).strip()

        return is_valid,data
