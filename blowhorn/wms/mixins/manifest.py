from rest_framework import status
from django.db.models import F

import random

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.submodels.tasks import WhTask, TaskType
from django.db import transaction
from blowhorn.wms.constants import WH_TASK_NEW, \
    WH_TASK_COMPLETED, ORDER_LINE_PICKED, ORDER_LINE_MANIFESTED, SHIPPING_TASK, \
    ORDER_LINE_SHIPPED

Hub = get_model('address', 'Hub')
Order = get_model('order', 'Order')
OrderLine = get_model('order', 'OrderLine')


class CF(F):
    ADD = '||'


class ManifestMixin(object):

    def _initialize_attributes(self, data):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.line_ids = data.get('line_ids')
        self.site_id = data.get('site_id')
        self.order_lines = OrderLine.objects.filter(id__in=self.line_ids,
                                                    status=ORDER_LINE_PICKED,
                                                    order__pickup_hub_id=self.site_id)
        if not self.line_ids:
            self.error_msg = 'Invalid line ids'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if len(self.order_lines) != len(self.line_ids):
            self.error_msg = 'Some line ids are invalid'
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def manifest_orders(self):
        OrderLine.objects.filter(id__in=self.line_ids, status=ORDER_LINE_PICKED
                                 ).update(
            status=ORDER_LINE_MANIFESTED, reference_number=random.randint(1, 999999999999))


class ShippingMixin(object):

    def _initialize_attributes(self, data):
        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.line_ids = data.get('line_ids')
        self.site_id = data.get('site_id')
        print('line_ids - ', self.line_ids)
        print('site_id - ', self.site_id)
        self.order_lines = OrderLine.objects.filter(id__in=self.line_ids,
                                                    status=ORDER_LINE_MANIFESTED,
                                                    order__pickup_hub_id=self.site_id)
        self.task_type = TaskType.objects.filter(name=SHIPPING_TASK).first()
        if not self.line_ids:
            self.error_msg = 'Invalid line ids'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if len(self.order_lines) != len(self.line_ids):
            self.error_msg = 'Some line ids are invalid'
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def ship_orders(self):
        inv_txn_list = []
        task_line_ids = []
        shipping_task = WhTask.objects.filter(order_line__in=self.order_lines, task_type=self.task_type,
                                              status=WH_TASK_NEW)
        print('shipping_task - ', shipping_task)
        if not shipping_task:
            self.error_msg = {'message': 'No SHIPPING task found for given order lines'}
            self.status = status.HTTP_400_BAD_REQUEST
            return
        try:
            with transaction.atomic():
                for task in shipping_task:
                    print('task - ', task)
                    inv = Inventory.objects.filter(sku=task.sku, tag=task.tag, location=task.from_loc, client=task.client,
                                             site=task.site).first()
                    inv_txn_list.append(InventoryTransaction(tag=task.tag, from_loc=task.from_loc, to_loc=task.to_loc,
                                                             sku=task.sku, client=task.client, qty=task.qty, status='Shipped',
                                                             site=task.site, reference=SHIPPING_TASK,
                                                             reference_number=task.id, supplier=inv.supplier))
                    task_line_ids.append(task.order_line_id)
                    print('inv - ', inv)
                    if inv.qty == task.qty:
                        inv.delete()
                    elif inv.qty > task.qty:
                        inv.qty = inv.qty - task.qty
                        inv.save()
                    else:
                        raise Exception('Invalid inv qty for line id - %d' % task.order_line_id)
                shipping_task.update(status=WH_TASK_COMPLETED)
                print('task_line_ids - ', task_line_ids)
                OrderLine.objects.filter(id__in=task_line_ids, status=ORDER_LINE_MANIFESTED
                                         ).update(
                    status=ORDER_LINE_SHIPPED)
                InventoryTransaction.objects.bulk_create(inv_txn_list)
        except Exception as e:
            self.error_msg = {'message': e.args[0]}
            self.status = status.HTTP_400_BAD_REQUEST
            return

