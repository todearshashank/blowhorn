from rest_framework import status
from config.settings import status_pipelines as StatusPipeline
from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.services.allocation import AllocateOrder

Order = get_model('order', 'Order')


class AllocateMixins(object):

    def _initialize_attributes(self, data):
        self.error_msg = None
        self.message = 'Success'
        self.status = status.HTTP_200_OK
        self.invalid_orders = []

        order_ids = data.get('order_ids')
        if not order_ids:
            self.error_msg = 'Order ids not given'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        self.order_qs = Order.objects.filter(id__in=order_ids).prefetch_related('order_lines')

        if len(order_ids) != self.order_qs.count():
            self.error_msg = 'Given order ids are not valid'
            self.status = status.HTTP_400_BAD_REQUEST
            return

    def run_allocation(self):
        for order in self.order_qs:
            if order.status != StatusPipeline.ORDER_NEW:
                self.error_msg = 'Only New orders can be allocated'
                self.status = status.HTTP_400_BAD_REQUEST
                return
            for line in order.order_lines.all():
                if not line.wh_sku:
                    self.error_msg = 'Orders with WMS SKUs can only be allocated'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return
            allocate_order = AllocateOrder(order)
            if not allocate_order.allocate_inventory():
                self.error_msg = allocate_order.error_msg
                self.status = status.HTTP_400_BAD_REQUEST
                return


