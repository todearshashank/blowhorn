from rest_framework import status
from django.db import transaction

from blowhorn.oscar.core.loading import get_model

from blowhorn.wms.models import Rack, LocationZone, StorageType, \
    LocationType, Location, Client
from blowhorn.address.utils import get_wms_hub_for_customer

Dimension = get_model('order', 'Dimension')
Hub = get_model('address', 'Hub')
City = get_model('address', 'City')
ProductGroup = get_model('wms', 'ProductGroup')
WhSite = get_model('wms', 'WhSite')

class WhSiteMixins(object):

    def _initialize_attributes(self, user, data):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.user = user
        self.site_id = data.get('site_id')
        self.site = None
        self.customer = user.customer if hasattr(user, 'customer') else None
        if not self.customer:
            self.error_msg = 'Customer not found'

        if self.site_id:
            try:
                self.site_qs = get_wms_hub_for_customer(self.customer, self.site_id)
            except:
                self.error_msg = 'Invalid Site Id'
                self.status = status.HTTP_400_BAD_REQUEST
                return
        else:
            self.site_qs = get_wms_hub_for_customer(self.customer)
            if not self.site_qs:
                self.error_msg = 'No site found for customer'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        self.location_data = {}
        self.zone_list = []
        self.return_data = {}
        self.response = {"site_data": []}

    def _get_location_data(self):

        zones = LocationZone.objects.filter(site__hub=self.site).prefetch_related('rack_zone', 'zone_locations').order_by(
            'sequence')

        self.return_data['site_id'] = self.site.id
        self.return_data['site_name'] = self.site.name
        self.return_data['site_city_name'] = self.site.city.name if self.site.city else None
        self.return_data['site_length'] = self.site.length
        self.return_data['site_breadth'] = self.site.breadth
        self.return_data['site_height'] = self.site.height
        self.return_data['site_uom'] = self.site.uom

        self.location_data = {"zones": []}
        for zone in zones:
            zone_data = {
                'zone_id': zone.id,
                'name':  zone.name,
                'racks': []
            }
            if zone.storage_type:
                zone_data['storage_type_name'] = zone.storage_type.name
                zone_data['storage_type_id'] = zone.storage_type.id

            if zone.loc_type:
                zone_data['loc_type_id'] = zone.loc_type.id
                zone_data['loc_type_name'] = zone.loc_type.name

            if zone.dimension:
                zone_data['length'] = zone.dimension.length
                zone_data['breadth'] = zone.dimension.breadth
                zone_data['height'] = zone.dimension.height
                zone_data['uom'] = zone.dimension.uom

            for rack in zone.rack_zone.all():
                rack_data = {
                    'rack_id': rack.id,
                    'name': rack.name,
                    'sequence': rack.sequence,
                    'weight': rack.weight,
                    'length': rack.length,
                    'breadth': rack.breadth,
                    'height': rack.height,
                    'uom': rack.uom,
                    'location_data': {
                        'front_side': {
                            'locations': []
                        },
                        'back_side': {
                            'locations': []
                        }
                    }
                }

                if rack.storage_type:
                    rack_data['storage_type_id'] = rack.storage_type.id
                    rack_data['storage_type_name'] = rack.storage_type.name
                elif zone.storage_type:
                    rack_data['storage_type_id'] = zone.storage_type.id
                    rack_data['storage_type_name'] = zone.storage_type.name

                if rack.loc_type:
                    rack_data['loc_type_id'] = rack.loc_type.id
                    rack_data['loc_type_name'] = rack.loc_type.name
                elif zone.loc_type:
                    rack_data['loc_type_id'] = zone.loc_type.id
                    rack_data['loc_type_name'] = zone.loc_type.name

                for location in rack.location_set.filter(front_rack_side=True).select_related('dimensions'):
                    location_data = {
                        'location_id': location.id,
                        'name': location.name,
                        'status': location.status,
                        'sequence': location.sequence,
                        'weight': location.weight,
                        'volume': location.volume,
                        'length': location.dimensions.length,
                        'breadth': location.dimensions.breadth,
                        'height': location.dimensions.height,
                        'uom': location.dimensions.uom,
                    }
                    rack_data['location_data']['front_side']['locations'].append(location_data)

                for location in rack.location_set.filter(front_rack_side=False).select_related('dimensions'):
                    location_data = {
                        'location_id': location.id,
                        'name': location.name,
                        'status': location.status,
                        'sequence': location.sequence,
                        'weight': location.weight,
                        'volume': location.volume,
                        'length': location.dimensions.length,
                        'breadth': location.dimensions.breadth,
                        'height': location.dimensions.height,
                        'uom': location.dimensions.uom,
                    }
                    rack_data['location_data']['back_side']['locations'].append(location_data)

                zone_data['racks'].append(rack_data)
            self.zone_list.append(zone_data)
        self.return_data['zones'] = self.zone_list

    def _get_site_map_data(self):

        for site in self.site_qs:
            self.site = site
            self._get_location_data()
            self.response['site_data'].append(self.return_data)


class ZoneMixins(object):

    def _initialize_attributes(self, user, data):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.response_data = {
            "message": "Success"
        }
        self.user = user
        self.customer = user.customer if hasattr(user, 'customer') else None
        if not self.customer:
            self.status = status.HTTP_400_BAD_REQUEST
            self.error_msg = 'Customer not found'
            return

        self.site_add = data.get('site_add', False)
        self.site_edit = data.get('site_edit', False)
        self.site_id = data.get('site_id')

        if self.site_add and self.site_id:
            self.error_msg = 'site_add cannot be True when site id is passed'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        if self.site_id:
            self.site = get_wms_hub_for_customer(self.customer, self.site_id)
            wh_obj = self.site
            if not self.site:
                self.error_msg = 'Invalid site id'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        self.client_id = data.get('client_id', False)
        self.client = None
        if self.client_id:
            try:
                self.client = Client.objects.get(pk=self.client_id)
            except:
                self.error_msg = 'Invalid client id'
                self.status = status.HTTP_400_BAD_REQUEST
                return

        self.site_city = data.get('site_city')
        if self.site_city:
            self.site_city = City.objects.filter(pk=self.site_city).first()

        if not self.site_city and self.site_add:
            self.error_msg = 'Invalid city id'
            self.status = status.HTTP_400_BAD_REQUEST
            return

        with transaction.atomic():

            site_data = {
                'is_wms_site': True
            }

            if data.get('site_name'):
                site_data['name'] = data.get('site_name')

            if data.get('site_length'):
                site_data['length'] = data.get('site_length')
            if data.get('site_breadth'):
                site_data['breadth'] = data.get('site_breadth')
            if data.get('site_height'):
                site_data['height'] = data.get('site_height')
            if data.get('uom'):
                site_data['uom'] = data.get('uom')
            if data.get('site_city'):
                site_data['city_id'] = data.get('site_city')
            # if self.client:
            #     site_data['client'] = self.client

            try:
                if self.site_add:
                    wh_obj = Hub.objects.create(**site_data)
                    site_data['hub'] = wh_obj

                    #For now DEFAULT site is added for each WH
                    # TODO: Get sites from FE and capture here
                    site_obj = WhSite.objects.create(**site_data)

                elif self.site_edit:
                    wh_obj = Hub.objects.filter(pk=self.site_id).update(**site_data)
            except Exception as e:
                self.error_msg = 'Site add/update failed - %s' % e.args[0]
                self.status = status.HTTP_400_BAD_REQUEST
                raise BaseException

            self.response_data['site_id'] = wh_obj.id
            self.response_data['site_name'] = wh_obj.name

            zones = data.get('zones')
            if not zones:
                return

            for zone in zones:
                zone_add = zone.get('zone_add', False)
                zone_edit = zone.get('zone_edit', False)
                zone_id = zone.get('zone_id')

                if zone_add and zone_edit:
                    self.error_msg = 'Both zone add and edit cannot be true'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

                if zone_edit and not zone_id:
                    self.error_msg = 'No zone id passed for zone edit'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

                if zone_edit or zone_id:
                    zone_obj = LocationZone.objects.filter(pk=zone.get('zone_id')).first()
                    if not zone_obj:
                        self.error_msg = 'Invalid zone id'
                        self.status = status.HTTP_400_BAD_REQUEST
                        return

                zone_data = {}
                if zone.get('name'):
                    zone_data['name'] = zone.get('name')
                if zone.get('loc_type_id'):
                    zone_data['loc_type_id'] = zone.get('loc_type_id')
                if zone.get('storage_type_id'):
                    zone_data['storage_type_id'] = zone.get('storage_type_id')

                dim = {}
                if zone.get('zone_length'):
                    dim['length'] = data.get('zone_length')
                if zone.get('zone_breadth'):
                    dim['breadth'] = data.get('zone_breadth')
                if zone.get('zone_height'):
                    dim['height'] = data.get('zone_height')
                if zone.get('uom'):
                    dim['height'] = data.get('uom')

                if zone_add:
                    dim = Dimension.objects.create(**dim)
                    zone_data['dimension'] = dim
                elif zone_edit and zone_obj.dimension and dim:
                    Dimension.objects.filter(pk=zone_obj.dimension.id).update(**dim)
                elif zone_edit and dim:
                    dim = Dimension.objects.create(**dim)
                    zone_data['dimension'] = dim

                if zone.get('zone_status'):
                    zone_data['status'] = zone.get('zone_status')

                try:
                    if zone_add:
                        zone_data['site'] = wh_obj
                        zone_obj = LocationZone.objects.create(**zone_data)
                    elif zone_edit:
                        zone_obj = LocationZone.objects.filter(id=zone_obj.id).update(**zone_data)
                except Exception as e:
                    self.error_msg = 'Zone add/update failed - %s' % e.args[0]
                    self.status = status.HTTP_400_BAD_REQUEST
                    raise BaseException

                racks = zone.get('racks')
                if not racks:
                    return

                # Add Racks
                for rack in racks:
                    rack_add = rack.get('rack_add', False)
                    rack_edit = rack.get('rack_edit', False)
                    rack_id = rack.get('rack_id')

                    rack_obj = None
                    if rack_edit or rack_id:
                        rack_obj = Rack.objects.filter(pk=rack.get('rack_id')).first()
                        if not rack_obj:
                            self.error_msg = 'Invalid rack id'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                    rack_data = {'zone': zone_obj}

                    if rack.get('name'):
                        rack_data['name'] = rack.get('name')
                    if rack.get('loc_type_id'):
                        rack_data['loc_type_id'] = rack.get('loc_type_id')
                    if rack.get('storage_type_id'):
                        rack_data['storage_type_id'] = rack.get('storage_type_id')

                    if rack.get('rack_length'):
                        rack_data['length'] = rack.get('rack_length')
                    if rack.get('rack_breadth'):
                        rack_data['breadth'] = rack.get('rack_breadth')
                    if rack.get('rack_height'):
                        rack_data['height'] = rack.get('rack_height')
                    if rack.get('uom'):
                        rack_data['uom'] = rack.get('uom')

                    if rack.get('rack_sequence'):
                        rack_data['sequence'] = rack.get('rack_sequence')

                    try:
                        if rack_add:
                            rack_data['site'] = wh_obj
                            rack_obj = Rack.objects.create(**rack_data)
                        elif rack_edit and rack_obj:
                            rack_obj = Rack.objects.filter(pk=rack_obj.id).update(**rack_data)
                    except Exception as e:
                        self.error_msg = 'Rack add/update failed - %s' % e.args[0]
                        self.status = status.HTTP_400_BAD_REQUEST
                        raise BaseException

                    locations = rack.get('locations')
                    if not locations:
                        return

                    # Add locations
                    for location in locations:
                        location_add = location.get('location_add', False)
                        location_edit = location.get('location_edit', False)
                        location_id = location.get('location_id', False)

                        if location_add and location_edit:
                            self.error_msg = 'Both location add and edit cannot be true'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                        if location_edit and not location_id:
                            self.error_msg = 'No location id passed for location edit'
                            self.status = status.HTTP_400_BAD_REQUEST
                            return

                        location_obj = None
                        if location_edit or location_id:
                            location_obj = Location.objects.filter(pk=location.get('location_id')).first()
                            if not location_obj:
                                self.error_msg = 'Invalid location id'
                                self.status = status.HTTP_400_BAD_REQUEST
                                raise BaseException

                        location_data = {'zone': zone_obj}
                        if rack_obj:
                            location_data['rack'] = rack_obj

                        if location.get('name'):
                            location_data['name'] = location.get('name')
                        if location.get('loc_type_id'):
                            location_data['loc_type_id'] = location.get('loc_type_id')
                        if location.get('storage_type_id'):
                            location_data['storage_type_id'] = location.get('storage_type_id')

                        if location.get('front_rack_side'):
                            location_data['front_rack_side'] = location.get('front_rack_side')
                        else:
                            location_data['front_rack_side'] = False

                        dim = {}
                        if location.get('location_length'):
                            dim['length'] = location.get('location_length')
                        if location.get('location_breadth'):
                            dim['breadth'] = location.get('location_breadth')
                        if location.get('location_height'):
                            dim['height'] = location.get('location_height')
                        if location.get('uom'):
                            dim['uom'] = location.get('uom')

                        if location_add:
                            dim = Dimension.objects.create(**dim)
                            location_data['dimensions'] = dim
                        elif location_edit and location_obj.dimensions and dim:
                            Dimension.objects.filter(pk=location_obj.dimensions.id).update(**dim)
                        elif location_edit and dim:
                            dim = Dimension.objects.create(**dim)
                            location_data['dimensions'] = dim

                        if location.get('location_sequence'):
                            location_data['sequence'] = location.get('location_sequence')
                        location_data['site'] = wh_obj
                        try:
                            if location_add:
                                location_obj = Location.objects.create(**location_data)
                            elif location_edit and location_obj:
                                location_obj = Location.objects.filter(pk=location_obj.id).update(**location_data)
                        except Exception as e:
                            self.error_msg = 'Location add/update failed - %s' % e.args[0]
                            self.status = status.HTTP_400_BAD_REQUEST
                            raise BaseException

        self.zone_data = data.get('zone_data', [])
        self.rack_data = data.get('rack_data', [])
        self.location_data = data.get('location_data', [])

    def _save_zone_data(self):

        for zone in self.zone_data:
            self.zone_id = zone.get('zone_id')
            self.zone = None
            if self.zone_id:
                try:
                    self.zone = LocationZone.objects.get(pk=self.zone_id)
                except:
                    self.error_msg = 'Invalid zone id'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            self.zone_name = zone.get('name')
            self.length = zone.get('length')
            self.breadth = zone.get('breadth')
            self.height = zone.get('height')
            self.storage_type = zone.get('storage_type_id')
            self.loc_type = zone.get('loc_type_id')
            self.zone_status = zone.get('status')
            self.uom = zone.get('uom')

            if self.storage_type:
                try:
                    self.storage_type = StorageType.objects.get(pk=self.storage_type)
                except:
                    self.error_msg = 'Invalid storage type'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            if self.loc_type:
                try:
                    self.loc_type = LocationType.objects.get(pk=self.loc_type)
                except:
                    self.error_msg = 'Invalid Location type'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            if self.zone:
                self.zone.name = self.zone_name
                self.zone.storage_type = self.storage_type if self.storage_type else self.zone.storage_type
                self.zone.loc_type = self.loc_type if self.loc_type else self.zone.loc_type
                self.zone.status = self.zone_status if self.zone_status else self.zone.status

                self.zone.save()
            else:
                # try:
                zone_data = {'name': self.zone_name}
                dim = {}
                if self.zone_status:
                    zone_data['status'] = self.zone_status
                if self.storage_type:
                    zone_data['storage_type'] = self.storage_type
                if self.loc_type:
                    zone_data['loc_type'] = self.loc_type
                if self.length:
                    dim['length'] = self.length
                if self.breadth:
                    dim['breadth'] = self.breadth
                if self.height:
                    dim['height'] = self.height
                if self.uom:
                    dim['uom'] = self.uom
                zone_data['site'] = self.site

                if self.length:
                    dim_obj = Dimension.objects.create(**dim)
                    zone_data['dimension'] = dim_obj

                try:
                    self.zone = LocationZone.objects.create(**zone_data)
                except:
                    self.error_msg = 'Location Zone creation failed'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            self.response_data = {
                "zone_id": self.zone.id,
                "name": self.zone.name
            }

    def _save_rack_data(self):
        for rack in self.rack_data:
            self.rack_id = rack.get('zone_id')
            self.rack = None
            if self.rack_id:
                try:
                    self.rack = Rack.objects.get(pk=self.rack_id)
                except:
                    self.error_msg = 'Invalid Rack id'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            self.rack_name = rack.get('name')
            self.length = rack.get('length')
            self.breadth = rack.get('breadth')
            self.height = rack.get('height')
            self.storage_type = rack.get('storage_type_id')
            self.loc_type = rack.get('loc_type_id')
            self.rack_status = rack.get('status')
            self.uom = rack.get('uom')

            if self.storage_type:
                try:
                    self.storage_type = StorageType.objects.get(pk=self.storage_type)
                except:
                    self.error_msg = 'Invalid storage type'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            if self.loc_type:
                try:
                    self.loc_type = LocationType.objects.get(pk=self.loc_type)
                except:
                    self.error_msg = 'Invalid Location type'
                    self.status = status.HTTP_400_BAD_REQUEST
                    return

            if self.rack:
                self.rack.name = self.rack_name
                self.rack.status = self.zone_status if self.zone_status else self.rack.status
                self.rack.save()
            else:
                # try:
                rack_data = {'name': self.rack_name}
                dim = {}
                if self.rack_status:
                    rack_data['status'] = self.zone_status
                if self.length:
                    dim['length'] = self.length
                if self.breadth:
                    dim['breadth'] = self.breadth
                if self.height:
                    dim['height'] = self.height
                if self.uom:
                    dim['uom'] = self.uom
                rack_data['site'] = self.site

                if self.length:
                    dim_obj = Dimension.objects.create(**dim)
                    rack_data['dimension'] = dim_obj

                self.zone = Rack.objects.create(**rack_data)
                # except:
                #     self.error_msg = 'Location Zone creation failed'
                #     self.status = status.HTTP_400_BAD_REQUEST
                #     return

            self.response_data = {
                "rack_id": self.rack.id,
                "name": self.rack_data.name
            }

    def _save_location_data(self):
        pass

    def _save_site_data(self):
        # self._save_zone_data()

        # self._save_rack_data()

        self._save_location_data()


class SiteConfigMixins(object):

    def _initialize_attributes(self, user):

        self.error_msg = None
        self.status = status.HTTP_200_OK
        self.user = user
        self.customer = user.customer if hasattr(user, 'customer') else None
        if not self.customer:
            self.error_msg = 'Customer not found'
            self.status = status.HTTP_400_BAD_REQUEST
            return
        self.sites = None
        self.site_config_data = {}

    def _get_site_config_data(self):

        self.warehouses = Hub.objects.filter(customer=self.customer, is_wms_site=True).prefetch_related('whsite_set')
        self.site_config_data['warehouses'] = []
        for wh in self.warehouses:
            wh_data = {'wh_id': wh.id, 'wh_name': wh.name, 'sites': []}
            for site in wh.whsite_set.all():
                site_data = {
                    'site_id': site.id,
                    'site_name': site.name
                }
                wh_data['sites'].append(site_data)
            self.site_config_data['warehouses'].append(wh_data)

        storage_types = dict(StorageType.objects.values_list('id', 'name'))
        loc_types = dict(LocationType.objects.values_list('id', 'name'))

        self.site_config_data['storage_types'] = storage_types
        self.site_config_data['loc_types'] = loc_types

        clients = dict(Client.objects.all().values_list('id', 'name'))
        self.site_config_data['clients'] = clients

        cities = dict(City.objects.all().values_list('id', 'name'))
        self.site_config_data['cities'] = cities

        product_groups = dict(ProductGroup.objects.all().values_list('id', 'name'))
        self.site_config_data['product_groups'] = product_groups

