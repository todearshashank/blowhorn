from django.utils.translation import ugettext_lazy as _
from django.db import models

from blowhorn.address.models import UserAddress
from blowhorn.common.models import BaseModel
from blowhorn.document.file_validator import FileValidator
from blowhorn.document.utils import generate_file_path
from blowhorn.order.models import ShippingAddress

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.constants import ORDER_LINE_STATUSES, ORDER_LINE_NEW, ALLOCATION_STATUSES, ALLOCATION_ALGORITHMS, \
    ALLOCATION_FIFO_ALGORITHM, OPERATION_CHOICES
from audit_log.models.fields import CreatingUserField

WhSite = get_model('wms', 'WhSite')


class WmsAllocation(BaseModel):

    site = models.ForeignKey(WhSite, null=True, blank=True, on_delete=models.PROTECT)

    no_of_orders_allocated = models.IntegerField(_('Number of orders allocated'), blank=True, null=True)

    allocation_time = models.DateTimeField(
        _('Job Last Ran At'), null=True, blank=True)

    def __str__(self):
        return str(self.site)

    class Meta:
        verbose_name = _("Allocation")
        verbose_name_plural = _("Allocations")


class WmsAllocationLine(BaseModel):

    order = models.ForeignKey('order.Order', null=True, blank=True, on_delete=models.PROTECT)

    order_line = models.ForeignKey('order.OrderLine', null=True, blank=True, on_delete=models.PROTECT)

    inventories = models.ManyToManyField('wms.Inventory', related_name='inventories')

    allocation = models.ForeignKey(WmsAllocation, null=True, blank=True, on_delete=models.PROTECT)

    requested_qty = models.IntegerField(_('Requested Quantity'), blank=True, null=True)

    allocated_qty = models.IntegerField('Allocated Quantity', blank=True, null=True)

    def __str__(self):
        return str(self.order_line)

    class Meta:
        verbose_name = _('Allocation Line')
        verbose_name_plural = _('Allocation Lines')


class WmsManifest(BaseModel):

    file = models.FileField(
        upload_to=generate_file_path, validators=[FileValidator()], max_length=256
    )

    status = models.CharField(_('Status'), choices=ORDER_LINE_STATUSES, default=ORDER_LINE_NEW, max_length=64)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    trip = models.ForeignKey('trip.Trip', on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return str(self.site)

    class Meta:
        verbose_name = _("WmsManifest")
        verbose_name_plural = _("WmsManifests")


class WmsManifestLine(BaseModel):
    order_line = models.ForeignKey('order.OrderLine', null=True, blank=True, on_delete=models.PROTECT)

    inventory = models.ForeignKey('wms.Inventory', null=True, blank=True, on_delete=models.PROTECT)

    allocation_line = models.ForeignKey(WmsAllocationLine, blank=True, null=True, on_delete=models.PROTECT)

    manifest = models.ForeignKey('wms.WmsManifest', null=True, blank=True, on_delete=models.PROTECT)

    inv_details = models.TextField(_('Text'), blank=True, null=True)

    def __str__(self):
        return str(self.order_line)

    class Meta:
        verbose_name = _("OrderLine")
        verbose_name_plural = _("Order Lines")


class WmsAllocationConfig(BaseModel):
    name = models.CharField(_("Allocation Config Name"), max_length=255, blank=True, null=True,
                            help_text=_("Allocation Config Name"))

    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.PROTECT)

    query_builder = models.ForeignKey('wms.QueryBuilder', null=True, blank=True, on_delete=models.PROTECT)

    product_group = models.ForeignKey('wms.ProductGroup', null=True, blank=True, on_delete=models.PROTECT)

    site = models.ForeignKey(WhSite, null=True, blank=True, on_delete=models.PROTECT)

    supplier = models.ForeignKey('wms.Supplier', null=True, blank=True, on_delete=models.PROTECT)

    client = models.ForeignKey('wms.Client', null=True, blank=True, on_delete=models.PROTECT)

    is_active = models.BooleanField(_("Is Active"), default=False)

    operation = models.CharField(_("Operation"), max_length=60, choices=OPERATION_CHOICES,
                                 null=True, blank=True)

    class Meta:
        unique_together = ('customer', 'name')


class ShippingLabel(BaseModel):
    user_address = models.ForeignKey(UserAddress, on_delete=models.PROTECT, null=True, blank=True)

    # manifest = models.ForeignKey(WmsManifest, on_delete=models.PROTECT, null=True, blank=True)

    file = models.FileField(
        upload_to=generate_file_path, validators=[FileValidator()], max_length=256
    )


class WmsContainer(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False, max_length=64)

    description = models.CharField(_('Description'), null=True, blank=True, max_length=256)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    # manifest = models.ForeignKey(WmsManifest, on_delete=models.PROTECT, null=True, blank=True)

    weight = models.FloatField(_('Weight'), null=True, blank=True)

    volume = models.FloatField(_('Volume'), null=True, blank=True)

    current_weight = models.FloatField(_('Current Weight'), default=0)

    current_volume = models.FloatField(_('Current Volume'), default=0)

    # shipping_address = models.ForeignKey(ShippingAddress, on_delete=models.PROTECT, null=True, blank=True)
    #
    length = models.FloatField(_('Length'), null=True, blank=True)

    breadth = models.FloatField(_('Breadth'), null=True, blank=True)

    height = models.FloatField(_('Height'), null=True, blank=True)

    uom = models.CharField(_('Unit Of Measurement'), max_length=50, null=True, blank=True)

    def __str__(self):
        return self.name


class WmsPallet(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=64, unique=True)

    description = models.CharField(_('Description'), null=True, blank=True,
                                   max_length=256)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    weight = models.FloatField(_('Weight'), null=True, blank=True)

    volume = models.FloatField(_('Volume'), null=True, blank=True)

    current_weight = models.FloatField(_('Current Weight'), default=0)

    current_volume = models.FloatField(_('Current Volume'), default=0)

    length = models.FloatField(_('Length'), null=True, blank=True)

    breadth = models.FloatField(_('Breadth'), null=True, blank=True)

    height = models.FloatField(_('Height'), null=True, blank=True)

    uom = models.CharField(_('Unit Of Measurement'), max_length=50, null=True, blank=True)

    def __str__(self):
        return self.name
