from django.db import models
from django.utils.translation import ugettext_lazy as _
from blowhorn.oscar.core.loading import get_model

from blowhorn.common.models import BaseModel
# from blowhorn.wms.submodels.location import Location, LocationZone, LocationType
# from blowhorn.wms.submodels.sku import ProductGroup
# from blowhorn.wms.submodels.tasks import TaskType

Location = get_model('wms', 'Location')
LocationZone = get_model('wms', 'LocationZone')
LocationType = get_model('wms', 'LocationType')
ProductGroup = get_model('wms', 'ProductGroup')
TaskType = get_model('wms', 'TaskType')


class PutawayAlgorithm(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=128, unique=True)

    by_zone = models.BooleanField(_('By Zone'), default=False)

    zone = models.ForeignKey(Location, null=True, blank=True, on_delete=models.PROTECT)

    location_sequence_order = models.CharField(_('By Location Sequence'), null=True, blank=True,
                                               choices=[('ASC', 'ASC'), ('DESC', 'DESC')],
                                               max_length=64)

    location_type = models.ForeignKey(LocationType, null=True, blank=True, on_delete=models.PROTECT)

    product_group = models.ForeignKey(ProductGroup, null=True, blank=True, on_delete=models.PROTECT)

    allow_sku_mixing = models.BooleanField(_('Allow SKU mixing'), default=False)

    sequence = models.PositiveIntegerField(_('Sequence'), unique=True)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), null=True, blank=True)


# class QueryBuilder(BaseModel):
#     name = models.CharField(_('Name'), null=False, blank=False,
#                             max_length=128, unique=True)
#
#     process = models.CharField(_('Name'), null=False, blank=False, max_length=128,
#                                choices=[('PutAway', 'PutAway'), ('Allocation', 'Allocation')])
#
#     sequence = models.PositiveIntegerField(_('Sequence'), unique=True)
#
#
# class QueryBuilderLines(models.Model):
#     query_builder = models.ForeignKey(QueryBuilder, on_delete=models.PROTECT)
#
#     from_model = models.CharField(_('From'), null=False, blank=False,
#                                 max_length=128)
#
#     where_condition = models.CharField(_('Where COndition'), null=False, blank=False,
#                               max_length=128, choices=[('AND', 'AND'), ('OR', 'OR')])
#
#     open_parenthesis = models.BooleanField(_('Open Parenthesis'), default=False)
#
#     filter = models.CharField(_('Filter On Field'), null=False, blank=False,
#                               max_length=128)
#
#     operator = models.CharField(_('Operator'), null=False, blank=False,
#                             max_length=128)
#
#     field_value = models.CharField(_('Filter Value'), null=False, blank=False,
#                               max_length=128)
#
#     close_parenthesis = models.BooleanField(_('Close Parenthesis'), default=False)
#
#     group_by = models.CharField(_('Group By'), null=False, blank=False,
#                             max_length=128)
#
#     having_clause = models.CharField(_('Having'), null=False, blank=False,
#                             max_length=128)
#
#     order_by = models.CharField(_('Order By'), null=False, blank=False,
#                             max_length=128)
#
#     functions = models.CharField(_('Functions'), null=False, blank=False,
#                             max_length=128, choices=[('Avg', 'Avg'), ('Max', 'Max'), ('Count', 'Count')])
#
#
