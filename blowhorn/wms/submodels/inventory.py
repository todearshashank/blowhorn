from django.utils.translation import ugettext_lazy as _
from django.db import models
from blowhorn.common.models import BaseModel
from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.submodels.containers import WmsContainer, WmsManifest

from blowhorn.wms.submodels.sku import WhSku, PackConfig
from blowhorn.wms.submodels.location import Location, Client
from blowhorn.wms.constants import INV_STATUSES, INV_UNLOCKED


Dimension = get_model('order', 'Dimension')
Order = get_model('order', 'Order')
WhSite = get_model('wms', 'WhSite')
Supplier = get_model('wms', 'Supplier')


class Inventory(BaseModel):
    tag = models.CharField(_('Tag'), null=False, blank=False, max_length=64)

    sku = models.ForeignKey(WhSku, on_delete=models.PROTECT)

    location = models.ForeignKey(Location, on_delete=models.PROTECT)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    pack_config = models.ForeignKey(PackConfig, null=True, blank=True, on_delete=models.PROTECT)

    tracking_level = models.CharField(_('Tracking Level'), null=True, blank=True, max_length=128)

    client = models.ForeignKey(Client, on_delete=models.PROTECT, null=True, blank=False)

    container = models.ForeignKey(WmsContainer, on_delete=models.PROTECT, null=True, blank=True)

    qty = models.PositiveIntegerField(_('Quantity'))

    allocated_qty = models.PositiveIntegerField(_('Allocated Quantity'), default=0)

    status = models.CharField(_('Status'), choices=INV_STATUSES,
                              null=False, blank=True, max_length=64,
                              default=INV_UNLOCKED)

    weight = models.DecimalField(_('Weight'), null=True, blank=True, decimal_places=2, max_digits=300)

    dimensions = models.ForeignKey(Dimension, null=True, blank=True, on_delete=models.PROTECT)

    expiry_date = models.DateTimeField(_('Expiry Date'), null=True, blank=True)

    mfg_date = models.DateTimeField(_('Manufacturing Date'), null=True, blank=True)

    supplier = models.ForeignKey(Supplier, null=True, blank=True, on_delete=models.PROTECT)

    remarks = models.CharField(_('Remarks'), null=True, blank=True,
                               max_length=256)

    is_shopify_synced = models.BooleanField(_('Is Shopify Synced'), default=False)

    class Meta:
        verbose_name = _("Inventory")
        verbose_name_plural = _("Inventories")
        unique_together = ("tag", "site")

    def __str__(self):
        return self.tag


class InventorySummary(BaseModel):
    tag = models.CharField(_('Tag'), null=False, blank=False,
                           max_length=64)

    sku = models.ForeignKey(WhSku, on_delete=models.PROTECT)

    location = models.ForeignKey(Location, on_delete=models.PROTECT)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    client = models.ForeignKey(Client, null=True, blank=False, on_delete=models.PROTECT)

    available_qty = models.PositiveIntegerField(_('Available Quantity'), default=0)

    unavailable_qty = models.PositiveIntegerField(_('Unavailable Quantity'), default=0)

    class Meta:
        verbose_name = _("Inventory Summary")
        verbose_name_plural = _("Inventory Summary")

    def __str__(self):
        return self.tag


class InventoryTransaction(BaseModel):

    # inventory = models.ForeignKey(Location, related_name='from_loc', null=True, blank=True, on_delete=models.PROTECT)

    tag = models.CharField(_('Tag'), null=False, blank=False,
                           max_length=64)

    from_loc = models.ForeignKey(Location, related_name='from_loc', null=True, blank=True, on_delete=models.PROTECT)

    to_loc = models.ForeignKey(Location, related_name='to_loc', null=True, blank=True, on_delete=models.PROTECT)

    pack_config = models.ForeignKey(PackConfig, null=True, blank=True, on_delete=models.PROTECT)

    tracking_level = models.CharField(_('Tracking Level'), null=True, blank=True, max_length=128)

    sku = models.ForeignKey(WhSku, on_delete=models.PROTECT)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    client = models.ForeignKey(Client, null=True, blank=False, on_delete=models.PROTECT)

    qty = models.PositiveIntegerField(_('Quantity'))

    uom = models.CharField(_('uom'), max_length=64, blank=True, null=True)

    container = models.ForeignKey(WmsContainer, on_delete=models.PROTECT, null=True, blank=True)

    supplier = models.ForeignKey(Supplier, null=True, blank=True, on_delete=models.PROTECT)

    status = models.CharField(_('Status'), max_length=50, null=True, blank=True)

    source = models.CharField(_('source'), max_length=50, null=True, blank=True)

    remarks = models.CharField(_('Remarks'), max_length=70, null=True, blank=True)

    reference = models.CharField(_('reference'), max_length=50, null=True, blank=True)

    reference_number = models.CharField(_('Reference Number'), max_length=50, null=True, blank=True)

    operation = models.CharField(_('Operation'), max_length=10, null=True, blank=True)

    qty_diff = models.PositiveIntegerField(_('Quantity Diff'), default=0)

    qty_left = models.PositiveIntegerField(_('Quantity Left'), default=0)

    order = models.ForeignKey(Order, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        verbose_name = _("Inventory Transaction")
        verbose_name_plural = _("Inventory Transactions")

    def __str__(self):
        return '%s | %s' % (self.tag, self.sku)
