from blowhorn.common.models import BaseModel
from django.db import models
from django.utils.translation import ugettext_lazy as _


from blowhorn.wms.submodels.location import WhSite


class ReturnOrder(BaseModel):

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    trip = models.ForeignKey('trip.Trip', on_delete=models.PROTECT, null=True, blank=True)

    amount_collected = models.DecimalField(_('Each Weight'), max_digits=10, decimal_places=2)


class ReturnOrderLine(BaseModel):

    order_line = models.ForeignKey('order.OrderLine', null=True, blank=True, on_delete=models.PROTECT)

    inventory = models.ForeignKey('wms.Inventory', null=True, blank=True, on_delete=models.PROTECT)

    return_order = models.ForeignKey(ReturnOrder, null=True, blank=True, on_delete=models.PROTECT)
