from django.utils.translation import ugettext_lazy as _
from django.db import models
from blowhorn.common.models import BaseModel
from blowhorn.oscar.core.loading import get_model

from blowhorn.order.helper import generate_file_path
from blowhorn.document.file_validator import FileValidator
from blowhorn.wms.submodels.sku import WhSku
from blowhorn.wms.submodels.location import Location
from blowhorn.wms.submodels.inbound import Supplier
from blowhorn.wms.constants import TYPE_CHOICES

Dimension = get_model('order', 'Dimension')
ProductGroup = get_model('wms', 'ProductGroup')
LocationZone = get_model('wms', 'LocationZone')


class QCAttributes(BaseModel):
    name = models.CharField(_("Name"), null=False, blank=False, default='QC', max_length=256, unique=True)

    text = models.CharField(_("Statement"), null=True, blank=True, max_length=256)

    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.PROTECT)

    # Attribute types
    type = models.CharField(
        choices=TYPE_CHOICES, default=TYPE_CHOICES[0][0],
        max_length=20, verbose_name=_("Type"))

    required = models.BooleanField(_('Required'), default=False)

    is_active = models.BooleanField(_('Is Active'), default=False)

    class Meta:
        verbose_name = _("QC Attribute")
        verbose_name_plural = _("QC Attributes")

    def __str__(self):
        return self.name


class QCConfiguration(BaseModel):
    name = models.CharField(_("Name"), null=True, blank=True, max_length=256)

    product_group = models.ForeignKey(ProductGroup, null=True, blank=True, on_delete=models.PROTECT)

    sku = models.ForeignKey(WhSku, null=True, blank=True, on_delete=models.PROTECT)

    supplier = models.ForeignKey(Supplier, null=True, blank=True, on_delete=models.PROTECT)

    zone = models.ForeignKey(LocationZone, null=True, blank=True, on_delete=models.PROTECT)

    location = models.ForeignKey(Location, null=True, blank=True, on_delete=models.PROTECT)

    is_active = models.BooleanField(default=False)

    qc_attributes = models.ManyToManyField(QCAttributes)

    qc_at_receivng = models.BooleanField(default=False)

    qc_at_picking = models.BooleanField(default=False)

    qc_at_shipping = models.BooleanField(default=False)

    sequence = models.PositiveIntegerField(_('Sequence'), null=True, blank=True)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), null=True, blank=True)

    def __str__(self):
        return self.name or str(self.id)

