from django.db import models
from django.utils.translation import ugettext_lazy as _

from blowhorn.oscar.core.loading import get_model
from blowhorn.common.models import BaseModel
from blowhorn.wms.submodels.sku import WhSku
from blowhorn.document.utils import generate_file_path
from blowhorn.document.file_validator import FileValidator
from blowhorn.wms.constants import ASN_STATUSES, ASN_NEW_STATUS

Client = get_model('wms', 'Client')
WhSite = get_model('wms', 'WhSite')
Supplier = get_model('wms', 'Supplier')


class POLineItems(BaseModel):

    purchase_order = models.ForeignKey('wms.PurchaseOrder', on_delete=models.PROTECT, null=False, blank=False)

    sku = models.ForeignKey(WhSku, null=False, blank=False, on_delete=models.PROTECT)

    status = models.CharField(_('status'), max_length=128,
                              choices=ASN_STATUSES, default=ASN_NEW_STATUS)

    qty = models.FloatField(_('Quantity'), null=False, blank=False)

    qty_received = models.FloatField(_('Quantity Received'), default=0, null=False, blank=False)

    price_per_qty = models.FloatField(_('Price Per Qty'), null=True, blank=True)

    item_desc = models.CharField(_('Item description'), null=True, blank=True, max_length=128)

    def __str__(self):
        return '%s-%s' % (self.purchase_order, self.sku)


class GRNLineItems(BaseModel):
    """
    Model to store the Received Qty
    """

    sku = models.ForeignKey(WhSku, on_delete=models.PROTECT)

    qty = models.FloatField(_('Quantity'))

    purchase_order = models.ForeignKey('wms.PurchaseOrder', on_delete=models.PROTECT)

    def __str__(self):
        return '%s-%s' % (self.purchase_order, self.sku)

class PurchaseOrder(BaseModel):
    po_number = models.CharField(_('PO Number'), null=False, blank=False,
                                 max_length=128, unique=True)

    receiving_number = models.CharField(_('Receiving Number'), null=True, blank=True, max_length=128, unique=True)

    invoice_number = models.CharField(_('Invoice Number'), null=True, blank=True,
                                      max_length=128)

    description = models.CharField(_('Description'), null=True, blank=True, max_length=128)

    supplier = models.ForeignKey(Supplier, null=True, blank=True, on_delete=models.PROTECT)

    # TODO: make site mandatory
    site = models.ForeignKey(WhSite, null=True, blank=True, on_delete=models.PROTECT)

    delivery_date = models.DateTimeField(_('Delivery Date'), null=True, blank=True)

    expiry_date = models.DateTimeField(_('Expiry Date'), null=True, blank=True)

    status = models.CharField(_('status'), null=False, blank=False, max_length=128,
                              choices=ASN_STATUSES, default=ASN_NEW_STATUS)

    # line_items = models.ForeignKey(POLineItems, on_delete=models.PROTECT)

    total_price = models.FloatField(_('Order Total Price'), null=True, blank=True)

    batch = models.CharField(_('Batch'), null=True, blank=True, max_length=128)

    serial_number = models.CharField(_('Serial Number'), null=True, blank=True, max_length=128)

    file = models.FileField(upload_to=generate_file_path,
            validators=[FileValidator()], max_length=256, null=True, blank=True)

    def __str__(self):
        return self.po_number or self.invoice_number or self.receiving_number


class ASNOrder(BaseModel):
    """
    Model to store ASN order
    """
    invoice_number = models.CharField(_('Invoice Number'), max_length=128)

    description = models.CharField(_('Description'), null=True, blank=True, max_length=128)

    supplier = models.ForeignKey(Supplier, null=True, blank=True, on_delete=models.PROTECT)

    # TODO: make site mandatory
    site = models.ForeignKey(WhSite, null=True, blank=True, on_delete=models.PROTECT)

    client = models.ForeignKey(Client, on_delete=models.PROTECT, null=True, blank=False)

    delivery_date = models.DateTimeField(_('Delivery Date'), null=True, blank=True)

    expiry_date = models.DateTimeField(_('Expiry Date'), null=True, blank=True)

    status = models.CharField(_('status'), max_length=128,
                              choices=ASN_STATUSES, default=ASN_NEW_STATUS)

    total_price = models.FloatField(_('Order Total Price'), null=True, blank=True)

    batch = models.CharField(_('Batch'), null=True, blank=True, max_length=128)

    serial_number = models.CharField(_('Serial Number'), null=True, blank=True, max_length=128)

    def __str__(self):
        return self.invoice_number


class ASNLineItems(BaseModel):
    """
    Model to store the ASN LIne items
    """

    asn_order = models.ForeignKey(ASNOrder, on_delete=models.PROTECT)

    sku = models.ForeignKey(WhSku, null=False, blank=False, on_delete=models.PROTECT)

    status = models.CharField(_('status'), max_length=128,
                              choices=ASN_STATUSES, default=ASN_NEW_STATUS)

    qty = models.FloatField(_('Quantity'), null=False, blank=False)

    qty_received = models.FloatField(_('Quantity Received'), default=0, null=False, blank=False)

    price_per_qty = models.FloatField(_('Price Per Qty'), null=True, blank=True)

    item_desc = models.CharField(_('Item description'), null=True, blank=True, max_length=128)

    tracking_level = models.CharField(_('Tracking Level'), null=True, blank=True, max_length=128)


    def __str__(self):
        return '%s-%s' % (self.asn_order, self.sku)

