from blowhorn.common.models import BaseModel
from django.utils.translation import ugettext_lazy as _
from django.db import models

from blowhorn.document.file_validator import FileValidator
from blowhorn.document.utils import generate_file_path
from blowhorn.wms.constants import STOCK_CHECK_STATUSES, NEW
from blowhorn.wms.models import ProductGroup
from blowhorn.wms.submodels.location import WhSite
from blowhorn.wms.submodels.sku import WhSku
from django.contrib.gis.db import models as gismodel


class StockCheckBatch(BaseModel):
    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    end_time = models.DateTimeField(_('End Time'), null=True, blank=True)

    file = models.FileField(upload_to=generate_file_path, validators=[FileValidator(allowed_extensions=["pdf"])],
                            max_length=256, blank=True, null=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'StockCheck Batch'
        verbose_name_plural = 'StockCheck Batch'


class StockCheck(BaseModel):

    batch = models.ForeignKey(StockCheckBatch, on_delete=models.PROTECT, null=True, blank=True)

    status = models.CharField(choices=STOCK_CHECK_STATUSES, default=NEW, max_length=20)

    sku = models.ForeignKey(WhSku, null=True, blank=True, on_delete=models.PROTECT)

    product_group = models.ForeignKey(ProductGroup, null=True, blank=True, on_delete=models.PROTECT)

    current_location = gismodel.PointField(null=True, blank=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        unique_together = ("batch", "sku", "product_group")
        verbose_name = 'Stock Check'
        verbose_name_plural = 'Stock Check'


class GainLossReason(BaseModel):
    name = models.CharField(_('Reason'), max_length=60)
    site = models.ForeignKey(WhSite, on_delete=models.PROTECT, null=True, blank=True)
    description = models.CharField(_('Description'), max_length=250, null=True, blank=True)
    is_active = models.BooleanField(_('Is Active'), default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Stock Gain Loss Reason")
        verbose_name_plural = _("Stock Gain Loss Reasons")
        unique_together = ("name", "site")


class StockCheckLines(models.Model):

    stock_check = models.ForeignKey(StockCheck, on_delete=models.PROTECT, null=True, blank=True)

    sku = models.ForeignKey(WhSku, on_delete=models.PROTECT, null=True, blank=True)

    tag = models.CharField(_('Tag'), max_length=64, null=True, blank=True)

    curr_qty = models.PositiveIntegerField(_('Current Quantity'), default=0)

    prev_qty = models.PositiveIntegerField(_('Previous Quantity'), default=0)

    reason = models.ForeignKey(GainLossReason, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Stock Check Line")
        verbose_name_plural = _("Stock Check Lines")
