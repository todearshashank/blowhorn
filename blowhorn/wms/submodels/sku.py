from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.db.models import Q
from blowhorn.oscar.core.loading import get_model
from blowhorn.common.middleware import current_request
from blowhorn.common.models import BaseModel
from blowhorn.wms.submodels.inbound import Supplier
from blowhorn.document.utils import generate_file_path
from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED, DISCOUNT_TYPES
from django.db.models.constraints import UniqueConstraint


Dimension = get_model('order', 'Dimension')
Client = get_model('wms', 'Client')
ProductGroup = get_model('wms', 'ProductGroup')
WhSite = get_model('wms', 'WhSite')
Customer = get_model('customer', 'Customer')


class TrackingLevel(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=64)

    level_one_name = models.CharField(_('Level 1 name'), null=False, blank=False,
                                      max_length=64)

    level_two_name = models.CharField(_('Level 2 name'), null=True, blank=True,
                                      max_length=64)

    level_three_name = models.CharField(_('Level 3 name'), null=True, blank=True,
                                        max_length=64)

    ratio_level_two_one = models.PositiveIntegerField(_('Ration Level 2 to 1'), null=True, blank=True)

    ratio_level_three_two = models.PositiveIntegerField(_('Ration Level 3 to 2'), null=True, blank=True)

    customer = models.ForeignKey('customer.Customer', on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('customer', 'name')


class PackConfig(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=64)

    tracking_level = models.ForeignKey(TrackingLevel, on_delete=models.PROTECT)

    each_volume = models.PositiveIntegerField(_('Each Volume'), null=True, blank=True)

    each_weight = models.PositiveIntegerField(_('Each Weight'), null=True, blank=True)

    tag_volume = models.PositiveIntegerField(_('Tag Volume'), null=True, blank=True)

    use_each_volume = models.BooleanField(_('Use each volume'), default=True)

    use_tag_volume = models.BooleanField(_('Use tag volume'), default=False)

    each_dimension = models.ForeignKey(Dimension, null=True, blank=True, on_delete=models.PROTECT)

    uom = models.CharField(_('UOM'), null=True, blank=True, max_length=100)

    customer = models.ForeignKey('customer.Customer', on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('customer', 'name')


class WhSku(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=255)

    ean = models.CharField(_('EAN Code'), null=True, blank=True, max_length=256)

    upc = models.CharField(_('UPC Code'), null=True, blank=True, max_length=256)

    description = models.CharField(_('Description'), null=True, blank=True, max_length=256)

    product_group = models.ForeignKey(ProductGroup, null=True, blank=True, on_delete=models.PROTECT)

    pack_config = models.ForeignKey(PackConfig, null=True, blank=True, on_delete=models.PROTECT)

    # site = models.ForeignKey(WhSite, on_delete=models.PROTECT, null=True, blank=True)

    supplier = models.ForeignKey(Supplier, null=True, blank=False, on_delete=models.PROTECT)

    client = models.ForeignKey(Client, null=True, blank=False, on_delete=models.PROTECT)

    # customer = models.ForeignKey(Customer, null=True, blank=True,
    #                              on_delete=models.PROTECT)

    each_weight = models.DecimalField(_('Each Weight'), max_digits=10, decimal_places=2,
                                      blank=True, null=True)

    each_dimension = models.ForeignKey(Dimension, null=True, blank=True, on_delete=models.PROTECT)

    each_volume = models.DecimalField(_('Each Volume'), max_digits=10, decimal_places=2,
                                      blank=True, null=True)

    has_expiry = models.BooleanField(_('Has Expiry'), default=False)

    shelf_life = models.DecimalField(_('Shelf Life'), max_digits=10, decimal_places=2, blank=True, null=True)

    shelf_life_uom = models.CharField(_('Shelf life in'), null=True, blank=True,
                                      max_length=64,
                                      choices=[('Hours', 'Hours'), ('Days', 'Days'), ('Months', 'Months'),
                                               ('Years', 'Years')])

    requires_putaway = models.BooleanField(_('Requires Putaway'), default=True)

    uom = models.CharField(_('UOM '), null=False, blank=False, max_length=64, default='KGs')

    file = models.FileField(upload_to=generate_file_path, max_length=256, null=True, blank=True)

    mrp = models.DecimalField(_("MRP"), decimal_places=2, max_digits=12, null=True, blank=True, default=0,
                              help_text=_('Maximum Retail Price'))
    discounts = models.DecimalField(_("Discounts"), decimal_places=2, max_digits=12, null=True, blank=True, default=0)
    discount_type = models.CharField(_('Discount Type'), max_length=128, default=DISCOUNT_TYPE_FIXED,
                                     choices=DISCOUNT_TYPES, null=True, blank=True)

    shopify_product_id = models.CharField(_('Shopify Product ID'), max_length=256, null=True, blank=True)

    shopify_variant_id = models.CharField(_('Shopify Variant ID'), max_length=256, null=True, blank=True)

    shopify_description = models.TextField(_('Shopify Description'), null=True, blank=True)

    shopify_inventory_id = models.CharField(_('Shopify Inventory ID'), max_length=256, null=True, blank=True)

    is_kit_sku = models.BooleanField(_('Is Kit Sku'), default=False)

    requires_shipping = models.BooleanField(_('Requires Shipping'), default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        _request = current_request()
        log_list = []
        current_user = _request.user if _request else ''
        if current_user and not current_user.is_authenticated:
            current_user = self.client.customer.user
        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')
                old_value = field.value_from_object(old)
                current_value = field.value_from_object(self)
                if old_value != current_value:
                    log_list.append(WhSkuChangeLog(field=field_name, old_value=str(old_value),
                                                 new_value=current_value, modified_by=current_user,
                                                 whsku=old))
            if log_list:
                WhSkuChangeLog.objects.bulk_create(log_list)
        return super().save(*args, **kwargs)

    class Meta:
        constraints = [
            UniqueConstraint (fields=["client", "ean"], condition=Q(ean__isnull=False) , name='unique_client_without_ean'),
            UniqueConstraint (fields=["client", "upc"], condition=Q(upc__isnull=False) , name='unique_client_without_upc'),
        ]
        unique_together = ("client", "name")


class WhSkuChangeLog(BaseModel):
    whsku = models.ForeignKey(WhSku, on_delete=models.PROTECT)
    field = models.CharField(max_length=50, null=True, blank=True)
    old_value = models.CharField(max_length=250, null=True, blank=True)
    new_value = models.CharField(max_length=250, null=True, blank=True)

    class Meta:
        verbose_name = _('WH SKU Change Log')
        verbose_name_plural = _('WH SKU Change Log')

    def __str__(self):
        return '%s' % self.whsku


class WhKitSku(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=255)

    description = models.CharField(_('Description'), null=True, blank=True, max_length=256)

    total_item_price = models.DecimalField(_('Total price for item'),
                                           max_digits=10,
                                           decimal_places=2, default=0)

    quantity = models.IntegerField(_('Sku Quantity'), default=0)

    def __str__(self):
        return '%s' % self.name

class KitSkuMapping(BaseModel):
    parent_sku = models.ForeignKey(WhSku, on_delete=models.PROTECT, related_name='parent')

    child_sku = models.ForeignKey(WhSku, on_delete=models.PROTECT, related_name='child')

    quantity = models.IntegerField(_('Child Sku Quantity'), default=0)

    def __str__(self):
        return '%s_%s_%s' % (self.parent_sku, self.child_sku, self.quantity)
    class Meta:
        verbose_name = _("Kit SKU Mapping")
        verbose_name_plural = _("Kit SKU Mappings")
