from django.db import models

from django.utils.translation import ugettext_lazy as _

from blowhorn.oscar.core.loading import get_model
from blowhorn.common.models import BaseModel
from blowhorn.wms.constants import PROCESSES, ALLOCATION_ALGORITHMS, ALLOCATION_FIFO_ALGORITHM, \
    QUERY_BUILDER_FILTER_ON_FIELD_VALUES, QUERY_BUILDER_FROM_MODEL_VALUES, QUERY_BUILDER_OPERATOR_VALUES, \
    QUERY_BUILDER_FIELD_FILTER_VALUES, QUERY_BUILDER_ORDER_BY_VALUES, QUERY_BUILDER_WHERE_CONDITION_VALUES, \
    QUERY_BUILDER_ORDERING_VALUES, QUERY_BUILDER_FUNCTIONS_VALUES

WhSite = get_model('wms', 'WhSite')


class QueryBuilder(BaseModel):

    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=128, unique=True)

    process = models.CharField(_('Process'), null=False, blank=False, max_length=128,
                               choices=PROCESSES)

    site = models.ForeignKey(WhSite, null=True, blank=True, on_delete=models.PROTECT)

    sequence = models.PositiveIntegerField(_('Sequence'))

    is_active = models.BooleanField(_('Is Active'), default=False)

    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        unique_together = ('process', 'sequence')

    def __str__(self):
        return self.name


class QueryBuilderLines(models.Model):

    query_builder = models.ForeignKey(QueryBuilder, on_delete=models.PROTECT)

    sequence = models.PositiveIntegerField(_('Sequence'))

    from_model = models.CharField(_('From'), null=False, blank=False,
                                  max_length=128,
                                  choices=QUERY_BUILDER_FROM_MODEL_VALUES)

    where_condition = models.CharField(_('Where Condition'), null=True, blank=True,
                                       max_length=128, choices=QUERY_BUILDER_WHERE_CONDITION_VALUES)

    open_parenthesis = models.BooleanField(_('Open Parenthesis'), default=False)

    filter = models.CharField(_('Filter On Field'), null=True, blank=True,
                              max_length=128,
                              choices=QUERY_BUILDER_FILTER_ON_FIELD_VALUES)

    field_filter = models.CharField(_('Value Of Field'), null=True, blank=True, max_length=128,
                                    choices=QUERY_BUILDER_FIELD_FILTER_VALUES)

    operator = models.CharField(_('Operator'), null=True, blank=True,
                                max_length=128,
                                choices=QUERY_BUILDER_OPERATOR_VALUES)

    field_value = models.CharField(_('Filter Value'), null=True, blank=True,
                                   max_length=128)

    close_parenthesis = models.BooleanField(_('Close Parenthesis'), default=False)

    group_by = models.CharField(_('Group By'), null=True, blank=True,
                                max_length=128)

    having_clause = models.CharField(_('Having'), null=True, blank=True,
                                     max_length=128)

    order_by = models.CharField(_('Order By'), null=True, blank=True,
                                max_length=128,
                                choices=QUERY_BUILDER_ORDER_BY_VALUES)

    ordering = models.CharField(_('Ordering'), null=True, blank=True, max_length=128,
                                choices=QUERY_BUILDER_ORDERING_VALUES)

    functions = models.CharField(_('Functions'), null=True, blank=True,
                                 max_length=128, choices=QUERY_BUILDER_FUNCTIONS_VALUES)
