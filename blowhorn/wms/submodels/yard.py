from django.utils.translation import ugettext_lazy as _
from django.db import models

from blowhorn.address.models import Hub
from blowhorn.oscar.core.loading import get_model
from blowhorn.common.models import BaseModel


Driver = get_model('driver', 'Driver')
Client = get_model('wms', 'Client')
WhSite = get_model('wms', 'WhSite')
Customer = get_model('customer', 'Customer')


class Yard(BaseModel):
    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    hub = models.ForeignKey(Hub, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.driver) + '|' + str(self.hub)


class YardActivity(BaseModel):
    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    hub = models.ForeignKey(Hub, on_delete=models.PROTECT)

    check_in = models.DateTimeField(null=True, blank=True)

    check_out = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.driver) + '|' + str(self.hub)

