import json
from django.utils.translation import ugettext_lazy as _
from django.db import models
from blowhorn.oscar.core.loading import get_model
from django.contrib.postgres.fields import ArrayField
from django.core.validators import RegexValidator
from django.contrib.postgres.fields import JSONField

from blowhorn.common.models import BaseModel
from blowhorn.address.models import GenericAddress
from blowhorn.wms.constants import LOC_STATUSES, LOC_UNLOCKED

ProductGroup = get_model('wms', 'ProductGroup')
Dimension = get_model('order', 'Dimension')
City = get_model('address', 'City')
Customer = get_model('customer', 'Customer')
User = get_model('users', 'User')
CustomerHubContact = get_model('address', 'CustomerHubContact')
Hub = get_model('address', 'Hub')


class WhSite(models.Model):
    """
    Site Model
    """

    name = models.CharField(
        _("Site Name"), max_length=255, blank=False,
        help_text=_("Site Name"))

    # The city this site is part of
    city = models.ForeignKey(City, null=True, on_delete=models.PROTECT)

    hub = models.ForeignKey(Hub, null=False, on_delete=models.PROTECT)

    address = models.OneToOneField(GenericAddress,
                                   related_name='site_address', null=True, blank=True,
                                   on_delete=models.CASCADE)

    shipping_address = models.ForeignKey('order.ShippingAddress',
                                         null=True,
                                         verbose_name='Site address as shipping address',
                                         on_delete=models.PROTECT)

    pincodes = ArrayField(models.PositiveIntegerField(
        validators=[RegexValidator(r'^\d{4,6}$')]), blank=True, null=True)

    store_code = models.CharField(_('Store Code'), max_length=50, blank=True,
                                  null=True)

    qr_code = models.CharField(
        _("QR Code"), max_length=25, blank=False, default='CUSTOMERQRCODE',
        help_text=_("QR Code has to be given to Hub Supervisor for drivers to start the trip"))

    # associates = models.ManyToManyField(User, related_name='site_associates',
    #                                     blank=True,
    #                                     limit_choices_to={'is_staff': True})
    #
    # supervisors = models.ManyToManyField(User, related_name='site_supervisors',
    #                                      blank=True,
    #                                      limit_choices_to={'is_staff': True})
    #
    # customer_hub_contact = models.ManyToManyField(CustomerHubContact,
    #                                               related_name='customer_site_contact',
    #                                               blank=True)

    client_billing_code = models.IntegerField(_('Client billing code'), blank=True, null=True,
                                              help_text=_("Client Billing Code"))

    shipping_code = models.IntegerField(_('Shipping code'), blank=True, null=True,
                                        help_text=_("Ship To Code"))

    # coverage = models.PolygonField(
    #     help_text=_("The coverage area for the slot"), null=True,
    #     blank=True
    # )

    length = models.FloatField(_('Length'), null=True, blank=True)

    breadth = models.FloatField(_('Breadth'), null=True, blank=True)

    height = models.FloatField(_('Height'), null=True, blank=True)

    uom = models.CharField(_('Unit Of Measurement'), max_length=50, null=True, blank=True)

    is_wms_site = models.BooleanField(_('Is WMS site'), default=False)

    # client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.PROTECT)

    # For GeoManager Queries
    # objects = HubManager()

    # TODO: Maybe check if the geopoint is in the coverage area of the city
    def __str__(self):
        return self.name

    __original_pincodes = None

    def __init__(self, *args, **kwargs):
        super(WhSite, self).__init__(*args, **kwargs)
        self.__original_pincodes = self.pincodes

    class Meta:
        verbose_name = _("WMS Site")
        verbose_name_plural = _("WMS Sites")


class Client(BaseModel):
    # TODO check if site is needed here
    name = models.CharField(_('Client Name'), null=False, blank=False,
                            max_length=64)

    description = models.CharField(_('Description'), null=True, blank=True,
                                   max_length=256)

    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ("customer", "name")


class StorageType(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=64, unique=True)

    description = models.CharField(_('Description'), null=True, blank=True,
                                   max_length=256)

    customer = models.ForeignKey(Customer, on_delete=models.PROTECT,
                                 null=True, blank=True)

    def __str__(self):
        return self.name


class LocationType(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=64, unique=True)

    description = models.CharField(_('Description'), null=True, blank=True,
                                   max_length=256)

    loc_prefix_code = models.CharField(_('Location Prefix code'), null=True, blank=True,
                                       max_length=64)

    customer = models.ForeignKey(Customer, on_delete=models.PROTECT,
                                 null=True, blank=True)

    # storage_type = models.ForeignKey(StorageType, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class LocationZone(BaseModel):
    name = models.CharField(_('Zone Name'), null=False, blank=False,
                            max_length=64)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    sequence = models.PositiveIntegerField(_('Sequence'), null=True, blank=True)

    for_putaway = models.BooleanField(_('Use For Putaway'), default=False)

    status = models.CharField(_('Status'), choices=[('UnLocked', 'UnLocked'), ('Locked', 'Locked')], null=False,
                              blank=False, max_length=64)

    loc_type = models.ForeignKey(LocationType, on_delete=models.PROTECT,
                                 null=True, blank=True)

    storage_type = models.ForeignKey(StorageType, null=True, blank=True,
                                     on_delete=models.PROTECT)

    dimension = models.ForeignKey(Dimension, null=True, blank=True, on_delete=models.PROTECT)

    product_group = models.ForeignKey(ProductGroup, null=True, blank=True, on_delete=models.PROTECT)

    is_receiving = models.BooleanField(_('Use For Receiving'), default=False)

    is_putaway = models.BooleanField(_('Use For Putaway'), default=False)

    is_packing = models.BooleanField(_('Use For Packing'), default=False)

    is_shipping = models.BooleanField(_('Use For Shipping'), default=False)

    is_deleted = models.BooleanField(_('Is Deleted'), default=False)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ("name", "site")


class Rack(BaseModel):
    name = models.CharField(_('Rack Name'), null=False, blank=False,
                            max_length=64)

    description = models.CharField(_('Description'), null=True, blank=True,
                                   max_length=256)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    zone = models.ForeignKey(LocationZone, on_delete=models.PROTECT, related_name='rack_zone')

    sequence = models.PositiveIntegerField(_('Sequence'), null=True, blank=True)

    loc_type = models.ForeignKey(LocationType, on_delete=models.PROTECT,
                                 null=True, blank=True)

    storage_type = models.ForeignKey(StorageType, null=True, blank=True,
                                     on_delete=models.PROTECT)

    weight = models.FloatField(_('Weight'), null=True, blank=True)

    volume = models.FloatField(_('Volume'), null=True, blank=True)

    current_weight = models.FloatField(_('Current Weight'), default=0)

    current_volume = models.FloatField(_('Current Volume'), default=0)

    length = models.FloatField(_('Length'), null=True, blank=True)

    breadth = models.FloatField(_('Breadth'), null=True, blank=True)

    height = models.FloatField(_('Height'), null=True, blank=True)

    uom = models.CharField(_('Unit Of Measurement'), max_length=50, null=True, blank=True)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), null=True, blank=True)

    class Meta:
        unique_together = ("name", "site")


class Location(BaseModel):
    name = models.CharField(_('Location Name'), null=False, blank=False,
                            max_length=64)

    client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.PROTECT)

    loc_type = models.ForeignKey(LocationType, on_delete=models.PROTECT, null=True, blank=True)

    storage_type = models.ForeignKey(StorageType, null=True, blank=True,
                                     on_delete=models.PROTECT)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    status = models.CharField(_('Status'), choices=LOC_STATUSES, null=False,
                              blank=False, max_length=64, default=LOC_UNLOCKED)

    zone = models.ForeignKey(LocationZone, on_delete=models.PROTECT, related_name='zone_locations')

    rack = models.ForeignKey(Rack, null=True, blank=True, on_delete=models.PROTECT)

    front_rack_side = models.BooleanField(default=True)

    dimensions = models.ForeignKey(Dimension, null=True, blank=True, on_delete=models.PROTECT)

    weight = models.FloatField(_('Weight'), null=True, blank=True)

    volume = models.FloatField(_('Volume'), null=True, blank=True)

    current_weight =  models.FloatField(_('Current Weight'), default=0)

    current_volume = models.FloatField(_('Current Volume'), default=0)

    allocated_volume = models.FloatField(_('Allocated Volume'), default=0)

    allocated_weight = models.FloatField(_('Allocated Weight'), default=0)

    sequence = models.PositiveIntegerField(_('Sequence'), null=True, blank=True)

    product_group = models.ForeignKey(ProductGroup, null=True, blank=True, on_delete=models.PROTECT)

    is_receiving = models.BooleanField(_('Use For Receiving'), default=False)

    is_putaway = models.BooleanField(_('Use For Putaway'), default=False)

    is_packing = models.BooleanField(_('Use For Packing'), default=False)

    is_shipping = models.BooleanField(_('Use For Shipping'), default=False)

    allow_sku_mixing = models.BooleanField(_('Allow SKU mixing'), default=True)

    shopify_location_id = models.CharField(_('Shopify Location'), max_length=255,
                            null=True, blank=True)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), null=True, blank=True)

    @property
    def remaining_volume(self):
        if self.volume:
            return self.volume - self.current_volume
        return None

    class Meta:
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")
        unique_together = ("name", "site")

    def __str__(self):
        return self.name


class LocationZoneHistory(BaseModel):

    CATEGORY_DATA_DELETE_REQUEST = 'data_delete_request'

    zone = models.CharField(_('Zone'), max_length=255)
    number_of_zones = models.IntegerField(_('No. of records'), help_text=_('No. of records affected'),
                                          default=0)
    category = models.CharField(max_length=255, null=True, blank=True)
    log_dump = JSONField(_('Log'), help_text=_('Log'))

    class Meta:
        verbose_name = _('Zone Data Log')
        verbose_name_plural = _('Zone Data Logs')

    @property
    def log_dump_json(self):
        if isinstance(self.log_dump, str):
            self.log_dump = json.loads(self.log_dump)
            if isinstance(self.log_dump, str):
                return json.loads(self.log_dump)
        return self.log_dump
