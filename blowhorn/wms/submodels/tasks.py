from django.utils.translation import ugettext_lazy as _
from django.db import models
from blowhorn.common.models import BaseModel

from blowhorn.oscar.core.loading import get_model
from blowhorn.users.models import User
from blowhorn.wms.submodels.sku import WhSku
from blowhorn.wms.submodels.location import Location, Client
from blowhorn.wms.submodels.qualitycheck import QCConfiguration
from blowhorn.wms.constants import WH_TASK_STATUSES, WH_TASK_NEW

WhSite = get_model('wms', 'WhSite')
ASNLineItems = get_model('wms', 'ASNLineItems')
POLineItems = get_model('wms', 'POLineItems')
Supplier = get_model('wms', 'Supplier')


class TaskType(BaseModel):
    name = models.CharField(_('Name'), null=False, blank=False,
                            max_length=64, unique=True)

    description = models.CharField(_('Description'), null=True, blank=True,
                                   max_length=256)

    def __str__(self):
        return self.name


class WhTask(BaseModel):

    task_type = models.ForeignKey(TaskType, on_delete=models.PROTECT)

    order = models.ForeignKey('order.Order', null=True, blank=True, on_delete=models.PROTECT)

    order_line = models.ForeignKey('order.OrderLine', null=True, blank=True, on_delete=models.PROTECT)

    asn_line = models.ForeignKey(ASNLineItems, null=True, blank=True, on_delete=models.PROTECT)

    po_line = models.ForeignKey(POLineItems, null=True, blank=True, on_delete=models.PROTECT)

    from_loc = models.ForeignKey(Location, related_name='whtask_from_loc', null=True, blank=True, on_delete=models.PROTECT)

    to_loc = models.ForeignKey(Location, related_name='whtask_to_loc', null=True, blank=True, on_delete=models.PROTECT)

    sku = models.ForeignKey(WhSku, on_delete=models.PROTECT, null=True, blank=True)

    client = models.ForeignKey(Client, null=True, blank=False, on_delete=models.PROTECT)

    site = models.ForeignKey(WhSite, on_delete=models.PROTECT)

    qty = models.PositiveIntegerField(_('Quantity'))

    tag = models.CharField(_('Name'), null=False, blank=False,
                            max_length=64)

    status = models.CharField(_('Status'),
                              choices=WH_TASK_STATUSES,
                              default=WH_TASK_NEW,
                              null=False, blank=True, max_length=64)

    operator = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    priority = models.PositiveIntegerField(_('Priority'), default=1)

    qc_config = models.ForeignKey(QCConfiguration, null=True, blank=True, on_delete=models.PROTECT)

    supplier = models.ForeignKey(Supplier, null=True, blank=True, on_delete=models.PROTECT)

    header_task = models.ForeignKey('self', null=True, blank=True, on_delete=models.PROTECT)

    remarks = models.CharField(_('Remarks'), null=True, blank=True,
                               max_length=256)

    def __str__(self):
        return '%s - %s' % (self.task_type.name, self.tag)

