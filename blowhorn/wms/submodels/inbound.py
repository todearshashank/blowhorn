from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator, MinLengthValidator

from blowhorn.customer.models import Customer
from blowhorn.order.helper import generate_file_path
from blowhorn.common.models import BaseModel
from phonenumber_field.modelfields import PhoneNumberField
from blowhorn.vehicle.models import VendorAddress

SUPPLIER_STATUSES = [
    ('onboarding', 'Onboarding'),
    ('active', 'Active'),
    ('inactive', 'Inactive')
]


class Supplier(BaseModel):
    name = models.CharField(
        _('Supplier Name'), blank=True, max_length=255, db_index=True)

    phone_number = PhoneNumberField(
        _("Mobile Number"), null=True, blank=True,
        help_text=_("Fleet Owner's primary mobile number e.g. +91{10 digit mobile "
                    "number}"))

    photo = models.ImageField(upload_to=generate_file_path,
                              null=True, blank=True, max_length=500)

    is_address_same = models.BooleanField(
        _("Permanent address is same as current address"),
        default=False, db_index=True)

    cin = models.CharField(_('CIN'), max_length=21, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{1}\d{5}[A-Za-z]{2}\d{4}[A-Za-z]{3}\d{6}$',
            message=_('First Alphabet, next 5 digits,'
                      'next 2 Alphabet, next 4 digits,'
                      'next 3 Alphabet, next 6 digits,'
                      'fixed length is 21')),
            MinLengthValidator(21)])

    pan = models.CharField(_('PAN'), max_length=10, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{5}\d{4}[A-Za-z]{1}$',
            message=_('First 5 digits, next 4 digits,'
                      'last Alphabet, fixed length is 10')),
            MinLengthValidator(10)])

    tan = models.CharField(_('TAN'), max_length=10, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{4}\d{5}[A-Za-z]{1}$',
            message=_('First 4 digits Alphabet,'
                      'next 5 digits,'
                      'last Alphabet, fixed length is 10')),
            MinLengthValidator(10)])

    current_address = models.ForeignKey(
        VendorAddress,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='supplier_current_address')

    invoice_address = models.ForeignKey(
        VendorAddress,
        null=True,
        blank=True,
        on_delete=models.PROTECT)

    bank_account = models.ForeignKey(
        'driver.BankAccount',
        null=True,
        blank=True,
        verbose_name='Bank Details', on_delete=models.PROTECT)

    state = models.ForeignKey('address.State', null=True, blank=True, on_delete=models.PROTECT)

    gstin = models.CharField(_('GSTIN'), max_length=15, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^\d{2}[A-Za-z]{5}\d{4}[A-Za-z]{1}\d{1}[A-Za-z]{1}\d{1}$',
            message=_('First 2 digits, next 5 Alphabets, next 4 digits,'
                      'one Alphabet, 1 digit, 1 Alphabet, 1 digit, fixed length is 15')),
            MinLengthValidator(15)])

    status = models.CharField(max_length=100, choices=SUPPLIER_STATUSES, default='onboarding')

    reason_for_inactive = models.CharField(_('Reason'), max_length=75, null=True, blank=True,
                                           help_text=_(
                                               "Applicable only for Inactive/Blacklisted status"))

    remarks = models.CharField(
        _("Remarks"), max_length=255, null=True, blank=True,
        help_text=_("Applicable only for Inactive/Blacklisted status"))

    customer = models.ForeignKey('customer.Customer', null=True, blank=True, on_delete=models.PROTECT)

    tos_response_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ("customer", "name")
