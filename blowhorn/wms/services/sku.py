from django.db import transaction

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.submodels.location import Location, Client, LocationType, \
    LocationZone
from blowhorn.wms.submodels.sku import PackConfig, WhSku, TrackingLevel

Dimension = get_model('order', 'Dimension')
Hub = get_model('address', 'Hub')


class SkuGenerator:
    def __init__(self, **kwargs):
        self.client_name = kwargs.get('client')
        self.sku_prefix = kwargs.get('sku_prefix', 'SKU')
        self.sku_count = kwargs.get('sku_count', 100)
        self.site = kwargs.get('site')
        self.dimension = kwargs.get('dimension')
        self.weight = kwargs.get('weight', 1000)
        self.pack_config = kwargs.get('pack_config')
        self.shelf_life = 10
        self.shelf_life_uom = 'Months'

    def sku_generator(self):
        if self.client_name:
            self.client = Client.objects.filter(name=self.client_name).first()
            if not self.client:
                return False
        else:
            self.client, _ = Client.objects.get_or_create(name='DEFAULT')

        if self.pack_config:
            self.pack_config = PackConfig.objects.filter(name=self.pack_config).first()
            if not self.pack_config:
                return False
        else:
            tracking_level, _ = TrackingLevel.objects.get_or_create(name='TRKL001', level_one_name='EACH',
                                                                    level_two_name='BOX', level_three_name='PALLET',
                                                                    ratio_level_two_one=10, ratio_level_three_two=10)

            self.pack_config, _ = PackConfig.objects.get_or_create(name='DEFAULT', tracking_level=tracking_level)

        if self.site:
            self.site = Hub.objects.filter(name=self.site).first()
            if not self.site:
                return False

        dimensions = Dimension.objects.filter(length=3, breadth=3, height=3, uom='Ft').first()
        if not dimensions:
            dimensions = Dimension.objects.create(length=3, breadth=3, height=3, uom='Ft')

        sku_list = []
        with transaction.atomic():
            for sku_seq in range(1, self.sku_count+1):
                sku_name = self.sku_prefix + str(sku_seq).zfill(3)
                sku_list.append(WhSku(
                    name=sku_name,
                    client=self.client,
                    pack_config=self.pack_config,
                    site=self.site,
                    each_dimension=dimensions,
                    each_weight=self.weight,
                    has_expiry=True,
                    shelf_life=12,
                    shelf_life_uom='Months'
                    )
                )
            WhSku.objects.bulk_create(sku_list)
