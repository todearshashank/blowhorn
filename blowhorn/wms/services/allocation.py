from django.db.models import Q, F
from django.db import transaction

from config.settings import status_pipelines as StatusPipeline
from blowhorn.wms.models import Location, Inventory
from blowhorn.wms.models import QueryBuilder, WhTask, TaskType
from blowhorn.wms.constants import PROCESS_ALLOCATION, PROCESS_PUTAWAY, PICK_TASK, INV_UNLOCKED, \
    ALLOCATION_FIFO_ALGORITHM, ALLOCATION_LIFO_ALGORITHM

INVENTORY_FIELD_MAPPING = {
        'Location__name': 'location__name',
        'Location__status': 'location__status',
        'zone__name': 'location__zone__name',
        'client__name': 'client__name',
        'whsku__name': 'sku__name',
        'loc_type__name': 'location__loc_type__name',
        'storage_type__name': 'location__storage_type__name',
        'Location__is_receiving': 'location__is_receiving',
        'Location__is_putaway': 'location__is_putaway',
        'Location__is_shipping': 'location__is_shipping',
        'Location__remaining_volume': 'location__remaining_volume',
        'Location__volume': 'location__volume',
        'Inventory__qty': 'qty',
        'Inventory__expiry_date': 'expiry_date',
        'Inventory__mfg_date': 'mfg_date'
}


class OrderAllocation(object):

    def __init__(self, obj, order_line, **kwargs):
        self.allocation_config = obj
        self.query_builder = obj.query_builder
        self.order = order_line.order
        self.order_line = order_line
        if self.query_builder.process == PROCESS_PUTAWAY:
            self.primary_model = Location
        elif self.query_builder.process == PROCESS_ALLOCATION:
            self.primary_model = Inventory
        self.sku = order_line.wh_sku
        self.client = order_line.wh_sku.client
        # TODO : remove default and put validation
        self.sku_qty = self.order_line.quantity - self.order_line.qty_allocated

    def create_query_from_builder(self):
        _params, filter_ = None, {}
        or_with_parenthesis = False
        and_with_parenthesis = False
        order_by_clause = []

        _params = Q(sku=self.sku, status=INV_UNLOCKED, qty__gt=0, site=self.allocation_config.site)

        for line in self.query_builder.querybuilderlines_set.all().order_by('sequence'):
            from_model = line.from_model
            if from_model == 'WhSku' and line.filter == 'whsku__name':
                # check if sku for putaway matches the given SKU as this algo is only for given SKU
                if self.sku != line.field_value:
                    return None

            if line.order_by:
                orderby_model = line.order_by.split('__')[0]
                order_by_column = line.order_by.split('__')[1]
                if line.order_by == 'Inventory__remaining_qty':
                    order_by_column = F('qty') - self.sku_qty
                # if eval(line.order_by) == orderby_model:
                #     query_str = line.filter.split('__')[1]
                order_by_column = order_by_column or 'id'
                order_by_column = '-' + order_by_column if line.ordering == 'desc' else order_by_column
                order_by_clause.append(order_by_column)

            if line.open_parenthesis and filter_:
                if _params:
                    _params = _params & Q(**filter_)
                    filter_ = {}
                else:
                    _params = Q(**filter_)
                filter_ = {}

            if line.open_parenthesis and line.where_condition == 'OR':
                or_with_parenthesis = True
            elif line.open_parenthesis and line.operator == 'AND':
                and_with_parenthesis = True

            query_str = None
            if line.filter:
                #     if eval(from_model) == self.primary_model:
                #         query_str = line.filter.split('__')[1]
                #         if line.operator != '=':
                #             query_str += line.operator
                #         # query_str += "'" + line.field_value + "'"
                #     else:
                query_str = INVENTORY_FIELD_MAPPING[line.filter]
                if line.operator != '=':
                    query_str += line.operator
                    # query_str += "'" + line.field_value + "'"

            if line.field_value:
                filter_[query_str] = line.field_value
            elif line.field_filter:
                if line.field_filter == 'Inventory__volume':
                    filter_[query_str] = self.inv_vol
                elif line.field_filter == 'whsku__qty':
                    filter_[query_str] = self.sku_qty

            if line.where_condition == 'OR' and not or_with_parenthesis:
                if _params:
                    _params = _params | Q(**filter_)
                    filter_ = None
                else:
                    _params = Q(**filter_)
                filter_ = {}

            if line.close_parenthesis and filter_ and or_with_parenthesis:
                if _params:
                    _params = _params | Q(**filter_)
                else:
                    _params = Q(**filter_)
                or_with_parenthesis = False
                filter_ = {}

            if filter_:
                if _params:
                    _params = _params & Q(**filter_)
                else:
                    _params = Q(**filter_)
                filter_ = {}

        # return filter_
        print(_params)
        if _params:
            qs = self.primary_model.objects.filter(_params).order_by(*order_by_clause)
        else:
            qs = self.primary_model.objects.order_by(*order_by_clause)
        print(str(qs.query))
        return qs

    def get_inventory(self):
        inventory_qs = self.create_query_from_builder()
        print('inventory_qs - ', inventory_qs)
        if inventory_qs:
            return inventory_qs.first()
        return None


class AllocateOrder(object):

    def __init__(self, order):
        self.order = order
        self.order_line = None
        self.error_msg = None

    def _run_allocation_algo(self):

        allocation_rules = QueryBuilder.objects.filter(
            process=PROCESS_ALLOCATION, is_active=True
        ).order_by('sequence')
        inventory = None

        for allocation_rule in allocation_rules:
            inventory = None
            inventory = OrderAllocation(allocation_rule, self.order_line).get_inventory()
            if inventory:
                break

        return inventory

    def allocate_inventory(self):
        try:
            with transaction.atomic():
                for order_line in self.order.order_lines.all():
                    self.order_line = order_line

                    inventory_obj = self._run_allocation_algo()
                    print('inventory_obj - ', inventory_obj)

                    if inventory_obj:
                        # try:
                        task_type = TaskType.objects.filter(name=PICK_TASK).first()
                        to_loc = Location.objects.filter(is_shipping=True).first()
                        site = self.order.pickup_hub.whsite_set.first()
                        pick_task = WhTask.objects.create(
                            task_type=task_type,
                            order=self.order,
                            order_line=self.order_line,
                            from_loc_id=inventory_obj.location_id,
                            to_loc=to_loc,
                            sku_id=inventory_obj.sku_id,
                            client=inventory_obj.client or inventory_obj.sku.client,
                            qty=self.order_line.quantity,
                            tag=inventory_obj.tag,
                            status='New',
                            site=site  # TODO: check to use pickup hub or delivery hub
                        )
                        inventory_obj.allocated_qty += self.order_line.quantity
                        inventory_obj.save()
                        self.order_line.qty_allocated = self.order_line.quantity
                        self.order_line.save()
                        print('pick_task - ', pick_task)
                    else:
                        self.error_msg = 'Inventory not found'
                        raise BaseException('Inventory not found')

                self.order.status = StatusPipeline.ORDER_ALLOCATED
                self.order.save()
        except Exception as e:
            print('e.args[0] - ', e.args[0])
            self.error_msg = 'Allocation Failed'
            return False

        return True

