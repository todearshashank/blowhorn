from datetime import timedelta, datetime
import pytz

from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from django.core.files.base import ContentFile
from blowhorn.utils.functions import timedelta_to_hmsstr
from rest_framework import status
from django.core.exceptions import ValidationError

from django.conf import settings
from blowhorn.common.utils import generate_pdf
from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED
from blowhorn.common.helper import CommonHelper
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.order.models import Order, OrderLine
from blowhorn.wms.constants import INV_LOCKED, WMS_STOCK_CHECKED, TAG_PREFIX_CODE
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.submodels.returnorder import ReturnOrderLine
from blowhorn.wms.submodels.sku import PackConfig
from blowhorn.wms.submodels.stockcheck import StockCheck, StockCheckBatch, StockCheckLines, \
    GainLossReason
from blowhorn.wms.submodels.containers import WmsAllocationLine
from blowhorn.wms.v1.views.wms import get_user_permission
from blowhorn.wms.constants import ORDER_LINE_MANIFESTED, \
    INV_LOCKED, WMS_QTY_ADD, WMS_QTY_SUBTRACT


class StockCheckService:

    def check_site_permissions(self, site_id, user, is_org_admin=None):
        sites = get_user_permission(user)
        _permission = int(site_id) in sites
        if is_org_admin and not _permission and not is_org_admin:
            _permission = False

        if not _permission:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            raise ValidationError(resp)

        self.stock_check_reasons = list(GainLossReason.objects.filter(site_id=site_id,
                                        is_active=True).values('id', 'name'))

        return _permission

    def _get_stockline_format(self, line):
        _gainloss = line.curr_qty - line.prev_qty
        if _gainloss > 0:
            self.total_gain += _gainloss
        elif _gainloss < 0:
            self.total_loss += _gainloss

        return {
                "tag": (line.tag).replace('_', ' ') if line.tag else '-',
                "prev_qty": line.prev_qty,
                "curr_qty": line.curr_qty,
                "diff": _gainloss,
                "reason": line.reason.name if line.reason else '-'
            }

    def create_stock_check_pdf(self, stock_check, batch, customer):
        _data = {}
        self.total_gain, self.total_loss = 0, 0
        tz = pytz.timezone(settings.ACT_TIME_ZONE)
        _data['performed_by'] = {
            'name' : batch.created_by.name,
            'date' : batch.created_date.astimezone(tz).strftime('%d %b %Y %I:%M %p'),
        }

        stock_check_lines = StockCheckLines.objects.filter(
                                stock_check_id=stock_check.id)

        _data['warehouse'] = {
            'name' : batch.site.hub.name,
            'address' : '%s %s %s' % (batch.site.hub.address.line1,
                                    batch.site.hub.address.line2, batch.site.hub.address.postcode)
        }

        _data['client'] = customer.name
        _data['footer'] = {
                'link': settings.HOST,
                'brand_name': settings.BRAND_NAME,
                'email': settings.COMPANY_SUPPORT_EMAIL,
                'contact': '+91 8045683500'
            }
        _data['logo'] = '%s/images/logo/stockcheck_logo2.png' % settings.ROOT_DIR.path('website/static')

        _stockchecklines = {}
        for _li in stock_check_lines:
            _line = self._get_stockline_format(_li)
            if _li.sku.id in _stockchecklines:
                _stockline = _stockchecklines[_li.sku.id]['results']
                _stockline.append(_line)
            else:
                _name = _li.sku.name
                _desc = ''
                _temp = _name.split('|')
                if len(_temp) > 1:
                    _name = _temp[0]
                    _desc = _temp[1::]

                _stockchecklines[_li.sku.id] = {
                    'id' : _li.sku.id,
                    'name' : _name,
                    'description' : _desc,
                    'results' : [_line,]
                }

        _data['sku_details'] = [_val for _,_val in _stockchecklines.items()]
        batch.end_time = timezone.now() + timedelta(seconds=1)
        _time_consumed = timedelta_to_hmsstr(batch.end_time - batch.created_date)
        _data['remarks'] = [
            'Stock Check completed at %s.' % (datetime.now().astimezone(tz).strftime('%d %b %Y %I:%M %p')),
            'Process took %s.' % _time_consumed,
            'Identified %s inventory gains.' % self.total_gain,
            'Identified %s inventory loss.' % abs(self.total_loss)
        ]

        if stock_check_lines:
            pdf_template_name = 'pdf/stock_check/main.html'
            filename = "Stock_Check_Reports_%s.pdf" % stock_check.id
            footer = {
                'template': 'pdf/stock_check/footer.html',
                'context': _data
            }
            pdf_data = generate_pdf(pdf_template_name, _data, footer=footer)
            batch.file.save(filename, ContentFile(pdf_data), save=True)

    def write_itl(self, inv, qty, status, user, tag=None,
                tracking_level=None, remarks=None,
                container=None, diff=0, operation=None, order=None, qty_left=0):

        if status == ORDER_LINE_MANIFESTED:
            operation = WMS_QTY_SUBTRACT
            diff = qty

        inv_trans = InventoryTransaction.objects.create(
            tag=tag or inv.tag,
            from_loc=inv.location,
            to_loc=inv.location,
            sku_id=inv.sku_id,
            client=inv.client,
            qty=qty,
            site=inv.site,
            supplier=inv.supplier,
            pack_config=inv.pack_config,
            status=status,
            container=container,
            tracking_level=tracking_level,
            remarks=remarks,
            qty_diff=diff,
            operation=operation,
            order=order,
            qty_left=qty_left
        )
        inv_trans.created_by = user
        inv_trans.modified_by = user
        inv_trans.save()

    def get_dict_response(self, sku, inv, site_id, weight):
        stock_check = StockCheck.objects.filter(sku=sku, batch__site_id=site_id,
                                                created_date__gte=timezone.now() - timedelta(
                                                    days=7)).first()

        return {
            "sku_id": sku.id,
            "sku_image": sku.file.url if sku.file else '',
            "attributes": [{"key": "Product Tag", "value": inv.tag},
                           {"key": "Manufacturer Name", "value": str(sku.supplier)},
                           {"key": "SKU", "value": str(sku)},
                           {"key": "Capacity", "value": weight}],
            "sku_name": sku.name,
            "mrp": sku.mrp or 0,
            "selling_price": sku.mrp - ((sku.discounts if sku.discounts else 0) if sku.discount_type == DISCOUNT_TYPE_FIXED else
                            (sku.mrp * (sku.discounts/100) if sku.discounts else 0)) if sku.mrp else 0,
            "manufacture_name": str(sku.supplier),
            "ean": sku.ean,
            "upc": sku.upc,
            "uom": sku.uom,
            "sku_group": str(sku.product_group),
            "is_completed": True if stock_check else False,
            "completed_date": stock_check.modified_date.isoformat() if stock_check else ''
        }

    def get_inv_trck_lvl(self, sku, inv):
        pack_config = PackConfig.objects.filter(whsku=sku).select_related('tracking_level').first()
        if not pack_config:
            pack_config = PackConfig.objects.filter(whsku__isnull=True).select_related(
                'tracking_level').first()
        if not pack_config:
            resp = {
                "message": "Tracking Level not defined"
            }
            raise ValidationError(resp)

        tracking_level = pack_config.tracking_level
        tracking_info = [{
            "name": tracking_level.level_one_name,
            "display": tracking_level.level_one_name,
            "value": 1}]
        if tracking_level.level_two_name:
            tracking_info.append({
                "name": tracking_level.level_two_name,
                "display": tracking_level.level_two_name + ' - ' + str(tracking_level.ratio_level_two_one)
                           + ' ' + tracking_level.level_one_name,
                "value": tracking_level.ratio_level_two_one
            })
        if tracking_level.level_three_name:
            tracking_info.append({
                "name": tracking_level.level_three_name,
                "display": tracking_level.level_three_name + ' - ' + str(tracking_level.ratio_level_three_two \
                                                                         * tracking_level.ratio_level_two_one) + ' ' +
                           tracking_level.level_one_name,
                "value": tracking_level.ratio_level_three_two
            })
        return tracking_info

    def get_product_resp(self, site_id, code, is_sku, orderline_id=None, is_allocation=False):
        query = Q(sku__ean=code) | Q(sku__upc=code)
        if is_sku:
            query |= Q(sku__name=code)
            query &= Q(site_id=site_id, wmsmanifestline__isnull=True, qty__gt=0)
        else:
            query |= Q(sku__product_group__name=code)
            query &= Q(site_id=site_id, wmsmanifestline__isnull=True, qty__gt=0)

        if orderline_id and is_allocation:
            inv_ids = WmsAllocationLine.objects.filter(order_line_id=orderline_id).values_list('inventories__id',
                                                                                               flat=True)
            invs = Inventory.objects.filter(id__in=inv_ids).order_by('sku', 'expiry_date', 'id')
        else:
            invs = Inventory.objects.filter(query).order_by('sku', 'expiry_date', 'id')

        if not invs:
            resp = {
                "message": "No Inventories present for the given SKU/Product Group "
            }
            raise ValidationError(resp)

        resp, inv_details, locked_inv = [], [], []
        dict_ = {}
        sku = invs[0].sku
        weight = "%s %s" % ('0', sku.uom)

        for inv in invs:
            if inv.status == INV_LOCKED:
                locked_inv.append({
                    "inv_id": inv.id,
                    "tag": inv.tag,
                    "qty": inv.qty,
                    "tracking_level": inv.tracking_level
                })
                continue
            whsku = inv.sku
            weight = sku.each_weight or '0' + ' ' + sku.uom

            if whsku == sku:
                inv_details.append({
                    "inv_id": inv.id,
                    "tag": inv.tag,
                    "qty": inv.qty,
                    "pack_config_id": inv.pack_config_id,
                    "tracking_level": inv.tracking_level,
                    "client_id": inv.client_id,
                    "tracking_info": self.get_inv_trck_lvl(sku, inv),
                    "reason" : list(StockCheckLines.objects.filter(tag=inv.tag).values_list('reason', flat=True))

                })
            else:
                if not dict_:
                    dict_ = self.get_dict_response(sku, inv, site_id, weight)

                dict_.update({"inv_details": inv_details})
                resp.append(dict_)
                sku = whsku
                dict_ = {}
                inv_details = [{
                    "inv_id": inv.id,
                    "tag": inv.tag,
                    "qty": inv.qty,
                    "pack_config_id": inv.pack_config_id,
                    "tracking_level": inv.tracking_level,
                    "client_id": inv.client_id,
                    "tracking_info": self.get_inv_trck_lvl(sku, inv),
                    "reason" : list(StockCheckLines.objects.filter(tag=inv.tag).values_list('reason', flat=True))
                }]

        dict_ = self.get_dict_response(sku, inv, site_id, weight)
        dict_.update(
            {
                "inv_details": inv_details,
                "locked_inv": locked_inv,
                "reasons": self.stock_check_reasons or []
            }
        )
        resp.append(dict_)
        return resp

    def get_shipping_address_dict(self, shipping_address):
        shipping_details = {
            'shipping_address' : {
                    'line1': shipping_address.line1,
                    'line3': shipping_address.line3,
                    'line4': shipping_address.line4,
                    'postcode': shipping_address.postcode,
                    'mobile': str(shipping_address.phone_number),
                    'name': shipping_address.first_name
            }
        }
        return shipping_details

    def get_details(self, user, data):
        '''
            StockInfo - Get API
        '''
        site_id = data.get('site_id')
        code = data.get('code')
        is_sku = data.get('is_sku') == "true"
        is_allocation = data.get('is_allocation') == "true"
        order_number = data.get('order_number')
        orderline_id = data.get('orderline_id', None)
        customer = CustomerMixin().get_customer(user)
        # check the site permissions for the user
        self.check_site_permissions(site_id, user, is_org_admin=customer.user == user)

        if order_number:
            data = []
            order = Order.objects.select_related('shipping_address').filter(number=order_number).first()
            if not order:
                return status.HTTP_400_BAD_REQUEST, 'Invalid Order Number'

            _shipping_address = self.get_shipping_address_dict(order.shipping_address)
            data.append(_shipping_address)

            for orderline in order.order_lines.all():
                code = orderline.wh_sku.name if orderline.wh_sku else orderline.sku.name
                resp = self.get_product_resp(site_id, code, is_sku) if code else None
                if not isinstance(resp, list):
                    return resp
                data.append({"qty": orderline.quantity, "sku_data": resp}) if resp else None

            return status.HTTP_200_OK, data

        if orderline_id and is_allocation:
            orderline = OrderLine.objects.filter(id=orderline_id).first()
            if not orderline:
                return status.HTTP_400_BAD_REQUEST, 'Invalid Order line ID'

            if not WmsAllocationLine.objects.filter(order_line_id=orderline_id).exists():
                is_allocation, orderline_id = False, None

            resp = self.get_product_resp(site_id, code, is_sku, orderline_id, is_allocation) if code else None
            if not isinstance(resp, list):
                return resp
            return status.HTTP_200_OK, data

        # get final response format
        resp = self.get_product_resp(site_id, code, is_sku)
        if not isinstance(resp, list):
            return resp

        return status.HTTP_200_OK, resp

    def get_latest_tag(self, tag):
        _tag = CommonHelper().get_unique_friendly_id(TAG_PREFIX_CODE)
        return _tag

    def get_qty_in_given_tracking_level(self, inv, tracking_lvl, tracking_level):
        if inv.tracking_level == tracking_lvl:
            return inv.qty, inv.tracking_level
        else:
            if inv.tracking_level == tracking_level.level_two_name:
                return tracking_level.ratio_level_two_one * inv.qty, tracking_lvl
            else:
                return tracking_level.ratio_level_two_one * inv.qty * tracking_level.ratio_level_three_two, tracking_lvl

    def __process_inventory_entry(self, x, item, tracking_level, st_check, user):
        if item['inv_id'] != x.id:
            return
        qty = int(item['physical_qty'])
        x.qty, _ = self.get_qty_in_given_tracking_level(x, item['tracking_level'], tracking_level)
        self.stock_history_list.append(StockCheckLines(stock_check=st_check, tag=x.tag,
                                                    prev_qty=x.qty, sku=x.sku,
                                                    curr_qty=qty, reason_id=item.get('reason_id', None)))
        if qty == x.qty:
            self.write_itl(x, qty, WMS_STOCK_CHECKED, user, remarks='Stock Check Passed',
                            tracking_level=item['tracking_level'])
            x.qty = qty
        elif qty > x.qty:
            self.write_itl(x, qty, WMS_STOCK_CHECKED, user, tracking_level=item['tracking_level'],
                            remarks='Stock Check Increased the count from %s to %s' % (x.qty, qty),
                            diff=qty-x.qty, operation=WMS_QTY_ADD, qty_left=qty)
            x.qty = qty
        else:
            tag = self.get_latest_tag(x.tag)
            self.write_itl(x, qty, WMS_STOCK_CHECKED, user, tracking_level=item['tracking_level'],
                            remarks='Stock Check Decreased the count from %s to %s' % (x.qty, qty),
                            diff=x.qty-qty, operation=WMS_QTY_SUBTRACT, qty_left=qty)
            self.write_itl(x, x.qty - qty, WMS_STOCK_CHECKED, user, tag=tag,
                            remarks='Inventory Created', tracking_level=item['tracking_level'])

            _inv = Inventory.objects.create(tag=tag, sku_id=x.sku_id,
                        client=x.client, qty=x.qty - qty, status=INV_LOCKED,
                        weight=x.weight, location_id=x.location_id,
                        site_id=x.site_id, tracking_level=item['tracking_level'],
                        supplier=x.supplier, pack_config=x.pack_config,
                        mfg_date=item.get('mfg_date', None))
            _inv.created_by=user
            _inv.modified_by=user
            _inv.save()

            x.qty = qty

        x.tracking_level = item['tracking_level']
        x.mfg_date = item.get('mfg_date', None)
        x.modified_by = user
        x.save()

    def create_entry(self, user, data):
        '''
            StockInfo - Post API
        '''
        # check the site permissions for the user
        self.stock_check_reasons = []
        inv_details = data.get('inv_details')
        site_id = data.get('site_id')
        self.check_site_permissions(site_id, user)

        # check for negative quantities
        ids = []
        for x in inv_details:
            ids.append(x.get('inv_id'))
            if x['physical_qty'] < 0:
                resp = {
                    "message": "Invalid Quantity"
                }
                return status.HTTP_400_BAD_REQUEST, resp

        # check if the locked inventory for items with count more than 0 exists
        invs = Inventory.objects.filter(id__in=ids, qty__gt=0).exclude(status__in=INV_LOCKED)
        if not invs:
            resp = {
                "message": "Invalid Inventory"
            }
            return status.HTTP_400_BAD_REQUEST, resp

        # Check if tracking level is defined
        pack_config = PackConfig.objects.filter(whsku=invs[0].sku).select_related('tracking_level').first()
        if not pack_config:
            pack_config = PackConfig.objects.filter(whsku__isnull=True
                            ).select_related('tracking_level').first()
            resp = {
                "message": "Tracking Level not defined"
            }
            return status.HTTP_400_BAD_REQUEST, resp

        _batch = StockCheckBatch.objects.create(site_id=site_id)
        sku = invs.last().sku
        customer = sku.client.customer
        # create the batch and stock check entry for sku
        _batch.created_by=user
        st_check = StockCheck.objects.create(sku=sku, batch=_batch)
        st_check.created_by=user
        self.stock_history_list = []

        # start processing the skus
        with transaction.atomic():
            for x in invs:
                for item in inv_details:
                    self.__process_inventory_entry(x, item, pack_config.tracking_level, st_check, user)

            StockCheckLines.objects.bulk_create(self.stock_history_list)
            self.create_stock_check_pdf(st_check, _batch, customer)

        st_check.save()
        _batch.save()
        # get final response format
        resp = self.get_product_resp(site_id, sku.name, True)
        return status.HTTP_200_OK, resp[0]

    def delete_entry(self, user, data):
        '''
            StockInfo - Delete API
        '''
        # check user's site permission
        site_id = data.get('site_id')
        self.check_site_permissions(site_id, user)

        # fetch the inventories based on ID
        inv_details = data.get('inv_details', [])
        inv_ids = [x['inv_id'] for x in inv_details]
        invs = Inventory.objects.filter(id__in=inv_ids)

        # create inventory transaction entries for deletion
        inventory_transaction_list = []
        sku = None
        for inv in invs:
            if not sku:
                sku = inv.sku
            itl = {'tag': inv.tag,
                   'from_loc': inv.location,
                   'sku': inv.sku,
                   'site': inv.site,
                   'client': inv.client,
                   'qty': inv.qty,
                   'uom': '',
                   'supplier': inv.supplier,
                   'status': 'Inventory Deleted',
                   'reference': ''
                }
            inventory_transaction_list.append(InventoryTransaction(**itl))

        # Delete the inventory and create the bulk inventory transactions
        with transaction.atomic():
            ReturnOrderLine.objects.filter(inventory__id__in=inv_ids).update(inventory=None)
            InventoryTransaction.objects.bulk_create(inventory_transaction_list)
            Inventory.objects.filter(id__in=inv_ids).delete()

        # get final response format
        resp = self.get_product_resp(site_id, sku.name, True)
        return status.HTTP_200_OK, resp[0]
