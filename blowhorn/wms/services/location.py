from django.db import transaction

from blowhorn.oscar.core.loading import get_model
from blowhorn.wms.submodels.location import Location, Client, LocationType, \
    LocationZone

Dimension = get_model('order', 'Dimension')
WhSite = get_model('wms', 'WhSite')


class LocationGenerator:

    def __init__(self, **kwargs):
        self.client_name = kwargs.get('client')
        self.loc_prefix = kwargs.get('loc_prefix', 'LOC')
        self.zone_count = kwargs.get('zone_count', 3)
        self.loc_type = kwargs.get('loc_type', None)
        self.site = kwargs.get('site')
        self.dimension = kwargs.get('dimension')
        self.weight = kwargs.get('weight', 1000)
        self.loc_count = kwargs.get('loc_count', 100)

        if self.client_name:
            self.client = Client.objects.filter(name=self.client_name).first()
            if not self.client:
                self.error_msg = 'Invalid Client'
        else:
            self.client, _ = Client.objects.get_or_create(name='DEFAULT')

        if self.loc_type:
            self.loc_type = LocationType.objects.filter(name=self.loc_type).first()
            if not self.loc_type:
                self.error_msg = 'Invalid Loc type'
        else:
            self.loc_type, _ = LocationType.objects.get_or_create(name='DEFAULT')

        if self.site:
            self.site = WhSite.objects.filter(name=self.site).first()
            if not self.site:
                self.error_msg = 'Invalid Site'
        else:
            self.error_msg = 'No Site name given'
            return

        self.dimensions = Dimension.objects.filter(length=3, breadth=3, height=3, uom='M').first()
        if not self.dimensions:
            self.dimensions = Dimension.objects.create(length=3, breadth=3, height=3, uom='M')

    def generate_location(self):
        location_list = []
        is_receiving = False
        is_putaway = False
        is_packing = False
        is_shipping = False

        with transaction.atomic():
            for zn_seq in range(self.zone_count):
                zone = LocationZone.objects.create(name=chr(65+zn_seq) + 'ZNE',
                                            site=self.site)
                for i in range(1, self.loc_count+1):
                    is_receiving = False
                    is_putaway = False
                    is_packing = False
                    is_shipping = False
                    loc_name = chr(65+zn_seq) + self.loc_prefix + str(i).zfill(3)
                    if i % 4 == 0:
                        is_receiving = True
                    elif i % 4 == 1:
                        is_putaway = True
                    elif i % 4 == 2:
                        is_packing = True
                    elif i % 4 == 3:
                        is_shipping = True
                    location_list.append(Location(
                        name=loc_name,
                        client=self.client,
                        loc_type=self.loc_type,
                        site=self.site,
                        status='UnLocked',
                        zone=zone,
                        dimensions=self.dimensions,
                        weight=self.weight,
                        is_receiving=is_receiving,
                        is_putaway=is_putaway,
                        is_packing=is_packing,
                        is_shipping=is_shipping
                        )
                    )
            Location.objects.bulk_create(location_list)

