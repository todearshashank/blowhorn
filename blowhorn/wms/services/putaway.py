from django.db.models import Q

from blowhorn.wms.models import Location, Inventory, LocationZone, WhSku, Client
from blowhorn.wms.models import QueryBuilder
from blowhorn.wms.constants import PROCESS_PUTAWAY, LOC_UNLOCKED

MODEL_FIELD_MAPPING = {

}


class PutAwayLocationAlgo(object):

    def __init__(self, obj, **kwargs):
        self.query_builder = obj
        self.primary_model = Location
        self.sku = kwargs.get('sku')
        self.client = kwargs.get('client')
        self.site = kwargs.get('site')
        # TODO : remove default and put validation
        self.inv_vol = kwargs.get('inv_vol', 10) if kwargs else 10
        self.inv_qty = kwargs.get('inv_qty')

    # def create_query_from_builder(self):
    #     _params, filter_ = None, {}
    #     for line in self.query_builder.querybuilderlines_set.all():
    #         from_model = line.from_model
    #         if from_model == 'WhSku' and line.filter == 'whsku__name':
    #             # check if sku for putaway matches the given SKU as this algo is only for given SKU
    #             if self.sku != line.field_value:
    #                 return None
    #
    #         query_str = None
    #         if eval(from_model) == self.primary_model:
    #             query_str = line.filter.split('__')[1]
    #             if line.operator != '=':
    #                 query_str += line.operator
    #             filter_[query_str] = line.field_value
    #             # query_str += "'" + line.field_value + "'"
    #         else:
    #             query_str = line.filter
    #             if line.operator != '=':
    #                 query_str += line.operator
    #             filter_[query_str] = line.field_value
    #             # query_str += "'" + line.field_value + "'"
    #     # return filter_
    #     print(filter_)
    #     return self.primary_model.objects.filter(**filter_).order_by('sequence')

    def create_query_from_builder(self):
        _params, filter_ = None, {}
        or_with_parenthesis = False
        and_with_parenthesis = False
        order_by_clause = []
        _params = (Q(site=self.site) & Q(status=LOC_UNLOCKED))
        for line in self.query_builder.querybuilderlines_set.all().order_by('sequence'):
            from_model = line.from_model
            if from_model == 'WhSku' and line.filter == 'whsku__name':
                # check if sku for putaway matches the given SKU as this algo is only for given SKU
                if self.sku != line.field_value:
                    return None

            if line.order_by:
                orderby_model = line.order_by.split('__')[0]
                orderby_column = line.order_by.split('__')[1]
                # if eval(line.order_by) == orderby_model:
                #     query_str = line.filter.split('__')[1]
                order_by_clause.append(orderby_column)

            if line.open_parenthesis and filter_:
                if _params:
                    _params = _params & Q(**filter_)
                    filter_ = {}
                else:
                    _params = Q(**filter_)
                filter_ = {}

            if line.open_parenthesis and line.where_condition == 'OR':
                or_with_parenthesis = True
            elif line.open_parenthesis and line.operator == 'AND':
                and_with_parenthesis = True

            query_str = None
            if line.filter:
                if eval(from_model) == self.primary_model:
                    query_str = line.filter.split('__')[1]
                    if line.operator != '=':
                        query_str += line.operator
                    # query_str += "'" + line.field_value + "'"
                else:
                    query_str = line.filter
                    if line.operator != '=':
                        query_str += line.operator
                    # query_str += "'" + line.field_value + "'"

            if line.field_value:
                filter_[query_str] = line.field_value
            elif line.field_filter:
                if line.field_filter == 'Inventory__volume':
                    filter_[query_str] = self.inv_vol

            if line.where_condition == 'OR' and not or_with_parenthesis:
                if _params:
                    _params = _params | Q(**filter_)
                    filter_ = None
                else:
                    _params = Q(**filter)
                filter_ = {}

            if line.close_parenthesis and filter_ and or_with_parenthesis:
                if _params:
                    _params = _params | Q(**filter_)
                    filter_ = {}
                else:
                    _params = Q(**filter_)
                or_with_parenthesis = False

            if filter_:
                if _params:
                    _params = _params & Q(**filter_)
                else:
                    _params = Q(**filter_)
                filter_ = {}

        # return filter_
        if _params:
            qs = self.primary_model.objects.filter(_params).order_by(*order_by_clause)
        else:
            qs = self.primary_model.objects.order_by(*order_by_clause)
        return qs

    def get_location(self):
        location_qs = self.create_query_from_builder()
        print('putaway algo location_qs - ', str(location_qs.query))

        if location_qs:
            return location_qs.first()
        return None


class FindPutawayLocation(object):

    def __init__(self, task_obj):
        self.wh_task = task_obj
        self.data = {
            'sku': task_obj.sku,
            'client': task_obj.client,
            'site': task_obj.site,
            'inv_vol': 10,# TODO
            'inv_qty': task_obj.qty
        }

    def _run_putaway_algo(self):
        _params = Q(process=PROCESS_PUTAWAY, is_active=True)
        site_param = Q(site__isnull=True) | Q(site=self.wh_task.site)
        _params = _params & Q(site_param)
        putaway_rules = QueryBuilder.objects.filter(process=PROCESS_PUTAWAY, is_active=True).order_by('sequence')
        to_loc = None

        for putaway_rule in putaway_rules:
            to_loc = None
            to_loc = PutAwayLocationAlgo(putaway_rule, **self.data).get_location()
            if to_loc:
                break

        return to_loc

    def get_putaway_location(self):
        return self._run_putaway_algo()
