import json
from blowhorn.common.base_task import TransactionAwareTask
from celery.utils.log import get_task_logger
from blowhorn.common.decorators import redis_batch_lock
from django.conf import settings
from config.settings import status_pipelines as StatusPipeline
from blowhorn import celery_app
from blowhorn.wms.mixins.inventory import InventorySummaryData
from blowhorn.order.models import OrderLine
from blowhorn.wms.submodels.sku import KitSkuMapping, WhKitSku

logger = get_task_logger(__name__)


@celery_app.task
@redis_batch_lock(period=10800, expire_on_completion=True)
def generate_inventory_summary():
    """
    Create the inventory summary
    """
    logger.info("Inventory summary creation started")
    InventorySummaryData.create_inventory_summary()

@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def update_order_kitskus(self, ids):
    ids = json.loads(ids)
    for _id in ids:
        sku = KitSkuMapping.objects.get(id=_id).parent_sku
        ols = OrderLine.objects.filter(wh_sku=sku, order__customer=sku.client.customer, order__status=StatusPipeline.ORDER_NEW)
        kits = KitSkuMapping.objects.filter(parent_sku=sku)

        if not kits or not ols:
            continue

        for o in ols:
            whkitsku = WhKitSku.objects.filter(name=sku.name, quantity=o.quantity).first()
            if not whkitsku:
                whkitsku = WhKitSku.objects.create(name=sku.name, quantity=o.quantity)
            for i in kits:
                OrderLine.objects.create(wh_sku=i.child_sku, quantity=(o.quantity*i.quantity), wh_kit_sku=whkitsku,order=o.order)

        ols.delete()
