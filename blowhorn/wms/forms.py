from django import forms
from django.core.exceptions import ValidationError

from blowhorn.wms.submodels.sku import WhSku


class WhskuForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(WhskuForm, self).__init__(*args, **kwargs)

    def clean(self):

        super(WhskuForm, self).clean()

        ean = self.cleaned_data.get('ean')
        upc = self.cleaned_data.get('upc')
        client = self.cleaned_data.get('client')
        if not client and self.instance and self.instance.id:
            sku = WhSku.objects.get(id=self.instance.id)
            client = sku.client
        if client:
            if ean and WhSku.objects.filter(client=client, ean=ean).exclude(id=self.instance.id).exists():
                raise ValidationError({'ean': ' Duplicate ean for this client'})
            if upc and WhSku.objects.filter(client=client, upc=upc).exclude(id=self.instance.id).exists():
                raise ValidationError({'upc': 'Duplicate upc for this client'})

    class Meta:
        model = WhSku
        fields = '__all__'
