from django.utils.translation import ugettext_lazy as _

TAG_PREFIX_CODE = 'TAG'

PROCESS_RECEIVING = 'Receiving'
PROCESS_PUTAWAY = 'PutAway'
PROCESS_ALLOCATION = 'Allocation'
PROCESS_PICKING = 'Picking'
PROCESS_SHIPPING = 'Shipping'

PUTAWAY_TASK = 'PUTAWAY'
PICK_TASK = 'PICK'
QC_TASK = 'QUALITYCHECK'
ORDER_TASK = 'ORDER-HEADER'
SHIPPING_TASK = 'SHIPPING'

# WMS Operations
ALLOCATION = 'ALLOCATION'
PUTAWAY = 'PUTAWAY'
PICKING = 'PICKING'

OPERATION_CHOICES = (
    (ALLOCATION, _("ALLOCATION")),
    (PUTAWAY, _("PUTAWAY")),
    (PICKING, _("PICKING")),
)

LOC_UNLOCKED = 'UnLocked'
LOC_LOCKED = 'Locked'

LOC_STATUSES = (
    (LOC_UNLOCKED, 'UnLocked'),
    (LOC_LOCKED, 'Locked')
)

INV_UNLOCKED = 'UnLocked'
INV_LOCKED = 'Locked'

INV_STATUSES = (
    (INV_UNLOCKED, 'UnLocked'),
    (INV_LOCKED, 'Locked')
)

INV_TXN_STATUSES = (
    ('Stock Check', _('Stock Check')),
    ('Quantity Updated', _('Quantity Updated')),
    ('Passed', _('Passed')),
    ('WMS Manifested', _('WMS Manifested')),
    ('Received', _('Received')),
    ('WMS Shipped', _('WMS Shipped')),
    ('Inventory Deleted', _('Inventory Deleted'))
)

PROCESSES = (
    (PROCESS_PUTAWAY, _('PutAway')),
    (PROCESS_ALLOCATION, _('Allocation'))
)


NEW = 'NEW'
IN_PROGRESS = 'IN_PROGRESS'
COMPLETED = 'COMPLETED'
STOCK_CHECK_STATUSES = (
    (PROCESS_PUTAWAY, _('PutAway')),
    (PROCESS_ALLOCATION, _('Allocation'))
)
QC_FAILED = 'Failed'
QC_PASSED = 'Passed'
ASN_NEW_STATUS = 'New'
ASN_IN_PROGRESS = 'In-Progress'
ASN_COMPLETED = 'Completed'
ASN_EXPIRED = 'Expired'

ASN_STATUSES = (
    (ASN_NEW_STATUS, _('New')),
    (ASN_IN_PROGRESS, _('In-Progress')),
    (ASN_COMPLETED, _('Completed')),
    (ASN_EXPIRED, _('Expired'))
)

ASN_END_STATUSES = [ASN_COMPLETED, ASN_EXPIRED]


WH_TASK_NEW = 'New'
WH_TASK_ASSIGEND = 'Assigned'
WH_TASK_INPROGRESS = 'In-Progress'
WH_TASK_COMPLETED = 'Completed'
WH_TASK_HOLD = 'Hold'
WH_TASK_REJECT = 'Rejected'

WH_TASK_STATUSES = (
    (WH_TASK_NEW, 'New'),
    (WH_TASK_ASSIGEND, 'Assigned'),
    (WH_TASK_INPROGRESS, 'In-Progress'),
    (WH_TASK_COMPLETED, 'Completed'),
    (WH_TASK_HOLD, 'Hold'),
    (WH_TASK_REJECT, 'Rejected')
)

ACTIVE_WHTASKS_STATUSES = [WH_TASK_NEW, WH_TASK_ASSIGEND, WH_TASK_INPROGRESS]

TEXT = "text"
INTEGER = "integer"
BOOLEAN = "boolean"
FLOAT = "float"
DATE = "date"
FILE = "file"

TYPE_CHOICES = (
    (TEXT, _("Text")),
    (INTEGER, _("Integer")),
    (BOOLEAN, _("True / False")),
    (FLOAT, _("Float")),
    (DATE, _("Date")),
    (FILE, _("File")),
)

TYPE_FIELD_MAPPING = {
    TEXT: 'value_text',
    INTEGER: 'value_integer',
    BOOLEAN: 'value_boolean',
    FLOAT: 'value_float',
    DATE: 'value_date',
    FILE: 'value_file'
}

PYTHON_DATA_FIELD_MAP = {
    TEXT: str,
    INTEGER: int,
    BOOLEAN: bool,
    FLOAT: float,
    DATE: 'date',
    FILE: 'file'
}

WMS_QTY_ADD = 'Add'
WMS_QTY_SUBTRACT = 'Subtract'

WMS_SOURCE_IMPORT = 'Import'
WMS_SOURCE_WEB = 'Web'
WMS_SOURCE_APP = 'App'

WMS_RECEIVED = 'Received'
WMS_STOCK_CHECKED = 'Stock Check'
WMS_INV_CREATED = 'Inventory Created'
WMS_INV_QTY_UPDATED = 'Quantity Updated'
WMS_INVENTORY_DELETED = 'Inventory Deleted'

ORDER_LINE_NEW = 'New'
ORDER_LINE_ALLOCATED = 'WMS Allocated'
ORDER_LINE_PICKED = 'WMS Picked'
ORDER_LINE_MANIFESTED = 'WMS Manifested'
ORDER_LINE_SHIPPED = 'WMS Shipped'

ORDER_LINE_STATUSES = (
    (ORDER_LINE_NEW, 'New'),
    (ORDER_LINE_ALLOCATED, 'Allocated'),
    (ORDER_LINE_PICKED, 'Picked'),
    (ORDER_LINE_MANIFESTED, 'Manifested'),
    (ORDER_LINE_SHIPPED, 'Shipped')
)

ALLOCATION_ALLOCATED = 'Allocated'
ALLOCATION_PARTIALLY_ALLOCATED = 'Partially Allocated'
ALLOCATION_SHORT = 'Allocation Short'

ALLOCATION_STATUSES = (
    (ALLOCATION_ALLOCATED, 'Allocated'),
    (ALLOCATION_PARTIALLY_ALLOCATED, 'Partially Allocated'),
    (ALLOCATION_SHORT, 'Allocation Short')
)

ALLOCATION_LIFO_ALGORITHM = 'Last In First Out'
ALLOCATION_FIFO_ALGORITHM = 'First In First Out'

ALLOCATION_ALGORITHMS = (
    (ALLOCATION_LIFO_ALGORITHM, 'Last In First Out'),
    (ALLOCATION_FIFO_ALGORITHM, 'First In First Out')
)

QUERY_BUILDER_FROM_MODEL_VALUES = [('Location', 'Location'),
                                   ('LocationZone', 'Location Zone'),
                                   ('LocationType', 'Location Type'),
                                   ('StorageType', 'Storage Type'),
                                   ('WhSku', 'SKU'),
                                   ('Client', 'Client'),
                                   ('Inventory', 'Inventory')]

QUERY_BUILDER_WHERE_CONDITION_VALUES = [('AND', 'AND'), ('OR', 'OR')]

QUERY_BUILDER_FILTER_ON_FIELD_VALUES = [('Location__name', 'Location Name'),
                                        ('Location__status', 'Location Status'),
                                        ('zone__name', 'Location Zone Name'),
                                        ('client__name', 'Client Name'),
                                        ('whsku__name', 'SKU Name'),
                                        ('loc_type__name', 'Location Type Name'),
                                        ('storage_type__name', 'Storage Type Name'),
                                        ('Location__is_receiving', 'Location Is Receiving'),
                                        ('Location__is_putaway', 'Location Is Putaway'),
                                        ('Location__is_shipping', 'Location Is Shipping'),
                                        ('Location__remaining_volume', 'Location Remaining Volume'),
                                        ('Location__volume', 'Location Volume'),
                                        ('Location__current_volume', 'Location Current Volume'),
                                        ('Inventory__qty', 'Inventory Qty'),
                                        ('Inventory__expiry_date', 'Inventory Expiry Date'),
                                        ('Inventory__mfg_date', 'Inventory Manufacture Date')
                                        ]

QUERY_BUILDER_FIELD_FILTER_VALUES = [('Location__name', 'Location Name'),
                                     ('Location__status', 'Location Status'),
                                     ('zone__name', 'Location Zone Name'),
                                     ('client__name', 'Client Name'),
                                     ('whsku__name', 'SKU Name'),
                                     ('whsku__qty', 'SKU Qty'),
                                     ('loc_type__name', 'Location Type Name'),
                                     ('storage_type__name', 'Storage Type Name'),
                                     ('Location__is_receiving', 'Location Is Receiving'),
                                     ('Location__is_putaway', 'Location Is Putaway'),
                                     ('Location__is_shipping', 'Location Is Shipping'),
                                     ('Inventory__volume', 'Inventory Volume')]

QUERY_BUILDER_OPERATOR_VALUES = [('=', '='),
                                 ('__gt', '>'),
                                 ('__lt', '<'),
                                 ('__gte', '>='),
                                 ('__lte', '<=')]

QUERY_BUILDER_ORDER_BY_VALUES = [('location__current_volume', 'Remaining Volume'),
                                 ('location__sequence', 'Location Sequence'),
                                 ('Inventory__expiry_date', 'Inventory Expiry Date'),
                                 ('Inventory__remaining_qty', 'Tag Remaining Qty'),
                                 ('Inventory__mfg_date', 'Inventory Manufacture Date')]

QUERY_BUILDER_ORDERING_VALUES = [('asc', 'Ascending'),
                                 ('desc', 'Descending')
                                 ]

QUERY_BUILDER_FUNCTIONS_VALUES = [('Avg', 'Avg'),
                                  ('Max', 'Max'),
                                  ('Count', 'Count')
                                  ]


INVENTORY_DATA_TO_BE_IMPORTED = [
    {'name': 'Tag (Unique Identifier)', 'label': 'tag', 'type': 'string'},
    {'name': 'Site', 'label': 'site', 'type': 'string'},
    {'name': 'SKU Name', 'label': 'sku_name', 'type': 'string'},
    {'name': 'Quantity', 'label': 'qty', 'type': 'string'},
    {'name': 'Tracking Level', 'label': 'tracking_level', 'type': 'string'},
    {'name': 'Expiry Date (YYYY/MM/DD)', 'label': 'expiry_date', 'type': 'datetime'}
]

INVENTORY_MANDATORY_LABELS = ['Tag (Unique Identifier)', 'Site', 'SKU Name', 'Quantity']

WH_SKU_DATA_TO_BE_IMPORTED = [
    {'name': 'Name (Unique Identifier)', 'label': 'name', 'type': 'string'},
    {'name': 'Description', 'label': 'description', 'type': 'string'},
    {'name': 'Supplier', 'label': 'supplier', 'type': 'string'},
    {'name': 'Product Group', 'label': 'product_group', 'type': 'string'},
    {'name': 'Pack Config', 'label': 'pack_config', 'type': 'string'},
    {'name': 'Each Weight', 'label': 'each_weight', 'type': 'string'},
    {'name': 'Client', 'label': 'client', 'type': 'string'},
    {'name': 'Max Retail Price', 'label': 'mrp', 'type': 'string'},
    {'name': 'Discounts', 'label': 'discounts', 'type': 'string'},
    {'name': 'Discount Type', 'label': 'discount_type', 'type': 'string'}
]

WH_SKU_MANDATORY_LABELS = ['Name (Unique Identifier)', 'Supplier', 'Product Group', 'Pack Config']

SUPPLIER_DATA_TO_BE_IMPORTED = [
    {'name': 'Name (Unique Identifier)', 'label': 'name', 'type': 'string'},
    {'name': 'Mobile', 'label': 'mobile', 'type': 'string'}
]

SUPPLIER_MANDATORY_LABELS = ['Name (Unique Identifier)']

PRODUCT_GROUP_DATA_TO_BE_IMPORTED = [
    {'name': 'Name (Unique Identifier)', 'label': 'name', 'type': 'string'},
    {'name': 'Description', 'label': 'description', 'type': 'string'}
]

PRODUCT_GROUP_MANDATORY_LABELS = ['Name (Unique Identifier)']

CLIENT_DATA_TO_BE_IMPORTED = [
    {'name': 'Client Name', 'label': 'name', 'type': 'string'},
    {'name': 'Description', 'label': 'description', 'type': 'string'}
]

CLIENT_MANDATORY_LABELS = ['Client Name']

INVENTORY_DATA_TO_BE_EXPORTED = [
    {'name': 'ID', 'label': 'id', 'type': 'string'},
    {'name': 'Tag', 'label': 'tag', 'type': 'string'},
    {'name': 'SKU', 'label': 'sku_name', 'type': 'string'},
    {'name': 'Site', 'label': 'site_name', 'type': 'string'},
    {'name': 'Status', 'label': 'status', 'type': 'string'},
    {'name': 'Quantity', 'label': 'qty', 'type': 'string'},
    {'name': 'Description', 'label': 'description', 'type': 'string'},
    {'name': 'Remarks', 'label': 'remarks', 'type': 'string'},
    {'name': 'Supplier', 'label': 'supplier_name', 'type': 'string'},
    {'name': 'Created By', 'label': 'created_by', 'type': 'string'},
    {'name': 'Created Date', 'label': 'created_date', 'type': 'datetime'},
    {'name': 'Modified By', 'label': 'modified_by', 'type': 'string'},
    {'name': 'Modified Date', 'label': 'modified_date', 'type': 'datetime'},
]

INVENTORY_TRANSACTION_DATA_TO_BE_EXPORTED = [
    {'name': 'ID', 'label': 'id', 'type': 'string'},
    {'name': 'Tag', 'label': 'tag', 'type': 'string'},
    {'name': 'SKU', 'label': 'sku_name', 'type': 'string'},
    {'name': 'From Location', 'label': 'from_loc_name', 'type': 'string'},
    {'name': 'To Location', 'label': 'to_loc_name', 'type': 'string'},
    {'name': 'Quantity', 'label': 'qty', 'type': 'string'},
    {'name': 'Status', 'label': 'status', 'type': 'string'},
    {'name': 'Supplier', 'label': 'supplier_name', 'type': 'string'},
    {'name': 'Created By', 'label': 'created_by', 'type': 'string'},
    {'name': 'Created Date', 'label': 'created_date', 'type': 'datetime'},
    {'name': 'Modified By', 'label': 'modified_by', 'type': 'string'},
    {'name': 'Modified Date', 'label': 'modified_date', 'type': 'datetime'}
]

WHSKU_DATA_TO_BE_EXPORTED = [
    {'name': 'ID', 'label': 'id', 'type': 'string'},
    {'name': 'Name', 'label': 'name', 'type': 'string'},
    {'name': 'Description', 'label': 'description', 'type': 'string'},
    {'name': 'Product Group', 'label': 'product_group_name', 'type': 'string'},
    {'name': 'Supplier', 'label': 'supplier_name', 'type': 'string'},
    {'name': 'Client', 'label': 'client_name', 'type': 'string'},
    {'name': 'Created By', 'label': 'created_by', 'type': 'string'},
    {'name': 'Created Date', 'label': 'created_date', 'type': 'datetime'},
    {'name': 'Modified By', 'label': 'modified_by', 'type': 'string'},
    {'name': 'Modified Date', 'label': 'modified_date', 'type': 'datetime'}
]


