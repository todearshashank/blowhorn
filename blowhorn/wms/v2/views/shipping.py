import json
from django.db import transaction
from django.db.models import F, Q
from blowhorn.payment.constants import PAYMENT_COLLECTED_FROM_CUSTOMER
from blowhorn.payment.models import OrderCODPayment
from blowhorn.payment.payment_bank.helpers import cod_orderlevel_save
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated


from blowhorn.apps.operation_app.v5.views.user import DriverHandoverItems
from blowhorn.legacy.util import LegacyCipher
from blowhorn.wms.submodels.returnorder import ReturnOrder, ReturnOrderLine
from blowhorn.wms.views.receiving import Receiving
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.models import CashHandover, Driver
from blowhorn.order.models import Order, Event, OrderPaymentHistory
from blowhorn.oscar.core.loading import get_model
from blowhorn.trip.models import Trip, StopOrders
from blowhorn.users.permission import IsActive
from blowhorn.wms.constants import INV_LOCKED, WMS_RECEIVED
from blowhorn.wms.submodels.inventory import Inventory
from blowhorn.wms.submodels.location import WhSite
from blowhorn.wms.v1.views.wms import get_user_permission
from blowhorn.wms.services.stockcheck import StockCheckService
from config.settings.status_pipelines import  REACHED_AT_HUB_RTO

OrderLine = get_model('order', 'OrderLine')

class ReturnOrderView(views.APIView):
    permission_classes = (IsActive, IsAuthenticated, )

    def get_pending_amt(self, driver, trip):
        order_ids = StopOrders.objects.filter(trip=trip).values_list('order_id', flat=True)
        query = Q(driver=driver, stops__trip=trip, status__in=[StatusPipeline.ORDER_RETURNED,
                                                               StatusPipeline.UNABLE_TO_DELIVER,
                                                               StatusPipeline.ORDER_PICKED,
                                                               StatusPipeline.ORDER_MOVING_TO_HUB,
                                                               StatusPipeline.ORDER_MOVING_TO_HUB_RTO])

        query |= Q(driver=driver, id__in=order_ids, status__in=[StatusPipeline.ORDER_RETURNED,
                                                               StatusPipeline.UNABLE_TO_DELIVER,
                                                               StatusPipeline.ORDER_PICKED,
                                                               StatusPipeline.ORDER_MOVING_TO_HUB,
                                                               StatusPipeline.ORDER_MOVING_TO_HUB_RTO])

        orders = Order.objects.filter(query).distinct('id')
        orders = orders.prefetch_related('order_lines')
        resp = {
                "trip_number": trip.trip_number if trip else '',
                "driver_name": driver.name,
                "driver_id": driver.id,
                "driver_vehicle": driver.driver_vehicle,
                "cod_amt": driver.cod_balance,
            }
        order_details = []
        for order in orders:
            shipping_address = order.shipping_address
            order_details.append({
                "order_number": order.number,
                "status": order.status,
                "reference_number": order.reference_number,
                "customer_name": order.customer.name,
                "order_amt": order.cash_on_delivery,
                "shipping_address": {
                    "address": {
                        "line1": shipping_address.line1,
                        "line3": shipping_address.line3,
                        "line4": shipping_address.state,
                        "postcode": shipping_address.postcode,
                    },
                    "contact": {
                        "mobile": str(shipping_address.phone_number),
                        "name": shipping_address.name

                    }
                },
                "order_lines": [{"line_id": line.id,
                                 "sku_name": line.wh_sku.name if line.wh_sku else line.sku.name,
                                 "sku_image": line.wh_sku.file.url if line.wh_sku and line.wh_sku.file else '',
                                 "is_wh_sku": True if line.wh_sku else False,
                                 "sku_id": line.wh_sku_id,
                                 "selling_price": line.total_item_price,
                                 "tracking_level": line.tracking_level or line.trk_level,
                                 "qty": line.quantity} for line in order.order_lines.all().annotate(
                        trk_level=F('wh_sku__pack_config__tracking_level__level_one_name'))]

            })
        resp.update({"order_details": order_details})
        if driver:
            amount = driver.cod_balance
        return resp, amount

    def create_inventory_from_orderline(self, records, user, site_id, return_order, sku_info):

        for record in records:
            manifestlines = record.wmsmanifestline_set.all()
            location = Receiving().get_or_create_location(site_id, client=record.client)

            for line in manifestlines:
                inv_details = json.loads(line.inv_details)
                tag_ = StockCheckService().get_latest_tag(inv_details['tag'])
                tracking_level = record.tracking_level
                if not tracking_level:
                    tracking_level = record.trk_level
                inv = Inventory.objects.create(tag=tag_, sku_id=inv_details['sku'], qty=sku_info[line.order_line_id][0],
                                               weight=inv_details['weight'], expiry_date=sku_info[line.order_line_id][1],
                                               location=location, site_id=site_id, tracking_level=tracking_level,
                                               pack_config_id=inv_details['pack_config'], client_id=inv_details['client'],
                                               supplier_id=inv_details['supplier'], status=INV_LOCKED)
                inv.created_by = user
                inv.modified_by = user
                inv.save()
                ReturnOrderLine.objects.create(return_order=return_order, inventory=inv, order_line=line.order_line)
                Receiving().write_itl(inv, user, tracking_level, record.pack_config_id, status=WMS_RECEIVED)

        return True

    def get(self, request):

        user = request.user
        data = request.GET.dict()
        site_id = data.get('site_id')
        driver_id = data.get('driver_id')
        driver_mobile = data.get('driver_mobile')
        driver = None
        if not driver_mobile:
            resp = {
                "message": "Invalid Mobile Number"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        sites = get_user_permission(user)
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        try:
            if len(driver_mobile) > 10:
                information = LegacyCipher.decode(driver_mobile)
                information = json.loads(information)
                driver_mobile = str(information.get('id'))
            driver = DriverHandoverItems().get_driver(driver_mobile='+91'+driver_mobile, driver_id=driver_id)
        except:
            resp = {
                "message": "Invalid Driver Information"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        if not driver:
            resp = {
                "message": "Driver Not found"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        trip = Trip.objects.filter(driver=driver, status__in=[
            StatusPipeline.TRIP_IN_PROGRESS, StatusPipeline.TRIP_ALL_STOPS_DONE]).first()
        resp, amount = self.get_pending_amt(driver, trip)

        if not resp['trip_number'] and not amount:
            resp = {
                "message": "No orders or cash to be collected from driver"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        return Response(status=status.HTTP_200_OK, data=resp)

    def post(self, request):
        data = request.data
        user = request.user
        site_id = data.get('site_id')
        driver_id = data.get('driver_id')
        trip_number = data.get('trip_number')
        sites = get_user_permission(user)
        cod_amt = data.get('cod_amt') or 0
        if not int(site_id) in sites:
            resp = {
                "message": "User doesn't have Site Permission"
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        driver = Driver.objects.filter(id=driver_id).first()
        order_details = data.get('order_details')
        if cod_amt and float(driver.cod_balance) != float(cod_amt):
            resp = {
                "message": "Driver has to pay %s amount" % (float(driver.cod_balance))
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        line_ids = []
        order_numbers = []
        sku_info = {}
        for order in order_details:
            order_numbers.append(order.get('order_number'))
            for line in order.get('order_lines', []):
                if line.get('is_wh_sku'):
                    line_ids.append(line['line_id'])
                    sku_info.update({line['line_id']: [int(line['physical_qty']) or int(line['qty']),
                                                       line.get('expiry_date', None)]})
        site = WhSite.objects.filter(id=site_id).first()
        orders = Order.objects.filter(number__in=order_numbers)

        with transaction.atomic():
            event_list = []
            rto_orders = []
            at_hub_rto = []
            at_hub = []
            for order in orders:
                if order.status in [StatusPipeline.ORDER_PICKED, StatusPipeline.ORDER_MOVING_TO_HUB,
                                    StatusPipeline.UNABLE_TO_DELIVER]:
                    at_hub.append(order.id)
                    event_status = StatusPipeline.REACHED_AT_HUB
                elif order.status in [StatusPipeline.ORDER_MOVING_TO_HUB_RTO, StatusPipeline.ORDER_RETURNED]:
                    if order.pickup_hub == site.hub:
                        rto_orders.append(order.id)
                        event_status = StatusPipeline.ORDER_RETURNED_RTO
                    else:
                        at_hub_rto.append(order.id)
                        event_status = StatusPipeline.REACHED_AT_HUB_RTO
                event_list.append(Event(status=event_status , hub_associate=user, order=order, driver=driver,
                                  remarks=str(site.hub)))

            Event.objects.bulk_create(event_list)
            if rto_orders or at_hub_rto:
                trip = Trip.objects.filter(trip_number=trip_number).first()
                return_order = ReturnOrder.objects.create(trip=trip, site_id=site_id, amount_collected=cod_amt)
                if rto_orders:
                    Order.objects.filter(id__in=rto_orders).update(status=StatusPipeline.ORDER_RETURNED_RTO,
                                                                   latest_hub=site.hub)
                if at_hub_rto:
                    Order.objects.filter(id__in=at_hub_rto).update(status=REACHED_AT_HUB_RTO, latest_hub=site.hub)
            if at_hub:
                Order.objects.filter(id__in=at_hub).update(status=StatusPipeline.REACHED_AT_HUB,latest_hub=site.hub)

            if rto_orders or at_hub_rto:
                # return orders logic
                if line_ids:
                    records = OrderLine.objects.filter(id__in=line_ids, order__pickup_hub=site.hub).annotate(
                        trk_level=F('wh_sku__pack_config__tracking_level__level_one_name'))
                    self.create_inventory_from_orderline(records, user, site_id, return_order, sku_info)
            
            if cod_amt and float(driver.cod_balance) == float(cod_amt):
                # BLOW-1166
                _trip = Trip.objects.filter(trip_number=trip_number).first()
                cash_handover = CashHandover.objects.create(driver=driver,
                                    collected_by=user,
                                    amount=cod_amt
                                )
                cod_orderlevel_save(
                    driver=driver,
                    trip=_trip,
                    cash_handover=cash_handover
                )

                Driver.objects.filter(id=driver_id).update(cod_balance=0)
                resp = {
                    "message": "Return Received"
                }
            else:
                resp = {
                    "message": "Item Received"
                }
        return Response(status=status.HTTP_200_OK, data=resp)


