import logging
import json

from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from rest_framework import status, generics, serializers, views, filters
from rest_framework.response import Response
from phonenumbers import PhoneNumber
from django.conf import settings


from blowhorn.oscar.core.loading import get_model

from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import AccountUser
from blowhorn.users.models import User
from blowhorn.wms.submodels.inventory import WhSite


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

ProductGroup = get_model('wms', 'ProductGroup')
Hub = get_model('address', 'Hub')


def get_user_permission(user, wh_site):
    customer = CustomerMixin().get_customer(user)
    ac = AccountUser.objects.filter(user=user).first()
    is_org_admin = user.is_customer() or (customer and customer.is_admin(user))
    if customer and (ac and ac.is_admin) or is_org_admin:
        # handle customer admin
        return WhSite.objects.filter(id=wh_site).exists()
    permission = CustomerMixin().get_permission_from_cache(user, reset_cache=True)

    if not isinstance(permission, dict):
        permission = json.loads(permission)
    # sites, wh_sites = [], []
    site_permission = permission['WMS'].get('sites', None)
    if site_permission:
        sites = site_permission.get('site', [])
        return wh_site in sites
    return False


def get_login_response(user, token, wh_site):

    response = {
        'name': user.name,
        'mobile': str(user.phone_number),
        'email': user.email,
        'mAuthToken': token,
        'site': {"name": wh_site.name, "id": wh_site.id}

    }
    return response


class Login(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request):
        data = request.data
        mobile = data.get('mobile')
        password = data.get('password')
        qr_code = data.get('hub_qr_code')
        if not mobile or not password:
            resp = {
                "message": 'Mobile number and password is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
        wh_site = WhSite.objects.filter(qr_code=qr_code).first()
        if not wh_site:
            resp = {
                "message": 'Invalid Site QR code'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

        phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, mobile)
        user = User.objects.filter(phone_number=phone_number, is_staff=False).first()
        if user:
            if not user.is_active and user.login_attempt > settings.INVALID_OTP_ATTEMPTS_ALLOWED:
                resp = {
                    "message": 'User is blocked, Please contact support'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)

            login_response = User.objects.do_login(
                request=request, email=user.email, password=password)

            if login_response.status_code == status.HTTP_200_OK:
                user = login_response.data.get('user')
                has_permission = get_user_permission(user, wh_site.id)
                if not has_permission:
                    resp = {
                        "message": "User doesn't have Site Permission"
                    }
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
                token = login_response.data.get('token')
                response = get_login_response(user, token, wh_site)
                return Response(status=status.HTTP_200_OK, data=response)
            resp = {
                "message": login_response.data
            }
            return Response(status=login_response.status_code, data=resp)

        resp = {
            "message": 'User Not Registered'
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=resp)
