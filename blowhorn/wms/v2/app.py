from django.conf.urls import url, include

from blowhorn.wms.v2.views import wms as warehouse_views
from blowhorn.wms.v2.views import  shipping
from blowhorn.wms.views import location
urlpatterns = [

    url(r'^login$', warehouse_views.Login.as_view(), name='wms-login-v1'),
    url(r'^hub-receiving', shipping.ReturnOrderView.as_view(), name='return-order'),
    url(r'^location/zone$', location.LocationZoneView.as_view(), name='location-zone'),
    url(r'^location/zone/(?P<pk>[0-9]+)$', location.LocationZoneView.as_view(), name='location-zone'),
    url(r'^location/zone/list$', location.LocationZoneListView.as_view(), name='location-zone-list'),
    url(r'^location/list', location.LocationListView.as_view(), name='location-list'),
    url(r'^location/(?P<pk>[0-9]+)$', location.LocationView.as_view(), name='location-update'),
    url(r'^location$', location.LocationView.as_view(), name='location'),
    url(r'^location/type/list', location.LocationTypeListView.as_view(), name='location-type-list'),
    url(r'^location/type$', location.LocationTypeView.as_view(), name='location-type'),
    url(r'^location/type/(?P<pk>[0-9]+)$', location.LocationTypeView.as_view(), name='location-type-list')

]
