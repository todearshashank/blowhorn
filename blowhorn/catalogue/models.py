"""
Vanilla product models
"""
from blowhorn.catalogue.abstract_models import *
from blowhorn.catalogue.abstract_models import *  # noqa
from blowhorn.oscar.core.loading import is_model_registered, get_class
from django.db.models import CharField
from blowhorn.document.utils import generate_file_path
from django.utils.translation import ugettext_lazy as _

ProductAttributesContainer = get_class(
    'catalogue.product_attributes', 'ProductAttributesContainer')

__all__ = ['ProductAttributesContainer']


class ProductClass(AbstractProductClass):
    caption = CharField(max_length=50, default='-')
    image = models.FileField(_("Product Image"), null=True,
                             blank=True, upload_to=generate_file_path,
                             max_length=256)
    is_active = models.BooleanField(default=False)


__all__.append('ProductClass')


class Category(AbstractCategory):
    path = CharField(max_length=255, default='/')
    depth = models.IntegerField(_("Depth"), default=0)
    numchild = models.IntegerField(_("Number of Child Categories"), default=0)
    # is_root = models.BooleanField(default=False)


__all__.append('Category')


class ProductCategory(AbstractProductCategory):
    pass


__all__.append('ProductCategory')


class Product(AbstractProduct):
    availability_role = models.CharField(max_length=128, null=True, blank=True)
    pass


__all__.append('Product')


class ProductRecommendation(AbstractProductRecommendation):
    pass


__all__.append('ProductRecommendation')


class ProductAttribute(AbstractProductAttribute):
    pass


__all__.append('ProductAttribute')


class ProductAttributeValue(AbstractProductAttributeValue):
    pass


__all__.append('ProductAttributeValue')


class AttributeOptionGroup(AbstractAttributeOptionGroup):
    pass


__all__.append('AttributeOptionGroup')


class AttributeOption(AbstractAttributeOption):
    pass


__all__.append('AttributeOption')


class Option(AbstractOption):
    pass


__all__.append('Option')


class ProductImage(AbstractProductImage):
    pass


__all__.append('ProductImage')
