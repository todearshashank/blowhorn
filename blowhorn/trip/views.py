# System and Django libraries
import os
import logging
import traceback
import pdfkit
from datetime import datetime
from django.conf import settings
from django.db.models import Q, Prefetch
from django.db import transaction
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _

# 3rd Party libraries
from rest_condition import Or
from rest_framework import status
from rest_framework import generics, views
from blowhorn.oscar.core.loading import get_model
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from rest_framework import exceptions
from rest_framework.parsers import MultiPartParser, FormParser

# Blowhorn libraries
from .serializers import (
    TripSerializer,
    ShippingAddressSerializer
)
from config.settings import status_pipelines as StatusPipeline
from .mixins import TripMixin
from blowhorn.common.helper import CommonHelper
from blowhorn.common.notification import Notification
from blowhorn.common.utils import export_to_spreadsheet
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.customer.permissions import IsNonIndividualCustomer
from .utils import TripService
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.order.utils import OrderUtil
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_SHIPMENT
from blowhorn.utils.functions import utc_to_ist
from blowhorn.driver.permissions import IsActiveDriver
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.permissions import IsCustomer
from blowhorn.customer.constants import TRIP_STOPS_DATA_TO_BE_IMPORTED
from blowhorn.contract.constants import REACH_LOCATION, END_TRIP
from blowhorn.contract.models import Screen
from blowhorn.trip.models import TripConstants, Stop, ConsignmentNote
from blowhorn.trip.redis_models import Trip as RedisTrip
from blowhorn.trip.services.trip_stops_import import TripStopsImport
from blowhorn.trip.utils import TripConsignment


Trip = get_model('trip', 'Trip')
TripDocument = get_model('trip', 'TripDocument')
Customer = get_model('customer', 'Customer')
Stop = get_model('trip', 'Stop')
Order = get_model('order', 'Order')
OrderLine = get_model('order', 'OrderLine')
ShippingAddress = get_model('order', 'ShippingAddress')
WayPoint = get_model('order', 'WayPoint')
AppEvents = get_model('trip', 'AppEvents')
DeliveredAddress = get_model('order', 'DeliveredAddress')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TripList(generics.CreateAPIView, CustomerMixin):
    permission_classes = (AllowAny, )
    serializer_class = TripSerializer
    queryset = serializer_class.queryset

    def get(self, request):
        trip_qs, order_qs = self.get_queryset()

        _dict = self.serializer_class().get_mytrips(
            request=request, trip_qs=trip_qs, order_qs=order_qs)

        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS'
        }
        return Response(status=status.HTTP_200_OK, data=_dict, headers=headers)

    def get_queryset(self):
        user = self.request.user
        customer = self.get_customer(user)
        if not customer:
            return self.queryset.none()

        trip_qs = Trip._default_manager.filter(
            Q(order__isnull=True) & Q(
                customer_contract__customer=customer) & Q(
                status__in=[StatusPipeline.TRIP_NEW,
                            StatusPipeline.TRIP_COMPLETED,
                            StatusPipeline.TRIP_IN_PROGRESS,
                            StatusPipeline.DRIVER_ACCEPTED,
                            StatusPipeline.TRIP_ALL_STOPS_DONE])
        )
        if trip_qs.exists():
            trip_qs = trip_qs.select_related('driver', 'driver__user')
            trip_qs = trip_qs.select_related(
                'vehicle', 'vehicle__vehicle_model')
            trip_qs = trip_qs.select_related('hub')
            trip_qs = trip_qs.select_related(
                'customer_contract', 'customer_contract__city')
            trip_qs = trip_qs.prefetch_related(
                'customer_contract__vehicle_classes')
            trip_qs = trip_qs.prefetch_related(
                Prefetch('stops',
                         queryset=Stop.objects.all().extra(
                             select={
                                 'start_time_null': 'start_time is null'},
                             order_by=['start_time']),
                         )
            )
            trip_qs = trip_qs.prefetch_related(
                Prefetch(
                    'stops__waypoint',
                    queryset=WayPoint.objects.select_related(
                        'shipping_address', 'order_line', 'order')
                )
            )
            trip_qs = list(trip_qs)

        order_qs = Order._default_manager.filter(
            Q(customer=customer) &
            ~Q(status=StatusPipeline.ORDER_CANCELLED)
        ).exclude(
            status=StatusPipeline.ORDER_CANCELLED)
        if order_qs.exists():

            order_qs = order_qs.select_related(
                'driver__user', 'pickup_address', 'customer__user',
                'vehicle_class_preference', 'shipping_address', 'labour'
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'waypoint_set',
                    queryset=WayPoint.objects.select_related(
                        'shipping_address', 'order_line')
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'trip_set',
                    queryset=Trip.objects.filter(
                        status__in=[
                            StatusPipeline.TRIP_NEW,
                            StatusPipeline.TRIP_IN_PROGRESS,
                            StatusPipeline.TRIP_COMPLETED,
                            StatusPipeline.DRIVER_ACCEPTED,
                            StatusPipeline.TRIP_ALL_STOPS_DONE
                        ]
                    ).select_related(
                        'driver__user',
                        'vehicle__vehicle_model__vehicle_class', 'hub'
                    ).prefetch_related(
                        Prefetch(
                            'stops',
                            queryset=Stop.objects.select_related('waypoint'))
                    ),
                    to_attr='trips'
                )
            )
            order_qs = list(order_qs)

        return trip_qs, order_qs


class TripTrackingView(views.APIView):
    permission_classes = (AllowAny, )
    serializer_class = TripSerializer
    queryset = serializer_class.queryset
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, number=None):
        return self.get_result(request, number=None)

    def post(self, request, number=None):
        return self.get_result(request, number=None)

    def get_result(self, request, number=None):
        response = None
        trip_qs, order_qs = self.get_queryset()
        if trip_qs.exists():
            response = TripService().get_trip_details_for_customer(
                request, trip_qs.first(), route_info=True)
        elif order_qs.exists():
            order = order_qs.first()
            if order.order_type == settings.SHIPMENT_ORDER_TYPE:
                order = order_qs.prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related(
                            'trip', 'trip__vehicle', 'trip__driver')
                    )
                )
                response = OrderUtil().get_shipment_order_details_for_customer(
                    order.first(), route_info=False)
            else:
                response = OrderUtil().get_booking_details_for_customer(
                    request, order, route_info=True)

        if response:
            return Response(status=status.HTTP_200_OK, data=response,
                            content_type='text/html; charset=utf-8')

        response = {
            'status': 'FAIL',
            'message': 'Trip doesn\'t exist'
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')

    class Meta:
        lookup_field = 'trip__trip_number'
        lookup_url_kwarg = 'trip__trip_number'

    def get_queryset(self):
        number = self.kwargs.pop('number')
        trip_qs = None
        order_qs = None

        trip_qs = Trip._default_manager.filter(
            trip_number=number)
        if trip_qs.exists():
            trip_qs = trip_qs.select_related('driver', 'driver__user')
            trip_qs = trip_qs.select_related(
                'vehicle', 'vehicle__vehicle_model')
            trip_qs = trip_qs.select_related('hub')
            trip_qs = trip_qs.select_related(
                'customer_contract', 'customer_contract__city')
            trip_qs = trip_qs.prefetch_related(
                'customer_contract__vehicle_classes')
            trip_qs = trip_qs.prefetch_related(
                Prefetch('stops',
                    queryset=Stop.objects.all().extra(
                        select={
                            'start_time_null': 'start_time is null'},
                        order_by=['start_time']),
                    )
            )
            trip_qs = trip_qs.prefetch_related(
                Prefetch(
                    'stops__waypoint',
                    queryset=WayPoint.objects.select_related(
                        'shipping_address', 'order_line', 'order')
                )
            )
        else:
            order_qs = Order._default_manager.filter(number=number)
            order_qs = order_qs.select_related(
                'driver__user', 'pickup_address', 'shipping_address',
                'vehicle_class_preference', 'customer__user', 'labour'
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'waypoint_set',
                    queryset=WayPoint.objects.select_related(
                        'shipping_address', 'order_line')
                )
            )
            order_qs = order_qs.prefetch_related(
                Prefetch(
                    'trip_set',
                    queryset=Trip.objects.filter(
                        status__in=[
                            StatusPipeline.TRIP_NEW,
                            StatusPipeline.TRIP_IN_PROGRESS,
                            StatusPipeline.TRIP_COMPLETED,
                            StatusPipeline.DRIVER_ACCEPTED,
                            StatusPipeline.TRIP_ALL_STOPS_DONE
                        ]
                    ).select_related(
                        'driver__user',
                        'vehicle__vehicle_model__vehicle_class', 'hub'
                    ).prefetch_related(
                        Prefetch(
                            'stops',
                            queryset=Stop.objects.select_related('waypoint'))
                    ),
                    to_attr='trips'
                )
            )

        return trip_qs, order_qs


class TripDepartureSheetView(generics.RetrieveAPIView):
    """API to get departure sheet data for trips with shipment type contracts."""

    serializer_class = TripSerializer

    def get(self, request, number=None):
        response = None
        trip_qs = self.get_queryset()
        if trip_qs.exists():
            response = self.get_sheet_data(trip=trip_qs.first())
        if response:
            return Response(status=status.HTTP_200_OK, data=response,
                            content_type='text/html; charset=utf-8')

        response = {
            'status': 'FAIL',
            'message': 'Trip doesn\'t exist'
        }

        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')

    def get_sheet_data(self, trip):

        sheet_data = {}

        driver_name = '%s' % trip.driver.name.title() if trip.driver else ''
        sheet_data['driver_name'] = driver_name
        sheet_data['vehicle_number'] = '%s' % trip.vehicle if trip.driver else ''
        sheet_data['date_time'] = utc_to_ist(
            trip.planned_start_time).strftime(
                "%Y-%m-%d %H:%M")

        shipments = []
        for stop in trip.stops_list:
            shipment_data = {}

            order = stop.order
            address = stop.waypoint.shipping_address if stop.waypoint else stop.order.shipping_address

            customer_data = {}
            customer_data['first_name'] = address.first_name
            customer_data['last_name'] = address.last_name
            customer_data['line1'] = address.line1
            customer_data['phone_number'] = \
                address.phone_number.national_number \
                if address.phone_number else ''

            shipment_data['tracking_id'] = order.reference_number if \
                order.reference_number else order.customer_reference_number
            shipment_data['company'] = '%s' % order.customer
            shipment_data['payment_mode'] = order.payment_mode
            shipment_data['customer'] = customer_data
            shipment_data['order_no'] = order.number
            shipment_data['delivery_instructions'] = order.delivery_instructions

            shipment_data['sku_details'] = self._get_sku_details(order)

            shipments.append(shipment_data)

        sheet_data['shipments'] = shipments

        return sheet_data

    class Meta:
        lookup_field = 'trip__trip_number'
        lookup_url_kwarg = 'trip__trip_number'

    def _get_sku_details(self, order):
        return OrderLine.objects.filter(order=order)\
            .select_related('sku')\
            .values('sku__name', 'quantity')

    def get_queryset(self):
        customer_query = Q()
        if not self.request.user.is_staff:
            customer = CustomerMixin().get_customer(self.request.user)
            customer_query = Q(order__customer=customer)
        number = self.kwargs.pop('number')
        trip_qs = Trip._default_manager.filter(
            trip_number=number,
            customer_contract__contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL])
        if trip_qs.exists():
            trip_qs = trip_qs.select_related(
                'customer_contract', 'driver', 'vehicle')
            trip_qs = trip_qs.prefetch_related(
                Prefetch('stops',
                         queryset=Stop.objects.filter(customer_query).all().extra(
                             select={
                                 'start_time_null': 'start_time is null '},
                             order_by=['sequence']),
                         to_attr='stops_list'
                         )
            )
            trip_qs = trip_qs.prefetch_related(
                Prefetch(
                    'stops__waypoint',
                    queryset=WayPoint.objects.select_related(
                        'shipping_address', 'order')
                )
            )

        return trip_qs


class TripSummary(generics.RetrieveAPIView, CustomerMixin):
    """API to get summary of trips in end statuses of a particular customer."""

    serializer_class = TripSerializer

    def get(self, request):

        results = self.get_queryset() or []
        summary = TripMixin().get_summary(results)
        headers = settings.HEADER

        return Response(status=status.HTTP_200_OK,
                        data=summary, headers=headers)

    def get_queryset(self):
        user = self.request.user
        customer = self.get_customer(user)
        if not customer:
            return self.queryset.none()

        trip_qs = Trip._default_manager.filter(
            (Q(order__customer=customer) |
             Q(customer_contract__customer=customer)
             ) & Q(order__order_type=settings.C2C_ORDER_TYPE) &
            Q(status=StatusPipeline.TRIP_COMPLETED)
        )
        trip_qs = trip_qs.select_related('order')

        return trip_qs


class TripStopEdit(views.APIView):
    permission_classes = (IsCustomer, )
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, number):
        from .serializers import TripStopEditSerializer

        order = Order.objects.prefetch_related('trip_set')
        order = order.prefetch_related('stops')
        order = order.filter(
            number=number,
            status__in=[
                StatusPipeline.ORDER_NEW, StatusPipeline.DRIVER_ACCEPTED,
                StatusPipeline.DRIVER_ARRIVED_AT_PICKUP, StatusPipeline.OUT_FOR_DELIVERY,
                StatusPipeline.DRIVER_ARRIVED_AT_DROPOFF
            ]
        ).first()
        if order:
            serializer_data = TripStopEditSerializer(order, context={'request': request})
            return Response(status=status.HTTP_200_OK, data=serializer_data.data)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def __get_address_data(self, address):
        phone_number = address.get('phone_number', '')
        if phone_number and not phone_number.startswith(
                settings.ACTIVE_COUNTRY_CODE):
            address.update(
                {'phone_number': settings.ACTIVE_COUNTRY_CODE + phone_number})

        latlng = address.pop('latlng', '')
        if latlng:
            lat = latlng[0]
            lng = latlng[1]
            address.update({'geopoint': CommonHelper.get_geopoint_from_latlong(lat, lng)})

        search_text = "%s%s%s%s%s%s" % (address.get('line1', ''), address.get('line2', ''),
                        address.get('line3', ''), address.get('line4', ''),
                        address.get('postcode', ''), address.get('state', ''))

        # search_text = address.get('line1', '') + address.get('line2', '') + \
        #     address.get('line3', '') + address.get('line4', '') + \
        #     address.get('postcode', '') + address.get('state', '')

        address['search_text'] = search_text
        return address

    def __get_address(self, address_id, address_data, order_data, sequence_id, waypoints):
        if address_id:
            # update address
            ShippingAddress.objects.filter(pk=address_id).update(**address_data)
        else:
            # create new address
            address = ShippingAddressSerializer(data=address_data)
            if address.is_valid(raise_exception=True):
                address = address.save()
                address_id = address.id
        if sequence_id == len(waypoints) - 1:
            order_data['shipping_address_id'] = address_id

        return address_id

    def put(self, request, number):
        data = request.data
        order_data = {}
        invalid_request = False
        order = Order.objects.get(number=number)
        waypoints = data.get('stops', [])
        completed_stops = len(data.get('completed_stops', []))
        # changed for trip edit issue
        # if Stop.objects.filter(order=order, status=StatusPipeline.TRIP_COMPLETED).count() != completed_stops:
        #     invalid_request = False
        delete_waypnt_ids = data.get('deleted_ids', [])
        invalid_del_waypoints = Stop.objects.filter(status=StatusPipeline.TRIP_COMPLETED,
                                                    waypoint_id__in=delete_waypnt_ids).exists()
        trip = Trip.objects.filter(order=order, status__in=[StatusPipeline.TRIP_NEW,
                                                            StatusPipeline.DRIVER_ACCEPTED,
                                                            StatusPipeline.TRIP_IN_PROGRESS,
                                                            StatusPipeline.TRIP_ALL_STOPS_DONE]).first()

        if invalid_request:
            raise exceptions.ValidationError("Please refresh the page - Trip data changed")

        if invalid_del_waypoints:
            raise exceptions.ValidationError("Please refresh the page - The stop being deleted is already completed")

        if order.status in StatusPipeline.ORDER_END_STATUSES:
            raise exceptions.ValidationError("Please refresh the page - The order has been already completed")

        if not trip:
            raise exceptions.ValidationError("Please refresh the page - Trip data changed/deleted")

        with transaction.atomic():
            Stop.objects.filter(waypoint_id__in=delete_waypnt_ids).delete()
            WayPoint.objects.filter(id__in=delete_waypnt_ids).delete()

            for waypoint in sorted(waypoints, key=lambda i: i['sequence_id']):
                if waypoint.get('completed', False):
                    continue
                waypoint_id = waypoint.get('id', None)
                sequence_id = waypoint.get('sequence_id', None)
                address = waypoint.get('shipping_address', {})
                address_id = address.get('id', None)
                address_data = self.__get_address_data(address)

                if sequence_id == 0:
                    if trip.status not in [StatusPipeline.TRIP_NEW, StatusPipeline.DRIVER_ACCEPTED]:
                        raise exceptions.ValidationError("Please refresh the page - Trip status is invalid")
                    if waypoint_id:
                        # as there is no waypoint for pickupaddress
                        Stop.objects.filter(waypoint_id=waypoint_id).delete()
                        WayPoint.objects.filter(id=waypoint_id).delete()
                    address = ShippingAddressSerializer(data=address_data)
                    if address.is_valid(raise_exception=True):
                        pickup_address = address.save()
                        order_data['pickup_address_id'] = pickup_address.id
                else:
                    address_id = self.__get_address(address_id, address_data, order_data, sequence_id, waypoints)

                    if waypoint_id:
                        # update waypoint with address and sequence id
                        WayPoint.objects.filter(pk=waypoint_id).update(
                            sequence_id=sequence_id,
                            shipping_address_id=address_id
                        )
                        Stop.objects.filter(
                            waypoint_id=waypoint_id,
                            trip=trip,
                            status__in=[
                                StatusPipeline.TRIP_NEW,
                                StatusPipeline.TRIP_IN_PROGRESS
                            ]
                        ).update(sequence=sequence_id, status=StatusPipeline.TRIP_NEW)
                    else:
                        # create new waypoint
                        waypoint_data = {
                            'sequence_id': sequence_id,
                            'order_id': order.id,
                            'shipping_address_id': address_id
                        }
                        waypoint_obj = WayPoint(**waypoint_data)
                        waypoint_obj.save()
                        Stop.objects.create(
                            trip=trip, waypoint=waypoint_obj, order=order,
                            status=StatusPipeline.TRIP_NEW, sequence=sequence_id)

            Order.objects.filter(pk=order.id).update(**order_data) if order_data else None

            if trip.status not in [StatusPipeline.TRIP_NEW, StatusPipeline.DRIVER_ACCEPTED]:
                stop_with_smallest_sequence = Stop.objects.filter(
                    trip=trip,
                    status=StatusPipeline.TRIP_NEW
                ).order_by('sequence').first()
                if stop_with_smallest_sequence:
                    stop_with_smallest_sequence.status = StatusPipeline.TRIP_IN_PROGRESS
                    stop_with_smallest_sequence.save()
                    current_step = Screen.objects.get(
                        name=REACH_LOCATION, trip_workflow=trip.trip_workflow).sequence
                    Trip.objects.filter(pk=trip.id).update(
                        current_step=current_step, status=StatusPipeline.TRIP_IN_PROGRESS)
                else:
                    current_step = Screen.objects.get(name=END_TRIP, trip_workflow=trip.trip_workflow).sequence
                    Trip.objects.filter(pk=trip.id).update(
                        current_step=current_step, status=StatusPipeline.TRIP_ALL_STOPS_DONE)
            if order.driver:
                dict_ = {
                    'username': order.driver_id,
                    'action': 'restore'
                }
                Notification.send_action_to_driver(order.driver.user, dict_)
            message = 'Trip %s updated successfully' % (order.number)
            return Response(status=status.HTTP_200_OK, data=message)


class TripDocumentUpload(views.APIView):
    permission_classes = (IsActiveDriver,)

    def post(self, request, trip_id):
        data = request.data.dict()
        amount = float(data.get('amount', 0))
        ocr_reading = data.get('ocr_reading', None)
        raw_data = data.get('raw_data', None)
        stop_id = data.get('stop_id', None)
        stop = None

        limit = float(TripConstants().get('TRIP_EXTRA_CHARGE_LIMIT', 200))
        if amount > limit:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Amount exceeded the limit')

        try:
            trip = Trip.objects.get(pk=trip_id)
        except BaseException:
            logger.error(traceback.format_exc())
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid Trip')
        if stop_id:
            stop = Stop.objects.filter(id=stop_id).first()

        file = data.get('file', None)
        document_type = data.get('document_type', None)

        if not file or not document_type:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid Data')

        response_data = {
            'trip_id': trip.id,
            'document_type': document_type
        }

        # if TripDocument.objects.filter(trip=trip, document_type=document_type).exists():
        #     return Response(status=status.HTTP_200_OK, data=response_data)
        trip_document_data = {
            'trip': trip,
            'document_type': document_type,
            'file': file,
            'amount': amount,
            'ocr_reading': ocr_reading,
            'raw_data': raw_data,
            'stop': stop
        }
        trip_document = TripDocument.objects.create(**trip_document_data)
        try:
            RedisTrip(trip.pk).update_documents([trip_document.to_json()])
        except BaseException:
            logger.info('Failed to store document --> Trip: %s, %s' % (trip.pk, trip_document.pk))

        trip.update_toll_and_parking()
        trip.save()

        return Response(status=status.HTTP_200_OK, data=response_data)


class StopsExportFileFormatAPIView(views.APIView, CustomerMixin):

    permission_classes = (IsAuthenticated, Or(
        IsBlowhornStaff, IsNonIndividualCustomer),)

    def get_filename(self, user_name):
        file = 'myfleet_trip_stops_' + str(user_name) + '_' + \
               str(datetime.now().strftime(settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        return filename

    def get(self, request):
        customer = self.get_customer(request.user)
        data_mapping = TRIP_STOPS_DATA_TO_BE_IMPORTED
        filename = self.get_filename(customer.name)
        response = {'data': '', 'errors': ''}
        input_data = {
            'data': [],
            'filename': filename,
            'data_mapping_constant': data_mapping,
            'only_format': False
        }

        file_path = export_to_spreadsheet(**input_data)

        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            response['errors'] = 'No file generated'
            status_code = status.HTTP_400_BAD_REQUEST
        return Response(response, status_code)


class StopsImportFileView(views.APIView, TripStopsImport):
    """
         Description     : Import the data from the file provided
         Request type    : POST
         Params          : trip_number
         Response        : Raise error if any issues else OK
    """

    permission_classes = (IsAuthenticated,
                          Or(IsBlowhornStaff, IsNonIndividualCustomer),)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, trip_number):
        data = request.data.dict()
        logger.info('Request: %s' % data)
        self._intialize_attributes(data, trip_number)

        if not self.values_list:
            if len(self.response['errors']) == 0:
                self.response['errors'] = _('No data found in file')
                self.status_code = status.HTTP_400_BAD_REQUEST
        else:
            self._generate_stops_from_data(
                self.values_list, self.filename, self.no_of_rows - 1)
        return Response(self.response, self.status_code)


class TripConsignmentNoteView(views.APIView, TripConsignment):
    """
        Description        : Download Trip consignment note
        Request type       : POST
        Params             : trip_number
        Response           : Consignment note PDF
    """

    permission_classes = (IsAuthenticated,
                          Or(IsBlowhornStaff, IsNonIndividualCustomer),)

    def get(self, request, trip_number):
        data = request.data
        response = {'data': '', 'errors': ''}
        trip = Trip.objects.select_related(
            'customer_contract', 'order',
            'customer_contract__customer',
            'order__pickup_address').filter(trip_number=trip_number).first()

        if not trip:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_('Trip not found'))

        if trip.has_consignment_note:
            consignment_note = ConsignmentNote.objects.filter(trip_id=trip.id).first()
        else:
            trip.has_consignment_note = True
            trip.save()

            content = self.get_consignment_note_content(trip, is_pdf_format=True)
            consignment_note = ConsignmentNote.objects.create(trip=trip, content=content)

        file = open("temp.txt", 'w')
        file.write(consignment_note.content)
        file.close()

        filename = '%s_consignment_note' % trip_number + '.pdf'

        pdf_file = pdfkit.from_file("temp.txt", filename)

        os.remove("temp.txt")

        with open(filename, "rb") as fh:
            response = HttpResponse(
                fh.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'inline; filename=' + \
                                              os.path.basename(filename)
            os.remove(filename)
            return response


