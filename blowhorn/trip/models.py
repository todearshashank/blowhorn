# Django and System libraries
import operator
from datetime import timedelta
from functools import reduce
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models
from django.utils import timezone
from django.db.models import Q, Prefetch, F
from django.conf import settings
from django.core.exceptions import ValidationError
from django.contrib.gis.geos import MultiLineString
from django.contrib.postgres.indexes import BrinIndex
from rest_framework.response import Response
from rest_framework import status

# 3rd party libraries
from django_fsm import FSMField, transition
from auditlog.models import AuditlogHistoryField
from postgres_copy import CopyManager

# blowhorn imports
from blowhorn.common.helper import CommonHelper
from blowhorn.driver.models import Driver, DriverPayment
from blowhorn.driver.redis_models import Driver as RedisDriver
from blowhorn.vehicle.models import Vehicle, Vendor
from config.settings import status_pipelines as StatusPipeline
from blowhorn.users.models import User
from blowhorn.common.models import BaseModel
from .trip_status_transition_conditions import (
    is_trip_spoc_or_supervisor,
    is_b2b_trip, can_complete_trip
)
from .constants import AVG_SPEED_OF_VEHICLE_KMS, \
    MAX_ALLOWED_TOTAL_TRIP_TIME_MINUTES, STOP_TYPES
from blowhorn.common.middleware import current_request
from blowhorn.utils.functions import utc_to_ist, minutes_to_dhms
from .helper import generate_file_path
from blowhorn.trip.constants import START_ODOMETER_PROOF, TRIP_DOCUMENT_TYPES
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_KIOSK, TOLL_CHARGE, PARKING_CHARGE, CONTRACT_TYPE_SHIPMENT
from blowhorn.document.file_validator import FileValidator
from blowhorn.customer.submodels.invoice_models import CustomerInvoice
from blowhorn.customer.constants import INVOICE_EDITABLE_STATUS
from django.contrib.postgres.fields import JSONField, ArrayField
from blowhorn.trip.signals import *

TRIP_CODE_PREFIX = 'TRIP'

TRIP_SUSPENSION_REASONS_VEHICLE_BREAKDOWN = u'Vehicle breakdown'
TRIP_SUSPENSION_REASONS_DRIVER_DENIED_DUTY = u'Driver denied Duty'
TRIP_SUSPENSION_REASONS_SICK = u'Sick'
TRIP_SUSPENSION_REASONS_FAMILY_ISSUE = u'Family Issues'
TRIP_SUSPENSION_REASONS_TRAFFIC_VIOLATION = u'Traffic violation'
TRIP_SUSPENSION_REASONS_NO_FUEL = u'No Fuel'
TRIP_SUSPENSION_REASONS_ACCIDENT = u'Accident'
TRIP_SUSPENSION_REASONS_CUSTOMER_CANCELLED = u'Customer cancelled'
TRIP_SUSPENSION_REASONS_TRIP_INTERRUPTED = u'Trip Interrupted'

TRIP_SUSPENSION_REASONS = (
    (TRIP_SUSPENSION_REASONS_VEHICLE_BREAKDOWN, _('Vehicle breakdown')),
    (TRIP_SUSPENSION_REASONS_DRIVER_DENIED_DUTY, _('Driver denied Duty')),
    (TRIP_SUSPENSION_REASONS_SICK, _('Sick')),
    (TRIP_SUSPENSION_REASONS_FAMILY_ISSUE, _('Family Issues')),
    (TRIP_SUSPENSION_REASONS_TRAFFIC_VIOLATION, _('Traffic violation')),
    (TRIP_SUSPENSION_REASONS_NO_FUEL, _('No Fuel')),
    (TRIP_SUSPENSION_REASONS_ACCIDENT, _('Accident')),
    (TRIP_SUSPENSION_REASONS_CUSTOMER_CANCELLED, _('Customer cancelled')),
    (TRIP_SUSPENSION_REASONS_TRIP_INTERRUPTED, _('Trip Interrupted'))
)

FIRST_MILE = u'First Mile'
MIDDLE_MILE = u'Middle Mile'
LAST_MILE = u'Last Mile'

TRIP_TYPE = (
    (FIRST_MILE, _('First Mile')),
    (MIDDLE_MILE, _('Middle Mile')),
    (LAST_MILE, _('Last Mile'))
)


class TripManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('customer_contract')

    def prefetch_queryset(self, **kwargs):

        qs = super().get_queryset()
        query = kwargs.pop('query', None)
        stops_list = kwargs.pop('stops_list', None)
        distinct_orders = kwargs.pop('distinct_orders', None)
        select_related = kwargs.pop('select_related', None)
        prefetch_related = kwargs.pop('prefetch_related', None)

        if query:
            qs = qs.filter(query)

        if select_related:
            for x in select_related:
                qs = qs.select_related(x)

        if prefetch_related:
            for x in prefetch_related:
                qs = qs.prefetch_related(x)

        if stops_list:
            qs = qs.prefetch_related(
                Prefetch('stops', to_attr='stops_list')
            )
        if distinct_orders:
            qs = qs.prefetch_related(
                Prefetch(
                    'stops', queryset=Stop.objects.distinct('order'),
                    to_attr='distinct_orders'
                )
            )
        return qs


class ActiveTripManager(models.Manager):

    def for_driver(self, driver):
        # Below query decides at what all statuses a driver will be active on a
        # trip.
        status_query = reduce(operator.or_, [
            ~Q(actual_start_time=None), Q(status=StatusPipeline.TRIP_IN_PROGRESS)])
        return self.filter(driver=driver).filter(status_query)


class TripConsignmentBatch(models.Model):
    """
    Batch to aggregate bulk insert of consignmment notes
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    number_of_entries = models.IntegerField(
        _("Num of entries"), null=True, blank=True)
    records_updated = models.IntegerField(
        _("Records Updated"), null=True, blank=True)
    description = models.CharField(_("Description"), max_length=100)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Consignment Batch")
        verbose_name_plural = _("Consignment Batches")

class MarginBatch(models.Model):
    """
    Batch to save time consumed for margin job
    """
    start_time = models.DateTimeField(_("Batch Start"), blank=True, null=True)
    records_updated = models.IntegerField(_("Records Updated"), null=True, blank=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)
    batch_month = models.IntegerField(_("Month"), default=0)
    progress = models.TextField(_('Progress'),
                blank=True, null=True, max_length=100)


    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Margin Batch")
        verbose_name_plural = _("Margin Batches")


class Trip(BaseModel):

    """
    Trip model for Blowhorn
    """

    # denotes a unique trip in the system

    trip_number = models.CharField(
        _("Trip Number"), max_length=128, db_index=True, unique=True, blank=True)

    # details on who would execute the trip
    driver = models.ForeignKey(
        Driver,
        on_delete=models.PROTECT, blank=True, null=True)

    vendor = models.ForeignKey(
        Vendor,
        on_delete=models.PROTECT, blank=True, null=True)

    invoice_driver = models.ForeignKey(
        Driver, on_delete=models.PROTECT, blank=True, null=True, related_name='invoiced_trips')

    # details on which vehicle will be used for the execution of this trip
    vehicle = models.ForeignKey(
        Vehicle,
        on_delete=models.PROTECT, blank=True, null=True)

    # Trip status pipeline. This should be a dict where each (key, value) #:
    #: corresponds to a status and a list of possible statuses that can follow
    #: that one.
    pipeline = getattr(StatusPipeline, 'TRIP_STATUS_PIPELINE', {})

    # Trip stops status pipeline. This should be a dict where each (key,
    #: value) pair corresponds to an 'trip' status and the corresponding
    #: *stops* status that needs to be set when the trip is set to the new
    #: status
    cascade = getattr(StatusPipeline, 'STOPS_STATUS_PIPELINE', {})

    # Planned Trip Start Time
    planned_start_time = models.DateTimeField(
        _("Scheduled At"), null=True, blank=True)

    trip_acceptence_time = models.DateTimeField(
        _("Accepted At"), null=True, blank=True)

    actual_start_time = models.DateTimeField(
        _("Started At"), null=True, blank=True)

    actual_end_time = models.DateTimeField(
        _("Ended At"), null=True, blank=True)

    estimated_drop_time = models.DateTimeField(
        _("Estimated drop time on trip begin"), null=True, blank=True)

    pickup_loading_timestamp = models.DateTimeField(
        _("Loaded At"), null=True, blank=True)

    # derived field that captures the unplanned minutes waited by the driver
    total_trip_wait_time_driver = models.IntegerField(
        _("Wait Time"), null=True, blank=True)

    route_trace = models.MultiLineStringField(
        _("Route Trace"), null=True, blank=True)

    _route_trace = models.MultiLineStringField(
        _("Route Trace Mongo"), null=True, blank=True)

    status = FSMField(default=StatusPipeline.TRIP_NEW,
                      protected=False, db_index=True)

    estimated_distance_km = models.FloatField(
        _("Estimated Distance (km)"), null=True, blank=True, default=0.0)

    total_distance = models.FloatField(
        _("Total Distance (km)"), null=True, blank=True, default=0.0)

    gps_distance = models.FloatField(
        _("GPS Distance (km)"), null=True, blank=True, default=0.0)

    _gps_distance = models.FloatField(
        _("GPS Distance Mongo (km)"), null=True, blank=True, default=0.0)

    app_distance = models.FloatField(
        _("App GPS Distance (km)"), null=True, blank=True, default=0.0)

    app_points_count = models.IntegerField(
        _("No. of gps Points sent from app"), null=True, blank=True)

    app_rds_points = models.IntegerField(
        _("No. of RDS gps Points sent from app"), null=True, blank=True)

    total_time = models.FloatField(
        _("Total Time (min)"), null=True)

    customer_contract = models.ForeignKey('contract.Contract',
                                          null=True, blank=True, verbose_name='Customer Contract',
                                          on_delete=models.PROTECT)

    # blowhorn contract for paying blowhorn driver
    blowhorn_contract = models.ForeignKey('contract.Contract', related_name='blowhorncontract',
                                          null=True, blank=True, verbose_name='Blowhorn Contract',
                                          on_delete=models.PROTECT)

    current_step = models.IntegerField(_("Current Step"),
                                       blank=True, default=0, null=True, db_index=True)

    order = models.ForeignKey('order.Order',
                              null=True, blank=True, on_delete=models.PROTECT)

    trip_creation_time = models.DateTimeField(_("Created time"),
                                              default=timezone.now, null=True, blank=True)

    reason_for_suspension = models.CharField(
        _('Reason'), max_length=50,
        choices=TRIP_SUSPENSION_REASONS, null=True, blank=True)

    trip_type = models.CharField(
        _('Trip Type'), max_length=50, default=LAST_MILE,
        choices=TRIP_TYPE)

    """
    Odometer Reading captures with/without proofs
    """
    meter_reading_start = models.IntegerField(
        _("MR Start"), null=True, blank=True)

    meter_reading_end = models.IntegerField(
        _("MR End"), null=True, blank=True)

    mt_start_time = models.DateTimeField(_("Start Time"),
                                         blank=True, null=True)

    mt_end_time = models.DateTimeField(_("End Time"),
                                       blank=True, null=True)
    order_ids = ArrayField(models.IntegerField(blank=True, null=True),
                       blank=True, null=True)

    ''' Trip package details'''
    assigned = models.IntegerField(
        _("Assigned Count"), default=0)
    picked = models.IntegerField(_("Picked Count"), default=0)
    delivered = models.IntegerField(_("Delivered Count"), default=0)
    attempted = models.IntegerField(_("Attempted Count"), default=0)
    rejected = models.IntegerField(_("Rejected Count"), default=0)
    pickups = models.IntegerField(_("MFN Seller Pickup Count"), default=0)
    returned = models.IntegerField(_("C Return Count"), default=0)
    delivered_cash = models.IntegerField(
        _("Delivered Packages Cash"), default=0)
    delivered_mpos = models.IntegerField(
        _("Delivered Packages MPOS"), default=0)

    weight = models.FloatField(
        _("Weight in Kg"), default=0.0)

    volume = models.FloatField(
        _("Volume in cubic centimeter"), default=0.0)

    toll_charges = models.DecimalField(
        _('Toll Charges'),
        max_digits=10, decimal_places=2,
        default=0)

    parking_charges = models.DecimalField(
        _('Parking Charges'),
        max_digits=10, decimal_places=2,
        default=0)

    hub = models.ForeignKey('address.Hub', null=True, blank=True,
                            on_delete=models.PROTECT)

    is_contingency = models.BooleanField(
        _('Contingency'), default=False, db_index=True)
    batch = models.ForeignKey('order.OrderBatch', null=True, blank=True,
                              on_delete=models.PROTECT)

    trip_workflow = models.ForeignKey(
        'contract.TripWorkFlow',
        verbose_name='Trip WorkFlow',
        on_delete=models.PROTECT, blank=True, null=True)

    total_cash_collected = models.DecimalField(
        _("Total Cash Collected"),
        decimal_places=2, max_digits=12,
        default=0,
        null=True, blank=True)

    has_consignment_note = models.BooleanField(
        _('Is Consignment Note Generated'), default=False, db_index=True)

    is_route_optimal = models.BooleanField(
        _('Is The Sequence Of Stops Optimal'), default=False, db_index=True)

    consignment_batch = models.ForeignKey(TripConsignmentBatch, null=True, blank=True,
                                          on_delete=models.PROTECT)

    app_name = models.CharField(
        _("Driver App Name used"), blank=True, null=True, max_length=100
    )

    app_version = models.CharField(
        _("Driver App Version used"), blank=True, null=True, max_length=100
    )
    margin = models.FloatField(
        default=0.0, help_text=_('Operating Margin Per Trip(%)')
    )
    driver_amount = models.FloatField(
        default=0.0, help_text=_('Amount paid to driver trip level')
    )
    customer_amount = models.FloatField(
        default=0.0, help_text=_('Amount paid by customer trip level')
    )
    margin_calculation = JSONField(
        blank=True, null=True, help_text=_('Calculations for Margin')
    )
    margin_latest_modified_time = models.DateTimeField(blank=True, null=True,
                                                       help_text=_("latest_modified_date of MarginHistory Table"))

    otp = models.CharField(
        _("OTP for Trip End"), blank=True, null=True, max_length=4
    )

    trip_base_pay = models.DecimalField(_("Agreed base pay"), default=0.0, null=True, blank=True,
                                        max_digits=10, decimal_places=2)

    trip_deduction_amount = models.DecimalField(_("Deduction Amount"), default=0.0, null=True,
                                                blank=True, max_digits=10, decimal_places=2)

    is_deleted = models.NullBooleanField(_("Is Deleted?"), null=True, blank=True)

    objects = TripManager()
    copy_data = CopyManager()
    active = ActiveTripManager()

    def __init__(self, *args, **kwargs):
        """ ove
        rriding the init method as we want to detect changes
            in important fields when saving
        """
        super().__init__(*args, **kwargs)
        self.__important_fields = [
            'actual_start_time', 'meter_reading_start',
            'actual_end_time', 'meter_reading_end',
            'toll_charges', 'parking_charges'
        ]
        for field in self.__important_fields:
            setattr(self, '__original_%s' % field, getattr(self, field))

    def have_important_fields_changed(self):
        for field in self.__important_fields:
            orig = '__original_%s' % field
            if getattr(self, orig) != getattr(self, field):
                return True
        return False

    def verify_start_meter_reading(self, start_meter_reading):
        if isinstance(start_meter_reading, str):
            start_meter_reading = int(start_meter_reading)
        vehicle = self.vehicle
        if vehicle.last_meter_reading and vehicle.last_meter_reading > start_meter_reading:
            return {'status': False,
                    'message': 'Start Meter Reading entered is less than Last Meter Reading of vehicle'}
        return {'status': True}

    def store_vehicle_last_meter_reading(self, meter_reading_end):
        if isinstance(meter_reading_end, str):
            meter_reading_end = int(meter_reading_end)

        self.vehicle.last_meter_reading = meter_reading_end
        self.vehicle.save()

    def update_toll_and_parking(self):
        toll_charges, parking_charges = 0,0
        docs = self.documents.all()
        for doc in docs:
            if doc.document_type == TOLL_CHARGE:
                toll_charges += doc.amount
            elif doc.document_type == PARKING_CHARGE:
                parking_charges += doc.amount

        self.toll_charges = toll_charges
        self.parking_charges = parking_charges

    @property
    def invoice_approved(self):
        return CustomerInvoice.objects.filter(invoicetrip__trip=self).exclude(status__in=INVOICE_EDITABLE_STATUS)

    def __is_valid_trip_total_time(self):
        """
        :return: is_valid (boolean), message
        """
        if self.total_time > MAX_ALLOWED_TOTAL_TRIP_TIME_MINUTES:
            return False, 'Total trip time can\'t be greater than %d hours.' \
                          'Check start and end time.' % \
                   int(MAX_ALLOWED_TOTAL_TRIP_TIME_MINUTES / 60)
        return True, ''

    def __is_valid_distance(self):
        """
        :return: is_valid (boolean), message
        """
        possible_distance_kms = self.total_time / 60 * AVG_SPEED_OF_VEHICLE_KMS
        if self.total_distance > possible_distance_kms:
            return False, 'Check the meter reading. Cannot be %0.2f kms in %s'\
                   % (self.total_distance, minutes_to_dhms(self.total_time))
        return True, ''

    def save(self, *args, **kwargs):

        if self.have_important_fields_changed():
            from blowhorn.customer.models import CustomerInvoice

            stale_reason = '%s Trip fields changed' % self.trip_number

            query = Q(invoicetrip__trip=self)
            if self.order:
                query = query | Q(invoiceorder__order=self.order)
            CustomerInvoice.objects.filter(
                 query
            ).filter(
                status__in=StatusPipeline.INVOICE_EDIT_STATUSES).update(
                is_stale=True, stale_reason=stale_reason)

            DriverPayment.objects.filter(paymenttrip__trip=self).update(
                is_stale=True, stale_reason=stale_reason)

        if self.status == StatusPipeline.TRIP_COMPLETED:
            if not self.actual_start_time or not self.actual_end_time:
                raise ValueError('Actual start and End time is mandatory for '
                                 'completed Trips')

        if self and self.driver_id and self.customer_contract and \
            self.customer_contract.contract_type in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
            trip = Trip.objects.filter(id=self.id).first()
            if trip and ((trip.driver_id != self.driver_id) or (self.customer_contract  != trip.customer_contract)):
                from blowhorn.order.tasks import alert_driver_prior_to_trip
                from ..order.models import OrderConstants
                time_ = self.planned_start_time - timedelta(
                    hours=OrderConstants().get('ALERT_DRIVER_PRIOR_TO_TRIP', 2))
                if time_ <= timezone.now():
                    time_ = timezone.now()
                alert_driver_prior_to_trip.apply_async((None, self.trip_number, ),
                                                       eta=time_)

        if not self.trip_number:
            self.trip_number = self._get_trip_reference_number()

        if self.actual_end_time and self.actual_start_time:
            time_delta = self.actual_end_time - self.actual_start_time
            minutes = time_delta.total_seconds() / 60
            self.total_time = minutes
        else:
            self.total_time = 0

        self.total_trip_wait_time_driver = 0
        if self.pickup_loading_timestamp and self.actual_start_time:
            time_delta = self.pickup_loading_timestamp - self.actual_start_time
            minutes = time_delta.total_seconds() / 60
            self.total_trip_wait_time_driver = minutes

        if self.meter_reading_start and self.meter_reading_end:
            self.store_vehicle_last_meter_reading(self.meter_reading_end)
            self.total_distance = (
                float(self.meter_reading_end) - float(self.meter_reading_start)
            )
        elif self.gps_distance and self.gps_distance > 0:
            self.total_distance = self.gps_distance
        else:
            self.total_distance = 0

        if self.total_distance:
            is_valid, message = self.__is_valid_distance()
            if not is_valid:
                if hasattr(self, 'app_name'):
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=message)
                else:
                    raise ValidationError(_(message))

        if self.total_time:
            is_valid, message = self.__is_valid_trip_total_time()
            if not is_valid:
                raise ValidationError(_(message))

        if self.weight and self.weight < 0:
            raise ValidationError('Weight cannot be negative')

        if self.volume and self.volume < 0:
            raise ValidationError('Volume cannot be negative')

        if not self.route_trace:
            self.route_trace = MultiLineString([])

        trip_list = []
        if not self.invoice_driver:
            self.invoice_driver = self.driver

        if self._get_pk_val():
            _request = current_request()
            current_user = _request.user if _request else ''
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')
                if field_name == 'status':
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                    if old_value != current_value:
                        trip_list.append(TripHistory(field=field_name,
                                                     old_value=str(old_value),
                                                     new_value=current_value,
                                                     user=current_user,
                                                     trip=old))

                        if current_value == StatusPipeline.TRIP_IN_PROGRESS:
                            try:
                                RedisDriver(self.driver_id)['ongoing'] = {
                                    'trip_no': self.trip_number,
                                    'contract_id': self.customer_contract_id,
                                    'customer_id': self.customer_contract.customer_id
                                }
                            except:
                                pass
                        if current_value in StatusPipeline.TRIP_END_STATUSES:
                            try:
                                if RedisDriver(
                                    self.driver_id
                                )['ongoing'].get('trip_no', None) == self.trip_number:
                                    RedisDriver(self.driver_id)['ongoing'] = {
                                        'trip_no': None,
                                        'contract_id': None,
                                        'customer_id': None
                                    }
                            except:
                                pass

                elif field_name == 'driver':
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)
                    if old_value != current_value:
                        self.invoice_driver_id = current_value

            TripHistory.objects.bulk_create(trip_list)

        return super(Trip, self).save()

    def _get_trip_reference_number(self):
        unique_string = CommonHelper().get_unique_friendly_id(TRIP_CODE_PREFIX)
        query_string = Trip.objects.filter(trip_number=unique_string)
        if 0 < query_string.count():
            unique_string = self._get_trip_reference_number()
        return unique_string

    def _get_orders_from_trip(self):
        from blowhorn.order.models import Order
        if self.order:
            return [self.order, ]

        return Order.objects.filter(stops__trip=self).distinct()

    def get_elapsed_time(self):
        """
        Return elapsed trip time in minutes.
        """

        if not self.actual_start_time or self.actual_end_time:
            # Trip haven't started yet or already ended.
            return None

        return "%0.2f" % ((timezone.now() - self.actual_start_time).total_seconds() / 60)

    def get_current_stop_in_progress(self):

        return self.stops.filter(
            status=StatusPipeline.TRIP_IN_PROGRESS).first()

    def is_all_stops_done(self):
        return True if self.status == StatusPipeline.TRIP_ALL_STOPS_DONE else False

    def is_trip_in_progress(self):
        return True if self.status in [StatusPipeline.TRIP_ALL_STOPS_DONE, StatusPipeline.TRIP_IN_PROGRESS] else False

    def get_next_stop(self):
        """
        Returns the next stop status of ongoing trip. Otherwise None
        """

        # TODO : Below logic will be deprecated on introduction of sequence id
        # in stops.
        current_waypoint_sequence_id = self.stops.filter(
            status="In-Progress").values_list('waypoint__sequence_id', flat=True).last()
        if current_waypoint_sequence_id:
            return Stop.objects.filter(
                waypoint__sequence_id=current_waypoint_sequence_id + 1).values_list(
                'waypoint__shipping_address__line1', flat=True).last()
        return None

    def validate_trip_conflict(self, existing_trip):
        ct = []
        for trip in existing_trip:
            start = None
            end = None
            if trip.actual_start_time:
                start = utc_to_ist(trip.actual_start_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)
            if trip.actual_end_time:
                end = utc_to_ist(trip.actual_end_time).strftime(
                    settings.ADMIN_DATETIME_FORMAT)

            if start or end:
                ct.append({
                    'trip': trip.trip_number,
                    'customer': trip.order.customer.name if trip.order else '',
                    'start': start,
                    'end': end
                })
        return ct

    def __str__(self):
        return self.trip_number

    @transition(field=status, source=[StatusPipeline.TRIP_NEW],
                target=StatusPipeline.TRIP_SCHEDULED_OFF,
                permission=is_trip_spoc_or_supervisor,
                conditions=[is_b2b_trip])
    def scheduled_off(self):
        print("Marking a trip Scheduled Off")

    @transition(field=status, source=[StatusPipeline.TRIP_NEW, StatusPipeline.DRIVER_ACCEPTED],
                target=StatusPipeline.TRIP_NO_SHOW,
                permission=is_trip_spoc_or_supervisor,
                conditions=[is_b2b_trip])
    def no_show(self):
        print("Marking a trip No-show")

    @transition(
        field=status, source=[
            StatusPipeline.TRIP_NEW, StatusPipeline.TRIP_IN_PROGRESS,
            StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.TRIP_ALL_STOPS_DONE],
        target=StatusPipeline.TRIP_COMPLETED,
        permission=is_trip_spoc_or_supervisor,
        conditions=[can_complete_trip])
    def complete(self):
        print("Trip completed")
        if self.order and self.order.status != StatusPipeline.ORDER_DELIVERED:
            self.order.status = StatusPipeline.ORDER_DELIVERED
            self.current_step = -1
            self.order.save()

        contract = self.customer_contract
        if contract and contract.contract_type == CONTRACT_TYPE_KIOSK:
            stops = Stop.objects.filter(trip=self)
            stops.update(status=StatusPipeline.TRIP_COMPLETED)
            from blowhorn.order.models import Order
            orders = Order.objects.filter(
                pk__in=stops.values_list('order_id', flat=True))
            orders.update(status=StatusPipeline.ORDER_DELIVERED)

        # if there is cash collected in the trip, reduce it from driver balance
        if self.total_cash_collected > 0:
            Driver.objects.filter(id=self.driver_id,
                                  cod_balance__gte=self.total_cash_collected).update(
                cod_balance=F('cod_balance') - self.total_cash_collected)

        # sync trip with app after trip completion
        driver_id = self.driver_id
        dict_ = {
            'username': driver_id,
            'action': 'restore',
        }
        from blowhorn.common.notification import Notification
        Notification.send_action_to_driver(self.driver.user, dict_)

        from blowhorn.apps.driver_app.v1.helpers.trip import (
            get_route_trace_for_trip_without_stops,
            get_route_trace_for_trip_without_stops_using_mongo
        )
        #stop changes
        route_trace, gps_distance = get_route_trace_for_trip_without_stops(
            trip=self, start_time=self.actual_start_time,
            end_time=self.actual_end_time)
        if route_trace:
            route_trace = MultiLineString([route_trace, ])

        _route_trace, _gps_distance = get_route_trace_for_trip_without_stops_using_mongo(
            self)
        if _route_trace:
            _route_trace = MultiLineString([_route_trace, ])

        if gps_distance:
            self.gps_distance = gps_distance
        if route_trace:
            self.route_trace = route_trace

        if _gps_distance:
            self._gps_distance = _gps_distance
        if _route_trace:
            self._route_trace = _route_trace
        self.save()

    class Meta:
        app_label = 'trip'
        verbose_name = _("Trip")
        verbose_name_plural = _("Trips")
        indexes = (
            BrinIndex(fields=['planned_start_time']),
        )


class TripMView(models.Model):
    app_version = models.CharField(_("Driver App Version used"), blank=True, null=True, max_length=100)

    status = models.CharField(max_length=30, null=True)

    trip_number = models.CharField(("Trip Number"), max_length=128, db_index=True, unique=True, blank=True)

    trip_base_pay = models.DecimalField(_("Agreed base pay"), default=0.0, null=True, blank=True, max_digits=10,
                                        decimal_places=2)

    hub_name = models.CharField(_("Hub Name"), max_length=128, blank=True)

    blowhorn_contract_name = models.CharField(_("Blowhorn Contract Name"), max_length=128, blank=True)

    commercial_classification = models.CharField(_("Commercial Class"), max_length=50, blank=False)

    contract_type = models.CharField(_('Contract Type'), max_length=15)

    customer_category = models.CharField(_('Customer Category'), max_length=45)

    customer_name = models.CharField(_('Customer Category'), max_length=45)

    legalname = models.CharField(max_length=45)

    city_name = models.CharField(max_length=45)

    conti = models.BooleanField(default=False)

    division_name = models.CharField(_('Customer Category'), max_length=45)

    precise_gps_distance = models.DecimalField(default=0.0, null=True, blank=True, max_digits=10, decimal_places=2)

    mongo_gps_distance = models.DecimalField(default=0.0, null=True, blank=True, max_digits=10, decimal_places=2)

    mongo_gps_distance = models.CharField(max_length=45, null=True)

    precise_total_time = models.CharField(max_length=45, null=True)

    app_points_count = models.CharField(max_length=45, null=True)

    formatted_app_distance = models.CharField(max_length=45, null=True)

    created_by = models.CharField(max_length=60, null=True)

    modified_by = models.CharField(max_length=60, null=True)

    driver_name = models.CharField(max_length=60, null=True)

    registration_certificate_number = models.CharField(max_length=20, null=True)

    contract_name = models.CharField(max_length=45, null=True)

    number = models.CharField(max_length=20, null=True)

    scheduled_date = models.CharField(max_length=45, null=True)

    end_date = models.CharField(max_length=45, null=True)

    scheduled_time = models.CharField(max_length=20, null=True)

    end_time = models.CharField(max_length=20, null=True)

    actual_st_date = models.CharField(max_length=45, null=True)

    actual_st_time = models.CharField(max_length=20, null=True)

    precise_total_distance = models.CharField(max_length=20, null=True)

    meter_reading_start = models.CharField(max_length=20, null=True)

    meter_reading_end = models.CharField(max_length=20, null=True)

    assigned = models.CharField(max_length=20, null=True)

    delivered = models.CharField(max_length=20, null=True)

    returned = models.CharField(max_length=20, null=True)

    attempted = models.CharField(max_length=20, null=True)

    rejected = models.CharField(max_length=20, null=True)

    pickups = models.CharField(max_length=20, null=True)

    delivered_cash = models.CharField(max_length=20, null=True)

    delivered_mpos = models.CharField(max_length=20, null=True)

    cum_distance = models.CharField(max_length=20, null=True)

    cum_time = models.CharField(max_length=20, null=True)

    toll_amount = models.CharField(max_length=20, null=True)

    operating_margin = models.CharField(max_length=20, null=True)

    revenue_side = models.CharField(max_length=20, null=True)

    cost_side = models.CharField(max_length=20, null=True)

    copy_data = CopyManager()


    class Meta:
        managed = False
        db_table = 'trip_tripmview'

class TripDocument(BaseModel):
    trip = models.ForeignKey(Trip, related_name='documents',
                             on_delete=models.PROTECT)

    stop = models.ForeignKey('trip.Stop', null=True, blank=True, on_delete=models.PROTECT)

    file = models.FileField(upload_to=generate_file_path,
                            validators=[FileValidator()],
                            max_length=256,
                            null=True,
                            blank=True)

    document_type = models.CharField(
        _("Document Type"),
        max_length=80,
        choices=TRIP_DOCUMENT_TYPES,
        default=START_ODOMETER_PROOF)

    amount = models.DecimalField(
        _('Amount'),
        max_digits=10, decimal_places=2,
        default=0)

    ocr_reading = models.CharField(
        _("OCR Reading"),
        max_length=30, blank=True,
        null = True)

    raw_data = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = _('Trip document')
        verbose_name_plural = _('Trip documents')

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s" % self.file

    def to_json(self):
        return {
            'id': self.id,
            'trip_id': self.trip_id,
            'stop_id': self.stop_id,
            'address': '%s' % self.stop.stop_address if self.stop and self.stop.stop_address else '',
            'file': self.file.url if self.file else '',
            'document_type': '%s' % self.document_type,
            'created_date': self.created_date.isoformat()
        }


class ConsignmentNote(models.Model):
    """
    For storing the consignment note file content
    """
    content = models.TextField()
    trip = models.OneToOneField(Trip, related_name='consignment_trip',  on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Consignment Note'
        verbose_name_plural = 'Consignment Notes'


class StopManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().select_related(
            'waypoint', 'waypoint__shipping_address', 'waypoint__order', 'order'
        )


class Stop(models.Model):

    """
    Trip Stop model
    """
    trip = models.ForeignKey(Trip, related_name='stops',
                             null=True, on_delete=models.PROTECT)
    waypoint = models.ForeignKey('order.WayPoint', null=True,
                                 on_delete=models.PROTECT,
                                 related_name='stops',)
    order = models.ForeignKey('order.Order', on_delete=models.PROTECT,
                              related_name='stops', null=True, blank=True)
    status = models.CharField(
        _("Stop Status"), blank=True, max_length=40, db_index=True
    )
    stop_address = models.ForeignKey('order.ShippingAddress', null=True,
                                     blank=True, verbose_name='Stop Address',
                                     on_delete=models.PROTECT)
    stop_type = models.CharField(_("Stop Type"), choices=STOP_TYPES,
                                 max_length=50, blank=True, null=True,
                                 db_index=True)
    route_trace = models.LineStringField(
        _("Stop Trace"), null=True)
    start_time = models.DateTimeField(
        _("Started At"), null=True, blank=True)

    reach_time = models.DateTimeField(
        _("Reached At"), null=True, blank=True)

    end_time = models.DateTimeField(
        _("Ended At"), null=True, blank=True)

    sequence = models.PositiveIntegerField(_("Sequence ID"),
                                           null=True, blank=True)

    distance = models.FloatField(_('Actual Distance'),
                                 null=True, blank=True, default=0)

    # Trip Stop pipeline.  This should be a dict where each (key, value)
    #: corresponds to a status and the possible statuses that can follow that
    #: one.
    pipeline = getattr(StatusPipeline, 'STOPS_STATUS_PIPELINE', {})

    _route_trace = models.LineStringField(
        _("Stop Trace"), null=True)

    _distance = models.FloatField(
        _('Actual Distance'), null=True, blank=True, default=0)

    def __str__(self):
        return str(self.id)

    class Meta:
        # unique_together = ("trip", "order", "stop_type")
        verbose_name = 'Stop'
        verbose_name_plural = 'Stops'


class AppEvents(models.Model):

    driver = models.ForeignKey('driver.Driver', null=True, blank=True,
                               on_delete=models.PROTECT)
    event = models.CharField(_("Event Triggered"), max_length=100, blank=True)
    trip = models.ForeignKey('trip.Trip', null=True, blank=True,
                             on_delete=models.PROTECT)
    stop = models.ForeignKey('trip.Stop', null=True, blank=True,
                             on_delete=models.PROTECT)
    order = models.ForeignKey('order.Order', null=True, blank=True,
                              on_delete=models.PROTECT)
    geopoint = models.PointField(null=True, blank=True)
    created_time = models.DateTimeField(auto_now_add=True)
    hub_associate = models.ForeignKey(User, null=True, blank=True,
                                      on_delete=models.PROTECT)
    device_id = models.CharField(_('Device ID'), max_length=255, blank=True,
                                 null=True)
    imei_number = models.CharField(_('IMEI Number'), null=True, blank=True,
                                   max_length=50)
    uuid = models.CharField(_('Universally Unique Identifier'), max_length=255,
                            blank=True, null=True)

    copy_data = CopyManager()
    objects = models.Manager()

    def __str__(self):
        return str(id)

    class Meta:
        verbose_name = _('App Event')
        verbose_name_plural = _('App Events')


class StopOrders(models.Model):

    stop = models.ForeignKey(Stop, on_delete=models.PROTECT)

    trip = models.ForeignKey(Trip, on_delete=models.PROTECT)

    order = models.ForeignKey('order.Order', on_delete=models.PROTECT,
                              related_name='stop_orders', null=True, blank=True)
    hub = models.ForeignKey('address.Hub', on_delete=models.PROTECT,
                               null=True, blank=True)


class TripConstants(models.Model):

    """
    Capturing the constants related to trip.
    """
    history = AuditlogHistoryField()

    name = models.CharField(max_length=50)
    value = models.CharField(max_length=20)
    description = models.CharField(max_length=200, blank=True, null=True)

    def get(self, name, default):
        try:
            constant = TripConstants._default_manager.get(name=name)
        except TripConstants.DoesNotExist:
            return default

        return constant.value

    class Meta:
        verbose_name = _('Trip Constants')
        verbose_name_plural = _('Trip Constants')

    def __str__(self):
        return self.name


class TripHistory(models.Model):

    """
    Capturing the trip history
    """
    trip = models.ForeignKey('trip.Trip', null=True, blank=True,  on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    old_value = models.CharField(max_length=100, null=True, blank=True)
    new_value = models.CharField(max_length=100, null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Trip History')
        verbose_name_plural = _('Trip History')

    def __str__(self):
        return self.trip.trip_number


class MarginHistory(BaseModel):
    trip = models.ForeignKey(
        Trip, on_delete=models.PROTECT, blank=True, null=True
    )
    margin = models.FloatField(
        null=True, blank=True,help_text=_("Operating Margin-Per-Trip")
    )
    margin_calculation = JSONField(
        null=True, blank=True, help_text=_("Margin-Calculation")
    )
    copy_data = CopyManager()
    objects = models.Manager()

    def __str__(self):
        return '%s' % self.margin


class UnassignedTrip(Trip):
    class Meta:
        proxy=True

class PartnerFeedback(BaseModel):

    driver = models.ForeignKey(Driver, blank=True, null=True,
                on_delete=models.PROTECT)
    trip = models.ForeignKey(Trip, on_delete=models.PROTECT)
    feedback = models.CharField(_("Trip Feedback"), max_length=200,
                blank=True, null=True)
    event_time = models.DateTimeField(_("Event Time"))

    def __str__(self):
        return str(id)

    class Meta:
        verbose_name = _('Trip Feedback')
        verbose_name_plural = _('Trips Feedback')


class FeedbackOptions(BaseModel):
    option = models.CharField(max_length=100)

    def __str__(self):
        return '%s' % self.option

class StopHistory(models.Model):

    stop = models.ForeignKey('trip.Stop', on_delete=models.CASCADE)

    routes_batch = models.ForeignKey('route_optimiser.RoutesBatch', null=True,
                                     blank=True, on_delete=models.CASCADE)

    old_sequence = models.PositiveIntegerField(_("Old Sequence"), null=True,
                                               blank=True)

    new_sequence = models.PositiveIntegerField(_("New Sequence"), null=True,
                                               blank=True)

    created_time = models.DateTimeField(_("Created Time"), auto_now_add=True)

from blowhorn.trip.submodels.old_tripmodel import *


