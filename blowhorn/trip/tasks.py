from django.conf import settings
from celery.schedules import crontab
# from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from blowhorn.trip.utils import TripService, TripConsignment
from blowhorn.common.decorators import redis_batch_lock
# from config.settings.base import celery_app
from blowhorn import celery_app

from .margin import MarginCalculation

logger = get_task_logger(__name__)


@celery_app.task
# @periodic_task(run_every=(crontab(minute="00", hour="01, 16")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def mark_inactive_trips():
    """
    Mark the trip as inactive if trip expiration has reached
    """
    logger.info("job started for marking trips as inactive")
    TripService().mark_noshow_trips()


@celery_app.task
# @periodic_task(run_every=(crontab(minute="29", hour="18")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def generate_consignments():
    """
        Generate the consignement notes for the trips at 12 A.M
    """
    logger.info("job started for generating the consignement notes for the trip")
    TripConsignment().generate_consignment_notes()


@celery_app.task
# @periodic_task(run_every=(crontab(minute=0, hour=23)))
@redis_batch_lock(period=84600, expire_on_completion=True)
def calculate_margin():
    """
    Calculate margin at trip level at 4:30 AM
    """
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return
    logger.info("job started for calculating margin at trip level")
    display_only = False
    MarginCalculation().start_calculation(display_only)
