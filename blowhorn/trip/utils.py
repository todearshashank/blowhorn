import base64
import logging
import operator
from django.utils import timezone
from django.db.models import F, Q
from django.conf import settings
from django.core.files.base import ContentFile
from datetime import timedelta, datetime
from random import randint
from functools import reduce

from blowhorn.vehicle.utils import get_vehicle_info
from blowhorn.vehicle.constants import DEFAULT_VEHICLE_CLASS
from blowhorn.utils.functions import (
    utc_to_ist, timedelta_to_str,
    get_tracking_url, geopoints_to_km,
    splice_route_info, dist_between_2_points,
    format_datetime, lat_lon_to_km)
from blowhorn.order.utils import OrderUtil
from blowhorn.common.helper import CommonHelper
from blowhorn.contract.constants import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME
from blowhorn.order.const import (
    STOP_STATUS_DONE, STOP_STATUS_PENDING,
)
from blowhorn.address.utils import AddressUtil
from config.settings import status_pipelines as StatusPipeline
from blowhorn.trip.models import Trip, TripConsignmentBatch, ConsignmentNote
from blowhorn.trip.mixins import TripMixin
from blowhorn.users.models import User
from blowhorn.customer.models import InvoiceTrip
from blowhorn.order.const import SIGNATURE, PROOF_OF_DELIVERY
from blowhorn.customer.constants import REJECTED, CANCELLED, CREATED


TIME_FORMAT = '%I:%M %p'
DATE_FORMAT = '%A, %d %B, %Y'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TripService(object):

    def mark_noshow_trips(self):
        """
        Mark the trips as NO-SHOW if preconfigured hours reach limit
        """
        trips = Trip.objects.filter(
            Q(
                status=StatusPipeline.TRIP_NEW,
                customer_contract__isnull=False,
                planned_start_time__lte=timezone.now() - timedelta(
                    hours=1) * F('customer_contract__trip_noshow_hrs')
            ) & ~Q(customer_contract__contract_type__in=[
                CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME])
        ).exclude(
            order=None
        ).update(
            status=StatusPipeline.TRIP_NO_SHOW,
            modified_date=timezone.now(),
            modified_by=User.objects.filter(
                email=settings.CRON_JOB_DEFAULT_USER
            ).first())

        logger.info("Updated %d rows", trips)

    def is_valid_to_mark_new(self, trip_qs):
        """
        :param trip_qs: Trip queryset
        :return: False if trip is already invoiced
        """
        return not InvoiceTrip.objects.filter(Q(trip__in=trip_qs) & ~Q(
            invoice__status__in=[REJECTED, CANCELLED, CREATED])).exists()

    def get_trip_from_order(self, order):
        return Trip.objects.filter(
            order=order,
            status=StatusPipeline.TRIP_COMPLETED).first()

    def store_image(self, img_data):

        img_data = base64.b64decode(img_data)
        content = ContentFile(img_data)
        return content

    def trip_for_vehicle_exists(self, vehicle):
        return Trip.objects.filter(vehicle=vehicle, status__in=[
            StatusPipeline.TRIP_STARTED,
            StatusPipeline.TRIP_COMPLETED, StatusPipeline.TRIP_IN_PROGRESS,
            StatusPipeline.TRIP_SCHEDULED_OFF, StatusPipeline.TRIP_NEW]).exists()

    def create_contingency_trip(self, old_driver, obj,
                                suspension_trip_reason=None, trip_status=None,
                                mark_contingency=False, trip_base_pay=None,
                                trip_deduction_amount=None):
        """
        When mark contingency is True, trip is suspended,
        but no new trip is created
        """
        trip = Trip.objects.filter(
            order_id=obj.id, driver=old_driver).first()
        if trip:
            if suspension_trip_reason:
                trip.reason_for_suspension = suspension_trip_reason
            if trip_status:
                trip.status = trip_status
                trip.save()
            elif trip.status not in StatusPipeline.TRIP_END_STATUSES:
                trip.status = StatusPipeline.TRIP_SUSPENDED
                trip.save()
            if trip_deduction_amount:
                trip.trip_deduction_amount = trip_deduction_amount
                trip.save()

        if not mark_contingency:
            planned_start_time = obj.pickup_datetime + timedelta(hours=1)
            new_trip = TripMixin().create_trip(
                driver=obj.driver,
                contract=obj.customer_contract,
                order=obj,
                blowhorn_contract=obj.driver.contract if obj.driver else None,
                hub=obj.hub,
                planned_start_time=planned_start_time,
                isBatchUser=False,
                invoice_driver=old_driver,
                is_contingency=True,
                trip_base_pay=trip_base_pay)

    def get_trip_details_for_customer(self, request, trip, route_info=False):
        from blowhorn.activity.mixins import ActivityMixin

        city = trip.customer_contract.city
        contract = trip.customer_contract
        total_duration = 0

        vehicle_class = contract.vehicle_classes.first()
        try:
            truck_url = vehicle_class.image.url
            truck_url_detail = vehicle_class.image_detail.url
        except:
            # Display default vehicle images if images are not available
            vehicle = get_vehicle_info(DEFAULT_VEHICLE_CLASS)
            truck_url = vehicle.get('image_url')
            truck_url_detail = vehicle.get('image_url_detail')

        driver = trip.driver
        driver_position = None
        vehicle = trip.vehicle
        driver_id = driver.id
        driver_name = driver.name
        driver_mobile = driver.user.national_number
        vehicle_number = vehicle.registration_certificate_number
        vehicle_model = vehicle.vehicle_model.model_name
        driver_pic = ''  # driver.photo
        finished_distance = 0
        total_distance = trip.total_distance or trip.gps_distance or 0
        route_trace = []
        time_arrived_at_pickup = None
        time_driver_accepted = None
        remaining_distance = 0
        pickup_distance_done = 0
        remaining_time_to_pickup_mins = 0
        pickup_distance = 0

        last_modified = trip.modified_date and trip.modified_date.strftime(
            '%Y%m%d%H%M%S')

        stops = trip.stops.all().order_by('end_time', 'reach_time', 'start_time', 'sequence')
        if stops:
            if stops[0].waypoint:
                pickup_address = stops[0].waypoint.order.pickup_address
            else:
                pickup_address = stops[0].order.pickup_address
                # if trip.hub:
                #     pickup_address = trip.hub.address
                # else:
                #     pickup_address = stops[0].order.pickup_address
        else:
            pickup_address = trip.order.pickup_address

        route_trace_list_dict = []

        if trip.status == StatusPipeline.TRIP_NEW:
            _status = 'upcoming'
            pickup_status = 'upcoming'
        elif trip.status == StatusPipeline.TRIP_COMPLETED:
            _status = 'done'
            pickup_status = 'done'
            finished_distance = total_distance
            time_arrived_at_pickup = trip.actual_start_time
            time_driver_accepted = trip.trip_acceptence_time

            if trip.route_trace:
                route_trace_list_dict = AddressUtil(
                ).get_route_trace_from_multilinestring(trip.route_trace)

            try:
                total_duration = trip.actual_end_time - trip.actual_start_time
                total_duration = timedelta_to_str(total_duration)
            except BaseException:
                pass
        else:
            _status = 'ongoing'
            pickup_status = 'done'

            total_distance = trip.total_distance or trip.gps_distance or 0
            time_driver_accepted = trip.trip_acceptence_time
            time_arrived_at_pickup = trip.actual_start_time

            if route_info and not time_arrived_at_pickup:
                route_trace = ActivityMixin().get_prior_route(trip=trip)
                if len(route_trace) > 1:
                    finished_distance = lat_lon_to_km(
                        [x for x in route_trace])

            route_trace_list_dict = [{
                'lat': x.y,
                'lng': x.x,
                'latitude': x.y,
                'longitude': x.x
            } for x in route_trace]

            last_geopoint = None
            if route_trace and route_trace[-1]:
                last_geopoint = CommonHelper.get_geopoint_from_latlong(
                    route_trace[-1][1], route_trace[-1][0])
            driver_position = last_geopoint
            last_modified = trip.modified_date and trip.modified_date.strftime(
                '%Y%m%d%H%M%S')
            if time_arrived_at_pickup:
                # Pickup has been reached
                post_pickup_trace = ActivityMixin().get_prior_route(
                    trip=trip, start_time=time_arrived_at_pickup)
                route_trace_list_dict = [{
                    'lng': p.x,
                    'lat': p.y,
                    'latitude': p.y,
                    'longitude': p.x
                } for p in post_pickup_trace]
                pickup_distance = geopoints_to_km(
                    [x.geopoint for x in splice_route_info(
                        route_trace, end_time=time_arrived_at_pickup)])

            else:
                DISTANCE_MULTIPLIER = 1.3
                AVERAGE_VEHICLE_SPEED_KMPH = 15.0
                # On the way to pickup.
                # Get the remaining distance and time to pickup
                # route_trace = self.get_prior_route(trip=trip)
                pickup_distance_done = finished_distance
                if len(route_trace) > 0:
                    remaining_distance = DISTANCE_MULTIPLIER * (
                        dist_between_2_points(
                            route_trace[-1].geopoint, pickup_address.geopoint)
                    )

                pickup_distance = pickup_distance_done + remaining_distance
                remaining_time_to_pickup_mins = \
                    (remaining_distance / AVERAGE_VEHICLE_SPEED_KMPH) * 60

        try:
            pickup_time_duration = timedelta_to_str(
                time_arrived_at_pickup - time_driver_accepted)
        except:
            pickup_time_duration = '%d Hours %d Mins' % (0, 0)

        if not time_arrived_at_pickup:
            # Pickup is not complete. Calculate expected arrival time
            time_arrived_at_pickup = timezone.now() + timedelta(
                minutes=remaining_time_to_pickup_mins)

        previous_time = time_arrived_at_pickup
        pickup = OrderUtil()._get_address_dict_for_customer(
            _time=time_arrived_at_pickup,
            _status=pickup_status,
            address=pickup_address,
        )

        total_cost = 0
        _stops = []
        if stops:
            for stop in stops:

                w_distance = 0
                w_address = stop.waypoint.shipping_address if stop.waypoint \
                    else stop.order.shipping_address
                start_time = stop.start_time
                end_time = stop.end_time
                extras = {}

                travel_time = OrderUtil().get_travel_time(
                    start_time=start_time, end_time=stop.end_time)
                if stop.status == StatusPipeline.TRIP_COMPLETED:
                    w_distance = stop.distance if stop.distance else 0
                    end_time = stop.end_time
                    w_status = 'done'
                else:
                    w_status = 'pending'

                if travel_time:
                    media_url = CommonHelper().get_media_url_prefix()
                    pod_file_url = ''
                    sign_file_url = ''

                    order = stop.waypoint.order \
                        if stop.waypoint else stop.order
                    sign_file_doc = order.documents.filter(
                        document_type=SIGNATURE).first()
                    pod_file_doc = order.documents.filter(
                        document_type=PROOF_OF_DELIVERY).first()
                    if sign_file_doc:
                        sign_file_url = media_url + str(sign_file_doc.file)
                    if pod_file_doc:
                        pod_file_url = media_url + str(pod_file_doc.file)

                    extras = {
                        'sign_file_url': sign_file_url,
                        'pod_file_url': pod_file_url,
                        'order_status': order.status,
                        'cod': float(order.cash_on_delivery or 0),
                        'cash_collected': float(order.cash_collected or 0),
                        'time': utc_to_ist(order.updated_time).strftime(
                            settings.ADMIN_DATETIME_FORMAT)
                    }

                _stop = OrderUtil()._get_address_dict_for_customer(
                    _time=end_time or start_time,
                    _status=w_status,
                    _name=w_address.identifier or w_address.first_name,
                    address=w_address,
                    distance=w_distance,
                    travel_time=travel_time,
                    extras=extras
                )
                previous_time = end_time or start_time
                _stops.append(_stop)

        dropoff_status = STOP_STATUS_DONE \
            if trip.actual_end_time else STOP_STATUS_PENDING
        dropoff_address = pickup_address
        dropoff_distance = 0
        travel_time = OrderUtil().get_travel_time(
            start_time=previous_time, end_time=trip.actual_end_time)
        dropoff = OrderUtil()._get_address_dict_for_customer(
            _time=trip.actual_end_time,
            _status=dropoff_status,
            address=dropoff_address,
            distance=dropoff_distance,
            travel_time=travel_time,
        )

        if _stops:
            pickup_stops_drop = [pickup] + _stops + [dropoff]
        else:
            pickup_stops_drop = [pickup] + [dropoff]

        requested_pickup_time = trip.planned_start_time
        pickup_stops_drop = OrderUtil().annotate_times_and_distance(
            pickup_stops_drop, driver_position,
            requested_pickup_time, _status
        )

        finished_distance = sum(
            [x.get('distance', 0) for x in pickup_stops_drop])
        remaining_distance = sum(
            [x.get('remaining_distance', 0) for x in pickup_stops_drop])
        estimated_distance = finished_distance + remaining_distance

        pickup = pickup_stops_drop[0]
        _stops = pickup_stops_drop[1:-1]
        dropoff = pickup_stops_drop[-1]

        tracking_url = ''
        if request:
            tracking_url = get_tracking_url(request, trip.trip_number)

        user_message = StatusPipeline.get_status_info(trip.status)

        if route_info:
            HEADER_ADDRESS_LENGTH = 40
        else:
            HEADER_ADDRESS_LENGTH = 24

        if pickup_address.line3:
            header_from = pickup_address.line3
        elif len(pickup_address.line1) <= HEADER_ADDRESS_LENGTH:
            header_from = pickup_address.line1
        else:
            header_from = pickup_address.line1[
                0:HEADER_ADDRESS_LENGTH - 3] + '...'

        header_from = header_from.lower().title()
        display_payment = False
        if trip.order and trip.order.order_type == 'booking':
            display_payment = True
        dict_ = {
            'last_modified': last_modified,
            'order_id': trip.trip_number,
            'order_real_id': str(trip.id),
            'order_key': trip.trip_number,
            'user_message': user_message,
            'display_payment': display_payment,
            'status': _status,
            'date_sortable': utc_to_ist(
                trip.planned_start_time).strftime('%Y%m%d%H%M%S'),
            'date': utc_to_ist(trip.planned_start_time).strftime(DATE_FORMAT),
            'time': utc_to_ist(trip.planned_start_time).strftime(TIME_FORMAT),
            'header_from': header_from,
            'header_to': header_from,
            'city': city and city.name,
            'truck_type': vehicle_class.commercial_classification,
            'truck_url': truck_url,
            'truck_url_detail': truck_url_detail,
            'tracking_url': tracking_url,
            'driver_id': driver_id,
            'driver_name': driver_name,
            'driver_mobile': driver_mobile,
            'driver_pic': driver_pic,
            'vehicle_number': vehicle_number,
            'vehicle_model': vehicle_model,
            'pickup_distance': pickup_distance,
            'pickup_distance_done': pickup_distance_done,
            'pickup_time_duration': pickup_time_duration,
            'finished_distance': finished_distance,
            'estimated_distance': estimated_distance,
            'remaining_distance': remaining_distance,
            'total_duration': total_duration,
            'total_distance': float(total_distance)
            if total_distance else finished_distance,
            'estimate': '-',
            'price': total_cost,
            'fare_breakup': {},
            'pickup': pickup,
            'dropoff': dropoff,
            'stops': _stops,
            'payment_status': 'paid',
            'route_info': route_trace_list_dict,
            'booking_datetime': utc_to_ist(trip.planned_start_time)
        }

        return dict_


class TripConsignment(object):

    def get_consignment_inner_table(self, trip, width='20'):
        format_table = '{:{align}{width}}'

        inner_table = format_table.format(
            'Particulars of Substance(s)', align='^', width='20')
        inner_table += format_table.format('No. of Packages',
                                           align='^', width='20')
        inner_table += format_table.format('Gross Weight(kg)',
                                           align='^', width='20')
        inner_table += format_table.format('Net Weight(kg)',
                                           align='^', width='20')
        inner_table += "\n"

        if trip.customer_contract:
            item_categories_count =\
                trip.customer_contract.item_categories.count()

            if item_categories_count:
                random_index = randint(0, item_categories_count - 1)
                item_category = trip.customer_contract.item_categories.all()[
                    random_index]

                random_weight = randint(
                    item_category.min_weight, item_category.max_weight)
                inner_table += format_table.format(item_category.name,
                                                   align='^', width=width)
                inner_table += format_table.format('1', align='^', width=width)
                inner_table += format_table.format(
                    random_weight, align='^', width=width)
                inner_table += format_table.format(
                    random_weight, align='^', width=width)
                inner_table += '\n'

        inner_table += '\n\n\n'

        return inner_table

    def get_consignment_note_content(self, trip, is_pdf_format=False):
        consignment_note_list = []
        width = '30' if is_pdf_format else '20'
        if trip.customer_contract:
            # cust_initial_disp_name = 'Unnamed'
            consignee_address = ''
            consignor_address = ''
            customer = trip.order.customer \
                if trip.order else trip.customer_contract.customer

            if customer:
                cust_legal_name = customer.legalname if customer.legalname \
                    else ''
                cust_name = customer.name if customer.name else ''

            cust_initial_disp_name = cust_legal_name if cust_legal_name \
                else cust_name

            consignor_address = customer.head_office_address \
                if customer and customer.head_office_address else ''

            if trip.order or trip.hub:
                consignee_address = trip.order.pickup_address \
                    if trip.order else trip.hub.address

            note_content = "Trip no. : {}\n\n\n".format(trip.trip_number)
            note_content += "Date and time of dispatch of " + \
                            "consignment : {}\n\n\n".format(
                                trip.actual_start_time.strftime("%d-%b-%Y %H:%M"))
            note_content += "Name and address of the " + \
                            "consignor: \n{}, {}\n\n\n".format(
                                cust_initial_disp_name, str(consignor_address))
            if cust_name:
                note_content += "Name and address of the consignee: " + \
                                "\nNominee/ Buyer of {} (C/o {}), {}\n\n\n".format(
                                    cust_name, cust_legal_name, str(consignee_address))
            else:
                note_content += "Name and address of the consignee: " + \
                                "\nNominee/ Buyer of {}, {}\n\n\n".format(
                                    cust_legal_name, str(consignee_address))
            note_content += "Description and quantity of the " + \
                            "consignment: \n\n"
            note_content += self.get_consignment_inner_table(trip, width)
            note_content += "Mode of transport: \n{} \n\n\n".format(
                trip.vehicle)
            note_content += "Date and time of receipt by the " + \
                            "consignee and his remarks: \n{} \n\n\n".format(
                                trip.actual_end_time.strftime("%d-%b-%Y %H:%M"))
            note_content += "Note : Consignor will be responsible " + \
                            "for paying the service tax. \n\n\n\n\n"
            note_content += "(This is a system generated consignment note hence no signature is required)".center(
                142)

        return note_content

    def generate_consignment_notes(self):
        """
            functionality for generating the consignment notes
            for each completed trip
        """

        query_set_max_limit = 10000
        consignment_note_list = []
        start_time = timezone.now()
        logging.info(
            "Generating Consignment Notes. Start Time = %s" % start_time)

        date_batch = start_time.date()
        desc_batch = date_batch.strftime('%d-%m-%Y')
        batch_description = "Consignment Batch  %s" % (desc_batch)

        trip_set = Trip.objects.filter(
            status=StatusPipeline.TRIP_COMPLETED,
            actual_end_time__lte=timezone.now(),
            has_consignment_note=False).exclude(
            customer_contract__item_categories=None).select_related(
            'customer_contract', 'order',
            'customer_contract__customer',
            'order__pickup_address')
        trip_count = trip_set.count()
        logging.info("Total number of trips : %s" % trip_count)

        trips = trip_set[
            :query_set_max_limit] \
            if trip_count > query_set_max_limit else trip_set

        consignment_batch = TripConsignmentBatch.objects.create(
            description=batch_description,
            number_of_entries=trip_count)
        trip_updated = 0

        for trip in trips:
            content = self.get_consignment_note_content(trip)

            consignment_note_list.append(ConsignmentNote(
                content=content, trip_id=trip.id))

            trip_updated += 1
            Trip.objects.filter(id=trip.id).update(
                has_consignment_note=True,
                consignment_batch=consignment_batch)

        if consignment_note_list:
            ConsignmentNote.objects.bulk_create(consignment_note_list)

        end_time = timezone.now()
        TripConsignmentBatch.objects.filter(id=consignment_batch.id).update(
            start_time=start_time,
            end_time=end_time,
            records_updated=trip_updated)

        total_time = end_time - start_time
        logging.info('Total time taken = %.1f seconds' %
                     total_time.total_seconds())


class TripExportClass(object):

    def get_filename(self):
        file = 'Trip' + '_' + \
            str(format_datetime(datetime.now(), settings.ADMIN_DATE_FORMAT))
        return file

    def get_query(self, params):

        actual_start_time__gte = datetime.strptime(
            params['actual_start_time__gte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('actual_start_time__gte', None) else None
        actual_start_time__lte = datetime.strptime(
            params['actual_start_time__lte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('actual_start_time__lte', None) else None
        planned_start_time__gte = datetime.strptime(
            params['planned_start_time__gte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('planned_start_time__gte', None) \
            else None
        planned_start_time__lte = datetime.strptime(
            params['planned_start_time__lte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('planned_start_time__lte', None) \
            else None

        query_params = []
        if params.get('status'):
            query_params.append(Q(status=params['status']))
        if params.get('is_contingency__exact'):
            query_params.append(Q(
                is_contingency__exact=params['is_contingency__exact']))
        if params.get('hub__id__exact'):
            query_params.append(
                Q(hub__id__exact=params['hub__id__exact']))
        if params.get('has_consignment_note__exact'):
            query_params.append(
                Q(has_consignment_note__exact=params[
                    'has_consignment_note__exact']))
        if params.get('driver__id__exact'):
            query_params.append(
                Q(driver__id__exact=params['driver__id__exact']))
        if params.get('customer_contract__id__exact'):
            query_params.append(
                Q(customer_contract__id__exact=params[
                    'customer_contract__id__exact']))
        if params.get('customer_contract__customer__id__exact'):
            query_params.append(
                Q(customer_contract__customer__id__exact=params[
                    'customer_contract__customer__id__exact']))
        if params.get('customer_contract__city__id__exact'):
            query_params.append(
                Q(customer_contract__city__id__exact=params[
                    'customer_contract__city__id__exact']))
        if params.get('batch_id__id__exact'):
            query_params.append(
                Q(batch_id__id__exact=params['batch_id__id__exact']))
        if actual_start_time__gte and actual_start_time__lte:
            query_params.append(
                Q(actual_start_time__date__range=(
                    actual_start_time__gte, actual_start_time__lte)))
        if planned_start_time__gte and planned_start_time__lte:
            query_params.append(
                Q(planned_start_time__date__range=(
                    planned_start_time__gte, planned_start_time__lte)))

        if query_params:
            return reduce(operator.and_, query_params)
        return query_params

    def get_queryset(self, request):
        query = self.get_query(request.GET.dict())
        if query:
            trip_qs = Trip.objects.filter(query)
        else:
            trip_qs = Trip.objects.all()
        trip_qs = trip_qs.select_related(
            'driver', 'customer_contract', 'order', 'vehicle'
        )

        trip_qs = trip_qs.values('trip_number', 'driver__name',
                                 'vehicle__vehicle_model__vehicle_class',
                                 'vehicle__registration_certificate_number',
                                 'planned_start_time', 'trip_acceptence_time',
                                 'actual_start_time', 'actual_end_time',
                                 'estimated_drop_time', 'total_distance',
                                 'pickup_loading_timestamp', 'status', 'order',
                                 'gps_distance', 'total_time', 'hub__name',
                                 'customer_contract', 'meter_reading_start',
                                 'meter_reading_end', 'mt_start_time',
                                 'mt_end_time', 'assigned', 'delivered_cash',
                                 'attempted', 'rejected', 'pickups',
                                 'returned', 'delivered', 'delivered_mpos')
        return trip_qs
