import argparse
from django.core.management.base import BaseCommand, CommandError
from blowhorn.trip.utils import TripService


class Command(BaseCommand):
    help = ("Marks the trip as NO-SHOW if driver did not turn up."
            "The configuration -hours to wait before marking a trip inactive is defined in the contract")

    def handle(self, *args, **options):
        TripService().mark_noshow_trips()
