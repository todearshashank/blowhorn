import argparse
from django.core.management.base import BaseCommand, CommandError
from blowhorn.trip.margin import MarginCalculation
# import cProfile


class Command(BaseCommand):
    help = 'Generates margin for completed trips for specified period'

    def str2bool(self,v):
        if isinstance(v, bool):
           return v
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    def add_arguments(self, parser):
        parser.add_argument('--start', type = int, help='Start processing from month (Jan-1,Feb-2...)')
        parser.add_argument('--end', type = int, help='End processing till month (Jan-1,Feb-2...)')
        parser.add_argument('--year', type = int, help='Year for which the processing is to be done')
        parser.add_argument('--display', type = self.str2bool ,nargs='?',
                        const=True, default=False ,help='To display the records only (1 for yes else 0)')
        parser.add_argument('--recalculate_for_all', type = self.str2bool ,nargs='?',
                        const=True, default=False ,help='To calculate margins again for all or not')
        parser.add_argument('--contract_id', type = int, help='Contract id for which processing is to be done')

    def handle(self, *args, **kwargs):
        start = kwargs['start']
        end = kwargs['end']
        year = kwargs['year']
        display_only = kwargs.get('display', False)
        recalculate_for_all = kwargs.get('recalculate_for_all', False)
        contract_id = kwargs.get('contract_id', None)

        # pr = cProfile.Profile()
        # pr.enable()
        
        if start and end and year:
            self.stdout.write('Processing for the mentioned period')
            MarginCalculation().generate_margin(start, end, year, 
                    display_only = display_only, 
                    recalculate_for_all = recalculate_for_all , 
                    contract_id = contract_id)
        
        elif not start and not end and not year:
            self.stdout.write('Processing for the default period')
            
            MarginCalculation().start_calculation(display_only = display_only, 
                                                    recalculate_for_all = recalculate_for_all , 
                                                    contract_id = contract_id)

        else:
            self.stdout.write('Please either provide all arguments or none of them')

        # pr.disable()
        # pr.print_stats()
