from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from blowhorn.trip.models import OldTrip
from blowhorn.common.admin import NonEditableAdmin

@admin.register(OldTrip)
class OldTripAdmin(NonEditableAdmin):
    list_display = (
        'old_id', 'flow_key', 'contract_type', 'driver_contract_key', 'default_hub', 'duty_type',
        'driver_number', 'vehicle_key', 'vehicle_class', 'distance_km', 'customer',
        'packages_received', 'return_attempted', 'return_rejected', 'delivered_mpos_packages', 
        'delivered_cash_packages', 'return_orders', 'cash_orders', 'total_orders',
        'cod_count', 'package_count', 'return_count', 'return_count_cod', 'mr_begin',
        'mr_end', 'absent_reason',
    )
