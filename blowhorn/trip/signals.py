from django.dispatch import Signal

trip_planned = Signal(providing_args=["trip", "driver"])

trip_status_changed = Signal(
    providing_args=["trip", "old_status", "new_status"])

trip_line_status_changed = Signal(
    providing_args=["trip_line", "old_status", "new_status"])

trip_stop_status_changed = Signal(
    providing_args=["trip_stop", "old_status", "new_status"])

trip_document_uploaded = Signal(providing_args=["instance"])
