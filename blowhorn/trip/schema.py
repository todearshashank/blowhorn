import graphene
from graphene_django.types import DjangoObjectType
from graphene_django.filter.fields import DjangoFilterConnectionField
from django.db.models import Prefetch
from config.settings import status_pipelines as StatusPipeline
from graphql_relay.node.node import from_global_id

from . import models
from blowhorn.order.models import Order
from blowhorn.driver.models import Driver
from blowhorn.driver.tasks import restore_driver_app
from blowhorn.apps.driver_app.v3.services.order.scan_orders import ScanOrderMixin

class CreateUpdateTrip(graphene.Mutation):
    messages = graphene.List(graphene.String)
    status = graphene.String()

    class Input:
        trip_number = graphene.String(required=False, default_value="")
        order_numbers = graphene.List(graphene.String, required=True)
        driver_id = graphene.String(required=True)

    def mutate(self, info, order_numbers, driver_id, trip_number):
        
        _driver_id = from_global_id(driver_id)[1]
        queryset = Order.objects.filter(number__in=order_numbers)
        queryset.prefetch_related('waypoint_set')
        queryset.prefetch_related('waypoint_set__stops')
        driver = Driver.objects.filter(pk=_driver_id).first()
        trip = None
        if trip_number:
            trip = models.Trip.objects.filter(trip_number=trip_number).first()

        messages = ['Something went wrong.']
        status = 'error'
        proceed = True
        records_with_end_statuses = []
        records_with_ongoing_trip = []  # orders fulfilled by other drivers in an ongoing trip

        for order in queryset:
            if order.status in StatusPipeline.ORDER_END_STATUSES:
                proceed = False
                records_with_end_statuses.append(order.number)
            elif order.status == StatusPipeline.OUT_FOR_DELIVERY or \
                    (order.status == StatusPipeline.ORDER_NEW and order.driver is not None):
                dict_ = {}
                waypoint = order.waypoint_set.all()[0]
                stop = waypoint.stops.all()[0]
                if stop.status in [
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS]:

                    dict_['Order'] = order.number
                    dict_['Trip'] = stop.trip.trip_number
                    proceed = False
                    records_with_ongoing_trip.append(dict_)

        if not proceed:
            if records_with_ongoing_trip:
                messages = ["Orders fulfilled by other ongoing trip: %s." % records_with_ongoing_trip]
                status = 'error'
            if records_with_end_statuses:
                messages = ["Orders are in end statuses, cannot create trip: %s." % records_with_end_statuses]
                status = 'error'

        try:
            trip = ScanOrderMixin(
                context={'request': info.context}, trip_id=trip.id if trip else None,
                hub_associate=info.context.user
            ).create_trip_stops_for_shipment_orders(
                driver=driver,
                order_numbers=queryset.values_list('number', flat=True),
            )
        except:
            messages = ['Driver assigning failed.']
            status = 'error'

        if trip:
            restore_driver_app(driver.id)
            if trip_number:
                messages = ['Orders assigned to trip: %s.' % trip_number, 'Driver: %s | %s.' % (driver.name, driver.driver_vehicle)]
            else:
                messages = ['New trip created and orders assigned.',  'Trip: %s. Driver: %s | %s.' % (trip.trip_number, driver.name , driver.driver_vehicle)]
            status = 'success'
        
        return CreateUpdateTrip(
            messages = messages,
            status = status
        )

class Mutation(graphene.ObjectType):
    create_update_trip = CreateUpdateTrip.Field()
        