import time
from calendar import monthrange
from django.utils import timezone
from django.utils.timezone import make_aware

from blowhorn.contract.constants import (
    AGGREGATE_CYCLE_DAILY,
    AGGREGATE_CYCLE_FIRST_TRIP,
    AGGREGATE_CYCLE_MONTH,
    AGGREGATE_CYCLE_SECOND_TRIP,
    AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP,
    AGGREGATE_CYCLE_TRIP,
    BASE_PAYMENT_CYCLE_DAILY,
    BASE_PAYMENT_CYCLE_FORTNIGHTLY,
    BASE_PAYMENT_CYCLE_MONTH,
    BASE_PAYMENT_CYCLE_PER_TRIP,
    BASE_PAYMENT_CYCLE_WEEK,
    CONTRACT_TYPE_BLOWHORN,
    CONTRACT_TYPE_C2C,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
    SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
    VEHICLE_SERVICE_ATTRIBUTE_HOURS,
    VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES
)

from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.models import *
from blowhorn.customer.submodels.invoice_models import CustomerInvoice, InvoiceTrip
from blowhorn.trip.models import *
from blowhorn.utils.functions import format_datetime
from datetime import datetime

class MarginCalculation:
    def get_factor(self, interval):
        factor = 1.0

        if interval == BASE_PAYMENT_CYCLE_FORTNIGHTLY:
            factor = 15.0
        elif interval == BASE_PAYMENT_CYCLE_MONTH:
            factor = 30.0
        elif interval == BASE_PAYMENT_CYCLE_WEEK:
            factor = 7.0

        return factor

    def set_consider_multiple_slab(self, flag):
        self.consider_multiple_slab = False

        if flag:
            self.consider_multiple_slab = True

    def get_driver_base_payment(self, contractTerm, interval, c2c):
        # Get base payment according to interval specified
        if contractTerm.buy_rate is None or contractTerm.buy_rate.base_payment_interval is None:
            return 0.0

        required_factor = float(self.get_factor(interval))
        base_factor = 1.0

        if c2c is False:
            base_factor = float(self.get_factor(contractTerm.buy_rate.base_payment_interval))

        amount = contractTerm.buy_rate.driver_base_payment_amount

        if amount is not None:
            amount = float(amount)
            amount = (amount * required_factor) / base_factor
        else:
            return 0.0

        return amount

    def get_customer_base_payment(self, contractTerm, interval, c2c):
        if contractTerm.sell_rate is None or contractTerm.sell_rate.base_pay_interval is None:
            return 0.0

        required_factor = self.get_factor(interval)
        base_factor = 1.0

        if c2c is False:
            base_factor = self.get_factor(contractTerm.sell_rate.base_pay_interval)

        amount = contractTerm.sell_rate.base_pay

        if amount is not None:
            amount = float(amount)
            amount = (amount * required_factor) / base_factor
        else:
            return 0.0

        return amount

    def get_driver_id(self, trip_):
        driver_id = trip_.driver_id

        if driver_id:
            return driver_id
        else:
            return 0

    def get_planned_start_time(self, trip):
        return trip.planned_start_time

    def convert_to_vehicle_attribute(self, total_time, attribute):
        # if attribute is hours convert minutes to hours
        tt = 0

        if total_time:
            if attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                return total_time / 60.0
            tt = total_time

        return tt

    def calculate_cumulative_distance(self, start, end, trip_list, trip_list_amount,
                                      kmslab, trip_info_daily, interval,
                                      bsi):
        length = end - start + 1
        max_slab_distance = 0.0
        offset = start

        for slab in kmslab:
            sum_of_distance = 0.0
            flag = False
            consider = False
            amount = 0.0
            cumulative_distance = [0.0] * (length + 1)

            start_dis = slab.start
            end_dis = slab.end
            step = float(slab.step_size)
            rate = float(slab.rate)

            if step == 0.0:
                continue

            if end_dis is None:
                end_dis = 0.0

            base_factor = float(self.get_factor(interval))
            max_slab_distance = max(max_slab_distance, end_dis)

            for index in range(start, end + 1):

                trip = trip_list[index]
                trip_distance = float(trip.total_distance)
                sum_of_distance += trip_distance

                if self.consider_multiple_slab and (bsi == 'S'):
                    if end_dis <= trip_distance:
                        cumulative_distance[index - offset] = end_dis - start_dis

                    else:
                        if trip_distance >= start_dis and trip_distance <= end_dis:
                            cumulative_distance[index - offset] = trip_distance - start_dis
                        else:
                            cumulative_distance[index - offset] = 0

                else:
                    if sum_of_distance >= start_dis and sum_of_distance <= end_dis:
                        if not flag:
                            cumulative_distance[index - offset] = sum_of_distance - start_dis
                            flag = True
                        else:
                            cumulative_distance[index - offset] = trip.total_distance

                    elif sum_of_distance < start_dis or consider:
                        cumulative_distance[index - offset] = 0.0

                    else:
                        max_sum = max(sum_of_distance - trip.total_distance, start_dis)
                        cumulative_distance[index - offset] = end_dis - max_sum
                        consider = True

                fa_per_trip = 0
                if hasattr(slab, 'fixed_amount') and base_factor != 0:
                    if slab.fixed_amount:
                        fa = float(slab.fixed_amount)
                        planned_start_time = format_datetime(trip.planned_start_time, '%Y-%m-%d')
                        qsum = base_factor * trip_info_daily[planned_start_time][trip.driver_id]
                        fa_per_trip = float(fa / qsum)

                if fa_per_trip > 0:
                    if 'fixed_amount' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['fixed_amount'] += fa_per_trip
                    else:
                        trip_list_amount[trip.id]['fixed_amount'] = fa_per_trip

                amount = ((cumulative_distance[index - offset] / step) * rate)
                if amount > 0.0:
                    slab_string = "distance slab [" + str(start_dis) + " - " + str(end_dis) + "] -----> " + str(amount)
                    slab_string += ' @' + str(rate)

                    if 'cumulative_distance' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['cumulative_distance'] += cumulative_distance[index - offset]
                    else:
                        trip_list_amount[trip.id]['cumulative_distance'] = cumulative_distance[index - offset]

                    if 'amount' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['amount'] += amount
                    else:
                        trip_list_amount[trip.id]['amount'] = amount

                    if 'slab' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['slab'].append(slab_string)
                    else:
                        trip_list_amount[trip.id]['slab'] = [slab_string]

    def calculate_cumulative_time(self, start, end, trip_list, trip_list_amount, minslab, trip_info_daily, interval,
                                  bsi):
        length = end - start + 1
        max_slab_time = 0.0
        offset = start

        for slab in minslab:
            total_time = 0.0
            amount = 0.0
            cumulative_time = [0.0] * (length + 1)
            flag = False
            consider = False

            start_time = slab.start
            end_time = slab.end
            step = float(slab.step_size)
            rate = float(slab.rate)

            if step == 0.0:
                continue

            if end_time is None:
                end_time = 0.0

            base_factor = float(self.get_factor(interval))
            max_slab_time = max(max_slab_time, end_time)

            for index in range(start, end + 1):
                trip = trip_list[index]
                trip_time = self.convert_to_vehicle_attribute(trip.total_time, slab.attribute)
                total_time += trip_time

                if self.consider_multiple_slab and (bsi == 'S'):
                    if end_time <= trip_time:
                        cumulative_time[index - offset] = end_time - start_time
                    else:
                        if trip_time >= start_time and trip_time <= end_time:
                            cumulative_time[index - offset] = \
                                trip_time - start_time
                        else:
                            cumulative_time[index - offset] = 0

                else:
                    if total_time >= start_time and trip_time <= end_time:
                        if not flag:
                            cumulative_time[index - offset] = total_time - start_time
                            flag = True
                        else:
                            cumulative_time[index - offset] = trip.total_time

                    elif total_time < start_time or consider:
                        cumulative_time[index - offset] = 0.0

                    else:
                        cumulative_time[index - offset] = end_time - max(total_time - trip.total_time, start_time)
                        consider = True

                fa_per_trip = 0
                if hasattr(slab, 'fixed_amount') and base_factor != 0:
                    if slab.fixed_amount:
                        fa = float(slab.fixed_amount)
                        planned_start_time = format_datetime(trip.planned_start_time, '%Y-%m-%d')
                        qsum = base_factor * trip_info_daily[planned_start_time][trip.driver_id]
                        fa_per_trip = float(fa / qsum)

                if fa_per_trip > 0:
                    if 'fixed_amount' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['fixed_amount'] += fa_per_trip
                    else:
                        trip_list_amount[trip.id]['fixed_amount'] = fa_per_trip

                amount = ((cumulative_time[index - offset] / step) * rate)
                if amount > 0.0:
                    slab_string = "time slab [" + str(start_time) + " - " + str(end_time) + "] -----> " + str(amount)
                    slab_string += ' @' + str(rate)

                    if 'cumulative_time' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['cumulative_time'] += cumulative_time[index - offset]
                    else:
                        trip_list_amount[trip.id]['cumulative_time'] = cumulative_time[index - offset]

                    if 'amount' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['amount'] += amount
                    else:
                        trip_list_amount[trip.id]['amount'] = amount

                    if 'slab' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['slab'].append(slab_string)
                    else:
                        trip_list_amount[trip.id]['slab'] = [slab_string]

    def calculate_distance_km(self, trip_list, trip_list_amount, kmslab, trip_info_daily, interval, bsi):
        length = len(trip_list)
        trip_list.sort(key=self.get_driver_id)
        start_driver = trip_list[0].driver_id
        start_index = 0

        for outer in range(1, length):
            if start_driver != trip_list[outer].driver_id:
                self.calculate_cumulative_distance(start_index, outer - 1, trip_list, trip_list_amount, kmslab,
                                                   trip_info_daily, interval, bsi)
                start_index = outer
                start_driver = trip_list[outer].driver_id

        self.calculate_cumulative_distance(start_index, length - 1, trip_list, trip_list_amount, kmslab,
                                           trip_info_daily, interval, bsi)

    def calculate_time_min(self, trip_list, trip_list_amount, minslab, trip_info_daily, interval, bsi):
        length = len(trip_list)
        trip_list.sort(key=self.get_driver_id)
        start_driver = trip_list[0].driver_id
        start_index = 0
        for outer in range(1, length):
            if start_driver != trip_list[outer].driver_id:
                self.calculate_cumulative_time(start_index, outer - 1, trip_list, trip_list_amount, minslab,
                                               trip_info_daily, interval, bsi)
                start_index = outer
                start_driver = trip_list[outer].driver_id
        self.calculate_cumulative_time(start_index, length - 1, trip_list, trip_list_amount, minslab,
                                       trip_info_daily, interval, bsi)

    def calculate_distance_km_daily(self, trips, trip_list_info, kmslab, trip_info_daily, interval, bsi):
        start = 0
        size_of_list = len(trips)

        for index in range(1, size_of_list):
            if (format_datetime(trips[start].planned_start_time, '%Y-%m-%d')) != (
                    format_datetime(trips[index].planned_start_time, '%Y-%m-%d')):
                self.calculate_distance_km(trips[start:index], trip_list_info, kmslab, trip_info_daily, interval, bsi)
                start = index

        self.calculate_distance_km(trips[start:size_of_list], trip_list_info, kmslab, trip_info_daily, interval, bsi)

    def calculate_time_min_daily(self, trips, trip_list_info, minslab, trip_info_daily, interval, bsi):
        start = 0
        size_of_list = len(trips)

        for index in range(1, size_of_list):
            if (format_datetime(trips[start].planned_start_time, '%Y-%m-%d')) != (
                    format_datetime(trips[index].planned_start_time, '%Y-%m-%d')):
                self.calculate_time_min(trips[start:index], trip_list_info, minslab, trip_info_daily, interval, bsi)
                start = index

        self.calculate_time_min(trips[start:size_of_list], trip_list_info, minslab, trip_info_daily, interval, bsi)

    def calculate_distance_per_trip_slab_level(self, start_index, end_index, trips, trip_list_info, kmslab, bsi):
        count = 1
        for index in range(start_index, end_index + 1):
            trip = trips[index]

            for slab in kmslab:
                distance = trip.total_distance

                start_dis = slab.start
                end_dis = slab.end

                step = float(slab.step_size)
                rate = float(slab.rate)
                amount = 0.0
                interval_distance = 0.0
                fa = 0

                if step == 0.0:
                    continue

                if hasattr(slab, 'fixed_amount'):
                    if slab.fixed_amount:
                        fa = float(slab.fixed_amount)

                if (count == 1 and slab.attribute == AGGREGATE_CYCLE_FIRST_TRIP) or \
                    (count == 2 and slab.attribute == AGGREGATE_CYCLE_SECOND_TRIP) or \
                    (count >= 3 and slab.attribute == AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP) or \
                        slab.attribute == AGGREGATE_CYCLE_TRIP:

                    if self.consider_multiple_slab and (bsi == 'S'):
                        if end_dis <= distance:
                            interval_time = end_dis - start_dis
                        else:
                            if distance >= start_dis and distance <= end_dis:
                                interval_time = distance - start_dis

                    else:
                        if distance >= start_dis and distance <= end_dis:
                            interval_distance = (distance - start_dis)
                        elif distance > end_dis:
                            interval_distance = (end_dis - start_dis)

                    amount = (interval_distance / step) * rate

                if amount > 0.0:
                    slab_string = "distance slab [" + str(start_dis) + " - " + str(end_dis) + "] -----> " + str(amount)
                    slab_string += ' @' + str(rate)

                    if 'cumulative_distance' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['cumulative_distance'] += interval_distance
                    else:
                        trip_list_info[trip.id]['cumulative_distance'] = interval_distance

                    if 'amount' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['amount'] += amount
                    else:
                        trip_list_info[trip.id]['amount'] = amount

                    if 'slab' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['slab'].append(slab_string)
                    else:
                        trip_list_info[trip.id]['slab'] = [slab_string]

                if fa > 0:
                    if 'fixed_amount' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['fixed_amount'] += fa
                    else:
                        trip_list_info[trip.id]['fixed_amount'] = fa

            count += 1

    def calculate_distance_per_trip_day_level(self, trips, trip_list_info, kmslab, bsi):
        length = len(trips)
        trips.sort(key=self.get_driver_id)
        start_driver = trips[0].driver_id
        start_index = 0

        for outer in range(1, length):
            if start_driver != trips[outer].driver_id:
                self.calculate_distance_per_trip_slab_level(start_index, outer - 1, trips, trip_list_info, kmslab, bsi)
                start_index = outer
                start_driver = trips[outer].driver_id

        self.calculate_distance_per_trip_slab_level(start_index, length - 1, trips, trip_list_info, kmslab, bsi)

    def calculate_distance_per_trip(self, trips, trip_list_info, kmslab, bsi):
        trips.sort(key=self.get_planned_start_time)
        start = 0
        size_of_list = len(trips)

        for index in range(1, size_of_list):
            if (format_datetime(trips[start].planned_start_time, '%Y-%m-%d')) != (
                    format_datetime(trips[index].planned_start_time, '%Y-%m-%d')):
                self.calculate_distance_per_trip_day_level(trips[start:index], trip_list_info, kmslab, bsi)
                start = index

        self.calculate_distance_per_trip_day_level(trips[start:size_of_list], trip_list_info, kmslab, bsi)

    def calculate_time_per_trip_slab_level(self, start_index, end_index, trips, trip_list_info, kmslab, bsi):
        count = 1
        for index in range(start_index, end_index + 1):
            trip = trips[index]

            for slab in kmslab:
                time_ = self.convert_to_vehicle_attribute(trip.total_time, slab.attribute)
                start_time = slab.start
                end_time = slab.end
                step = float(slab.step_size)
                rate = float(slab.rate)
                amount = 0.0
                interval_time = 0.0
                fa = 0

                if step == 0.0:
                    continue

                if hasattr(slab, 'fixed_amount'):
                    if slab.fixed_amount:
                        fa = float(slab.fixed_amount)

                if (count == 1 and slab.attribute == AGGREGATE_CYCLE_FIRST_TRIP) or \
                    (count == 2 and slab.attribute == AGGREGATE_CYCLE_SECOND_TRIP) or \
                    (count >= 3 and slab.attribute == AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP) or \
                        slab.attribute == AGGREGATE_CYCLE_TRIP:

                    if self.consider_multiple_slab and (bsi == 'S'):
                        if end_time <= time_:
                            interval_time = end_time - start_time
                        else:
                            if time_ >= start_time and time_ <= end_time:
                                interval_time = time_ - start_time
                    else:
                        if time_ >= start_time and time_ <= end_time:
                            interval_time = (time_ - start_time)
                        elif time_ > end_time:
                            interval_time = (end_time - start_time)

                    amount = (interval_time / step) * rate

                if amount > 0.0:
                    slab_string = "time slab [" + str(start_time) + " - " + str(end_time) + "] -----> " + str(amount)
                    slab_string += ' @' + str(rate)

                    if 'cumulative_time' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['cumulative_time'] += interval_time
                    else:
                        trip_list_info[trip.id]['cumulative_time'] = interval_time

                    if 'amount' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['amount'] += amount
                    else:
                        trip_list_info[trip.id]['amount'] = amount

                    if 'slab' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['slab'].append(slab_string)
                    else:
                        trip_list_info[trip.id]['slab'] = [slab_string]

                if fa > 0:
                    if 'fixed_amount' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['fixed_amount'] += fa
                    else:
                        trip_list_info[trip.id]['fixed_amount'] = fa

            count += 1

    def calculate_time_per_trip_day_level(self, trips, trip_list_info, kmslab, bsi):
        length = len(trips)
        trips.sort(key=self.get_driver_id)
        start_driver = trips[0].driver_id
        start_index = 0

        for outer in range(1, length):
            if start_driver != trips[outer].driver_id:
                self.calculate_time_per_trip_slab_level(start_index, outer - 1, trips, trip_list_info, kmslab, bsi)
                start_index = outer
                start_driver = trips[outer].driver_id

        self.calculate_time_per_trip_slab_level(start_index, length - 1, trips, trip_list_info, kmslab, bsi)

    def calculate_time_per_trip(self, trips, trip_list_info, kmslab, bsi):
        trips.sort(key=self.get_planned_start_time)
        start = 0
        size_of_list = len(trips)

        for index in range(1, size_of_list):
            if (format_datetime(trips[start].planned_start_time, '%Y-%m-%d')) != (
                    format_datetime(trips[index].planned_start_time, '%Y-%m-%d')):
                self.calculate_time_per_trip_day_level(trips[start:index], trip_list_info, kmslab, bsi)
                start = index

        self.calculate_time_per_trip_day_level(trips[start:size_of_list], trip_list_info, kmslab, bsi)

    def calculate_package(self, trip_list, trip_list_amount, slabs, bsi, trip_info_daily=None, interval=None):
        '''
            This method does the shipment calculation for trips on the basis of driver
        '''
        length = len(trip_list)

        trip_list.sort(key=self.get_driver_id)
        start_driver = trip_list[0].driver_id
        start_index = 0
        shipment_function = None

        if trip_info_daily is None and interval is None:
            shipment_function = self.calculate_package_per_trip_slab_level
        else:
            shipment_function = self.calculate_cumulative_package

        # Loop through the trip list
        for outer in range(1, length):
            # send trips for a driver for futher calculation
            if start_driver != trip_list[outer].driver_id:
                shipment_function(start_index, outer - 1, trip_list, trip_list_amount, slabs, bsi,
                                  trip_info_daily, interval)
                start_index = outer
                start_driver = trip_list[outer].driver_id

        shipment_function(start_index, length - 1, trip_list, trip_list_amount, slabs, bsi,
                          trip_info_daily, interval)

    def calculate_package_daily(self, trips, trip_list_info, slabs, bsi, trip_info_daily=None, interval=None):
        '''
            This method does the shipment calculation for trips on the basis of planned start date
        '''
        start = 0
        trips.sort(key=self.get_planned_start_time)
        size_of_list = len(trips)

        # loop through the trip list
        for index in range(1, size_of_list):
            # send trips for a particular start date for further calculation
            if (format_datetime(trips[start].planned_start_time, '%Y-%m-%d')) != (
                    format_datetime(trips[index].planned_start_time, '%Y-%m-%d')):
                self.calculate_package(trips[start:index], trip_list_info, slabs, bsi, trip_info_daily, interval)
                start = index

        self.calculate_package(trips[start:size_of_list], trip_list_info, slabs, bsi, trip_info_daily, interval)

    def get_trip_package_count(self, trip, attribute_string):
        # create a dictionary for different package count for a trip
        package_count = 0

        package_count_dict = {
            SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED: trip.assigned,
            SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED: trip.delivered,
            SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED: trip.attempted,
            SHIPMENT_SERVICE_ATTRIBUTE_REJECTED: trip.rejected,
            SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS: trip.pickups,
            SHIPMENT_SERVICE_ATTRIBUTE_RETURNED: trip.returned,
            SHIPMENT_SERVICE_ATTRIBUTE_MPOS: trip.delivered_mpos,
            SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES: trip.delivered_cash
        }

        # replace the shipment status with the respective count from above dict
        for key, val in package_count_dict.items():
            attribute_string = attribute_string.replace(key, str(val))

        # return the package count
        package_count = eval(attribute_string)
        return package_count

    def calculate_cumulative_package(self, start, end, trip_list, trip_list_amount, slabs, bsi, trip_info_daily,
                                     interval):
        '''
            This method takes a set of trips from trip_list[start:end] and calculates the amount as per the
            slab and fixed amount share as per interval & trip_info_daily for each trip.
            The amount and fixed amount for each trip is added/updated in the trip_list_amount list
        '''
        length = end - start + 1
        offset = start

        # calculate the base factor as per the interval
        base_factor = float(self.get_factor(interval))

        # Start calculation for the amount as per the trip's cumulative package count, rate and slab
        for slab in slabs:
            package_count = 0
            flag = False
            consider = False
            amount = 0.0
            cumulative_package = [0] * (length + 1)

            slab_start = slab.start
            slab_end = 0 if slab.end is None else slab.end
            rate = float(slab.rate)

            # exit if step 0
            if slab.step_size == 0:
                continue

            # process the trips for a driver in trip_list
            for index in range(start, end + 1):
                attribute_string = slab.attribute
                trip = trip_list[index]

                # get the package count for the trip as per the attribute string
                record_package_count = self.get_trip_package_count(trip, attribute_string)
                package_count += record_package_count

                if self.consider_multiple_slab and (bsi == 'S'):
                    if slab_end <= record_package_count:
                        cumulative_package[index - offset] = slab_end - slab_start
                    else:
                        if record_package_count in range(slab_start, slab_end):
                            cumulative_package[index - offset] = \
                                record_package_count - slab_start
                        else:
                            cumulative_package[index - offset] = 0

                else:
                    # cumulative package calculation for trip as per the slab
                    if package_count in range(slab_start, slab_end + 1):
                        if not flag:
                            cumulative_package[index - offset] = package_count - slab_start
                            flag = True
                        else:
                            cumulative_package[index - offset] = record_package_count

                    elif package_count < slab_start or consider:
                        cumulative_package[index - offset] = 0

                    else:
                        temp_count = package_count - record_package_count
                        cumulative_package[index - offset] = slab_end - max(temp_count, slab_start)
                        consider = True

                # calculate the amount as per the cumulative_package, rate and step
                amount = ((cumulative_package[index - offset] / slab.step_size) * rate)

                # update the trip_list_amount for amount and slab key for particular trip
                if amount > 0:
                    slab_string = "shipment slab [" + str(slab_start) + " - " + str(slab_end) + "] -----> "
                    slab_string += str(amount) + ' @' + str(rate)

                    if 'cumulative_package' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['cumulative_package'] += cumulative_package[index - offset]
                    else:
                        trip_list_amount[trip.id]['cumulative_package'] = cumulative_package[index - offset]

                    if 'amount' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['amount'] += amount
                    else:
                        trip_list_amount[trip.id]['amount'] = amount

                    if 'slab' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['slab'].append(slab_string)
                    else:
                        trip_list_amount[trip.id]['slab'] = [slab_string]

                fa_per_trip = 0
                if hasattr(slab, 'fixed_amount') and base_factor != 0:
                    if slab.fixed_amount:
                        fa = float(slab.fixed_amount)
                        planned_start_time = format_datetime(trip.planned_start_time, '%Y-%m-%d')
                        qsum = base_factor * trip_info_daily[planned_start_time][trip.driver_id]
                        fa_per_trip = float(fa / qsum)

                if fa_per_trip > 0:
                    if 'fixed_amount' in trip_list_amount[trip.id]:
                        trip_list_amount[trip.id]['fixed_amount'] += fa_per_trip
                    else:
                        trip_list_amount[trip.id]['fixed_amount'] = fa_per_trip

    def calculate_package_per_trip_slab_level(self, start, end, trips, trip_list_info, slabs, bsi, trip_info_daily=None,
                                              interval=None):
        count = 1

        for index in range(start, end + 1):
            trip = trips[index]
            flag_to_get_package_count_oncefortrip = False
            package_count = 0

            for slab in slabs:
                amount = 0
                cumulative_count = 0

                slab_start = slab.start
                slab_end = 0 if slab.end is None else slab.end

                if slab.step_size == 0:
                    continue

                if not flag_to_get_package_count_oncefortrip:
                    package_count = self.get_trip_package_count(trip, slab.attribute)
                    flag_to_get_package_count_oncefortrip = True

                if (count == 1 and slab.attribute == AGGREGATE_CYCLE_FIRST_TRIP) or \
                    (count == 2 and slab.attribute == AGGREGATE_CYCLE_SECOND_TRIP) or \
                    (count >= 3 and slab.attribute == AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP) or \
                        slab.attribute == AGGREGATE_CYCLE_TRIP:
                    if self.consider_multiple_slab and (bsi == 'S'):
                        if slab_end <= package_count:
                            cumulative_count = slab_end - slab_start
                        else:
                            if package_count in range(slab_start, slab_end):
                                cumulative_count = package_count - slab_start

                    else:
                        if package_count in range(slab_start, slab_end + 1):
                            cumulative_count = (package_count - slab_start)
                        elif package_count > slab_end:
                            cumulative_count = (slab_end - slab_start)

                    amount = (cumulative_count / slab.step_size) * float(slab.rate)

                if amount > 0:
                    slab_string = "shipment slab [" + str(slab_start) + " - " + str(slab_end) + "] -----> "
                    slab_string += str(amount) + ' @' + str(slab.rate)

                    if 'cumulative_package' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['cumulative_package'] += cumulative_count
                    else:
                        trip_list_info[trip.id]['cumulative_package'] = cumulative_count

                    if 'amount' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['amount'] += amount
                    else:
                        trip_list_info[trip.id]['amount'] = amount

                    if 'slab' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['slab'].append(slab_string)
                    else:
                        trip_list_info[trip.id]['slab'] = [slab_string]

                fa = 0
                if hasattr(slab, 'fixed_amount'):
                    if slab.fixed_amount:
                        fa = float(slab.fixed_amount)

                if fa > 0:
                    if 'fixed_amount' in trip_list_info[trip.id]:
                        trip_list_info[trip.id]['fixed_amount'] += fa
                    else:
                        trip_list_info[trip.id]['fixed_amount'] = fa
            count += 1

    def driver_adjustment_calculation(self, start_date, end_date, contract_id, buy_trip_list):
        driver_payments_info = DriverPayment.objects.filter(start_datetime__gte=start_date,
                                                            end_datetime__lte=end_date,
                                                            contract_id=contract_id).prefetch_related(Prefetch('paymenttrip_set__trip'))

        driver_adjustment_modified_date = {}
        for payment in driver_payments_info:
            payment_trips_for_adjustment = payment.paymenttrip_set.all()
            modified_date = payment.modified_date.strftime('%Y-%m-%d')
            count_of_trips = 0.0
            total_adjustment = 0.0

            if payment.adjustment:
                total_adjustment = float(payment.adjustment)

            if total_adjustment != 0.0:
                for payment_trip in payment_trips_for_adjustment:
                    if hasattr(payment_trip, 'trip'):
                        if payment_trip.trip.status == StatusPipeline.TRIP_COMPLETED:
                            count_of_trips += 1.0
                for payment_trip in payment_trips_for_adjustment:
                    if hasattr(payment_trip, 'trip'):
                        if payment_trip.trip.status == StatusPipeline.TRIP_COMPLETED:
                            driver_adjustment_modified_date[payment_trip.trip_id] = modified_date
                            if payment_trip.trip_id in buy_trip_list:
                                buy_trip_list[payment_trip.trip_id]['driver_adjustment'] = float(
                                    total_adjustment / count_of_trips)

        return driver_adjustment_modified_date

    def driver_deduction_calculation(self, contract_term, leave_trips, buy_trip_list):
        deduction_amount = 0.0

        if contract_term.buy_rate is not None and \
                contract_term.buy_rate.driver_deduction_per_leave is not None:
            deduction_amount = float(contract_term.buy_rate.driver_deduction_per_leave)

        if deduction_amount != 0.0 and len(leave_trips) != 0:
            start_d = format_datetime(leave_trips[0].planned_start_time, '%Y-%m-%d')
            number_of_trips = {}
            leave_info_daily = {}

            for trip in leave_trips:
                if (format_datetime(trip.planned_start_time, '%Y-%m-%d')) != start_d:
                    leave_info_daily[start_d] = number_of_trips
                    start_d = format_datetime(trip.planned_start_time, '%Y-%m-%d')

                if trip.driver_id in number_of_trips:
                    number_of_trips[trip.driver_id] += 1.0
                else:
                    number_of_trips[trip.driver_id] = 1.0

            leave_info_daily[start_d] = number_of_trips
            for trip in leave_trips:
                start_d = format_datetime(trip.planned_start_time, '%Y-%m-%d')
                buy_trip_list[trip.id] = {
                    'deduction': (deduction_amount / leave_info_daily[start_d][trip.driver_id])}

    def basepay_and_customer_adjustment(self, c2c, contract_term, trips, trip_info_daily,
                                        buy_trip_list, sell_trip_list,
                                        customer_invoice_trips_info):

        customer_adjustment_modified_date = {}
        customer_base_payment = self.get_customer_base_payment(contract_term,
                                                               BASE_PAYMENT_CYCLE_DAILY, c2c)
        driver_base_payment = self.get_driver_base_payment(contract_term,
                                                           BASE_PAYMENT_CYCLE_DAILY, c2c)

        buy_flag_check = (not c2c) and (contract_term.buy_rate) and \
            (contract_term.buy_rate.base_payment_interval != BASE_PAYMENT_CYCLE_PER_TRIP)
        sell_flag_check = (not c2c) and (contract_term.sell_rate) and \
            (contract_term.sell_rate.base_pay_interval != BASE_PAYMENT_CYCLE_PER_TRIP)

        # Get the base pay per trip - buy_trip_list
        for trip in trips:
            buy_trip_list[trip.id] = {'base_pay': driver_base_payment}
            sell_trip_list[trip.id] = {'base_pay': customer_base_payment}
            customer_id = trip.customer_contract.customer_id
            planned_start_time = format_datetime(trip.planned_start_time, '%Y-%m-%d')

            if buy_flag_check:
                number_of_trips1 = trip_info_daily[planned_start_time][trip.driver_id]
                buy_trip_list[trip.id]['base_pay'] /= number_of_trips1

            # Add the customer adjustment & base pay parameter for the sell_trip_list
            if sell_flag_check:
                number_of_trips2 = trip_info_daily[planned_start_time][trip.driver_id]
                sell_trip_list[trip.id]['base_pay'] /= number_of_trips2

            if customer_id in customer_invoice_trips_info:
                if trip.id in customer_invoice_trips_info[customer_id]['trips']:
                    customer_adjustment_modified_date[trip.id] = \
                        customer_invoice_trips_info[customer_id]['trips'][trip.id]
                    count = customer_invoice_trips_info[customer_id]['count']
                    amount = customer_invoice_trips_info[customer_id]['amount']
                    sell_trip_list[trip.id]['customer_adjustment'] = amount / count

        return customer_adjustment_modified_date

    def trip_invoice_calculation(self, start_date, end_date, key):
        # Fetch the InvoiceTrip for current processing month
        invoice_trips = InvoiceTrip.objects.filter(invoice__service_period_start__gte=start_date,
                                                   invoice__service_period_end__lte=end_date,
                                                   trip__customer_contract_id=key).select_related('invoice')

        customer_invoice_trips_info = {}
        for invoice_trip in invoice_trips:
            customer_id = invoice_trip.invoice.customer_id
            trip_id = invoice_trip.trip_id
            modified_date = invoice_trip.invoice.modified_date.strftime('%Y-%m-%d')

            if customer_id not in customer_invoice_trips_info:
                adjustment_amount = float(invoice_trip.invoice.non_taxable_adjustment)
                adjustment_amount += float(invoice_trip.invoice.taxable_adjustment)
                if adjustment_amount > 0.0:
                    customer_invoice_trips_info[customer_id] = {
                        'trips': {trip_id: modified_date},
                        'count': 1.0,
                        'amount': adjustment_amount
                    }
            else:
                customer_invoice_trips_info[customer_id]['trips'][trip_id] = modified_date
                customer_invoice_trips_info[customer_id]['count'] += 1.0

        return customer_invoice_trips_info

    def get_slab_details(self, contract_term):
        result = {
            'buy_kmslab': None,
            'buy_minslab': None,
            'sell_kmslab': None,
            'sell_minslab': None,
            'shipment_slab_dict': {}
        }
        buy_kmslab = None
        buy_minslab = None
        sell_kmslab = None
        sell_minslab = None
        shipment_slab_dict = {}

        attribute_list = [SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
                          SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
                          SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
                          SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
                          SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
                          SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
                          SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
                          SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
                          SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
                          SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
                          SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
                          SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
                          SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED]

        # Find the buy_vehicle_slab if it exists
        if contract_term.buy_rate:
            buy_vehicle_slab = BuyVehicleSlab.objects.filter(buy_rate_id=contract_term.buy_rate_id)
            buy_shipment_slab = BuyShipmentSlab.objects.filter(buy_rate_id=contract_term.buy_rate_id)

            if buy_vehicle_slab:
                temp = buy_vehicle_slab.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES)
                if temp.exists():
                    result['buy_kmslab'] = temp

                temp = buy_vehicle_slab.filter(~Q(attribute=VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES))
                if temp.exists():
                    result['buy_minslab'] = temp

            if buy_shipment_slab:
                shipment_list = []

                for slab in attribute_list:
                    temp = buy_shipment_slab.filter(attribute=slab)

                    if len(temp) != 0:
                        shipment_list.append(temp)

                shipment_slab_dict['buy'] = shipment_list

        # Find the sell_vehicle_slab if it exists
        if contract_term.sell_rate:
            sell_vehicle_slab = SellVehicleSlab.objects.filter(sell_rate_id=contract_term.sell_rate_id)
            sell_shipment_slab = SellShipmentSlab.objects.filter(sell_rate_id=contract_term.sell_rate_id)

            self.set_consider_multiple_slab(contract_term.sell_rate.apply_step_function)

            if sell_vehicle_slab:
                temp = sell_vehicle_slab.filter(attribute=VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES)
                if temp.exists():
                    result['sell_kmslab'] = temp

                temp = sell_vehicle_slab.filter(~Q(attribute=VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES))
                if temp.exists():
                    result['sell_minslab'] = temp

            if sell_shipment_slab:
                shipment_list = []

                for slab in attribute_list:
                    temp = sell_shipment_slab.filter(attribute=slab)

                    if len(temp) != 0:
                        shipment_list.append(temp)

                    shipment_slab_dict['sell'] = shipment_list

        if len(shipment_slab_dict) != 0:
            result['shipment_slab_dict'] = shipment_slab_dict

        return result

    def get_total_trips_based_on_contract(self, start_date, end_date, contract_id):
        """
            Filter all trips that falls in the date_range,consider modified and unmodified trips as there are
            dependencies between driver and trips (Don't consider 
        """
        valid_status_list = [StatusPipeline.TRIP_COMPLETED, StatusPipeline.TRIP_NO_SHOW,
                             StatusPipeline.TRIP_SUSPENDED, StatusPipeline.TRIP_CANCELLED]

        if contract_id:
            if isinstance(contract_id, int):
                total_trips = Trip.objects.filter(
                    status__in=valid_status_list,
                    planned_start_time__range=(start_date,end_date),
                    customer_contract=contract_id
                ).order_by('planned_start_time')
            else:
                total_trips = None
        else:
            total_trips = Trip.objects.filter(
                status__in=valid_status_list,
                planned_start_time__range=(start_date,end_date),
                blowhorn_contract__isnull=True
            ).order_by('planned_start_time')

        return total_trips

    def trips_and_leave_trips_separation(self, contract_trips):
        trips = []
        leave_trips = []
        only_once_flag = True
        number_of_trips = {}
        trip_info_daily = {}

        # Calculation of trips which are not completed or not
        # Also per day trip count for each driver i.e. trip_info_daily
        for trip in contract_trips:
            if trip.status == StatusPipeline.TRIP_COMPLETED:
                trips.append(trip)

                if only_once_flag:
                    start_d = format_datetime(trips[0].planned_start_time, '%Y-%m-%d')
                    only_once_flag = False

                if (format_datetime(trip.planned_start_time, '%Y-%m-%d')) != start_d:
                    trip_info_daily[start_d] = number_of_trips
                    start_d = format_datetime(trip.planned_start_time, '%Y-%m-%d')
                    number_of_trips = {}

                if trip.driver_id in number_of_trips:
                    number_of_trips[trip.driver_id] += 1.0

                else:
                    number_of_trips[trip.driver_id] = 1.0

            else:
                leave_trips.append(trip)

        if len(number_of_trips) != 0 and not only_once_flag:
            trip_info_daily[start_d] = number_of_trips

        return trips, leave_trips, trip_info_daily

    def update_margin_data(self, display_only, recalculate_for_all, total_trips_list, buy_trip_list, sell_trip_list, customer_adjustment_modified_date, driver_adjustment_modified_date):

        marginhistory_create_list = []
        batch_size = 0
        trip_count = 0
        for trip in total_trips_list:
            # try:
            tid = trip.id

            if tid in buy_trip_list:
                buy_val = buy_trip_list[tid]
                dict1 = {}
                margin_modified = False

                if recalculate_for_all:
                    margin_modified = True

                else:
                    if trip.marginhistory_set.exists() and trip.margin_latest_modified_time:
                        latest_margin_history_date = trip.margin_latest_modified_time.strftime(
                            '%Y-%m-%d')
                        if tid in customer_adjustment_modified_date \
                                and customer_adjustment_modified_date[tid] >= latest_margin_history_date:
                            margin_modified = True

                        if margin_modified is False and tid in driver_adjustment_modified_date \
                                and driver_adjustment_modified_date[tid] >= latest_margin_history_date:
                            margin_modified = True

                        if margin_modified is False and trip.modified_date.strftime(
                                '%Y-%m-%d') > latest_margin_history_date:
                            margin_modified = True
                    else:
                        margin_modified = True

                if margin_modified is True:
                    total_buy_amount = 0.0
                    total_sell_amount = 0.0
                    buy_adjustment_amount = 0.0
                    sell_adjustment_amount = 0.0

                    if 'amount' in buy_val:
                        total_buy_amount += buy_val['amount']
                    if 'base_pay' in buy_val:
                        total_buy_amount += buy_val['base_pay']
                    if 'fixed_amount' in buy_val:
                        total_buy_amount += buy_val['fixed_amount']
                    if 'deduction' in buy_val:
                        total_buy_amount += buy_val['deduction']
                    if 'driver_adjustment' in buy_val:
                        buy_adjustment_amount += buy_val['driver_adjustment']

                    sell_val = {}
                    if tid in sell_trip_list:
                        sell_val = sell_trip_list[tid]
                        if 'amount' in sell_val:
                            total_sell_amount += sell_val['amount']
                        if 'base_pay' in sell_val:
                            total_sell_amount += sell_val['base_pay']
                        if 'fixed_amount' in sell_val:
                            total_sell_amount += sell_val['fixed_amount']
                        if 'customer_adjustment' in sell_val:
                            sell_adjustment_amount += sell_val['customer_adjustment']

                    if 'deduction' in buy_val:
                        dict1['message'] = "Driver deduction is applied"

                    if total_sell_amount + sell_adjustment_amount != 0.0:
                        tot_sell = total_sell_amount + sell_adjustment_amount
                        tot_buy = total_buy_amount + buy_adjustment_amount
                        margin = ((tot_sell - tot_buy) / tot_sell) * 100.0

                    else:
                        margin = -(total_buy_amount + buy_adjustment_amount)

                    margin_details = {'margin_without_adjustment': 0.0}

                    if 'driver_adjustment' in buy_val or 'customer_adjustment' in sell_val:
                        margin_details['margin_with_adjustment'] = margin
                        margin_without_adj = 0.0

                        if total_sell_amount != 0.0:
                            margin_without_adj = (total_sell_amount - total_buy_amount) / total_sell_amount * 100.0
                        else:
                            margin_without_adj = -total_buy_amount

                        margin_details['margin_without_adjustment'] = margin_without_adj

                    else:
                        margin_details = {'margin_without_adjustment': margin}

                    dict1 = {
                        'sell_details': sell_val,
                        'buy_details': buy_val,
                        'margin_details': margin_details
                    }

                    trip_count += 1
                    if display_only:
                        print('-' * 80)
                        print(trip.trip_number, trip.status, margin)
                        print('Sell', self.consider_multiple_slab)
                        print(dict1['sell_details'])
                        print('Buy')
                        print(dict1['buy_details'])
                        print('Margin')
                        print(dict1['margin_details'])

                    else:
                        updated_data = {
                            'margin' : margin,
                            'customer_amount' : (total_sell_amount + sell_adjustment_amount),
                            'driver_amount' : (total_buy_amount + buy_adjustment_amount),
                            'margin_calculation' : dict1,
                            'margin_latest_modified_time' : timezone.now()
                        }
                        
                        Trip.objects.filter(id=tid).update(**updated_data)

                        batch_size += 1
                        mh = MarginHistory(
                            trip=trip,
                            margin=margin,
                            margin_calculation=dict1
                        )
                        marginhistory_create_list.append(mh)

                        if batch_size == 1000:
                            MarginHistory.objects.bulk_create(marginhistory_create_list)
                            batch_size = 0
                            marginhistory_create_list.clear()

            # except Exception as e:
            #     print('Margin job exception',e)

        if not display_only:
            if batch_size != 0:
                MarginHistory.objects.bulk_create(marginhistory_create_list)

        return trip_count

    def start_calculation(self, display_only=False, recalculate_for_all=False, contract_id=None):
        '''
            This method calculates the margins for each trip on monthly basis
            for completed and no show trips for current and last two months trips.
            The calculation is based on three : DriverPayment,InvoiceTrip,Trip
        '''
        count = 0
        current_date = timezone.now()
        month = current_date.month
        year = current_date.year

        kwargs = {
            'display_only' : display_only,
            'recalculate_for_all' : recalculate_for_all,
            'contract_id' : contract_id
        }

        if month > 2:
            self.generate_margin(month-2, month, year, **kwargs)
        else:
            if month == 1:
                start_month = 1 
                end_month = 1

            elif month == 2:
                start_month = 1
                end_month = 2
            
            self.generate_margin(start_month, end_month, year, **kwargs)

            diff = 3 - month

            if diff == 1:
                start_month = 12
                end_month = 12

            elif diff == 2:
                start_month = 11
                end_month = 12

            self.generate_margin(start_month, end_month, year-1, **kwargs)


    def generate_margin(self, start_month, end_month, processing_year, display_only=False, recalculate_for_all=False, contract_id=None):
        '''
            Input:  start_month, end_month and processing_year
                    display_only, recalculate_for_all
            This method calculates the margins for each trip on monthly basis
            for completed trips.
            The calculation is based on three : DriverPayment,InvoiceTrip,Trip
        '''

        # Execute the logic on monthly basis for trips margin
        for month in range(start_month, end_month + 1):
            program_start_time = timezone.now()
            trip_count = 0
            print('Processing for month: ', month, ':', processing_year, ' -- ', program_start_time)
            batch = MarginBatch.objects.create(records_updated=0,
                                               start_time=program_start_time, batch_month=month)

            start_date = make_aware(datetime(processing_year, month, 1))
            last_day_of_month = monthrange(processing_year, month)[1]
            end_date = make_aware(datetime(processing_year, month, last_day_of_month))

            # total_trips = self.get_total_trips_based_on_contract(month, contract_id)
            valid_status_list = [StatusPipeline.TRIP_COMPLETED, StatusPipeline.TRIP_NO_SHOW,
                                 StatusPipeline.TRIP_SUSPENDED, StatusPipeline.TRIP_CANCELLED]

            contract_list = Trip.objects.filter(status__in=valid_status_list,
                                                planned_start_time__range=(start_date,end_date), 
                                                blowhorn_contract__isnull=True
                                                ).values_list('customer_contract_id', flat=True).distinct()

            if contract_list.exists():
                """
                Dividing trips based on contract id 
                """
                for key in contract_list:
                    buy_trip_list = {}
                    sell_trip_list = {}
                    c2c = False

                    contract_term = ContractTerm.objects.filter(contract_id=key, wef__lte=end_date)

                    if contract_term.exists():
                        contract_term = contract_term.latest('wef')

                    else:
                        print('ContractTerm doesnot Exist ---> Going for next contract ', key)
                        continue

                    if contract_term.contract.contract_type == CONTRACT_TYPE_C2C:
                        c2c = True

                    total_trips = self.get_total_trips_based_on_contract(start_date, end_date, key)

                    if not total_trips.exists():
                        print('No Total Trips ---> Going for next contract ')
                        continue

                    # print('0',timezone.now())
                    completed_trips, leave_trips, trip_info_daily = self.trips_and_leave_trips_separation(
                        total_trips)

                    if len(completed_trips) == 0 and len(leave_trips) == 0:
                        print('No Completed & Leave Trips ---> Going for next contract ')
                        continue
                    # Process further is not blowhorn contract
                    # if not total_trips[0].blowhorn_contract:
                    #     print('blowhorn contract so going for next')
                    #     continue

                    # print('1',timezone.now(),len(completed_trips),len(leave_trips),contract_term,key)
                    slab_info = self.get_slab_details(contract_term)
                    # print('slab info')
                    # print(slab_info)
                    # print('*'*50)
                    # print('trip invoice and basepay calculation')
                    # print('2',timezone.now())
                    customer_invoice_trips_info = self.trip_invoice_calculation(start_date, end_date, key)
                    # print('customer_invoice_trips_info', customer_invoice_trips_info)

                    # print('3',timezone.now())
                    i = [c2c, contract_term, completed_trips, trip_info_daily,
                         buy_trip_list, sell_trip_list, customer_invoice_trips_info]
                    customer_adjustment_modified_date = self.basepay_and_customer_adjustment(*i)
                    # print('*'*20, 'buy_trip_list')
                    # print(buy_trip_list)
                    # print('*'*20, 'sell_trip_list')
                    # print(sell_trip_list)
                    # print('*'*50)

                    # print('4',timezone.now())
                    if len(leave_trips) != 0:
                        self.driver_deduction_calculation(contract_term, leave_trips, buy_trip_list)
                    # print('Driver driver_deduction_calculation')
                    # print('buy_trip_list')
                    # print(buy_trip_list)
                    # print('*'*50)
                    # print('5',timezone.now())
                    # import pdb
                    # pdb.set_trace()
                    if completed_trips:
                        buy_kmslab = slab_info.get('buy_kmslab', None)
                        bsi = 'B'
                        if buy_kmslab:
                            # print('buy_kmslab')
                            if buy_kmslab[0].interval == AGGREGATE_CYCLE_MONTH:
                                self.calculate_distance_km(completed_trips, buy_trip_list, buy_kmslab,
                                                           trip_info_daily, BASE_PAYMENT_CYCLE_MONTH, bsi)
                            elif buy_kmslab[0].interval == AGGREGATE_CYCLE_DAILY:
                                self.calculate_distance_km_daily(completed_trips, buy_trip_list, buy_kmslab,
                                                                 trip_info_daily,
                                                                 AGGREGATE_CYCLE_DAILY, bsi)
                            else:
                                self.calculate_distance_per_trip(
                                    completed_trips, buy_trip_list, buy_kmslab, bsi
                                )
                            # print(buy_trip_list)

                        buy_minslab = slab_info.get('buy_minslab', None)
                        if buy_minslab:
                            # print('buy_minslab')
                            if buy_minslab[0].interval == AGGREGATE_CYCLE_MONTH:
                                self.calculate_time_min(completed_trips, buy_trip_list, buy_minslab,
                                                        trip_info_daily, BASE_PAYMENT_CYCLE_MONTH, bsi)
                            elif buy_minslab[0].interval == AGGREGATE_CYCLE_DAILY:
                                self.calculate_time_min_daily(completed_trips, buy_trip_list, buy_minslab,
                                                              trip_info_daily,
                                                              AGGREGATE_CYCLE_DAILY, bsi)
                            else:
                                self.calculate_time_per_trip(
                                    completed_trips, buy_trip_list, buy_minslab, bsi)
                            # print(buy_trip_list)

                        sell_kmslab = slab_info.get('sell_kmslab', None)
                        bsi = 'S'
                        if sell_kmslab:
                            # print('sell_kmslab')
                            if sell_kmslab[0].interval == AGGREGATE_CYCLE_MONTH:
                                self.calculate_distance_km(completed_trips, sell_trip_list, sell_kmslab,
                                                           trip_info_daily, BASE_PAYMENT_CYCLE_MONTH, bsi)
                            elif sell_kmslab[0].interval == AGGREGATE_CYCLE_DAILY:
                                self.calculate_distance_km_daily(completed_trips, sell_trip_list, sell_kmslab,
                                                                 trip_info_daily,
                                                                 AGGREGATE_CYCLE_DAILY, bsi)
                            else:
                                self.calculate_distance_per_trip(
                                    completed_trips, sell_trip_list, sell_kmslab, bsi)

                        sell_minslab = slab_info.get('sell_minslab', None)
                        if sell_minslab:
                            # print('sell_minslab')
                            if sell_minslab[0].interval == AGGREGATE_CYCLE_MONTH:
                                self.calculate_time_min(completed_trips, sell_trip_list, sell_minslab,
                                                        trip_info_daily, BASE_PAYMENT_CYCLE_MONTH, bsi)
                            elif sell_minslab[0].interval == AGGREGATE_CYCLE_DAILY:
                                self.calculate_time_min_daily(completed_trips, sell_trip_list, sell_minslab,
                                                              trip_info_daily,
                                                              AGGREGATE_CYCLE_DAILY, bsi)
                            else:
                                self.calculate_distance_per_trip(
                                    completed_trips, sell_trip_list, sell_minslab, bsi)
                            # print(sell_trip_list)

                        # Start calculation for shipment slabs
                        shipment_slab_dict = slab_info['shipment_slab_dict']
                        for k, slab_list in shipment_slab_dict.items():
                            if k == 'buy':
                                bsi = 'B'
                                temp = buy_trip_list
                            else:
                                bsi = 'S'
                                temp = sell_trip_list

                            for slab in slab_list:
                                if slab:
                                    if slab[0].interval == AGGREGATE_CYCLE_MONTH:
                                        self.calculate_package(completed_trips, temp, slab, bsi,
                                                               trip_info_daily, BASE_PAYMENT_CYCLE_MONTH)
                                    elif slab[0].interval == AGGREGATE_CYCLE_DAILY:
                                        self.calculate_package_daily(completed_trips, temp, slab, bsi,
                                                                     trip_info_daily, BASE_PAYMENT_CYCLE_DAILY)
                                    else:
                                        self.calculate_package_daily(completed_trips, temp, slab, bsi)

                    # Driver Adjustment Calculation
                    # print('6',timezone.now())
                    driver_adjustment_modified_date = self.driver_adjustment_calculation(start_date, end_date,
                                                                                         key, buy_trip_list)
                    # print('driver_adjustment_calculation')
                    # print(driver_adjustment_modified_date)
                    # total_trips_list = completed_trips + leave_trips
                    # print('7',timezone.now())
                    i = [display_only, recalculate_for_all, total_trips,
                         buy_trip_list, sell_trip_list,
                         customer_adjustment_modified_date, driver_adjustment_modified_date]
                    trip_count += self.update_margin_data(*i)
                    # print('8',timezone.now())

            batch.end_time = timezone.now()
            batch.records_updated = trip_count
            print('Total trips ',trip_count)
            batch.save()
