# Django and System libraries
import json
import itertools
import traceback
import logging
import math
import pytz
import re
from collections import Counter

from django.db import transaction
from django.utils import timezone
from openpyxl import load_workbook
from django.conf import settings
from datetime import timedelta,datetime
from django.utils.translation import ugettext_lazy as _

# 3rd party libraries
from rest_framework import status, exceptions
from blowhorn.oscar.core.loading import get_model

# Blowhorn Imports
from blowhorn.customer.constants import TRIP_STOPS_DATA_TO_BE_IMPORTED, STOPS_MANDATORY_FIELDS
from blowhorn.common.helper import CommonHelper
from blowhorn.address.utils import get_city_from_geopoint
from blowhorn.trip.constants import PICKUP, ORDER_DELIVERY
from blowhorn.customer.mixins import CustomerMixin
from config.settings import status_pipelines as StatusPipeline

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

Country = get_model('address', 'Country')
Trip = get_model('trip', 'Trip')
Order = get_model('order', 'Order')
WayPoint = get_model('order', 'WayPoint')
Stop = get_model('trip', 'Stop')
ShippingAddress = get_model('order', 'ShippingAddress')


class TripStopsImport(object):

    def _intialize_attributes(self, data, trip_number):
        """
            This method initializes the variables and perform basic prechecks
            on input file like file name, customer id and empty file check.
            Also does the row processing of the excel sheet where each row
            is equivalent to one order.
        """
        self.filename = data.get('stops')
        self.response = {'data': '', 'errors': ''}
        self.status_code = status.HTTP_200_OK
        self.values_list = []
        self.sequence = 0

        self.trip = Trip.objects.filter(trip_number=trip_number).first()
        if self.trip.stops.exists():
            self.sequence = self.trip.stops.order_by('sequence').last().sequence
        if not self.trip:
            self.response['errors'] = 'Trip does not exist'
            self.status_code = status.HTTP_400_BAD_REQUEST

        # Initialize error lists for further processing
        self._initialize_error_list()

        # Load the workbook and initial validation
        error_message = self._initialize_validations()
        if error_message:
            self.response['errors'] = error_message
            self.status_code = status.HTTP_400_BAD_REQUEST
            return

    def _initialize_validations(self):
        error_message = None

        # Process further only if file name present
        if not self.filename:
            error_message = 'No File Passed'
            return error_message

        # Load the workbook
        wb_obj = load_workbook(filename=self.filename, data_only=True)
        sheet_obj = wb_obj.active
        headers = [cell.value for cell in sheet_obj[1]]
        self.no_of_rows = sheet_obj.max_row

        # Process the rows for order data and throw error if empty file
        row_error = self._row_processing(sheet_obj, headers)

        if not self.values_list:
            error_message = 'File is Empty'
            return error_message

        return error_message

    def _initialize_error_list(self):
        """
            This method initializes the intance variables
            for records with errors required in later processsing
        """

        self.row_index = 1
        self.duplicate_sequence = []
        self.records_with_invalid_latlong = []
        self.out_of_coverage_latlong = []
        self.records_with_invalid_pincodes = []
        self.records_without_mandatory_fields = []
        self.addresses_list = []
        self.waypoints_list = []
        self.stops_data_list = []

        country_code = settings.COUNTRY_CODE_A2
        self.country = Country.objects.get(pk=country_code)

    def input_float_validation(self, data):
        is_valid = False
        data = str(data)
        temp = data.replace('.', '')

        if temp.isdigit():
            data_float = float(data)
            if math.floor(data_float) == data_float:
                is_valid = True
                data = str(int(data_float))

        return is_valid, data

    def _row_processing(self, sheet, headers):
        """
            This method does the enrichment of stops data fetched from excel sheet
        """
        row_error = []
        required_headers = [stop_data['name'] for stop_data in TRIP_STOPS_DATA_TO_BE_IMPORTED]
        required_headers = required_headers + ['Sl No.']
        sequences = []
        index = 1
        for row in sheet.iter_rows(min_row=2):
            stops_data = {}
            index += 1
            for key, cell in zip(headers, row):
                if key != cell.value and cell.value:
                    if key in required_headers:
                        stops_data[key] = cell.value

                    if key == 'Sequence' and cell.value in sequences:
                        self.duplicate_sequence.append(str(index))
                    sequences.append(key)

            if stops_data:
                self.values_list.append(stops_data)
        return row_error

    def __parse_params(self, data):
        fail = False
        customer_mobile = ''

        for i, val in enumerate(data):
            self.row_index += 1
            self.sequence += 1
            geopoint = None

            if not set(list(val.keys())).intersection(set(STOPS_MANDATORY_FIELDS)) == set(STOPS_MANDATORY_FIELDS):
                self.records_without_mandatory_fields.append(str(self.row_index))

            # Stop Type
            stop_type = val.get('Stop Type', 'Drop')
            if stop_type and stop_type == 'Pickup':
                stop_type = PICKUP
            else:
                stop_type = ORDER_DELIVERY

            # Phone Number
            phone_number = val.get('Customer Phone', None) or None
            if phone_number:
                is_number_valid, phone_number = self.input_float_validation(phone_number)
                if is_number_valid:
                    customer_mobile = phone_number

            # Pin Code
            pincode = val.get('Pin Code')

            is_valid, postcode = self.input_float_validation(pincode)

            if not is_valid:
                self.records_with_invalid_pincodes.append(self.row_index)
                pincode = None
                fail = True
            if not fail:
                customer_name = val.get('Customer', '')
                customer_email = val.get('Customer Email', '')

                line1 = val.get('Customer Address')

                latitude = val.get('Latitude', None)
                longitude = val.get('Longitude', None)
                sequence = self.sequence

                if latitude and not re.match("^[+-]*\d+(\.)*(\d+)*$", str(latitude)):
                    self.records_with_invalid_latlong.append(self.row_index)

                if longitude and not self.records_with_invalid_latlong and \
                        not re.match("^[+-]*\d+(\.)*(\d+)*$", str(longitude)):
                    self.records_with_invalid_latlong.append(self.row_index)

                if latitude and longitude and not self.records_with_invalid_latlong:
                    geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)
                    city = get_city_from_geopoint(geopoint, field='coverage_pickup')
                    # if not city:
                    #     self.out_of_coverage_latlong.append(self.row_index)

                self.addresses_list.append(
                    ShippingAddress(**{
                        'first_name': customer_name or '',
                        'phone_number': '%s%s' % (settings.ACTIVE_COUNTRY_CODE, str(customer_mobile)),
                        'line1': line1,
                        'postcode': pincode,
                        'geopoint': geopoint,
                        'country': self.country
                    })
                )

                self.waypoints_list.append(
                    {
                        'sequence_id': sequence,
                        'order': self.trip.order
                    }
                )

                self.stops_data_list.append(
                    {
                        'trip': self.trip,
                        'order': self.trip.order,
                        'status': StatusPipeline.TRIP_NEW,
                        'sequence': sequence,
                        'stop_type': stop_type
                    }
                )
        return fail

    def __raise_error(self, fail):
        error_message = ''

        if self.duplicate_sequence:
            error_message += (
                'Sequence must be unique. Check rows: %s. ' % self.duplicate_sequence
            )

        if self.records_with_invalid_pincodes:
            error_message += (
                'Invalid pincodes given for rows: %s. ' % self.records_with_invalid_pincodes
            )

        if self.records_with_invalid_latlong:
            error_message += (
                'Invalid latitude/longitude given for rows: %s. ' % self.records_with_invalid_latlong
            )

        # if self.out_of_coverage_latlong:
        #     error_message += (
        #         '{} does not provide service for the given geopoints(latitude and longitude) for rows: {}. '.format(
        #             settings.BRAND_NAME, self.out_of_coverage_latlong
        #         )
        #     )

        if self.records_without_mandatory_fields:
            error_message += (
                'Mandatory fields like %s are missing in rows: %s. ' % (
                    STOPS_MANDATORY_FIELDS, self.records_without_mandatory_fields)
            )

        if error_message != '':
            raise exceptions.ValidationError({'errors': error_message})

    def _create_stops(self):
        waypoint_creation_list = []
        stops_creation_data_list = []
        i = 0
        with transaction.atomic():
            shipping_addresses = ShippingAddress.objects.bulk_create(self.addresses_list)
            for i, waypoint in enumerate(self.waypoints_list):
                waypoint['shipping_address_id'] = shipping_addresses[i].id
                waypoint_creation_list.append(WayPoint(**waypoint))

            waypoints = WayPoint.objects.bulk_create(waypoint_creation_list)

            for i, stop_data in enumerate(self.stops_data_list):
                stop_data['stop_address_id'] = shipping_addresses[i].id
                stop_data['waypoint_id'] = waypoints[i].id
                stops_creation_data_list.append(Stop(**stop_data))
            stops = Stop.objects.bulk_create(stops_creation_data_list)

    def _generate_stops_from_data(self, data, filename, no_of_rows):

        fail = self.__parse_params(data)
        self.__raise_error(fail)

        try:
            self._create_stops()
        except BaseException:
            raise exceptions.ValidationError(traceback.format_exc())

        self.response = {
            'trip_number': self.trip.trip_number,
            'number_of_stops': len(self.values_list)
        }
        return
