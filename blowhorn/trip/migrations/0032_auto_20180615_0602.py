# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-06-15 06:02
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
from django.db import migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0031_auto_20180612_0705'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trip',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='trip_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='trip',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='trip_modified_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
