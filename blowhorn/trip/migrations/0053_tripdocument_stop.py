# Generated by Django 2.1.1 on 2020-03-09 06:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0052_auto_20200307_0556'),
    ]

    operations = [
        migrations.AddField(
            model_name='tripdocument',
            name='stop',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='trip.Stop'),
        ),
    ]
