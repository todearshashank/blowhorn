# Generated by Django 2.2.19 on 2021-03-10 09:25

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0060_trip_picked'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='order_ids',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(blank=True, null=True), blank=True, null=True, size=None),
        ),
    ]
