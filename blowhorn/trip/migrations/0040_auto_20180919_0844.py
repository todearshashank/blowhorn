# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-19 08:44
from __future__ import unicode_literals

import audit_log.models.fields
import blowhorn.document.file_validator
import blowhorn.trip.helper
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('trip', '0039_auto_20180829_0751'),
    ]

    operations = [
        migrations.CreateModel(
            name='TripDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('file', models.FileField(blank=True, max_length=256, null=True, upload_to=blowhorn.trip.helper.generate_file_path, validators=[blowhorn.document.file_validator.FileValidator()])),
                ('document_type', models.CharField(choices=[('toll_charges', 'Capture Toll Charges Proof'), ('parking_charges', 'Capture Parking Charges Proof'), ('start_meter_reading_proof', 'Start Odometer Proof'), ('end_meter_reading_proof', 'End Odometer Proof'), ('trip_proof', 'Driver Data Log')], default='start_meter_reading_proof', max_length=80, verbose_name='Document Type')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tripdocument_created_by', to=settings.AUTH_USER_MODEL)),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tripdocument_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Trip document',
                'verbose_name_plural': 'Trip documents',
            },
        ),
        migrations.RemoveField(
            model_name='trip',
            name='meter_reading_end_proof',
        ),
        migrations.RemoveField(
            model_name='trip',
            name='meter_reading_start_proof',
        ),
        migrations.AddField(
            model_name='trip',
            name='parking_charges',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Parking Charges'),
        ),
        migrations.AddField(
            model_name='trip',
            name='toll_charges',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Toll Charges'),
        ),
        migrations.AddField(
            model_name='tripdocument',
            name='trip',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='documents', to='trip.Trip'),
        ),
    ]
