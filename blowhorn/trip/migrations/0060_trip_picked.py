# Generated by Django 2.2.17 on 2021-01-21 16:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0059_auto_20210118_0710'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='picked',
            field=models.IntegerField(default=0, verbose_name='Picked Count'),
        ),
    ]
