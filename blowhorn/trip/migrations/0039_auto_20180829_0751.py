# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-29 07:51
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0038_auto_20180725_0813'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='_gps_distance',
            field=models.FloatField(blank=True, default=0.0, null=True, verbose_name='GPS Distance Mongo (km)'),
        ),
        migrations.AddField(
            model_name='trip',
            name='_route_trace',
            field=django.contrib.gis.db.models.fields.MultiLineStringField(blank=True, null=True, srid=4326, verbose_name='Route Trace Mongo'),
        ),
    ]
