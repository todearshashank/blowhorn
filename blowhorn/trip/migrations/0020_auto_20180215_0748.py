# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-02-15 07:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0019_auto_20180201_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trip',
            name='pickups',
            field=models.IntegerField(default=0, verbose_name='MFN Seller Pickup Count'),
        ),
        migrations.AlterField(
            model_name='trip',
            name='returned',
            field=models.IntegerField(default=0, verbose_name='C Return Count'),
        ),
    ]
