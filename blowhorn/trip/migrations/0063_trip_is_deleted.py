# Generated by Django 2.2.24 on 2021-10-08 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0062_trip_trip_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='is_deleted',
            field=models.NullBooleanField(verbose_name='Is Deleted?'),
        ),
    ]
