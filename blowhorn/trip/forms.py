from django import forms
from django.core.exceptions import ValidationError
from django.utils import timezone
from datetime import timedelta
from blowhorn.oscar.core.loading import get_model
from django.conf import settings
from django.db.models import Q

from config.settings import status_pipelines as StatusPipeline
from blowhorn.utils.functions import utc_to_ist, get_in_ist_timezone
from blowhorn.contract.constants import (
    CONTRACT_STATUS_ACTIVE,
    CONTRACT_TYPE_HYPERLOCAL,
    CONTRACT_TYPE_SHIPMENT,
    CONTRACT_TYPE_BLOWHORN, TOLL_CHARGE, PARKING_CHARGE
)
from blowhorn.trip.constants import METER_READING_CAP

Trip = get_model('trip', 'Trip')
TripConstants = get_model('trip', 'TripConstants')
Driver = get_model('driver', 'Driver')
Contract = get_model('contract', 'Contract')


def AddTrip(user):
    class TripForm(forms.ModelForm):

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['status'].initial = StatusPipeline.TRIP_SCHEDULED_OFF
            self.fields['status'].widget.attrs['readonly'] = True
            self.fields['status'].widget.attrs['disable'] = True

            if self.fields.get('driver', None):
                self.fields['driver'].autocomplete = False
                self.fields['driver'].queryset = Driver.objects.filter(
                        status=StatusPipeline.ACTIVE
                    )
            if self.fields.get('customer_contract', None):
                self.fields['customer_contract'].autocomplete = False
                self.fields[
                    'customer_contract'].queryset = Contract.objects.filter(
                    Q(spocs=user) |
                    Q(supervisors=user),
                    status=CONTRACT_STATUS_ACTIVE,
                    contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]
                ).distinct()

            if self.fields.get('blowhorn_contract', None):
                self.fields['blowhorn_contract'].autocomplete = False
                self.fields[
                    'blowhorn_contract'].queryset = Contract.objects.filter(
                    (Q(spocs=user) |
                    Q(supervisors=user)),
                    status=CONTRACT_STATUS_ACTIVE,
                    contract_type=CONTRACT_TYPE_BLOWHORN
                ).distinct()

        def clean(self, *args, **kwargs):

            start = self.cleaned_data.get('meter_reading_start')
            end = self.cleaned_data.get('meter_reading_end')
            contract = self.cleaned_data.get('customer_contract')
            blowhorn_contract = self.cleaned_data.get('blowhorn_contract')
            hub = self.cleaned_data.get('hub')
            scheduled_at = self.cleaned_data.get('planned_start_time')
            driver = self.cleaned_data.get('driver')

            if not driver:
                raise ValidationError({
                    'driver': 'Driver is mandatory'
                })

            if not scheduled_at:
                raise ValidationError({
                    'planned_start_time': 'Schedule at is mandatory'
                })
            else:
                current_time = utc_to_ist(timezone.now())
                if scheduled_at.date() > current_time.date():
                    raise ValidationError({
                        'planned_start_time':
                            'Scheduled at should be current or past date'
                    })

            trip = Trip.objects.filter(driver=driver,
                                       planned_start_time__date=scheduled_at.date()).exists()

            if trip:
                raise ValidationError(
                    'Trip with selected driver for given schedule at time already exist')

            if not contract and not blowhorn_contract:
                raise ValidationError(
                    'Select a Contract'
                )

            if not hub:
                raise ValidationError({
                    'hub': 'Hub is mandatory'
                })

            if end and not start:
                raise ValidationError(
                    'Enter Start Meter Reading'
                )
            if start and end:
                if end <= start:
                    raise ValidationError(
                        'End Meter Reading should be greater than Start Meter Reading'
                    )
    return TripForm


class DocumentsInlineFormSet(forms.models.BaseInlineFormSet):

    def clean(self):
        super(DocumentsInlineFormSet, self).clean()
        toll_charges, parking_charges = 0, 0

        for form in self.forms:
            data = form.cleaned_data
            document_type = data.get('document_type')
            amount = data.get('amount', 0)
            limit = float(TripConstants().get('TRIP_EXTRA_CHARGE_LIMIT', 200))
            if amount > limit:
                raise ValidationError('amount cannot be greater then %s' % limit)
            if document_type == TOLL_CHARGE:
                toll_charges += amount
            elif document_type == PARKING_CHARGE:
                parking_charges += amount

        if self.instance:
            self.instance.toll_charges = toll_charges
            self.instance.parking_charges = parking_charges
            # self.instance.save()


class TripEditForm(forms.ModelForm):
    def clean(self, *args, **kwargs):
        scheduled_at = get_in_ist_timezone(self.instance.planned_start_time)
        accepted_at = self.cleaned_data.get('trip_acceptence_time')
        status = self.cleaned_data.get('status')
        started_at = self.cleaned_data.get('actual_start_time')
        ended_at = self.cleaned_data.get('actual_end_time')
        mr_start = self.cleaned_data.get('meter_reading_start')
        mr_end = self.cleaned_data.get('meter_reading_end')
        assigned = self.cleaned_data.get('assigned') or 0
        delivered = self.cleaned_data.get('delivered') or 0
        rejected = self.cleaned_data.get('rejected') or 0
        returned = self.cleaned_data.get('returned') or 0
        attempted = self.cleaned_data.get('attempted') or 0
        pickups = self.cleaned_data.get('pickups') or 0
        delivered_cash = self.cleaned_data.get('delivered_cash') or 0
        delivered_mpos = self.cleaned_data.get('delivered_mpos') or 0
        weight = self.cleaned_data.get('weight') or 0
        volume = self.cleaned_data.get('volume') or 0
        package_count = delivered + rejected + returned + \
            attempted + pickups + delivered_cash + delivered_mpos
        is_contingency = self.instance.is_contingency
        driver = self.instance.driver

        if weight < 0 or volume < 0:
            raise ValidationError(
                'Trip Parameters value cannot be negative'
            )

        #if not driver:
        #    raise ValidationError(
        #        'Add driver to trip'
        #    )

        acceptance_hour_before_scheduled_at = None
        acceptance_hour_after_scheduled_at = None
        current_trip = self.instance

        """
        Trip acceptance at range based on configure hours
        """
        if current_trip.customer_contract and \
                current_trip.customer_contract.hours_before_scheduled_at > 0:
            acceptance_hour_before_scheduled_at = scheduled_at - timedelta(hours=current_trip.customer_contract.hours_before_scheduled_at)
        else:
            acceptance_hour_before_scheduled_at = scheduled_at - timedelta(
                hours=int(TripConstants().get(settings.TRIP_ACCEPTANCE_HOUR_AFTER, 0)))

        if current_trip.customer_contract and \
                current_trip.customer_contract.hours_after_scheduled_at > 0:
            acceptance_hour_after_scheduled_at = scheduled_at + timedelta(hours=current_trip.customer_contract.hours_after_scheduled_at)

        else:
            acceptance_hour_after_scheduled_at = scheduled_at + timedelta(
                hours=int(TripConstants().get(settings.TRIP_ACCEPTANCE_HOUR_BEFORE, 0)))

        '''
        started at limit is 24 hours after scheduled date
        ended at limit is 24 hours after starting the trip
        '''
        ended_at_limit = None
        started_at_limit = scheduled_at + timedelta(hours=24)
        if started_at:
            ended_at_limit = started_at + timedelta(hours=24)

        """
        Started at and ended at is mandatory if trip is completed
        """
        if self.instance.status == StatusPipeline.TRIP_COMPLETED and status:
            if not started_at or not ended_at or not accepted_at:
                raise ValidationError('started At/Ended At/Accepted At is '
                                      'mandatory for completed Trips')

        """
        Accepted at can be before/after scheduled at based on configured hours
        """
        if (is_contingency and
                ((accepted_at and accepted_at < acceptance_hour_before_scheduled_at) or
                 (accepted_at and accepted_at > started_at_limit))):
            raise ValidationError(
                'Accepted at should be in range of allowed hours(max 24Hrs from scheduled time)'
            )
        if (not is_contingency and
                ((accepted_at and accepted_at < acceptance_hour_before_scheduled_at) or
                 (accepted_at and accepted_at > acceptance_hour_after_scheduled_at))):
            raise ValidationError(
                'Accepted at should be in range of allowed hours'
            )

        """
        If scheduled is after Started at, Ended at and Accepted at
        raise validation message
        """
        if ((started_at and accepted_at) and accepted_at > started_at) or \
            ((ended_at and accepted_at) and accepted_at > ended_at):
            raise ValidationError(
                'Started at/ Ended at can not be before Accepted at'
            )

        """
        Accepted at is neccessary if Started at given
        """
        if (started_at and not accepted_at) or (ended_at and not accepted_at):
                raise ValidationError(
                    'Please provide Accepted at'
                )

        """
        Accepted at cannot be before Started at
        """
        if (started_at and accepted_at) and \
                (accepted_at > started_at):
            raise ValidationError(
                'Started At can not be before Accepted at'
            )

        """
        Started should be in range of allowed hours
        """
        if not is_contingency and started_at and \
                ((started_at > acceptance_hour_after_scheduled_at) or
                (started_at < acceptance_hour_before_scheduled_at)):
            raise ValidationError('Started at should be in range of allowed hours')

        """
        Start and end of trip can be same
        """
        if (started_at and ended_at) and \
                started_at == ended_at:
            raise ValidationError(
                'Started at and Ended at cannot be same'
            )

        """
        Accepted at cannot be before Ended at
        """
        if (ended_at and accepted_at) and \
                (accepted_at > ended_at):
            raise ValidationError(
                'Ended At can not be before Accepted at'
            )

        """
        Ended at cannot be less than Started at
        """
        if started_at and ended_at:
            if started_at > ended_at:
                raise ValidationError(
                    'Ended At cannot be less than started at'
                )

        """
        Trip Ended at should be within 24 hours after trip starts
        """
        if ended_at and ended_at_limit:
            if ended_at > ended_at_limit:
                raise ValidationError(
                    'Trip shoulb be end with in 24 hours after start '
                )

        """
        Meter end reading and Meter start reading shoulb be valid input
        """
        mr_reading_cap = int(TripConstants().get('METER_READING_CAP', METER_READING_CAP))
        if mr_start:
            if mr_start < 0:
                raise ValidationError(
                    'Meter Start readings cant be negative'
                )
            if mr_start >= mr_reading_cap:
                raise ValidationError(
                    'Meter Start readings cannot be greater than %s' % mr_reading_cap
                )

        if mr_end:
            if mr_end < 0:
                raise ValidationError(
                    'Meter End readings cant be negative'
                )
            if mr_end >= mr_reading_cap:
                raise ValidationError(
                    'Meter End readings cannot be greater than %s' % mr_reading_cap
                )

        """
        Meter end reading is required if end meter reading is given
        """
        if (mr_end and not mr_start) or (mr_start and not mr_end):
            raise ValidationError(
                'Enter Start and End Meter Reading'
            )
        """
        End meter reading should be greater than Start meter reading
        """
        if mr_start and mr_end:
            if mr_end < mr_start:
                raise ValidationError(
                    'End Meter Reading should be greater than Start Meter Reading'
                )

        """
        Start meter reading is neccessary if started at is given vice-versa
        """
        if mr_start and not started_at:
            raise ValidationError(
                'Please Provide Startted At'
            )

        """
        End meter required if ended at is given vice-versa
        """
        if mr_end and not ended_at:
            raise ValidationError(
                'Please Provide Ended At'
            )

        """
        Packages cann't be negative
        """
        if assigned < 0 or delivered < 0 or delivered_mpos < 0 or \
            delivered_cash < 0 or attempted < 0 or rejected < 0 or \
            returned < 0 or pickups < 0:
            raise ValidationError(
                'Please enter valid packages details'
            )

        """
        If package details are provided without start meter reading
        or started at then raise validation error
        """
        if assigned == 0 and package_count > 0:
            raise ValidationError(
                'Please enter assigned packages details'
            )
        if (assigned and not started_at) or \
                package_count and not started_at:
            raise ValidationError(
                'Please enter Time related details'
            )

        """
        If assigned count is less than total packages count
        """
        if assigned < package_count:
            raise ValidationError(
                'Package details are wrong'
            )

        if started_at and accepted_at and ended_at:
            query = Q(driver=current_trip.driver)

            query = query & (Q(actual_start_time__range=(started_at, ended_at))
                | Q(actual_end_time__range=(started_at, ended_at))
                | Q(actual_start_time__lte=started_at, actual_end_time__gte=ended_at)
                | Q(actual_start_time__gte=started_at, actual_end_time__lte=ended_at))

            existing_trip = Trip.objects.filter(query).exclude(id=current_trip.id).exclude(
                status__in = [StatusPipeline.TRIP_NO_SHOW, StatusPipeline.DUMMY_STATUS,
                    StatusPipeline.TRIP_SCHEDULED_OFF, StatusPipeline.TRIP_CANCELLED])
            if existing_trip.exists():
                trip = Trip.objects.get(id=self.instance.id)
                error_trip = trip.validate_trip_conflict(existing_trip)
                raise ValidationError(
                    'Trip Actual start time / Actual End time conflicting with some other Trips'
                    ' or Driver has In-Progress Trip check %s' % error_trip
                )
