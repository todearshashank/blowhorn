from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BrinIndex

class OldTrip(models.Model):
    old_id = models.BigIntegerField(unique=True)
    flow_key = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    contract_type = models.CharField(max_length=20, null=True, blank=True, db_index=True)
    driver_contract_key = models.CharField(max_length=100, null=True, blank=True, db_index=True)
    default_hub = models.CharField(max_length=64, null=True, blank=True, db_index=True)
    duty_type = models.CharField(max_length=20, null=True, blank=True, db_index=True)
    driver_number = models.CharField(max_length=20, null=True, blank=True, db_index=True)
    vehicle_key = models.CharField(max_length=20, null=True, blank=True, db_index=True)
    vehicle_class = models.CharField(max_length=40, null=True, blank=True, db_index=True)
    created_time = models.DateTimeField(null=True, blank=True)
    last_modified = models.DateTimeField(null=True, blank=True)
    distance_km = models.FloatField(null=False, default=0)
    customer = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    packages_received = models.BigIntegerField(null=False, default=0)
    return_attempted = models.BigIntegerField(null=False, default=0)
    return_rejected = models.BigIntegerField(null=False, default=0)
    delivered_mpos_packages = models.BigIntegerField(null=False, default=0)
    delivered_cash_packages = models.BigIntegerField(null=False, default=0)
    return_orders = models.BigIntegerField(null=False, default=0)
    cash_orders = models.BigIntegerField(null=False, default=0)
    total_orders = models.BigIntegerField(null=False, default=0)
    package_count = models.BigIntegerField(null=False, default=0)
    cod_count = models.BigIntegerField(null=False, default=0)
    return_count = models.BigIntegerField(null=False, default=0)
    return_count_cod = models.BigIntegerField(null=False, default=0)
    customer_returns = models.BigIntegerField(null=False, default=0)
    mr_begin = models.BigIntegerField(null=False, default=0)
    mr_end = models.BigIntegerField(null=False, default=0)
    mr_begin_time = models.DateTimeField(null=True, blank=True)
    mr_end_time = models.DateTimeField(null=True, blank=True)
    absent_reason = models.CharField(null=True, blank=True, max_length=50)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _('Old Trip')
        verbose_name_plural = _('Old Trips')
        indexes = (
            BrinIndex(fields=['created_time']),
        )

