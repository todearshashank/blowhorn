from datetime import date
from django.conf import settings
from blowhorn.common.helper import CommonHelper
import json
import re
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
MEDIA_UPLOAD_STRUCTURE = getattr(
    settings, "MEDIA_UPLOAD_STRUCTURE", ""
)

doc_code = 'trip_doc'


def generate_file_path(instance, filename):
    """
    Returns the file path as per the defined directory structure.
    """
    module_name = instance._meta.app_label + "s"
    instance_label = instance.document_type
    instance_handle = instance_label + "_" + str(instance.trip.trip_number)
    file_name = str(date.today()) + "/" + \
        CommonHelper().get_unique_friendly_id() + "/" + filename.upper()

    return MEDIA_UPLOAD_STRUCTURE.format(
        module_name=module_name,
        instance_handle=instance_handle,
        doc_code=doc_code,
        file_name=file_name
    ).replace("//", "/")

def margin_formatter(margin_history):
    """
        Returns the formatted margin display for trips - Operating Margin
    """
    template = None
    try:
        if margin_history.margin_calculation:
            # fetch the margin details if present
            dict1 = json.loads(margin_history.margin_calculation)

            if isinstance(dict1, str):
                dict1 = json.loads(dict1)
            
            margin_details = dict1.get('margin_details',None)
            sell_details = dict1.get('sell_details',None)
            buy_details = dict1.get('buy_details',None)
            
            print(sell_details)
            print(buy_details)
            print(margin_details)

            # initilalize in case not present
            if not margin_details:
                margin_details = {}
            if not buy_details:
                buy_details = {}
            if not sell_details:
                sell_details = {}
            
            # fetch the detials from dictionary and set None in case doesnt exist
            sell_base_pay = sell_details.get('base_pay',0.0)
            buy_base_pay = buy_details.get('base_pay',0.0)
            customer_adjustment = sell_details.get('customer_adjustment',0.0)
            driver_adjustment = buy_details.get('driver_adjustment',0.0)
            sell_amount = sell_details.get('amount',0.0) 
            buy_amount = buy_details.get('amount',0.0)
            margin_with_adjustment = margin_details.get('margin_with_adjustment',0.0)
            margin_without_adjustment = margin_details.get('margin_without_adjustment',0.0)
            deduction = buy_details.get('deduction',0.0)
            credit_notes = sell_details.get('credit_notes',0.0)
            sell_fixed_amount = sell_details.get('fixed_amount',0.0)
            buy_fixed_amount = buy_details.get('fixed_amount',0.0)
            sell_cumulative_package = sell_details.get('cumulative_package',0)
            sell_cumulative_distance = sell_details.get('cumulative_distance',0)
            sell_cumulative_time = sell_details.get('cumulative_time',0)
            buy_cumulative_package = buy_details.get('cumulative_package',0)
            buy_cumulative_distance = buy_details.get('cumulative_distance',0)
            buy_cumulative_time = buy_details.get('cumulative_time',0)

            # extract the slab details 
            slab_dict = slab_extraction(sell_details.get('slab',None),buy_details.get('slab',None))
            distance_slab_list = slab_dict['distance']
            time_slab_list = slab_dict['time']
            shipment_slab_list = slab_dict['shipment']

            # # extract the fixed amount detials
            # fixed_amount_dict = fixed_amount_extraction(sell_details.get('fixed_amount',None),buy_details.get('fixed_amount',None))
            # distance_fa = fixed_amount_dict['distance']
            # time_fa = fixed_amount_dict['time']
            # shipment_fa = fixed_amount_dict['shipment']

            # calculate the total revenue and cost 
            revenue_total = sell_base_pay + sell_amount + customer_adjustment + credit_notes + sell_fixed_amount
            cost_total = buy_base_pay + buy_amount + driver_adjustment + deduction + buy_fixed_amount

            # initialization of the template
            template = """<pre>"""
            template += """<table style="width:80%">"""
            template += """<tr><th colspan='2'><b>REVENUE</></th><th></th><th colspan='2'><b>COST</b></th></tr> """
            template += """<tr><th colspan='2'><b>{}</></th><th></th><th colspan='2'><b>{}</b></th></tr> """.format(
                                    '_'*40,'_'*32)
            template += """<tr style="height: 15px;"></tr>"""
            
            # base pay part of template
            template += """<tr><td>Base Payment</td><td>{:.2f}</td><td></td><td>Base Payment</td><td>{:.2f}</td></tr>""".format(
                                sell_base_pay,buy_base_pay)
            template += """<tr><td>Slab Fixed Amount</td><td>{:.2f}</td><td></td><td>Slab Fixed Amount</td><td>{:.2f}</td></tr>""".format(
                                sell_fixed_amount,buy_fixed_amount)
            template += """<tr style="height: 15px;"></tr>"""

            # cumulative part of template
            if sell_cumulative_distance != 0 or buy_cumulative_distance != 0:
                template += """<tr><td>Cumulative Distance </td><td>{:.2f} km</td><td></td><td>Cumulative Distance</td><td>{:.2f} km</td></tr>""".format(
                                    sell_cumulative_distance,buy_cumulative_distance)
            if sell_cumulative_package != 0 or buy_cumulative_package != 0:
                template += """<tr><td>Cumulative Package </td><td>{:.2f}</td><td></td><td>Cumulative Package</td><td>{:.2f}</td></tr>""".format(
                                    sell_cumulative_package,buy_cumulative_package)
            if sell_cumulative_time != 0 or buy_cumulative_time != 0:
                template += """<tr><td>Cumulative Time </td><td>{:.2f} hours</td><td></td><td>Cumulative Time</td><td>{:.2f} hours</td></tr>""".format(
                                    sell_cumulative_time,buy_cumulative_time)
            
            template += """<tr style="height: 15px;"></tr>"""
            flag = False
            template += """<tr><td>Slab Details :</td><td></td><td></td><td>Slab Details :</td><td></td></tr>"""

            for distance_slab in distance_slab_list:
            # slab and fixed amount part of template
                if not flag:
                    template += """<tr><td>KMs {} @{} </td><td>{}</td><td></td><td>KMs {} @{} </td><td>{}</td></tr>""".format(
                                        *distance_slab)
                    flag = True
                else:
                    template += """<tr><td>    {} @{} </td><td>{}</td><td></td><td>    {} @{} </td><td>{}</td></tr>""".format(
                                        *distance_slab)
                    
            # template += """<tr><td>Fixed Amount - KMs {}</td><td>{}</td><td></td><td>Fixed Amount - KMs {}</td><td>{}</td></tr>""".format(
            #                     *distance_fa)

            flag = False
            for time_slab in time_slab_list:
                if not flag:
                    template += """<tr><td>Hours {} @{} </td><td>{}</td><td></td><td>Hours {} @{} </td><td>{}</td></tr>""".format(
                                        *time_slab)
                    flag = True
                else:
                    template += """<tr><td>      {} @{} </td><td>{}</td><td></td><td>      {} @{} </td><td>{}</td></tr>""".format(
                                        *time_slab)
                    
            # template += """<tr><td>Fixed Amount - Hours {}</td><td>{}</td><td></td><td>Fixed Amount - Hours {}</td><td>{}</td></tr>""".format(
            #                     *time_fa)

            flag = False
            for shipment_slab in shipment_slab_list:
                if not flag:
                    template += """<tr><td>Shipment {} @{} </td><td>{}</td><td></td><td>Shipment {} @{} </td><td>{}</td></tr>""".format(
                                        *shipment_slab)
                    flag = True
                else:
                    template += """<tr><td>         {} @{} </td><td>{}</td><td></td><td>         {} @{} </td><td>{}</td></tr>""".format(
                                        *shipment_slab)
                    
            # template += """<tr><td>Fixed Amount - Shipment {}</td><td>{}</td><td></td><td>Fixed Amount - Shipment {}</td><td>{}</td></tr>""".format(
            #                     *shipment_fa)
            
            # credit notes, customer adjustment and driver adjustment part of template
            template += """<tr style="height: 15px;"></tr>"""
            template += """<tr><td>Invoice Adjustment </td><td>{:.2f}</td><td></td><td>Driver Payment Adjustment </td><td>{:.2f}</td></tr>""".format(
                                customer_adjustment,driver_adjustment)
            template += """<tr><td>Credit Notes </td><td>{:.2f}</td><td></td><td>Deduction</td><td>{:.2f}</td></tr>""".format(
                                credit_notes,deduction)
            
            # total revenue and cost part of template
            template += """<tr><td></td><td>{}</td><td></td><td></td><td>{}</td></tr>""".format('-'*10,'-'*10)
            template += """<tr><td>Revenue Total</td><td>{:.2f}</td><td></td><td>Cost Total</td><td>{:.2f}</td></tr>""".format(
                                revenue_total,cost_total)
            
            # add empty lines to template
            template += """<tr style="height: 15px;"></tr>"""
            template += """<tr style="height: 15px;"></tr>"""
            template += """<tr><td></td><td>{}</td><td>{}</td><td></td><td></td></tr>""".format(
                                '-'*35,'-'*10)

            # margin part of template
            template += """<tr><td></td><td>Difference (Revenue - Cost)</td><td>{:.2f}</td><td></td><td></td></tr>""".format(
                                revenue_total - cost_total)
            template += """<tr style="height: 15px;"></tr>"""
            template += """<tr><td></td><td>Net Margin (with adjustment)</td><td>{:.2f}</td><td></td><td></td></tr>""".format(
                                margin_with_adjustment)
            template += """<tr><td></td><td>Net Margin (without adjustment)</td><td>{:.2f}</td><td></td><td></td></tr>""".format(
                                margin_without_adjustment)
            
            # end the template
            template += """</table>"""
            template += """</pre>"""

    except Exception as e:
        template = None
        logger.info("Exception in generate margin display {} - {}".format(str(e.args),str(margin_history)))
        
    return template


def slab_extraction(slab_sell,slab_buy):
    """
        Extract the slab details from slab info
    """
    result = {'shipment' : [],'time' : [],'distance' : []}

    re_slab = re.compile(r'(\[.*\]) -----> [Rs]*([0-9.]+) \@([0-9.]+)')
    
    if slab_sell:
        for slab in slab_sell:
            slab_grp = re_slab.search(slab)
            if slab_grp:
                t = slab_grp.groups()
                amount = '{:.3f}'.format(float(t[1]))
                temp = [t[0],t[2],amount]

                if 'shipment' in slab:
                    result['shipment'].append(temp)
                    
                if 'time' in slab:
                    result['time'].append(temp)
                    
                if 'distance' in slab:
                    result['distance'].append(temp)

    if slab_buy:
        for slab in slab_buy:
            slab_grp = re_slab.search(slab)
            
            if slab_grp:
                t = slab_grp.groups()
                amount = '{:.3f}'.format(float(t[1]))
                
                if 'shipment' in slab:
                    head = 'shipment'
                    
                if 'time' in slab:
                    head = 'time'
                    
                if 'distance' in slab:
                    head = 'distance'

                slab_dict = result[head]

                if len(slab_dict) == 0:
                    temp = ['',0.0,0.0,t[0],t[2],amount]
                    result[head].append(temp)

                else:
                    added = False
                    for e in slab_dict:
                        if len(e) == 3:
                            e.extend([t[0],t[2],amount])
                            added = True
                            break

                    if not added:
                        temp = ['',0.0,0.0,t[0],t[2],amount]
                        result[head].append(temp)

    for element in result:
        slab_list = result[element]

        for e in slab_list:
            if len(e) == 3:
                e.extend(['',0.0,0.0])


    return result



def fixed_amount_extraction(sell_fixed_amount,buy_fixed_amount):
    """
        Extract the fixed amount details from fixed amount tuple
    """
    result = {'shipment' : ['',0.0,'',0.0],'time' : ['',0.0,'',0.0],'distance' : ['',0.0,'',0.0]}

    loop = [sell_fixed_amount,buy_fixed_amount]

    i = 0
    for fixed_amount_tuple in loop:
        
        if fixed_amount_tuple: 
            
            for fa in fixed_amount_tuple:
                print(fa,'---------------')
                fixed_amount_slab,fixed_amount = fa
                re_fa = re.compile(r'(\[.*\])')

                fa_grp = re_slab.search(fixed_amount_slab)
                print(fa_grp,'-------')

                if fa_grp:
                    t = fa_grp.groups()
                    amount = '{:.3f}'.format(float(fixed_amount))

                    if 'shipment' in fixed_amount_slab:
                        result['shipment'][i:i+2] = t[0],amount
                        
                    if 'time' in fixed_amount_slab:
                        result['time'][i:i+2] = t[0],amount
                        
                    if 'distance' in fixed_amount_slab:
                        result['distance'][i:i+2] = t[0],amount

        i += 2


    return result