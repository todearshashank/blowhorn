from django.conf.urls import url
from django.apps import AppConfig
from blowhorn.trip.views import (
    TripList, TripSummary, TripTrackingView, TripDepartureSheetView,
    TripDocumentUpload, TripStopEdit, StopsExportFileFormatAPIView, StopsImportFileView,
    TripConsignmentNoteView
)
from blowhorn.trip.subviews.route_trace import MongoRouteTraceView, TripRouteTraceView


urlpatterns = [
    url(r'trip/route-trace/mongo$',
        MongoRouteTraceView.as_view(), name='trip-route-trace-mongo'),
    url(r'trip/route-trace$',
        TripRouteTraceView.as_view(), name='trip-route-trace'),

    url(r'^trips$', TripList.as_view(), name='trip-list'),
    url(r'^trip/summary$', TripSummary.as_view(),
        name='trip-summary'),
    url(r'^trip/(?P<number>[\w-]+)/$',
        TripTrackingView.as_view(), name='track-trip'),
    url(r'^trip/(?P<number>[\w-]+)/departure-sheet$',
        TripDepartureSheetView.as_view(), name='trip-departure-sheet'),
    url(r'^order/(?P<number>[\w-]+)/stops/$',
        TripStopEdit.as_view(), name='trip-stop-edit'),
    url(r'^trips/document/(?P<trip_id>[\w-]+)/upload$',
        TripDocumentUpload.as_view(), name='trip-document-upload'),
    url(r'trip/stops/export',
        StopsExportFileFormatAPIView.as_view(), name='trip-stops-upload-template'),
    url(r'trip/stops/import/(?P<trip_number>[\w-]+)',
        StopsImportFileView.as_view(), name='trip-stops-import'),
    url(r'trip/consignment_note/(?P<trip_number>[\w-]+)',
        TripConsignmentNoteView.as_view(), name='trip-consignment-note'),
]
