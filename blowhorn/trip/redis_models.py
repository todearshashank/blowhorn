import json
from django.conf import settings

from blowhorn.common.redis_ds.redis_dict import RedisDict
from blowhorn.common.redis_ds.redis_list import RedisList
from blowhorn.driver.constants import DRIVER_INSTANCE_REDIS_PATTERN

REDIS_HOST = getattr(settings, 'REDIS_HOST', 'localhost')


class TripDocument(RedisList):

    def append(self, val):
        assert type(val) is dict, "Failed to insert - Unsupported type. " \
                                     "Only `dict` objects are allowed."
        self.rpush(val)

    def delete(self, force=True):
        """ Delete this object from redis"""

        if force:
            return super(TripDocument, self).delete()

        raise ValueError("Cannot delete this instance from Redis.")


class Trip(RedisDict):
    """ BlowHorn customer specific details.
    Please note that any details found on redis will be real-time.
    """

    def __init__(self, id, **kwargs):

        RedisDict.__init__(
            self,
            id=str(id),
            fields=self.__get_model(),
            defaults=kwargs
        )

    def __get_model(self):
        """ Returns the model for Trip. Any new fields if required for
        customer in Redis should be added here in below format in below
        dictionary.
        <name> : <value_type>
        Example : <name> : str"""

        return {
            'documents': TripDocument.as_child(self, 'documents', json),
        }

    def update_documents(self, documents):
        """
        Store documents related to current trip object in redis
        """
        for d in documents:
            self['documents'].append(d)

    def get_documents(self, document_types=None):
        """
        Returns documents related to current trip object in redis
        """
        if document_types:
            return [d for d in self['documents'] if d['document_type'] in document_types]
        return [d for d in self['documents']]

    def delete(self, force=True):
        """ Delete this object from redis"""

        if force:
            Trip(self.id)['documents'].delete(force=True)
            return super(Trip, self).delete()

        raise ValueError("Cannot delete this instance from Redis.")
