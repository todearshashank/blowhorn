# System and Django libraries
import os
import csv
import base64
import json
import logging
from datetime import datetime, timedelta

from django import forms
from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin.helpers import ActionForm
from django.contrib.gis.db import models
from django.db import transaction
from django.db.models import Q, DateTimeField
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django.db.models import F
from django.http import HttpResponse
from django.contrib.admin import SimpleListFilter

from blowhorn.document.widgets import ImagePreviewWidget
from blowhorn.utils.datetimepicker import DateTimePicker
from blowhorn.trip.models import MarginHistory, MarginBatch, TripMView
from blowhorn.trip.helper import margin_formatter

# 3rd Party libraries
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from blowhorn.address.models import State, City
from blowhorn.address.utils import state_city_cache
from blowhorn.address.constants import UPDATE_FLAG
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.models import Driver, PaymentTrip, APPROVED, \
    DriverPayment, DriverVehicleMap, DriverGPSInformation, CLOSED
from blowhorn.contract.models import Contract
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_SHIPMENT, \
    CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SPOT, CONTRACT_STATUS_ACTIVE
from blowhorn.address.widgets import GooglePolylineFieldWidget
from .forms import AddTrip, TripEditForm, DocumentsInlineFormSet
from blowhorn.driver.admin import apply_contract_permission_filter
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from .constants import TRIP_ALLOWED_FIELDS_FOR_EDIT
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from blowhorn.utils.functions import utc_to_ist, minutes_to_dhms
from blowhorn.utils.filters import GenericDependancyFilter, \
    multiple_choice_list_filter
from blowhorn.customer.submodels.invoice_models import InvoiceTrip
from blowhorn.utils.admin_list_filter_title import list_filter_title
from blowhorn.trip.utils import TripService
from blowhorn.common.admin import BaseAdmin
from blowhorn.utils.datetime_function import DateTime

from django.utils.html import *

Trip = get_model('trip', 'Trip')
TripDocument = get_model('trip', 'TripDocument')
Hub = get_model('address', 'Hub')
AppEvents = get_model('trip', 'AppEvents')
Stop = get_model('trip', 'Stop')
WayPoint = get_model('order', 'WayPoint')
Order = get_model('order', 'Order')
TripConstants = get_model('trip', 'TripConstants')
TripHistory = get_model('trip', 'TripHistory')
UnassignedTrip = get_model('trip', 'UnassignedTrip')
FeedbackOptions = get_model('trip','FeedbackOptions')
PartnerFeedback = get_model('trip', 'PartnerFeedback')
StopHistory = get_model('trip', 'StopHistory')
WmsManifest = get_model('wms', 'WmsManifest')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class StateFilter(SimpleListFilter):
    title = _('state')
    parameter_name = 'customer_contract__city__state'

    def lookups(self, request, model_admin):
        return [(state.id, state) for state in State.objects.all()]

    def queryset(self, request, queryset):
        if self.value():
            if 'customer_contract__city' in request.GET:
                state = request.GET['customer_contract__city__state']
                city = request.GET['customer_contract__city']
                current_cities = state_city_cache(UPDATE_FLAG).get(state)
                valid_state = False
                if current_cities and int(city) in current_cities:
                    valid_state = True
                if valid_state:
                    return queryset.filter(customer_contract__city_id=city,
                                           customer_contract__city__state__id=state)
                else:
                    qs = Trip.objects.filter(customer_contract__city__state__id=str(self.value()))
                    return qs
            return queryset.filter(customer_contract__city__state__id=str(self.value()))


class CityFilter(SimpleListFilter):
    title = _('city')
    parameter_name = 'customer_contract__city'

    def lookups(self, request, model_admin):
        if 'customer_contract__city__state' in request.GET:
            state = request.GET['customer_contract__city__state']
            cities = City.objects.filter(state=state)
        else:
            cities = City.objects.all()
        return [(city.id, city) for city in cities]

    def queryset(self, request, queryset):
        if self.value():
            if 'customer_contract__city__state' in request.GET:
                state = request.GET['customer_contract__city__state']
                city = request.GET['customer_contract__city']
                current_cities = state_city_cache(UPDATE_FLAG).get(state)
                valid_state = False
                if city in current_cities:
                    valid_state = True
                if valid_state:
                    return queryset.filter(customer_contract__city_id=str(self.value()),
                                           customer_contract__city__state__id=state)

            return queryset.filter(customer_contract__city_id=str(self.value()))


class MarginFilter(SimpleListFilter):
    title = _('Margin')
    parameter_name = 'margin'

    def lookups(self, request, model_admin):
        return [
            ('-5', 'less than -5'),
            ('-5&0', '-5 to 0'),
            ('0', 'less than 0'),
            ('0&5', '0 to 5'),
            ('5&10', '5 to 10'),
            ('10&15', '10 to 15'),
            ('15', 'greater than 15')
        ]

    def queryset(self, request, queryset):
        if self.value():
            val = [int(x) for x in self.value().split('&')]
            if len(val) == 1:
                if val[0] <= 0:
                    return queryset.filter(margin__lt=int(self.value()))
                elif len(val) == 1 and val[0] > 0:
                    return queryset.filter(margin__gte=int(self.value()))
            else:
                return queryset.filter(Q(margin__gte=val[0]) &
                                       Q(margin__lt=val[1]))


class WmsManifestInline(admin.TabularInline):
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    model = WmsManifest

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    extra = 0
    can_delete = False


class DocumentsInline(admin.TabularInline):
    formfield_overrides = {
        models.DateField: {
            "widget": DateTimePicker(options={"format": "DD-MM-YYYY"})
        },
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    model = TripDocument
    formset = DocumentsInlineFormSet
    exclude = ('stop',)

    def has_add_permission(self, request, obj):
        if obj:
            payment_trip = obj.paymenttrip_set.all()

            for trip in payment_trip:
                if (trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]) \
                    and (trip.payment.status in  [CLOSED,APPROVED]):
                    return False
        return True

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.mark_read_only:
            return [f.name for f in self.model._meta.fields]
        return ''
    extra = 0
    can_delete = False


class StopInlineAdmin(admin.TabularInline):
    model = Stop
    fields = ('trip', 'id', 'link_to_order', 'order_status',
              'order_reference_no','order_cash_on_delivery', 'customer_name', 'distance', 'sequence', 'status',
              'stop_type', 'start_time', 'reach_time', 'end_time', 'address')
    readonly_fields = fields
    extra = 0
    exclude = ['route_trace']
    can_delete = False

    def address(self, stop):

        if stop.stop_address:
            address = stop.stop_address
        elif not stop.waypoint:
            address = stop.order.shipping_address if stop.order else None
        else:
            address = stop.waypoint.shipping_address if stop.waypoint else None
        return address

    def link_to_order(self, obj):

        if obj.waypoint:
            order = obj.waypoint.order
            obj.customer_name = obj.waypoint.shipping_address.first_name
            if order.order_type == settings.C2C_ORDER_TYPE and not obj.customer_name:
                obj.customer_name = order.customer.name
        else:
            order = obj.order
            obj.customer_name = order.shipping_address.first_name if order else None
        if not order:
            obj.order_status = '-'
            obj.order_reference_no = '-'
            return None
        obj.order_status = order.status
        obj.order_reference_no = order.reference_number if order.reference_number else '-'

        if order.order_type == settings.SHIPMENT_ORDER_TYPE:
            link = reverse(
                "admin:order_shipmentorder_change", args=[order.id])
            # return u'<a href="%s">%s</a>' % (link, order)
        elif order.order_type == settings.C2C_ORDER_TYPE:
            link = reverse(
                "admin:order_bookingorder_change", args=[order.id])
            # return u'<a href="%s">%s</a>' % (link, order)
        elif order.order_type == settings.SUBSCRIPTION_ORDER_TYPE:
            link = reverse(
                "admin:order_fixedorder_change", args=[order.id])
            # return u'<a href="%s">%s</a>' % (link, order)
        elif order.order_type == settings.SPOT_ORDER_TYPE:
            link = reverse(
                "admin:order_spotorder_change", args=[order.id])

        elif order.order_type == settings.DEAFCOM_ORDER_TYPE:
            link = reverse(
                "admin:order_defcomorder_change", args=[order.id])

        if link and obj:
            return format_html('<a href="%s">%s</a>' % (link, obj))
        return '-'

    # link = reverse("admin:trip_trip_change", args=[obj.id])
    # return format_html('<a href="%s">%s</a>' % (link, obj))

    # format_html("<a href='{}'>{}</a>", url, trip.payment.id)

    def order_status(self, obj):
        return obj.order_status

    def order_cash_on_delivery(self,obj):
        return obj.order.cash_on_delivery

    def order_reference_no(self, obj):
        return obj.order_reference_no

    def customer_name(self, obj):
        return obj.customer_name

    def get_queryset(self, request):
        # Exclude the waypoint which points to the dropoff address
        # already present in the order
        qs = super().get_queryset(request)
        # qs = qs.exclude(order__shipping_address=F('shipping_address'))
        return qs.order_by('sequence')

    link_to_order.allow_tags = True
    link_to_order.short_description = 'Order'

    # Disable the Stops Add button on admin
    def has_add_permission(self, request):
        return False


class TripHistoryInline(admin.TabularInline):
    model = TripHistory
    readonly_fields = ['field', 'old_value',
                       'new_value', 'user', 'modified_time']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DriverGPSInformationInline(admin.TabularInline):
    model = DriverGPSInformation

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class MarginHistoryInline(admin.TabularInline):
    verbose_name_plural = 'Margin History'
    model = MarginHistory
    ordering =['-modified_date']
    readonly_fields = ['margin','modified_date','get_margin_calculation']
    exclude = ['margin_calculation']

    def get_margin_calculation(self, obj):
        if type(obj.margin_calculation) != type(None):
            dict1 = json.loads(obj.margin_calculation)
            dict1 = json.loads(obj.margin_calculation)

            margin_details = dict1.get('margin_details')
            sell_details = dict1.get('sell_details')
            buy_details = dict1.get('buy_details')

            template = """<table>"""

            if type(margin_details) != type(None):
                if 'margin_with_adjustment' in margin_details:
                    template += """<tr><th>Margin with Adjustment:</th><td>{}</td></tr>""".format(
                        round(margin_details['margin_with_adjustment'], 2))
                else:
                    template += """<h3>No adjustments</h3><br>"""

                if 'margin_without_adjustment' in margin_details:
                    template += """<tr><th>Margin Without Adjustment:</th><td>{}</td></tr>""".format(
                        round(margin_details['margin_without_adjustment'], 2))
            template += """</table>"""
            template += """<br>"""
            if 'message' in dict1:
                template += """<p>{}</p>""".format(dict1.get('message'))
            template += """<table style ='border-spacing : 50px 10px;border:3px solid red'>"""
            template += """<tr><th>Sell Details</th><th>Buy Details</th></tr>"""
            template += """<tr>"""
            if 'base_pay' in sell_details:
                template += """
                              <td>Base Pay:{} </td>
                                      """.format(round(sell_details.get('base_pay'), 2))
            if 'base_pay' in buy_details:
                template += """
                             <td>Base Pay:{} </td>
                                     """.format(round(buy_details.get('base_pay'), 2))
            template += """</tr>"""
            template += """<tr><td>"""
            if 'amount' in sell_details:
                template += """
                                 <h2>Total Amount(Slab):{}</h2>
                                 """.format(round(sell_details.get('amount'), 2))
                template += """<p>Slab details :</p>"""
                for slab in sell_details['slab']:
                    template += """<p>{}</p>""".format(slab)
            template += """</td>"""
            template += "<td>"
            if 'amount' in buy_details:
                template += """
                                 <h2>Total Amount(Slab):{}</h2>
                             """.format(round(buy_details.get('amount'), 2))
                template += """<p>Slab details :</p>"""
                for slab in buy_details['slab']:
                    template += """<p>{}</p>""".format(slab)
            template += """</td>"""
            template += """</tr>"""
            # template += format_html_join('',"<tr>{}</tr>",sell_details['slab')
            template += """<tr><td>"""
            # if 'fixed_amount' in sell_details:
            #     template += """<h2>Fixed Amount Details:</h2>"""
            #     for fixed_slab in sell_details['fixed_amount']:
            #         template += """<p>{} ---> {}</p>""".format(fixed_slab[0], fixed_slab[1])
            # template += """</td>"""
            # template += """<td>"""
            # if 'fixed_amount' in buy_details:
            #     template += """<h2>Fixed Amount Details:</h2>"""
            #     for fixed_slab in buy_details['fixed_amount']:
            #         template += """<p>{} ---> {}</p>""".format(fixed_slab[0], fixed_slab[1])
            # template += """</td></tr>"""
            # template += """<tr><td>"""
            if 'customer_adjustment' in sell_details:
                template += """<h2>Customer invoice adjustment :{}</h2>""".format(
                    round(sell_details['customer_adjustment'], 2))
            template += """</td>"""
            template += """<td>"""
            if 'driver_adjustment' in buy_details:
                template += """<h2>driver adjustment</th><td>{}</h2>""".format(
                    round(buy_details['driver_adjustment'], 2))
            template += """</td></tr>"""
            template += """</table>"""
            return format_html(template)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    get_margin_calculation.short_description = _('Margin Calculation Breakdown')



def export_trip_list(modeladmin, request, queryset):
    file_path = 'trips_data'
    tz_diff = DateTime.get_timezone_offset()
    trip_ids = queryset.values_list('id', flat=True)
    if 'RDS_READ_REP_1' in os.environ:
        trip = TripMView.copy_data.using('readrep1').filter(id__in=trip_ids)
    else:
        trip = TripMView.copy_data.filter(id__in=trip_ids)
    trip.to_csv(
        file_path, 'app_version',
        'status', 'trip_number', 'trip_base_pay', 'hub_name',
        'blowhorn_contract_name',
        'commercial_classification',
        'customer_category',
        'contract_type',
        'city_name',
        'customer_name',
        'legalname',
        'division_name',
        'conti', 'precise_gps_distance', 'mongo_gps_distance',
        'formatted_app_distance', 'app_points_count', 'created_by',
        'modified_by', 'driver_name',
        'registration_certificate_number',
        'contract_name', 'number', 'scheduled_date',
        'scheduled_time', 'actual_st_date', 'actual_st_time',
        'end_date', 'end_time', 'precise_total_time',
        'id', 'precise_total_distance', 'meter_reading_start',
        'meter_reading_end', 'assigned', 'delivered',
        'attempted', 'rejected', 'pickups',
        'returned', 'delivered_cash', 'delivered_mpos', 'cum_distance',
            'cum_time', 'toll_amount', 'operating_margin', 'revenue_side', 'cost_side'
    )

    modified_file = '%s_%s.csv' % (
        utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT),
        os.path.splitext(file_path)[0])

    if not os.path.exists(file_path):
        return HttpResponse(400, _('File doesn\'t exists'))
    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'Driver App Version', 'Status', 'Trip Number', 'Supply Base Cost',
            'Hub', 'Blowhorn Contract', 'Vehicle Class',
            'Customer Category', 'Duty Type', 'City', 'Customer Name',
            'Customer LegalName',
            'Customer Division', 'Is Contingency', 'GPS Distance', 'Mongo GPS Distance',
            'App Distance', 'App Points Count',
            'Created By', 'Modified By', 'Driver',
            'Vehicle', 'Customer Contract', 'Order', 'Scheduled Date',
            'Scheduled Time', 'Actual Start Date', 'Actual Start Time',
            'Actual End Date', 'Actual End Time',
            'Total Duration (in minutes)', 'ID',
            'Total Distance', 'Meter Reading Start', 'Meter Reading End',
            'Assigned', 'Delivered', 'Attempted', 'Rejected', 'Pickups',
            'Returned', 'Delivered Cash', 'Delivered MPOS', 'Cumulative Kms',
            'Cumulative Minutes', 'Toll Charges',
            'Operating Margin', 'Revenue Side', 'Cost Side'
        ])

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1
        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + \
                                              os.path.basename(modified_file)
            os.remove(modified_file)
            return response


class TripAdmin(FSMTransitionMixin, admin.ModelAdmin):
    list_display = (
        'trip_number', 'status', 'driver_name', 'customer',
        'vehicle', 'hub_name', 'planned_start_time', 'trip_type',
        'actual_start_time', 'actual_end_time', 'get_trip_base_pay', 'precise_total_distance',
        'precise_gps_distance', 'precise_total_time', 'city',
        'is_contingency', 'get_batch', 'get_consignment_note', 'get_margin', 'get_departure_sheet',
    )

    inlines = [
        StopInlineAdmin, TripHistoryInline, DocumentsInline,
        MarginHistoryInline, DriverGPSInformationInline, WmsManifestInline,]

    fsm_field = ['status', ]

    actions = [export_trip_list]

    related_filter_model_fields = {
        'customer_contract': ['customer_contract__city',
                              'customer_contract__customer'],
        'hub': ['customer_contract__city', 'customer_contract__customer']
    }

    status_filter_config = {
        'title': 'status',
        'lookup_choices': StatusPipeline.TRIP_ALL_STATUSES
    }
    list_filter = [
        CityFilter, StateFilter,
        ('customer_contract__customer', admin.RelatedOnlyFieldListFilter),
        ('customer_contract', GenericDependancyFilter),
        ('hub', GenericDependancyFilter),
        multiple_choice_list_filter(**status_filter_config),
        ('planned_start_time', DateRangeFilter),
        ('actual_start_time', DateRangeFilter),
        'is_contingency', 'blowhorn_contract',
        MarginFilter,
        ('has_consignment_note', list_filter_title('Consignment Note')),
        'app_version', 'app_name']

    list_select_related = (
        'hub', 'vehicle', 'driver',
        'customer_contract__city', 'city',
    )

    search_fields = (
        'trip_number', 'driver__name',
        'vehicle__registration_certificate_number'
    )

    def get_consignment_note(self, obj):
        if obj.consignment_trip and obj.actual_end_time:
            content = base64.b64encode(
                bytes(obj.consignment_trip.content, 'utf-8'))
            data = 'data:text/plain;base64,{}'.format(content.decode("utf-8"))
            return format_html("<div><a href={} download='{}_{}.txt'>{}</a></div>",
                               data, obj.trip_number, datetime.strftime(
                                   obj.actual_end_time, "%d_%m_%Y"),
                               obj.trip_number)
        return ''

    get_consignment_note.short_description = 'Consignment Note'

    def get_departure_sheet(self, obj):

        return format_html('<a href="/departuresheet/%s" target="_blank">Get Departure Sheet</a>' % (
            obj.trip_number))

    get_departure_sheet.short_description = 'Departure Sheet'

    def get_payment_id(self, obj):
        payment_trip = obj.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                url = reverse(
                    'admin:driver_driverpayment_change', args=[trip.payment.id])
                return format_html("<a href='{}'>{}</a>", url, trip.payment.id)
        return ''

    get_payment_id.short_description = 'Driver Payment Id'

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        try:
            if PaymentTrip.objects.filter(trip__id=str(object_id),
                                          payment__status=APPROVED):
                extra_context['show_save'] = True
                extra_context['show_save_and_continue'] = False
                extra_context['show_delete'] = False
            else:
                extra_context['show_save'] = True
                extra_context['show_save_and_continue'] = True
                extra_context['show_delete'] = True
            return super().change_view(
                request, object_id, extra_context=extra_context)

        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(request.user, qs)

        if not request.GET.get('_changelist_filters') and not request.GET.get('planned_start_time__gte') and \
           not request.GET.get('actual_start_time'):
            end = timezone.now() + timedelta(days=10)
            start = end - timedelta(days=55)
            qs = qs.filter(planned_start_time__range=(start, end))

        qs = qs.annotate(hub_name=F('hub__name'), driver_name=F('driver__name'),
                         vehicle_class=F('vehicle__vehicle_model__vehicle_class'),
                         order_type=F('order__order_type', ), order_city=F('order__city'),
                         contract_city=F('customer_contract__city'), order_customer_name=F('order__customer__name'),
                         contract_customer_name=F('customer_contract__customer__name'),
                         contract_name=F('customer_contract__name'), contract_type=F('customer_contract__contract_type'))

        qs = qs.prefetch_related('paymenttrip_set')
        return qs

    def hub_name(self, obj):
        return obj.hub_name

    hub_name.short_description = 'Hub'

    def num_stops(self, obj):
        return 0
        return obj.stops

    num_stops.short_description = 'Stops'
    num_stops.admin_order_field = 'stops'

    def precise_total_time(self, obj):
        if obj.total_time:
            return minutes_to_dhms(obj.total_time)

    def precise_total_distance(self, obj):
        if obj.total_distance:
            return "%.2f" % obj.total_distance

    def precise_gps_distance(self, obj):
        if obj.gps_distance:
            return "%.2f" % obj.gps_distance

    precise_gps_distance.short_description = 'GPS Distance (km)'
    precise_total_distance.short_description = 'Total Distance (km)'
    precise_total_time.short_description = 'Total Time'
    precise_total_time.admin_order_field = 'total_time'

    def city(self, obj):
        if obj.order:
            if obj.order_type == settings.C2C_ORDER_TYPE:
                return obj.order_city

            elif obj.order_type == settings.SUBSCRIPTION_ORDER_TYPE or \
                obj.order_type == settings.DEAFCOM_ORDER_TYPE or \
                obj.order_type == settings.SPOT_ORDER_TYPE:
                return obj.contract_city

        # Shipment order
        else:
            return obj.contract_city

    def customer(self, obj):
        if obj.order:
            if obj.order_type == settings.C2C_ORDER_TYPE:
                return obj.order_customer_name

            elif obj.order_type == settings.SUBSCRIPTION_ORDER_TYPE or \
                obj.order_type == settings.DEAFCOM_ORDER_TYPE or \
                obj.order_type == settings.SPOT_ORDER_TYPE:

                return obj.contract_customer_name
        # Shipment order
        else:
            return obj.contract_customer_name

    def contract(self, obj):
        if obj.order and obj.customer_contract:
            return obj.contract_name
        # Shipment order
        else:
            return obj.customer_contract

    contract.short_description = 'Customer Contract'

    def driver_name(self, obj):
        return obj.driver_name

    def get_vehicle(self, obj):
        return '%s | %s' %(obj.vehicle, obj.vehicle_class)

    def trip_order(self, obj):
        if obj.order:
            link = None
            # order_type = if order.order_type
            if obj.order_type == settings.C2C_ORDER_TYPE:
                link = reverse(
                    "admin:order_bookingorder_change", args=[obj.order.id])
                # return u'<a href="%s">%s</a>' % (link, obj.order)

            elif obj.order_type == settings.SUBSCRIPTION_ORDER_TYPE:
                link = reverse(
                    "admin:order_fixedorder_change", args=[obj.order.id])
                # return u'<a href="%s">%s</a>' % (link, obj.order)

            elif obj.order_type == settings.SPOT_ORDER_TYPE:
                link = reverse(
                    "admin:order_spotorder_change", args=[obj.order.id])
                # return u'<a href="%s">%s</a>' % (link, obj.order)
            elif obj.order_type == settings.DEAFCOM_ORDER_TYPE:
                link = reverse(
                    "admin:order_defcomorder_change", args=[obj.order.id])

            if link and obj.order:
                return format_html('<a href="%s">%s</a>' % (link, obj.order))

        return None

    trip_order.allow_tags = True
    trip_order.short_description = 'Order'

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields':
                    ('driver', 'get_vehicle', 'status', 'customer', 'trip_order',
                     'reason_for_suspension', 'is_contingency', 'get_batch',
                     'trip_workflow', 'current_step', 'get_payment_id', 'get_trip_base_pay',
                     'trip_deduction_amount', 'invoice_driver', 'total_cash_collected', 'app_name',
                     'app_version')
            }),
            ('Operating Margins', {
                'fields': ('get_margin', 'get_customer_amount', 'get_driver_amount',
                           'get_margin_calculation')
            }),
            ('Time Related and Meter Readings', {
                'fields': ('time_related_note', ('planned_start_time',
                                                 'trip_acceptence_time'),
                           ('actual_start_time', 'actual_end_time'),
                           'pickup_loading_timestamp',
                           'total_trip_wait_time_driver', 'precise_total_time',
                           'meter_related_note',
                           'meter_reading_start', 'meter_reading_end',
                           'total_distance')
            }),
            ('Trip Charges', {
                'fields': ('toll_charges', 'parking_charges',)
            }),
            ('Trip Packages', {
                'fields': (('assigned', 'delivered'),
                           ('rejected', 'returned'),
                           ('attempted', 'picked'),
                           ('delivered_cash', 'delivered_mpos'),
                           'pickups', 'order_ids')
            }),
            ('Trip Parameters', {
                'fields': ('volume', 'weight')
            }),
            ('Audit Log', {
                'fields': ('created_date', 'created_by', 'modified_date',
                           'modified_by'),
            }),
        )

        # for existing trip
        if obj:
            # if contract type is shipment
            if obj.customer_contract and obj.contract_type in [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
                fieldsets += (('Others', {
                    'fields':
                        ('customer_contract', 'blowhorn_contract',
                         'hub', 'gps_distance', 'app_distance',
                         'app_points_count', 'otp'),
                }),)
                self.remove_from_fieldsets(
                    fieldsets, ('is_contingency', 'trip_order'))
                self.remove_from_fieldsets(
                    fieldsets, ('reason_for_suspension',))

            # for fixed/spot type of contracts
            else:
                fieldsets += (('Others', {
                    'fields': ('contract', 'blowhorn_contract', 'hub',
                               'gps_distance', '_gps_distance', 'app_distance',
                               'app_points_count', 'otp'),
                }),)
            if obj.trip_workflow and not obj.trip_workflow.end_trip_otp:
                self.remove_from_fieldsets(
                    fieldsets, ('otp',))

        # new trip
        # applicable only for shipment type of contracts
        else:
            fieldsets = (
                ('General', {
                    'fields':
                        ('driver', 'status', 'planned_start_time',
                         'customer_contract', 'blowhorn_contract', 'hub',)
                }),

            )

        if obj and not obj.order:
            self.remove_from_fieldsets(fieldsets, ('is_contingency',))

        return fieldsets

    def get_trip_base_pay(self, obj):
        return obj.trip_base_pay

    get_trip_base_pay.short_description = _("Supply Base Cost")

    def time_related_note(self, obj):
        return format_html('<br>')

    time_related_note.short_description = _("** TIME RELATED")

    def meter_related_note(self, obj):
        return format_html('<br>')

    meter_related_note.short_description = _("** METER READINGS")

    def get_margin(self,obj):
        if obj:
            if obj.margin:
                return round(obj.margin,2)
        return 0.0
    get_margin.short_description = 'Operating Margin'

    def get_customer_amount(self, obj):
        if obj:
            if obj.customer_amount:
                return round(obj.customer_amount, 2)
        return 0.0
    get_customer_amount.short_description = 'Revenue Side'

    def get_driver_amount(self, obj):
        if obj:
            if obj.driver_amount:
                return round(obj.driver_amount,2)
        return 0.0
    get_driver_amount.short_description = 'Cost Side'

    def get_batch(self, obj):
        batch = obj.batch
        if batch:
            url = reverse(
                'admin:order_orderbatch_change', args=[batch.id])
            return format_html('<a target="_blank" href="{}">{}</a>', url, batch)
        return obj.batch
    get_batch.short_description = 'Order Batch'

    def remove_from_fieldsets(self, fieldsets, fields):
        # function to remove fields from fieldset depending on condition
        for fieldset in fieldsets:
            for field in fields:
                if field in fieldset[1]['fields']:
                    new_fields = []
                    for new_field in fieldset[1]['fields']:
                        if new_field not in fields:
                            new_fields.append(new_field)

                    fieldset[1]['fields'] = tuple(new_fields)
                    break

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = TripEditForm
        else:
            self.form = AddTrip(request.user)

        return super(TripAdmin, self).get_form(request, obj, **kwargs)

    def get_margin_calculation(self, obj):

        slip = margin_formatter(obj)
        return format_html(slip)

    get_margin_calculation.short_description = _('Calculation Breakdown')

    '''
    For subscription and spot booking
    Driver, vehicle and customer contract are non editable
    '''

    def get_readonly_fields(self, request, obj=None):
        self.readonly_fields = [
            'created_date', 'created_by', 'modified_date', 'trip_order',
            'modified_by', 'precise_total_time', 'total_distance',
            'customer', 'contract', 'is_contingency', 'blowhorn_contract',
            'mt_start_time', 'mt_end_time', 'actual_start_time',
            'actual_end_time', 'trip_acceptence_time', 'get_batch',
            'get_payment_id', 'app_name', 'app_version', 'trip_type',
            'total_cash_collected', 'time_related_note', 'meter_related_note',
            'gps_distance', '_gps_distance', 'app_distance', 'app_points_count',
            'toll_charges', 'parking_charges', 'get_margin', 'get_customer_amount',
            'get_driver_amount', 'get_margin_calculation', 'get_trip_base_pay',
            'trip_deduction_amount', 'order_ids', 'otp']

        if obj:
            payment_trip = PaymentTrip.objects.filter(
                trip__id=obj.id,
                payment__status=APPROVED,
                payment__payment_type=DriverPayment.PAYMENT_CHOICES[0][0])
            obj.mark_read_only = payment_trip.exists() or obj.invoice_approved
            self.readonly_fields += [
                'status', 'hub', 'driver', 'customer_contract',
                'get_vehicle', 'planned_start_time']

            if (obj.status not in StatusPipeline.TRIP_END_STATUSES) or \
                    (obj.status == StatusPipeline.TRIP_COMPLETED and not
                    obj.mark_read_only):

                self.readonly_fields.remove('actual_start_time')
                self.readonly_fields.remove('trip_acceptence_time')
                self.readonly_fields.remove('actual_end_time')

            elif obj.status in StatusPipeline.TRIP_END_STATUSES:
                self.readonly_fields += [
                    f.name for f in self.model._meta.fields]
                self.readonly_fields.remove('route_trace')
                self.readonly_fields.remove('invoice_driver')

        else:
            self.readonly_fields.remove('blowhorn_contract')
            self.readonly_fields += [
                'meter_reading_start', 'meter_reading_end',
                'assigned', 'delivered', 'attempted', 'rejected',
                'pickups', 'returned', 'delivered_cash', 'delivered_mpos',
                'total_trip_wait_time_driver']

        self.readonly_fields += ['trip_workflow', 'current_step']

        return self.readonly_fields

    def save_model(self, request, obj, form, change):

        trip_list = []
        current_user = request.user

        if change:
            if 'invoice_driver' in form.changed_data:
                InvoiceTrip.objects.filter(trip__id=obj.id).update(
                    invoice_driver=obj.invoice_driver)
            old = Trip.objects.get(pk=obj.id)
            for field in old._meta.fields:

                field_name = field.__dict__.get('name')

                if field_name in TRIP_ALLOWED_FIELDS_FOR_EDIT:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(obj)

                    if old_value != current_value:
                        if isinstance(field, DateTimeField):
                            # Changing the already existing datetime value to
                            # ist as it is saved as utc in the Trip model.
                            # Keeping the current datetime value as it is
                            # because it will be in the ist format itself
                            old_value = datetime.strftime(utc_to_ist(
                                old_value), settings.ADMIN_DATETIME_FORMAT) \
                                if old_value else None
                            current_value = datetime.strftime(
                                current_value,
                                settings.ADMIN_DATETIME_FORMAT) \
                                if current_value else None

                        trip_list.append(TripHistory(field=field_name,
                                                     old_value=str(old_value),
                                                     new_value=current_value,
                                                     user=current_user,
                                                     trip=old))

            TripHistory.objects.bulk_create(trip_list)
        else:
            if form.cleaned_data.get('driver') and not \
                    form.cleaned_data.get('vehicle'):
                obj.vehicle = DriverVehicleMap.objects.get(
                    driver=form.cleaned_data.get('driver')).vehicle
                driver = form.cleaned_data.get('driver')
                driver_qs = Driver.objects.filter(pk=driver.id
                                                  ).select_related(
                    'contract').first()
                if driver_qs.contract:
                    obj.blowhorn_contract = driver_qs.contract

        super().save_model(request, obj, form, change)

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(TripAdmin, self). \
            formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "hub":
            ''' Show only shipment contract hubs'''
            formfield.queryset = Hub.objects.filter(customer_id__isnull=True)
        return formfield

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',
                    '/static/admin/css/trip.css',)
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            '/static/admin/js/on_delete_pop_up.js',
            '/static/admin/js/trip_confirmation.js',
            '/static/admin/js/trip/main.js',
        )

    class Meta:
        fields = '__all__'


class ShipmentTripSummary(Trip):

    class Meta:
        proxy = True
        verbose_name = _('Shipment Trip Summary')
        verbose_name_plural = _('Shipment Trips Summary')


class ShipmentTripSummaryAdmin(admin.ModelAdmin):

    actions = None
    list_display = ('driver', 'hub', 'trip_number', 'status',
                    'actual_start_time', 'total_orders', 'out_for_delivery',
                    'delivered',
                    'rejected', 'unable_to_deliver')

    exclude = ['route_trace', 'current_step']
    inlines = [
        StopInlineAdmin,
    ]
    list_filter = ['status',
                   ('driver', admin.RelatedOnlyFieldListFilter),
                   ('hub', admin.RelatedOnlyFieldListFilter),
                   'actual_start_time']

    list_select_related = (
        'driver', 'hub',
    )

    ''' change_view and add_view are used here
        to disable save and continue button
    '''

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        # extra_context['show_save_and_add_another'] = False
        extra_context['show_save'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    search_fields = ('trip_number',)

    def get_fieldsets(self, request, obj=None):
        fieldsets = ()
        return fieldsets

    def total_orders(self, obj):

        self.out_for_delivery_orders = 0
        self.delivered_orders = 0
        self.rejected_orders = 0
        self.unable_to_deliver_orders = 0

        stop_qs = Stop.objects.filter(
            trip=obj
        )

        for stop in stop_qs:
            order = stop.waypoint.order if stop.waypoint else stop.order
            if order.status == StatusPipeline.OUT_FOR_DELIVERY:
                self.out_for_delivery_orders += 1
            elif order.status == StatusPipeline.ORDER_DELIVERED:
                self.delivered_orders += 1
            elif order.status == StatusPipeline.ORDER_RETURNED:
                self.rejected_orders += 1
            elif order.status == StatusPipeline.UNABLE_TO_DELIVER:
                self.unable_to_deliver_orders += 1

        return stop_qs.count()

    def delivered(self, obj):
        return self.delivered_orders

    def rejected(self, obj):
        return self.rejected_orders

    def unable_to_deliver(self, obj):
        return self.unable_to_deliver_orders

    def out_for_delivery(self, obj):
        return self.out_for_delivery_orders

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        return qs.filter(
            order=None,
            status__in=[StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.TRIP_CANCELLED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE]
        )

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    # To disable the Delete button on admin
    def has_delete_permission(self, request, obj=None):
        return False


class AppEventsAdmin(admin.ModelAdmin):

    list_display = ('event', 'driver', 'hub_associate', 'order', 'trip',
                    'created_time',)

    readonly_fields = ('event', 'driver', 'hub_associate', 'order', 'trip',
                       'created_time', 'stop')

    search_fields = ('order__number', 'trip__trip_number')

    list_filter = [('driver', admin.RelatedOnlyFieldListFilter),
                   'event',
                   ('hub_associate', admin.RelatedOnlyFieldListFilter),
                   'created_time']

    actions = ['export_app_events_data']

    list_select_related = (
        'driver', 'hub_associate', 'order', 'trip',
        'stop',
    )

    def export_app_events_data(self, request, queryset):
        ids = queryset.values_list('id', flat=True)
        queryset = AppEvents.copy_data.filter(id__in=ids)

        file_path = 'driver_details'

        queryset.annotate(
        ).to_csv(
            file_path,
            'event', 'driver__name', 'driver__vehicles__registration_certificate_number',
            'hub_associate', 'order', 'trip__trip_number', 'created_time'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow(['Event Triggered', 'Driver', 'Driver Vehicle', 'Hub Associate', 'Order', 'Trip',
                        'Created Time'
                        ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response

    export_app_events_data.short_description = 'Export App Events details in excel'

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


class TripConstantsAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'description')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['name']
        else:
            return []


# This is a temporary view for reassigning the contract for trip
class TripProxy(Trip):
    class Meta:
        proxy = True
        verbose_name = _('Contract Trip Update View')
        verbose_name_plural = _('Contract Trip Update View')


def update_trip_detail(modeladmin, request, queryset):
    print('Updating the trips...')
    contract_id = request.POST.get('contract')
    if not contract_id:
        messages.error(request, 'Contract is required')
        return False

    contract = Contract.objects.get(pk=contract_id)
    if contract.contract_type not in [CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SPOT]:
        messages.error(request, 'Contract type should be either FIXED or SPOT')
        return False

    allowed_trip_numbers = []
    rejected_trip_numbers = []
    for trip in queryset:
        if trip.customer_contract.customer == contract.customer:
            allowed_trip_numbers.append(trip.trip_number)
        else:
            rejected_trip_numbers.append(trip.trip_number)

    updated_trips = 0
    updated_orders = 0
    updated_payments = 0
    with transaction.atomic():
        print('Trip numbers: %s' % allowed_trip_numbers)
        trip_qs = Trip.objects.filter(trip_number__in=allowed_trip_numbers)
        data = {
            'customer_contract_id': contract.id,
            'modified_by_id': request.user.pk,
            'modified_date': timezone.now()
        }
        change_log = []
        for trip in trip_qs:
            change_log.append(TripHistory(
                field='contract',
                old_value='%s' % trip.customer_contract.name if
                trip.customer_contract else '',
                new_value='%s' % contract.name,
                user=request.user,
                trip=trip)
            )
        TripHistory.objects.bulk_create(change_log)

        updated_trips = trip_qs.update(**data)
        print('Number of updated trips: %s' % updated_trips)

        order_pks = [t.order_id for t in trip_qs]
        order_qs = Order.objects.filter(pk__in=order_pks)
        if contract.contract_type == CONTRACT_TYPE_FIXED:
            data['order_type'] = settings.SUBSCRIPTION_ORDER_TYPE
        elif contract.contract_type == CONTRACT_TYPE_SPOT:
            data['order_type'] = settings.SPOT_ORDER_TYPE
        updated_orders = order_qs.update(**data)
        print('Number of updated orders: %s' % updated_orders)

        payment_pks = PaymentTrip.objects.filter(
            trip__trip_number__in=allowed_trip_numbers) \
            .values_list('payment', flat=True)
        payment_data = {
            'contract_id': contract.id,
            'modified_by_id': request.user.pk,
            'modified_date': timezone.now()
        }
        updated_payments = DriverPayment.objects.filter(pk__in=payment_pks) \
            .update(**payment_data)
        print('Number of updated payment records: %s' % updated_payments)

    if updated_trips:
        messages.success(
            request,
            'Update Details-> Trips: %s, '
            'Orders: %s, '
            'Payments: %s'
            % (updated_trips, updated_orders, updated_payments)
        )

    if rejected_trip_numbers:
        messages.error(request,
                       'Customer for new contract is different from '
                       'currently assigned contract.'
                       '%s' % ', '.join(rejected_trip_numbers))


update_trip_detail.short_description = _('Update Selected')


def mark_trip_as_new(modeladmin, request, queryset):
    trip_qs = queryset.filter(status=StatusPipeline.TRIP_NO_SHOW)

    if trip_qs:
        proceed = TripService().is_valid_to_mark_new(trip_qs)
        if not proceed:
            messages.error(request, 'Invoiced trips cannot be marked New')
            return


    change_log = []
    for trip in trip_qs:
        change_log.append(TripHistory(
            field='status',
            old_value='%s' % trip.status,
            new_value='%s' % StatusPipeline.TRIP_NEW,
            user=request.user,
            trip=trip)
        )
    TripHistory.objects.bulk_create(change_log)
    logger.info('Created trip status update log..')
    updated_trips = trip_qs.update(status=StatusPipeline.TRIP_NEW,
                                   modified_by=request.user,
                                   modified_date=timezone.now())

    if updated_trips:
        messages.success(request, 'Successfully set to `New`: %s' %
                         updated_trips)
    else:
        messages.error(request, 'No `No-Show` trips selected')


mark_trip_as_new.short_description = _('Set to New')


class TripActionForm(ActionForm):
    contract = forms.ModelChoiceField(
        queryset=Contract.objects.filter(status=CONTRACT_STATUS_ACTIVE),
        required=False)


@admin.register(TripProxy)
class TripProxyAdmin(BaseAdmin):
    formfield_overrides = {
        models.MultiLineStringField: {"widget": GooglePolylineFieldWidget},
    }

    #action_form = TripActionForm
    actions = [mark_trip_as_new]
    list_display = (
        'trip_number', 'status', 'driver', 'customer',
        'planned_start_time',
        'actual_start_time', 'actual_end_time', 'city', )

    list_filter = ['status', 'customer_contract__city']

    inlines = [StopInlineAdmin, TripHistoryInline]
    list_select_related = (
        'customer_contract__city', 'city',
    )

    search_fields = ('trip_number', 'driver__name',
                     'vehicle__registration_certificate_number',
                     'customer_contract__name', )
    multi_search_fields = ('trip_number',)

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def get_payment_id(self, obj):
        payment_trip = obj.paymenttrip_set.all()
        for trip in payment_trip:
            if trip.payment.payment_type == DriverPayment.PAYMENT_CHOICES[0][0]:
                url = reverse(
                    'admin:driver_driverpayment_change', args=[trip.payment.id])
                return format_html("<a href='{}'>{}</a>", url, trip.payment.id)
        return ''

    get_payment_id.short_description = 'Driver Payment Id'

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_delete'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(request.user, qs)
        qs = qs.select_related('driver', 'hub', 'batch', 'invoice_driver')
        qs = qs.select_related('customer_contract__city')
        qs = qs.select_related('vehicle__vehicle_model__vehicle_class')
        qs = qs.select_related('order__customer')

        return qs.select_related('customer_contract__customer')

    def hub_name(self, obj):
        return obj.hub and obj.hub.name

    hub_name.short_description = 'Hub'
    hub_name.admin_order_field = 'hub__name'

    def precise_total_time(self, obj):
        if obj.total_time:
            return minutes_to_dhms(obj.total_time)

    def precise_total_distance(self, obj):
        if obj.total_distance:
            return "%.2f" % obj.total_distance

    def precise_gps_distance(self, obj):
        if obj.gps_distance:
            return "%.2f" % obj.gps_distance

    precise_gps_distance.short_description = 'GPS Distance (km)'
    precise_total_distance.short_description = 'Total Distance (km)'
    precise_total_time.short_description = 'Total Time'
    precise_total_time.admin_order_field = 'total_time'

    def city(self, obj):
        if obj.order:
            if obj.order.order_type == settings.C2C_ORDER_TYPE:
                return obj.order.city

            elif obj.order.order_type == settings.SUBSCRIPTION_ORDER_TYPE or \
                    obj.order.order_type == settings.SPOT_ORDER_TYPE:
                return obj.customer_contract.city
        # Shipment order
        else:
            return (obj and obj.customer_contract and obj.customer_contract.city)

    city.admin_order_field = 'customer_contract__city'

    def customer(self, obj):
        if obj.order:
            if obj.order.order_type == settings.C2C_ORDER_TYPE:
                return obj.order.customer.name

            elif obj.order.order_type == settings.SUBSCRIPTION_ORDER_TYPE or \
                    obj.order.order_type == settings.SPOT_ORDER_TYPE:
                return obj.customer_contract.customer.name
        # Shipment order
        else:
            return (obj
                    and obj.customer_contract
                    and obj.customer_contract.customer
                    and obj.customer_contract.customer.name)

        return '--'

    customer.admin_order_field = 'order__customer'

    def contract(self, obj):
        if obj.order:
            return obj.customer_contract.name
        # Shipment order
        else:
            return obj.customer_contract

    contract.short_description = 'Customer Contract'

    def driver_name(self, obj):
        return obj and obj.driver and obj.driver.name

    def trip_order(self, obj):
        if obj.order:
            # order_type = if order.order_type
            if obj.order.order_type == settings.C2C_ORDER_TYPE:
                link = reverse(
                    "admin:order_bookingorder_change", args=[obj.order.id])
                return u'<a href="%s">%s</a>' % (link, obj.order)

            elif obj.order.order_type == settings.SUBSCRIPTION_ORDER_TYPE:
                link = reverse(
                    "admin:order_fixedorder_change", args=[obj.order.id])
                return u'<a href="%s">%s</a>' % (link, obj.order)

            elif obj.order.order_type == settings.SPOT_ORDER_TYPE:
                link = reverse(
                    "admin:order_spotorder_change", args=[obj.order.id])
                return u'<a href="%s">%s</a>' % (link, obj.order)

        return None

    trip_order.allow_tags = True
    trip_order.short_description = 'Order'

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields':
                    ('driver', 'vehicle', 'status', 'customer',
                        'invoice_driver')
            }),

        )

        return fieldsets

    def get_batch(self, obj):
        batch = obj.batch
        if batch:
            url = reverse(
                'admin:order_orderbatch_change', args=[batch.id])
            return format_html('<a target="_blank" href="{}">{}</a>', url, batch)
        return obj.batch
    get_batch.short_description = 'Order Batch'

    def remove_from_fieldsets(self, fieldsets, fields):
        # function to remove fields from fieldset depending on condition
        for fieldset in fieldsets:
            for field in fields:
                if field in fieldset[1]['fields']:
                    new_fields = []
                    for new_field in fieldset[1]['fields']:
                        if new_field not in fields:
                            new_fields.append(new_field)

                    fieldset[1]['fields'] = tuple(new_fields)
                    break

    def get_form(self, request, obj=None, **kwargs):
        self.form = TripEditForm
        return super(TripProxyAdmin, self).get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        return [
            'created_date', 'created_by', 'modified_date', 'trip_order',
            'modified_by', 'precise_total_time', 'total_distance',
            'customer', 'contract', 'is_contingency', 'blowhorn_contract',
            'mt_start_time', 'mt_end_time', 'actual_start_time',
            'actual_end_time', 'trip_acceptence_time', 'get_batch',
            'get_payment_id', 'status', 'hub', 'driver', 'customer_contract',
            'vehicle', 'planned_start_time', 'meter_reading_start',
            'meter_reading_end', 'assigned', 'delivered', 'attempted',
            'rejected', 'pickups', 'returned', 'delivered_cash',
            'delivered_mpos', 'total_trip_wait_time_driver', 'trip_workflow',
            'current_step', 'invoice_driver', 'reason_for_suspension',
            'pickup_loading_timestamp']

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(TripProxyAdmin, self). \
            formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "hub":
            ''' Show only shipment contract hubs'''
            formfield.queryset = Hub.objects.filter(customer_id__isnull=True)
        return formfield

    class Meta:
        fields = '__all__'

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/admin/js/trip/trip_proxy.js'
              )

class MarginBatchAdmin(admin.ModelAdmin):
    list_display = ('start_time', 'end_time', 'records_updated',
                        'batch_month', 'progress')

    def get_readonly_fields(self, request, obj=None):
        return ['start_time', 'end_time', 'records_updated',
                    'batch_month', 'progress']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class UnassignedTripAdmin(admin.ModelAdmin):

    formfield_overrides = {
        models.MultiLineStringField: {"widget": GooglePolylineFieldWidget},
    }
    list_display = (
        'trip_number', 'status', 'hub_name', 'customer',
        'planned_start_time', 'city', )

    list_filter = ['status', 'customer_contract__city']

    inlines = [StopInlineAdmin, TripHistoryInline]
    list_select_related = (
        'customer_contract__city', 'city',
    )

    search_fields = ('trip_number',
                     'customer_contract__name', )
    multi_search_fields = ('trip_number',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = True
        extra_context['show_save_and_continue'] = True
        extra_context['show_delete'] = True
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_queryset(self, request):
        qs = self.model.objects.filter(driver=None)
        qs = apply_contract_permission_filter(request.user, qs)
        qs = qs.select_related('driver', 'hub', 'batch', 'invoice_driver')
        qs = qs.select_related('customer_contract__city')
        qs = qs.select_related('vehicle__vehicle_model__vehicle_class')
        qs = qs.select_related('order__customer')

        return qs.select_related('customer_contract__customer')

    def hub_name(self, obj):
        return obj.hub and obj.hub.name

    hub_name.short_description = 'Hub'
    hub_name.admin_order_field = 'hub__name'

    def city(self, obj):
        if obj.order:
            if obj.order.order_type == settings.C2C_ORDER_TYPE:
                return obj.order.city

            elif obj.order.order_type == settings.SUBSCRIPTION_ORDER_TYPE or \
                    obj.order.order_type == settings.SPOT_ORDER_TYPE:
                return obj.customer_contract.city
        else:
            return (obj and obj.customer_contract and obj.customer_contract.city)

    city.admin_order_field = 'customer_contract__city'

    def customer(self, obj):
        if obj.order:
            if obj.order.order_type == settings.C2C_ORDER_TYPE:
                return obj.order.customer.name

            elif obj.order.order_type == settings.SUBSCRIPTION_ORDER_TYPE or \
                    obj.order.order_type == settings.SPOT_ORDER_TYPE:
                return obj.customer_contract.customer.name
        # Shipment order
        else:
            return (obj
                    and obj.customer_contract
                    and obj.customer_contract.customer
                    and obj.customer_contract.customer.name)

        return '--'

    customer.admin_order_field = 'order__customer'

    def contract(self, obj):
        if obj.order:
            return obj.customer_contract.name
        # Shipment order
        else:
            return obj.customer_contract

    contract.short_description = 'Customer Contract'

    def driver_name(self, obj):
        return obj and obj.driver and obj.driver.name

    def trip_order(self, obj):
        if obj.order:
            # order_type = if order.order_type
            if obj.order.order_type == settings.C2C_ORDER_TYPE:
                link = reverse(
                    "admin:order_bookingorder_change", args=[obj.order.id])
                return u'<a href="%s">%s</a>' % (link, obj.order)

            elif obj.order.order_type == settings.SUBSCRIPTION_ORDER_TYPE:
                link = reverse(
                    "admin:order_fixedorder_change", args=[obj.order.id])
                return u'<a href="%s">%s</a>' % (link, obj.order)

            elif obj.order.order_type == settings.SPOT_ORDER_TYPE:
                link = reverse(
                    "admin:order_spotorder_change", args=[obj.order.id])
                return u'<a href="%s">%s</a>' % (link, obj.order)

        return None

    trip_order.allow_tags = True
    trip_order.short_description = 'Order'

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields':
                    ('driver', 'vehicle', 'status', 'customer',
                        'invoice_driver')
            }),

        )

        return fieldsets

    def get_batch(self, obj):
        batch = obj.batch
        if batch:
            url = reverse(
                'admin:order_orderbatch_change', args=[batch.id])
            return format_html('<a target="_blank" href="{}">{}</a>', url, batch)
        return obj.batch
    get_batch.short_description = 'Order Batch'

    def remove_from_fieldsets(self, fieldsets, fields):
        # function to remove fields from fieldset depending on condition
        for fieldset in fieldsets:
            for field in fields:
                if field in fieldset[1]['fields']:
                    new_fields = []
                    for new_field in fieldset[1]['fields']:
                        if new_field not in fields:
                            new_fields.append(new_field)

                    fieldset[1]['fields'] = tuple(new_fields)
                    break

    def get_form(self, request, obj=None, **kwargs):
        self.form = TripEditForm
        return super(UnassignedTripAdmin, self).get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        return [
            'created_date', 'created_by', 'modified_date', 'trip_order',
            'modified_by', 'precise_total_time', 'total_distance',
            'customer', 'contract', 'is_contingency', 'blowhorn_contract',
            'mt_start_time', 'mt_end_time', 'actual_start_time',
            'actual_end_time', 'trip_acceptence_time', 'get_batch',
            'get_payment_id', 'status', 'hub', 'customer_contract',
            'vehicle', 'planned_start_time', 'meter_reading_start',
            'meter_reading_end', 'assigned', 'delivered', 'attempted',
            'rejected', 'pickups', 'returned', 'delivered_cash',
            'delivered_mpos', 'total_trip_wait_time_driver', 'trip_workflow',
            'current_step', 'invoice_driver', 'reason_for_suspension',
            'pickup_loading_timestamp']

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(UnassignedTripAdmin, self). \
            formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == "hub":
            ''' Show only shipment contract hubs'''
            formfield.queryset = Hub.objects.filter(customer_id__isnull=True)
        return formfield

    def save_model(self, request, obj, form, change):

        trip_list = []
        current_user = request.user

        if change:
            if 'invoice_driver' in form.changed_data:
                InvoiceTrip.objects.filter(trip__id=obj.id).update(
                    invoice_driver=obj.invoice_driver)
            old = Trip.objects.get(pk=obj.id)
            for field in old._meta.fields:

                field_name = field.__dict__.get('name')

                if field_name in TRIP_ALLOWED_FIELDS_FOR_EDIT:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(obj)

                    if old_value != current_value:
                        if isinstance(field, DateTimeField):
                            # Changing the already existing datetime value to
                            # ist as it is saved as utc in the Trip model.
                            # Keeping the current datetime value as it is
                            # because it will be in the ist format itself
                            old_value = datetime.strftime(utc_to_ist(
                                old_value), settings.ADMIN_DATETIME_FORMAT) \
                                if old_value else None
                            current_value = datetime.strftime(
                                current_value,
                                settings.ADMIN_DATETIME_FORMAT) \
                                if current_value else None

                        trip_list.append(TripHistory(field=field_name,
                                                     old_value=str(old_value),
                                                     new_value=current_value,
                                                     user=current_user,
                                                     trip=old))

            TripHistory.objects.bulk_create(trip_list)
        if form.cleaned_data.get('driver') and not \
                form.cleaned_data.get('vehicle'):
            obj.vehicle = DriverVehicleMap.objects.get(
                driver=form.cleaned_data.get('driver')).vehicle
            driver = form.cleaned_data.get('driver')
            driver_qs = Driver.objects.filter(pk=driver.id
                                                ).select_related(
                'contract').first()
            if driver_qs.contract:
                obj.blowhorn_contract = driver_qs.contract

            stops = Stop.objects.filter(trip=old)
            with transaction.atomic():
                for stop in stops:
                    stop.order.driver = driver_qs
                    stop.order.invoice_driver = driver_qs
                    stop.order.save()
                    stop.save()

        super().save_model(request, obj, form, change)

    class Meta:
        fields = '__all__'

class PartnerFeedbackAdmin(admin.ModelAdmin):
    list_display = ('driver', 'trip', 'event_time', 'feedback')

    def get_readonly_fields(self, request, obj=None):
        return ['driver', 'trip', 'event_time', 'feedback']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class StopHistoryAdmin(admin.ModelAdmin):

    list_display = ('get_trip', 'get_order', 'old_sequence', 'new_sequence', 'created_time')

    def get_trip(self, obj):
        if obj.stop:
            return obj.stop.trip

    get_trip.short_description = 'Trip'

    def get_order(self, obj):
        if obj.stop:
            return obj.stop.order

    get_order.short_description = 'Order'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('stop', 'stop__order', 'routes_batch')
        return qs

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class FeedbackOptionsAdmin(admin.ModelAdmin):
    list_display = ('option',)

admin.site.register(FeedbackOptions, FeedbackOptionsAdmin)
admin.site.register(PartnerFeedback, PartnerFeedbackAdmin)
admin.site.register(UnassignedTrip, UnassignedTripAdmin)
admin.site.register(ShipmentTripSummary, ShipmentTripSummaryAdmin)
admin.site.register(Trip, TripAdmin)
admin.site.register(AppEvents, AppEventsAdmin)
admin.site.register(TripConstants, TripConstantsAdmin)
admin.site.register(MarginBatch, MarginBatchAdmin)
admin.site.register(StopHistory, StopHistoryAdmin)


from blowhorn.trip.subadmins.old_tripadmin import *
