# System & Django
import pytz
from datetime import datetime
from django.db import transaction
from django.conf import settings

# 3rd Party Libraries
from blowhorn.oscar.core.loading import get_model
from import_export import resources, fields
from rest_framework import status

# Blowhorn Modules
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.constants import APPROVED
from blowhorn.utils.functions import utc_to_ist, ist_to_utc, minutes_to_dhms, format_datetime
from .mixins import TripMixin

PaymentTrip = get_model('driver', 'PaymentTrip')
DriverPayment = get_model('driver', 'DriverPayment')
Order = get_model('order', 'Order')
Trip = get_model('trip', 'Trip')
AppEvents = get_model('trip', 'AppEvents')


class TripResource(resources.ModelResource):

    driver = fields.Field(
        column_name="DRIVER")

    vehicle = fields.Field(column_name="VEHICLE")

    customer_contract = fields.Field(column_name="CUSTOMER CONTRACT")

    order = fields.Field(column_name="ORDER")

    planned_start_date = fields.Field(
        column_name="SCHEDULED DATE")

    planned_start_time = fields.Field(
        column_name="SCHEDULED TIME")

    start_date = fields.Field(
        column_name="ACTUAL START DATE")

    start_time = fields.Field(
        column_name="ACTUAL START TIME")

    end_date = fields.Field(
        column_name="ACTUAL END DATE")

    end_time = fields.Field(
        column_name="ACTUAL END TIME")

    vehicle_class = fields.Field(column_name="Vehicle Class")

    duty_type = fields.Field(column_name="Duty Type")

    gps_distance = fields.Field(column_name="GPS Distance")

    customer_category = fields.Field(column_name="Customer Category")

    is_contingency = fields.Field(column_name="Is Contingency")

    hub_name = fields.Field(column_name="Hub")

    total_time = fields.Field(column_name="Total Duration")

    def dehydrate_driver(self, trip):
        if trip and trip.driver and trip.driver.user:
            return trip.driver.user.name
        return ''

    def dehydrate_vehicle(self, trip):
        if trip and trip.vehicle:
            return trip.vehicle.registration_certificate_number
        return ''

    def dehydrate_customer_contract(self, trip):
        if trip and trip.customer_contract:
            return '%s' % trip.customer_contract
        return ''

    def dehydrate_order(self, trip):
        if trip and trip.order:
            return '%s' % trip.order
        return ''

    def dehydrate_planned_start_time(self, trip):
        return format_datetime(trip.planned_start_time, settings.ADMIN_TIME_FORMAT)

    def dehydrate_planned_start_date(self, trip):
        return format_datetime(trip.planned_start_time, settings.ADMIN_DATE_FORMAT)

    def dehydrate_start_time(self, trip):
        return format_datetime(trip.actual_start_time, settings.ADMIN_TIME_FORMAT)

    def dehydrate_start_date(self, trip):
        return format_datetime(trip.actual_start_time, settings.ADMIN_DATE_FORMAT)

    def dehydrate_end_time(self, trip):
        return format_datetime(trip.actual_end_time, settings.ADMIN_TIME_FORMAT)

    def dehydrate_end_date(self, trip):
        return format_datetime(trip.actual_end_time, settings.ADMIN_DATE_FORMAT)

    def dehydrate_vehicle_class(self, trip):
        if trip and trip.vehicle:
            return trip.vehicle.vehicle_model.vehicle_class.commercial_classification
        return ''

    def dehydrate_duty_type(self, trip):
        if trip and trip.customer_contract:
            return trip.customer_contract.contract_type
        return ''

    def dehydrate_gps_distance(self, trip):
        if trip.gps_distance:
            return "%.2f" % trip.gps_distance
        return ''

    def dehydrate_customer_category(self, trip):
        if trip and trip.customer_contract and trip.customer_contract.customer:
            return trip.customer_contract.customer.customer_category
        return ''

    def dehydrate_is_contingency(self, trip):
        return 'Yes' if trip.is_contingency else 'No'

    def dehydrate_hub_name(self, trip):
        return str(trip.hub) if trip.hub else ''

    def dehydrate_total_time(self, trip):
        return minutes_to_dhms(trip.total_time)

    class Meta:
        model = Trip
        skip_unchanged = True
        report_skipped = False
        fields = ('status', 'trip_number', 'id', 'meter_reading_start',
                  'meter_reading_end', 'total_distance',
                  'total_time', 'assigned', 'delivered', 'attempted',
                  'rejected', 'pickups', 'returned', 'delivered_cash',
                  'delivered_mpos', 'hub', 'is_contingency', 'created_by',
                  'modified_by')
        export_order = ('status', 'trip_number', 'hub_name', 'vehicle_class',
                        'customer_category', 'duty_type', 'is_contingency',
                        'gps_distance', 'created_by', 'modified_by')


class AppEventResource(resources.ModelResource):

    driver = fields.Field(
        column_name="Driver",
    )

    trip = fields.Field(column_name="Trip")

    order = fields.Field(column_name="Order")

    hub_associate = fields.Field(column_name="Hub Associate")

    created_time = fields.Field(column_name="Created Date Time")

    def dehydrate_driver(self, appevent):
        return appevent.driver.name

    def dehydrate_trip(self, appevent):
        if appevent.trip:
            return appevent.trip.trip_number
        return ''

    def dehydrate_order(self, appevent):
        if appevent.order:
            return appevent.order.number
        return ''

    def dehydrate_hub_associate(self, appevent):
        if appevent.hub_associate:
            return appevent.hub_associate.user.name
        return ''

    def dehydrate_created_time(self, appevent):
        return utc_to_ist(appevent.created_time)

    class Meta:
        model = AppEvents
        exclude = ('geopoint', 'stop')
