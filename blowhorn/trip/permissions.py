from rest_framework.permissions import BasePermission


class IsAllowedToWrite(BasePermission):

    def has_permission(self, request, view):
        # TODO : This logic is expected to be updated.
        return True


class IsAllowedToRead(BasePermission):

    def has_object_permission(self, request, view, obj):
        return hasattr(obj, "customer_contract") and \
               hasattr(obj.customer_contract, "customer") and \
               hasattr(obj.customer_contract.customer, "user") and \
               obj.customer_contract.customer.user == request.user
