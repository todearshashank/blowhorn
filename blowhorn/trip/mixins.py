# System and Django libraries
import pytz
import json
import logging
import operator
from datetime import datetime
from django.utils import timezone
from django.conf import settings
from django.contrib.gis.geos import LineString, MultiLineString
from datetime import timedelta

# 3rd Party libraries
from rest_framework.response import Response
from rest_framework import status as status_code

# Blowhorn libraries
from .models import Trip, Stop, AppEvents
from blowhorn.driver.models import (
    DriverVehicleMap,
    DriverActivityHistory
)
from config.settings import status_pipelines as StatusPipeline
from blowhorn.address.utils import AddressUtil
from blowhorn.common.helper import CommonHelper
from blowhorn.driver.serializers import DriverActivitySerializer
from blowhorn.order.const import STATUS_UPDATE, ORDER_UPDATES_FIREBASE_PATH
from blowhorn.order.models import OrderConstants
from blowhorn.order.tasks import broadcast_booking_updates_customer
from blowhorn.users.models import User
from blowhorn.utils.functions import geopoints_to_km
from blowhorn.driver.redis_models import Driver as RedisDriver
from blowhorn.contract.models import CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME
from blowhorn.trip.constants import STOP_TYPES
from blowhorn.trip.constants import PICKUP, HUB_DELIVERY, ORDER_DELIVERY


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DISTANCE_CONVERTER_IN_KM = 100


class TripMixin(object):

    def get_summary(self, trips):
        total_cost = 0
        total_distance = 0
        number_trips = 0
        datewise_data = {}
        for trip in trips:
            if trip.order:
                total_cost += trip.order.total_payable
            date = trip.planned_start_time.strftime('%d-%b-%Y')
            date_sortable = trip.planned_start_time.strftime('%Y-%m-%d')

            distance = trip.total_distance or trip.gps_distance or 0
            distance = round(distance, 1)
            total_distance += distance

            number_trips += 1

            if date not in datewise_data:
                datewise_data[date] = {
                    'date': date,
                    'date_sortable': date_sortable,
                    'trips': 0,
                    'distance': 0,
                    'cost': 0,
                }

            d = datewise_data[date]
            d['trips'] = d['trips'] + 1
            d['distance'] = d['distance'] + distance
            d['cost'] = d['cost'] + (trip.order.total_payable if trip.order else 0)

        summary = {
            "total_cost": total_cost,
            "total_distance": total_distance,
            "number_trips": number_trips,
            "data":
                sorted(
                    datewise_data.values(),
                    key=lambda x: x.get('date_sortable'))
        }

        return summary

    def create_trip(self,
                    planned_start_time=None,
                    driver=None,
                    vehicle=None,
                    estimated_distance_kms=0,
                    contract=None,
                    order=None,
                    isBatchUser=False,
                    status=None,
                    hub=None,
                    customer_contract=None,
                    blowhorn_contract=None,
                    is_contingency=False,
                    batch=None,
                    invoice_driver=None,
                    trip_workflow=None,
                    trip_base_pay=0.00,
                    meter_reading_start=None,
                    meter_reading_end=None,
                    actual_start_time=None,
                    actual_end_time=None,
                    trip_acceptence_time=None):
        if driver:
            # if driver.status == StatusPipeline.INACTIVE:
            #     return Response(
            #         status=status_code.HTTP_400_BAD_REQUEST,
            #         data='driver status is InActive.'
            #     )
            driver_vehicle_map = DriverVehicleMap._default_manager.filter(
                driver=driver).first()
            if not driver_vehicle_map:
                return Response(
                    status=status_code.HTTP_400_BAD_REQUEST,
                    data='no vehicle associated to this driver.'
                )
            vehicle = driver_vehicle_map.vehicle

        trip_status = status if status else StatusPipeline.TRIP_NEW

        if hub:
            trip_hub = hub
        else:
            trip_hub = order.hub if order else None

        if customer_contract or contract:
            trip_workflow = customer_contract.trip_workflow if customer_contract else contract.trip_workflow

        trip_data = {
            'vehicle': vehicle,
            'driver': driver,
            'vendor': driver.owner_details if driver else None,
            'status': trip_status,
            'customer_contract': contract,
            'blowhorn_contract': blowhorn_contract,
            'order': order,
            'invoice_driver': invoice_driver or driver,
            'planned_start_time': planned_start_time if planned_start_time else timezone.now(),
            'hub': trip_hub if trip_hub else None,
            'is_contingency': is_contingency if is_contingency else False,
            'batch': batch,
            'trip_workflow': trip_workflow,
            'trip_base_pay': trip_base_pay,
            'meter_reading_start': meter_reading_start,
            'meter_reading_end': meter_reading_end,
            'actual_start_time': actual_start_time,
            'actual_end_time': actual_end_time,
            'trip_acceptence_time': trip_acceptence_time
        }
        if isBatchUser:
            trip_data['created_by'] = User.objects.filter(
                email=settings.CRON_JOB_DEFAULT_USER).first()
            trip_data['created_date'] = timezone.now()

        trip = Trip(**trip_data)
        trip.save()

        if driver:
            from blowhorn.driver.tasks import push_notification_to_driver_app

            push_notification_to_driver_app.apply_async((driver.id, 'new_trip'),
                                                        eta=timezone.now() + timedelta(minutes=0.2))
            logger.info(
                'Trip:- "%s" created successfully for driver:- "%s-%s-%s"',
                trip,
                driver.user.name,
                driver.user.national_number,
                planned_start_time
            )
        else:
            logger.info('Trip:- "%s" created successfully at:- "%s"',
                        trip, planned_start_time)

        return trip

    def update_driver_for_trip(self, driver, order, blowhorn_contract=None,
                               trip_base_pay=None):
        driver_vehicle_map = DriverVehicleMap._default_manager.filter(
            driver=driver).first()
        if not driver_vehicle_map:
            return Response(
                status=status_code.HTTP_400_BAD_REQUEST,
                data='no vehicle associated to this driver.'
            )

        vehicle = driver_vehicle_map.vehicle
        trip_status = StatusPipeline.TRIP_NEW
        invoice_driver = driver

        Trip.objects.filter(order=order).update(
            driver=driver, vehicle=vehicle, invoice_driver=invoice_driver,
            status=trip_status, blowhorn_contract=blowhorn_contract,
            planned_start_time=order.pickup_datetime, hub=order.hub, trip_base_pay=trip_base_pay)

    def update_route_trace_for_trip(self, trip):
        # only completed stops route trace will be considered for whole trip
        # route
        total_distance = 0
        try:
            trip_stop = Stop.objects.filter(
                trip=trip, route_trace__isnull=False)
            if trip_stop.exists():
                stop_route = trip_stop.order_by('end_time').values_list(
                    'route_trace', flat=True)
                route_trace = MultiLineString(list(stop_route))
                trip_queryset = Trip.objects.filter(pk=trip.id)
                trip_queryset.update(route_trace=route_trace)
                total_distance = sum(trip_stop.values_list('distance', flat=True))
            else:
                total_distance = self.update_route_trace_for_stop(trip=trip)

            logger.info('Route Trace Updation for trip:- "%s" successful' % trip)
        except BaseException:
            logger.info(
                'Route Trace Updation for trip:- "%s" failed', trip)

        return total_distance

    def create_trip_stop(self, trip, waypoint_id, order, sequence=None):
        trip_stop_data = {
            'trip': trip,
            'waypoint_id': waypoint_id,
            'status': StatusPipeline.TRIP_NEW,
            'sequence': sequence
        }

        trip_stop = Stop(**trip_stop_data)
        trip_stop.save()

        logger.info('Stop %d created successfully for order:- "%s"',
                    trip_stop.id, order)
        return trip_stop

    def update_route_trace_for_stop(self, trip_stop=None, geopoint=None, trip=None):

        try:
            route_trace, activity_geopoints = [], []
            distance = 0

            start_date = timezone.now().date() - timedelta(days=1)
            end_date = timezone.now().date()

            activity_history = DriverActivityHistory.objects.filter(
                stop=trip_stop,
                trip=trip_stop.trip if trip_stop else trip,
                driver=trip_stop.trip.driver if trip_stop else trip.driver
            ).only('gps_timestamp', 'location_accuracy_meters', 'geopoint',
                   'vehicle_speed_kmph')

            route_trace = []
            if len(activity_history) > 0:
                processed_geopoints = AddressUtil().get_processed_geopoints(activity_history)
                activity_geopoints = [x.geopoint for x in processed_geopoints]
            activity_gpt_len = len(activity_geopoints)

            if activity_geopoints and activity_gpt_len > 1:
                distance = geopoints_to_km([x for x in activity_geopoints])
                route_trace = LineString(activity_geopoints)

            if not route_trace:
                return distance
            if trip_stop:
                Stop.objects.filter(pk=trip_stop.id).update(
                    route_trace=route_trace,
                    distance=distance
                )
                logger.info(
                    'Route Trace Updated for stop:- %d', trip_stop.id)
            elif trip and route_trace:
                trip.route_trace = MultiLineString([route_trace, ])
                trip.gps_distance = distance
                trip.save()
            return distance
        except BaseException:
            logger.error(
                'Route Trace Updated failed for stop:- %d', trip_stop.id)
            return 0

    def get_order_response(self, trip):
        trip_details = {}
        trip_stop = []

        stop_queryset = Stop.objects.filter(trip=trip).order_by(
            'waypoint__order__expected_delivery_time')

        for stop in stop_queryset:
            order = stop.waypoint.order if stop.waypoint else stop.order
            trip_stop_list = self.get_trip_stop_details(
                order=order, trip_stop=stop)
            trip_stop.append(trip_stop_list)

        trip_details['trip_stops'] = trip_stop
        trip_details['geofence_radius_meters'] = OrderConstants().get(
            'GEOFENCE_RADIUS_METERS', 100)

        start_meter_reading = trip.meter_reading_start if trip.meter_reading_start else -1
        end_meter_reading = trip.meter_reading_end if trip.meter_reading_end else -1

        trip_details['start_meter_reading'] = start_meter_reading
        trip_details['end_meter_reading'] = end_meter_reading

        return trip_details

    def get_trip_stop_details(self, order, trip_stop):
        # The response to the app is sent in ordered way...Numbering of orders
        # in App is done according to the order sent by this method

        trip_stop_details = {}
        shipping_address = {}
        shipping_address_obj = order.shipping_address

        # change local timezone here
        local_tz = pytz.timezone(settings.IST_TIME_ZONE)

        trip_stop_details = CommonHelper.get_json_data_from_model_obj(
            trip_stop)
        trip_stop_details[
            'order_details'] = CommonHelper.get_json_data_from_model_obj(order)
        if order.expected_delivery_time:
            trip_stop_details['order_details']['expected_delivery_time'] = \
                order.expected_delivery_time.astimezone(
                    local_tz).strftime('%d-%b-%Y %H:%M:%S')

        if trip_stop_details['order_details']['status'] in [StatusPipeline.ORDER_DELIVERED, StatusPipeline.DELIVERY_ACK]:
            trip_stop_details['order_details']['deliveryStatus'] = StatusPipeline.MASTER_STATII[
                StatusPipeline.ORDER_DELIVERED]['value']
        elif trip_stop_details['order_details']['status'] == StatusPipeline.ORDER_RETURNED:
            trip_stop_details['order_details']['deliveryStatus'] = StatusPipeline.MASTER_STATII[
                StatusPipeline.ORDER_RETURNED]['value']
        elif trip_stop_details['order_details']['status'] == StatusPipeline.UNABLE_TO_DELIVER:
            trip_stop_details['order_details']['deliveryStatus'] = StatusPipeline.MASTER_STATII[
                StatusPipeline.UNABLE_TO_DELIVER]['value']
        else:
            trip_stop_details['order_details'][
                'deliveryStatus'] = StatusPipeline.MASTER_STATII[StatusPipeline.ORDER_NEW]['value']

        shipping_address = CommonHelper.get_json_data_from_model_obj(
            shipping_address_obj)

        if shipping_address_obj.geopoint:
            shipping_address['latitude'] = shipping_address_obj.geopoint.y
            shipping_address[
                'longitude'] = shipping_address_obj.geopoint.x
        else:
            shipping_address['latitude'] = None
            shipping_address['longitude'] = None
        trip_stop_details['shipping_address'] = shipping_address
        trip_stop_details['id'] = trip_stop.id

        return trip_stop_details

    def _create_app_event(self, data):
        appEvents = AppEvents(**data)
        appEvents.save()

        return appEvents

    def create_app_event(self, event, geopoint, driver=None, stop=None,
                         hub_associate=None, trip=None, order=None):
        # Store time and status details in events model
        AppEvents.objects.create(
            event=event,
            order=order,
            trip=trip,
            driver=driver,
            geopoint=geopoint,
            hub_associate=hub_associate,
            stop=stop
        )

    def update_trip_status(self, trip, target_status):

        Trip.objects.filter(id=trip.id).update(
            status=target_status
        )

        logger.info('Trip "%s" State Transition from "%s" to "%s" SUCCESSFUL',
                    trip, trip.status, target_status)

    def update_stop_status(self, stop, target_status, order=None):

        if target_status == StatusPipeline.TRIP_NEW:
            start_time = None

            Stop.objects.filter(id=stop.id).update(
                status=target_status,
                start_time=start_time)
        elif target_status == StatusPipeline.TRIP_IN_PROGRESS:
            start_time = timezone.now()

            Stop.objects.filter(id=stop.id).update(
                status=target_status,
                start_time=start_time)
        elif target_status in [
            StatusPipeline.TRIP_COMPLETED,
            StatusPipeline.TRIP_CANCELLED
        ]:
            end_time = timezone.now()

            Stop.objects.filter(id=stop.id).update(
                status=target_status,
                end_time=end_time)

            if order and order.order_type == settings.C2C_ORDER_TYPE and \
                order.customer_contract.contract_type in [CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME]:
                channel_name = ORDER_UPDATES_FIREBASE_PATH % order.number
                broadcast_booking_updates_customer.apply_async(
                    args=[order.pk, channel_name, STATUS_UPDATE])

        logger.info('Stop %d State Transition from "%s" to "%s" SUCCESSFUL',
                    stop.id, stop.status, target_status)

    def get_sorted_trips(self, trips, status, reversed=False):
        filtered = [x for x in trips if x.get('status') == status]
        return sorted(
            filtered, key=lambda x: x.get('date_sortable'), reverse=reversed)

    def create_multiple_stops_trip(self, trip, stop_details=None):
        stop_list = []
        if stop_details is not None:    
            for stop_detail in stop_details:
                if stop_detail["type"]=="PICK":
                    stop_list.append(
                        Stop(**{
                            "trip": trip,
                            "status": StatusPipeline.TRIP_NEW,
                            "order": stop_detail["order"],
                            "stop_type": PICKUP,
                            "stop_address": stop_detail["order"].pickup_address,
                            "sequence": stop_detail["sequence"]
                        })
                    )
                elif stop_detail["type"]=="DROP":
                    stop_list.append(
                        Stop(**{
                            "trip": trip,
                            "status": StatusPipeline.TRIP_NEW,
                            "order": stop_detail["order"],
                            "stop_type": ORDER_DELIVERY,
                            "stop_address": stop_detail["order"].shipping_address,
                            "sequence": stop_detail["sequence"]
                        })
                    ) 
            Stop.objects.bulk_create(stop_list)
        return stop_list
