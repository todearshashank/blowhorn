from django.utils.translation import ugettext_lazy as _

from rest_framework import response, views
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from blowhorn.activity.mixins import ActivityMixin
from blowhorn.activity.models import DriverTripLog
from blowhorn.address.utils import AddressUtil
from blowhorn.trip.models import Trip
from blowhorn.users.permission import IsBlowhornStaff


class MongoRouteTraceView(views.APIView, ActivityMixin):
    """
        GET /api/trip/route-trace
        Returns raw gps data for a trip or order by fetching from Mongo DB
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def get(self, request):
        data = request.data or request.GET
        order_id = data.get('order_id', None)
        trip_id = data.get('trip_id', None)

        if not order_id and not trip_id:
            return DriverTripLog.custom_manager.none()

        query_params = {}
        if order_id:
            query_params['orderId'] = order_id

        if trip_id:
            query_params['tripId'] = trip_id

        qs = DriverTripLog.custom_manager.fetch(
            query_params,
            fetch_only=['locationInfo']
        ).values('locationInfo')
        return response.Response(qs)


class TripRouteTraceView(views.APIView, ActivityMixin):
    """
        GET /api/trip/route-trace
        Returns list of list of lat longs for a trip
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def get(self, request):
        data = request.data or request.GET
        trip_id = data.get('trip_id', None)

        if not trip_id:
            return response.Response([])

        trip = Trip.objects.get(id=trip_id)
        route_trace = AddressUtil().convert_multilinestring_to_latlong(trip.route_trace)
        return response.Response(route_trace)
