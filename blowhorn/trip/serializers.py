import logging
from rest_framework import serializers, status
from rest_framework.response import Response

from blowhorn.oscarapi.serializers.utils import OscarModelSerializer

from blowhorn.common.serializers import BaseSerializer
from blowhorn.address.serializer import ShippingAddressSerializer
from .models import Trip, Stop, AppEvents, TripDocument
from .mixins import TripMixin
from .utils import TripService
from blowhorn.order.utils import OrderUtil
from blowhorn.order.models import WayPoint, Order
from blowhorn.order.mixins import OrderPlacementMixin
from config.settings import status_pipelines as StatusPipeline

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TripSerializer(OscarModelSerializer):
    queryset = Trip.objects.all()

    def get_mytrips(self, request, trip_qs, order_qs):
        """
        Order select_related Driver
        Order prefetch_related Trip
        filter_by Customer
        order_by pickup_datetime DESC
        """
        trip_results_json = []

        for trip in trip_qs:
            response_dict = TripService().get_trip_details_for_customer(
                    request=request,
                    trip=trip
                )
            if response_dict:
                trip_results_json.append(
                    response_dict
                )

        current_trips = TripMixin().get_sorted_trips(
            trip_results_json, 'ongoing'
        )
        past_trips = TripMixin().get_sorted_trips(
            trip_results_json, 'done', True
        )
        upcoming_trips = TripMixin().get_sorted_trips(
            trip_results_json, 'upcoming'
        )

        order_results_json = []

        for order in order_qs:
            response_dict = OrderUtil().get_booking_details_for_customer(
                request=request,
                order=order
            )
            if response_dict:
                order_results_json.append(
                    response_dict
                )
        current_orders = OrderPlacementMixin().get_sorted_orders(
            order_results_json, 'ongoing'
        )

        unpaid_past_orders = []
        paid_past_orders = []
        for x in order_results_json:
            if x.get('status') == 'done' and x.get('payment_status') == 'un-paid':
                unpaid_past_orders.append(x)
            if x.get('status') == 'done' and x.get('payment_status') == 'paid':
                paid_past_orders.append(x)
        sorted_unpaid_past_orders = OrderPlacementMixin().get_sorted_orders(unpaid_past_orders, 'done', True)
        sorted_paid_past_orders = OrderPlacementMixin().get_sorted_orders(paid_past_orders, 'done', True)

        past_orders = sorted_unpaid_past_orders + sorted_paid_past_orders

        upcoming_orders = OrderPlacementMixin().get_sorted_orders(
            order_results_json, 'upcoming'
        )

        current = current_trips + current_orders
        past = past_trips + past_orders
        upcoming = upcoming_trips + upcoming_orders

        _dict = {
            'current': current,
            'past': past,
            'upcoming': upcoming,
        }
        return _dict

    class Meta:
        model = Trip
        fields = '__all__'


class TripStopSerializer(BaseSerializer):
    queryset = Stop.objects.all()
    address = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Stop
        fields = '__all__'

    def get_address(self, stop):
        address = stop.waypoint.shipping_address if stop.waypoint else stop.order.shipping_address
        return ShippingAddressSerializer(
            address,
            fields=[
                "id", "line1", "line2", "line3", "line4", "postcode", "geopoint", "status", "start_time", "end_time",
                "distance"]).data

# class MeterReadingSerializer(OscarModelSerializer):
#     class Meta:
#         model = MeterReading
#         fields = '__all__'


# @todo remove this
class AppEventsSerializer(serializers.ModelSerializer, TripMixin):
    queryset = AppEvents.objects.all()

    def _create_event(self):
        response = self.get_validated_data()

        if response:
            data = self._create_app_event(data=response)
            if data:
                return data

        logger.error('App Event creation failure')
        return False

    def get_validated_data(self):
        data = self.initial_data
        # Validate whether all required inputs are there or not?
        mandatory_fields = ['geopoint', 'event']

        for field in mandatory_fields:
            if field not in data:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=field + ' is mandatory.')

        return data

    class Meta:
        model = AppEvents
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    """
    A serializer for ``Order``.
    """
    class Meta(object):

        model = Order
        fields = '__all__'


class TripsSerializer(serializers.ModelSerializer):
    """
    A serializer for ``Trip``.
    """

    class Meta(object):
        model = Trip
        fields = ('id', 'trip_number',)


class StopsSerializer(serializers.ModelSerializer):
    """
    A serializer for ``Trip``.
    """
    class Meta(object):
        model = Stop
        fields = ('id', 'status',)


class WaypointsSerializer(serializers.ModelSerializer):
    """
    A serializer for ``Trip``.
    """
    completed = serializers.SerializerMethodField(read_only=True)
    shipping_address = ShippingAddressSerializer()

    class Meta(object):
        model = WayPoint
        fields = ('id', 'shipping_address', 'completed', 'sequence_id')

    def get_completed(self, waypoint):
        stop = waypoint.stops.first()
        if stop and stop.status == StatusPipeline.TRIP_COMPLETED:
            return True
        return False


class TripStopEditSerializer(serializers.ModelSerializer):
    """
    A serializer for ``Stop``.
    """

    waypoints = serializers.SerializerMethodField(read_only=True)
    trip = serializers.SerializerMethodField(read_only=True)
    pickup = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = Order
        fields = ("waypoints", "trip", "id", "pickup",)

    def get_trip(self, order):
        trip = order.trip_set.filter(status__in=[StatusPipeline.TRIP_NEW, StatusPipeline.TRIP_IN_PROGRESS]).first()
        if trip:
            return TripsSerializer(trip).data
        return {}

    def get_waypoints(self, order):
        return WaypointsSerializer(order.waypoint_set.all(), many=True).data

    def get_pickup(self, order):

        pickup_address = ShippingAddressSerializer(order.pickup_address).data
        pickup = {
            "shipping_address": pickup_address,
            "completed": self.get_completed(order),
            "sequence_id": self.get_sequence_id(order)
        }
        return pickup

    def get_completed(self, order):
        return order.status not in [StatusPipeline.ORDER_NEW, StatusPipeline.DRIVER_ACCEPTED]

    def get_sequence_id(self, order):
        return 0


class TripDocumentSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(TripDocumentSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = TripDocument
