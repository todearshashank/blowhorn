from django.conf import settings

MAX_ALLOWED_TOTAL_TRIP_TIME_MINUTES = 2160  # 36 hours
MAX_ALLOWED_ACTUAL_START_TIME_AND_PLANNED_TIME_DELTA_MINUTES = 1440
AVG_SPEED_KMPH = {
    'blowhorn': 60,
    'mzansigo': 120
}
AVG_SPEED_OF_VEHICLE_KMS = AVG_SPEED_KMPH.get(settings.WEBSITE_BUILD, 60)

"""Trip related constants to be added here."""
from django.utils.translation import ugettext_lazy as _
from blowhorn.contract.constants import (
    TRIP_DATA_PROOF, START_ODOMETER_PROOF, END_ODOMETER_PROOF,
    TOLL_CHARGE, PARKING_CHARGE
)

# calculate stop/trip distance from driver activity history
DAH = 'DAH'
# calculate stop/trip distance from mongo
Mongo = 'Mongo'

trip_gpsdistance_from = Mongo

calculate_distance_using = [Mongo]

# Allowed columns for update
TRIP_ALLOWED_FIELDS_FOR_EDIT = [
    'delivered_mpos', 'delivered_cash', 'returned',
    'pickups', 'rejected', 'attempted', 'delivered', 'assigned', 'meter_reading_end',
    'meter_reading_start', 'planned_start_time', 'trip_acceptence_time', 'status',
    'actual_start_time', 'actual_end_time', 'total_trip_wait_time_driver', 'driver',
    'toll_charges', 'parking_charges'
]

TRIP_DOCUMENT_TYPES = (
    (TOLL_CHARGE, _('Capture Toll Charges Proof')),
    (PARKING_CHARGE, _('Capture Parking Charges Proof')),
    (START_ODOMETER_PROOF, _('Start Odometer Proof')),
    (END_ODOMETER_PROOF, _('End Odometer Proof')),
    (TRIP_DATA_PROOF, _('Driver Data Log')),
)

MAX_ALLOWED_TRIPS_HOURS_BEFORE_SCHEDULED_START = 2

PICKUP = 'Pickup'
HUB_DELIVERY = 'Hub'
ORDER_DELIVERY = 'Delivery'
ORDER_RTO = 'Return'
# hub movement for returning order to origin(RTO: Return To Origin)
HUB_RTO = 'Hub Return'

STOP_TYPES = (
    (PICKUP, _('Pickup')),
    (HUB_DELIVERY, _('Hub')),
    (ORDER_DELIVERY, _('Delivery')),
    (HUB_RTO, _('Hub Return')),
    (ORDER_RTO, _('Return'))
)


METER_READING_CAP = 1000000
