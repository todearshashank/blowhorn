from django.conf import settings

from blowhorn.contract.permissions import is_spoc_or_supervisor
from blowhorn.contract.constants import CONTRACT_TYPE_KIOSK

'''Permission for contingency add trip'''


def is_trip_spoc_or_supervisor(instance, user):
    return is_spoc_or_supervisor(instance.customer_contract, user)


"""
@birju bhai @surendhar
@todo need to add condition for c2c
(whether suspend, no-show, scheduled-off needed or not)
"""


def is_b2b_trip(instance):
    return True if instance.order and not instance.is_contingency else False


def can_complete_trip(trip):

    if trip.order:
        # For C2C trips Once we completer order trip will be marked as
        # completed
        if trip.total_time and trip.order.order_type != settings.C2C_ORDER_TYPE:
            return True

        elif trip.order.order_type != settings.C2C_ORDER_TYPE:
            can_complete_trip.hint = 'Please Fill required fields to ' \
                                     'complete a trip'
            return False

    elif trip.customer_contract and \
        trip.customer_contract.contract_type == CONTRACT_TYPE_KIOSK:
        return True
    else:
        # shipment trips
        if trip.total_time:
            return True
        can_complete_trip.hint = 'Please Fill required fields to ' \
                                 'complete a trip'
        return False

    return False
