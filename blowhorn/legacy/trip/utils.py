import json
import logging
import traceback
from datetime import datetime
from django.utils import timezone
from django.db import transaction
from django.conf import settings
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract import constants
from blowhorn.trip.models import Trip
from blowhorn.driver.models import DriverVehicleMap
from blowhorn.utils.functions import utc_to_ist, timedelta_to_str
from blowhorn.order.models import Order, Event
from blowhorn.trip.mixins import TripMixin


class BusinessFlowUtil(object):

    def get_other_value(self, item_name, trip, default_value=None):
        if trip:
            # for trip in trip_packages:
            if item_name == constants.SKEY_PACKAGES_RECEIVED:
                return trip.assigned

            elif item_name == constants.SKEY_CASH_PACKAGES:
                return trip.delivered_cash

            elif item_name == constants.SKEY_MPOS_PACKAGES:
                return trip.delivered_mpos

            elif item_name == constants.SKEY_ATTEMPTED:
                return trip.attempted

            elif item_name == constants.SKEY_REJECTED:
                return trip.rejected

            elif item_name == constants.SKEY_RETURNED:
                return trip.returned

            else:
                return default_value

    def get_value(self, trip, item_name):
        default_value = None
        if item_name in ['meter_reading_start', 'meter_reading_end']:
            if (item_name == 'meter_reading_start'):
                return trip.meter_reading_start
            elif (item_name == 'meter_reading_end'):
                return trip.meter_reading_end
            else:
                return default_value
        else:
            return self.get_other_value(item_name, trip, default_value)

    def update_order_status(self, trip, status, driver, geopoint=None):
        order = trip.order
        if not order:
            return False
        try:
            order.status = status
            order.save()

            Event.objects.create(
                status=status,
                order=order,
                address=order.shipping_address,
                driver=driver,
                current_location=geopoint
            )

        except:
            return False
        return True

    def _set_values(self, trip):
        values = {}
        items = constants.KEYS_LIST
        for i in items:
            try:
                val = self.get_value(trip, i)
                if val is not None:
                    values[i] = val
            except BaseException:
                logging.info(traceback.format_exc())
        return values

    def _evaluate(self, expr, trip, g):
        e = trip  # Do not remove
        # Also do not remove the arguments passed to this function as they might
        # be needed to eval/exec the expression from the flow
        try:
            exec(g)
        except BaseException:
            logging.info(traceback.format_exc())

        # Check if string is not empty and then only evaluate
        # otherwise will raise exception and since we are logging
        # it just clutters the log
        # calling strip only if expr is not None
        if (isinstance(expr, str)) and expr.strip():
            try:
                val = eval(expr)
                logging.info('Evaluating : %s , Value = %s' % (expr, val))
                return val
            except BaseException:
                logging.info('Error Evaluating : %s' % expr)
                logging.debug(traceback.format_exc())
            # else:
            #     logging.debug('Type of expr : %s is %s' % (expr, type(expr)))
        return expr

    def get_param_values_dict(self, trip):
        g = {}
        items = constants.KEYS_LIST
        for i in items:
            try:
                val = self.get_value(trip, i)
                if val is not None:
                    #setattr(self, i, val)
                    g[i] = val
            except BaseException:
                logging.info(traceback.format_exc())
        return g

    def _get_display_data(self, cstep, driver, bu, trip, g):
        display_datas = []
        for d in cstep.get('display_data'):
            if not self._evaluate(d.get('condition'), trip, g):
                continue
            key_value = d.get('key')
            val = None
            if key_value == 'meter_reading_start':
                val = trip.meter_reading_start
            elif key_value == 'meter_reading_end':
                val = trip.meter_reading_end
            elif 'driver.' in (key_value):
                val = trip.driver.user.name
            elif key_value == 'start_time' or key_value == 'end_time':
                val = utc_to_ist(timezone.now()).strftime("%H:%M")
            elif key_value == 'mt_start_time':
                val = utc_to_ist(trip.mt_start_time).strftime(
                    "%H:%M") if trip else ''
            elif key_value == 'mt_end_time':
                if trip:
                    if trip.mt_end_time:
                        val = utc_to_ist(trip.mt_end_time).strftime("%H:%M")
            elif key_value == 'vehicle_no':
                driver_vehicle_map = DriverVehicleMap.objects.filter(
                    driver=trip.driver).first()
                val = driver_vehicle_map.vehicle.registration_certificate_number
            elif key_value == 'total_time':
                val = timedelta_to_str(trip.mt_end_time - trip.mt_start_time)
            elif key_value == 'total_kms':
                val = int(trip.meter_reading_end) - \
                    int(trip.meter_reading_start)
            elif key_value == 'mpos_percent':
                val = self._get_percents(key_value, trip, trip)
            elif key_value == 'delivery_percent':
                val = self._get_percents(key_value, trip, trip)
            elif key_value == 'packages_delivered':
                val = self._get_percents(key_value, trip, trip)
            elif key_value not in ['meter_reading_start', 'meter_reading_end', 'start_time', 'end_time', 'mt_start_time', 'mt_end_time', 'vehicle_no']:
                if key_value == constants.SKEY_PACKAGES_RECEIVED:
                    val = trip.assigned

                elif key_value == constants.SKEY_CASH_PACKAGES:
                    val = trip.delivered_cash

                elif key_value == constants.SKEY_MPOS_PACKAGES:
                    val = trip.delivered_mpos

                elif key_value == constants.SKEY_ATTEMPTED:
                    val = trip.attempted

                elif key_value == constants.SKEY_REJECTED:
                    val = trip.rejected

                elif key_value == constants.SKEY_RETURNED:
                    val = trip.returned

            allow_edit = self._evaluate(d.get('edit_condition'), trip, g)
            key = d.get('key') if allow_edit else ''
            display_datas.append(
                (d.get('display_name'), str(val), key, d.get('keypad_code')))
        return display_datas

    def _get_percents(self, key_value, meter_reading, trip):
        val = None
        mpos_percent = 0

        if trip.assigned > 0:
            delivery_percent = 100 * \
                (trip.assigned - trip.attempted) / trip.assigned
            mpos_p_cash = trip.delivered_mpos + trip.delivered_cash
            if mpos_p_cash > 0:
                mpos_percent = 100 * trip.delivered_mpos / \
                    (trip.delivered_mpos + trip.delivered_cash)
        else:
            meter_reading.meter_reading_end = meter_reading.meter_reading_start
            meter_reading.meter_reading_end.save()

        if key_value == 'delivery_percent':
            val = delivery_percent
        elif key_value == 'packages_delivered':
            val = trip.assigned - trip.attempted - trip.rejected
        else:
            val = mpos_percent
        return val

    def _get_data_to_capture(self, cstep, driver, bu, trip, g):
        data_to_capture = []
        for d in cstep.get('data_to_capture'):
            show = True
            if d.get('condition'):
                show = self._evaluate(d.get('condition'), trip, g)
            if not show:
                continue
            data_to_capture.append(
                (d.get('display_name'), d.get('key'), d.get('keypad_code'))
            )
        return data_to_capture

    def _get_validation_array(self, cstep, driver, bu, trip, g):
        validation_array = []
        for v in cstep.get('validation'):
            try:
                rhs = 0
                print('_get_validation_array')
                print(v)
                print(v.get('rhs'))
                if v.get('rhs') == 'meter_reading_start + 210':
                    rhs = 210

                rhs = self._evaluate(v.get('rhs'), trip, g)
                print(rhs)
                if v.get('error_msg') == constants.RETURN_PKG_ERR_MSG or \
                        v.get('error_msg') == constants.CASH_ORDER_ERR_MSG:
                    rhs = v.get('rhs')

            except BaseException:
                logging.info(traceback.format_exc())
                continue

            validation_array.append({
                'lhs': v.get('lhs'),
                'rhs': rhs,
                'operator': v.get('operator'),
                'error_msg': v.get('error_msg')
            })
        print(validation_array)
        return validation_array

    def store_data(self, trip, form_data, step_count, steps, driver, geopoint=None):
        """
        saves the trip data that the driver inputs through app
        """
        if trip.current_step >= step_count:
            logging.warn('API step = %s, Current step = %s' %
                         (step_count, trip.current_step))
            logging.warn('Not storing info again')
            return {"is_success": False, "message": 'Please Restore the app'}
        time = timezone.now()
        if trip.current_step < step_count:
            trip.current_step = step_count
            trip.save()

        for k, v in form_data.items():
            if v != 'None':
                v_int = int(v)
            else:
                v_int = 0
            nk = constants.get_skey_for_dkey(k)
            if nk in ['meter_reading_start', 'meter_reading_end']:
                if trip.meter_reading_start is None:
                    with transaction.atomic():
                        # update trip status to In-progress
                        trip.status = StatusPipeline.TRIP_IN_PROGRESS
                        trip.actual_start_time = timezone.now()
                        trip.meter_reading_start = form_data.get(nk)
                        trip.mt_start_time = timezone.now()
                        trip.save()

                        response = self.update_order_status(
                            trip, StatusPipeline.OUT_FOR_DELIVERY, driver, geopoint)
                        if not response:
                            return {"is_success": False, "message": 'Order updation failure'}
                else:
                    if nk == 'meter_reading_start':
                        trip.meter_reading_start = form_data.get(nk)
                        trip.mt_start_time = timezone.now()
                    elif (nk == 'meter_reading_end'):
                        trip.meter_reading_end = form_data.get(nk)
                        trip.mt_end_time = timezone.now()
                        trip.actual_end_time = timezone.now()

                    trip.save()
            else:
                if nk == constants.SKEY_PACKAGES_RECEIVED:
                    trip.assigned = v_int

                elif nk == constants.SKEY_CASH_PACKAGES:
                    trip.delivered_cash = v_int

                elif nk == constants.SKEY_MPOS_PACKAGES:
                    trip.delivered_mpos = v_int

                elif nk == constants.SKEY_ATTEMPTED:
                    trip.attempted = v_int

                elif nk == constants.SKEY_REJECTED:
                    trip.rejected = v_int

                elif nk == constants.SKEY_RETURNED:
                    trip.returned = v_int

                trip.save()

        if trip.current_step == len(steps):
            trip.status = StatusPipeline.TRIP_COMPLETED
            trip.total_distance = int(
                trip.meter_reading_end) - trip.meter_reading_start
            total_timedelta = trip.mt_end_time - trip.mt_start_time
            trip.total_time = total_timedelta.total_seconds() / 60
            trip.save()

            TripMixin().update_route_trace_for_trip(trip=trip)

            logging.info('Setting the order as delivered')
            response = self.update_order_status(
                trip, StatusPipeline.ORDER_DELIVERED, driver, geopoint)
            if not response:
                return {"is_success": False, "message": 'Order updation failure'}

        logging.info('API Step = %s, trip.current_step = %s' %
                     (step_count, trip.current_step))
        logging.info(trip)
        return {"is_success": True}

    def ffwd_to_next_step(self, trip, flow, driver, bu, contract):
        steps = flow.get('steps')
        cstep = steps[trip.current_step - 1]
        if not cstep.get('ffwd_condition'):
            return False
        else:
            values = self._set_values(trip)
            logging.info('Setting Values: %s' % json.dumps(values, indent=2))
            g_ = ';'.join(['%s=%s' % (k, str(v).lstrip('0') or '0') for k, v in values.items()])
            exec(g_)
            return self._evaluate(cstep.get('ffwd_condition'), trip, g_)

    def get_response(self, trip, bu, steps, flow, contract, driver, expected_current_step, geopoint=None):
        """ Response to be sent to driver/station app for a particular tripsheet """
        e = trip  # Do not remove
        g = self.get_param_values_dict(trip)
        logging.info('Setting Values: %s' % json.dumps(g, indent=2))
        g_ = ';'.join(['%s=%s' % (k, str(v).lstrip('0') or '0') for k, v in g.items()])
        exec(g_)
        cstep = steps[expected_current_step]
        std_args = (driver, bu, contract, trip, g_)
        display_data = self._get_display_data(cstep, driver, bu, trip, g_)
        data_to_capture = self._get_data_to_capture(
            cstep, driver, bu, trip, g_)
        validation_array = self._get_validation_array(
            cstep, driver, bu, trip, g_)
        action_url = 'step/%d' % (trip.current_step + 1)
        action = constants.ACTION_FORM if cstep.get(
            'data_to_capture') else constants.ACTION_DISPLAY
        auto_dismiss = cstep.get('auto_dismiss')
        auto_dismiss_timeout = cstep.get('timeout')
        qr_code = self._evaluate(cstep.get('qr_code'), trip, g_)
        bottom_text = self._evaluate(cstep.get('bottom_text'), trip, g_)
        preview = bool(self._evaluate(
            cstep.get('preview_condition'), trip, g_))
        title = cstep.get('title')
        pic_url = ''

        if trip.current_step <= expected_current_step:
            if action == constants.ACTION_DISPLAY:
                trip.current_step += 1
                if trip.current_step == len(steps):
                    trip.status = StatusPipeline.TRIP_COMPLETED
                    trip.total_distance = int(
                        trip.meter_reading_end) - trip.meter_reading_start
                    total_timedelta = trip.mt_end_time - trip.mt_start_time
                    trip.total_time = total_timedelta.total_seconds() / 60

                    TripMixin().update_route_trace_for_trip(trip=trip)

                trip.save()
                logging.info('Setting the order as delivered')
                response = self.update_order_status(
                    trip, StatusPipeline.ORDER_DELIVERED, driver, geopoint)
                if not response:
                    return {"is_success": False, "message": 'Order updation failure'}
        else:
            logging.info('Expected current step %d, not matching trip current step %d' %
                         (expected_current_step, trip.current_step)
                         )

        response_dict = {
            "is_success": True,
            'status': True,
            'title': title,
            'action': action,
            'action_url': action_url,
            'auto_dismiss': auto_dismiss,
            'auto_dismiss_timeout': auto_dismiss_timeout,
            'current_step': trip.current_step,
            'total_steps': len(steps),
            'further_steps_remaining': trip.current_step < len(steps),
            'display_data': display_data,
            'preview': preview,
            'data_to_capture': data_to_capture,
            'validation_array': validation_array,
            'expected_qr_code': qr_code,
            'bottom_text': bottom_text,
            'auth_token': '',
            'username': driver.user.name,
            'extras': {
                'customer_key': str(bu),
                'contract_key': str(contract),
                'tripsheet_key': str(trip),
                'driver_type': 'b2b',
            },
            'driver_details': [],
            'pic_url': pic_url,
            'contract': str(contract),
        }

        return response_dict

    def get_standard_duty_status_and_step_response():
        response_dict = {
            'status': True,
            'title': 'Tilte',
            'action': constants.ACTION_SUCCESS,
            'action_url': '',
            'auto_dismiss': True,
            'auto_dismiss_timeout': 4,
            'current_step': 0,
            'total_steps': 0,
            'further_steps_remaining': False,
            'display_data': [],
            'preview': True,
            'data_to_capture': [],
            'validation_array': [],
            'expected_qr_code': '',
            'bottom_text': '',
            'auth_token': '',
            'username': '',
            'extras': {},
            'driver_details': [],
            'pic_url': '',
        }
        return response_dict
