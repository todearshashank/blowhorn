from django.apps import AppConfig
from django.conf.urls import url
from blowhorn.legacy.customer.app import application as legacy_customer_app


class LegacyApplication(AppConfig):
    def get_urls(self):
        urlpatterns = [
            url(r'^user/v7/', legacy_customer_app.urls),
        ]
        return self.post_process_urls(urlpatterns)
