# System && Django
import os
import re
import json
import logging
import requests
import traceback
import phonenumbers
from datetime import datetime, timedelta
from django.conf import settings
from django import forms
from django.shortcuts import get_object_or_404
from django.contrib.auth import logout
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_decode
from django.views.generic import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
from django.core.paginator import Paginator
from django.views.generic import base
from django.db.models import Q, Prefetch, Case, When, Value, IntegerField

# 3rd Party Libraries
from rest_framework import generics, status
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import BasicAuthentication
from blowhorn.oscar.core.loading import get_model
from fcm_django.models import FCMDevice

from config.settings import status_pipelines as StatusPipeline
from blowhorn.common.helper import CommonHelper
from blowhorn.common.notification import Notification
# from blowhorn.common.oscar_loading import get_class
from blowhorn.address.models import UserAddress, City
from blowhorn.order.models import Order, Event
from blowhorn.legacy.adapter import LegacyAdapter
from blowhorn.order.mixins import OrderPlacementMixin
from blowhorn.users.models import User
from blowhorn.order.serializers.OrderSerializer import OrderSerializer
from blowhorn.customer.serializers import CustomerFeedbackSerializer
from blowhorn.common.communication import Communication
from blowhorn.common import sms_templates
from blowhorn.customer.models import Customer
from blowhorn.address.mixins import AddressMixin
from blowhorn.utils.mail import Email
from blowhorn.utils.functions import ist_to_utc, get_tracking_url, get_base_url
from blowhorn.vehicle.utils import VehicleUtils
from .social_profile import FacebookProfile, GoogleProfile
from blowhorn.trip.models import Trip, Stop
from blowhorn.customer.permissions import IsCustomer
from .constants import (
    APP_ORDER_STATUS_NEW,
    APP_ORDER_STATUS_PAST,
    APP_ORDER_STATUS_CURRENT
)
from blowhorn.contract.constants import CONTRACT_TYPE_C2C
from blowhorn.contract.models import CONTRACT_STATUS_ACTIVE, Contract
from blowhorn.order.models import WayPoint
from blowhorn.utils.functions import utc_to_ist
from blowhorn.address.utils import within_city_working_hours, get_city_from_geopoint
from blowhorn.order.utils import OrderUtil
from blowhorn.order.views import OrderList
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.order.const import PAYMENT_STATUS_UNPAID
from blowhorn.order.tasks import push_customer_feedback_to_slack
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.views import OrderCreate
from blowhorn.payment.views import Razorpay as PaymentRazorpay

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

IST_OFFSET_HOURS = 5.5
LABOUR_COST_LIGHT = 200
LABOUR_COST_MEDIUM = 300
LABOUR_COST_HEAVY = 400
DEFAULT_FROM_EMAIL = 'test@blowhorn.net'
KEY_STATUS = "status"
KEY_MESSAGE = "message"

ALLOWED_TIME_FORMATS = [
    '%d-%b-%Y %H:%M',  # 05-Oct-2017 15:00
    '%d %b %Y %H:%M',  # 05 Oct 2017 15:00
    '%b %d, %Y %H:%M',  # Oct 13, 2017 9:00
    '%A, %d %B %Y %I%p'  # Friday, 10 October 2017 5PM
]

User = get_model('users', 'User')


@permission_classes([])
class CustomerLogin(generics.CreateAPIView, CustomerMixin):
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)
    """
    Customer / User Authentication to be done here and the response with the user token to be provided.

    """

    def post(self, request):
        return Response(status=206, data='A newer version of Blowhorn is available, Please update. ')
        # @todo: To be implemented
        data = request.data.dict()
        email = data.get("email").strip()
        password = data.get("password").strip()

        # For now the basic username, password authentication is been done.
        login_response = User.objects.do_login(
            request=request, email=email, password=password)

        if login_response.status_code == status.HTTP_200_OK:
            user = login_response.data.get('user')
            token = login_response.data.get('token')
            response = {
                'status': 'PASS',
                'message': {
                    'name': user.name,
                    'email': user.email,
                    'mAuthToken': token,
                    'mobile_status': 'verified' if user.is_mobile_verified else 'verification_pending',
                    'email_status': 'verified' if user.is_email_verified else 'verification_pending',
                    'b_myfleet_required': False,
                    'b_messenger_subscribed': False,
                    'authorized_branding_user': False
                }
            }

            if not self.get_customer(user):
                print('Users are not allowed to login register as customer')
                return Response(status=status.HTTP_401_UNAUTHORIZED,
                                data='Users are not allowed to login')
            return Response(status=status.HTTP_200_OK, data=response,
                            content_type='text/html; charset=utf-8')

        response = {
            'text': login_response.data,
        }
        return Response(status=login_response.status_code, data=response, content_type='text/html; charset=utf-8')


class CustomerProfile(generics.CreateAPIView, generics.RetrieveAPIView):

    def get(self, request):
        return Response(status=403)
        view = CustomerProfileReal.as_view()
        response = view(request)
        # Catch the reponse and if it is not 200 status,
        # manually change to below response for app to handle
        if response.status_code != 200:
            response.data = {'status': 'UNAUTHORIZED'}
            response.status_code = 200
        return response


class CustomerProfileReal(generics.CreateAPIView, generics.RetrieveAPIView):
    """
    Customer / User Authentication to be done here and the response with the user token to be provided.

    """

    def get(self, request):
        data = request.GET.dict()
        latitude = data.get('lat')
        longitude = data.get('lon')
        is_firebase_user_login = data.get("is_firebase_user_login")

        user = request.user
        user_address = UserAddress.objects.filter(user=user).first()
        phone_number = user.phone_number.as_international if user.phone_number else None

        geopoint = CommonHelper.get_geopoint_from_latlong(
            latitude, longitude)

        city = LegacyAdapter.get_city_from_geopoint(geopoint)
        if not city:
            # By default city  will be bangalore, Need to change
            city = City.objects.filter(id=1).first()

        vehicle_classes = []
        if city:
            vehicle_class_dict = \
                VehicleUtils().get_vehicle_classes_from_c2c_contract(city=city,
                                                                     app=True)
            vehicle_classes = vehicle_class_dict[city.name]

        saved_locations = AddressMixin().get_saved_user_addresses(request.user)
        recent_locations = AddressMixin().get_recent_user_addresses(user)
        message = {
            'name': user.name,
            'email': user.email,
            'mobile': str(phone_number),
            'address': {
                'line': user_address.line1 if user_address else None,
                'area': user_address.line3 if user_address else None,
                'city': user_address.line4 if user_address else None,
                'postal_code': user_address.postcode if user_address else None,
            },
            'mobile_status': 'verified' if user.is_mobile_verified else 'verification_pending',
            'email_status': 'verified' if user.is_email_verified else 'verification_pending',
            'lat': latitude,
            'lon': longitude,
            'saved_locations': saved_locations,
            'recent_locations': recent_locations,
            'items': settings.C2C_ITEMS,
            'vehicle_classes': vehicle_classes,
            'time_slot_details': self.get_time_slots_detail(city),
            'action_type': 'profile-view-flash'
        }

        customer_cancellation_reasons = []
        for key in OrderUtil().fetch_order_cancellation_reasons():
            _reason_dict = {
                'key': key,
                'value': key
            }
            customer_cancellation_reasons.append(_reason_dict)
        cityno = str(city.contact)

        response = {
            'status': 'PASS',
            'message': message,
            'promotions': [],
            'coverage': self._get_coverage_area(),
            'call_support': cityno,
            'call_support_promotion': cityno,
            'share_sub_text': settings.C2C_CONSTANTS['APP_SHARE_SUB_TEXT'],
            'share_body_text': settings.C2C_CONSTANTS['APP_SHARE_BODY_TEXT'],
            'user_type': 'c2c',
            'clone_visible': False,
            'force_update': False,
            'price': settings.C2C_CONSTANTS['PRICE_MOBILE_TEXT'],
            'price_details': settings.C2C_CONSTANTS['PRICE_MOBILE_DETAILS_TEXT'],
            'customer_cancellation_reasons': customer_cancellation_reasons,
            'vehicle_classes': vehicle_classes
        }
        if is_firebase_user_login == 'false':
            firebase_auth_token = CommonHelper.generate_firebase_token(
                settings.FIREBASE_KEYS['customer'])
            response.update({"firebase_auth_token": firebase_auth_token})

        return Response(status=status.HTTP_200_OK, data=response, content_type='text/html; charset=utf-8')

    def post(self, request):
        pass

    def city_contact_number(self, latitude, longitude):

        destination_geopoint = GEOSGeometry(
            'POINT(%s %s)' % (longitude, latitude))
        current_city = City._default_manager.filter(
            coverage__contains=destination_geopoint).first()
        return current_city.contact

    def _get_coverage_area(self):
        coverage = {}
        cities = City.objects.all()
        for city in cities:
            has_c2c_contracts = Contract.objects.filter(contract_type=CONTRACT_TYPE_C2C,
                                                        city=city,
                                                        status=CONTRACT_STATUS_ACTIVE).exists()
            if has_c2c_contracts:
                city_coordinates = city.coverage.coords
                latlon_list = []
                for latlong_arr in city_coordinates:
                    for latlong in latlong_arr:
                        latlon_list.append({
                            'lat': latlong[1],
                            'lon': latlong[0],
                        })
                coverage[city.name] = latlon_list
        return coverage

    def get_time_slots_detail(self, city):
        num_days_future = 14
        start_hour = city.start_time.strftime('%H:%M') if city.start_time else ''
        end_hour = city.end_time.strftime('%H:%M') if city.end_time else ''
        slots = []
        for i in range(num_days_future):
            date = timezone.now() + timedelta(days=i)
            slots.append({
                'date': date.date(),
                'from': start_hour,
                'to': end_hour,
                'closed_hours_details': []
            })

        return slots


class CustomerFCM(generics.CreateAPIView, CustomerMixin):

    def post(self, request):
        user = request.user
        customer = self.get_customer(user)
        if not customer:
            logger.info('Customer not found for user %s' % user)
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='You are not a customer')

        data = request.data.dict()
        # device_type = 'android'
        registration_id = data.get('registration-id')
        device_type = data.get('client', 'android')

        user_device = FCMDevice.objects.filter(user=user, active=True)
        if user_device.exists():
            if user_device.count() == 1:
                Notification.update_fcm_key_for_user(user_device.first(),
                                                     registration_id, '',
                                                     device_type)
        else:
            Notification.add_fcm_key_for_user(request.user, registration_id,
                                              '', device_type.lower())

        response = {
            'status': 'PASS',
            'message': 'Registration id successfully saved'
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


class CustomerBookingSlots(generics.RetrieveAPIView):

    def get(self, request):
        # Timings
        num_days_future = 14
        data = request.GET.dict()

        destination_latitude = data.get('from-lat')  # data.get('to-lat')
        destination_longitude = data.get('from-lon')  # data.get('to-lon')

        destination_geopoint = GEOSGeometry('POINT(%s %s)' % (
            destination_longitude, destination_latitude))

        current_city = City._default_manager.filter(
            coverage__contains=destination_geopoint).first()

        start_hour = int(current_city.start_time.strftime('%H'))
        end_hour = int(current_city.end_time.strftime('%H'))
        date = datetime.now()
        all_slots = {}
        slots = {}
        for i in range(num_days_future):
            date = date + timedelta(days=i)
            this_day_slots = []
            this_slots = all_slots.get(date.strftime('%d-%b-%Y'), {})

            for t in range(24):
                time_24hr = datetime.strptime("%s" % t, "%H")
                time_12hr = time_24hr.strftime("%I%p")
                slot_status = ''
                if t < start_hour or t > end_hour:
                    slot_status = 'disabled'

                this_day_slots.append({
                    't': t,
                    'time': time_12hr,
                    'status': slot_status,
                })
                slots[i] = this_day_slots
        return Response(status=status.HTTP_200_OK, data=slots,
                        content_type="application/json")


@permission_classes([])
class Invoice(generics.RetrieveAPIView):

    def post(self, request, order_number):
        context = {'request': request}
        data = request.data.dict()
        order = Order.objects.filter(number=order_number)
        order = order.prefetch_related('trip_set')
        order = order[0]
        trip = order.trip_set.all()[0]

        try:
            Email().send_receipt('cash', order, trip, [data.get('email')])
            return Response(status=status.HTTP_200_OK,
                            data="Invoice sent successfully")

        except BaseException:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="Unable to send invoice")


class CustomerBookingService(generics.CreateAPIView, CustomerMixin):
    permission_classes = (IsCustomer, )

    def get(self, request, booking_id):
        """
        api for live tracking
        """

        order_qs = Order.objects.filter(number=booking_id,
                                        order_type=settings.C2C_ORDER_TYPE)
        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        # @todo remove app parameter and use same dates and time format
        # in both app and website
        o_json = OrderUtil().get_booking_details_for_customer(
            request=request,
            order=order_qs[0],
            route_info=True,
            app=True
        )
        return Response(status=status.HTTP_200_OK, data=o_json,
                        content_type="application/json")

    def post(self, request):
        dict_response = {
            KEY_STATUS: "FAIL",
            KEY_MESSAGE: {
                'failure_message': 'Something Went wrong!!'
            }
        }
        context = {'request': request}
        data = list(request.data.dict())

        user = request.user
        customer = self.get_customer(user)
        proceed = customer.is_eligible_for_booking()
        if not proceed:
            dict_response[KEY_MESSAGE]['failure_message'] = \
                'Please complete payment for previous trips to proceed with booking.'
            return Response(dict_response, status=status.HTTP_400_BAD_REQUEST)

        try:
            data = json.loads(data[0])
        except ValueError:
            logger.error('Invalid format to send data.')
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="Invalid format to send data.")

        now_or_later = data['timeframe']['now_or_later']
        later_value = data['timeframe']['later_value']

        data['device_type'] = data.get('device_type')
        if not data.get('device_type'):
            data['device_type'] = 'iOS'

        eta, data['pickup_datetime'] = self._get_pickup_time_details(
            now_or_later, later_value)

        is_valid_booking_time = customer._check_booking_window_time(
            booking_time=data['pickup_datetime'],
            lat=data.get('pickup')['geopoint'].get('lat', 0),
            lon=data.get('pickup')['geopoint'].get('lng', 0)
        )
        if not is_valid_booking_time:
            dict_response[KEY_MESSAGE]['failure_message'] = \
                'Service is unavailable at this time. Please schedule for later.'
            return Response(dict_response, status=status.HTTP_400_BAD_REQUEST)

        data['customer'] = customer
        data['status'] = StatusPipeline.ORDER_NEW

        labour = data.get('labour_option', '')
        count = 0
        cost = 0
        if isinstance(labour, str):
            if labour in ['light', 'heavy']:
                count = 1
                cost = LABOUR_COST_HEAVY

        elif isinstance(labour, dict):
            cost = labour.get('cost_per_labour', 0)
            count = labour.get('num_of_labours', 0)

        data['labour_info'] = {
            'no_of_labours': count,
            'labour_cost': cost
        }
        data['vehicle_class_preference'] = data.get('vehicle_class')

        # @todo:  api also gives registration-id which as per the current system is the FCM id
        # see if it is needed in sending customer notifications , but try to not use it and use
        # the common helpers send notification function which will find this id
        # from the table
        pickup_address = data.get('pickup')
        pick_address = pickup_address['address']
        data['pickup_address'] = {
            "title": "",
            "first_name": pickup_address['contact'].get('name', '') if pickup_address.get('contact', None) else '',
            "last_name": "",
            "line1": pickup_address['address'].get('full_address', '') if pickup_address.get('address', None) else '',
            "line2": '',
            "line3": pick_address.get('area', ''),
            "line4": pickup_address['address'].get('city', '') if pickup_address.get('address', None) else '',
            "postcode": pickup_address['address'].get('postal_code', None) if pickup_address.get('address',
                                                                                                 None) else None,
            "phone_number": pickup_address['contact'].get('mobile', '') if pickup_address.get('contact', None) else '',
            "latitude": pickup_address['geopoint'].get('lat', 0) if pickup_address.get('geopoint', None) else 0,
            "longitude": pickup_address['geopoint'].get('lng', 0) if pickup_address.get('geopoint', None) else 0
        }

        dropoff_address = data.get('dropoff')
        drop_address = dropoff_address.get('address', '')
        data['shipping_address'] = {
            "title": "",
            "first_name": dropoff_address['contact'].get('name', '') if dropoff_address.get('contact', None) else '',
            "last_name": "",
            "line1": dropoff_address['address'].get('full_address', '') if dropoff_address.get('address', None) else '',
            "line2": '',
            "line3": drop_address.get('area', ''),
            "line4": dropoff_address['address'].get('city', '') if dropoff_address.get('address', None) else '',
            "postcode": dropoff_address['address'].get('postal_code', None) if dropoff_address.get('address',
                                                                                                   None) else None,
            "phone_number": dropoff_address['contact'].get('mobile', '') if dropoff_address.get('contact',
                                                                                                None) else '',
            "latitude": dropoff_address['geopoint'].get('lat', 0) if dropoff_address.get('geopoint', None) else 0,
            "longitude": dropoff_address['geopoint']['lng'] if dropoff_address.get('geopoint', None) else 0
        }

        waypoints = data.get('waypoints')

        waypoint_data = []
        i = 1
        for waypoint in waypoints:
            contact = waypoint.get('contact')
            address = waypoint.get('address')
            geopoint = waypoint.get('geopoint')
            waypoint_data.append({
                "title": "",
                "first_name": contact.get('name') if contact else '',
                "last_name": "",
                "line1": address.get('full_address') if address else '',
                "line2": address.get('area') if address else '',
                "line3": address.get('area') if address else '',
                "line4": address.get('city') if address else '',
                "postcode": address.get('postal_code') if address else '',
                "phone_number": contact.get('mobile') if contact else '',
                "latitude": geopoint.get('lat') if geopoint else '',
                "longitude": geopoint.get('lng') if geopoint else '',
                'sequence_id': i
            })
            i += 1

        data['waypoints'] = waypoint_data
        data['order_type'] = settings.C2C_ORDER_TYPE
        data['booking_type'] = now_or_later

        order_serializer = OrderSerializer(context=context, data=data)
        order = order_serializer.save()

        order_number = ''
        if order:
            order_number = order.number
            OrderCreate().post_save(order)

        BOOKING_TYPE = settings.C2C_ORDER_TYPE
        background_color = '#13A893'
        tracking_url = get_tracking_url(request, order_number)
        dict_response = {
            KEY_STATUS: "PASS",
            KEY_MESSAGE: {
                'booking_id': order_number,
                'friendly_id': order_number,
                'tracking_url': tracking_url,
                'booking_type': 'advanced',
                'failure_title': 'Thank You!',
                'failure_message': 'Sorry! Unable to take your booking',
                'success_title': 'Congratulations!',
                'success_message': 'We have received your booking and will contact you shortly',
                'background_color': background_color,
                'buttons': {
                    'done': True,
                    'call_support': False,
                }
            }
        }

        if order:
            template_dict = sms_templates.template_booking_acceptance
            message = template_dict['text'] % (
                order_number, tracking_url)
            Communication.send_sms(sms_to=user.national_number, message=message,
                priority='high', template_dict=template_dict
            )

        return Response(status=status.HTTP_200_OK, data=dict_response)

    def fix_pickup_time(self, time):
        """ Added a fix for pickup time since App is sending in
            09 Oct 2017 9:00 instead of 09 Oct 2017 09:00
        """
        m = re.match(r'(\d{2} \S{3} \d{4}) (\d:\d{2})', time)
        if m:
            return '%s 0%s' % (m.group(1), m.group(2))
        else:
            return time

    def _get_pickup_time_details(self, now_or_later, pickup_time):
        now = datetime.now()
        if now_or_later == 'now':
            pickup_date_time_obj = now
        else:
            pickup_time_fixed = self.fix_pickup_time(pickup_time)
            for f in ALLOWED_TIME_FORMATS:
                logging.info('Matching time "%s" with format "%s"' % (
                    pickup_time_fixed, f))
                try:
                    pickup_date_time_obj = ist_to_utc(
                        datetime.strptime(pickup_time_fixed, f))
                    break
                except:
                    pass

        time_to_action = pickup_date_time_obj - now
        eta_s = time_to_action.total_seconds()
        return eta_s, pickup_date_time_obj

    def get_labour_cost(self, labour):
        labour_type = {
            'light': LABOUR_COST_LIGHT,
            'medium': LABOUR_COST_MEDIUM,
            'heavy': LABOUR_COST_HEAVY,
        }
        return labour_type.get(labour, 0)


class CustomerLogout(generics.CreateAPIView):

    def post(self, request):
        data = request.GET.dict()
        # Remove the FCM Key on logout for the driver device
        resp = Notification.remove_fcm_key_for_user(
            user=request.user, registration_id=data.get('fcm_id'))
        logout(request)
        return Response(status=status.HTTP_200_OK)


class CustomerCurrentBooking(generics.RetrieveAPIView):

    def get(self, request):
        # Get all the in-progress bookings.
        orders = []
        order_qs = Order._default_manager.filter(
            user=request.user,
            order_type=settings.C2C_ORDER_TYPE,
            status__in=StatusPipeline.get_child_statii(StatusPipeline.IN_PROGRESS)).order_by(
            '-pickup_datetime')

        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        out = []
        if order_qs.exists():
            for order in order_qs:
                trip = OrderPlacementMixin().get_trip_for_order(order)
                data = LegacyAdapter().get_booking_for_customer(order, trip)

                # @todo remove app parameter and use same dates and time format
                # in both app and website
                dict_ = OrderUtil().get_booking_details_for_customer(
                    request=request,
                    order=order,
                    app=True,
                    route_info=True,
                )
                out.append(dict_)
                orders.append(data)

        response = {
            KEY_STATUS: 'PASS',
            'message': orders,
            'v7': out
        }

        return Response(status=status.HTTP_200_OK, data=response, content_type='text/html; charset=utf-8')


class CustomerFeedback(generics.CreateAPIView, CustomerMixin):

    def _set_field_details(self, title, val, short='false', link=None):
        return {
            'title': title,
            'value': '%s' % val if not link else '<%s|%s>' % (link, val),
            'short': short
        }

    def _get_slack_message(self, request, customer, user, feedback_text):
        author_name = '%s | %s' % (customer.name, user.phone_number)
        base_url = get_base_url(request)
        title_link = ''.join([base_url, reverse('admin:customer_customer_change', args=[customer.id])])

        title = '%s' % user.email
        recent_order = Order.objects.get_recent_booking_of_customer(customer)
        fields = []
        if recent_order:
            order_number = recent_order.get('number', '')
            order_url = ''.join([base_url, reverse('admin:order_bookingorder_changelist'),
                                '?q=', order_number])
            fields.extend([
                self._set_field_details('Recent Order', order_number, short='true', link=order_url),
                self._set_field_details('City', recent_order.get('city__name', ''), short='true')
            ])
        fields.append(self._set_field_details('Message', feedback_text))

        slack_message = {
            "attachments": [{
                "author_name": author_name,
                "title": title,
                "title_link": title_link,
                "thumb_url": settings.THUMBNAIL_LINK,
                "color": "#52c0d2",
                "fields": fields
            }]
        }
        return slack_message

    def post(self, request):
        data = request.data.dict()
        feedback_text = data.pop('feedback', '')
        user = request.user

        customer = self.get_customer(user)
        if not customer:
            logger.info('Feedback API: Customer not exists for %s' % user)
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        feedback_data = {
            'feedback': feedback_text,
            'customer': customer.id,
            'extra_details': json.dumps(data)
        }
        serializers = CustomerFeedbackSerializer(data=feedback_data)
        if serializers.is_valid():
            result = serializers.save()
            success_msg = True
            message = 'Success'
            slack_message = self._get_slack_message(request, customer, user,
                                                    feedback_text)
            push_customer_feedback_to_slack.apply_async((slack_message,),)
            response_dict = {
                KEY_STATUS: success_msg,
                KEY_MESSAGE: message
            }
            return Response(status=status.HTTP_200_OK, data=response_dict,
                            content_type='text/html; charset=utf-8')
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        content_type='text/html; charset=utf-8')


@permission_classes([])
class CustomerSignup(generics.CreateAPIView):

    def post(self, request):
        return Response(status=206, data='A newer version of Blowhorn is available, Please update. ')
        data = request.data.dict()
        email = data.get('email', None)
        password = data.get('password', None)

        response = {
            'status': 'FAIL',
            'message': {
                'text': '',
            }
        }

        if not email or not password:
            response['message']['text'] = 'email and password required'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        if User._default_manager.filter(email=email.lower()).exists():
            response['message']['text'] = 'User with this email already exists.'
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response,
                            content_type='text/html; charset=utf-8')

        customer_response = Customer.objects.create_customer(
            email=email.lower(),
            password=password,
            name=data.get('name'),
            is_active=True
        )

        if customer_response.status_code != status.HTTP_200_OK:
            return Response(status=customer_response.status_code,
                            data=customer_response.data,
                            content_type='text/html; charset=utf-8')

        user = customer_response.data.get('user')
        login_response = User.objects.do_login(request=request, user=user)

        if login_response.status_code != status.HTTP_200_OK:
            return Response(status=login_response.status_code,
                            data=login_response.data,
                            content_type='text/html; charset=utf-8')

        token = login_response.data.get('token')
        response['status'] = True
        response['message'] = {
            'name': user.name,
            'email': user.email,
            'mAuthToken': token,
        }
        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


class CustomerMobile(generics.CreateAPIView):

    def post(self, request):
        data = request.data.dict()
        header = request.META
        # import phonenumbers

        mobile = data.get('mobile')

        if not mobile:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Invalid Mobile Number')

        user = request.user
        # Update the number only when the old number is not the same as the new
        # number
        phone_number = phonenumbers.parse(
            mobile, settings.DEFAULT_COUNTRY.get('country_code'))

        if user.phone_number != phone_number:
            user.phone_number = phone_number
            user.save()

        otp, created = User.objects.retrieve_otp(user)
        template_dict = sms_templates.template_otp
        message = template_dict['text'] % otp
        Communication.send_sms(sms_to=phone_number.national_number,
                               message=message, priority='high', template_dict=template_dict)
        return Response(status=status.HTTP_200_OK)


class CustomerMobileVerify(generics.CreateAPIView):

    def post(self, request):
        """
            Used by customer app to verify the OTP sent to it
            API Call: POST /mobileverify
        """
        data = request.data.dict()
        user_otp = data.get("otp")
        user = request.user

        otp, created = User.objects.retrieve_otp(user)
        if str(user_otp) != otp:
            return Response(status=status.HTTP_402_PAYMENT_REQUIRED,
                            data='OTP Mismatch')

        user.is_mobile_verified = True
        user.is_active = True
        user.save()
        return Response(status=status.HTTP_200_OK)


class CustomerCoupons(generics.CreateAPIView):

    def post(self, request):
        return Response(status=status.HTTP_401_UNAUTHORIZED,
                        data='Invalid Coupon')


class CustomerBookingsWeb(generics.RetrieveAPIView, CustomerMixin):

    def get(self, request):
        data = request.GET.dict()
        offset = int(data.get('start', 0))
        limit = int(data.get('length', 10))
        page = int(data.get('page') or (offset % limit) + 1)
        user = User.objects.get(email=request.user)
        customer = self.get_customer(user)
        order_qs = Order._default_manager.filter(order_type=settings.C2C_ORDER_TYPE,
                                                 customer=customer.id).exclude(
            status=StatusPipeline.ORDER_CANCELLED).order_by('-pickup_datetime')[offset:limit]
        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )
        out = []
        for order in order_qs:
            trip = OrderPlacementMixin().get_trip_for_order(order)
            data = LegacyAdapter().get_booking_for_customer(order, trip)
            out.append(data)

        response = {
            'data': out,
            'page_size': limit,
        }

        return Response(status=status.HTTP_200_OK, data=response,
                        content_type='text/html; charset=utf-8')


class BookingsWeb(generics.RetrieveAPIView, CustomerMixin):

    def get(self, request, when=None):
        """
        returns the current, past and upcoming order based on request
        """
        data = request.GET.dict()
        cursor = data.get('cursor') if data.get('cursor') else 1

        user = User.objects.get(email=request.user)
        customer = self.get_customer(user)

        query_params = Q(order_type=settings.C2C_ORDER_TYPE, customer=customer)

        if when == APP_ORDER_STATUS_PAST:
            query = Q(status=StatusPipeline.ORDER_DELIVERED)
        if when == APP_ORDER_STATUS_NEW:
            ordering = 'pickup_datetime'
            query = Q(status=StatusPipeline.ORDER_NEW)
        elif when == APP_ORDER_STATUS_CURRENT:
            ordering = '-pickup_datetime'
            query = ~Q(status__in=[StatusPipeline.ORDER_DELIVERED,
                                   StatusPipeline.ORDER_NEW,
                                   StatusPipeline.ORDER_CANCELLED]
                       )
        query_params = query_params & query if query else query_params

        order_qs = Order._default_manager.filter(query_params)

        if when == APP_ORDER_STATUS_PAST:
            order_qs = order_qs.annotate(
                payment_status_int=Case(
                    When(payment_status=PAYMENT_STATUS_UNPAID, then=Value('0')),
                    default=Value('1'),
                    output_field=IntegerField()
                )
            ).order_by('payment_status_int', '-pickup_datetime')
        else:
            order_qs = order_qs.order_by(ordering)

        order_qs = order_qs.select_related(
            'driver__user', 'pickup_address', 'shipping_address',
            'vehicle_class_preference', 'customer__user', 'labour'
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'waypoint_set',
                queryset=WayPoint.objects.select_related(
                    'shipping_address', 'order_line')
            )
        )
        order_qs = order_qs.prefetch_related(
            Prefetch(
                'trip_set',
                queryset=Trip.objects.filter(
                    status__in=[
                        StatusPipeline.TRIP_NEW,
                        StatusPipeline.TRIP_IN_PROGRESS,
                        StatusPipeline.TRIP_COMPLETED,
                        StatusPipeline.DRIVER_ACCEPTED,
                        StatusPipeline.TRIP_ALL_STOPS_DONE,
                        StatusPipeline.TRIP_CANCELLED
                    ]
                ).select_related(
                    'driver__user',
                    'vehicle__vehicle_model__vehicle_class', 'hub'
                ).prefetch_related(
                    Prefetch(
                        'stops',
                        queryset=Stop.objects.select_related('waypoint'))
                ),
                to_attr='trips'
            )
        )

        order = order_qs
        # order = order.select_related('shipping_address')

        paginator = Paginator(order, 10)

        if int(cursor) <= paginator.num_pages:

            order_page = paginator.page(cursor)

            # @todo remove app parameter and use same dates and time format
            # in both app and website
            results_json = OrderSerializer().get_orders(
                request=request,
                results=order_page.object_list,
                app=True
            )

            current = results_json['current']
            past = results_json['past']
            upcoming = results_json['upcoming']
        else:
            current = []
            past = []
            upcoming = []

        cursor = int(cursor) + 1
        dict_ = {
            'current': current,
            'past': past,
            'upcoming': upcoming,
            'more': '',
            'cursor': str(cursor),
        }

        return Response(status=status.HTTP_200_OK, data=dict_, content_type='text/html; charset=utf-8')


class CustomerBookingStatus(generics.RetrieveAPIView):

    def post(self, request):
        data = request.data.dict()
        order_data = {}

        order = Order.objects.filter(number=data.get('booking_id')).first()

        if order and order.status==StatusPipeline.ORDER_NEW and \
            'cancel_booking' == data.get('booking_action', ''):
            reason = data.get('cancellation_reason')
            other_reason = data.get('other_cancellation_reason')

            remarks = reason + '  ' + other_reason + 'App'

            Order.objects.filter(id=order.id).update(status=StatusPipeline.ORDER_CANCELLED,
                                                    reason_for_cancellation=reason,
                                                    other_cancellation_reason=other_reason)

            event = Event.objects.create(
                status=StatusPipeline.ORDER_CANCELLED,
                order=order,
                remarks=remarks,
            )
            # @todo save is not saving user so saving it again
            event.created_by = request.user
            event.save()

            message = 'Booking Cancelled'
            response_dict = {
                'status': 200,
                'data': {},
                'message': message
            }
            return Response(status=status.HTTP_200_OK, data=response_dict,
                            content_type="application/json")

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Cannot cancel the order.Please contact Support',
                        content_type="application/json")


class CustomerPromotions(generics.RetrieveAPIView):

    def get(self, request):
        response = {
            'data': [],
            'page_size': 10,
        }
        return Response(status=status.HTTP_200_OK, data=response, content_type='text/html; charset=utf-8')


@permission_classes([])
class SocialLogin(generics.CreateAPIView):

    def set_response(self, request, user, mobile=False):
        if not user:
            response_status = 'FAIL'
            message = {
                'why': 'account_unavailable'
            }
        else:
            email = user.get('email')
            name = user.get('name')
            if not (email and name):
                response_status = 'FAIL'
                message = {
                    'why': 'account_unavailable',
                    'name': name,
                    'email': email,
                }
            else:
                user = User._default_manager.filter(
                    email=email.lower()).first()
                if not user:
                    extra_fields = {
                        'is_email_verified': True
                    }
                    customer_response = Customer.objects.create_customer(
                        email=email,
                        name=name,
                        is_active=True,
                        **extra_fields
                    )

                    if customer_response.status_code != status.HTTP_200_OK:
                        return Response(status=customer_response.status_code,
                                        data=customer_response.data, content_type='text/html; charset=utf-8')

                    user = customer_response.data.get('user')

                login_response = User.objects.do_login(
                    request=request, user=user)

                if login_response.status_code != status.HTTP_200_OK:
                    response_status = 'FAIL'
                    message = login_response.data
                else:
                    response_status = 'PASS'
                    message = {
                        'mobile_status': 'verified' if user.is_mobile_verified else 'verification_pending',
                        'email_status': 'verified' if user.is_email_verified else 'verification_pending',
                        'name': name,
                        'email': email,
                        'b_messenger_subscribed': False,
                        'b_myfleet_required': False,
                        'authorized_branding_user': False
                    }
                    if mobile:
                        # payload = jwt_payload_handler(user)
                        # mAuthToken = jwt_encode_handler(payload)
                        mAuthToken = login_response.data.get('token')
                        message['mAuthToken'] = mAuthToken

        response_dict = {
            'status': response_status,
            'message': message
        }
        return Response(status=status.HTTP_200_OK, data=response_dict, content_type='text/html; charset=utf-8')


@permission_classes([])
class GoogleLogin(generics.CreateAPIView):

    def post(self, request):
        return Response(status=206, data='A newer version of Blowhorn is available, Please update. ')
        data = request.data
        accessToken = data.get("access_token")
        isMobile = request.META.get('HTTP_MOBILE')
        mobile_app = True if isMobile else False
        rname = data.get("name")
        remail = data.get("email")

        sp = GoogleProfile()
        user = sp.get(google_token=accessToken)
        return SocialLogin().set_response(request, user, mobile_app)


@permission_classes([])
class FacebookLogin(generics.CreateAPIView):

    def post(self, request):
        return Response(status=206, data='A newer version of Blowhorn is available, Please update. ')
        data = request.data
        accessToken = data.get("access_token")
        isMobile = request.META.get('HTTP_MOBILE')
        mobile_app = True if isMobile else False
        mobile_app = True
        rname = data.get("name")
        remail = data.get("email")

        sp = FacebookProfile()
        user = sp.get(token=accessToken)
        return SocialLogin().set_response(request, user, mobile_app)


class CustomerPayment(generics.CreateAPIView):

    def post(self, request):
        data = request.data.dict()
        booking_key = data.get('booking_key')
        order = Order.objects.filter(number=booking_key).first()
        if order:
            # PaymentMixin.update_source(order=order, payment_type='razorpay')
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


@permission_classes([])
class Razorpay(generics.CreateAPIView):

    def post(self, request):
        data = request.data.dict()
        return PaymentRazorpay.as_view()(request._request, data)


class DriversLocation(generics.RetrieveAPIView):

    def get(self, request):
        """
            Used by customer app to get drivers  currentloaction
        """
        data = request.GET.dict()
        order_id = data.get('booking_id')
        driver_id = data.get('driver_id')
        status_dict = 'FAIL'
        response = {}
        try:
            if not driver_id:
                order = Order._default_manager.filter(number=order_id)
                trip = OrderPlacementMixin().get_trip_for_order(order)
                driver_id = trip.driver
                driver_details = DriverActivity.objects.filter(
                    order_id=order_id,
                    trip_id=trip,
                    driver_id=driver_id
                ).last()
                if driver_details.count() > 0:
                    lat = driver_details.geopoint.y
                    lon = driver_details.geopoint.x
                    status_dict = 'PASS'
        except BaseException:
            traceback.format_exc()
        if status_dict == 'PASS':
            response = {
                KEY_STATUS: status_dict,
                KEY_MESSAGE: {
                    'lat': lat,
                    'lon': lon,
                }
            }
        return Response(status=status.HTTP_200_OK, data=response)


@permission_classes([])
class PasswordReset(generics.CreateAPIView, FormView):

    def post(self, request):
        """ Used by the users to reset the password
            API Call: POST /passwordforget
        """
        status_message = ''
        message = ''
        data = request.data.dict()
        user_email = data.get('email')
        if user_email:
            try:
                user = User.objects.get(email=user_email)
            except User.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)

            url_protocol = 'https' #request.META.get('wsgi.url_scheme', 'http')
            url = ''.join([url_protocol, '://',
                           request.META.get('HTTP_HOST', 'blowhorn.com'),
                           '/accounts/password/reset/'])

            client = requests.session()
            client.get(url)
            if 'csrftoken' in client.cookies:
                csrftoken = client.cookies['csrftoken']
                data = dict(email=user_email,
                            csrfmiddlewaretoken=csrftoken)
                client.post(url, data=data, headers=dict(Referer=url))
                status_message = 'PASS'
                message = 'SUCCESS'

        else:
            status_message = 'FAIL'
            message = 'User e-mail cannot be blank'

        response_dict = {
            KEY_STATUS: status_message,
            KEY_MESSAGE: message,
        }
        return Response(status=status.HTTP_200_OK, data=response_dict,
                        content_type='text/html; charset=utf-8')


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(label="New password",
                                    widget=forms.PasswordInput(
                                        attrs={'cols': 10, 'rows': 20}))
    new_password2 = forms.CharField(label="New password confirmation",
                                    widget=forms.PasswordInput)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
            specialCharacters = r"[\$#@!\*]"
            if password2 is None or len(password2) < 8 or \
                re.search('[0-9]', password2) is None or \
                re.search('[A-Z]', password2) is None or \
                    not any(c in specialCharacters for c in password2):
                raise forms.ValidationError("Invalid password. Password must \
                    contain atleast One Uppercase , One Lowercase and Special \
                    characters")

        return password2


class PasswordResetConfirmView(FormView):
    template_name = settings.APPS_DIR.path(
        'templates').path('account/password_reset_from_key.html')
    success_url = '/'
    form_class = SetPasswordForm

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        UserModel = User
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                new_password = form.cleaned_data['new_password2']
                user.set_password(new_password)
                user.is_email_verified = True
                user.save()
                messages.success(request, 'Password has been reset.')
                return self.form_valid(form)
            else:
                messages.error(
                    request, 'Password reset has not been unsuccessful.')
                return self.form_invalid(form)
        else:
            messages.error(request,
                           'The reset password link is no longer valid. Request a new one')
            return self.form_invalid(form)


class QuickBook(generics.CreateAPIView):
    # authentication_classes = (
    #     CsrfExemptSessionAuthentication, BasicAuthentication)

    permission_classes = (IsAuthenticated,)

    def post(self, request):

        request_vars = json.loads(request.body)
        from_location = request_vars.get('from')
        to_location = request_vars.get('to')
        truck_type = request_vars.get('truck_type')
        coordinates = from_location.get('geopoint')
        device_type = request_vars.get('device_type')

        if not from_location or \
                not to_location or not truck_type:
            response = {
                'status': 'FAIL',
                'message': 'Unable to place order'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        geopoint = CommonHelper.get_geopoint_from_latlong(
            coordinates.get('lat'), coordinates.get('lng'))

        city = get_city_from_geopoint(geopoint)
        now = utc_to_ist(timezone.now())
        # Check if outside of working hours.
        if not within_city_working_hours(city, now.time()):
            response = {
                'status': 'FAIL',
                'message': 'No Vehicles available now'
            }
            return Response(status=status.HTTP_404_NOT_FOUND, data=response)

        formatted_data = OrderUtil().format_quickbook_data(
            from_location, to_location, truck_type, device_type=device_type)

        context = {'request': request}
        return OrderList().create_c2c_order(request, formatted_data, context)
