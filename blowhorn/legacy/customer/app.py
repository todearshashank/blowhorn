from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.apps import AppConfig

from .views import CustomerLogin, CustomerProfile, CustomerFCM, GoogleLogin, FacebookLogin
from .views import CustomerLogout, CustomerCurrentBooking, CustomerBookingSlots, QuickBook
from .views import CustomerSignup, CustomerPromotions, CustomerBookingsWeb, CustomerFeedback
from .views import CustomerBookingService, CustomerMobile, CustomerMobileVerify, Razorpay, CustomerPayment, CustomerBookingStatus
from .views import DriversLocation, PasswordReset, PasswordResetConfirmView, BookingsWeb, Invoice
from .views import CustomerCoupons
from  blowhorn.customer.views import SaveUserAddress


class LegacyCustomerApplication(AppConfig):

    def get_urls(self):
        # Add custom URL's here
        urlpatterns = [
            url(r'^login$', CustomerLogin.as_view(), name='old-customer-login'),
            url(r'^signup$', CustomerSignup.as_view(), name='old-customer-signup'),
            url(r'^fblogin$', FacebookLogin.as_view(), name='old-customer-fblogin'),
            # url(r'^glogin$', GoogleLogin.as_view(), name='old-customer-glogin'),
            url(r'^googlelogin$', GoogleLogin.as_view(), name='old-customer-googlelogin'),
            url(r'^logout$', CustomerLogout.as_view(), name='old-customer-logout'),
            url(r'^mobile$', CustomerMobile.as_view(), name='old-customer-mobile'),
            url(r'^mobileverify$', CustomerMobileVerify.as_view(), name='old-customer-mobileverify'),
            url(r'^profile$', CustomerProfile.as_view(), name='old-customer-profile'),
            url(r'^passwordforget$', PasswordReset.as_view(), name='old-customer-forget'),
            url(r'^fcmid$', CustomerFCM.as_view(), name='old-customer-updatefcmid'),
            url(r'^password/reset/complete/$', auth_views.PasswordResetCompleteView, name='password_reset_complete'),
            url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
                PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
            url(r'^userlocation$', SaveUserAddress.as_view(), name='SaveUserAddress'),

            url(r'^order$', CustomerBookingService.as_view(), name='old-customer-bookingservice'),
            url(r'^order/(\S+)$', CustomerBookingService.as_view(), name='old-customer-bookingservice'),
            url(r'^promotionservice$', CustomerLogin.as_view(), name='old-customer-promotionservice'),
            url(r'^bookingstatus$', CustomerBookingStatus.as_view(), name='old-customer-bookingstatus'),
            url(r'^currentbooking$', CustomerCurrentBooking.as_view(), name='old-customer-currentbooking'),
            url(r'^bookingdetails$', CustomerLogin.as_view(), name='old-customer-bookingdetails'),

            url(r'^bookingsweb$', CustomerBookingsWeb.as_view(), name='old-customer-bookingsweb'),
            url(r'^orders/(\S+)$', BookingsWeb.as_view(), name='old-customer-bookingsweb'),
            url(r'^promotions$', CustomerPromotions.as_view(), name='old-customer-promotions'),

            url(r'^driverlocation$', DriversLocation.as_view(), name='old-customer-driverlocation'),
            url(r'^ratedriver$', CustomerLogin.as_view(), name='old-customer-ratedriver'),
            url(r'^coupon$', CustomerCoupons.as_view(), name='old-customer-coupon'),
            url(r'^slots$', CustomerBookingSlots.as_view(), name='old-customer-slots'),
            url(r'^payment$', CustomerPayment.as_view(), name='old-customer-payment'),
            url(r'^razorpay$', Razorpay.as_view(), name='old-customer-razorpay'),
            url(r'^feedback$', CustomerFeedback.as_view(), name='old-customer-feedback'),
            url(r'invoice/(\S+)$', Invoice.as_view(), name='invoice'),
            url(r'^quickbook$', QuickBook.as_view(), name='quickbook'),
        ]

        return self.post_process_urls(urlpatterns)
