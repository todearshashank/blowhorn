from django.utils.translation import ugettext_lazy as _

APP_ORDER_STATUS_NEW = 'upcoming'
APP_ORDER_STATUS_COMPLETED = 'done'
APP_ORDER_STATUS_INPROGRESS = 'ongoing'

APP_ORDER_STATUS_CURRENT = 'current'
APP_ORDER_STATUS_PAST = 'past'

APP_ORDER_STATUS = (
    (APP_ORDER_STATUS_NEW, _('upcoming')),
    (APP_ORDER_STATUS_COMPLETED, _('done')),
    (APP_ORDER_STATUS_INPROGRESS, _('ongoing')),
)


APP_TRIP_STATUS_NEW = 'pending'
APP_TRIP_STATUS_COMPLETED = 'done'
APP_TRIP_STATUS_INPROGRESS = 'ongoing'

APP_TRIP_STATUS = (
    (APP_TRIP_STATUS_NEW, _('pending')),
    (APP_TRIP_STATUS_COMPLETED, _('done')),
    (APP_TRIP_STATUS_INPROGRESS, _('ongoing')),
)
