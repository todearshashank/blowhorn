from django.utils import timezone
from config.settings import status_pipelines as StatusPipeline
from datetime import datetime, timedelta

import os
import json

from blowhorn.contract.contract_helpers.contract_helper import ContractHelper
from blowhorn.driver.payment_helpers.c2c_payment_helper import PaymentHelper
from blowhorn.trip.models import Trip

from .constants import (
    APP_ORDER_STATUS_NEW,
    APP_ORDER_STATUS_COMPLETED,
    APP_ORDER_STATUS_INPROGRESS,
    APP_TRIP_STATUS_NEW,
    APP_TRIP_STATUS_COMPLETED
)
from blowhorn.contract.constants import VEHICLE_SERVICE_ATTRIBUTE_MINUTES
from blowhorn.vehicle.constants import C2C_VEHICLE_CLASSES, DEFAULT_VEHICLE_CLASS
from blowhorn.address.maps import GoogleMaps
from blowhorn.utils.functions import utc_to_ist, get_tracking_url
from blowhorn.utils.functions import dist_between_2_points, geopoints_to_km
from blowhorn.order.utils import OrderUtil
from blowhorn.vehicle.utils import get_vehicle_info
from blowhorn.vehicle.constants import VEHICLE_ICON_PREFIX

DATE_FORMAT = '%d-%b-%Y'
TIME_FORMAT = '%I:%M %p'
AVERAGE_VEHICLE_SPEED_KMPH = 15.0
DISTANCE_MULTIPLIER = 1.3


class BookingDetails(object):

    def _get_stops_details(self, trip, order):
        """
        returns stops for given trip
        """
        stops = []
        customer_phone_number = ''
        pickup_phone_number = ''

        if order.customer and order.customer.user.phone_number:
            customer_phone_number = order.customer.user.national_number

        time_ = timezone.now()
        dropoff_address = []
        waypoints = sorted(order.waypoint_set.all(), key = lambda x: x.id)
        stop_distance = 0
        previous_dist = 0
        previous_time = 0
        for waypoint in waypoints:
            shipping_address_str = ','.join(
                order.shipping_address.active_address_fields())
            wpoint_address = waypoint.shipping_address
            wpoint_address_str = ','.join(
                wpoint_address.active_address_fields())
            if wpoint_address_str != shipping_address_str:
                stop = waypoint.stops.all(
                )[0] if waypoint.stops.all() else ''

                stop_distance = stop.distance if stop else 0

                if stop and stop.status == StatusPipeline.TRIP_COMPLETED:
                    stop_status = APP_TRIP_STATUS_COMPLETED
                else:
                    stop_status = APP_TRIP_STATUS_NEW

                stop_pickup_coordinates = waypoint.shipping_address.geopoint.coords
                shipping_address = waypoint.shipping_address

                if shipping_address:
                    stop_phone_number = shipping_address.phone_number.national_number \
                        if shipping_address.phone_number else ''

                if not stop_phone_number:
                    stop_phone_number = customer_phone_number

                waypoint_dist =  waypoint.estimated_distance if waypoint.estimated_distance else 0
                previous_dist += waypoint_dist
                waypoint_minutes = (waypoint_dist/AVERAGE_VEHICLE_SPEED_KMPH) * 60
                previous_time += waypoint_minutes

                time_ = utc_to_ist(
                    order.pickup_datetime + timedelta(minutes=previous_time))

                minutes_ = 0
                hours = waypoint_minutes // 60
                minutes_ = waypoint_minutes % 60

                stops.append({
                    'name': shipping_address.line3,
                    'address': shipping_address.line1,
                    'area': shipping_address.line3,
                    'postal_code': shipping_address.postcode,

                    "location": {
                        "lat": stop_pickup_coordinates[1],
                        "lng": stop_pickup_coordinates[0]
                    },
                    'time': time_.strftime(TIME_FORMAT),
                    'status': stop_status,
                    'wait_duration': '',
                    'distance': stop_distance if stop_distance else waypoint_dist,
                    'remaining_distance': waypoint_dist,
                    'estimated_distance': waypoint_dist,
                    'travel_time': '%d Hours %d Mins' % (hours, minutes_),
                    "contact": {
                        "mobile": str(stop_phone_number),
                        "name": shipping_address.first_name,
                        "email": order.customer.user.email if order else ''
                    },
                })

                dropoff_address = stop_pickup_coordinates

        return stops, dropoff_address

    def _get_pickup_address(self, order, trip):
        """
        return pickup address as dict from order
        """
        pickup_coordinates = order.pickup_address.geopoint.coords

        customer_phone_number = ''
        pickup_phone_number = ''

        pickup_address = order.pickup_address

        if trip and trip.actual_start_time:
            pickup_status = APP_TRIP_STATUS_COMPLETED
        else:
            pickup_status = APP_TRIP_STATUS_NEW

        if order.customer and order.customer.user.phone_number:
            customer_phone_number = order.customer.user.national_number

        if pickup_address and pickup_address.phone_number:
            pickup_phone_number = pickup_address.phone_number.national_number

        if not pickup_phone_number:
            pickup_phone_number = customer_phone_number

        rem_dist =0
        if trip and trip.driver and trip.driver._driveractivity_cache:
            driver_geopoint = trip.driver.driveractivity.geopoint
            pickup_geopt = order.pickup_address.geopoint
            rem_dist = dist_between_2_points(driver_geopoint, pickup_geopt)

        pickup = {
            'name': pickup_address.first_name,
            'address': pickup_address.line1,
            'area': pickup_address.line3,
            'postal_code': pickup_address.postcode,
            "time": utc_to_ist(order.pickup_datetime).strftime(TIME_FORMAT),
            'status': pickup_status,
            "driver_pic": "",
            'wait_duration': '',
            'distance': rem_dist,
            'remaining_distance': rem_dist,
            'estimated_distance': rem_dist,
            'travel_time': '',
            "contact": {
                "mobile": str(pickup_phone_number),
                "name": pickup_address.first_name,
                "email":  order.customer.user.email if order else ''
            },
            "location": {
                "lat": pickup_coordinates[1],
                "lng": pickup_coordinates[0]
            },
        }
        return pickup

    def _get_dropoff_address(self, order, trip):
        """
        returns dropoff address as dict from order
        """
        dropoff_coordinates = order.shipping_address.geopoint.coords

        shipping_address = order.shipping_address

        if trip and trip.actual_end_time:
            dropoff_status = APP_TRIP_STATUS_COMPLETED
        else:
            dropoff_status = APP_TRIP_STATUS_NEW

        customer_phone_number = order.customer.user.national_number if order else ''
        shipping_phone_number = shipping_address.phone_number.national_number \
            if shipping_address.phone_number else ''

        if not shipping_phone_number:
            shipping_phone_number = customer_phone_number

        trip_minutes = trip.total_time if trip and trip.total_time else 0
        seconds = trip.total_time * 60 if trip and trip.total_time else 0
        hours = (seconds) // 3600
        minutes = (seconds % 3600) // 60

        last_geopoint = order.pickup_address.geopoint

        dist_ = 0
        dist_sum = 0
        distance = 0
        total_distance = 0
        total_dist_minutes = 0
        if trip and trip.actual_end_time:
            time_ = trip.actual_end_time

        waypoints = sorted(order.waypoint_set.all(), key = lambda x: x.id)

        shipping_address_str = ','.join(
            order.shipping_address.active_address_fields())

        w_distance = 0
        for i, waypoint in enumerate(waypoints):
            w_address = waypoint.shipping_address
            wpoint_str = ','.join(w_address.active_address_fields())
            stop = waypoint.stops.all()
            stop = stop[0] if stop else ''
            w_distance += stop.distance if stop and stop.distance else 0
            _time = 0
            if shipping_address_str != wpoint_str:
                last_geopoint = waypoint.shipping_address.geopoint
            dist_ = waypoint.estimated_distance if waypoint.estimated_distance else 0
            dist_sum += dist_

        minutes = (dist_sum/AVERAGE_VEHICLE_SPEED_KMPH) * 60
        waypoint_minutes = (dist_/AVERAGE_VEHICLE_SPEED_KMPH) * 60
        total_dist_minutes = (dist_sum/AVERAGE_VEHICLE_SPEED_KMPH) * 60
        time_ = order.pickup_datetime + timedelta(minutes=total_dist_minutes)

        waypoint_hour = waypoint_minutes //60
        waypoint_minutes = waypoint_minutes % 60

        dropoff = {
            'name': shipping_address.first_name,
            'address': shipping_address.line1,
            'area': shipping_address.line3,
            'postal_code': shipping_address.postcode,
            "location": {
                "lat": dropoff_coordinates[1],
                "lng": dropoff_coordinates[0]
            },
            "time": utc_to_ist(time_).strftime(TIME_FORMAT),
            'status': dropoff_status,
            'wait_duration': '',
            'distance': dist_,
            'remaining_distance': dist_,
            'estimated_distance': dist_,
            'travel_time': '%d Hours %d Mins' % (waypoint_hour, waypoint_minutes),
            "contact": {
                "mobile": str(shipping_phone_number),
                "name": shipping_address.first_name if order else '',
                "email": order.customer.user.email if order else ''
            },
        }
        return dropoff

    def get_booking_details_for_user(self, request, order, route_info=False):
        now = timezone.now()
        status = ''

        trips = Trip.objects.filter(order=order).prefetch_related('stops')
        trips = trips.select_related('driver__driveractivity')
        trips = trips.prefetch_related('stops__waypoint')
        trips = trips.prefetch_related('stops__waypoint__shipping_address')
        trip = trips.all()[0] if trips else ''

        pickup_geopoint = order.pickup_address.geopoint
        pickup_hours = 0
        pickup_minutes = 0

        driver_geopoint = None
        if trip and trip.driver and trip.driver._driveractivity_cache:
            driver_geopoint = trip.driver.driveractivity.geopoint

            minutes = trip.stops.all(
            )[0].waypoint.estimated_distance if trip.stops.all()[0] else 0
            pickup_hours = minutes // 60 if minutes else 0
            pickup_minutes = minutes if minutes else 0
        customer_phone_number = order.customer.user.national_number if order else ''

        gps_distance =0

        if order.status == StatusPipeline.ORDER_NEW:
            status = APP_ORDER_STATUS_NEW
            user_message = "Your Blowhorn trip is Booked!"

        elif order.status == StatusPipeline.ORDER_DELIVERED:
            status = APP_ORDER_STATUS_COMPLETED
            user_message = "Your Blowhorn trip is completed!"
        else:
            status = APP_ORDER_STATUS_INPROGRESS
            user_message = "Your Blowhorn trip is in Progress!"

        stops, dropoff_address = self._get_stops_details(trip, order)

        rem_dist = 0
        pickup_dist = 0
        if trip and trip.driver and dropoff_address:
            if status != APP_ORDER_STATUS_COMPLETED and driver_geopoint:
                pickup_geopt = order.pickup_address.geopoint
                dropoff_geopt = order.shipping_address.geopoint
                rem_dist = dist_between_2_points(driver_geopoint, dropoff_geopt)
                pickup_dist = dist_between_2_points(driver_geopoint ,pickup_geopt)

        pickup = self._get_pickup_address(order, trip)

        dropoff = self._get_dropoff_address(order, trip)

        if trip and trip.total_time:
            total_time = trip.total_time
        else:
            total_time = sum([waypoint.estimated_time if waypoint.estimated_time else 0
                              for waypoint in order.waypoint_set.all()
                              ])

        minutes = total_time if total_time else 0
        hours = minutes // 60

        labour_count = 0
        labour_cost = 0
        fixed_amount = 0

        if hasattr(order, 'labour'):
            labour_cost = order.labour.labour_cost
            labour_count = order.labour.no_of_labours

        route_trace_list_dict = []
        if trip and trip.route_trace:
            route_trace_list_dict = [{'lat': x.x[1], 'lng': x.x[0]}
                                     for x in trip.route_trace]

        city = order.city if order else ''

        vehicle_class = order.vehicle_class_preference

        amount = 0
        time_amount = 0
        dist_amount = 0

        calculation = ''

        if order.invoice_details:
            calculation = json.loads(order.invoice_details)

            time_cal = calculation.get('time')
            if time_cal:
                for time in time_cal:
                    time_amount += time.get('amount')


            dist_cal = calculation.get('distance')
            if dist_cal:
                for dist in dist_cal:
                    dist_amount += dist.get('amount')

        fixed_amount = calculation.get('fixed',0.0) if calculation else 0.0
        labour_cost = calculation.get('labour',0.0) if calculation else 0.0

        total_price = order.total_incl_tax if order.total_incl_tax else 0

        components = {
            "distance": trip.gps_distance if trip and trip.gps_distance else 0.0,
            'fixed': float(fixed_amount),
            'labour': float(labour_cost),
            'time': float(time_amount),
            'distance': float(dist_amount),
        }

        if order.payment_status == 'Un-Paid':
            order.payment_status = 'unpaid'
        elif order.payment_status == 'Paid':
            order.payment_status = 'paid'

        if trip and trip.gps_distance:
            total_distance = trip.gps_distance
        else:
            total_distance = sum([waypoint.estimated_distance if waypoint.estimated_distance else 0.0
                                    for waypoint in order.waypoint_set.all()
                                    ])
        minutes = (total_distance / AVERAGE_VEHICLE_SPEED_KMPH) * 60
        hours = minutes // 60
        waypoints = sorted(order.waypoint_set.all(), key = lambda x: x.id)
        w_distance = 0.0
        dist_ = 0.0
        for waypoint in waypoints:
            stop = waypoint.stops.all()
            stop = stop[0] if stop else ''
            w_distance += stop.distance if stop and stop.distance else 0.0
            dist_ += waypoint.estimated_distance if waypoint.estimated_distance else 0.0

        minutes = (dist_/AVERAGE_VEHICLE_SPEED_KMPH) * 60

        total_hour = minutes //60
        total_minutes = minutes % 60
        total_time = utc_to_ist(order.pickup_datetime + timedelta(minutes=total_minutes)).strftime(TIME_FORMAT)

        waypoint_minutes = (w_distance/AVERAGE_VEHICLE_SPEED_KMPH) * 60

        route_trace = OrderUtil().get_prior_route(order=order)

        pickup_distance_done = geopoints_to_km([x.geopoint for x in route_trace])
        remaining_distance = 0.0

        if len(route_trace) > 0:
            remaining_distance = dist_between_2_points(
                route_trace[-1].geopoint, order.pickup_address.geopoint)
            remaining_distance = DISTANCE_MULTIPLIER * remaining_distance

        pickup_distance = pickup_distance_done + remaining_distance

        try:
            image_selected_url = vehicle_class.image_selected.url.split('/')
            image_selected_url = VEHICLE_ICON_PREFIX + image_selected_url[-1]
        except:
            # Display default vehicle images if images are not available
            vehicle = get_vehicle_info(DEFAULT_VEHICLE_CLASS)
            image_selected_url = vehicle.get('image_url_selected')


        dict_ = {
            'order_id': order.number,
            'order_real_id': order.number,
            'order_key': order.number,
            'status': status,
            'detail_status': status,
            "user_message": user_message,
            "last_modified": order.date_placed.date().strftime(DATE_FORMAT),
            "date_sortable": order.pickup_datetime.date().strftime(DATE_FORMAT),
            "date": order.pickup_datetime.date().strftime(DATE_FORMAT),
            "time": total_time,
            'header_from': order.pickup_address.line3,
            'header_to': order.shipping_address.line3,
            'city': order.pickup_address.line4,
            'items': order.items,
            'labour_count': labour_count,
            'labour_cph': int(labour_cost if labour_cost else 0),
            "truck_type": str(vehicle_class.commercial_classification) if vehicle_class else '',
            "tracking_url": get_tracking_url(request, order.number),
            'truck_url_detail': image_selected_url,
            'driver_name': order.driver.name if order.driver else '',
            'driver_mobile': str(order.driver.user.national_number) if order.driver else '',
            'driver_pic': '',
            'vehicle_number': trip.vehicle.registration_certificate_number if trip and trip.vehicle else '',
            'vehicle_model': trip.vehicle.vehicle_model.model_name if trip and trip.vehicle else '',
            'vehicle_class' : str(vehicle_class),
            'truck_url': image_selected_url,

            'pickup_distance': pickup_distance,
            'pickup_distance_done': pickup_distance_done,

            'pickup_time_duration': "%d Hours %d Mins" % (total_hour, total_minutes),
            'finished_distance': trip.gps_distance if trip and trip.gps_distance else dist_,
            "estimated_distance": float(order.estimated_distance_km),
            'remaining_distance': rem_dist,
            'total_duration': '%d Hours %d Mins' % (total_hour, total_minutes),
            "total_distance": dist_ ,

            'estimate': '-',
            'price': float(total_price),
            'fare_breakup': components,
            'pickup': pickup,
            'dropoff': dropoff,
            'stops': stops if stops else [],
            'payment_status': order.payment_status if order.payment_status else '',
            'route_info': route_trace_list_dict,
        }

        return dict_

    def get_truck_url(self, vehicle_class, attribute):
        """
        legacy app truck url is different so getting it from legacy constants
        """
        for vc in C2C_VEHICLE_CLASSES:
            if vc.get('value') == vehicle_class:
                return vc.get(attribute)
