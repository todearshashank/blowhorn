import datetime
import logging
from django.conf import settings

from config.settings import status_pipelines as StatusPipeline
from blowhorn.address.models import City
from blowhorn.address.utils import AddressUtil
from blowhorn.common.helper import CommonHelper
from blowhorn.driver.models import DriverRating, DriverActivity
from blowhorn.order.models import OrderConstants, Labour
from blowhorn.utils.functions import utc_to_ist

RPT_FORMAT_OTHER = '%d-%b-%Y %H:%M'
TIME_FORMAT = '%I:%M %p'
DATE_FORMAT = '%A, %d %B, %Y'
DATE_TIME_FORMAT = '%A, %d %B, %Y, %I:%M %p'
IST_OFFSET_HOURS = 5.5

LIGHT_LABOUR = 'light'
HEAVY_LABOUR = 'heavy'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class LegacyAdapter:
    # This function need to standardized

    def get_labour_category(self, labour_count):
        if labour_count in range(1, 2):
            return LIGHT_LABOUR
        else:
            return HEAVY_LABOUR

    def get_waypoint_dict(self, wpoint_address):
        full_address = ','.join(wpoint_address.active_address_fields())
        location = wpoint_address.geopoint
        phone_number = ''
        if wpoint_address.phone_number:
            phone_number = wpoint_address.phone_number.national_number

        return {
            'name': wpoint_address.first_name,
            'address': '%s' % full_address,
            'lat': location.y,
            'lon': location.x,
            'contact': {
                'name': wpoint_address.first_name,
                'mobile': phone_number,
            }
        }

    def get_booking_for_driver(self, order, driver, action='booking'):

        eta_pickup_address_s = 0
        customer = order.customer
        user = customer.user

        customer_rating = customer.customer_rating
        customer_star_rating = 0
        customer_star_rating_value = 0
        if customer_rating:
            customer_star_rating = customer_rating.cumulative_rating
            customer_star_rating_value = float(customer_star_rating)

        labour_count_desc = '--'
        labour_cat_desc = '--'
        labour_cost_desc = '--'

        # This need to addressed
        # r_dict = get_response_dict_for_booking_status(booking_db,
        #                                               booking_db.current_status)

        try:
            labour = order.labour
        except:
            labour = Labour.objects.filter(order=order).first()

        if labour:
            no_of_labours = labour.no_of_labours
            labour_cost = labour.labour_cost
            if order.status == StatusPipeline.ORDER_DELIVERED:
                # For completed bookings lets send the
                # numeric since the driver app will use it for calculation
                labour_count_desc = no_of_labours
                labour_cost_desc = labour_cost
            else:
                if no_of_labours == 0:
                    labour_count_desc = '--'
                    labour_cat_desc = '--'
                elif no_of_labours == 1:
                    labour_cost_desc = '%d x %d' % (
                        labour_cost, no_of_labours)
                    labour_count_desc = 'Driver Only'
                else:
                    labour_cost_desc = '%d x %d' % (
                        labour_cost, no_of_labours)
                    labour_count_desc = 'Driver + %d' % (
                        no_of_labours - 1)

        trip_cost = float(order.total_incl_tax)
        total_fare = float(order.total_incl_tax)

        extras = []
        pickup_address = order.pickup_address
        shipping_address = order.shipping_address
        if pickup_address.phone_number:
            extras.append('Pickup : %s' %
                          pickup_address.phone_number.national_number)
        if shipping_address.phone_number:
            extras.append('Dropoff : %s' %
                          shipping_address.phone_number.national_number)
        extra_info = '; '.join(extras)

        pickup_address_str = ','.join(
            order.pickup_address.active_address_fields())
        shipping_address_str = ','.join(
            order.shipping_address.active_address_fields())

        waypoint_qs = sorted(order.waypoint_set.exclude(
            sequence_id=None), key=lambda x: x.sequence_id)
        waypoints = []
        for wpoint in waypoint_qs:
            wpoint_address = wpoint.shipping_address
            wpoint_address_str = ','.join(
                wpoint_address.active_address_fields())
            if wpoint_address_str != shipping_address_str:
                wpoint_dict = self.get_waypoint_dict(wpoint_address)
                waypoints.append(wpoint_dict)

        dict_ = {
            'value_json': {
                'type': 'booking',
                'action': action,
                'username': driver.id,
                ################################################
                # Including booking id and booking key
                # Key has been passed to ID and needs migration
                # Don't remove below 3 parameters
                'booking_id': order.number,
                'booking_key': order.number,
                'booking_id_int': order.id,
                ################################################
                'waypoints': waypoints,
                'waypoint_count': order.waypoint_set.count(),
                'event': order.status,
                'trip_status': order.status,
                'pickup_address': '%s' % pickup_address_str,
                'pickup_landmark': pickup_address_str,
                'pickup_date': str(
                    utc_to_ist(order.pickup_datetime).strftime(
                        "%d-%b-%Y")),
                'pickup_time': str(
                    utc_to_ist(order.pickup_datetime).strftime(
                        "%H:%M %p")),
                'dropoff_address': '%s' % shipping_address_str,
                'dropoff_landmark': shipping_address_str,
                'cost': float(order.estimated_cost),
                'trip_cost_valid': True if order.total_incl_tax else False,
                'trip_cost': trip_cost,
                'total_fare': total_fare,
                'labour_count': labour_count_desc,
                'labour_cph': labour_cost_desc,
                'labour_cat': labour_cat_desc,
                'labour_count_desc': labour_count_desc,
                'labour_cph_desc': labour_cost_desc,
                'labour_cat_desc': labour_cat_desc,
                'eta_pickup_address_min': int(eta_pickup_address_s) / 60,
                'response_timeout_seconds': settings.MAX_DRIVER_RESPONSE_TIME_SECONDS,
                'pickup_lat': pickup_address.geopoint.y if pickup_address.geopoint else '',
                'pickup_lon': pickup_address.geopoint.x if pickup_address.geopoint else '',
                'dropoff_lat': shipping_address.geopoint.y if shipping_address.geopoint else '',
                'dropoff_lon': shipping_address.geopoint.x if shipping_address.geopoint else '',
                'items': order.items,
                'customer_name': user.name,
                'customer_mobile': user.national_number,
                'customer_email': user.email,
                'customer_star_rating': '%.1f stars' % customer_star_rating,
                'customer_star_rating_value': customer_star_rating_value,
                'driver_cancellable': False,
                'payment_status': order.payment_status,
                'extra_info': extra_info,
            }
        }
        return dict_

    def get_booking_for_customer(self, order, trip, route_info=False):
        data = {}

        data['type'] = order.order_type
        data['booking_key'] = order.number
        data['booking_id'] = order.number
        data['friendly_id'] = order.number
        data['message'] = 'Your {} booking has been confirmed'.format(settings.BRAND_NAME)
        data['display_address'] = str(order.shipping_address)
        data['booking_date'] = str(LegacyAdapter.utc_to_ist(
            order.date_placed).strftime(DATE_FORMAT))
        data['requested_date_time'] = str(LegacyAdapter.utc_to_ist(
            order.pickup_datetime).strftime('%d-%b-%Y %I:%M %p'))
        data['booking_time'] = str(LegacyAdapter.utc_to_ist(
            order.date_placed).strftime(TIME_FORMAT))

        data['customer_email'] = order.user.email if order.user else order.guest_email

        try:
            # Try putting the real distance here.
            data['distance'] = '%.1f' % 2
        except:
            data['distance'] = ''

        try:
            time_ = LegacyAdapter.utc_to_ist(trip.actual_start_time)
            data['start_time'] = time_.strftime(TIME_FORMAT)
            data['start_date'] = time_.strftime(DATE_FORMAT)
        except:
            data['start_time'] = ''
            data['start_date'] = ''

        try:
            time_ = LegacyAdapter.utc_to_ist(order.pickup_datetime)
            data['pickup_time_or_rpt'] = time_.strftime(TIME_FORMAT)
            data['pickup_date_or_rpt'] = time_.strftime(DATE_FORMAT)
            data['pickup_date_time'] = time_.strftime(DATE_TIME_FORMAT)
        except:
            data['pickup_time_or_rpt'] = ''
            data['pickup_date_or_rpt'] = ''
            data['pickup_date_time'] = ''

        try:
            time_ = LegacyAdapter.utc_to_ist(trip.actual_start_time)
            data['pickup_time'] = time_.strftime(TIME_FORMAT)
            data['pickup_date'] = time_.strftime(DATE_FORMAT)
        except:
            data['pickup_time'] = ''
            data['pickup_date'] = ''

        try:
            droptime_ = LegacyAdapter.utc_to_ist(trip.actual_end_time)
            data['dropoff_time'] = droptime_.strftime(TIME_FORMAT)
            data['dropoff_date'] = droptime_.strftime(DATE_FORMAT)
        except:
            data['dropoff_time'] = ''
            data['dropoff_date'] = ''

        data['time_booking'] = str(LegacyAdapter.utc_to_ist(
            order.pickup_datetime).strftime('%d-%b-%Y %H:%M'))
        if order.status == StatusPipeline.ORDER_NEW:
            current_status = 'booking_accepted'
        elif order.status == StatusPipeline.ORDER_DELIVERED:
            current_status = 'completed'
        else:
            current_status = order.status
        data['current_status'] = current_status
        if route_info:
            if order.status == 'ORDER_COMPLETED':
                data['route_info'] = _route_info_to_dataarray(db.route_info)
            else:
                data['route_info'] = get_prior_route(
                    order=db.key, start_time=db.get_event_time('arrived_at_pickup'))
        else:
            data['route_info'] = []
        data['comment'] = str(
            order.notes.first().message) if order.notes.first() is not None else None

        driver = trip.driver if trip else None
        vehicle = trip.vehicle if trip else None
        if driver:
            phonenumber = AddressUtil().get_national_number_from_user_phone(driver.user)
            data['driver_name'] = driver.user.name
            data['driver_id'] = driver.id
            data['driver_phone'] = phonenumber
            data['driver_picture'] = ''  # trip.driver.photo
            driver_location = DriverActivity._default_manager.filter(
                driver_id=driver.id).first()
            lat = driver_location.geopoint.y if driver_location else ''
            lon = driver_location.geopoint.x if driver_location else ''
            data['driver_geopt'] = {'lat': lat, 'lon': lon}
            driverrating = DriverRating._default_manager.filter(
                id=driver.id).first()
            if driverrating:
                rating = int(driverrating.cumulative_rating)
                data['rating'] = str(rating)
            else:
                data['rating'] = str(0)
        else:
            phonenumber = ''
            data['driver_id'] = ''
            data['driver_name'] = ''
            data['driver_phone'] = ''
            data['driver_picture'] = ''
            data['driver_geopt'] = {}
            data['rating'] = str(0)
        if vehicle:
            data['driver_vehicle_model'] = vehicle.vehicle_model.model_name
            data['driver_vehicle_number'] = vehicle.registration_certificate_number
        else:
            data['driver_vehicle_model'] = ''
            data['driver_vehicle_number'] = ''

        data['pickup_area'] = str(order.pickup_address.line2)
        data['dropoff_area'] = str(order.shipping_address.line2)
        data['pickup_city'] = order.pickup_address.line4
        data['dropoff_city'] = order.shipping_address.line4
        data['pickup_address'] = str(order.pickup_address)
        data['dropoff_address'] = str(order.shipping_address)

        pickup_coordinates = order.pickup_address.geopoint.coords if order.pickup_address.geopoint else None
        shipping_coordinates = order.shipping_address.geopoint.coords if order.shipping_address.geopoint else None
        data['pickup_geopt'] = {'lat': pickup_coordinates[
            1], 'lon': pickup_coordinates[0]} if pickup_coordinates else None
        data['dropoff_geopt'] = {'lat': shipping_coordinates[
            1], 'lon': shipping_coordinates[0]} if shipping_coordinates else None
        # [CommonHelper.get_public_info_from_model_obj(waypoint.shipping_address) for waypoint in order.waypoints_set.all()]

        shipping_address_str = ','.join(
            order.shipping_address.active_address_fields())
        waypoint_qs = order.waypoint_set.all()
        waypoints = []
        for wpoint in waypoint_qs:
            wpoint_address = wpoint.shipping_address
            wpoint_address_str = ','.join(
                wpoint_address.active_address_fields())
            if wpoint_address_str != shipping_address_str:
                wpoint_dict = self.get_waypoint_dict(wpoint_address)
                waypoints.append(wpoint_dict)

        data['waypoints'] = waypoints
        # CommonHelper.get_public_info_from_model_obj(order.waypoints_set.first())
        data['current_waypoint'] = []
        data['items_moved'] = order.items if order.items else []

        labour = Labour._default_manager.filter(order_id=order.id).first()
        data['labour_requested'] = "Yes" if labour else "No"
        if order.status == StatusPipeline.ORDER_DELIVERED:
            # @todo rates should be fetched from rate card not from db
            # discuss with Nikhil
            data['min_fare'] = float(
                OrderConstants().get('C2C_BASE_PRICE', 200))
            data['other_charges'] = float(
                labour.no_of_labours * labour.labour_cost if labour else 0)
            data['extra_charges'] = float(
                OrderConstants().get('TRANSACTION_FEE_PERCENT', 1))
            data['sub_total'] = float(order.total_excl_tax)
            data['total'] = int(data['sub_total'] +
                                data['extra_charges'] + data['other_charges'])
            data['discount'] = ''
            data['cost_components'] = []

            data['payment_status_user'] = order.payment_status
            data['payment_status_remote'] = order.payment_status

        else:
            data['min_fare'] = ''
            data['other_charges'] = ''
            data['extra_charges'] = ''
            data['sub_total'] = ''
            data['total'] = ''
            data['discount'] = ''
            data['payment_status_user'] = ''
            data['payment_status_remote'] = ''
            data['cost_components'] = []

        data['payment_mode'] = ''

        data['cost'] = data['total']
        try:
            actual_start_time = LegacyAdapter.utc_to_ist(
                trip.actual_start_time)
            actual_end_time = LegacyAdapter.utc_to_ist(trip.actual_end_time)
            trip = actual_end_time - actual_start_time
            seconds = trip.total_seconds()
            hours = seconds // 3600
            minutes = (seconds - (hours * 3600)) // 60
            data['trip_duration'] = '%d Hours %d Mins' % (hours, minutes)
        except:
            data['trip_duration'] = ''

        return data

    def utc_to_ist(date):
        return date + datetime.timedelta(hours=IST_OFFSET_HOURS)

    def ist_to_utc(date):
        return date - datetime.timedelta(hours=IST_OFFSET_HOURS)

    def get_city_from_latlong(latitude, longitude):
        geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)
        return LegacyAdapter.get_city_from_geopoint(geopoint)

    def get_city_from_geopoint(geopoint):
        city = City._default_manager.filter(coverage_dropoff__contains=geopoint)
        return city.first()
