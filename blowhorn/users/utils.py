# System and Django
import re
import logging
import gzip
from django.conf import settings
from urllib.parse import urlparse, parse_qs
from pathlib import Path
from difflib import SequenceMatcher
from django.core.exceptions import FieldDoesNotExist
from blowhorn.users.constants import DEFAULT_USER_ATTRIBUTES, MAX_SIMILARITY, DEFAULT_PASSWORD_LIST_PATH

from phonenumber_field.phonenumber import PhoneNumber

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class UserUtils(object):

    def get_query_param_value(self, url, param_name):
        parsed_url = urlparse(url)
        url_params = parse_qs(parsed_url.query)
        url_params = url_params.get(param_name, None)
        if url_params:
            return url_params[0]
        return False

    def is_staff_email(self, email):
        if not email:
            return False
        domain = re.search("@[\w.]+", email)
        return domain and domain.group()[1:] in settings.DOMAIN_WHITELIST

    def get_user_from_phone_number(self, phone_number):
        """
        :param phone_number: String 10-digit phone-number
        :return: instance of Customer
        """
        from blowhorn.users.models import User
        from blowhorn.customer.models import Customer

        if settings.ACTIVE_COUNTRY_CODE not in phone_number:
            phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE,
                                       phone_number)
        customer = Customer.objects.filter(user__phone_number=phone_number).first()
        user = customer.user if customer else None
        # user = User.objects.filter(
        #     phone_number=phone_number,
        #     customer__isnull=False,
        #     # last_otp_sent_at__isnull=False
        # ).order_by('-last_otp_sent_at').first()

        if not user:
            email = '%s@%s' % (phone_number,
                               settings.DEFAULT_DOMAINS.get('customer'))
            try:
                user = User.objects.get(email=email)
            except BaseException as e:
                logger.info('User not found for phone number: %s' % phone_number)
                return False

        return user

def get_partner_user_from_phone_number(phone_number, is_driver, is_vendor):
    """
    :param phone_number: String 10-digit phone-number
    :return: user instance for vendor or driver
    """
    from blowhorn.users.models import User
    domain = None
    user = None

    if is_driver:
        domain = settings.DEFAULT_DOMAINS.get('driver')
    elif is_vendor:
        domain = settings.DEFAULT_DOMAINS.get('vendor')
    else:
        return user

    checklist = ['%s@%s' % (phone_number,
                       settings.DEFAULT_DOMAINS.get('driver')),
                '%s@%s' % (phone_number,
                       settings.DEFAULT_DOMAINS.get('vendor')),
                ]

    user_list = User.objects.filter(email__in = checklist)

    if user_list.exists():
        users = [u for u in user_list if u.email.find(domain) >= 0]

        if len(users) != 0:
            user = users[0]

    return user


def user_attribute_similarity(password, user):

    user.user_attributes = DEFAULT_USER_ATTRIBUTES
    user.max_similarity = MAX_SIMILARITY

    for attribute_name in DEFAULT_USER_ATTRIBUTES:
        value = getattr(user, attribute_name, None)
        if not value or not isinstance(value, str):
            continue
        value_parts = re.split(r'\W+', value) + [value]
        for value_part in value_parts:
            if SequenceMatcher(a=password.lower(), b=value_part.lower()).quick_ratio() >= MAX_SIMILARITY:
                try:
                    verbose_name = str(user._meta.get_field(attribute_name).verbose_name)
                except FieldDoesNotExist:
                    verbose_name = attribute_name
                return 'The password is too similar to the %s.\n' % verbose_name

    return ''


def common_password_validator(password):

    try:
        with gzip.open(DEFAULT_PASSWORD_LIST_PATH, 'rt', encoding='utf-8') as f:
            common_passwords = {x.strip() for x in f}
    except OSError:
        with open(DEFAULT_PASSWORD_LIST_PATH) as f:
            common_passwords = {x.strip() for x in f}
    if password.lower().strip() in common_passwords:
        return 'This password is too common.\n'
    return ''


def validate_customer_password(password1, password2=None, oldpassword=None, user=None):

    error_msg = ''

    if user and oldpassword and not user.check_password(oldpassword):
        return False, 'Current password is incorrect'

    if password2 and password1 != password2:
        error_msg += 'The two password fields do not match.\n'
    if len(password1) < 8 or len(password1) > 15:
        error_msg += 'Password must contain 8-15 characters.\n'

    # Password should not be similar to name, email
    error_msg += user_attribute_similarity(password1, user) if user else ''

    # Password should not be commonly used
    error_msg += common_password_validator(password1)

    error_msg += 'This password is entirely numeric.' if password1.isdigit() else ''

    if error_msg:
        return False, error_msg
    return True, 'Password is valid'


