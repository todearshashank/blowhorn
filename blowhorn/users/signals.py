import json
from blowhorn.users.messages import STAFF_MESSAGES
from blowhorn.users.models import User
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from blowhorn.users.tasks import send_notification_mail

@receiver(post_save, sender=User)
def send_notification_email(sender, instance, created, **kwargs):
    """Send notification on addition of staff user.
    """
    message = ''
    if created and instance.is_staff:
        message = STAFF_MESSAGES['create']
    elif not instance.old_staff_status and instance.is_staff:
        message = STAFF_MESSAGES['add']
    elif instance.old_staff_status and not instance.is_staff:
        message = STAFF_MESSAGES['remove']

    if message:
        message['email'] = instance.email
        print('Sending notification to %s' % instance.email)
        # send_notification_mail.delay(json.dumps(message))
