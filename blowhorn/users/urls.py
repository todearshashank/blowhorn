from django.conf.urls import url

from .views import SupplyUserLogin

urlpatterns = [
    url(r'^login', SupplyUserLogin.as_view(), name='supply-user-login'),
]
