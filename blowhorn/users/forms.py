import django
import re
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from phonenumber_field.phonenumber import PhoneNumber
from django.conf import settings
from blowhorn.driver import messages
from blowhorn.users.utils import UserUtils
from blowhorn.utils.functions import validate_phone_number


class UserCreationForm(forms.ModelForm):

    """
    A form for creating new users.
    Includes all the required fields, plus a repeated password.
    """

    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'password_mismatch': _("The two password fields didn't match."),
        'mandatory_mobile':
            _('Mobile Number is mandatory for creating a Staff User')
    }
    email = forms.EmailField(label=_("Email Address"), max_length=254)
    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))
    # phone_number = forms.CharField(validators=[validate_phone_number],
    #     required=False, max_length=10, label='Mobile Number',
    #     help_text=_('Mobile Number is mandatory for creating a Staff User'))

    class Meta:
        model = get_user_model()
        fields = ('email', 'phone_number')
        help_texts = {
            'phone_number': _(
                "Customer's primary mobile number e.g. %s{10 digit mobile number}" % settings.ACTIVE_COUNTRY_CODE)
        }

    def clean_email(self):
        """
        Clean form email.
        :return str email: cleaned email
        :raise forms.ValidationError: Email is duplicated
        """
        # Since User.email is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        email = self.cleaned_data["email"]
        user = get_user_model()._default_manager.filter(
            email__iexact=email)

        if user.exists():
            raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )
        return email

    def clean_password2(self):
        """
        Check that the two password entries match.
        :return str password2: cleaned password2
        :raise forms.ValidationError: password2 != password1
        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get('phone_number')
        email = self.cleaned_data.get('email')
        if UserUtils().is_staff_email(email) and not phone_number:
            raise forms.ValidationError(
                self.error_messages['mandatory_mobile'],
                                       code='mandatory_mobile')

        # if phone_number:
        #     phone_number = PhoneNumber(settings.DEFAULT_COUNTRY.get(
        #         'isd_code'), phone_number)
        return phone_number

    def save(self, commit=True):
        """
        Save user.
        Save the provided password in hashed format.
        :return users.models.User: user
        """
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):

    """
    A form for updating users.
    Includes all the fields on the user, but replaces the password field
    with admin's password hash display field.
    """

    # In Django 1.9 the url for changing the password was changed (#15779)
    # A url name was also added in 1.9 (same issue #15779),
    # so reversing the url is not possible for Django < 1.9
    password = ReadOnlyPasswordHashField(label=_("Password"), help_text=_(
        "Raw passwords are not stored, so there is no way to see "
        "this user's password, but you can change the password "
        "using <a href=\"%(url)s\">this form</a>."
    ) % {'url': 'password/' if django.VERSION < (1, 9) else '../password/'})
    # phone_number = forms.CharField(validators=[validate_phone_number],
    #     required=False, max_length=10, label='Mobile Number',
    #     help_text=_('Mobile Number is mandatory for creating a Staff User'))

    class Meta:
        model = get_user_model()
        exclude = ()
        help_texts = {
            'phone_number': _(
                "Customer's primary mobile number e.g. %s{10 digit mobile number}" % settings.ACTIVE_COUNTRY_CODE)
        }

    def __init__(self, *args, **kwargs):
        """Init the form."""

        # overriding the phone number field to hide national code
        # phone_number_obj = kwargs['instance'].phone_number
        email = kwargs['instance'].email
        # if phone_number_obj:
        #     phone_number = phone_number_obj.__dict__.get('national_number')
        #     if phone_number:
        #         initial = kwargs.get('initial', {})
        #         initial['phone_number'] = str(phone_number).zfill(10)
        #         kwargs['initial'] = initial
        super(UserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

        # Regular Expression to check if the email ends with
        # '@driver.blowhorn.net'
        if re.search(r'@driver.blowhorn.net$', email):
            self.fields['email'].widget.attrs['readonly'] = True

    def clean(self):
        phone_number = self.cleaned_data.get('phone_number')
        email = self.cleaned_data.get('email')
        is_staff = self.cleaned_data.get('is_staff')

        if re.search(r'@driver.blowhorn.net$', str(email)):
            user = get_user_model().objects.filter(
                phone_number=phone_number).exclude(
                id=self.instance.id)

            if user.exists():
                # If any user exists with the current phone number after
                # excluding the driver then raise the error
                raise ValidationError({
                    'phone_number': (messages.MOBILE_NUMBER_ALREADY_REGISTERED)
                })

            driver_phone_number = re.findall(r'\d{10}$', str(phone_number))

            if driver_phone_number:
                email = driver_phone_number[0] + '@driver.blowhorn.net'
                self.cleaned_data['email'] = email

        user = get_user_model()._default_manager.filter(
            email__iexact=email).exclude(pk=self.instance.id)

        if user.exists():
            raise ValidationError({
                'email': ("A user with that email already exists.")
            })

        if is_staff:
            if not phone_number:
                raise ValidationError(
                    _('Provide Mobile Number for a Staff User'))

        if self.initial.get('is_staff') \
            and not self.cleaned_data.get('is_staff'):
            raise ValidationError(
                _('Changing `Staff Status` is not allowed here. Deactivate the user from action in list view.')
            )

        if self.cleaned_data.get('is_staff') and \
            self.initial.get('is_active') \
            and not self.cleaned_data.get('is_active'):
            raise ValidationError(
                _('Deactivation is not allowed here. Use action from list view.')
            )

        return self.cleaned_data

    def clean_password(self):
        """
        Clean password.
        Regardless of what the user provides, return the initial value.
        This is done here, rather than on the field, because the
        field does not have access to the initial value.
        :return str password:
        """
        return self.initial["password"]

    # def clean_phone_number(self):
    #     phone_number = self.cleaned_data.get('phone_number')
    #     if phone_number:
    #         phone_number = PhoneNumber(
    #             settings.DEFAULT_COUNTRY.get('isd_code'), phone_number)
    #     return phone_number
