from rest_framework import permissions
from blowhorn.users.models import UserSession


class IsBlowhornStaff(permissions.BasePermission):
    """
    Permission to allow access for Blowhorn Staff only.
    """
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated \
                and request.user.is_blowhorn_staff()


class IsActive(permissions.BasePermission):
    """
    Permission to allow access for Blowhorn Staff only.
    """

    def has_permission(self, request, view):
        return request.user.is_active_user() if request.user and not request.user.is_anonymous else False


class IsMobileVerified(permissions.BasePermission):
    """
    Permission to allow access for Blowhorn customer app.
    """

    def has_permission(self, request, view):
        return request.user.is_mobile_verified


class HasValidSession(permissions.BasePermission):
    """
    Permission to allow access for Blowhorn Staff only.
    """

    def has_permission(self, request, view):
        return UserSession.objects.is_valid_session(request)
