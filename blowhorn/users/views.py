# Django and System libraries
import json
import logging

# 3rd party libraries
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework import views, status
from rest_framework.authentication import BasicAuthentication
from blowhorn.oscar.core.loading import get_model

# blowhorn imports
from config.settings import base as base_settings
from blowhorn.apps.customer_app.v1.social_profile import GoogleProfile
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.vehicle import utils as vehicle_util
from blowhorn.driver import constants as DriverConst
from blowhorn.address import utils as address_utils
from blowhorn.document.serializers import DocumentTypeSerializer
from blowhorn.document.constants import DRIVER_DOCUMENT, \
    VEHICLE_DOCUMENT, DOCUMENT_STATUSES


User = get_model('users', 'User')
DocumentType = get_model('document', 'DocumentType')


def google_login(request):
    """
    Method for handling Google Auth based authentication
    """
    result = {}
    logging.info('Request from the app is: %s' % request)
    google_token = str(request.META.get('HTTP_TOKEN', '')).strip()

    if not google_token:
        result['status_code'] = status.HTTP_400_BAD_REQUEST
        result['error_msg'] = 'Authentication token was not found'
        return result

    id_info = GoogleProfile().get(
        token=google_token,
        allowed_domains=base_settings.DOMAIN_WHITELIST
    )
    if not id_info:
        logging.info('Failed due to incorrect domain')
        result['status_code'] = status.HTTP_400_BAD_REQUEST
        result['error_msg'] = 'Please sign in with blowhorn account'
        return result

    user_email = id_info['email']
    user_name = id_info['name']

    logging.info('User name: %s' % user_name)
    logging.info('User e-mail: %s' % user_email)

    user_qs = User.objects.filter(email__startswith=user_email[:-4])
    if not user_qs:
        logging.info('User e-mail: %s does not exist' % user_email)
        # TODO: Status code need to be changed for the next app release
        result['status_code'] = status.HTTP_401_UNAUTHORIZED
        result['error_msg'] = 'User does not exist'
        return result

    user = user_qs.filter(is_staff=True).first()

    # To verify if the obtained user has the staff permission or not
    if not user:
        logging.info(
            'User e-mail: %s is not authorised for supply login' % user_email)
        result['status_code'] = status.HTTP_401_UNAUTHORIZED
        result['error_msg'] = 'Unauthorised User'
        return result

    logging.info('User is %s' % user)
    login_response = User.objects.do_login(user=user, request=request)
    logging.info('login response is %s' % login_response)

    if login_response.status_code != status.HTTP_200_OK:
        result['status_code'] = status.HTTP_400_BAD_REQUEST
        result['error_msg'] = 'Authentication Failure'
        return result

    token = login_response.data.get('token')
    logging.info('token is %s' % token)

    if not token:
        logging.error('Authorisation token could not be generated')
        result['status_code'] = status.HTTP_400_BAD_REQUEST
        result['error_msg'] = 'Authentication token generation failure'
        return result

    result = {
        'token': token,
        'user_name': user_name,
        'user_email': user_email,
        'status_code': status.HTTP_200_OK,
        'user_object': user
    }
    logging.info('Successfully authenticated user: %s : %s' %
                 (user_name, user_email))
    return result


@permission_classes([])
class SupplyUserLogin(views.APIView):
    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def post(self, request):
        """
        :param request: Have the google authentication token
        :return: If token is valid and the user is present with the
                staff permission then return email, name, vehicle_data
        """
        result = {}
        logging.info('User is authenticating Google token from supply app')

        body = request.data
        location_details = json.loads(body.get('location_details'))

        logging.info('App Version: %s' % body.get('app_version', ''))

        logged_in_user = google_login(request)

        if logged_in_user.get('status_code', '') != status.HTTP_200_OK:
            return Response(status=logged_in_user['status_code'],
                            data=logged_in_user.get('error_msg'))

        vehicle_model_list = vehicle_util.get_vehicle_models()

        operating_cities = address_utils.get_operating_cities(location_details)

        driver_document_type = DocumentType.objects.filter(
            identifier=DRIVER_DOCUMENT)
        vehicle_document_type = DocumentType.objects.filter(
            identifier=VEHICLE_DOCUMENT)

        serialized_driver_doc_type_data = DocumentTypeSerializer(
            driver_document_type, many=True)
        serialized_vehicle_doc_type_data = DocumentTypeSerializer(
            vehicle_document_type, many=True)
        driver_document_type = serialized_driver_doc_type_data.data \
            if serialized_driver_doc_type_data.data else []
        vehicle_document_type = serialized_vehicle_doc_type_data.data \
            if serialized_vehicle_doc_type_data.data else []

        result = {
            'driver_document_list': driver_document_type,
            'vehicle_document_list': vehicle_document_type,
            'document_statuses': DOCUMENT_STATUSES,
            'auth_token': logged_in_user.get('token'),
            'name': logged_in_user.get('user_name'),
            'email': logged_in_user.get('user_email'),
            'vehicle_model': vehicle_model_list if vehicle_model_list else [],
            'working_preferences': DriverConst.WORKING_PREFERENCES_MAP,
            'operating_city_name': operating_cities if operating_cities else []
        }

        return Response(status=status.HTTP_200_OK, data=result,
                        content_type='text/html; charset=utf-8')
