import json
import logging
from datetime import timedelta
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.middleware import current_request
from blowhorn.common.models import BaseModel
from blowhorn.users.constants import NOTIFICATION_CATEGORIES, \
    NOTIFICATION_STATUSES, NOTIFICATION_STATUS_ACTIVE, FYA, FYI, \
    NOTIFICATION_EXPIRY_TIME_SECONDS
from blowhorn.users.managers import NotificationMessageTemplateManager
from blowhorn.users.models import User

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class AppNotification(BaseModel):
    notification_group = models.CharField(_('Notification Group'),
                                          max_length=125)

    message = models.TextField(help_text=_('Body of notification'))

    expiry_time = models.DateTimeField(_('Expiry Time'), null=True, blank=True,
                                       help_text=_('Expiry Time is applicable '
                                                   'to FYI notifications only'))

    source = models.ForeignKey(User, on_delete=models.PROTECT,
                               help_text=_('User who initiated'))

    action_owners = models.ManyToManyField(User, related_name='action_owners')

    broadcast_list = models.ManyToManyField(User, related_name='broadcast_list')

    status = models.CharField(max_length=125, choices=NOTIFICATION_STATUSES,
                              default=NOTIFICATION_STATUS_ACTIVE,
                              help_text=_('FYI: automatically expires after '
                                          'certain duration, FYA: expires once '
                                          'action has been taken'))

    extra_context = JSONField(help_text=_('Additional information'),
                              null=True, blank=True)

    class Meta:
        verbose_name = _('App Notification')
        verbose_name_plural = _('App Notifications')

    def __str__(self):
        return '%s | %s' % (self.notification_group, self.source)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.expiry_time = timezone.now() + timedelta(
                seconds=NOTIFICATION_EXPIRY_TIME_SECONDS)

        super(AppNotification, self).save(*args, **kwargs)


class NotificationMessageTemplate(BaseModel):

    category = models.CharField(_('Notification Category'), max_length=10,
                                choices=NOTIFICATION_CATEGORIES)

    notification_group = models.CharField(_('Notification Group'),
                                          max_length=125)

    action = models.CharField(max_length=125, help_text=_(
        'Actions like accepted, rejected, requested'), default='NA')

    title = models.TextField(help_text=_('Title'),null=True, blank=True)

    template = models.TextField(
        help_text=_('Template for notification content. It can contain valid '
                    'placeholders.'))

    is_active = models.BooleanField(default=False)

    objects = NotificationMessageTemplateManager()

    class Meta:
        verbose_name = _('Notification Message Template')
        verbose_name_plural = _('Notification Message Templates')
        unique_together = ('category', 'notification_group')

    def _capture_changes(self):
        _request = current_request()
        current_user = _request.user if _request else ''
        change_log = []

        old = self.__class__.objects.get(pk=self._get_pk_val())
        for field in self.__class__._meta.fields:
            field_name = field.__dict__.get('name')
            old_value = field.value_from_object(old)
            current_value = field.value_from_object(self)
            if old_value != current_value:
                changes = {
                    'field': '%s' % (field.__dict__.get(
                        'verbose_name') or field_name),
                    'old': '%s' % old_value,
                    'new': '%s' % current_value or '--EMPTY--'
                }
                change_log.append(changes)

        if bool(change_log):
            history = {
                'message_template': self,
                'log': json.dumps(change_log),
                'modified_by': current_user,
                'modified_date': timezone.now()
            }
            NotificationMessageTemplateChangeLog.objects.create(**history)

    def __str__(self):
        return '%s | %s' % (self.category, self.notification_group)

    def save(self, *args, **kwargs):
        if self.pk:
            self._capture_changes()
            self.action = self.action.lower()

        super().save(*args, **kwargs)


class NotificationMessageTemplateChangeLog(BaseModel):
    message_template = models.ForeignKey(NotificationMessageTemplate,
                                         on_delete=models.PROTECT)
    log = models.TextField(_('Log'))

    class Meta:
        verbose_name = _('Notification Message Template Change Log')
        verbose_name_plural = _('Notification Message Template Change Log')

    def __str__(self):
        return '%s' % self.message_template
