from django.utils.translation import ugettext_lazy as _

AUDITLOG_FIELDS = ('created_date', 'created_by', 'modified_date', 'modified_by')

GOOGLE_ISSUER_LIST = ['accounts.google.com', 'https://accounts.google.com']
DOT_NET_DOMAIN = 'blowhorn.net'
LOGIN_VALIDATION_MESSAGES = {
    'email_required': 'Email is mandatory',
    'invalid_credentials': 'Incorrect email or password',
    'activation_pending': 'Account activation pending',
    'invalid_password': 'Incorrect password entered. Reset the password if you forgot your password.',
    'invalid_otp': 'Invalid OTP. Please request again for OTP',

}

# EMAIL HISTORY
OPEN = 'open'
PROCESSED = 'processed'
DROPPED = 'dropped'
DELIVERED = 'delivered'
DEFERRED = 'deferred'
BOUNCE = 'bounce'
SPAM_REPORT = 'spamreport'
UNSUBSCRIBE = 'unsubscribe'
GROUP_UNSUBSCRIBE = 'group_unsubscribe'
GROUP_RESUBSCRIBE = 'group_resubscribe'
ALLOWED_EMAIL_EVENTS = [OPEN, PROCESSED, DEFERRED, DROPPED, DELIVERED, BOUNCE,
                        SPAM_REPORT, UNSUBSCRIBE, GROUP_RESUBSCRIBE,
                        GROUP_UNSUBSCRIBE]
COMMON_FIELDS = ['email', 'timestamp', 'event', 'smtp_id', 'category',
                 'useragent', 'attempt']
EMAIL_EVENTS = (
    (PROCESSED, _('Processed')),
    (DELIVERED, _('Delivered')),
    (OPEN, _('Open')),
    (DROPPED, _('Dropped')),
    (DEFERRED, _('Deferred')),
    (BOUNCE, _('Bounce')),
    (SPAM_REPORT, _('Spam Report')),
    (UNSUBSCRIBE, _('Unsubscribe')),
    (GROUP_UNSUBSCRIBE, _('Group Unsubscribe')),
    (GROUP_RESUBSCRIBE, _('Group Resubscribe')),
)
NOTIFY_EVENTS = [DEFERRED, DROPPED, BOUNCE, SPAM_REPORT, UNSUBSCRIBE]

# Notification
FYI = 'FYI'
FYA = 'FYA'
NOTIFICATION_CATEGORIES = (
    (FYI, _('For Your Information')),
    (FYA, _('For Your Action')),
)

NOTIFICATION_STATUS_ACTIVE = 'active'
NOTIFICATION_STATUS_EXPIRED = 'expired'
NOTIFICATION_STATUSES = (
    (NOTIFICATION_STATUS_ACTIVE, _('Active')),
    (NOTIFICATION_STATUS_EXPIRED, _('Expired'))
)

NOTIFICATION_GROUP_ORDER = 'order'
NOTIFICATION_GROUP_EXTERNAL_VERIFICATION = 'document_external_verification'
NOTIFICATION_GROUP_INTERNAL_VERIFICATION = 'internal_verification'
NOTIFICATION_GROUP_DRIVER_PAYMENT = 'driver_payment'
NOTIFICATION_GROUP_CUSTOMER_INVOICE = 'customer_invoice'

# Expire FYI in 24 hours
NOTIFICATION_EXPIRY_TIME_SECONDS = 86400

DEVICE_HISTORY_FIELDS = ['uuid', 'imei_number', 'device_id']


NUMBER_OF_NOTIFICATIONS_IN_PAGE = 50

DEFAULT_USER_ATTRIBUTES = ('name', 'email')
MAX_SIMILARITY = 0.7
DEFAULT_PASSWORD_LIST_PATH = 'common-passwords.txt.gz'
OPS_MANAGERS = 'Ops Managers'

