import re
import json
import logging
from datetime import timedelta

from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin)
from django.contrib.sessions.models import SessionManager, Session
from django.core.mail import send_mail
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.phonenumber import PhoneNumber

from rest_framework import status
from rest_framework.response import Response
from django_otp.oath import totp
from phonenumber_field.modelfields import PhoneNumberField
from rest_framework_jwt.settings import api_settings
from blowhorn.oscar.models.fields import NullCharField

from blowhorn.common.middleware import current_request
from blowhorn.address.constants import country_calling_codes
from blowhorn.common import sms_templates
from blowhorn.common.models import BaseModel
from blowhorn.common.tasks import send_sms
from blowhorn.users.utils import UserUtils
from blowhorn.users.constants import LOGIN_VALIDATION_MESSAGES, DEVICE_HISTORY_FIELDS
from config.settings.status_pipelines import ACTIVE
from blowhorn.contract.constants import EXEMPT

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class UserManager(BaseUserManager):

    """
    Custom manager for blowhorn user
    """

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Create and Save an User with email and password
            :param str email: user email
            :param str password: user password
            :param bool is_staff: whether user staff or not
            :param bool is_superuser: whether user admin or not
            :return users.models.User user: user
            :raise ValueError: email is not set
        """
        now = timezone.now()

        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email)

        is_active = extra_fields.pop("is_active", False)

        user = self.model(email=email, is_staff=is_staff, is_active=is_active,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """
        Create and save an User with email and password
        :param str email: user email
        :param str password: user password
        :return users.models.User user: regular user
        """
        is_staff = extra_fields.pop("is_staff", False)
        return self._create_user(email, password, is_staff, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save an User with the given email and password.
        :param str email: user email
        :param str password: user password
        :return users.models.User user: admin user
        """
        return self._create_user(email, password, True, True, is_active=True,
                                 **extra_fields)

    def do_login(self, request, email=None,
                 password=None, user=None, mobile=None, otp=None, **extra_fields):
        """
            Returns the JWT token in case of success, returns the error
            response in case of login failed.
        """
        if user:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
        else:
            if not email and not mobile:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data='Email is mandatory.')
            if email:
                user = authenticate(email=email, password=password)
        if mobile and otp:
            if not mobile in settings.TESTING_NUMBERS:
                device_otp, created = User.objects.retrieve_otp(user)
            else:
                #defaulting otp to 1234 for testing number
                device_otp, created = 1234, True
            if str(device_otp) != otp:
                user.login_attempt = user.login_attempt + 1
                if user.login_attempt > settings.INVALID_OTP_ATTEMPTS_ALLOWED:
                    user.is_active = False
                user.save()
                return Response(status=status.HTTP_402_PAYMENT_REQUIRED, data='Invalid OTP')
            if user:
                user.is_active = True
                if not user.is_mobile_verified:
                    user.is_mobile_verified=True
                # user.save()

        if user is None:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Incorrect email or password')

        # Return if the user is not active.
        if not user.is_active:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Account activation pending')

        # Login the User
        login(request, user)
        if user.login_attempt:
            user.login_attempt = 0
        user.save()
        token = self.generate_auth_token(user)

        response = {
            'token': token,
            'user': user
        }
        return Response(status=status.HTTP_200_OK, data=response)

    def _set_response(self, is_success, _status, message):
        return {
            'is_success': is_success,
            'status': _status,
            'message': message
        }

    def do_login_new(self, request, email=None, password=None, user=None,
                     mobile=None, otp=None,
                     **extra_fields):
        """
            Returns the JWT token in case of success, returns the error
            response in case of login failed.
        """
        if user:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
        else:
            if not email:
                return self._set_response(
                    False, status.HTTP_400_BAD_REQUEST,
                    LOGIN_VALIDATION_MESSAGES['email_required'])

            user = authenticate(email=email, password=password)

        if mobile and otp:
            if not mobile in settings.TESTING_NUMBERS:
                device_otp, created = User.objects.retrieve_otp(user)
            else:
                #defaulting otp to 1234 for testing number
                device_otp, created = 1234, True
            if str(device_otp) != otp:
                user.login_attempt = user.login_attempt + 1
                if user.login_attempt > settings.INVALID_OTP_ATTEMPTS_ALLOWED:
                    user.is_active = False
                user.save()
                return self._set_response(False, status.HTTP_400_BAD_REQUEST, LOGIN_VALIDATION_MESSAGES['invalid_otp'])
            if user:
                user.is_active = True
                if not user.is_mobile_verified:
                    user.is_mobile_verified = True

        if user is None:
            return self._set_response(
                    False, status.HTTP_401_UNAUTHORIZED, LOGIN_VALIDATION_MESSAGES['invalid_password'])

        if not user.is_active:
            return self._set_response(
                    False, status.HTTP_401_UNAUTHORIZED,
                    LOGIN_VALIDATION_MESSAGES['activation_pending'])

        login(request, user)
        user.save()
        token = self.generate_auth_token(user)

        message = {
            'token': token,
            'user': user
        }
        return self._set_response(True, status.HTTP_200_OK, message)

    def generate_auth_token(self, user):
        # Generating the JWT Token
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return token

    def retrieve_otp(self, user):
        from blowhorn.customer.mixins import CustomerMixin

        created = False
        device = user.totpdevice_set.last()
        if not device:
            created = True
            device = user.totpdevice_set.create(
                step=settings.OTP_CONFIG.get('step'),
                digits=settings.OTP_CONFIG.get('digits'))

        otp = "%0*d" % (device.digits,
                        totp(key=device.bin_key,
                             digits=device.digits,
                             step=device.step))

        user.last_otp_value = "%0*d" % (4, int(otp))
        user.last_otp_sent_at = timezone.now()
        user.save()

        customer = CustomerMixin().get_customer(user)
        if customer:
            from blowhorn.customer.models import Customer
            Customer.objects.add_otp(customer, otp)

        return otp, created

    def staff_users(self):
        return super().get_queryset().filter(
            is_active=True,
            is_staff=True,
            email__icontains='blowhorn.com'
        )


class User(AbstractBaseUser, PermissionsMixin):
    """
    Blowhorn user does not have the username field
    It uses email as USERNAME_FIELD for authentication
    The following attributes are inherited from the superclasses:
        * password
        * last_login
        * is_superuser
    """

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(
        _('Name of User'), blank=True, max_length=255, db_index=True)

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        db_index=True
    )

    is_staff = models.BooleanField(
        _('staff status'), default=False, help_text=_(
            'Designates whether the user can log into this admin site.'))

    is_active = models.BooleanField(_('active'), default=False, help_text=_(
        'Designates whether this user should be treated as '
        'active. Unselect this instead of deleting accounts.'))

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    phone_number = PhoneNumberField(
        _("Mobile Number"), blank=True,
        help_text=_("Customer's primary mobile number e.g. +91{10 digit mobile number}"))

    country_code = models.CharField('Country Code', max_length=2, blank=True,
                                    null=True)

    is_email_verified = models.BooleanField(_('Email verified'), default=False,
                                            help_text=_('Whether the email has been verified by the user.'))

    is_mobile_verified = models.BooleanField(_('Mobile Verified'),
                                             default=False, help_text=_(
            'Whether the mobile number has been verified by the user.'))

    last_otp_value = models.CharField(_('last OTP value'), max_length=4,
                                      null=True, blank=True)

    last_otp_sent_at = models.DateTimeField(_('last OTP sent at'), null=True,
                                            blank=True)

    city = models.CharField(_('City'), max_length=255, null=True, blank=True)

    is_mobile_whitelisted = models.BooleanField(_('Mobile Whitelisted'),
                                                default=False)

    objects = UserManager()

    login_attempt = models.SmallIntegerField(_('UnSuccessful Attempts'),
                                             default=0)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this User."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __str__(self):
        return self.email

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.old_staff_status = self.is_staff

    def save(self, *args, **kwargs):
        self.email = self.email.lower()
        if not self.pk:
            phone_number = self.phone_number
            if phone_number:
                country_code = country_calling_codes.get(str(phone_number.country_code))
                if country_code:
                    self.country_code = country_code

            '''Set STAFF flag if user belongs to WHITELISTED DOMAINS'''
            if UserUtils().is_staff_email(self.email):
                self.is_staff = True
                self.is_active = True
        else:
            from blowhorn.driver.models import Driver
            domain = re.search("@[\w.]+", self.email)
            if re.search(settings.DRIVER_DOMAIN_REGEX, self.email):
                driver = Driver.objects.filter(user=self.id).first()
                if driver:
                    driver.name = self.name
                    driver.save()
            # check for customers
            elif domain and domain.group()[1:] not in settings.DOMAIN_WHITELIST:
                if self.is_customer():
                    customer = self.customer
                    customer.name = self.name
                    if not customer.service_tax_category:
                        customer.service_tax_category = EXEMPT
                    customer.save()
        super(User, self).save(*args, **kwargs)

    def is_customer(self):
        """
        Check if user is customer. Returns `true` on success, otherwise `false`.
        """

        return hasattr(self, 'customer')

    def is_blowhorn_staff(self):
        """
        Check if user is staff. Returns `true` on success, otherwise `false`.
        """

        return self.is_staff

    def is_active_driver(self):
        """
        Check if user is driver. Returns `true` on success, otherwise `false`.
        """

        return self.is_active and hasattr(self, 'driver') and self.driver.status == ACTIVE

    def is_active_user(self):
        """
        Check if user is active. Returns `true` on success, otherwise `false`.
        """

        return self.is_active

    def send_otp(self, phone_number):
        """
        :param phone_number: 10-digit mobile number
        """
        otp, created = User.objects.retrieve_otp(self)
        self.is_mobile_verified = False
        logger.info('Sending otp to %s: %s' % (phone_number, otp))
        template_dict = sms_templates.template_otp
        message = template_dict['text'] % otp

        if settings.DEBUG:
            return
        template_json = json.dumps(template_dict)
        send_sms.apply_async(args=[phone_number, message, template_json])

    def send_sms(self, message, template_dict):
        phone_number = '%s' % self.phone_number
        logger.info('Sending sms to %s: %s' % (phone_number, message))
        template_json = json.dumps(template_dict)
        if phone_number and settings.ENABLE_SLACK_NOTIFICATIONS:
            send_sms.apply_async(args=[phone_number, message, template_json])

    @property
    def national_number(self):
        if self.phone_number:
            return self.phone_number.national_number
        return '%s' % self.phone_number

    @staticmethod
    def autocomplete_search_fields():
        return 'name', 'email', 'phone_number'

    @property
    def is_city_manager(self):
        from blowhorn.address.models import City
        return City.objects.filter(managers=self).exists()

    @property
    def is_company_executive(self):
        from blowhorn.company.models import Company
        return True if self.email in Company.objects.exclude(executives__email__isnull=True
                                                                                    ).values_list('executives__email',
                                                                                                  flat=True) else False
    def is_sales_representative(self, city=None):
        from blowhorn.address.models import City
        if city:
             return City.objects.filter(sales_representatives=self, id=city.id).exists()
        return City.objects.filter(sales_representatives=self).exists()

    @property
    def is_sales_head(self):
        from blowhorn.company.models import Company
        return Company.objects.filter(sales_head__email=self.email).exists()

    def is_ops_manager(self, city_qs=None):
        from blowhorn.address.models import City
        if city_qs:
            return City.objects.filter(id__in=city_qs, operation_managers=self).exists()
        return City.objects.filter(operation_managers=self).exists()

    def is_supply_user(self, city=None):
        from blowhorn.address.models import City
        if city:
            return City.objects.filter(id=city.id, supply_users=self).exists()
        return City.objects.filter(supply_users=self).exists()


class UserSessionManager(SessionManager):

    def is_valid_session(self, request):
        """
        :param request: django request
        :returns: boolean
        """
        user = request.user
        token = request.META.get('HTTP_TOKEN', None)
        current_session = self.get_session_using_token(token)

        ''' Below query is added to remove multiple devices which are (may be)
            already logged in whose session data not available in UserSession.
            This query can be changed to get a single session after some period
            of time.
        '''
        session_keys = list(self.get_session_keys_using_user(user, token))
        if not current_session and len(session_keys) > 0:
            logout(request)
            return False
        return True

    def get_session_using_token(self, token):
        """
        :param token: jwt token (string)
        :returns: Django session of given token (instance)
        """
        session = None
        try:
            session = UserSession.objects.get(token=token)
        except UserSession.DoesNotExist:
            logging.info('Session doesn\'t exists')
        return session

    def get_session_using_session_key(self, session_key):
        session = None
        try:
            session = UserSession.objects.get(session_key=session_key)
        except UserSession.DoesNotExist:
            logging.info('Session doesn\'t exists')
        return session

    def get_session_keys_using_user(self, user, token):
        """
        :param user: instance of User,
        :param token: jwt token (string)
        :returns: Django session keys of given user excluding given token
        """
        return UserSession.objects.filter(Q(user=user) &
                ~Q(token=token)).values_list('session_key', flat=True)

    def remove_sessions_using_session_keys(self, session_keys):
        """
        :param session_keys: list of session_keys
        :returns: count of removed sessions
        """
        return UserSession.objects.filter(
            session_key__in=list(session_keys)).delete()

    def remove_session_using_token(self, token):
        """
        :param token: token (string)
        :returns: count of deleted sessions
        """
        return UserSession.objects.filter(token=token).delete()

    def remove_django_sessions(self, session_keys):
        """
        :param session_keys: session_keys (list)
        :returns: number of deleted django sessions
        """
        sessions = None
        if session_keys:
            sessions = Session.objects.filter(
                session_key__in=list(session_keys)
            ).delete()
        return sessions


class UserSession(models.Model):
    session_key = models.CharField(_('Session Key'), max_length=40,
                                   primary_key=True)
    user = models.ForeignKey(User, related_name='session_user',
                             on_delete=models.PROTECT)
    token = NullCharField(_('Token'), max_length=255, unique=True, blank=True,
                          null=True)

    objects = UserSessionManager()

    class Meta:
        verbose_name = _('User Session')
        verbose_name_plural = _('User Sessions')

    def __str__(self):
        return '%s: %s' % (self.session_key, self.user)


class UserLoginDevice(models.Model):
    device_user = models.ForeignKey(User, on_delete=models.PROTECT)
    uuid = models.CharField(_('Universally Unique Identifier'), max_length=255, blank=True,
                            null=True)
    device_id = models.CharField(_('Device ID'), max_length=255, blank=True, null=True)
    imei_number = models.CharField(_('IMEI Number'), max_length=255, blank=True, null=True)
    last_login_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s-%s' % (self.device_user.email, self.imei_number)

    def _capture_changes(self):
        _request = current_request()
        current_user = _request.user if _request else ''
        change_log = []

        old = self.__class__.objects.get(pk=self._get_pk_val())
        for field in self.__class__._meta.fields:

            field_name = field.__dict__.get('name')
            if field_name in DEVICE_HISTORY_FIELDS:
                old_value = field.value_from_object(old)
                current_value = field.value_from_object(self)

                if old_value != current_value:
                    changes = {
                        'field': '%s' % (field.__dict__.get(
                            'verbose_name') or field_name),
                        'old': '%s' % old_value,
                        'new': '%s' % current_value or '--EMPTY--'
                    }
                    change_log.append(changes)
        if bool(change_log):
            history = {
                'user_device': self,
                'log': json.dumps(change_log),
                'modified_date': timezone.now()
                }
            LoginDeviceHistory.objects.create(**history)

    def save(self, *args, **kwargs):
        if self.pk:
            self._capture_changes()
        super().save(*args, **kwargs)


class LoginDeviceHistory(models.Model):
    user_device = models.ForeignKey(UserLoginDevice, on_delete=models.CASCADE)
    modified_date = models.DateTimeField(auto_now_add=True)
    log = models.TextField(_('Log'))


class Donation(BaseModel):
    """
    user donation table
    """
    #user who gave donation
    user = models.ForeignKey(User, models.CASCADE)

    recipient = models.ForeignKey(User, models.CASCADE, null=True, blank=True, related_name='recipient')

    amount = models.DecimalField(_('Amount'), max_digits=10, decimal_places=2)

    order = models.ForeignKey('order.Order', on_delete=models.PROTECT, null=True, blank=True)

    description = models.CharField(_("Description"), max_length=250, null=True, blank=True)

    source = models.ForeignKey('payment.Source', on_delete=models.PROTECT, null=True, blank=True)

    wallet_source = models.ForeignKey('payment.TransactionOrder', on_delete=models.PROTECT, null=True, blank=True)

    response = models.TextField('Payment response', null=True)

