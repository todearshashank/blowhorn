STAFF_MESSAGES = {
    'create': {
        'subject': 'You have been added to enterprise.blowhorn.com',
        'body': 'You have been added as a staff user in enterprise.blowhorn.com.'
            ' Contact admin for password and change the password after first login'
    },
    'add': {
        'subject': 'You have been added as a staff to enterprise.blowhorn.com',
        'body': 'You have been added as a staff user in enterprise.blowhorn.com.'
    },
    'remove': {
        'subject': 'You have been removed as a staff from enterprise.blowhorn.com',
        'body': 'You have been removed from staff in enterprise.blowhorn.com.'
        'You\'re no longer allowed to access.'
    }
}
