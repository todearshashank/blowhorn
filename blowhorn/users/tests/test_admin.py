import pytest

from django.urls import reverse

@pytest.mark.django_db
def test_fixture_create_user(create_user):
    user = create_user(email='ci@blowhorn.com')
    assert user.is_authenticated is True

def test_an_admin_view(admin_client):
    response = admin_client.get('/admin/')
    assert response.status_code == 200

def test_vehicleclass_admin(admin_client):
    response = admin_client.post(
        "/admin/vehicle/vehicleclass/add/",
        {
            "commercial_class": "some class",
            "licence_category_code": "LCV",
            "licence_category_desc": "Low commercial vehicle",
            "_save": "Save",
        },
    )
    assert response.status_code == 200
