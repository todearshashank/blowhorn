from django.test import SimpleTestCase

from blowhorn.users.forms import UserCreationForm

class UserFormTest(SimpleTestCase):
    def test_empty_form(self):
        form = UserCreationForm()
        self.assertFalse(form.is_valid())
