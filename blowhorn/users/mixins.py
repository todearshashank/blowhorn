import logging
from django.utils import timezone

from blowhorn.users.constants import NOTIFICATION_STATUS_EXPIRED, NOTIFICATION_STATUS_ACTIVE
from blowhorn.users.submodels.notification import AppNotification


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class AuthMixin(object):

    def has_model_permissions(self, user, model, perms, app):
        for p in perms:
            if not user.has_perm("%s.%s_%s" % (app, p, model.__name__.lower())):
                return False

            return True

    def atleast_one_permission(self, user, model, perms, app):
        for p in perms:
            if user.has_perm("%s.%s_%s" % (app, p, model.__name__.lower())):
                return True
        return False


class NotificationMixin(object):

    def expire_notifications(self):
        notifications = AppNotification.objects.filter(status=NOTIFICATION_STATUS_ACTIVE,
                                                       expiry_time__lt=timezone.now(), action_owners__isnull=True)
        logger.info('Total notifications to expire: %s' % notifications.count())
        notifications.update(status=NOTIFICATION_STATUS_EXPIRED)
