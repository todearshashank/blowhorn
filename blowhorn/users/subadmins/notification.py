from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.admin import BaseAdmin
from blowhorn.users.submodels.notification import \
    NotificationMessageTemplateChangeLog
from blowhorn.utils.datetimerangefilter import DateTimeRangeFilter
from blowhorn.utils.functions import get_formatted_log


class NotificationAdmin(BaseAdmin):

    actions = None

    list_display = ('source', 'notification_group', 'status', 'expiry_time',)

    list_filter = ('notification_group', 'status',
                   ('created_date', DateTimeRangeFilter))

    search_fields = ('source__email', 'action_owners__email',
                     'broadcast_list__email')

    # readonly_fields = ('source', 'category', 'notification_group', 'status',
    #                    'expiry_time', 'redirect_url', 'message',
    #                    'action_owners', 'broadcast_list',) + AUDITLOG_FIELDS

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('source', 'created_by', 'modified_by')
        return qs.prefetch_related('action_owners', 'broadcast_list')

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)


class NotificationMessageTemplateChangeLogInline(admin.TabularInline):
    model = NotificationMessageTemplateChangeLog
    can_delete = False
    max_num = 5000
    extra = 0
    readonly_fields = ('modified_by', 'modified_date', 'get_log')
    exclude = ('log',)
    verbose_name = 'Changelog'
    verbose_name_plural = 'Changelog'

    def has_add_permission(self, request, obj=None):
        return False

    def get_log(self, obj):
        return get_formatted_log(obj, 'log')

    get_log.allow_tags = True
    get_log.short_description = _('Log')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('modified_by', 'message_template')


class NotificationTemplateAdmin(BaseAdmin):
    list_display = ('category', 'notification_group', 'is_active', 'template',
                    'created_date', 'created_by', 'modified_date', 'action',
                    'modified_by')

    readonly_fields = ('created_date', 'created_by', 'modified_date',
                       'modified_by')

    show_full_result_count = False
    actions_selection_counter = False

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            return (('General', {
                'fields': ('category', 'notification_group', 'action', 'title')
            }),)

        return (
            ('General', {
                'fields': ('category', 'notification_group', 'action',
                           'title', 'template', 'is_active')
            }),
            ('Auditlog', {
                'fields': ('created_date', 'created_by', 'modified_date')
            }),
        )

    def get_inline_instances(self, request, obj=None):
        inlines = []
        if obj:
            inlines.append(
                NotificationMessageTemplateChangeLogInline(self.model,
                                                           self.admin_site))
        return inlines
