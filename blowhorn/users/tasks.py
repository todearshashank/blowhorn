from __future__ import absolute_import, unicode_literals
import json
import logging
import requests
import traceback
from django.conf import settings
from django.core.mail import send_mail

from celery import shared_task
# from celery.decorators import periodic_task
from celery.schedules import crontab

from blowhorn import celery_app
from blowhorn.common.decorators import redis_batch_lock
from blowhorn.users.constants import DEFERRED, SPAM_REPORT
from blowhorn.utils.functions import set_slack_field_details


@celery_app.task(serializer='json')
def send_notification_mail(_dict=None):
    message = json.loads(_dict)
    send_mail(
        message['subject'],
        message['body'],
        settings.DEFAULT_FROM_EMAIL,
        [message['email']]
    )


def get_color(event):
    if event in [DEFERRED, SPAM_REPORT]:
        return '#FFA500'
    else:
        return '#FF0000'


@celery_app.task()
def notify_slack_email_failure(json_data):
    json_data = json.loads(json_data)
    slack_channel = settings.EMAIL_FAILURE_SLACK_CHANNEL.get('name', '')
    slack_channel_url = settings.EMAIL_FAILURE_SLACK_CHANNEL.get('url', '')
    if settings.DEBUG:
        slack_channel = settings.TESTING_SLACK_CHANNEL_DEFAULT
        slack_channel_url = settings.TESTING_SLACK_URL

    attachments = []
    for event in json_data:
        fields = [
            set_slack_field_details(
                data.get('email', ''),
                "%s | %s" % (data.get('reason', '') or '--',
                             data.get('response', None) or '--')
            ) for data in json_data[event]]

        attachments.append({
            "title": "%s" % event.upper(),
            "fields": fields,
            "color": get_color(event)
        })

    slack_message = {
        "attachments": attachments
    }
    slack_message.update({'channel': slack_channel})
    try:
        requests.post(url=slack_channel_url, json=slack_message)
    except ConnectionResetError:
        logging.error(traceback.format_exc())
