# Generated by Django 2.1.1 on 2020-10-14 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0013_donation'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='login_attempt',
            field=models.SmallIntegerField(default=0, verbose_name='UnSuccessful Attempts'),
        ),
    ]
