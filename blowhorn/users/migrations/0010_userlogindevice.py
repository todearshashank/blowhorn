# Generated by Django 2.1.1 on 2019-12-13 06:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_user_country_code'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserLoginDevice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.CharField(blank=True, max_length=255, null=True, verbose_name='Universally Unique Identifier')),
                ('device_id', models.CharField(blank=True, max_length=255, null=True, verbose_name='Device ID')),
                ('imei_number', models.CharField(blank=True, max_length=255, null=True, verbose_name='IMEI Number')),
                ('last_login_time', models.DateTimeField(auto_now_add=True)),
                ('device_user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
