from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin as _UserAdmin
from django.db.models import Sum
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from auditlog.admin import LogEntryAdmin as LogEntryAdmin_
from auditlog.models import LogEntry

from blowhorn.users.forms import UserChangeForm, UserCreationForm
from blowhorn.users.models import User, UserLoginDevice, LoginDeviceHistory, Donation
from blowhorn.utils.log_mixins import LogEntryAdminMixin
from blowhorn.users.subadmins.notification import NotificationAdmin, \
    NotificationTemplateAdmin
from blowhorn.users.submodels.notification import AppNotification, \
    NotificationMessageTemplate
from blowhorn.utils.functions import get_formatted_log
from blowhorn.oscar.core.loading import get_model
from blowhorn.utils.datetimerangefilter import DateRangeFilter

City = get_model('address', 'City')


def mark_staff_user_inactive(modeladmin, request, queryset):
    """Things to do before de-activating a staff user
    """
    from blowhorn.contract.models import Contract
    from blowhorn.address.models import City
    from blowhorn.company.models import Company
    users_list = queryset.values('pk', 'is_staff', 'is_active')
    curated_list = [x.get('pk') for x in users_list if x.get('is_staff') and x.get('is_active')]

    if not curated_list:
        message = 'selected non staff users %s' % users_list
        messages.error(request, message)
        return False

    print('de-activating users', curated_list)
    for user_id in curated_list:
        user = User.objects.filter(id=user_id)
        print('de-activating user', user)
        contracts_as_spoc = Contract.objects.filter(spocs__in=user)
        print('In contracts as SPOCs', contracts_as_spoc)
        for contract in contracts_as_spoc:
            contract.spocs.remove(user.first())
        contracts_as_supervisor = Contract.objects.filter(supervisors__in=user)
        print('In contracts as supervisors', contracts_as_supervisor)
        for contract in contracts_as_supervisor:
            contract.supervisors.remove(user.first())
        city_as_manager = City.objects.filter(managers__in=user)
        print('In cities as managers', city_as_manager)
        for city in city_as_manager:
            city.managers.remove(user.first())
        company_as_finuser = Company.objects.filter(finance_user__in=user)
        print('In Companies as finance users', company_as_finuser)
        for company in company_as_finuser:
            company.finance_user.remove(user.first())
        city_as_ops_mgr = City.objects.filter(operation_managers__in=user)
        print('In cities as operations manager', city_as_ops_mgr)
        for city in city_as_ops_mgr:
            city.operation_managers.remove(user.first())
        city_as_sales = City.objects.filter(sales_representatives__in=user)
        print('In cities as sales rep', city_as_sales)
        for city in city_as_sales:
            city.sales_representatives.remove(user.first())
        company_as_executive = Company.objects.filter(executives__in=user)
        print('In company as an executive', company_as_executive)
        for company in company_as_executive:
            company.executives.remove(user.first())
        company_as_collection = Company.objects.filter(collection_representatives__in=user)
        print('In company as collection assoc', company_as_collection)
        for company in company_as_collection:
            company.collection_representatives.remove(user.first())
        print('Finally set him inactive, change staff status to false')
        user.update(is_staff=False, is_active=False, is_email_verified=False,is_mobile_verified=False)


class UserAdmin(_UserAdmin):

    """Extend the django defined User Admin model."""

    fieldsets = (
        (None, {'fields': ('name', 'email', 'password', 'phone_number', 'last_otp_value', 'last_otp_sent_at' , 'is_email_verified', 'is_mobile_verified')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (_('Enter username and password'), {'classes': (
            'wide',), 'fields': ('email', 'password1', 'password2', 'phone_number')}),
    )

    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    actions = [mark_staff_user_inactive]

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'name', 'is_active', 'is_staff', 'phone_number')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email', 'phone_number',)
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)

    def get_readonly_fields(self, request, obj=None):
        """
        Extra restrictions for non-Super Users
        1. Superuser field should be readonly
        2. User should not be able to edit other users and give permissions
            unless they are a city manager.

        Point 2 done so as to not burden the people with Superuser access
        (which would be very few) when new employees are added and new permissions
        need to given/removed to users.
        """

        readonly_fields = ['last_login', 'date_joined', 'last_otp_value', 'last_otp_sent_at']
        # Everything is editable for Superuser
        if request.user.is_superuser:
            return readonly_fields

        readonly_fields.append('is_superuser')

        # Only City managers can edit following fields
        city_manager = City.objects.filter(managers=request.user).exists()
        if not city_manager:
            readonly_fields.extend(['is_staff', 'groups', 'user_permissions'])
        else:
            readonly_fields.extend(['user_permissions'])

        # User cannot edit details of other staff unless city manager
        # User cannot edit details of superuser.
        if obj and obj.is_superuser:
            readonly_fields.extend(['name', 'email', 'password', 'phone_number',
                'is_staff', 'groups', 'user_permissions'])
        elif obj and obj != request.user and \
            ((obj.is_staff and not city_manager)):
            readonly_fields.extend(['name', 'email', 'password', 'phone_number',])

        return readonly_fields


# Register the new UserAdmin
admin.site.register(User, UserAdmin)


class LogEntryAdmin(LogEntryAdmin_, LogEntryAdminMixin):
    search_fields = ['timestamp', 'object_repr', 'changes', 'actor__email']
    actions = None

# class ChangeLogAdmin(admin.ModelAdmin):
#     changelog_template = 'admin/changelog.html'

#     def has_add_permission(self, request):
#         return False

class LoginDeviceHistoryInline(admin.TabularInline):
    model = LoginDeviceHistory
    can_delete = False
    readonly_fields = ('user_device', 'modified_date', 'get_log')
    exclude = ('log',)
    verbose_name = _('Changelog')
    verbose_name_plural = _('Changelog')

    def has_add_permission(self, request):
        return False

    def get_log(self, obj):
        return get_formatted_log(obj, 'log')
    get_log.allow_tags = True
    get_log.short_description = _('Log')


class UserLoginDeviceAdmin(admin.ModelAdmin):
    list_display = ('id', 'device_user', 'uuid', 'imei_number', 'device_id', 'last_login_time')

    readonly_fields = ('id', 'device_user', 'uuid', 'imei_number', 'device_id', 'last_login_time')

    search_fields = ['id', 'device_user__email', 'uuid', 'imei_number', 'device_id']

    inlines = [LoginDeviceHistoryInline]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    actions = None


class DonationAdmin(admin.ModelAdmin):
    change_list_template = 'admin/users/donation/change_list.html'
    list_display = ('id', 'user', 'amount', 'paytm_source',
        'razorpay_source', 'description', 'created_date')
    list_filter = [
        ('created_date', DateRangeFilter)
    ]
    search_fields = ['user__email']
    actions = None

    def get_readonly_fields(self, request, obj=None):
        if obj :
            return [f.name for f in self.model._meta.fields]
        return ''

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def razorpay_source(self, obj):
        if obj.source:
            return '%s' % obj.source.reference or obj.source.wallet_reference
        return '-'

    def paytm_source(self, obj):
        if obj.wallet_source:
            url = reverse(
                'admin:payment_transactionorder_change', args=[obj.wallet_source_id])
            return format_html("<a href='{}'>{}</a>", url, obj.wallet_source.order_number)
        return '-'

    def get_total(self):
        return Donation.objects.values('amount').aggregate(
            total_amount=Sum('amount')).get('total_amount', 0.0)

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save_and_add_another'] = False
        extra_context['total_amount'] = self.get_total()
        return super(DonationAdmin, self).changelist_view(request,
            extra_context=extra_context)


admin.site.unregister(LogEntry)
admin.site.register(Donation, DonationAdmin)
admin.site.register(LogEntry, LogEntryAdmin)
admin.site.register(AppNotification, NotificationAdmin)
admin.site.register(NotificationMessageTemplate, NotificationTemplateAdmin)
admin.site.register(UserLoginDevice, UserLoginDeviceAdmin)