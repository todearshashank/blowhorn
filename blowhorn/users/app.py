from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'blowhorn.users'
    verbose_name = "Users"

    def ready(self):
        from blowhorn.users import signals  # noqa
