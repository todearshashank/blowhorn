# System & Django
import json

# 3rd Party Libraries
from blowhorn.oscar.core.loading import get_model
from rest_framework import serializers
from django.conf import settings

# Blowhorn Modules
from blowhorn.address.models import UserAddress, Country
from blowhorn.common.helper import CommonHelper

# Country = get_model('address', 'Country')
User = get_model('users', 'User')
UserSession = get_model('users', 'UserSession')


class UserAddressSerializer(serializers.Serializer):

    def validate(self, attr):
        request = self.context['request']
        geopoint = CommonHelper.get_geopoint_from_latlong(request.get('lat'), request.get('lon'))
        country_code = settings.COUNTRY_CODE_A2
        country = Country.objects.filter(iso_3166_1_a2=country_code).get()
        contact_mobile = str(request.get('contact_mobile', ''))
        if contact_mobile and not contact_mobile.startswith(settings.ACTIVE_COUNTRY_CODE):
            phone_number = settings.ACTIVE_COUNTRY_CODE + contact_mobile
        else:
            phone_number = request.get('contact_mobile', '')

        attr['identifier'] = request.get('name', '')
        attr['line1'] = request.get('full_address', '')
        # attr['line2'] = request.get('area')
        attr['line3'] = request.get('area', '')
        attr['line4'] = request.get('city', '')
        attr['postcode'] = request.get('postal_code', '')
        attr['country'] = country
        attr['geopoint'] = geopoint
        attr['user'] = self.context.get('user', '')
        attr['phone_number'] = phone_number
        attr['first_name'] = request.get('contact_name', '')

        if not attr['line1']:
            attr['line1'] = request.get('area', '')

        if not attr['line3']:
            attr['line3'] = request.get('full_address', '')

        return attr

    def create(self, validated_data):
        return UserAddress.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.identifier = validated_data.get('identifier', instance.identifier)
        instance.line1 = validated_data.get('line1', instance.line1)
        instance.line2 = validated_data.get('line2', instance.line2)
        instance.line4 = validated_data.get('line4', instance.line4)
        instance.postcode = validated_data.get('postcode', instance.postcode)
        instance.geopoint = validated_data.get('geopoint', instance.geopoint)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.save()
        return instance

    class Meta:
        model = UserAddress
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    queryset = User.objects.all()

    class Meta:
        model = User
        fields = '__all__'


class UserSessionSerializer(serializers.ModelSerializer):
    queryset = UserSession.objects.all()

    class Meta:
        model = UserSession
        fields = '__all__'
