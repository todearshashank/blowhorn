# System and Django
import logging
from django import forms
from django.conf import settings
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from django.dispatch import receiver
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _

# 3rd Party Packages
from allauth.account.adapter import DefaultAccountAdapter, get_adapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.forms import SetPasswordField, PasswordField
from allauth.exceptions import ImmediateHttpResponse
from allauth.socialaccount.signals import pre_social_login
from allauth.account.utils import perform_login
from allauth.utils import get_user_model
from rest_framework import status
from rest_framework.response import Response
from blowhorn.oscar.core.loading import get_model
from phonenumber_field.phonenumber import PhoneNumber

# Blowhorn modules
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, \
    CUSTOMER_CATEGORY_NON_ENTERPRISE
from blowhorn.customer.serializers import CustomerTaxInformationSerializer
from blowhorn.users.utils import UserUtils
from blowhorn.contract.constants import GTA


Customer = get_model('customer', 'Customer')
User = get_model('users', 'User')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class SignupForm(forms.Form):
    """
        Override default all-auth signup form to add custom fields
    """
    name = forms.CharField()
    email = forms.EmailField(required=True,)
    password1 = SetPasswordField()
    password2 = PasswordField()
    phone_number = forms.CharField(required=False)
    is_business_user = forms.BooleanField(required=False)
    gstin = forms.CharField(required=False)
    state = forms.CharField(required=False)
    legal_name = forms.CharField(required=False)

    class Meta:
        fields = ('email', 'password1',  'password2', 'name', 'phone_number', 'state')

    def clean(self):
        super(SignupForm, self).clean()
        dummy_user = get_user_model()
        password = self.cleaned_data.get('password1')
        brand_name = self.cleaned_data.get('name')
        is_business_customer = self.cleaned_data.get('is_business_user')
        email = self.cleaned_data.get('email')
        phone_number = self.cleaned_data.get('phone_number')
        mobile = PhoneNumber(
            settings.ACTIVE_COUNTRY_CODE, phone_number) if phone_number else None

        is_mobile_no_taken = User.objects.filter(phone_number=mobile, customer__isnull=False).exists()
        if is_mobile_no_taken:
            self.add_error(
                'email', _('Mobile number is associated with another account'))

        if UserUtils().is_staff_email(email):
            self.add_error(
                    'email', _('Company email is not allowed for signup'))

        if password:
            try:
                get_adapter().clean_password(password, user=dummy_user)
            except forms.ValidationError as e:
                self.add_error('password1', e)

        if settings.SIGNUP_PASSWORD_ENTER_TWICE \
                and 'password1' in self.cleaned_data \
                and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] \
                    != self.cleaned_data['password2']:
                self.add_error(
                    'password2',
                    _('You must type the same password each time.'))

        if is_business_customer:
            existing_customer = Customer.objects.filter(name=brand_name).exclude(
                customer_category=CUSTOMER_CATEGORY_INDIVIDUAL).exists()

            if existing_customer:
                self.add_error(
                    'name', _('Customer With this name already exists'))

        return self.cleaned_data

    def signup(self, request, user):
        user.is_staff = False
        user.save()


class AccountAdapter(DefaultAccountAdapter):
    def clean_password(self, password, user=None):
        """
        Validates a password based on AUTH_PASSWORD_VALIDATORS from settings
        """
        validate_password(password, user)
        return password

    def get_custom_redirect_url(self, is_staff_mail):
        if is_staff_mail:
            return '/admin'
        return '/'

    def get_login_redirect_url(self, request):
        is_staff_mail = UserUtils().is_staff_email(request.user.email)
        return self.get_custom_redirect_url(is_staff_mail)

    @transaction.atomic
    def save_user(self, request, user, form, commit=True):
        data = form.cleaned_data
        user.name = data.get('name', '')
        email = data.get('email', False)
        user.email = email.lower() if email else ''
        mobile = data.get('phone_number')
        if mobile:
            if settings.ACTIVE_COUNTRY_CODE not in mobile:
                mobile = PhoneNumber(
                    settings.ACTIVE_COUNTRY_CODE, mobile)
            user.phone_number = mobile
        user.set_password(data.get('password1', False))

        user.is_active = True
        if commit:
            user.save()
            customer = Customer.objects.create(user=user, name=user.name,
                                               slug=email)
            if user.phone_number:
                user.send_otp(user.national_number)

            is_business_user = data.get('is_business_user', False)
            gstin = data.get('gstin', None)
            legal_name = data.get('legal_name', None)
            customer.service_tax_category = GTA if is_business_user else None
            customer.is_tax_registered = is_business_user
            customer.customer_category = CUSTOMER_CATEGORY_NON_ENTERPRISE \
                if is_business_user else CUSTOMER_CATEGORY_INDIVIDUAL

            if is_business_user:
                if not legal_name:
                    _out = {
                        'status': 'FAIL',
                        'message': _('Company name is required for business')
                    }
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=_out)
                customer.legalname = legal_name
                customer.save()
                data['customer_id'] = customer.pk

                if gstin:
                    tax_info = CustomerTaxInformationSerializer(data=data)
                    if tax_info.is_valid():
                        try:
                            tax_info.save()
                        except:
                            response = {
                                'status': 'FAIL',
                                'message': _('Failed to save record')
                            }
                            return Response(status=status.HTTP_400_BAD_REQUEST,
                                            data=response)
                    else:
                        response = {
                            'status': 'FAIL',
                            'message': _('Invalid GSTIN')
                        }
                        return Response(status=status.HTTP_400_BAD_REQUEST,
                                        data=response)
        return user

    def is_open_for_signup(self, request):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def get_connect_redirect_url(self, request, socialaccount):
        return AccountAdapter().get_custom_redirect_url(
            socialaccount.account.extra_data['is_staff_mail'])

    def is_open_for_signup(self, request, sociallogin):
        return not sociallogin.account.extra_data.get('is_staff_mail', True)

    @transaction.atomic
    def save_user(self, request, sociallogin, form=None):
        """
        Saves a newly signed up social login. In case of auto-signup,
        the signup form is not available.
        """
        user = sociallogin.user
        fullname = sociallogin.account.extra_data.get('name', '')
        if UserUtils().is_staff_email(user.email):
            raise ImmediateHttpResponse(redirect('/'))

        user.name = fullname
        user.is_active = True
        user.is_email_verified = True
        logger.info('Social: Creating new user: ', user)
        user.set_unusable_password()
        if form:
            get_adapter().save_user(request, user, form)
        else:
            get_adapter().populate_username(request, user)
        sociallogin.save(request)
        Customer.objects.create(user=user, name=fullname)


@receiver(pre_social_login)
def link_to_local_user(sender, request, sociallogin, **kwargs):
    """
    Social Sign In for existing local user causes error.
    Since, all-auth trying to create new user with same email.
    Refer: https://github.com/pennersr/django-allauth/issues/215
    """
    email_address = sociallogin.account.extra_data['email']
    is_staff_mail = UserUtils().is_staff_email(email_address)
    if is_staff_mail:
        raise ImmediateHttpResponse(redirect('/'))

    sociallogin.account.extra_data['is_staff_mail'] = is_staff_mail
    User = get_user_model()
    user = User.objects.filter(email=email_address).first()
    if user:
        redirect_url = AccountAdapter().get_custom_redirect_url(is_staff_mail)
        perform_login(request, user,
                      email_verification=settings.ACCOUNT_EMAIL_VERIFICATION)
        raise ImmediateHttpResponse(redirect(redirect_url))
