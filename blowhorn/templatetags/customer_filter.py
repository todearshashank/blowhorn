from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def subtractify(context, invoice):
    #credit note amount are stored in negative
    return round(float(invoice.get('balance_amount') or 0) + float(invoice.get('credit_amount') or 0),2)
