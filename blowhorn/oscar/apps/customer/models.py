from blowhorn.oscar.apps.customer import abstract_models
from blowhorn.oscar.core.loading import is_model_registered

__all__ = []


# if not is_model_registered('customer', 'ProductAlert'):
class ProductAlert(abstract_models.AbstractProductAlert):
    pass

    # __all__.append('ProductAlert')


class CommunicationEventType(abstract_models.AbstractCommunicationEventType):
    pass


class Notification(abstract_models.AbstractNotification):
    pass


class Email(abstract_models.AbstractEmail):
    pass


