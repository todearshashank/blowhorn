import datetime
import posixpath
OSCAR_IMAGE_FOLDER = 'images/products/%Y/%m/'


def get_image_upload_path(instance, filename):
    return posixpath.join(datetime.datetime.now().strftime(OSCAR_IMAGE_FOLDER), filename)
