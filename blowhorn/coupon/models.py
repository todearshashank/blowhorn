import json
from dateutil import tz
from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.middleware import current_request
from blowhorn.common.models import BaseModel
from blowhorn.coupon.constants import (
    COUPON_ACTIVE, COUPON_STATUSES, DISCOUNT_TYPE_FIXED, DISCOUNT_TYPES,
    DEVICE_TYPE_ALL, DEVICE_TYPES, DISCOUNT_TYPE_PERCENTAGE, MULTI_USE,
    ONCE_PER_CUSTOMER, USAGE_CHOICES, DISCOUNT_VALUE_FORMATTER)
from blowhorn.coupon.constants import (
    TAC_MAX_DISCOUNT, TAC_DISCOUNT_DISCLAIMER, TAC_ONCE_PER_CUSTOMER,
    TAC_MULTIUSE_LIMIT, LIMIT_PER_CUSTOMER_STR, TAC_VALID_TILL,
    TAC_MIN_AMOUNT_FOR_ELIGIBILITY
)
from blowhorn.customer.models import Customer
from blowhorn.coupon.constants import EXCLUDE_COUPON_HISTORY
from blowhorn.utils.functions import ist_to_utc


class Coupon(BaseModel):
    code = models.CharField(_('Code'), max_length=128, unique=True,
                            help_text=_('Code displayed to customer'))
    message = models.TextField(
        _('Message'), null=True, blank=True,
        help_text=_('Custom message when coupon is applied. Default message '
                    'will be displayed if this field is empty'))
    status = models.CharField(_('Status'), max_length=128,
                              default=COUPON_ACTIVE, choices=COUPON_STATUSES)
    cities = models.ManyToManyField('address.City')

    usage_type = models.CharField(_('Usage Type'), max_length=128,
                                  choices=USAGE_CHOICES, default=MULTI_USE)

    device_type = models.CharField(_('Device Type'), max_length=128,
                                   default=DEVICE_TYPE_ALL,
                                   choices=DEVICE_TYPES)
    discount_type = models.CharField(_('Discount Type'), max_length=128,
                                     default=DISCOUNT_TYPE_FIXED,
                                     choices=DISCOUNT_TYPES)
    discount_value = models.DecimalField(
        _('Discount Value Per Order'), max_digits=6, decimal_places=2, default=0)
    max_allowed_discount = models.DecimalField(
        _('Max. allowed discount amount'), max_digits=6, decimal_places=2,
        default=0, null=True, blank=True,
        help_text=_('Max. cap for discount for coupon '
                    '(applicable if discount_type is percentage)'))

    minimum_amount_for_eligibility = models.DecimalField(
        _('Min. fare required'), max_digits=6, decimal_places=2, null=True,
        blank=True, help_text=_('Min. fare required to eligible to apply this '
                                'coupon'))
    usage_limit = models.PositiveIntegerField(
        _('Usage Limit'), null=True, blank=True,
        help_text=_('Max. number of redeems allowed'))
    limit_per_customer = models.PositiveIntegerField(
        _('Limit per Customer'), null=True, blank=True,
        help_text=_('Max. number of times a customer can use this coupon'))
    limit_per_customer_per_day = models.PositiveIntegerField(
        null=True, blank=True, default=1,
        help_text=_('Max. number of redeems allowed in a day per customer'))
    start_time = models.DateTimeField(_('Start Time'))
    end_time = models.DateTimeField(_('End Time'))
    display_to_customer = models.BooleanField(
        _('Display to customer'), default=False,
        help_text=_('Enabled coupon will be visible to customer in app '
                    'and website')
    )
    is_allowed_for_house_shifting = models.BooleanField(
        _('Is Allowed For House Shifting?'), default=False,
        help_text=_('Enabled coupon can be applied for house-shifting bookings')
    )

    # Fields for reporting
    total_redeemed_amount = models.DecimalField(
        _('Total Redeemed Amount'), max_digits=12, decimal_places=2, default=0,
        help_text=_('Total amount redeemed by customers'))
    usage_count = models.PositiveIntegerField(
        _('Usage Count'), default=0,
        help_text=_('Number of times coupon applied'))

    class Meta:
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')

    def __str__(self):
        return self.code

    def _capture_changes(self):
        _request = current_request()
        current_user = _request.user if _request else ''
        change_log = []

        old = self.__class__.objects.get(pk=self._get_pk_val())
        for field in self.__class__._meta.fields:
            field_name = field.__dict__.get('name')
            if field_name not in EXCLUDE_COUPON_HISTORY:
                if field_name == 'cities':
                    old_value = ', '.join([c.name for c in old.cities.all()])
                    current_value = ', '.join(
                        [c.name for c in self.cities.all()])
                else:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                if old_value != current_value:
                    changes = {
                        'field': '%s' % (field.__dict__.get(
                            'verbose_name') or field_name),
                        'old': '%s' % old_value,
                        'new': '%s' % current_value or '--EMPTY--'
                    }
                    change_log.append(changes)

        if bool(change_log):
            history = {
                'coupon': self,
                'log': json.dumps(change_log),
                'modified_by': current_user,
                'modified_date': timezone.now()
            }
            CouponChangeLog.objects.create(**history)

    def save(self, *args, **kwargs):
        if self.pk:
            self._capture_changes()
        super().save(*args, **kwargs)

    @property
    def display_text(self):
        """
        Text displayed on successful apply of a coupon
        :return: display_text (str)
        """
        if self.message:
            return self.message
        elif self.discount_type == DISCOUNT_TYPE_FIXED:
            return _('You\'ll get ₹%d discount on this order.') % int(
                self.discount_value)
        elif self.discount_type == DISCOUNT_TYPE_PERCENTAGE:
            return _('You\'ll get %d%% discount on this order.') % int(
                self.discount_value)

    @property
    def terms_and_conditions(self):
        terms_and_conditions = [_(TAC_DISCOUNT_DISCLAIMER)]
        if self.usage_type == ONCE_PER_CUSTOMER:
            terms_and_conditions.append(_(TAC_ONCE_PER_CUSTOMER))
        elif self.usage_type == MULTI_USE:
            if self.limit_per_customer:
                limit_per_customer = '%s %s' % (
                    self.limit_per_customer, 'times') \
                    if self.limit_per_customer and self.limit_per_customer > 3 \
                    else LIMIT_PER_CUSTOMER_STR.get(self.limit_per_customer)

                multi_use_str = TAC_MULTIUSE_LIMIT.format(
                    limit_per_customer=limit_per_customer)
                terms_and_conditions.append(_(multi_use_str))

        if self.discount_type == DISCOUNT_TYPE_PERCENTAGE:
            max_discount = TAC_MAX_DISCOUNT.format(
                max_allowed_discount=int(self.max_allowed_discount))
            terms_and_conditions.append(_(max_discount))

        if self.minimum_amount_for_eligibility:
            tac_eligibility = TAC_MIN_AMOUNT_FOR_ELIGIBILITY.format(
                discount_value=self.get_discount_value,
                eligibility_fare=self.minimum_amount_for_eligibility
            )
            terms_and_conditions.append(tac_eligibility)

        valid_till = TAC_VALID_TILL.format(
            end_time=self.end_time.strftime(settings.APP_DATETIME_FORMAT))
        terms_and_conditions.append(_(valid_till))
        return terms_and_conditions

    @property
    def get_discount_value(self):
        """
        Discount value
        :return: (str)
        """
        return DISCOUNT_VALUE_FORMATTER[self.discount_type] % self.discount_value

    @property
    def is_expired(self):
        """
        Checks expiry date
        :return: boolean
        """
        return self.end_time < timezone.now()

    @property
    def is_usage_limit_reached(self):
        """
        Checks Usage Limit
        :return: boolean
        """
        return self.usage_limit and self.usage_limit <= self.usage_count

    def is_active(self, pickup_time=None):
        """
        :param pickup_time: pickup_datetime of order (datetime)

        :return: boolean
        """
        if self.status == COUPON_ACTIVE:
            if not pickup_time:
                pickup_time = timezone.now()
            else:
                pickup_time = ist_to_utc(pickup_time).replace(
                    tzinfo=tz.tzoffset('UTC', 0))
            return self.start_time <= pickup_time <= self.end_time
        return False

    def is_eligible_fare(self, amount):
        """
        :param amount:
        :return: boolean
        """
        return self.minimum_amount_for_eligibility \
                and float(amount) >= float(self.minimum_amount_for_eligibility)

    def is_available_to_customer(self, customer=None, pickup_datetime=None):
        """
        Whether coupon is available to the given customer.
        :param customer: instance of Customer
        :param pickup_datetime: requested pickup datetime
        :return: is_available (boolean), message (str)
        """
        is_available, message = False, ''
        if self.is_per_day_limit_reached_for_customer(customer, pickup_datetime):
            message = _('Your daily limit has been reached for this coupon')
        elif self.usage_type == MULTI_USE:
            is_available = not self.is_customer_limit_reached(customer)
            if not is_available:
                message = _('Usage limit has been reached')
        elif self.usage_type == ONCE_PER_CUSTOMER:
            is_available = not self.coupons.filter(
                coupon=self, customer=customer).exists()
            if not is_available:
                message = _('You have already used this coupon in one of '
                            'previous orders')
        return is_available, message

    def is_available_for_device_type(self, device_type):
        """
        Whether coupon is available to given device.
        :param device_type: device (app/web) (str)
        :return: boolean
        """
        return self.device_type == DEVICE_TYPE_ALL \
                or device_type == self.device_type

    def is_customer_limit_reached(self, customer):
        """
        Whether coupon limit reached for given customer.
        :param customer: instance of Customer
        :return: boolean
        """
        return self.limit_per_customer and self.limit_per_customer <= self.coupons.filter(
            coupon=self, customer=customer).count()

    def is_per_day_limit_reached_for_customer(self, customer, pickup_datetime):
        """
        Whether coupon per-day limit reached for given customer.
        :param customer: instance of Customer
        :param pickup_datetime: requested pickup date and time
        :return: boolean
        """
        if not pickup_datetime:
            pickup_datetime = timezone.now()
        else:
            pickup_datetime = ist_to_utc(pickup_datetime).replace(
                tzinfo=tz.tzoffset('UTC', 0))
        return self.limit_per_customer_per_day and \
               self.limit_per_customer_per_day <= self.coupons.\
                   select_related('order').filter(
                    coupon=self,
                    customer=customer,
                    order__pickup_datetime__date=pickup_datetime.date()).count()

    def is_valid_in_city(self, city):
        """
        Whether coupon is available in given city.
        :param city: name of City (str)
        :return: boolean
        """
        return any([city == i for i in [c.name for c in self.cities.all()]])

    def increase_usage_count(self):
        self.usage_count += 1
        self.save()

    def decrease_usage_count(self):
        self.usage_count -= 1
        self.save()

    def increase_redeemed_amount(self, discount):
        self.total_redeemed_amount = float(
            self.total_redeemed_amount) + float(discount)
        self.save()


class CouponChangeLog(BaseModel):
    coupon = models.ForeignKey(Coupon, on_delete=models.PROTECT)
    log = models.TextField(_('Log'))

    class Meta:
        verbose_name = _('Coupon Change Log')
        verbose_name_plural = _('Coupon Change Log')

    def __str__(self):
        return '%s' % self.coupon


class CouponRedeemHistory(BaseModel):
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    coupon = models.ForeignKey(Coupon, on_delete=models.PROTECT,
                               related_name='coupons')
    order = models.ForeignKey('order.Order', on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Coupon Redeem History')
        verbose_name_plural = _('Coupon Redeem History')

    def __str__(self):
        return '%s - %s' % (self.coupon, self.order)
