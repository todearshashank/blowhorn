from rest_framework import serializers

from blowhorn.coupon.models import Coupon


class CouponModelSerializer(serializers.ModelSerializer):
    message = serializers.SerializerMethodField(read_only=True)
    terms_and_conditions = serializers.ReadOnlyField()

    def __init__(self, *args, **kwargs):
        super(CouponModelSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    def get_message(self, obj):
        return obj.display_text

    class Meta:
        model = Coupon
