from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.admin import BaseAdmin
from blowhorn.coupon.models import Coupon, CouponChangeLog, CouponRedeemHistory
from blowhorn.coupon.constants import (AUDITLOG_FIELDS,
                                       COUPON_REPORT_FIELDSET,
                                       COUPON_FIELDS_GENERAL,
                                       COUPON_READONLY_ADD,
                                       COUPON_DRAFT,
                                       COUPON_ACTIVE,
                                       COUPON_INACTIVE,
                                       COUPON_LIST_FILTER_DEVICE_TYPE,
                                       COUPON_LIST_FILTER_USAGE_TYPE,
                                       COUPON_LIST_FILTER_DISCOUNT_TYPE,
                                       COUPON_REDEEM_LIST_FILTER_DEVICE_TYPE,
                                       COUPON_REDEEM_LIST_FILTER_USAGE_TYPE,
                                       COUPON_REDEEM_LIST_FILTER_DISCOUNT_TYPE)
from blowhorn.coupon.forms import CouponForm, CouponEditForm
from blowhorn.coupon.mixins import CouponMixin
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from blowhorn.utils.filters import simple_list_filter
from blowhorn.utils.functions import get_formatted_log


class CouponChangeLogInline(admin.TabularInline, CouponMixin):
    model = CouponChangeLog
    can_delete = False
    max_num = 5000
    extra = 0
    readonly_fields = ('modified_by', 'modified_date', 'get_log')
    exclude = ('log',)
    verbose_name = 'Changelog'
    verbose_name_plural = 'Changelog'

    def has_add_permission(self, request):
        return False

    def get_log(self, obj):
        return get_formatted_log(obj, 'log')
    get_log.allow_tags = True
    get_log.short_description = _('Log')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('modified_by', 'coupon')


class CouponRedeemHistoryInline(admin.TabularInline):
    model = CouponRedeemHistory
    can_delete = False
    max_num = 5000
    extra = 0
    readonly_fields = ('order', 'get_discount', 'get_email', 'get_city',
                       'created_date')
    exclude = ('customer',)
    verbose_name = 'Usage History'
    verbose_name_plural = 'Usage History'

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('order', 'order__user', 'order__city',
                                 'coupon')

    def get_discount(self, obj):
        return obj.order.discount
    get_discount.short_description = _('discount')

    def get_email(self, obj):
        return obj.order.user.email
    get_email.short_description = _('email')

    def get_city(self, obj):
        return '%s' % obj.order.city
    get_city.short_description = _('city')


@admin.register(Coupon)
class CouponAdmin(BaseAdmin):
    list_display = ('code', 'device_type', 'discount_type', 'discount_value',
                    'usage_limit', 'limit_per_customer',
                    'limit_per_customer_per_day', 'max_allowed_discount',
                    'start_time', 'end_time', 'is_allowed_for_house_shifting',
                    'total_redeemed_amount', 'usage_count')
    list_filter = (
        simple_list_filter(**COUPON_LIST_FILTER_DEVICE_TYPE),
        simple_list_filter(**COUPON_LIST_FILTER_DISCOUNT_TYPE),
        simple_list_filter(**COUPON_LIST_FILTER_USAGE_TYPE),
        ('start_time', DateRangeFilter))
    actions = None
    show_full_result_count = False
    actions_selection_counter = False
    search_fields = ('code',)
    conditional_inlines = [CouponChangeLogInline, CouponRedeemHistoryInline]

    def get_cities(self, obj=None):
        return ', '.join(obj.cities.all())

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (('General', {
                'fields': COUPON_FIELDS_GENERAL
            }),) + COUPON_REPORT_FIELDSET

        return (
            ('General', {
                'fields': COUPON_FIELDS_GENERAL
            }),
        )

    def get_inline_instances(self, request, obj=None):
        inlines = []
        if obj:
            inlines.append(CouponChangeLogInline(self.model, self.admin_site))
            inlines.append(CouponRedeemHistoryInline(self.model,
                                                     self.admin_site))
        return inlines

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = CouponEditForm
        else:
            self.form = CouponForm
        return super().get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            readonly_fields = ('total_redeemed_amount', 'usage_count',) + AUDITLOG_FIELDS
            if obj.status == COUPON_DRAFT or obj.status == COUPON_ACTIVE:
                return readonly_fields

            elif obj.status == COUPON_INACTIVE:
                return readonly_fields + COUPON_FIELDS_GENERAL
        return COUPON_READONLY_ADD

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('created_by', 'modified_by')
        return qs.prefetch_related('cities')

    def has_delete_permission(self, request, obj=None):
        if obj and obj.status != COUPON_DRAFT:
            return False
        return super().has_delete_permission(request, obj=obj)

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        if object_id:
            obj = self.model.objects.get(pk=object_id)
            if obj.status == COUPON_INACTIVE:
                extra_context['show_save'] = False
                extra_context['show_save_and_continue'] = False
        else:
            extra_context['show_save'] = True

        try:
            return super().change_view(request, object_id,
                                       extra_context=extra_context)
        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)


@admin.register(CouponRedeemHistory)
class CouponRedeemHistory(BaseAdmin):
    list_display = ('id', 'order_number', 'coupon_code', 'customer_email',
                    'customer_phone_number', 'discount', 'order_city',
                    'created_date',)

    list_filter = (
        'order__city',
        simple_list_filter(**COUPON_REDEEM_LIST_FILTER_DEVICE_TYPE),
        simple_list_filter(**COUPON_REDEEM_LIST_FILTER_DISCOUNT_TYPE),
        simple_list_filter(**COUPON_REDEEM_LIST_FILTER_USAGE_TYPE),
        ('created_date', DateRangeFilter))
    search_fields = ('coupon__code', 'order__number', 'customer__name',
                     'order_user__email')
    readonly_fields = ('coupon_code', 'created_date', 'order_number',
                       'order_city', 'customer_email', 'customer_phone_number',
                       'discount',)
    actions = None
    show_full_result_count = False
    fieldsets = (
        ('General', {
            'fields': ('coupon_code', 'order_number', 'order_city',
                       'customer_email', 'customer_phone_number', 'discount')
        }),
        ('Auditlog', {
            'fields': ('created_date',)
        }),
    )

    def coupon_code(self, obj=None):
        url = reverse("admin:coupon_coupon_change", args=[obj.coupon_id])
        return format_html("<a target='_blank' href='{}'>{}</a>", url,
                           obj.coupon or '-')

    def customer_email(self, obj=None):
        url = reverse("admin:users_user_change", args=[obj.order.user_id])
        return format_html("<a target='_blank' href='{}'>{}</a>", url,
                           obj.order.user.email or '-')
    customer_email.short_description = _('email')

    def customer_phone_number(self, obj=None):
        if obj:
            return obj.order.user.phone_number
        return ''
    customer_phone_number.short_description = _('phone number')

    def order_city(self, obj=None):
        if obj:
            return '%s' % obj.order.city
        return ''
    order_city.short_description = _('city')

    def order_number(self, obj=None):
        url = reverse("admin:order_bookingorder_change", args=[obj.order_id])
        return format_html("<a target='_blank' href='{}'>{}</a>", url,
                           obj.order or '-')

    def discount(self, obj=None):
        return obj.order.discount or 0

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('coupon', 'order', 'order__city',
                                 'order__user', 'customer',)

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)
