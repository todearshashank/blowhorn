from itertools import zip_longest

from django.db import connection
from django.utils.crypto import get_random_string

from blowhorn.coupon.constants import ALLOWED_CHARS_COUPON


def generate_code(length, chars=ALLOWED_CHARS_COUPON,
                  group_length=4, separator='-'):
    """Create a string of `length` chars grouped by `group_length` chars."""
    random_string = (i for i in
                     get_random_string(length=length, allowed_chars=chars))
    return separator.join(
        ''.join(filter(None, a))
        for a in zip_longest(*[random_string] * group_length)
    )


def get_unused_code(length=12, group_length=4, separator='-'):
    """Generate a code, check in the db if it already exists and return it.
    :param int length: the number of characters in the code
    :param int group_length: length of character groups separated by `seperator`
    :return: coupon code
    :rtype: str
    """
    cursor = connection.cursor()
    while True:
        code = generate_code(length, group_length=group_length,
                             separator=separator)
        cursor.execute("SELECT 1 FROM coupon_coupon WHERE code=%s", [code])
        if not cursor.fetchall():
            return code
