from django.utils.translation import ugettext_lazy as _

AUDITLOG_FIELDS = ('created_date', 'created_by', 'modified_date', 'modified_by')

# MODEL CONSTANT VALUES
ALLOWED_CHARS_COUPON = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789'
EXCLUDE_COUPON_HISTORY = ['total_redeemed_amount', 'usage_count']

COUPON_DRAFT = 'draft'
COUPON_ACTIVE = 'active'
COUPON_INACTIVE = 'inactive'
COUPON_STATUSES = (
    (COUPON_DRAFT, _('Draft')),
    (COUPON_ACTIVE, _('Active')),
    (COUPON_INACTIVE, _('Inactive'))
)


DISCOUNT_TYPE_FIXED = 'fixed'
DISCOUNT_TYPE_PERCENTAGE = 'percentage'
DISCOUNT_TYPES = (
    (DISCOUNT_TYPE_FIXED, _('Fixed')),
    (DISCOUNT_TYPE_PERCENTAGE, _('Percentage'))
)

DISCOUNT_VALUE_FORMATTER = {
    DISCOUNT_TYPE_FIXED: '₹%d',
    DISCOUNT_TYPE_PERCENTAGE: '%d%%'
}

DEVICE_TYPE_ALL = 'all'
DEVICE_TYPE_APP = 'app'
DEVICE_TYPE_WEB = 'web'
DEVICE_TYPES = (
    (DEVICE_TYPE_ALL, _('All')),
    (DEVICE_TYPE_APP, _('App')),
    (DEVICE_TYPE_WEB, _('Web'))
)
MULTI_USE, ONCE_PER_CUSTOMER = ('multi_use', 'once_per_customer')
USAGE_CHOICES = (
    (ONCE_PER_CUSTOMER, _("Can only be used once per customer")),
    (MULTI_USE, _("Can be used multiple times by multiple customers")),
)

# ADMIN
COUPON_REPORT_FIELDSET = (
    ('Usage Info', {
        'fields': ('total_redeemed_amount', 'usage_count')
    }),
    ('Auditlog', {
        'fields': AUDITLOG_FIELDS
    }),
)

COUPON_FIELDS = (
    'code', 'cities', 'status', 'device_type', 'usage_type', 'discount_type',
    'discount_value', 'max_allowed_discount', 'minimum_amount_for_eligibility',
    'usage_limit', 'limit_per_customer', 'limit_per_customer_per_day',
    'start_time', 'end_time', 'display_to_customer',
    'is_allowed_for_house_shifting'
)

COUPON_FIELDS_GENERAL = COUPON_FIELDS + ('message', )

COUPON_ADD_FORMSET = (
    ('General', {
        'fields': COUPON_FIELDS_GENERAL
    }),
)

COUPON_UPDATE_FORMSET = (
    ('General', {
        'fields': COUPON_FIELDS_GENERAL
    }),
) + COUPON_REPORT_FIELDSET

COUPON_READONLY_ADD = ()
COUPON_READONLY_UPDATE_DRAFT = ('total_redeemed_amount', 'usage_count',
                                'created_date', 'created_by', 'modified_date',
                                'modified_by',)
COUPON_READONLY_UPDATE_ACTIVE = COUPON_FIELDS + COUPON_READONLY_UPDATE_DRAFT
COUPON_READONLY_UPDATE_INACTIVE = COUPON_FIELDS_GENERAL + COUPON_READONLY_UPDATE_DRAFT
# COUPON_LIST_SELECT_RELATED = ('cities')
COUPON_SEARCH_FIELDS = ('code',)

COUPON_REDEEM_LIST_DISPLAY = ('coupon', 'order', 'customer')
COUPON_REDEEM_LIST_FILTER = COUPON_REDEEM_LIST_DISPLAY
COUPON_REDEEM_SEARCH_FIELDS = ('coupon__code', 'order__number', 'customer__name')
COUPON_REDEEM_LIST_SELECT_RELATED = ('coupon', 'order', 'customer')
COUPON_REDEEM_READONLY_FIELDS = ('coupon', 'order', 'customer') + AUDITLOG_FIELDS

# TERMS AND CONDITIONS TEMPLATES
PERCENTAGE_COUPON_DESCRIPTION = 'Enjoy, {discount_value} off upto ' \
                         '{max_allowed_discount} on your {order_count} order ' \
                         'with blowhorn!'
FIXED_COUPON_DESCRIPTION = 'Enjoy, {discount_value} discount on your ' \
                           '{order_count} order with blowhorn!'
LIMIT_PER_CUSTOMER_STR = {
    1: 'once',
    2: 'twice',
    3: 'thrice'
}
TAC_ONCE_PER_CUSTOMER = 'Valid once per user.'
TAC_NOT_VALID_FOR_HOUSE_SHIFTING = 'Not applicable for house shifting.'
TAC_MULTIUSE_LIMIT = 'Valid {limit_per_customer} per user.'
TAC_DISCOUNT_DISCLAIMER = 'Discount is applicable only on vehicle fare.'
TAC_VALID_TILL = 'Valid till {end_time}.'
TAC_MAX_DISCOUNT = 'Maximum discount will be upto ₹{max_allowed_discount}.'
TAC_PER_DAY_LIMIT = 'Valid {daily_limit} per day.'
TAC_MIN_AMOUNT_FOR_ELIGIBILITY = '{discount_value} discount on actual bill ' \
                                 'amount exceeding ₹{eligibility_fare}.'

COUPON_LIST_FILTER_DEVICE_TYPE = {
    'title': 'Device Type',
    'parameter_name': 'device_type',
    'lookup_choices': DEVICE_TYPES,
    'query_match_type': 'exact'
}

COUPON_LIST_FILTER_USAGE_TYPE = {
    'title': 'Usage Type',
    'parameter_name': 'usage_type',
    'lookup_choices': USAGE_CHOICES,
    'query_match_type': 'exact'
}

COUPON_LIST_FILTER_DISCOUNT_TYPE = {
    'title': 'Discount Type',
    'parameter_name': 'coupon__discount_type',
    'lookup_choices': DISCOUNT_TYPES,
    'query_match_type': 'exact'
}

COUPON_REDEEM_LIST_FILTER_DEVICE_TYPE = {
    'title': 'Device Type',
    'parameter_name': 'coupon__device_type',
    'lookup_choices': DEVICE_TYPES,
    'query_match_type': 'exact'
}

COUPON_REDEEM_LIST_FILTER_USAGE_TYPE = {
    'title': 'Usage Type',
    'parameter_name': 'coupon__usage_type',
    'lookup_choices': USAGE_CHOICES,
    'query_match_type': 'exact'
}

COUPON_REDEEM_LIST_FILTER_DISCOUNT_TYPE = {
    'title': 'Discount Type',
    'parameter_name': 'discount_type',
    'lookup_choices': DISCOUNT_TYPES,
    'query_match_type': 'exact'
}
