from dateutil import tz
from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from blowhorn.coupon.constants import DISCOUNT_TYPE_PERCENTAGE
from blowhorn.coupon.mixins import CouponMixin
from blowhorn.coupon.models import CouponRedeemHistory
from blowhorn.utils.functions import ist_to_utc


class CouponForm(forms.ModelForm, CouponMixin):

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)

        cleaned_code = self.cleaned_data.get('code', None)
        if cleaned_code:
            cleaned_code = self.clean_coupon_code(cleaned_code)
            if not cleaned_code:
                self.add_error('code', _('Only Alphanumeric characters allowed.'))
        self.cleaned_data['code'] = cleaned_code

        start_time = self.cleaned_data.get('start_time', None)
        end_time = self.cleaned_data.get('end_time', None)

        if start_time and end_time:
            if start_time >= end_time:
                self.add_error('start_time',
                               _('Start should be less than end time'))

            start_time = ist_to_utc(start_time).replace(
                tzinfo=tz.tzoffset('UTC', 0))
            end_time = ist_to_utc(end_time).replace(
                tzinfo=tz.tzoffset('UTC', 0))

        now = timezone.now()
        if start_time and start_time < now:
            self.add_error('start_time',
                           _('Start time cannot be a past time'))

        if end_time and end_time < now:
            self.add_error('end_time', _('End time cannot be past time'))

        if self.cleaned_data.get('discount_type', None) == \
                DISCOUNT_TYPE_PERCENTAGE and \
                not self.cleaned_data.get('max_allowed_discount', None):
            self.add_error(
                'max_allowed_discount',
                _('Max. allowed discount is mandatory if discount is percentage')
            )


class CouponEditForm(forms.ModelForm, CouponMixin):

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        cleaned_code = self.cleaned_data.get('code', None)
        if cleaned_code:
            cleaned_code = self.clean_coupon_code(cleaned_code)
            if not cleaned_code:
                self.add_error('code', _('Only Alphanumeric characters allowed.'))
        self.cleaned_data['code'] = cleaned_code

        if self.cleaned_data.get('discount_type', None) == \
                DISCOUNT_TYPE_PERCENTAGE and \
                not self.cleaned_data.get('max_allowed_discount', None):
            self.add_error('max_allowed_discount',
                _('Max. allowed discount is mandatory if discount type is '
                  'percentage'))

        start_time = self.cleaned_data.get('start_time', None)
        end_time = self.cleaned_data.get('end_time', None)
        if start_time and end_time:
            if start_time >= end_time:
                self.add_error('start_time',
                               _('Start should be less than end time'))

            start_time = ist_to_utc(start_time).replace(
                tzinfo=tz.tzoffset('UTC', 0))
            end_time = ist_to_utc(end_time).replace(
                tzinfo=tz.tzoffset('UTC', 0))

        redeem_qs = self.instance.coupons.filter(
            coupon=self.instance).order_by('order__pickup_datetime')
        if redeem_qs.exists():
            oldest_order = redeem_qs.first().order
            latest_order = redeem_qs.last().order

            if oldest_order.pickup_datetime <= timezone.now() or \
                    latest_order.pickup_datetime >= timezone.now():

                if self.initial.get('code', None) != cleaned_code:
                    self.add_error('code',
                        _('Code can\'t be changed. Already used in an order.'))

                if self.initial.get('status', None) != \
                        self.cleaned_data.get('status', None):
                    self.add_error('status',
                        _('Status can\'t be changed. Already used.'))

                if self.initial.get('discount_type', None) != \
                        self.cleaned_data.get('discount_type', None):
                    self.add_error('discount_type',
                        _('Discount type can\'t be changed. Already used in an '
                          'order.'))

                if self.initial.get('discount_value', None) != \
                        self.cleaned_data.get('discount_value', None):
                    self.add_error('discount_value', _('Discount Value can\'t '
                                                       'be changed. Already '
                                                       'used.')
                                   )

                if self.cleaned_data.get('usage_limit', 0) < \
                        self.instance.usage_count:
                    self.add_error('usage_limit',
                        _('Usage limit is lesser than number of times coupon '
                          'already used in an order.'))

            if start_time and start_time > latest_order.pickup_datetime or \
                    start_time > oldest_order.pickup_datetime:
                formatted_time = oldest_order.pickup_datetime.strftime(
                    settings.ADMIN_DATETIME_FORMAT)
                self.add_error('start_time',
                    _('Already used for an order with pickup time %s' %
                      formatted_time))

            if end_time and end_time < latest_order.pickup_datetime:
                formatted_time = latest_order.pickup_datetime.strftime(
                    settings.ADMIN_DATETIME_FORMAT)
                self.add_error('end_time',
                    _('Already used for an order with pickup time: %s' %
                        formatted_time))
