from django.conf.urls import url
from blowhorn.coupon.views import Coupon


urlpatterns = [
    url(r'^coupon/apply$', Coupon.as_view(), name='coupon-apply'),
]
