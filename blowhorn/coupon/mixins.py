import re
import logging
from datetime import datetime
from django.utils import timezone
from django.conf import settings
from django.db.models import F
from django.utils.translation import ugettext_lazy as _

from blowhorn.address.utils import get_city_from_latlong
from blowhorn.contract.models import Contract
from blowhorn.coupon.models import Coupon, CouponRedeemHistory
from blowhorn.coupon.constants import (
    DEVICE_TYPE_APP, DISCOUNT_TYPE_FIXED, TAC_MAX_DISCOUNT,
    DISCOUNT_TYPE_PERCENTAGE, MULTI_USE, ONCE_PER_CUSTOMER,
    TAC_DISCOUNT_DISCLAIMER, TAC_ONCE_PER_CUSTOMER, TAC_MULTIUSE_LIMIT,
    LIMIT_PER_CUSTOMER_STR, TAC_VALID_TILL, TAC_MIN_AMOUNT_FOR_ELIGIBILITY,
    TAC_NOT_VALID_FOR_HOUSE_SHIFTING, COUPON_ACTIVE, DEVICE_TYPE_ALL)
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.order.const import ORDER_TYPE_NOW, ORDER_TYPE_LATER
from dateutil.parser import parse

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CouponMixin(object):

    def clean_coupon_code(self, code):
        """
        :param code: raw code entered by user
        :return: removes special chars, and converts alpha to upper
        """
        if code:
            cleaned_code = re.sub(r'([^a-zA-Z0-9]+?)', '', code)
            return cleaned_code.upper()
        return None

    def get_coupon_from_code(self, code):
        """
        :param code: coupon-code (str)
        :return: instance of Coupon if coupon-code exists; otherwise None
        """
        try:
            return Coupon.objects.get(code=code.upper())
        except Coupon.DoesNotExist:
            return None

    def is_valid_for_customer(self, coupon, customer):
        """
        :param coupon: instance of Coupon
        :param customer: instance of Customer
        :return: Boolean
        """
        if not isinstance(coupon, int):
            coupon = self.get_coupon_from_code(coupon)

        return coupon.is_valid_for_customer(customer)

    def __get_limit_string(self, coupon, field):
        """
        :param coupon:
        :param field:
        :return:
        """
        val = getattr(coupon, field, 0)
        if val and val > 3:
            return '%s times' % val
        return LIMIT_PER_CUSTOMER_STR.get(val)

    def __get_terms_and_conditions(self, coupon):
        """
        :param coupon: instance of Coupon
        :return: TAC with formatted values on general template (list of strings)
        """
        terms_and_conditions = [_(TAC_DISCOUNT_DISCLAIMER)]
        if coupon.usage_type == ONCE_PER_CUSTOMER:
            terms_and_conditions.append(_(TAC_ONCE_PER_CUSTOMER))
        elif coupon.usage_type == MULTI_USE:
            if coupon.limit_per_customer:
                limit_per_customer = '%s %s' % (
                    coupon.limit_per_customer, 'times') \
                    if coupon.limit_per_customer \
                    and coupon.limit_per_customer > 3 \
                    else LIMIT_PER_CUSTOMER_STR.get(coupon.limit_per_customer)

                multi_use_str = TAC_MULTIUSE_LIMIT.format(
                    limit_per_customer=limit_per_customer)
                terms_and_conditions.append(_(multi_use_str))

        if coupon.discount_type == DISCOUNT_TYPE_PERCENTAGE:
            max_discount = TAC_MAX_DISCOUNT.format(
                max_allowed_discount=int(coupon.max_allowed_discount))
            terms_and_conditions.append(_(max_discount))

        if coupon.minimum_amount_for_eligibility:
            tac_eligibility = TAC_MIN_AMOUNT_FOR_ELIGIBILITY.format(
                discount_value=coupon.get_discount_value,
                eligibility_fare=coupon.minimum_amount_for_eligibility
            )
            terms_and_conditions.append(tac_eligibility)

        if not coupon.is_allowed_for_house_shifting:
            terms_and_conditions.append(TAC_NOT_VALID_FOR_HOUSE_SHIFTING)

        valid_till = TAC_VALID_TILL.format(
            end_time=coupon.end_time.strftime(settings.APP_DATETIME_FORMAT))
        terms_and_conditions.append(_(valid_till))
        return terms_and_conditions

    def __response_dict(self, is_valid, message, coupon=None):
        """
        :param is_valid: valid/invalid coupon (boolean)
        :param message: str
        :param coupon: instance of Coupon
        :return: dict for response data
        """
        response = {
            'message': message
        }
        if is_valid:
            response['terms_and_conditions'] = self.__get_terms_and_conditions(
                coupon)
            response['discount_type'] = coupon.discount_type
            response['discount_value'] = int(coupon.discount_value)
            if coupon.discount_type == DISCOUNT_TYPE_PERCENTAGE:
                response['max_allowed_discount'] = float(coupon.max_allowed_discount)
        return response

    def validate_coupon_data(self, user, data, is_utc_time_format=False):
        """
        :param user: instance of User
        :param data: dict with request data
        :param is_utc_time_format:
        :return: valid/invalid coupon (boolean), response data (dict)
        """
        city = data.get('city', None) or data.get('line4', None)
        coupon_code = data.get('coupon_code', None)
        timeframe = data.get('time_frame', None)
        pickup_date = data.get('date', None)
        device_type = data.get('device_type', DEVICE_TYPE_APP)
        estimated_fare = data.get('estimated_fare', None)
        contract_id = data.get('contract_id', None)

        if not coupon_code:
            logger.info('Empty coupon code from customer')
            return False, self.__response_dict(
                False, _('Please, fill the coupon code'))

        if not timeframe or (timeframe != ORDER_TYPE_NOW and not pickup_date):
            logger.info('Time frame not present')
            return False, self.__response_dict(
                False, _('Select time from WHEN section'))

        if not city:
            lat = data.get('from-lat', None)
            lng = data.get('from-lon', None)
            if not lat or not lng:
                logger.info('City is not selected, geopoint is absent')
                return False, self.__response_dict(
                    False, _('Select the city from home page'))

            city = get_city_from_latlong(latitude=lat, longitude=lng)
            if not city:
                logger.info('Not able to get city from lat-lng')
                return False, self.__response_dict(
                    False, _('Out of coverage area'))
            city = city.name

        pickup_datetime = None
        if timeframe and timeframe == ORDER_TYPE_LATER:
            if not pickup_date:
                logger.info('Pickup date not provided for book later option')
                return False, self.__response_dict(False, _('Pickup date not provided'))

            if is_utc_time_format:
                pickup_datetime = parse(pickup_date)
            else:
                pickup_datetime = datetime.strptime(
                    pickup_date, '%d %b %Y %H:%M'
                )

        coupon = self.get_coupon_from_code(coupon_code)
        if not coupon:
            logger.info('Invalid coupon code entered by customer')
            return False, self.__response_dict(
                False, _('Sorry, invalid coupon code'))

        if contract_id:
            try:
                contract = Contract.objects.get(pk=contract_id)
            except Contract.DoesNotExist:
                return False, self.__response_dict(
                    False, _('Please, select a rate card'))

            if contract.is_house_shifting_contract and \
                    not coupon.is_allowed_for_house_shifting:
                return False, self.__response_dict(
                    False,
                    _('Sorry, coupon is not valid for house shifting')
                )

        if estimated_fare and not coupon.is_eligible_fare(estimated_fare):
            logger.info('Fare %s is not eligible for coupon %s' %
                        (estimated_fare, coupon))
            return False, self.__response_dict(
                False, 'Coupon is eligible for fare above ₹%s only' %
                       coupon.minimum_amount_for_eligibility)

        if coupon.is_usage_limit_reached:
            logger.info('Usage limit reached')
            return False, self.__response_dict(
                False, _('Sorry, coupon is expired'))

        if not coupon.is_valid_in_city(city):
            logger.info('Coupon is not valid for city %s' % city)
            return False, self.__response_dict(
                False, _('Sorry, coupon is not valid in %s' % city))

        if not coupon.is_available_for_device_type(device_type):
            logger.info('Coupon is not valid for device-type %s' % device_type)
            return False, self.__response_dict(
                False, _('Sorry, coupon is valid only for booking from %s' %
                         coupon.device_type))

        if coupon.is_expired:
            logger.info('Customer entered expired coupon code')
            return False, self.__response_dict(
                False, _('Sorry, coupon is expired'))

        if not coupon.is_active(pickup_time=pickup_datetime):
            logger.info('Pickup date is not in coupon active date-range: %s' %
                        pickup_date)
            return False, self.__response_dict(
                False, _('Sorry, invalid coupon code'))

        if not user.is_authenticated:
            return False, self.__response_dict(
                False, _('Coupon is available only for registered customers'))

        customer = CustomerMixin().get_customer(user)
        is_available, message = coupon.is_available_to_customer(
            customer, pickup_datetime=pickup_datetime)
        if not is_available:
            logger.info('%s: %s' % (message, user.email))
            return False, self.__response_dict(False, message)

        return True, self.__response_dict(True, coupon.display_text,
                                          coupon=coupon)

    def validate_coupon_usage(self, coupon, order, new_pickup_datetime):
        """
        :param coupon: instance of Coupon
        :param order: instance of Order
        :param new_pickup_datetime: pickup_datetime (datetime)
        :return: valid/invalid (boolean), message (str)
        """
        if not coupon.is_active(pickup_time=new_pickup_datetime):
            return False, _('Coupon is not active for order pickup time')

        if coupon.is_usage_limit_reached:
            return False, _('Coupon has reached it\'s usage limit')

        customer = order.customer
        is_available, message = coupon.is_available_to_customer(
            customer, new_pickup_datetime)
        if not is_available:
            return False, message

        coupon.increase_usage_count()
        CouponMixin().create_redeem_history(order, customer, coupon)
        return True, ''

    def create_redeem_history(self, order, customer, coupon):
        """
        :param order: instance of Order
        :param customer: instance of Customer
        :param coupon: instance of Coupon
        :return: instance of CouponRedeemHistory
        """
        return CouponRedeemHistory.objects.create(**{
            'coupon': coupon,
            'customer': customer,
            'order': order
        })

    def __delete_redeem_history(self, order):
        """
        :param order: instance of Order
        :return: count of deleted records
        """
        return CouponRedeemHistory.objects.filter(order=order).delete()

    def __apply_fixed_discount(self, value, amount):
        """
        :param value: discount value (float/Decimal)
        :param amount: actual amount of Order (float/Decimal)
        :return: cost after discount (float)
        """
        return max(float(amount) - float(value), 0)

    def __apply_percentage_discount(self, max_allowed_discount, value, amount):
        """
        :param max_allowed_discount: discount cap value (float/Decimal)
        :param value: discount value (float/Decimal)
        :param amount: actual amount of Order (float/Decimal)
        :return: discount (float), cost after discount (float)
        """
        if max_allowed_discount:
            discount = min(float(max_allowed_discount),
                           float(amount) * float(value) / 100.0)
        else:
            discount = float(amount) * float(value) / 100.0
        return discount, (float(amount) - discount)

    def apply_coupon(self, order):
        """
        :param order: instance of Order
        :return: discount (float)
        """
        discount = 0
        amount = order.actual_cost
        coupon = order.coupon
        if coupon.minimum_amount_for_eligibility and \
                not coupon.is_eligible_fare(amount):
            return discount, amount

        if coupon.discount_type == DISCOUNT_TYPE_FIXED:
            discount = coupon.discount_value
            amount = self.__apply_fixed_discount(coupon.discount_value, amount)

        elif coupon.discount_type == DISCOUNT_TYPE_PERCENTAGE:
            discount, amount = self.__apply_percentage_discount(
                coupon.max_allowed_discount, coupon.discount_value, amount)

        if order.id and not order.total_payable:
            # for fare estimate order wont have id
            # so don't increment the redeem count
            coupon.increase_redeemed_amount(discount)
        return float(discount), float(amount)

    def get_active_coupons(self, data):
        """
        :param data: filter params (dict)
        :param is_utc_time_format: boolean
        :return: list of coupon data (list of dict)
        """
        city = data.get('city', None) or data.get('line4', None)
        timeframe = data.get('time_frame', None)
        pickup_date = data.get('date', None)
        device_type = data.get('device_type', DEVICE_TYPE_APP)
        if not city:
            lat = data.get('latitude', None)
            lng = data.get('longitude', None)
            if not lat or not lng:
                logger.info('City is not selected, geopoint is absent')
                return False, self.__response_dict(
                    False, _('Select the city from home page'))

            city = get_city_from_latlong(latitude=lat, longitude=lng)
            if not city:
                logger.info('Not able to get city from lat-lng')
                return False, self.__response_dict(
                    False, _('Out of coverage area'))
            city = city.name

        pickup_datetime = None
        if timeframe and timeframe == ORDER_TYPE_LATER:
            if not pickup_date:
                logger.info('Pickup date not provided for book later option')
                return False, self.__response_dict(False, _('Pickup date not provided'))

            pickup_datetime = parse(pickup_date)
        else:
            pickup_datetime = timezone.now()

        return True, Coupon.objects.prefetch_related(
            'cities'
        ).filter(
            display_to_customer=True,
            status=COUPON_ACTIVE,
            cities__name=city,
            start_time__lte=pickup_datetime,
            end_time__gte=pickup_datetime,
            device_type__in=[device_type, DEVICE_TYPE_ALL],
            usage_count__lt=F('usage_limit')
        )
