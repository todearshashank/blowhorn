# Django and System libraries
import logging

# 3rd party libraries
from rest_framework import views
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status

# blowhorn imports
from blowhorn.coupon.mixins import CouponMixin

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Coupon(views.APIView, CouponMixin):
    """
    Validation of given coupon code
    POST api/coupon/apply
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        data = request.data
        is_valid, data = self.validate_coupon_data(request.user, data)
        if not is_valid:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=data)

        return Response(status=status.HTTP_200_OK, data=data)
