import json
from django.utils.html import mark_safe

def get_formatted_value(obj):
    if obj and obj.changes:
        changes = json.loads(obj.changes)
        template = """
        <table class='history'>
            <thead>
                <th>Field</th>
                <th>Old</th>
                <th>New</th>
            </thead>
            <tbody>
        """
        for row in changes:
            template += """
                <tr>
                    <td>{field}</td>
                    <td>{old_val}</td>
                    <td>{new_val}</td>
                </tr>
            """.format(
                field=row.get('field'),
                old_val=row.get('old'),
                new_val=row.get('new')
            )
        template += "</tbody></table>"
        return mark_safe(template)
    return ''
