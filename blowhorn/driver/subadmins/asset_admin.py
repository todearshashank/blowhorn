from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from blowhorn.driver.submodels.asset_models import AssetCategory, Asset, \
    AssetTransaction, AssetHistory
from blowhorn.driver.submodels.constants import NOT_ISSUED
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from .format_html import get_formatted_value
from blowhorn.driver.subadmins.forms import AssetForm


class AssetTransactionInline(admin.TabularInline):
    model = AssetTransaction
    extra = 0
    can_delete = False
    readonly_fields = ('asset', 'txn_reference', 'amount_collected', 'time',
        'associate', 'remarks')

    def has_add_permission(self, request, obj=None):
        return False

class AssetHistoryInline(admin.TabularInline):
    model = AssetHistory
    extra = 0
    can_delete = False
    fields = ['modified_time', 'user', 'get_changes']
    readonly_fields = fields
    verbose_name = 'History'
    verbose_name_plural = 'History'

    def has_add_permission(self, request, obj=None):
        return False

    def get_changes(self, obj):
        return get_formatted_value(obj)

    get_changes.allow_tags = True
    get_changes.short_description = _('Changes')

    class Media:
        css = {
            'all': ('/static/admin/css/drivers.css',)
        }


@admin.register(AssetCategory)
class AssetCategory(admin.ModelAdmin):
    list_display = ('name',)

    def has_add_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

@admin.register(Asset)
class AssetAdmin(FSMTransitionMixin, admin.ModelAdmin):
    form = AssetForm
    fsm_field = ['lending_status',]
    list_display = ('driver', 'category', 'identifier', 'quantity',
        'security_deposit_collected', 'lending_status', 'recovery_amount',
        'get_balance')
    list_filter = ('driver', 'category', 'lending_status',)
    search_fields = ('identifier', 'created_by', 'modified_by')

    fieldsets = (
        ('General', {
            'fields': ('driver', 'category', 'identifier', 'quantity',
            'cost_per_asset', 'security_deposit_collected', 'lending_status',
            'recovery_amount', 'balance'),
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                'modified_by'),
        })
    )

    def get_balance(self, obj):
        return obj and obj.balance

    get_balance.short_description = _('Balance')

    def has_delete_permission(self, request, obj=None):
        if obj and obj.lending_status == NOT_ISSUED:
            return True
        return False

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['created_date', 'created_by',
            'modified_date', 'modified_by', 'balance', 'recovery_amount']

        if obj and obj.lending_status not in [NOT_ISSUED]:
            readonly_fields += ['driver', 'category', 'identifier', 'quantity',
                'cost_per_asset', 'lending_status']
        return readonly_fields

    inlines = [AssetTransactionInline, AssetHistoryInline]
