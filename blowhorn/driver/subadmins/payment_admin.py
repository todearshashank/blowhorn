from django.contrib import admin
from django.utils.translation import ugettext as _
from blowhorn.driver.models import DriverPaymentAdjustment, DriverPayment
from blowhorn.driver.admin import DriverAdjustmentAdmin, HubFilter, \
    AdjustmentCashPaidContractFilter, AdjustmentDriverFilter
from django.db.models import Sum, When, Case, FloatField, F, Q, Value, Func, ExpressionWrapper
from blowhorn.driver.models import DriverConstants
from blowhorn.utils.filters import multiple_choice_list_filter
from blowhorn.driver.constants import UNAPPROVED
from blowhorn.address.permissions import is_city_manager
from blowhorn.common.utils import AbsoluteSum

APPROVED = 'Approved'
NEW = DriverPaymentAdjustment.NEW


class AdjustmentApproval(DriverPaymentAdjustment):
    class Meta:
        proxy = True
        verbose_name = _('Adjustment Approval view')
        verbose_name_plural = _('Adjustment Approval view')


@admin.register(AdjustmentApproval)
class AdjustmentApprovalAdmin(DriverAdjustmentAdmin):
    status_filter_config = {
        'title': 'status',
        'lookup_choices': [st[0] for st in DriverPaymentAdjustment.ADJUSTMENT_STATUSES]
    }

    # multiple_choice_list_filter(**status_filter_config),

    list_filter = [
        multiple_choice_list_filter(**status_filter_config),
        'payment__contract__city', HubFilter,
        'category', AdjustmentCashPaidContractFilter,
        AdjustmentDriverFilter, 'status',
        ('payment__contract__customer', admin.RelatedOnlyFieldListFilter),
        # ('adjustment_datetime', DateFilter)
    ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        status_list = request.GET.get('status__in')
        if not request.GET.get('status') and not status_list:
            qs = qs.filter(status=DriverPaymentAdjustment.NEW)

        if status_list:
            qs = qs.filter(status__in=status_list.split(','))

        dp_record = DriverPaymentAdjustment.objects.filter(status__in=[NEW, APPROVED]).values('payment_id').\
            annotate(amount=AbsoluteSum('adjustment_amount'))
        user = request.user
        executive_limit = float(DriverConstants().get('DP_ADJUSTMENT_APPROVAL_LIMIT', 3000))
        manager_limit = float(DriverConstants().get('DP_ADJUSTMENT_APPROVAL_LIMIT_FOR_CITY_MANAGER', 3000))

        executive_eligible_ids = dp_record.filter(amount__gt=executive_limit).values_list('payment_id', flat=True)
        citymanger_eligible_ids = dp_record.filter(amount__range=(manager_limit, executive_limit)).values_list('payment_id', flat=True)

        if user.is_company_executive:
            qs = qs.filter(payment_id__in=executive_eligible_ids)
        elif user.is_city_manager:
            from blowhorn.address.models import City
            cities = City.objects.filter(managers=user).values_list('id', flat=True)
            if cities:
                qs = qs.filter(payment__contract__city_id__in=cities, payment_id__in=citymanger_eligible_ids)
            else:
                qs = qs.none()

        return qs
