from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django import forms
from django.forms.models import BaseInlineFormSet
from django.core.exceptions import ValidationError
from django.conf import settings
from datetime import datetime
from django.db.models import Q

from blowhorn.utils.readonly_inputfield import ReadOnlyInput
from blowhorn.driver.submodels.constants import ASSET_LENDING_STATUS_CHOICES, NOT_ISSUED
ERROR_MESSAGES = {
    'amount_tally_mismatch': '%s amount should be less than or equal to Cost per Asset x Number of Assets'
}


class AssetForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AssetForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.lending_status != NOT_ISSUED:
                for _field in self.fields:
                    if _field == 'category':
                        display_value = '%s' % self.instance.category
                        self.fields[_field].widget = ReadOnlyInput(display_value=display_value)
                    else:
                        self.fields[_field].widget = ReadOnlyInput()

    def clean(self, *args, **kwargs):
        error_msg = {}
        recovery_amount = self.cleaned_data.get('recovery_amount')
        security_deposit_collected = self.cleaned_data.get('security_deposit_collected')

        if self.instance.lending_status == NOT_ISSUED:
            cost_per_asset = self.cleaned_data.get('cost_per_asset')
            quantity = self.cleaned_data.get('quantity')

            total_amount = quantity * cost_per_asset
            if recovery_amount and recovery_amount > total_amount:
                error_msg['recovery_amount'] = ERROR_MESSAGES['amount_tally_mismatch'] % 'Recovery'

            if security_deposit_collected and security_deposit_collected > total_amount:
                error_msg['security_deposit_collected'] = ERROR_MESSAGES['amount_tally_mismatch'] % 'Security deposit'

            if bool(error_msg):
                raise ValidationError(error_msg)
        else:
            self.cleaned_data['category'] = self.instance.category


class AssetInlineFormset(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(AssetInlineFormset, self).__init__(*args, **kwargs)

