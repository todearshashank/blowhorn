import os
import csv
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.db.models import F, Value, CharField, Func, Min, Max
from django.utils import timezone
from datetime import timedelta
from django.http import HttpResponse

from blowhorn.driver.models import OldPayment
from blowhorn.common.admin import NonEditableAdmin
from blowhorn.utils.functions import utc_to_ist


def export_old_payment(modeladmin, request, queryset):

    old_payment_ids = queryset.values_list('id', flat=True)
    queryset = OldPayment.copy_data.filter(id__in=old_payment_ids)

    file_path = 'old_payments'

    queryset.annotate(
        created_date=Func(F('created_time') + timedelta(hours=5.5),
                          Value("DD/MM/YYYY"),
                          function='to_char',
                          output_field=CharField()),
        created_time_formated=Func(F('created_time') + timedelta(hours=5.5),
                           Value("HH24:MI"),
                           function='to_char',
                           output_field=CharField()),

        last_modified_date=Func(
            Min(F('last_modified')) + timedelta(hours=5.5),
            Value("DD/MM/YYYY"),
            function='to_char',
            output_field=CharField()
        ),
        last_modified_time=Func(
            Min(F('last_modified')) + timedelta(hours=5.5),
            Value("HH24:MI"),
            function='to_char',
            output_field=CharField()
        ),
        payment_period_start_dates=Func(
            Max(F('payment_period_start_date')) + timedelta(hours=5.5),
            Value("DD/MM/YYYY"),
            function='to_char',
            output_field=CharField()
        ),
        payment_period_start_time=Func(
            Max(F('payment_period_start_date')) + timedelta(hours=5.5),
            Value("HH24:MI"),
            function='to_char',
            output_field=CharField()
        ),
        payment_period_end_dates=Func(
            Max(F('payment_period_end_date')) + timedelta(hours=5.5),
            Value("DD/MM/YYYY"),
            function='to_char',
            output_field=CharField()
        ),
        payment_period_end_time=Func(
            Max(F('payment_period_end_date')) + timedelta(hours=5.5),
            Value("HH24:MI"),
            function='to_char',
            output_field=CharField()
        )
    ).to_csv(
        file_path,
        'old_id', 'created_date', 'created_time_formated', 'last_modified_date', 'last_modified_time',
        'driver_contract', 'sla', 'customer', 'city', 'driver',
        'leaves', 'weekly_off', 'payment_type', 'hubs', 'payment_cycle', 'extra_payment',
        'extra_hours_payment', 'payment_frequency', 'final_payment', 'deduction',
        'extra_kms_payment', 'payment_period_start_dates', 'payment_period_start_time',
        'payment_period_end_dates', 'payment_period_end_time', 'payment_month_and_year',
        'adjustment', 'base_payment', 'gross_payment', 'payment_state', 'payment_added_on',
        'payment_added_by', 'payment_reason', 'bank_account_name',
        'bank_account_number', 'bank_account_bank_name', 'bank_account_ifsc', 'adjustment_history',
    )

    modified_file = '%s_' % str(utc_to_ist(
        timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'Old Id', 'Created Date', 'Created Time', 'Last Modified Date', 'Last Modified Time',
            'Driver Contract', 'SLA', 'Customer', 'City', 'Driver',
            'Leaves', 'Weekly Off', 'Payment Type', 'Hubs', 'Payment Cycle', 'Extra Payment',
            'Extra Hours Payment', 'Payment Frequency', 'Final Payment', 'Deduction',
            'Extra KMs Payment', 'Payment Period Start Date', 'Payment Period Start Time',
            'Payment Period End Date', 'Payment Period End Time', 'Payment Month And Year',
            'Adjustment', 'Base Payment', 'Gross Payment', 'Payment State', 'Payment Added On', 'Payment Added By',
            'Payment Reason', 'Bank Account Name', 'Bank Account Number', 'Adjustment History'
            'Bank Account Bank Name', 'Bank Account Ifsc',
        ])
        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1

        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(
                fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + \
                os.path.basename(modified_file)
            os.remove(modified_file)
            return response


@admin.register(OldPayment)
class OldPaymentAdmin(NonEditableAdmin):

    actions = [export_old_payment]
    list_display = (
        'old_id', 'created_time', 'driver_contract', 'sla', 'customer', 'city', 'driver',
        'leaves', 'weekly_off', 'payment_type', 'hubs', 'payment_cycle', 'extra_payment',
        'extra_hours_payment', 'payment_frequency', 'final_payment', 'deduction',
        'extra_kms_payment', 'payment_month_and_year', 'base_payment', 'gross_payment',
        'payment_state', 'payment_added_by', 'payment_reason', 'bank_account_name',
        'bank_account_number', 'bank_account_bank_name', 'bank_account_ifsc', 'adjustment', 'adjustment_history',
    )
