import math
import simplejson as json
from config.settings import status_pipelines as StatusPipeline
from django.conf import settings
from datetime import timedelta
import pytz
from django.core.files.base import ContentFile
from django.utils import timezone
from django.forms.models import model_to_dict
from django.db.models import Sum

from blowhorn.trip.models import Trip
from blowhorn.customer.submodels.invoice_models import CustomerInvoice
from blowhorn.customer.constants import APPROVED
from blowhorn.contract.constants import (
    VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES,
    VEHICLE_SERVICE_ATTRIBUTE_MINUTES,
    VAS_SERVICE_ATTRIBUTE_FLOOR_NUMBER,
    VAS_SERVICE_ATTRIBUTE_NUMBER_OF_ITEMS,
    VAS_SERVICE_ATTRIBUTE_POD,
    VEHICLE_SERVICE_ATTRIBUTE_HOURS,
    CONTRACT_TYPE_C2C, CONTRACT_TYPE_KIOSK, VAS_SERVICE_ATTRIBUTE_PICKUP_FLOOR_NUMBER,
    VAS_SERVICE_ATTRIBUTE_DROPPOFF_FLOOR_NUMBER)
# from blowhorn.driver.constants import VEHICLE_THEORETICAL_SPEED_LIMIT_AVG_KMPH

DATE_FORMAT = "%d-%b-%Y"


class PaymentHelper(object):
    def __init__(self):
        self.calculation = {}

    def _get_filtered_slabs(self, source_slabs, attributes, vas_slab=False):
        slabs = []
        for slab in source_slabs:
            if not vas_slab and slab.attribute in attributes:
                slabs.append(slab)
            elif vas_slab and slab.vas_attribute and (slab.vas_attribute.display_attribute \
                                                      or slab.vas_attribute.attribute) in attributes:
                slabs.append(slab)
        return slabs

    def _get_vehicle_slabs(self, attributes):
        slabs = self._get_filtered_slabs(self.vehicle_slabs, attributes)
        return slabs

    def _get_vas_slabs(self, vas_slabs, attributes, vas_slab=True):
        return self._get_filtered_slabs(vas_slabs, attributes, vas_slab)

    def _get_slab_amount(self, slab, value):
        amount = 0
        if slab.end is None:
            print('End slab is None. Skipping slab calculation')
            return amount

        if value > slab.end:
            slab_diff = slab.end - slab.start
            amount += float(slab.fixed_amount) or float(math.ceil(slab_diff / slab.step_size)) * float(
                slab.rate)
        elif value >= slab.start:
            slab_diff = value - slab.start
            amount += float(slab.fixed_amount) or float(math.ceil(slab_diff / slab.step_size)) * float(
                slab.rate)
        return amount

    def _get_amount_for_given_slabs(self, trip, slabs, value,
                                    loading_unloading_time=0):
        amount = 0
        calculation = []
        if loading_unloading_time:
            value += loading_unloading_time

        for slab in slabs:
            slab_amount = self._get_slab_amount(slab, float(value))
            amount += float(slab_amount)
            calculation.append({
                'attribute': slab.attribute,
                'slab': model_to_dict(slab),
                'date': timezone.now().date().strftime(DATE_FORMAT),
                'value': value,
                'amount': slab_amount,
            })

        return amount, calculation

    def get_night_min_used(self, trip, end):
        return self.get_total_night_time(trip, end)

    def get_overlap_time(self,start, end, nstart, nend):
        overlapping_time = 0
        if start < nstart <= end:
            if end >= nend:
                overlapping_time = (nend - nstart).total_seconds()/60
            elif end < nend:
                overlapping_time = (end - nstart).total_seconds()/60
        elif nstart < start <= nend:
            if end <= nend:
                overlapping_time = (end - start).total_seconds()/60
            elif end > nend:
                overlapping_time = (nend - nstart).total_seconds() / 60
        return overlapping_time

    def get_total_night_time(self, trip, end):
        nstart = trip.customer_contract.city.night_hours_start_time
        nend = trip.customer_contract.city.night_hours_end_time
        # total_days = (end - trip.actual_start_time).days + 1
        # for count in range(total_days):
        #     day = trip.actual_start_time.date() + timedelta(day=count)
        # assumption is trip wont last more then 24hr
        tz = pytz.timezone(settings.ACT_TIME_ZONE)
        day =  trip.actual_start_time.astimezone(tz)

        night_hrs_start = day.replace(hour=nstart.hour, minute=nstart.minute)
        night_hrs_end = (day + timedelta(days=1)).replace(hour=nend.hour, minute=nend.minute)
        return self.get_overlap_time(trip.actual_start_time, end, night_hrs_start, night_hrs_end)

    def _get_amount_for_service(self, trip, attribute, value):
        self.calculation['over_distance'] = []
        vehicle_slabs = self._get_vehicle_slabs([attribute])
        amount, calculation = self._get_amount_for_given_slabs(
            trip, vehicle_slabs, float(value))
        return amount, calculation

    def get_total_amount(self, sell_rate, order, trip=None, labour_charge=0):
        """
        Returns contract for given city and vehicle class
        """
        amount = 0
        distance_amount = 0
        time_amount = 0
        # vas_floor_amount, vas_floor_calculation = 0, {}
        # vas_item_amount, vas_item_calculation = 0, {}
        d_calculation = {}
        t_calculation = {}
        calculation = {}
        # dist_value = 0.0
        # time_value = 0.0

        self.order = order
        if sell_rate:
            labour_cost = order.get_labour_cost() or labour_charge
            if trip:
                # Distance
                dist_value = (trip.gps_distance or 0 \
                    if trip.status not in StatusPipeline.TRIP_END_STATUSES \
                    else 0) + (order.trip_set.filter(
                    status__in=StatusPipeline.TRIP_END_STATUSES) \
                    .aggregate(distance=Sum('gps_distance')) \
                    .get('distance', 0) or 0)

                # Time
                end_time = trip.actual_end_time \
                    if trip.actual_end_time else timezone.now()
                time_diff = (end_time - trip.actual_start_time).total_seconds()
                time_value = (float("%0.2f" % (time_diff / 60)) \
                    if trip.status not in StatusPipeline.TRIP_END_STATUSES \
                    else 0) + (order.trip_set.filter(
                    status__in=StatusPipeline.TRIP_END_STATUSES) \
                    .aggregate(time=Sum('total_time')).get('time', 0) or 0)
            else:
                dist_value = order.estimated_distance_km or 0
                time_value = order.estimated_time_in_mins

            dist_value = max(dist_value , order.estimated_distance_km or 0)

            #to do driver payment
            # if trip and not trip.total_distance:
            #     trip.total_distance = float(dist_value)
            #     Trip.objects.filter(id=trip.id).update(total_distance=dist_value)

            self.vehicle_slabs = sell_rate.sellvehicleslab_set.all()
            if self.vehicle_slabs:
                if dist_value > 0:
                    distance_amount, d_calculation = self._get_amount_for_service(
                        trip, VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES, dist_value)

                if time_value > 0:
                    time_amount, t_calculation = self._get_amount_for_service(
                        trip, VEHICLE_SERVICE_ATTRIBUTE_MINUTES, time_value)

            else:
                print('Vehicle Slabs not defined .. skipping..')

            vas_amount = 0
            cal = []
            # vas_attributes = vas_slabs.distinct('vas_attribute__attribute')
            for slab in sell_rate.sellvasslab_set.all():
                attribute = None
                if slab.vas_attribute:
                    if slab.vas_attribute.display_attribute:
                        attribute = slab.vas_attribute.display_attribute
                    elif slab.vas_attribute.attribute:
                        attribute = slab.vas_attribute.attribute

                # applicable_slab = self._get_vas_slabs(vas_slabs, attribute) if attribute else None
                val = 0
                if attribute == VAS_SERVICE_ATTRIBUTE_FLOOR_NUMBER:
                    val = order.floor_number
                elif attribute == VAS_SERVICE_ATTRIBUTE_PICKUP_FLOOR_NUMBER:
                    val = 0
                    if order.pickup_address and not order.pickup_address.is_service_lift_available and labour_cost > 0:
                        val = order.pickup_address.floor_number or 0
                elif attribute == VAS_SERVICE_ATTRIBUTE_DROPPOFF_FLOOR_NUMBER:
                    val = 0
                    if order.shipping_address and not order.shipping_address.is_service_lift_available and labour_cost > 0:
                        val = order.shipping_address.floor_number or 0
                elif attribute == VAS_SERVICE_ATTRIBUTE_NUMBER_OF_ITEMS:
                    val = order.number_of_items
                elif attribute == VAS_SERVICE_ATTRIBUTE_POD:
                    val = 1 if order.is_pod_required else 0 # there is no count of pod stored in order hardcoding it for now.

                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_MINUTES:
                    val = 0
                    if trip:
                        val = self.get_night_min_used(trip, end_time)
                elif attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                    val = time_value/60

                slab_amount = self._get_slab_amount(slab, float(val))
                cal.append({
                    'attribute': str(slab.vas_attribute) if attribute in [VEHICLE_SERVICE_ATTRIBUTE_MINUTES,
                                                                          VEHICLE_SERVICE_ATTRIBUTE_HOURS]  else attribute,
                    'slab': model_to_dict(slab),
                    'date': timezone.now().date().strftime(DATE_FORMAT),
                    'value': val,
                    'amount': slab_amount,
                })

                # if applicable_slab:
                #     cal_amount, cal = \
                #         self._get_amount_for_given_slabs(
                #             trip,
                #             [slab],
                #             val)
                vas_amount += slab_amount

            if sell_rate.base_pay is None:
                sell_rate.base_pay = 0

            goods_carrying_charge = 0
            if labour_cost > 0:
                if order.pickup_address and not order.pickup_address.is_lift_accessible:
                    goods_carrying_charge += float(sell_rate.charge_for_carrying_goods or 0)
                if order.shipping_address and not order.shipping_address.is_lift_accessible:
                    goods_carrying_charge += float(sell_rate.charge_for_carrying_goods or 0)

            amount += float(sell_rate.base_pay) + distance_amount + \
                      time_amount + vas_amount + goods_carrying_charge

            calculation['fixed'] = int(sell_rate.base_pay)
            calculation['time'] = t_calculation
            calculation['distance'] = d_calculation
            calculation['vas_amount'] = cal
            if goods_carrying_charge:
                calculation['charge_for_carrying_goods'] = goods_carrying_charge
            # calculation['vas_floor'] = vas_floor_calculation
            calculation['amount_excl_tax'] = float(amount)
            return amount, calculation

    def calculate_discount(self, order):
        from blowhorn.coupon.mixins import CouponMixin
        return CouponMixin().apply_coupon(order)

    def create_invoice(self, calculation, order, trip=None):
        time_amount = 0
        time_cal = calculation.get('time')
        if time_cal:
            for time in time_cal:
                time_amount += time.get('amount')

        dist_amount = 0
        dist_cal = calculation.get('distance')
        if dist_cal:
            for dist in dist_cal:
                dist_amount += dist.get('amount')

        cgst_percent = 0
        cgst_amount = 0
        sgst_percent = 0
        sgst_amount = 0

        tax_dist = calculation['tax_distribution']

        if tax_dist:
            cgst_percent = tax_dist['CGST']['percentage']
            cgst_amount = tax_dist['CGST']['amount']

            sgst_percent = tax_dist['SGST']['percentage']
            sgst_amount = tax_dist['SGST']['amount']

        invoice_number = CustomerInvoice().generate_invoice_number(
            timezone.now().date(), self.order.city.state_set.all()[0].code)

        calculation['invoice_number'] = invoice_number

        if not trip:
            trip = Trip.objects.filter(order=order, status=StatusPipeline.TRIP_COMPLETED).first()

        defaults = {
            'invoice_number': invoice_number,
            'customer': self.order.customer,
            'contract': self.order.customer_contract,
            'service_period_start': trip.actual_start_time if trip.actual_start_time else timezone.now(),
            'service_period_end': trip.actual_end_time if trip.actual_end_time else timezone.now(),
            'charges_basic': calculation.get('fixed', 0),
            'status': APPROVED,
            'basic_pay_calculation': {
                'count_all_working_days': 1,
                'count_distinct_drivers': 1,
                'distribution_days': 1
            },
            'bh_source_state': self.order.order_state,
            'charges_service': time_amount + dist_amount,
            'basic_per_day': calculation.get('fixed'),
            'days_counted': 1,
            'taxable_amount': calculation.get('amount_excl_tax'),
            'cgst_percentage': cgst_percent,
            'cgst_amount': cgst_amount,
            'sgst_percentage': sgst_percent,
            'sgst_amount': sgst_amount,
            'igst_percentage': 0,
            'igst_amount': 0,
            'total_amount': calculation.get('amount_incl_tax'),
            'modified_date': timezone.now(),
            "timestamp_approved": timezone.now()

        }
        customer_invoice = CustomerInvoice(**defaults)

        #        bytes = InvoiceHelper.export_invoice_to_pdf(customer_invoice, self.order)
        bytes = False

        if bytes:
            content = ContentFile(bytes)
            customer_invoice.file.save(str(invoice_number) + ".pdf", content, save=True)
            customer_invoice.save()
        else:
            customer_invoice.save()
