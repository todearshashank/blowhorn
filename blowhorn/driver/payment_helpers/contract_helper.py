import calendar
from datetime import timedelta
from blowhorn.oscar.core.loading import get_model
PaymentInterval = get_model('contract', 'PaymentInterval')
Trip = get_model('trip', 'Trip')
DriverPayment = get_model('driver', 'DriverPayment')
Driver = get_model('driver', 'Driver')

# these days are not present all the months
TROUBLE_DAYS = [29, 30, 31]


class ContractHelper(object):
    def __init__(self, contract):
        self.contract = contract
        self.contract_terms = contract.contractterm_set.all()

    def get_previous_date_from_reference_matching_day(self, reference, day):
        """ Function to get a date given a reference date and a day
            Ex: Ref = 05-Dec-2017, day=01 ==> 01-Dec-2017
                Ref = 15-Jan-2018, day=28 ==> 28-Dec-2017
                Ref = 05-Mar-2018, day=31 ==> 01-Mar-2018 (instead of 31-Feb)

            The absence of the day in the month will result in get the next
            valid date in the forward direction
        """
        next_date = None
        prev_month = None
        month_switchover = False
        for i in range(31):
            nd = reference - timedelta(days=i)
            month = nd.month

            # if the required day lies in the trouble days, then we have to
            # figure out when in the linear search we should stop.
            # if the month switches over to previous one and the given day
            # is one of 29, 30, 31, then we got to stop here. Take the next
            # date as we have gone past.
            if prev_month and prev_month != month:
                month_switchover = True

            if month_switchover and day in TROUBLE_DAYS \
                and nd.day not in TROUBLE_DAYS:
                next_date = nd + timedelta(days=1)
                break

            if day == nd.day:
                next_date = nd
                break

            prev_month = month

        return next_date

    def get_next_date_from_reference_matching_day(self, reference, day):
        """ Function to get a date given a reference date and a day
            Ex: Ref = 05-Dec-2017, day=01 ==> 01-Jec-2018
                Ref = 15-Jan-2017, day=28 ==> 28-Jan-2018
                Ref = 05-Feb-2017, day=31 ==> 28-Feb-2018
        """
        next_date = None
        prev_month = None
        month_switchover = False
        for i in range(31):
            nd = reference + timedelta(days=i)
            month = nd.month

            # if the required day lies in the trouble days, then we have to
            # figure out when in the linear search we should stop.
            # if the month switches over to previous one and the given day
            # is one of 29, 30, 31, then we got to stop here. Take the previous
            # date as we have gone past.
            if prev_month and prev_month != month:
                month_switchover = True

            if month_switchover and day in TROUBLE_DAYS \
                and nd.day not in TROUBLE_DAYS:
                next_date = nd - timedelta(days=1)
                break

            if day == nd.day:
                next_date = nd
                break

            prev_month = month

        return next_date

    def get_payment_month_for_given_date(self, reference_date):
        """ Get the very first and last date of the month containing this date
            spanning all cycles defined
            1-15, 16-31
                01-Dec-2017 => 01-Dec-2017 and 31-Dec-2017
                15-Dec-2017 => 01-Dec-2017 and 31-Dec-2017
                02-Nov-2017 => 01-Nov-2017 and 30-Nov-2017
            26-10, 11-25
                01-Dec-2017 => 26-Nov-2017 and 25-Dec-2017
                15-Dec-2017 => 26-Nov-2017 and 25-Dec-2017
                27-Dec-2017 => 26-Dec-2017 and 25-Jan-2018
        """
        intervals = list(self.contract.payment_cycle.paymentinterval_set.all())
        first_day = intervals[0].start_day
        last_day = intervals[-1].end_day

        start_date = self.get_previous_date_from_reference_matching_day(reference_date, first_day)
        #        # go backwards and find the date on which the first day matches
        #        for i in range(31):
        #            prev_date = reference_date - timedelta(days=i)
        #            if first_day == prev_date.day:
        #                start_date = prev_date
        #                break

        # go forwards and find the date on which the last day matches
        end_date = self.get_next_date_from_reference_matching_day(reference_date, last_day)

        if start_date is None or end_date is None:
            assert False, 'Unable to find the Cycle Start and dates for %s. Found %s to %s' % (
                reference_date, start_date, end_date)

        if (end_date - start_date).days > 31:
            assert False, 'Delta too high in start and end date calculation. Ref: %s, Got %s to %s' % (
                reference_date, start_date, end_date)

        return (start_date, end_date)

    def get_payment_cycle_for_given_date(self, reference_date, cycle_type='payment_cycle'):
        """ Get the first and last date of the Cycle this date falls under """
        start_date = end_date = None
        matched_interval = None

        start_date_and_interval_list = []

        cycle = getattr(self.contract, cycle_type, None)

        assert cycle, "Cycle not configured. Aborting."

        intervals = cycle.paymentinterval_set.all()

        for interval in intervals:
            if interval.start_day <= interval.end_day and \
                reference_date.day not in range(interval.start_day, interval.end_day + 1):
                continue

            potential_start_date = self.get_previous_date_from_reference_matching_day(
                reference_date, interval.start_day)
            start_date_and_interval_list.append((potential_start_date, interval))

        # We have a set of start dates for each interval for the given reference date.
        # only one interval is required which is the closest. Fetch that.
        # get the one with the latest start date
        start_date, matched_interval = sorted(start_date_and_interval_list)[-1]

        # Interval has been fetched. Go forwards and match the last day
        end_date = self.get_next_date_from_reference_matching_day(
            reference_date, matched_interval.end_day)

        if start_date is None or end_date is None:
            assert False, 'Unable to find the Cycle Start and dates for %s. Found %s to %s' % (
                reference_date, start_date, end_date)

        if (end_date - start_date).days > 31:
            assert False, 'Delta too high in start and end date calculation. Ref: %s, Got %s to %s' % (
                reference_date, start_date, end_date)

        return (start_date, end_date)

    def get_contract_term(self, date):
        """ Get the applicable contract term for given date """
        terms_before = [term for term in self.contract_terms if term.wef <= date]
        if not terms_before:
            return None

        sorted_terms = sorted(terms_before, key=lambda x: x.wef)
        return sorted_terms[-1]

    def get_buy_rate(self, date, contract, driver):
        """ Get the applicable buy rate for given date """
        term = self.get_contract_term(date)
        if not term:
            return None

        if driver and driver.partner:
            # middle mile driver will have partner associated with him
            return term.middle_mile_buy_rate

        buy_rate = term.first_mile_buy_rate if (driver and contract.is_inter_city_contract and
                                                    driver.operating_city == contract.source_city) else term.buy_rate
        return buy_rate

    def get_sell_rate(self, date):
        """Returns the applicable sell rate for given date.
        """

        term = self.get_contract_term(date)
        if not term:
            return None

        return term.sell_rate

    def get_slabs_for_given_rate_package(self, cls, rate_package_attr,
                                         rate_package_val):
        return cls.objects.filter(**{rate_package_attr: rate_package_val})
