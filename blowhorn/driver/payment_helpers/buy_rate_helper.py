import logging
import math
from blowhorn.oscar.core.loading import get_model
from django.forms.models import model_to_dict
import simplejson as json
import decimal
from config.settings import status_pipelines as StatusPipeline

from blowhorn.contract.constants import (
    CONTRACT_TYPE_SPOT,
    BASE_PAYMENT_CYCLE_DAILY,
    BASE_PAYMENT_CYCLE_WEEK,
    BASE_PAYMENT_CYCLE_FORTNIGHTLY,
    BASE_PAYMENT_CYCLE_MONTH,
    AGGREGATE_CYCLE_DAILY,
    AGGREGATE_CYCLE_CYCLE,
    BASE_PAYMENT_CYCLE_PER_TRIP,
    AGGREGATE_CYCLE_MONTH,
    AGGREGATE_CYCLE_TRIP,
    AGGREGATE_CYCLE_FIRST_TRIP,
    AGGREGATE_CYCLE_SECOND_TRIP,
    AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP,
    # PAY_CYCLE_CYCLE,
    PAY_CYCLE_MONTH,
    VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES,
    VEHICLE_SERVICE_ATTRIBUTE_MINUTES,
    VEHICLE_SERVICE_ATTRIBUTE_HOURS,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
    SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
    SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED,

    CONTRACT_TYPE_C2C, DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS, DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS,DIMENSION_SERVICE_ATTRIBUTE_GRAMS)

SHIPMENT_ATTRIBUTES = [
    VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES,
    VEHICLE_SERVICE_ATTRIBUTE_HOURS,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
    SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
    SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED,
]

DIMENSION_ATTRIBUTES = [
    DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS,
    DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS,
    DIMENSION_SERVICE_ATTRIBUTE_GRAMS,
]

PaymentInterval = get_model('contract', 'PaymentInterval')
Trip = get_model('trip', 'Trip')
DriverPayment = get_model('driver', 'DriverPayment')
Driver = get_model('driver', 'Driver')
DATE_FORMAT = "%d-%b-%Y"


class BuyRateHelper(object):
    def __init__(self, buy_rate):
        self.is_ceiling_applicable = False
        self.buy_rate = buy_rate
        self.vehicle_slabs = buy_rate.buyvehicleslab_set.all()
        self.shipment_slabs = buy_rate.buyshipmentslab_set.all()
        self.dimension_slabs = buy_rate.buydimensionslab_set.all()
        self.calculation = {}

    def has_bonus_slabs(self):
        vehicle_bonus_slabs = [slab for slab in self.vehicle_slabs if \
                               slab.split_payment
                               ]
        shipment_bonus_slabs = [slab for slab in self.shipment_slabs if \
                                slab.split_payment
                                ]

        dimension_bonus_slabs = [slab for slab in self.dimension_slabs if \
                                 slab.split_payment
                                 ]

        return bool(vehicle_bonus_slabs or shipment_bonus_slabs or dimension_bonus_slabs)

    def _get_base_amount(self, trip_set, contingency_trip=None):
        amount_per_day = 0.0
        rate = self.buy_rate
        completed_statuses = [StatusPipeline.TRIP_COMPLETED,
                              StatusPipeline.TRIP_SCHEDULED_OFF] \
            if not rate.base_payment_interval == BASE_PAYMENT_CYCLE_PER_TRIP else [
            StatusPipeline.TRIP_COMPLETED]

        if rate.base_payment_interval == BASE_PAYMENT_CYCLE_DAILY:
            amount_per_day = rate.driver_base_payment_amount
        elif rate.base_payment_interval == BASE_PAYMENT_CYCLE_WEEK:
            amount_per_day = rate.driver_base_payment_amount / 7
        elif rate.base_payment_interval == BASE_PAYMENT_CYCLE_FORTNIGHTLY:
            amount_per_day = rate.driver_base_payment_amount / 15
        elif rate.base_payment_interval == BASE_PAYMENT_CYCLE_MONTH:
            amount_per_day = rate.driver_base_payment_amount / 30

        days_to_consider = trip_set.get_total_work_days()

        is_blowhorn_contract = trip_set.is_blowhorn_contract()

        # If base payment is per day, only consider days with completed trips
        if rate.base_payment_interval == BASE_PAYMENT_CYCLE_DAILY:
            days_to_consider = trip_set.get_total_completed_trip_days()

        tripset_basepay_amount = 0
        unplanned_days = 0
        trip_basepay_calculation = []

        if not is_blowhorn_contract:
            tripset_basepay_amount, trip_basepay_calculation = trip_set.get_tripset_basepay_amount()
            unplanned_days = trip_set.get_unplanned_working_days()

        # If the base payment defined is per trip then
        # compute it for all the trips
        if rate.base_payment_interval == BASE_PAYMENT_CYCLE_PER_TRIP:
            amount_per_day = rate.driver_base_payment_amount
            if is_blowhorn_contract:
                days_to_consider = trip_set.get_no_of_completed_trips()
            else:
                days_to_consider = trip_set.get_no_of_completed_trips() - trip_set.get_no_of_unplanned_trips()
                # Since we are subtracting the unplanned trips count in `days_to_consider` itself, we
                # should not subtract `unplanned_days` from `days_to_consider` again, so making it 0 in
                # this case
                unplanned_days = 0

        # No contingency/spot payments for blowhorn driver,so calculating only amount per day * days
        if is_blowhorn_contract:
            value = amount_per_day * days_to_consider

        else:
            if contingency_trip and contingency_trip.status in completed_statuses:
                value = contingency_trip.trip_base_pay or amount_per_day
                amount_per_day = contingency_trip.trip_base_pay or amount_per_day
                trip_basepay_calculation = []
                logging.info(
                    'Contingency calc: %s, %s' % (contingency_trip.trip_base_pay, contingency_trip))

            else:
                value = amount_per_day * (days_to_consider - unplanned_days)
                value = value + tripset_basepay_amount

        if isinstance(value, decimal.Decimal):
            value = float(value)

        calculation = {
            'rate': amount_per_day,
            'days': days_to_consider - unplanned_days,
            'amount': amount_per_day * (days_to_consider - unplanned_days),
            'base_payment_cycle': rate.base_payment_interval,
            'trip_basepay_calculation': trip_basepay_calculation
        }

        return value, calculation

    def _get_deduction_amount(self, trip_set):
        no_show_days = trip_set.get_number_of_no_show_days() \
            if self.buy_rate.base_payment_interval != BASE_PAYMENT_CYCLE_PER_TRIP else 0

        value = (self.buy_rate.driver_deduction_per_leave * no_show_days)

        excess_deduction = trip_set.get_deduction_amount()

        calculation = {
            'rate': self.buy_rate.driver_deduction_per_leave,
            'days': no_show_days,
            'amount': value,
            'excess_deduction': excess_deduction
        }
        return value, calculation, excess_deduction

    def _get_attribute_value(self, trip_set, attribute, interval):
        values = None
        if interval in [AGGREGATE_CYCLE_TRIP,
                        AGGREGATE_CYCLE_FIRST_TRIP,
                        AGGREGATE_CYCLE_SECOND_TRIP,
                        AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP]:
            if attribute == VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES:
                values = trip_set.get_tripwise_distances()
            elif attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                values = trip_set.get_tripwise_times_in_hrs()
            elif attribute in SHIPMENT_ATTRIBUTES:
                values = trip_set.get_tripwise_shipmentvalue(attribute)
            elif attribute in VEHICLE_SERVICE_ATTRIBUTE_MINUTES:
                values = trip_set.get_tripwise_triptimes()
            elif attribute == DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS:
                values = trip_set.get_tripwise_distances()
            elif attribute == DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS:
                values = trip_set.get_tripwise_volumes()
            elif attribute == DIMENSION_SERVICE_ATTRIBUTE_GRAMS:
                values = trip_set.get_tripwise_grams()
        else:
            if attribute == VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES:
                values = trip_set.get_daywise_distances()
            elif attribute == VEHICLE_SERVICE_ATTRIBUTE_HOURS:
                values = trip_set.get_daywise_triptimes()
            elif attribute in SHIPMENT_ATTRIBUTES:
                values = trip_set.get_daywise_shipmentvalue(attribute)
            elif attribute in VEHICLE_SERVICE_ATTRIBUTE_MINUTES:
                values = trip_set.get_daywise_triptime_in_minutes()
            elif attribute == DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS:
                values = trip_set.get_daywise_weights()
            elif attribute == DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS:
                values = trip_set.get_daywise_volumes()
            elif attribute == DIMENSION_SERVICE_ATTRIBUTE_GRAMS:
                values = trip_set.get_daywise_grams()

        if values is None:
            assert False, 'Attribute values cannot be None'

        if interval in [AGGREGATE_CYCLE_CYCLE, AGGREGATE_CYCLE_MONTH]:
            return sum([x[1] for x in values])

        if interval in [AGGREGATE_CYCLE_DAILY]:
            values = trip_set.get_daywise_data(values)

        return values

    def _get_slab_amount(self, slab, value, trip_count_of_the_day=None):
        amount = 0
        value_extra = 0
        fixed_amount = 0
        if slab.end is None:
            logging.info('End slab is None. Skipping slab calculation')
            return amount, value_extra, fixed_amount

        if slab.step_size <= 0:
            logging.error('Slab step-size is zero. Skipping slab calculation')
            return amount, value_extra, fixed_amount

        if (slab.interval == AGGREGATE_CYCLE_FIRST_TRIP and trip_count_of_the_day != 1) or \
            (
                slab.interval == AGGREGATE_CYCLE_SECOND_TRIP and trip_count_of_the_day != 2) or \
            (
                slab.interval == AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP and trip_count_of_the_day < 3):
            logging.info('Invalid trip for the slab')
            return amount, value_extra, fixed_amount

        if slab.is_ceiling_applicable:
            self.is_ceiling_applicable = True
            value = math.ceil(value)

        # range is not considering the float values
        if slab.start <= value <= slab.end:  # value in range(slab.start, slab.end + 1):
            value_extra = value - slab.start
            amount += float(value_extra) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount
        elif value > slab.end and self.buy_rate.apply_step_function:
            value_extra = slab.end - slab.start
            amount += float(value_extra) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount
        elif value > slab.start and self.buy_rate.apply_step_function:
            value_extra = (float(value) - float(slab.start))
            amount += float(value_extra) * float(slab.rate) / float(slab.step_size)
            fixed_amount = slab.fixed_amount
        if isinstance(amount, decimal.Decimal):
            amount = float(amount)
        return amount, value_extra, float(fixed_amount)

    def _get_amount_for_given_slabs(self, trip_set, slabs):
        amount = 0
        fixed_amount = 0
        calculation = []
        rate = self.buy_rate
        for slab in slabs:
            attr_values = self._get_attribute_value(trip_set, slab.attribute, slab.interval)
            if isinstance(attr_values, list):
                if slab.interval in [AGGREGATE_CYCLE_TRIP,
                                     AGGREGATE_CYCLE_FIRST_TRIP,
                                     AGGREGATE_CYCLE_SECOND_TRIP,
                                     AGGREGATE_CYCLE_THIRD_ONWARDS_TRIP]:
                    prev_day = None
                    trip_count_of_the_day = 0
                    for day, value, trip in attr_values:
                        if day.date() == prev_day:
                            trip_count_of_the_day += 1
                        else:
                            trip_count_of_the_day = 1
                        print(trip)
                        print(trip_count_of_the_day)
                        print(slab)
                        print(value)
                        slab_amount, value_extra, fixed_amount_ = self._get_slab_amount(slab, value,
                                                                                       trip_count_of_the_day)

                        base_amount = 0
                        if rate.base_payment_interval == BASE_PAYMENT_CYCLE_PER_TRIP:
                            base_amount = rate.driver_base_payment_amount

                        amount += slab_amount
                        fixed_amount += fixed_amount_
                        calculation.append({
                            'attribute': slab.attribute,
                            'slab': model_to_dict(slab),
                            'date': day.strftime(DATE_FORMAT),
                            'value': value,
                            'amount': slab_amount,
                            'value_extra': value_extra,
                            'trip': trip.trip_number,
                            'base_amount': base_amount,
                            'labour_cost': trip_set.get_labour_cost_for_trip(trip),
                            'toll_charges': trip_set.get_toll_charges_for_trip(trip),
                            'parking_charges': trip_set.get_parking_charges_for_trip(trip),
                            'cash_collected': trip_set.get_cash_collected_for_trip(trip)
                        })
                        prev_day = day.date()
                else:
                    for day, value in attr_values:
                        slab_amount, value_extra, fixed_amount_ = self._get_slab_amount(slab, value)
                        amount += slab_amount
                        fixed_amount += fixed_amount_
                        calculation.append({
                            'attribute': slab.attribute,
                            'slab': model_to_dict(slab),
                            'date': day.strftime(DATE_FORMAT),
                            'value': value,
                            'amount': slab_amount,
                            'value_extra': value_extra,
                        })
            else:
                slab_amount, value_extra, fixed_amount_ = self._get_slab_amount(slab, attr_values)
                amount += slab_amount
                fixed_amount += fixed_amount_
                calculation.append({
                    'attribute': slab.attribute,
                    'slab': model_to_dict(slab),
                    'value': attr_values,
                    'amount': slab_amount,
                    'value_extra': value_extra,
                })

        return amount, calculation, fixed_amount

    def _get_filtered_slabs(self, source_slabs, attributes, bonus):
        """ filter the slabs based on the given set of attributes and
            also whether it is a bonus pay.
            Bonus means select only PAY_CYCLE_MONTH only
            otherwise every matching one except PAY_CYCLE_MONTH
        """
        slabs = []

        for slab in source_slabs:
            if slab.attribute in attributes:
                if bonus and slab.split_payment:
                    slabs.append(slab)
                elif not bonus and not slab.split_payment:
                    slabs.append(slab)

        return slabs

    def _get_vehicle_slabs(self, attributes, bonus):
        slabs = self._get_filtered_slabs(self.vehicle_slabs, attributes, bonus)
        return slabs

    def _get_shipment_slabs(self, attributes, bonus):
        slabs = self._get_filtered_slabs(self.shipment_slabs, attributes, bonus)
        return slabs

    def _get_dimension_slabs(self, attributes, bonus):
        slabs = self._get_filtered_slabs(self.dimension_slabs, attributes, bonus)
        return slabs

    def _get_distance_amount(self, trip_set, bonus):
        self.calculation['over_distance'] = []
        amount = 0
        fixed_amount = 0
        slabs = self._get_vehicle_slabs([VEHICLE_SERVICE_ATTRIBUTE_KILOMETRES], bonus)
        amount, calculation, fixed_amount = self._get_amount_for_given_slabs(trip_set, slabs)
        if fixed_amount:
            calculation.append({'fixed_amount': fixed_amount})
        return amount + fixed_amount, calculation

    def _get_volume_amount(self, trip_set, bonus):
        self.calculation['over_volume'] = []
        slabs = self._get_dimension_slabs([DIMENSION_SERVICE_ATTRIBUTE_CUBIC_CMS], bonus)
        amount, calculation, fixed_amount = self._get_amount_for_given_slabs(trip_set, slabs)
        if fixed_amount:
            calculation.append({'fixed_amount': fixed_amount})
        return amount + fixed_amount, calculation

    def _get_weight_amount(self, trip_set, bonus):
        self.calculation['over_volume'] = []
        slabs = self._get_dimension_slabs([DIMENSION_SERVICE_ATTRIBUTE_KILOGRAMS], bonus)
        amount, calculation, fixed_amount = self._get_amount_for_given_slabs(trip_set, slabs)
        if fixed_amount:
            calculation.append({'fixed_amount': fixed_amount})
        return amount + fixed_amount, calculation

    def _get_gram_amount(self, trip_set, bonus):
        self.calculation['over_volume'] = []
        slabs = self._get_dimension_slabs([DIMENSION_SERVICE_ATTRIBUTE_GRAMS], bonus)
        amount, calculation, fixed_amount = self._get_amount_for_given_slabs(trip_set, slabs)
        if fixed_amount:
            calculation.append({'fixed_amount': fixed_amount})
        return amount + fixed_amount, calculation


    def _get_time_amount(self, trip_set, bonus):
        amount = 0
        attribute = [VEHICLE_SERVICE_ATTRIBUTE_HOURS, VEHICLE_SERVICE_ATTRIBUTE_MINUTES]

        slabs = self._get_vehicle_slabs(attribute, bonus)
        amount, calculation, fixed_amount = self._get_amount_for_given_slabs(trip_set, slabs)
        if fixed_amount:
            calculation.append({'fixed_amount': fixed_amount})
        return amount + fixed_amount, calculation

    def _get_shipment_amount(self, trip_set, bonus):
        amount = 0
        fixed_amount = 0
        slabs = self._get_shipment_slabs(
            [
                SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
                SHIPMENT_SERVICE_ATTRIBUTE_PICKED,
                SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
                SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
                SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
                SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
                SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
                SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
                SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
                SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
                SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
                SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED,
                SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
                SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
            ],
            bonus
        )

        amount, calculation, fixed_amount = self._get_amount_for_given_slabs(trip_set, slabs)
        if fixed_amount:
            calculation.append({'fixed_amount': fixed_amount})
        return amount + fixed_amount, calculation

    def get_payment_data(self, trip_set, bonus=None, contingency_trip=None):
        if bonus is None:
            logging.info('Bonus not specified. Need True or False. Returning!!')
            return {}
        calculation = {}
        base_amount = 0
        cash_collected = 0
        tip_amount = 0
        leave_deduction = 0
        labour_cost = 0
        toll_charges = 0
        parking_charges = 0
        excess_deduction = 0
        if not bonus:
            base_amount, b_calculation = self._get_base_amount(trip_set,
                                                               contingency_trip=contingency_trip)
            leave_deduction, l_calculation, excess_deduction = self._get_deduction_amount(trip_set)
            cash_collected = trip_set.get_cash_collected_amount()
            tip_amount = trip_set.get_tip_amount()
            labour_cost = trip_set.get_labour_cost()

            toll_charges = trip_set.get_toll_charges()
            parking_charges = trip_set.get_parking_charges()

            calculation['base'] = b_calculation
            calculation['deduction'] = l_calculation

        distance_amount, d_calculation = self._get_distance_amount(trip_set, bonus)
        time_amount, t_calculation = self._get_time_amount(trip_set, bonus)

        shipment_parameters = trip_set.get_shipment_parameters()
        shipment_amount, s_calculation = self._get_shipment_amount(trip_set, bonus)

        weight_amount, w_calculation = self._get_weight_amount(trip_set, bonus)
        volume_amount, v_calculation = self._get_volume_amount(trip_set, bonus)

        calculation['time'] = t_calculation
        calculation['distance'] = d_calculation
        calculation['shipment'] = s_calculation
        calculation['weight'] = w_calculation
        calculation['volume'] = v_calculation

        json_calculation = json.dumps(calculation)

        earnings = base_amount + distance_amount + time_amount + shipment_amount + weight_amount + volume_amount

        return {
            'total_distance': trip_set.get_total_distance(),
            'total_time': trip_set.get_total_triptime(),

            'absent_days': trip_set.get_number_of_no_show_days() \
                if self.buy_rate.base_payment_interval != BASE_PAYMENT_CYCLE_PER_TRIP else 0,

            'base_payment': base_amount,
            'leave_deduction': leave_deduction,
            'total_deductions': leave_deduction,
            'distance_payment': distance_amount,
            'time_payment': time_amount,
            'weight_payment': weight_amount,
            'volume_payment': volume_amount,
            'total_earnings': earnings,


            'total_shipment_delivered': shipment_parameters.get('delivered'),
            'total_shipment_attempted': shipment_parameters.get('attempted'),
            'total_shipment_rejected': shipment_parameters.get('rejected'),
            'total_package_returned': shipment_parameters.get('returned'),
            'total_package_pickups': shipment_parameters.get('pickups'),
            'total_package_delivered_cash': shipment_parameters.get('delivered_cash'),
            'total_package_delivered_mpos': shipment_parameters.get('delivered_mpos'),
            'total_shipment_amount': shipment_amount,
            'calculation': json_calculation,
            'labour_cost': labour_cost,
            'toll_charges': toll_charges,
            'parking_charges': parking_charges,
            'cash_collected': cash_collected,
            'tip_amount': tip_amount,
            'excess_deduction': excess_deduction
        }


t_base = """Base Amount:
  %2d days x %-9.2f\t\t\t\t\t\t%10.2f
"""

t_trip_basepay = """   %s:\t\t\t\t\t\t%10.2f
"""

trip_wise_calculation = """ \n%s \n  Base amount  \t\t\t\t\t\t\t%10.2f\n"""

t_deduction = """Deductions:
  %2d days x %-9.2f\t\t\t\t\t\t%10.2f
\n"""

t_variable = """  %s: %d %s [Slab: %2d - %2d] @ %-9.2f \t\t%10.2f
"""
tw_variable = """  %s: %d %s [Slab: %2d - %2d] @ %-9.2f \t\t%10.2f
"""

ruler = """---------------------------------------------------------------------------
"""
gross_pay = """%-20s\t\t\t\t\t\t%10.2f
"""

trip_amount = """Trip Payment \t\t\t\t\t\t\t%10.2f
"""

labour_and_toll = """  %-20s\t\t\t\t\t\t%10.2f
"""

order_cash_collected = """Cash Collected \t\t\t\t\t\t\t%10.2f\n"""

total_cash_collected = """Total Cash Collected \t\t\t\t\t\t%10.2f\n"""

earnings = """ Earnings \t\t\t\t\t\t%10.2f\n"""


def construct_trip_data(driver_payment):
    paymenttrip = driver_payment.paymenttrip_set.all()
    paymenttrip = paymenttrip.select_related('trip')

    try:
        data = json.loads(driver_payment.calculation)
    except:
        return ''

    time = data.get('time')
    variable = (
        ('distance', 'Km', data.get('distance')),
        ('weight', 'Kg', data.get('weight')),
        ('Volume', 'CC', data.get('weight')),
    )

    for t in time:
        time_attribute = t.get('attribute')[:3]
        variable += ('time', time_attribute, data.get('time')),
        break

    r = ''
    total_trip_amount = 0
    cash_collected = 0

    for pt in paymenttrip:
        trip_data = {}
        trip_total = 0
        for title, unit, components in variable:  # runs 2 times time and distance loop
            fixed_amount_title = 'Fixed Amount' + '(' + title + '):'
            fixed_amount = components[-1].get('fixed_amount', 0.0) if components else None
            if fixed_amount:
                r += gross_pay % (fixed_amount_title, components[-1].get('fixed_amount'))
            for d in data.get(title, []):
                if pt.trip.trip_number == d['trip']:
                    cash_collected = d.get('cash_collected', 0)
                    slab = d.get('slab', {})
                    var_data = t_variable % (
                        d.get('date', slab.get('interval')), d.get('value'), unit,
                        slab.get('start'), slab.get('end'), slab.get('rate'), d.get('amount'))

                    if "base_amount" not in trip_data:
                        r += trip_wise_calculation % (d.get('trip'), d.get('base_amount'))
                        r += labour_and_toll % ('Labour:', d.get('labour_cost'))
                        r += labour_and_toll % ('Toll Charges:', d.get('toll_charges'))
                        trip_total += d.get('base_amount') + d.get('labour_cost') + d.get(
                            'toll_charges')
                    if not var_data in trip_data:
                        r += var_data
                        trip_total += d.get('amount')
                    trip_data.update({
                        var_data: d.get('amount'),
                        "base_amount": d.get('base_amount'),
                        "labour_cost": d.get('labour_cost'),
                        "toll_charges": d.get('toll_charges'),
                        "parking_charges": d.get('parking_charges'),
                        "cash_collected": d.get('cash_collected'),
                    })
        r += ruler
        r += trip_amount % (trip_total)
        total_trip_amount += trip_total
        r += order_cash_collected % (-cash_collected)
        r += ruler
        r += gross_pay % ('Remaining Payable:', trip_total - cash_collected)
        r += ruler

    r += gross_pay % ('Total Trip Amount:', total_trip_amount)
    r += gross_pay % ('Adjustments:', driver_payment.adjustment)
    r += gross_pay % ('Tips', driver_payment.tip_amount)
    r += gross_pay % ('Gross Pay:', driver_payment.gross_pay)
    r += gross_pay % ('Cash Paid:', -driver_payment.cash_paid)
    r += gross_pay % ('Total Cash Collected:', -driver_payment.cash_collected)
    r += ruler
    r += gross_pay % ('Net Payable:', driver_payment.net_pay)
    return r


def driver_payment_calculation_formatter(driver_payment):
    if driver_payment.buy_rate.base_payment_interval == BASE_PAYMENT_CYCLE_PER_TRIP \
        and driver_payment.contract.contract_type == CONTRACT_TYPE_C2C:
        return construct_trip_data(driver_payment)
    r = ''
    data = json.loads(driver_payment.calculation)
    base = data.get('base')
    if base and base.get('amount') != 0:
        r += t_base % (base.get('days'), base.get('rate'), base.get('amount'))
    if base and base.get('trip_basepay_calculation'):
        trip_basepay = base.get('trip_basepay_calculation')
        if trip_basepay:
            r += '\nSupply Cost(Base Amount):\n'
            for i in trip_basepay:
                if i.get('trip_number') and i.get('base_pay'):
                    r += t_trip_basepay % (i.get('trip_number'), i.get('base_pay'))

    variable_components = (
        ('Extra Kms', 'Km', data.get('distance')),
        ('Packages', '', data.get('shipment')),
    )
    if data.get('volume', ''):
        variable_components += (('Extra Volume', 'CC', data.get('volume')),)

    if data.get('weight', ''):
        variable_components += (('Extra Weight', 'KG', data.get('weight')),)


    time = data.get('time')
    attribute = None
    for t in time:
        attribute = t.get('attribute')[:3]
        break

    if attribute:
        variable_components += ('Overtime', attribute, data.get('time')),
    all_vc = ''
    for title, unit, components in variable_components:
        vc = ''
        fixed_amount_title = 'Fixed Amount' + '(' + title + '):'
        fixed_amount = components[-1].get('fixed_amount', 0.0) if components else None
        if fixed_amount:
            r += gross_pay % (fixed_amount_title, components[-1].get('fixed_amount'))
        for t in components:
            slab = t.get('slab', {})
            amount = t.get('amount')
            if not amount or amount == 0:
                continue

            vc += t_variable % (t.get('date', slab.get('interval')), t.get('value'), unit,
                                slab.get('start'), slab.get('end'), slab.get('rate'),
                                t.get('amount'))
        all_vc += '%s\n' % title if vc else ''
        all_vc += vc

    if all_vc:
        r += '\nVariable Components:\n'
        r += all_vc

    r += ruler
    r += gross_pay % ('Total Earnings:', driver_payment.total_earnings)
    r += ruler

    deduction = data.get('deduction')
    if deduction:
        r += t_deduction % (deduction.get('days'), deduction.get('rate'),
                            -deduction.get('amount') if deduction.get('amount') > 0 else 0.00)
        if deduction.get('excess_deduction'):
            r += gross_pay % ('Excess Deductions:', deduction.get('excess_deduction'))
    r += ruler
    r += gross_pay % ('Adjustments:', driver_payment.adjustment)
    r += gross_pay % ('Toll Charges:', (float(driver_payment.toll_charges)))
    r += gross_pay % ('Parking Charges:', (float(driver_payment.parking_charges)))
    r += gross_pay % ('Labour:', (float(driver_payment.labour_cost)))
    r += ruler
    r += gross_pay % ('Tips:', driver_payment.tip_amount)
    r += gross_pay % ('Gross Pay:', driver_payment.gross_pay)
    r += gross_pay % ('Cash Paid:', -driver_payment.cash_paid)
    r += gross_pay % ('Cash Collected:', -driver_payment.cash_collected)
    r += ruler
    r += gross_pay % ('Net Payable:', driver_payment.net_pay)

    return r
