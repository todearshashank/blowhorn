import logging
import math
import collections
from datetime import datetime, timedelta
from django.db.models.functions import Cast
from django.db.models.fields import DateField
from collections import defaultdict
from django.db.models import Q, F, Count, ExpressionWrapper, DateTimeField
from django.conf import settings

from blowhorn.contract.models import SellVASSlab
from blowhorn.oscar.core.loading import get_model
import decimal
from blowhorn.utils.functions import IST_OFFSET_HOURS
from blowhorn.common.utils import camel_case_to_snake_case
from config.settings import status_pipelines as StatusPipeline
from blowhorn.order.models import Order
from blowhorn.order.const import PAYMENT_MODE_CASH, PAYMENT_STATUS_PAID
from blowhorn.contract.constants import (
    CONTRACT_TYPE_HYPERLOCAL,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED,
    SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED,
    SHIPMENT_SERVICE_ATTRIBUTE_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS,
    SHIPMENT_SERVICE_ATTRIBUTE_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_MPOS,
    SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED,
    CONTRACT_TYPE_SHIPMENT,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED,
    SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED,
    CONTRACT_TYPE_FIXED,
)

PaymentInterval = get_model('contract', 'PaymentInterval')
Trip = get_model('trip', 'Trip')
DriverPayment = get_model('driver', 'DriverPayment')
Driver = get_model('driver', 'Driver')


VALID_STATUSES = [
    StatusPipeline.TRIP_COMPLETED,
    StatusPipeline.TRIP_SCHEDULED_OFF,
    StatusPipeline.TRIP_NEW,
    StatusPipeline.TRIP_IN_PROGRESS,
    StatusPipeline.TRIP_NO_SHOW,
    StatusPipeline.TRIP_SUSPENDED,
]

expression_planned_start_time_ist = ExpressionWrapper(
        F('planned_start_time') + timedelta(hours=IST_OFFSET_HOURS), output_field=DateTimeField()
    )


class TripSet(object):

    def __init__(self, trips, **kwargs):
        self.trips = trips
        self.number_of_working_days = 0
        self.number_of_days_worked = 0
        self.number_of_unplanned_working_days = 0
        self.number_of_no_shows = 0
        self.new_and_inprogress_trips = []
        self.completed_trips = []
        self.scheduled_off_trips = []
        self.other_trips = []
        self.completed_and_scheduled_off_trips = []
        self.valid_trips = []
        self.unplanned_trips = []
        self.active_days = set()
        self.contract = kwargs.pop("contract", None)
        self.cost_per_labour = self.contract.labourconfiguration.payment_per_labour \
              if self.contract and hasattr(self.contract, 'labourconfiguration') else 0
        self.bucket_type = kwargs.pop("bucket_type", None)
        self.bucket_value = kwargs.pop("bucket_value", None)
        self.contract_type = kwargs.pop("contract_type", None)
        self.dp_contract_type = kwargs.pop("dp_contract_type", None)
        if trips:
            sorted_trips = sorted(trips, key=lambda x: x.planned_start_time)
            completed_statuses = [StatusPipeline.TRIP_COMPLETED]
            for trip in sorted_trips:
                if trip.status in completed_statuses:
                    self.completed_trips.append(trip)
                    self.completed_and_scheduled_off_trips.append(trip)
                    self.active_days.add(trip.actual_start_time.date())
                elif trip.status == StatusPipeline.TRIP_SCHEDULED_OFF:
                    self.scheduled_off_trips.append(trip)
                    self.completed_and_scheduled_off_trips.append(trip)
                else:
                    self.other_trips.append(trip)

                if trip.status in [StatusPipeline.TRIP_NEW, StatusPipeline.TRIP_IN_PROGRESS]:
                    self.new_and_inprogress_trips.append(trip)

                if trip.status in VALID_STATUSES:
                    self.valid_trips.append(trip)

        if self.valid_trips:
            first_date = self.valid_trips[0].planned_start_time_ist.date()
            last_date = self.valid_trips[-1].planned_start_time_ist.date()
            self.number_of_working_days = (last_date - first_date).days + 1

            worked_dates = set([x.planned_start_time_ist.date()
                                for x in self.completed_and_scheduled_off_trips])
            self.number_of_days_worked = len(worked_dates)

            self.number_of_no_shows = self.number_of_working_days - self.number_of_days_worked

            # Considers the trips where base pay is entered at trip level for spot
            # and fixed orders
            [self.unplanned_trips.append(i) for i in self.completed_trips if
             not i.is_contingency and i.trip_base_pay]

        # logging.info('\t\t\tWindow Size = %d, DaysWorked = %d, Trips: Completed = %d,
        # ScheduledOff = %d, Others = %d' % (
        #     self.number_of_working_days, self.number_of_days_worked,
        #     len(self.completed_trips), len(self.scheduled_off_trips), len(self.other_trips)))

        if self.unplanned_trips:
            unplanned_dates = set([x.planned_start_time_ist.date()
                                   for x in self.unplanned_trips])
            self.number_of_unplanned_working_days = len(unplanned_dates)

        self.data_complete = (len(self.new_and_inprogress_trips) == 0)

    def get_total_work_days(self):
        return self.number_of_working_days

    def get_total_completed_trip_days(self):
        return self.number_of_days_worked

    def get_number_of_no_show_days(self):
        return self.number_of_no_shows

    def get_unplanned_working_days(self):
        return self.number_of_unplanned_working_days

    def _get_daywise_trips(self, trips):
        daywise_trips = collections.defaultdict(set)

        for trip in trips:
            daywise_trips[trip.planned_start_time_ist.date()].add(trip)
        return daywise_trips

    def _get_daywise_trips_v2(trips):
        daywise_trips = collections.defaultdict(set)

        for trip in trips:
            daywise_trips[trip.planned_start_time_ist.date()].add(trip)
        return daywise_trips

    def get_no_of_completed_trips(self):
        return len(self.completed_trips)

    def get_no_of_unplanned_trips(self):
        return len(self.unplanned_trips)

    def get_daywise_triptimes(self):
        """
        Returns hours for each day for the `trip_set`.
        """

        trip_times = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)

        for day, trips in daywise_trips.items():
            total_trip_time_for_the_day = math.ceil(
                sum([(trip.actual_end_time - trip.actual_start_time).total_seconds() / 3600.0
                     for trip in trips if (isinstance(trip.actual_start_time, datetime)
                                           and isinstance(trip.actual_end_time, datetime))
                     ])
            )
            trip_times.append((day, total_trip_time_for_the_day))

        return trip_times

    def get_daywise_weights(self):
        """
        Returns hours for each day for the `trip_set`.
        """

        trip_weights = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)

        for day, trips in daywise_trips.items():
            total_weight_for_the_day = math.ceil(
                sum([trip.weight for trip in trips if trip.weight])
            )
            trip_weights.append((day, total_weight_for_the_day))

        return trip_weights

    def get_daywise_volumes(self):
        """
        Returns hours for each day for the `trip_set`.
        """

        trip_volumes = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)

        for day, trips in daywise_trips.items():
            total_volume_for_the_day = math.ceil(
                sum([trip.volume for trip in trips if trip.volume])
            )
            trip_volumes.append((day, total_volume_for_the_day))

        return trip_volumes

    def get_daywise_grams(self):
        """
        Returns hours for each day for the `trip_set`.
        """

        trip_grams = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)

        for day, trips in daywise_trips.items():
            total_weight_for_the_day = math.ceil(
                sum([trip.weight*1000 for trip in trips if trip.weight])
            )
            trip_grams.append((day, total_weight_for_the_day))

        return trip_grams


    def get_daywise_vas_slab(self):
        trip_base_pay = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)
        for day, trips in daywise_trips.items():
            total_trip_base_pay_for_the_day = math.ceil(
                sum([trip.trip_base_pay for trip in trips if trip.trip_base_pay])
            )
            trip_base_pay.append((day, total_trip_base_pay_for_the_day))

        return trip_base_pay

    def get_daywise_data(self, values):
        """
        Returns the value for each day for the list of tuples.
        """

        data = {}

        for day, value in values:
            if not day in data.keys():
                data[day] = 0
            data[day] += value

        return list(data.items())

    def get_day_wise_trip_minutes(self):
        """
        Returns minutes for each day for the `trip_set`.
        """

        trip_minutes_by_day = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)
        for day, trips in daywise_trips.items():
            total_trip_minutes_for_the_day = math.ceil(
                sum([(trip.actual_end_time - trip.actual_start_time).total_seconds() / 60.0
                     for trip in trips if (isinstance(trip.actual_start_time, datetime)
                                           and isinstance(trip.actual_end_time, datetime))
                     ])
            )
            trip_minutes_by_day.append((day, total_trip_minutes_for_the_day))
        return trip_minutes_by_day

    def get_labour_cost(self):
        labour_amount = 0.0
        amount = 0.0
        for trip in self.completed_trips:
            try:
                # all order wont have labour so using try catch block
                labour_cost = self.cost_per_labour
                labour_count = trip.order.labour.no_of_labours
                amount = labour_cost * labour_count
            except:
                amount = 0.0

            labour_amount += float(amount)
        return float(labour_amount)

    def get_cash_collected_amount(self):
        cash_collected = 0.0
        for trip in self.completed_trips:
            order = trip.order
            if order and order.payment_mode == PAYMENT_MODE_CASH and order.payment_status == PAYMENT_STATUS_PAID:
                cash_collected += float(order.total_payable) - float(order.amount_paid_online)
            # if order and order.order_type == settings.DEFAULT_ORDER_TYPE:
            #     cash_collected += float(order.extra_charges)
        return cash_collected

    def get_tip_amount(self):
        tip = 0.0
        for trip in self.completed_trips:
            order = trip.order
            if order and order.tip and order.payment_status == PAYMENT_STATUS_PAID:
                tip += float(order.tip)
        return tip

    def get_labour_cost_for_trip(self, trip):
        labour_cost = 0.0

        try:
            # all order wont have labour so using try catch block
            contract = trip.customer_contract
            cost = contract.labourconfiguration.payment_per_labour if hasattr(contract, 'labourconfiguration') else 0
            # labour_cost = trip.customer_contract.labour.labour_cost
            labour_count = trip.order.labour.no_of_labours
            labour_cost = cost * labour_count
        except:
            labour_cost = 0.0

        return float(labour_cost)

    def get_cash_collected_for_trip(self, trip):
        cash_collected = 0.0
        order = trip.order
        if order and order.payment_mode == PAYMENT_MODE_CASH and order.payment_status == PAYMENT_STATUS_PAID:
            cash_collected = float(order.total_payable) - float(order.amount_paid_online)

        return cash_collected

    def get_toll_charges_for_trip(self, trip):
        toll_charges = 0.0

        if trip.order:
            toll_charges = float(trip.order.toll_charges) or float(trip.toll_charges)
        else:
            # for shipment trips
            toll_charges = float(trip.toll_charges)

        return float(toll_charges)

    def get_toll_charges(self):
        toll_charges = 0.0
        for trip in self.completed_trips:
            if trip.order:
                toll_charges += float(trip.order.toll_charges) or float(trip.toll_charges)
            else:
                # for shipment trips
                toll_charges += float(trip.toll_charges)

        return toll_charges

    def get_parking_charges_for_trip(self, trip):
        parking_charges = 0.0

        if trip.order:
            parking_charges = float(trip.parking_charges)

        return float(parking_charges)

    def get_parking_charges(self):
        parking_charges = 0.0
        for trip in self.completed_trips:
            if trip.order:
                parking_charges += float(trip.parking_charges)

        return parking_charges

    def get_tripwise_triptimes(self):
        trip_times = []

        for trip in self.completed_trips:
            if trip.total_time is None:
                trip.total_time = 0
            trip_times.append((trip.planned_start_time_ist, trip.total_time, trip))

        return trip_times

    def get_daywise_triptime_in_minutes(self):
        trip_minutes = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)

        for day, trips in daywise_trips.items():
            total_time_for_the_day = math.ceil(
                sum([trip.total_time for trip in trips if trip.total_time])
            )
            trip_minutes.append((day, total_time_for_the_day))
        return trip_minutes

    def get_daywise_distances(self):
        trip_distances = []
        daywise_trips = self._get_daywise_trips(self.completed_trips)

        for day, trips in daywise_trips.items():
            total_distance_for_the_day = math.ceil(
                sum([trip.total_distance for trip in trips if trip.total_distance])
            )
            trip_distances.append((day, total_distance_for_the_day))
        return trip_distances

    def get_tripwise_times(self):
        trip_times = []

        for trip in self.completed_trips:
            trip_times.append((trip.planned_start_time_ist, trip.total_time, trip))

        return trip_times

    def get_tripwise_times_in_hrs(self):
        trip_times = []

        for trip in self.completed_trips:
            trip_times.append((trip.planned_start_time_ist, decimal.Decimal(trip.total_time/60), trip))

        return trip_times

    def get_tripwise_times_in_min(self):
        trip_times = []

        for trip in self.completed_trips:
            trip_times.append((trip.planned_start_time_ist, decimal.Decimal(trip.total_time), trip))

        return trip_times

    def get_tripwise_weights(self):
        trip_times = []

        for trip in self.completed_trips:
            trip_times.append((trip.planned_start_time_ist, trip.weight, trip))

        return trip_times


    def get_tripwise_volumes(self):
        trip_times = []

        for trip in self.completed_trips:
            trip_times.append((trip.planned_start_time_ist, trip.volume, trip))

        return trip_times

    def get_tripwise_grams(self):
            trip_times = []

            for trip in self.completed_trips:
                trip_times.append((trip.planned_start_time_ist, trip.weight * 1000, trip))

            return trip_times

    def get_tripwise_defcom_vas_slab(self):
        trip_defcom_sell_rate = []
        for trip in self.completed_trips:
            trip_defcom_sell_rate.append((trip.planned_start_time_ist, decimal.Decimal(trip.trip_base_pay), trip))

        return trip_defcom_sell_rate

    def get_tripwise_distances(self):
        trip_distances = []

        for trip in self.completed_trips:
            trip_distances.append((trip.planned_start_time_ist, trip.total_distance, trip))

        return trip_distances

    def get_tripwise_shipmentvalue(self, attribute):
        shipment_values = []

        for trip in self.completed_trips:
            shipment_value = 0
            shipment_value = self._get_attribute_value_from_packages(
                trip, attribute)
            shipment_values.append((trip.planned_start_time_ist, shipment_value, trip))
        return shipment_values

    def _get_attribute_value_from_packages(self, trip, attribute):
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED:
            return trip.assigned
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_PICKED:
            return trip.picked
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED:
            return trip.delivered
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_ATTEMPTED:
            return trip.attempted
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_REJECTED:
            return trip.rejected
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_PICKUPS:
            return trip.pickups
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_RETURNED:
            return trip.returned
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_MPOS:
            return trip.delivered_mpos
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_CASH_PACKAGES:
            return trip.delivered_cash
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED:
            try:
                return (trip.assigned - trip.attempted + trip.returned)
            except:
                return 0
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_REJECTED:
            try:
                return (trip.assigned - trip.attempted + trip.rejected)
            except:
                return 0
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_DELIVERED_P_REJECTED:
            try:
                return (trip.delivered + trip.rejected)
            except:
                return 0
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED:
            try:
                return (trip.assigned - trip.attempted)
            except:
                return 0
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_ATTEMPTED_P_RETURNED_P_REJECTED:
            try:
                return (trip.assigned - trip.attempted + trip.rejected + trip.returned)
            except:
                return 0
        if attribute == SHIPMENT_SERVICE_ATTRIBUTE_ASSIGNED_M_RETURNED:
            try:
                return (trip.assigned - trip.returned)
            except:
                return 0

    def get_daywise_shipmentvalue(self, attribute):
        if self.contract_type == [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]:
            orders = Order.objects.filter(customer_contract=self.contract, pickup_datetime__date__in=self.active_days)
            orders = orders.filter(**{self.bucket_type: self.bucket_value})
            orders = orders.filter(status__in=[StatusPipeline.ORDER_DELIVERED, StatusPipeline.DELIVERY_ACK])
            shipment_count_date_wise = list(orders.distinct().extra(
                {'pickup_date': "date(pickup_datetime)"}
            ).values('pickup_date').annotate(pickup_count=Count('id')).values_list('pickup_date', 'pickup_count'))
        else:
            shipment_count_date_wise = []
            daywise_trips = self._get_daywise_trips(self.completed_trips)
            for day, trips in daywise_trips.items():
                for trip in trips:
                    shipment_value = self._get_attribute_value_from_packages(
                        trip, attribute)
                    shipment_count_date_wise.append((day, shipment_value))
        return shipment_count_date_wise

    def get_total_triptime(self):
        return sum([x[1] for x in self.get_daywise_triptimes()])

    def get_total_weight(self):
        return sum([x[1] for x in self.get_daywise_weights()])

    def get_total_volume(self):
        return sum([x[1] for x in self.get_daywise_volumes()])

    def get_total_distance(self):
        return sum([x[1] for x in self.get_daywise_distances()])

    def get_total_weight_grams(self):
        return sum([x[1] for x in self.get_daywise_grams()])

    def get_shipment_parameters(self):
        attempted = delivered = returned = rejected = pickups = delivered_cash \
            = delivered_mpos = assigned = 0

        for trip in self.completed_trips:
            attempted += trip.attempted
            delivered += trip.delivered
            returned += trip.returned
            rejected += trip.rejected
            pickups += trip.pickups
            delivered_cash += trip.delivered_cash
            delivered_mpos += trip.delivered_mpos
            assigned += trip.assigned

        return {
            'attempted': attempted,
            'delivered': delivered,
            'returned': returned,
            'rejected': rejected,
            'pickups': pickups,
            'delivered_cash': delivered_cash,
            'delivered_mpos': delivered_mpos,
            'assigned': assigned,
        }

    def get_count_work_days_driver_wise(self):
        """
        Returns the number of work days for each driver.

        response : {<driver_id_1>: <total_days_worked>, <driver_id_2>: <total_days_worked>, }
        """

        trip_date_by_driver_id = self.trips.filter(
            ~Q(planned_start_time=None), ~Q(invoice_driver_id=None)
        ).annotate(
            date=Cast('planned_start_time_ist', DateField())
        ).values_list('invoice_driver_id', 'date').distinct()

        trip_date_by_driver_id = set(trip_date_by_driver_id)

        count_work_days_by_driver = defaultdict(int)
        for (driver_id, date) in trip_date_by_driver_id:
            count_work_days_by_driver[driver_id] += 1

        return count_work_days_by_driver

    def get_count_trips_driver_wise(self):
        """
        Returns the number of trips for each driver.
        response : {<driver_id_1>: <total_trips>, <driver_id_2>: <total_trips>, }
        """

        count_trips_by_driver = defaultdict(int)
        for trip in self.trips:
            count_trips_by_driver[trip.invoice_driver_id] += 1

        return count_trips_by_driver

    def get_trips_by_aggregate_level(self, aggregate_level):
        """
        Returns grouped trip_sets by an aggregate level.
        `aggregate_level` takes any of the values `city`, `hub`, `vehicle_class`\
        `driver`, `vehicle_number`.

        input : city
        response : [{"value": "Bangalore", "trip_set": <TripSet instance_1>},
                    {"value": "Chennai", "trip_set": <TripSet instance_2>}]
        """
        aggregate_level = camel_case_to_snake_case(aggregate_level)

        field_name = {
            "city": "order__city",
            "hub": "hub",
            "vehicle_class": "vehicle__vehicle_model__vehicle_class",
            "driver": "invoice_driver",
            "vehicle_number": "vehicle__registration_certificate_number"
        }.get(aggregate_level, None)

        distinct_values = list(set(self.trips.filter(
            **{"%s__isnull" % field_name: False}
        ).values_list(field_name, flat=True)))

        self.trip_set_buckets = []
        for value in distinct_values:
            self.trip_set_buckets.append({
                "aggregate_value": value,
                "trip_set": TripSet(
                    trips=self.trips.filter(**{field_name: value}), contract_type=self.contract_type,
                    contract=self.contract,
                    bucket_type=field_name,
                    bucket_value=value
                )
            })

        return self.trip_set_buckets

    def get_toll_parking_charges(invoice):
        parking_sum, toll_sum = 0, 0
        trips = Trip.objects.filter(invoicetrip__invoice=invoice).annotate(
                        planned_start_time_ist=expression_planned_start_time_ist).order_by('actual_start_time')

        for trip in trips:
            parking_sum += float(trip.parking_charges) if trip.parking_charges else 0
            toll_sum += float(trip.toll_charges) if trip.toll_charges else 0

        return trips, parking_sum, toll_sum

    def get_deduction_amount(self):
        total_deduction_amount = 0
        if self.dp_contract_type == CONTRACT_TYPE_FIXED:
            for i in self.valid_trips:
                if i.trip_deduction_amount:
                    total_deduction_amount = total_deduction_amount + i.trip_deduction_amount
        return total_deduction_amount

    def get_tripset_basepay_amount(self):
        total_basepay_amount = 0
        trip_basepay_calculation = []
        for i in self.completed_trips:
            if i.trip_base_pay:
                total_basepay_amount = total_basepay_amount + i.trip_base_pay
                trip_basepay_calculation.append({
                    'trip_number': i.trip_number,
                    'base_pay': i.trip_base_pay
                })
        return total_basepay_amount, trip_basepay_calculation

    def is_blowhorn_contract(self):
        is_blowhorn_contract = False
        for trip in self.completed_trips:
            if trip.blowhorn_contract_id:
                is_blowhorn_contract = True
                break
        return is_blowhorn_contract
