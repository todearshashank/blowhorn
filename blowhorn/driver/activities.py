from datetime import timedelta, datetime

import logging
from django.utils import timezone
from django.db import transaction
from django.db.models import Min, Max, F, Prefetch, Q
from django.conf import settings

from blowhorn.oscar.core.loading import get_model

from config.settings import status_pipelines as StatusPipeline

from blowhorn.utils.functions import geopoints_to_km
from blowhorn.document.models import DocumentType
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT

Driver = get_model('driver', 'Driver')
DriverConstants = get_model('driver', 'DriverConstants')
DriverActivityHistory = get_model('driver', 'DriverActivityHistory')
DriverLogInfo = get_model('driver', 'DriverLogInfo')
DriverLogBatch = get_model('driver', 'DriverLogBatch')
DriverLogActivity = get_model('driver', 'DriverLogActivity')
DriverHistory = get_model('driver', 'DriverHistory')
User = get_model('users', 'User')
Trip = get_model('trip', 'Trip')


class ActivitiesMonitoring(object):
    """
    This function calculates gps distance for every driver who has login logout
    interval of less than 24Hrs or logged in before less than 24Hrs. Calculated
    distance with login details is put in table DriverLogActivity
    """

    def calculate_driver_gps_distance(self, *args, **kwargs):
        driverlogins = DriverLogInfo.objects.filter(action='Login').exclude(processed=True). \
            order_by('action_date')
        starttime = timezone.now()
        total_processed = 0
        with transaction.atomic():
            batch = DriverLogBatch.objects.create(start_time=starttime,
                                                  number_of_entries=total_processed)
            for driverlogin in driverlogins:
                driverlogout = DriverLogInfo.objects.filter(
                    driver_id=driverlogin.driver_id,
                    action='Logout',
                    action_date=(
                        DriverLogInfo.objects. filter(
                            action='Logout',
                            driver_id=driverlogin.driver_id,
                            action_date__gte=driverlogin.action_date).exclude(
                            processed=True). aggregate(
                            Min('action_date'))['action_date__min'])).first()
                if driverlogout is None:
                    end_time_for_dist_calc = timezone.now()
                else:
                    end_time_for_dist_calc = driverlogout.action_date

                if (end_time_for_dist_calc - driverlogin.action_date > timedelta(hours=24)):
                    logging.info('\t\t\tDriver id - %s has not logout for more than 24Hrs, hence skipping this record for gps distance calculation' % driverlogin.driver_id)

                    driverlogin.processed = True
                    driverlogin.save()

                    if driverlogout:
                        driverlogout.processed = True
                        driverlogout.save()
                    continue

                activity_geopoints = DriverActivityHistory.objects.filter(driver_id=driverlogin.driver_id, created_time__range=(
                    driverlogin.action_date, end_time_for_dist_calc)).order_by('created_time').values_list('geopoint', flat=True)
                gps_distance = geopoints_to_km([x for x in activity_geopoints])
                data = {
                    'driver_id': driverlogin.driver_id,
                    'login_time': driverlogin.action_date,
                    'logout_time': driverlogout.action_date if driverlogout else None,
                    'gps_distance': gps_distance,
                    'batch': batch
                }

                driverlogact = DriverLogActivity(**data)
                driverlogact.save()

                if driverlogout:
                    driverlogin.processed = True
                    driverlogin.save()
                    driverlogout.processed = True
                    driverlogout.save()
                total_processed += 1
            batch.end_time = timezone.now()
            batch.number_of_entries = total_processed
            batch.save()
            logging.info('\t\t\tNumber of driver login records processed : %s' % total_processed)


def deactivate_old_drivers():
    """
    Deactivate drivers who do not have done any trips in last DAYS_OLDER_DEACTIVATE days
    """

    day = datetime.today().weekday()
    day %= 2
    todays_date = datetime.now()
    time_start_1 = todays_date.replace(hour=21, minute=0, second=0)
    time_start_2 = todays_date.replace(hour=18, minute=0, second=0)

    if day == 0:
        if todays_date > time_start_1:
            _params = Q(operating_city_id=1)
        elif todays_date > time_start_2:
            _params = Q(operating_city_id=2)
        else:
            _params = Q(operating_city_id=3)
    if day == 1:
        if todays_date > time_start_1:
            _params = Q(operating_city_id=4)
        elif todays_date > time_start_2:
            _params = Q(operating_city_id=6)
        else:
            _params = ~Q(operating_city_id__in=[1,2,3,4,6])


    from blowhorn.driver.util import get_driver_missing_doc
    DAYS_OLDER_DEACTIVATE = int(DriverConstants().get('DAYS_OLDER_DEACTIVATE', 30))
    days_older_deactivate = timezone.now() - timedelta(days=DAYS_OLDER_DEACTIVATE)
    drivers = Driver.objects.filter(status=StatusPipeline.ACTIVE).filter(_params)
    drivers = drivers.prefetch_related('drivervehiclemap_set', 'drivervehiclemap_set__vehicle',
                                       'drivervehiclemap_set__vehicle__vehicle_document')
    drivers = drivers.prefetch_related('driver_document')
    drivers = drivers.prefetch_related(
        Prefetch('driverhistory_set',
            queryset=DriverHistory.objects.filter(new_value=StatusPipeline.ACTIVE
                                                      ).order_by('-modified_time')))
    drivers = drivers.prefetch_related(
        Prefetch('trip_set', queryset=Trip.objects.filter(status=StatusPipeline.TRIP_COMPLETED,
                                               planned_start_time__gt=days_older_deactivate
                                               ).order_by('-planned_start_time')))

    modified_by = User.objects.filter(
        email=settings.CRON_JOB_DEFAULT_USER).first()
    mandatory_driver_doc_type = DocumentType.objects.filter(is_document_mandatory=True, identifier=DRIVER_DOCUMENT)
    mandatory_vehicle_doc_type = DocumentType.objects.filter(is_document_mandatory=True, identifier=VEHICLE_DOCUMENT)

    driver_history_list = []
    driver_ids = []
    missing_doc_driver_ids = []

    for driver in drivers:
        missing_doc = get_driver_missing_doc(driver, mandatory_driver_doc_type, mandatory_vehicle_doc_type)
        if not missing_doc:
            last_activated = driver.driverhistory_set.all()
            history_event = last_activated[0] if last_activated else None
            last_activated_datetime = history_event.modified_time if history_event else None
            if last_activated_datetime and last_activated_datetime + timedelta(days=DAYS_OLDER_DEACTIVATE) >= timezone.now():
                continue

            days_older_deactivate = timezone.now() - timedelta(days=DAYS_OLDER_DEACTIVATE)
            last_completed_trip = driver.trip_set.all()
            last_completed_trip_exist = True if last_completed_trip and last_completed_trip[0] \
                    and last_completed_trip[0].planned_start_time > days_older_deactivate else False

            if not last_completed_trip_exist:
                driver_history_list.append(DriverHistory(driver_id=driver.id,
                                                          field='status',
                                                          old_value=StatusPipeline.ACTIVE,
                                                          new_value=StatusPipeline.INACTIVE,
                                                          reason='System Deactivated',
                                                          user=modified_by))
                driver_ids.append(driver.id)
        elif missing_doc:
            driver_history_list.append(DriverHistory(driver_id=driver.id,
                                                     field='status',
                                                     old_value=StatusPipeline.ACTIVE,
                                                     new_value=StatusPipeline.ONBOARDING,
                                                     reason='System Deactivated',
                                                     user=modified_by))
            missing_doc_driver_ids.append(driver.id)

    if missing_doc_driver_ids:
        Driver.objects.filter(id__in=missing_doc_driver_ids).update(
            status=StatusPipeline.ONBOARDING, modified_date=timezone.now())

    if driver_ids:
        Driver.objects.filter(id__in=driver_ids).update(
            status=StatusPipeline.INACTIVE, modified_date=timezone.now())

    DriverHistory.objects.bulk_create(driver_history_list)
