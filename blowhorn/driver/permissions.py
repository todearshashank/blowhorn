from django.conf import settings

from rest_framework import permissions


class IsActiveDriver(permissions.BasePermission):
    """
    Permission to allow access for Active Drivers.
    """

    def has_permission(self, request, view):

        if request.user.is_authenticated and request.user.is_active_driver():
            return True
        if request.user.is_authenticated and hasattr(request.user, 'vendor') and request.user.vendor:
            return True

        return False


class APIVerifiedCustomer(permissions.BasePermission):
    """
        API key verification for the notification url of the BetterPlace
    """

    def has_permission(self, request, view):
        if not request.META.get('HTTP_APIKEY', None):
            return False

        if not (request.META.get('HTTP_APIKEY', None) == settings.BETTERPLACE_BLOWHORNS_ENDPOINT_API_KEY):
            return False

        return True
