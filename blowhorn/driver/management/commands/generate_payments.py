import argparse
from django.core.management.base import BaseCommand, CommandError
from blowhorn.driver.payments import PaymentService


class Command(BaseCommand):
    help = 'Generates payment for drivers defined in `Contracts`'

    def add_arguments(self, parser):
        parser.add_argument('-c', '--contract', nargs='?', help='Contract Name')
        parser.add_argument('-i', '--contract_id', nargs='?', type=int, help='DB Contract ID')

    def handle(self, *args, **kwargs):
        if kwargs['contract'] and kwargs['contract_id']:
            self.stdout.write('Enter either contract name or id, not both')
            return
        elif kwargs['contract']:
            self.stdout.write('generating payment services for contract %s' % (kwargs['contract']))
        elif kwargs['contract_id']:
            self.stdout.write('generating payment services for contract %d' %(kwargs['contract_id']))
        else:
            self.stdout.write('generating payment services for all contracts')

        PaymentService().generate_payments(**kwargs)

