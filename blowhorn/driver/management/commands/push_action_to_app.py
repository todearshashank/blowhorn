import argparse
import json
import logging
from django.core.management.base import BaseCommand, CommandError

from fcm_django.models import FCMDevice

from blowhorn.apps.driver_app.v3.services.driver.login import LogIn
from blowhorn.common.notification import Notification
from blowhorn.driver.models import DriverActivity
from blowhorn.livetracking.constants import ACTIONS

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

ALLOWED_COMP_OPERATORS = ['<', '<=', '>=', '>', '==']
OPERATOR_EQUAL = '=='

class Command(BaseCommand):
    help = 'Pushes notification to driver app'

    def add_arguments(self, parser):
        parser.add_argument('-a', '--action', nargs='?', help='Action (str): Allowed Actions: %s' % ACTIONS)
        parser.add_argument('-bc', '--build_code', nargs='?', type=int, help='App Build Code (int)')
        parser.add_argument('-d', '--driver_pks', nargs='?', type=list, help='Driver IDs (list of pks)')
        parser.add_argument('-u', '--user_pks', nargs='?', type=list, help='User IDs (list of pks)')
        parser.add_argument('-o', '--operator', nargs='?', type=str,
            help='Operator for build code comparision. e.g., `code_in_activity <= build_code`.\
             ALLOWED_COMP_OPERATORS: %s' % ALLOWED_COMP_OPERATORS)

    def handle(self, *args, **kwargs):
        action = kwargs.get('action')
        if not action:
            self.stdout.write('Action is mandatory')
            return

        build_code = kwargs.get('build_code')
        if not build_code:
            self.stdout.write('Build code is mandatory')
            return

        operator = kwargs.get('operator') or OPERATOR_EQUAL
        if operator and operator not in ALLOWED_COMP_OPERATORS:
            self.stdout.write('%s operation is not allowed' % operator)
            return

        logger.info('Action: %s, Build Code: %s, Operator: %s' % (action, build_code, operator))
        driver_pks = kwargs.get('driver_pks')
        driver_activity = DriverActivity.objects.select_related('driver', 'driver__user')
        if driver_pks:
            logger.info('Driver PKs: %s' % driver_pks)
            driver_activity = driver_activity.filter(driver_id__in=driver_pks)

        user_pks = kwargs.get('user_pks')
        params = {}
        if user_pks:
            params = dict(user_id__in=user_pks)
        else:
            users = []
            for da in driver_activity:
                other_details = da.other_details
                if isinstance(other_details, str):
                    other_details = json.loads(other_details)

                _code = int(other_details.get('app_code'))
                if eval('%d %s %d' % (_code, operator, build_code)):
                    users.append(da.driver.user_id)

            params = dict(user_id__in=users)

        logger.info('User selected for FCM push: %s' % users)
        devices = FCMDevice.objects.filter(**params)
        message_status = devices.send_message(data={
            'blowhorn_notification': {
                'action': action
            }
        })
