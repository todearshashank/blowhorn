import argparse
from django.core.management.base import BaseCommand, CommandError
from blowhorn.driver.activities import ActivitiesMonitoring


class Command(BaseCommand):
    help = """Creates Driver Log Activity records for driver with login time, distance travelled 
           """

    def handle(self, *args, **kwargs):
        ActivitiesMonitoring().calculate_driver_gps_distance()
