# Django and System libraries
import logging
import json
import requests
import random
import operator
from datetime import datetime
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext as _
from django.conf import settings
from django.db import transaction
from django.core.exceptions import ValidationError
from django.db.models import Q

from blowhorn.oscar.core.loading import get_model
from dateutil import parser
from functools import reduce

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.models import DriverBalance, DriverPaymentAdjustment, Driver
from blowhorn.common.tasks import send_async_mail
from blowhorn.common import serializers as common_serializer
from blowhorn.driver import constants as DrivConst
from .serializers import DriverListSerializer
from blowhorn.users.models import User
from blowhorn.document.constants import *
from blowhorn.contract.constants import CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED
from blowhorn.apps.driver_app.v1.views.driver import DocumentUpload
from blowhorn.document.models import DocumentEvent, DocumentArchive, \
    DocumentType
from blowhorn.document.utils import mark_driver_onboarding
from blowhorn.apps.operation_app.v5.mixins import NotificationMixin
from blowhorn.apps.operation_app.v5.constants import COSMOS_SCREEN_URLS, \
    NOTIFICATION_GROUP_INTERNAL_VERIFICATION, NOTIFICATION_TEMPLATES, \
    REQUESTED, VERIFICATION_STATUS, NOTIFICATION_GROUP_EXTERNAL_VERIFICATION
from blowhorn.contract.permissions import is_finance_user
from blowhorn.driver.tasks import push_notification_to_driver_app

# Country = get_model('address', 'Country')
Company = get_model('company', 'Company')
State = get_model('address', 'State')
DriverAddress = get_model('driver', 'DriverAddress')
Driver = get_model('driver', 'Driver')
CourtRecord = get_model('driver', 'CourtRecord')
DriverDocument = get_model('driver', 'DriverDocument')
VehicleDocument = get_model('vehicle', 'VehicleDocument')
Contract = get_model('contract', 'Contract')
City = get_model('address', 'City')


# Used in driver payments export to generate Kotak bank file
BLOWHORN_PAYABLE_ACCOUNT_NUMBER = '7811633144'


class DriverSettlementRecord(object):

    def __init__(self, driver, amount, batch_id, transaction_id):
        self.name = driver.user.name
        self.bank_account = driver.get_bank_account_details()
        self.mobile = driver.user.phone_number
        self.amount = amount
        self.batch_id = batch_id
        self.transaction_id = transaction_id

    def get_bank_upload_line(self, date):
        payment_mode = ''
        payment_ref_number = self.transaction_id
        beneficiary_code = ''
        approved_by = ''
        beneficiary_mobile = ('%s' % self.mobile).replace('+91', '')
        payment_date = date.strftime('%d/%m/%Y')
        debit_narration = '-'.join(['DRP', str(self.name),
                                    str(self.batch_id), str(
                                        self.transaction_id)])
        credit_narration = '-'.join(['Blowhorn Payment',
                                     str(self.batch_id), str(
                                         self.transaction_id)])
        content = [
            'BLOWHORN',
            'RPAY',
            payment_mode,
            payment_ref_number,
            payment_date,
            BLOWHORN_PAYABLE_ACCOUNT_NUMBER,
            '%.1f' % self.amount,
            beneficiary_code,
            self.bank_account.account_name,
            self.bank_account.ifsc_code,
            self.bank_account.account_number,
            approved_by,
            beneficiary_mobile,
            debit_narration,
            credit_narration,
            self.batch_id,
            self.transaction_id,
        ]

        return '~'.join(map(str, content))


class DriverSettlement(object):
    """ Class to handle the driver settlement process of generating
        the bank upload file and sending email
    """

    def __init__(self, batch_id):
        self.records = []
        self.total_amount = 0
        self.batch_id = batch_id
        self.bank_file = None

    def add(self, driver, amount, batch_id, transaction_id):
        self.records.append(
            DriverSettlementRecord(driver, amount, batch_id, transaction_id)
        )
        self.total_amount += amount

    def get_number_of_entries(self):
        return len(self.records)

    def get_total_amount(self):
        return self.total_amount

    def get_bank_file(self):
        return self.bank_file

    def generate_bank_file(self, payment_date):
        file_contents = ''
        for r in self.records:
            file_contents += '%s\n' % r.get_bank_upload_line(payment_date)

        self.bank_file = file_contents

    def send_email(self, recepient, date):
        body = """
Total Payment Count : %d
Total Payment Amount : %.1f
""" % (len(self.records), self.total_amount)

        filename = '%s_B%s_N%d_INR%.0f.txt' % (
            date.strftime('%d-%b-%Y_%H%M'),
            self.batch_id,
            len(self.records),
            self.total_amount
        )
        content = self.get_bank_file()
        subject = 'Driver Payment'
        mime = 'text/plain'

        attachments = [(filename, content, mime), ]
        send_async_mail.delay([recepient], subject,
                              body=body, attachments=attachments)


class DriverFilter(SimpleListFilter):
    title = _('driver')
    parameter_name = 'driver'

    def lookups(self, request, model_admin):
        return [(d.driver.id, "%s | %s" % (d.driver.name,
                                           d.driver.driver_vehicle))
                for d in DriverBalance.objects.filter(amount__gt=0)]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(driver=str(self.value()))
        return queryset


class ContractTypeFilter(SimpleListFilter):

    """
    To filter payment types
    """

    title = _('Contract Type')
    parameter_name = 'contract_type'
    related_filter_parameter = 'payment__contract__contract_type'

    def lookups(self, request, model_admin):
        choice = [CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SPOT]
        return [(c, c) for c in choice]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(contract__contract_type=self.value())
        return queryset


def get_drivers_list(city, num=None, page=None):
    if city:
        logging.info(
            'Fetching the drivers list for the following city : %s' % city)
        driver_list = Driver.objects.filter(operating_city=city).exclude(
            status='Inactive').select_related('user')
    else:
        logging.info('Fetching the drivers list for all the cities')
        driver_list = Driver.objects.exclude(
            status='Inactive').select_related('user')

    if driver_list:
        page_to_be_displayed = page if page else DrivConst.PAGE_TO_BE_DISPLAYED
        number_of_entry = num if num else DrivConst.NUMBER_OF_ENTRY_IN_PAGE

        logging.info(
            'Page to be displayed for the '
            'pagination purpose: %s' % page_to_be_displayed)
        logging.info(
            'Number of entry per page for '
            'pagination purpose: %s' % number_of_entry)

        serialized_data = common_serializer.PaginatedSerializer(
            queryset=driver_list, num=number_of_entry,
            page=page_to_be_displayed,
            serializer_method=DriverListSerializer)

        logging.info('Returning the serialized data for the drivers list')

        return serialized_data.data


def get_detailed_data(params):
    # country = Country.objects.get(pk='IN')
    vehicle_parameters = params['vehicles'][
        0] if params.get('vehicles', []) else {}
    driver_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'can_drive_and_deliver': params.get('can_drive_and_deliver', ''),
        'can_source_additional_labour': params.get(
            'can_source_additional_labour', ''),
        'driving_license_number': params.get(
            'driving_license_number', params.get('mobile', '')),
        # 'owner_contact_no': params.get('owner_contact_no', ''),
        # 'owner_name': params.get('owner_name', ''),
        # 'owner_pan': params.get('owner_pan', ''),
        'work_status': params.get('work_status', ''),
        'working_preferences': params.get('working_preferences', []),
        'own_vehicle': params.get('driver_is_owner', False),
        'operating_city': params.get('operating_city', ''),
        'pan': params.get('pan_number', '')
    }

    vehicle_data = {
        'vehicle_model': vehicle_parameters.get('vehicle_model', ''),
        'vehicle_class': vehicle_parameters.get('vehicleClass', ''),
        'body_type': vehicle_parameters.get('body_type_name', ''),
        'registration_certificate_number': params.get('driver_vehicle', ''),
        'model_year': vehicle_parameters.get('vehicle_model_year', '')
    }

    user_data = {
        'phone': params.get('mobile', ''),
        'name': params.get('name', ''),
        'password': params.get('password', None),
        'email': params.get('email', ''),
        'status': params.get('status', StatusPipeline.SUPPLY),
        'user_id': None
    }

    vendor_data = {
        'phone_number': params.get('owner_contact_no', ''),
        'name': params.get('owner_name', ''),
        'pan': params.get('owner_pan_number', '')
    }

    detailed_data = {
        'driver_data': driver_data,
        'vehicle_data': vehicle_data,
        'user_data': user_data,
        'vendor_data': vendor_data
    }

    return detailed_data


def get_user_for_driver(user_data):
    user = Driver.objects.upsert_user(
        email=user_data['email'],
        name=user_data['name'],
        phone_number=user_data['phone'],
        password=user_data['password'],
        user_id=user_data.get('user_id', None),
        is_active=True if user_data[
            'status'] == StatusPipeline.ACTIVE else False
    )
    return user


def get_verification_resultant_data(data):
    verification_common_data = {
        'client_status': getattr(StatusPipeline, data['clientStatus'],
                                 None) if data.get('clientStatus') else None,
        'bpss_status': getattr(StatusPipeline, data['bpssStatus'],
                               None) if data.get('bpssStatus') else None,
        'reference_id': data.get('referenceId', None)
    }

    return verification_common_data


def get_verified_address_details(address_data):
    data = address_data[0]
    address_details = {
        'bgv_status': getattr(StatusPipeline, data['status'],
                              StatusPipeline.PENDING) if data.get(
            'status') else StatusPipeline.PENDING,
        'bgv_sub_status': getattr(StatusPipeline, data['subStatus'],
                                  None) if data.get('subStatus') else None,
        'remark': data.get('remark', ''),
        'verification_requested_on': parser.parse(
            data['requestedOn']) if data.get('requestedOn') else None,
        'verification_completed_on': parser.parse(
            data['completedOn']) if data.get('completedOn') else None,
        'verification_extra_details': data.get('moreDetails', None)
    }
    return address_details


def get_verified_document_details(data):
    document_details = {
        'number': data.get('detail'),
        'bgv_status': getattr(StatusPipeline, data['status'],
                              StatusPipeline.PENDING) if data.get(
            'status') else StatusPipeline.PENDING,
        'bgv_remark': data.get('remark', ''),
        'verification_requested_on': parser.parse(
            data['requestedOn']) if data.get('requestedOn') else None,
        'verification_completed_on': parser.parse(
            data['completedOn']) if data.get('completedOn') else None,
        'verification_extra_details': data.get('moreDetails')
    }

    return document_details


def get_verified_court_record(court_data, bgv_obj):
    data = court_data[0]
    verification_requested_on = parser.parse(
        data['requestedOn']) if data.get('requestedOn') else None
    verification_completed_on = parser.parse(
        data['completedOn']) if data.get('completedOn') else None
    verified_court_record_data = []

    more_details = data.get('moreDetails', {})
    if not more_details:
        return {}

    for val in more_details.get('result', {}):
        court_records = data['moreDetails']['result'][val]
        court_type = getattr(DrivConst, val, None)

        for record in court_records:
            try:
                state = State.objects.get(name=record.get('state', None))
            except:
                state = None

            court_data = {
                'background_verification': bgv_obj,
                'court_type': court_type,
                'state': state,
                'name': record.get('name'),
                'case_type': record.get('caseType'),
                'case_number': record.get('caseNo'),
                'case_date': record.get('caseDate'),
                'remark': record.get('remark'),
                'district': record.get('district'),
                'criminal_status': record.get('isCriminal'),
                'address_type': getattr(DrivConst,
                                        record['addressType']) if record.get(
                    'addressType') else None,
                'court_verification_requested_on': verification_requested_on,
                'court_verification_completed_on': verification_completed_on
            }

            verified_court_record_data.append(CourtRecord(**court_data))

    return verified_court_record_data


def driver_data_validation_for_bgv(driver):

    errors = []

    if not driver.name:
        errors.append("Name of the driver is needed for verification")

    if not driver.father_name:
        errors.append(
            "Father's name is needed for the the verification")

    # if not driver.gender:
    #     raise ValidationError(
    #         "Gender of the driver is needed for the verification")

    if not driver.operating_city:
        errors.append(
            "Operating City of the driver is needed for the verification")

    if not driver.date_of_birth:
        errors.append(
            "Date of Birth of the driver is needed for the verification")

    if not driver.current_address:
        errors.append(
            "Current address of the driver is needed for the verification")

    if not driver.is_address_same and not driver.permanent_address:
        errors.append(
            "Need Permanent address for verification as it is not "
            "same as current address")

    if errors:
        raise ValidationError(errors)

    return True


def get_validated_document_data_for_bgv(driver, document_types):
    document_data_list = []

    if not (document_types and set(document_types).issubset(
            BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING)):
        raise ValidationError(
            "Select valid documents to be sent for verification")

    for docs in document_types:
        if docs in BETTERPLACE_DRIVER_DOCUMENT_CODE:
            document = DriverDocument.objects.filter(
                driver=driver,
                document_type__code=BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING[
                    docs]).first()
        else:
            vehicle = driver.vehicles.all().first()
            document = VehicleDocument.objects.filter(
                vehicle=vehicle,
                document_type__code=BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING[
                    docs]).first()

        if not document:
            raise ValidationError(docs + " is not uploaded for the driver")

        if not document.number or document.number.startswith('-') or \
                document.number.count(" "):
            raise ValidationError(
                "Enter valid document number without space for " + docs)

        if docs in STATE_REQUIRED_FOR_THE_DOCUMENTS and not document.state:
            raise ValidationError("Name of the state is needed for " + docs +
                                  " for verification")

        document_data = {
            "action": "",
            "name": "",
            "docType": docs,
            "docNo": document.number,
            "state": document.state.name if document.state else "",
            "clientRefId": ""
        }
        document_data_list.append(document_data)

    return document_data_list


def get_driver_address(address, address_type):

    if address and not address.city:
        raise ValidationError("City is needed in address for verification")

    if address and not address.state:
        raise ValidationError("State is needed in address for verification")

    address_data = {
        "addressType": address_type,
        "line1": address.line1 if address else "",
        "line2": address.line2 if address else "",
        "city": address.city if address else "",
        "state": address.state if address else "",
        "pincode": address.postcode if address else "",
        "same": ""

    }

    if address_type == 'PERMANENT' and not address:
        address_data["same"] = "true"

    return address_data


def get_data_for_background_verification(driver_obj, document_types):
    bgv_data_format = {}
    address_list = []

    driver_data_validation_for_bgv(driver_obj)
    document_data = get_validated_document_data_for_bgv(
        driver_obj, document_types)

    bgv_data_format["fName"] = driver_obj.name
    bgv_data_format["fatherName"] = driver_obj.father_name
    bgv_data_format["gender"] = "MALE"
    bgv_data_format["location"] = driver_obj.operating_city.name
    bgv_data_format["mobile"] = str(driver_obj.user.national_number)
    bgv_data_format["dob"] = driver_obj.date_of_birth.strftime("%d-%m-%Y")

    address_list.append(get_driver_address(
        driver_obj.current_address, 'PRESENT'))
    address_list.append(get_driver_address(
        driver_obj.permanent_address, 'PERMANENT'))

    bgv_data_format["addresses"] = address_list
    bgv_data_format["documents"] = document_data

    return bgv_data_format


def create_bgv_profile(bgv_data):
    create_profile_url = getattr(
        settings, 'BETTERPLACE_ENDPOINT_CREATE_PROFILE', None)
    api_key = getattr(settings, 'BETTERPLACE_API_KEY', None)

    headers = {"Content-Type": "application/json", "ApiKey": api_key}

    logging.info(
        'This is the input data for the create profile of the '
        'betterplace : %s' % (json.dumps(bgv_data), ))

    try:
        response = requests.post(
            create_profile_url, data=json.dumps(bgv_data), headers=headers)
    except:
        response = None

    logging.info('Below is the response %s' % (response, ))

    return response


def get_verified_data(reference_id):
    response_data = {}
    verification_status_url = getattr(
        settings, 'BETTERPLACE_ENDPOINT_VERIFICATION_STATUS', None)

    api_key = getattr(settings, 'BETTERPLACE_API_KEY', None)
    input_data = [{
        'id': reference_id
    }]
    headers = {"Content-Type": "application/json", "ApiKey": api_key}
    logging.info(
        'This is the url fetching the verification status of the '
        'betterplace : %s' % (verification_status_url, ))

    try:
        response = requests.post(
            verification_status_url, data=json.dumps(input_data),
            headers=headers)
    except:
        response = None

    logging.info('Below is the response %s' % (response, ))

    if response and response.status_code == 202:
        logging.info('This is the response content : %s' % response.json())
        response_data = response.json()
    else:
        raise Exception("No/Bad response obtained")

    return response_data


def get_background_verification_status(bgv_object, reference_id):
    verified_data = get_verified_data(reference_id)
    data = verified_data.get('data', [])
    if not data:
        raise Exception("No data found for the verified driver")
    data = data[0]

    with transaction.atomic():
        try:
            bgv_data = get_verification_resultant_data(data)
            bgv_object.update(**bgv_data)
            bgv_obj = bgv_object.first()

            driver = bgv_obj.driver
            data_report = data.get('report', {})
            city_managers = list(
                driver.operating_city.managers.all().values_list('id', flat=True))
            users_to_be_notified = [bgv_obj.created_by.pk] + city_managers
            verified_entities = []

            if data.get('clientStatus') in \
                    DrivConst.BACKGROUND_VERIFICATION_STATUS_FOR_FAIL:
                Driver.objects.filter(id=driver.id).update(
                    bgv_status=DrivConst.VERIFICATION_FAILED)
            elif data.get('clientStatus') == 'VERIFIED':
                Driver.objects.filter(id=driver.id).update(
                    bgv_status=DrivConst.VERIFICATION_PASSED)

            # To check if the address details are there in the verification
            # result or not and if it's there then
            # update the address record
            if "ADDRESS" in data_report:
                if "CURRENT_ADDRESS" in data_report.get(
                        "ADDRESS", {}).get("result", {}):
                    current_address_data = get_verified_address_details(
                        data_report["ADDRESS"]["result"].get(
                            "CURRENT_ADDRESS", []))
                    if driver.current_address:
                        DriverAddress.objects.filter(
                            id=driver.current_address_id).update(
                            **current_address_data)

                if "PERMANENT_ADDRESS" in data_report.get(
                        "ADDRESS", {}).get("result", {}):
                    permanent_address_data = get_verified_address_details(
                        data_report["ADDRESS"]["result"].get(
                            "PERMANENT_ADDRESS", []))
                    if driver.current_address:
                        DriverAddress.objects.filter(
                            id=driver.current_address_id).update(
                            **permanent_address_data)

                verified_entities.append('Address')
            # To check if the document details are there in the verification
            # result or not and if it's there then update the
            # various document record submitted for the driver verification
            if "DOCUMENT" in data_report:
                for val in BETTERPLACE_DOCUMENT_TYPE_CODE_MAPPING.keys():
                    if val in data_report.get("DOCUMENT", {}).get(
                            "result", {}):
                        for doc_data in data_report["DOCUMENT"][
                                "result"].get(val):
                            if doc_data.get('detail', ''):
                                document_data = get_verified_document_details(
                                    doc_data)
                                if val in BETTERPLACE_DRIVER_DOCUMENT_CODE:
                                    DriverDocument.objects.filter(
                                        number=doc_data['detail']).update(
                                        **document_data)
                                else:
                                    VehicleDocument.objects.filter(
                                        number=doc_data['detail']).update(
                                        **document_data)

                verified_entities.append('Documents')

            # To check if the criminal record are there in the verification
            # result or not and if it's there then update the
            # various court records provided after the verification
            if "CRIMINAL" in data_report:
                if "COURT_RECORD" in data_report.get("CRIMINAL", []).get(
                        "result", {}):
                    court_data = get_verified_court_record(
                        data_report["CRIMINAL"]["result"]["COURT_RECORD"],
                        bgv_obj)
                    if court_data:
                        CourtRecord.objects.bulk_create(court_data)
                        verified_entities.append('Court Record')

            message_template = NOTIFICATION_TEMPLATES.get(
                NOTIFICATION_GROUP_EXTERNAL_VERIFICATION, {}).get(
                VERIFICATION_STATUS)
            extra_context = {
                'driver_id': driver.id,
                'subject': '%s' % driver,
                'action_by': 'betterplace',
                'redirect_url': '',
                'remarks': ''
            }
            NotificationMixin().save_and_broadcast_notification(
                action_owners=[],
                broadcast_list=users_to_be_notified,
                action_by=bgv_obj.created_by,
                action=VERIFICATION_STATUS,
                extra_context=extra_context,
                notification_group=NOTIFICATION_GROUP_EXTERNAL_VERIFICATION,
                message=message_template.format(
                    entity=', '.join(verified_entities),
                    action_by=extra_context.get('action_by'),
                    driver=extra_context.get('subject')
                )
            )
        except:
            raise Exception('Something went wrong while creating the record')

    return reference_id


class DriverPaymentExportClass(object):

    def get_query(self, params, user, net_pay_gt_one=False, is_settled=None):
        query_params = []
        if not params:
            return ''

        start_datetime__gte = datetime.strptime(
            params['start_datetime__gte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('start_datetime__gte', None) else None

        start_datetime__lte = datetime.strptime(
            params['start_datetime__lte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('start_datetime__lte', None) \
            else None

        end_datetime__gte = datetime.strptime(
            params['end_datetime__gte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('end_datetime__gte', None) else None

        end_datetime__lte = datetime.strptime(
            params['end_datetime__lte'], '%d-%m-%Y').strftime(
            '%Y-%m-%d') if params.get('end_datetime__lte', None) \
            else None

        if not user.is_superuser and not is_finance_user(None, user):
            cities = City.objects.get_cities_for_managerial_user(user)
            contracts = Contract.objects.filter(
                Q(spocs=user) |
                Q(supervisors=user) |
                Q(city__in=cities)).distinct()
            query_params.append(Q(contract__in=contracts))

        if params.get('q', None):
            search_query = params['q'].split(' ')
            if len(search_query) > DrivConst.MAXIMUM_NUMBER_OF_QUERIES:
                query_params.append(Q(id__in=search_query))
            else:
                query_params.append(
                    Q(driver__name__icontains=params['q']) |
                    Q(paymenttrip__trip__hub__name__icontains=params['q']) |
                    Q(contract__name__icontains=params['q']) |
                    Q(paymenttrip__trip__vehicle__registration_certificate_number__icontains=params['q']) |
                    Q(id__icontains=params['q']))

        if params.get('is_settled__exact', '') == '1':
            query_params.append(Q(is_settled=True))
        elif params.get('is_settled__exact', '') == '0':
            query_params.append(Q(is_settled=False))

        if params.get('status'):
            query_params.append(Q(status=params['status']))

        if params.get('status__in'):
            statuses = params['status__in'].split(',')
            query_params.append(Q(status__in=statuses))

        if params.get('batch_id__id__exact'):
            query_params.append(Q(batch_id__id__exact=params['batch_id__id__exact']))

        if params.get('contract__city__id__exact'):
            query_params.append(
                Q(contract__city__id__exact=params[
                    'contract__city__id__exact']))

        if params.get('contract__customer__id__exact'):
            query_params.append(
                Q(contract__customer__id__exact=params['contract__customer__id__exact']))

        if params.get('contract__division__id__exact'):
            query_params.append(
                Q(contract__division__id__exact=params['contract__division__id__exact']))

        if params.get('driver__id__exact'):
            query_params.append(
                Q(driver__id__exact=params['driver__id__exact']))

        if params.get('contract__id__exact'):
            query_params.append(
                Q(contract__id__exact=params[
                    'contract__id__exact']))

        if params.get('hub'):
            query_params.append(
                Q(paymenttrip__trip__hub=params['hub']))

        if params.get('payment_type__exact'):
            query_params.append(
                Q(payment_type__exact=params['payment_type__exact']))

        if start_datetime__gte and start_datetime__lte:
            query_params.append(
                Q(start_datetime__range=(
                    start_datetime__gte, start_datetime__lte)))
        elif start_datetime__gte:
            query_params.append(
                Q(start_datetime__gte=
                    start_datetime__gte))
        elif start_datetime__lte:
            query_params.append(
                Q(start_datetime__lte=
                    start_datetime__lte))

        if end_datetime__gte and end_datetime__lte:
            query_params.append(
                Q(end_datetime__range=(
                    end_datetime__gte, end_datetime__lte)))
        elif end_datetime__gte:
            query_params.append(
                Q(end_datetime__gte=
                  end_datetime__gte))
        elif end_datetime__lte:
            query_params.append(
                Q(end_datetime__lte=
                    end_datetime__lte))

        if net_pay_gt_one:
            query_params.append(Q(net_pay__gte=1, status='Approved'))
        if is_settled is not None:
            query_params.append(Q(is_settled=is_settled))

        if query_params:
            return reduce(operator.and_, query_params)
        return query_params


class DriverRestore(object):

    def restore_driver_app(self, driver):
        if driver:
            from blowhorn.common.notification import Notification
            json_data = {
                "blowhorn_notification": {
                    'username': driver.id,
                    'action': 'restore'
                }
            }
            Notification.push_to_fcm_device(driver.user, json_data)


def get_process_associate(instance):
    #instance is payment
    driver = instance.driver
    new_adjustment = instance.driverpaymentadjustment_set.filter(status=DriverPaymentAdjustment.NEW).first()
    if new_adjustment and new_adjustment.action_owner_id:
        return new_adjustment.action_owner_id

    process_associates = City.objects.filter(id=driver.operating_city_id, process_associates__is_active=True). \
        values_list('process_associates', flat=True)

    if not process_associates:
        process_associates = City.objects.filter(process_associates__is_active=True). \
            values_list('process_associates', flat=True)

    if not process_associates:
        process_associates = User.objects.filter(is_active=True, is_superuser=True). \
            values_list('id', flat=True)

    total_associates = process_associates.count()

    if total_associates > 0:
        return process_associates[random.randint(0, total_associates - 1)]
    return None



def create_document_event(driver_document=None, vehicle_document=None,
                          vendor_document=None, driver=None, vendor=None,
                          created_by=None):
    from blowhorn.document.constants import VERIFICATION_PENDING
    doc_query = Q(status=VERIFICATION_PENDING)
    # check the doc event for this doc exist , if present don't create new
    if driver_document:
        query = Q(driver_document=driver_document)
    elif vehicle_document:
        query = Q(vehicle_document=vehicle_document)
    elif vendor_document:
        query = Q(vendor_document=vendor_document)
    doc_query = doc_query & query

    existing_doc = DocumentEvent.copy_data.filter(doc_query).exists()
    if not existing_doc:
        spoc_id = DocumentUpload().get_spoc(
            driver=driver if driver else None,
            vendor=vendor if vendor else None,
            vehicle=vehicle_document.vehicle if vehicle_document else None)

        event = DocumentEvent(status=VERIFICATION_PENDING,
                              action_owner_id=spoc_id, created_by=created_by)
        archive = DocumentArchive(old_status=VERIFICATION_PENDING,
                                  action_owner_id=spoc_id,
                                  new_status=VERIFICATION_PENDING,
                                  created_by=created_by)
        doc_type = None
        if driver_document:
            event.driver_document = driver_document
            archive.driver_document = driver_document
            doc_type = driver_document.document_type
        elif vehicle_document:
            event.vehicle_document = vehicle_document
            archive.vehicle_document = vehicle_document
            doc_type = vehicle_document.document_type
        elif vendor_document:
            event.vendor_document = vendor_document
            archive.vendor_document = vendor_document
            doc_type = vendor_document.document_type

        event.save()
        event.created_by = created_by
        event.save()
        archive.document_event = event
        archive.save()

        user_id = None
        subject = None
        if driver:
            user_id = driver.id
            subject = driver
        elif vehicle_document:
            user_id = vehicle_document.vehicle.driver_set.first()
            subject = Driver.objects.get(pk=user_id) if user_id else vehicle_document.vehicle

        extra_context = {
            'subject': '%s' % subject,
            'document_type': '%s' % doc_type,
            'action_by': created_by.name or '%s' % created_by,
            'redirect_url': COSMOS_SCREEN_URLS.get(
                NOTIFICATION_GROUP_INTERNAL_VERIFICATION),
        }
        message_template = NOTIFICATION_TEMPLATES.get(
            NOTIFICATION_GROUP_INTERNAL_VERIFICATION, {}).get(REQUESTED)
        NotificationMixin().save_and_broadcast_notification(
            action_owners=[spoc_id],
            broadcast_list=[],
            action_by=created_by,
            action=REQUESTED,
            extra_context=extra_context,
            notification_group=NOTIFICATION_GROUP_INTERNAL_VERIFICATION,
            message=message_template.format(
                subject=extra_context.get('subject'),
                action_owner=extra_context.get('action_by'),
                document_type=doc_type,
            )
        )

        # mark_driver_onboarding(driver_document=driver_document,
        # vehicle_document=vehicle_document, driver=driver)


def get_subject_for_document(user, driver, host, doc_id, rejected):

    doc = DocumentEvent.copy_data.get(id=doc_id)

    if not rejected:
        subject = 'Driver/Vendor Document Verification mail '
        body = 'Dear %s, <br>' % user.name
        if driver:
            body += 'Driver %s has uploaded the document Please verify it <br>' % str(driver)
        else:
            body += 'New Document has been uploaded for verification , ' \
                    'please verify it <br>'
    else:
        subject = 'Driver/Vendor Document has been Rejected '

        body = 'Dear %s, <br>' % str(doc.created_by)
        body += '<br><br>Your document has been rejected. You can check the ' \
                'reason for rejection on Archive Tab,<br>'

    company = Company.objects.first()
    body += host + '/admin/document/documentevent/%s' % doc_id
    body += '<br><br>Regards,<br>' + 'Team %s'  %('Blowhorn' if settings.COUNTRY_CODE_A2 == 'IN' else company.name)

    return subject, body


def get_driver_missing_doc(driver, mandatory_driver_doc_type=None, mandatory_vehicle_doc_type=None, status=None):

    if not mandatory_driver_doc_type:
        mandatory_driver_doc_type = DocumentType.objects.filter(
            is_document_mandatory=True, identifier=DRIVER_DOCUMENT)

    if not mandatory_vehicle_doc_type:
        mandatory_vehicle_doc_type = DocumentType.objects.filter(
            is_document_mandatory=True,
            identifier=VEHICLE_DOCUMENT)

    doc = []
    missing_doc = mark_driver_onboarding(mandatory_driver_doc_type, driver, driver_document=True, status=status)
    if missing_doc:
        doc.extend(missing_doc)
    # assumption is that driver will be associated with one vehicle
    vehicle_map = driver.drivervehiclemap_set.all() if driver else None
    if vehicle_map and \
        vehicle_map[0].vehicle:
        missing_vehicle_doc = mark_driver_onboarding(mandatory_vehicle_doc_type,
                                                     driver,
                                                     vehicle=vehicle_map[
                                                         0].vehicle,
                                                     status=status)
        if missing_vehicle_doc:
            doc.extend(missing_vehicle_doc)
    else:
        doc.append('vehicle')
    return doc


def get_financial_year(date):
    if date.month < 4:
        financial_year = str(date.year - 1) + '-' + str(date.year)
    else:
        financial_year = str(date.year) + '-' + str(date.year + 1)
    return financial_year


def driver_push_notification_query(driver_criteria):
    query = Q(status='active')
    if driver_criteria:
        if driver_criteria.operating_cities.exists():
            query &= Q(operating_city__in=driver_criteria.operating_cities.all())
        if driver_criteria.own_vehicle:
            query &= Q(own_vehicle=True)
        if driver_criteria.vehicle_classes.exists():
            query &= Q(vehicles__vehicle_model__vehicle_class__in=driver_criteria.vehicle_classes.all())
        if driver_criteria.driver_ids:
            query &= Q(id__in=driver_criteria.driver_ids)
        drivers = Driver.objects.filter(query)
    else:
        drivers = Driver.objects.filter(query)
    return drivers
