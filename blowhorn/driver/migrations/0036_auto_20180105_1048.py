# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-05 10:48
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0035_auto_20171226_0944'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='can_drive_and_deliver',
            field=models.BooleanField(default=False, verbose_name='Interested In Driving plus Delivery?'),
        ),
        migrations.AddField(
            model_name='driver',
            name='can_read_english',
            field=models.BooleanField(default=True, verbose_name='Can you read basic english?'),
        ),
        migrations.AddField(
            model_name='driver',
            name='can_source_additional_labour',
            field=models.BooleanField(default=False, verbose_name='Can source additional labour'),
        ),
        migrations.AddField(
            model_name='driver',
            name='work_status',
            field=models.CharField(blank=True, choices=[('OCCUPIED', 'Occupied with other vendor'), ('AVAILABLE', 'Available')], max_length=25, null=True, verbose_name='Present working status?'),
        ),
        migrations.AddField(
            model_name='driver',
            name='working_preferences',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, choices=[('ADHOC', 'Adhoc'), ('MONTHLY', 'Monthly'), ('SME', 'sme')], max_length=100, null=True, verbose_name='Working Preference'), null=True, size=None),
        ),
    ]
