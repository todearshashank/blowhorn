# Generated by Django 2.1.1 on 2019-08-08 15:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0114_auto_20190807_0933'),
    ]

    operations = [
        migrations.AddField(
            model_name='driverpayment',
            name='is_fifty_percent_settlement',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Is fifty percent settled'),
        ),
    ]
