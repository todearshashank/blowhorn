# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-13 13:11
from __future__ import unicode_literals

import blowhorn.document.utils
from decimal import Decimal
import django.core.validators
from django.db import migrations, models
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0030_auto_20171212_0945'),
    ]

    operations = [
        migrations.AddField(
            model_name='drivercashpaid',
            name='proof',
            field=models.FileField(max_length=256, null=True, upload_to=blowhorn.document.utils.generate_file_path),
        ),
        migrations.AddField(
            model_name='drivercashpaid',
            name='status',
            field=django_fsm.FSMField(default='New', max_length=50),
        ),
        migrations.AlterField(
            model_name='drivercashpaid',
            name='cash_paid',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))], verbose_name='Cash Paid Amount'),
        ),
    ]
