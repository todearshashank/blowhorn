# Generated by Django 2.2.23 on 2021-06-24 03:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0154_auto_20210414_0640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='backgroundverification',
            name='source',
            field=models.CharField(choices=[('Cosmos', 'Cosmos'), ('Website', 'Website'), ('Self Signup', 'Self Signup'), ('Driver Referral', 'Driver Referral')], default='Website', max_length=20, verbose_name='Verification Sent Via'),
        ),
        migrations.AlterField(
            model_name='driver',
            name='source',
            field=models.CharField(choices=[('Cosmos', 'Cosmos'), ('Website', 'Website'), ('Self Signup', 'Self Signup'), ('Driver Referral', 'Driver Referral')], default='Website', max_length=20, verbose_name='Source'),
        ),
    ]
