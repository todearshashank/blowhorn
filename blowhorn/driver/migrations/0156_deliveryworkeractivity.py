# Generated by Django 2.2.24 on 2021-12-04 08:27

import audit_log.models.fields
from django.conf import settings
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trip', '0064_tripmview'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('address', '0052_auto_20211012_1607'),
        ('driver', '0155_auto_20210624_0332'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryWorkerActivity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('activity_time', models.DateTimeField(blank=True, null=True)),
                ('activity', models.CharField(blank=True, choices=[('Login', 'Login'), ('Logout', 'Logout'), ('In Trip', 'In Trip')], max_length=50, null=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=4326)),
                ('app_version', models.CharField(blank=True, max_length=100, null=True, verbose_name='App Version used')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='deliveryworkeractivity_created_by', to=settings.AUTH_USER_MODEL)),
                ('driver', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='driver.Driver')),
                ('hub', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='address.Hub')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='deliveryworkeractivity_modified_by', to=settings.AUTH_USER_MODEL)),
                ('trip', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='trip.Trip')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Delivery Worker Activity',
                'verbose_name_plural': 'Delivery Worker Activities',
            },
        ),
    ]
