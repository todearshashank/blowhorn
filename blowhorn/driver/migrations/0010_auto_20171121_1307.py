# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-21 13:07
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0009_auto_20171120_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bankaccount',
            name='ifsc_code',
            field=models.CharField(help_text='First 4 Alphabets, and then next 7 digits', max_length=11, validators=[django.core.validators.RegexValidator(message='First 4 Alphabets, and then next 7 digits', regex='^[A-Za-z]{4}\\d{7}$'), django.core.validators.MinLengthValidator(11)], verbose_name='IFSC Code for the bank'),
        ),
    ]
