# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-16 10:09
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('driver', '0005_driverledger_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='drivercashpaid',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='drivercashpaid_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='drivercashpaid',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='drivercashpaid',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='drivercashpaid_modified_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='drivercashpaid',
            name='modified_date',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='driverpaymentadjustment',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='driverpaymentadjustment_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='driverpaymentadjustment',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='driverpaymentadjustment',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='driverpaymentadjustment_modified_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='driverpaymentadjustment',
            name='modified_date',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='nonoperationpayment',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='nonoperationpayment_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='nonoperationpayment',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='nonoperationpayment',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='nonoperationpayment_modified_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='nonoperationpayment',
            name='modified_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
