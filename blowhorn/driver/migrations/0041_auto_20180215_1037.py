# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-02-15 10:37
from __future__ import unicode_literals

from django.db import migrations, models
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0040_auto_20180130_1143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='is_address_same',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Permanent address is same as current address'),
        ),
        migrations.AlterField(
            model_name='driver',
            name='own_vehicle',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Own Vehicle?'),
        ),
        migrations.AlterField(
            model_name='driver',
            name='status',
            field=models.CharField(choices=[('supply', 'Supply'), ('onboarding', 'Onboarding'), ('active', 'Active'), ('inactive', 'Inactive')], db_index=True, default='supply', max_length=15, verbose_name='Status'),
        ),
        migrations.AlterField(
            model_name='drivercashpaid',
            name='status',
            field=django_fsm.FSMField(db_index=True, default='New', max_length=50),
        ),
        migrations.AlterField(
            model_name='driverledger',
            name='is_settlement',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Is Settlement'),
        ),
        migrations.AlterField(
            model_name='driverledger',
            name='status',
            field=django_fsm.FSMField(db_index=True, default=None, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='driverpayment',
            name='end_datetime',
            field=models.DateField(db_index=True, verbose_name='Until'),
        ),
        migrations.AlterField(
            model_name='driverpayment',
            name='payment_type',
            field=models.CharField(blank=True, choices=[('Fixed', 'Fixed'), ('Bonus', 'Bonus')], db_index=True, max_length=25, null=True, verbose_name='Payment Type'),
        ),
        migrations.AlterField(
            model_name='driverpayment',
            name='start_datetime',
            field=models.DateField(db_index=True, verbose_name='From'),
        ),
        migrations.AlterField(
            model_name='driverpayment',
            name='status',
            field=django_fsm.FSMField(db_index=True, default='Unapproved', max_length=50),
        ),
        migrations.AlterField(
            model_name='driverpaymentadjustment',
            name='category',
            field=models.CharField(blank=True, choices=[('Disciplinary Issue', 'Disciplinary Issue'), ('Buy Rate Difference', 'Buy Rate Difference'), ('Toll Charges', 'Toll Charges'), ('Labour Charges', 'Labour Charges'), ('Previous Balance', 'Previous Balance'), ('Miscellaneous', 'Miscellaneous'), ('Parking Fees', 'Parking Fees')], db_index=True, max_length=25, null=True, verbose_name='Payment Category'),
        ),
        migrations.AlterField(
            model_name='driverpaymentadjustment',
            name='status',
            field=django_fsm.FSMField(db_index=True, default='New', max_length=50),
        ),
        migrations.AlterField(
            model_name='nonoperationpayment',
            name='end_datetime',
            field=models.DateField(db_index=True, verbose_name='Payment Until'),
        ),
        migrations.AlterField(
            model_name='nonoperationpayment',
            name='start_datetime',
            field=models.DateField(db_index=True, verbose_name='Payment From'),
        ),
        migrations.AlterField(
            model_name='nonoperationpayment',
            name='status',
            field=django_fsm.FSMField(db_index=True, default='New', max_length=50),
        ),
    ]
