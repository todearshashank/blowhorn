# Generated by Django 2.1.1 on 2019-10-16 08:06

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('vehicle', '0034_auto_20191001_1138'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('driver', '0122_driverpaymentadjustmenthistory'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankAccountHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('action', models.CharField(blank=True, max_length=256, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='bankaccount_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='bankaccount_modified_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='modified_date',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='bankaccounthistory',
            name='bank_account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='driver.BankAccount', verbose_name='Bank Details'),
        ),
        migrations.AddField(
            model_name='bankaccounthistory',
            name='driver',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='driver.Driver'),
        ),
        migrations.AddField(
            model_name='bankaccounthistory',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bankaccounthistory',
            name='vendor',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='vehicle.Vendor', verbose_name='Fleetowner'),
        ),
    ]
