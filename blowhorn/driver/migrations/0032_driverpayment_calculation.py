# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-18 09:14
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0031_auto_20171213_1311'),
    ]

    operations = [
        migrations.AddField(
            model_name='driverpayment',
            name='calculation',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True),
        ),
    ]
