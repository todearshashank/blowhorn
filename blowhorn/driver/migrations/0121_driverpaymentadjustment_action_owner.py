# Generated by Django 2.1.1 on 2019-10-01 17:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('driver', '0120_auto_20190929_0801'),
    ]

    operations = [
        migrations.AddField(
            model_name='driverpaymentadjustment',
            name='action_owner',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL),
        ),
    ]
