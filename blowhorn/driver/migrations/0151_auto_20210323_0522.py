# Generated by Django 2.2.19 on 2021-03-23 05:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0150_driver_partner'),
    ]

    operations = [
        migrations.AddField(
            model_name='driverpayment',
            name='volume_payment',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, verbose_name='Volume Slab Pay'),
        ),
        migrations.AddField(
            model_name='driverpayment',
            name='weight_payment',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, verbose_name='Weight Slab Pay'),
        ),
    ]
