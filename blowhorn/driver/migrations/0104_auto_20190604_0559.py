# Generated by Django 2.1.1 on 2019-06-04 05:59

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0030_auto_20190604_0559'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('trip', '0045_auto_20190604_0559'),
        ('driver', '0103_auto_20190401_1004'),
    ]

    operations = [
        migrations.CreateModel(
            name='CashHandover',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('amount', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True, verbose_name='Amount collected')),
                ('collected_by', models.ForeignKey(limit_choices_to={'is_staff': True}, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='cashhandover_created_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='driver',
            name='cod_balance',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='cod amount with driver'),
        ),
        migrations.AddField(
            model_name='cashhandover',
            name='driver',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='driver.Driver'),
        ),
        migrations.AddField(
            model_name='cashhandover',
            name='hub',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='address.Hub'),
        ),
        migrations.AddField(
            model_name='cashhandover',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='cashhandover_modified_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='cashhandover',
            name='trip',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='trip.Trip'),
        ),
    ]
