# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-06-08 07:05
from __future__ import unicode_literals

import audit_log.models.fields
from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0016_state_rcm_message'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('driver', '0067_drivererror'),
    ]

    operations = [
        migrations.AddField(
            model_name='backgroundverification',
            name='created_by',
            field=audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='backgroundverification_created_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='backgroundverification',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='backgroundverification',
            name='document_type',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, choices=[('DRIVING_LICENCE', 'Driving Licence'), ('VOTER_ID', 'Voter ID'), ('AADHAR', 'AADHAR'), ('BANK_ACCOUNT', 'Bank Account'), ('PAN', 'PAN'), ('RC', 'RC')], max_length=100, null=True, verbose_name='Documents to be Verified'), null=True, size=None),
        ),
        migrations.AddField(
            model_name='backgroundverification',
            name='modified_by',
            field=audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='backgroundverification_modified_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='backgroundverification',
            name='modified_date',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='driver',
            name='father_name',
            field=models.CharField(blank=True, db_index=True, max_length=255, null=True, verbose_name="Father's Name"),
        ),
        migrations.AddField(
            model_name='driverdocument',
            name='state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='address.State', verbose_name='Document Issuing State'),
        ),
        migrations.AlterField(
            model_name='backgroundverification',
            name='driver',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='driver.Driver'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='courtrecord',
            name='background_verification',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='driver.BackgroundVerification'),
        ),
    ]
