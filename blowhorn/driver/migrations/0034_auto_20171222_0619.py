# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-12-22 06:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0033_auto_20171220_1031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driverpayment',
            name='end_datetime',
            field=models.DateField(verbose_name='Until'),
        ),
        migrations.AlterField(
            model_name='driverpayment',
            name='start_datetime',
            field=models.DateField(verbose_name='From'),
        ),
    ]
