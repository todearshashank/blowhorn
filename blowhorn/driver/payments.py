from datetime import timedelta, datetime

import logging
from django.conf import settings
from django.utils import timezone
from django.db import transaction
from django.db.models import F, ExpressionWrapper, DateTimeField, Q
from django.db.models import Prefetch

from blowhorn.oscar.core.loading import get_model

from blowhorn.utils.functions import IST_OFFSET_HOURS
from blowhorn.driver.constants import (
    PAYMENT_TYPE_FIXED,
    PAYMENT_TYPE_BONUS,
    #APPROVED,
    UNAPPROVED,
    CLOSED
)
from blowhorn.contract.utils import SubscriptionService
from blowhorn.contract.constants import CONTRACT_TYPE_SME, CONTRACT_TYPE_C2C, CONTRACT_TYPE_BLOWHORN
from blowhorn.driver.payment_helpers.contract_helper import ContractHelper
from blowhorn.driver.payment_helpers.trips_helper import TripSet
from blowhorn.driver.payment_helpers.buy_rate_helper import BuyRateHelper
from config.settings import status_pipelines as StatusPipeline

Trip = get_model('trip', 'Trip')
Driver = get_model('driver', 'Driver')
DriverPayment = get_model('driver', 'DriverPayment')
PaymentTrip = get_model('driver', 'PaymentTrip')
PaymentBatch = get_model('driver','PaymentBatch')
User = get_model('users', 'User')

DATE_FORMAT = "%d-%b-%Y"

#temporary fix to consider payment status as Approved
APPROVED = u'Approved'

# max past days for which to generate fixed driver payments
MAX_PAST_DAYS = 30
C2C_REFERENCE_DATE = datetime(2018, 2, 16).date()

expression_planned_start_time_ist = ExpressionWrapper(
    F('planned_start_time') + timedelta(hours=IST_OFFSET_HOURS), output_field=DateTimeField()
)

expression_actual_end_time_ist = ExpressionWrapper(
    F('actual_end_time') + timedelta(hours=IST_OFFSET_HOURS), output_field=DateTimeField()
)


class PaymentService(object):

    def _get_trips(self, contract, driver, start_date, end_date):
        """
        Returns all the trips for driver and contract
        for given date range
        """
        query = Q(driver=driver)
        if contract.contract_type == CONTRACT_TYPE_BLOWHORN:
            query = query & Q(blowhorn_contract=contract)
        else:
            query = query & Q(customer_contract=contract, blowhorn_contract__isnull=True)

        trips = Trip.objects \
            .filter(query)\
            .annotate(planned_start_time_ist=expression_planned_start_time_ist,
            actual_end_time_ist=expression_actual_end_time_ist) \
            .filter(planned_start_time_ist__date__range=(start_date, end_date))

        trips = trips.select_related('order')
        trips = trips.select_related('order__labour')

        return trips

    def _has_suspended_trips(self, trips):
        """
        Returns True if trip set has suspend Trips
        """

        for trip in trips:
            if trip.status == StatusPipeline.TRIP_SUSPENDED:
                return True

        return False

    def _get_unique_drivers_for_date_range(self, contract, start_date, end_date):
        """
        if the driver is absent for all the days till now
        fetch unique trips based on planned start time
        and generate payment
        """
        query = Q(driver__isnull=False)

        if contract.contract_type == CONTRACT_TYPE_BLOWHORN:
            query = query & Q(blowhorn_contract = contract)
        else:
            query = query & Q(customer_contract=contract, blowhorn_contract__isnull =True)

        driver_ids = Trip.objects \
            .filter(query) \
            .annotate(planned_start_time_ist=expression_planned_start_time_ist) \
            .filter(planned_start_time_ist__date__range=(start_date, end_date))\
            .distinct('driver') \
            .values_list('driver', flat=True)

        return Driver.objects.filter(pk__in=driver_ids)

    def _create_payment_record(self, contract, driver, start_date, end_date, payment_type):
        payment_record = DriverPayment(
            contract=contract,
            driver=driver,
            owner_details=driver.owner_details,
            start_datetime=start_date,
            end_datetime=end_date,
            payment_type=payment_type,
        )

        return payment_record

    def _get_payment_record(self, contract, driver, start_date, end_date, payment_type, contingency_trip=None):
        payment_records = DriverPayment.objects \
            .filter(contract=contract) \
            .filter(driver=driver) \
            .filter(start_datetime__gte=start_date) \
            .filter(end_datetime__lte=end_date) \
            .filter(payment_type=payment_type)

        if contingency_trip:
            payment_records = payment_records.filter(paymenttrip__trip=contingency_trip)
        elif contract.contract_type != CONTRACT_TYPE_BLOWHORN:
            payment_records = payment_records.filter(paymenttrip__trip__is_contingency=False)

        payment_records = payment_records.prefetch_related(
            Prefetch('paymenttrip_set__trip',
                     queryset=Trip.objects.annotate(
                         planned_start_time_ist=expression_planned_start_time_ist,
                         actual_end_time_ist=expression_actual_end_time_ist
                     ),
                     )
        )

        if not payment_records:
            payment_records = [self._create_payment_record(
                contract, driver, start_date, end_date, payment_type)]

        return payment_records

    def _do_for_a_driver(self, driver, contract, trips, buy_rate_helper, start_date, end_date,
                            bonus=False, contingency_trip=None, batch=None):
        """ Generate the payment for a driver with given contract, buy rate and date range """

        payment_type = PAYMENT_TYPE_FIXED
        if bonus:
            payment_type = PAYMENT_TYPE_BONUS

        payment_records = self._get_payment_record(
            contract, driver, start_date, end_date, payment_type, contingency_trip
        )

        approved_prs = [pr for pr in payment_records if pr.status == APPROVED]
        other_prs = [pr for pr in payment_records if pr.status != APPROVED]

        logging.debug('\t\t\tTrips : %s' % trips)

        unaccounted_trips = set(trips)
        accounted_trips = set()

        if approved_prs:
            logging.info(
                '\t\t\tPayments already approved. %s. Checking for unaccounted Trips/Unapproved Payments!' % approved_prs)

        for apr in approved_prs + other_prs:
            accounted_trips.update(
                set([pt.trip for pt in apr.paymenttrip_set.all()]))
            unaccounted_trips.difference_update(accounted_trips)

        logging.info('\t\t\tNew/Unaccounted Trips : %s' % unaccounted_trips)

        unapproved_prs = [pr for pr in other_prs if pr.status == UNAPPROVED]

        if not unaccounted_trips and not unapproved_prs:
            # No unaccounted trips and no unapproved payment records
            # Nothing to do. Return.
            return 0, 0

        update_rec = 0
        insert_rec = 0

        trips_to_consider = unaccounted_trips
        if unapproved_prs:
            logging.info('\t\t\tUnapproved Payment Record : %s' % unapproved_prs)
            payment_record = unapproved_prs[0]
            logging.info('\t\t\tEarnings = %s' % payment_record.total_earnings)
            trips_to_consider = unaccounted_trips.union(
                set([pt.trip for pt in payment_record.paymenttrip_set.all()]))
            update_rec = 1
        else:
            logging.info('\t\t\tCreating new Payment Record')
            payment_record = self._create_payment_record(
                contract, driver, start_date, end_date, payment_type)
            insert_rec = 1

        if not trips_to_consider:
            assert False, 'Cannot come to this part without trips to consider. Check your code!!'

        logging.debug('\t\t\tConsidered Trips : %s' % trips_to_consider)

        trip_set = TripSet(trips_to_consider, contract=contract,
                           dp_contract_type=contract.contract_type)
        payment_data = buy_rate_helper.get_payment_data(trip_set, bonus=bonus,
                                                        contingency_trip=contingency_trip)
        payment_data.update({'owner_details': driver.owner_details})

        total_earnings = payment_data.get('total_earnings', 0)

        if total_earnings == 0 and payment_record.total_earnings is None and \
            not self._has_suspended_trips(trips_to_consider):
            # If the total earnings is zero and previous record doesn't exist, don't
            # save this record. Otherwise do save. Need to clear stale flag and also
            # if the old value was non-zero then need to update.
            logging.info('\t\t\tEarnings = %s and no previous Record. Not saving!' %
                         total_earnings)
            return 0, 0

        for field, value in payment_data.items():
            if isinstance(value, str) or isinstance(value, object):
                setattr(payment_record, field, value)
            else:
                setattr(payment_record, field, float(value))

        with transaction.atomic():
            payment_record.is_stale = False
            payment_record.can_approve = trip_set.data_complete
            if contingency_trip:
                payment_record.start_datetime = contingency_trip.planned_start_time_ist.date()
                payment_record.end_datetime = contingency_trip.planned_start_time_ist.date()
            else:
                payment_record.start_datetime = start_date
                payment_record.end_datetime = end_date
            payment_record.buy_rate = buy_rate_helper.buy_rate
            if batch:
                payment_record.batch = batch

            payment_record.save()

            present_trips = [
                pt.trip for pt in payment_record.paymenttrip_set.all()]
            new_payment_trips = [PaymentTrip(payment=payment_record, trip=trip)
                                 for trip in trips_to_consider if trip not in present_trips]
            PaymentTrip.objects.bulk_create(new_payment_trips)

            logging.info('\t\t\tPk = %s, Earnings = %.1f, GrossPay = %0.1f' % (
                payment_record.pk, payment_record.total_earnings, payment_record.gross_pay)
                         )

    #            logging.info('\t\t%s' % json.dumps(payment_data.get('calculation'), indent=2))
            return update_rec, insert_rec

    def get_contracts(self, *args, **kwargs):
        i_contract = kwargs.pop('contract', None)
        i_contract_id = kwargs.pop('contract_id', None)
        contract_list = kwargs.pop('contract_list', None)
        contracts = None
        if i_contract and i_contract_id:
            return
        if i_contract:
            contracts = SubscriptionService().get_active_contracts(contract=i_contract)
        elif i_contract_id:
            contracts = SubscriptionService().get_active_contracts(contract_id=i_contract_id)
        elif contract_list:
            contracts = SubscriptionService().get_active_contracts(contract_list=contract_list)
        else:
            contracts = SubscriptionService().get_active_contracts()

        contracts = contracts.prefetch_related(
            'payment_cycle__paymentinterval_set')
        contracts = contracts.prefetch_related('contractterm_set__buy_rate')
        contracts = contracts.prefetch_related(
            'contractterm_set__buy_rate__buyvehicleslab_set')
        contracts = contracts.prefetch_related(
            'contractterm_set__buy_rate__buyshipmentslab_set')
        return contracts

    def _get_max_past_days(self, contract):
        if contract.contract_type in [CONTRACT_TYPE_SME, CONTRACT_TYPE_C2C]:
            return min(
                (timezone.now().date() - C2C_REFERENCE_DATE).days + 1,
                MAX_PAST_DAYS
            )
        else:
            return MAX_PAST_DAYS

    def _get_fixed_and_bonus_intervals(self, contract_helper, days=31):
        """ An interval is a tuple of start and end date """
        now = timezone.now()
        fixed_intervals = set()
        bonus_intervals = set()
        for i in range(days):
            date = (now - timedelta(days=i)).date()
            fixed_interval = contract_helper.get_payment_cycle_for_given_date(
                date)
            fixed_intervals.add(fixed_interval)

            bonus_interval = contract_helper.get_payment_month_for_given_date(
                date)
            bonus_intervals.add(bonus_interval)

        return fixed_intervals, bonus_intervals

    def _merge_fixed_and_bonus(self, fixed, bonus):
        """ Merge the bonus interval and fixed intervals
            contained in it """
        intervals = []
        for b in bonus:
            intervals.append(
                (b, [f for f in fixed if f[0] >= b[0] and f[1] <= b[1]])
            )
        return intervals

    def generate_payments(self, *args, **kwargs):
        """
        Generate Driver Payments
        """

        start_time = timezone.now()
        logging.info("Generating Payments. Start Time = %s" % start_time)

        date_batch = start_time.date()
        desc_batch = date_batch.strftime('%d-%m-%Y')
        desc_batch = "Driver Payment  %s" %(desc_batch)
        batch = PaymentBatch.objects.create(description=desc_batch, number_of_entries=0)
        update_processed = 0
        insert_processed = 0
        days = kwargs.pop('days', None)

        contracts = self.get_contracts(*args, **kwargs)
        if not contracts:
            logging.info('No active contracts')
            return

        for contract in contracts:
            logging.info('')
            logging.info("** %s **" % (contract))

            contract_helper = ContractHelper(contract)

            if not days:
                days = self._get_max_past_days(contract_helper.contract)

            fixed_intervals, bonus_intervals = self._get_fixed_and_bonus_intervals(
                contract_helper, days=days)
            intervals = self._merge_fixed_and_bonus(
                fixed_intervals, bonus_intervals)

            # bi = Bonus Interval(full month), fi = Fixed Interval (Cycle)
            # One bonus interval (month) can have multiple fixed intervals
            for bi, fis in sorted(intervals, reverse=True):
                logging.info("Month Date Range : %s to %s" % (
                    bi[0].strftime(DATE_FORMAT), bi[1].strftime(DATE_FORMAT)))

                drivers = self._get_unique_drivers_for_date_range(
                    contract, bi[0], bi[1])

                sorted_fis = sorted(fis, reverse=True)
                for driver in drivers:
                    buy_rate = contract_helper.get_buy_rate(bi[1], contract, driver)

                    if buy_rate is None:
                        logging.info('\tBuy Rate is None. Skipping!')
                        continue
                    buy_rate_helper = BuyRateHelper(buy_rate)
                    logging.info('')
                    logging.info("\tDriver : %s" % driver)
                    trips = self._get_trips(contract, driver, bi[0], bi[1])

                    for fi in sorted_fis:
                        logging.info("\t\tCycle : %s to %s" % (
                            fi[0].strftime(DATE_FORMAT), fi[1].strftime(DATE_FORMAT)))

                        fibuy_rate = contract_helper.get_buy_rate(fi[1], contract, driver)
                        if not fibuy_rate:
                            logging.info('\tFixed Buy Rate is None. Skipping!')
                            continue

                        fibuy_rate_helper = BuyRateHelper(fibuy_rate)

                        if contract.contract_type == CONTRACT_TYPE_BLOWHORN:
                            fi_trips = [trip for trip in trips if trip.planned_start_time_ist.date() >= fi[0] and
                                        trip.planned_start_time_ist.date() <= fi[1]]
                            if not fi_trips:
                                continue
                            upd_rec, ins_rec = self._do_for_a_driver(
                                driver, contract, fi_trips, fibuy_rate_helper, fi[0], fi[1], bonus=False, batch=batch)
                            update_processed += upd_rec
                            insert_processed += ins_rec
                        else:
                            fi_trips = [trip for trip in trips if trip.planned_start_time_ist.date() >= fi[0] and
                                        trip.planned_start_time_ist.date() <= fi[1] and
                                        not trip.is_contingency]

                            if fi_trips:
                                upd_rec, ins_rec = self._do_for_a_driver(
                                    driver, contract, fi_trips, fibuy_rate_helper, fi[0], fi[1], bonus=False, batch=batch)
                                update_processed += upd_rec
                                insert_processed += ins_rec

                            contingency_trips = [trip for trip in trips
                                                if trip.planned_start_time_ist.date() >= fi[0] and
                                                trip.planned_start_time_ist.date() <= fi[1] and
                                                trip.is_contingency]

                            for contingency_trip in contingency_trips:
                                upd_rec, ins_rec = self._do_for_a_driver(
                                    driver, contract, [contingency_trip], fibuy_rate_helper, fi[0], fi[1],
                                                bonus=False, contingency_trip=contingency_trip, batch=batch)
                                update_processed += upd_rec
                                insert_processed += ins_rec

                    if buy_rate_helper and not buy_rate_helper.has_bonus_slabs():
                        logging.info('\t\tBuy Rate has no bonus slabs. Skipping!')
                        continue

                    logging.info("\t\tBonus : %s to %s" % (
                        bi[0].strftime(DATE_FORMAT), bi[1].strftime(DATE_FORMAT)))

                    if contract.contract_type == CONTRACT_TYPE_BLOWHORN:
                        upd_rec, ins_rec = self._do_for_a_driver(
                            driver, contract, trips, buy_rate_helper, bi[0], bi[1], bonus=True, batch=batch)
                        update_processed += upd_rec
                        insert_processed += ins_rec
                    else:
                        non_contingency_trips = [trip for trip in trips if not trip.is_contingency]
                        if non_contingency_trips:
                            upd_rec, ins_rec = self._do_for_a_driver(
                                driver, contract, non_contingency_trips, buy_rate_helper, bi[0], bi[1], bonus=True, batch=batch)
                            update_processed += upd_rec
                            insert_processed += ins_rec
                        contingency_trips = [trip for trip in trips if trip.is_contingency]
                        for contingency_trip in contingency_trips:
                            cont_trip = [contingency_trip]
                            upd_rec, ins_rec = self._do_for_a_driver(
                                driver, contract, cont_trip, buy_rate_helper, bi[0], bi[1], bonus=True,
                                contingency_trip=contingency_trip, batch=batch)
                            update_processed += upd_rec
                            insert_processed += ins_rec

        end_time = timezone.now()
        batch.end_time = end_time
        batch.number_of_entries = insert_processed
        batch.records_updated = update_processed
        batch.save()
        total_time = end_time - start_time
        logging.info('Total time taken = %.1f seconds' % total_time.total_seconds())
