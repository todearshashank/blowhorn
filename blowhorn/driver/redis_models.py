import json
import redis
import re
from django.conf import settings

from blowhorn.common.redis_ds.redis_list import RedisList
from blowhorn.common.redis_ds.redis_dict import RedisDict
from blowhorn.driver.constants import DRIVER_INSTANCE_REDIS_PATTERN

REDIS_HOST = getattr(settings, 'REDIS_HOST', 'localhost')


class ActivityLog(RedisList):

    def append(self, val):
        assert type(val) is dict, "Failed to insert - Unsupported type. " \
                                     "Only `dict` objects are allowed."
        self.rpush(val)

    def delete(self, force=True):
        """ Delete this object from redis"""

        if force:
            return super(ActivityLog, self).delete()

        raise ValueError("Cannot delete this instance from Redis.")


class Driver(RedisDict):
    """ BlowHorn driver specific details.
    Please note that any details found on redis will be real-time and would be
    different from past. Example : vehicle_reg_no
    Any driver related data expected to be stored (or retrieved) from cache is
    to be handled with an instance of `Driver` class. """

    def __init__(self, id, **kwargs):

        RedisDict.__init__(
            self,
            id=str(id),
            fields=self.__get_model(),
            defaults=kwargs
        )

        self['ind_last_synced_log'] = -1 \
            if len(self['activity_log']) == 0 else \
            self['ind_last_synced_log']

    def __get_model(self):
        """ Returns the model for driver. Any new fields if required for driver
        in Redis should be added here in below format in below dictionary.
        <name> : <value_type>
        Example : <name> : str"""

        return {
            'activity_log': ActivityLog.as_child(self, 'activity_log', json),
            'ind_last_synced_log': int,
            'vehicle': dict,
            'ongoing': json,
            'device_details': json
        }

    def filter_activity_logs(self, *queries, order_by=None):
        """ Returns the activity log given the filters.
        """

        logs = self['activity_log']
        for query in queries:
            for field, value, relate in query:
                logs = [log for log in logs if relate(value, log[field])]
        return sorted(
            logs, key=lambda x: x.get(order_by, "")) if order_by else logs

    @staticmethod
    def all():
        """ Returns list of ids for all driver instances on Redis.
        """

        driver_ids = []
        redis_obj = redis.StrictRedis(host=REDIS_HOST, decode_responses=True)
        for key in redis_obj.keys():
            if re.fullmatch(DRIVER_INSTANCE_REDIS_PATTERN, key):
                driver_id = re.search("[0-9]+", key).group(0)
                driver_ids.append(driver_id)
        return driver_ids

    def delete(self, force=True):
        """ Delete this object from redis"""

        if force:
            Driver(self.id)['activity_log'].delete(force=True)
            return super(Driver, self).delete()

        raise ValueError("Cannot delete this instance from Redis.")
