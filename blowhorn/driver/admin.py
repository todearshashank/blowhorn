# Django and System libraries
import os
import csv
import pytz
import decimal
import phonenumbers
import json
import logging
from datetime import timedelta, date
from django.contrib import admin
from django.contrib import messages as admin_messages
from django.db.models import Q, Prefetch, Sum, Count, \
Func, Value, F, CharField, Case, When, IntegerField
from django.utils.safestring import mark_safe
from django import forms
from django.contrib.gis.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import SimpleListFilter
from django.utils.html import format_html
from django.urls import reverse
from django.utils import timezone
from django.forms.models import BaseInlineFormSet
from django.conf import settings
from django.db import transaction
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.contrib.postgres.aggregates import StringAgg, ArrayAgg
from django.contrib.admin.helpers import ActionForm
from blowhorn.users.models import User
from .payment_transition_conditions import can_approve_payment, is_current_user_can_approve
from ckeditor.widgets import CKEditorWidget
from django.contrib.postgres.fields import ArrayField
from array_tags import widgets

# 3rd party libraries
# from blowhorn.oscar.apps.order.admin import *
from import_export.admin import ExportMixin
from mapwidgets.widgets import GooglePointFieldWidget, \
    GooglePointFieldInlineWidget
from blowhorn.oscar.core.loading import get_model
from jet.filters import RelatedFieldAjaxListFilter
from django.contrib.admin import AdminSite

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from .models import (
    Driver, DriverAddress, AlternateContact, DriverConstants, DriverInactiveReason,
    DriverDocument, DriverDocumentPage, BankAccount, DriverVehicleMap, CashHandover,
    DriverActivity, DriverLogInfo, DriverPaymentDetails,
    DriverPaymentAdjustment, DriverCashPaid, UNAPPROVED, APPROVED,
    PaymentTrip, DriverPayment, DriverPaymentHistory, DriverLedger,
    DriverLedgerBatch, DriverBalance, NonOperationPayment,
    DriverActivityHistory, DriverHistory, DriverPreferredLocation,
    DriverLogActivity, DriverLogBatch, PaymentBatch, DriverReferrals,
    DriverReferralConfiguration, BackGroundVerification, DriverError,
    DeviceDetails, LoadPreference, DriverPaymentSettlementView, DriverPaymentSettlementReversalView,
    DriverDeduction, DriverGPSInformation, DriverLoanDetails, DriverTds,
    DriverPaymentAdjustmentHistory, BankAccountHistory, BankMigrationHistory, DriverAcknowledgement, DriverWellBeing,
    EventType, NotificationCampaign, DriverNotificationCriteria, DriverNotificationResponse, DriverSourcingOption,
    DriverResponseFields, DriverResponse, NotificationContentLanguage, NotificationFormLanguage
)
from .forms import (
    DriverForm, DriverEditForm, NonOpsForm,
    BankAccountCreationForm, BankAccountChangeForm, DriverAdjustmentForm,
    DriverAddressForm, AlternateContactForm, AddDriverAdjustmentInlineForm,
    BackGroundVerificationForm, AddDriverCashPaidInlineForm, BankAccountActionForm, DriverWellBeingFormset
)
from .util import DriverSettlement, get_financial_year
from .resource import (
    DriverAdjustmentResource,
    NonOperationPaymentResource, DriverResource,
    DriverActivityHistoryResource
)
from .mixins import BankAccountUtils, NotificationMixin
from blowhorn.document.constants import DRIVER_DOCUMENT, VERIFICATION_PENDING
from blowhorn.contract.admin import ContractFilter
from .payment_helpers.buy_rate_helper import \
    driver_payment_calculation_formatter
# from .constants import PAYMENT_TYPE_FIXED
from .util import DriverFilter
from blowhorn.utils.datetimepicker import DateTimePicker
from blowhorn.utils.functions import get_in_ist_timezone, utc_to_ist
from blowhorn.utils.fsm_mixins import FSMTransitionMixin
from blowhorn.address.utils import AddressUtil, state_city_cache
from blowhorn.address.constants import UPDATE_FLAG
from blowhorn.driver import messages
from blowhorn.trip.models import Trip
from blowhorn.utils.datetimerangefilter import DateRangeFilter, DateFilter
from blowhorn.utils.crypt import Cipher
from blowhorn.document.widgets import ImagePreviewWidget
from blowhorn.utils.functions import minutes_to_dhms
from blowhorn.utils.filters import GenericDependancyFilter
from blowhorn.driver.constants import DRIVER_ALLOWED_FIELDS_FOR_EDIT, \
    MANDATORY_DOCUMENT_TYPES, CLOSED, UNAPPROVED, WITHHELD, CLOSED, TRIP_PACKAGES_LIST
from blowhorn.contract.constants import CONTRACT_TYPE_BLOWHORN, \
    CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME
from blowhorn.contract.models import Contract
from blowhorn.address.models import City, State
from .subadmins import asset_admin
from .subadmins import old_paymentadmin
from .submodels.asset_models import Asset
from .subadmins.forms import AssetForm, AssetInlineFormset
from blowhorn.driver.util import get_data_for_background_verification, \
    create_bgv_profile, ContractTypeFilter
from blowhorn.address.models import Hub
from blowhorn.address.permissions import is_city_manager
from blowhorn.apps.operation_app.v3.services.driver import\
    DriverBackGroundVerificationMixin
from blowhorn.driver.util import DriverPaymentExportClass
from blowhorn.common.admin import BaseAdmin
from blowhorn.utils.filters import multiple_choice_list_filter
from blowhorn.driver.serializers import DriverLedgerSerializer
from blowhorn.driver.util import create_document_event
from blowhorn.document.constants import EXPIRED
from blowhorn.contract.permissions import is_finance_user
from django.utils.html import format_html
from blowhorn.driver.constants import UNAPPROVED_DEACTIVATION_PERIOD, NOTIFICATION_GROUP_PAYMENT_PAID
from blowhorn.driver.constants import PUSH, IN_APP, TEXT, IMAGE, NOTIFICATION_CHOICES, FIELD_FORM, WEB_FORM, \
    ANNOUNCEMENT_FORM, BUTTONS
from blowhorn.apps.driver_app.v3.helpers.driver import send_field_name_values
from .submodels.driver_submodels import DeliveryWorkerActivity

DATETIME_FORMAT = settings.ADMIN_DATETIME_FORMAT
ResourceAllocation = get_model('contract', 'ResourceAllocation')

# To disable default delete action
# from django.contrib.admin import site
# site.disable_action('delete_selected')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def export_driver_adjustment_data(modeladmin, request, queryset):

    if queryset:
        driver_adjustment_ids = queryset.values_list('id', flat=True)
        queryset = DriverPaymentAdjustment.copy_data.filter(id__in=driver_adjustment_ids)
    else:
        queryset = DriverPaymentAdjustment.copy_data.all()

    file_path = 'driver_payment_adjustment'
    queryset = queryset.order_by('-id')
    queryset.select_related(
        'driver', 'contract', 'payment', 'payment__driver',
        'payment__contract', 'payment__contract__customer'
    ).prefetch_related('payment__paymenttrip_set', 'driver__driverbalance'
    ).annotate(
        vehicle_class = StringAgg(
            'payment__paymenttrip__trip__vehicle__vehicle_model__vehicle_class__commercial_classification',
            delimiter=',',
            distinct=True),
        vehicle = StringAgg(
            'payment__paymenttrip__trip__vehicle__registration_certificate_number',
            delimiter=',',
            distinct=True),
        vehicle_model = StringAgg(
            'payment__paymenttrip__trip__vehicle__vehicle_model__model_name',
            delimiter=',',
            distinct=True)

    ).to_csv(
        file_path,
        'id', 'status', 'payment__driver__name', 'payment__contract__name', 'payment__contract__city__name',
        'payment__contract__customer__legalname', 'payment', 'vehicle_class', 'vehicle',
        'vehicle_model', 'adjustment_amount', 'adjustment_datetime', 'category', 'description'
    )

    modified_file = '%s_' % str(utc_to_ist(timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'ID', 'Status', 'Driver', 'Contract Name', 'City', 'Customer', 'Payment Reference',
            'Vehicle Class', 'Vehicle', 'Vehicle Model', 'Adjustment Amount', 'Adjustment Datetime',
            'Category', 'Description'
        ])

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1

        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(modified_file)
            os.remove(modified_file)
            return response


def close_payments(modeladmin, request, queryset):
    """ Close the payment records selected
    """
    ids = list(queryset.values_list('id', flat=True))
    updated_records = DriverPayment.objects.filter(id__in=ids, status=UNAPPROVED).update(
        status=CLOSED,
        modified_date=timezone.now(),
        modified_by_id=request.user)
    message = 'No. of closed payments: %s' % updated_records
    admin_messages.success(request, message)


def approve_payments(modeladmin, request, queryset):
    """
    Approve the payment records selected
    """
    ids = list(queryset.values_list('id', flat=True))
    uneligible_records = DriverPayment.objects.filter(id__in=ids).exclude(status=UNAPPROVED).exists()

    if uneligible_records:
        message = 'Choose Only UnApproved Records'
        admin_messages.error(request, message)
        return

    error_rec = []
    for rec in queryset:
        can_approve = can_approve_payment(rec)
        if not can_approve:
            error_rec.append(rec.id)
        if can_approve and not is_current_user_can_approve(rec):
            error_rec.append(rec.id)

    if error_rec:
        message = 'There are problem with these records, open individual record to see detailed error report %s' %(list(set(error_rec)))
        admin_messages.error(request, message)
        return


    with transaction.atomic():
        for rec in queryset:
            rec.approve()
            rec.status = APPROVED
            rec.save()

    message = 'Approved Sucessfully'
    admin_messages.success(request, message)

def open_payments(modeladmin, request, queryset):
    """ Open the payment records selected
    """
    ids = list(queryset.values_list('id', flat=True))
    updated_records = DriverPayment.objects.filter(id__in=ids, status=CLOSED).update(
        status=UNAPPROVED,
        modified_date=timezone.now(),
        modified_by_id=request.user)
    message = 'No. of opened payments: %s' % updated_records
    admin_messages.success(request, message)

def export_driver_payment_data(modeladmin, request, queryset):
    net_pay_gt_one = False
    if type(queryset[0]) != DriverPayment:
        net_pay_gt_one = True
    is_settled=None
    if type(queryset[0]) == DriverPaymentSettlementView:
        is_settled=False
    if type(queryset[0]) == DriverPaymentSettlementReversalView:
        is_settled=True
    query = DriverPaymentExportClass().get_query(request.GET.dict(),
                                                 request.user, net_pay_gt_one=net_pay_gt_one, is_settled=is_settled)

    if 'RDS_READ_REP_1' in os.environ:
        if query:
            queryset = DriverPayment.copy_data.using('readrep1').filter(query)
        else:
            queryset = DriverPayment.copy_data.using('readrep1').all()
    else:
        if query:
            queryset = DriverPayment.copy_data.filter(query)
        else:
            queryset = DriverPayment.copy_data.all()

    file_path = 'driver_payment'
    queryset.select_related(
        'driver', 'contract', 'buy_rate', 'sell_rate', 'batch'
    ).prefetch_related(Prefetch(
        'paymenttrip_set',
        queryset=PaymentTrip.objects.select_related(
            'trip__hub', 'trip__hub__customer',
            'trip__vehicle'
            'trip__vehicle__vehicle_model__vehicle_class',
            'trip__vehicle__vehicle_model'
        ))
    ).prefetch_related(
        'driver__driverbalance'
    ).annotate(
        formatted_start_datetime=Func(F('start_datetime') + timedelta(hours=5.5),
                            Value("DD/MM/YYYY"),
                            function='to_char',
                            output_field=CharField()),
        formatted_end_datetime=Func(F('end_datetime') + timedelta(hours=5.5),
                            Value("DD/MM/YYYY"),
                            function='to_char',
                            output_field=CharField()),
        vehicle_class=StringAgg(
            'paymenttrip__trip__vehicle__vehicle_model__vehicle_class__commercial_classification',
            delimiter=',',
            distinct=True),
        vehicle = StringAgg(
            'paymenttrip__trip__vehicle__registration_certificate_number',
            delimiter=',',
            distinct=True),
        vehicle_model=StringAgg(
            'paymenttrip__trip__vehicle__vehicle_model__model_name',
            delimiter=',',
            distinct=True),
        hub=StringAgg(
            'paymenttrip__trip__hub__name',
            delimiter=',',
            distinct=True),

        contingency=StringAgg(Case(
            When(paymenttrip__trip__is_contingency=True, then=Value('Yes')),
            default=Value('No'),
            output_field=CharField()
            ),
        delimiter=',',
        distinct=True),

        is_stale_modified=Case(
            When(is_stale=True, then=Value('Yes')),
            default=Value('No'),
            output_field=CharField()
        ),
        completed_trip_count=Count(Case(
            When(paymenttrip__trip__status=StatusPipeline.TRIP_COMPLETED, then=1),
            output_field=IntegerField(),
        )),
        driver_pan=F('driver__pan'),
        # tds_accepted=Case(
        #     When(Q(driver__drivertds__year=get_financial_year(timezone.now())) & Q(
        #         driver__drivertds__is_tds_accepted='True'),
        #          then=Value('Yes')),
        #     default=Value('No'),
        #     output_field=CharField()
        # ),
        # payment_settled=Case(
        #     When(Q(status=APPROVED) & Q(modified_date__lt=models.Subquery(
        #                 DriverLedger.objects.filter(
        #                     driver_id=models.OuterRef('driver_id'),
        #                     is_settlement=True).order_by(
        #                     '-transaction_time').values('transaction_time')[:1])
        #         ), then=Value('Yes')),
        #     When((Q(status=APPROVED) & Q(driver__driverbalance__amount__gt=0)) | ~Q(status=APPROVED),
        #          then=Value('No')),
        #     default=Value('Yes'),
        #     output_field=CharField())


    ).to_csv(
        file_path,
        'id', 'contract__name', 'contract__contract_type', 'status',
        'driver__name', 'driver_id', 'driver_pan', 'payment_type', 'vehicle_class',
        'vehicle', 'vehicle_model', 'hub',
        'contract__city__name', 'contract__customer__user__name',
        'contract__division__name', 'contingency',
        'formatted_start_datetime', 'formatted_end_datetime', 'completed_trip_count',
        'absent_days', 'leave_deduction', 'total_deductions', 'distance_payment',
        'time_payment', 'total_shipment_amount', 'adjustment', 'gross_pay', 'cash_paid',
        'net_pay', 'is_stale_modified', 'is_settled', 'batch', 'total_distance', 'total_time',
        'total_shipment_assigned', 'total_shipment_delivered', 'total_shipment_attempted',
        'total_shipment_rejected', 'total_package_returned', 'total_package_pickups',
        'total_package_delivered_cash', 'total_package_delivered_mpos', 'base_payment',
        'total_earnings', 'labour_cost', 'toll_charges', 'cash_collected'
    )

    modified_file = '%s_' % str(utc_to_ist(timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

    with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
        r = csv.reader(inFile)
        w = csv.writer(outfile)

        w.writerow([
            'ID', 'Contract Name', 'Contract Type', 'Status',
            'Driver', 'Driver Id', 'Driver PAN', 'Payment Type', 'Vehicle Class',
            'Vehicle', 'Vehicle Model', 'Hub', 'City', 'Customer', 'Division',
            'Has Contingency', 'Start Datetime', 'End Datetime', 'Completed Trip Count',
            'Absent Days', 'Leave Deduction', 'Total Deductions',
            'Distance Payment', 'Time Payment', 'Total Shipment Amount',
            'Adjustment', 'Gross Pay', 'Cash Paid', 'Net Pay', 'Stale',
            'Payment Settled', 'Batch', 'Total Distance', 'Total Time',
            'Total Shipment Assigned', 'Total Shipment Delivered', 'Total Shipment Attempted',
            'Total Shipment Rejected', 'Total Package Returned', 'Total Package Pickups',
            'Total Package Delivered Cash', 'Total Package Delivered MPOS', 'Base Payment',
            'Total Earnings', 'Labour Cost', 'Toll Charges', 'Cash Collected'


        ])

        i = 0
        for row in r:
            if i != 0:
                w.writerow(row)
            i = i + 1

        os.remove(file_path)
    if os.path.exists(modified_file):
        with open(modified_file, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(modified_file)
            os.remove(modified_file)
            return response


def apply_contract_permission_filter(user, qs):
    """ Common filter to apply for filtering out contracts
        based on access level
    """
    is_financeuser = is_finance_user(None, user)

    if not user.is_superuser and not is_financeuser:
        cities = City.objects.get_cities_for_given_user(user)
        contracts = Contract.objects.filter(
            Q(spocs=user) |
            Q(supervisors=user) |
            Q(city__in=cities)).distinct()
        if qs.model == DriverPayment:
            qs = qs.filter(contract__in=contracts)
        elif qs.model in [DriverPaymentAdjustment, DriverCashPaid]:
            qs = qs.filter(payment__contract__in=contracts)
        elif qs.model == Trip:
            qs = qs.filter(
                Q(customer_contract__isnull=True) |
                Q(customer_contract__in=contracts))
    return qs


class ExpiryDaysFilter(SimpleListFilter):
    """
        To filter the driver payments on the basis of expiry date
    """
    title = _('Expiry Days')
    parameter_name = 'end_datetime'

    def lookups(self, request, model_admin):
        return [
            ('0', 'expired'),
            ('1&5', '1 to 5'),
            ('6&10', '6 to 10'),
            ('11&15', '11 to 15'),
            ('16', 'greater than 15')
        ]

    def queryset(self, request, queryset):
        '''
            The filter excludes the approved, closed and withheld status
            and payments having end_date greater than today
        '''
        exclude_status = [APPROVED, CLOSED, WITHHELD]
        qs = queryset.exclude(status__in = exclude_status)

        if self.value():
            val = [int(x) for x in self.value().split('&')]

            if len(val) == 1:
                limit = date.today() + timedelta(days=val[0]) - timedelta(days=UNAPPROVED_DEACTIVATION_PERIOD)

                if val[0] <= 0:
                    return qs.filter(end_datetime__lte=limit)

                elif len(val) == 1 and val[0] > 0:
                    return qs.filter(Q(end_datetime__gte=limit) &
                                       Q(end_datetime__lte=date.today()))
            else:
                start = date.today() + timedelta(days=val[0]) - timedelta(days=UNAPPROVED_DEACTIVATION_PERIOD)
                end = date.today() + timedelta(days=val[1]) - timedelta(days=UNAPPROVED_DEACTIVATION_PERIOD)
                return qs.filter(Q(end_datetime__gte=start) &
                                 Q(end_datetime__lte=end))


class DriverResponseFieldsInlineForm(forms.ModelForm):

    def clean(self):

        field_type = self.cleaned_data.get('field_type')
        notification = self.cleaned_data.get('notification_campaign')

        if field_type and not notification.form_type:
            raise ValidationError(
                {'field_type': 'Select IN APP Form Type'}
            )

        if notification.notification_type == IN_APP:

            if notification.form_type == FIELD_FORM and not field_type:
                raise ValidationError(
                    {'field_type': 'Select Field Type'}
                )
            if notification.form_type == WEB_FORM and field_type != BUTTONS:
                raise ValidationError(
                    {'field_type': 'Buttons mandatory for Web Form Type'}
                )
            if notification.form_type == ANNOUNCEMENT_FORM and field_type:
                raise ValidationError(
                    {'field_type': 'Do not select any field type for Announcement Form type.'}
                )
            if not notification.form_type:
                raise ValidationError(
                    {'field_type': 'Do not select any field type for Announcement Form type.'}
                )

    class Meta:
        model = DriverResponseFields
        fields = '__all__'


class NotificationCampaignForm(forms.ModelForm):
    notification_content = forms.CharField(required=False, widget=CKEditorWidget())
    notification_type = forms.ChoiceField(required=True, choices=NOTIFICATION_CHOICES)

    def clean(self):
        super(NotificationCampaignForm, self).clean()
        start_time = self.cleaned_data.get('start_time', None)
        end_time = self.cleaned_data.get('end_time', None)
        notification_type = self.cleaned_data.get('notification_type', None)
        notification_interval = self.cleaned_data.get('notification_interval', None)
        form_type = self.cleaned_data.get('form_type', None)
        url = self.cleaned_data.get('url', None)
        mandatory = self.cleaned_data.get('mandatory', None)
        content = self.cleaned_data.get('content', None)

        if notification_type == IN_APP:

            if (form_type == ANNOUNCEMENT_FORM) and mandatory:
                raise ValidationError(
                    {'mandatory': 'Announcement Form Type cannot be mandatory'}
                )

            if form_type == WEB_FORM and not url:
                raise ValidationError(
                    {'url': 'URL required for Announcement Form/Web Form type'}
                )

            if form_type == FIELD_FORM and url:
                raise ValidationError(
                    {'url': 'URL not required for Field Form Type'}
                )
            if not form_type:
                raise ValidationError(
                    {'form_type': 'Form Type required for In App notification'}
                )

            if not notification_interval:
                raise ValidationError(
                    {'notification_interval': 'Notification Interval mandatory in In App type notification.'}
                )
        if not content:
            raise ValidationError({'content': 'Content is mandatory'})
        if not start_time:
            raise ValidationError({'start_time': 'Start time is mandatory'})
        if not end_time:
            raise ValidationError({'end_time': 'End time is mandatory'})

    class Meta:
        model = NotificationCampaign
        fields = '__all__'


class DriverNotificationCriteriaInline(admin.TabularInline):

    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    driver_ids = forms.CharField(widget=CKEditorWidget())
    model = DriverNotificationCriteria
    extra = 1
    max_num = 1


class NotificationContentLanguageAdmin(admin.ModelAdmin):
    model = NotificationContentLanguage

    list_display = ('title_en',)
    list_filter = ('title_en',)

    fieldsets = (

        ('English', {
            'fields': ('title_en', 'short_description_en', 'description_en', 'img_english')
        }),
        ('Kannada', {
            'fields': ('title_kn', 'short_description_kn', 'description_kn', 'img_kannada')
        }),
        ('Marathi', {
            'fields': ('title_mr', 'short_description_mr', 'description_mr', 'img_marathi')
        }),
        ('Telugu', {
            'fields': ('title_te', 'short_description_te', 'description_te', 'img_telugu')
        }),
        ('Tamil', {
            'fields': ('title_ta', 'short_description_ta', 'description_ta', 'img_tamil')
        }),
        ('Hindi', {
            'fields': ('title_hi', 'short_description_hi', 'description_hi', 'img_hindi')
        }),
    )


class NotificationFormLanguageAdmin(admin.ModelAdmin):
    model = NotificationFormLanguage

    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    field_values_en = forms.CharField(widget=CKEditorWidget())
    field_values_kn = forms.CharField(widget=CKEditorWidget())
    field_values_mr = forms.CharField(widget=CKEditorWidget())
    field_values_te = forms.CharField(widget=CKEditorWidget())
    field_values_ta = forms.CharField(widget=CKEditorWidget())
    field_values_hi = forms.CharField(widget=CKEditorWidget())

    list_display = ('question_title',)
    list_filter = ('question_title',)

    fieldsets = (
        ('General', {
            'fields': ('question_title',)
        }),
        ('English', {
            'fields': ('question_en', 'field_values_en')
        }),
        ('Kannada', {
            'fields': ('question_kn', 'field_values_kn')
        }),
        ('Marathi', {
            'fields': ('question_mr', 'field_values_mr')
        }),
        ('Telugu', {
            'fields': ('question_te', 'field_values_te')
        }),
        ('Tamil', {
            'fields': ('question_ta', 'field_values_ta')
        }),
        ('Hindi', {
            'fields': ('question_hi', 'field_values_hi')
        }),
    )


class DriverResponseFieldsInline(admin.TabularInline):
    model = DriverResponseFields
    form = DriverResponseFieldsInlineForm
    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
    }
    field_values = forms.CharField(widget=CKEditorWidget())

    fields = ('notification_form', 'field_type')

    verbose_name = "Driver Response Form"
    verbose_name_plural = "Driver Response Form"

    extra = 0


class DriverNotificationAdmin(admin.ModelAdmin):

    model = NotificationCampaign
    formfield_overrides = {
        ArrayField: {'widget': widgets.AdminTagWidget},
        models.ImageField: {
            "widget": ImagePreviewWidget
        }
    }
    form = NotificationCampaignForm

    list_display = ('title', 'notification_type', 'form_type',
                    'notification_interval', 'is_active')
    search_fields = ['title', 'form_type',
                     'notification_interval', 'is_active']
    fieldsets = (
        ('General', {
            'fields': ('title', 'notification_type', 'content', 'start_time', 'end_time', 'is_active',
                       'url', 'mandatory')
        }),

        ('In App Notification', {
            'fields': ('form_type', 'notification_interval')
        }),
    )
    list_filter = ('notification_type', 'notification_interval', 'is_active', 'form_type')
    inlines = [DriverNotificationCriteriaInline, DriverResponseFieldsInline]


class DriverResponseAdminInline(admin.TabularInline):

    model = DriverResponse
    extra = 0
    readonly_fields = [
        'driver_notification_response', 'notification', 'form_field', 'field_response', 'get_question'
    ]

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('Form Responses', {
                'fields': (
                    ('get_question', 'field_response'),
                )
            }),
        )

        return fieldsets

    def get_question(self, obj):
        if obj.driver_notification_response:
            language = obj.driver_notification_response.driver.last_used_locale
            form_language = obj.form_field.notification_form
            question = send_field_name_values(language, form_language)
            return question[0]
        return None

    get_question.short_description = 'Question'

    can_delete = False

    def has_add_permission(self, request, obj):
        return False


class DriverNotificationResponseAdmin(admin.ModelAdmin):

    model = DriverNotificationResponse
    list_display = ('driver', 'notification_campaign',
                    'last_response_time', 'phone_number')
    search_fields = ['driver__name', 'notification_campaign__title',
                     'driver__vehicles__registration_certificate_number']
    readonly_fields = ('driver', 'notification_campaign', 'last_response_time')
    fieldsets = (
        ('General', {
            'fields': ('driver', 'notification_campaign', 'last_response_time')
        }),
    )
    inlines = [DriverResponseAdminInline]
    actions = ['export_responses']
    list_filter = ['notification_campaign', ('last_response_time', DateRangeFilter)]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def phone_number(self, obj):
        return obj.driver.user.phone_number

    def export_responses(self, request, queryset):
        ids = queryset.values_list('id', flat=True)
        queryset = DriverNotificationResponse.copy_data.filter(id__in=ids)

        file_path = 'driver_notification_responses'

        queryset.annotate(
            driver_contact=F('driver__user__phone_number'),
            driver_name=F('driver__name'),
            notification_campaign_name=F('notification_campaign__title')
        ).to_csv(
            file_path,
            'driver', 'driver_name', 'notification_campaign_name', 'last_response_time', 'form_type',
            'driver_contact'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow(['Driver ID', 'Driver Name', 'Notification Campaign', 'Form Type'
                        'Last Response Time', 'Driver Phone Number'
                        ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response
    export_responses.short_description = 'Export Notification Responses'


class PaymentContractFilter(ContractFilter):

    """
    To filter out only the selected Contract Payments
    """

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(contract=self.value())


class AdjustmentCashPaidContractFilter(ContractFilter):

    """
    To filter out only the selected contract Payment Adjustment
    """

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(payment__contract=self.value())


class AdjustmentDriverFilter(SimpleListFilter):

    """
    To filter out only the active Drivers
    """

    title = _('driver')
    parameter_name = 'payment__driver'

    def lookups(self, request, model_admin):
        return [(d.id, d) for d in Driver.objects.filter(
            status=StatusPipeline.ACTIVE)]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(payment__driver__id=str(self.value()))


class StateFilter(SimpleListFilter):
    title = _('state')
    parameter_name = 'contract__city__state'

    def lookups(self, request, model_admin):
        return [(state.id, state) for state in State.objects.all()]

    def queryset(self, request, queryset):
        if self.value():
            if 'contract__city' in request.GET:
                state = request.GET['contract__city__state']
                city = request.GET['contract__city']
                current_cities = state_city_cache(UPDATE_FLAG).get(state)
                valid_state = False
                if current_cities and int(city) in current_cities:
                    valid_state = True
                if valid_state:
                    return queryset.filter(contract__city_id=city, contract__city__state__id=state)
                else:
                    qs = DriverPayment.objects.filter(contract__city__state__id=str(self.value()))
                    return qs
            return queryset.filter(contract__city__state__id=str(self.value()))


class CityFilter(SimpleListFilter):
    title = _('city')
    parameter_name = 'contract__city'

    def lookups(self, request, model_admin):
        if 'contract__city__state' in request.GET:
            state = request.GET['contract__city__state']
            cities = City.objects.filter(state=state)
        else:
            cities = City.objects.all()
        return [(city.id, city) for city in cities]

    def queryset(self, request, queryset):
        if self.value():
            if 'contract__city__state' in request.GET:
                state = request.GET['contract__city__state']
                city = request.GET['contract__city']
                current_cities = state_city_cache(UPDATE_FLAG).get(state)
                valid_state = False
                if city in current_cities:
                    valid_state = True
                if valid_state:
                    return queryset.filter(contract__city_id=str(self.value()),
                                           contract__city__state__id=state)

            return queryset.filter(contract__city_id=str(self.value()))


class HubFilter(SimpleListFilter):

    """
    To filter out the hub for the payments
    """

    title = _('hub')
    parameter_name = 'hub'
    related_filter_parameter = 'payment__paymenttrip__trip__hub'

    def lookups(self, request, model_admin):
        return [(hub.id, hub) for hub in Hub.objects.all()]

    def queryset(self, request, queryset):
        if self.value():
            try:
                return queryset.filter(
                    paymenttrip__trip__hub_id=self.value()).distinct('id')
            except:
                return queryset.filter(
                    payment__paymenttrip__trip__hub_id=
                        self.value())


class DocumentsInlineFormset(BaseInlineFormSet):
    # valiations on driver documents

    def clean(self):
        super(DocumentsInlineFormset, self).clean()
        driver_status = self.instance.status
        list_added_documents = []
        list_mandatory_document = MANDATORY_DOCUMENT_TYPES
        for form in self.forms:
            data = form.cleaned_data
            if not data.get("id"):
                continue
            is_delete = data.get('DELETE')
            document_status = data.get('status') or form.instance.status
            inst_dtype = data.get('document_type')
            list_added_documents.append('%s' % inst_dtype)

            if document_status in [StatusPipeline.ACTIVE, StatusPipeline.INACTIVE, EXPIRED] and form.changed_data:
                raise ValidationError('You cannot edit Active, Inactive or Expired Document')
            # Dont alllow to delete of active drivers
            if driver_status == StatusPipeline.ACTIVE or \
                    driver_status == StatusPipeline.ONBOARDING:
                if document_status == StatusPipeline.ACTIVE and is_delete:
                    raise ValidationError(
                        messages.CANNOT_DELETE_ACTIVE_DRIVER_DOC)

            # if driver_status == StatusPipeline.ACTIVE:
            #     if document_status == VERIFICATION_PENDING:
            #         raise ValidationError(
            #             messages.DOCUMENTS_ARE_MANDATORY_TO_SET_DRIVER_ACTIVE)

            # if driver_status == StatusPipeline.ONBOARDING and \
            #         document_status != StatusPipeline.ACTIVE:
            #     raise ValidationError(messages.ALL_DOCUMENT_SHOULD_BE_ACTIVE)

        if not self.forms and driver_status == StatusPipeline.ACTIVE:
            raise ValidationError(
                messages.DOCUMENTS_ARE_MANDATORY_TO_SET_DRIVER_ACTIVE)

        # missing_docs = set(list_mandatory_document).difference(
        #     set(list_added_documents))
        # display_msg = messages.ALL_MANDATORY_DOCUMENT_SHOULD_BE_ADDED + \
        #               ' - ' + ', '.join(missing_docs)
        # print(set(list_added_documents) > set(list_mandatory_document))
        # if driver_status == StatusPipeline.ACTIVE and \
        #     not set(list_added_documents) >= set(list_mandatory_document):
        #     raise ValidationError(display_msg)


class DocumentsInline(admin.TabularInline):
    formfield_overrides = {
        models.DateField: {
            "widget": DateTimePicker(options={"format": "DD-MM-YYYY"})
        },
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    model = DriverDocument
    fields = ('id', 'get_file', 'document_type', 'number', 'state', 'status',
              'bgv_status', 'expiry_date', 'created_by', 'modified_by', 'get_action_owner')
    readonly_fields = fields
    formset = DocumentsInlineFormset
    extra = 0

    def get_file(self, obj):
        if obj.file:
            url = obj.file.url
            return format_html('<a href="%s" target="_blank"><img src="%s" height="100" width="100"/></a>' % (url, url))
        return None

    get_file.short_description = 'File'

    def get_action_owner(self, obj):
        action_owner = [str(x.action_owner) for x in obj.documentevent_set.all() if x.action_owner is not None]
        return action_owner

    get_action_owner.short_description = 'Action Owner'

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(DocumentsInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'document_type':
            field.queryset = field.queryset.filter(identifier=DRIVER_DOCUMENT)

        return field

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        qs = qs.select_related('created_by', 'modified_by')
        qs = qs.prefetch_related('documentevent_set', 'documentevent_set__action_owner')
        return qs


    class Media:
        js = ('/static/admin/js/driver_document_inline.js',)


class DriverVehicleInlineFormSet(BaseInlineFormSet):
    # valiations on driver vehicle mapping

    def clean(self):
        super(DriverVehicleInlineFormSet, self).clean()
        driver_status = self.instance.status
        mark_onboarding = True
        # when no form is there
        # if not self.forms and driver_status == StatusPipeline.ACTIVE:
        #     raise ValidationError(
        #         messages.VEHICLE_IS_MANDATORY_FOR_ACTIVE_DRIVER)

        for form in self.forms:
            data = form.cleaned_data
            is_delete = data.get('DELETE')
            vehicle = data.get('vehicle')
            # if driver_status == StatusPipeline.ACTIVE and not vehicle:
            #     raise ValidationError(
            #         messages.VEHICLE_IS_MANDATORY_FOR_ACTIVE_DRIVER)

            if not is_delete:
                # dont mark him onboarding if atleast one vehicle mapping exists
                mark_onboarding = False
        if self.instance.status == StatusPipeline.ACTIVE and mark_onboarding:
            self.instance.status = StatusPipeline.ONBOARDING


class DriverVehicleInline(admin.TabularInline):
    model = DriverVehicleMap
    # At the moment there is a restriction that only one vehicle can be
    # associated with a driver. And one vehicle association is mandated
    # if the driver needs to be made active
    max_num = 1
    extra = 1
    formset = DriverVehicleInlineFormSet
    can_delete = False

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]



class DriverHistoryInline(admin.TabularInline):
    model = DriverHistory
    readonly_fields = ['field', 'old_value',
                       'new_value', 'user', 'modified_time', 'reason']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DriverPaymentContingencyFilter(admin.SimpleListFilter):
    title = _('has contingency')
    parameter_name = 'contingency'

    def lookups(self, request, model_admin):
        return [('True', 'True'), ('False', 'False'), ]

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(paymenttrip__trip__is_contingency=True)
        elif self.value() == 'False':
            return queryset.exclude(paymenttrip__trip__is_contingency=True)
        return queryset


class DriverPreferredLocationAdminInline(admin.TabularInline):
    model = DriverPreferredLocation
    extra = 4
    max_num = 4
    min_num = 1

    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldInlineWidget}
    }


class DriverAssetInline(admin.TabularInline):
    model = Asset
    extra = 0
    form = AssetForm
    formset = AssetInlineFormset


def send_driver_app_link(modeladmin, request, queryset):
    """
    Custom Action Button to send driver app link to driver as a sms
    """
    from blowhorn.common import sms_templates
    from blowhorn.common.tasks import send_sms
    template_dict = sms_templates.SHARE_APP_MESSAGE_DRIVER_APP
    message = template_dict['text']

    phone_numbers = list(queryset.values_list('user__phone_number', flat=True))
    template_json = json.dumps(template_dict)
    for phone_number in phone_numbers:
        send_sms.apply_async(args=[phone_number, message, template_json])
    # send_message_to_drivers.delay(phone_numbers, message, template_json)


# make the changes in return_to_hub_confirmation.js
# if making changes in short description here
send_driver_app_link.short_description = _('Send Driver App Link to Driver')


def capture_driver_wellbeing_details(modeladmin, request, queryset):
    """
    Customer action to bulk update cpature_wellbeing_status flag of drivers
    """
    queryset.update(capture_wellbeing_status=True)
    admin_messages.success(request, 'Updated capture well being flag for the drivers')


class DriverTDSInline(admin.TabularInline):
    model = DriverTds
    # max_num = 1
    extra = 0

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    # formset = DriverVehicleInlineFormSet


class DriverWellBeingInline(admin.TabularInline):
    model = DriverWellBeing
    extra = 0
    # formset = DriverWellBeingFormset

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_delete_permission(self, request, obj=None):
        return False


class DriverAdmin(BaseAdmin):
    # paginator = LargeTablePaginator
    show_full_result_count = False

    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }
    actions = [send_driver_app_link, capture_driver_wellbeing_details, 'export_driver_data']
    list_display_links = ('name',)
    list_display = ('name', 'get_driver_vehicle', 'is_marketplace_driver',
                    'get_phone_number', 'operating_city', 'status', 'contracts', 'created_by',
                    'created_date', 'source', 'id_card', 'is_blowhorn_driver', 'get_tos_accepted','is_defcom_driver')

    inlines = [DriverVehicleInline, DocumentsInline,
               DriverPreferredLocationAdminInline,
               DriverAssetInline, DriverHistoryInline, DriverTDSInline, DriverWellBeingInline]

    list_filter = ['status', 'operating_city', 'is_marketplace_driver', 'capture_wellbeing_status']
    list_select_related = (
        'operating_city', 'created_by'
    )

    fieldsets = (
        ('General', {
            'fields': (
                'name', 'get_phone_number', 'get_vehicle_model', 'status', 'reason_for_inactive',
                'description', 'driving_license_number', 'password', 'pan', 'capture_wellbeing_status',
                'is_blowhorn_driver', 'is_marketplace_driver', 'contract',
                'operating_city', 'get_tos_accepted', 'date_of_joining', 'reason_for_change',
                'load_preference', 'supply_source','is_defcom_driver', 'cod_balance'
            )
        }),
        # ('Address', {
        #     'fields': (
        #         'current_address', 'is_address_same', 'permanent_address',
        #         'emergency_contact1', 'emergency_contact2',
        #         'reference_contact1', 'reference_contact2'
        #     )
        # }),
        ('Personal Info', {
            'fields': (
                'photo', 'date_of_birth', 'father_name', 'partner',
                'educational_qualifications', 'marital_status', 'own_vehicle', 'bank_account',
                ('owner_details', 'get_owner_bank_details'),
                'balance', 'referred_by', 'working_preferences',
                'prior_monthly_income', 'can_drive_and_deliver',
                'can_read_english', 'can_source_additional_labour',
                'work_status'
            )
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    search_fields = ['user__phone_number', 'name',
                     'vehicles__registration_certificate_number',
                     'resourceallocation__contract__name']

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        qs = qs.select_related('operating_city', 'user',
                               'created_by', 'modified_by')
        qs = qs.prefetch_related('vehicles')
        qs = qs.prefetch_related('resourceallocation_set__contract')
        qs = qs.prefetch_related('driver_document', 'driver_document__documentevent_set')
        qs = qs.prefetch_related('resourceallocation_set__contract__division')
        qs = qs.prefetch_related('asset_set')
        return qs

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        # Added refrences here beacuse it will impact admin queryset
        # count/timings
        qs = qs.select_related('operating_city', 'user', 'operating_area',
                               'permanent_address', 'current_address',
                               'emergency_contact1', 'emergency_contact2',
                               'bank_account', 'driver_rating',
                               'reference_contact1', 'reference_contact2',
                               'permanent_address__country',
                               'current_address__country', 'created_by',
                               'modified_by')
        qs = qs.prefetch_related('vehicles__vehicle_model__vehicle_class')
        qs = qs.prefetch_related('resourceallocation_set__hub')
        qs = qs.prefetch_related('resourceallocation_set__contract__division')
        return list(qs)

    def get_readonly_fields(self, request, obj=None):
        self.readonly_fields = ('created_date', 'created_by', 'modified_date', 'get_tos_accepted',
                                'modified_by', 'date_of_joining', 'balance', 'get_vehicle_model',
                                'get_owner_bank_details', 'reason_for_inactive', 'description',
                                'pan', 'driving_license_number', 'get_phone_number',
                                'date_of_birth', 'father_name',
                                'educational_qualifications', 'marital_status', 'own_vehicle',
                                'owner_details', 'get_owner_bank_details',
                                'balance', 'referred_by', 'working_preferences',
                                'prior_monthly_income', 'can_drive_and_deliver',
                                'can_read_english', 'can_source_additional_labour',
                                'work_status', 'operating_city', 'supply_source', 'cod_balance')

        if obj:
            # if not obj.status == StatusPipeline.SUPPLY:
            #     self.readonly_fields += ('partner',)
            self.readonly_fields += ('name',)
            if obj.get_bank_account_details() and not (request.user.is_city_manager or
                                                       request.user.is_superuser):
                self.readonly_fields += ('bank_account',)
        return self.readonly_fields

    def get_vehicle_model(self, obj):
        vehicles = obj.vehicles.all()
        if vehicles:
            return vehicles[0].vehicle_model
        return None

    get_vehicle_model.short_description = 'Vehicle Model'

    def get_owner_bank_details(self, obj):
        if not obj.own_vehicle:
            return obj.owner_details and obj.owner_details.bank_account

    get_owner_bank_details.short_description = 'Owner Bank Details'

    def get_tos_accepted(self, obj):
        return obj.tos_accepted

    get_tos_accepted.short_description = 'Tos accepted'
    get_tos_accepted.boolean = True

    def get_driver_vehicle(self, obj):
        """ Return the first vehicle from the mapped list along with
            change link """
        vehicles = obj.vehicles.all()
        if vehicles:
            vehicle = vehicles[0]
            url = reverse(
                'admin:vehicle_vehicle_change', args=[vehicle.id])
            return format_html(
                '<a target="_blank" href="{}">{}</a>', url, vehicle)
        return obj.driver_vehicle

    def get_phone_number(self, obj):
        return obj.user.phone_number

    get_phone_number.short_description = 'Mobile Number'

    def id_card(self, obj):
        """
            Generating driver id card
        """
        if obj:
            return format_html("""
            <a title='Click here to generate id card'
            class="btn btn-info btn-sm"
            href="/api/driver/%s/idcard"
            target="_blank">
            ID Card</a>
            """ % obj.pk)

        return None

    id_card.allow_tags = True

    get_driver_vehicle.admin_order_field = 'driver_vehicle'
    get_driver_vehicle.short_description = 'Vehicle'

    def contracts(self, obj):
        contracts = [x.contract for x in obj.resourceallocation_set.all()]
        return ', '.join(map(str, contracts))

    def date_of_joining(self, obj):
        local_tz = pytz.timezone(settings.IST_TIME_ZONE)
        if obj.status in [StatusPipeline.ACTIVE, StatusPipeline.INACTIVE] \
                and obj.active_from:
            return obj.active_from.astimezone(
                local_tz).strftime('%d-%m-%Y %H:%M')
        return None

    date_of_joining.short_description = 'Date of Joining'

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if obj:
            self.form = DriverEditForm
        else:
            self.form = DriverForm
        return super(DriverAdmin, self).get_form(request, obj, **kwargs)

    def set_active_inactive(self, status, obj):
        # If driver is set as active
        # set active date to current day
        if status == StatusPipeline.ACTIVE:
            obj.active_from = timezone.now()
            # In case of reactive of driver set leaving date as none
            if obj.date_of_leaving:
                obj.date_of_leaving = None
                obj.reason_for_inactive = None
                obj.description = None

        # Set all the attributes related to inactive as none
        # except the date of leaving, set it as current day
        elif status == StatusPipeline.INACTIVE:
            obj.date_of_leaving = timezone.now()

        return obj

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'photo':
            request = kwargs.pop("request", None)
            kwargs['widget'] = ImagePreviewWidget
            return db_field.formfield(**kwargs)
        return super(DriverAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)

    def save_formset(self, request, form, formset, change):
        editable_fields = ['file', 'document_type', 'number', 'state', 'status',
                           'bgv_status', 'expiry_date', ]
        for form in formset.forms:
            if isinstance(form.instance, DriverDocument):
                if any(x in editable_fields for x in form.changed_data):
                    if form.instance and form.instance.id:
                        form.instance.status = VERIFICATION_PENDING
                        create_document_event(driver_document=form.instance, driver=form.instance.driver, created_by=request.user)
        formset.save()

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            status = data.get('status')
            reason_for_change = data.get('reason_for_change', None)

            driver_list = []
            current_user = request.user

            if change:
                existing_driver = Driver.objects.filter(pk=obj.id).first()
                if existing_driver.status != status:
                    obj = self.set_active_inactive(status, obj)
                name = obj.user.name

                old = Driver.objects.get(pk=obj.id)
                for field in old._meta.fields:
                    field_name = field.__dict__.get('name')

                    if field_name in DRIVER_ALLOWED_FIELDS_FOR_EDIT:
                        old_value = field.value_from_object(old)
                        current_value = field.value_from_object(obj)

                        if field_name == 'bank_account' and reason_for_change:
                            if obj.own_vehicle and DriverPayment.objects.filter(driver=obj, status=APPROVED,
                                                                                is_settled=False).exists():
                                self.message_user(
                                    request,
                                    "Driver has unsettled payments, settle the payments to change the bank account",
                                    level=admin_messages.ERROR)
                                return
                            old_value = str(getattr(old, field_name))
                            new_value = str(getattr(obj, field_name))

                            driver_list.append(DriverHistory(
                                field=field_name,
                                old_value=old_value,
                                new_value=new_value,
                                user=current_user, driver=old,
                                reason=reason_for_change))

                            if new_value and new_value != 'None':
                                bank_account = BankAccount.objects.get(pk=obj.bank_account_id)
                                action = 'Mapped-driver'
                                bank_account.add_bank_account_history(bank_account, request.user, action, old)
                            if old_value:
                                bank_account = BankAccount.objects.get(pk=old.bank_account_id)
                                action = 'Unmapped-driver'
                                bank_account.add_bank_account_history(bank_account, request.user, action, old)

                        elif old_value != current_value and \
                                (old_value or current_value):
                            old_value = str(getattr(old, field_name))
                            new_value = str(getattr(obj, field_name))

                            if field_name == 'owner_details':
                                old_value = "%s | %s" % (
                                    old.owner_details.name,
                                    old.owner_details.bank_account) \
                                    if old.owner_details else None
                                new_value = "%s | %s" % (
                                    obj.owner_details.name,
                                    obj.owner_details.bank_account) \
                                    if obj.owner_details else None

                            driver_list.append(DriverHistory(
                                field=field_name,
                                old_value=old_value,
                                new_value=new_value,
                                user=current_user,
                                driver=old))

            else:
                existing_driver = None
                obj.driver_rating = Driver.objects.upsert_driver_rating(
                    num_of_feedbacks=0,
                    cumulative_rating=0,
                )
                name = data.get('name')

                obj = self.set_active_inactive(status, obj)

            # obj.user = Driver.objects.upsert_user(
            #     email=data.get('email'),
            #     name=name,
            #     phone_number=data.get('phone_number'),
            #     password=data.get('password', None),
            #     user_id=existing_driver.user.id if existing_driver else None,
            #     is_active=True if status == StatusPipeline.ACTIVE else False
            # )

            try:
                DriverBackGroundVerificationMixin().get_data_for_background_verification(obj)
                data_verification_ready = True
            except Exception as e:
                data_verification_ready = False

            if obj.is_address_same:
                obj.permanent_address = obj.current_address

            if driver_list:
                DriverHistory.objects.bulk_create(driver_list)

            obj.name = name
            obj.data_ready_for_bgv = data_verification_ready
            obj.save()
            super().save_model(request, obj, form, change)

    def get_phone_number(self, obj):
        if obj.user:
            return obj.user.phone_number
        return None

    '''
    Below set of methods are used to save model field
    after inline fields are saved
        1.response_add
        2.response_change
        3.after_saving_model_and_related_inlines
    '''

    def response_add(self, request, new_object):
        obj = self.after_saving_model_and_related_inlines(new_object)
        return super(DriverAdmin, self).response_add(request, obj)

    def response_change(self, request, obj):
        obj = self.after_saving_model_and_related_inlines(obj)
        return super(DriverAdmin, self).response_change(request, obj)

    def after_saving_model_and_related_inlines(self, obj):
        driver_documents = DriverDocument.objects.filter(driver=obj.pk, status=StatusPipeline.ACTIVE)
        active_doc = 0
        # If all the driver documents are verified(active)
        # then update the driver status to 'Onboarding'
        if driver_documents:
            for document in driver_documents:
                if document.status == StatusPipeline.ACTIVE:
                    active_doc += 1

            if len(driver_documents) >= 1:
                if obj.status not in [StatusPipeline.ACTIVE,
                                      StatusPipeline.INACTIVE]:
                    obj.status = StatusPipeline.ONBOARDING
                    obj.save()

        return obj

    get_phone_number.short_description = 'Driver Phone'

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        try:
            return super().change_view(
                request, object_id, extra_context=extra_context)

        except BaseException as err:
            admin_messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def add_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        try:
            return super().add_view(request, extra_context=extra_context)
        except BaseException as err:
            messages.error(request, err)
            return HttpResponseRedirect(request.path)

    def export_driver_data(self, request, queryset):
        ids = queryset.values_list('id', flat=True)
        queryset = Driver.copy_data.filter(id__in=ids)

        file_path = 'driver_details'

        queryset.annotate(
            get_vehicles=ArrayAgg('vehicles__registration_certificate_number',
                                  delimiter=',', distinct=True),
            get_contracts=ArrayAgg('contract')
        ).to_csv(
            file_path,
            'name', 'get_vehicles', 'is_marketplace_driver',
            'user__phone_number', 'operating_city__name', 'status', 'get_contracts', 'created_by',
            'created_date', 'source', 'is_blowhorn_driver', 'tos_accepted'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow(['Driver', 'Vehicles', 'Is Marketplace Driver', 'Phone Number', 'Operating City',
                        'Status', 'Contracts', 'Created By', 'Created Date', 'Source', 'Is Blowhorn Driver',
                        'Tos Accepted'
                        ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response

    export_driver_data.short_description = 'Export Driver details in excel'

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',
                    '/static/admin/css/drivers.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/driver_admin.js',
              '/static/admin/js/driver_bank_details_change_reason.js',
              '/static/website/js/lib/jquery.qrcode.min.js',
              'website/js/lib/dom-to-img.js',
              )


class DriverInactiveReasonAdmin(admin.ModelAdmin):
    list_display = ('reason', 'is_active')


class DriverDocumentPageAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }

    list_display = ('driver_document', 'page_number', 'file', 'uploaded_at')

    fieldsets = (
        ('Document', {
            'fields': ('id', 'file', 'driver_document', 'page_number', 'uploaded_at', 'get_doc_id')
        }),
    )

    readonly_fields = ['id', 'file', 'driver_document', 'page_number', 'uploaded_at', 'get_doc_id']

    def has_delete_permission(self, request, obj=None):
        return False

    def get_doc_id(self, obj):
        return obj.driver_document_id


class DriverDocumentAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    list_display = ('id', 'get_title', 'file', 'document_type',
                    'date_uploaded', 'created_by', 'modified_by', 'status')
    list_filter = ['status']

    list_select_related = (
        'document_type',
    )

    search_fields = [
        'id', 'driver__name', 'document_type__name', 'date_uploaded', 'status'
    ]

    fieldsets = (
        ('Document', {
            'fields': ('id', 'file', 'document_type', 'number', 'state',
                       'status', 'bgv_status', 'expiry_date',
                       'non_transport_validity', 'transport_validity',
                       'issue_date', 'driver')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    readonly_fields = ['id', 'created_date', 'created_by', 'modified_date',
                       'modified_by', 'bgv_status']

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(DriverDocumentAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'document_type':
            field.queryset = field.queryset.filter(identifier=DRIVER_DOCUMENT)

        return field

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False


class LoadPreferenceAdmin(admin.ModelAdmin):

    list_display = ('name', 'level', 'created_by', 'modified_by')

    fields = ('name', 'level')


class DriverConstantsAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'description')


class DeviceDetailsDriverFilter(SimpleListFilter):

    """
    To filter out only the active Drivers
    """

    title = _('driver')
    parameter_name = 'payment__driver'

    def lookups(self, request, model_admin):
        return [(d.id, d) for d in Driver.objects.filter(
            status=StatusPipeline.ACTIVE)]

    def queryset(self, request, queryset):
        if self.value():
            return DeviceDetails.objects.filter(
                pk=Driver.objects.get(pk=self.value()).device_details.id)


class DriverDeviceDetailsAdmin(admin.ModelAdmin):
    list_display = ('driver', 'app_name',
                    'app_version_name', 'app_version_code')
    list_filter = (DeviceDetailsDriverFilter, 'app_name', 'app_version_name')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver',)
        return qs

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    def driver(self, obj):
        return Driver.objects.filter(device_details=obj).first()

    driver.short_description = 'Driver'

    ''' change_view and add_view are used here
        to disable save and continue button
    '''

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        # extra_context['show_save_and_add_another'] = False
        return super().change_view(
            request, object_id,
            extra_context=extra_context
        )

    class Meta:
        model = DeviceDetails
        fields = '__all__'


class DriverAppErrorsAdmin(admin.ModelAdmin):

    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'line1'},
        'GooglePointFieldWidget'
    )
    formfield_overrides = {
        models.PointField:
            {"widget": GooglePointFieldWidget(
                settings=PointFieldWidgetSettings)},
    }

    list_display = ('url', 'driver', 'created_date', 'trip', 'order', 'stop', )
    list_filter = ('driver', ('created_date', DateFilter),)
    search_fields = ['url', 'trip', 'order', 'stop', ]
    readonly_fields = ('url', 'traceback', 'driver',
                       'created_date', 'trip', 'order', 'stop', )

    fieldsets = (
        (None, {
            'fields': ('url', 'driver', 'trip', 'order', 'stop', 'traceback',)
        }),
        ('Location', {
            'fields': ('current_location', )
        }),
    )

    actions = None

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('trip', 'order', 'stop', 'driver')
        return qs

    # Disable the Stops Add button on admin
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)


class DriverAddressAdmin(admin.ModelAdmin):
    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'line1'},
        'GooglePointFieldWidget'
    )
    formfield_overrides = {
        models.PointField:
            {"widget": GooglePointFieldWidget(
                settings=PointFieldWidgetSettings)},
    }
    form = DriverAddressForm

    fieldsets = (
        (None, {
            'fields': ('title', 'first_name', 'last_name',
                       'line1', 'geopoint', 'line4', 'state', 'postcode',
                       'country',)
        }),
    )


class PaymentTripAdminInline(admin.TabularInline):
    verbose_name_plural = 'Trips'
    model = PaymentTrip
    readonly_fields = (
        'get_trip_number', 'get_trip_base_pay', 'get_trip_deduction', 'get_order_number', 'get_customer', 'get_status', 'get_payment_status',
        'get_planned_start_time',
        'get_actual_start_time', 'get_actual_end_time',
        'get_total_distance', 'get_total_time',
        'get_mr_info', 'get_toll_charges', 'get_parking_charges', 'get_packages_info')
    fields = readonly_fields
    extra = 0
    max_num = 0
    can_delete = False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('trip')
        qs = qs.select_related('trip__order')
        qs = qs.select_related('trip__order__customer')
        qs = qs.order_by('trip__planned_start_time')
        return qs

    def get_customer(self, obj):
        if obj.trip and obj.trip.order:
            return obj.trip.order.customer
        return ''

    get_customer.short_description = _('Customer')

    def get_toll_charges(self, obj):
        toll_charges = 0
        if obj.trip:
            toll_charges = obj.trip.toll_charges
        if not toll_charges:
            toll_charges = obj.trip.order.toll_charges
        return toll_charges

    get_toll_charges.short_description = _('Toll Charges')

    def get_parking_charges(self, obj):
        if obj.trip:
            return obj.trip.parking_charges
        return ''

    get_parking_charges.short_description = _('Parking Charges')

    def get_trip_number(self, obj):
        if obj.trip:
            #            return obj.trip.trip_number
            url = reverse(
                'admin:trip_trip_change', args=[obj.trip.id])
            return format_html("<a href='{}?_changelist_filters=q%{}%'>{}</a>", url,
                               obj.trip.trip_number, obj.trip.trip_number)
        return None

    get_trip_number.short_description = _('Trip Number')

    def get_order_number(self, obj):
        if obj.trip and obj.trip.order :
            order = obj.trip.order
            order_id = obj.trip.order_id
            url = ''
            if order.order_type == settings.SUBSCRIPTION_ORDER_TYPE:
                url = reverse(
                    'admin:order_fixedorder_change', args=[order_id])
            elif order.order_type == settings.DEFCOM_ORDER_TYPE:
                url = reverse(
                    'admin:order_defcomorder_change', args=[order_id])
            else:
                url = reverse(
                    'admin:order_' + order.order_type + 'order_change', args=[order_id])
            return format_html("<a href='{}'>{}</a>", url,
                                   str(order))
        return None

    get_order_number.short_description = _('Order Number')

    def get_payment_status(self, obj):
        if obj.trip and obj.trip.order:
            order = obj.trip.order
            if order.order_type == settings.DEFAULT_ORDER_TYPE:
                return order.payment_status
        return None

    get_payment_status.short_description = _('Payment Status')

    def get_actual_start_time(self, obj):
        if obj.trip and obj.trip.actual_start_time:
            return get_in_ist_timezone(
                obj.trip.actual_start_time).strftime(DATETIME_FORMAT)
        return None

    get_actual_start_time.short_description = _('Trip Start')

    def get_planned_start_time(self, obj):
        if obj.trip:
            return get_in_ist_timezone(
                obj.trip.planned_start_time).strftime(DATETIME_FORMAT)
        return None

    get_planned_start_time.short_description = _('Scheduled At')

    def get_actual_end_time(self, obj):
        if obj.trip and obj.trip.actual_end_time:
            return get_in_ist_timezone(
                obj.trip.actual_end_time).strftime(DATETIME_FORMAT)
        return None

    get_actual_end_time.short_description = _('Trip End')

    def get_mr_info(self, obj):
        if obj.trip.meter_reading_end:
            return '%s to %s' % (
                obj.trip.meter_reading_start, obj.trip.meter_reading_end)
        return '-'

    get_mr_info.short_description = _('Meter Readings')

    def get_total_distance(self, obj):
        if obj.trip and obj.trip.total_distance is not None:
            return "%.2f" % obj.trip.total_distance
        return None

    get_total_distance.short_description = _('Total Distance (kms)')

    def get_total_time(self, obj):
        if obj.trip and obj.trip.total_time is not None:
            return minutes_to_dhms(obj.trip.total_time)
        return None

    get_total_time.short_description = _('Total Time')

    def get_status(self, obj):
        if obj.trip:
            return obj.trip.status
        return None

    get_status.short_description = _('Status')

    def get_packages_info(self, obj):
        _dict = {}
        for i in TRIP_PACKAGES_LIST:
            count = getattr(obj.trip, i, 0)
            if count:
                _dict[i] = count
        if _dict:
            return _dict
        else:
            return '-'

    get_packages_info.short_description = _('Packages')

    def get_trip_base_pay(self, obj):
        return obj.trip.trip_base_pay

    get_trip_base_pay.short_description = _('Supply Base Cost')

    def get_trip_deduction(self, obj):
        return obj.trip.trip_deduction_amount

    get_trip_deduction.short_description = _('Trip Deduction')

    def has_add_permission(self, request):
        """
        Trips are aggregated by payment engine, no add access
        """
        return False


class DriverPaymentDetailsAdminInline(admin.TabularInline):
    model = DriverPaymentDetails
    extra = 0
    readonly_fields = ('payment_info_prettified',)

    def payment_info_prettified(self, instance):
        """ Function to display pretty JSON """
        # Convert the data to sorted, indented JSON
        response = json.dumps(instance.data, sort_keys=True, indent=2)

        # Truncate the data. Alter as needed
        response = response[:5000]

        # Get the Pygments formatter
        formatter = HtmlFormatter(style='colorful')

        # Highlight the data
        response = highlight(response, JsonLexer(), formatter)

        # Get the stylesheet
        style = "<style>" + formatter.get_style_defs() + "</style><br>"

        # Safe the output
        return mark_safe(style + response)

    payment_info_prettified.short_description = _('Payment Details')

    def has_add_permission(self, request):
        """
        System generated, no need for editing
        """
        return False


class DriverAdjustmentInlineForm(forms.ModelForm):
    description = forms.CharField(
        widget=forms.TextInput(attrs={'style': 'width:25ch'}), )
    status = forms.CharField(
        widget=forms.TextInput(attrs={'style': 'width:20ch'}), )


class AddToDriverPaymentInline(admin.TabularInline):

    """ Base Class to be used for Adding Cash paid and
        Adjustments
    """
    readonly_fields = ('status', 'created_by',)
    extra = 0
    can_delete = False

    def get_queryset(self, request):
        """ Since this is editable, do not show
            already present records
        """
        qs = super().get_queryset(request)
        qs = qs.none()
        return qs

    def get_max_num(self, request, obj=None, **kwargs):
        """ Adding is disabled if the Payment object
            status is not Unapproved
        """
        if obj and obj.status != UNAPPROVED:
            return 0


class AddDriverCashPaidInline(AddToDriverPaymentInline):
    verbose_name_plural = 'Add Cash Paid'
    model = DriverCashPaid
    fields = ('cash_paid_datetime', 'description', 'cash_paid', 'proof',)
    can_delete = False
    form = AddDriverCashPaidInlineForm


class AddDriverAdjustmentInline(AddToDriverPaymentInline):
    verbose_name_plural = 'Add Adjustments'
    model = DriverPaymentAdjustment
    fields = ('category', 'description', 'adjustment_amount',
              'adjustment_datetime',)
    form = AddDriverAdjustmentInlineForm


class DriverAdjustmentAndCashPaidInline(admin.TabularInline):
    extra = 0
    max_num = 0
    can_delete = False
    can_add = False
    show_change_link = True

    def has_add_permission(self, request):
        return False


class DriverAdjustmentInline(DriverAdjustmentAndCashPaidInline):
    verbose_name_plural = 'Adjustments'
    model = DriverPaymentAdjustment
    fields = ('category', 'description', 'adjustment_amount',
              'adjustment_datetime', 'status', 'action_owner',)
    readonly_fields = fields


class DriverPaymentHistoryInline(admin.TabularInline):
    extra = 0
    verbose_name_plural = 'History'
    model = DriverPaymentHistory
    readonly_fields = ['field', 'old_value',
                       'new_value', 'user', 'modified_time']
    ordering = ['-modified_time']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DriverCashPaidInline(DriverAdjustmentAndCashPaidInline):
    verbose_name_plural = 'Cash Paid'
    model = DriverCashPaid
    fields = (
        'created_by',
        'cash_paid_datetime',
        'description',
        'cash_paid',
        'status',
        'proof')
    readonly_fields = fields


# form = DriverAdjustmentInlineForm
#
# TODO: The fields need to be readonly if the status is not NEW
# In below method, obj is actually parent object not the inline one
#    def get_readonly_fields(self, request, obj=None):
#        if obj and obj.status != UNAPPROVED:
#            return self.fields
#        return self.readonly_fields
#
#    def get_max_num(self, request, obj=None, **kwargs):
#        if obj and obj.status != UNAPPROVED:
#            return 0
#
#        return None

#    class Media:
#        js = ('/static/admin/js/driver_payment_adjustments.js',)

class DriverPaymentSettlementBaseAdmin(BaseAdmin):
    list_display = ('id', 'get_contract','driver_name', 'get_customer', 'get_city',
                        'start_datetime', 'end_datetime', 'status', 'is_fifty_percent_settlement', 'net_pay',
                    'get_vehicleclass', 'get_hub', 'get_division')

    fieldsets = (
        ('General', {
            'fields': ('get_contract', 'driver_name',  'get_customer', 'status',
                       'get_city', ('start_datetime', 'end_datetime'),
                       'net_pay',
                       )
        }),)
    search_fields = ('driver__driver_vehicle', 'id',)

    multi_search_fields = ('id',)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    list_filter = [
        ('contract__city'), ('contract__city__state'),
        HubFilter,
        ('contract__customer', admin.RelatedOnlyFieldListFilter),
        ('contract', admin.RelatedOnlyFieldListFilter),
        ('driver', admin.RelatedOnlyFieldListFilter),
        'payment_type',
        ContractTypeFilter,
        ('start_datetime', DateFilter),
        ('end_datetime', DateFilter),
    ]

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):

        fields = ('get_contract', 'driver_name',  'get_customer', 'status',
                  'get_city', 'start_datetime', 'end_datetime', 'net_pay')
        return fields

    def get_balance_payable(self, obj):
        return obj.net_pay - obj.settled_amount

    get_balance_payable.short_description = 'balance payable'

    def get_contract(self, obj):
        if obj.contract:
            return obj.contract.name
        return None

    get_contract.short_description = 'Contract'

    def get_city(self, obj):
        if obj.contract:
            return obj.contract.city
        return '-'

    def get_customer(self, obj):
        if obj and obj.contract:
            return obj.contract.customer
        return None

    get_city.short_description = 'City'
    get_customer.short_description = 'Customer'

    def get_division(self, obj):
        if obj.contract:
            return obj.contract.division
        return None

    get_division.short_description = _('Division')

    def get_hub(self, obj):
        hubs = set([str(x.trip.hub)
                    for x in obj.paymenttrip_set.all() if x.trip])

        # hub can be None so display the hub if exist other wise return none
        return ', '.join([hub for hub in hubs if hub])

    get_hub.short_description = _('Hub')

    def get_vehicleclass(self, obj):
        vehicle_classes = set([str(x.trip.vehicle.vehicle_model.vehicle_class)
                               for x in obj.paymenttrip_set.all()
                               if x.trip.vehicle])
        return ', '.join(vehicle_classes)

    get_vehicleclass.short_description = _('Vehicle Class')

    def bank_account(self, obj):
        return obj.driver.get_bank_account_details()

    def city(self, obj):
        return obj.driver.operating_city

    def driver_name(self, obj):
        return "%s | %s" % (obj.driver.name, obj.driver.driver_vehicle)

    driver_name.admin_order_field = 'driver__name'


class DriverPaymentSettlementAdmin(DriverPaymentSettlementBaseAdmin):
    actions = ['get_settlement_preview', 'get_50_percent_settlement_preview', 'settle_today', 'percent_settle_today',
               'settle_tomorrow', export_driver_payment_data]

    def settle_today(self, request, queryset):
        self.settle_payments(request, queryset, timezone.now())

    def percent_settle_today(self, request, queryset):
        self.settle_payments(request, queryset, timezone.now(), 50)

    percent_settle_today.short_description = _("Settle 50%% today")

    def settle_tomorrow(self, request, queryset):
        now = timezone.now()
        tomorrow = now + timedelta(days=1)
        self.settle_payments(request, queryset, tomorrow)

    # def percent_settle_tomorrow(self, request, queryset):
    #     now = timezone.now()
    #     tomorrow = now + timedelta(days=1)
    #     self.settle_payments(request, queryset, tomorrow, 50)
    #
    # percent_settle_tomorrow.short_description = 'Settle 50%% tomorrow'

    def get_settlement_preview(self, request, queryset):
        # aggregate = queryset.aggregate(total=Sum('net_pay'), count=Count('id'))
        amount = 0
        count = 0
        for payment in queryset:
            if payment.is_fifty_percent_settlement:
                amount += float(payment.net_pay/2)
            else:
                amount += float(payment.net_pay)
            count += 1

        preview_message = 'Settlement Preview: Sum=%.2f, Count=%d' % (amount, count)
        self.message_user(request, preview_message, level=admin_messages.INFO)

    def get_50_percent_settlement_preview(self, request, queryset):
        amount = 0
        count = 0
        for payment in queryset:
            amount += float(payment.net_pay / 2)
            count += 1

        preview_message = 'Settlement Preview: Sum=%.2f, Count=%d' % (amount, count)
        self.message_user(request, preview_message, level=admin_messages.INFO)

    @transaction.atomic
    def settle_payments(self, request, queryset, settlement_date,
                        settlement_percent=100, is_non_ops_Settlement=False):
        settlement_date_ist = get_in_ist_timezone(settlement_date)
        print('settlement_time', settlement_date_ist)
        cannot_settle = False
        if queryset.exclude(status=APPROVED).exists():
            self.message_user(
                request, "Choose Only Approved Records for Settlement.",
                level=admin_messages.ERROR)
            return

        if queryset.filter(is_settled=True).exists():
            self.message_user(
                request, "Cannot settle. Some Records Settled before.",
                level=admin_messages.ERROR)
            return

        missing_info = []
        for s in queryset.prefetch_related('driver__user',
                                           'driver__bank_account',
                                           'driver__owner_details'):
            cannot_settle = False

            if s.driver.get_bank_account_details() is None:
                missing_info.append(s.id)
                cannot_settle = True
                break

        if cannot_settle:
            self.message_user(
                request, "Cannot Settle. Bank Information incomplete %s" %(missing_info),
                level=admin_messages.ERROR)
        else:
            batch = DriverLedgerBatch(  # edit
                description='Settlement'
            )
            batch.save()

            settlement = DriverSettlement(batch.pk)
            count = 0
            total_amount = 0.0
            generate_bank_file = False

            for s in queryset.select_related('driver__user',
                                             'driver__bank_account',
                                             'driver__owner_details'):
                if is_non_ops_Settlement:
                    settle_amount = round(float(min(s.amount, s.driver.driverbalance.amount)), 2)
                    ledger_data = {
                        'nops_reference': s.id
                    }
                else:
                    if settlement_percent == 50:
                        if s.is_fifty_percent_settlement:
                            s.is_settled = True
                            # s.is_fifty_percent_settlement = False
                        else:
                            s.is_fifty_percent_settlement = True
                    else:
                        s.is_settled = True

                    if settlement_percent == 100 and s.is_fifty_percent_settlement:
                        s.net_pay = s.net_pay/2

                    settle_amount = round(min(float(s.net_pay) *
                                              (settlement_percent / 100), s.driver.driverbalance.amount), 2)
                    # if the total amount is settled or ledger amount is 0
                    # then payment is settled
                    if settle_amount == s.net_pay or \
                        settle_amount == s.driver.driverbalance.amount:
                        s.is_settled = True

                    ledger_data ={
                        'ops_reference': s.id
                    }

                if settle_amount >= 1:
                    count += 1
                    total_amount += float(settle_amount)
                    generate_bank_file = True

                    bank_account = s.driver.get_bank_account_details()
                    # ledger_entry = DriverLedger(
                    #     driver=s.driver,
                    #     owner_details=s.driver.owner_details,
                    #     amount=-settle_amount,
                    #     description='Settlement',
                    #     batch=batch,
                    #     is_settlement=True,
                    #     bank_account='%s-%s-%s' % (
                    #         bank_account.ifsc_code,
                    #         bank_account.account_number,
                    #         bank_account.account_name,
                    #     ),
                    #     bank_details=bank_account
                    # )
                    ledger_data.update({
                        'driver': s.driver.id,
                        'owner_details': s.driver.owner_details.id if s.driver.owner_details else None,
                        'amount': -settle_amount,
                        'description': 'Settlement',
                        'batch': batch.id,
                        'is_settlement': True,
                        'bank_account': '%s-%s-%s' % (
                            bank_account.ifsc_code,
                            bank_account.account_number,
                            bank_account.account_name,
                        ),
                        'bank_details': bank_account.id
                    })
                    ledger_entry = DriverLedgerSerializer(data=ledger_data)
                    if ledger_entry.is_valid():
                        ledger = ledger_entry.save()
                        settlement.add(s.driver, float(settle_amount), batch.id, ledger.id)
                        NotificationMixin.send_notification(NOTIFICATION_GROUP_PAYMENT_PAID,
                            s.driver.id, message_parms=[settle_amount,settings.OSCAR_DEFAULT_CURRENCY])
                    else:
                        print('Error updating record-----', ledger_entry.errors)
                        # raise ValueError(ledger_entry.errors)
                        self.message_user(
                            request,
                            ledger_entry.errors,
                            level=admin_messages.ERROR)
                s.save()

            if generate_bank_file:
                settlement.generate_bank_file(settlement_date_ist)
                batch.settlement_file = settlement.get_bank_file()
                batch.number_of_entries = count
                batch.total_amount = total_amount
                batch.save()

                now_ist = get_in_ist_timezone(timezone.now())
                settlement.send_email(request.user.email, now_ist)

                self.message_user(
                    request,
                    "Settled %d payments. Batch id is %s. "
                    "You will receive an email shortly with the bank "
                    "upload file." %
                    (count, batch.pk))
            else:
                self.message_user(
                    request,
                    "Could not generate payment file. Settlement Amount not enough for payment")

    def get_queryset(self, request):
        # user = request.user
        qs = super().get_queryset(request)
        # qs = apply_contract_permission_filter(user, qs)
        qs = qs.filter(net_pay__gte=1, status=APPROVED).exclude(is_settled=True)
        qs = qs.select_related('driver', 'contract')
        qs = qs.prefetch_related('driver__operating_city')
        qs = qs.prefetch_related('driver__bank_account')
        qs = qs.prefetch_related('driver__owner_details')
        # qs = qs.select_related('batch')
        qs = qs.select_related(
            'contract__customer', 'contract__division', 'contract__city')
        qs = qs.prefetch_related(Prefetch(
            'paymenttrip_set',
            queryset=PaymentTrip.objects.select_related(
                'trip__hub', 'trip__hub__customer',
                'trip__vehicle__vehicle_model__vehicle_class'
            ))
        )

        return qs


class DriverPaymentSettlementReversalAdmin(DriverPaymentSettlementBaseAdmin):
    actions = ['reverse_settlement', export_driver_payment_data]

    @transaction.atomic
    def reverse_settlement(self, request, queryset):
        if queryset.exclude(is_settled=True).exists():
            self.message_user(
                request, "Can Reverse. Only Settled payments",
                level=admin_messages.ERROR)
            return

        for s in queryset:
            reverse_amount = s.settled_amount if s.settled_amount > 0 else s.net_pay
            ledger_entry = DriverLedger(
                driver=s.driver,
                ops_reference=s,
                amount=reverse_amount,
                description='Bank-Reversal. Settlement-%s' % s.pk,
            )
            ledger_entry.save()
            s.is_settled = False
            s.save()

    def get_queryset(self, request):
        # user = request.user
        qs = super().get_queryset(request)
        # qs = apply_contract_permission_filter(user, qs)
        qs = qs.filter(net_pay__gte=1, status=APPROVED).exclude(is_settled=False)
        qs = qs.select_related('driver', 'contract')
        qs = qs.prefetch_related('driver__operating_city')
        qs = qs.prefetch_related('driver__bank_account')
        qs = qs.prefetch_related('driver__owner_details')
        # qs = qs.select_related('batch')
        qs = qs.select_related(
            'contract__customer', 'contract__division', 'contract__city')
        qs = qs.prefetch_related(Prefetch(
            'paymenttrip_set',
            queryset=PaymentTrip.objects.select_related(
                'trip__hub', 'trip__hub__customer',
                'trip__vehicle__vehicle_model__vehicle_class'
            ))
        )

        return qs


class DriverPaymentAdmin(FSMTransitionMixin, BaseAdmin):

    list_display = (
        'id', 'get_contract', 'get_contract_type', 'status', 'get_driver',
        'payment_type', 'get_vehicle', 'get_vehicleclass', 'get_hub',
        'get_city', 'get_expiry_days', 'start_datetime', 'end_datetime', 'get_customer',
        'get_division', 'get_trip_window', 'absent_days', 'total_earnings',
        'total_deductions', 'adjustment', 'get_cash_collected', 'gross_pay',
        'get_cash_paid_negative', 'net_pay', 'get_is_settled', 'is_fifty_percent_settlement',
        'has_contingency', 'is_stale_payment', 'get_batch')

    list_select_related = (
        'contract__customer', 'driver',
    )
    fsm_field = ['status', ]

    actions = [export_driver_payment_data, approve_payments,
                close_payments, open_payments]

    fieldsets = (
        ('General', {
            'fields': ('get_contract', 'get_contract_type', 'status',
                       'payment_type', 'get_driver', 'get_city',
                       ('start_datetime', 'end_datetime'),
                       'get_trip_window', 'get_is_settled',
                       'is_stale_payment', 'stale_reason', 'get_batch',
                       'has_contingency')
        }),
        ('Service Details', {
            'fields': ('total_distance', 'get_total_time',)
        }),
        ('Payment Details', {
            'fields': (('base_payment', 'distance_payment', 'time_payment',
                        'total_shipment_amount', 'weight_payment', 'volume_payment'), 'total_earnings',
                       ('absent_days', 'leave_deduction', 'total_deductions', 'excess_deduction'),
                       'adjustment', 'labour_cost', 'toll_charges', 'parking_charges',
                       'gross_pay', 'get_cash_paid_negative',
                       'get_cash_collected', 'tip_amount', 'net_pay')
        }),
        ('Calculation', {
            'fields': ('get_calculation',),
        }),
        ('Present Bank Details', {
            'fields': ('benefeciary_name', 'ifsc_code', 'account_number',
                       'bank_detail_note'),
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    readonly_fields = ['created_date', 'created_by', 'modified_date', 'status',
                       'payment_type', 'modified_by', 'get_contract',
                       'get_contract_type', 'get_division', 'base_payment',
                       'distance_payment', 'time_payment', 'total_earnings',
                       'absent_days', 'leave_deduction', 'total_deductions', 'excess_deduction',
                       'gross_pay', 'tip_amount','net_pay', 'total_distance', 'parking_charges',
                       'get_total_time', 'total_shipment_amount', 'get_driver',
                       'adjustment', 'get_cash_paid_negative', 'get_is_settled',
                       'get_calculation', 'can_approve', 'get_trip_window',
                       'start_datetime', 'end_datetime', 'get_city', 'weight_payment',
                       'labour_cost', 'benefeciary_name', 'ifsc_code', 'volume_payment',
                       'account_number', 'toll_charges', 'get_cash_collected',
                       'is_stale_payment', 'stale_reason', 'get_batch',
                       'has_contingency', 'bank_detail_note','get_expiry_days']

    necessary_inlines = [
        PaymentTripAdminInline,
        DriverAdjustmentInline,
        DriverCashPaidInline,
        #        DriverPaymentDetailsAdminInline,
        DriverPaymentHistoryInline,
    ]

    conditional_inlines = [
        AddDriverAdjustmentInline,
        AddDriverCashPaidInline,
    ]

    related_filter_model_fields = {
        'contract': ['contract__city', 'contract__customer'],
        'contract__division': ['contract__customer', ]
    }
    status_filter_config = {
        'title': 'status',
        'lookup_choices': [st[0] for st in DriverPayment.PAYMENT_STATUSES]
    }
    list_filter = [
        CityFilter, StateFilter,
        HubFilter, 'is_settled',
        ('contract__customer', admin.RelatedOnlyFieldListFilter),
        ('contract', GenericDependancyFilter),
        ('contract__division', GenericDependancyFilter),
        multiple_choice_list_filter(**status_filter_config),
        ('driver', admin.RelatedOnlyFieldListFilter),
        'payment_type',
        ('start_datetime', DateFilter),
        ('end_datetime', DateFilter),
        'batch_id', DriverPaymentContingencyFilter,
        ExpiryDaysFilter,
    ]

    # search_fields = [
    #     'driver__name', 'paymenttrip__trip__hub__name', 'contract__name',
    #     'paymenttrip__trip__vehicle__registration_certificate_number', 'id']

    search_fields = ['id']
    multi_search_fields = ('id',)

    def get_actions(self, request):
        """ Restrict close and open of driver payments to super-users only
        """
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            del actions['close_payments']
            del actions['open_payments']
        return actions

    """
    @TODO this query is wrong, fetch hubs based on related orders
    for this payment
    """
    def get_queryset(self, request):
        user = request.user
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(user, qs)
        qs = qs.select_related('driver')
        qs = qs.prefetch_related('driver__operating_city')
        qs = qs.prefetch_related('driver__bank_account')
        qs = qs.prefetch_related('driver__owner_details')
        qs = qs.select_related('batch')
        qs = qs.select_related(
            'contract__customer', 'contract__division', 'contract__city')
        qs = qs.prefetch_related(Prefetch(
            'paymenttrip_set',
            queryset=PaymentTrip.objects.select_related(
                'trip__hub', 'trip__hub__customer', 'trip__vehicle',
                'trip__vehicle__vehicle_model',
                'trip__vehicle__vehicle_model__vehicle_class'
            ))
        )
        qs = qs.select_related(
            'driver__driverbalance'
        )
        qs = qs.prefetch_related(Prefetch('driver__driverledger_set',
                                          queryset=DriverLedger.objects.filter(
                                              is_settlement=True)))
        # qs = qs.distinct()
        # Very expensive to do this.
#        qs = qs.annotate(
#            contingency=Count(Case(
#                When(paymenttrip__trip__is_contingency=True, then=1),
#                output_field=IntegerField(),
#            ))
#        )
        return qs

    def get_inline_instances(self, request, obj=None):
        inlines = [inline(self.model, self.admin_site)
                   for inline in self.necessary_inlines]

        # Show Add Inlines only if payment is unapproved status only
        if obj and obj.status == UNAPPROVED:
            inlines.append(
                AddDriverAdjustmentInline(
                    self.model,
                    self.admin_site))
            inlines.append(
                AddDriverCashPaidInline(
                    self.model,
                    self.admin_site))

        return inlines

    def get_expiry_days(self, obj):
        """
            Get the days count for payments expiry
        """
        difference = (obj.end_datetime + timedelta(days=UNAPPROVED_DEACTIVATION_PERIOD)) - date.today()
        div_style = '<div style="width:100%%; height:100%%; background-color:{};"><center>{}</center></div>'

        if (obj.status in [APPROVED, WITHHELD, CLOSED]) or (obj.end_datetime > date.today()):
            return format_html(div_style.format('darkseagreen','NA'))

        elif difference.days <= 0:
            return format_html(div_style.format('#A8A8A8','EXPIRED'))

        elif difference.days in range(1,6):
            return format_html(div_style.format('#FF6347',difference.days))

        elif difference.days in range(6,11):
            return format_html(div_style.format('orange',difference.days))

        elif difference.days in range(11,16):
            return format_html(div_style.format('yellow',difference.days))

        elif difference.days > 15:
            return format_html(div_style.format('limegreen',difference.days))

        return difference.days

    get_expiry_days.short_description = 'Will Expire in Days'
    get_expiry_days.admin_order_field = 'end_datetime'
    get_expiry_days.allow_tags = True

    def has_contingency(self, obj):
        return bool([x for x in obj.paymenttrip_set.all()
                     if x.trip.is_contingency])

    has_contingency.short_description = _('Has Contingency trip(s)')
#    has_contingency.admin_order_field = 'paymenttrip__trip__is_contingency'
    has_contingency.boolean = True

    def get_is_settled(self, obj):
        return obj.is_settled

    get_is_settled.short_description = 'Is Settled'
    get_is_settled.boolean = True

    def bank_detail_note(self, obj):
        bank_details = obj.driver.get_bank_account_details()

        if bank_details:
            return format_html(
                '<b>This is the Current Account On Driver Profile as of Now</b>')
        return None

    bank_detail_note.short_description = _('NOTE')

    def get_contract(self, obj):
        type = obj.contract.contract_type
        if type == CONTRACT_TYPE_BLOWHORN:
            url = reverse(
                'admin:contract_blowhorncontract_change', args=[obj.contract.id])
        elif type == CONTRACT_TYPE_C2C:
            url = reverse(
                'admin:contract_c2ccontract_change',args=[obj.contract.id])
        elif type == CONTRACT_TYPE_SME:
            url = reverse(
                'admin:contract_smecontract_change', args=[obj.contract.id])
        else:
            url = reverse(
                'admin:contract_contract_change', args=[obj.contract.id])

        return format_html(
            '<a target="_blank" href="{}">{}</a>', url, obj.contract.name)

    get_contract.short_description = _('Contract')
    get_contract.admin_order_field = 'contract'

    def get_contract_type(self, obj):
        return obj.contract.contract_type

    get_contract_type.short_description = _('Contract Type')
    get_contract_type.admin_order_field = 'contract__contract_type'

    def get_division(self, obj):
        return obj.contract.division

    get_division.short_description = _('Division')
    get_division.admin_order_field = 'contract__division'

    def get_driver(self, obj):
        return obj.driver.name

    get_driver.short_description = _('Driver')
    get_driver.admin_order_field = 'driver'

    def get_total_time(self, obj):
        trip_time = sum([pt.trip.total_time
                         for pt in obj.paymenttrip_set.all() if pt.trip.total_time])

        if trip_time:
            return minutes_to_dhms(trip_time)

        return 0.0

    get_total_time.short_description = _('Total Time(Hrs)')

    def get_trip_window(self, obj):
        trip_times = [
            x.trip.planned_start_time for x in obj.paymenttrip_set.all()]
        if not trip_times:
            return ''

        min_ = get_in_ist_timezone(min(trip_times))
        max_ = get_in_ist_timezone(max(trip_times))
        return '%s - %s %s' % (min_.day, max_.day, max_.strftime('%b'))

    get_trip_window.short_description = _('Trip window')

    def get_hub(self, obj):
        hubs = set([str(x.trip.hub)
                    for x in obj.paymenttrip_set.all() if x.trip])

        # hub can be None so display the hub if exist other wise return none
        return ', '.join([hub for hub in hubs if hub])

    get_hub.short_description = _('Hub')

    def get_vehicle(self, obj):
        vehicles = set([str(x.trip.vehicle)
                        for x in obj.paymenttrip_set.all() if x.trip.vehicle])
        return ', '.join(vehicles)

    get_vehicle.short_description = _('Vehicle')

    def get_vehicleclass(self, obj):
        vehicle_classes = set([str(x.trip.vehicle.vehicle_model.vehicle_class)
                               for x in obj.paymenttrip_set.all()
                               if x.trip.vehicle])
        return ', '.join(vehicle_classes)

    get_vehicleclass.short_description = _('Vehicle Class')

    def get_customer(self, obj):
        if obj:
            return obj.contract.customer
        return None

    get_customer.short_description = _('Customer')
    get_customer.admin_order_field = 'contract__customer'

    def is_stale_payment(self, obj):
        if obj:
            num_modified_trips = 0
            for t in obj.paymenttrip_set.all():
                if t.trip.modified_date >= obj.modified_date:
                    num_modified_trips += 1
            return obj.is_stale or num_modified_trips > 0
        return False

    is_stale_payment.short_description = _('Stale')

    def get_cash_paid_negative(self, obj):
        return -obj.cash_paid

    get_cash_paid_negative.short_description = _('Cash Paid')
    get_cash_paid_negative.admin_order_field = 'cash_paid'

    def get_cash_collected(self, obj):
        return -obj.cash_collected

    get_cash_collected.short_description = _('Cash Collected')
    get_cash_collected.admin_order_field = 'cash_collected'

    def get_calculation(self, obj):
        slip = driver_payment_calculation_formatter(obj)
        return format_html('<pre>%s</pre>' % slip)

    get_calculation.short_description = _('Details')

    def benefeciary_name(self, obj):
        bank_details = obj.driver.get_bank_account_details()

        if bank_details:
            return bank_details.account_name
        return None

    def ifsc_code(self, obj):
        bank_details = obj.driver.get_bank_account_details()

        if bank_details:
            return bank_details.ifsc_code
        return None

    def account_number(self, obj):
        bank_details = obj.driver.get_bank_account_details()

        if bank_details:
            return bank_details.account_number
        return None

    def get_city(self, obj):
        return obj.contract.city

    def get_batch(Self, obj):
        batch = obj.batch
        if batch:
            url = reverse(
                'admin:driver_paymentbatch_change', args=[batch.id])
            return format_html(
                '<a target="_blank" href="{}">{}</a>', url, batch)
        return obj.batch

    get_batch.short_description = 'Payment Batch'

    get_city.short_description = _('City')

    def get_payment_settled(self, obj):
        if obj.status == APPROVED:
            settled = True
            if obj.driver.driverbalance and obj.driver.driverbalance.amount > 0:
                settled = False
                sorted_ledger = sorted(
                    obj.driver.driverledger_set.filter(is_settlement=True), key=lambda x: x.transaction_time, reverse=True)
                if sorted_ledger and sorted_ledger[0].transaction_time > obj.modified_date:
                    settled = True
            return settled
        return False

    get_payment_settled.short_description = 'Payment Settled'
    get_payment_settled.boolean = True

    def has_add_permission(self, request):
        """
        Payments are system generated, no permissions to add
        """
        return False

    def has_delete_permission(self, request, obj=None):
        pks = []
        delete_action = False
        del_selected = False
        sel_action = False

        """
        If this is getting called for delete action check if any of the
        selected records are not in approved status
        """
        for key, values in request.POST.lists():
            if key == 'action' and values == ['delete_selected']:
                del_selected = True
            if key == '_selected_action':
                sel_action = True
                pks = values

        if del_selected and sel_action and \
            not DriverPayment.objects.filter(id__in=pks,
                                             status=APPROVED).exists():
            delete_action = True

        else:
            delete_action = False

        if delete_action or (obj and obj.status != APPROVED):
            return super(DriverPaymentAdmin, self).has_delete_permission(request, obj=obj)
        return False

    def add_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        return super().add_view(request, extra_context=extra_context)

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        driverpayment=DriverPayment.objects.get(pk=object_id)
        if driverpayment.status == APPROVED:
            extra_context['show_save'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/driver/driver_payment.js',)


class PaymentBatchAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_time', 'description',
                    'end_time', 'number_of_entries', 'records_updated')

    fields = ('start_time', 'description', 'end_time',
              'number_of_entries', 'records_updated')

    readonly_fields = ('start_time', 'description',
                       'end_time', 'number_of_entries', 'records_updated')

    search_fields = ['id']
    list_filter = [('start_time', DateRangeFilter)]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    actions = None


class DriverLedgerAdmin(FSMTransitionMixin, admin.ModelAdmin):
    search_fields = ('driver__user__name',
                     'driver__vehicles__registration_certificate_number',
                     'bank_account', 'id',
                     'description', 'order__number'
                     )

    list_filter = (('driver', RelatedFieldAjaxListFilter), 'batch_id')

    list_display = ('id', 'driver', 'order', 'transaction_time', 'description',
                    'credit', 'debit', 'balance', 'reference', 'get_batch_id',
                    'modified_by', 'status', 'is_settlement')

    list_select_related = (
        'driver', 'modified_by',
    )

    fields = ('driver', 'description', 'amount', 'bank_account')
    readonly_fields = fields
    fsm_field = ('status',)
    actions = None
    show_full_result_count = False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related(
            'order', 'driver', 'modified_by')
#        qs = qs.select_related('batch', 'ops_reference')
        qs = qs.order_by('-transaction_time')
        return qs

    def has_add_permission(self, request):
        return False

    def credit(self, obj):
        return obj.amount if obj.amount > 0 else ''

    credit.admin_order_field = 'amount'

    def debit(self, obj):
        return obj.amount if obj.amount <= 0 else ''

    debit.admin_order_field = 'amount'

    def get_batch_id(self, obj):
        return obj.batch_id

    debit.get_batch_id = 'Batch'

    def reference(self, obj):
        if obj.ops_reference_id:
            url = reverse(
                'admin:driver_driverpayment_change',
                args=[obj.ops_reference_id])
            return format_html("<a href='{}'>{}</a>", url,
                               obj.ops_reference_id)
        elif obj.nops_reference_id:
            url = reverse(
                'admin:driver_nonoperationpayment_change',
                args=[obj.nops_reference_id])
            return format_html("<a href='{}'>{}</a>", url,
                               obj.nops_reference_id)
        elif obj.deduction_reference_id:
            url = reverse(
                'admin:driver_driverdeduction_change',
                args=[obj.deduction_reference_id])
            return format_html("<a href='{}'>{}</a>", url,
                               obj.deduction_reference_id)

        return ''

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def has_delete_permission(self, request, obj=None):
        return False


class DriverLedgerInlineAdmin(admin.TabularInline):
    model = DriverLedger
    readonly_fields = (
        'get_link', 'driver', 'transaction_time', 'amount', 'description',
        'balance', 'ops_reference', 'batch')

    fields = readonly_fields
    extra = 0
    max_num = 0
    can_delete = False

    def get_link(self, obj):
        if obj and obj.id:
            url = reverse('admin:driver_driverledger_change', args=[obj.id])
            return format_html("<a href='{}'>{}</a>", url, obj.id)

        return ''

    get_link.short_description = 'ID'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.order_by('-transaction_time')
        return qs


class DriverBalanceAdmin(BaseAdmin):
    list_per_page = 500
    search_fields = ('driver__driver_vehicle',)
    multi_search_fields = ('driver__driver_vehicle',)
    list_display = ('driver_name', 'city', 'amount', 'bank_account')
    list_filter = (('driver__operating_city',
                    admin.RelatedOnlyFieldListFilter), DriverFilter)
    readonly_fields = list_display
    inlines = [DriverLedgerInlineAdmin]
    # actions = ('get_settlement_preview', 'settle_today', 'percent_settle_today',
    #            'settle_tomorrow', 'percent_settle_tomorrow')

    # list_select_related = ('city', 'bank_account',)

    # Remove the delete action
    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def bank_account(self, obj):
        return obj.driver.get_bank_account_details()

    def city(self, obj):
        return obj.driver.operating_city

    def driver_name(self, obj):
        return "%s | %s" % (obj.driver.name, obj.driver.driver_vehicle)

    driver_name.admin_order_field = 'driver__name'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.filter(amount__gte=1)
        qs = qs.select_related('driver')
        qs = qs.prefetch_related('driver__operating_city')
        qs = qs.prefetch_related('driver__bank_account')
        qs = qs.prefetch_related('driver__owner_details')
        return qs

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def settle_today(self, request, queryset):
        self.settle_payments(request, queryset, timezone.now())

    def percent_settle_today(self, request, queryset):
        self.settle_payments(request, queryset, timezone.now(), 50)

    percent_settle_today.short_description = _("Settle 50%% today")

    def settle_tomorrow(self, request, queryset):
        now = timezone.now()
        tomorrow = now + timedelta(days=1)
        self.settle_payments(request, queryset, tomorrow)

    def percent_settle_tomorrow(self, request, queryset):
        now = timezone.now()
        tomorrow = now + timedelta(days=1)
        self.settle_payments(request, queryset, tomorrow, 50)

    percent_settle_tomorrow.short_description = 'Settle 50%% tomorrow'

    def get_settlement_preview(self, request, queryset):
        aggregate = queryset.aggregate(total=Sum('amount'), count=Count('id'))
        preview_message = 'Settlement Preview: Sum=%.2f, Count=%d' % (
            aggregate['total'], aggregate['count'])
        self.message_user(request, preview_message, level=admin_messages.INFO)

    @transaction.atomic
    def settle_payments(self, request, queryset, settlement_date,
                        settlement_percent=100):
        settlement_date_ist = get_in_ist_timezone(settlement_date)
        print('settlement_time', settlement_date_ist)
        cannot_settle = False
        for s in queryset.prefetch_related('driver__user',
                                           'driver__bank_account',
                                           'driver__owner_details'):

            if s.driver.get_bank_account_details() is None:
                cannot_settle = True
                break

        if cannot_settle:
            self.message_user(
                request, "Cannot Settle. Bank Information incomplete",
                level=admin_messages.ERROR)
        else:
            batch = DriverLedgerBatch(
                description='Settlement'
            )
            batch.save()

            settlement = DriverSettlement(batch.pk)
            count = 0
            total_amount = decimal.Decimal(0.0)
            for s in queryset.select_related('driver__user',
                                             'driver__bank_account',
                                             'driver__owner_details'):
                count += 1
                settle_amount = s.amount * decimal.Decimal(settlement_percent/100)
                total_amount += settle_amount
                # print(s.driver.user.name,
                #       s.driver.bank_account.account_name,
                #       s.driver.bank_account.account_number,
                #       s.driver.bank_account.ifsc_code, s.amount,
                #       )
                bank_account = s.driver.get_bank_account_details()
                ledger_entry = DriverLedger(
                    driver=s.driver,
                    owner_details=s.driver.owner_details,
                    amount=-settle_amount,
                    description='Settlement',
                    batch=batch,
                    is_settlement=True,
                    bank_account='%s-%s-%s' % (
                        bank_account.ifsc_code,
                        bank_account.account_number,
                        bank_account.account_name,
                    ),
                    bank_details=bank_account
                )
                ledger_entry.save()
                settlement.add(s.driver, settle_amount, batch.pk, ledger_entry.pk)

            settlement.generate_bank_file(settlement_date_ist)
            batch.settlement_file = settlement.get_bank_file()
            batch.number_of_entries = count
            batch.total_amount = total_amount
            batch.save()

            now_ist = get_in_ist_timezone(timezone.now())
            settlement.send_email(request.user.email, now_ist)

            self.message_user(
                request,
                "Settled %d payments. Batch id is %s. "
                "You will receive an email shortly with the bank "
                "upload file." %
                (count, batch.pk))


class DriverLedgerBatchAdmin(admin.ModelAdmin):
    list_display = ('id', 'transaction_time',
                    'description', 'number_of_entries', 'total_amount',)
    fields = ('id', 'transaction_time', 'description', 'number_of_entries',
              'total_amount', 'settlement_file')
    readonly_fields = fields
    search_fields = ('id',)
    inlines = [DriverLedgerInlineAdmin]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        return False

    #    def has_change_permission(self, request, obj=None):
    #        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)


class BankAccountHistoryInline(admin.TabularInline):
    model = BankAccountHistory
    verbose_name_plural = "Bank Account History"

    readonly_fields = ['user',  'created_time', 'bank_account', 'driver', 'vendor', 'action']

    can_delete = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


def merge_bank(modeladmin, request, queryset):
    logger.info('Merging bank accounts.. %s' % request.POST)
    bank_account_id = request.POST.get('bank_account')

    if not bank_account_id:
        admin_messages.error(request, 'Select a target Bank Account')
        return False

    merging_accounts_list = queryset.values('pk', 'account_name', 'account_number', 'ifsc_code')
    logger.info('Bank accounts list: %s' % merging_accounts_list)

    conflicting_pk = [x.get('pk') for x in merging_accounts_list if
                      int(x.get('pk')) == int(bank_account_id)]
    if not conflicting_pk:
        successful_migration = BankAccountUtils().merge_bank_data(bank_account_id,
                                                                merging_accounts_list)

        if successful_migration:
            message = 'No. of bank accounts migrated: %s' % successful_migration
            admin_messages.success(request, message)
            admin_messages.success(request, '')

        if successful_migration:
            admin_messages.info(request, 'Check Bank Account Migration History for detail')
    else:
        message = 'Conflicting: %s. The target bank account should not be in checked list' % \
                  conflicting_pk[0]
        admin_messages.error(request, message)


merge_bank.short_description = _("Merge Bank Accounts")


class BankAccountAdmin(admin.ModelAdmin, forms.ModelForm):

    """Bank Account Admin model."""

    inlines = [BankAccountHistoryInline]
    fieldsets = (
        ('General', {'fields': ('bank_name', 'account_name', 'account_number',
                                'account_number2', 'ifsc_code')}),
        ('Audit log', {'fields': ('created_by', 'created_date', 'modified_by', 'modified_date')}),
    )

    action_form = BankAccountActionForm

    actions = [merge_bank]

    # The fields to be used in displaying the BankAccount model.
    list_display = ('id', 'account_name', 'account_number', 'ifsc_code')
    search_fields = ('id', 'account_name', 'account_number', 'ifsc_code')
    readonly_fields = ('created_by', 'created_date', 'modified_by', 'modified_date')
    ordering = ('account_name',)

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if obj:
            self.form = BankAccountChangeForm
        else:
            self.form = BankAccountCreationForm
        return super(BankAccountAdmin, self).get_form(request, obj, **kwargs)

    def get_actions(self, request):
        actions = super(BankAccountAdmin, self).get_actions(request)
        if not(request.user.is_city_manager or request.user.is_superuser):
            del actions['merge_bank']
        return actions


class NonOpsPaymentAdmin(FSMTransitionMixin, BaseAdmin, forms.ModelForm):

    # TODO: division should only display divisions applicable
    # for selected customer
    resource_class = NonOperationPaymentResource
    list_display = ['id', 'driver', 'customer', 'division', 'city', 'status',
                    'start_datetime', 'end_datetime',
                    'purpose', 'amount', 'is_settled']
    list_filter = ['start_datetime', 'end_datetime', 'city', 'status', 'is_settled']

    list_select_related = (
        'driver', 'city', 'customer',
    )

    actions = ['settle_today',]
    form = NonOpsForm
    search_fields = ['driver__user__name', 'purpose', 'description', 'amount',
                     'customer__user__name', 'id']
    multi_search_fields = ('id',)
    fsm_field = ['status', ]

    def settle_today(self, request, queryset):
        if queryset.filter(amount__lte=0).exists():
            self.message_user(
                request, "Settlement not Allowed for Amount < 0.",
                level=admin_messages.ERROR)
            return

        DriverPaymentSettlementAdmin(self, admin_site=AdminSite).settle_payments(request, queryset, timezone.now(),
                                                                                 is_non_ops_Settlement=True)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def get_export_queryset(self, request):
        qs = super().get_export_queryset(request)
        qs = qs.select_related('division', 'driver',
                               'customer', 'city')
        return list(qs)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['status', 'is_settled']
        if obj and obj.status != obj.NEW:
            readonly_fields = readonly_fields + self.list_display
            if obj.purpose == 'asset':
                readonly_fields.append('asset')
        return readonly_fields

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        qs = qs.select_related(
            'customer', 'division', 'driver', 'city', 'city__address')
        return qs

class AssignActionOwnerForm(ActionForm):
    process_ids = City.objects.values_list('process_associates', flat=True)
    user = forms.ModelChoiceField(
        queryset=User.objects.filter(is_active=True, is_staff=True, id__in=process_ids), required=False)


def reassign_action_owner(modeladmin, request, queryset):
    """
    reassign action owner
    """
    new_owner_id = request.POST.get('user')
    if not new_owner_id:
        admin_messages.error(request, "action owner not selected")
        return False
    if queryset.exclude(status=DriverPaymentAdjustment.NEW).exists():
        admin_messages.error(request, "Action Owner can be changed only for New Adjustment")
        return False

    queryset.update(action_owner_id=new_owner_id)
    admin_messages.success(
        request, "Action Owner Changed successfully")


reassign_action_owner.short_description = _('Reassign Adjustment')


class DriverAdjustmentHistoryInline(admin.TabularInline):
    verbose_name = _('History')
    verbose_name_plural = _('History')
    model = DriverPaymentAdjustmentHistory
    extra = 0
    can_delete = False
    fields = ('field', 'old_value', 'new_value', 'remarks',
              'get_action_by', 'get_time')
    readonly_fields = fields

    def get_action_by(self, obj):
        return obj.created_by

    get_action_by.short_description = _('action by')

    def get_time(self, obj):
        return utc_to_ist(obj.created_date).strftime(DATETIME_FORMAT)

    get_time.short_description = _('time')

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return True


class DriverAdjustmentAdmin(FSMTransitionMixin, admin.ModelAdmin):
    resource_class = DriverAdjustmentResource
    readonly_fields = ('status', 'get_driver', 'get_contract', 'get_total_adjustment_amount',
                       'get_gross_pay', 'get_other_adjustment', 'action_owner',
                       'get_cash_paid', 'get_this_adjustment_amount',
                       'get_net_pay', 'payment_ref', 'created_date', 'action_owner',
                       'created_by', 'modified_date', 'modified_by')

    list_display = ('get_id', 'get_driver', 'get_contract', 'get_hub', 'payment_ref', 'action_owner',
                    'category', 'adjustment_datetime', 'adjustment_amount', 'get_total_adjustment_amount',
                    'description', 'status')

    list_select_related = (
        'driver', 'contract', 'payment', 'payment__driver',
        'payment__contract',
    )

    search_fields = [
        'driver__name', 'payment__driver__name', 'category', 'description', 'action_owner__email',
        'payment__contract__name', 'payment__id',
        'driver__vehicles__registration_certificate_number',
        'payment__driver__vehicles__registration_certificate_number']

    list_filter = [
        'payment__contract__city', HubFilter,
        'category', AdjustmentCashPaidContractFilter,
        AdjustmentDriverFilter, 'status',
        ('payment__contract__customer', admin.RelatedOnlyFieldListFilter),
        ('adjustment_datetime', DateFilter)]

    fsm_field = ['status', ]

    form = DriverAdjustmentForm
    action_form = AssignActionOwnerForm
    actions = [export_driver_adjustment_data, reassign_action_owner]
    inlines = [DriverAdjustmentHistoryInline]

    def get_fieldsets(self, request, obj=None):
        if obj is None:
            fieldsets = (
                ('General', {
                    'fields': ('adjustment_datetime', 'category',
                               'description', 'adjustment_amount',
                               'status', 'driver', 'contract',)
                }),
                ('Audit Log', {
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                }),
            )
        else:
            fieldsets = (
                ('General', {
                    'fields': ('adjustment_datetime', 'category',
                               'description', 'action_owner',
                               ('get_gross_pay', 'get_other_adjustment',
                                'get_cash_paid',),
                               'adjustment_amount', 'get_net_pay', 'status',
                               'payment_ref', 'get_driver', 'get_contract',
                               'remarks')
                }),
                ('Audit Log', {
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                }),
            )

        return fieldsets

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            remarks = data.get('remarks')
            obj.remarks = remarks
            obj.action_by = request.user
        obj.save()
        super().save_model(request, obj, form, change)

    def payment_ref(self, obj):
        url = reverse('admin:driver_driverpayment_change', args=[obj.payment])
        return format_html("<a href='{}'>{}</a>", url, obj.payment or '')

    payment_ref.admin_order_field = 'payment'
    payment_ref.short_description = 'Payment Ref'

    def get_id(self, obj):
        link = reverse("admin:driver_driverpaymentadjustment_change",
                       args=[obj.id])
        return format_html('<a href="%s">%s</a>' % (link, obj.id))

    get_id.short_description = 'Adjustment id'
    get_id.admin_order_field = 'id'

    def get_total_adjustment_amount(self, obj):
        return obj.total_adjustment

    get_total_adjustment_amount.short_description = 'Approved + current Adjustment'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related(
            'driver', 'contract', 'payment', 'created_by', 'modified_by',
            'payment__driver', 'payment__contract', 'payment__contract__division'
        )
        qs = qs.select_related(
            'contract__customer', 'contract__city'
        )
        qs = qs.annotate(adjustment_sum=Sum('payment__driverpaymentadjustment__adjustment_amount'))
        # qs = qs.filter(adjustment_sum__gt=float(DriverConstants().get('DP_ADJUSTMENT_APPROVAL_LIMIT', 3000)))
        qs = qs.prefetch_related(Prefetch(
            'payment__paymenttrip_set',
            queryset=PaymentTrip.objects.select_related(
                'trip__vehicle__vehicle_model__vehicle_class', 'trip__hub'
            ))
        )
        qs = qs.prefetch_related(
            'payment__paymenttrip_set__trip__hub',
            'payment__paymenttrip_set__trip__hub__customer'
        )
        return qs

    def get_export_queryset(self, request):
        # Convert the queryset to list as the export function uses
        # an iterator and hence prefetch related doesn't work. Without
        # prefetch_related the number of queries explodes impacting performance
        # and function not working at all for large volumes
        qs = super().get_export_queryset(request)
        qs = qs.select_related('driver', 'contract', 'payment')
        qs = qs.select_related(
            'contract__customer', 'contract__city')
        # qs = qs.prefetch_related(Prefetch(
        #     'payment__paymenttrip_set',
        #     queryset=PaymentTrip.objects.select_related(
        #         'trip__vehicle__vehicle_model__vehicle_class'
        #     )))
        qs = qs.prefetch_related('payment__paymenttrip_set')
        return list(qs)

    def get_hub(self, obj):
        hubs = set([str(x.trip.hub)
                    for x in obj.payment.paymenttrip_set.all() if x.trip])

        # hub can be None so display the hub if exist other wise return none
        return ', '.join([hub for hub in hubs if hub])

    get_hub.short_description = _('Hub')

    def get_gross_pay(self, obj):
        return (obj.payment and obj.payment.gross_pay)

    get_gross_pay.short_description = 'Gross Pay'

    def get_other_adjustment(self, obj):
        """ Get other adjustments excluding this one """
        if obj.payment:
            if obj.status == obj.APPROVED:
                return (obj.payment.adjustment - obj.adjustment_amount)
            else:
                return (obj.payment.adjustment)
        return None

    get_other_adjustment.short_description = 'Other Adjustments'

    def get_this_adjustment_amount(self, obj):
        return obj.adjustment_amount

    get_this_adjustment_amount.short_description = '** This Adjustment **'

    def get_cash_paid(self, obj):
        return (obj.payment and -obj.payment.cash_paid)

    get_cash_paid.short_description = 'Cash Paid'

    def get_net_pay(self, obj):
        """ Get net payable amount after considering whether this is
            already included in the net pay or not """
        if obj.payment:
            if obj.status == obj.APPROVED:
                return (obj.payment.net_pay)
            else:
                return (obj.payment.net_pay + obj.adjustment_amount)
        return None

    get_net_pay.short_description = 'Net Pay'

    def get_driver(self, obj):
        return obj.driver or (obj.payment and obj.payment.driver)

    get_driver.admin_order_field = 'payment__driver'
    get_driver.short_description = _('Driver')

    def get_contract(self, obj):
        return obj.contract or (obj.payment and obj.payment.contract)

    get_contract.admin_order_field = 'payment__contract'
    get_contract.short_description = _('Contract')

#    def get_queryset(self, request):
#        qs = super().get_queryset(request)
#        qs = apply_contract_permission_filter(request.user, qs)
#        qs = qs.order_by('-adjustment_datetime')
#        return qs

    def get_readonly_fields(self, request, obj=None):
        new_readonly_fields = self.readonly_fields
        if obj:
            if obj.payment:
                new_readonly_fields += ('driver', 'contract')

            if obj.status != obj.NEW or \
                not (request.user == obj.created_by or
                     request.user in obj.payment.contract.supervisors.all()):
                # Make the amount readonly either if the status is not NEW or
                # the user is not one of the creator or supervisors
                new_readonly_fields += (
                    'adjustment_datetime', 'category', 'description',
                    'adjustment_amount',)
                # If payment refernce is present, driver and contract
                # should not be allowed to be edited

        return new_readonly_fields

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        if not request.user.is_superuser and not request.user.is_city_manager:
            del actions['reassign_action_owner']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    class Media:
        css = {
            'all': (
                'website/css/toastr.css',
                '/static/website/css/bootstrap.css',
                '/static/admin/driver/css/payment_adjustment_fsm_remark.css'
            )
        }
        js = (
            '/static/website/js/lib/jquery.min.js',
            '/static/website/js/lib/bootstrap.min.js',
            '/static/admin/driver/js/payment_adjustment_reason.js',
        )


class AlternateContactAdmin(admin.ModelAdmin):
    form = AlternateContactForm

    list_display = ('name', 'phone_number', 'relationship')
    search_fields = ['name', 'phone_number', 'relationship']
    fields = ('name', 'alternate_number', 'relationship')

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            alternate_number = data.get('alternate_number')

            if alternate_number:
                obj.phone_number = phonenumbers.parse(
                    alternate_number,
                    settings.COUNTRY_CODE_A2
                )

            obj.save()
            super().save_model(request, obj, form, change)


class DriverCashPaidAdmin(FSMTransitionMixin, admin.ModelAdmin):
    list_display = (
        'created_by', 'get_driver', 'get_contract', 'payment_ref',
        'cash_paid_datetime', 'cash_paid', 'description', 'status')

    list_filter = [
        'payment__contract__city',
        ('created_by', admin.RelatedOnlyFieldListFilter),
        AdjustmentCashPaidContractFilter,
    ]

    readonly_fields = ('payment', 'get_driver', 'get_contract', 'payment_ref',
                       'created_by', 'proof', 'status',)

    search_fields = ['driver__name', 'payment__driver__name']

    # list_select_related = ('driver', 'contract', 'payment', 'payment__driver',
    #                        'payment__contract',)

    fsm_field = ('status',)

    def payment_ref(self, obj):
        url = reverse('admin:driver_driverpayment_change', args=[obj.payment])
        return format_html("<a href='{}'>{}</a>", url, obj.payment or '')

    def get_fields(self, request, obj=None):
        if obj is None:
            fields = (
                'driver', 'contract', 'cash_paid_datetime', 'description',
                'cash_paid', 'proof',)
        else:
            if obj.payment:
                fields = (
                    'get_driver', 'get_contract', 'cash_paid_datetime',
                    'description', 'cash_paid', 'payment', 'created_by',
                    'status', 'proof',)
            else:
                fields = (
                    'driver', 'contract', 'cash_paid_datetime', 'description',
                    'cash_paid', 'created_by', 'status', 'proof',)

        return fields

    def get_readonly_fields(self, request, obj=None):
        new_readonly_fields = self.readonly_fields
        if obj:
            if obj.payment:
                new_readonly_fields += ('driver', 'contract')
                if obj.payment.status != UNAPPROVED:
                    new_readonly_fields += self.list_display
            if obj.status != obj.NEW:
                new_readonly_fields += self.get_fields(request, obj)

        return new_readonly_fields

    def get_queryset(self, request):
        user = request.user
        qs = super().get_queryset(request)
        qs = apply_contract_permission_filter(user, qs)
        qs = qs.select_related('driver', 'payment', 'contract')
        qs = qs.select_related(
            'contract__city', 'payment__driver', 'payment__contract',
            'created_by', 'payment__contract__city')
        qs = qs.prefetch_related('contract__customer__customerdivision_set')
        qs = qs.order_by('-cash_paid_datetime')
        return qs

    def get_driver(self, obj):
        return obj.driver or (obj.payment and obj.payment.driver)

    get_driver.short_description = _('Driver')

    def get_contract(self, obj):
        return obj.contract or (obj.payment and obj.payment.contract)

    get_contract.short_description = _('Contract')

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


class DriverActivityHistoryAdmin(admin.ModelAdmin):
    resource_class = DriverActivityHistoryResource
    formfield_overrides = {
        models.PointField: {'widget': GooglePointFieldWidget}
    }

    list_display = ['driver', 'event', 'order_str', 'trip_str', 'stop_str',
                    'get_geopoint', 'created_time', 'device_timestamp',
                    'location_accuracy_meters', 'vehicle_speed_kmph']

    readonly_fields = list_display + ['other_details', 'gps_timestamp']
    list_select_related = ('driver',)
    search_fields = ['order_str', 'trip_str', 'stop_str']
    list_filter = ('driver', ('created_time', DateRangeFilter))

    exclude = ('order', 'trip', 'stop')

    actions = None
    empty_value_display = '--'
    show_full_result_count = False
    actions_selection_counter = False

    fieldsets = (
        ('General', {
            'fields': ('driver', 'event',
                       ('order_str', 'trip_str', 'stop_str'),
                       ('created_time', 'device_timestamp'),)
        }),
        ('Location', {
            'fields': ('geopoint',)
        }),
        ('Other', {
            'fields': ('location_accuracy_meters', 'vehicle_speed_kmph',
                       'other_details')
        })
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_geopoint(self, obj):
        if obj and obj.geopoint:
            lat = obj.geopoint.coords[1]
            lon = obj.geopoint.coords[0]
            latlon = '%s,%s' % (lat, lon)
            return format_html(
                '<a href="http://maps.google.com/maps?q={}" target="_blank">{}'
                '</a>', latlon, latlon)
        return ''

    get_geopoint.short_description = 'location'

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        if not (request.GET.get('driver__id__exact') or request.GET.get('q')) \
                and not request.GET.get('_changelist_filters'):
            return qs.none()

        qs = qs.select_related('driver')
        qs = qs.select_related('trip')
        qs = qs.select_related('order')
        qs = qs.order_by('-created_time')
        return qs


class DriverActivityAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }

    actions = None
    list_filter = ['driver', ]
    list_display = ['id', 'driver', 'orientation_from_north',
                    'get_event', 'trip_str',
                    'get_trip_status', 'order_str', 'get_order_status',
                    'get_geopoint', 'created_time', 'updated_time']

    list_select_related = ('driver',)

    additional_fields = ['event', 'location_accuracy_meters',
                         'vehicle_speed_kmph', 'order_str', 'trip_str',
                         'stop_str', 'device_timestamp', 'other_details']

    readonly_fields = list_display + additional_fields

    fieldsets = (
        ('General', {
            'fields': ('driver', 'event',
                       ('order_str', 'trip_str', 'stop_str'),
                       ('created_time', 'device_timestamp'),)
        }),
        ('Location', {
            'fields': ('geopoint', 'orientation_from_north')
        }),
        ('Other', {
            'fields': ('location_accuracy_meters', 'vehicle_speed_kmph',
                       'other_details')
        })
    )

    # Django way of linking foreign key fields in admin view
    def get_order(self, obj):
        if obj.order:
            return obj.order.number
        else:
            return None

    get_order.short_description = 'Order'
    get_order.admin_order_field = 'order'  # used for sorting

    # To disable the Add button on admin
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_geopoint(self, obj):
        if obj and obj.geopoint:
            lat = obj.geopoint.coords[1]
            lon = obj.geopoint.coords[0]
            latlon = '%s,%s' % (lat, lon)
            return format_html(
                '<a href="http://maps.google.com/maps?q={}" target="_blank">{}'
                '</a>', latlon, latlon)
        return ''

    get_geopoint.short_description = 'Location'

    def get_trip(self, obj):
        if obj.trip:
            return obj.trip.trip_number
        else:
            return None

    get_trip.short_description = 'Trip'
    get_trip.admin_order_field = 'trip'

    def get_event(self, obj):
        if obj:
            return obj.event
        else:
            return None

    get_event.short_description = 'Event'
    get_event.admin_order_field = 'event'

    def get_order_status(self, obj):
        if obj.order:
            return obj.order.status
        else:
            return None

    def get_trip_status(self, obj):
        if obj.trip:
            return obj.trip.status
        else:
            return None

    get_order_status.short_description = 'Order Status'
    get_order_status.admin_order_field = 'order__status'
    get_trip_status.short_description = 'Trip Status'
    get_trip_status.admin_order_field = 'trip__status'

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver', 'trip', 'order')
        return qs


class DriverLogInfoAdmin(admin.ModelAdmin):

    list_display = ('driver', 'action', 'action_date')
    fields = ('driver', 'action', 'action_date')
    readonly_fields = ('driver', 'action', 'action_date')

    list_filter = ['driver', 'action',
                   ('action_date', DateFilter),
                   ]
    list_select_related = ('driver',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver')
        return qs

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)


class DriverLogActivityAdmin(admin.ModelAdmin):

    list_display = ('driver', 'login_time', 'logout_time',
                    'gps_distance', 'get_batch_id')
    fields = ('driver', 'login_time', 'logout_time', 'gps_distance', 'batch')
    readonly_fields = ('driver', 'login_time', 'logout_time', 'gps_distance')

    list_filter = ['driver',
                   ('login_time', DateFilter),
                   ('logout_time', DateFilter)
                   ]
    list_select_related = ('driver',)

    def get_batch_id(self, obj):
        batch = obj.batch
        if batch:
            url = reverse(
                'admin:driver_driverlogbatch_change', args=[batch.id])
            return format_html('<a target="_blank" href="{}">{}</a>',
                               url, batch)
        return obj.batch

    get_batch_id.short_description = 'Batch id'

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver', 'batch')
        return qs

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)


class DriverLogBatchAdmin(admin.ModelAdmin):

    list_display = ('id', 'start_time', 'end_time', 'number_of_entries')
    fields = ('id', 'start_time', 'end_time', 'number_of_entries')
    readonly_fields = ('id', 'start_time', 'end_time', 'number_of_entries')
    search_fields = ['id']

    list_filter = [('start_time', DateFilter), ]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)


class DriverReferralAdmin(admin.ModelAdmin):

    list_display = ('driver', 'referred_by', 'referral_config')
    readonly_fields = ('driver', 'referred_by',
                       'activation_timestamp', 'marked_inactive',
                       'bonus_generated', 'inactivation_timestamp',
                       'created_by', 'created_date', 'modified_by',
                       'modified_date')
    search_fields = ['driver__name', 'referred_by__name']

    list_filter = [('activation_timestamp', DateFilter), ]

    fieldsets = (
        ('Referral Details', {
            'fields': ('driver', 'referred_by', 'referral_config',
                       'activation_timestamp', 'marked_inactive',
                       'bonus_generated', 'inactivation_timestamp')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DriverDeductionAdmin(FSMTransitionMixin, admin.ModelAdmin):
    fsm_field = ['status', ]

    list_display = ('id', 'driver_id', 'driver', 'partner', 'status', 'deduction_amount', 'reference_id', 'transaction_id', 'deduction_date')
    readonly_fields = list_display + ('remark',)
    search_fields = ['driver__name', 'partner__legalname']
    list_filter = ['status']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False



class DriverReferralConfigurationAdmin(admin.ModelAdmin):

    list_display = ('city', 'vehicle_class', 'bonus_amount', 'min_active_duration', 'is_active')
    search_fields = ['bonus_amount', 'min_active_duration']
    list_filter = ['is_active','city', 'vehicle_class']

    fieldsets = (
        ('Referral Configuration', {
            'fields': ('is_active', 'city', 'vehicle_class', 'bonus_amount', 'min_active_duration')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        self.readonly_fields = ('created_date', 'created_by', 'modified_date',
                                'modified_by')
        if obj:
            self.readonly_fields += (
                # 'city', 'commercial_classification', 'bonus_amount', 'min_active_duration')
                'bonus_amount', 'min_active_duration')

        return self.readonly_fields


class BackGroundVerificationAdmin(admin.ModelAdmin):
    form = BackGroundVerificationForm
    list_display = ('driver', 'reference_id', 'client_status', 'source')
    search_fields = ['driver__name',
                     'driver__vehicles__registration_certificate_number']

    readonly_fields = ('created_date', 'created_by', 'modified_date',
                       'modified_by')

    actions = ['export_bgv']

    fieldsets = (
        ('Driver Details', {
            'fields': ('driver', 'document_type',)
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def export_bgv(self, request, queryset):
        bgv_ids = queryset.values_list('id', flat=True)
        queryset = BackGroundVerification.copy_data.filter(id__in=bgv_ids).order_by('-id')

        file_path = 'background_verifications'

        queryset.annotate(
            operating_city=F('driver__operating_city__name')
        ).to_csv(
            file_path,
            'driver__name', 'operating_city', 'reference_id', 'document_type', 'client_status', 'bpss_status', 'source'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow(['Driver', 'Operating City', 'Reference ID', 'Client Status', 'Bpss_status', 'Source'
                        ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response
    export_bgv.short_description = 'Export Selected Background Verifications'

    def has_add_permission(self, request):
        if request.user:
            return request.user.is_superuser
        return False

    def changeform_view(self, request, object_id=None, form_url='',
                        extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        return super(BackGroundVerificationAdmin, self).changeform_view(
            request, object_id, extra_context=extra_context)

    def get_readonly_fields(self, request, obj=None):
        new_readonly_fields = self.readonly_fields
        if obj:
            new_readonly_fields += ('driver', 'document_type')

        return new_readonly_fields

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data
            driver = data.get('driver')
            document_type = data.get('document_type')

            bgv_data = get_data_for_background_verification(
                driver, document_type)
            response = create_bgv_profile(bgv_data)

            if response and response.status_code == 202:
                response_data = response.json()
                reference_id = response_data.get(
                    'data', {}).get('referenceId', None)

                if reference_id:
                    obj.driver = driver
                    obj.reference_id = reference_id

                obj.save()
                super().save_model(request, obj, form, change)


class CashHandoverAdmin(admin.ModelAdmin):
    list_display = (
        'driver', 'collected_by', 'amount', 'hub', 'created_date', 'trip')

    list_filter = ('driver', 'collected_by', ('created_date', DateFilter),)
    search_fields = ['trip__trip_number']

    fields = ('driver', 'collected_by', 'amount', 'hub', 'trip')
    readonly_fields = ('driver', 'collected_by', 'amount', 'hub', 'trip')

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DriverGPSInformationAdmin(admin.ModelAdmin):
    list_display = ('driver', 'trip', 'is_gps_on', 'device_timestamp', 'created_date')

    search_fields = ['driver__name', 'driver__driver_vehicle']

    list_filter = [ ('driver', admin.RelatedOnlyFieldListFilter)]
    # search_fields = ['trip__number']

    fields = list_display
    readonly_fields = list_display

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DriverLoanAdmin(admin.ModelAdmin):
    list_display = ('partner', 'driver',  'created_time', 'is_driver_detail', 'is_balance_enquiry')

    list_filter = [('driver', admin.RelatedOnlyFieldListFilter),
                   ('partner', admin.RelatedOnlyFieldListFilter),]

    search_fields = ['driver__name', 'driver__driver_vehicle', 'partner__legalname']

    fields = list_display
    readonly_fields = list_display

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DriverAcknowledgementAdmin(admin.ModelAdmin):
    list_display = ('driver', 'trip', 'order', 'is_accepted', 'created_date')
    readonly_fields = list_display
    search_fields = ('order__number', 'trip__trip_number', 'driver__name', 'driver__driver_vehicle')


class BankMigrationHistoryAdmin(admin.ModelAdmin):
    list_display = ('bank_account', 'number_of_accounts', 'created_by')
    search_fields = ('bank_account',)
    readonly_fields = ('bank_account', 'number_of_accounts', 'get_log_dump')
    exclude = ('log_dump',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_log_dump(self, obj):
        val = obj.log_dump_json
        formatted_text = ''
        for v in val:
            formatted_text += str(v) + '\n'
        return formatted_text

    get_log_dump.short_description = _('Log')


class EventTypeAdmin(admin.ModelAdmin):
    list_display = ('event_name', 'description')
    search_fields = ('event_name',)
    readonly_fields = ('created_by', 'created_date', 'modified_by', 'modified_date')

    fieldsets = (
        ('Event', {
            'fields': ('key', 'event_name', 'data_type', 'return_type', 'description')
        }),
        ('Audit Log', {
            'fields': ('created_by', 'created_date', 'modified_by', 'modified_date')
        })
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return request.user.is_superuser


class DriverSourcingOptionAdmin(admin.ModelAdmin):
    list_display = ('source', 'display_in_cosmos', 'display_in_partner_app')
    search_fields = ('source', )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return request.user.is_superuser


class DeliveryWorkerActivityAdmin(admin.ModelAdmin):

    list_display = ('id', 'user', 'activity', 'activity_time', 'hub', 'driver', 'trip',
                    'get_geopoint', 'app_version')

    readonly_fields = list_display
    fieldsets = (
        ('General', {
            'fields': list_display
        }),
    )

    list_filter = ('activity',
                   ('activity_time', DateFilter),
                   ('hub', admin.RelatedOnlyFieldListFilter),
                   ('user', admin.RelatedOnlyFieldListFilter),
                   ('driver', admin.RelatedOnlyFieldListFilter),
                   )

    search_fields = ('trip__trip_number', 'driver__name', 'driver__driver_vehicle')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_geopoint(self, obj):
        if obj and obj.location:
            lat = obj.location.coords[1]
            lon = obj.location.coords[0]
            latlon = '%s,%s' % (lat, lon)
            return format_html(
                '<a href="http://maps.google.com/maps?q={}" target="_blank">{}'
                '</a>', latlon, latlon)
        return ''

    get_geopoint.short_description = 'location'


admin.site.register(DriverPaymentAdjustment, DriverAdjustmentAdmin)
admin.site.register(LoadPreference, LoadPreferenceAdmin)
admin.site.register(DriverCashPaid, DriverCashPaidAdmin)
admin.site.register(DriverBalance, DriverBalanceAdmin)
admin.site.register(DriverPayment, DriverPaymentAdmin)
admin.site.register(DriverLedger, DriverLedgerAdmin)
admin.site.register(DriverLedgerBatch, DriverLedgerBatchAdmin)
admin.site.register(DriverAddress, DriverAddressAdmin)
admin.site.register(AlternateContact, AlternateContactAdmin)
admin.site.register(BankAccount, BankAccountAdmin)
admin.site.register(DriverActivity, DriverActivityAdmin)
admin.site.register(DriverActivityHistory, DriverActivityHistoryAdmin)
admin.site.register(Driver, DriverAdmin)
admin.site.register(DriverDocument, DriverDocumentAdmin)
admin.site.register(DriverConstants, DriverConstantsAdmin)
admin.site.register(DeviceDetails, DriverDeviceDetailsAdmin)
admin.site.register(NonOperationPayment, NonOpsPaymentAdmin)
admin.site.register(DriverLogInfo, DriverLogInfoAdmin)
admin.site.register(DriverLogActivity, DriverLogActivityAdmin)
admin.site.register(DriverLogBatch, DriverLogBatchAdmin)
admin.site.register(PaymentBatch, PaymentBatchAdmin)
admin.site.register(DriverReferrals, DriverReferralAdmin)
admin.site.register(DriverReferralConfiguration,
                    DriverReferralConfigurationAdmin)
admin.site.register(BackGroundVerification, BackGroundVerificationAdmin)
admin.site.register(DriverError, DriverAppErrorsAdmin)
admin.site.register(DriverPaymentSettlementView, DriverPaymentSettlementAdmin)
admin.site.register(CashHandover, CashHandoverAdmin)
admin.site.register(DriverInactiveReason, DriverInactiveReasonAdmin)
admin.site.register(DriverDeduction, DriverDeductionAdmin)
admin.site.register(DriverGPSInformation, DriverGPSInformationAdmin)
admin.site.register(DriverLoanDetails, DriverLoanAdmin)
admin.site.register(DriverDocumentPage, DriverDocumentPageAdmin)
admin.site.register(BankMigrationHistory, BankMigrationHistoryAdmin)
admin.site.register(DriverAcknowledgement, DriverAcknowledgementAdmin)
admin.site.register(EventType, EventTypeAdmin)
admin.site.register(DriverSourcingOption, DriverSourcingOptionAdmin)
# admin.site.register(DriverPaymentSettlementReversalView, DriverPaymentSettlementReversalAdmin)
admin.site.register(NotificationCampaign, DriverNotificationAdmin)
admin.site.register(DriverNotificationResponse, DriverNotificationResponseAdmin)
# admin.site.register(DriverResponseFields, DriverResponseFieldsAdmin)
admin.site.register(NotificationFormLanguage, NotificationFormLanguageAdmin)
admin.site.register(NotificationContentLanguage, NotificationContentLanguageAdmin)
admin.site.register(DeliveryWorkerActivity, DeliveryWorkerActivityAdmin)
from .subadmins import payment_admin
