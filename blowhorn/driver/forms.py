import re
from datetime import datetime
from django.conf import settings
from django.forms import fields as formfields
from django.urls import reverse
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.admin.helpers import ActionForm
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db.models import Max, Min
from django.forms.models import BaseInlineFormSet

from config.settings import status_pipelines as StatusPipeline
from blowhorn.users.models import User
from blowhorn.driver import messages
from blowhorn.driver import constants as driver_const
from blowhorn.utils.functions import validate_phone_number
from blowhorn.utils.functions import get_in_ist_timezone
from blowhorn.contract.models import ResourceAllocation
from blowhorn.utils.datetimepicker import DateTimePicker
from blowhorn.driver.models import Driver, BankAccount, AlternateContact, \
    NonOperationPayment, DriverWellBeing
from blowhorn.driver.submodels.asset_models import Asset
from blowhorn.contract.constants import (
    CONTRACT_STATUS_ACTIVE,
    CONTRACT_TYPE_BLOWHORN
)
from blowhorn.common.fields import CustomModelChoiceField
from blowhorn.common.middleware import current_request
from blowhorn.contract.models import Contract
from blowhorn.document.constants import BETTERPLACE_DOCUMENT_TYPE_CHOICES
from blowhorn.driver.util import get_data_for_background_verification
from blowhorn.driver.util import get_driver_missing_doc
from blowhorn.document.models import DocumentType
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT


def validate_age(date_of_birth):
    """
        Validation for the age of the Driver. If Driver's age is above 18
        then return date of birth else throw an error
    """
    try:
        born = datetime.strptime(str(date_of_birth), "%Y-%m-%d")
        now = datetime.now()
        calc_age = ((now - born).days / driver_const.NUMBER_OF_DAYS_IN_YEAR)
        if driver_const.DRIVER_MIN_AGE <= calc_age:
            return date_of_birth
        else:
            raise ValidationError(
                "Driver should be above " + str(driver_const.DRIVER_MIN_AGE) + " years of age")
    except ValueError:
        raise ValueError('Incorrect data format, should be YYYY-MM-DD')


def validate_pan(pan, driver_id):

    if Driver.objects.filter(
        pan=pan
    ).exclude(pk=driver_id).exists():
        raise ValidationError("Driver already exists with same PAN number")


class DriverForm(forms.ModelForm):
    name_validator = RegexValidator(
        regex=r'^[a-zA-Z_\s]+$',
        message=_("Only Alphabets characters allowed")
    )
    name = forms.CharField(max_length=100,
                           validators=[name_validator])
    phone_number = forms.CharField(label='Mobile Number',
                                    max_length=10, validators=[validate_phone_number])

    password = forms.RegexField(regex=r'^\d{4}$', required=True,
                                error_messages={'invalid': messages.MUST_BE_FOUR_DIGIT_PIN},
                                max_length=4)
    working_preferences = forms.MultipleChoiceField(
        choices=driver_const.WORKING_PREFERENCES, required=False)

    reason_for_change = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            driver = kwargs['instance']
            user = driver.user if driver else None
            if user:
                initial = {'name': user.name,
                           'phone_number': user.national_number}
                kwargs['initial'] = initial
        super(DriverForm, self).__init__(*args, **kwargs)

        if self.fields.get('contract', None):
            self.fields['contract'].autocomplete = False
            self.fields['contract'].queryset = Contract.objects.filter(
                contract_type=CONTRACT_TYPE_BLOWHORN, status=CONTRACT_STATUS_ACTIVE)

        status = self.instance.status
        for field_name in self.fields:
            field = self.fields[field_name]
            if isinstance(field, formfields.DateField):
                field.widget = DateTimePicker(
                    options={"format": "DD-MM-YYYY"})

        '''
            If status is Onboarding
                Allowed statuses are Onboarding, Active and Inactive
            If status is Active or Inactive
                Allowed statuses are Active and Inactive
        '''
        if status == StatusPipeline.ONBOARDING:
            self.fields[
                'status'].widget.choices = StatusPipeline.DRIVER_ALLOWED_STATUSES

        elif status == StatusPipeline.ACTIVE or \
                status == StatusPipeline.INACTIVE:
            self.fields[
                'status'].widget.choices = StatusPipeline.DRIVER_END_STATUSES

        else:
            self.fields[
                'status'].widget.choices = StatusPipeline.DRIVER_DEFAULT_STATUS

    def clean(self, *args, **kwargs):
        phone_number = self.cleaned_data.get('phone_number')
        status = self.cleaned_data.get('status')
        date_of_birth = self.cleaned_data.get('date_of_birth')

        driver_number = settings.ACTIVE_COUNTRY_CODE + \
            phone_number if phone_number else None
        # inactive_reason = self.cleaned_data.get('reason_for_inactive')
        # description = self.cleaned_data.get('description')
        pan = self.cleaned_data.get('pan')
        is_blowhorn_driver = self.cleaned_data.get('is_blowhorn_driver')
        contract = self.cleaned_data.get('contract')

        if pan:
            validate_pan(pan, None)
        if date_of_birth:
            validate_age(date_of_birth)

        # If mobile number is already registered
        if User.objects.filter(
            is_staff=False,
            phone_number=driver_number,
            email__contains=settings.DEFAULT_DOMAINS['driver']).exists():
            raise ValidationError({
                'phone_number': messages.MOBILE_NUMBER_ALREADY_REGISTERED
            })

        if not is_blowhorn_driver and contract:
            raise ValidationError({
                'contract': 'Contract is not Required for this Driver'
            })

        if is_blowhorn_driver and not contract:
            raise ValidationError({
                'contract':'Blowhorn Driver Should have contract'
            })

        # If status is inactive, mandatory to give reason and description
        # if status == StatusPipeline.INACTIVE:
        #     if not inactive_reason:
        #         raise ValidationError({
        #             'reason_for_inactive': (messages.INACTIVE_REASON_MANDATORY)
        #         })
        #
        #     if not description:
        #         raise ValidationError({
        #             'description': (messages.INACTIVE_DESCRIPTION_MANDATORY)
        #         })

    class Meta:
        model = Driver
        exclude = ['user', 'driver_rating']

    class Media:
        js = ('/static/admin/js/driver_admin.js',)


class DriverEditForm(forms.ModelForm):
    # name_validator = RegexValidator(
    #     regex=r'^[a-zA-Z_\s]+$',
    #     message=_("Only Alphabets characters allowed")
    # )
    # name = forms.CharField(max_length=100,
    #                        validators=[name_validator])
    #
    # phone_number = forms.CharField(label='Mobile Number',
    #                                max_length=10, validators=[validate_phone_number])

    password = ReadOnlyPasswordHashField(label=_("password"))

    # working_preferences = forms.MultipleChoiceField(
    #     choices=driver_const.WORKING_PREFERENCES, required=False)

    reason_for_change = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        user = None
        if 'instance' in kwargs:
            driver = kwargs['instance']
            user = driver.user if driver else None
            if user:
                initial = {'name': user.name,
                           'phone_number':
                               str(user.national_number),#.zfill(
                                   #10),
                           'password': user.password}
                kwargs['initial'] = initial
        super(DriverEditForm, self).__init__(*args, **kwargs)

        if self.fields.get('contract', None):
            self.fields['contract'].autocomplete = False
            self.fields['contract'].queryset = Contract.objects.filter(
                contract_type=CONTRACT_TYPE_BLOWHORN, status=CONTRACT_STATUS_ACTIVE)

        status = self.instance.status
        for field_name in self.fields:
            field = self.fields[field_name]
            if isinstance(field, formfields.DateField):
                field.widget = DateTimePicker(
                    options={"format": "DD-MM-YYYY"})
        if user:
            # No reverse url for password change. Need to construct manually
            link = reverse('admin:users_user_change', args=[user.id])
            url_components = link.split('/')[:-2]
            url_components.append('password')
            final_url = '/'.join(url_components)
            help_text = """Raw passwords are not stored, so there is no way to see "
            this user's password, but you can change the password
            using""" + u'<a href="%s" target="_blank"> this form</a>' % (
                final_url)
            self.fields['password'].help_text = help_text

        '''
            If status is Onboarding
                Allowed statuses are Onboarding, Active and Inactive
            If status is Active or Inactive
                Allowed statuses are Active and Inactive
            Not allowed to change status from 'Supply' to any other statuses

        '''
        # if status == StatusPipeline.ONBOARDING:
        #     self.fields[
        #         'status'].widget.choices = StatusPipeline.DRIVER_ALLOWED_STATUSES

        # elif status == StatusPipeline.ACTIVE or \
        #         status == StatusPipeline.INACTIVE:
        #     self.fields[
        #         'status'].widget.choices = StatusPipeline.DRIVER_END_STATUSES
        # elif status == StatusPipeline.BLACKLISTED:
        #     self.fields[
        #         'status'].widget.choices = StatusPipeline.DRIVER_BLACKLISTED_STATUS
        # else:
        #     self.fields[
        #         'status'].widget.choices = StatusPipeline.DRIVER_DEFAULT_STATUS

        '''
            Commenting out the above cases to prevent the manual changing
            of the status from the admin
        '''

        if status and status in StatusPipeline.DRIVER_DISPLAY_STATUSES:
            self.fields[
                'status'].widget.choices = StatusPipeline.DRIVER_DISPLAY_STATUSES[status]
        else:
            self.fields[
                'status'].widget.choices = StatusPipeline.DRIVER_DEFAULT_STATUS


    def clean(self, *args, **kwargs):
        status = self.cleaned_data.get('status')
        # inactive_reason = self.cleaned_data.get('reason_for_inactive')
        # description = self.cleaned_data.get('description')
        date_of_birth = self.cleaned_data.get('date_of_birth')

        # Fetching the phone number of the driver
        # phone_number = self.cleaned_data.get('phone_number')
        # driver_number = settings.ACTIVE_COUNTRY_CODE + \
            # phone_number if phone_number else None
        is_blowhorn_driver = self.cleaned_data.get('is_blowhorn_driver')
        contract = self.cleaned_data.get('contract')

        pan = self.cleaned_data.get('pan')
        if pan:
            validate_pan(pan, self.instance.id)

        if date_of_birth:
            validate_age(date_of_birth)

        # user = User.objects.filter(
        #     is_staff=False,
        #     phone_number=driver_number,
        #     email__icontains=settings.DEFAULT_DOMAINS['driver']
        # ).exclude(id=self.instance.user_id)
        # if user.exists():
        #     """
        #     If any user exists with the same phone number
        #     after excluding the driver
        #     then raise the error
        #     """
        #     raise ValidationError({
        #         'phone_number': messages.MOBILE_NUMBER_ALREADY_REGISTERED
        #     })

        if not is_blowhorn_driver and contract:
            self.cleaned_data['contract'] = None

        if is_blowhorn_driver and not contract:
            raise ValidationError({
                'contract':'Blowhorn Driver Should have contract'
            })

        if status == StatusPipeline.ACTIVE:
            mandatory_driver_doc_type = DocumentType.objects.filter(is_document_mandatory=True, identifier=DRIVER_DOCUMENT)
            mandatory_vehicle_doc_type = DocumentType.objects.filter(is_document_mandatory=True,
                                                                     identifier=VEHICLE_DOCUMENT)

            missing_doc = get_driver_missing_doc(self.instance, mandatory_driver_doc_type, mandatory_vehicle_doc_type, status=status)

            if missing_doc:
                raise ValidationError('Mandatory Document %s is missing check all mandatory document is active' %(missing_doc))

        # If status is inactive, mandatory to give reason and description
        if status == StatusPipeline.INACTIVE:
            driver_allocated = ResourceAllocation.objects.filter(
                drivers=self.instance,
                contract__status=CONTRACT_STATUS_ACTIVE).first()

            if driver_allocated:
                raise ValidationError(
                    messages.DRIVER_ALLOCATED_FOR_CONTRACT %
                    driver_allocated.contract)

            # if not inactive_reason:
            #     raise ValidationError({
            #         'reason_for_inactive': (messages.INACTIVE_REASON_MANDATORY)
            #     })

            # if not description:
            #     raise ValidationError({
            #         'description': (messages.INACTIVE_DESCRIPTION_MANDATORY)
            #     })

    class Meta:
        model = Driver
        exclude = ['user', 'driver_rating']

    class Media:
        js = ('/static/admin/js/driver_admin.js',)


class NonOpsForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea, required=False)

    def __init__(self, *args, **kwargs):
        super(NonOpsForm, self).__init__(*args, **kwargs)
        self.fields['description'].required = True
        if self.instance and self.instance.status == NonOperationPayment.NEW \
                and self.instance.purpose == 'asset':
            self.fields['asset'].autocomplete = False
            self.fields['asset'].queryset = Asset.objects.filter(
                    driver=self.instance.driver)

    def clean(self, *args, **kwargs):
        purpose = self.cleaned_data.get('purpose')
        description = self.cleaned_data.get('description')
        start_datetime = self.cleaned_data.get('start_datetime')
        end_datetime = self.cleaned_data.get('end_datetime')
        asset = self.cleaned_data.get('asset')

        if purpose:
            if purpose == driver_const.NON_OPS_OTHERS and not description:
                raise forms.ValidationError("If you choose Purpose as 'Others' \
                    'You should Provide the description.")
            elif purpose == driver_const.NON_OPS_ASSET and not asset:
                raise forms.ValidationError("Asset is mandatory if purpose is 'Asset'")

        if start_datetime and end_datetime and \
                start_datetime > end_datetime:
            raise forms.ValidationError(
                "End Date cannot be lesser than Start Date.")

    class Media:
        js = ('/static/admin/js/driver/nonops_payment.js',)


class BankAccountCreationForm(forms.ModelForm):

    """
    A form for creating new bank account for drivers.
    Includes all the required fields, plus a repeated account number.
    """

    error_messages = {
        'account_mismatch': _("The two Account Number fields didn't match."),
        'invalid_ifsc': _("Please enter a valid IFSC code."),
    }

    account_number2 = forms.CharField(
        label=_("Account Number confirmation"), max_length=20,
        widget=forms.TextInput(attrs={'style': 'width:36ch'}),
        help_text=_("Enter same Account Number as above, for verification."))

    class Meta:
        model = BankAccount
        fields = ('account_name', 'ifsc_code')

    def clean_account_number2(self):
        """
        Check that the two password entries match.
        :return str account_number2: cleaned account_number2
        :raise forms.ValidationError: account_number1 != account_number2
        """
        account_number1 = self.cleaned_data.get("account_number")
        account_number2 = self.cleaned_data.get("account_number2")
        if account_number1 and account_number2 and \
                account_number1 != account_number2:
            raise forms.ValidationError(
                self.error_messages['account_mismatch'],
                code='account_mismatch',
            )
        return account_number2

    def clean_ifsc_code(self):
        """
        IFSC Validation for bank accounts
        """
        reg_exp = re.compile(driver_const.IFSC_REGEX, re.UNICODE)
        ifsc_code = self.cleaned_data.get("ifsc_code")
        match = reg_exp.match(ifsc_code)

        if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            if match is not None:
                return ifsc_code
            else:
                raise forms.ValidationError(
                    self.error_messages['invalid_ifsc'],
                    code='invalid_ifsc',
                )
        return ifsc_code

    class Media:
        css = {
            'all': ('/static/admin/css/driver_bank_account.css',)
        }
        js = ('/static/admin/js/driver_bank_account.js',)


class BankAccountChangeForm(forms.ModelForm):

    """
    A form for creating new users.
    Includes all the required fields, plus a repeated password.
    """
    error_messages = {
        'invalid_ifsc': _("Please enter a valid IFSC code."),
    }

    account_number2 = forms.CharField(
        label=_("Account Number confirmation"),
        help_text=_("Enter the same Account Number"
                    " as above, for verification."))

    def __init__(self, *args, **kwargs):
        """Init the form."""

        # import only if this form is called
        from django.forms.widgets import HiddenInput
        # overriding the account number field to display values
        account_number1 = kwargs['instance'].account_number

        if account_number1:
            initial = kwargs.get('initial', {})
            initial['account_number2'] = account_number1
            kwargs['initial'] = initial
        super(BankAccountChangeForm, self).__init__(*args, **kwargs)

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['account_number'].widget.attrs['readonly'] = True
            self.fields['account_number2'].widget = HiddenInput()

    def clean_ifsc_code(self):
        """
        IFSC Validation for bank accounts
        """
        reg_exp = re.compile(driver_const.IFSC_REGEX, re.UNICODE)
        ifsc_code = self.cleaned_data.get("ifsc_code")
        match = reg_exp.match(ifsc_code)

        if settings.COUNTRY_CODE_A2 == settings.DEFAULT_COUNTRY_CODE:
            if match is not None:
                return ifsc_code
            else:
                raise forms.ValidationError(
                    self.error_messages['invalid_ifsc'],
                    code='invalid_ifsc',
                )
        return ifsc_code

    class Meta:
        model = BankAccount
        exclude = ['account_number2', ]


class DriverAddressForm(forms.ModelForm):
    line1 = forms.CharField(label='Full Address')
    postcode = forms.CharField(label='Post/Zip-code')


class AlternateContactForm(forms.ModelForm):

    """
    A form for creating new users.
    Includes all the required fields, plus a repeated password.
    """

    alternate_number = forms.CharField(max_length=10, required=False,
                                       label=_("Alternate Contact Number"),
                                       validators=[validate_phone_number],
                                       widget=forms.TextInput(attrs={'style': 'width:36ch'}), )

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            alternate_contact = kwargs['instance']

            if alternate_contact and alternate_contact.phone_number:
                number = str(
                    alternate_contact.phone_number.national_number).zfill(10)
            else:
                number = None

            initial = {
                'alternate_number': number
            }
            kwargs['initial'] = initial
        super(AlternateContactForm, self).__init__(*args, **kwargs)

    class Meta:
        model = AlternateContact
        fields = '__all__'


def adjustment_date_check(payment, date, attribute):
    trip_window = payment.paymenttrip_set.aggregate(
        min_date=Min('trip__planned_start_time'),
        max_date=Max('trip__planned_start_time'))

    if trip_window and trip_window.get('min_date', None) and trip_window.get('max_date', None):
        trip_window_start = get_in_ist_timezone(
            trip_window.get('min_date'))
        trip_window_end = get_in_ist_timezone(trip_window.get('max_date'))
        if date:
            if date < trip_window_start.date() or \
                date > trip_window_end.date():
                raise ValidationError({
                    attribute:
                        (messages.ADJUSTMENT_DATE_SHOULD_BE_FALL_UNDER_TRIP_WINDOW)
                        })


class AddDriverAdjustmentInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AddDriverAdjustmentInlineForm, self).__init__(*args, **kwargs)
        if self.fields.get('category', None):
            self.fields['category'].required = True
        if self.fields.get('description', None):
            self.fields['description'].required = True

    def clean(self, *args, **kwargs):
        adjustment_datetime = self.cleaned_data.get('adjustment_datetime')
        payment = self.cleaned_data.get('payment')
        adjustment_date_check(payment, adjustment_datetime, 'adjustment_datetime')


class AddDriverCashPaidInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AddDriverCashPaidInlineForm, self).__init__(*args, **kwargs)
        if self.fields.get('description', None):
            self.fields['description'].required = True

    def clean(self, *args, **kwargs):
        cash_paid_datetime = self.cleaned_data.get('cash_paid_datetime')
        adjustment_date_check(self.instance.payment, cash_paid_datetime, 'cash_paid_datetime')


class DriverAdjustmentForm(forms.ModelForm):
    remarks = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(DriverAdjustmentForm, self).__init__(*args, **kwargs)
        if self.fields.get('category', None):
            self.fields['category'].required = True
        if self.fields.get('description', None):
            self.fields['description'].required = True

    def clean(self, *args, **kwargs):
        adjustment_datetime = self.cleaned_data.get('adjustment_datetime')
        adjustment_date_check(self.instance.payment, adjustment_datetime,
                              'adjustment_datetime')


class BackGroundVerificationForm(forms.ModelForm):

    document_type = forms.MultipleChoiceField(
        choices=BETTERPLACE_DOCUMENT_TYPE_CHOICES)

    def clean(self, *args, **kwargs):
        data = self.cleaned_data
        driver = data.get('driver')
        document_type = data.get('document_type')
        get_data_for_background_verification(driver, document_type)


class BankAccountActionForm(ActionForm):
    bank_account = CustomModelChoiceField(
        label=_("Target Bank Account"),
        queryset=BankAccount.objects.all(),
        required=False,
        field_names=['pk', 'account_name', 'account_number', 'ifsc_code']
    )


class DriverWellBeingFormset(BaseInlineFormSet):

    def save_new(self, form, commit=True):
        obj = super().save_new(form, commit=False)
        obj.modified_by = current_request().user

        if commit:
            event = DriverWellBeing.objects.filter(driver=obj.driver, event_type=obj.event_type).exclude(pk=obj.pk)
            if event.exists():
                raise ValidationError(
                    _('The event %s already exists, please edit the existing one' % str(event))
                )

            obj.save()
        return obj

    def save_existing(self, form, instance, commit=True):
        obj = super().save_new(form, commit=False)
        obj.modified_by = current_request().user

        if commit:
            event = DriverWellBeing.objects.filter(driver=obj.driver, event_type=obj.event_type).exclude(pk=obj.pk)
            if event.exists():
                raise ValidationError(
                    _('The event %s already exists, please edit the existing one' % str(event))
                )
            obj.save()
        return obj
