from blowhorn.driver.models import (
    DriverPaymentAdjustment,
    DriverCashPaid,
    DriverDocument,
)
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from django.urls import reverse
from blowhorn.driver.tasks import mailer
from django.conf import settings
from urllib.parse import urljoin
from blowhorn.driver.util import create_document_event, get_process_associate
from blowhorn.common.middleware import current_request


@receiver(post_save, sender=DriverDocument)
def document_event(sender, instance, created, **kwargs):
    if created:
        create_document_event(
            driver_document=instance,
            driver=instance.driver,
            created_by=current_request().user,
        )


@receiver(post_save, sender=DriverPaymentAdjustment)
def driver_payment_adjustment_email(sender, instance, created, **kwargs):
    print("Received signal", instance, created)

    if created:
        process_associate_id = get_process_associate(instance.payment)
        instance.action_owner_id = process_associate_id
        instance.save()

    recepients = [x.email for x in instance.payment.contract.supervisors.all()]

    # Added check. created_by is None when this is first called upon creation.
    if instance.created_by:
        recepients.append(instance.created_by.email)
    if instance.modified_by:
        recepients.append(instance.modified_by.email)

    url = urljoin(
        settings.HOST,
        reverse("admin:driver_driverpaymentadjustment_change", args=[instance.pk]),
    )
    message = """
Driver: %s
Contract: %s
Amount: %.2f
Category: %s
Description: %s
Status : %s

To Approve/Reject/Other Actions: Click Below

%s
""" % (
        instance.payment.driver,
        instance.payment.contract,
        instance.adjustment_amount,
        instance.category,
        instance.description,
        instance.status,
        url,
    )

    # IF the record was acutally edited and the status is still new,
    # it is better to show the status as edited to be more meaningful
    better_status = instance.status
    if not created and instance.status == instance.NEW:
        better_status = "Edited"

    subject = "%s Adjustment | %0.2f | %s | %s" % (
        better_status,
        instance.adjustment_amount,
        instance.payment.driver,
        instance.payment.contract,
    )
    # mailer.delay(subject, message, list(set(recepients)))


@receiver(post_save, sender=DriverCashPaid)
def driver_payment_cashpaid_email(sender, instance, created, **kwargs):
    print("Received signal", instance)
    recepients = [x.email for x in instance.payment.contract.supervisors.all()]

    # Added check. created_by is None when this is first called upon creation.
    if instance.created_by:
        recepients.append(instance.created_by.email)
    if instance.modified_by:
        recepients.append(instance.modified_by.email)

    url = urljoin(
        settings.HOST,
        reverse("admin:driver_driverpaymentadjustment_change", args=[instance.pk]),
    )
    message = """
Driver: %s
Contract: %s
Amount: %.2f
Description: %s
Status : %s

To Approve/Reject/Other Actions: Click Below
%s
""" % (
        instance.payment.driver,
        instance.payment.contract,
        instance.cash_paid,
        instance.description,
        instance.status,
        url,
    )

    better_status = instance.status
    if not created and instance.status == instance.NEW:
        better_status = "Edited"

    subject = "%s Cash Paid | %0.2f | %s | %s" % (
        better_status,
        instance.cash_paid,
        instance.payment.driver,
        instance.payment.contract,
    )
    # mailer.delay(subject, message, recepients)

