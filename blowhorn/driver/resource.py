from django.db import connection
from django.conf import settings

from import_export.widgets import ForeignKeyWidget
from blowhorn.oscar.core.loading import get_model
from import_export import resources, fields
from blowhorn.utils.functions import utc_to_ist

Contract = get_model('contract', 'Contract')
BuyRate = get_model('contract', 'BuyRate')
SellRate = get_model('contract', 'SellRate')
Driver = get_model('driver', 'Driver')
DriverPayment = get_model('driver', 'DriverPayment')
DriverPaymentAdjustment = get_model('driver', 'DriverPaymentAdjustment')
NonOperationPayment = get_model('driver', 'NonOperationPayment')
DriverActivityHistory = get_model('driver', 'DriverActivityHistory')


class DriverActivityHistoryResource(resources.ModelResource):

    driver_name = fields.Field(column_name="Driver",)

    order_number = fields.Field(column_name="Order",)

    geopoint = fields.Field(column_name="GeoPoints",)

    trip = fields.Field(column_name="Trip Number",)

    def dehydrate_driver_name(self, history):
        return history.driver.name

    def dehydrate_trip(self, history):
        if history.trip:
            return history.trip.trip_number
        return '-'

    def dehydrate_order_number(self, history):
        if history.order:
            return history.order.number
        return '-'

    def dehydrate_geopoint(self, history):
        if history and history.geopoint:
            lat = history.geopoint.coords[1]
            lon = history.geopoint.coords[0]
            latlon = '%s,%s' % (lat, lon)
            return latlon
        return ''

    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    class Meta:
        model = DriverActivityHistory
        fields = ('stop_str', 'event', 'gps_timestamp', 'created_time', 'device_timestamp',
                  'location_accuracy_meters', 'vehicle_speed_kmph')


# class DriverPaymentResource(resources.ModelResource):
#
#     driver_name = fields.Field(
#         column_name="Driver",
#     )
#
#     contract_name = fields.Field(attribute="contract",
#                                  column_name="Contract Name",
#                                  widget=ForeignKeyWidget(Contract,
#                                                          'name'))
#
#     contract_type = fields.Field(column_name='Contract Type')
#
#     vehicle = fields.Field(column_name="Vehicle")
#
#     vehicle_class = fields.Field(column_name="Vehicle Class")
#
#     vehicle_model = fields.Field(column_name="Vehicle Model")
#
#     hub = fields.Field(column_name="Hub")
#
#     city = fields.Field(column_name="City")
#
#     customer = fields.Field(column_name="Customer")
#
#     # customer_name = fields.Field(attribute="contract",
#     #                              column_name="customer",
#     #                              widget=ForeignKeyWidget(Contract,
#     #                                                      'customer'))
#
#     division = fields.Field(column_name="Division")
#     contingency = fields.Field(column_name="Has Contingency")
#     is_stale = fields.Field(column_name="Stale")
#     is_payment_settled = fields.Field(column_name="Payment Settled")
#
#     def dehydrate_driver_name(self, payment):
#         return payment.driver.name
#
#     def dehydrate_contract_type(self, payment):
#         if payment.contract:
#             return payment.contract.contract_type
#         return ''
#
#     def dehydrate_vehicle(self, payment):
#         paymenttrips = payment.paymenttrip_set.all()
#         vehicles = [pt.trip.vehicle for pt in paymenttrips]
#         return ', '.join(map(str, set(vehicles)))
#
#     def dehydrate_vehicle_class(self, payment):
#         paymenttrips = payment.paymenttrip_set.all()
#         vehicle_classes = [pt.trip.vehicle.vehicle_model.vehicle_class.commercial_classification
#                            for pt in paymenttrips if pt.trip.vehicle is not None]
#         return ', '.join(set(vehicle_classes))
#
#     def dehydrate_vehicle_model(self, payment):
#         paymenttrips = payment.paymenttrip_set.all()
#         vehicle_model = [
#             pt.trip.vehicle.vehicle_model.model_name for pt in paymenttrips if pt.trip.vehicle is not None]
#         return ', '.join(set(vehicle_model))
#
#     def dehydrate_hub(self, payment):
#         paymenttrips = payment.paymenttrip_set.all()
#         hubs = [pt.trip.hub for pt in paymenttrips if pt.trip.hub]
#         return ', '.join(map(str, set(hubs)))
#
#     def dehydrate_city(self, payment):
#         if payment.contract:
#             return payment.contract.city.name
#         return ''
#
#     def dehydrate_customer(self, payment):
#         if payment.contract and payment.contract.customer:
#             return payment.contract.customer.user.name
#         return ''
#
#     def dehydrate_division(self, payment):
#         if payment.contract and payment.contract.division:
#             return payment.contract.division.name
#         return ''
#
#     def dehydrate_contingency(self, payment):
#         #        return payment.contingency > 0
#         return bool([x for x in payment.paymenttrip_set.all()
#                      if x.trip.is_contingency])
#
#     def dehydrate_is_stale(self, payment):
#         return payment.is_stale
#
#     def dehydrate_is_payment_settled(self, payment):
#         if payment.status == APPROVED:
#             settled = "Yes"
#             if payment.driver.driverbalance and \
#                     payment.driver.driverbalance.amount > 0:
#                 settled = "No"
#                 sorted_ledger = sorted(
#                     payment.driver.driverledger_set.all(),
#                     key=lambda x: x.transaction_time, reverse=True)
#                 if sorted_ledger and \
#                         sorted_ledger[0].transaction_time > payment.modified_date:
#                     settled = "Yes"
#             return settled
#         return "No"
#
#     def before_export(self, queryset, *args, **kwargs):
#         print('Queries Count at begin : %d' % len(connection.queries))
#
#     def after_export(self, queryset, data, *args, **kwargs):
#         print('Queries Count at end : %d' % len(connection.queries))
#
#     class Meta:
#         model = DriverPayment
#         export_order = (
#             'id', 'contract_name', 'contract_type', 'status',
#             'driver_name', 'payment_type', 'vehicle_class', 'vehicle',
#             'vehicle_model', 'hub', 'city', 'customer', 'division',
#             'contingency', 'start_datetime', 'end_datetime',
#             'absent_days', 'leave_deduction', 'total_deductions',
#             'distance_payment', 'time_payment', 'total_shipment_amount',
#             'adjustment', 'gross_pay', 'cash_paid', 'net_pay'
#         )
#         exclude = ('driver', 'contract', 'buy_rate', 'sell_rate',
#                    'created_date', 'modified_by', 'calculation',
#                    'modified_date', 'buy_rate', 'created_at',
#                    'created_by', 'modified_at', 'calculations', 'can_approve')


class DriverAdjustmentResource(resources.ModelResource):
    driver_name = fields.Field(
        column_name="Driver",
    )

    contract_name = fields.Field(column_name="Contract Name")

    payment = fields.Field(column_name="Payment Reference")

    customer = fields.Field(attribute="contract",
                            column_name="customer",
                            widget=ForeignKeyWidget(Contract,
                                                    'customer'))

    contract_city = fields.Field(column_name="City")

    vehicle = fields.Field(column_name="Vehicle")

    vehicle_class = fields.Field(column_name="Vehicle Class")

    vehicle_model = fields.Field(column_name="Vehicle Model")

    def dehydrate_driver_name(self, adjustment):
        if adjustment and adjustment.payment:
            return adjustment.payment.driver
        return ''

    def dehydrate_contract_name(self, adjustment):
        if adjustment and adjustment.payment:
            return adjustment.payment.contract
        return ''

    def dehydrate_payment(self, adjustment):
        if adjustment:
            return adjustment.payment
        return ''

    def dehydrate_customer(self, adjustment):
        if adjustment and adjustment.payment:
            return adjustment.payment.contract.customer
        return ''

    def dehydrate_contract_city(self, adjustment):
        if adjustment and adjustment.payment:
            return adjustment.payment.contract.city
        return ''

    def dehydrate_vehicle(self, adjustment):
        paymenttrips = adjustment.payment.paymenttrip_set.all()
        vehicles = [pt.trip.vehicle for pt in paymenttrips]
        return ', '.join(map(str, set(vehicles)))

    def dehydrate_vehicle_class(self, adjustment):
        paymenttrips = adjustment.payment.paymenttrip_set.all()
        vehicle_classes = [pt.trip.vehicle.vehicle_model.vehicle_class.commercial_classification
                           for pt in paymenttrips]
        return ', '.join(set(vehicle_classes))

    def dehydrate_vehicle_model(self, adjustment):
        paymenttrips = adjustment.payment.paymenttrip_set.all()
        vehicle_model = [
            pt.trip.vehicle.vehicle_model.model_name for pt in paymenttrips]
        return ', '.join(set(vehicle_model))

    class Meta:
        model = DriverPaymentAdjustment
        export_order = (
            'id', 'status', 'driver_name', 'contract_name', 'contract_city',
            'customer', 'payment', 'vehicle_class', 'vehicle', 'vehicle_model',
            'adjustment_amount'

        )
        exclude = (
            'created_date', 'created_by', 'modified_date', 'modified_by',
            'driver', 'contract', 'payment')


class NonOperationPaymentResource(resources.ModelResource):
    driver_name = fields.Field(
        column_name="Driver",
    )

    city = fields.Field(
        column_name="City",
    )

    customer_name = fields.Field(
        column_name="Business User",
    )

    division_name = fields.Field(
        column_name="Division",
    )

    payment_from = fields.Field(
        column_name="Payment From",
    )

    payment_until = fields.Field(
        column_name="Payment Untill",
    )

    purpose = fields.Field(
        column_name="Payment Purpose",
    )

    def dehydrate_driver_name(self, payment):
        if payment:
            return payment.driver
        return ''

    def dehydrate_city(self, payment):
        city = payment.city
        return city

    def dehydrate_customer_name(self, payment):
        customer = payment.customer
        return customer

    def dehydrate_division_name(self, payment):
        division_name = payment.division.name if payment.division else ''
        return division_name

    def dehydrate_payment_from(self, payment):
        return payment.start_datetime

    def dehydrate_payment_until(self, payment):
        return payment.end_datetime

    def dehydrate_purpose(self, payment):
        return payment.purpose

    class Meta:
        model = NonOperationPayment
        export_order = ('driver_name', 'customer_name', 'division_name', 'city',
                        'payment_from', 'payment_until', 'purpose', 'amount')
        exclude = ('id', 'driver', 'customer', 'created_date', 'created_by',
                   'modified_date', 'modified_by', 'division', 'start_datetime',
                   'end_datetime', 'purpose',)


class DriverResource(resources.ModelResource):
    driver_id = fields.Field(
        column_name="ID"
    )

    driver_name = fields.Field(
        column_name="Driver",
    )

    vehicle = fields.Field(
        column_name="Vehicle",
    )

    operating_city = fields.Field(
        column_name="Operating City",
    )

    status = fields.Field(
        column_name="Status",
    )

    mobile_number = fields.Field(column_name="Mobile")

    contracts = fields.Field(column_name="Contracts")

    vehicle_class = fields.Field(column_name="Vehicle Class")

    hub = fields.Field(column_name="Hub")

    created_date = fields.Field(column_name="Created Date")

    def dehydrate_driver_id(self, driver):
        return driver.id

    def dehydrate_driver_name(self, driver):
        return driver.name

    def dehydrate_vehicle(self, driver):
        return driver.driver_vehicle

    def dehydrate_operating_city(self, driver):
        return driver.operating_city

    def dehydrate_status(self, driver):
        return driver.status

    def dehydrate_mobile_number(self, driver):
        return driver.user.phone_number

    def dehydrate_contracts(self, driver):
        contracts = [x.contract for x in driver.resourceallocation_set.all()]
        return ', '.join(map(str, contracts))

    def dehydrate_vehicle_class(self, driver):
        if driver.vehicles:
            return ', '.join([str(vc.vehicle_model.vehicle_class) for vc in driver.vehicles.all()])
        return ''

    def dehydrate_hub(self, driver):
        return ', '.join([str(x.hub) for x in driver.resourceallocation_set.all()])

    def dehydrate_created_date(self, driver):
        created_date = ''
        if driver.created_date:
            created_date = utc_to_ist(driver.created_date).strftime(
                settings.ADMIN_DATETIME_FORMAT)

        return created_date

    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    class Meta:
        model = Driver
        fields = ('driver_id', 'driver_name', 'vehicle', 'vehicle_class', 'operating_city', 'mobile_number',
                  'status', 'contracts', 'hub', 'created_by', 'created_date')
        export_order = ('driver_id', 'driver_name', 'vehicle', 'vehicle_class', 'operating_city', 'mobile_number',
                        'status', 'contracts', 'hub', 'created_by', 'created_date')
