# Django and System libraries
import json
import logging
import datetime
import operator
import os
import decimal
import requests
from functools import reduce
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from django.utils.translation import gettext_lazy as _


from django.http import HttpResponse
from django.conf import settings
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.contrib.gis.measure import Distance
from django.contrib.gis.db.models.functions import Distance as Dt

from django import db
from django.db.models import Q, Prefetch, Sum
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils import timezone as tzzone
from django.db.models.query import EmptyQuerySet

# 3rd party libraries
from rest_framework import generics, views
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.parsers import MultiPartParser, FormParser
from pytz import timezone
import phonenumbers

# blowhorn imports
from config.settings import status_pipelines as StatusPipeline
from .constants import AVERAGE_VEHICLE_SPEED_KMPH
from .serializers import (
    DriverListSerializer,
    DriverAppSerializer,
    DriverContractSerializer,
    DriverDocumentSerializer,
    DriverActivateSerializer,
    DriverSerializer,
    DriverAutofillSerializer,
)
from .models import (
    Driver,
    DriverVehicleMap,
    DriverPreferredLocation,
    BackGroundVerification,
    DriverDeduction,
    DriverLedger,
    DriverLoanDetails,
    DriverPayment, DriverActivity, DriverConstants, DriverWellBeing, EventType
)
from blowhorn.address import utils as address_utils
from blowhorn.driver.util import (
    get_detailed_data,
    get_user_for_driver,
    get_background_verification_status
)
from blowhorn.common.utils import export_lists_to_spreadsheet
from blowhorn.common.helper import CommonHelper
from blowhorn.common import serializers as common_serializer
from blowhorn.contract.models import ResourceAllocation
from blowhorn.driver import constants as DriverConst
from blowhorn.vehicle.utils import add_vehicle, add_vendor
from blowhorn.vehicle.models import Vehicle
from blowhorn.address.models import City, Slots
from blowhorn.document.models import DocumentType
from blowhorn.utils.functions import ist_to_utc
from blowhorn.document.constants import VERIFICATION_PENDING, REJECT
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.driver.permissions import APIVerifiedCustomer
from blowhorn.utils.functions import utc_to_ist
from blowhorn.order.models import Order, OrderConstants
from blowhorn.customer.models import get_partner_from_api_key, Partner, Customer
from blowhorn.contract.constants import CONTRACT_TYPE_FIXED
from ..address.helpers.location_helper import LocationHelper
from ..address.utils import get_city_from_pincode, get_city_from_geopoint
from ..common.middleware import CsrfExemptSessionAuthentication
from ..integration_apps.sub_models.sub_models import PartnerLeadStatus
from ..trip.models import Trip

DATE_FORMAT = '%d-%b-%Y'
VEHICLE_EXIST = 'Vehicle with corresponding registration number already exists'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverDetailView(LoginRequiredMixin, DetailView):
    model = Driver
    # These next two lines tell the view to index lookups by drivername
    slug_field = 'drivername'
    slug_url_kwarg = 'drivername'


class DriverRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('drivers:detail',
                       kwargs={'drivername': self.request.driver.drivername})


class DriverUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['name', ]
    model = Driver

    # send the driver back to their own page after a successful update

    def get_success_url(self):
        return reverse('driver:detail',
                       kwargs={'drivername': self.request.driver.drivername})

    def get_object(self):
        # Only get the Driver record for the driver making the request
        return Driver.objects.get(user__email=self.request.user.email)


class DriverListView(LoginRequiredMixin, ListView):
    model = Driver
    # These next two lines tell the view to index lookups by drivername
    slug_field = 'drivername'
    slug_url_kwarg = 'drivername'


class DriversList(views.APIView):
    permission_classes = (IsAuthenticated,)
    # authentication_classes = (CsrfExemptSessionAuthentication,
    #                           BasicAuthentication)

    def post(self, request):
        """
        For displaying the filtered list of the drivers
        in a paginated manner
        """

        logging.info(
            "Following is the request data for driver filter :"
            " " + str(request.body))

        query_params = json.loads(request.body)
        filter_data = {}

        # TODO: To be uncommented while working with postman
        # query_params = request.data

        logging.info(
            "Following is the query params for driver filter :"
            " " + str(query_params))

        city = City.objects.filter(
            name=query_params.get('operating_city')).first()

        if not city:
            return Response(status=status.HTTP_404_NOT_FOUND,
                            data='No driver details found for this city')

        filter_data['operating_city'] = city

        if query_params.get('timestamp', ''):
            try:
                timestamp = datetime.datetime.strptime(
                    query_params.get('timestamp'), '%Y-%m-%d %H:%M:%S')
                last_modified = ist_to_utc(timestamp)
                last_modified_utc = last_modified.replace(
                    tzinfo=timezone('UTC'))
            except Exception as e:
                return Response(
                    status=status.HTTP_404_NOT_FOUND,
                    data='Provide timestamp in correct format')

            filter_data['modified_date__gte'] = last_modified

        driver_list = Driver.objects.filter(
            **filter_data).prefetch_related('driverpreferredlocation_set').order_by('status')

        if driver_list:
            page_to_be_displayed = query_params.get('next') if query_params.get(
                'next', '') else DriverConst.PAGE_TO_BE_DISPLAYED
            number_of_entry = DriverConst.NUMBER_OF_ENTRY_IN_PAGE

            logging.info(
                'Page to be displayed for the pagination purpose'
                ': %s' % page_to_be_displayed)
            logging.info(
                'Number of entry per page for pagination purpose'
                ': %s' % number_of_entry)

            serialized_data = common_serializer.PaginatedSerializer(
                queryset=driver_list, num=number_of_entry,
                page=page_to_be_displayed,
                serializer_method=DriverListSerializer
            )

            return Response(status=status.HTTP_200_OK,
                            data=serialized_data.data)

        else:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data='No driver found')


class DriverAdd(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    parser_classes = (MultiPartParser, FormParser)

    # authentication_classes = (CsrfExemptSessionAuthentication,
    #                           BasicAuthentication)

    def post(self, request):
        """
        For adding a new Driver
        """

        # logging.info("Request from the app" + str(request.data.dict()))
        # data = request.data.dict()

        try:
            data = json.loads(request.body)
            driver_details = data.get('driver_details', {})
            logging.info("Request from the app : " + str(request.body))
        except:
            data = request.data.dict()
            driver_details = json.loads(data.get('driver_details', '{}'))
            logging.info(
                "Request from the app dict : " + str(request.data.dict()))

        # TODO: To be uncommented while working with postman
        # data = request.data

        logging.info("Following are the params for driver : %s",
                     driver_details)

        if not driver_details:
            logging.info("Driver details not found")
            response_dict = {
                "status": False,
                "message": "Driver details not found"
            }
            return Response(data=response_dict,
                            status=status.HTTP_400_BAD_REQUEST,
                            content_type='text/html; charset=utf-8')

        detailed_data = get_detailed_data(params=driver_details)

        driver_data = detailed_data.get('driver_data', {})
        vehicle_data = detailed_data.get('vehicle_data', {})
        user_data = detailed_data.get('user_data', {})
        vendor_data = detailed_data.get('vendor_data', {})

        sid = db.transaction.savepoint()

        logger.info('city: %s', driver_data['operating_city'])

        city = City.objects.filter(
            name=driver_data.get('operating_city')).first()

        if city:
            driver_data['operating_city'] = city.id

        try:
            phone = phonenumbers.parse(driver_data.get('phone', ''),
                                       settings.COUNTRY_CODE_A2)
        except:
            phone = ''

        filter_data = {
            'name': driver_data.get('name', ''),
            'driver_vehicle': vehicle_data.get('registration_certificate_number', ''),
            'user__phone_number': phone
        }

        driver = Driver.objects.filter(**filter_data).first()

        if not driver or driver_details.get('isupdating'):
            if request.META.get('HTTP_DEVICE_TYPE', '') in settings.USER_CLIENT_TYPES:
                driver_data['source'] = DriverConst.USER_ACCESS_APP

            if request.user:
                driver_data['created_by'] = request.user
                driver_data['modified_by'] = request.user

            if driver_details.get('isupdating'):
                user_data['user_id'] = driver.user_id

            try:
                user = get_user_for_driver(user_data)
            except db.IntegrityError:
                logging.info("User with the Phone number already exist")

                return Response(data="Driver Phone number already exist",
                                status=status.HTTP_400_BAD_REQUEST,
                                content_type='text/html; charset=utf-8')

            driver_data['user'] = user.id

            try:
                vehicle = add_vehicle(
                    vehicle_params=vehicle_data, is_update=driver_details.get('isupdating'))
                if not vehicle:
                    logging.info(VEHICLE_EXIST)
                    db.transaction.savepoint_rollback(sid)

                    return Response(data=VEHICLE_EXIST,
                                    status=status.HTTP_400_BAD_REQUEST)
            except db.IntegrityError as e:
                logging.info(
                    "Following error occured while creating vehicle : " + str(e))
                db.transaction.savepoint_rollback(sid)

                return Response(data="Please Enter valid vehicle details",
                                status=status.HTTP_400_BAD_REQUEST)

            if driver_details.get('isupdating'):
                serializer = DriverActivateSerializer(
                    instance=driver, data=driver_data)
            else:
                serializer = DriverAppSerializer(data=driver_data)

            if serializer.is_valid():
                logging.info(
                    "Going to the app serializer with data: %s", driver_data)
                try:
                    driver = serializer.save(operating_city=city, user=user)

                except db.IntegrityError as e:
                    logging.info(
                        "Following error occured while creating vehicle : %s", e)
                    response_dict = {
                        "status": False,
                        "message": e.args[0]
                    }
                    return Response(data=response_dict,
                                    status=status.HTTP_400_BAD_REQUEST)

                if not driver.driver_vehicle or (driver.driver_vehicle != vehicle.registration_certificate_number):
                    DriverVehicleMap.objects.create(
                        driver=driver, vehicle=vehicle)

                if driver_details.get('address_model', []):
                    preferred_location_data = []
                    DriverPreferredLocation.objects.filter(
                        driver=driver).delete()
                    for cord_data in driver_details['address_model']:
                        if isinstance(cord_data, dict) and cord_data.get('coordinates', {}):
                            latitude = cord_data.get(
                                'coordinates').get('latitude', None)
                            longitude = cord_data.get(
                                'coordinates').get('longitude', None)

                            logger.info('Latitude:- %s longitude: %s',
                                        latitude, longitude)

                            if latitude and longitude:
                                geopoint = CommonHelper.get_geopoint_from_latlong(
                                    latitude, longitude)

                                preferred_location_data.append(DriverPreferredLocation(
                                    geopoint=geopoint, driver_id=driver.id))

                    if preferred_location_data:
                        DriverPreferredLocation.objects.bulk_create(
                            preferred_location_data)

                if not driver_details.get('driver_activation', False):
                    response_dict = {'driver_id': driver.id,
                                     'vehicle_id': vehicle.id}

                    return Response(status=status.HTTP_200_OK,
                                    data=response_dict)
            else:
                db.transaction.savepoint_rollback(sid)
                logging.info(
                    "This is the serializer error : %s", serializer.errors)
                return Response(data=serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            if not driver_details.get('driver_activation', False):
                logging.info(
                    "Driver activation details needed")
                db.transaction.savepoint_rollback(sid)
                return Response(data="Driver already exists. Needs to be activated",
                                status=status.HTTP_400_BAD_REQUEST)

        if driver_details.get('driver_activation', False):
            if request.user:
                driver_data['modified_by'] = request.user

            if not driver_data.get('own_vehicle', False):
                try:
                    vendor = add_vendor(vendor_data)
                except (db.IntegrityError, ValidationError) as e:
                    logging.info(
                        "Following error occured while creating vendor : " + str(e))
                    db.transaction.savepoint_rollback(sid)

                    return Response(data="Please Enter valid vendor details",
                                    status=status.HTTP_400_BAD_REQUEST)

                driver_data['owner_details'] = vendor.id

            if driver_data.get('driving_license_number', '') == driver_data.get('phone', ''):
                logging.error('Driving License Number field is empty')
                db.transaction.savepoint_rollback(sid)
                return Response(data="Enter a valid driving license number",
                                status=status.HTTP_400_BAD_REQUEST)

            if city:
                driver_data['operating_city'] = city.id

            driver_data['user'] = driver.user.id
            # driver_data.pop('phone')

            serializer = DriverActivateSerializer(
                instance=driver, data=driver_data)

            if serializer.is_valid():
                logging.info(
                    "Going to the activate serializer with data: %s", driver_data)
                try:
                    driver = serializer.save()

                except db.IntegrityError as e:
                    logging.info(
                        "Following error occured while activating driver : %s", e)
                    response_dict = {
                        "status": False,
                        "message": e.args[0]
                    }

                    return Response(data=e.args[0],
                                    status=status.HTTP_400_BAD_REQUEST)

            else:
                logging.info(
                    "Error occur in serializer : %s ", serializer.errors)
                db.transaction.savepoint_rollback(sid)
                return Response(data=serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)

            return Response(status=status.HTTP_200_OK,
                            data='Driver Details Added')


class DriverDocumentUpload(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in driver document add : " + str(request.data.dict()))

        documents = request.data.dict()
        driver_id = ''
        doc_status = {}

        if not documents:
            logging.info(
                "No documents where provided for the verification of driver")
            return Response(data="Please upload the documents",
                            status=status.HTTP_400_BAD_REQUEST)

        if documents.get('document_status', []):
            document_status = json.loads(documents.pop('document_status'))

            for statuses in document_status:
                driver_id = driver_id if driver_id else statuses.get(
                    'driver_id', '')

                if statuses.get('document_type', ''):
                    doc_status[statuses['document_type']] = statuses.get(
                        'document_status', '')

        if not driver_id:
            logging.info(
                "Documents not added for the activatioon of driver")
            return Response(data="Documents for activation of Driver is needed",
                            status=status.HTTP_400_BAD_REQUEST)

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                logging.info(
                    "Invalid document type is provided")
                return Response(data="Provide the valid document type",
                                status=status.HTTP_400_BAD_REQUEST)

            document_data = {
                'file': documents[key],
                'document_type': doc_type.id,
                'driver': driver_id,
                'status': doc_status[key] if doc_status.get(key, '') else VERIFICATION_PENDING
            }

            document_serializer = DriverDocumentSerializer(
                data=document_data)
            if document_serializer.is_valid():
                document_serializer.save(
                    created_by=request.user, modified_by=request.user)
            else:
                logging.info(document_serializer.errors)
                return Response(document_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='Driver Details Added')


class DriverFilter(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    # authentication_classes = (CsrfExemptSessionAuthentication,
    #                           BasicAuthentication)

    def post(self, request):
        logging.info(
            "Following is the request data for driver filter :"
            " " + str(request.body))

        query_params = json.loads(request.body)

        # TODO: To be uncommented while working with postman
        # query_params = request.data

        logging.info(
            "Following is the query params for driver filter :"
            " " + str(query_params))

        filter_data = {}

        address_model = query_params.get('address_model', {})
        working_preferences = query_params.get('working_preferences', [])
        vehicle_model = query_params.get('vehicle_model', '')

        latitude = address_model.get('latitude', None)
        longitude = address_model.get('longitude', None)
        geopoint = CommonHelper.get_geopoint_from_latlong(latitude, longitude)

        city = address_utils.get_city_from_latlong(latitude, longitude)

        if geopoint:
            filter_data['preferred_location_geopoint__distance_lt'] = (
                geopoint, Distance(km=DriverConst.SEARCH_RADIUS_IN_KM))

        # if address_model.get('place', ''):
        #     filter_data['preferred_location_name'] = address_model['place']

        if city:
            filter_data['operating_city'] = city

        if working_preferences:
            filter_data['working_preferences__overlap'] = working_preferences

            if list(
                    set(
                        DriverConst.DISPLAY_EXTRA_OPTIONS_FOR_PREFERENCES
                    ).intersection(set(working_preferences))):
                if query_params.get('can_drive_and_deliver', False):
                    filter_data['can_drive_and_deliver'] = query_params.get(
                        'can_drive_and_deliver', False)
                if query_params.get('can_read_english', False):
                    filter_data['can_read_english'] = query_params.get(
                        'can_read_english', False)
                if query_params.get('can_source_additional_labour', False):
                    filter_data[
                        'can_source_additional_labour'] = query_params.get(
                        'can_source_additional_labour', False)

        if query_params.get('work_status', '') and query_params[
                'work_status'] != 'ALL':
            filter_data['work_status'] = query_params.get('work_status')

        if query_params.get('status', ''):
            filter_data['status'] = query_params['status']

        if vehicle_model:
            filter_data['vehicles__vehicle_model__model_name'] = vehicle_model

        # To exclude all the drivers who are in some contracts
        allocated_drivers = ResourceAllocation.objects.all().values_list(
            'drivers', flat=True)

        logging.info(
            "Following is the query params for driver filter just before :"
            " " + str(filter_data))

        driver = Driver.objects.filter(
            **filter_data).exclude(id__in=allocated_drivers).order_by('status')
        logging.info("Following are the drivers : " + str(driver))

        if driver:
            page_to_be_displayed = query_params['next'] if query_params.get(
                'next', '') else DriverConst.PAGE_TO_BE_DISPLAYED

            number_of_entry = DriverConst.NUMBER_OF_ENTRY_IN_PAGE

            logging.info(
                "Page to be displayed for the pagination "
                "purpose: %s" % page_to_be_displayed)
            logging.info(
                "Number of entry per page for pagination purpose "
                ": %s" % number_of_entry)

            serialized_data = common_serializer.PaginatedSerializer(
                queryset=driver, num=number_of_entry,
                page=page_to_be_displayed,
                serializer_method=DriverListSerializer
            )

            return Response(status=status.HTTP_200_OK,
                            data=serialized_data.data)

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='No driver found')


class DriverForContractFilter(views.APIView):
    """
    View to fetch list of drivers which can be assigned to a given order
    """

    permission_classes = (IsAuthenticated, IsBlowhornStaff)

    def get(self, request):
        params = request.GET

        try:
            order = Order.objects.filter(
                id=params.get('order_id')).select_related(
                'customer_contract', 'vehicle_class_preference').first()
            vehicle_class = order.vehicle_class_preference
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Invalid order id')

        if order:
            if not vehicle_class:
                _dict = dict(status=StatusPipeline.ACTIVE,
                             vehicles__vehicle_model__vehicle_class__in= \
                                 order.customer_contract.vehicle_classes.all(),
                             vehicles__body_type__in=order.customer_contract.body_types.all(),
                             operating_city_id=order.city_id)
            else:
                _dict = dict(status=StatusPipeline.ACTIVE,
                             vehicles__vehicle_model__vehicle_class=vehicle_class,
                             vehicles__body_type__in=order.customer_contract.body_types.all(),
                             operating_city_id=order.city_id)

            drivers = Driver.objects.prefetch_related(
                'vehicles', 'vehicles__vehicle_model', 'vehicles__vehicle_model__vehicle_class'
            ).filter(
                **_dict
            ).distinct('id', 'is_marketplace_driver').order_by('-is_marketplace_driver')

            if drivers.exists():
                driver_serializer = DriverContractSerializer(drivers, many=True)
                return Response(status=status.HTTP_200_OK,
                                data=driver_serializer.data)
        return Response(status=status.HTTP_404_NOT_FOUND, data='No active driver found')


class BackGroundVerificationView(views.APIView):
    """
        For creating a driver profile in the BetterPlace's db for verification pupose
        and get the response containing the reference id of the newly created driver and create
        a record for the background verification for the particular driver at our end
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        create_profile_url = getattr(
            settings, 'BETTERPLACE_ENDPOINT_CREATE_PROFILE', None)

        api_key = getattr(settings, 'BETTERPLACE_API_KEY', None)

        input_data = request.data

        headers = {"Content-Type": "application/json", "ApiKey": api_key}

        logging.info(
            'This is the input data for the create profile of the betterplace : %s' % (json.dumps(input_data), ))

        logging.info(
            'This is the url for the create profile of the betterplace : %s' % (create_profile_url, ))

        try:
            response = requests.post(
                create_profile_url, data=json.dumps(input_data), headers=headers)
        except:
            response = None

        logging.info('Below is the response %s' % (response, ))

        if response and response.status_code == 202:
            logging.info('This is the response status : %s' %
                         response.status_code)
            logging.info('This is the response content : %s' % response.json())

            try:
                phone = phonenumbers.parse(input_data.get('mobile', ''),
                                           settings.COUNTRY_CODE_A2)
            except:
                phone = ''

            driver_data = {
                'name': input_data.get('fName', ''),
                'user__phone_number': phone
            }

            try:
                driver = Driver.objects.get(**driver_data)
            except:
                driver = None

            response_data = response.json()
            reference_id = response_data.get(
                'data', {}).get('referenceId', None)

            BackGroundVerification.objects.create(
                driver=driver, reference_id=reference_id)

            logging.info(
                'Background verification record generated for the driver')

        return Response(data=response)


class BGVProfileStatus(views.APIView):
    """
        Getting the status of the profile's created for verification
    """
    permission_classes = (APIVerifiedCustomer,)

    def post(self, request):
        profile_status = json.loads(request.body)
        logging.info("This is the request for profile status : %s " %
                     profile_status)

        if not profile_status.get('referenceId'):
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="No reference ID provided")

        # if not profile_status.get('newStatus') == StatusPipeline.VERIFIED.upper():
        #     return Response(status=status.HTTP_400_BAD_REQUEST,
        #                     data="New status not in verified state")

        reference_id = profile_status.get('referenceId')
        bgv_obj = BackGroundVerification.objects.filter(
            reference_id=reference_id)

        if not bgv_obj:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="Invalid Reference ID")

        # try:
        get_background_verification_status(bgv_obj, reference_id)
        # except Exception as e:
        # return Response(status=status.HTTP_400_BAD_REQUEST, data=e.args[0])

        return Response(status=status.HTTP_200_OK, data="Verified data stored")


def _get_eligibility_criteria(partner):

    if not hasattr(partner, 'drivereligibility'):
        return None

    driver_eligibility = partner.drivereligibility
    filter_data = {}
    if driver_eligibility.dp_start_month >0:
        filter_data = {'driverpayment__start_datetime__lte': tzzone.now() - relativedelta(months=driver_eligibility.dp_start_month)}
    criteria_map = {
        'status': 'status',
        'own_vehicle': 'own_vehicle'

    }

    if driver_eligibility.is_fixed_driver:
        filter_data.update({'resourceallocation__contract__contract_type': CONTRACT_TYPE_FIXED})

    allowed_status = driver_eligibility.statuses.values_list('status', flat=True)

    if not isinstance(allowed_status, EmptyQuerySet):
        filter_data.update({'status__in': allowed_status})

    criteria_key = partner.drivereligibility._meta.get_fields()
    for key in criteria_key:
        value = getattr(driver_eligibility, key.name, None)
        criteria_key_map = criteria_map.get(key.name)
        if criteria_key_map and value:
            filter_data.update({str(criteria_map.get(key.name)): value})

    return filter_data


class DriverInformation(generics.ListAPIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        data = request.GET
        driverId = data.get('driverId', None)
        try:
            driverId = int(driverId)
        except:
            _out = {
                'status': 'FAIL',
                'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)
        mobile = data.get('mobile', None)
        partner = get_partner_from_api_key(api_key)

        try:
            driverId = int(driverId)
        except:
            _out = {
                    'status': 'FAIL',
                    'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not partner:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not mobile and not driverId:
            _out = {
                'status': 'FAIL',
                'message': 'mobile or driverId is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)
        if driverId:
            data = {'id': driverId}
        elif mobile:
            email = str(mobile) +'@'+  settings.DEFAULT_DOMAINS.get('driver')
            data = {'user__email': email}

        filter_data = {"id": driverId}

        elig_query = _get_eligibility_criteria(partner)
        if elig_query:
            filter_data.update(elig_query)

        driver = Driver.objects.filter(**filter_data)
        driver = driver.select_related('permanent_address', 'current_address', 'bank_account').first()

        if driver:
            DriverLoanDetails.objects.create(driver=driver, partner=partner, is_driver_detail=True)
            permanent_address = driver.permanent_address
            current_address = driver.current_address
            bank_info = driver.bank_account
            response = {
                'name': driver.name,
                'gender':driver.gender,
                'DOB': driver.date_of_birth,
                'joining_date': str(driver.active_from),
                'contact_no': str(driver.user.phone_number) if driver.user else '',
                'father_name': driver.father_name,
                'marital_status': driver.marital_status,
                'educational_detail': driver.educational_qualifications,
                'vehicle_no': driver.driver_vehicle,
                'license': driver.driving_license_number,
                'PAN': driver.pan,
                'owner': "Yes" if driver.own_vehicle else "No",
                "account_info":{
                    'name': bank_info.account_name if bank_info else '' ,
                    'account_no': bank_info.account_number if bank_info else '' ,
                    "ifsc_code": bank_info.ifsc_code if bank_info else ''
                }
            }
            # if permanent_address:
            response.update({
                'permanent_address':{
                    'address': permanent_address.line1 if permanent_address else '',
                    'city': permanent_address.line4 if permanent_address else '',
                    'state': permanent_address.state if permanent_address else '',
                    'postcode': permanent_address.postcode if permanent_address else '',
                    'country': str(permanent_address.country) if permanent_address else '',
                    'complete_address': permanent_address.search_text if permanent_address else ''

                }
            })
            # if current_address:
            response.update({
                'current_address':{
                    'address': current_address.line1 if current_address else '',
                    'city': current_address.line4 if current_address else '',
                    'state': current_address.state if current_address else '',
                    'postcode': current_address.postcode if current_address else '',
                    'country': str(current_address.country) if current_address else '',
                    'complete_address': current_address.search_text if current_address else ''

                }
            })
            _out = {
                'status': 'SUCCESS',
                'message': response
            }
            return Response(status=status.HTTP_200_OK, data=_out)
        _out = {
            'status': 'FAIL',
            'message': ' Driver not eligible for advance salary'
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)


class DriverBalance(generics.ListAPIView):

    permission_classes = (AllowAny,)

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        data = request.GET
        driverId = data.get('driverId', None)

        try:
            driverId = int(driverId)
        except:
            _out = {
                'status': 'FAIL',
                'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        mobile = data.get('mobile', None)
        partner = get_partner_from_api_key(api_key)
        try:
            driverId = int(driverId)
        except:
            _out = {
                    'status': 'FAIL',
                    'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not partner:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not mobile and not driverId:
            _out = {
                'status': 'FAIL',
                'message': 'mobile or driverId is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if driverId:
            data = {'id': driverId}
        elif mobile:
            email = str(mobile) +'@'+  settings.DEFAULT_DOMAINS.get('driver')
            data = {'user__email': email}

        filter_data = {"id": driverId}

        elig_query = _get_eligibility_criteria(partner)
        if elig_query:
            filter_data.update(elig_query)

        driver = Driver.objects.filter(**filter_data).first()

        if driver:
            DriverLoanDetails.objects.create(driver_id=driverId, partner=partner, is_balance_enquiry=True)
            balance = driver.driverbalance if hasattr(driver, 'driverbalance') else None

            unapproved_bal = DriverPayment.objects.filter(driver=driver, status=DriverConst.UNAPPROVED). \
                aggregate(Sum('net_pay'))['net_pay__sum'] or 0
            response = {
                'balance_amount': (float(balance.amount if balance else 0) + float(unapproved_bal))
            }

            _out = {
                'status': 'SUCCESS',
                'message': response
            }
            return Response(status=status.HTTP_200_OK, data=_out)
        _out = {
            'status': 'FAIL',
            'message': 'Driver Details Not Found, you can use drivereligilibity api to get detailed information'
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)


class DriverEligibility(generics.ListAPIView):

    permission_classes = (AllowAny,)

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        data = request.GET
        driverId = data.get('driverId', None)
        mobile = data.get('mobile', None)
        partner_id = data.get('client_id', None)
        partner = get_partner_from_api_key(api_key)

        if not driverId:
            driver = request.user.driver if hasattr(request.user, 'driver') else None
            driverId = driver.id if driver else None
            if driver and partner_id and not partner:
                partner = Partner.objects.filter(id=partner_id).first()
        # status = 'FAIL'
        error = ''


        try:
            driverId = int(driverId)
        except:
            _out = {
                    'status': 'FAIL',
                    'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not partner:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not mobile and not driverId:
            _out = {
                'status': 'FAIL',
                'message': 'mobile or driverId is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if driverId:
            driver = Driver.objects.filter(id=driverId).first()

            if not driver:
                error.append('Invalid driver Id %s' % (driverId))
            if driver and hasattr(partner, 'drivereligibility'):
                driver_eligibility = partner.drivereligibility
                start = tzzone.now() - relativedelta(months=driver_eligibility.dp_start_month)

                if not DriverPayment.objects.filter(start_datetime__lte=start).exists():
                    error = 'Driver does not have payments older than %s months' % (driver_eligibility.dp_start_month)
                if not error and driver_eligibility.own_vehicle and not driver.own_vehicle:
                    error = 'Driver is not a owner'
                if not error and driver.status not in driver_eligibility.statuses.values_list('status', flat=True):
                    error = 'Driver is in %s status' % (driver.status)
                if not error and driver_eligibility.is_fixed_driver and not ResourceAllocation.objects.filter(drivers__in=[driverId]).exists():
                    error = 'Not a fixed driver'
                if not error and driver.operating_city not in driver_eligibility.cities.all():
                    error = 'Operating city of driver is %s' % driver.operating_city
                service_availed = PartnerLeadStatus.objects.filter(driver=driver).exclude(loan_status='repaid').exists()

                if service_availed:
                    error = 'You Applied Already'

                if error:
                    _out = {
                        'status': 'FAIL',
                        'message': error
                    }
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)
                _out = {
                    'status': 'SUCCESS',
                    'message': 'Driver is Eligible as per your criteria' if not partner_id else "You are Eligible"
                }
                return Response(status=status.HTTP_200_OK, data=_out)

    def post(self, request):
        self.get(request)


class DriverDeductionview(generics.ListAPIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        data = request.data
        driverId = data.get('driverId', None)

        try:
            driverId = int(driverId)
        except:
            _out = {
                'status': 'FAIL',
                'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        mobile = data.get('mobile', None)
        referenceUuid = data.get('referenceUuid', None)
        transactionId = data.get('transactionId', None)
        deductionMonth = data.get('deductionMonth', None)
        remarks = data.get('remarks', None)
        deductionAmount = data.get('deductionAmount', 0) or 0
        partner = get_partner_from_api_key(api_key)

        if not partner:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            driverId = int(driverId)
        except:
            _out = {
                    'status': 'FAIL',
                    'message': 'driverId must be an Integer'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        try:
            deductionAmount = decimal.Decimal(float(deductionAmount))
        except:
            _out = {
                    'status': 'FAIL',
                    'message': 'deductionAmount must be an number'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not mobile and not driverId:
            _out = {
                'status': 'FAIL',
                'message': 'mobile or driverId is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not referenceUuid:
            _out = {
                'status': 'FAIL',
                'message': 'referenceUuid is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if not deductionMonth or not deductionAmount:
            _out = {
                'status': 'FAIL',
                'message': 'deductionAmount and  deductionMonth is required'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if deductionAmount >= 0:
            _out = {
                'status': 'FAIL',
                'message': 'deductionAmount should be less than 0'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if driverId:
            query = Q(id=driverId)
        elif mobile:
            email = str(mobile) +'@'+  settings.DEFAULT_DOMAINS.get('driver')
            query = Q(user__email=email)

        driver = Driver.objects.filter(query).first()

        if DriverDeduction.objects.filter(partner_id=partner.id, reference_id=referenceUuid).exists():
            _out = {
                'status': 'FAIL',
                'message': 'Deduction with this reference id exists'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        if driver:
            balance = driver.driverbalance if hasattr(driver, 'driverbalance') else None

            unapproved_bal = DriverPayment.objects.filter(driver=driver, status=DriverConst.UNAPPROVED). \
                aggregate(Sum('net_pay'))['net_pay__sum'] or 0

            aval_balance_amount = round((float(balance.amount if balance else 0) + float(unapproved_bal)),2)

            if abs(deductionAmount) > aval_balance_amount:
                _out = {
                    'status': 'FAIL',
                    'message': 'Deduction Amount is exceeding driver balance, his balance is %s' % (aval_balance_amount)
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

            with transaction.atomic():
                deduction = DriverDeduction(
                    partner=partner,
                    driver=driver,
                    remark=remarks,
                    deduction_date=parse(deductionMonth),
                    transaction_id=transactionId,
                    reference_id=referenceUuid,
                    deduction_amount=deductionAmount,
                )
                deduction.save()

                DriverLedger(
                    driver=driver,
                    amount=deductionAmount if deductionAmount < 0 else 0,
                    description='Deduction posted by partner %s,  for %s' % (
                        str(partner.legalname),
                        parse(deductionMonth).strftime(DATE_FORMAT)
                    ),
                    deduction_reference=deduction
                ).save()
            _out = {
                'status': 'SUCCESS',
                'message': {
                    'referenceId': deduction.id
                }
            }
            return Response(status=status.HTTP_200_OK, data=_out)

        _out = {
            'status': 'FAIL',
            'message': 'Driver Details Not Found'
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)



class DriverListExport(generics.ListAPIView):
    """
    List drivers for a city.

    * Requires authentication.
    * Only customers are able to access this view.
    """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def get(self, request, *args, **kwargs):

        qs = list(self.get_queryset())
        driver_list = self.get_driver_list(list(qs))
        filename = self.get_filename(self.request.GET.get(
            'city', None), self.request.GET.get('status', None))
        file_path = export_lists_to_spreadsheet(
            [driver_list], filename, [True])
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                logging.info(
                    'This is the file path "%s" with name "%s"' % (fh, filename))

                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                    os.path.basename(file_path)
                os.remove(file_path)
                return response
        else:
            response['errors'] = 'No file generated'
            status_code = status.HTTP_400_BAD_REQUEST
        return Response(response, status_code)

    def get_queryset(self):
        query_filter = self.get_query(self.request.query_params)
        if query_filter:
            driver_qs = Driver.objects.filter(query_filter)
        else:
            driver_qs = Driver.objects.all()

        driver_qs = driver_qs.select_related(
            'operating_city', 'contract', 'user'
        )

        driver_qs = driver_qs.prefetch_related(
            Prefetch(
                'vehicles',
                queryset=Vehicle.objects.select_related(
                    'vehicle_model'),
                to_attr='vehicle'
            )
        )

        driver_qs = driver_qs.prefetch_related(
            Prefetch(
                'driverpreferredlocation_set',
                queryset=DriverPreferredLocation.objects.all(),
                to_attr='pref_loc'
            )
        )

        driver_qs = driver_qs.prefetch_related(
            Prefetch(
                'resourceallocation_set',
                queryset=ResourceAllocation.objects.select_related(
                    'contract', 'hub'),
                to_attr='res_allc'
            )
        )

        return driver_qs

    def get_query(self, params):
        city = params.get('city', None)
        status = params.get('status', None)
        query = []
        if city:
            query.append(Q(operating_city__id=city))
        if status:
            query.append(Q(status=status))
        return reduce(operator.and_, query) if len(query) else ''

    def get_filename(self, city_id, status):
        if city_id is not None and status is not None:
            city = City.objects.filter(id=city_id).first()
            file = 'driver_list_' + city.name  + '_' + status + '_' +\
                str(datetime.datetime.now().strftime(
                    settings.FILE_TIMESTAMP_FORMAT))
        elif city_id is not None:
            city = City.objects.filter(id=city_id).first()
            file = 'driver_list_' + city.name  + '_all_statuses_' +\
                str(datetime.datetime.now().strftime(
                    settings.FILE_TIMESTAMP_FORMAT))
        elif status is not None:
            file = 'driver_list_all_cities_' + status + '_' +\
                str(datetime.datetime.now().strftime(
                    settings.FILE_TIMESTAMP_FORMAT))
        else:
            file = 'driver_list_all_cities_all_statuses' + \
                str(datetime.datetime.now().strftime(
                    settings.FILE_TIMESTAMP_FORMAT))
        filename = file + '.xlsx'
        return filename

    def get_driver_list(self, drivers):
        driver_list = [('Sl No', 'Name', 'Vehicle Number', 'Vehicle Model',
                        'City', 'Mobile', 'Status',
                        'Contract Name', 'Hub Name', 'Preferred Locations', 'Own Vehicle', 'Can Drive And Deliver',
                        'Created By', 'Created On')]
        for count, driver in enumerate(drivers):
            preferred_locations = []
            contract_name = []
            hub_name = []
            model_name = ''
            created_date = ''

            for x in driver.pref_loc:
                preferred_locations.append(str(x.geopoint.coords[::-1]))

            for allocated_res in driver.res_allc:
                contract_name.append(allocated_res.contract)
                hub_name.append(str(allocated_res.hub))

            if driver.vehicle:
                model_name = driver.vehicle[0].vehicle_model.model_name

            if driver.created_date:
                created_date = utc_to_ist(driver.created_date).strftime(
                    settings.ADMIN_DATETIME_FORMAT)

            city = driver.operating_city.name if driver.operating_city else ''
            vehicle_owner = 'Yes' if driver.own_vehicle else 'No'
            can_deliver = 'Yes' if driver.can_drive_and_deliver else 'No'
            locations = ', '.join(preferred_locations)
            contracts = ', '.join(map(str, contract_name))
            hubs = ', '.join(hub_name) if hub_name else ''
            created_by = str(driver.created_by) if driver.created_by else ''

            driver_list.append((count + 1, driver.name, driver.driver_vehicle,
                                model_name, city, driver.user.national_number,
                                driver.status, contracts, hubs, locations,
                                vehicle_owner, can_deliver, created_by, created_date))
        return driver_list

class DriversAllActive(views.APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    permission_classes = (AllowAny, )

    def get(self, request):
        search_term = request.GET.get('data', None)

        driver_list = Driver.objects.filter(
            Q(name__icontains=search_term) | Q(driver_vehicle__icontains=search_term),
            status=StatusPipeline.ACTIVE,
            driver_vehicle__isnull=False,
        )
        driver_list_serializer = DriverAutofillSerializer(driver_list, many=True)
        return Response(data = driver_list_serializer.data, status=status.HTTP_200_OK)

class DriverAvailability(generics.ListAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    permission_classes = (AllowAny,)

    def get_available_drivers(self, customer, params=None, order=None):
        consider_eta = False
        distance_within = 0
        query = Q()

        if params:
            eta_within_mins = params.get('eta_within_mins', 0)
            pincode = params.get('postcode', None)
            lat = params.get('lat', None)
            lon = params.get('lon', None)
            if 'eta_within_mins' in params:
                consider_eta = True
            distance_within = float(AVERAGE_VEHICLE_SPEED_KMPH / 60) * float(eta_within_mins)
            pickup_address_geopoint = CommonHelper.get_geopoint_from_latlong(lat, lon)
            commercial_class = params.get('commercial_class')
        else:
            commercial_class = order.vehicle_class_preference.commercial_classification
            pickup_address_ = order.pickup_address
            pickup_address_geopoint = pickup_address_.geopoint
            pincode = pickup_address_.postcode

        if commercial_class:
            query = query & Q(
                driver__vehicles__vehicle_model__vehicle_class__commercial_classification=commercial_class)

        distance_radius_km = distance_within if consider_eta else float(
            DriverConstants().get('DRIVER_AVL_DISTANCE_IN_KM', 5))
        driver_last_updated_min = float(DriverConstants().get('DRIVER_LAST_UPDATED_TIME', 5))

        city = get_city_from_geopoint(pickup_address_geopoint, field='coverage_pickup')
        if not city:
            city = get_city_from_pincode(pincode)

        driver_ids = []
        if order:
            driver_ids = Slots.objects.filter(city=city, customer=customer, hub=order.pickup_hub,
                                          ).values_list('autodispatch_drivers', flat=True)

        if driver_ids:
            query = query & Q(driver_id__in = driver_ids)
        #     driver_ids = Slots.objects.filter(city=city).values_list('autodispatch_drivers', flat=True)
        time_before_next_trip = OrderConstants().get('MINUTES_BEFORE_NEXT_TRIP', 60)

        min_planned_start_time = tzzone.now() + \
                                 datetime.timedelta(minutes=(time_before_next_trip))
        query = query & Q(
            updated_time__gte=(tzzone.now() - datetime.timedelta(minutes=driver_last_updated_min)),
            geopoint__distance_lt=(pickup_address_geopoint, Distance(km=distance_radius_km)))

        activities = DriverActivity.objects.filter(query
            ).annotate(
            distance=Dt('geopoint', pickup_address_geopoint)).only('id', 'updated_time', 'geopoint','driver').exclude(
            driver_id__in=(
                Trip.objects.filter(
                    planned_start_time__lte=min_planned_start_time,
                    status__in=[StatusPipeline.TRIP_IN_PROGRESS, StatusPipeline.DRIVER_ACCEPTED,
                                StatusPipeline.TRIP_ALL_STOPS_DONE]).values_list('driver_id', flat=True))).order_by(
            'distance')
        return activities

    def get(self, request):
        params = request.GET
        lat = params.get('lat', None)
        lon = params.get('lon', None)
        pincode = params.get('postcode', None)

        if not pincode:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Pickup Pincode is required')


        if not lat or not lon:
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Pickup Lat and lon is required')

        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        response = []
        activities = self.get_available_drivers(customer, params=params)
        for activity in activities:
            driver = activity.driver
            geopoint = activity.geopoint
            rating, count = driver.get_rating()
            _data ={
                "name": driver.name,
                "phone_number": str(driver.user.phone_number.national_number),
                "vehicle_number": driver.driver_vehicle,
                "current_location": {
                    "lat": geopoint.y,
                    "lon":  geopoint.x
                },
                "distance_to_pickup_in_km": round(activity.distance.km, 1),
                "rating": rating or 0,
                "no_of_trips": count

            }
            response.append(_data)
        if not response:
            return Response(status=status.HTTP_200_OK,
                            data=_('No delivery partners currently available'))
        return Response(status=status.HTTP_200_OK, data=response)


class DriverWellBeingView(views.APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request):
        from django.utils import timezone
        driver = request.user.driver if hasattr(request.user, 'driver') else None
        response_data = {}
        capture_data = []
        capture = driver.capture_wellbeing_status if driver else False
        if capture:
            time_interval = int(DriverConstants().get('WELLBEING_CAPTURE_INTERVAL', 8))
            threshold = timezone.now() - datetime.timedelta(time_interval)
            capture = False if DriverWellBeing.objects.filter(driver=driver,
                                                              last_captured_on__gte=threshold).exists() else True

        response_data['capture'] = capture
        response_data['title'] = 'Driver Well being'

        if settings.ENABLE_DRIVER_WELL_BEING and capture:

            for event in EventType.objects.iterator():
                capture_data.append({
                    'display': event.event_name,
                    'description': event.description,
                    'key': event.key,
                    'data_type': event.data_type,
                    'return_type': event.return_type
                })

            min_temp = int(DriverConstants().get('MIN_ALLOWED_TEMPERATURE', 75))
            max_temp = int(DriverConstants().get('MAX_ALLOWED_TEMPERATURE', 115))
            response_data['capture_data'] = capture_data
            response_data['min_temp'] = min_temp
            response_data['max_temp'] = max_temp
        return Response(status=status.HTTP_200_OK, data=response_data)

    def post(self, request):
        data = json.loads(request.data.get('driver_data'))
        driver = request.user.driver if hasattr(request.user, 'driver') else None

        if driver:
            for event in data:
                if event.get('key') == 'temperature':
                    min_temp = int(DriverConstants().get('MIN_ALLOWED_TEMPERATURE', 75))
                    max_temp = int(DriverConstants().get('MAX_ALLOWED_TEMPERATURE', 115))

                    if not int(float(event.get('value'))) in range(min_temp, max_temp):
                        message = _('Temperature must be in the range of %s to %s') % (min_temp, max_temp)
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=message)

                driver_event = DriverWellBeing.objects.filter(driver=driver,
                                                              event_type__key=event.get('key')).first()
                if driver_event:
                    driver_event.event_status = event.get('value')
                    driver_event.save()
                else:
                    event_type = EventType.objects.filter(key=event.get('key')).first()
                    event_data = {
                        'driver': driver,
                        'event_type': event_type,
                        'event_status': event.get('value'),
                        'modified_by': request.user
                    }
                    created_event = DriverWellBeing.objects.create(**event_data)

        return Response(status=status.HTTP_200_OK, data=_('Event updated successfully'))
