# Django and System libraries
import pytz
import json
import logging
import phonenumbers
from django.utils import timezone
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.conf import settings
from datetime import datetime

# 3rd party libraries
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status

# blowhorn imports
from blowhorn.common.serializers import BaseSerializer
from config.settings import status_pipelines as StatusPipeline
from blowhorn.order.models import Order
from blowhorn.common.helper import CommonHelper
from blowhorn.trip.models import Trip, Stop
from .models import Driver, DriverVehicleMap, DriverActivity, DeviceDetails, \
    DriverDocument, DriverPreferredLocation, DriverLedger, \
    DriverRatingDetails, DriverDocumentPage
from .mixins import DriverMixin
from blowhorn.utils import functions as utils_func
from blowhorn.address.serializer import FormattedAddressSerializer
from blowhorn.address.utils import AddressUtil
from blowhorn.vehicle.serializers import VehicleSerializer, VehicleClassSerializer
from blowhorn.driver.constants import DRIVER_ACTIVITY_IDLE, DRIVER_ACTIVITY_OCCUPIED
from blowhorn.driver.models import BankAccount
from blowhorn.document.serializers import DocumentSerializer, DocumentPageSerializer
from blowhorn.users.models import User


class DriverSerializer(BaseSerializer, DriverMixin):
    queryset = Driver.objects.all()
    phone_number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Driver
        fields = '__all__'

    def get_phone_number(self, driver):
        return str(driver.user.phone_number)[-10:]


class DriverVehicleMapSerializer(serializers.ModelSerializer, DriverMixin):
    queryset = DriverVehicleMap.objects.all()

    class Meta:
        model = DriverVehicleMap
        fields = '__all__'


class DriverLedgerSerializer(serializers.ModelSerializer, DriverMixin):
    queryset = DriverLedger.objects.all()

    class Meta:
        model = DriverLedger
        exclude = ('balance',)


class DriverActivitySerializer(serializers.ModelSerializer, DriverMixin):
    queryset = DriverActivity.objects.all()

    def create(self):
        response = self.get_validated_data()
        data = self.create_activity(data=response)

        if not data:
            return False

        return data

    def update(self):
        response = self.get_validated_data()
        data = self.update_activity(data=response)

        if not data:
            return False

        return data

    def delete(self):
        response = self.get_validated_data()
        data = self.delete_activity(data=response)
        return data

    def get_validated_data(self):
        data = self.initial_data

        # Validate whether all required inputs are there or not?
        mandatory_fields = ['latitude', 'longitude',
                            'gps_timestamp', 'device_timestamp']

        for field in mandatory_fields:
            if field not in data:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data=field + ' is mandatory.'
                )

        order = None
        stop = None
        trip = None

        event = DRIVER_ACTIVITY_IDLE
        stop_str = None
        order_str = None
        trip_str = None

        extras = data.get('extras')
        if extras:
            if isinstance(extras, str):
                extras = json.loads(extras)

            trip_number = extras.get('tripsheet_key')
            if trip_number:
                trip = Trip.objects.get(trip_number=trip_number)

        if not trip:
            trip = Trip.objects.filter(
                driver=data.get('driver'),
                status__in=[
                    StatusPipeline.TRIP_IN_PROGRESS,
                    StatusPipeline.DRIVER_ACCEPTED,
                    StatusPipeline.TRIP_ALL_STOPS_DONE]
            ).first()

            if not trip:
                trip = Trip.objects.filter(
                    driver=data.get('driver'),
                    status=StatusPipeline.TRIP_NEW,
                    order__order_type=settings.C2C_ORDER_TYPE
                ).first()

        if trip:
            event = DRIVER_ACTIVITY_OCCUPIED
            trip_str = trip.trip_number
            try:
                stop = Stop.objects.filter(
                    trip=trip, status=StatusPipeline.TRIP_IN_PROGRESS).latest('start_time')
                stop_str = stop.pk
            except Stop.DoesNotExist:
                pass

            order_number = data.get('order_number', None)
            if order_number:
                order = get_object_or_404(Order, number=order_number)
                order_str = order.number

        # Set / Derive the model fields
        validated_data = {}
        validated_data['driver'] = data.get('driver')

        # **WARNING: Do not use following ForeignKey relations. Will be removed soon.
        validated_data['trip'] = trip
        validated_data['order'] = order
        validated_data['stop'] = stop

        validated_data['trip_str'] = trip_str
        validated_data['order_str'] = order_str
        validated_data['stop_str'] = stop_str

        try:
            validated_data['gps_timestamp'] = pytz.timezone(
                settings.TIME_ZONE).localize(
                datetime.strptime(
                    data.get('gps_timestamp'),
                    '%d-%b-%Y %H:%M:%S'))
        except:
            validated_data['gps_timestamp'] = timezone.now()

        device_timestamp = data.get('device_timestamp')

        time_params_received = True
        if device_timestamp and not isinstance(device_timestamp, datetime):
            try:
                validated_data['device_timestamp'] = pytz.timezone(
                    settings.IST_TIME_ZONE).localize(
                    datetime.strptime(
                        device_timestamp,
                        '%d-%b-%Y %H:%M:%S'))
            except:
                time_params_received = False
                validated_data['device_timestamp'] = timezone.now()
        else:
            time_params_received = False
            validated_data['device_timestamp'] = timezone.now()

        validated_data['location_accuracy_meters'] = data.get(
            'location_accuracy_meters', 0
        )
        validated_data['vehicle_speed_kmph'] = data.get(
            'vehicle_speed_kmph', 0
        )
        validated_data['geopoint'] = CommonHelper.get_geopoint_from_latlong(
            data.get('latitude'), data.get('longitude')
        )
        validated_data['event'] = event

        # Calculate angle between two locations in degrees
        angle_bearing = utils_func.calculate_angle_between_two_points(
            data.pop('prev_location', None), validated_data['geopoint'])
        formatted_angle = float("%.2f" % angle_bearing)
        validated_data['orientation_from_north'] = formatted_angle

        # All the keys not present in validated data will be encoded as other
        # details.
        other_details = {}
        for i in list(set(data) - set(validated_data)):
            other_details[i] = data[i]
        other_details['time_params_received'] = time_params_received
        validated_data['other_details'] = json.loads(json.dumps(other_details))

        return validated_data

    class Meta:
        model = DriverActivity
        fields = '__all__'


class DriverPreferredLocationSerializer(serializers.ModelSerializer):

    coordinates = serializers.SerializerMethodField(
        source='get_coordinates')

    def get_coordinates(self, driverpreferredlocation):
        coordinates = {}
        if driverpreferredlocation.geopoint:
            latlng_dict = AddressUtil().get_latlng_from_geopoint(
                driverpreferredlocation.geopoint)
            coordinates['latitude'] = latlng_dict.get('lat')
            coordinates['longitude'] = latlng_dict.get('lng')
        return coordinates

    class Meta:
        model = DriverPreferredLocation
        fields = ('id', 'driver', 'geopoint','coordinates',)


class DriverListSerializer(serializers.ModelSerializer):
    mobile = serializers.CharField(source='user.phone_number')

    address_model = serializers.SerializerMethodField(
        source='get_address_model')

    driver_is_owner = serializers.BooleanField(source='own_vehicle')

    vehicles = serializers.SerializerMethodField(source='get_vehicles')

    operating_city = serializers.SerializerMethodField(
        source='get_operating_city')

    owner_contact_no = serializers.SerializerMethodField(
        source='get_owner_contact_number')

    class Meta:
        model = Driver
        fields = ('id', 'name', 'driver_vehicle', 'mobile', 'driver_is_owner', 'owner_contact_no',
                  'working_preferences', 'can_drive_and_deliver', 'can_read_english', 'driving_license_number',
                  'can_source_additional_labour', 'work_status', 'address_model', 'status', 'vehicles', 'operating_city')

    def get_address_model(self, driver):
        address_dict = []

        driver_preferred_location = driver.driverpreferredlocation_set.all()
        if driver_preferred_location:
            address_dict = DriverPreferredLocationSerializer(driver_preferred_location, many=True).data

        return address_dict

    def get_vehicles(self, driver):
        vehicle_queryset = driver.vehicles.all()
        if vehicle_queryset:
            return VehicleSerializer(vehicle_queryset, many=True).data
        return []

    def get_operating_city(self, driver):
        return driver.operating_city.name

    def get_owner_contact_no(self, driver):
        if driver.owner_details and driver.owner_details.phone_number:
            return str(driver.owner_details.phone_number)
        return ""


class DriverAppSerializer(serializers.ModelSerializer):

    def validate_name(self, name):
        if not utils_func.validate_name(name):
            logging.error("Please Enter a valid name")
            raise serializers.ValidationError("Please Enter a valid name")
        return name

    def validate(self, data):
        data = self.initial_data
        if not utils_func.validate_phone_number(data.get('phone')):
            logging.error("Please Enter a valid phone number")
            raise serializers.ValidationError(
                "Please Enter a valid phone number")
        return data

    def create(self, validated_data):
        validated_data.pop('phone')
        driver = Driver.objects.create(**validated_data)
        driver.created_by = validated_data.get('created_by')
        driver.modified_by = validated_data.get('created_by')
        driver.save()

        if not driver:
            return False

        return driver

    class Meta:
        model = Driver
        fields = '__all__'


class DriverActivateSerializer(serializers.ModelSerializer):

    # modified_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Driver
        fields = '__all__'


class DriverContractSerializer(serializers.ModelSerializer):
    # driver = serializers.SerializerMethodField(source='get_driver')
    mobile = serializers.SerializerMethodField(source='get_mobile')
    vehicles = serializers.SerializerMethodField(source='get_vehicles')

    class Meta:
        model = Driver
        fields = ('id', 'name', 'driver_vehicle', 'is_blowhorn_driver', 'is_marketplace_driver', 'mobile', 'vehicles')

    # def get_driver(self, driver):
    #     return str(driver)

    def get_mobile(self, driver):
        return str(driver.user.phone_number)

    def get_vehicles(self, driver):
        vehicles = driver.vehicles.all()
        vehicles_list = []
        for v in vehicles:
            dict_ = {
                'vehicle_class_name': v.vehicle_model.vehicle_class.commercial_classification,
                'body_type_name': v.body_type.body_type,
                'supply_cap': float(v.vehicle_model.vehicle_class.per_day_limit)
            }
            vehicles_list.append(dict_)
        return vehicles_list


class DeviceDetailSerializer(serializers.ModelSerializer, DriverMixin):

    class Meta:
        model = DeviceDetails
        fields = '__all__'


class DriverDocumentPageSerializer(DocumentPageSerializer):

    def create(self, validated_data):
        driver_doc_page = DriverDocumentPage.objects.create(**validated_data)
        driver_doc_page.created_by = validated_data.get('created_by')
        driver_doc_page.modified_by = validated_data.get('modified_by')
        driver_doc_page.save()

        if not driver_doc_page:
            return False

        return driver_doc_page

    class Meta:
        model = DriverDocumentPage
        fields = DocumentPageSerializer.Meta.fields + ('driver_document',)


class DriverDocumentSerializer(DocumentSerializer):

    def create(self, validated_data):
        driver_doc = DriverDocument.objects.create(**validated_data)
        driver_doc.created_by = validated_data.get('created_by')
        driver_doc.modified_by = validated_data.get('modified_by')
        driver_doc.save()

        if not driver_doc:
            return False

        return driver_doc

    class Meta:
        model = DriverDocument
        fields = DocumentSerializer.Meta.fields + ('driver', 'state')


class AvailableDriverSerializer(BaseSerializer):
    mobile = serializers.CharField(source='driver.user.phone_number')

    location_dict = serializers.SerializerMethodField(
        source='get_address_model')

    vehicle_class = serializers.SerializerMethodField(source='get_vehicle_class')

    def get_location_dict(self, driver_activity):
        location_dict = {}

        if driver_activity.geopoint:
            latlng_dict = AddressUtil().get_latlng_from_geopoint(
                driver_activity.geopoint)
            location_dict['latitude'] = latlng_dict.get('lat')
            location_dict['longitude'] = latlng_dict.get('lng')

        return location_dict

    def get_vehicle_class(self, driver_activity):
        if driver_activity and driver_activity.driver:
            vehicle_queryset = driver_activity.driver.vehicles.all()
            if vehicle_queryset:
                return VehicleClassSerializer(vehicle_queryset, many=True).data
        return []

    class Meta:
        model = DriverActivity
        fields = ['driver', 'location_dict', 'vehicle_class', 'mobile']


class BankAccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = BankAccount
        fields = '__all__'


class DriverRatingDetailSerializer(serializers.ModelSerializer):
    remark = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(DriverRatingDetailSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    def get_remark(self, obj):
        return obj.remark.remark

    class Meta:
        model = DriverRatingDetails


class DriverAutofillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = fields = ('id', 'name', 'driver_vehicle', 'is_blowhorn_driver', 'is_marketplace_driver', 'contract')