# System and Django libraries
import time
import logging
import json
# import architect
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import pgettext_lazy
from django.utils import timezone
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.indexes import BrinIndex
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models import Sum, Q, Manager, F, ExpressionWrapper, DateTimeField, Count
from django.core.validators import RegexValidator, MinLengthValidator, MinValueValidator
from decimal import Decimal
from django.db import transaction
from django.db.models.functions import Cast
from datetime import timedelta

# 3rd party libraries
from phonenumber_field.modelfields import PhoneNumberField
from blowhorn.oscar.apps.address.abstract_models import AbstractAddress
from auditlog.models import AuditlogHistoryField
from django_fsm import FSMField, transition, ConcurrentTransitionMixin
from blowhorn.oscar.models.fields import NullCharField
import phonenumbers
from postgres_copy import CopyManager

# blowhorn imports
from blowhorn.driver.redis_models import Driver as RedisDriver
from blowhorn.document.models import Document, DocumentPage
from blowhorn.document.utils import generate_file_path
from blowhorn.vehicle.models import Vehicle
from blowhorn.users.models import User
from blowhorn.common.models import BaseModel
from config.settings import status_pipelines as StatusPipeline
from blowhorn.customer.models import Customer, CustomerDivision, Partner
from blowhorn.customer.constants import CREATED, SETTLED, REJECTED
from blowhorn.address.models import City
from .payment_transition_conditions import is_payment_spoc, \
    is_payment_supervisor, \
    is_payment_spoc_or_supervisor, is_super_user, is_current_user_can_approve
from .payment_transition_conditions import is_adjustment_supervisor, \
    is_adjustment_spoc_or_supervisor
from .payment_transition_conditions import can_approve_payment
from .payment_transition_conditions import can_unapprove_payment
from .payment_transition_conditions import can_unapprove_adjustment, \
    can_approve_adjustment
from .payment_transition_conditions import is_nop_payment_approver, \
    is_nop_payment_unapprover, is_eligible_for_unapprovable
from .payment_transition_conditions import is_nops_payment_approvable
from blowhorn.driver import constants as DriverConst
from blowhorn.common.middleware import current_request
from blowhorn.document.constants import BETTERPLACE_DOCUMENT_TYPE_CHOICES
from config.settings.status_pipelines import (
    DRIVER_ACCEPTED, TRIP_IN_PROGRESS, TRIP_NEW,
    TRIP_ALL_STOPS_DONE, TRIP_COMPLETED, TRIP_VEHICLE_LOAD
)
from blowhorn.contract.constants import (
    CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED,
)
from blowhorn.driver.constants import NOTIFICATION_CHOICES, \
     FIELD_TYPE, FORM_TYPE, NOTIFICATION_INTERVALS
from blowhorn.contract.permissions import is_finance_user

WITHHELD = u'Withheld'
APPROVED = u'Approved'
UNAPPROVED = u'Unapproved'
REVERTED = u'Reverted'
ADJUSTED = u'Adjusted'
CLOSED = u'Closed'
SUSPENDED = u'Suspended'
DATE_FORMAT = '%d-%b-%Y'

alphabets_only_validator = RegexValidator(regex=r'^[a-zA-Z_\s]+$',
                                          message=_('Only Alphabets '
                                                    'characters allowed'))

alphanumeric_validator = RegexValidator(regex=r'^[a-zA-Z0-9_\s]+$',
                                        message=_('Only AlphabetNumeric '
                                                  'characters allowed'))


class NotificationContentLanguage(models.Model):

    title_en = models.CharField(_('Title'), max_length=50)
    title_kn = models.CharField(_('Title'), max_length=50)
    title_mr = models.CharField(_('Title'), max_length=50)
    title_te = models.CharField(_('Title'), max_length=50)
    title_ta = models.CharField(_('Title'), max_length=50)
    title_hi = models.CharField(_('Title'), max_length=50)

    short_description_en = models.CharField(_('Short Description'), max_length=50, null=True, blank=True)
    short_description_kn = models.CharField(_('Short Description'), max_length=50, null=True, blank=True)
    short_description_mr = models.CharField(_('Short Description'), max_length=50, null=True, blank=True)
    short_description_te = models.CharField(_('Short Description'), max_length=50, null=True, blank=True)
    short_description_ta = models.CharField(_('Short Description'), max_length=50, null=True, blank=True)
    short_description_hi = models.CharField(_('Short Description'), max_length=50, null=True, blank=True)

    description_en = models.TextField(_('Notification Content'), null=True, blank=True, help_text=_('Text content'))
    description_kn = models.TextField(_('Notification Content'), null=True, blank=True, help_text=_('Text content'))
    description_mr = models.TextField(_('Notification Content'), null=True, blank=True, help_text=_('Text content'))
    description_te = models.TextField(_('Notification Content'), null=True, blank=True, help_text=_('Text content'))
    description_ta = models.TextField(_('Notification Content'), null=True, blank=True, help_text=_('Text content'))
    description_hi = models.TextField(_('Notification Content'), null=True, blank=True, help_text=_('Text content'))

    img_english = models.ImageField(
        _('Image (English)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For English Image, upload here'))
    img_kannada = models.ImageField(
        _('Image (Kannada)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Kannada Image, upload here'))
    img_marathi = models.ImageField(
        _('Image (Marathi)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Marathi Image, upload here'))
    img_telugu = models.ImageField(
        _('Image (Telugu)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Telugu Image, upload here'))
    img_tamil = models.ImageField(
        _('Image (Tamil)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Tamil Image, upload here'))
    img_hindi = models.ImageField(
        _('Image (Hindi)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Hindi Image, upload here'))

    def __str__(self):
        return self.title_en


class NotificationCampaign(models.Model):

    title = models.CharField(_('Title'), max_length=50)

    notification_type = models.CharField(
        _('Notification Type'),
        max_length=30,
        choices=NOTIFICATION_CHOICES,
        null=True, blank=True,
        help_text=_('Only Text is sent in case of Push Notifications')
    )

    content = models.ForeignKey(NotificationContentLanguage, related_name='notification_content',
                                null=True, blank=True, on_delete=models.PROTECT)

    notification_interval = models.CharField(
        _('Notification Interval'),
        max_length=30,
        choices=NOTIFICATION_INTERVALS,
        null=True, blank=True
    )

    notification_content = models.TextField(_('Notification Content'),  null=True, blank=True,
                                            help_text=_('For Text content type'))
    push_notification_content = models.TextField(_('Notification Content'), null=True, blank=True)
    img_telugu = models.ImageField(
        _('Image (Telugu)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Telugu Image, upload here'))
    img_tamil = models.ImageField(
        _('Image (Tamil)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Tamil Image, upload here'))
    img_kannada = models.ImageField(
        _('Image (Kannada)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Kannada Image, upload here'))
    img_marathi = models.ImageField(
        _('Image (Marathi)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Marathi Image, upload here'))
    img_hindi = models.ImageField(
        _('Image (Hindi)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For Hindi Image, upload here'))
    img_english = models.ImageField(
        _('Image (English)'),
        upload_to=generate_file_path,
        max_length=500, blank=True, null=True,
        help_text=_('For English Image, upload here'))

    start_time = DateTimeField(null=True, blank=True,)
    end_time = DateTimeField(null=True, blank=True,)

    scheduled_time = models.TimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    short_description = models.CharField(_('Short Description'),
                                         max_length=30,
                                         null=True, blank=True)

    url = models.URLField(verbose_name='URL', null=True, blank=True)
    mandatory = models.BooleanField(default=False)

    form_type = models.CharField(
        _('Form Type'),
        max_length=30,
        choices=FORM_TYPE,
        null=True, blank=True
    )

    objects = models.Manager()

    def __str__(self):
        return self.title


class NotificationFormLanguage(models.Model):

    question_title = models.CharField(_('Question Title'), max_length=50)

    question_en = models.CharField(_('Question'), blank=True, null=True, max_length=250)
    question_kn = models.CharField(_('Question'), blank=True, null=True, max_length=250)
    question_mr = models.CharField(_('Question'), blank=True, null=True, max_length=250)
    question_te = models.CharField(_('Question'), blank=True, null=True, max_length=250)
    question_ta = models.CharField(_('Question'), blank=True, null=True, max_length=250)
    question_hi = models.CharField(_('Question'), blank=True, null=True, max_length=250)

    field_values_en = ArrayField(models.CharField(blank=True, null=True, max_length=50), blank=True, null=True)
    field_values_kn = ArrayField(models.CharField(blank=True, null=True, max_length=50), blank=True, null=True)
    field_values_mr = ArrayField(models.CharField(blank=True, null=True, max_length=50), blank=True, null=True)
    field_values_te = ArrayField(models.CharField(blank=True, null=True, max_length=50), blank=True, null=True)
    field_values_ta = ArrayField(models.CharField(blank=True, null=True, max_length=50), blank=True, null=True)
    field_values_hi = ArrayField(models.CharField(blank=True, null=True, max_length=50), blank=True, null=True)

    def __str__(self):
        return self.question_title


class DriverResponseFields(models.Model):
    notification_campaign = models.ForeignKey(NotificationCampaign,
                                              related_name='notification_response',
                                              null=True, blank=True,
                                              on_delete=models.PROTECT)
    notification_form = models.ForeignKey(NotificationFormLanguage, related_name='notification_language',
                                          null=True, blank=True, on_delete=models.PROTECT)

    name = models.CharField(_('Name'), max_length=250)

    field_type = models.CharField(
        _('Field Type'),
        max_length=30,
        choices=FIELD_TYPE,
        null=True, blank=True
    )
    field_values = ArrayField(models.CharField(blank=True, null=True, max_length=50), blank=True,
                              null=True)

    objects = models.Manager()


class NotificationReadStatus(BaseModel):
    notification_campaign = models.ForeignKey(NotificationCampaign,
                                              related_name='read_status',
                                              null=True, blank=True,
                                              on_delete=models.PROTECT)
    driver = models.ForeignKey('driver.Driver', null=True, blank=True,
                               on_delete=models.PROTECT)

    is_read = models.BooleanField(default=False)
    objects = models.Manager()


class DriverNotificationCriteria(models.Model):
    notification_campaign = models.ForeignKey(NotificationCampaign,
                                              related_name='driver_criteria',
                                              null=True, blank=True,
                                              on_delete=models.PROTECT)
    operating_cities = models.ManyToManyField('address.City',
                                              blank=True,
                                              verbose_name='Operating Cities')
    own_vehicle = models.BooleanField(
                    _('Own Vehicle?'), default=False)
    vehicle_classes = models.ManyToManyField('vehicle.VehicleClass',
                                             blank=True,
                                             verbose_name='Vehicle Class')
    driver_ids = ArrayField(models.PositiveIntegerField(blank=True, null=True), blank=True,
                            null=True)
    objects = models.Manager()


class DriverNotificationResponse(models.Model):

    driver = models.ForeignKey('driver.Driver', null=True, blank=True,
                               on_delete=models.PROTECT)
    notification_campaign = models.ForeignKey(NotificationCampaign,
                                              related_name='driver_response',
                                              null=True, blank=True,
                                              on_delete=models.PROTECT)
    last_response_time = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()
    copy_data = CopyManager()

    def __str__(self):
        return self.driver.name


class DriverResponse(models.Model):
    driver_notification_response = models.ForeignKey(DriverNotificationResponse, on_delete=models.PROTECT)
    notification = models.ForeignKey(NotificationCampaign, on_delete=models.PROTECT)
    form_field = models.ForeignKey(DriverResponseFields, on_delete=models.PROTECT)
    field_response = models.CharField('Field Response', max_length=50)

    objects = models.Manager()


class DriverManager(models.Manager):

    def upsert_user(self, email, name, phone_number,
                    password=None, user_id=None,
                    is_active=False, domain=None, city=None):
        now = timezone.now()
        phone_number = phonenumbers.parse(phone_number,
                                          settings.COUNTRY_CODE_A2)
        email = '%s@%s' % (phone_number.national_number,
                           settings.DEFAULT_DOMAINS.get('driver') if not domain else domain)

        if user_id:
            user = User.objects.get(pk=user_id)
            user.phone_number = phone_number
            user.email = email
            user.name = name
            user.is_active = is_active
            user.city = city
        else:
            user = User(
                phone_number=phone_number,
                email=email,
                is_mobile_verified=True,
                last_login=now,
                name=name,
                is_active=is_active,
                city=city
            )
            if password:
                user.set_password(password)

        user.save()
        return user

    def upsert_driver_rating(self, num_of_feedbacks=0,
                             cumulative_rating=0, rating_id=None):
        driver_rating = DriverRating(
            num_of_feedbacks=num_of_feedbacks,
            cumulative_rating=cumulative_rating
        )
        if rating_id:
            driver_rating.id = rating_id

        driver_rating.save()
        return driver_rating


class BankAccount(BaseModel):
    """
    Bank details for the driver
    """
    # ifsc_validator = RegexValidator(regex=r'^[A-Za-z]{4}[0]\w{6}$',
    #                                 message=_('First 4 Alphabets(Bank Code), '
    #                                           'then 0 and next 6 Alphanumeric characters(Branch Code)'))

    account_name = models.CharField(
        _("Full name as registered in the bank"), max_length=100,
        validators=[alphabets_only_validator])

    bank_name = models.CharField(
        _("Bank Name"), max_length=100, null=True, blank=True,
        validators=[alphabets_only_validator])

    account_number = models.CharField(
        _("Bank Account number"), max_length=20,
        validators=[alphanumeric_validator])

    ifsc_code = models.CharField(
        _("IFSC Code for the bank / Branch Code"), max_length=50,)
        # validators=[MinLengthValidator(11)],
        # help_text=_("First 4 Bank Code, then 0 and then next 6 Alphanumeric characters(Branch Code)"))

    def __str__(self):
        return '%s-%s-%s' % (self.account_name,
                             self.account_number, self.ifsc_code)

    def add_bank_account_history(self, bank_account, action_owner, action, driver=None, vendor=None):
        history_data = {
            'driver': driver,
            'vendor': vendor,
            'bank_account': bank_account,
            'user': action_owner,
            'action': action
        }
        history_log = BankAccountHistory(**history_data)
        history_log.save()

    class Meta:
        verbose_name = _("Bank account")
        verbose_name_plural = _("Bank accounts")
        unique_together = ('account_number', 'ifsc_code')


class BankAccountHistory(models.Model):
    """
    Store bank account history
    """
    user = models.ForeignKey(
        'users.User', on_delete=models.PROTECT)
    bank_account = models.ForeignKey(
        'driver.BankAccount',
        null=True,
        blank=True,
        verbose_name='Bank Details', on_delete=models.PROTECT)
    driver = models.ForeignKey(
        'driver.Driver',
        null=True, blank=True,
        on_delete=models.PROTECT)
    vendor = models.ForeignKey('vehicle.Vendor',
                               null=True, blank=True,
                               verbose_name='Fleetowner',
                               on_delete=models.PROTECT)
    created_time = models.DateTimeField(auto_now_add=True)
    action = models.CharField(blank=True, max_length=256, null=True)


class DriverAddressManager(Manager):

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.select_related('country')
        return qs


class DriverAddress(AbstractAddress):
    """
    Driver Address from  Oscar AbstractAddress
    """
    geopoint = models.PointField(null=True, blank=True)

    bgv_status = models.CharField(
        _("Verification Status"),
        max_length=30,
        choices=StatusPipeline.BACKGROUND_VERIFICATION_STATUSES,
        default=StatusPipeline.PENDING, null=True, blank=True
    )

    bgv_sub_status = models.CharField(
        _("Verification Status"),
        max_length=30,
        choices=StatusPipeline.BACKGROUND_VERIFICATION_SUB_STATUSES,
        null=True, blank=True
    )

    remark = models.CharField(
        _('Remark'), max_length=255, null=True, blank=True)

    verification_requested_on = models.DateField(_('Address Verification Requested On'),
                                                 null=True, blank=True)
    verification_completed_on = models.DateField(_('Address Verification Completed On'),
                                                 null=True, blank=True)

    verification_extra_details = JSONField(blank=True, null=True)

    objects = DriverAddressManager()

    class Meta:
        verbose_name = _("Driver's address")
        verbose_name_plural = _("Driver addresses")


class DriverInactiveReason(BaseModel):
    reason = models.TextField()
    is_active = models.BooleanField()

    class Meta:
        verbose_name = _("Driver Inactive Reason")
        verbose_name_plural = _("Driver Inactive Reasons")


class DriverRating(BaseModel):
    """
    Aggregated rating for the driver based on his/her trips
    so far with blowhorn
    """
    cumulative_rating = models.DecimalField(
        _("Aggregated rating for the driver"),
        max_digits=3, decimal_places=2)

    num_of_feedbacks = models.IntegerField(
        _("Num of feedbacks received"), default=0)

    # Running counts of driver metrics
    # remember to update these during trip planning and execution
    accepted_count = models.IntegerField(
        _("Num of accepted trips"), default=0)

    rejected_count = models.IntegerField(
        _("Num of rejected trips"), default=0)

    missed_count = models.IntegerField(
        _("Num of missed trips"), default=0)

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s" % self.cumulative_rating


class DriverRatingDetails(BaseModel):
    """ Order wise rating for given by customer."""
    driver = models.ForeignKey(
        'driver.Driver', on_delete=models.PROTECT)

    order = models.ForeignKey(
        'order.Order', on_delete=models.PROTECT, null=True, blank=True)

    user = models.ForeignKey(
        'users.User', on_delete=models.PROTECT)

    rating = models.DecimalField(
        _("Aggregated rating for the driver"),
        max_digits=3, decimal_places=2)

    remark = models.ForeignKey(
        'driver.RatingCategory', on_delete=models.PROTECT, null=True, blank=True)

    customer_remark = models.CharField(
        _("Remark from the user"), max_length=500, null=True, blank=True)

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s-%s" % (self.driver, self.order)

    class Meta:
        verbose_name = _('Service Rating')
        verbose_name_plural = _('Service Ratings')


class RatingConfig(BaseModel):
    """ Order wise rating for given by customer."""
    rating = models.PositiveIntegerField(
        _("Rating for the driver"), unique=True)

    title = models.CharField(_("Title"), max_length=250)

    def __str__(self):
        return str(self.rating)

    class Meta:
        verbose_name = _('Rating Configuration')
        verbose_name_plural = _('Rating Configurations')


class RatingCategory(BaseModel):
    """ Order wise rating for given by customer."""

    remark = models.CharField(_("Remark"), max_length=250)

    def __str__(self):
        return str(self.remark)

    class Meta:
        verbose_name = _('Rating Category')
        verbose_name_plural = _('Rating Categories')


class DriverRatingAggregate(BaseModel):
    """ Order wise rating for given by customer."""
    rating = models.ForeignKey(RatingConfig, on_delete=models.PROTECT)

    remarks = models.ManyToManyField(RatingCategory, related_name='remarks')

    order_type = models.CharField(_("Order Type"), max_length=30, db_index=True,
                                  choices=settings.ORDER_TYPE_DICT, default=settings.DEFAULT_ORDER_TYPE)

    def __str__(self):
        return str(self.rating)

    class Meta:
        verbose_name = _('Rating Aggregate')
        verbose_name_plural = _('Rating Aggregates')


class AlternateContact(models.Model):
    """
    Driver's reference and emergency contacts
    """
    name = models.CharField(
        _("Alternate Contact Name"), max_length=255, blank=False,
        help_text=_("Name of the Alternate Contact"))

    phone_number = PhoneNumberField(
        _("Alternate Contact Number"), blank=True,
        help_text=_(
            "Alternate contact number e.g. +91{10 digit mobile number}"))

    relationship = models.CharField(
        _("Relationship with the Driver"), max_length=50,
        help_text=_("Relationship with the Driver"))

    class Meta:
        verbose_name = _("Alternate contact")
        verbose_name_plural = _("Alternate contacts")

    def __str__(self):
        return self.name


class DriverPreferredLocation(models.Model):

    driver = models.ForeignKey(
        'driver.Driver', null=True, on_delete=models.PROTECT)
    geopoint = models.PointField(
        _('Location Geopoint'),
        null=True, srid=4326,
        help_text=_("Represented as (longitude, latitude)"))

    class Meta:
        verbose_name = _("Driver's Preferred Location")
        verbose_name_plural = _("Driver's Preferred Locations")


class DeviceDetails(models.Model):
    os_version = models.CharField(
        _('OS Version'), null=True, blank=True, max_length=10)
    app_name = models.CharField(_('App Name'), null=True,
                                blank=True, max_length=50)
    app_version_name = models.CharField(_('App Version Name'), null=True,
                                        blank=True, max_length=50)
    app_version_code = models.CharField(_('App Version Code'), null=True,
                                        blank=True, max_length=50)
    imei_number = models.CharField(_('IMEI Number'), null=True, blank=True,
                                   max_length=50)
    device_name = models.CharField(_('Device Name'), null=True, blank=True,
                                   max_length=100)
    sim_network = models.CharField(_('Sim Network'), null=True, blank=True,
                                   max_length=100)
    sim_number = models.CharField(_('Sim Number'), null=True, blank=True,
                                  max_length=100)
    sim_sr_number = models.CharField(_('Sim Serial Number'), null=True, blank=True,
                                     max_length=50)
    uuid = models.CharField(_('Universally Unique Identifier'), max_length=255,
                            blank=True, null=True)
    device_id = models.CharField(_('Device ID'), max_length=255, blank=True,
                                 null=True)

    class Meta:
        verbose_name = _("Device Detail")
        verbose_name_plural = _("Device Details")

    def __str__(self):
        return "%s | %s" % (self.imei_number, self.device_name)


class LoadPreference(BaseModel):
    name = models.CharField(_('Load'), max_length=50)
    level = models.IntegerField(_("Level of Load"), default=0)

    class Meta:
        verbose_name = _('Load Preference')
        verbose_name_plural = _('Load Preferences')

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s" % self.name


class Driver(BaseModel):
    """
    Main Driver model for blowhorn
    """

    source = models.CharField(_('Source'), max_length=20,
                              choices=DriverConst.SOURCE_OF_USER_ACCESS, default=DriverConst.USER_ACCESS_WEBSITE)
    referred_by = models.ForeignKey(User, null=True, blank=True, verbose_name='Referred By',
                                    on_delete=models.PROTECT,
                                    related_name='referred_by')
    supply_source = models.CharField(_('Supply Source'), max_length=100, null=True, blank=True)
    date_of_birth = models.DateField(_('Date of Birth'),
                                     null=True, blank=True,
                                     help_text=_("Date of Birth format should be: DD MM YYYY"))

    father_name = models.CharField(
        _("Father's Name"), max_length=255, null=True, blank=True, db_index=True)

    driving_license_number = models.CharField(
        _('Driving License Number'), max_length=50, null=True, blank=True)

    operating_area = models.ForeignKey('address.Area', null=True,
                                       verbose_name='Operating Area',
                                       on_delete=models.PROTECT)

    operating_city = models.ForeignKey('address.City', null=True,
                                       verbose_name='Operating City',
                                       on_delete=models.PROTECT)

    own_vehicle = models.BooleanField(
        _("Own Vehicle?"), default=False, db_index=True)

    is_defcom_driver = models.BooleanField(_('Defcom Drivers'), default=False)

    tos_accepted = models.BooleanField(
        _("Driver Accepted TOS ?"), default=False, db_index=True)

    tos_response_date = models.DateTimeField(null=True, blank=True)

    balance = models.IntegerField(_("Balance"), default=0)

    cod_balance = models.DecimalField(_("cod amount with driver"),
                                      max_digits=10, decimal_places=2,
                                      default=0)
    has_covid_pass = models.BooleanField(
        _("Has Covid Pass ?"), default=False, db_index=True)

    # NullCharField as source data is bad and contains many blanks
    pan = NullCharField(_('PAN'), max_length=10, unique=True,
                        null=True, blank=True, validators=[RegexValidator(
                            regex=r'^[A-Za-z]{5}\d{4}[A-Za-z]{1}$',
                            message=_('First 5 digits, next 4 digits,'
                                      'last Alphabet, fixed length is 10')),
        MinLengthValidator(10)])

    owner_contact_no = PhoneNumberField(
        _("Owner Contact Number"), blank=True)

    owner_details = models.ForeignKey('vehicle.Vendor',
                                      null=True, blank=True,
                                      verbose_name='Vendor/Owner Details',
                                      on_delete=models.PROTECT)

    permanent_address = models.ForeignKey(
        DriverAddress,
        null=True, blank=True,
        verbose_name='Permanent Address',
        on_delete=models.PROTECT,
        related_name='permanent_address')

    current_address = models.ForeignKey(
        DriverAddress,
        null=True, blank=True,
        verbose_name='Current Address',
        on_delete=models.PROTECT,
        related_name='current_address')

    emergency_contact1 = models.ForeignKey(
        AlternateContact,
        null=True,
        blank=True,
        verbose_name='Emergency Contact 1',
        on_delete=models.PROTECT,
        related_name='emergency_contact1')

    emergency_contact2 = models.ForeignKey(
        AlternateContact,
        null=True,
        blank=True,
        verbose_name='Emergency Contact 2',
        on_delete=models.PROTECT,
        related_name='emergency_contact2')

    reference_contact1 = models.ForeignKey(
        AlternateContact,
        null=True,
        blank=True,
        verbose_name='Reference Contact 1',
        on_delete=models.PROTECT,
        related_name='reference_contact1')

    reference_contact2 = models.ForeignKey(
        AlternateContact,
        null=True,
        blank=True,
        verbose_name='Reference Contact 2',
        on_delete=models.PROTECT,
        related_name='reference_contact2')

    photo = models.ImageField(upload_to=generate_file_path,
                              null=True, blank=True, max_length=500)

    # Personal details
    MARRIED, UNMARRIED, OTHERS = ('Married', 'Unmarried', 'Others')
    MARITAL_STATII = (
        (MARRIED, _("Married")),
        (UNMARRIED, _("UnMarried")),
        (OTHERS, _("Others"))
    )

    marital_status = models.CharField(
        _("Marital Status"),
        max_length=15, choices=MARITAL_STATII, blank=True, null=True)

    gender = models.CharField(
        _('Gender'), max_length=15,
        choices=DriverConst.GENDER_CHOICES, default=DriverConst.GENDER_MALE)

    EDUCATIONAL_QUALIFICATION_CHOICES = (
        ('pre_ssc', 'Pre 10th'),
        ('ssc', '10'),
        ('puc', '10 + 2'),
        ('diploma', 'Diploma'),
        ('bachelors', 'Graduate'),
        ('masters', 'Post Graduate'),
        ('doctorate', 'Doctorate'),
        ('Undergraduate', 'Undergraduate'),
        ('Graduate', 'Graduate'),
    )

    partner = models.OneToOneField(Partner, on_delete=models.CASCADE, null=True, blank=True)

    educational_qualifications = models.CharField(
        _('Educational Qualifications'),
        choices=EDUCATIONAL_QUALIFICATION_CHOICES,
        max_length=255, blank=True, null=True)

    driver_rating = models.ForeignKey(
        DriverRating,
        null=True,
        blank=True,
        verbose_name='Driver Rating', on_delete=models.PROTECT)

    bank_account = models.ForeignKey(
        BankAccount,
        null=True,
        blank=True,
        verbose_name='Bank Details', on_delete=models.PROTECT)

    vehicles = models.ManyToManyField(Vehicle, through='DriverVehicleMap')

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    objects = DriverManager()

    status = models.CharField(
        _('Status'), max_length=15,
        choices=StatusPipeline.DRIVER_STATUSES, default=StatusPipeline.SUPPLY, db_index=True)

    active_from = models.DateTimeField(_('Active from'), blank=True, null=True)

    # The bonus configuration for the referrer
    referral_config = models.ForeignKey(
        'driver.DriverReferralConfiguration', on_delete=models.PROTECT, blank=True, null=True)

    # To check if the bonus has been generated for the referrer or not
    bonus_generated = models.BooleanField(
        _("Bonus Generated for the Driver"), default=False)

    # The timestamp when the driver got transitioned to INACTIVE state
    inactivation_timestamp = models.DateTimeField(
        _('Date of Inactivation'), blank=True, null=True)

    date_of_leaving = models.DateTimeField(
        _('Date of Leaving'), blank=True, null=True)

    reason_for_inactive = models.CharField(_('Reason'), max_length=75, null=True, blank=True,
                                           help_text=_("Applicable only for Inactive/Blacklisted status"))

    description = models.TextField(_("Description"), null=True, blank=True,
                                   max_length=100,
                                   help_text=_("Applicable only for Inactive/Blacklisted status"))

    comments = models.TextField(
        _('Comments'), null=True, blank=True, max_length=100)

    # from old driver data
    contract_type = models.CharField(max_length=50, default='Marketplace')

    is_address_same = models.BooleanField(
        _("Permanent address is same as current address"),
        default=False, db_index=True)

    driver_vehicle = models.CharField(
        _('Vehicle'), max_length=25, null=True, blank=True, db_index=True)

    name = models.CharField(
        _('Name'), max_length=255, null=True, blank=True, db_index=True)

    working_preferences = ArrayField(models.CharField(
        _('Working Preference'),
        choices=DriverConst.WORKING_PREFERENCES,
        max_length=100, blank=True, null=True), blank=True, null=True)

    can_drive_and_deliver = models.BooleanField(
        _("Interested In Driving plus Delivery?"),
        default=False)

    can_read_english = models.BooleanField(
        _("Can you read basic english?"),
        default=True)

    can_source_additional_labour = models.BooleanField(
        _("Can source additional labour"),
        default=False)

    work_status = models.CharField(
        _('Present working status?'),
        choices=DriverConst.WORK_STATUS,
        max_length=25, blank=True, null=True)

    capture_wellbeing_status = models.BooleanField(_('Capture Well-Being Status'), default=False)

    device_details = models.OneToOneField(DeviceDetails, on_delete=models.SET_NULL,
                                          null=True)

    is_blowhorn_driver = models.BooleanField(
        _("Blowhorn Driver"), default=False)

    is_marketplace_driver = models.BooleanField(
        _("Marketplace Driver"), default=False
    )

    sanitizer_recvd = models.BooleanField(_("Received hand sanitizer"), default=False)

    # This flag can be used for driver events notifications shown in the driver app in the /eventstatus API
    driver_event = models.BooleanField(default=False)

    prior_monthly_income = models.IntegerField(
        _("Monthly Income Prior Joining Blowhorn"), null=True, blank=True)

    contract = models.ForeignKey('contract.BlowhornContract', on_delete=models.PROTECT,
                                 null=True, blank=True)

    approved_for_bgv = models.BooleanField(
        _("Is approved for Background Verification"), default=False)

    bgv_unapproval_reason = models.CharField(
        _("Reason for Unapproval for verification"),
        max_length=255, null=True, blank=True)

    bgv_status = models.CharField(
        _('Background Verification Status'),
        choices=DriverConst.VERIFICATION_STATUS,
        max_length=40, null=True, blank=True)

    bgv_requested_by = models.ForeignKey(User, null=True, blank=True,
                                         verbose_name='Background Verification Requested By',
                                         on_delete=models.PROTECT,
                                         related_name='bgv_requested_by')

    bgv_reviewer = models.ForeignKey(User, null=True, blank=True, verbose_name='Background Verification Data Reviewer',
                                     on_delete=models.PROTECT,
                                     related_name='bgv_reviewer')

    data_ready_for_bgv = models.BooleanField(
        _("Informations for Background Verification are Present"), default=False)

    bgv_unapproval_reason = models.CharField(_('Reason of Unapproval for Background Verification'), max_length=150,
                                             null=True, blank=True,
                                             help_text=_("In case of unapproval of Background Verification"))
    load_preference = models.ForeignKey(LoadPreference, null=True, blank=True,
                                        verbose_name='Load Preference',
                                        on_delete=models.PROTECT)

    duty_timings = models.CharField(
        _('Duty Timings'),
        choices=DriverConst.DUTY_TIMINGS,
        max_length=60, null=True, blank=True)

    previous_company = models.CharField(
        _('Previous Company'),
        max_length=250, null=True, blank=True)

    # languages_known = models.ManyToManyField(Languages, related_name='languages', null=True, blank=True)
    languages_known = ArrayField(models.CharField(max_length=128, blank=True, null=True), blank=True, null=True)

    last_used_locale = models.CharField(max_length=20, default='en')

    copy_data = CopyManager()

    class Meta:
        verbose_name = _('Driver')
        verbose_name_plural = _('Drivers')

    def __str__(self):
        return "%s | %s" % (self.name, self.driver_vehicle)

    @property
    def avatar_url(self):
        if self.photo:
            return self.photo.url
        return None

    @staticmethod
    def autocomplete_search_fields():
        return 'name', 'driver_vehicle'

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if self.driving_license_number:
            self.driving_license_number = self.driving_license_number.upper()
        if self.pan:
            self.pan = self.pan.upper()

        # Check if this Driving license number already exists
        # if Driver.objects.filter(driving_license_number=self.driving_license_number) \
        #         .exclude(pk=self.pk).exists():
        #     raise ValidationError({
        #         'driving_license_number': _('DL already exists')
        #     })

    def update_driver_log_info(self, action):
        log_data = {
            'driver': self,
            'action': action,
            'action_date': timezone.now()
        }
        driver_log = DriverLogInfo(**log_data)
        driver_log.save()

    def get_bank_account_details(self):
        if self.own_vehicle:
            return self.bank_account
        else:
            return self.owner_details_id and self.owner_details.bank_account

    # def get_financial_year(self):
    #     date = timezone.now().date()
    #     if date.month < 4:
    #         financial_year = str(date.year - 1) + '-' + str(date.year)
    #     else:
    #         financial_year = str(date.year) + '-' + str(date.year + 1)
    #     return financial_year

    # def is_tds_accepted(self):
    #     from blowhorn.driver.util import get_financial_year
    #     financial_year = get_financial_year()
    #     return DriverTds.objects.filter(driver=self, year=financial_year, is_tds_accepted=True).exists()


    def _get_driver_vehicle(self):
        dv_map = DriverVehicleMap.objects.filter(driver=self).first()
        if not dv_map:
            return False
        return dv_map.vehicle

    def get_accepted_trips(self):
        from blowhorn.trip.models import Trip
        return Trip.objects.filter(
            driver=self,
            status=DRIVER_ACCEPTED
        )

    def get_in_progress_trips(self):
        from blowhorn.trip.models import Trip
        return Trip.objects.filter(
            driver=self,
            status__in=[TRIP_IN_PROGRESS, TRIP_ALL_STOPS_DONE]
        )

    def get_assigned_trips(self):
        # return trips assigned to driver
        from blowhorn.trip.models import Trip
        return Trip.objects.filter(
            driver=self,
            status__in=[DRIVER_ACCEPTED, TRIP_VEHICLE_LOAD,
                        TRIP_IN_PROGRESS, TRIP_ALL_STOPS_DONE]
        )

    def completed_trips_without_rating(self):
        # return trips assigned to driver
        from blowhorn.trip.models import Trip
        return Trip.objects.filter(
            driver=self,
            status=TRIP_COMPLETED,
            order__status=StatusPipeline.ORDER_DELIVERED,
            order__order_type=settings.DEFAULT_ORDER_TYPE,
            order__orderrating__rating=0
        ).exclude(current_step=-1)

    def get_pending_payment_c2c_trips(self):
        from blowhorn.trip.models import Trip
        # TODO add credit logic also
        return Trip.objects.filter(
            driver=self,
            status=TRIP_COMPLETED,
            current_step__gt=0,
            order__payment_status='Un-Paid',
            order__order_type='booking'
        )

    # TODO function name needs to be changed
    # TODO check whether to add logic (same day trip) later
    def get_c2c_and_shipment_new_trips(self):
        from blowhorn.trip.models import Trip
        return Trip.objects.filter(
            driver=self, status=TRIP_NEW
        ).exclude(customer_contract__contract_type__in=[CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED])

    # TODO check whether to add logic (same day trip) later
    def get_b2b_trips(self):
        from blowhorn.trip.models import Trip
        return Trip.objects.annotate(
            current_time=Cast(timezone.now(), output_field=DateTimeField()),
            supposed_start_time=ExpressionWrapper(F('planned_start_time') - timedelta(hours=1) * F(
                'customer_contract__hours_before_scheduled_at'), output_field=DateTimeField()),
            supposed_end_time=ExpressionWrapper(F('planned_start_time') + timedelta(hours=1) * F(
                'customer_contract__hours_after_scheduled_at'), output_field=DateTimeField()),
        ).filter(
            driver=self, status=TRIP_NEW,
            customer_contract__contract_type__in=[
                CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED],
            current_time__gte=F('supposed_start_time'),
            current_time__lte=F('supposed_end_time')
        )

    # TODO check whether to add logic (same day trip) later
    def get_new_trips(self):
        return (
            self.get_b2b_trips() | self.get_c2c_and_shipment_new_trips()
        ).order_by('planned_start_time')

    def on_driver_active(self):
        # Create driver instance on Redis.
        if not RedisDriver(self.id)['vehicle']:
            _map = self.drivervehiclemap_set.last()
            if _map and _map.vehicle:
                RedisDriver(self.id)['vehicle'] = {
                    'reg_no': _map.vehicle.registration_certificate_number,
                    'model': _map.vehicle.vehicle_model.model_name,
                    'class': _map.vehicle.vehicle_model.vehicle_class.commercial_classification
                }

    def on_driver_inactive(self):
        # Remove driver instance from Redis
        RedisDriver(self.id).delete(force=True)

    def get_rating(self):
        """
        :returns avg rating value
        """
        result = DriverRatingDetails.objects.filter(
            driver=self
        ).aggregate(
            sum=Sum('rating'), count=Count('id'))
        reviews_sum = result['sum'] or 0
        reviews_count = result['count'] or 0
        rating = None
        if reviews_count > 0:
            rating = float(reviews_sum) / reviews_count
        return rating, reviews_count

    def save(self, *args, **kwargs):
        _request = current_request()
        driver_list = []
        current_user = _request.user if _request else ''

        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')

                if field_name in ['status', 'driver_vehicle']:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                    if old_value != current_value:

                        if field_name == "status":
                            if current_value == StatusPipeline.ACTIVE:
                                pass
                                self.on_driver_active()
                            else:
                                self.on_driver_inactive()

                        driver_list.append(DriverHistory(field=field_name, old_value=str(old_value),
                                                         new_value=current_value, user=current_user,
                                                         driver=old))

            DriverHistory.objects.bulk_create(driver_list)

        return super().save(*args, **kwargs)


class DriverDocument(Document):

    driver = models.ForeignKey(Driver, related_name='driver_document',
                               on_delete=models.PROTECT)

    non_transport_validity = models.DateField(_('Non Transport Validity Till'),
                                    blank=True, null=True)

    transport_validity = models.DateField(_('Transport Validity Till'),
                                    blank=True, null=True)

    issue_date = models.DateField(_('Issue Date'),
                                    blank=True, null=True)

    class Meta:
        verbose_name = _('Driver document')
        verbose_name_plural = _('Driver documents')

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s" % self.file

    def get_title(self):
        """
        Return driver's name
        """
        return self.driver.user.name

    get_title.short_description = pgettext_lazy(u"Driver Name", u"Name")


class DriverDocumentPage(DocumentPage):
    driver_document = models.ForeignKey(DriverDocument, related_name='driver_document_pages',
                                        on_delete=models.PROTECT)

    def __str__(self):
        return u"%s" % self.file

    class Meta:
        verbose_name = _('Driver document Page')
        verbose_name_plural = _('Driver Document Pages')


class DriverGPSInformation(BaseModel):
    driver = models.ForeignKey(Driver, related_name='driver_gps',
                               on_delete=models.PROTECT)

    trip = models.ForeignKey('trip.Trip',  on_delete=models.PROTECT, null=True)

    device_timestamp = models.DateTimeField(_('Device TimeStamp'))

    is_gps_on = models.BooleanField(_('Is Gps On'),  default=False)

    network_ontime = models.DateTimeField(_('Network OnTime'), null=True, blank=True)

    network_offtime = models.DateTimeField(_('Network OffTime'), null=True, blank=True)

    # description = models.CharField(_('Description'), max_length=250)

    class Meta:
        verbose_name = _('Driver GPS ')
        verbose_name_plural = _('Driver GPS')

    def __str__(self):
        return str(self.driver)


class DriverVehicleMap(models.Model):
    """
    Vehicles and Drivers association.
    """
    history = AuditlogHistoryField()

    driver = models.ForeignKey(Driver, null=True, on_delete=models.PROTECT)
    vehicle = models.ForeignKey(Vehicle, null=True, on_delete=models.PROTECT)

    def clean(self, *args, **kwargs):
        driver_vehicle = DriverVehicleMap.objects.filter(
            vehicle=self.vehicle, driver__isnull=False)

        # if len(driver_vehicle) > 1:
        #     raise ValidationError({
        #         'More than 1 driver attached to this vehicle'
        #     })
        driver_vehicle = driver_vehicle.first()
        # Check if this vehicle already exists
        if driver_vehicle:
            if not driver_vehicle.driver == self.driver:
                if driver_vehicle.driver.status != StatusPipeline.INACTIVE:
                    raise ValidationError({
                        'vehicle': (_('Vehicle is already mapped to Driver %s')
                                    % driver_vehicle.driver)
                    })

        super(DriverVehicleMap, self).clean(*args, **kwargs)

    def __str__(self):
        return "(%s, %s)" % (self.vehicle, self.driver)

    def save(self, *args, **kwargs):

        _request = current_request()
        current_user = _request.user if _request else ''

        driver_vehicle = DriverVehicleMap.objects.filter(
            vehicle=self.vehicle, driver__isnull=False).first()

        if driver_vehicle and not driver_vehicle.driver == self.driver:
            DriverHistory.objects.create(field='driver_vehicle',
                                         old_value=str(
                                             driver_vehicle.vehicle.registration_certificate_number),
                                         new_value=None, user=current_user, driver=driver_vehicle.driver)

            Driver.objects.filter(id=driver_vehicle.driver_id).update(
                driver_vehicle=None)
            driver_vehicle.delete()

        super().save(*args, **kwargs)
        self.driver.driver_vehicle = self.vehicle.registration_certificate_number
        self.driver.save()
        self.on_vehicle_change()

    def __update_driver_vehicle_redis(self, driver, vehicle):
        """ Updates vehicle details of driver instance on Redis.
        """

        RedisDriver(driver.id)['vehicle'] = {
            'reg_no': vehicle.registration_certificate_number,
            'model': vehicle.vehicle_model.model_name,
            'class': vehicle.vehicle_model.vehicle_class.
                commercial_classification
        }

    def on_vehicle_change(self):
        """ Any events to be triggered when a driver vehicle has got updated.
        """

        self.__update_driver_vehicle_redis(self.driver, self.vehicle)

    class Meta:
        verbose_name = _('Driver Vehicle Mapping')
        verbose_name_plural = _('Driver Vehicle Mapping')


class DriverActivity(models.Model):
    """
    Capturing the Driver Activity, location, associated trips
    and other device details
    """
    driver = models.OneToOneField(Driver, on_delete=models.SET_NULL, null=True)

    # **WARNING: Do not use following ForeignKey relations. Will be removed soon.
    trip = models.ForeignKey('trip.Trip', null=True, blank=True,
                             on_delete=models.SET_NULL)
    stop = models.ForeignKey('trip.Stop', null=True, blank=True,
                             on_delete=models.SET_NULL)
    order = models.ForeignKey('order.Order', null=True, blank=True,
                              on_delete=models.SET_NULL)

    # New values will be stored as string to avoid multiple queries
    # on each location update
    trip_str = models.CharField(
        _('Trip ID'), max_length=40, null=True, blank=True)
    stop_str = models.CharField(
        _('Stop ID'), max_length=40, null=True, blank=True)
    order_str = models.CharField(
        _('Order ID'), max_length=40, null=True, blank=True)

    idle, occupied, OTHERS = ('idle', 'occupied', 'Others')
    DRIVER_EVENT_TYPES = (
        (idle, _("IDLE")),
        (occupied, _("OCCUPIED")),
        (OTHERS, _("Others"))
    )
    event = models.CharField(_("event"), max_length=30,
                             null=True, choices=DRIVER_EVENT_TYPES)
    geopoint = models.PointField()
    created_time = models.DateTimeField(auto_now_add=True)
    updated_time = models.DateTimeField(auto_now=True)
    gps_timestamp = models.DateTimeField()
    device_timestamp = models.DateTimeField()
    location_accuracy_meters = models.FloatField()
    vehicle_speed_kmph = models.FloatField(null=True)
    other_details = JSONField(blank=True, null=True)
    orientation_from_north = models.FloatField(_('Vehicle Direction'),
                                               help_text='Vehicle marker orientation (angle in degrees)', blank=True,
                                               null=True, default=0)

    class Meta:
        verbose_name = _('Driver Activity')
        verbose_name_plural = _('Driver Activity')


# @architect.install('partition', type='range', subtype='date', constraint='day', column='created_time')
class DriverActivityHistory(models.Model):
    """
    Capturing the history of Driver Activity captured,
    including his location, device details
    """
    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    # **WARNING: Do not use following ForeignKey relations. Will be removed soon.
    trip = models.ForeignKey('trip.Trip', null=True, blank=True,
                             on_delete=models.SET_NULL)
    stop = models.ForeignKey('trip.Stop', null=True, blank=True,
                             on_delete=models.SET_NULL)
    order = models.ForeignKey('order.Order', null=True, blank=True,
                              on_delete=models.SET_NULL)

    # New values will be stored as string to avoid multiple queries
    # on each location update
    trip_str = models.CharField(
        _('Trip'), max_length=40, null=True, blank=True)
    stop_str = models.CharField(
        _('Stop'), max_length=40, null=True, blank=True)
    order_str = models.CharField(_('Order'), max_length=40, null=True,
                                 blank=True)

    event = models.CharField(max_length=30, null=True)
    geopoint = models.PointField()
    created_time = models.DateTimeField(auto_now_add=True)
    gps_timestamp = models.DateTimeField()
    device_timestamp = models.DateTimeField()
    location_accuracy_meters = models.FloatField()
    vehicle_speed_kmph = models.FloatField(null=True)
    other_details = JSONField(blank=True, null=True)
    orientation_from_north = models.FloatField(_('Vehicle Direction'),
                                               help_text='Vehicle marker orientation (angle in degrees)', blank=True,
                                               null=True, default=0)

    class Meta:
        verbose_name = _('Driver Activity History')
        verbose_name_plural = _('Driver Activity History')


class DriverLogInfo(models.Model):
    """
    Capturing the Driver login, logout, on duty and off duty status change time
    """
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    action = models.CharField(
        _('Action'), max_length=25)

    action_date = models.DateTimeField(
        _('Action Date'), blank=True, null=True)

    processed = models.BooleanField(
        _('Record Processed'), default=False, db_index=True)

    last_plugged_at = models.DateTimeField(
        _('Last plugged device for charging at'), blank=True, null=True)

    last_unplugged_at = models.DateTimeField(
        _('Last unplugged device for charging at'), blank=True, null=True)

    battery = models.PositiveIntegerField(
        _("Battery Level"), blank=True, null=True)

    charging_status = models.BooleanField(
        _('Charging Status'), default=False, blank=True)

    significant_level = models.CharField(
        _('Significant level of battery'), max_length=50, null=True, blank=True)

    charging_mode = models.CharField(
        _('Charging Mode'), max_length=50, null=True, blank=True)

    extra_details = JSONField(blank=True, null=True)

    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.driver

    class Meta:
        verbose_name = _('Driver Log Info')
        verbose_name_plural = _('Driver Log Info')


class DriverLogBatch(models.Model):
    """
    Batch to aggregate bulk drive login info processing for distance calculation
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    number_of_entries = models.IntegerField(
        _("Num of entries"), null=True, blank=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Driver Log Info Batch")
        verbose_name_plural = _("Driver Log Info Batch")


class DriverLogActivity(models.Model):
    """
    capture distance travelled by driver during a login interval
    """
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    login_time = models.DateTimeField(
        _('Login Time'), blank=True, null=True)
    logout_time = models.DateTimeField(
        _('Logout Time'), blank=True, null=True)
    gps_distance = models.DecimalField(
        _('Distance Travelled (Kms)'), max_digits=10,
        decimal_places=2, blank=True, null=True)
    batch = models.ForeignKey(DriverLogBatch, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.driver

    class Meta:
        verbose_name = _('Driver Log Activity')
        verbose_name_plural = _('Driver Log Activities')


class DriverConstants(models.Model):
    """
    Capturing the constants related to drivers.
    """
    history = AuditlogHistoryField()

    name = models.CharField(max_length=50)
    value = models.CharField(max_length=20)
    description = models.CharField(max_length=200, blank=True, null=True)

    def get(self, name, default):
        try:
            constant = DriverConstants._default_manager.get(name=name)
        except DriverConstants.DoesNotExist:
            return default

        return constant.value

    class Meta:
        verbose_name = _('Driver Constants')
        verbose_name_plural = _('Driver Constants')


class DriverPayment(ConcurrentTransitionMixin, BaseModel):
    """
    Driver Payment main model
    """
    PAYMENT_STATUSES = (
        (WITHHELD, 'Withheld'),
        (APPROVED, 'Approved'),
        (UNAPPROVED, 'Unapproved'),
        (REVERTED, 'Reverted'),
        (ADJUSTED, 'Adjusted'),
        (CLOSED, 'Closed'),
        (SUSPENDED, 'Suspended'),
    )

    PAYMENT_CHOICES = (
        ('Fixed', 'Fixed'),
        ('Bonus', 'Bonus'),
    )

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    owner_details = models.ForeignKey('vehicle.Vendor',
                                      null=True, blank=True,
                                      verbose_name='Vendor/Owner Details',
                                      on_delete=models.PROTECT)

    contract = models.ForeignKey('contract.Contract', on_delete=models.PROTECT,
                                 null=True, blank=True)

    buy_rate = models.ForeignKey('contract.BuyRate', null=True, blank=True,
                                 on_delete=models.PROTECT)

    sell_rate = models.ForeignKey('contract.SellRate', null=True, blank=True,
                                  on_delete=models.PROTECT)

    batch = models.ForeignKey('PaymentBatch', null=True, blank=True,
                              on_delete=models.PROTECT)

    payment_type = models.CharField(_('Payment Type'),
                                    max_length=25, choices=PAYMENT_CHOICES,
                                    null=True, blank=True, db_index=True)

    is_settled = models.BooleanField(
        _("Is Payment Record Settled"), default=False, db_index=True)

    status = FSMField(default=UNAPPROVED, protected=False, db_index=True)

    absent_days = models.IntegerField(_('Days Absent'), default=0)

    is_stale = models.BooleanField(
        _("Is Payment Record Stale"), default=False, db_index=True)

    stale_reason = models.CharField(_('Reason For Stale'), max_length=256,
                                    null=True, blank=True)

    can_approve = models.BooleanField(
        _("Can the Payment be Approved"), default=True, db_index=True)

    start_datetime = models.DateField(_('From'))

    end_datetime = models.DateField(_('Until'))

    total_distance = models.DecimalField(_('Total Distance (kms)'),
                                         max_digits=10, decimal_places=2,
                                         null=True, blank=True)

    total_time = models.DecimalField(_('Total Time (mins)'),
                                     max_digits=10, decimal_places=2,
                                     null=True, blank=True)

    total_shipment_assigned = models.IntegerField(
        _('Shipment Delivered'), default=0)

    total_shipment_delivered = models.IntegerField(
        _('Shipment Delivered'), default=0)

    total_shipment_attempted = models.IntegerField(
        _('Shipment Assigned'), default=0)

    total_shipment_rejected = models.IntegerField(
        _('Shipment Rejected'), default=0)

    total_package_returned = models.IntegerField(
        _('Shipment Returned'), default=0)

    total_package_pickups = models.IntegerField(
        _('Shipment Pickups'), default=0)

    total_package_delivered_cash = models.IntegerField(
        _('Shipment Delivered Cash'), default=0)

    total_package_delivered_mpos = models.IntegerField(
        _('Shipment Delivered Mops'), default=0)

    base_payment = models.DecimalField(_('Base Payment'),
                                       max_digits=10, decimal_places=2,
                                       null=True, blank=True)

    distance_payment = models.DecimalField(_('Distance Slab Pay'),
                                           max_digits=10, decimal_places=2,
                                           null=True, blank=True)

    weight_payment = models.DecimalField(_('Weight Slab Pay'),
                                          max_digits=10, decimal_places=2,
                                          null=True, blank=True)

    volume_payment = models.DecimalField(_('Volume Slab Pay'),
                                          max_digits=10, decimal_places=2,
                                          null=True, blank=True)

    time_payment = models.DecimalField(_('Time Slab Pay'),
                                       max_digits=10, decimal_places=2,
                                       null=True, blank=True)

    total_shipment_amount = models.DecimalField(_('Shipment Amount'),
                                                max_digits=10,
                                                decimal_places=2, default=0)

    # total_earnings = base_payment + distance_payment +
    #                   time_payment + total_shipment_amount
    total_earnings = models.DecimalField(_('Total Earnings'),
                                         max_digits=10, decimal_places=2,
                                         help_text=_(
                                             "Base Payment + Distance + Time + Shipment"),
                                         null=True, blank=True)

    leave_deduction = models.DecimalField(_('Leaves deduction'),
                                          max_digits=10, decimal_places=2,
                                          null=True, blank=True)

    total_deductions = models.DecimalField(_('Total Deductions'),
                                           max_digits=10, decimal_places=2,
                                           null=True, blank=True)

    adjustment = models.DecimalField(_('Adjustment Amount'),
                                     max_digits=10,
                                     decimal_places=2, default=0)

    labour_cost = models.DecimalField(_('Labour Cost'),
                                      max_digits=10, decimal_places=2,
                                      default=0)

    toll_charges = models.DecimalField(_('Toll Charges'),
                                       max_digits=10, decimal_places=2,
                                       default=0)

    parking_charges = models.DecimalField(_('Parking Charges'),
                                       max_digits=10, decimal_places=2,
                                       default=0)

    # gross_pay = (total_earnings - total_deductions) + adjustments
    gross_pay = models.DecimalField(_('Gross Pay'),
                                    max_digits=10, decimal_places=2,
                                    help_text=_(
                                        "Earnings + Adjustment + Labour + Toll - Deductions"),
                                    null=True, blank=True)

    cash_paid = models.DecimalField(_('Cash Paid to Driver'),
                                    max_digits=10,
                                    decimal_places=2, default=0)

    cash_collected = models.DecimalField(_('Cash Collected'),
                                         max_digits=10,
                                         decimal_places=2, default=0)

    tip_amount = models.DecimalField(_('Tips'), max_digits=10, decimal_places=2, default=0)

    excess_deduction = models.DecimalField(_('Excess Deduction Amount'), default=0.0, null=True,
                                           blank=True, max_digits=10, decimal_places=2)

    # incase if driver balance is less then net pay we will pay the driver balance
    # and the amount can be store here . if reversal happen we need to reverse
    # only paid amount.
    settled_amount = models.DecimalField(_('Settled Amount'),
                                         help_text=_(
                                             "Amount settled for this payment "),
                                         max_digits=10,
                                         decimal_places=2, default=0)

    # net_pay = gross pay - (cash paid + Cash collected)
    net_pay = models.DecimalField(_('Net Payable'),
                                  help_text=_(
                                      "Gross Pay + Cash Paid + Cash Collected "),
                                  max_digits=10,
                                  decimal_places=2, default=0)

    # Json data stored as text dump. Not using JSONField, as it encodes
    # the data upon each save, adding excape characters repeatedly
    calculation = models.TextField(blank=True, null=True)

    # to mark the payment settled when two times 50% setttlement is made
    is_fifty_percent_settlement = models.BooleanField(
        _("Is fifty percent settled"), default=False, db_index=True)

    objects = models.Manager()

    copy_data = CopyManager()

    def __str__(self):
        return str(self.id)

    @transition(field=status, source=[WITHHELD], target=UNAPPROVED,
                permission=is_payment_supervisor)
    def release(self):
        print("release")

    @transition(field=status, source=UNAPPROVED, target=WITHHELD,
                permission=is_payment_spoc_or_supervisor)
    def withhold(self):
        print("withhold")

    @transition(field=status, source=[WITHHELD, UNAPPROVED], target=SUSPENDED,
                permission=is_super_user)
    def suspend(self):
        print("SUSPENDED")

    @transition(field=status, source=SUSPENDED, target=UNAPPROVED,
                permission=is_super_user)
    def undo_suspend(self):
        print("unsuspend")

    @transition(field=status, source=UNAPPROVED, target=APPROVED,
                permission=is_payment_spoc_or_supervisor,
                conditions=[can_approve_payment, is_current_user_can_approve])
    def approve(self, *args, **kwargs):
        print(args, kwargs)
        DriverLedger(
            driver=self.driver,
            owner_details=self.owner_details,
            amount=self.gross_pay,
            description='%s, %s - %s' % (
                self.contract,
                self.start_datetime.strftime(DATE_FORMAT),
                self.end_datetime.strftime(DATE_FORMAT)
            ),
            ops_reference=self,
        ).save()

        # Show cash paid component separately
        total_amount_collected = self.cash_paid + self.cash_collected
        if total_amount_collected > 0.0:
            DriverLedger(
                driver=self.driver,
                amount=-total_amount_collected,
                description='Advance Cash Paid + Cash Collected-%s, %s - %s' % (
                    self.contract,
                    self.start_datetime.strftime(DATE_FORMAT),
                    self.end_datetime.strftime(DATE_FORMAT)
                ),
                ops_reference=self,
            ).save()

        from .mixins import NotificationMixin
        if self.net_pay <= 0:
            self.is_settled=True
        else:
            NotificationMixin.send_notification(DriverConst.NOTIFICATION_GROUP_PAYMENT_APPROVED,
                self.driver.id, message_parms=[self.net_pay,settings.OSCAR_DEFAULT_CURRENCY])

        if self.total_deductions and self.total_deductions != 0:
            NotificationMixin.send_notification(DriverConst.NOTIFICATION_GROUP_TRIP_DEDUCTION,
                self.driver.id, message_parms=[self.total_deductions,settings.OSCAR_DEFAULT_CURRENCY])
        print("approve")

    @transition(field=status, source=[UNAPPROVED, WITHHELD], target=CLOSED,
                permission=is_payment_spoc_or_supervisor)
    def close(self):
        print("close")

    @transition(field=status, source=CLOSED, target=UNAPPROVED,
                permission=is_super_user)
    def reopen(self):
        print("reopen")

    @transition(field=status, source=APPROVED, target=UNAPPROVED,
                permission=is_payment_supervisor,
                conditions=[can_unapprove_payment])
    def unapprove(self):
        # Reverse action on the net_pay and not on gross pay
        # otherwise -gross_pay + cash_paid
        DriverLedger(
            driver=self.driver,
            amount=-self.net_pay,
            description='Approval-Reversal %s, %s - %s' % (
                self.contract,
                self.start_datetime.strftime(DATE_FORMAT),
                self.end_datetime.strftime(DATE_FORMAT)
            ),
            ops_reference=self,
        ).save()
        print("unapprove")

    @transition(field=status, source=[ADJUSTED, UNAPPROVED], target=ADJUSTED,
                permission=is_payment_spoc, custom=dict(admin=False))
    def adjust(self):
        print("adjust")

    @transition(field=status, source=ADJUSTED, target=UNAPPROVED,
                permission=is_payment_supervisor)
    def approve_by_supervisor(self):
        print("approve_by_supervisor")

    def save(self, *args, **kwargs):
        # gross pay is derived using this formula((total_earnings - total_deductions) + adjustment)
        # net pay is derived using this formula((gross pay - cash paid) +
        # adjustment)
        self.gross_pay = (float(self.total_earnings) - (float(self.total_deductions))) \
                         + float(self.adjustment) + float(self.labour_cost or 0) + \
                         float(self.toll_charges or 0) + float(self.parking_charges or 0) + float(
                            self.excess_deduction) + float(self.tip_amount)

        self.net_pay = float(self.gross_pay) - \
            (float(self.cash_paid) + float(self.cash_collected))

        payment_list = []
        _request = current_request()
        current_user = _request.user if _request else ''

        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')

                if field_name == 'status':
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                    if old_value != current_value:
                        payment_list.append(DriverPaymentHistory(field=field_name, old_value=str(old_value),
                                                                 new_value=current_value, user=current_user, payment=old))

            DriverPaymentHistory.objects.bulk_create(payment_list)

        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('Driver Payment')
        verbose_name_plural = _('Driver Payments')
        indexes = (
            BrinIndex(fields=['start_datetime', 'start_datetime']),
        )


class DriverPaymentHistory(models.Model):

    """
    Capturing the driver payment status change history
    """
    payment = models.ForeignKey(DriverPayment, null=True, blank=True, on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    old_value = models.CharField(max_length=50, null=True, blank=True)
    new_value = models.CharField(max_length=50, null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Driver Payment History')
        verbose_name_plural = _('Driver Payment History')

    def __str__(self):
        return '%s' % self.pk


class PaymentBatch(models.Model):
    """
    Batch to aggregate bulk inserts for Payments
    """
    start_time = models.DateTimeField(_("Batch Start"), auto_now_add=True)
    description = models.CharField(_("Description"), max_length=100)
    number_of_entries = models.IntegerField(
        _("Num of entries"), null=True, blank=True)
    records_updated = models.IntegerField(
        _("Records Updated"), null=True, blank=True)
    end_time = models.DateTimeField(_("Batch End"), blank=True, null=True)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _("Payment Batch")
        verbose_name_plural = _("Payment Batches")


class DriverPaymentAdjustmentHistory(BaseModel):
    adjustment = models.ForeignKey(
        'driver.DriverPaymentAdjustment', on_delete=models.PROTECT)
    field = models.CharField(max_length=100)
    old_value = models.CharField(max_length=255)
    new_value = models.CharField(max_length=255)
    remarks = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s | %s | %s' % (self.adjustment, self.field, self.created_by)

    class Meta:
        verbose_name = _('Driver Payment Adjustment History')
        verbose_name_plural = _('Driver Payment Adjustment History')


class DriverPaymentAdjustment(BaseModel):
    """
    Driver Adjustment
    """
    NEW = u'New'
    APPROVED = u'Approved'
    REJECTED = u'Rejected'
    ADJUSTMENT_STATUSES = (
        (NEW, 'New'),
        (APPROVED, 'Approved'),
        (REJECTED, 'Rejected'),
    )

    ADJUSTMENT_REASONS = (
        ('Disciplinary Issue', 'Disciplinary Issue'),
        ('Buy Rate Difference', 'Buy Rate Difference'),
        ('Toll Charges', 'Toll Charges'),
        ('Labour Charges', 'Labour Charges'),
        ('Previous Balance', 'Previous Balance'),
        ('Miscellaneous', 'Miscellaneous'),
        ('Parking Fees', 'Parking Fees'),
    )

    status = FSMField(default=NEW, protected=False, db_index=True)

    driver = models.ForeignKey(Driver, null=True, blank=True,
                               on_delete=models.PROTECT)

    contract = models.ForeignKey('contract.Contract', null=True, blank=True,
                                 on_delete=models.PROTECT)

    payment = models.ForeignKey(DriverPayment, null=True, blank=True,
                                on_delete=models.PROTECT)

    adjustment_datetime = models.DateField(
        _('Adjustment Date'), null=True, blank=True, default=timezone.now)

    category = models.CharField(_('Payment Category'),
                                max_length=25, choices=ADJUSTMENT_REASONS,
                                null=True, blank=True, db_index=True)

    description = models.TextField(blank=True, null=True)

    adjustment_amount = models.DecimalField(_('Adjustment Amount'),
                                            max_digits=10,
                                            decimal_places=2, default=0)

    action_owner = models.ForeignKey(
        "users.User", null=True, blank=True, on_delete=models.DO_NOTHING
    )

    objects = models.Manager()

    copy_data = CopyManager()

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _('Driver Payment Adjustment')
        verbose_name_plural = _('Driver Payment Adjustments')

    @property
    def total_adjustment(self):
        """
        Returns the `sum of approved and current adjustment` for the order.
        """
        approved_adj_rec = DriverPaymentAdjustment.objects.filter(
            status=APPROVED,
            payment_id=self.payment_id
        ).aggregate(adjustment_amount=Sum('adjustment_amount'))
        total_approved_adjustment = approved_adj_rec.get('adjustment_amount') or 0
        return total_approved_adjustment + (
            self.adjustment_amount if
            self.status == DriverPaymentAdjustment.NEW else 0)

    @transition(field=status, source=NEW, target=APPROVED,
                # permission=is_adjustment_supervisor,
                conditions=[can_approve_adjustment])
    def approve(self):
        if self.payment:
            sum_ = DriverPaymentAdjustment.objects.filter(payment=self.payment)\
                .filter(Q(status=APPROVED) | Q(pk=self.pk)) \
                .aggregate(Sum('adjustment_amount'))

            total_adjustment = sum_.get('adjustment_amount__sum') or 0
            self.payment.adjustment = total_adjustment
            self.payment.save()
        print("approve")

    @transition(field=status, source=[NEW], target=REJECTED,
                permission=is_adjustment_spoc_or_supervisor)
    def reject(self):
        DriverPaymentAdjustmentHistory.objects.create(
            adjustment=self,
            field='status',
            old_value=self.NEW,
            new_value=self.REJECTED,
            remarks=self.remarks,
            created_by=self.action_by,
            created_date=timezone.now()
        )
        print('Payment adjustment %s rejected' % self.pk)

    @transition(field=status, source=APPROVED, target=NEW,
                permission=is_adjustment_spoc_or_supervisor,
                conditions=[can_unapprove_adjustment])
    def unapprove(self):
        if self.payment:
            sum_ = DriverPaymentAdjustment.objects.filter(payment=self.payment) \
                .exclude(pk=self.pk) \
                .filter(status=self.APPROVED) \
                .aggregate(Sum('adjustment_amount'))

            total_adjustment = sum_.get('adjustment_amount__sum') or 0
            self.payment.adjustment = total_adjustment
            self.payment.save()
        print("unapprove")

class DriverCashPaid(BaseModel):
    """
    Amount Received by the driver in Advance
    """
    NEW = u'New'
    APPROVED = u'Approved'
    REJECTED = u'Rejected'
    ADJUSTMENT_STATUSES = (
        (NEW, 'New'),
        (APPROVED, 'Approved'),
        (REJECTED, 'Rejected'),
    )

    status = FSMField(default=NEW, protected=False, db_index=True)
    driver = models.ForeignKey(Driver, null=True, blank=True,
                               on_delete=models.PROTECT)

    contract = models.ForeignKey('contract.Contract', null=True, blank=True,
                                 on_delete=models.PROTECT)

    payment = models.ForeignKey(DriverPayment, null=True, blank=True,
                                on_delete=models.PROTECT)

    cash_paid_datetime = models.DateField(_('Cash Paid Date'))

    description = models.CharField(max_length=200, blank=True, null=True)

    cash_paid = models.DecimalField(_('Cash Paid Amount'), max_digits=10,
                                    decimal_places=2, default=0,
                                    validators=[MinValueValidator(Decimal('0.01'))])

    proof = models.FileField(upload_to=generate_file_path, null=True,
                             max_length=256)

    def __str__(self):
        return '%s' % self.pk

    @transition(field=status, source=NEW, target=APPROVED,
                permission=is_adjustment_supervisor,
                conditions=[can_approve_adjustment])
    def approve(self):
        if self.payment:
            # Save the total cash paid onto the master payment
            # ORing self.pk since that will not be in approved state yet
            sum_ = DriverCashPaid.objects.filter(payment=self.payment) \
                .filter(Q(status=APPROVED) | Q(pk=self.pk)) \
                .aggregate(Sum('cash_paid'))

            total_cash_paid = sum_.get('cash_paid__sum') or 0
            self.payment.cash_paid = total_cash_paid
            self.payment.save()
        print("approve")

    @transition(field=status, source=NEW, target=REJECTED,
                permission=is_adjustment_spoc_or_supervisor)
    def reject(self):
        print("reject")

    @transition(field=status, source=APPROVED, target=NEW,
                permission=is_adjustment_spoc_or_supervisor,
                conditions=[can_unapprove_adjustment])
    def unapprove(self):
        """ Unapproving an approved Record. Re-calculate the total
            cash paid and update the parent payment record
        """
        if self.payment:
            # Excluding self.pk since that will still be in approved state
            sum_ = DriverCashPaid.objects.filter(payment=self.payment) \
                .exclude(pk=self.pk) \
                .filter(status=APPROVED) \
                .aggregate(Sum('cash_paid'))

            total_cash_paid = sum_.get('cash_paid__sum') or 0
            self.payment.cash_paid = total_cash_paid
            self.payment.save()
        print("unapprove")

    class Meta:
        verbose_name = _('Driver Cash Paid')
        verbose_name_plural = _('Driver Cash Paids')


class DriverPaymentDetails(models.Model):
    """
    Captures line items of one Driver payment record
    """
    payment = models.ForeignKey(DriverPayment, on_delete=models.PROTECT)

    payment_info = JSONField(_('Payment Information'), blank=True, null=True)

    class Meta:
        verbose_name = _('Driver Payment Info')
        verbose_name_plural = _('Driver Payment Info')


class PaymentTrip(models.Model):
    """
    Trips associated with a driver payment record
    """
    payment = models.ForeignKey(DriverPayment, on_delete=models.CASCADE)

    trip = models.ForeignKey('trip.Trip', on_delete=models.PROTECT)

    def __str__(self):
        return u'%s-%s' % (self.payment, self.trip)

    class Meta:
        verbose_name = _('Payment Trip')
        verbose_name_plural = _('Payment Trips')


class NonOperationPayment(BaseModel):
    """
    Non Operation Payment associated with a driver
    """
    from blowhorn.driver.submodels.asset_models import Asset

    NEW = u'New'
    APPROVED = u'Approved'
    REJECTED = u'Rejected'

    status = FSMField(default=NEW, protected=False, db_index=True)

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    customer = models.ForeignKey(Customer, null=True, blank=True,
                                 verbose_name='Business User',
                                 on_delete=models.PROTECT)

    division = models.ForeignKey(CustomerDivision, null=True, blank=True,
                                 verbose_name='Customer Division',
                                 on_delete=models.PROTECT)

    city = models.ForeignKey(City, on_delete=models.PROTECT)

    start_datetime = models.DateField(_('Payment From'), db_index=True)

    end_datetime = models.DateField(_('Payment Until'), db_index=True)

    purpose = models.CharField(
        _("Payment Purpose"),
        max_length=35, choices=DriverConst.NON_OPS_CONST, blank=False, null=False)

    asset = models.ForeignKey(
        Asset, null=True, blank=True, on_delete=models.PROTECT)

    description = models.CharField(max_length=200, blank=True, null=True)

    amount = models.DecimalField(
        _("Total Amount"), decimal_places=2, max_digits=12, default=0)
    is_settled = models.BooleanField(
        _("Is Settled"), default=False, db_index=True)

    def _update_asset_balance(self, amount):
        """
        Updates balance in asset
        :param amount: Amount of NonOperationPayment (Decimal)
        """
        asset = Asset.objects.select_for_update() \
            .get(pk=self.asset.pk)
        balance = float(self.asset.balance) + float(amount)
        asset.balance = balance
        if balance == 0:
            # @todo: need to handle reversal by fetching old status
            asset.lending_status = RECOVERED
        asset.save()

    @transition(field=status, source=NEW, target=APPROVED,
                permission=is_nop_payment_approver,
                conditions=[is_nops_payment_approvable])
    def approve(self):
        DriverLedger(
            driver=self.driver,
            amount=self.amount,
            description='%s, %s - %s' % (
                self.purpose,
                self.start_datetime.strftime(DATE_FORMAT),
                self.end_datetime.strftime(DATE_FORMAT)
            ),
            nops_reference=self,
        ).save()
        if self.amount <= 0:
            self.is_settled=True

        if self.purpose == 'asset':
            self._update_asset_balance(self.amount)
        print("approve")

    @transition(field=status, source=NEW, target=REJECTED,
                permission=is_nop_payment_approver)
    def reject(self):
        if self.purpose == 'asset':
            self._update_asset_balance(-self.amount)
        print("reject")

    @transition(field=status, source=APPROVED, target=NEW,
                permission=is_nop_payment_unapprover,
                conditions=[is_eligible_for_unapprovable])
    def unapprove(self):
        DriverLedger(
            driver=self.driver,
            amount=-self.amount,
            description='Approval-Reversal %s, %s - %s' % (
                self.purpose,
                self.start_datetime.strftime(DATE_FORMAT),
                self.end_datetime.strftime(DATE_FORMAT)
            ),
            nops_reference=self,
        ).save()
        if self.purpose == 'asset':
            self._update_asset_balance(-self.amount)
        print("unapprove")

    class Meta:
        verbose_name = _('Non Operation Payment')
        verbose_name_plural = _('Non Operation Payments')

    def __str__(self):
        return '%s' % self.driver


class DriverLedgerBatch(BaseModel):
    """
    Model to generate and store batch when bulk operations are performed.
    """
    transaction_time = models.DateTimeField(auto_now_add=True)
    description = models.CharField(_("Description"), max_length=250)
    settlement_file = models.TextField(null=True)
    number_of_entries = models.IntegerField(_("Num of entries"), null=True)
    total_amount = models.DecimalField(_('Total Amount'), null=True,
                                       max_digits=10, decimal_places=2)

    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name_plural = _('Driver Ledger Batches')


class DriverPaymentSettlementView(DriverPayment):

    class Meta:
        proxy = True
        verbose_name = _('Payment Settlement View')
        verbose_name_plural = _('Payment Settlement View')


class DriverPaymentSettlementReversalView(DriverPayment):

    class Meta:
        proxy = True
        verbose_name = _('Payment Reversal View')
        verbose_name_plural = _('Payment Reversal View')


class DriverBalance(BaseModel):
    """
    Driver Balance Amount
    """
    driver = models.OneToOneField(Driver, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return "%s" % self.driver


class DriverLedger(BaseModel):
    """
    Driver Ledger with all his transactions and settlements
    """

    SETTLED = u'Settled'
    REVERSED = u'Reversed'

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    owner_details = models.ForeignKey('vehicle.Vendor',
                                      null=True, blank=True,
                                      verbose_name='Vendor/Owner Details',
                                      on_delete=models.PROTECT)

    order = models.ForeignKey('order.Order', null=True,
                              blank=True, on_delete=models.PROTECT)
    driver_balance = models.ForeignKey(DriverBalance, null=True, default=None,
                                       on_delete=models.PROTECT)
    transaction_time = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(_('Amount'), max_digits=10, decimal_places=2)
    balance = models.DecimalField(
        _('Balance'), max_digits=10, decimal_places=2)
    description = models.CharField(_("Description"), max_length=250)
    ops_reference = models.ForeignKey(DriverPayment, null=True, blank=True,
                                      on_delete=models.PROTECT)
    deduction_reference = models.ForeignKey('driver.DriverDeduction', null=True, blank=True,
                                      on_delete=models.PROTECT)
    nops_reference = models.ForeignKey(
        NonOperationPayment, null=True, blank=True,
        on_delete=models.PROTECT)
    batch = models.ForeignKey(DriverLedgerBatch, null=True, blank=True,
                              on_delete=models.PROTECT)
    is_settlement = models.BooleanField(
        _("Is Settlement"), default=False, db_index=True)
    bank_account = models.CharField(
        _("Settlement Account"), max_length=100, null=True)
    bank_details = models.ForeignKey(
        BankAccount,
        null=True,
        blank=True,
        verbose_name='Bank Details', on_delete=models.PROTECT)
    status = FSMField(default=None, null=True, protected=False, db_index=True)

    def __str__(self):
        return str(self.id)

    def _add_asset_transaction(self, amount):
        """
        Add a transition record in asset
        """
        _request = current_request()
        current_user = _request.user if _request else ''
        AssetTransaction.objects.create(
            txn_reference=self.nops_reference,
            amount_collected=amount,
            associate=current_user,
            remarks=self.nops_reference.description,
            asset=self.nops_reference.asset
        )

    @transition(field=status, source='*', target=REVERSED,
                permission=is_finance_user,
                conditions=[lambda x: x.is_settlement and x.status is None]
                )
    def reverse_settlement(self):
        print("reverse settlement")
        DriverLedger(
            driver=self.driver,
            amount=-self.amount,
            ops_reference=self.ops_reference if self.ops_reference else None,
            nops_reference=self.nops_reference if self.nops_reference else None,
            description='Bank-Reversal. Settlement-%s' % self.pk
        ).save()

        if self.ops_reference:
            if self.ops_reference.is_settled:
                self.ops_reference.is_settled = False
            elif self.ops_reference.is_fifty_percent_settlement:
                self.ops_reference.is_fifty_percent_settlement = False

            self.ops_reference.save()

        if self.nops_reference:
            self.nops_reference.is_settled = False
            self.nops_reference.save()
        if self.nops_reference and \
                self.nops_reference.purpose == 'asset':
            self._add_asset_transaction(-self.amount)

    @transaction.atomic()
    def save(self, *args, **kwargs):
        # Whenever new Driver ledger entry is being added (pk is None)
        # Find the previous balance and add it to the new transaction
        # amount and update the new balance
        if not self.pk:
            # Handle conrurrency with pessimistic row lock
            ledger_created = False
            for i in range(10):
                try:
                    with transaction.atomic():
                        last_transaction = DriverLedger.objects \
                            .select_for_update(nowait=True) \
                            .filter(driver=self.driver) \
                            .order_by('-transaction_time') \
                            .first()
                        if last_transaction:
                            new_balance = Decimal(last_transaction.balance) + Decimal(self.amount)
                        else:
                            new_balance = self.amount
                        self.balance = new_balance
                        # Also update in the driver balance Table the latest balance
                        # for this driver.
                        db_balance, created = DriverBalance.objects.get_or_create(
                                driver=self.driver)
                        db_balance.amount = self.balance
                        db_balance.save()
                        self.driver_balance = db_balance
                        if self.nops_reference and \
                            self.nops_reference.purpose == 'asset':
                            self._add_asset_transaction(-self.amount)
                        ledger_created = True
                        break

                except:
                    print('Record found locked, retry fetching latest driver ledger record')
                    time.sleep(1)
                    continue
            if not ledger_created:
                raise Exception("Something went wrong, please try again!")
        super().save(*args, **kwargs)

    @staticmethod
    def add_transaction(driver, amount, description=None, ops_reference=None):
        # Handle concurrency with pessimistic row lock
        with transaction.atomic():
            last_transaction = DriverLedger.objects \
                .select_for_update() \
                .filter(driver=driver) \
                .order_by('-transaction_time') \
                .first()
            if last_transaction:
                new_balance = last_transaction.balance + amount
            else:
                new_balance = amount

            driver_balance = DriverBalance(
                driver=driver,
                amount=new_balance,
            ).save()

            DriverLedger(
                driver=driver,
                driver_balance=driver_balance,
                amount=amount,
                balance=new_balance,
                description=description,
                ops_reference=ops_reference,
            ).save()

            # if self.nops_reference and \
            #         self.nops_reference.purpose == 'asset':
            #     self._add_asset_transaction(self.amount)

    class Meta:
        verbose_name = _('Driver Ledger')
        verbose_name_plural = _('Driver Ledger')
        indexes = (
            BrinIndex(fields=['transaction_time']),
        )


class DriverDeduction(models.Model):

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    partner = models.ForeignKey('customer.Partner',  on_delete=models.PROTECT)

    deduction_amount = models.DecimalField(_('Deduction Amount (Rs)'),
                                       max_digits=10, decimal_places=2, default=0)

    reference_id = models.CharField(max_length=50)

    transaction_id = models.CharField(max_length=50, null=True)

    remark = models.CharField(
        _('Remark'), max_length=255, null=True, blank=True)

    deduction_date = models.DateTimeField(
        _('Deduction Date'))

    created_time = models.DateTimeField(auto_now_add=True)

    status = FSMField(default=CREATED, protected=False, db_index=True)

    class Meta:
        verbose_name = _('Driver Deduction')
        verbose_name_plural = _('Driver Deductions')
        unique_together = ("partner", "reference_id")

    def __str__(self):
        return str(self.partner)

    @transition(field=status, source=[CREATED],
                target=SETTLED)
    def settle(self):
        logging.info("settling amount")

    @transition(field=status, source=[CREATED],
                target=REJECTED)
    def reject(self):
        logging.info("reject amount")


class DriverLoanDetails(models.Model):

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    partner = models.ForeignKey('customer.Partner', on_delete=models.PROTECT)

    created_time = models.DateTimeField(auto_now_add=True)

    is_driver_detail = models.BooleanField(
        _("is driver detail"), default=False)

    is_balance_enquiry = models.BooleanField(
        _("is balance enquiry"), default=False)

    class Meta:
        verbose_name = _('Driver Loan Information')
        verbose_name_plural = _('Driver Loan Information')

    def __str__(self):
        return str(self.partner) +  str(self.driver)


class DriverHistory(models.Model):

    """
    Capturing the Driver history
    """
    driver = models.ForeignKey('driver.Driver', null=True, blank=True, on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    old_value = models.CharField(max_length=250, null=True, blank=True)
    new_value = models.CharField(max_length=250, null=True, blank=True)
    user = models.CharField(max_length=50)
    reason = models.CharField(max_length=255, null=True, blank=True)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Driver History')
        verbose_name_plural = _('Driver History')

    def __str__(self):
        return str(self.driver_id)


from .submodels.asset_models import *
from .submodels.old_payments import OldPayment
from .submodels.driver_submodels import *


class DriverReferrals(BaseModel):
    """
        Table for storing the driver referrals
    """
    # The referred driver
    driver = models.ForeignKey(
        'driver.Driver',
        related_name='driver_referred', on_delete=models.PROTECT)

    # The driver who put the referral
    referred_by = models.ForeignKey(
        'driver.Driver',
        related_name='driver_referred_by', on_delete=models.PROTECT)

    # The bonus configuration for the referrer
    referral_config = models.ForeignKey(
        'driver.DriverReferralConfiguration', on_delete=models.PROTECT)

    # The timestamp when the driver get transitioned to ACTIVE state
    activation_timestamp = models.DateTimeField(
        _('Date of Activation'), blank=True, null=True)

    # To check if the driver is marked inactive
    marked_inactive = models.BooleanField(
        _("Driver Marked Inactive"), default=False)

    # To check if the bonus has been generated for the referrer or not
    bonus_generated = models.BooleanField(
        _("Bonus Generated for the Driver"), default=False)

    # The timestamp when the driver got transitioned to INACTIVE state
    inactivation_timestamp = models.DateTimeField(
        _('Date of Inactivation'), blank=True, null=True)

    class Meta:
        verbose_name = _('Driver Referral')
        verbose_name_plural = _('Driver Referrals')

    def __str__(self):
        return str(self.driver)


class DriverReferralConfiguration(BaseModel):
    """
        This model stores the various combination for referral
        amount: city, vehicle class and
        minimum number of months of driver activation
        Preference order for bonus:
                city and vehicle class
                vehicle class
                city
    """

    # City for which the referral configuration is defined
    city = models.ForeignKey('address.City',
                            verbose_name='City',
                            blank = True, null = True,
                            on_delete=models.PROTECT)

    # Vehicle class for which referral configuration is defined
    vehicle_class = models.ForeignKey('vehicle.VehicleClass',
                                verbose_name = 'Vehicle Class',
                                blank = True, null = True,
                                on_delete=models.PROTECT)

    # Bonus amount to be paid to the referrer
    bonus_amount = models.DecimalField(_('Bonus Amount (Rs)'),
                                       max_digits=5, decimal_places=2, default=0)

    # Minimum number of active months of referred driver
    min_active_duration = models.IntegerField(
        _('Minimum Months of Activation for Driver'), default=3)

    # flag to check if configuration is active to be applicable
    is_active = models.BooleanField(default = True)

    class Meta:
        verbose_name = _('Driver Referral Configuration')
        verbose_name_plural = _('Driver Referral Configurations')

    def __str__(self):
        status = 'ActiveConfiguration' if self.is_active else 'ActiveConfiguration'

        return "%s_%s_%s_%s_%s_%s" % (self.city, self.vehicle_class,
                self.min_active_duration, "Months",
                self.bonus_amount,status)


class BackGroundVerification(BaseModel):
    """
        storing the data of the verified driver
    """
    driver = models.ForeignKey('driver.Driver', on_delete=models.PROTECT)
    reference_id = models.CharField(
        _("Reference ID"), null=True, blank=True, max_length=30)

    document_type = ArrayField(models.CharField(
        _('Documents to be Verified'),
        choices=BETTERPLACE_DOCUMENT_TYPE_CHOICES,
        max_length=100, blank=True, null=True), null=True)

    client_status = models.CharField(
        _("Client Status"),
        max_length=30,
        choices=StatusPipeline.BACKGROUND_VERIFICATION_STATUSES,
        default=StatusPipeline.PENDING, null=True, blank=True)

    # Baseline Personnel Security Standard
    bpss_status = models.CharField(
        _("BPSS Status"),
        max_length=30,
        choices=StatusPipeline.BACKGROUND_VERIFICATION_STATUSES,
        default=StatusPipeline.PENDING, null=True, blank=True)

    source = models.CharField(_('Verification Sent Via'), max_length=20,
                              choices=DriverConst.SOURCE_OF_USER_ACCESS,
                              default=DriverConst.USER_ACCESS_WEBSITE)

    objects = models.Manager()

    copy_data = CopyManager()

    class Meta:
        verbose_name = _('Background Verification')
        verbose_name_plural = _('Background Verifications')


class CourtRecord(models.Model):
    """
        For capturing the various court records for a driver for background verification
    """

    background_verification = models.ForeignKey(
        BackGroundVerification, null=True, blank=True, on_delete=models.CASCADE)

    court_type = models.CharField(
        _("Address Type"),
        max_length=30,
        choices=DriverConst.COURT_TYPES,
        default=DriverConst.DISTRICT_COURT,
        null=True, blank=True)

    state = models.ForeignKey('address.State', null=True,
                              verbose_name='Verifying Court State',
                              on_delete=models.PROTECT)

    name = models.CharField(
        _("Name of the court"), max_length=255, null=True, blank=False,
    )

    case_type = models.CharField(
        _("Case Type"), max_length=255, null=True, blank=False,
    )

    case_number = models.CharField(
        _("Case Number"), max_length=100, null=True
    )

    case_date = models.DateField(_('Case Date'),
                                 null=True, blank=True)
    remark = models.CharField(
        _('Remark'), max_length=255, null=True, blank=True)
    district = models.CharField(
        _('Remark'), max_length=50, null=True, blank=True)
    criminal_status = models.CharField(
        _('Criminal Status'), max_length=50, null=True, blank=True)
    address_type = models.CharField(
        _("Address Type"),
        max_length=30,
        choices=DriverConst.ADDRESS_IDENTIFIER,
        default=DriverConst.CURRENT_ADDRESS,
        null=True, blank=True)
    court_verification_requested_on = models.DateField(_('Court Verification Requested On'),
                                                       null=True, blank=True)
    court_verification_completed_on = models.DateField(_('Court Verification Completed On'),
                                                       null=True, blank=True)

    class Meta:
        verbose_name = _('Court Record')
        verbose_name_plural = _('Court Records')


class DriverError(BaseModel):
    """
        For capturing the various errors occuring in driver app
    """
    from django.contrib.gis.db.models import PointField
    driver = models.ForeignKey('driver.Driver', on_delete=models.CASCADE)
    trip = models.ForeignKey('trip.Trip', null=True, blank=True, on_delete=models.CASCADE)
    stop = models.ForeignKey('trip.Stop', null=True, blank=True, on_delete=models.CASCADE)
    order = models.ForeignKey('order.Order', null=True, blank=True, on_delete=models.CASCADE)
    current_location = PointField(null=True, blank=True)
    traceback = models.TextField(
        _("Traceback"), null=True, blank=True, max_length=10000000)
    url = models.CharField(
        _('Api EndPoint'), max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = _('Driver Error')
        verbose_name_plural = _('Driver Errors')


class DriverTds(BaseModel):
    driver = models.ForeignKey('driver.Driver', on_delete=models.PROTECT, null=True, blank=True)
    vendor = models.ForeignKey('vehicle.Vendor', on_delete=models.PROTECT, null=True, blank=True)
    year = models.CharField(_('Year'), max_length=255, null=True, blank=True)
    is_tds_accepted = models.BooleanField(_("TDS Accepted?"), default=False, db_index=True)
    accepted_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = _("Driver/Fleet Owner  TDS")
        verbose_name_plural = _("Driver/Fleet Owner TDS")
        # unique_together = ("driver", "year")


class CashHandover(BaseModel):
    """
    Table to store amount collected by hub associate, driver will handover COD amount
    """

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT, null=True,
                               blank=True)
    collected_by = models.ForeignKey(User, on_delete=models.PROTECT,
                                     limit_choices_to={'is_staff': True})
    amount = models.DecimalField(_("Amount collected"), max_digits=10,
                                 decimal_places=2, null=True, blank=True)
    hub = models.ForeignKey('address.Hub', on_delete=models.PROTECT, null=True,
                            blank=True)
    trip = models.ForeignKey('trip.Trip', on_delete=models.PROTECT, null=True,
                             blank=True)


class BankMigrationHistory(BaseModel):
    bank_account = models.CharField(_('Target Bank Account'), max_length=255)
    number_of_accounts = models.IntegerField(_('No. of bank accounts merged'),
                                             help_text=_('No. of bank accounts merged'),
                                             default=0)
    log_dump = JSONField(_('Log'), help_text=_('Migration log'))

    class Meta:
        verbose_name = _('Bank Account Migration History')
        verbose_name_plural = _('Bank Account Migration History')

    @property
    def log_dump_json(self):
        if isinstance(self.log_dump, str):
            self.log_dump = json.loads(self.log_dump)
            if isinstance(self.log_dump, str):
                return json.loads(self.log_dump)
        return self.log_dump


class DriverAcknowledgement(BaseModel):

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    trip = models.ForeignKey('trip.Trip', on_delete=models.PROTECT, null=True, blank=True)

    order = models.ForeignKey('order.Order', on_delete=models.PROTECT, null=True, blank=True)

    is_accepted = models.BooleanField(_("is Acknowledged"), default=False)


class EventType(BaseModel):

    key = models.CharField(max_length=15, unique=True, blank=True, null=True)
    event_name = models.CharField(max_length=100, unique=True, blank=False)
    data_type = models.IntegerField(
        _('Data Type'), default=DriverConst.INTEGER,
        choices=DriverConst.EVENT_DATA_TYPES, null=True, blank=True)
    return_type = models.CharField(_('Return Type'), default=DriverConst.INTEGER_, max_length=15,
                                   choices=DriverConst.RETURN_DATA_TYPES, null=True, blank=True)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '%s - %s' % (self.event_name, self.description)

    class Meta:
        verbose_name = _('Well-Being Event Type')
        verbose_name_plural = _('Well-Being Event Types')


class DriverWellBeing(models.Model):
    """
    Capture driver well being details like temperature, Aarogya Sethu app etc
    """

    driver = models.ForeignKey('driver.Driver', on_delete=models.PROTECT)
    event_type = models.ForeignKey('driver.EventType', on_delete=models.PROTECT)
    event_status = models.CharField(max_length=50, blank=False)
    last_captured_on = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return '%s | %s' % (self.driver.name, self.event_type.event_name)

    class Meta:
        verbose_name = _('Driver Well-Being')
        verbose_name_plural = _('Driver Well-Being')


class DriverSourcingOption(models.Model):
    """
        Supply sourcing options for driver will be stored in this model
    """

    source = models.CharField(max_length=100, blank=False)
    is_active = models.BooleanField(default=True)
    display_in_cosmos = models.BooleanField(_('To be shown in Cosmos?'), default=False)
    display_in_partner_app = models.BooleanField(_('To be shown in Partner App?'), default=False)

    def __str__(self):
        return '%s' % self.source
