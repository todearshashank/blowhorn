from django.apps import AppConfig


class DriverConfig(AppConfig):
    name = 'blowhorn.driver'

    def ready(self):
        import blowhorn.driver.signals

