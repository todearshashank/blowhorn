'''
Used to define all the messages used in driver modeule/app
'''

CANNOT_DELETE_ACTIVE_DRIVER_DOC = 'Cannot delete Documents of Active/Onbarding Driver'
MOBILE_NUMBER_MUST_BE_TEN_OF_DIGIT = 'Mobile number must be of 10 digits'
MUST_BE_FOUR_DIGIT_PIN = 'PIN number must be of 4 digits'
MOBILE_NUMBER_ALREADY_REGISTERED = 'Mobile number is already registered'
INACTIVE_REASON_MANDATORY = 'Please select Reason'
INACTIVE_DESCRIPTION_MANDATORY = 'Provide Description for Reason'
DOCUMENTS_ARE_MANDATORY_TO_SET_DRIVER_ACTIVE = 'Please Add Driver Document, Verify and set it as Active'
ALL_DOCUMENT_SHOULD_BE_ACTIVE = "All Driver Documents should be on Active status"
ALL_MANDATORY_DOCUMENT_SHOULD_BE_ADDED = "Add missing mandatory documents"
VEHICLE_IS_MANDATORY_FOR_ACTIVE_DRIVER = 'Vehicle is mandatory for active driver'
CANNOT_DELETE_ACTIVE_DRIVER_VEHICLE = 'Cannot delete active driver vehicle'
ADJUSTMENT_DATE_SHOULD_BE_FALL_UNDER_TRIP_WINDOW = "Ajustment date should fall under trip window of payment"
DRIVER_ALLOCATED_FOR_CONTRACT = "Cannot set Driver as inactive, driver is allocated for contract %s"
RESTRICTED_ACCESS_ON_ACTIVATE = "Only Supply users can activate the drivers"
MANDATORY_FIELD_FOR_ACTIVATION = "Add missing details %s"
