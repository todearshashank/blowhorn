import simplejson as json
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from django_fsm import FSMField, transition

from blowhorn.common.middleware import current_request
from blowhorn.common.models import BaseModel
from blowhorn.common.random_primary import RandomPrimaryIdModel
from .constants import NOT_ISSUED, ISSUED, RECOVERED, LOST, \
    PARTIAL_RECOVERY, ASSET_LENDING_STATUS_CHOICES
from blowhorn.driver.models import Driver
from blowhorn.users.models import User


class AssetCategory(BaseModel):
    name = models.CharField(
        _("Asset Category"), max_length=32)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Asset category")
        verbose_name_plural = _("Asset categories")


class Asset(BaseModel):
    """
    The physical asset that will be allotted to.
    For simplicity quantity is part of this model
    """
    driver = models.ForeignKey(Driver, on_delete=models.PROTECT)

    category = models.ForeignKey(AssetCategory, on_delete=models.CASCADE)

    identifier = models.CharField(
        _("Model/Serial #"), max_length=64, blank=True)

    quantity = models.PositiveIntegerField(
        _("# of assets"), default=1)

    cost_per_asset = models.DecimalField(
        _("Cost of 1 Asset"), decimal_places=2, max_digits=12, default=0)

    security_deposit_collected = models.DecimalField(
        _("Security Deposit Collected"), decimal_places=2, max_digits=12, default=0)

    lending_status = FSMField(
        _("Lending Status"), choices=ASSET_LENDING_STATUS_CHOICES,
        default=NOT_ISSUED, protected=False, db_index=True)

    # The amount to be recovered, we do not have a depreciation algorithm set up yet
    # so this field will be filled by operations associate (against the original_cost * quantity)
    recovery_amount = models.DecimalField(
          _("Recovery Amount"), decimal_places=2, max_digits=12, default=0)

    # The amount that is pending to be collected by burrower (quantity*original_cost)
    # Running balance amount - decremented upon every Asset Transaction
    balance = models.DecimalField(
        _("Balance Amount"), decimal_places=2, max_digits=12, default=0)

    class Meta:
        verbose_name = _("Asset")
        verbose_name_plural = _("Assets")

    def _capture_changes(self):
        _request = current_request()
        current_user = _request.user if _request else ''
        asset_changes = []

        FIELDS_TO_CAPTURE = ['lending_status', 'recovery_amount']
        if self.lending_status == NOT_ISSUED:
            FIELDS_TO_CAPTURE += ['category', 'identifier', 'quantity',
                'cost_per_asset', 'security_deposit_collected']

        old = self.__class__.objects.get(pk=self._get_pk_val())
        for field in self.__class__._meta.fields:
            field_name = field.__dict__.get('name')

            if field_name in FIELDS_TO_CAPTURE:
                # @todo Need to call __str__ of remote-field to make it generic
                if field_name == 'category':
                    old_value = old.category.name
                    current_value = self.category.name
                else:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                if old_value != current_value:
                    changes = {
                        'field': '%s' % (field.__dict__.get('verbose_name') or field_name),
                        'old': '%s' % old_value,
                        'new': '%s' % current_value
                    }
                    asset_changes.append(changes)

        if bool(asset_changes):
            history = {
                'asset': self,
                'changes': json.dumps(asset_changes),
                'user': current_user
            }
            AssetHistory.objects.create(**history)

    def _calculate_recovery_amount(self):
        return self.quantity * self.cost_per_asset

    def save(self, *args ,**kwargs):
        self.recovery_amount = self._calculate_recovery_amount()
        if self._get_pk_val():
            self._capture_changes()
        else:
            self.balance = self.recovery_amount
        return super(Asset, self).save()

    def __str__(self):
        return '%s |  %s | %s' % (self.driver, self.category, self.identifier)

    # lending status related FSM transitions
    @transition(field=lending_status, source=[NOT_ISSUED], target=ISSUED)
    def issue(self):
        print("Issue Asset")

    @transition(field=lending_status, source=[ISSUED], target=RECOVERED)
    def recovered(self):
        print("Recovered amount for asset")

    @transition(field=lending_status, source=[ISSUED], target=LOST)
    def lost(self):
        print("Lost Asset")

    @transition(field=lending_status, source=[ISSUED], target=PARTIAL_RECOVERY)
    def partially_recovered(self):
        print("Issue Asset")

    @transition(field=lending_status, source=[LOST], target=[ISSUED, RECOVERED, PARTIAL_RECOVERY])
    def found(self):
        print("Asset Found")

class AssetTransaction(models.Model):
    """
    Transactions against an asset
    """
    asset = models.ForeignKey(Asset, on_delete=models.PROTECT)

    txn_reference = models.CharField(
        _("Txn Ref"), max_length=64, help_text=_("like NonOPS Payment ID"))

    amount_collected = models.DecimalField(
        _("Amount Collected"), decimal_places=2, max_digits=12)

    time = models.DateTimeField(_("Created time"), default=timezone.now)

    associate = models.ForeignKey(User, on_delete=models.PROTECT,
        limit_choices_to={'is_staff': True})

    remarks = models.CharField(
        _("Remarks"), max_length=128)

    class Meta:
        verbose_name = _("Asset Transaction")
        verbose_name_plural = _("Asset Transactions")

class AssetHistory(models.Model):
    """
    Capturing changes in asset model
    """
    asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
    changes = models.TextField(_('Changes'), help_text=_('Changes in Asset model'),
        null=True, blank=True)
    user = models.CharField(max_length=50)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Asset History')
        verbose_name_plural = _('Asset History')

    def __str__(self):
        return '%s' % self.asset
