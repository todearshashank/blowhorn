from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db import models
from django.contrib.postgres.indexes import BrinIndex
from postgres_copy import CopyManager
from django.contrib.postgres.fields import JSONField

class OldPayment(models.Model):
    old_id = models.BigIntegerField(unique=True)
    created_time = models.DateTimeField(null=True, blank=True)
    last_modified = models.DateTimeField(null=True, blank=True)
    driver_contract = models.CharField(max_length=100, null=True, blank=True, db_index=True)
    sla = models.CharField(max_length=100, null=True, blank=True, db_index=True)
    customer = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    city = models.CharField(max_length=40, null=True, blank=True, db_index=True)
    driver = models.CharField(max_length=40, null=True, blank=True, db_index=True)
    leaves = models.IntegerField(null=True, blank=True)
    weekly_off = models.IntegerField(null=True, blank=True)
    payment_type = models.CharField(max_length=30, null=True, blank=True, db_index=True)
    payment_cycle =  models.CharField(max_length=30, null=True, blank=True)
    hubs = models.CharField(max_length=200, null=True, blank=True)
    extra_payment = models.FloatField(null=False, default=0)
    extra_hours_payment = models.FloatField(null=False, default=0)
    payment_frequency = models.CharField(max_length=40, null=True, blank=True)
    payment_period_start_date = models.DateTimeField(null=True, blank=True)
    payment_period_end_date = models.DateTimeField(null=True, blank=True)
    final_payment = models.FloatField(null=False, default=0)
    adjustment = models.FloatField(null=False, default=0)
    adjustment_history = JSONField(blank=True, null=True)
    deduction = models.IntegerField(null=False, default=0)
    extra_kms_payment = models.IntegerField(null=False, default=0)
    payment_month_and_year = models.CharField(max_length=40, null=True, blank=True)
    base_payment = models.FloatField(null=False, default=0)
    gross_payment = models.FloatField(null=False, default=0)
    payment_state = models.CharField(max_length=40, null=True, blank=True)
    payment_added_on = models.DateTimeField(null=True, blank=True)
    payment_reason = models.CharField(max_length=400, null=True, blank=True)
    payment_added_by = models.CharField(max_length=40, null=True, blank=True)
    bank_account_name = models.CharField(max_length=100, null=True, blank=True)
    bank_account_number = models.CharField(max_length=40, null=True, blank=True)
    bank_account_bank_name = models.CharField(max_length=100, null=True, blank=True)
    bank_account_ifsc = models.CharField(max_length=40, null=True, blank=True)

    objects = models.Manager()

    copy_data = CopyManager()
    
    def __str__(self):
        return '%s' % self.pk

    class Meta:
        verbose_name = _('Old Payment')
        verbose_name_plural = _('Old Payments')
        indexes = (
            BrinIndex(fields=['created_time']),
        )
    
