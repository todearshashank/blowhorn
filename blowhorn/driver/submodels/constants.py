from django.utils.translation import ugettext_lazy as _

NOT_ISSUED, ISSUED, RECOVERED, LOST, PARTIAL_RECOVERY = 'Not Issued', \
        'Issued', 'Recovered', 'Lost', 'Partial Recovery'

ASSET_LENDING_STATUS_CHOICES = (
    (NOT_ISSUED, _(NOT_ISSUED)),
    (ISSUED, _(ISSUED)),
    (RECOVERED, _(RECOVERED)),
    (LOST, _(LOST)),
    (PARTIAL_RECOVERY, _(PARTIAL_RECOVERY)),
)

ID_CARD, PHONE, SIM, FIRE_EXTINGUISHER, GPS_DEVICE, BH_SHIRT, CARRY_BAG, MISCELLANEOUS = \
        'ID Card', 'Phone/Mobile', 'SIM Card', 'Fire Extinguisher', 'GPS Device', \
        'Blowhorn T-Shirt', 'Carry Bag', 'Miscellaneous'

ASSET_CATEGORY_CHOICES = (
    (ID_CARD, _(ID_CARD)),
    (PHONE, _(PHONE)),
    (SIM, _(SIM)),
    (FIRE_EXTINGUISHER, _(FIRE_EXTINGUISHER)),
    (GPS_DEVICE, _(GPS_DEVICE)),
    (BH_SHIRT, _(BH_SHIRT)),
    (CARRY_BAG, _(CARRY_BAG)),
    (MISCELLANEOUS, _(MISCELLANEOUS)),
)
