from blowhorn.common.models import BaseModel
from django.db import models
from django.utils.translation import ugettext_lazy as _
from blowhorn.users.models import*
from blowhorn.address.models import*
from blowhorn.driver.models import Driver
from blowhorn.driver.constants import DELIVERY_WORKER_ACTIVITY
from blowhorn.trip.models import*


class DriverStatus(models.Model):
    """
    Model to configure Items for Order
    """
    status = models.CharField(
        _("Item category"), max_length=200)


    def __str__(self):
        return str(self.status)

    class Meta:
        verbose_name = _("Driver Status")
        verbose_name_plural = _("Driver Status")


class DriverEligibility(BaseModel):
    """
        Table for filtering driver eligibility
    """
    partner = models.OneToOneField('customer.Partner',  on_delete=models.PROTECT)

    cities = models.ManyToManyField('address.City', blank=True)

    statuses = models.ManyToManyField(DriverStatus, blank=True)

    own_vehicle = models.BooleanField(
        _("Consider only owners?"), default=False)

    is_fixed_driver = models.BooleanField(
        _("Consider only fixed drivers?"), default=False)

    dp_start_month = models.IntegerField(
            _('provide details if DP older then specified months'), default=0)

    class Meta:
        verbose_name = _('Driver Eligibility')
        verbose_name_plural = _('Driver Eligibility')

    def __str__(self):
        return str(self.partner)


class DeliveryWorkerActivity(BaseModel):

    user = models.ForeignKey(User, on_delete=models.PROTECT)

    hub = models.ForeignKey(Hub, on_delete=models.PROTECT, null=True, blank=True)

    driver = models.ForeignKey(Driver, on_delete=models.PROTECT, null=True, blank=True)

    trip = models.ForeignKey(Trip, on_delete=models.PROTECT, null=True, blank=True)

    activity_time = models.DateTimeField(null=True, blank=True)

    activity = models.CharField(max_length=50, null=True, blank=True,
                                choices=DELIVERY_WORKER_ACTIVITY)

    location = models.PointField(null=True, blank=True)

    app_version = models.CharField(
        _("App Version used"), blank=True, null=True, max_length=100
    )

    class Meta:
        verbose_name = _('Delivery Worker Activity')
        verbose_name_plural = _('Delivery Worker Activities')

    def __str__(self):
        return '%s-%s' % (self.user.name, self.user.phone_number)

