import graphene
import json
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from django.db.models import Prefetch, Q

from config.settings import status_pipelines as StatusPipeline
from . import models
from blowhorn.trip.models import Trip
from blowhorn.users.models import User
from blowhorn.vehicle.models import Vehicle, VehicleModel

class DriverPreferredLocationType(DjangoObjectType):
    class Meta:
        model = models.DriverPreferredLocation

class UserType(DjangoObjectType):
    class Meta:
        model = User

class VehicleType(DjangoObjectType):
    class Meta:
        model = Vehicle

class VehicleModelType(DjangoObjectType):
    class Meta:
        model = VehicleModel

class TripType(DjangoObjectType):
    class Meta:
        model = Trip
        filter_fields = {'status': ['in'], 'order': ['isnull']}
        exclude_fields = ['route_trace', '_route_trace']
        interfaces = (graphene.Node, )

class DriverType(DjangoObjectType):
    class Meta:
        model = models.Driver
        filter_fields = { 'id': ['exact'], 'name': ['icontains'], 'operating_city__name': ['exact'], 'driver_vehicle': ['icontains'], 'status': ['exact']}
        interfaces = (graphene.Node, )

class Query(graphene.AbstractType):
    active_drivers = DjangoFilterConnectionField(DriverType)
    active_drivers_with_trips = DjangoFilterConnectionField(DriverType)
    drivers = DjangoFilterConnectionField(DriverType)
    driver_statuses = graphene.List(graphene.String)

    def resolve_active_drivers(self, info, **args):
        return models.Driver.objects.filter(status=StatusPipeline.ACTIVE).select_related('operating_city').prefetch_related(
            Prefetch(
                'driverpreferredlocation_set',
                queryset = models.DriverPreferredLocation.objects.all()
            )
        )

    def resolve_drivers(self, info, **args):
        return models.Driver.objects.select_related('operating_city').prefetch_related(
            Prefetch(
                'driverpreferredlocation_set',
                queryset = models.DriverPreferredLocation.objects.all()
            )
        ).all()

    def resolve_active_drivers_with_trips(self, info, **args):
        return models.Driver.objects.filter(status=StatusPipeline.ACTIVE).prefetch_related(
                Prefetch(
                    'trip_set',
                    queryset = Trip.objects.select_related('order').all()
                )
            )

    def resolve_driver_statuses(self, info, **args):
        return [status[0] for status in StatusPipeline.DRIVER_STATUSES]

