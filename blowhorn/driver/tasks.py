# System & Django
from __future__ import absolute_import, unicode_literals
import json
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Sum, Max, F
from django.utils import timezone
from blowhorn.address.models import City
import datetime
from datetime import datetime, timedelta
from django.utils import timezone

# 3rd Party Libraries
from celery import shared_task
from celery.schedules import crontab
# from celery.decorators import periodic_task
from celery.utils.log import get_task_logger

# Blowhorn Modules
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.driver.models import DriverActivity, APPROVED, DriverPaymentHistory
from blowhorn.common.firebase_operations import firebase_add_update_database, \
    delete_active_drivers, delete_active_driver
from blowhorn.driver.payments import PaymentService
from blowhorn.driver.activities import ActivitiesMonitoring
from blowhorn.common.notification import Notification
from blowhorn.driver.models import Driver, DriverPaymentAdjustment, DriverConstants, DriverPayment, \
    NotificationCampaign, DriverNotificationCriteria, NotificationReadStatus
from blowhorn.users.models import User
from blowhorn.company.models import Company
from blowhorn.driver.activities import deactivate_old_drivers
from blowhorn.common.decorators import redis_batch_lock
from blowhorn.driver.models import UNAPPROVED, CLOSED, WITHHELD
from blowhorn.driver.constants import UNAPPROVED_DEACTIVATION_PERIOD, WITHHELD_DEACTIVATION_PERIOD, DAYS_IN_MONTH, \
    DRIVER_LANGUAGE_CODES, PUSH
from blowhorn.apps.driver_app.v3.helpers.driver import send_notification_content

from blowhorn.driver import constants as DriverConst
from blowhorn.vehicle.constants import VEHICLE_INSURANCE_CONSTANT
from blowhorn.vehicle.models import VehicleDocument
from blowhorn.apps.integrations.v1.insurance.symbo import Symbo, SymboBlowhornMap
# from config.settings.base import celery_app
from blowhorn import celery_app


logger = get_task_logger(__name__)


@celery_app.task(serializer='json')
def driver_location_broadcast(loc_details=None):
    location_dict = json.loads(loc_details)
    firebase_add_update_database(
        json.dumps(location_dict),
        settings.FIREBASE_ACTIVE_DRIVERS_URL + str(location_dict.get('driver_id'))
    )


@celery_app.task
# @periodic_task(run_every=(crontab(hour="23", minute="30")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def deactivate_driver_payments():
    """
    Scheduled job at 5 AM to mark the unapproved and withheld driver payments after 45 days of payment end datetime to
    CLOSED status :return:
    """
    expiry_date = datetime.now() - timedelta(days=UNAPPROVED_DEACTIVATION_PERIOD)
    DriverPayment.objects.filter(status=UNAPPROVED, end_datetime__lt=expiry_date).update(
        status=CLOSED,
        modified_date=timezone.now(),
        modified_by=User.objects.filter(
            email=settings.CRON_JOB_DEFAULT_USER
        ).first()
    )

    exp_date = datetime.now() - timedelta(days=WITHHELD_DEACTIVATION_PERIOD)
    DriverPayment.objects.filter(status=WITHHELD, end_datetime__lt=exp_date).update(
        status=CLOSED,
        modified_date=timezone.now(),
        modified_by=User.objects.filter(
            email=settings.CRON_JOB_DEFAULT_USER
        ).first()
    )


@celery_app.task
def close_unsettled_payment():
    approved_date = timezone.now() - timedelta(days=DAYS_IN_MONTH)
    driver_qs = DriverPayment.objects.filter(status=APPROVED, is_settled=False,
                                             driverpaymenthistory__field='status',
                                             driverpaymenthistory__new_value=APPROVED,
                                             driverpaymenthistory__modified_time__lt=approved_date
                                             )
    history_list = []
    for driver_payment in driver_qs:
        history_list.append(DriverPaymentHistory(
            field='status',
            old_value=APPROVED,
            new_value=CLOSED,
            user=settings.CRON_JOB_DEFAULT_USER,
            modified_time=timezone.now(),
            payment=driver_payment)
            )
    DriverPaymentHistory.objects.bulk_create(history_list)
    driver_qs.update(status=CLOSED,modified_date=timezone.now())



@celery_app.task
# @periodic_task(run_every=(crontab(hour="1,4,7,9,14", minute="30")))
@redis_batch_lock(period=7200, expire_on_completion=True)
def generate_driver_payments():
    """
    Scheduled job at 7 AM,10 AM, 1 PM, 3 PM and 8 PM to generate driver payments
    for all resources(driver) allocated in contracts
    """
    logger.info("generating payments")
    if settings.ENABLE_SLACK_NOTIFICATIONS:
        PaymentService().generate_payments()


@celery_app.task
# @periodic_task(run_every=(crontab(hour="2,6,10,14", minute="15")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def driver_gps_distance():
    """
    Scheduled job at 8 AM, 12PM, 4 PM, and 8 PM to generate driver login time
    to distance travelled data
    """
    logger.info("Calculate Driver GPS distance to populate table Driver Log Activity")
    # commenting this to check, weather it is causing performance degrading
    return
    if settings.ENABLE_SLACK_NOTIFICATIONS:
        ActivitiesMonitoring().calculate_driver_gps_distance()


@celery_app.task
def generate_driver_payment(contract_list=None):
    PaymentService().generate_payments(contract_list=contract_list)


@celery_app.task
def mailer(subject, message, recepients):
    send_mail(
        subject,
        message,
        settings.DEFAULT_FROM_EMAIL,
        recepients
    )


@celery_app.task
# @periodic_task(run_every=(crontab(hour="19", minute="30")))
@redis_batch_lock(period=10800, expire_on_completion=True)
def clear_driver_activity():
    """
    Daily job at night 1AM to delete all driver activity
    from Firebase as well as DriverActivity
    """
    print("Deleting driver activities from DriverActivity")
    if settings.DEBUG:
        return True
    activity = DriverActivity.objects.all().delete()
    logger.info("Adding task to delete location from firebase")
    delete_active_drivers()


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES)
def delete_driver_activity_from_firebase(self, driver_id):
    """
    Delete a activity of given driver from firebase
    """
    logger.info("Deleting driver activities from Firebase")
    delete_active_driver(driver_id)


@celery_app.task
def restore_driver_app(driver_id):
    """
    Restores driver app
    """
    driver = Driver.objects.get(pk=driver_id)
    Notification.driver_app_action(driver, 'restore')


@celery_app.task
def push_notification_to_driver_app(driver_id, action):
    driver = Driver.objects.get(pk=driver_id)
    Notification.send_push_notification_to_driver_app(driver, action)

# @celery_app.task
# def send_message_to_drivers(driver_phone_numbers, message, template_json):
#     from blowhorn.common.tasks import send_sms
#     """
#     Sends App link to drivers Mobile as a sms
#     """
#     for phone_number in driver_phone_numbers:
#         send_sms.apply_async(args=[phone_number, message, template_json])


# @celery_app.task(base=TransactionAwareTask, bind=True,
#              max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
# def send_mail_to_spocs_regarding_driver_document(self, spoc_id, driver_id,
#                                                  doc_id, host, rejected=False):
#     from blowhorn.utils.mail import Email
#     from .util import get_subject_for_document
#     """
#     Sends mail to spocs when driver document is assigned to them
#     """
#     driver = None
#     if driver_id:
#         driver = Driver.objects.get(id=driver_id)
#     user = User.objects.get(id=spoc_id)

#     subject, body = get_subject_for_document(user, driver, host, doc_id,
#                                              rejected)

    # Email().send_notification_to_spocs_regarding_driver_document(user, subject,
    #                                                              body)


@celery_app.task
# @periodic_task(run_every=(crontab(hour="0, 18, 21", minute="15")))
def deactivate_drivers():
    """
    schedueld to run at 2:45AM, 6:45AM, and 11:45pm everyday
    """
    logger.info(
        "Deactivate drivers who do not have done any trips in last 30 days")
    deactivate_old_drivers()


# @celery_app.task(base=TransactionAwareTask, bind=True,
#              max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
# def adjustment_exceeding_mail(self, payment_id, city_id, request_host):
#     from blowhorn.utils.mail import Email
#     from blowhorn.company.models import Company

#     sum_ = DriverPaymentAdjustment.objects.filter(payment_id=payment_id) \
#         .exclude(status=DriverPaymentAdjustment.REJECTED) \
#         .aggregate(Sum('adjustment_amount'))

#     total_adjustment = sum_.get('adjustment_amount__sum') or 0  # + self.adjustment_amount
#     executive_limit = float(DriverConstants().get('DP_ADJUSTMENT_APPROVAL_LIMIT', 3000))
#     manager_limit = float(DriverConstants().get('DP_ADJUSTMENT_APPROVAL_LIMIT_FOR_CITY_MANAGER', 1000))
#     if (total_adjustment > executive_limit or total_adjustment > manager_limit) and \
#         not settings.ENABLE_SLACK_NOTIFICATIONS:  # and not self.id:

#         email_ids = Company.objects.values_list('executives__email', flat=True) \
#             if total_adjustment > executive_limit else \
#             City.objects.filter(id=city_id).values_list('managers__email', flat=True)

#         subject = 'Adjustment Exceeding the limit '
#         body = 'Dear Executives, <br>'
#         body += '<br><br> Adjustment has exceeded the limit. please review it.<br>'
#         body += request_host + '/admin/driver/driverpayment/%s' % (payment_id)

#         body += '<br><br>Regards,<br>' + 'Team %s'  %('Blowhorn' if settings.COUNTRY_CODE_A2 == 'IN' else company.name)

        # Email().send_email_message(email_ids, subject, body)


@celery_app.task
def push_driver_notification(driver_id, action, data_dict=None):
    driver = Driver.objects.get(pk=driver_id)
    response = {'action' : action, 'data' : data_dict}
    Notification.push_to_fcm_device(driver.user, {'blowhorn_notification': response})


@celery_app.task
# @periodic_task(run_every=(crontab(hour="20", minute="30")))
@redis_batch_lock(period=76400, expire_on_completion=True)
def create_leads():
    """
    Daily job at night 2AM to create Leads on Symbo,
    task locked for 21hr
    """
    BEFORE = timezone.now() - timedelta(days=DriverConstants().get('DAYS_BEFORE_EXPIRY', 30))
    # range wont consider end so adding 1 day to configured value
    AFTER = timezone.now() + timedelta(days=DriverConstants().get('DAYS_AFTER_EXPIRY', 30) + 1)
    if settings.DEBUG:
        return
    posted_doc_ids = SymboBlowhornMap.objects.values_list('vehicle_document_id', flat=True)

    drivers = Driver.objects.filter(
        drivervehiclemap__vehicle__vehicle_document__document_type__code=VEHICLE_INSURANCE_CONSTANT,
        drivervehiclemap__vehicle__vehicle_document__expiry_date__range=(BEFORE, AFTER),
        status='active').annotate(expiry_date=F(
        'drivervehiclemap__vehicle__vehicle_document__expiry_date'), vehicle=F(
        'drivervehiclemap__vehicle'), doc_id=F('drivervehiclemap__vehicle__vehicle_document'),
        vehicle_class=F('drivervehiclemap__vehicle__vehicle_model__vehicle_class__commercial_classification')
    ).order_by(
        '-drivervehiclemap__vehicle__vehicle_document__document_type').distinct(
        'drivervehiclemap__vehicle__vehicle_document__document_type', 'id').exclude(doc_id__in=posted_doc_ids)
    drivers = drivers.select_related('user', 'current_address', 'permanent_address')

    for driver in drivers:
        Symbo().createLead(driver)


@celery_app.task
# @periodic_task(run_every=(crontab(hour="19", minute="30")))
@redis_batch_lock(period=84600, expire_on_completion=True)
def expire_driver_event_status():
    """
    Daily job at 1AM to set the driver event flag to False
    """
    if settings.ENABLE_SLACK_NOTIFICATIONS and settings.ENABLE_DRIVER_EVENT:
        Driver.objects.filter(driver_event=True).update(driver_event=False)


@celery_app.task
# @periodic_task(run_every=(crontab(hour="05", minute="00")))
@redis_batch_lock(period=84600, expire_on_completion=True)
def push_notification_to_driver():
    from blowhorn.driver.util import driver_push_notification_query
    active_campaigns = NotificationCampaign.objects.filter(is_active=True, notification_type=PUSH)

    for campaign in active_campaigns:
        content = campaign.content
        read_status = NotificationReadStatus.objects.filter(notification_campaign_id=campaign.id).first()

        if campaign.start_time < timezone.now() < campaign.end_time and content and not read_status:

            driver_criteria = DriverNotificationCriteria.objects.filter(notification_campaign_id=campaign.id).first()
            drivers = driver_push_notification_query(driver_criteria)

            for language in DRIVER_LANGUAGE_CODES:
                notification_content = send_notification_content(language, content)
                user_ids = list(drivers.filter(last_used_locale=language).values_list('user__id', flat=True))
                Notification.push_to_fcm_devices(title=notification_content[0], body=notification_content[2],
                                                 user_pks=user_ids)

            driver_ids = list(drivers.values_list('id', flat=True))

            notification_read_status = []
            for driver in driver_ids:
                notification_read_status.append(NotificationReadStatus(
                    notification_campaign_id=campaign.id,
                    driver_id=driver
                ))
            if notification_read_status:
                NotificationReadStatus.objects.bulk_create(notification_read_status)
