import json
from django.conf import settings
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from blowhorn.document.constants import DRIVER_DOCUMENT, ACTIVE
from blowhorn.driver.models import Driver, DriverDocument
from blowhorn.driver.serializers import DriverSerializer
from blowhorn.order.models import Order
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.utils.crypt import Cipher
from blowhorn.utils.html_to_pdf import render_to_pdf
from config.settings import status_pipelines as StatusPipeline


@api_view(['GET', 'POST'])
def driver_list(request):
    """
    List all drivers or create a new driver
    """

    if request.method == 'GET':
        drivers = Driver.objects.all()
        serializer = DriverSerializer(drivers, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DriverSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def driver_detail(request, pk):
    """
    Get, udpate, or delete a specific driver
    """

    try:
        driver = Driver.objects.get(pk=pk)
    except Driver.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = DriverSerializer(driver)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = DriverSerializer(driver, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        driver.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def search_driver(request):
    """
    List all active drivers
    """
    if request.method == 'GET':
        order_id = request.GET.get('order_id', None)
        search_term = request.GET.get('term', None)
        create_trip = request.GET.get('create_trip', False)
        assign_orders = request.GET.get('assign_orders', False)
        if not order_id:
            return Response(status=400, data='Select orders')

        create_trip = True if create_trip == 'true' else False
        assign_orders = True if assign_orders == 'true' else False
        query = Q()
        if create_trip:
            query = ~Q(
                trip__status__in=[StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED,
                                  StatusPipeline.TRIP_IN_PROGRESS,
                                  StatusPipeline.TRIP_ALL_STOPS_DONE])
        if assign_orders:
            query = Q(
                trip__status__in=[StatusPipeline.DRIVER_ACCEPTED, StatusPipeline.DRIVER_ASSIGNED,
                                  StatusPipeline.TRIP_IN_PROGRESS,
                                  StatusPipeline.TRIP_ALL_STOPS_DONE])

        order = Order.objects.get(pk=int(order_id))
        drivers = Driver.objects.filter(
            Q(name__icontains=search_term) | Q(driver_vehicle__icontains=search_term), query,
            status=StatusPipeline.ACTIVE,
            driver_vehicle__isnull=False,
            operating_city__in=[order.city, order.source_city],
        ).values_list('id', 'name', 'driver_vehicle')
        return Response(drivers)


def get_avatar_url(driver):
    """
    To get URL of driver photo
    :param driver: Instance of Driver
    """
    avatar_url = driver.photo.url if driver.photo else ''
    if not avatar_url:
        photo = DriverDocument.objects.filter(
            driver_id=driver.pk,
            document_type__code='Photo',
            status=ACTIVE,
            document_type__identifier=DRIVER_DOCUMENT
        ).select_related('document_type')\
        .order_by('date_uploaded').last()
        if photo is not None:
            avatar_url = photo.file.url if photo.file else ''

    return avatar_url


@api_view(['GET'])
@permission_classes([IsBlowhornStaff])
def download_driver_id_card(request, id=None):
    """
    Renders ID card from server
    @todo Send PDF stream as response so it will download directly
    """
    if request.method == 'GET':
        driver_pk = id
        driver = Driver.objects.get(pk=driver_pk)
        download = request.GET.get('download', False)
        phone_number = driver.user.phone_number
        registration_number = driver.driver_vehicle
        dict_for_qr = {
            'id': phone_number.national_number if phone_number else ''
        }
        qr_value = Cipher.encode(json.dumps(dict_for_qr))
        avatar_url = get_avatar_url(driver)
        show_download = driver.name and avatar_url and phone_number \
            and registration_number
        context_data = {
            'driver_name': driver.name,
            'avatar_url': avatar_url,
            'phone_number': phone_number,
            'registration_number': registration_number,
            'qr_value': qr_value.decode("utf-8"),
            'build': settings.WEBSITE_BUILD,
            'brand_name': settings.BRAND_NAME,
            'disclaimer': 'Driver ID card is not valid for any other purposes other than at %s for driver identification.' % settings.BRAND_NAME,
            'frontcover': '/static/img/%s/BACK_WHITEBG.jpg' % settings.WEBSITE_BUILD,
            'backcover': '/static/img/%s/FRONT_WHITEBG.png' % settings.WEBSITE_BUILD,
            'show_download': show_download
        }
        template_path = 'admin/driver/id_card.html'
        if download:
            pdf = render_to_pdf(template_path, context_data)
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "id_card_%s.pdf" % driver.name
            content = "attachment; filename=%s" % filename
            response['Content-Disposition'] = content
            return response

        return render_to_response(template_path, context_data)
