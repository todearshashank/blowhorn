from django.conf.urls import url
from django.apps import AppConfig
from .views import (
    DriversList,
    DriverAdd,
    DriverForContractFilter,
    DriverDocumentUpload,
    BackGroundVerificationView,
    BGVProfileStatus,
    DriverListExport,
    DriverInformation,
    DriverBalance,
    DriverDeductionview,
    DriverEligibility, DriverAvailability,
    DriversAllActive, DriverWellBeingView
)
from .apiviews import search_driver, download_driver_id_card

urlpatterns = [
            url(r'^driver/driverlist$', DriversList.as_view(),
                name='driver-list'),
            url(r'^driver/filter$', DriversList.as_view(),
                name='driver-filter'),
            url(r'^driver/add$', DriverAdd.as_view(),
                name='driver-add'),
            url(r'^driver/filters/$', DriverForContractFilter.as_view(),
                name='driver-contract-filter'),
            url(r'^driver/documentupload$', DriverDocumentUpload.as_view(),
                name='driver-document-upload'),
            url(r'^driver/background-verification$', BackGroundVerificationView.as_view(),
                name='driver-background-verification'),
            url(r'^driver/background-verification-status$', BGVProfileStatus.as_view(),
                name='driver-background-verification-status'),
            url(r'^driver/drivers/export', DriverListExport.as_view(), name='drivers-export'),
            url(r'^drivers/detail', DriverInformation.as_view(), name='driver-info'),
            url(r'^drivers/balance', DriverBalance.as_view(), name='driver-balance'),
            url(r'^drivers/deduction/', DriverDeductionview.as_view(), name='driver-deduction'),
            url(r'^drivers/drivereligibility/', DriverEligibility.as_view(), name='driver-eligibility'),
            url(r'^drivers/allactive/', DriversAllActive.as_view(), name='driver-allactive'),

            url(r'^driver/wellbeing', DriverWellBeingView.as_view(), name='driver-well-being'),

            url(r'^driver/availability/', DriverAvailability.as_view(), name='driver-availability'),

            # Admin
            url(r'driver/search/', search_driver, name='driver-search'),
            url(r'driver/(?P<id>[0-9]+)/idcard$', download_driver_id_card, name='driver-id-card')
        ]
