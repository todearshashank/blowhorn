from django.utils.translation import ugettext_lazy as _
from datetime import timedelta
from blowhorn.oscar.core.loading import get_model
from blowhorn.contract.permissions import (
    is_spoc, is_supervisor,
    is_spoc_or_supervisor
)
from blowhorn.address.permissions import is_city_manager
from django.utils import timezone
from blowhorn.common.middleware import current_request
from blowhorn.contract.constants import CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SPOT, \
    CONTRACT_STATUS_ACTIVE, BASE_PAYMENT_CYCLE_PER_TRIP, PAYMENT_INTERVAL_DAYS
from blowhorn.contract.permissions import is_citymanager, is_finance_user
from django.conf import settings
from blowhorn.order.const import PAYMENT_STATUS_PAID
from config.settings import status_pipelines as StatusPipeline
from django.db.models import Count

"""
Permissions
"""


def get_buy_rate(buy_rate_id):
    BuyRate = get_model('contract', 'BuyRate')
    return BuyRate.objects.filter(id=buy_rate_id).first()


def get_cash_paid_and_adjustment_amount(payment):
    from blowhorn.driver.payments import APPROVED
    adjustment, cash_paid = 0,0
    approved_adjustment = payment.driverpaymentadjustment_set.filter(status=APPROVED)
    approved_cashpaid = payment.drivercashpaid_set.filter(status=APPROVED)
    for adj in approved_adjustment:
        adjustment += adj.adjustment_amount

    for cash in approved_cashpaid:
        cash_paid += cash.cash_paid

    return adjustment, cash_paid


def is_payment_spoc(instance, user):
    return is_spoc(instance.contract, user)


def is_payment_supervisor(instance, user):
    return is_supervisor(instance.contract, user)



def is_company_executive(email):
    from blowhorn.company.models import Company
    return email in Company.objects.exclude(executives__email__isnull=True
                                                                   ).values_list('executives__email', flat=True)

def is_current_user_can_approve(instance):
    from blowhorn.company.models import Company
    PaymentTrip = get_model('driver', 'PaymentTrip')
    executives = list(Company.objects.exclude(executives__email__isnull=True
                                                               ).values_list('executives__email', flat=True))
    if current_request().user.email in executives:
        return True

    pts = PaymentTrip.objects.filter(
        payment__driver=instance.driver,
        payment__start_datetime=instance.start_datetime,
        payment__end_datetime=instance.end_datetime,
        trip__status__in=[StatusPipeline.TRIP_COMPLETED, StatusPipeline.TRIP_SCHEDULED_OFF]
    ).values('trip__planned_start_time__date', 'trip__trip_base_pay', 'trip__is_contingency',
             'payment__buy_rate').annotate(
            total_trips_on_the_day=Count('trip'))
    pts = pts.prefetch_related('trip')
    is_contingency = [True for pt in pts if pt.get('trip__is_contingency')]

    if instance.contract.contract_type == CONTRACT_TYPE_SPOT or is_contingency:
        can_approve = True
        buy_rate = instance.buy_rate
        pt = instance.paymenttrip_set.all()[0] if instance.paymenttrip_set.all() else None
        driver_veh_class_amt = pt.trip.vehicle.vehicle_model.vehicle_class.per_day_limit if pt else 0
        interval = buy_rate.base_payment_interval

        if interval == BASE_PAYMENT_CYCLE_PER_TRIP:
            for pt in pts:
                if pt.get('total_trips_on_the_day') * buy_rate.driver_base_payment_amount > driver_veh_class_amt:
                    is_current_user_can_approve.hint = 'Per Day Trip Amount (%s) is Exceeded. ' \
                                                       'Contact Company Executives %s' \
                           % (driver_veh_class_amt, executives)
                    return False
            return can_approve

        # prepare dict with date wise to include either base pay or buy rate
        per_day_amounts = dict()

        for pt in pts:
            day, trip_base_pay, total_trips = pt.get('trip__planned_start_time__date'), pt.get('trip__trip_base_pay'), \
                                              pt.get('total_trips_on_the_day')
            buy_rate = get_buy_rate(pt.get('payment__buy_rate'))
            driver_base_payment_amount = buy_rate.driver_base_payment_amount or 0
            interval = buy_rate.base_payment_interval
            factor = PAYMENT_INTERVAL_DAYS.get(interval)
            amount = trip_base_pay if trip_base_pay else driver_base_payment_amount/factor
            trip_amount = amount * total_trips

            if day not in per_day_amounts:
                per_day_amounts[day] = trip_amount or 0
            else:
                per_day_amounts[day] += trip_amount or 0

        for day_trip_amount in per_day_amounts.values():
            if day_trip_amount and day_trip_amount > driver_veh_class_amt:
                is_current_user_can_approve.hint = 'Per Day Trip Amount (%s) is exceeded. ' \
                                                   'Contact Company Executives %s' % (driver_veh_class_amt, executives)
                return False
        return can_approve
    return True


def is_payment_spoc_or_supervisor(instance, user):
    if is_company_executive(user.email):
        return True

    return is_spoc_or_supervisor(instance.contract, user)


def is_adjustment_spoc(instance, user):
    # Contract can be null if autogenerated. Then check contract in the payment
    return is_spoc(instance.contract or instance.payment.contract, user)

def is_adjustment_supervisor(instance, user):

    if is_company_executive(user.email):
        return True
    if is_citymanager(instance.payment.contract, current_request().user):
        return True
    return is_supervisor(instance.contract or instance.payment.contract, user)


def is_adjustment_spoc_or_supervisor(instance, user):
    if is_company_executive(user.email):
        return True
    if hasattr(instance, 'action_owner') and user == instance.action_owner:
        return True
    return is_spoc_or_supervisor(
        instance.contract or instance.payment.contract, user)


def is_nop_payment_approver(instance, user):
    return is_city_manager(instance.city, user)


def is_nop_payment_unapprover(instance, user):
    return is_city_manager(instance.city, user)


def is_driver_ledger_finance_user(instance, user):
    return is_finance_user(None, user)

def is_eligible_for_unapprovable(instance):
    if instance.amount <= 0:
        return False
    if instance.is_settled:
        return False
    return True

"""
Transition Conditions
"""


def can_unapprove_payment(instance):
    """ Condition to check whether a payment record can be reversed
        after approval.

        A payment record can only be unapproved if the balance in the
        driver ledger at least has enough to facilitate reversal. If
        driver ledger is already settled, it means he has already been
        paid and unapproving(reversal) is meaningless since he has
        already been paid
    """

    if instance.is_settled:
        return False
    if instance.is_fifty_percent_settlement:
        return False

    driver_ledger = get_model('driver', 'DriverLedger')
    last_transaction = driver_ledger.objects.filter(
        driver=instance.driver).order_by('-transaction_time').first()
    if last_transaction and last_transaction.balance >= instance.net_pay:
        return True
    elif current_request().user.is_superuser:
        """ Allow super user to unapprove payment even if it is settled
            other condition apply like he must be spoc or supervisor to
            unapprove.
        """
        return True
    else:
        can_unapprove_payment.hint = 'Cannot unapprove. Driver credit balance'
        ' (%s) is less than this Net Pay.' % (
            last_transaction and last_transaction.balance)
        return False


def approval_days_gap(instance):
    from blowhorn.driver.models import DriverConstants
    if instance.contract and instance.contract.contract_type == CONTRACT_TYPE_FIXED:
        approve_date = instance.end_datetime + \
                       timedelta(days=int(DriverConstants().get('DAYS_GAP_TO_APPROVE_PAYMENT', 5)))
        if timezone.now().date() < approve_date and \
            instance.end_datetime - instance.start_datetime > timedelta(0):
            return False
    return True


def can_approve_payment(instance):
    from blowhorn.driver.models import DriverConstants, DriverDocument
    from blowhorn.vehicle.models import VendorDocument
    from blowhorn.document.models import Document, DOCUMENT_PAN
    from blowhorn.driver.payment_helpers.contract_helper import ContractHelper
    """ Condition to check whether a payment record can be approved """
    # TODO: can be used to validate staleness of the paymentord
    # Like, ratecard has been modified or cycle incompleter
    # more trips need to processed, etc...
    hint = []

    contract = get_model('contract', 'Contract')
    customer_invoice_config = get_model('customer', 'CustomerInvoiceConfig')
    if settings.WEBSITE_BUILD == settings.DEFAULT_BUILD:
        if instance.driver.own_vehicle:
            active_pan = DriverDocument.objects.filter(status=Document.ACTIVE,
                                                       driver=instance.driver,
                                                       document_type__code=DOCUMENT_PAN).exists()
            if not active_pan:
                hint.append('The driver must have active PAN document to approve this payment!')
        else:
            if instance.driver.owner_details:
                active_pan = VendorDocument.objects.filter(
                    status=Document.ACTIVE,
                    vendor=instance.driver.owner_details,
                    document_type__code=DOCUMENT_PAN).exists()
                if not active_pan:
                    hint.append(
                        'The Fleet Owner (%s) of this driver must have active PAN document'
                        ' to approve this payment!' % instance.driver.owner_details)
    if instance.contract in contract.objects.filter(status=CONTRACT_STATUS_ACTIVE,
        contract_type__in=[CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED]). \
            exclude(id__in=customer_invoice_config.objects.filter(). \
                values_list('contracts', flat=True)):
        hint.append('Contract is not in Business Customer -> Invoice Config. '
            'Add it to proceed. No Invoice, No driver payment !!')
        can_approve_payment.hint = hint
        return False

    if instance.is_stale:
        hint.append('Wait!. Record is stale. '
                    '(Needs re-processing as Buy Rate has been edited)')

    resourceallocation = get_model('contract', 'ResourceAllocation')

    # Disallow approval of Payment if driver is in Resource Allocation
    # and you are in between the driver payment cycle
    if resourceallocation.objects.filter(contract=instance.contract).filter(
            drivers=instance.driver).exists():

        if not approval_days_gap(instance):
            hint.append('You can approve only after %s days of cycle end date' % int(DriverConstants().get('DAYS_GAP_TO_APPROVE_PAYMENT', 5)))
        # elif instance.end_datetime >= timezone.now().date():
        #     hint.append('You can approve the payment'
        #                 ' when payment cycle ends!')

    paymenttrip = get_model('driver', 'PaymentTrip')
    if paymenttrip.objects.filter(payment=instance).filter(
            trip__modified_date__gte=instance.modified_date).exists():
        hint.append('Wait!. Record is stale.'
                    ' Trip(s) have been edited!')

    if not instance.can_approve:
        hint.append('Close Pending Trips.')

    if instance.driver.get_bank_account_details() is None:
        hint.append('Please update Bank Information')

    adjustments = get_model('driver', 'DriverPaymentAdjustment')
    if adjustments.objects.filter(
            payment=instance, status=adjustments.NEW).exists():
        hint.append('Approve/Reject pending Adjustments.')

    cashpaid = get_model('driver', 'DriverCashPaid')
    if cashpaid.objects.filter(
            payment=instance, status=adjustments.NEW).exists():
        hint.append('Approve/Reject pending Cash Paid records.')

    if paymenttrip.objects.filter(payment = instance, trip__order__order_type = settings.C2C_ORDER_TYPE,
                                  trip__order__status=StatusPipeline.ORDER_DELIVERED,
                                  trip__status=StatusPipeline.TRIP_COMPLETED).\
                                  exclude(trip__order__payment_status=PAYMENT_STATUS_PAID).exists():
        hint.append('Payment not Completed for c2c/sme booking')

    if hint:
        can_approve_payment.hint = hint
        return False

    return True


def can_approve_adjustment(instance):
    """ Condition to check whether a payment adjustment record can be approved
        Adjustment can only be approved only if parent payment record is in
        UNAPPROVED status
    """

    from blowhorn.driver.models import DriverConstants, DriverPaymentAdjustment, DriverCashPaid
    from blowhorn.company.models import Company
    adjustment, cash_paid = get_cash_paid_and_adjustment_amount(instance.payment)
    hint = []
    current_user = current_request().user

    if isinstance(instance, DriverPaymentAdjustment):
        adjustment += instance.adjustment_amount if instance.adjustment_amount > 0 else 0
    if isinstance(instance, DriverCashPaid):
        cash_paid += instance.cash_paid if instance.cash_paid > 0 else 0

    dp_adjustment_limit = float(DriverConstants().get('DP_ADJUSTMENT_APPROVAL_LIMIT', 3000))
    dp_adjustment_limit_for_city_manager = float(DriverConstants().get('DP_ADJUSTMENT_APPROVAL_LIMIT_FOR_CITY_MANAGER', 1000))
    company_executive = current_user.email in Company.objects.exclude(executives__email__isnull=True
                                       ).values_list('executives__email', flat=True)

    if not company_executive\
        and (cash_paid > dp_adjustment_limit or adjustment > dp_adjustment_limit or \
        cash_paid + adjustment > dp_adjustment_limit):
        hint.append('Huge Amount , company executive can approve the Adjustment.')

    elif (not company_executive and not is_citymanager(instance.payment.contract, current_user))\
        and (cash_paid > dp_adjustment_limit_for_city_manager or adjustment > dp_adjustment_limit_for_city_manager or \
             cash_paid + adjustment > dp_adjustment_limit_for_city_manager):
        hint.append('Excess Amount , City Manager/ company executive can approve the Adjustment.')

    if hint:
        can_approve_adjustment.hint = hint
        return False

    from .models import UNAPPROVED
    if instance.payment and instance.payment.status == UNAPPROVED:
        if hasattr(instance, 'action_owner'):
            if not hint:
                if current_user == instance.action_owner:
                    #if hint has data which means current user is not
                    #eligible to approve
                    return True
                else:
                    can_approve_adjustment.hint = 'Contact %s for Approval' % str(instance.action_owner if instance.action_owner else 'Process Team')
                    return False

        return True

    can_approve_adjustment.hint = ('Cannot approve. Adjustment not linked to '
                                   'Driver Payment yet(check tomorrow) or is not in Unapproved State')
    return False


def can_unapprove_adjustment(instance):
    """
    Condition to check whether a payment adjustment record can be unapproved
    Adjustment can only be reversed(unapproved) only if parent payment record
    is in UNAPPROVED status
    """
    from .models import UNAPPROVED
    if instance.payment and instance.payment.status == UNAPPROVED:
        return True

    can_unapprove_adjustment.hint = 'Cannot unapprove. Driver Payment (parent)'
    ' is not in Unapproved State'
    return False

##########################################################

def is_nops_payment_approvable(instance):
    """
    Conditions to check whether non-ops payment can be approved
    """
    if instance.purpose == 'asset':
        asset = instance.asset
        if instance.amount > asset.balance:
            is_nops_payment_approvable.hint = \
                    _('Amount is greater than asset balance %s' % asset.balance)
            return False
    return True

def is_super_user(instance, user):
    return user.is_superuser
