import re
import json
import logging
from datetime import date
from django.utils import timezone
from datetime import timedelta, datetime

from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.measure import Distance as DistanceMeasure
from django.utils.translation import ugettext_lazy as _
from django.db.models import Case, When, Value, IntegerField, Max, F
from django import db
from blowhorn.driver.constants import NON_OPS_OTHERS
from blowhorn.driver.models import APPROVED, DATE_FORMAT
from blowhorn.driver.resource import NonOperationPayment
from blowhorn.integration_apps.decentro.service import DecentroService

from rest_framework import status
from rest_framework.response import Response
from blowhorn.oscar.core.loading import get_model

from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.models import DriverActivity, \
    DriverConstants, Driver, BankAccount, BankMigrationHistory, DriverLedger, BankAccountHistory
from blowhorn.trip.models import Trip
from blowhorn.order.models import OrderConstants
from blowhorn.utils.functions import log_db_queries, get_value_from_obj
from blowhorn.vehicle.models import CityAlternateVehicleClass, Vendor
from blowhorn.address.models import City
from blowhorn.users.submodels.notification import NotificationMessageTemplate

Order = get_model('order', 'Order')

TIME_FORMAT = '%d-%b-%Y %H:%M:%S'
GPS_TIMESTAMP_FORMAT = '%d-%b-%Y %H:%M:%S'
DISTANCE_CONVERTER_IN_KM = 100
AVERAGE_VEHICLE_SPEED_KMPH = 15.0
DEFAULT_MAX_LOAD_LIMIT_LEVEL = '1000000'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverMixin(object):

    def get_vehicle_for_driver(self, driver):
        vehicles = driver.vehicles.all()
        if vehicles:
            return vehicles[0]
        return False

    def get_nearest_available_driver(self, pickup_address_geopoint, order=None,
                                     vehicle_class_preference=None, city_id=None):
        # from blowhorn.driver.models import Driver
        max_load_limit = 0
        distance_radius_meter = float(DriverConstants().get('DRIVER_DISTANCE_IN_KM', 5))

        if not order and not vehicle_class_preference:
            print("order or vehicle class preference is required exiting..")
            exit()

        min_planned_start_time = timezone.now()
        # resource_allocation_driver = Driver.objects.filter(id__in=ResourceAllocation.objects.values_list('drivers'))
        if order:
            # rejected_drivers = Driver.objects.filter(id__in=order.rejected_drivers.values_list('id'))

            time_to_fullfill_order_min = (float(order.estimated_distance_km) / AVERAGE_VEHICLE_SPEED_KMPH) * 60
            time_before_next_trip = OrderConstants().get('MINUTES_BEFORE_NEXT_TRIP', 60)

            min_planned_start_time = min_planned_start_time + \
                timedelta(minutes=(time_to_fullfill_order_min + time_before_next_trip))

            max_load_limit = order.item_category.all().aggregate(
                max_level=Max('load_preference__level'))['max_level']
            max_load_limit = max_load_limit if max_load_limit else 0

            vehicle_class_preference = [order.vehicle_class_preference]
            city_id = order.city_id

        available_drivers = DriverActivity.objects.annotate(
            driver_load_limit=Case(
                When(driver__load_preference__isnull=False, then=F('driver__load_preference__level')),
                default=Value(DEFAULT_MAX_LOAD_LIMIT_LEVEL),
                output_field=IntegerField()
            ),
            dispatch_sequence_id=Case(
                When(driver__vehicles__vehicle_model__vehicle_class__in=vehicle_class_preference, then=0),
                default=F('driver__vehicles__vehicle_model__vehicle_class__vehicle_class_sequence'),
                output_field=IntegerField()
            ),
        ).filter(
            driver__vehicles__vehicle_model__vehicle_class__in=list(vehicle_class_preference) +
                [vehicle_class for vehicle_class in CityAlternateVehicleClass.objects.filter(city=city_id,
                 vehicle_class__in=vehicle_class_preference).values_list('alternate_vehicle_class')],
            updated_time__gte=(timezone.now() - timedelta(minutes=15)),
            geopoint__distance_lt=(pickup_address_geopoint, DistanceMeasure(km=distance_radius_meter)),
            driver_load_limit__gte=max_load_limit
        ).annotate(
            distance=Distance('geopoint', pickup_address_geopoint)
        ).exclude(
            driver_id__in=(
                Trip.objects.filter(
                    planned_start_time__lte=min_planned_start_time,
                    status__in=[StatusPipeline.TRIP_IN_PROGRESS, StatusPipeline.DRIVER_ACCEPTED,
                                StatusPipeline.TRIP_ALL_STOPS_DONE]
                ).distinct('driver_id').exclude(driver_id=None).values_list('driver', flat=True))
        ).order_by('dispatch_sequence_id','distance')#

        auto_dispatch_driver = City.objects.filter(id=city_id).values_list('autodispatch_driver__id',
                                                                                 flat=True).exclude(autodispatch_driver=None)

        if auto_dispatch_driver.exists():
            available_drivers = available_drivers.filter(driver_id__in=auto_dispatch_driver)


        # print("********** Available drivers***************", available_drivers)
        # cache.set('available_drivers' + '-' + order.number, driver)
        return available_drivers

        # if available_drivers.exists():
        #     return available_drivers
        # else:
        #     print('Not able to find the driver')
        #     return False

    def create_activity(self, data):
        driver_existing_activity = DriverActivity._default_manager.filter(
            driver=data.get('driver')).first()

        if driver_existing_activity is not None:
            return None

        try:
            driver_activity = DriverActivity(**data)
            driver_activity.save()

            # Dumping to DriverActivityHistory would be deprecated soon
            # driver_activity_history = DriverActivityHistory(**data)
            # driver_activity_history.save()

            return driver_activity
        except:
            return None

    def update_activity(self, data):
        driver_existing_activity = DriverActivity._default_manager.filter(
            driver=data.get('driver')).first()
        if driver_existing_activity is None:
            return None
        try:
            driver_activity = DriverActivity(**data)
            # Set the ID to update
            driver_activity.id = driver_existing_activity.id
            driver_activity.created_time = driver_existing_activity.created_time
            driver_activity.save()

            # Dumping to DriverActivityHistory would be deprecated soon
            # driver_activity_history = DriverActivityHistory(**data)
            # driver_activity_history.save()

            return driver_activity
        except:
            return None

    def delete_activity(self, data):
        from blowhorn.driver.tasks import delete_driver_activity_from_firebase
        driver_existing_activity = DriverActivity._default_manager.filter(
            driver=data.get('driver')).first()
        if driver_existing_activity is not None:
            driver_existing_activity.delete()
            driver = data.get('driver')
            delete_driver_activity_from_firebase.apply_async(
                args=[driver.id]
            )
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data=_("Driver activity doesn't exists.")
            )
        # driver_activity_history = DriverActivityHistory(**data)
        # driver_activity_history.save()
        return Response(
            status=status.HTTP_200_OK,
            data=_("Driver Activity Deleted.")
        )

    def __get_device_details(self, driver):
        """
        :param driver: instance of Driver
        :return: driver device details (dict)
        """
        stored_device = driver.device_details
        if stored_device:
            from blowhorn.driver.serializers import DeviceDetailSerializer
            return DeviceDetailSerializer(stored_device).data
        return {}

    def __get_driver_vehicle(self, driver):
        """
        :param driver: instance of Driver
        :return: instance of Vehicle
        """
        dv_map = self.get_vehicle_for_driver(driver)
        try:
            return dv_map.vehicle
        except BaseException:
            return None

    @log_db_queries
    def __get_driver_profile_details(self, driver):
        """
        :param driver: instance of Driver
        :return: basic details of driver and vehicle (dict)
        """
        vehicle = self.__get_driver_vehicle(driver)
        vehicle_model_instance = get_value_from_obj(vehicle, 'vehicle_model')
        vehicle_class_instance = get_value_from_obj(vehicle_model_instance,
                                                    'vehicle_class')
        vehicle_class = get_value_from_obj(vehicle_class_instance,
                                                  'commercial_classification')
        vehicle_model = get_value_from_obj(vehicle_model_instance, 'model_name')
        # rc_num = get_value_from_obj(vehicle, 'registration_certificate_number')

        user = driver.user
        last_login = user.last_login
        login_time = last_login and last_login.timestamp() * 1000
        profile = {
            'id': driver.id,
            'name': driver.name,
            'mobile': user.national_number,
            'avatar_url': driver.avatar_url,
            'login_time': login_time,
            'licence_number': driver.driving_license_number,
            'vehicle_model': vehicle_model,
            'vehicle_class': vehicle_class,
            'vehicle_number': driver.driver_vehicle
        }
        return profile

    def get_order_for_trip(self, trip):
        if trip.order:
            return trip.order
        stops = trip.stops.all()
        if stops:
            return stops[0].waypoint.order if stops[0].waypoint else stops[0].order

        return None

    def __get_device_battery_details(self, details):
        """
        :param details: dict
        :return: battery percent (str)
        """
        battery_details = details.get('battery_info', {})
        if battery_details and isinstance(battery_details, str):
            battery_details = json.loads(battery_details)

        return battery_details

    def __get_other_details_from_activity(self, driver_activity):
        """
        :param driver_activity: instance of DriverActivity
        :return: additional details stored in activity (parsed)
        """
        other_details = driver_activity.other_details
        if isinstance(other_details, str):
            other_details = json.loads(driver_activity.other_details)
        return other_details


    def get_driver_location_details(self, driver_activity, driver=None):
        """
        :param driver_activity: instance of DriverActivity
        :param driver: instance of Driver
        :return: driver location details along with profile and vehicle info
        """
        trip_number = ''
        distance_km = 0
        trip = driver_activity.trip
        if trip:
            trip_number = trip.trip_number
            distance_km = trip.total_distance

        profile = driver and self.__get_driver_profile_details(driver)
        device_details = self.__get_device_details(driver)
        other_details = self.__get_other_details_from_activity(driver_activity)
        battery_info = self.__get_device_battery_details(other_details)
        device_details['battery_info'] = battery_info

        app_version = other_details.get('app_version', None)
        if app_version:
            device_details['app_version_name'] = app_version

        gps_timestamp = driver_activity.gps_timestamp.timestamp() * 1000
        last_updated_time = driver_activity.updated_time.isoformat()
        driver_coords = driver_activity.geopoint.coords

        details = {
            'driver_id': driver.id,
            'profile': profile,
            'device_details': device_details,
            'location': {
                'city': driver.operating_city.name,
                'distance_km': distance_km,
                'currentTime': last_updated_time,
                'mAccuracy': driver_activity.location_accuracy_meters,
                'mLatitude': driver_coords[1],
                'mLongitude': driver_coords[0],
                'mSpeed': driver_activity.vehicle_speed_kmph,
                'mTime': gps_timestamp,
            },
            'extras': {
                'trip_number': trip_number,
                'event': driver_activity.event,
                'has_covid_pass': driver.has_covid_pass,
            },
        }
        return details

    def __get_end_customer(self, shipping_address):
        if shipping_address:
            return shipping_address.first_name
        return ''

    @log_db_queries
    def get_trip_data(self, trip):
        """
        :param trip: instance of Trip
        :return: dict of trip and customer details
        """
        order = self.get_order_for_trip(trip)
        booking_id = ''
        booking_type = ''
        customer_pk = ''
        customer_name = ''
        if order:
            booking_id = order.number
            booking_type = order.order_type
            customer = order.customer
            if customer:
                customer_pk = customer.id
                customer_name = customer.name

        return {
            'trip_number': trip.trip_number,
            'customer_name': customer_name,
            'customer_pk': customer_pk,
            'booking_id': booking_id,
            'booking_type': booking_type,
            # 'end_customer': self.__get_end_customer(order.shipping_address),
        }


class BankAccountUtils(object):

    def get_driver_vendor_bank_details(self, user):
        _result = {}
        driver = user.driver if hasattr(user, 'driver') else None
        vendor = user.vendor if hasattr(user, 'vendor') else None
        _result['has_fleet_owner'] = False

        if not (driver or vendor):
            err = {
                'has_fleet_owner' : _result['has_fleet_owner'],
                'message' : 'User is not a registered driver/fleet owner'
            }
            return False, err

        _ba = None
        if driver:
            _ba = driver.bank_account
            _result['has_fleet_owner'] = True if driver.owner_details else False

        if vendor:
            _ba = vendor.bank_account

        if not _ba:
            err = {
                'message' : 'No Bank Account mapped to the user',
                'has_fleet_owner' : _result['has_fleet_owner'],
            }
            return False, err

        if _ba:
            _result['bank_name'] =  _ba.bank_name or ''
            _result['account_name'] =  _ba.account_name
            _result['account_number'] =  _ba.account_number
            _result['ifsc_code'] =  _ba.ifsc_code

        return True, _result

    def add_driver_vendor_bank(self, user, data):
        bank_account_number = data.get('bank_account_number', None)
        ifsc_code = data.get('ifsc_code', None)
        bank_name = data.get('bank_name', None)

        driver = user.driver if hasattr(user, 'driver') else None
        vendor = user.vendor if hasattr(user, 'vendor') else None
        _existing_account = None
        _msg = {
            'message' : ''
        }

        # basic driver/vendor validation
        if driver:
            _existing_account = driver.bank_account
        elif vendor:
            _existing_account = vendor.bank_account
        else:
            _msg['message'] = 'User is not a registered driver/fleet owner'
            return False, _msg
        
        # check for driver operating city because needed later for creating non ops payment
        if not driver.operating_city:
            _msg['message'] = 'Please update the operating city'
            return False,  _msg

        # validate the account number & ifsc formats
        if not (bank_account_number or ifsc_code):
            _msg['message'] = 'Please provide bank account number & ifsc code'
            return False, _msg

        ifsc_code = ifsc_code.upper()
        bank_account_number = str(bank_account_number)

        if not re.search(r'^[A-Za-z]{4}[0]\w{6}$', ifsc_code):
            _msg['message'] = 'Please provide valid ifsc code'
            return False, _msg

        if not re.match(r'^\d+$', bank_account_number):
            _msg['message'] = 'Please provide valid bank account number'
            return False, _msg

        # check if bank account already exists with same and it shouldnt be mapped to another user
        _ba = BankAccount.objects.filter(account_number=bank_account_number, ifsc_code=ifsc_code).first()
        if _existing_account and _ba and \
            _existing_account.account_number == _ba.account_number and \
            _existing_account.ifsc_code == _ba.ifsc_code:
            _msg['message'] = 'Already mapped to same account'
            return False, _msg

        driver_with_bankaccount_exists, vendor_with_bankaccount_exists = False, False
        if driver:
            driver_with_bankaccount_exists = Driver.objects.filter(id=driver.id,
                    bank_account=_ba).exclude(id=driver.id).exists()
        elif vendor:
            vendor_with_bankaccount_exists = Vendor.objects.filter(id=vendor.id,
                    bank_account=_ba).exclude(id=vendor.id).exists()
        if driver_with_bankaccount_exists or vendor_with_bankaccount_exists:
            _msg['message'] = 'Bank Account is already linked to another user'
            return False, _msg

        # vendor logic is still not there completely & currently will be used for driver side only.
        # check for unsettled payments
        if driver:
            from blowhorn.apps.driver_app.v1.serializers.driver import DriverPayment
            dp = DriverPayment.objects.filter(driver=driver, status=APPROVED, is_settled=False).exists()
            if dp:
                _msg['message'] = 'Account cannot be changed since approved unsettled payment exists'
                return False, _msg

        # call decentro api and validate the document and charge in case of call
        is_verified, err, _out, user_to_be_charged = DecentroService().bank_verification(user, bank_account_number, ifsc_code)
        if driver and user_to_be_charged:
            NonOperationPayment.objects.create(driver=driver, status=APPROVED, city=driver.operating_city,
                purpose=NON_OPS_OTHERS, description='Bank Account Verification Penny Drop', amount=-3.5,
                is_settled=True, start_datetime=date.today(), end_datetime=date.today())

            DriverLedger(
                driver=driver,
                amount=-3.5,
                description='%s, %s - %s' % (
                    'Account Penny Drop Verification',
                    datetime.now().strftime(DATE_FORMAT),
                    datetime.now().strftime(DATE_FORMAT)
                )
            ).save()


        if not is_verified:
            return False, err

        with db.transaction.atomic():
            # create bank account if not present
            if not _ba:
                _bank_details = {
                    'account_name' : _out.get('beneficiaryName','').title(),
                    'account_number' : bank_account_number,
                    'ifsc_code' : ifsc_code,
                    'bank_name' : bank_name
                }
                _ba = BankAccount.objects.create(**_bank_details)
                _ba.created_by=user

            # map the bank accoun to driver/vendor
            if driver:
                driver.bank_account = _ba
                driver.modified_by=user
                driver.save()

                _ba.add_bank_account_history(_ba, user, 'Mapped-D', driver=driver)

                if _existing_account:
                    _existing_account.add_bank_account_history(_existing_account, user, 'Ummapped-D', driver=driver)

            elif vendor:
                vendor.bank_account = _ba
                vendor.modified_by=user
                vendor.save()

                _ba.add_bank_account_history(_ba, user, 'Mapped-FO', vendor=vendor)

                if _existing_account:
                    _existing_account.add_bank_account_history(_existing_account, user, 'Unmapped-FO', vendor=vendor)

            if bank_name and not _ba.bank_name:
                _ba.bank_name = bank_name

            _ba.modified_by=user
            _ba.save()

        _msg['message'] = 'Account has been added successfully'
        return True, _msg

    def delete_bank_accounts(self, bank_account_ids):
        deleted_banks = 0
        log_dump = ''
        try:
            with db.transaction.atomic():
                deleted_banks = BankAccount.objects.filter(id__in=bank_account_ids).delete()
                if deleted_banks:
                    deleted_banks = deleted_banks[0]
                    log_dump = "%s bank account(s) deleted: %s" % (deleted_banks, bank_account_ids)
        except BaseException as e:
            log_dump = '%s' % e

        return deleted_banks, log_dump

    def __do_write(self, bank_account, bank_account_ids):
        log_dump = []
        with db.transaction.atomic():
            try:
                drivers = Driver.objects.filter(bank_account_id__in=bank_account_ids).update(
                    bank_account=bank_account)
                if drivers:
                    log_dump.append('No. of drivers: %s' % drivers)
                ledgers = DriverLedger.objects.filter(bank_details_id__in=bank_account_ids).update(
                    bank_details=bank_account
                )
                if ledgers:
                    log_dump.append('No. of driver ledgers: %s' % ledgers)
                vendors = Vendor.objects.filter(bank_account_id__in=bank_account_ids).update(
                    bank_account=bank_account
                )
                if vendors:
                    log_dump.append('No. of vendors: %s' % vendors)
                bank_account_history = BankAccountHistory.objects.filter(
                    bank_account_id__in=bank_account_ids).update(bank_account=bank_account)
            except BaseException as e:
                log_dump.append('%s' % e)
        return log_dump

    def merge_bank_data(self, bank_account_id, merging_accounts_list):
        bank_account = BankAccount.objects.get(pk=bank_account_id)
        migration_history = {
            'bank_account': ''.join([str(bank_account), '(', str(bank_account.id), ')'])
        }

        bank_account_ids = [x.get('pk') for x in merging_accounts_list]
        migration_history['log_dump'] = self.__do_write(bank_account, bank_account_ids)

        deleted_banks, log = self.delete_bank_accounts(bank_account_ids)
        migration_history['number_of_accounts'] = deleted_banks
        migration_history['log_dump'].append(log)
        migration_history['log_dump'].append(
            ['%s-%s-%s' % (x.get('account_name'), x.get('account_number'), x.get('ifsc_code')) for x
                          in merging_accounts_list]
        )
        logger.info('Migration history: %s' % migration_history)
        try:
            BankMigrationHistory.objects.create(**migration_history)
        except BaseException as e:
            logger.info('%s' % e)
            logger.error("Bank Account migration failed!!")
        return deleted_banks


class NotificationMixin:
    '''
        Send notification to the driver
        Input :
            notification_group: From NotificationMessageTemplate table column
            driver_id : Driver id
            extra_content: Extra content required in dict format
            message_params = List of params for string formatting of template message
                            ('Notification for {}'.format(*message_params))
    '''
    def send_notification(notification_group, driver_id,
                                extra_content=None, message_parms=None):
        from blowhorn.driver.tasks import push_driver_notification
        if not notification_group:
            return

        # Fetch the message templates
        message_templates = NotificationMessageTemplate.objects.filter(
                        notification_group=notification_group)

        if message_templates.exists():
            message_template = message_templates.latest('-modified_by')
            if not message_template.is_active:
                return

            # Format the message if required
            message = message_template.template
            if message and message_parms and len(message_parms) != 0:
                message = message.format(*message_parms)

            # Create the parameter dictionary
            if message:
                notification_data = {}
                action = message_template.action

                if type(extra_content) == dict:
                    notification_data.update(extra_content)

                notification_data.update({
                    'category' : message_template.category,
                    'title' : message_template.title,
                    'message': message,
                    'notification_group': notification_group
                })

                # Push the notification
                push_driver_notification.apply_async((driver_id, action,
                    notification_data))
