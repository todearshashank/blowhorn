from django.contrib import admin
from blowhorn.stock.models import ProductClass, Category, Product, ProductRecommendation, \
    ProductAttribute, ProductAttributeValue, AttributeOptionGroup, AttributeOption, Option, ProductImage, \
    ProductStockRecord, ProductPartner, ProductStockAlert, WeightBased, ProductCategory


admin.site.register(ProductClass)
admin.site.register(Category)
admin.site.register(ProductCategory)
admin.site.register(Product)
admin.site.register(ProductRecommendation)
admin.site.register(ProductAttribute)
admin.site.register(ProductAttributeValue)
admin.site.register(AttributeOptionGroup)
admin.site.register(AttributeOption)
admin.site.register(Option)
admin.site.register(ProductImage)
admin.site.register(ProductStockRecord)
admin.site.register(ProductPartner)
admin.site.register(ProductStockAlert)
admin.site.register(WeightBased)
