from djongo import models
# from django.contrib.gis.db import models as gis_model
from django.db.models import Manager as GeoManager


class WorkSpecificLog(models.Model):

    tripId = models.CharField(
        max_length=128, null=True, blank=True
    )

    contractId = models.CharField(
        max_length=128, null=True, blank=True
    )

    stopId = models.CharField(
        max_length=40, null=True, blank=True
    )

    orderId = models.CharField(
        max_length=40, null=True, blank=True
    )

    tripStatus = models.CharField(
        max_length=50, null=True, blank=True
    )

    tripDistance = models.FloatField(
        default=0.0
    )

    class Meta:
        abstract = True


class DriverActivityLog(models.Model):

    createdTime = models.DateTimeField(
        auto_now_add=True
    )

    driverId = models.CharField(
        max_length=128, null=True
    )

    seqId = models.BigIntegerField(
        default=-1
    )

    city = models.CharField(
        max_length=50, null=True, blank=True
    )

    online = models.BooleanField(
        default=True
    )

    event = models.CharField(
        max_length=128, null=True, blank=True
    )

    locationInfo = models.JSONField(
        null=True, blank=True
    )

    vehicleInfo = models.JSONField(
        null=True, blank=True
    )

    appInfo = models.JSONField(
        null=True, blank=True
    )

    deviceTimeStamp = models.DateTimeField(
        null=True, blank=True
    )

    deviceInfo = models.JSONField(
        null=True, blank=True
    )

    class Meta:
        abstract = True


class DriverOfflineLog(DriverActivityLog):
    pass


class DriverTripLogManager(GeoManager):

    def __apply_filter(self, qs, query_params):
        if isinstance(query_params, dict):
            return qs.filter(**query_params)

        return qs.filter(query_params)

    def __apply_order_by(self, qs, params):
        assert isinstance(params, list), '`order_by` must be a list'
        return qs.order_by(*params)

    def __apply_fetch_only(self, qs, fetch_only):
        assert isinstance(fetch_only, list) or \
               isinstance(fetch_only, tuple), '`fetch_only` must be either list or tuple'
        return qs.only(*fetch_only)

    def __add_select_related(self, qs, selected_related):
        assert isinstance(selected_related, list) or isinstance(
            selected_related, tuple), '`selected_related` must be either list or tuple'
        return qs.select_related(*selected_related)

    def __add_prefetch_related(self, qs, prefetch_related):
        assert isinstance(prefetch_related, list) or isinstance(
            prefetch_related, tuple), '`prefetch_related` must be either list or tuple'
        return qs.prefetch_related(*prefetch_related)

    def fetch(
        self,
        query_params=None,
        fetch_only=None,
        selected_related=None,
        prefetch_related=None,
        order_by=None,
    ):
        """
        :param query_params: dict or constructed query filter
        :param fetch_only: list or tuple of fields to be fetched
        :param selected_related: list or tuple of related model field names
        :param prefetch_related: list or tuple of reverse related model field names
        :param order_by: list of fields to be ordered by
        :return: queryset
        """
        qs = self.get_queryset()
        qs = self.__apply_filter(qs, query_params) if query_params else qs.all()
        if fetch_only is not None:
            qs = self.__apply_fetch_only(qs, fetch_only)

        if selected_related is not None:
            qs = self.__add_select_related(qs, selected_related)

        if prefetch_related is not None:
            qs = self.__add_prefetch_related(qs, prefetch_related)

        if order_by is not None:
            qs = self.__apply_order_by(qs, order_by)

        return qs


class DriverTripLog(DriverActivityLog, WorkSpecificLog):

    objects = models.Manager()
    custom_manager = DriverTripLogManager()

    class Meta:
        indexes = [
            models.Index(fields=['tripId', ]),
            models.Index(fields=['orderId', ]),
            models.Index(fields=['tripId', 'orderId', 'stopId']),
            models.Index(fields=['contractId']),
        ]
