import paho.mqtt.client as mqtt

from django.conf import settings


MQTT_BROKER_HOST = getattr(settings, 'MQTT_BROKER_HOST', 'broker.hivemq.com')
MQTT_BROKER_PORT = getattr(settings, 'MQTT_BROKER_PORT', 1883)
MQTT_BROKER_USERNAME = getattr(settings, 'MQTT_BROKER_USERNAME', None)
MQTT_BROKER_PASSWORD = getattr(settings, 'MQTT_BROKER_PASSWORD', None)
MQTT_KEEP_ALIVE_INTERVAL = getattr(settings, 'MQTT_KEEP_ALIVE_INTERVAL', 60)

DEBUG = getattr(settings, 'DEBUG', True)


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected = OK")
    else:
        print("Bad connection Returned code = ", rc)


def on_message(client, userdata, msg):
    listen_to_mqtt(msg.topic, msg.payload)


def on_publish(client, userdata, mid):
    print("Published successfully = OK")


def listen_to_mqtt(topic, message):
    if "something" in topic:
        pass


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_publish = on_publish
if MQTT_BROKER_USERNAME and MQTT_BROKER_PASSWORD:
    client.username_pw_set(MQTT_BROKER_USERNAME, MQTT_BROKER_PASSWORD)
print("Connecting to MQTT broker %s:%s | Keep Alive : %s" % (
    MQTT_BROKER_HOST, MQTT_BROKER_PORT, MQTT_KEEP_ALIVE_INTERVAL))
client.connect_async(MQTT_BROKER_HOST, MQTT_BROKER_PORT, MQTT_KEEP_ALIVE_INTERVAL)
if DEBUG:
    client.loop_forever()
else:
    client.loop_start()
