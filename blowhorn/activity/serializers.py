from django.conf import settings

from rest_framework import serializers

from blowhorn.activity.models import DriverTripLog
from blowhorn.customer.constants import EVENT_INGESTION_DEFAULT_TIME_FORMAT


DEBUG = getattr(settings, 'DEBUG', True)


class GPSEventSerializer(serializers.ModelSerializer):

    tracking_id = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField()
    latitude = serializers.SerializerMethodField()
    longitude = serializers.SerializerMethodField()
    altitude = serializers.SerializerMethodField()
    source = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    attributes = serializers.SerializerMethodField()

    class Meta:
        model = DriverTripLog
        fields = [
            'tracking_id', 'time', 'latitude', 'longitude', 'altitude',
            'source', 'type', 'attributes']

    def get_tracking_id(self, event):
        return event.vehicleInfo.get('registration_certificate_number', None)

    def get_time(self, event):
        return event.createdTime.strftime(EVENT_INGESTION_DEFAULT_TIME_FORMAT)

    def get_latitude(self, event):
        return event.locationInfo.get('mLatitude', None)

    def get_longitude(self, event):
        return event.locationInfo.get('mLongitude', None)

    def get_altitude(self, event):
        return event.locationInfo.get('mAltitude', None)

    def get_source(self, event):
        return 'testing' if DEBUG else 'blow_horn'

    def get_type(self, event):
        return 'vehicle_event' if DEBUG else 'location'

    def get_attributes(self, event):
        return {}


class DriverTripLogSerializer(serializers.ModelSerializer):

    id = serializers.CharField(max_length=1000, read_only=True)

    def __init__(self, *args, **kwargs):
        super(DriverTripLogSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = DriverTripLog
