import logging
from datetime import datetime

from blowhorn.activity.models import DriverTripLog
from blowhorn.activity.constants import PRIOR_ROUTE_QUERY_FIELDS, \
    MAX_ACCEPTABLE_ACCURACY_METERS, DEFAULT_ACCURACY_METERS, \
    VEHICLE_THEORETICAL_SPEED_LIMIT_AVG_KMPH, MIN_TIMEDELTA_SECONDS, \
    VEHICLE_THEORETICAL_SPEED_LIMIT_KMPH
from blowhorn.common.helper import CommonHelper
from blowhorn.utils.functions import dist_between_2_points
from django.db.models import Q

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ActivityMixin(object):

    def _smoothen_geopoints(
        self,
        points,
        max_location_accuracy_meters=VEHICLE_THEORETICAL_SPEED_LIMIT_AVG_KMPH,
        min_time_delta_seconds=MIN_TIMEDELTA_SECONDS,
        max_speed_limit=VEHICLE_THEORETICAL_SPEED_LIMIT_KMPH,
    ):
        """ Function to smoothen a given array of geopoints
            based on gps accuracy, time closeness of adjacent points
            calculated speed between adjacent points
        """
        if not points:
            return []
        smoothened_points = points[:1]
        smoothened_geopoints = [CommonHelper.get_geopoint_from_latlong(
            smoothened_points[0].locationInfo.get('mLatitude', None),
            smoothened_points[0].locationInfo.get('mLongitude', None)), ]

        max_speed_violated_points = 0
        low_speed_small_distance_points = 0
        time_closeness_points = 0
        total_distance_km = 0.0
        total_distance_small_distance_inclusive_km = 0.0
        smoothened_distance_km = 0.0

        for i, p in enumerate(points[1:]):
            p1 = smoothened_points[-1]
            p2 = p

            p1_geopoint = CommonHelper.get_geopoint_from_latlong(
                p1.locationInfo.get('mLatitude', None),
                p1.locationInfo.get('mLongitude', None))
            p2_geopoint = CommonHelper.get_geopoint_from_latlong(
                p2.locationInfo.get('mLatitude', None),
                p2.locationInfo.get('mLongitude', None)
            )

            distance_km = dist_between_2_points(p1_geopoint, p2_geopoint)
            p1_gps_timestamp, p2_gps_timestamp = p1.deviceTimeStamp, p2.deviceTimeStamp

            if isinstance(p1_gps_timestamp, str):
                p1_gps_timestamp = datetime.datetime.strptime(
                    p1_gps_timestamp, "%Y-%m-%d %H:%M:%S")
                p2_gps_timestamp = datetime.datetime.strptime(
                    p2_gps_timestamp, "%Y-%m-%d %H:%M:%S")
            time_delta_secs = (p2_gps_timestamp - p1_gps_timestamp).total_seconds()
            total_distance_km += distance_km

            if time_delta_secs < min_time_delta_seconds or distance_km < 0.02:
                time_closeness_points += 1
                continue

            this_accuracy = p1.locationInfo.get('mAccuracy', None) + \
                p2.locationInfo.get('mAccuracy', None)
            # logger.info(distance_km, type(distance_km), this_accuracy,
            #             type(this_accuracy))
            if distance_km * 1000 < this_accuracy:
                continue

            # Sanity check if this point makes sense like a random gps jump will
            # result in high speed
            speed_instant_kmph = distance_km * 60 * 60 / time_delta_secs
            if speed_instant_kmph > max_speed_limit:
                max_speed_violated_points += 1
                continue

            # Filter out points that usually occur when device is stationary
            # for a long time and add to noise
            # - Calculated speed < 5% max
            # - GPS speed < 5% max
            # - Distance < accuracy threshold
            if speed_instant_kmph < 0.05 * max_speed_limit \
                and float(p2.locationInfo.get('mSpeed')) < 0.05 * \
                max_speed_limit and (distance_km * 1000) < \
                    10:
                low_speed_small_distance_points += 1
                total_distance_small_distance_inclusive_km += distance_km
                continue

            smoothened_distance_km += distance_km
            smoothened_points.append(p2)
            smoothened_geopoints.append(p2_geopoint)

        return smoothened_geopoints

    def _filter_on_accuracy_meters(self, results):
        return [
            r for r in results
            if r.locationInfo.get('mAccuracy', DEFAULT_ACCURACY_METERS) <= MAX_ACCEPTABLE_ACCURACY_METERS
        ]

    def get_prior_route(self, order=None, trip=None, stop=None, start_time=None):
        """
        :param order: instance of Order
        :param trip: instance of Trip
        :param stop: instance of Stop
        :param start_time: datetime object
        :return: list of geopoints
        """
        query_params = None

        if order is None and trip is None:
            return []

        if trip:
            query_params = query_params & Q(
                tripId=str(trip.id)) if query_params else Q(tripId=str(trip.id))

        if stop:
            query_params = query_params & Q(stopId=str(stop.id)) \
                if query_params else Q(stopId=stop.id)

        if start_time and isinstance(start_time, datetime):
            query_params = query_params & Q(createdTime__gte=start_time)
        elif trip and trip.actual_start_time:
            query_params = query_params & Q(
                createdTime__gte=trip.actual_start_time)

        if order:
            query_params = query_params & Q(
                orderId=str(order.id)) if query_params else Q(
                orderId=str(order.id))

        if trip.actual_end_time:
            query_params = query_params & Q(
                createdTime__lte=trip.actual_end_time)
        results = DriverTripLog.custom_manager.fetch(
            query_params, fetch_only=PRIOR_ROUTE_QUERY_FIELDS)

        if not results:
            return []

        results_filtered = self._filter_on_accuracy_meters(results)
        sorted_points = sorted(results_filtered,
                               key=lambda x: x.locationInfo.get('mTime'))
        smoothed_points = self._smoothen_geopoints(sorted_points)
        return smoothed_points

