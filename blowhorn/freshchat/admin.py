from django.contrib import admin

from blowhorn.freshchat.models import WhatsappMessage


@admin.register(WhatsappMessage)
class WhatsappMessageAdmin(admin.ModelAdmin):
    list_display = ('order', 'template_name', 'status_code')

    search_fields = ['order__number']

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

