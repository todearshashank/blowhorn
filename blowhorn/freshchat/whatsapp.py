import json
import requests
from django.conf import settings

from blowhorn.freshchat.constant import WHATSAPP_ENABLED, WHHATSAPP_CONFIG
from blowhorn.freshchat.models import WhatsappMessage

def send_whatsapp_notification(to_numbers_list, msg_template_data, order):

    if not WHATSAPP_ENABLED.get(settings.WEBSITE_BUILD) or \
        not settings.ENABLE_SLACK_NOTIFICATIONS:
        return

    request_body = {"from": {
        "phone_number": WHHATSAPP_CONFIG[settings.WEBSITE_BUILD].get(
            'from_phone_number')},
        "to": []}

    for to_number in to_numbers_list:
        request_body["to"].append({"phone_number": to_number})

    request_body.update({"data": {
        "message_template": {
            "storage": "none",
            "namespace": WHHATSAPP_CONFIG[settings.WEBSITE_BUILD].get(
                'namespace'),
            "template_name": msg_template_data.get("template_name"),
            "language": {
                "policy": "fallback",
                "code": "en"
            },
            "template_data": [
                {"data": param} for param in
                msg_template_data.get("params_data")
            ]
        }
    }})
    headers = {'Authorization': settings.WHATSAPP_KEY}
    response = requests.post(url=settings.WHATSAPP_OUTBOUND_MSG_URL,
                             headers=headers, data=json.dumps(request_body))

    response_dict = response.__dict__
    status_code = response_dict.get('status_code')
    whatsapp_msg = {
        'order': order,
        'template_name': msg_template_data.get("template_name"),
        'status_code': status_code
    }

    _resp_data = response_dict.get('_content')
    _resp_data = json.loads(_resp_data)
    if status_code == 202 or status_code == 200:
        whatsapp_msg.update({'request_id': _resp_data.get('request_id'),
                             'get_href': _resp_data['link'].get('href')})
    else:
        whatsapp_msg.update({'error_message': _resp_data.get('error_message')})

    WhatsappMessage.objects.create(**whatsapp_msg)

