from django.db import models
from django.utils.translation import ugettext_lazy as _


class WhatsappMessage(models.Model):
    """
    Order Message
    """
    order = models.ForeignKey('order.Order', null=True, blank=True,
                              on_delete=models.PROTECT)

    template_name = models.CharField(_("Template Name"), max_length=50,
                                     null=True, blank=True)

    trip = models.ForeignKey('trip.Trip', null=True, blank=True,
                             on_delete=models.PROTECT)

    time = models.DateTimeField(_("Time"), auto_now_add=True)

    message = models.TextField(_("Message Text"), null=True, blank=True)

    status = models.CharField(_("Message Status"), max_length=64, null=True,
                              blank=True)

    request_id = models.CharField(_("Request Id"), max_length=128, null=True,
                                  blank=True)

    status_code = models.CharField(_("API call Status"), max_length=64,
                                   null=True, blank=True)

    error_message = models.CharField(_("Error message"), max_length=128,
                                     null=True, blank=True)

    get_href = models.CharField(_("Get message status url"), max_length=512,
                                null=True, blank=True)

    class Meta:
        verbose_name = _('WhatsApp Message')

    def __str__(self):
        return '%s-%s' % (self.order, self.template_name)

