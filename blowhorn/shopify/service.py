import requests
import logging
import hmac
import re
import hashlib
import string
import random
from blowhorn.coupon.constants import DISCOUNT_TYPE_FIXED
from blowhorn.wms.submodels.sku import KitSkuMapping
import phonenumbers
import json
from decimal import Decimal as D
from requests.auth import HTTPBasicAuth
from datetime import datetime

from django.db import transaction
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ValidationError

from rest_framework import status

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Content, MimeType, TrackingSettings, ClickTracking, To

from blowhorn.common.firebase_operations import broadcast_to_firebase
from blowhorn.oscar.core.loading import get_model
from blowhorn.order.const import PAYMENT_MODE_CASH, PAYMENT_MODE_ONLINE
from blowhorn.address.utils import get_wms_hub_for_customer
from blowhorn.shopify.onboarding_email import ONBOARDING_NEW_EMAIL_HEAD
from blowhorn.shopify.welcome_email import (WELCOME_EMAIL_HEAD,
        WELCOME_EMAIL_BODY_OLDUSER, WELCOME_EMAIL_BODY_NEWUSER, WELCOME_EMAIL_FOOTER)
from blowhorn.shopify.constants import (APP_INSTALLATION_URL, APP_PREINSTALL_URL,
    EC001_HMAC_ERROR, EC002_PARM_MISSING, EC003_INVALID_SHOPNAME,
    EC004_TOKEN_NOT_FOUND, EC005_SHOPNAME_MISSING, EC006_INVALID_REQUEST_ORIGIN,
    FIREBASE_SHOPIFY_SYNC, GET_SHOPIFY_STOREDATA_URL, GET_STORE_AUTH_TOKEN, PAYMENT_COD_GATEWAY,
    POST_INSTALLATION_REDIRECT_URL, PUSH_ORDER_REDIRECT,
    SYNC_ALL_ORDERS, SYNC_ORDERS, SYNC_SPECIFIC_ORDERS)

WhSku = get_model('wms', 'WhSku')
Customer = get_model('customer', 'Customer')
Partner = get_model('customer', 'Partner')
PartnerCustomer = get_model('customer', 'PartnerCustomer')
User = get_model('users', 'User')
Order = get_model('order', 'Order')
ShopifyAuthcode = get_model('shopify', 'ShopifyAuthcode')
ShopifyAccessScope = get_model('shopify', 'ShopifyAccessScope')
OrderBatch = get_model('order', 'OrderBatch')
ShopifyWebhookSubscription = get_model('shopify', 'ShopifyWebhookSubscription')
ShopifyLog = get_model('shopify', 'ShopifyLog')

HOST = settings.HOST
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class ShopifyService:

    def validate_hmac(self, parameters, orderlist=None):
        hmac_verified = False
        request_hmac = parameters.get('hmac', None)

        varlist = []
        for key,value in parameters.items():
            if key in ['hmac','ids[]']:
                continue
            temp = '{}={}'.format(key,value)
            varlist.append(temp)

        if orderlist:
            _order = ["%s" % e for e in orderlist]
            temp = '{}={}'.format('ids',_order)
            varlist.append(temp)

        if len(varlist) != 0:
            parms = '&'.join(varlist)
            generated_hmac = hmac.new(
                key=settings.SHOPIFY_SECRET.encode('utf-8'),
                msg=parms.encode('utf-8'),
                digestmod=hashlib.sha256
            )
            hmac_verified = generated_hmac.hexdigest() == request_hmac

        return hmac_verified

    def validate_shopname(self,shopname):
        is_valid_shopname = None
        pattern = '^[a-zA-Z0-9][a-zA-Z0-9\-]*\.myshopify\.com$'
        is_valid_name = re.match(pattern, shopname)

        if is_valid_name:
            is_valid_shopname = shopname.split('.')[0]

        return is_valid_shopname

    def parameter_extraction(self,parameters):
        hmac = parameters.get('hmac', None)
        shop = parameters.get('shop', None)
        code = parameters.get('code', None)
        state = parameters.get('state', None)

        return hmac, shop, code, state

    def generate_user_password(self):
        LETTERS = string.ascii_letters
        NUMBERS = string.digits

        printable = f'{LETTERS}{NUMBERS}'
        printable = list(printable)
        random.shuffle(printable)

        length = random.randint(8,10)
        random_password = random.choices(printable, k=length)
        random_password = ''.join(random_password)
        return random_password

    def validate_authorization_code(self, shop, code, to_be_verified):
        is_verified_request = False
        _code = None
        if to_be_verified:
            sac = ShopifyAuthcode.objects.filter(shopname=shop, code=code)

            if sac.exists():
                sac_ob = sac.latest('modified_date')
                is_verified_request = True
                _code = sac_ob.code
        else:
            _code = self.generate_user_password()
            ShopifyAuthcode.objects.create(shopname=shop, code=_code)

        return is_verified_request, _code

    def shopify_request_validation(self, parms, to_be_verified, order_list=None):
        is_hmac_valid = self.validate_hmac(parms, order_list)

        if not is_hmac_valid:
            raise ValidationError(status.HTTP_400_BAD_REQUEST, EC001_HMAC_ERROR)

        hmac, shopname, code, state = self.parameter_extraction(parms)
        if not shopname:
            raise ValidationError(status.HTTP_400_BAD_REQUEST, EC005_SHOPNAME_MISSING)

        shopname = self.validate_shopname(shopname)
        if not shopname:
            raise ValidationError(status.HTTP_400_BAD_REQUEST, EC003_INVALID_SHOPNAME)

        if to_be_verified:
            if not (hmac and shopname and code and state):
                raise ValidationError(status.HTTP_400_BAD_REQUEST, EC002_PARM_MISSING)
        else:
            if not (hmac and shopname):
                raise ValidationError(status.HTTP_400_BAD_REQUEST, EC002_PARM_MISSING)

        is_verified, state =self.validate_authorization_code(shopname, state, to_be_verified)
        if to_be_verified and not is_verified:
            raise ValidationError(status.HTTP_400_BAD_REQUEST, EC006_INVALID_REQUEST_ORIGIN)

        return hmac, shopname, code, state

    def send_welcome_mail(self, is_old_user, name, password, email):
        try:
            body_text = WELCOME_EMAIL_BODY_NEWUSER.format(user=name, email=email, password=password, host=HOST)
            if is_old_user:
                body_text = WELCOME_EMAIL_BODY_OLDUSER.format(user=name, email=email)

            _text = WELCOME_EMAIL_HEAD + body_text + WELCOME_EMAIL_FOOTER
            self.send_shopify_mail(_text, "Welcome to Blowhorn", email)

        except Exception as e:
            print(e)

    def send_onboarding_mail(self, email, customername):
        try:
            _text = ONBOARDING_NEW_EMAIL_HEAD
            subject = "Customer Onboarding %s" % customername
            self.send_shopify_mail(_text, subject, email, cc=[settings.CHANNEL_SALES_MAIL])

        except Exception as e:
            print(e)

    def send_shopify_mail(self, data, subject, email, cc=[]):
        sendgrid_client = SendGridAPIClient(settings.SENDGRID_API_KEY)
        content = Content(MimeType.html, data)
        message = Mail(settings.DEFAULT_FROM_EMAIL, None, subject, content)

        if not settings.ENABLE_SLACK_NOTIFICATIONS:
            message.to = settings.DEFAULT_TESTMAIL
        else:
            message.to = To(email)
            message.cc = cc

        tracking_settings = TrackingSettings()
        tracking_settings.click_tracking = ClickTracking(enable=False, enable_text=False)

        message.tracking_settings = tracking_settings
        sendgrid_client.send(message=message)

    def create_shopify_user(self, shopname, token):
        CUSTOMER_CATEGORY_ENTERPRISE = u'Enterprise'
        url = GET_SHOPIFY_STOREDATA_URL.format(shop=shopname)
        headers = {'content-type': 'application/json', 'X-Shopify-Access-Token': token}
        response = requests.get(url , headers=headers)
        response_data = json.loads(response.text)
        resp = response_data.get('shop')
        extra_data_key = ['shop_owner','phone']

        mobile_number = resp.get('phone', None)
        _mobile = None
        if not (mobile_number == None):
            try:
                _mobile = phonenumbers.parse(mobile_number,settings.ACTIVE_COUNTRY_CODE)
            except:
                _mobile = None

        password=self.generate_user_password()
        extra_data = {}
        for key in extra_data_key:
            extra_data[key] = resp.get(key,None)

        customer = None
        old_customer = False
        with transaction.atomic():
            user = User.objects.filter(email=resp.get('email', None)).first()
            if not user:
                user_parm = {
                    "email" : resp.get('email', None),
                    "password" : password,
                    "name" : resp.get('name', None),
                    "is_active" : True,
                    "is_email_verified" : True
                }
                if _mobile:
                    user_parm["phone_number"] = _mobile.national_number

                print('user_parm',user_parm)
                user = User.objects.create_user(**user_parm)

            customer = Customer.objects.filter(user=user).first()
            if not customer:
                name = '%s - %s' % (resp.get('name', None), 'Shopify')
                customer = Customer.objects.create(
                     user=user, legalname=resp.get('name', None),
                     name=name,
                     source=resp.get('domain', None),
                     is_active=True,
                     customer_category = CUSTOMER_CATEGORY_ENTERPRISE,
                     customer_specific_info=extra_data
                )
            else:
                old_customer = True
                if not customer.legalname:
                    customer.legalname = resp.get('name', None)
                if not customer.name:
                    customer.name = resp.get('name', None)
                if not customer.source:
                    customer.source=resp.get('domain', None)
                if not customer.is_active:
                    customer.is_active = True

            customer.customer_specific_info = extra_data
            customer.save()

        if customer:
            partner = Partner.objects.get(legalname='Shopify')
            store_details = json.dumps(resp)
            _store_domain=resp.get('myshopify_domain', None)
            _pcm = PartnerCustomer.objects.filter(store_domain=_store_domain, partner_token=token)

            if not _pcm:
                PartnerCustomer.objects.filter(partner=partner,
                    customer=customer).update(is_active=False, is_deleted=True)
                pcm = PartnerCustomer.objects.create(customer=customer,
                        email=resp.get('email', None),
                        partner_token=token,
                        store_domain=_store_domain,
                        partner=partner,
                        store_data=store_details
                    )

                if settings.ENABLE_SLACK_NOTIFICATIONS:
                    logger.info('Sending mail to %s' % customer)
                    self.send_welcome_mail(old_customer, customer.name, password, user.email)
                    self.send_onboarding_mail(user.email, customer.name)

                from blowhorn.shopify.tasks import register_fulfillment
                register_fulfillment.apply_async(args=[pcm.id])

    def pre_install_verification(self, parms):
        scope = 'write_orders,read_orders,read_customers,read_products,read_inventory,'\
                    'read_locations,read_fulfillments,write_fulfillments,' \
                    'read_assigned_fulfillment_orders,write_assigned_fulfillment_orders'

        scope_list = ShopifyAccessScope.objects.filter(is_active=True
                        ).values_list('scope_name', flat=True)
        if scope_list:
            scope = ','.join(scope_list)
        hmac, shopname, code, state = self.shopify_request_validation(parms, False)

        request_data = {
            'shop': shopname,
            'api_key': settings.SHOPIFY_CLIENT_ID,
            'scopes': scope,
            'redirect_uri': HOST + APP_INSTALLATION_URL,
            'nonce': state,
        }

        redirect_url = APP_PREINSTALL_URL.format(**request_data)
        return redirect_url

    def push_bulk_order(self, parms):
        order_list = parms.getlist('ids[]', None)
        shopname = parms.get('shop', None)
        redirect_url = PUSH_ORDER_REDIRECT.format(shop=shopname)

        pc = PartnerCustomer.objects.filter(partner__legalname='Shopify',
                store_domain=shopname, is_active=True).first()

        if pc and order_list:
            _orders = ','.join(order_list)
            from blowhorn.shopify.tasks import sync_customer_orders
            sync_customer_orders.apply_async(args=[pc.customer.id, _orders],)

        return redirect_url

    def app_installation(self, parms):
        hmac, shopname, code, state = self.shopify_request_validation(parms, True)

        redirect_url = HOST + POST_INSTALLATION_REDIRECT_URL
        headers = {'content-type': 'application/json'}

        _url = GET_STORE_AUTH_TOKEN.format(shop=shopname)
        request_data = {
                        'client_id' : settings.SHOPIFY_CLIENT_ID,
                        'client_secret' : settings.SHOPIFY_SECRET,
                        'code' : code,
                    }
        response = requests.post(_url , json=request_data , headers=headers)
        resp = json.loads(response.text)
        token = resp.get('access_token', None)

        if not token:
            raise ValidationError(status.HTTP_400_BAD_REQUEST, EC004_TOKEN_NOT_FOUND)

        self.create_shopify_user(shopname, token)
        return redirect_url

    def get_formatted_order_data(self, data, pickup_data, is_wms_flow, delivery_code):
        order = {}
        status = False

        shipping_lines = data.get('shipping_lines', None)
        if shipping_lines and delivery_code:
            _code_arr = delivery_code.split(',')
            _code = shipping_lines[0].get('code', None)
            if _code and not(_code in _code_arr):
                return status, order

        # reference and customer reference number
        order['reference_number'] = data.get('name')
        order['customer_reference_number'] = data.get('id')

        order['currency_code'] = data.get('currency')
        # Weight in grams and store in kg
        weight = data.get('total_weight',0)
        if weight != 0:
            order['weight'] = weight * 0.001

        # cod & amount processing
        _total_amount = float(data.get('total_price',0))
        _total_tax = float(data.get('total_tax'))
        order['tax'] = _total_tax
        order['total_tax_inclusive_amount'] = _total_amount
        order['total_tax_exclusive_amount'] = _total_amount - _total_tax
        order['discount'] = data.get('total_discounts')

        gateway = data.get('gateway', None)
        order['payment_mode'] = PAYMENT_MODE_ONLINE
        order['cash_on_delivery'] = ''
        order['is_cod'] = False

        # cod is the total outstanding amount
        outstanding_amount = float(data.get('total_outstanding',0))
        order['total_payable'] = outstanding_amount
        if outstanding_amount > 0:
            order['payment_mode'] = PAYMENT_MODE_CASH
            order['cash_on_delivery'] = outstanding_amount
            order['is_cod'] = True
        elif gateway == PAYMENT_COD_GATEWAY:
            order['payment_mode'] = PAYMENT_MODE_CASH
            order['cash_on_delivery'] = _total_amount
            order['is_cod'] = True

        shipping_price_set = data.get('total_shipping_price_set', {})
        if shipping_price_set:
            _shipping_total = float(shipping_price_set['shop_money']['amount'])
            order['shipping_charges'] = _shipping_total

        # Shipping address & phone number processing
        delivery_address_details = data.get('shipping_address', None)
        if not delivery_address_details:
            return status, order
        _customer_mobile = data.get('phone', None)
        customer_data = data.get('customer', None)
        if customer_data and not _customer_mobile:
            _customer_mobile = customer_data.get('phone', None)

        customer_mobiles = delivery_address_details.get('phone', _customer_mobile) or _customer_mobile
        if customer_mobiles:
            customer_mobiles_arr = customer_mobiles.split('/')
            if len(customer_mobiles_arr) == 1:
                order['customer_mobile'] = str(customer_mobiles).strip()
            elif len(customer_mobiles_arr) == 2:
                order['customer_mobile'] = str(customer_mobiles_arr[0]).strip()
                order['alternate_customer_mobile'] = str(customer_mobiles_arr[1]).strip()

        if delivery_address_details:
            _delivery_pincode = delivery_address_details.get('zip', '') or ''
            if ' ' in _delivery_pincode:
                _delivery_pincode = _delivery_pincode.strip().replace(' ', '')
            order['customer_name'] = delivery_address_details.get('name', None)
            order['customer_email'] = data.get('contact_email', None) or data.get('email', None)
            order['delivery_postal_code'] = _delivery_pincode
            order['delivery_lat'] = delivery_address_details.get('latitude', None)
            order['delivery_lon'] = delivery_address_details.get('longitude', None)

            delivery_address = "%s %s" % (delivery_address_details.get('address1'),
                                    delivery_address_details.get('address2'))
            address_details = [delivery_address, delivery_address_details.get('city')]
            _province = delivery_address_details.get('province', None)
            if _province:
                address_details.append(_province)

            address_details.append(delivery_address_details.get('country'))
            order['delivery_address'] = ','.join(address_details)

        # if pickup_address_details:
        #     order['pickup_customer_name'] = pickup_address_details.get('name', None)
        #     order['pickup_customer_mobile'] = pickup_address_details.get('phone', None)
        #     order['pickup_postal_code'] = pickup_address_details.get('zip', None)
        #     order['pickup_lat'] = pickup_address_details.get('latitude', None)
        #     order['pickup_lon'] = pickup_address_details.get('longitude', None)

        #     pickup_address = pickup_address_details.get('address1') + \
        #                             pickup_address_details.get('address2')
        #     address_details = [
        #                         pickup_address,
        #                         pickup_address_details.get('city'),
        #                         pickup_address_details.get('province'),
        #                         pickup_address_details.get('country')
        #                     ]
        #     order['pickup_address'] = ','.join(address_details)

        skus, kit_skus = self.get_order_lines(data, is_wms_flow)
        if len(skus) == 0 and len(kit_skus) == 0:
            return status, order

        # remarks and notes processing
        order['delivery_instructions'] = data.get('note', None)
        _remarks = data.get('note_attributes', None)
        if _remarks:
            order['remarks'] = json.dumps(_remarks)
            _date = [item['value'] for item in _remarks
                            if item['name'] == 'date' and item['value'] != '']
            if _date:
                _expected_delivery_time = None
                try:
                    _expected_delivery_time = datetime.fromisoformat(_date[0]).isoformat()
                except ValueError:
                    pass

                if not _expected_delivery_time:
                    try:
                        _expected_delivery_time = datetime.strptime(_date[0], '%d/%m/%Y').isoformat()
                    except ValueError:
                        pass

                order['expected_delivery_time'] = _expected_delivery_time if _expected_delivery_time else ""

        order['item_details'] = skus
        order['kit_skus'] = kit_skus
        order['source'] = 'shopify'
        status = True
        return status, order

    def get_order_lines(self, data, is_wms_flow):
        order_lines = data.get('line_items',[])
        skus = []
        kit_skus = []

        for ol in order_lines:
            temp={}
            _kit_sku = None
            temp['item_name'] = ol['title']

            _gm = ol['grams']
            if _gm:
                temp['weight'] = _gm * 0.001 if _gm else 0
                temp['uom'] = 'kg'

            temp['item_price_per_each'] = ol['price']
            temp['mrp'] = ol['price']
            temp['item_quantity'] = ol['quantity']
            temp['discounts'] = ol['total_discount']
            temp['discount_type'] = DISCOUNT_TYPE_FIXED

            temp['total_item_price'] = ol['price']
            if temp['total_item_price'] and temp['item_quantity']:
                temp['total_item_price'] = str(D(ol['price']) * D(temp['item_quantity']))

            if is_wms_flow:
                # temp['is_whsku'] = True
                _variant_id = ol['variant_id']

                _kit_sku = WhSku.objects.filter(shopify_variant_id=_variant_id, is_kit_sku=True).first()
                if _kit_sku:
                    temp['name'] = ol['title']
                    kits = KitSkuMapping.objects.filter(parent_sku=_kit_sku).select_related('child_sku')
                    temp['item_details'] = [{
                        'item_name' : k.child_sku.name,
                        'item_quantity' : k.quantity,
                        'shopify_variant_id' : k.child_sku.shopify_variant_id,
                        'shopify_product_id' : k.child_sku.shopify_product_id
                    } for k in kits]

                temp['shopify_variant_id'] = _variant_id
                temp['shopify_product_id'] = ol['product_id']

            _tax_lines = ol.get('tax_lines', None)
            for tax in _tax_lines:
                _tax_category = tax.get('title', None)
                if not _tax_category:
                    continue
                if _tax_category == 'CGST':
                    temp['cgst'] = tax['price']
                if _tax_category == 'SGST':
                    temp['sgst'] = tax['price']
                if _tax_category == 'IGST':
                    temp['igst'] = tax['price']

            if is_wms_flow and _kit_sku:
                kit_skus.append(temp)
            else:
                skus.append(temp)

        return skus, kit_skus

    def get_lastsync_datetime(self, partnercustomer):
        _end = partnercustomer.date_created
        end_time = _end.isoformat()

        # batch = OrderBatch.objects.filter(customer=partnercustomer.customer,
        #                 source='Shopify', start_time__gte=_end)
        # if batch:
        #     end_time = batch.latest('start_time').start_time.isoformat()

        return end_time

    def process_create_event_order(self, customer, order, is_wms_flow, delivery_code):
        print("1")
        order_number = order.get('id')
        order_exists = Order.objects.filter(customer=customer,
                            customer_reference_number=order_number).exists()

        sync_response_list = {}
        if order_exists:
            sync_response_list[''] = 'Shopify order #%s already exists in the system' % order_number

        print('Order exists', order_exists)
        if not order_exists:
            proceed_flag, _order = self.get_formatted_order_data(order, {}, is_wms_flow, delivery_code)
            print('Proceed:', proceed_flag, 'Order exists', order_exists)
            if proceed_flag:
                ref_number, final_message = self.create_order(_order, customer.api_key, is_wms_flow)

                if ref_number:
                    sync_response_list[final_message] = [ref_number,]
                else:
                    sync_response_list[''] = 'Shopify order #%s has been created successfully' % order_number
        print("2")
        if sync_response_list:
            summary_json = json.dumps(sync_response_list)
            from blowhorn.shopify.tasks import push_sync_notif_to_dashboard
            push_sync_notif_to_dashboard.apply_async(args=[summary_json, customer.id])

    def sync_orders(self, customer , orders=None, sync_all=False, last_sync_time=None):
        source_name = 'Shopify'
        if orders:
            source_name = 'Shopify Event'

        pc = PartnerCustomer.objects.filter(partner__legalname='Shopify', customer=customer, is_active=True).first()
        if (not pc) or (pc and not pc.partner_token):
                return

        auth=HTTPBasicAuth(settings.SHOPIFY_CLIENT_ID, settings.SHOPIFY_SECRET)
        headers = {'content-type': 'application/json', 'X-Shopify-Access-Token' : pc.partner_token}

        shopname = pc.store_domain.split('.')[0]
        if not last_sync_time:
            last_sync_time = self.get_lastsync_datetime(pc)
        if orders:
            _url = SYNC_SPECIFIC_ORDERS.format(shop=shopname, orderlist=orders)
        elif sync_all:
            _url = SYNC_ALL_ORDERS.format(shop=shopname)
        else:
            _url = SYNC_ORDERS.format(shop=shopname, start_time=last_sync_time)

        response = requests.get(_url ,auth=auth ,headers=headers)
        data = json.loads(response.text)
        pickup_data = json.loads(pc.store_data)

        _orders = data.get('orders', [])
        batch = None
        rec_bh_processed, rec_bh_total = 0,0
        order_number_list = [o['id'] for o in _orders]
        existing_orders = Order.objects.filter(customer=customer,
                                customer_reference_number__in=order_number_list
                                ).values_list('reference_number',
                                flat=True).distinct()
        if _orders:
            desc_batch = '%s_%s_ordersync_%s_%s' % ('Shopify',
                            customer.name, len(_orders), len(existing_orders))
            batch = OrderBatch.objects.create(description=desc_batch,
                        number_of_entries=0, customer=customer, source=source_name)

        sync_response_list = {}
        for order in _orders:
            try:
                order_number = str(order.get('order_number'))
                print('Order ID', order_number)
                if order_number in existing_orders:
                    continue
                print('Syncing....')
                temp_reference = 'Shopify%s' % order_number
                api_data = ShopifyLog(request_body=json.dumps(order),
                            customer=customer,
                            reference_number=temp_reference,
                            category='Sync Orders',
                            source='Order')

                api_data.save()
                proceed_flag, _order = self.get_formatted_order_data(order, pickup_data,
                                    pc.is_wms_flow, pc.delivery_code)
                print(proceed_flag, _order)
                if proceed_flag:
                    rec_bh_total += 1
                    ref_number, final_message = self.create_order(_order,
                        customer.api_key, pc.is_wms_flow)

                    if ref_number:
                        if final_message in sync_response_list:
                            sync_response_list[final_message].append(ref_number)
                        else:
                            sync_response_list[final_message] = [ref_number,]
                    else:
                        rec_bh_processed +=1

            except Exception as e:
                logger.error(str(e))

        if rec_bh_processed > 0:
                sync_response_list[''] = '%s shopify orders have been synced successfully' % rec_bh_processed

        summary_json = json.dumps(sync_response_list)
        if batch:
            batch.summary = summary_json
            batch.end_time = timezone.now()
            batch.successfull = rec_bh_processed
            batch.number_of_entries = rec_bh_total
            batch.save()

        if summary_json:
            from blowhorn.shopify.tasks import push_sync_notif_to_dashboard
            push_sync_notif_to_dashboard.apply_async(args=[summary_json, customer.id])

    def create_order(self, input_data, api_key, is_wms_flow):
        """
            Call blowhorn shipment order api and create order
        """

        headers = {
            "api-key" : api_key,
            "Content-type": "application/json"
        }

        host_url = HOST
        shopify_oid = input_data['customer_reference_number']
        ref_number = None
        final_message = None
        client_url = host_url + '/api/orders/shipment'
        response = requests.post(client_url, json=input_data, headers=headers)
        order = json.loads(response.content)

        if isinstance(order, dict):
            _status = order.get('status')
            message = order.get('message')
        else:
            message = order[0]
            _status = 'FAIL'

        if _status != 'PASS':
            ref_number = input_data['reference_number']
            final_message = message

        if response.status_code == status.HTTP_200_OK:
            from blowhorn.shopify.tasks import create_fulfillment_order
            create_fulfillment_order.apply_async(
                args=[shopify_oid])

        return ref_number, final_message

    def dashboard_push_sync_updates(self, summary, customer_id):
        summary_dict = json.loads(summary)
        _url = FIREBASE_SHOPIFY_SYNC % customer_id
        for k,v in summary_dict.items():
            _json = json.dumps(dict({k : v}))
            broadcast_to_firebase(_url, _json)

    def fetch_tracking_updates(self, parms):
        order_list = parms.getlist('order_names[]', None)
        shopname = parms.get('shop', None)
        data = {
            "message": "Successfully received the tracking numbers",
            "success": True

        }
        bh_order_list = [e.split('.')[0] for e in order_list]
        pc = PartnerCustomer.objects.filter(store_domain=shopname, is_active=True).first()
        if pc:
            orders = Order.objects.filter(customer=pc.customer,
                        source='shopify',
                        reference_number__in=bh_order_list).values_list(
                            'number', 'reference_number'
                        )

            order_dict = {}
            for o in orders:
                order_dict[o[1]] = o[0]

            _tracking_numbers = {}
            for o in order_list:
                _name = o.split('.')[0]
                _tracking_numbers[o] = order_dict.get(_name, '')

            data['tracking_numbers'] = _tracking_numbers

        return data
