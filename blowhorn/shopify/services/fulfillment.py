import requests
import json
from requests.auth import HTTPBasicAuth

from django.conf import settings
from rest_framework import status

from blowhorn.oscar.core.loading import get_model
from config.settings import status_pipelines as StatusPipeline
from blowhorn.order.const import PAYMENT_STATUS_PAID

from blowhorn.shopify.constants import (CREATE_FULFILLMENT,
    CREATE_SHIPMENT_EVENT,
    DEREGISTER_AS_FULFILLMENT_SERVICE, FULFILLMENT_CALLBACK_URL, PAYMENT_UPDATE,
    REGISTER_AS_FULFILLMENT_SERVICE, COMPLETE_FULFILLMENT,
    CANCEL_FULFILLMENT)


PartnerCustomer = get_model('customer', 'PartnerCustomer')
Order = get_model('order', 'Order')
ShopifyWebhookSubscription = get_model('shopify', 'ShopifyWebhookSubscription')
ShopifyLog = get_model('shopify', 'ShopifyLog')
Event = get_model('order', 'Event')

HOST = settings.HOST

def _get_request_config(pcm):
    auth = HTTPBasicAuth(settings.SHOPIFY_CLIENT_ID, settings.SHOPIFY_SECRET)
    headers = {'content-type': 'application/json', 'X-Shopify-Access-Token' : pcm.partner_token}
    shopname = pcm.store_domain.split('.')[0]
    return auth, headers, shopname

class ShopifyFulfillmentRegistration:
    '''
        This class takes care of the registering & deregistering Blowhorn as
        fulfillment service for the customer.
    '''

    def __init__(self, pcm):
        self.pcm = pcm


    def register_as_fulfillment_service(self):
        '''
            Register as fulfillment service &
            runs after successful installtion of app.
        '''
        auth, headers, shopname = _get_request_config(self.pcm)
        _url = REGISTER_AS_FULFILLMENT_SERVICE.format(shop=shopname)
        callback_url = HOST + FULFILLMENT_CALLBACK_URL

        request_data = {
            "fulfillment_service": {
                "name": "Blowhorn Fulfillment",
                "callback_url": callback_url,
                "inventory_management": False,
                "tracking_support": True,
                "fulfillment_orders_opt_in": False,
                "requires_shipping_method": True,
                "format": "json"
            }
        }

        # make fulfillment registration request
        response = requests.post(_url, json=request_data, auth=auth, headers=headers)
        data = json.loads(response.text)

        location_id = None
        # get the location id from response and store in partner customer entry
        if data:
            fulfillment_service = data.get('fulfillment_service', None)
            location_id = fulfillment_service.get('location_id', None) or None

        if location_id:
            PartnerCustomer.objects.filter(id=self.pcm.id).update(
                location_id=location_id,
                fulfillment_service_data=json.dumps(fulfillment_service)
            )


    def deregister_as_fulfillment_service(self):
        '''
            Deregister as fulfillment service
        '''
        auth, headers, shopname = _get_request_config(self.pcm)
        fulfillment_service_data = json.loads(self.pcm.fulfillment_service_data)

        if not fulfillment_service_data:
            return

        sid = fulfillment_service_data.get('id')
        _url = DEREGISTER_AS_FULFILLMENT_SERVICE.format(shop=shopname,
                    fulfillment_service_id=sid)
        requests.delete(_url, auth=auth, headers=headers)


class ShopifyFulfillmentService:
    '''
        This class handles the fulfillment and shipment events updates
        to shopify
    '''

    def _get_fulfillment_create_payload(self, pcm, order, fulfillment_status):
        request_data = {
            "fulfillment": {
                    "location_id": pcm.location_id,
                    "tracking_number": order.number,
                    "tracking_company" : "Blowhorn",
                    "tracking_urls": [
                        '%s/track/%s' % (settings.HOST, order.number),
                        '%s/track-parcel/%s' % (settings.HOST, order.number),
                    ],
                    "status" : fulfillment_status,
                    "notify_customer": True

            }
        }
        return request_data

    def _get_valid_url(self, pcm, shopname, order, fulfillment_status):
        '''
            This method gets the valid shopify fulfillment url based on the fulfillment_status
            since shopify has different end points for fulfillment creation, success &
            cancellation.
        '''
        _url, request_data = None, None
        _parms = {
            "shop" : shopname,
            "order_id" : order.customer_reference_number
        }

        # prepapre the fulfillment request based on the order status
        if fulfillment_status == StatusPipeline.SHOPIFY_FULFILLMENT_OPEN:
            _url = CREATE_FULFILLMENT
            request_data = self._get_fulfillment_create_payload(pcm, order,
                                fulfillment_status)

        elif order.shopify_fulfillment_id:
            _parms["fulfillment_id"] = order.shopify_fulfillment_id
            if fulfillment_status == StatusPipeline.SHOPIFY_FULFILLMENT_SUCCESS:
                _url = COMPLETE_FULFILLMENT
            elif fulfillment_status == StatusPipeline.SHOPIFY_FULFILLMENT_CANCELLLED:
                _url = CANCEL_FULFILLMENT

        # return the url and request data
        _url = _url.format(**_parms) if _url else None
        return _url, request_data

    def _make_fulfillment_request(self, pcm, order, fulfillment_status):
        '''
            Make the fulfillment based on valid order status
        '''
        # return if already fulfilled
        is_already_fulfilled = False
        if fulfillment_status == StatusPipeline.SHOPIFY_FULFILLMENT_SUCCESS:
            is_already_fulfilled = ShopifyLog.objects.filter(
                reference_number=order.reference_number,
                customer=order.customer,
                response_status=status.HTTP_201_CREATED,
                category='Status success',
                source = 'Fulfillment'
            ).exists()

        if is_already_fulfilled:
            return

        # get the shopname and url based on the fulfillment status
        auth, headers, shopname = _get_request_config(pcm)
        _url, request_data = self._get_valid_url(pcm, shopname, order, fulfillment_status)
        if not _url:
            return

        # make fulfillment request and store the response
        _url = _url.format(shop=shopname, order_id=order.customer_reference_number)
        if request_data:
            response = requests.post(_url , json=request_data , auth=auth, headers=headers)
        else:
            response = requests.post(_url , auth=auth, headers=headers)

        ShopifyLog.objects.create(
            reference_number=order.reference_number,
            request_body=json.dumps(response.text),
            customer=order.customer,
            reason=fulfillment_status,
            response_status=response.status_code,
            category='Status %s' % (fulfillment_status),
            source = 'Fulfillment'
        )

        data = json.loads(response.text)
        return data

    def _make_event_request(self, pcm, order, order_status, message=None):
        '''
            This method makes the order event updates to shopify along with
            the remarks.
        '''
        auth, headers, shopname = _get_request_config(pcm)
        _url = CREATE_SHIPMENT_EVENT.format(shop=shopname,
                        order_id=order.customer_reference_number,
                        fulfillment_id=order.shopify_fulfillment_id)
        request_data = {
            "event": {
                "status": order_status
            }
        }
        if message:
            request_data["event"]["message"] = message

        # make event update request and store the log
        response = requests.post(_url , json=request_data , auth=auth, headers=headers)
        ShopifyLog.objects.create(
            reference_number=order.reference_number,
            request_body=json.dumps(request_data),
            reason=json.dumps(response.text),
            customer=order.customer,
            response_status=response.status_code,
            category=order_status,
            source='Event'
        )

        data = json.loads(response.text)
        return data

    def create_shipment_event(self, order_id, order=None, is_manifested=False, ignore_payment_status=False):
        """
            This method triggers the shipment status events & fulfillment
            status updates to shopify based on the mapping in status pipeline
            (Also to push updates fulfillment id should be created)
        """
        if not order:
            order = Order.objects.filter(id=order_id).first()

        if not order:
            return

        # proceed only if active partner customer mapping exists and orde has fulfillment id
        pcm = PartnerCustomer.objects.filter(customer=order.customer, is_active=True).first()
        if not pcm or not order.shopify_fulfillment_id:
            return

        # Get the corressponding shopify shipment status
        _status = StatusPipeline.SHOPIFY_STATUS_MAPPING.get(order.status, None)

        if _status:
            valid_message = [StatusPipeline.UNABLE_TO_DELIVER, StatusPipeline.ORDER_DELIVERED,
                            StatusPipeline.ORDER_CANCELLED, StatusPipeline.ORDER_RETURNED]
            message = None

            # Set the message for customer in case of end status if present
            if order.status in valid_message:
                events = Event.objects.filter(order=order, status=order.status)
                if events:
                    event = events.latest('created_date')
                    message = event.remarks if event.remarks else None

            self._make_event_request(pcm, order, _status, message=message)

        if (order.status == StatusPipeline.ORDER_DELIVERED and order.cash_on_delivery > 0):
            #  Trigger the payment captured event to shopify in case order is delivered and paid
            auth, headers, shopname = _get_request_config(pcm)
            if ignore_payment_status or (not ignore_payment_status and
                order.payment_status==PAYMENT_STATUS_PAID):

                _url = PAYMENT_UPDATE.format(shop=shopname,
                        order_id=order.customer_reference_number)
                
                request_data = {
                    "transaction": {
                        "kind" : "capture",
                        "source" : "external"
                    }
                }

                response = requests.post(_url , json=request_data , auth=auth, headers=headers)
                data = json.loads(response.text)
                ShopifyLog.objects.create(
                    reference_number=order.reference_number,
                    request_body=json.dumps(request_data),
                    reason=json.dumps(data),
                    customer=order.customer,
                    response_status=response.status_code,
                    category='Paid',
                    source='Payment'
                )

        # start the fulfillment status update only if valid fulfillment trigger
        _fstatus = StatusPipeline.SHOPIFY_ORDER_MANIFESTED if is_manifested else order.status
        fulfillment_status = StatusPipeline.SHOPIFY_FULFILLMENT_MAPPING.get(_fstatus, None)
        if not fulfillment_status:
            return

        data = self._make_fulfillment_request(pcm, order, fulfillment_status)

    def create_order_fulfillment(self, oid):
        '''
            Order Fulfillment Creation with Pending Status
        '''
        order = Order.objects.filter(customer_reference_number=oid).first()
        pcm = PartnerCustomer.objects.filter(customer=order.customer, is_active=True).first()
        if not order or not pcm:
            return

        # make the fulfillment request
        data = self._make_fulfillment_request(pcm, order, StatusPipeline.SHOPIFY_FULFILLMENT_OPEN)

        _fulfillment = data.get('fulfillment', {})
        fid = _fulfillment.get('id', None)
        if not fid:
            return

        order.shopify_fulfillment_id = fid
        order.save()

        # create in_transit shopify event
        self._make_event_request(pcm, order, StatusPipeline.SHOPIFY_IN_TRANSIT_STATUS)