import requests
import logging
import hmac
import hashlib
import json
import csv
import base64
from requests.auth import HTTPBasicAuth
from io import StringIO

from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.utils import timezone

from rest_framework import status

from blowhorn.oscar.core.loading import get_model
from blowhorn.order.mixins import OrderPlacementMixin, OrderlineMixin
from config.settings import status_pipelines as StatusPipeline
from blowhorn.integration_apps.models import ApiCallSaver
from blowhorn.order.const import PAYMENT_MODE_CASH, PAYMENT_MODE_ONLINE
from blowhorn.utils.mail import Email
from blowhorn.shopify.services.product import ShopifyProductService
from blowhorn.shopify.service import ShopifyService

from blowhorn.shopify.constants import (
    PAYMENT_COD_GATEWAY, SHOPIFY_CUSTOMER_DATA_REQUEST, WEBHOOK_DELETETION_URL,
    WEBHOOK_FULFILLMENTEVENT_CREATED, WEBHOOK_FULFILLMENT_CREATED,
    WEBHOOK_ORDERS_CANCEL, WEBHOOK_ORDERS_CREATE, WEBHOOK_ORDERS_DELETE,
    WEBHOOK_ORDERS_FULFILLED, WEBHOOK_ORDERS_PAID, WEBHOOK_ORDERS_PARTIALLYFULFILLED,
    WEBHOOK_ORDERS_UPDATE, WEBHOOK_PRODUCT_CREATE, WEBHOOK_PRODUCT_DELETE, 
    WEBHOOK_PRODUCT_UPDATE, WEBHOOK_SUBSCRIPTION_URL)


Customer = get_model('customer', 'Customer')
Partner = get_model('customer', 'Partner')
PartnerCustomer = get_model('customer', 'PartnerCustomer')
User = get_model('users', 'User')
Order = get_model('order', 'Order')
ShopifyAuthcode = get_model('shopify', 'ShopifyAuthcode')
ShopifyAccessScope = get_model('shopify', 'ShopifyAccessScope')
OrderBatch = get_model('order', 'OrderBatch')
ShopifyWebhookSubscription = get_model('shopify', 'ShopifyWebhookSubscription')
ShopifyLog = get_model('shopify', 'ShopifyLog')

logger = logging.getLogger(__name__)

class ShopifyWebhookService:
    '''
        This class is handling different webhooks.
    '''
    
    def check_gdpr_requests(self, identifier, customer, header, txnid, order_list):
        '''
            GDPR webhooks
        '''
        acs = ApiCallSaver.objects.filter(api_name='Shopify GDPR', reference_id=customer.id,
                identifier=identifier, transaction_id=txnid, govtcheck_status=True,
                timestamp__date=timezone.now().date()
            ).first()

        if acs:
            return None

        call_saver = {}
        call_saver['api_name'] = 'Shopify GDPR'
        call_saver['reference_id'] = customer.id
        call_saver['identifier'] = identifier
        call_saver['doctype'] = header
        call_saver['api_url'] = header
        call_saver['request_rc'] = 200
        call_saver['govtcheck_status'] = False
        call_saver['api_rc'] = 200
        call_saver['in_govt_status'] = False
        call_saver['transaction_id'] = txnid
        call_saver['response_text'] = json.dumps(order_list)
        acs = ApiCallSaver.objects.create(**call_saver)
        
        return acs
    
    def verify_webhook(self, data, hmac_header):
        '''
            Verify the webhook via hmac validation
        '''
        digest = hmac.new(settings.SHOPIFY_SECRET.encode('utf-8'), 
                            data, hashlib.sha256).digest()
        computed_hmac = base64.b64encode(digest)
        return hmac.compare_digest(computed_hmac, hmac_header.encode('utf-8'))

    def send_customer_data(self, parms, hmac_header):
        '''
            Sends the requested customer data in csv file
            "Name Address Pincode" to customer's shopify email id
        '''
        is_hmac_valid = self.verify_webhook(parms, hmac_header)
        parms = json.loads(parms.decode('utf-8'))
        shopname = parms.get('shop_domain', None)

        # return if hmac validation failed or shopname doesnt exist
        if not is_hmac_valid or not shopname:
            return False

        # inputs
        _orders_requested = parms.get('orders_requested', None)
        _customer = parms.get('customer', None)
        _id = _customer.get('id', None)
        _pc = PartnerCustomer.objects.filter(store_domain=shopname, is_active=True).first()
        filename= '%s_%s.csv' % ('customer_data', timezone.now())
        subject = "Customer %s Data Request" % _id

        # if valid request and email id exists, prepare the csv
        if _orders_requested and _pc and _pc.email:
            has_no_existing_request = self.check_gdpr_requests('Customer Data Request', _pc.customer,
                    'customers/data_request', _id, _orders_requested)

            if not has_no_existing_request:
                return True

            orders_requested = ['%s' % (ids) for ids in _orders_requested]
            orders = Order.objects.select_related('shipping_address','customer__user').filter(
                    customer_reference_number__in=orders_requested, is_deleted=False,
                    customer__user__email=_pc.email)

            if settings.DEBUG:
                recipients = settings.DEFAULT_TESTMAIL
            else:
                recipients = Email()._Email__clean_recipients([_pc.email,])

            # proceed only if requested orders exist
            if orders:
                # prepare the csv
                is_header_printed = False;
                outfile = StringIO()
                csvwriter = csv.writer(outfile)
                for order in orders:
                    if not is_header_printed:
                        csvwriter.writerow(['Name', 'Address', 'Postcode'])
                        is_header_printed = True
                    csvwriter.writerow([order.shipping_address.first_name, 
                                            order.shipping_address.line1, 
                                            order.shipping_address.postcode])
                body = SHOPIFY_CUSTOMER_DATA_REQUEST.format(brand_name=settings.BRAND_NAME)
                mail = EmailMultiAlternatives(
                    subject, body, to=recipients,
                    from_email=settings.DEFAULT_FROM_EMAIL
                )
                # attach the csv and send mail
                mail.attach(filename, outfile.getvalue().encode('utf-8'))
                mail.send()
                has_no_existing_request.govtcheck_status = True
                has_no_existing_request.save()

        return True

    def delete_customer_data(self, parms, hmac_header):
        '''
            This method deletes the customer data i.e.
            soft delete (flag updated)

        '''
        is_hmac_valid = self.verify_webhook(parms, hmac_header)
        parms = json.loads(parms.decode('utf-8'))
        shopname = parms.get('shop_domain', None)

        # proceed if valid hmac and shopname
        if not is_hmac_valid or not shopname:
            return False

        _customer = parms.get('customer', None)
        _id = _customer.get('id', None)
        _orders_requested = parms.get('orders_to_redact', None)
        _pc = PartnerCustomer.objects.filter(store_domain=shopname).first()

        # update the order's is_deleted flag as true
        if _orders_requested and _pc and _pc.email:
            has_no_existing_request = self.check_gdpr_requests('Customer Delete Request', _pc.customer,
                    'customers/redact', _id, _orders_requested)

            if not has_no_existing_request:
                return True

            orders_requested = ['%s' % (ids) for ids in _orders_requested]
            Order.objects.filter(customer_reference_number__in=orders_requested, is_deleted=False,
                customer__user__email=_pc.email).update(is_deleted=True)

            has_no_existing_request.govtcheck_status = True
            has_no_existing_request.save()

        return True

    def delete_store_data(self, parms, hmac_header):
        '''
            This method deletes the store data i.e.
            soft delete (flag updated)

        '''
        is_hmac_valid = self.verify_webhook(parms, hmac_header)
        parms = json.loads(parms.decode('utf-8'))
        shopname = parms.get('shop_domain', None)

        # proceed if valid hmac and shopname
        if not is_hmac_valid or not shopname:
            return False

        # mark the partner customer entry as deleted by updating the is_deleted flag
        _pc = PartnerCustomer.objects.filter(store_domain=shopname)
        if _pc:
            _pc.update(is_deleted=True, is_active=False)

        return True

    def subscribe_webhook(self, partner_customer , event):
        '''
            Subscribe webhook for the customer
        '''
        try:
            webhook_id = None
            _status = False
            auth=HTTPBasicAuth(settings.SHOPIFY_CLIENT_ID, settings.SHOPIFY_SECRET)
            headers = {'content-type': 'application/json', 
                        'X-Shopify-Access-Token' : partner_customer.partner_token
                    }

            shopname = partner_customer.store_domain.split('.')[0]
            _url = WEBHOOK_SUBSCRIPTION_URL.format(shop=shopname)
            data = {
                "webhook": {
                    "topic": event.topic,
                    "address": event.callback,
                    "format": event.payload_format
                }
            }

            response = requests.post(_url ,json.dumps(data), auth=auth ,headers=headers)
            _data = None
            if response.status_code == status.HTTP_201_CREATED:
                _response = json.loads(response.text)
                _data = _response.get('webhook', None)

            if _data:
                webhook_id = _data.get('id', None)
                _status = True
            else:
                logger.error(response.text)

        except Exception as e:
            logger.error(str(e))

        return _status, webhook_id

    def delete_webhook(self, partner_customer, webhook_id):
        '''
            Delete webhook subscription for the customer
        '''
        try:
            _status = False
            auth=HTTPBasicAuth(settings.SHOPIFY_CLIENT_ID, settings.SHOPIFY_SECRET)
            headers = {'content-type': 'application/json', 
                        'X-Shopify-Access-Token' : partner_customer.partner_token
                    }

            shopname = partner_customer.store_domain.split('.')[0]
            _url = WEBHOOK_DELETETION_URL.format(shop=shopname, 
                        webhook_id=webhook_id)
            response = requests.delete(_url ,auth=auth ,headers=headers)
            if response.status_code == status.HTTP_200_OK:
                sws = ShopifyWebhookSubscription.objects.filter(webhook_id=webhook_id)
                sws.update(is_active=False, is_deleted=True)
                _status = True
            else:
                logger.error(response.text)

        except Exception as e:
            logger.error(str(e))

        return _status
    
    def process_webhook(self, parms, hmac, domain, topic):
        is_hmac_valid = self.verify_webhook(parms, hmac)
        parms = json.loads(parms.decode('utf-8'))
        ref_no = str(parms.get('id', None))
        fulfillment_ref_no = str(parms.get('order_id', None))
        tracking_company = parms.get('tracking_company', None)
        fullfillment_event_list = [WEBHOOK_FULFILLMENTEVENT_CREATED,
                                    WEBHOOK_ORDERS_FULFILLED, WEBHOOK_ORDERS_PARTIALLYFULFILLED]
        
        if not is_hmac_valid:
            return False

        _topic = topic.split('/')
        sws = ShopifyWebhookSubscription.objects.filter(pcm__store_domain=domain,
                    event__category=_topic[0],event__topic_name=_topic[1],
                    is_deleted=False, is_active=True).latest('created_date')
        if not sws:
            return False

        ShopifyLog.objects.create(
            reference_number=sws.pcm.customer.name,
            request_body=json.dumps(parms),
            customer=sws.pcm.customer,
            category="%s - %s" %(topic,domain),
            source='Webhook'
        )
        sync_response_list = {}

        # process create webhook
        if sws.event.topic == WEBHOOK_ORDERS_CREATE:
            ShopifyService().process_create_event_order(sws.pcm.customer, parms,
                    sws.pcm.is_wms_flow, sws.pcm.delivery_code)

        # process payment update
        if sws.event.topic == WEBHOOK_ORDERS_PAID:
            order = Order.objects.filter(customer_reference_number=ref_no, source='shopify', status=StatusPipeline.ORDER_DELIVERED).first()

            if order:
                # cod & amount processing
                outstanding_amount = float(parms.get('total_outstanding',0))
                gateway = parms.get('gateway')
                update_needed = True
                if outstanding_amount != order.cash_on_delivery:
                    order.payment_mode = PAYMENT_MODE_ONLINE
                    order.cash_on_delivery = 0
                    order.total_payable = float(parms.get('current_total_price',0))
                    order.updated_time = timezone.now()
                    if outstanding_amount > 0:
                        order.payment_mode = PAYMENT_MODE_CASH
                        order.cash_on_delivery = outstanding_amount
                    elif gateway == PAYMENT_COD_GATEWAY:
                        update_needed = False
                    else:
                        order.payment_source = 'Paid Via Shopify'

                    if update_needed:
                        order.save()

        # process cancel webhook
        elif sws.event.topic == WEBHOOK_ORDERS_CANCEL:
            orders = Order.objects.filter(customer_reference_number=ref_no, source='shopify').exclude(status=StatusPipeline.ORDER_DELIVERED)

            if orders:
                OrderPlacementMixin().create_event(
                    orders[0],
                    StatusPipeline.ORDER_CANCELLED,
                    None, None, None, None,
                    "Cancel Via Shopify"
                )
                updated_count = orders.update(status=StatusPipeline.ORDER_CANCELLED,
                                    updated_time=timezone.now(),
                                    reason_for_cancellation=parms.get('cancel_reason', None))
                sync_response_list['Shopify orders cancelled successfully'] = '%s' % updated_count

        # process update webhook
        elif sws.event.topic == WEBHOOK_ORDERS_UPDATE:
            _cancel = parms.get('cancelled_at', None)
            if not _cancel:
                order = Order.objects.filter(customer_reference_number=ref_no,
                    status=StatusPipeline.ORDER_NEW, source='shopify').first()

                if not order:
                    return True

                # cod & amount processing
                outstanding_amount = float(parms.get('total_outstanding',0))
                if outstanding_amount != order.cash_on_delivery:
                    order.payment_mode = PAYMENT_MODE_ONLINE
                    order.cash_on_delivery = 0
                    order.total_payable = float(parms.get('current_total_price',0))
                    order.updated_time = timezone.now()
                    if outstanding_amount > 0:
                        order.payment_mode = PAYMENT_MODE_CASH
                        order.cash_on_delivery = outstanding_amount
                    order.save()

                orderlines = ShopifyService().get_order_lines(parms, True)
                OrderlineMixin().update_orderlines(orderlines, order)

                OrderPlacementMixin().create_event(
                    order,
                    order.status,
                    None, None, None, None,
                    "Orderline Update Via Shopify"
                )

        # process delete webhook
        elif sws.event.topic == WEBHOOK_ORDERS_DELETE:
            orders = Order.objects.filter(customer_reference_number=ref_no,
                status=StatusPipeline.ORDER_NEW, source='shopify')

            if orders:
                OrderPlacementMixin().create_event(
                    orders[0],
                    StatusPipeline.DUMMY_STATUS,
                    None, None, None, None,
                    "Delete Via Shopify"
                )
                updated_count = orders.update(status=StatusPipeline.DUMMY_STATUS,
                                    updated_time=timezone.now())
                sync_response_list['Shopify orders deleted successfully'] = '%s' % updated_count

        # process fulfillment webhook
        elif sws.event.topic in fullfillment_event_list:
            _ref_no = ref_no
            if sws.event.topic == WEBHOOK_FULFILLMENT_CREATED:
                _ref_no = fulfillment_ref_no

            orders = Order.objects.filter(customer_reference_number=_ref_no,
                        source='shopify').exclude(status=StatusPipeline.ORDER_DELIVERED)

            message = 'Fulfilled via Shopify %s' % tracking_company
            if orders:
                OrderPlacementMixin().create_event(
                    orders[0],
                    StatusPipeline.ORDER_CANCELLED,
                    None, None, None, None,
                    message
                )
                updated_count = orders.update(status=StatusPipeline.ORDER_CANCELLED,
                                    updated_time=timezone.now(),
                                    reason_for_cancellation=message)
                sync_response_list['Shopify orders fulfillment updated successfully'] = '%s' % updated_count
        
        # process product webhook
        elif sws.event.topic in [WEBHOOK_PRODUCT_CREATE, WEBHOOK_PRODUCT_UPDATE]:
            ShopifyProductService(pcm=sws.pcm).process_create_update_product_webhook(parms)

        elif sws.event.topic == WEBHOOK_PRODUCT_DELETE:
            ShopifyProductService(pcm=sws.pcm).process_delete_product_webhook(parms)

        if sync_response_list:
            summary_json = json.dumps(sync_response_list)
            from blowhorn.shopify.tasks import push_sync_notif_to_dashboard
            push_sync_notif_to_dashboard.apply_async(args=[summary_json, sws.pcm.customer.id])

        return True