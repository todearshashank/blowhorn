from blowhorn.order.models import Event, Order
from datetime import timedelta
from rest_framework import status
from django.utils import timezone
from blowhorn.shopify.services.fulfillment import ShopifyFulfillmentService
from blowhorn.shopify.models import PartnerCustomer, ShopifyLog
from config.settings import status_pipelines as StatusPipeline
from blowhorn.shopify.service import ShopifyService

class ShopifyReconciliation:
    '''
        This class is responsible for doing the shopify reconciliation daily night
        only if ENABLE_DAILY_SYNC flag is true in partner customer mapping.
            - Sync Orders
            - Create fulfillment if not created
            - Mark as fulfilled if order is delivered & not updated in shopfy
            
    '''

    def __init__(self):
        self.last_run_time = self._get_last_batch_time()

    def fulfill_orders(self, partner_cust_dict):
        '''
            This method makes fulfillment request based on delivered order events
        '''
        # get the shopify delivered order events from last run
        order_events = Event.objects.filter(status=StatusPipeline.ORDER_DELIVERED, 
                            time__gte=self.last_run_time, order__source='shopify'
                            ).order_by('order').distinct('order')

        for oe in order_events:
            _pc = partner_cust_dict.get(oe.order.customer_id, None)
            if not _pc:
                continue
            ShopifyFulfillmentService()._make_fulfillment_request(_pc, oe.order, StatusPipeline.SHOPIFY_FULFILLMENT_SUCCESS)

    def _get_last_batch_time(self):
        # get the last run time
        _last_run_time = timezone.now() - timedelta(hours=24)
        _lastest_run = ShopifyLog.objects.filter(category='Batch', response_status=status.HTTP_200_OK)
        if _lastest_run:
            _lastest_run = _lastest_run.latest('created_at')
            _last_run_time = _lastest_run.created_at

        return _last_run_time

    def start_process(self):
        partner_cust_dict = {}
        # check for active partner customer & sync the orders from last run if any missed
        pcs = PartnerCustomer.objects.filter(partner__legalname='Shopify', is_active=True)
        for pc in pcs:
            partner_cust_dict[pc.customer.id] = pc
            if pc.enable_daily_sync:
                ShopifyService().sync_orders(pc.customer, last_sync_time=self.last_run_time.isoformat())

        # create fulfillment for orders if not created
        _orders_pending_fulfillment = Order.objects.filter(source='shopify', 
                date_placed__gte=self.last_run_time, 
                shopify_fulfillment_id__isnull=True).exclude(status__in=[StatusPipeline.DUMMY_STATUS, StatusPipeline.CANCELLED])

        for order in _orders_pending_fulfillment:
            ShopifyFulfillmentService().create_order_fulfillment(order.customer_reference_number)

        # Complete fulfillment for delivered elements
        self.fulfill_orders(partner_cust_dict)


