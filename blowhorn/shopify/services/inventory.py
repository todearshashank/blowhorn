import requests
import json
import logging
from requests.auth import HTTPBasicAuth

from django.conf import settings
from blowhorn.oscar.core.loading import get_model

from blowhorn.oscar.core.loading import get_model
from blowhorn.shopify.constants import ADJUST_INVENTORY
from blowhorn.wms.submodels.inventory import Inventory

ShopifyLog = get_model('shopify', 'ShopifyLog')
PartnerCustomer = get_model('customer', 'PartnerCustomer')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ShopifyInventoryService:
    '''
        This class handles the inventory updates to shopify.
    '''

    def _get_request_config(self, pcm):
        auth = HTTPBasicAuth(settings.SHOPIFY_CLIENT_ID, settings.SHOPIFY_SECRET)
        headers = {
            'content-type': 'application/json',
            'X-Shopify-Access-Token': pcm.partner_token
        }
        shopname = pcm.store_domain.split('.')[0]
        return auth, headers, shopname

    def _get_inventory(self, params):
        return Inventory.objects.filter(**params).select_related('client').first()

    def adjust_inventory(self, inventory_id, customer_id, qty):
        '''
            Inventory Addition Shopify Update
        '''
        # check for active partner customer mapping
        pcm = PartnerCustomer.objects.filter(
            customer_id=customer_id, is_active=True, is_wms_flow=True
        ).first()

        if not pcm:
            logger.info('Active Partner Customer Mapping not found')
            return

        # prepare the payload and update the shopify inventory
        # if X quantity received then +X is pushed which means X quantity added.
        auth, headers, shopname = self._get_request_config(pcm)
        _url = ADJUST_INVENTORY.format(shop=shopname)

        request_data = {
            "location_id": pcm.location_id,
            "inventory_item_id": inventory_id,
            "available_adjustment": qty
        }
        log_data = {
            'reference_number': inventory_id,
            'request_body': json.dumps(request_data),
            'customer': pcm.customer,
            'category': 'Addition',
            'source' : 'Inventory'
        }

        response = requests.post(_url, json=request_data, auth=auth, headers=headers)
        data = json.loads(response.text)
        log_data['reason'] = json.dumps(data)
        log_data['response_status'] = response.status_code
        ShopifyLog.objects.create(**log_data)
