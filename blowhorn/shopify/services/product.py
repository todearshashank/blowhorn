from decimal import Decimal as D
import requests
import json
from requests.auth import HTTPBasicAuth

from django.conf import settings
from blowhorn.oscar.core.loading import get_model

from blowhorn.oscar.core.loading import get_model
from blowhorn.shopify.constants import PRODUCT_SYNC, PRODUCT_ID_SYNC

PartnerCustomer = get_model('customer', 'PartnerCustomer')
WhSku = get_model('wms', 'WhSku')
ProductGroup = get_model('wms', 'ProductGroup')
PackConfig = get_model('wms', 'PackConfig')
TrackingLevel = get_model('wms', 'TrackingLevel')
Supplier = get_model('wms', 'Supplier')
Client = get_model('wms', 'Client')
ShopifyLog = get_model('shopify', 'ShopifyLog')
Order = get_model('order', 'Order')
OrderLine = get_model('order', 'OrderLine')

class ShopifyProductService:
    '''
        This class is all about sync of products for wms flow.
    '''

    def __init__(self, pcm):
        self.pack_config = None
        self.supplier = None
        self.client = None
        self.pcm = pcm

    def _get_request_config(self, pcm):
        auth = HTTPBasicAuth(settings.SHOPIFY_CLIENT_ID, settings.SHOPIFY_SECRET)
        headers = {
            'content-type': 'application/json',
            'X-Shopify-Access-Token': pcm.partner_token
        }
        shopname = pcm.store_domain.split('.')[0]
        return auth, headers, shopname

    def _setup(self):
        '''
            This method prepares the initial setup for syncing the products:
            
            1. Fetch pack config with name Each for customer & create if not present
            2. Fetch Default Client & Supplier for customer and create if not present
        '''
        # pack config processing
        if not self.pack_config:
            self.pack_config = PackConfig.objects.filter(customer=self.pcm.customer, name='Each').first()

        if not self.pack_config:
            # tracking level
            _trackinglevel = TrackingLevel.objects.filter(
                            customer=self.pcm.customer, 
                            name="Each").first()

            if not _trackinglevel:
                _trackinglevel = TrackingLevel.objects.create(customer=self.pcm.customer,
                                    name="Each", level_one_name='Each')

            # create pack config if not present for customer
            self.pack_config = PackConfig.objects.create(customer=self.pcm.customer, 
                                name='Each', tracking_level=_trackinglevel)

        # supplier processing
        if not self.supplier:
            self.supplier = Supplier.objects.filter(
                                customer=self.pcm.customer, 
                                name='Default Supplier').first()


        if not self.supplier:
            self.supplier = Supplier.objects.create(
                                customer=self.pcm.customer,
                                name='Default Supplier')

        # client processing
        if not self.client:
            self.client = Client.objects.filter(
                                customer=self.pcm.customer, 
                                name='Default Client').first()


        if not self.client:
            self.client = Client.objects.create(
                                customer=self.pcm.customer,
                                name='Default Client')

    def _create_product(self, product):
        '''
            Create products
        '''
        _main_title = product['title']
        variants = product.get('variants',[])
        
        # check for product type and create product group if not present
        product_type = product.get('product_type', _main_title) or _main_title
        product_group = ProductGroup.objects.filter(customer=self.pcm.customer,
                            name=product_type).first()
        if not product_group:
            product_group = ProductGroup(customer=self.pcm.customer,name=product_type)
            product_group.save()
        
        main_product_qs = WhSku.objects.filter(name=_main_title)
        main_product = main_product_qs.first()
        is_extra_product_required = False
        _price = D(product.get('price',0))
        i = 0

        # a whsku is created for each variant
        for _variant in variants:
            _product = {}
            _shopify_variant_id = _variant['id']
            variant_qs = WhSku.objects.filter(shopify_variant_id=_shopify_variant_id)
            
            is_update_required = True
            is_title_update_required = False
            if variant_qs:
                pass

            elif main_product:
                if i != 0:
                    is_update_required = False
                    is_extra_product_required = True
                    is_title_update_required = True

            else:
                is_update_required = False
                is_title_update_required = True

            # description
            _description = _main_title

            for j in range(1,4):
                _key = 'option%s' % j
                _option_value = _variant.get(_key, None)
                if _option_value and (_option_value !="Default Title"):
                    _description += '-' + _option_value

            _product['shopify_description'] = _description
            _product['shopify_inventory_id'] = _variant['inventory_item_id']
            _product['requires_shipping'] = _variant.get('requires_shipping', False)
            _variant_price = D(_variant.get('price', 0))
            _product['mrp'] = _variant_price if _variant_price > 0 else _price

            # title of product
            _title = _variant.get('title', None)
            if is_title_update_required:
                if (not _title or _title == "Default Title") and not is_extra_product_required:
                    _title = _main_title
                else:
                    _title = '%s %s' % (_main_title, _title)

                _product['name'] = _title
            
            if not variant_qs:
                _product['shopify_product_id'] = product.get('id')
                _product['shopify_variant_id'] = _shopify_variant_id

            # barcode & unique product code
            # _ean = _variant.get('barcode', None)
            # if _ean:
            #     _product['ean'] = _ean

            # weight
            _product['uom'] = _variant.get('weight_unit', 'KG')
            _product['each_weight'] = _variant.get('weight', 0)
            i += 1
        
            if is_update_required:
                main_product_qs.update(**_product)
            else:
                # create a new product
                _product['client'] = self.client
                _product['supplier'] = self.supplier
                _product['product_group'] = product_group
                _product['pack_config'] = self.pack_config
                WhSku.objects.create(**_product)
            
        return i

    def _process_product_response(self, data):
        products = data.get('products', [])
        if not products:
            return

        exception_message = {}
        products = data.get('products', [])
        total_product_count = len(products)
        total_product_variant_created = 0
        total_synced_count = 0
        total_failed_count = 0
        
        # loop thru the products and process
        for product in products:
            try:
                total_product_variant_created += self._create_product(product)
                total_synced_count += 1

            except Exception as e:
                print(e)
                exception_message[product['id']] = str(e)
                total_failed_count += 1

        # prepare the summary
        summary = {
            'Total Products' : total_product_count,
            'Total Product Synced' : total_synced_count,
            'Total Product Failed' : total_failed_count,
            'Total Variant Product Created' : total_product_variant_created,
            'Product Creation Exceptions' : exception_message
        }

        return summary

    def sync_specific_order_product(self, order_id, inventory_dict):
        # check the partner customer mapping
        if not self.pcm.is_active or not self.pcm.store_domain:
            print('Inactive partner customer mapping or store domain missing')
            return

        # return if order doesnt exists
        order = Order.objects.filter(order_id=order_id).first()
        if not order:
            return

        id_list = []
        inv_list = []
        # get the shopify_product_id & shopify_varinat_id
        auth, headers, shopname = self._get_request_config(self.pcm)
        for k,v in inventory_dict.items():
            id_list.append(v['shopify_product_id'])
            inv_list.append(v['shopify_variant_id'])

        # fetch the products and process
        id_list_string = ','.join(id_list)
        _url = PRODUCT_ID_SYNC.format(shop=shopname, id_list_string=id_list_string)
        response = requests.get(_url , auth=auth, headers=headers)
        data = json.loads(response.text)
        summary = self._process_product_response(data)

        # store the summary in logs
        ShopifyLog.objects.create(
            reference_number=order.customer_reference_number,
            request_body=json.dumps(inventory_dict),
            reason=json.dumps(summary),
            customer=order.customer,
            response_status=response.status_code,
            category='Specific Sync',
            source='Product'
        )

        # update the orderline sku for the order
        skus = WhSku.objects.filter(shopify_varinat_id__in=inv_list
                    ).values_list('shopify_variant_id', 'id')
        if len(inventory_dict) != len(skus):
            return

        sku_dict = {}
        for sku in skus:
            _shopify_variant_id = sku[0]
            _id =  sku[1]
            sku_dict[_shopify_variant_id] = _id

        for k,v in inventory_dict.items():
            _variant = v['shopify_variant_id']
            _id = sku_dict.get(_variant, None)
            OrderLine.objects.filter(sku_id=k, wh_sku__isnull=True
                ).update(sku=None, wh_sku_id=_id)

    def sync_products(self):
        '''
            This method sync the products for the customer
        '''
        # check the partner customer mapping
        if not self.pcm.is_active or not self.pcm.store_domain:
            print('Inactive partner customer mapping or store domain missing')
            return

        # initialize the setup
        self._setup()
        if not (self.client and self.supplier and self.pack_config):
            print('Setup initialization failed')
            return

        # prepare the sync request for shopify
        auth, headers, shopname = self._get_request_config(self.pcm)
        _url = PRODUCT_SYNC.format(shop=shopname)
        response = requests.get(_url ,auth=auth ,headers=headers)
        data = json.loads(response.text)
        summary = self._process_product_response(data)

        # create log
        ShopifyLog.objects.create(
            reference_number=self.pcm.customer.name,
            request_body=json.dumps(data),
            reason=json.dumps(summary),
            customer=self.pcm.customer,
            response_status=response.status_code,
            category='Sync',
            source='Product'
        )

    def process_create_update_product_webhook(self, data):
        '''
            This method is called from product create/update webhook endpoint
        '''
        # check the partner customer mapping
        if not self.pcm.is_active or not self.pcm.store_domain:
            print('Inactive partner customer mapping or store domain missing')
            return
        
        # initialize the setup
        self._setup()
        if not (self.client and self.supplier and self.pack_config):
            print('Setup initialization failed')
            return
        
        exception_message = {}
        total_product_count = 1
        total_product_variant_created = 0
        total_synced_count = 0
        total_failed_count = 0
        
        try:
            total_product_variant_created += self._create_product(data)
            total_synced_count += 1

        except Exception as e:
            exception_message[data['id']] = str(e)
            total_failed_count += 1

        # prepare the summary
        summary = {
            'Total Products' : total_product_count,
            'Total Product Synced' : total_synced_count,
            'Total Product Failed' : total_failed_count,
            'Total Variant Product Created' : total_product_variant_created,
            'Product Creation Exceptions' : json.dumps(exception_message)
        }

        # create the log
        ShopifyLog.objects.create(
            reference_number=self.pcm.customer.name,
            request_body=json.dumps(data),
            reason=json.dumps(summary),
            customer=self.pcm.customer,
            category='Create/Update',
            source='Product Webhook'
        )
    
    def process_delete_product_webhook(self, data):
        '''
            This method is called from product delete webhook endpoint
        '''
        # check the partner customer mapping
        if not self.pcm.is_active or not self.pcm.store_domain:
            print('Inactive partner customer mapping or store domain missing')
            return

        # make all the variants for the corressponding prodcut id as None
        shopify_product_id = data['id']
        WhSku.objects.filter(
                shopify_product_id=shopify_product_id).update(
                    shopify_product_id=None,
                    shopify_variant_id=None,
                    shopify_description=None,
                    shopify_inventory_id=None
                )
        
        # create log
        ShopifyLog.objects.create(
            reference_number=self.pcm.customer.name,
            request_body=json.dumps(data),
            customer=self.pcm.customer,
            category='Delete',
            source='Product Webhook'
        )
