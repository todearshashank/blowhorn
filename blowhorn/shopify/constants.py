EC001_HMAC_ERROR = 'HMAC validation failed'
EC002_PARM_MISSING = 'Required parameters missing'
EC003_INVALID_SHOPNAME = 'The shopname is invalid'
EC004_TOKEN_NOT_FOUND = 'Token missing'
EC005_SHOPNAME_MISSING = 'Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request'
EC006_INVALID_REQUEST_ORIGIN = 'Request origin cannot be verified'

SHOPIFY_CUSTOMER_ID = 39370
VERSION = '2021-10'

GET_SHOPIFY_STOREDATA_URL = 'https://{shop}.myshopify.com/admin/api/%s/shop.json' % VERSION
GET_STORE_AUTH_TOKEN = 'https://{shop}.myshopify.com/admin/oauth/access_token'
APP_PREINSTALL_URL = 'https://{shop}.myshopify.com/admin/oauth/authorize?client_id={api_key}&scope={scopes}&redirect_uri={redirect_uri}&state={nonce}'

# order sync url
SYNC_ORDERS = 'https://{shop}.myshopify.com/admin/api/%s/orders.json?fulfillment_status=unfulfilled&created_at_min={start_time}' % VERSION
SYNC_ALL_ORDERS = 'https://{shop}.myshopify.com/admin/api/%s/orders.json?fulfillment_status=unfulfilled' % VERSION
SYNC_SPECIFIC_ORDERS = 'https://{shop}.myshopify.com/admin/api/%s/orders.json?fulfillment_status=unfulfilled&ids={orderlist}' % VERSION

# product sync url
PRODUCT_SYNC = 'https://{shop}.myshopify.com/admin/api/%s/products.json?limit=250' % VERSION
PRODUCT_ID_SYNC = 'https://{shop}.myshopify.com/admin/api/%s/products.json?ids={id_list_string}&limit=250' % VERSION

# payment url
PAYMENT_UPDATE = 'https://{shop}.myshopify.com/admin/api/%s/orders/{order_id}/transactions.json' % VERSION

# fulfillment url
REGISTER_AS_FULFILLMENT_SERVICE = 'https://{shop}.myshopify.com/admin/api/%s/fulfillment_services.json' % VERSION
DEREGISTER_AS_FULFILLMENT_SERVICE = 'https://{shop}.myshopify.com/admin/api/%s/fulfillment_services/{fulfillment_service_id}.json' % VERSION
CREATE_FULFILLMENT = 'https://{shop}.myshopify.com/admin/api/%s/orders/{order_id}/fulfillments.json' % VERSION
UPDATE_FULFILLMENT = 'https://{shop}.myshopify.com/admin/api/%s/orders/{order_id}/fulfillments/{fulfillment_id}.json' % VERSION
COMPLETE_FULFILLMENT = 'https://{shop}.myshopify.com/admin/api/%s/orders/{order_id}/fulfillments/{fulfillment_id}/complete.json' % VERSION
CANCEL_FULFILLMENT = 'https://{shop}.myshopify.com/admin/api/%s/orders/{order_id}/fulfillments/{fulfillment_id}/cancel.json' % VERSION
CREATE_SHIPMENT_EVENT = 'https://{shop}.myshopify.com/admin/api/%s/orders/{order_id}/fulfillments/{fulfillment_id}/events.json' % VERSION
ADJUST_INVENTORY = 'https://{shop}.myshopify.com/admin/api/%s/inventory_levels/adjust.json' % VERSION

# webhook url
WEBHOOK_SUBSCRIPTION_URL = 'https://{shop}.myshopify.com/admin/api/%s/webhooks.json' % VERSION
WEBHOOK_DELETETION_URL = 'https://{shop}.myshopify.com/admin/api/%s/webhooks/{webhook_id}.json' % VERSION

# Webhooks
WEBHOOK_ORDERS_CANCEL = 'orders/cancelled'
WEBHOOK_ORDERS_CREATE = 'orders/create'
WEBHOOK_ORDERS_UPDATE = 'orders/updated'
WEBHOOK_ORDERS_DELETE = 'orders/delete'
WEBHOOK_ORDERS_PAID = 'orders/paid'
WEBHOOK_FULFILLMENT_CREATED = 'fulfillments/create'
WEBHOOK_FULFILLMENTEVENT_CREATED = 'fulfillment_events/create'
WEBHOOK_ORDERS_FULFILLED = 'orders/fulfilled'
WEBHOOK_ORDERS_PARTIALLYFULFILLED = 'orders/partially_fulfilled'
WEBHOOK_PRODUCT_CREATE = 'products/create'
WEBHOOK_PRODUCT_UPDATE = 'products/update'
WEBHOOK_PRODUCT_DELETE = 'products/delete'

APP_INSTALLATION_URL='/api/shopify/install'
FULFILLMENT_CALLBACK_URL='/api/shopify/fulfillment/update'
POST_INSTALLATION_REDIRECT_URL='?next=shopify'
PUSH_ORDER_REDIRECT = 'https://{shop}/admin/orders'
FIREBASE_SHOPIFY_SYNC = 'dashboard/%s/ordersync'
PAYMENT_COD_GATEWAY = 'Cash on Delivery (COD)'

SHOPIFY_CUSTOMER_DATA_REQUEST = """
    Dear Sir/ Madam,
    Hope you are doing well.

    Please find attached customer data as per the request.
    
    Thanks & Regards,
    Team {brand_name}"""
