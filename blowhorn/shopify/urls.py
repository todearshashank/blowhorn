from django.conf.urls import url

from blowhorn.shopify import views

urlpatterns = [
    # app installation endpoints
    url(r'^shopify/verification$', views.ShopifyVerifyRequest.as_view(), name='customer-shopify-verification'),
    url(r'^shopify/install$', views.ShopifyInstallApp.as_view(), name='customer-shopify-installation'),
    # gdpr webook endpoints
    url(r'^shopify/customer/view$', views.ShopifyViewCustomer.as_view(), name='customer-shopify-view-data'),
    url(r'^shopify/customer/delete$', views.ShopifyDeleteCustomer.as_view(), name='customer-shopify-delete-data'),
    url(r'^shopify/store/delete$', views.ShopifyDeleteStore.as_view(), name='store-shopify-delete-data'),
    url(r'^shopify/store/order$', views.ShopifyOrderPushRequest.as_view(), name='store-shopify-bulk-push'),
    # webhook url endpoint
    url(r'^shopify/order/update$', views.ShopifyWebhookUpdate.as_view(), name='order-update-callback'),
    # fulfillnment updates
    url(r'^shopify/fulfillment/update/fetch_stock$', views.ShopifyFulfillmentUpdate.as_view(), name='fulfillment-stockfetch-callback'),
    url(r'^shopify/fulfillment/update/fulfillment_order_notification$', views.ShopifyFulfillmentUpdate.as_view(), name='fulfillment-notification-callback'),
    url(r'^shopify/fulfillment/update/fetch_tracking_numbers$', views.ShopifyFetchTracking.as_view(), name='shopify-fetch-tracking'),
]

