import logging
import json
from django.conf import settings
from blowhorn import celery_app
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.common.decorators import redis_batch_lock
from blowhorn.order.models import Order
from blowhorn.shopify.models import ShopifyLog

from config.settings import status_pipelines as StatusPipeline
from blowhorn.shopify.services.fulfillment import ShopifyFulfillmentService, \
        ShopifyFulfillmentRegistration
from blowhorn.shopify.services.product import ShopifyProductService
from blowhorn.shopify.services.inventory import ShopifyInventoryService
from blowhorn.shopify.services.product import ShopifyProductService
from blowhorn.oscar.core.loading import  get_model
from blowhorn.shopify.services.reconciliation import ShopifyReconciliation

Customer = get_model('customer', 'Customer')
PartnerCustomer = get_model('customer', 'PartnerCustomer')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def sync_customer_orders(self, id, orderlist):
    '''
        This task syncs the customer's shopify orders
        Triggered from two endoints i.e. dashboard sync request and selective bulk push via shopify
    '''
    customer = Customer.objects.filter(id=id).first()
    if customer:
        from blowhorn.shopify.service import ShopifyService
        ShopifyService().sync_orders(customer, orderlist)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def create_fulfillment_order(self, oid):
    '''
        This task creates the fulfillment for the shopify orders
        and fulfillment id is return from shopify which is updated in order
    '''
    ShopifyFulfillmentService().create_order_fulfillment(oid)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def adjust_inventory_on_shopify(self, inventory_id, customer_id, qty):
    '''
        This task is triggered when inventory receiving happens via web or mobile
        to update the inventory count on shopify.
    '''
    ShopifyInventoryService().adjust_inventory(inventory_id, customer_id, qty)


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def register_fulfillment(self, pcm_id):
    '''
        This task registers Blowhorn as fulfillment service on shopify for the
        customer and is trigerred after successfull app installation.
    '''
    pcm = PartnerCustomer.objects.filter(id=pcm_id).first()
    logger.info('Registering partner %s for fulfillment service.' % pcm)
    ShopifyFulfillmentRegistration(pcm).register_as_fulfillment_service()
    ShopifyProductService(pcm).sync_products()


@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def push_sync_notif_to_dashboard(self, summary, customer_id):
    '''
        This task takes care of pushing any shopify updates to the customer dashboard
    '''
    if summary:
        from blowhorn.shopify.service import ShopifyService
        ShopifyService().dashboard_push_sync_updates(summary, customer_id)

@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def sync_specific_inventory(self, order_id, inventory):
    '''
        This task sync a specific inventory if needed while order creation
    '''
    inventory_dict = json.loads(inventory)
    ShopifyProductService().sync_specific_order_product(order_id, inventory_dict)

@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def create_shopify_event(self, oid):
    '''
        This task is our order event equivalent and triggered to push
        the order event updates.
    '''
    from blowhorn.shopify.services.fulfillment import ShopifyFulfillmentService
    ShopifyFulfillmentService().create_shipment_event(oid)

@celery_app.task(base=TransactionAwareTask, bind=True, max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def shopify_bulk_handle(self, oids, operation):
    '''
        This task is for admin usage functionality to handle fulfillment
        and log reference_number field is used to get details
    '''
    _omp = {
        'create_fulfillment' : StatusPipeline.SHOPIFY_FULFILLMENT_OPEN,
        'complete_fulfillment' : StatusPipeline.SHOPIFY_FULFILLMENT_SUCCESS,
        'cancel_fulfillment' : StatusPipeline.SHOPIFY_FULFILLMENT_CANCELLLED
    }

    _status = _omp.get('operation', None)
    if not _status:
        return

    logs  = ShopifyLog.objects.filter(id__in=oids.split(','))
    for log in logs:
        _order = Order.objects.filter(reference_number=log.reference_number, customer=log.customer).first()
        if not _order:
            continue
        pcm = PartnerCustomer.objects.filter(customer=_order.customer, is_active=True).first()
        _status = _omp.get('operation', None)
        if _status:
            ShopifyFulfillmentService()._make_fulfillment_request(pcm, _order, _status)

@celery_app.task
@redis_batch_lock(period=3600, expire_on_completion=True)
def shopify_reconciliation():
    '''
        This task runs daily at night to sync orders if pending
          and push delivery updates & fulfillment updtes.
        Trigerred only if intergation is active and 
          enable_daily_sync flag is enabled in parter customer entry.
    '''
    if not settings.ENABLE_SLACK_NOTIFICATIONS:
        return
    ShopifyReconciliation().start_process()