import logging
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError

from blowhorn.shopify.service import ShopifyService
from blowhorn.shopify.services.webhook import ShopifyWebhookService

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class ShopifyVerifyRequest(APIView):
    """
    Shopify Install Verification
    """
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            data = request.query_params
            redirect_url = ShopifyService().pre_install_verification(data)
            return HttpResponseRedirect(redirect_url)

        except ValidationError as e:
            return Response(status=e.args[0], data=e.args[1])

        except Exception as e:
            logger.error(e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Please try again after sometime')

class ShopifyInstallApp(APIView):
    """
    Shopify App Installation 
    """
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            data = request.query_params
            redirect_url = ShopifyService().app_installation(data)
            return HttpResponseRedirect(redirect_url)

        except ValidationError as e:
            return Response(status=e.args[0], data=e.args[1])

        except Exception as e:
            logger.error(e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Please try again after sometime')

class ShopifyViewCustomer(APIView):
    """
    customers/data_request - Requests to view stored customer data.
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            code = status.HTTP_401_UNAUTHORIZED 
            shopify_hmac = request.headers.get('X-Shopify-Hmac-SHA256')
            is_success = ShopifyWebhookService().send_customer_data(request.body, shopify_hmac)

            if is_success:
                code = status.HTTP_200_OK

        except Exception as e:
            logger.error(e)

        return Response(status=code)

class ShopifyDeleteCustomer(APIView):
    """
    customers/data_request - Requests to delete stored customer data.
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            code = status.HTTP_401_UNAUTHORIZED
            shopify_hmac = request.headers.get('X-Shopify-Hmac-SHA256')
            is_success = ShopifyWebhookService().delete_customer_data(request.body, shopify_hmac)

            if is_success:
                code = status.HTTP_200_OK

        except Exception as e:
            logger.error(e)

        return Response(status=code)

class ShopifyDeleteStore(APIView):
    """
    shop/redact - Requests deletion of shop data.
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            code = status.HTTP_401_UNAUTHORIZED
            shopify_hmac = request.headers.get('X-Shopify-Hmac-SHA256')
            is_success = ShopifyWebhookService().delete_store_data(request.body, shopify_hmac)

            if is_success:
                code = status.HTTP_200_OK

        except Exception as e:
            logger.error(e)

        return Response(status=code)

class ShopifyOrderPushRequest(APIView):
    """
    Shopify Process Bulk Orders from App
    """
    permission_classes = (AllowAny,)

    def get(self, request):
        try:
            data = request.query_params
            redirect_url = ShopifyService().push_bulk_order(data)
            return HttpResponseRedirect(redirect_url)

        except ValidationError as e:
            return Response(status=e.args[0], data=e.args[1])

        except Exception as e:
            logger.error(e)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='Please try again after sometime')

class ShopifyWebhookUpdate(APIView):
    """
    Shopify Process the updates coming from webhook
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            code = status.HTTP_401_UNAUTHORIZED
            _hmac = request.headers.get('X-Shopify-Hmac-SHA256')
            _domain = request.headers.get('X-Shopify-Shop-Domain')
            _topic = request.headers.get('X-Shopify-Topic')
            if (_hmac and _domain and _topic):
                is_success = ShopifyWebhookService().process_webhook(request.body, _hmac, _domain, _topic)
                if is_success:
                    code = status.HTTP_200_OK

        except Exception as e:
            logger.error(e)

        return Response(status=code)
    
class ShopifyFulfillmentUpdate(APIView):
    """
    Shopify Process the fulfillment updates
    """
    permission_classes = (AllowAny,)

    def get(self, request):
        return Response(status=status.HTTP_200_OK)
    
    def post(self, request):
        return Response(status=status.HTTP_200_OK)

class ShopifyFetchTracking(APIView):
    """
    Shopify fetch tracking updates
    """
    permission_classes = (AllowAny,)

    def get(self, request):
        _data = ShopifyService().fetch_tracking_updates(request.query_params)    
        return Response(status=status.HTTP_200_OK, data=_data)