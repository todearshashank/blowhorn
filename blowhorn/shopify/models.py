from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import JSONField

from blowhorn.common.models import BaseModel
from blowhorn.oscar.core.loading import get_model

PartnerCustomer = get_model('customer', 'PartnerCustomer')

class ShopifyAuthcode(BaseModel):

    shopname = models.CharField(_('Shop Name'), max_length=50)
    code = models.CharField(_('Authorization Code'), max_length=10)

    class Meta:
        verbose_name = _("Shopify Authorization Code")
        verbose_name_plural = _("Shopify Authorization Codes")

    def __str__(self):
        return self.code

class ShopifyAccessScope(BaseModel):

    scope_name = models.TextField(_('Access Scope'))
    is_active = models.BooleanField(_('Is Active'), default=False)

    class Meta:
        verbose_name = _("Shopify Access Scope")
        verbose_name_plural = _("Shopify Access Scope")

    def __str__(self):
        return self.scope_name

class ShopifyWebhook(BaseModel):
    category = models.CharField(_('Category'), max_length=100)
    topic_name = models.CharField(_('Topic'), max_length=100)
    callback = models.TextField(_('Callback Url'))
    payload_format = models.CharField(_('Format'), max_length=10, default='json')

    @property
    def topic(self):
        return '%s/%s' % (self.category,self.topic_name)

    class Meta:
        verbose_name = _("Webhook Topic")
        verbose_name_plural = _("Webhook Topics")

    def __str__(self):
        return '%s/%s' % (self.category, self.topic_name)

class ShopifyWebhookSubscription(BaseModel):
    event = models.ForeignKey('shopify.ShopifyWebhook', 
                null=True, blank=True, on_delete=models.PROTECT)
    pcm = models.ForeignKey(PartnerCustomer, on_delete=models.PROTECT)
    is_active = models.BooleanField(_('Is Active'), default=False)
    is_deleted = models.BooleanField(_('Is Deleted'), default=False)
    webhook_id = models.TextField(_('Webhook ID'), null=True, blank=True)
   
    def save(self, *args, **kwargs):
        if not self.pk:
            from blowhorn.shopify.services.webhook import ShopifyWebhookService
            if self.pcm.partner.legalname != 'Shopify':
                raise ValidationError(
                    _("Event subscription is only allowed for Shopify customers"))
            
            sws = ShopifyWebhookSubscription.objects.filter(event=self.event, 
                        pcm=self.pcm, is_active=True)
            if sws.exists():
                raise ValidationError(
                    _("Event subscription already exists for the customer"))

            is_active, webhook_id = ShopifyWebhookService().subscribe_webhook(self.pcm, self.event)
            if not webhook_id:
                raise ValidationError(
                    _("Event subscription failed"))
            self.is_active = is_active
            self.webhook_id = webhook_id
            self.is_deleted = False
            return super().save(*args, **kwargs)

class ShopifyLog(models.Model):
    request_body = JSONField(_("Request Body"), blank=True, null=True)
    category = models.CharField(_("Category"), max_length=255, blank=True, null=True)
    source = models.CharField(_("Source Model"), max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    customer = models.ForeignKey("customer.Customer", on_delete=models.CASCADE)
    reference_number = models.CharField(max_length=100, null=True, blank=True, db_index=True)
    response_status = models.CharField(max_length=30,null =True, blank=True)
    reason = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _('Shopify Log')
        verbose_name_plural = _('Shopify Logs')

    def __str__(self):
        return self.reference_number