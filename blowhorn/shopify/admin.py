from django.contrib import admin

from blowhorn.common.admin import BaseAdmin
from blowhorn.shopify.tasks import shopify_bulk_handle
from blowhorn.utils.datetimerangefilter import DateRangeFilter
from blowhorn.shopify.models import (ShopifyAuthcode, ShopifyWebhook,
        ShopifyWebhookSubscription, ShopifyAccessScope, ShopifyLog)
from blowhorn.customer.models import PartnerCustomer

@admin.register(ShopifyAuthcode)
class ShopifyAuthcodeAdmin(admin.ModelAdmin):
    list_display = ("shopname", "code")
    search_fields = ["shopname"]
    readonly_fields = list_display

    def has_add_permission(self, request):
        return False
    
    def has_delete_permission(self, request):
        return False

@admin.register(ShopifyAccessScope)
class ShopifyAccessScopeAdmin(admin.ModelAdmin):
    list_display = ('scope_name', 'is_active')
    search_fields = ['scope_name']
    list_filter = ('is_active',)

@admin.register(ShopifyWebhook)
class ShopifyWebhookAdmin(BaseAdmin):
    list_display = ("category", "topic_name", "callback", "payload_format", "modified_date")
    list_filter = ("category", "topic_name")
    # readonly_fields = list_display

    def has_add_permission(self, request, obj=None):
        return request.user.is_superuser
    
    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

@admin.register(ShopifyWebhookSubscription)
class ShopifyWebhookSubscriptionAdmin(BaseAdmin):
    list_display = ("event", "pcm", "is_active", "is_deleted", "webhook_id", "modified_date")
    list_filter = ("event", "pcm", "is_active", "is_deleted")
    readonly_fields = ("is_active", "is_deleted", "webhook_id", "modified_date")

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'pcm':
            kwargs['queryset'] = PartnerCustomer.objects.exclude(is_active=False)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def delete_subscription(modeladmin, request, queryset):
        from blowhorn.shopify.services.webhook import ShopifyWebhookService
        for wid in queryset:
            ShopifyWebhookService().delete_webhook(wid.pcm, wid.webhook_id)
    delete_subscription.short_description = "Delete Event Subscription"

    actions = [delete_subscription]

    def has_add_permission(self, request, obj=None):
        return request.user.is_superuser
    
    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

@admin.register(ShopifyLog)
class ShopifyLogAdmin(admin.ModelAdmin):
    list_display = ['reference_number', 'customer', 'source', 'category', 'response_status', 'created_at']

    readonly_fields = (
        'reference_number', 'response_status', 'source', 'category', 'reason', 'created_at', 'customer', 'request_body')

    search_fields = ['reference_number']

    list_filter = (('customer',admin.RelatedOnlyFieldListFilter),
                   ('created_at', DateRangeFilter),
                   'source', 'category', 'response_status')

    def create_fulfillment(modeladmin, request, queryset):
        id_list = [entry.id for entry in queryset]
        shopify_bulk_handle.apply_async(args=[','.join(id_list),'create_fulfillment'])
    create_fulfillment.short_description = "Create Fulfillment"

    def complete_fulfillment(modeladmin, request, queryset):
        id_list = [entry.id for entry in queryset]
        shopify_bulk_handle.apply_async(args=[','.join(id_list),'complete_fulfillment'])
    complete_fulfillment.short_description = "Complete Fulfillment"

    def cancel_fulfillment(modeladmin, request, queryset):
        id_list = [entry.id for entry in queryset]
        shopify_bulk_handle.apply_async(args=[','.join(id_list),'cancel_fulfillment'])
    cancel_fulfillment.short_description = "Cancel Fulfillment"

    actions = [create_fulfillment, complete_fulfillment, cancel_fulfillment]