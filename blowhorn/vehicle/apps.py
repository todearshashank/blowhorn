from django.apps import AppConfig


class VehicleConfig(AppConfig):
    name = 'blowhorn.vehicle'
    verbose_name = "Vehicle"

    def ready(self):
        from blowhorn.vehicle import signals
