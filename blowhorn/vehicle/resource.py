from import_export import resources, fields
from django.db import connection

from .models import Vehicle
from blowhorn.utils.functions import utc_to_ist

class VehicleResource(resources.ModelResource):
    vehicle_class = fields.Field( column_name="Vehicle_class",)

    def dehydrate_vehicle_model(self, vehicle):
        return vehicle.vehicle_model

    def dehydrate_body_type(self, vehicle):
        return vehicle.body_type

    def dehydrate_vehicle_class(self, vehicle):
        if vehicle.vehicle_model:
            return vehicle.vehicle_model.vehicle_class
        return ''

    def dehydrate_creation_date(self, vehicle):
        if vehicle.creation_date:
            return utc_to_ist(vehicle.creation_date).strftime(
                '%d-%m-%Y %H:%M')
        return ''

    def before_export(self, queryset, *args, **kwargs):
        print('Queries Count at begin : %d' % len(connection.queries))

    def after_export(self, queryset, data, *args, **kwargs):
        print('Queries Count at end : %d' % len(connection.queries))

    class Meta:
        model = Vehicle
        skip_unchanged = True
        report_skipped = False

        fields = ('id', 'registration_certificate_number', 'vehicle_model', 'model_year',
            'creation_date', 'body_type')
        export_order = ('id', 'registration_certificate_number', 'vehicle_model',
            'vehicle_class', 'body_type', 'model_year','creation_date')