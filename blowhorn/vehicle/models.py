# System and Django libraries
import json
import datetime
from django.db.models import Manager
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import pgettext_lazy
from django.core.validators import RegexValidator, MinLengthValidator,\
    MinValueValidator, MaxValueValidator
from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import JSONField
from sortedm2m.fields import SortedManyToManyField
from django.contrib.gis.db.models.fields import LineStringField
# 3rd Party libraries
from phonenumber_field.modelfields import PhoneNumberField
from blowhorn.oscar.apps.address.abstract_models import AbstractAddress
from postgres_copy import CopyManager

# Blowhorn imports
from blowhorn.common.middleware import current_request
from blowhorn.address.models import City
from blowhorn.common.models import BaseModel
from blowhorn.document.models import Document, DocumentPage
from blowhorn.document.constants import REJECT, VERIFICATION_PENDING
from blowhorn.document.file_validator import FileValidator
from blowhorn.document.utils import generate_file_path
from blowhorn.driver import constants as DriverConstant
from blowhorn.vehicle.constants import VENDOR_STATUSES,\
    VENDOR_STATUS_ONBOARDING, VENDOR_WORKING_PERFERENCES


class VendorAddressManager(Manager):

    def get_queryset(self):
        return super().get_queryset().select_related('country')


class VendorAddress(AbstractAddress):
    """
    Vendor Address from  Oscar AbstractAddress
    """
    geopoint = models.PointField(null=True, blank=True)

    objects = VendorAddressManager()

    class Meta:
        verbose_name = _("Fleet Owner's address")
        verbose_name_plural = _("Fleet Owner addresses")


class VehicleClass(models.Model):

    """
    Define vehicle class
    """
    commercial_classification = models.CharField(
        _("Commercial Class"), max_length=50, blank=False,
        help_text=_("Vehicle class like Mini-Truck, Auto-Rickshaw, Truck, Cargo-Van"))

    licence_category_code = models.CharField(
        _("Licence Category"), max_length=50, blank=False,
        help_text=_("Licence Category like LCV, SCV"))

    licence_category_desc = models.CharField(
        _("Licence Category Description"), max_length=100, blank=False,
        help_text=_("Licence Category description like including motorcars, jeeps, taxis, delivery vans"))

    vehicle_class_sequence = models.IntegerField(null=True, blank=True)

    carrying_capacity = models.TextField(
        _("Carrying Capacity"), blank=True,
        null=True, )

    image = models.FileField(
        _("Default vehicle icon"),
        upload_to=generate_file_path,
        validators=[FileValidator()],
        max_length=256, blank=True, null=True)

    image_selected = models.FileField(
        _("Vehicle icon when selected"),
        upload_to=generate_file_path,
        validators=[FileValidator()],
        max_length=256, blank=True, null=True)

    image_detail = models.FileField(
        _("Vehicle icon for detail"),
        upload_to=generate_file_path,
        validators=[FileValidator()],
        max_length=256, blank=True, null=True)

    app_icon = models.CharField(
        _("Vehicle Icon Name for App"),
        blank=True, null=True, max_length=50)

    description = models.TextField(_('Description of vehicle class'),
                                   blank=True, null=True)

    capacity = models.CharField(
        _("Capacity"), blank=True, null=True, max_length=10,
        help_text=_("Tonnage of vehicle. e.g. 1.6 T"))

    length = models.CharField(_('Length'),
                              blank=True, null=True, max_length=20,
                              help_text=_("Length of loading deck e.g, 5ft 4in"))

    breadth = models.CharField(_('Width'),
                               blank=True, null=True, max_length=20,
                               help_text=_("Width of loading deck e.g, 5ft 4in"))

    height = models.CharField(_('Height'),
                              blank=True, null=True, max_length=20,
                              help_text=_("Height of loading deck e.g, 5ft 4in"))

    creation_date = models.DateTimeField(_("Creation Date"), auto_now_add=True)

    per_day_limit = models.DecimalField(_('Driver Payment/day Approval limit for spoc'),
                                            max_digits=10, decimal_places=2, default=1000)

    class Meta:
        verbose_name = _("Vehicle Class")
        verbose_name_plural = _("Vehicle Classes")

    def __str__(self):
        return "%s | %s" % (self.commercial_classification, self.licence_category_code)

    def summary(self):
        return u"%s, %s" % (self.licence_category_code, self.commercial_classification)

    @staticmethod
    def autocomplete_search_fields():
        return 'commercial_classification',


class VehicleModel(models.Model):

    """
    Vehicle Model
    """
    model_name = models.CharField(
        _("Name of the Vehicle Model"), max_length=50, blank=False,
        help_text=_("Vehicle Model like TATA ACE"))

    capacity = models.CharField(
        _("Capacity"), blank=True, max_length=10,
        help_text=_("Capacity for the vehicle model. e.g. 1.6 T"))

    dimensions = models.CharField(
        _("Dimensions"), blank=True, max_length=20,
        help_text=_("Vehicle dimension e.g. length X breadth X height"))

    vehicle_class = models.ForeignKey(VehicleClass, on_delete=models.PROTECT)

    creation_date = models.DateTimeField(_("Creation Date"), auto_now_add=True)

    class Meta:
        verbose_name = _("Vehicle Model")
        verbose_name_plural = _("Vehicle Models")

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s-%s" % (self.model_name, self.capacity)


class CityVehicleClassAlias(models.Model):

    """
    Vehicle Class Alias
    """
    city = models.ForeignKey(City, on_delete=models.PROTECT)

    vehicle_class = models.ForeignKey(VehicleClass,
                                      on_delete=models.PROTECT)

    alias_name = models.CharField(_("Alias Name"),
                                  max_length=50)

    class Meta:
        verbose_name = _("Vehicle Class Alias")
        verbose_name_plural = _("Vehicle Class Alias")
        unique_together = ("city", "vehicle_class")

    def __str__(self):
        return self.alias_name


class CityAlternateVehicleClass(models.Model):
    """
        Vehicle Alternate Vehicle class
    """
    city = models.ForeignKey(City, on_delete=models.PROTECT)

    vehicle_class = models.ForeignKey(VehicleClass,
                                      on_delete=models.PROTECT)

    alternate_vehicle_class = SortedManyToManyField(
        'vehicle.VehicleClass', _("Alternate Vehicle Class that can be used incase of unavailabilty of vehicle"),
        blank=True, related_name='alternate_vehicle_class'
    )

    def __str__(self):
        return str(self.vehicle_class) + ' - ' + str(self.city)


class BodyType(models.Model):

    """
    Vehicle Body Type
    """
    body_type = models.CharField(
        _("vehicle body type"), blank=False, max_length=30,
        help_text=_("Body type like Container, Tarpaulin, Open-Top"))

    def __str__(self):
        return self.body_type

    class Meta:
        verbose_name = _("Body Type")
        verbose_name_plural = _("Body Types")


class Vendor(BaseModel):
    name = models.CharField(
        _('Name of Fleet Owner'), blank=True, max_length=255, db_index=True)

    phone_number = PhoneNumberField(
        _("Mobile Number"), unique=True,
        help_text=_("Fleet Owner's primary mobile number e.g. +91{10 digit mobile "
                    "number}"))

    father_name = models.CharField(_('Father\'s name'), blank=True,
                                   max_length=255)

    date_of_birth = models.DateField(_('Date of Birth'), null=True, blank=True)

    gender = models.CharField(
        _('Gender'), max_length=15, choices=DriverConstant.GENDER_CHOICES,
        default=DriverConstant.GENDER_MALE)

    pan = models.CharField(
        _("Fleet Owner PAN"), max_length=10,
        null=True, blank=True, unique=True,
        validators=[RegexValidator(
            regex=r'^[A-Za-z]{5}\d{4}[A-Za-z]{1}$',
            message=_('First 5 digits, next 4 digits, last Alphabet, fixed '
                      'length is 10')), MinLengthValidator(10)])

    photo = models.ImageField(upload_to=generate_file_path,
                              null=True, blank=True, max_length=500)

    is_address_same = models.BooleanField(
        _("Permanent address is same as current address"),
        default=False, db_index=True)

    source = models.CharField(_("Device Type"), max_length=30,
                              null=True, blank=True, default='Web')

    current_address = models.ForeignKey(
        VendorAddress,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='current_address')

    permanent_address = models.ForeignKey(
        VendorAddress,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='permanent_address')

    bank_account = models.ForeignKey(
        'driver.BankAccount',
        null=True,
        blank=True,
        verbose_name='Bank Details', on_delete=models.PROTECT)

    user = models.OneToOneField('users.User', null=True,
                                on_delete=models.CASCADE)

    status = models.CharField(max_length=100, choices=VENDOR_STATUSES,
                              default=VENDOR_STATUS_ONBOARDING)

    reason_for_inactive = models.CharField(_('Reason'), max_length=75, null=True, blank=True,
                                           help_text=_(
                                               "Applicable only for Inactive/Blacklisted status"))

    remarks = models.CharField(
        _("Remarks"), max_length=255, null=True, blank=True,
        help_text=_("Applicable only for Inactive/Blacklisted status"))

    preferred_locations = models.ManyToManyField('VendorPreferredLocation', blank=True,
                                                 related_name='vendor_preferred_locations')

    tos_accepted = models.BooleanField(
        _("Fleet Owner Accepted TOS ?"), default=False, db_index=True)

    tos_response_date = models.DateTimeField(null=True, blank=True)

    working_preferences = ArrayField(models.CharField(
        _('Working Preference'),
        choices=VENDOR_WORKING_PERFERENCES,
        max_length=100, blank=True, null=True), null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        _request = current_request()
        vendor_list = []
        current_user = _request.user if _request else ''
        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())

            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')

                if field_name in ['status']:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                    if old_value != current_value:

                        vendor_list.append(VendorHistory(field=field_name, old_value=str(old_value),
                                                         new_value=current_value, user=current_user, vendor=old))
            VendorHistory.objects.bulk_create(vendor_list)

        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("Fleet Owner")
        verbose_name_plural = _("Fleet Owners")


class VendorDocument(Document):
    vendor = models.ForeignKey(Vendor, related_name='vendor_document',
                               on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Fleet Owner document')
        verbose_name_plural = _('Fleet Owner documents')

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s" % self.file

    def get_title(self):
        """
        Return vendor's name
        """
        return self.vendor.name

    get_title.short_description = pgettext_lazy(u"Vendor Name", u"Vendor Name")


class VendorDocumentPage(DocumentPage):
    vendor_document = models.ForeignKey(VendorDocument, related_name='vendor_document_pages',
                                        on_delete=models.PROTECT)

    def __str__(self):
        return u"%s" % self.file

    class Meta:
        verbose_name = _('Fleet Owner document Page')
        verbose_name_plural = _('Fleet Owner Document Pages')


class Vehicle(models.Model):

    """
    Vehicle for blowhorn
    """

    vehicle_model = models.ForeignKey(
        VehicleModel,
        null=False,
        verbose_name='Vehicle Model',
        on_delete=models.PROTECT)

    body_type = models.ForeignKey(
        BodyType,
        null=False,
        verbose_name='Body Type',
        on_delete=models.PROTECT)

    registration_certificate_number = models.CharField(
        _("RC Number"), max_length=25, db_index=True, blank=False,
        unique=True, )#validators=[RegexValidator(
            # regex=r'^[A-Z]{2}[0-9]{1,2}',
            # message=_('Registration Number format: AA1*')),
            # MinLengthValidator(7)])

    vendor = models.ForeignKey(
        Vendor, null=True, blank=True, on_delete=models.PROTECT)

    model_year = models.PositiveIntegerField(
        _("Model Year"), null=True,
        validators=[
            MinValueValidator(1800),
            MaxValueValidator(datetime.datetime.now().year)],
        help_text="Use the following format: YYYY")

    last_meter_reading = models.IntegerField(_("Last Meter Reading"), null=True,
                                             blank=True)

    creation_date = models.DateTimeField(_("Creation Date"), auto_now_add=True)

    copy_data = CopyManager()
    objects = models.Manager()

    class Meta:
        verbose_name = _("Vehicle")
        verbose_name_plural = _("Vehicles")

    def __str__(self):
        return self.registration_certificate_number

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)

        self.registration_certificate_number = self.registration_certificate_number.replace(" ", "")

        self.registration_certificate_number = self.registration_certificate_number.upper(
        )

        # Check if this vehicle already exists
        if Vehicle.objects\
                .filter(registration_certificate_number=self.registration_certificate_number)\
                .exclude(pk=self.pk).exists():
            raise ValidationError({
                'registration_certificate_number': _('Vehicle already exists')
            })


class VehicleDocument(Document):
    vehicle = models.ForeignKey(Vehicle, related_name='vehicle_document',
                                on_delete=models.PROTECT)

    invoid_payload = JSONField(_('Extra Details'), blank=True, null=True,
                help_text=_('Extra details for document'))

    class Meta:
        verbose_name = _("Vehicle document")
        verbose_name_plural = _("Vehicle documents")

    def __str__(self):
        return self.summary()

    def summary(self):
        return u"%s" % self.file

    def get_title(self):
        """
        Return registration_certificate_number
        """
        return self.vehicle.registration_certificate_number

    get_title.short_description = pgettext_lazy(
        u"Vehicle Number", u"Vehicle Number")


class VehicleDocumentPage(DocumentPage):
    vehicle_document = models.ForeignKey(VehicleDocument, related_name='vehicle_document_page',
                                        on_delete=models.PROTECT)

    def __str__(self):
        return u"%s" % self.file

    class Meta:
        verbose_name = _('Vehicle Document Page')
        verbose_name_plural = _('Vehicle Document Pages')


class VendorHistory(models.Model):

    """
    Capturing the Vendor history
    """
    vendor = models.ForeignKey('vehicle.Vendor', null=True, blank=True, on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    old_value = models.CharField(max_length=250, null=True, blank=True)
    new_value = models.CharField(max_length=250, null=True, blank=True)
    user = models.CharField(max_length=50)
    reason = models.CharField(max_length=255, null=True, blank=True)
    modified_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Fleet Owner History')
        verbose_name_plural = _('Fleet Owner History')

    def __str__(self):
        return str(self.vendor)


class VendorPreferredLocation(BaseModel):
    vendor = models.ForeignKey(Vendor, on_delete=models.PROTECT)
    city = models.ForeignKey('address.City', on_delete=models.PROTECT)
    locations = models.MultiPointField(
        _('Location'), null=True, blank=True,
        help_text=_("Represented as (longitude, latitude)"))

    class Meta:
        verbose_name = _("Vendor's Preferred Location")
        verbose_name_plural = _("Vendor's Preferred Locations")

    def __str__(self):
        return str(self.city.name)


class VendorInactiveReason(BaseModel):
    reason = models.TextField()
    is_active = models.BooleanField()

    class Meta:
        verbose_name = _("Fleet Owner Inactive Reason")
        verbose_name_plural = _("Fleet Owner Inactive Reasons")


class VendorMigrationHistory(BaseModel):
    vendor = models.CharField(_('Target Fleet Owner'), max_length=255)
    number_of_vendors = models.IntegerField(_('No. of fleet owners merged'),
                                            help_text=_('No. of fleet owners merged'),
                                            default=0)
    log_dump = JSONField(_('Log'), help_text=_('Migration log'))

    class Meta:
        verbose_name = _('Fleet Owner Migration History')
        verbose_name_plural = _('Fleet Owner Migration History')

    @property
    def log_dump_json(self):
        if isinstance(self.log_dump, str):
            self.log_dump = json.loads(self.log_dump)
            if isinstance(self.log_dump, str):
                return json.loads(self.log_dump)
        return self.log_dump
