import re
from django import forms
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.contrib.admin.helpers import ActionForm
from blowhorn.driver import messages

from blowhorn.vehicle.models import VehicleModel, Vendor
from blowhorn.utils.functions import validate_phone_number
from blowhorn.common.fields import CustomModelChoiceField
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.conf import settings
from blowhorn.users.models import User
from django.core.exceptions import ValidationError
from config.settings import status_pipelines as StatusPipeline


class DocumentsInlineFormset(forms.models.BaseInlineFormSet):
    # valiations on vehicle documents

    def clean(self):
        super(DocumentsInlineFormset, self).clean()
        from blowhorn.document.constants import EXPIRED

        for form in self.forms:
            data = form.cleaned_data
            if not data.get("id"):
                continue
            document = form.instance
            document_status = data.get('status') or document.status
            doc_type = document.document_type
            if document_status in [StatusPipeline.ACTIVE, StatusPipeline.INACTIVE, EXPIRED]:
                if form.changed_data:
                    raise ValidationError('You cannot edit Active, Inactive or Expired Document')
            if not re.match(doc_type.regex,  document.number):
                raise ValidationError(doc_type.error_message)


class VendorForm(forms.ModelForm):

    """
    A form for creating new users.
    Includes all the required fields, plus a repeated password.
    """

    name_validator = RegexValidator(
        regex=r'^[a-zA-Z_\s]+$',
        message=_("Only Alphabets are allowed")
    )

    name = forms.CharField(max_length=100,
                           validators=[name_validator])

    phone = forms.CharField(max_length=10, required=True,
                            label=_("Phone Number"),
                            validators=[validate_phone_number],
                            widget=forms.TextInput(attrs={'style': 'width:36ch'}), )

    password = forms.RegexField(regex=r'^\d{4}$', required=True,
                                error_messages={'invalid': messages.MUST_BE_FOUR_DIGIT_PIN},
                                max_length=4)

    reason_for_change = forms.CharField(widget=forms.HiddenInput(), required=False)

    def clean(self):

        super(VendorForm, self).clean()

        phone_number = self.cleaned_data.get('phone', '')
        if phone_number:
            email = phone_number + '@' + settings.DEFAULT_DOMAINS.get('vendor')
            if User.objects.filter(email=email).exists():
                raise ValidationError({'phone': 'Vendor With this phone number already exists'})



    class Meta:
        model = Vendor
        fields = '__all__'


class VendorEditForm(forms.ModelForm):

    name_validator = RegexValidator(
        regex=r'^[a-zA-Z_\s]+$',
        message=_("Only Alphabets are allowed")
    )

    name = forms.CharField(max_length=100,
                           validators=[name_validator])

    phone = forms.CharField(max_length=10, required=False,
                            label=_("Phone Number"),
                            validators=[validate_phone_number],
                            widget=forms.TextInput(attrs={'style': 'width:36ch'}), )

    password = ReadOnlyPasswordHashField(label=_("password"))

    reason_for_change = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(VendorEditForm, self).__init__(*args, **kwargs)
        user = None
        if 'instance' in kwargs:
            vendor = kwargs['instance']
            user = vendor.user if vendor else None

            if vendor and vendor.phone_number:
                number = str(
                    vendor.phone_number.national_number).zfill(10)
            else:
                number = None

            initial = {
                'phone': number,
                'password': user.password if user else None
            }
            kwargs['initial'] = initial

        if user:
            # No reverse url for password change. Need to construct manually
            link = reverse('admin:users_user_change', args=[user.id])
            url_components = link.split('/')[:-2]
            url_components.append('password')
            final_url = '/'.join(url_components)
            help_text = """Raw passwords are not stored, so there is no way to see "
            this user's password, but you can change the password
            using""" + u'<a href="%s" target="_blank"> this form</a>' % (
                final_url)
            self.fields['password'].help_text = help_text

    def clean(self):

        super(VendorEditForm, self).clean()
        phone_number = self.cleaned_data.get('phone', '')
        pan = self.cleaned_data.get('pan', '')
        if phone_number:
            email = phone_number + '@' + settings.DEFAULT_DOMAINS.get('vendor')
            if Vendor.objects.filter(user__email=email).exclude(id=self.instance.id).exists():
                raise ValidationError({'phone': 'Vendor With this phone number already exists'})

        if pan:
            if Vendor.objects.filter(pan=pan).exclude(pk=self.instance.id).exists():
                raise ValidationError({'pan': 'Fleet Owner with the pan number already exists!'})

    class Meta:
        model = Vendor
        fields = '__all__'


class VendorAddressForm(forms.ModelForm):
    line1 = forms.CharField(label='Full Address')
    postcode = forms.CharField(label='Post/Zip-code')


class VehicleClassForm(forms.ModelForm):

    """
    Form for adding vehicle classes
    """

    def __init__(self, *args, **kwargs):
        super(VehicleClassForm, self).__init__(*args, **kwargs)
        if self.fields.get('app_icon', None):
            APP_VEHICLE_ICONS_NAME = (
                ('', '--'),
                ('cargo_van', 'cargo van'), ('light_truck', 'light truck'), ('mini_truck', 'mini truck'),
                ('pickup_truck', 'pickup truck'),
                ('three_wheeler', 'three wheeler'),
                ('tata_ace', 'tata ace')
            )
            self.fields['app_icon'] = forms.ChoiceField(
                choices=APP_VEHICLE_ICONS_NAME,
                required=False)


class VendorActionForm(ActionForm):
    vendor = CustomModelChoiceField(
        label=_("Target Fleet Owner"),
        queryset=Vendor.objects.all(),
        required=False,
        field_names=['pk', 'name']
    )

class VehicleModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VehicleModelForm, self).__init__(*args, **kwargs)

    def clean(self):
        super(VehicleModelForm, self).clean()
        vehicle_class = self.cleaned_data.get('vehicle_class')
        if self.instance.id and vehicle_class and (vehicle_class != self.instance.vehicle_class):
            raise ValidationError({'vehicle_class': 'Vehicle Class cannot be changed'})

    class Meta:
        model = VehicleModel
        fields = '__all__'
