# System and Django libraries
import copy
import logging
from collections import defaultdict
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError
from django import db
from django.db.models import Count

import phonenumbers
from phonenumber_field.phonenumber import PhoneNumber

# Blowhorn Modules
from blowhorn.vehicle.models import VehicleClass, VehicleModel, BodyType, Vehicle, Vendor, \
    CityVehicleClassAlias, VendorPreferredLocation, VendorDocument, VendorMigrationHistory, \
    VendorHistory
from blowhorn.driver.models import Driver, DriverLedger, DriverPayment, BankAccountHistory, \
    DriverTds
from blowhorn.vehicle.constants import C2C_VEHICLE_CLASSES
from blowhorn.vehicle.serializers import VehicleModelSerializer, VendorSerializer
from blowhorn.contract.contract_helpers.contract_helper import ContractHelper \
    as C2CContractHelper
from blowhorn.contract.constants import VEHICLE_SERVICE_ATTRIBUTE_MINUTES
from blowhorn.vehicle.constants import DEFAULT_VEHICLE_CLASS, \
    DEFAULT_VEHICLE_CLASS_DESCRIPTION, VEHICLE_ICONS, VEHICLE_ICON_PREFIX
from blowhorn.utils import functions as utils_func
from blowhorn.order.models import Order
from blowhorn.trip.models import Trip
from blowhorn.users.models import User

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def get_truck_url(vehicle_class, detailed=False):
    for vc in C2C_VEHICLE_CLASSES:
        if vc.get('value') == vehicle_class:
            if detailed:
                return vc.get('image_url_detail')
            else:
                return vc.get('image_url_selected')


def get_vehicle_info(vehicle_class, default=True):
    if settings.WEBSITE_BUILD != settings.DEFAULT_BUILD:
        return {
            'image_url_selected': '/static/%s/assets/truck/Ace.png' % settings.WEBSITE_BUILD,
            'image_url_detail': '/static/%s/assets/truck/Ace.png' % settings.WEBSITE_BUILD
        }
    for vc in C2C_VEHICLE_CLASSES:
        if vc.get('value').upper() == vehicle_class.upper():
            return vc
        elif default:
            return C2C_VEHICLE_CLASSES[0]


def get_vehicle_class_using_preference(vehicle_class, city=None):
    v_class = VehicleClass.objects.filter(
        commercial_classification=vehicle_class).first()

    if not v_class:
        vehicle_class_alias = CityVehicleClassAlias.objects.filter(
            alias_name=vehicle_class, city=city).first()
        if v_class:
            v_class = vehicle_class_alias.vehicle_class
    return v_class


def get_vehicle_models():
    vehicle_model = VehicleModel.objects.all()
    body_types = BodyType.objects.values_list('body_type', flat=True)
    serialized_vehicle_data = VehicleModelSerializer(
        vehicle_model, many=True, context={'body_types': body_types})

    return serialized_vehicle_data.data


def add_vehicle(vehicle_params, is_update):
    if vehicle_params.get('registration_certificate_number'):
        vehicle = Vehicle.objects.filter(
            registration_certificate_number=vehicle_params['registration_certificate_number']).first()

        if vehicle:
            if not is_update:
                return None
            return vehicle

        vehicle_model = VehicleModel.objects.filter(
            model_name=vehicle_params.get('vehicle_model')).first()

        body_type = BodyType.objects.filter(
            body_type=vehicle_params.get('body_type')).first()

        vehicle = Vehicle.objects.create(
            vehicle_model=vehicle_model,
            body_type=body_type,
            registration_certificate_number=vehicle_params.get(
                'registration_certificate_number'),
            model_year=vehicle_params.get('model_year'))

        return vehicle


def add_vendor(vendor_details):
    if vendor_details.get('name', ''):
        if not utils_func.validate_name(vendor_details.get('name', '')):
            logging.error("Please Enter a valid name")
            raise ValidationError("Please Enter a valid Owner name")

    if not utils_func.validate_phone_number(vendor_details.get('phone_number', '')):
        logging.error('Owner phone number is in incorrect format')
        raise ValidationError('Owner phone number is in incorrect format')

    phone_number = PhoneNumber(settings.ACTIVE_COUNTRY_CODE, vendor_details.get('phone_number'))

    vendor = Vendor.objects.filter(phone_number=phone_number,
                                   pan=vendor_details.get('pan', '')).first()

    if not vendor:
        vendor_serializer = VendorSerializer(data=vendor_details)

        if vendor_serializer.is_valid():
            logging.info(
                "Going to the app serializer with data: %s", vendor_details)
            try:
                vendor = vendor_serializer.save()
            except db.IntegrityError as e:
                return ValidationError(e)

        else:
            raise ValidationError(vendor_serializer.errors)

    return vendor


class VehicleUtils(object):

    def get_vehicle_details_from_c2c_contract(self, contract, app=False):
        """
        :param contract: instance of C2CContract
        :param app: boolean
        :return: Object of vehicle details
        """
        contract_term = contract.contractterm_set.all()
        sell_rate = C2CContractHelper().get_sell_rate(
            contract_term, timezone.now().date())

        if not sell_rate:
            return {}

        base_pay = str(0)
        if app:
            base_pay = 0
        vehicle_slabs = None
        if sell_rate:
            slabs = sorted(sell_rate.sellvehicleslab_set.all(),
                           key=lambda x: x.start)
            vehicle_slabs = [x for x in slabs if
                             x.attribute == VEHICLE_SERVICE_ATTRIBUTE_MINUTES]

            base_pay = str(int(sell_rate.base_pay)) if not app else int(
                sell_rate.base_pay)
        base_minute = 0
        prorate_cost = 0
        if vehicle_slabs:
            base_minute = vehicle_slabs[0].start
            prorate_cost = vehicle_slabs[0].rate

        vehicle_class = contract.vehicle_classes.all()[0]
        commercial_classification = vehicle_class.commercial_classification

        vehicle_alias = CityVehicleClassAlias.objects.filter(
            vehicle_class=vehicle_class, city=contract.city).first()
        alias_name = vehicle_alias.alias_name if vehicle_alias else commercial_classification
        try:
            if app:
                # It's a temporary fix with relative url for customer app
                image_url = vehicle_class.image.url.split('/')
                image_url = VEHICLE_ICON_PREFIX + image_url[-1]

                image_selected_url = vehicle_class.image_selected.url.split(
                    '/')
                image_selected_url = VEHICLE_ICON_PREFIX + \
                    image_selected_url[-1]

                image_detail_url = vehicle_class.image_detail.url.split('/')
                image_detail_url = VEHICLE_ICON_PREFIX + image_detail_url[-1]
            else:
                image_url = vehicle_class.image.url
                image_selected_url = vehicle_class.image_selected.url
                image_detail_url = vehicle_class.image_detail.url
        except:
            # Display default vehicle images if images are not available
            vehicle = get_vehicle_info(DEFAULT_VEHICLE_CLASS)
            image_url = vehicle.get('image_url')
            image_selected_url = vehicle.get('image_url_selected')
            image_detail_url = vehicle.get('image_url_detail')

        good_for_text = vehicle_class.description or \
            DEFAULT_VEHICLE_CLASS_DESCRIPTION
        capacity = vehicle_class.capacity

        return {
            'vehicle_class': alias_name,
            'value': alias_name,
            'label': alias_name,
            'image_url': image_url,
            'image_url_selected': image_selected_url,
            'image_url_detail': image_detail_url,
            'rate': {
                'base_price': base_pay,
                'base_minutes': base_minute,
                'prorate_10m': str(int(prorate_cost)) if not app else int(
                    prorate_cost),
            },
            'rate_text': 'Rs ' + str(base_pay) + '/hr',
            'display': {
                'text': sell_rate.display_text,
                'description': sell_rate.display_description
            },
            'good_for_text': good_for_text,
            'dimensions': {
                'length': vehicle_class.length or 'NA',
                'width': vehicle_class.breadth or 'NA',
                'height': vehicle_class.height or 'NA'
            },
            'max_weight': capacity,
        }

    def get_vehicle_classes_from_c2c_contract(self, city=None, app=False):
        """
        :return: Object of vehicle classes associated with cities
                 configured in C2C Contract
        # NOTE: Circular dependency with contract/utils
        """
        from blowhorn.contract.utils import ContractUtils

        contracts = ContractUtils().get_c2c_contracts(city)
        vehicle_dict = defaultdict(list)
        for contract in contracts:
            vehicle = self.get_vehicle_details_from_c2c_contract(
                contract, app=app)
            if bool(vehicle):
                vehicle_dict[contract.city.name].append(copy.deepcopy(vehicle))
        return vehicle_dict

    def get_default_vehicle_class_contract(self, customer=None, city=None,
                                           is_authenticated=True):
        """
        Returns most used vehicle class for a given user or default to vehicle
            class of the city
        """
        pref_vehicle_cls_id, pref_customer_contract_id = 0, 0
        if not is_authenticated:
            if city is not None and city.default_vehicle_class \
                    and city.default_contract:
                pref_vehicle_cls_id = city.default_vehicle_class_id
                pref_customer_contract_id = city.default_contract_id

        else:
            try:
                vehicle_cls = Order.objects.filter(
                    customer=customer, city=city).values(
                    'vehicle_class_preference__id',
                    'customer_contract__id').last()

                if vehicle_cls:
                    pref_vehicle_cls_id = vehicle_cls['vehicle_class_preference__id']
                    pref_customer_contract_id = vehicle_cls['customer_contract__id']
                elif city and city.default_vehicle_class and city.default_contract:
                    pref_vehicle_cls_id = city.default_vehicle_class_id
                    pref_customer_contract_id = city.default_contract_id

            except:
                logger.info('Error in finding default vehicle from '
                            'order for %s' % customer)
                pass

        return pref_vehicle_cls_id, pref_customer_contract_id

    def get_vehicle_icons(self):
        """
        :return: dict of icons ({vehicle_class: icon_url})
        """
        vehicle_icons = {}
        vehicle_classes = VehicleClass.objects.only('commercial_classification',
                                                    'image_detail')
        for vc in vehicle_classes:
            v_icon = vc.image_detail.url if vc.image_detail else None
            vehicle_icons[vc.commercial_classification] = v_icon
        return vehicle_icons


class VendorUtils(object):

    def __delete_vendors(self, vendor_pks, merging_vendors_list):
        deleted_vendors = 0
        log_dump = ''
        try:
            with db.transaction.atomic():
                deleted_vendors = Vendor.objects.filter(id__in=vendor_pks).delete()
                if deleted_vendors:
                    deleted_vendors = deleted_vendors[1].get('vehicle.Vendor')
                    log_dump = '%s fleet owner(s) deleted: %s' % (deleted_vendors, vendor_pks)

                for i in merging_vendors_list:
                    user_email = User.objects.filter(email=i.get('user__email')).first()
                    if not Vendor.objects.filter(user__email=user_email).exists():
                        user_email.delete()

        except BaseException as e:
            log_dump = '%s' % e

        return deleted_vendors, log_dump

    def __do_write(self, vendor, vendor_pks):
        log_dump = []
        with db.transaction.atomic():
            try:
                drivers = Driver.objects.filter(owner_details_id__in=vendor_pks).update(
                    owner_details=vendor)

                if drivers:
                    log_dump.append('No. of drivers: %s' % drivers)

                vehicles = Vehicle.objects.filter(vendor_id__in=vendor_pks).update(
                    vendor=vendor
                )

                if vehicles:
                    log_dump.append('No. of vehicles: %s' % vehicles)

                trips = Trip.objects.filter(vendor_id__in=vendor_pks).update(
                    vendor=vendor
                )

                if trips:
                    log_dump.append('No. of trips: %s' % trips)

                locations = VendorPreferredLocation.objects.filter(vendor_id__in=vendor_pks).update(
                    vendor=vendor
                )

                if locations:
                    log_dump.append('No. of locations: %s' % locations)

                documents = VendorDocument.objects.filter(vendor_id__in=vendor_pks).update(
                    vendor=vendor
                )

                if documents:
                    log_dump.append('No. of documents: %s' % documents)

                driver_ledgers = DriverLedger.objects.filter(owner_details_id__in=vendor_pks).update(
                    owner_details=vendor
                )
                if driver_ledgers:
                    log_dump.append('No. of driver_ledgers: %s' % driver_ledgers)

                driver_payments = DriverPayment.objects.filter(
                    owner_details_id__in=vendor_pks).update(owner_details=vendor)
                if driver_payments:
                    log_dump.append('No. of driver_payments: %s' % driver_payments)

                bank_account_history = BankAccountHistory.objects.filter(
                    vendor_id__in=vendor_pks).update(
                    vendor=vendor
                )

                vendor_history = VendorHistory.objects.filter(vendor_id__in=vendor_pks).update(
                    vendor=vendor
                )

                tds = DriverTds.objects.filter(vendor_id__in=vendor_pks).update(
                    vendor=vendor
                )

            except BaseException as e:
                log_dump.append('%s' % e)
        return log_dump

    def merge_vendor_data(self, vendor_pk, merging_vendors_list):
        vendor = Vendor.objects.get(pk=vendor_pk)
        migration_history = {
            'vendor': ''.join([vendor.name, '(', str(vendor.pk), ')'])
        }

        vendor_pks = [x.get('pk') for x in merging_vendors_list]
        migration_history['log_dump'] = self.__do_write(vendor, vendor_pks)

        deleted_vendors, log = self.__delete_vendors(vendor_pks, merging_vendors_list)
        migration_history['number_of_vendors'] = deleted_vendors
        migration_history['log_dump'].append(log)
        migration_history['log_dump'].append(
            '%s' % [x.get('user__email')for x in merging_vendors_list]
        )
        logger.info('Migration History: %s' % migration_history)
        try:
            VendorMigrationHistory.objects.create(**migration_history)
        except BaseException as e:
            logger.info('%s' % e)
            logger.error("Vendor migration creation failed!!")
        return deleted_vendors


