from django.utils.translation import gettext_lazy as _


DEFAULT_VEHICLE_CLASS = 'Mini-Truck'
DEFAULT_VEHICLE_CLASS_DESCRIPTION = 'Good for parcels, furniture, plants, mattresses etc.'
C2C_VEHICLE_CLASSES = [
    {
        'vehicle_class': 'Mini-Truck',
        'value': 'Mini-Truck',
        'label': 'Mini-Truck',
        'image_url': '/static/assets/truck/Ace-N.png',
        'image_url_selected': '/static/assets/truck/Ace-S.png',
        'image_url_detail': '/static/assets/truck/Ace.png',
        'rate': {
            'base_price': 300,
            'base_minutes': 60,
            'prorate_10m': 50,
        },
        'rate_text': 'Rs 300/hr',
        'good_for_text': 'Good for small parcels, furniture, plants, mattresses etc.',
        'dimensions': {
            'length': '6ft',
            'width': '4ft 6in',
            'height': '5ft 9in',
        },
        'max_weight': '750 kg',
    },
    {
        'vehicle_class': 'Pickup',
        'value': 'Pickup',
        'label': 'Pickup',
        'image_url': '/static/assets/truck/Bolero-N.png',
        'image_url_selected': '/static/assets/truck/Bolero-S.png',
        'image_url_detail': '/static/assets/truck/Bolero.png',
        'rate': {
            'base_price': 450,
            'base_minutes': 60,
            'prorate_10m': 75,
        },
        'rate_text': 'Rs 450/hr',
        'good_for_text': 'Good for medium sized parcels, furniture, sofas etc.',
        'dimensions': {
            'length': '7ft',
            'width': '5ft',
            'height': '6ft',
        },
        'max_weight': '1500 kg',
    },
    {
        'vehicle_class': 'Light-Truck',
        'value': 'Light-Truck',
        'label': 'Light-Truck',
        'image_url': '/static/assets/truck/Tata407-N.png',
        'image_url_selected': '/static/assets/truck/Tata407-S.png',
        'image_url_detail': '/static/assets/truck/Tata407.png',
        'rate': {
            'base_price': 600,
            'base_minutes': 60,
            'prorate_10m': 100,
        },
        'rate_text': 'Rs 600/hr',
        'good_for_text': 'Good for big parcels, furniture, cot, sofas etc.',
        'dimensions': {
            'length': '10ft',
            'width': '5ft 6in',
            'height': '6ft 6in',
        },
        'max_weight': '2400 kg',
    },
]

VEHICLE_ICONS = {
    'minitruck': {
        'idle': '/static/website/images/mini_truck_idle.png',
        'occupied': '/static/website/images/mini_truck_busy.png',
        'b2b': '/static/website/images/mini_truck_b2b.png'
    },
    'pickup': {
        'idle': '/static/website/images/mini_truck_idle.png',
        'occupied': '/static/website/images/mini_truck_busy.png',
        'b2b': '/static/website/images/mini_truck_b2b.png'
    },
    'lighttruck': {
        'idle': '/static/website/images/mini_truck_idle.png',
        'occupied': '/static/website/images/mini_truck_busy.png',
        'b2b': '/static/website/images/mini_truck_b2b.png'
    },
    'autorickshaw': {
        'idle': '/static/website/images/auto_idle.png',
        'occupied': '/static/website/images/auto_busy.png',
        'b2b': '/static/website/images/auto_b2b.png'
    },
    'cargovan': {
        'b2b': '/static/website/images/van_b2b.png',
        'idle': '/static/website/images/van_idle.png',
        'occupied': '/static/website/images/van_busy.png'
    },
    'truck': {
        'b2b': '/static/website/images/truck_b2b.png',
        'idle': '/static/website/images/truck_idle.png',
        'occupied': '/static/website/images/truck_busy.png'
    }
}

VEHICLE_ICON_PREFIX = '/static/assets/truck/'

VENDOR_ALLOWED_FIELDS_FOR_EDIT = ['pan', 'bank_account']

VEHICLE_INSURANCE_CONSTANT = 'Insurance'
VEHICLE_FITNESS_CERTIFICATE_CONSTANT = 'FC'
VENDOR_STATUS_ONBOARDING = 'onboarding'
VENDOR_STATUS_ACTIVE = 'active'
VENDOR_STATUS_INACTIVE = 'inactive'
VENDOR_STATUS_BLACKLISTED = 'blacklisted'
VENDOR_STATUSES = (
    (VENDOR_STATUS_ONBOARDING, _('Onboarding')),
    (VENDOR_STATUS_ACTIVE, _('Active')),
    (VENDOR_STATUS_INACTIVE, _('Inactive')),
    (VENDOR_STATUS_BLACKLISTED, _('Blacklisted')),
)
ADHOC = 'adhoc'
ONLINE_BOOKING = 'online_booking'
SUBSCRIPTION = 'subscription'
DRIVE_AND_DELIVER = 'drive_and_deliver'
EXTRA_LABOUR = 'extra_labour'
VENDOR_WORKING_PERFERENCES = (
    (ADHOC, _('Adhoc')),
    (ONLINE_BOOKING, _('Online Booking')),
    (SUBSCRIPTION, _('Subscription')),
    (DRIVE_AND_DELIVER, _('Driver and Deliver')),
    (EXTRA_LABOUR, _('Extra Labour')),
)

VEHICLE_NUMBER_REGEX = {
    'IN': r'^[A-Z]{2}[0-9]{1,2}[A-Z]{0,3}[0-9]{4}$',
    'ZA': r'^.*$'
}
