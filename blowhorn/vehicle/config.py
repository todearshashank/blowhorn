from django.apps import AppConfig


class VehicleConfig(AppConfig):
    name = 'blowhorn.vehicle'

    def ready(self):
        import blowhorn.vehicle.signals

