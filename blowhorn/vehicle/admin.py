from datetime import timedelta
import os
import csv
import logging
from django.contrib import admin, messages
from django.contrib.messages import get_messages
from django.contrib.gis.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.http import HttpResponse
from blowhorn.utils.functions import utc_to_ist
from django.db.models import Func, F, Value, CharField
from blowhorn.utils.datetime_function import DateTime
from mapwidgets.widgets import GooglePointFieldWidget

from .models import Vehicle, VehicleClass, VehicleModel, \
    BodyType, VehicleDocument, Vendor, VendorAddress, VendorDocument, \
    CityVehicleClassAlias, VendorHistory, CityAlternateVehicleClass, \
    VendorInactiveReason, VendorMigrationHistory, \
    VehicleDocumentPage, VendorDocumentPage

from .utils import VendorUtils
from blowhorn.driver.models import BankAccount
from blowhorn.document.constants import VEHICLE_DOCUMENT, DRIVER_DOCUMENT
from blowhorn.vehicle.forms import VehicleModelForm, VendorForm, VendorAddressForm, \
    VehicleClassForm, VendorEditForm, DocumentsInlineFormset, VendorActionForm
from blowhorn.address.utils import AddressUtil
from blowhorn.utils.datetimepicker import DateTimePicker
from blowhorn.document.widgets import ImagePreviewWidget
from blowhorn.vehicle.constants import VENDOR_ALLOWED_FIELDS_FOR_EDIT
from blowhorn.trip.utils import TripService
from blowhorn.driver.models import DriverTds
from blowhorn.common.tasks import update_cms
from ..contract.constants import CONTRACT_TYPE_C2C, CONTRACT_STATUS_ACTIVE, CONTRACT_TYPE_SME
from ..contract.models import Contract

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

tz_diff = DateTime.get_timezone_offset()


class DocumentsInline(admin.TabularInline):
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    model = VehicleDocument
    fields = (
    'id', 'file', 'document_type', 'number', 'status', 'bgv_status', 'expiry_date', 'created_by', 'modified_by')
    readonly_fields = fields  # ('created_by', 'modified_by', 'bgv_status', 'status')
    formset = DocumentsInlineFormset
    extra = 0

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(DocumentsInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'document_type':
            field.queryset = field.queryset.filter(identifier=VEHICLE_DOCUMENT)

        return field

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False


class VendorDocumentsInline(admin.TabularInline):
    formfield_overrides = {
        models.DateField: {
            "widget": DateTimePicker(options={"format": "DD-MM-YYYY"})
        },
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    model = VendorDocument
    fields = (
        'id', 'get_file', 'document_type', 'number', 'state', 'status', 'bgv_status', 'expiry_date',
        'created_by', 'modified_by', 'get_action_owner')
    readonly_fields = fields
    extra = 0

    def get_file(self, obj):
        if obj.file:
            url = obj.file.url
            return format_html('<a href="%s" target="_blank"><img src="%s" height="100" width="100"/></a>' % (url, url))
        return None

    get_file.short_description = 'File'
    
    def get_action_owner(self, obj):
        action_owner = [str(x.action_owner) for x in obj.documentevent_set.all() if x.action_owner is not None]
        return action_owner

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(VendorDocumentsInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'document_type':
            field.queryset = field.queryset.filter(identifier=DRIVER_DOCUMENT)

        return field

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


class VendorHistoryInline(admin.TabularInline):
    model = VendorHistory
    readonly_fields = ['field', 'old_value',
                       'new_value', 'user', 'modified_time', 'reason']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class VendorTDSInline(admin.TabularInline):
    model = DriverTds
    # max_num = 1
    extra = 0

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


def merge_vendor(modeladmin, request, queryset):
    logger.info('Merging vendors.. %s' % request.POST)
    vendor_id = request.POST.get('vendor')
    if not vendor_id:
        messages.error(request, 'Select a target Fleet Owner')
        return False

    merging_vendors_list = queryset.values('pk', 'user__pk', 'user__email')
    logger.info('Vendors list: %s' % merging_vendors_list)

    conflicting_pk = [x.get('user__email') for x in merging_vendors_list if
                      int(x.get('pk')) == int(vendor_id)]
    if not conflicting_pk:
        successful_migration = VendorUtils().merge_vendor_data(vendor_id, merging_vendors_list)

        if successful_migration:
            message = 'No. of fleet owners migrated: %s' % successful_migration
            messages.success(request, message)
            messages.success(request, '')

        if successful_migration:
            messages.info(request, 'Check Fleet Owner Migration History for detail')
    else:
        message = 'Conflicting: %s. The target fleet owner should not be in checked list' % \
                  conflicting_pk[0]
        messages.error(request, message)


merge_vendor.short_description = _("Merge Fleet Owners")


class VendorAdmin(admin.ModelAdmin):
    search_fields = ['name', 'phone_number', 'pan']
    list_display = ('id', 'name', 'phone_number', 'pan', 'status', 'get_locations', 'tos_accepted',
                    'tos_response_date')
    list_filter = ('status',)

    action_form = VendorActionForm

    actions = [merge_vendor]

    fieldsets = (
        ('General', {
            'fields': (
                'photo', 'name', 'phone_number', 'password', 'status', 'reason_for_inactive',
                'remarks', 'pan', 'preferred_locations', 'bank_account',
                'source', 'tos_accepted', 'tos_response_date')
        }),
        ('Audit Log', {
            'fields': ('created_by', 'created_date', 'modified_by', 'modified_date')
        })
    )

    inlines = [VendorDocumentsInline, VendorHistoryInline, VendorTDSInline]

    def has_add_permission(self, request):
        return False

    def get_locations(self, obj):
        return ", ".join([p.city.name for p in obj.preferred_locations.all()])
    get_locations.short_description = _("Preferred Locations")

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'photo':
            # Remove the request obj, otherwise form_field
            request = kwargs.pop("request", None)
            kwargs['widget'] = ImagePreviewWidget
            return db_field.formfield(**kwargs)
        return super(VendorAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    def get_actions(self, request):
        actions = super(VendorAdmin, self).get_actions(request)
        if not (request.user.is_city_manager or request.user.is_superuser):
            del actions['merge_vendor']
        return actions

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['name', 'status', 'phone_number', 'pan', 'preferred_locations', 'source',
                           'created_by', 'created_date', 'modified_by', 'modified_date',
                           'reason_for_inactive', 'remarks', 'bank_account']
        return readonly_fields

    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if obj:
            self.form = VendorEditForm
        else:
            self.form = VendorForm
        return super().get_form(request, obj=None, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('user')
        return qs

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            data = form.cleaned_data

            # phone_number = data.get('phone')
            reason_for_change = data.get('reason_for_change', None)
            vendor_list = []
            current_user = request.user
            # if phone_number:
            #     obj.phone_number = phonenumbers.parse(
            #         phone_number,
            #         settings.DEFAULT_COUNTRY.get('country_code')
            #     )

            if change:
                old = Vendor.objects.get(pk=obj.id)
                for field in old._meta.fields:
                    field_name = field.__dict__.get('name')

                    if field_name in VENDOR_ALLOWED_FIELDS_FOR_EDIT:
                        old_value = field.value_from_object(old)
                        current_value = field.value_from_object(obj)

                        if field_name == 'bank_account' and reason_for_change:
                            old_value = str(getattr(old, field_name))
                            new_value = str(getattr(obj, field_name))

                            vendor_list.append(VendorHistory(field=field_name, old_value=old_value,
                                                             new_value=new_value, user=current_user, vendor=old,
                                                             reason=reason_for_change))
                            if new_value and new_value != 'None':
                                bank_account = BankAccount.objects.get(pk=obj.bank_account_id)
                                action = 'Mapped-FO'
                                bank_account.add_bank_account_history(bank_account, request.user, action, driver=None,
                                                                      vendor=old)
                            if old_value:
                                bank_account = BankAccount.objects.get(pk=old.bank_account_id)
                                action = 'Unmapped-FO'
                                bank_account.add_bank_account_history(bank_account, request.user, action, driver=None,
                                                                      vendor=old)

                        elif old_value != current_value and (old_value or current_value):
                            vendor_list.append(VendorHistory(field=field_name, old_value=str(getattr(old, field_name)),
                                                             new_value=str(getattr(obj, field_name)),
                                                             user=current_user, vendor=old))
                if vendor_list:
                    VendorHistory.objects.bulk_create(vendor_list)
            #
            # obj.user = DriverManager().upsert_user(
            #     email=data.get('email'),
            #     name=data.get('name'),
            #     phone_number=str(data.get('phone') or old.phone_number),
            #     password=data.get('password', None),
            #     user_id=obj.user_id if obj.user_id else None,
            #     is_active=True,
            #     domain=settings.DEFAULT_DOMAINS.get('vendor')
            # )

            # obj.save()
            super().save_model(request, obj, form, change)

    class Media:
        css = {
            'all': ('/static/website/css/bootstrap.css',)
        }
        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/vendor_bank_details_change_reason.js',
              )


class VendorInactiveReasonAdmin(admin.ModelAdmin):
    list_display = ('reason', 'is_active')


class VehicleAdmin(admin.ModelAdmin):
    list_display = ('registration_certificate_number', 'vehicle_model',
                    'vehicle_class', 'body_type', 'model_year', 'creation_date',)

    search_fields = ['registration_certificate_number']
    list_filter = [
        'vehicle_model', 'vehicle_model__vehicle_class', 'body_type', 'model_year', ]
    list_select_related = (
        'vehicle_model', 'vehicle_class', 'body_type',
    )
    actions = ['export_vehicle_data']
    inlines = [DocumentsInline]

    def vehicle_class(self, obj):
        return obj.vehicle_model.vehicle_class

    vehicle_class.admin_order_field='vehicle_model__vehicle_class'

    def body_type(self, obj):
        return obj.body_type

    def get_queryset(self, request):
        qs=super().get_queryset(request)
        qs=qs.select_related('vehicle_model__vehicle_class')
        qs=qs.select_related('body_type')
        qs=qs.select_related('vendor')
        return qs

    def get_readonly_fields(self, request, obj=None):
        if obj:
            if TripService().trip_for_vehicle_exists(obj):
                return ['vehicle_model', 'registration_certificate_number', 'last_meter_reading']
            else:
                return ['registration_certificate_number', 'last_meter_reading']
        else:
            return []

    def export_vehicle_data(self, request, queryset):
        ids = queryset.values_list('id', flat=True)
        queryset = Vehicle.copy_data.filter(id__in=ids)

        file_path = 'driver_details'

        queryset.annotate(
            creation_datetime=Func(
                F('creation_date') + timedelta(hours=tz_diff),
                Value("DD/MM/YYYY HH24: MI"),
                function='to_char',
                output_field=CharField()
            )
        ).to_csv(
            file_path,
            'registration_certificate_number', 'vehicle_model__model_name',
            'vehicle_model__vehicle_class__commercial_classification',
            'vehicle_model__vehicle_class__licence_category_code',
            'body_type__body_type', 'model_year', 'creation_datetime'
        )

        modified_file = '%s_' % str(utc_to_ist(
            timezone.now()).date()) + os.path.splitext(file_path)[0] + ".csv"

        with open(file_path, 'r') as inFile, open(modified_file, 'w') as outfile:
            r = csv.reader(inFile)
            w = csv.writer(outfile)

            w.writerow(['RC NUMBER', 'VEHICLE MODEL', 'VEHICLE CLASS', 'Licence Category Code', 'BODY TYPE', 'MODEL YEAR',
                        'CREATION DATE'
                        ])
            i = 0
            for row in r:
                if i != 0:
                    w.writerow(row)
                i = i + 1

            os.remove(file_path)

        if os.path.exists(modified_file):
            with open(modified_file, 'rb') as fh:
                response = HttpResponse(
                    fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + \
                                                  os.path.basename(modified_file)
                os.remove(modified_file)
                return response

    export_vehicle_data.short_description = 'Export Vehicle details in excel'


class VehicleClassAdmin(admin.ModelAdmin):
    formfield_overrides={
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }

    form = VehicleClassForm

    list_display = ('commercial_classification',
                    'licence_category_code', 'licence_category_desc', 'vehicle_class_sequence',)

    fieldsets = (
        ('General', {
            'fields': (
                'commercial_classification', 'licence_category_code', 'carrying_capacity',
                'licence_category_desc', 'vehicle_class_sequence', 'description', 'per_day_limit',
            )
        }),
        ('Icons', {
            'fields': (
                'image', 'image_selected',
                'image_detail', 'app_icon'
            )
        }),
        ('Physical Attributes', {
            'fields': (
                ('length', 'breadth', 'height'),
                'capacity'
            )
        }),
    )

    def save_model(self, request, obj, form, change):
        is_c2c_sme_contract = Contract.objects.filter(vehicle_classes__id=obj.id,
                                                      contract_type__in=[CONTRACT_TYPE_C2C, CONTRACT_TYPE_SME],
                                                      status=CONTRACT_STATUS_ACTIVE).exists()
        if is_c2c_sme_contract:
            update_cms.apply_async((False, obj.id, False), eta=timezone.now() + timedelta(seconds=5))
        super().save_model(request, obj, form, change)


class VehicleModelAdmin(admin.ModelAdmin):
    form = VehicleModelForm
    list_display = ('model_name', 'vehicle_class', 'capacity', 'dimensions',)

class CityVehicleClassAliasAdmin(admin.ModelAdmin):
    list_display=('city', 'vehicle_class', 'alias_name',)


class VehicleDocumentAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }
    list_display = ('id', 'get_title', 'file', 'document_type',
                    'date_uploaded', 'status')
    list_filter = ['status']
    search_fields = ['id', 'vehicle__registration_certificate_number',
                     'document_type__name', 'date_uploaded', 'status']

    list_select_related = ('document_type',)

    fieldsets = (
        ('Document', {
            'fields': ('id', 'file', 'document_type', 'number', 'status',
                        'bgv_status', 'expiry_date', 'vehicle', 'invoid_payload')
        }),
        ('Audit Log', {
            'classes': ('collapse',),
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    # readonly_fields = ['created_date', 'created_by', 'modified_date',
    #                    'modified_by', 'bgv_status', 'status']

    def get_readonly_fields(self, request, obj=None):
        # if obj and obj.status in [StatusPipeline.ACTIVE, StatusPipeline.INACTIVE]:
        return [f.name for f in self.model._meta.fields]
        # else:
        #     return ['created_date', 'created_by', 'modified_date',
        #                        'modified_by', 'bgv_status', 'status']

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):

        field = super(VehicleDocumentAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

        if db_field.name == 'document_type':
            field.queryset=field.queryset.filter(identifier=VEHICLE_DOCUMENT)

        return field

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


class VehicleDocumentPageAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }

    list_display = ('vehicle_document', 'page_number', 'file', 'uploaded_at')

    fieldsets = (
        ('Document', {
            'fields': ('id', 'file', 'vehicle_document', 'page_number', 'uploaded_at')
        }),
    )

    readonly_fields = ['id', 'file', 'vehicle_document', 'page_number', 'uploaded_at']

    def has_delete_permission(self, request, obj=None):
        return False


class VendorDocumentPageAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }

    list_display = ('vendor_document', 'page_number', 'file', 'uploaded_at')

    fieldsets = (
        ('Document', {
            'fields': ('id', 'file', 'vendor_document', 'page_number', 'uploaded_at')
        }),
    )

    readonly_fields = ['id', 'file', 'vendor_document', 'page_number', 'uploaded_at']

    def has_delete_permission(self, request, obj=None):
        return False


class VendorAddressAdmin(admin.ModelAdmin):
    PointFieldWidgetSettings = AddressUtil().updateMapWidgetOptions(
        {'dependantInput': 'line1'},
        'GooglePointFieldWidget'
    )

    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget(settings=PointFieldWidgetSettings)},
    }

    form = VendorAddressForm

    fieldsets=(
        (None, {
            'fields': ('title', 'first_name', 'last_name',
                       'line1', 'geopoint', 'line4', 'state', 'postcode',
                       'country',)
        }),
    )


class FleetownerPreferredLocationAdmin(admin.ModelAdmin):
    list_display = ()


class VendorMigrationHistoryAdmin(admin.ModelAdmin):
    list_display = ('vendor', 'number_of_vendors', 'created_by')
    search_fields = ('vendor',)
    readonly_fields = ('vendor', 'number_of_vendors', 'get_log_dump')
    exclude = ('log_dump',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    def get_log_dump(self, obj):
        val = obj.log_dump_json
        formatted_text = ''
        for v in val:
            formatted_text += str(v) + '\n'
        return formatted_text

    get_log_dump.short_description = _('Log')


admin.site.register(VehicleClass, VehicleClassAdmin)
admin.site.register(VehicleModel, VehicleModelAdmin)
admin.site.register(BodyType)
admin.site.register(Vehicle, VehicleAdmin)
admin.site.register(CityAlternateVehicleClass)
admin.site.register(VehicleDocument, VehicleDocumentAdmin)
admin.site.register(VehicleDocumentPage, VehicleDocumentPageAdmin)
admin.site.register(VendorDocumentPage, VendorDocumentPageAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(VendorAddress, VendorAddressAdmin)
admin.site.register(CityVehicleClassAlias, CityVehicleClassAliasAdmin)
admin.site.register(VendorInactiveReason, VendorInactiveReasonAdmin)
admin.site.register(VendorMigrationHistory, VendorMigrationHistoryAdmin)
