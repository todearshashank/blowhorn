from blowhorn.vehicle.models import VehicleDocument, VendorDocument
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from blowhorn.driver.util import create_document_event
from blowhorn.common.middleware import current_request


@receiver(post_save, sender=VendorDocument)
def vendor_document_event(sender, instance, created, **kwargs):
    if created:
        create_document_event(
            vendor_document=instance,
            vendor=instance.vendor,
            created_by=current_request().user,
        )


@receiver(post_save, sender=VehicleDocument)
def document_event(sender, instance, created, **kwargs):
    if created:
        create_document_event(
            vehicle_document=instance,
            driver=instance.vehicle.driver_set.first(),
            created_by=current_request().user,
        )
