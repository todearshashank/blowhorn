# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-02-05 05:57
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicle', '0012_vehicle_model_year'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vehicle',
            name='model_year',
            field=models.PositiveIntegerField(help_text='Use the following format: YYYY', null=True, validators=[django.core.validators.MinValueValidator(1800), django.core.validators.MaxValueValidator(2018)], verbose_name='Model Year'),
        ),
    ]
