# Generated by Django 2.1.1 on 2018-11-23 07:08

from django.db import migrations, models
import django.db.models.deletion
import sortedm2m.fields


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0026_auto_20181120_1037'),
        ('vehicle', '0026_vendor_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='CityAlternateVehicleClass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alternate_vehicle_class', sortedm2m.fields.SortedManyToManyField(blank=True, help_text=None, related_name='alternate_vehicle_class', sorted='Alternate Vehicle Class that can be used incase of unavailabilty of vehicle', to='vehicle.VehicleClass')),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='address.City')),
                ('vehicle_class', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='vehicle.VehicleClass')),
            ],
        ),
    ]
