# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-03-07 11:58
from __future__ import unicode_literals

import blowhorn.document.file_validator
import blowhorn.document.utils
import django.contrib.gis.db.models.fields
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import oscar.models.fields
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0014_auto_20180125_1259'),
        ('driver', '0045_auto_20180306_1009'),
        ('document', '0002_documenttype_identifier'),
        ('vehicle', '0013_auto_20180205_0557'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, db_index=True, max_length=255, verbose_name='Name of Vendor')),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(help_text="Customer's primary mobile number e.g. +91{10 digit mobile number}", max_length=128, verbose_name='Mobile Number')),
                ('pan', models.CharField(max_length=10, unique=True, validators=[django.core.validators.RegexValidator(message='First 5 digits, next 4 digits,last Alphabet, fixed length is 10', regex='^[A-Za-z]{5}\\d{4}[A-Za-z]{1}$'), django.core.validators.MinLengthValidator(10)], verbose_name='Vendor PAN')),
                ('photo', models.ImageField(blank=True, max_length=500, null=True, upload_to=blowhorn.document.utils.generate_file_path)),
                ('bank_account', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='driver.BankAccount', verbose_name='Bank Details')),
            ],
            options={
                'verbose_name': 'Vendor',
                'verbose_name_plural': 'Vendors',
            },
        ),
        migrations.CreateModel(
            name='VendorAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, choices=[('Mr', 'Mr'), ('Miss', 'Miss'), ('Mrs', 'Mrs'), ('Ms', 'Ms'), ('Dr', 'Dr')], max_length=64, verbose_name='Title')),
                ('first_name', models.CharField(blank=True, max_length=255, verbose_name='First name')),
                ('last_name', models.CharField(blank=True, max_length=255, verbose_name='Last name')),
                ('line1', models.CharField(max_length=255, verbose_name='First line of address')),
                ('line2', models.CharField(blank=True, max_length=255, verbose_name='Second line of address')),
                ('line3', models.CharField(blank=True, max_length=255, verbose_name='Third line of address')),
                ('line4', models.CharField(blank=True, max_length=255, verbose_name='City')),
                ('state', models.CharField(blank=True, max_length=255, verbose_name='State/County')),
                ('postcode', oscar.models.fields.UppercaseCharField(blank=True, max_length=64, verbose_name='Post/Zip-code')),
                ('search_text', models.TextField(editable=False, verbose_name='Search text - used only for searching addresses')),
                ('geopoint', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=4326)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='address.Country', verbose_name='Country')),
            ],
            options={
                'verbose_name': "Vendor's address",
                'verbose_name_plural': 'Vendor addresses',
            },
        ),
        migrations.CreateModel(
            name='VendorDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(max_length=256, upload_to=blowhorn.document.utils.generate_file_path, validators=[blowhorn.document.file_validator.FileValidator()])),
                ('number', models.CharField(default='----', help_text='Input the number from the file uploaded..', max_length=100, verbose_name='Document Number')),
                ('status', models.CharField(choices=[('verification_pending', 'Verification pending'), ('active', 'Active'), ('inactive', 'Inactive')], default='verification_pending', help_text='Current status of document. Active/inactive, etc..', max_length=25, verbose_name='Status')),
                ('expiry_date', models.DateField(blank=True, help_text='Expiry date of document', null=True, verbose_name='Expiry Date')),
                ('date_uploaded', models.DateTimeField(auto_now_add=True, verbose_name='Date uploaded')),
                ('document_type', models.ForeignKey(help_text='Document type like Registration Certificate, Licence, etc..', on_delete=django.db.models.deletion.PROTECT, to='document.DocumentType')),
                ('vendor', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='vendor_document', to='vehicle.Vendor')),
            ],
            options={
                'verbose_name': 'Vendor document',
                'verbose_name_plural': 'Vendor documents',
            },
        ),
        migrations.AddField(
            model_name='vendor',
            name='current_address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='current_address', to='vehicle.VendorAddress'),
        ),
        migrations.AddField(
            model_name='vehicle',
            name='vendor',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='vehicle.Vendor'),
        ),
    ]
