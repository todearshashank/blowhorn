# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-12 12:03
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicle', '0009_auto_20180111_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vehicle',
            name='registration_certificate_number',
            field=models.CharField(db_index=True, max_length=25, unique=True, validators=[django.core.validators.RegexValidator(message='Registration Number format: AA11BB1111', regex='^[A-Z]{2}[0-9]{1,2}[A-Z]{1,3}[0-9]{4}$'), django.core.validators.MinLengthValidator(7)], verbose_name='RC Number'),
        ),
        migrations.AlterField(
            model_name='vehicleclass',
            name='breadth',
            field=models.CharField(blank=True, help_text='Width of loading deck e.g, 5ft 4in', max_length=20, null=True, verbose_name='Width'),
        ),
        migrations.AlterField(
            model_name='vehicleclass',
            name='capacity',
            field=models.CharField(blank=True, help_text='Tonnage of vehicle. e.g. 1.6 T', max_length=10, null=True, verbose_name='Capacity'),
        ),
        migrations.AlterField(
            model_name='vehicleclass',
            name='height',
            field=models.CharField(blank=True, help_text='Height of loading deck e.g, 5ft 4in', max_length=20, null=True, verbose_name='Height'),
        ),
        migrations.AlterField(
            model_name='vehicleclass',
            name='length',
            field=models.CharField(blank=True, help_text='Length of loading deck e.g, 5ft 4in', max_length=20, null=True, verbose_name='Length'),
        ),
    ]
