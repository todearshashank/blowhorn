# Generated by Django 2.1.1 on 2019-11-28 07:12

import audit_log.models.fields
from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('vehicle', '0035_auto_20191023_1753'),
    ]

    operations = [
        migrations.CreateModel(
            name='VendorMigrationHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('vendor', models.CharField(max_length=255, verbose_name='Target Fleet Owner')),
                ('number_of_vendors', models.IntegerField(default=0, help_text='No. of fleet owners merged', verbose_name='No. of fleet owners merged')),
                ('log_dump', django.contrib.postgres.fields.jsonb.JSONField(help_text='Migration log', verbose_name='Log')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='vendormigrationhistory_created_by', to=settings.AUTH_USER_MODEL)),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='vendormigrationhistory_modified_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Fleet Owner Migration History',
                'verbose_name_plural': 'Fleet Owner Migration History',
            },
        ),
    ]
