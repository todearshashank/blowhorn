# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-06-02 16:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0016_state_rcm_message'),
        ('vehicle', '0018_auto_20180517_0716'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicledocument',
            name='state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='address.State', verbose_name='Document Issuing State'),
        ),
        migrations.AddField(
            model_name='vendordocument',
            name='state',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='address.State', verbose_name='Document Issuing State'),
        ),
    ]
