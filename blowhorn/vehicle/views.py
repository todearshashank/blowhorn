import logging
import json

from rest_framework import generics, views, status
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.parsers import MultiPartParser, FormParser

from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.customer.models import Customer
from blowhorn.vehicle import utils as vehicle_util
from blowhorn.vehicle.models import VehicleClass
from blowhorn.vehicle.serializers import VehicleDocumentSerializer
from blowhorn.document.models import DocumentType
from blowhorn.users.permission import IsBlowhornStaff


@permission_classes([])
class VehicleModelList(views.APIView):

    def get(self, request):
        """
        Fetch all the vehicle models along with their vehicle class and body type
        """
        try:
            vehicle_model_list = vehicle_util.get_vehicle_models()
            logging.info('Vehicle model list fetched: ' +
                         str(vehicle_model_list))

            return Response(status=status.HTTP_200_OK, data=vehicle_model_list)
        except Exception as e:
            logging.error('Get vehicle model list output error: %s' % e)
            return Response(status=status.HTTP_400_BAD_REQUEST)


class VehicleDocumentUpload(views.APIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):

        logging.info(
            "Request from the app in vehicle document add : " + str(request.data.dict()))

        documents = request.data.dict()
        vehicle_id = None
        doc_status = {}

        if not documents:
            logging.info(
                "No documents where provided for the verification of vehicle")
            return Response(data="Please upload the documents",
                            status=status.HTTP_400_BAD_REQUEST)

        if documents.get('document_status', []):
            document_status = json.loads(documents.pop('document_status'))

            for statuses in document_status:
                vehicle_id = vehicle_id if vehicle_id else statuses.get(
                    'vehicle_id', '')

                if statuses.get('document_type', ''):
                    doc_status[statuses['document_type']] = statuses.get(
                        'document_status', '')

        if not vehicle_id:
            logging.info(
                "Documents not added for the activation of vehicle")
            return Response(data="Documents for activation of Vehicle is needed",
                            status=status.HTTP_400_BAD_REQUEST)

        for key in documents:
            doc_type = DocumentType.objects.filter(code=key).first()
            if not doc_type:
                logging.info(
                    "Invalid document type is provided")
                return Response(data="Provide the valid document type",
                                status=status.HTTP_400_BAD_REQUEST)

            document_data = {
                'file': documents[key],
                'document_type': doc_type.id,
                'vehicle': vehicle_id,
                'status': doc_status[key] if doc_status.get(key, '') else VERIFICATION_PENDING
            }

            document_serializer = VehicleDocumentSerializer(
                data=document_data)
            if document_serializer.is_valid():
                document_serializer.save(created_by=request.user, modified_by=request.user)
            else:
                logging.info(document_serializer.errors)
                return Response(document_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK,
                        data='Vehicle Details Added')


class VehicleClassList(generics.ListAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    permission_classes = (AllowAny,)

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = None
        if api_key:
            customer = Customer.objects.filter(api_key=api_key).first()

        if not customer:
            return Response(status=status.HTTP_401_UNAUTHORIZED,
                            data='Unauthorized, pass correct api_key in request header')

        vehicle_class = VehicleClass.objects.all()

        response = []

        for cls in vehicle_class:
            _data = {
                "commercial_class": cls.commercial_classification,
                "licence_category": cls.licence_category_desc,
                "vehicle_attributes": {
                    "length": cls.length,
                    "capacity": cls.capacity,
                    "breadth": cls.breadth,
                    "height": cls.height

                }

            }
            response.append(_data)
        return Response(status=status.HTTP_200_OK, data=response)


