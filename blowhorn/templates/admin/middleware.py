import pytz

from django.utils import timezone
from django.conf import settings
from blowhorn.common.middleware_mixin import MiddlewareMixin


class TimezoneMiddleware(MiddlewareMixin):

    def process_request(self, request):
        tzname = settings.ACT_TIME_ZONE
        if tzname:
            timezone.activate(pytz.timezone(tzname))
        else:
            timezone.deactivate()
