from blowhorn.common.serializers import BaseSerializer
from rest_framework import serializers

from .models import Company, Office


class CompanySerializer(BaseSerializer):

    class Meta:
        model = Company
        fields = ['name', ]


class OfficeSerializer(BaseSerializer):

    company = CompanySerializer()
    cin = serializers.SerializerMethodField()
    pan = serializers.SerializerMethodField()

    class Meta:
        model = Office
        fields = '__all__'

    def get_cin(self, office):
        return office.company.cin

    def get_pan(self, office):
        return office.company.pan
