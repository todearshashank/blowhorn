from blowhorn.company.models import CompanyBankAccount


def get_company_bank_account_details(config):
    """
    - Returns the bank account detail
    - If invoice config exists for a invoice then return the corresponding
      bank detail
    - If no invoice config exists then return the first bank account detail

    """
    if config:
        bank_obj = config.company_bank_account
    else:
        bank_obj = CompanyBankAccount.objects.first()

    bank_details = {
        'account_name': bank_obj.account_name,
        'account_number': bank_obj.account_number,
        'ifsc_code': bank_obj.ifsc_code
    }

    return bank_details
