import decimal
import logging

from django.conf import settings

from blowhorn.company.models import GSTInformation
from blowhorn.company.constants import CGST, SGST, IGST
from blowhorn.contract.constants import GTA, EXEMPT, VAT, BSS
from blowhorn.customer.models import CUSTOMER_CATEGORY_INDIVIDUAL, CUSTOMER_CATEGORY_NON_ENTERPRISE, \
    CUSTOMER_CATEGORY_ENTERPRISE


class TaxHelper(object):

    @staticmethod
    def get_applicable_tax_categories(
        contract=None, order=None, customer=None, service_type=None, invoice_state=None, service_state=None):
        """
        Returns applicable tax categories for given `contract`.
        """
        if settings.WEBSITE_BUILD == settings.MZANSIGO:
            return [VAT]
        if invoice_state != service_state:
            return [IGST]
        if contract and contract.customer:
            if contract.customer.has_tax_registered() and contract.service_tax_category == GTA:
                return []

            if contract.invoice_state == contract.service_state:
                return [CGST, SGST]
            return [IGST]
        elif order:
            if order.customer and order.customer.service_tax_category in [GTA, BSS]:
                return [CGST, SGST]
            return [VAT]
        elif customer and service_type and invoice_state and service_state:
            if customer.has_tax_registered() and service_type == GTA:
                return [CGST, SGST]
            if invoice_state == service_state:
                return [CGST, SGST]

        return None

    @staticmethod
    def _apply_tax_on_amount(amount, percentage):
        """
        Returns `percentage` % of `amount`.
        """
        return round(decimal.Decimal(amount) * (decimal.Decimal(percentage) /
                                                decimal.Decimal(100.0)), 2)

    @classmethod
    def calculate_tax_amount(
        cls, taxable_amount, contract=None, order=None, customer=None, service_type=None,
        invoice_state=None, service_state=None, is_adjustment = False, tax_exempt = False):
        """
        Returns total tax amount and distribution for given `contract` and `taxable_amount`.
        """

        from blowhorn.order.models import OrderConstants
        if tax_exempt:
            return round(0, 2), {}, True

        applicable_tax_categories = None
        if contract and contract.customer:
            service_type = contract.service_tax_category
            applicable_tax_categories = cls.get_applicable_tax_categories(contract, None)
            state = contract.service_state
            customer_category = contract.customer.customer_category
        elif service_type and invoice_state and service_state and not order:
            state = service_state
            applicable_tax_categories = cls.get_applicable_tax_categories(
                customer=customer, service_type=service_type, invoice_state=invoice_state, service_state=service_state)
            customer_category = customer.customer_category
        elif order:
            state = order.order_state
            service_type = order.customer.service_tax_category if order.customer else GTA
            applicable_tax_categories = cls.get_applicable_tax_categories(None, order)
            customer_category = order.customer.customer_category if order.customer else CUSTOMER_CATEGORY_INDIVIDUAL

        assert applicable_tax_categories is not None, "Error getting `applicable_tax_categories`."

        logging.info("Applicable Tax Categories : %s " % applicable_tax_categories)
        logging.info("Service Type : %s" % service_type)

        if not service_type:
            service_type = GTA
            logging.info("Setting Service Type : %s" % service_type)

        rcm = False
        total_tax = 0
        distribution = {}
        if service_type == EXEMPT:
            return round(total_tax, 2), distribution, rcm

        if service_type in [GTA, VAT]:
            # if customer_category != CUSTOMER_CATEGORY_ENTERPRISE:
            if (not is_adjustment and 0 < taxable_amount <= OrderConstants().get("MINIMUM_AMOUNT_FOR_TAX", 750)
                or customer_category == CUSTOMER_CATEGORY_INDIVIDUAL) or \
                (customer_category == CUSTOMER_CATEGORY_NON_ENTERPRISE and order and not order.customer.gst_info):
                return round(total_tax, 2), distribution, rcm

            rcm = True

        try:
            gst_config = GSTInformation.objects.get(
                state=state, service_type=service_type)
        except GSTInformation.DoesNotExist:
            gst_config = None

        assert gst_config, "GST not configured for State : {state} for Service {service}".format(
            state=state,
            service=service_type
        )

        if settings.WEBSITE_BUILD == settings.MZANSIGO:
            rcm = False

        for tax_category, percent in gst_config.percentage.items():
            if tax_category in applicable_tax_categories:
                amount_tax = TaxHelper._apply_tax_on_amount(taxable_amount, percent)
                total_tax += amount_tax
                distribution[tax_category] = {"percentage": percent, "amount": float(amount_tax)}
                logging.info("Applying {tax_category} at {percentage}% on {taxable_amount}% amounted to {amount}".format(
                    tax_category=tax_category, percentage=percent, taxable_amount=taxable_amount, amount=amount_tax)
                )
                distribution['rcm'] = rcm

        return round(total_tax, 2), distribution, rcm
