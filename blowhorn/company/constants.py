# Tax categories
CGST = u"CGST"
SGST = u"SGST"
IGST = u"IGST"
VAT = u"VAT"

CATBUS_CIN = "U74900KA2014PTC074571"

COMPANY_HEADER_FOOTER_DATA = {
    'blowhorn' : {
                'email' : 'invoice@blowhorn.com',
                'currency_unit' : 'Rs.',
                'currency_word' : 'Rupees'
    },

    'mzansigo' : {
                'email' : 'accounts@mzansigo.co.za',
                'currency_unit' : 'R',
                'currency_word' : 'Rand'
    }
}


