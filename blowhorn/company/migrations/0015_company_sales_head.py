# Generated by Django 2.1.1 on 2020-01-28 07:03

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('company', '0014_auto_20191001_1717'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='sales_head',
            field=models.ManyToManyField(blank=True, limit_choices_to={'is_staff': True}, related_name='sales_heads', to=settings.AUTH_USER_MODEL),
        ),
    ]
