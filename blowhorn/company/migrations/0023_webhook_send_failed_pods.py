# Generated by Django 2.2.24 on 2021-07-26 17:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0022_webhook_status_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='webhook',
            name='send_failed_pods',
            field=models.BooleanField(blank=True, default=False, null=True, verbose_name='Send Failed PODs ?'),
        ),
    ]
