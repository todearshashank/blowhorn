# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-04-26 14:37
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0002_companybankaccount'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='cin',
            field=models.CharField(blank=True, max_length=21, null=True, unique=True, validators=[django.core.validators.RegexValidator(message='First Alphabet, next 5 digits,next 2 Alphabet, next 4 digits,next 3 Alphabet, next 6 digits,fixed length is 21', regex='^[A-Za-z]{1}\\d{5}[A-Za-z]{2}\\d{4}[A-Za-z]{3}\\d{6}$'), django.core.validators.MinLengthValidator(21)], verbose_name='CIN'),
        ),
        migrations.AddField(
            model_name='company',
            name='pan',
            field=models.CharField(blank=True, max_length=10, null=True, unique=True, validators=[django.core.validators.RegexValidator(message='First 5 digits, next 4 digits,last Alphabet, fixed length is 10', regex='^[A-Za-z]{5}\\d{4}[A-Za-z]{1}$'), django.core.validators.MinLengthValidator(10)], verbose_name='PAN'),
        ),
    ]
