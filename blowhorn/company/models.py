import simplejson as json
import requests
from django.conf import settings
from django.db import models
# from rest_hooks.utils import get_module
# from rest_hooks import signals
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField, ArrayField
from django.core.validators import RegexValidator, MinLengthValidator
from phonenumber_field.modelfields import PhoneNumberField

from auditlog.registry import auditlog
from auditlog.models import AuditlogHistoryField

from blowhorn.address.models import State
from blowhorn.common.models import BaseModel
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.models import SERVICE_TAX_CATEGORIES
from blowhorn.document.file_validator import FileValidator
from blowhorn.document.utils import generate_file_path
from blowhorn.users.models import User
from blowhorn.common.middleware import current_request
# from rest_hooks.models import AbstractHook
from config.settings.status_pipelines import ORDER_STATUSES, ORDER_NEW, ORDER_STATUS_CHOICES

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class Company(BaseModel):
    name = models.CharField(_("Name"), max_length=255)

    cin = models.CharField(_('CIN/CRN'), max_length=21, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{1}\d{5}[A-Za-z]{2}\d{4}[A-Za-z]{3}\d{6}$|^\d{4}\/\d{6}\/\d{2}$',
            message=_('First Alphabet, next 5 digits,'
                      'next 2 Alphabet, next 4 digits,'
                      'next 3 Alphabet, next 6 digits,'
                      'fixed length is 21 /'
                      '(4 digits/6 digits/2 digits)'))])

    pan = models.CharField(_('PAN'), max_length=10, unique=True,
                           null=True, blank=True, validators=[RegexValidator(
            regex=r'^[A-Za-z]{5}\d{4}[A-Za-z]{1}$',
            message=_('First 5 digits, next 4 digits,'
                      'last Alphabet, fixed length is 10')),
            MinLengthValidator(10)])

    executives = models.ManyToManyField(User, related_name='executives', blank=True,
                                        limit_choices_to={'is_staff': True,
                                                          'is_active': True})

    collection_representatives = models.ManyToManyField(
        User, related_name='collection_representatives', blank=True,
        limit_choices_to={'is_staff': True})

    revenue_locked_date = models.DateField(_('Date till revenue is locked'),
                                     null=True, blank=True,
                                     help_text=_("Date till revenue is locked: DD MM YYYY"))

    non_enterprise_manager = models.ManyToManyField(
        User, related_name="non_enterprise_managers", blank=True,
        limit_choices_to={'is_staff': True})

    sales_head = models.ManyToManyField(
        User, related_name="sales_heads", blank=True,
        limit_choices_to={'is_staff': True}
    )

    finance_user = models.ManyToManyField(User, related_name='finance_user',
                    blank=True, limit_choices_to={'is_staff': True})

    class Meta:
        verbose_name = _("Company")
        verbose_name_plural = _("Company")

    def save(self, *args, **kwargs):
        _request = current_request()
        changed_list = []
        current_user = _request.user if _request else ''

        if self._get_pk_val():
            old = self.__class__.objects.get(pk=self._get_pk_val())
            for field in self.__class__._meta.fields:
                field_name = field.__dict__.get('name')

                if field_name in ['revenue_locked_date']:
                    old_value = field.value_from_object(old)
                    current_value = field.value_from_object(self)

                    if old_value != current_value:
                        changed_list.append(CompanyHistory(field=field_name, old_value=str(old_value),
                                                           new_value=str(current_value), user=current_user,
                                                           company=old))

            CompanyHistory.objects.bulk_create(changed_list)

        return super().save(*args, **kwargs)

    def __str__(self):
        return self.name


alphabets_only_validator = RegexValidator(regex=r'^[a-zA-Z_\s]+$',
                                          message=_('Only Alphabets '
                                                    'characters allowed'))

alphanumeric_validator = RegexValidator(regex=r'^[a-zA-Z0-9_\s]+$',
                                        message=_('Only AlphabetNumeric '
                                                  'characters allowed'))


class CompanyBankAccount(BaseModel):
    ''' Stores bank accounts for the company
        Configurable at customer/contract level
    '''

    history = AuditlogHistoryField()

    ifsc_validator = RegexValidator(
        regex=r'^[A-Za-z]{4}[0]\w{6}$|^\d{6}$',
        message=_('6 Digit Branch Code/First 4 Alphabets(Bank Code), then 0 and next 6 '
                  'Alphanumeric characters(Branch Code)'))

    account_name = models.CharField(
        _("Full name as registered in the bank"), max_length=100,
        validators=[alphabets_only_validator])

    account_number = models.CharField(
        _("Bank Account number"), max_length=20,
        validators=[alphanumeric_validator])

    ifsc_code = models.CharField(
        _("Branch Code/IFSC Code for the bank"), max_length=11,
        validators=[ifsc_validator],
        help_text=_("6 Digit Branch Code/First 4 Bank Code, then 0 and then next 6 Alphanumeric "
                    "characters(Branch Code)"))

    nick_name = models.CharField(
        _("Nick Name"), max_length=250, null=True, blank=True)

    def __str__(self):
        return '%s %s %s' % (self.nick_name, self.account_name,
                             self.account_number)

    class Meta:
        verbose_name = _("Company Bank Account")
        verbose_name_plural = _("Company Bank Accounts")


auditlog.register(CompanyBankAccount)


class Office(BaseModel):
    address = models.TextField(
        _("Address")
    )

    state = models.ForeignKey('address.State', on_delete=models.CASCADE)

    gst_identification_no = models.CharField(
        _("GST Identification/VAT No."),
        max_length=15
    )

    is_head = models.BooleanField(_("Is head office ?."), default=False)

    company = models.ForeignKey(Company, related_name="offices",
                                on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Office")
        verbose_name_plural = _("Offices")

    def __str__(self):
        return str(self.state) + " : " + str(self.address)


class OfficeGSTInformation(BaseModel):

    office = models.OneToOneField(
        Office, on_delete=models.PROTECT
    )

    # gst_identification_no = models.CharField(
    #     _("GST Identification/VAT No."),
    #     max_length=15
    # )

    address = models.CharField(
        _("Office Address"),
        max_length=99
    )

    city = models.CharField(
        _("Office City"),
        max_length=40
    )

    pin_code = models.CharField(
        _("Pin Code"),
        max_length=6
    )

    mobile_number = PhoneNumberField(
        _("GST Registered Mobile Number"), blank=True, null=True)

    email = models.EmailField(
        verbose_name='email address',
        max_length=255)

    gst_user_id = models.CharField(
        _("gst user id"),
        max_length=50
    )

    token_expire_time = models.CharField(
        _("Token Expire On"), null=True, blank=True,
        max_length=50
    )


class GSTInformation(BaseModel):
    state = models.ForeignKey(
        State, null=True, blank=True, on_delete=models.PROTECT
    )

    service_type = models.CharField(
        _('GTA/BSS'), max_length=12, null=True, blank=True,
        choices=SERVICE_TAX_CATEGORIES)

    _percentage = JSONField(null=True, db_column="percentage")

    class Meta:
        unique_together = ("state", "service_type")
        verbose_name = _("GST/VAT Information")
        verbose_name_plural = _("GST/VAT Information")

    def __str__(self):
        return str(self.state) + str(self.service_type)

    @property
    def percentage(self):
        """
        Returns the `_percentage` in json format.
        """

        return json.loads(self._percentage) if isinstance(self._percentage, str) \
            else self._percentage


class NewsManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset()

    def get_all_news(self):
        return self.get_queryset()


class News(BaseModel):
    thumbnail = models.FileField(
        upload_to=generate_file_path,
        validators=[FileValidator()],
        max_length=256, blank=True, null=True)
    heading = models.TextField(help_text=_('News headline'))
    body = models.TextField(help_text=_('First paragraph from published news'))
    published_date = models.DateField()
    publishers_logo = models.FileField(
        upload_to=generate_file_path,
        validators=[FileValidator(**{
            'allowed_extensions': [
                'png', 'jpeg', 'jpg', 'svg'
            ],
            'allowed_mimetypes': [
                'image/jpeg', 'image/png', 'application/svg+xml',
                'image/svg+xml',
            ]
        })],
        max_length=256, blank=True, null=True,
        help_text=_('Logo of news portal where new got published'))
    article_link = models.TextField(help_text=_('Link to the original page'))

    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')

    def clean(self):
        if not self.article_link.startswith(('https://', 'http://')):
            self.article_link = 'http://%s' % self.article_link.strip()

    def __str__(self):
        return '%s | %s' % (self.heading[:50], self.published_date)


class CompanyHistory(BaseModel):
    """
    Capturing the Company history
    """
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    field = models.CharField(max_length=50)
    old_value = models.CharField(max_length=250, null=True, blank=True)
    new_value = models.CharField(max_length=250, null=True, blank=True)
    user = models.CharField(max_length=50)

    class Meta:
        verbose_name = _('Company History')
        verbose_name_plural = _('Company History')

    def __str__(self):
        return str(self.company_id)


class AbstractHook(models.Model):
    """
    Stores a representation of a Hook.
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(User, related_name='%(class)ss', on_delete=models.CASCADE, null=True, blank=True)
    event = models.CharField('Event', max_length=64, db_index=True)
    target = models.URLField('Target URL', max_length=255)

    class Meta:
        abstract = True


class WebHook(AbstractHook):
    is_active = models.BooleanField(default=True)

    verify_payload = models.BooleanField(_('Verify payload using HMAC Encryption'), default=False)

    hmac_secret = models.CharField(_('HMAC Secret'), blank=True, null=True, max_length=50)

    headers = JSONField(_('Headers'), default=dict)

    status_config = ArrayField(
        models.CharField(choices=ORDER_STATUS_CHOICES, max_length=50, blank=True, null=True), blank=True, null=True
    )

    send_failed_pods = models.BooleanField(_('Send Failed PODs ?'), default=False, null=True, blank=True)

    def save(self, *args, **kwargs):
        import secrets
        if not self.hmac_secret:
            self.hmac_secret = secrets.token_hex(16)

        return super().save(*args, **kwargs)

    def serialize_hook(self, instance):
        from blowhorn.order.models import Order, OrderDocument
        shipping_address = instance.shipping_address
        shipment_info = None
        try:
            if instance.shipment_info:
                shipment_info = json.loads(instance.shipment_info)
        except:
            pass

        event = instance.event_set.filter(status=instance.status).last()

        data = {
            "WaybillNo": instance.number,
            "eventType": "updateOrderStatus",
            "OrderStatus": ORDER_STATUSES.get(instance.status, "Pending"),
            "CourierComment": "",
            'StatusDateTime': instance.updated_time.isoformat(),
            'ToMobileNumber': str(shipping_address.phone_number) if shipping_address.phone_number else '',
            'ToEmail': shipment_info.get('customer_email', '') if shipment_info and type(shipment_info) == dict else '',
            'ToName': shipping_address.first_name,
            'Client': str(instance.customer),
            'StatusCode': ORDER_STATUSES.get(instance.status, ORDER_NEW),
            'EDD': instance.expected_delivery_time.isoformat() if instance.expected_delivery_time else '',
            "order_no": instance.reference_number,
            "order_status": ORDER_STATUSES.get(instance.status, "Pending"),
        }

        if event and event.current_location:
            data.update({"location": {'lat': event.current_location.y, 'long': event.current_location.x}})

        if instance.status in [StatusPipeline.ORDER_DELIVERED, StatusPipeline.ORDER_PICKED]:
            pods = OrderDocument.objects.filter(order__number=instance.number).exclude(is_document_uploaded=True)
            pod_files = []
            for pod in pods:
                pod_files.append(pod.file.url.replace("\u0026", "&"))
            if pod_files:
                pods.update(is_document_uploaded=True)
                data.update({'pod_docs': pod_files})
                data.update({'podDocs': pod_files})
        return data

    def deliver_hook(self, instance, payload_override=None, headers=None):
        """
        Deliver the payload to the target URL.

        By default it serializes to JSON and POSTs.
        """
        import logging
        import hmac
        import urllib.parse
        import hashlib

        logging.basicConfig(level=logging.DEBUG)
        logger = logging.getLogger(__name__)

        if not headers:
            headers = {'Content-Type': 'application/json'}

        payload = payload_override or self.serialize_hook(instance)
        sync_request = headers.pop('is_sync', None)

        if self.verify_payload:
            payload_bytes = urllib.parse.urlencode(payload).encode('utf8')
            sign = hmac.new(self.hmac_secret.encode('utf8'), payload_bytes, hashlib.sha256).hexdigest()
            headers.update({'Sign': sign})

            if self.status_config:
                # Send webhooks only for the statuses configured in the status config
                if payload.get('OrderStatus', None) in self.status_config:
                    response = requests.post(self.target, json=payload, headers=headers)
                else:
                    logger.info(
                        'Webhook not triggered for order number: %s, as status %s is not configured in status config' % (
                            instance.number, payload.get('OrderStatus', None)))
                    return
            else:
                # Send webhooks for all statuses
                response = requests.post(self.target, json=payload, headers=headers)
                logger.info('Webhook for order number: %s. Payload: %s Response received: %s' % (
                    instance.number, payload, response.content))

            return response

        if self.status_config:
            # Send webhooks only for the statuses configured in the status config
            if payload.get('OrderStatus', None) in self.status_config:
                response = requests.post(self.target, json=payload, headers=headers)
            else:
                logger.info(
                    'Webhook not triggered for order number: %s, as status %s is not configured in status config' % (
                        instance.number, payload.get('OrderStatus', None)))
                return
        else:
            # Send webhooks for all statuses
            response = requests.post(self.target, json=payload, headers=headers)

        logger.info('Webhook for order number: %s. Payload: %s Response received: %s' % (
            instance.number, payload, response.content))

        return response


def find_and_fire_hook(event_name, instance, **kwargs):
    from blowhorn.order.models import Order
    user = instance.user if isinstance(instance, Order) else instance.order.user
    # customer_id = user.customer.id if hasattr(user, 'customer') else None
    # if customer_id:
    #     statuses = StatusPipeline.WEBHOOK_STATUS_MAP.get(customer_id)
    #     if statuses and not instance.status in statuses:
    #         return
    filters = {
        'event': event_name,
        'is_active': True,
        'user': user
    }

    hooks = WebHook.objects.filter(**filters)
    for hook in hooks:
        hook.deliver_hook(instance, headers=json.loads(hook.headers))



