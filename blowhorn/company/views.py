# Django and system libraries
import json
import logging
from django.utils.translation import gettext_lazy as _

# 3rd party libraries
from rest_framework import generics, views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import BasicAuthentication, SessionAuthentication

# blowhorn imports
from blowhorn.address.models import State
from blowhorn.company.models import Office
from blowhorn.order.models import Order
from blowhorn.company.models import WebHook
from blowhorn.customer.views import get_customer_from_api_key

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class WebHooksList(views.APIView):
    permission_classes = (AllowAny, )

    def get(self, request):
        """
            Returns the active configured webhooks for the customer
        """
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = get_customer_from_api_key(api_key)

        if not customer:
            response = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        webhooks = WebHook.objects.filter(user=customer.user, is_active=True)

        if not webhooks.exists():
            response = {
                'status': 'FAIL',
                'message': 'Could not find any active webhooks'
            }
            return Response(status=status.HTTP_200_OK, data=response)

        data = []

        for webhook in webhooks.iterator():
            headers = json.loads(webhook.headers)
            data.append({
                'webhookId': webhook.id,
                'createdAt': webhook.created.strftime('%b %d, %Y'),
                'callbackUrl': webhook.target,
                'headers': headers,
                'verify_payload': webhook.verify_payload
            })

        return Response(status=status.HTTP_200_OK, data=data)


class WebHooksSubscribe(views.APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        """
            Creates a new webhook for the customer with given parameters
        """

        api_key = request.META.get('HTTP_API_KEY', None)
        customer = get_customer_from_api_key(api_key)

        data = request.data
        logger.info('Data received for webhook creation: %s' % data)

        if not customer:
            response = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        event = 'order.added'
        user = customer.user
        target = data.get('callbackUrl', None)
        headers = data.get('headers', {})
        verify_payload = data.get('verify_payload', False)

        try:
            json.dumps(headers)
        except TypeError:
            response = {
                'status': 'FAIL',
                'message': 'headers has to be in JSON format'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        if not target:
            response = {
                'status': 'FAILED',
                'message': 'callbackUrl cannot be empty'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        if WebHook.objects.filter(target=target).exists():
            response = {
                'status': 'FAIL',
                'message': 'Webhook is already configured for the specified URL'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        webhook = WebHook.objects.create(
            event=event,
            user=user,
            target=target,
            headers=headers,
            verify_payload=verify_payload
        )
        logger.info('Webhook created for customer %s. Webhook ID - %s' % (customer.name, webhook.id))

        response = {
            'status': 'PASS',
            'message': 'Webhook configured successfully'
        }

        return Response(status=status.HTTP_200_OK, data=response)


class WebHooksUpdate(views.APIView):
    permission_classes = (AllowAny,)

    def put(self, request):
        """
            Update the parameters in existing webhooks
        """

        api_key = request.META.get('HTTP_API_KEY', None)
        customer = get_customer_from_api_key(api_key)

        data = request.data
        logger.info('Data received for webhook updation: %s' % data)

        if not customer:
            response = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        webhook_id = data.get('webhookId', None)
        target = data.get('callbackUrl', None)
        headers = data.get('headers', None)
        is_active = data.get('is_active', True)
        verify_payload = data.get('verify_payload', False)

        webhook = WebHook.objects.filter(id=webhook_id).last()

        if not webhook:
            response = {
                'status': 'FAIL',
                'message': 'Invalid webhook ID'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

        if target:
            webhook.target = target
        if headers:
            try:
                json.dumps(headers)
            except TypeError:
                response = {
                    'status': 'FAIL',
                    'message': 'headers has to be in JSON format'
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=response)

            webhook.headers = headers
        webhook.is_active = is_active
        webhook.verify_payload = verify_payload
        webhook.save()

        response = {
            'status': 'PASS',
            'message': 'Updated the webhook successfully'
        }

        return Response(status=status.HTTP_200_OK, data=response)


class TransporterDetail(generics.ListAPIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = get_customer_from_api_key(api_key)

        if not customer:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        data = request.GET
        state_name = data.get('state')

        state = State.objects.filter(name=state_name).first()
        if not state:
            _out = {
                'status': 'FAIL',
                'message': 'Invalid state name, Use get State api to get state names'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        office = Office.objects.filter(state=state).select_related('company').first()

        if office:
            _out = {
                'status': 'SUCCESS',
                'message': {
                    'legal_name': office.company.name if office.company else '',
                    'gst_no': office.gst_identification_no
                }
            }
            return Response(status=status.HTTP_200_OK, data=_out)

        _out = {
            'status': 'FAIL',
            'message': 'GST Info not available for this State. Contact Sales Team.'
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)


class TransporterDetail(generics.ListAPIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        customer = get_customer_from_api_key(api_key)

        if not customer:
            _out = {
                'status': 'FAIL',
                'message': 'Unauthorized'
            }
            return Response(status=status.HTTP_401_UNAUTHORIZED, data=_out)

        data = request.data
        order_number = data.get('number')
        order = Order.objects.filter(number=order_number, customer=customer).first()
        if not order:
            _out = {
                'status': 'FAIL',
                'message': 'Invalid Order number'
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)

        message = {
            'Mode': 'Road',
            'Vehicle / Trans Doc No & Dt.': str(order.driver.driver_vehicle) + ' & ' + order.number + ' & ' \
                                            + order.pickup_datetime.date(),
            'From': order.pickup_address.line3,

        }

        _out = {
            'status': 'FAIL',
            'message': 'GST Info not available for this State Contact Slaes Team.'
        }
        return Response(status=status.HTTP_400_BAD_REQUEST, data=_out)
