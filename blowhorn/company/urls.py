from django.conf.urls import url
from .views import WebHooksList, WebHooksSubscribe, WebHooksUpdate
from .views import TransporterDetail

urlpatterns = [
            url(r'^ewaybill/get-transporter-details$', TransporterDetail.as_view(), name='transporter-detail'),
            url(r'^webhooks/list', WebHooksList.as_view(), name='webhooks-list'),
            url(r'^webhooks/subscribe', WebHooksSubscribe.as_view(), name='webhooks-subscribe'),
            url(r'^webhooks/update', WebHooksUpdate.as_view(), name='webhooks-update')
        ]
