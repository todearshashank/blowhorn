from django import forms
from django.contrib import admin
from django.db import models
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from .models import GSTInformation, Company, Office, CompanyBankAccount, News, CompanyHistory, WebHook, \
    OfficeGSTInformation
from blowhorn.common.admin import BaseAdmin
from blowhorn.document.widgets import ImagePreviewWidget
from blowhorn.users.constants import AUDITLOG_FIELDS
from blowhorn.users.models import User


class InvoiceOfficeInlineFormset(forms.models.BaseInlineFormSet):

    def clean(self):
        head_office_count = 0
        states = []
        for form in self.forms:
            try:
                if form.cleaned_data and not form.cleaned_data['DELETE']:
                    states.append(form.cleaned_data['state'])
                    if form.cleaned_data['is_head']:
                        head_office_count += 1
            except AttributeError:
                pass
        if len(states) != len(set(states)):
            raise forms.ValidationError(_('Only one office address is expected '
                                          'to be added for each state.'))
        if head_office_count != 1:
            raise forms.ValidationError(_('Only one office would be marked '
                                          'as head office.'))


class OfficeInline(admin.TabularInline):
    model = Office
    formset = InvoiceOfficeInlineFormset
    extra = 0

class CompanyHistoryInline(admin.TabularInline):
    model = CompanyHistory
    extra = 0
    list_display = ['field', 'old_value',
                       'new_value', 'user', 'created_date']

    readonly_fields = list_display

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class CompanyForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        users = User.objects.filter(is_staff=True, is_active=True).exclude(
            email__icontains='blowhorn.net')

        if 'non_enterprise_manager' in self.fields:
            self.fields['non_enterprise_manager'].autocomplete = False
            self.fields['non_enterprise_manager'].queryset = users

        if 'collection_representatives' in self.fields:
            self.fields['collection_representatives'].autocomplete = False
            self.fields['collection_representatives'].queryset = users

        if 'executives' in self.fields:
            self.fields['executives'].autocomplete = False
            self.fields['executives'].queryset = users

        if 'sales_head' in self.fields:
            self.fields['sales_head'].autocomplete = False
            self.fields['sales_head'].queryset = users

        if 'finance_user' in self.fields:
            self.fields['finance_user'].autocomplete = False
            self.fields['finance_user'].queryset = users


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    inlines = [OfficeInline, CompanyHistoryInline]
    form = CompanyForm


@admin.register(GSTInformation)
class GSTInformationAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'state', 'service_type', 'percentage'
    )


@admin.register(OfficeGSTInformation)
class OfficeGSTInformationAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'office', 'address', 'mobile_number',
    )

@admin.register(CompanyBankAccount)
class CompanyBankAccount(admin.ModelAdmin):

    list_display = ('nick_name', 'account_name', 'account_number')


@admin.register(News)
class NewsAdmin(BaseAdmin):
    list_display = ('get_heading', 'get_article_link', 'published_date',
                    'created_by', 'created_date', 'modified_by',
                    'modified_date')
    search_fields = ('heading', 'created_by', 'modified_by')
    list_select_related = ('created_by', 'modified_by')
    list_filter = (('created_date', admin.DateFieldListFilter),)
    readonly_fields = AUDITLOG_FIELDS
    formfield_overrides = {
        models.FileField: {
            "widget": ImagePreviewWidget
        }
    }

    def get_heading(self, obj):
        if obj is not None:
            return obj.heading[:80]
        return '--NA--'

    get_heading.short_description = _('headline')

    def get_article_link(self, obj):
        if obj is not None:
            return format_html("<a href='{}' target='_blank'>{}</a>",
                               obj.article_link, 'View')
        return '--NA--'

    get_article_link.short_description = _('article link')

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (('General', {
                    'fields': ('heading', 'body', 'published_date',
                               'article_link')
                }),
                ('Thumbnails', {
                    'fields': ('thumbnail', 'publishers_logo',)
                }),
                ('Auditlog', {
                    'fields': AUDITLOG_FIELDS
                }),
            )
        return (
            (None, {
                'fields': ('heading', 'body', 'published_date', 'thumbnail',
                           'publishers_logo', 'article_link')
            }),
        )

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('created_by', 'modified_by')


@admin.register(WebHook)
class WebHookAdmin(admin.ModelAdmin):
    list_display = ('id', 'event', 'user', 'is_active', 'target', 'send_failed_pods')

    fields = ('event', 'user', 'is_active', 'target', 'headers', 'verify_payload', 'status_config', 'send_failed_pods')

    search_fields = ['customer__name']

    actions = None
