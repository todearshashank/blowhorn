from django.conf.urls import url
from blowhorn.report.views import *

urlpatterns = [
    url(r'^report/order/status$', ShipmentStatus.as_view(), name='order-status'),
    url(r'^report/order/timeplaced$', ShipmentTimePlaced.as_view(), name='time-placed'),
    url(r'^report/order/paymentmode$', ShipmentPaymentMode.as_view(), name='payment-mode'),
    url(r'^report/order/attempts$', ShipmentAttempts.as_view(), name='order-attempts'),
    url(r'^report/order/placed$', ShipmentPlacedTrend.as_view(), name='order-placed'),
    url(r'^report/order/deliveryperformance$', ShipmentDeliveryPerformance.as_view(), name='order-deliveryperformance'),
    url(r'^report/order/samedaydelivery$', ShipmentSameDayDelivery.as_view(), name='order-samedaydelivery'),
    url(r'^report/order/sla$', ShipmentSLA.as_view(), name='order-sla'),
    url(r'^report/order/autodispatch$', ShipmentAutoDispatch.as_view(), name='order-autodispatch'),
    url(r'^report/trip/stats$', TripCompletedStats.as_view(), name='trip-completed-stats'),
    url(r'^report/trip/allstats$', TripAllStats.as_view(), name='trip-all-stats'),
    url(r'^report/trip/contract$', TripContractCategory.as_view(), name='trip-contract-category'),
    url(r'^report/trip/status$', TripStatusCategory.as_view(), name='trip-status-category'),
    url(r'^report/trip/feedback$', SubmitFeedback.as_view(), name='user-feedback'),
    url(r'^report/customer$', ReportCreation.as_view(), name='report-genration'),
    url(r'^report/heatmap$', CustomerOrderHeatmapView.as_view(), name='order-heatmap'),
    url(r'^report/schedules/supportdata$', ReportScheduleSupportDataView.as_view(), name='report-schedules-supportdata'),
    url(r'^report/schedules$', CustomerReportScheduleListView.as_view(), name='report-schedules'),
    url(r'^report/wms/inventory/tiles$', WmsInventoryTiles.as_view(), name='wms-analytics-inventory-tiles'),
    url(r'^report/wms/inventory/creationtrend$', WmsInventoryCreationTrend.as_view(), name='wms-analytics-inventory-creationtrend'),
    url(r'^report/wms/inventory/hubsummary$', WmsHubSummary.as_view(), name='wms-analytics-inventory-hubsummary'),
    url(r'^report/wms/inventory/expiring$', WmsExpiringInventory.as_view(), name='wms-analytics-inventory-expiring'),
    url(r'^report/wms/inventory/statustrend$', WmsInventoryByStatus.as_view(), name='wms-analytics-inventory-gruopby-status'),
]
