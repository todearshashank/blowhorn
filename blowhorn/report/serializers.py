from rest_framework import serializers

from blowhorn.customer.models import Report, CustomerReportSchedule


class CustomerReportScheduleListSerializer(serializers.ModelSerializer):
    queryset = CustomerReportSchedule.objects.all()
    report_type_name = serializers.CharField(source='report_type.name', read_only=True)
    report_type_category = serializers.CharField(source='report_type.category', read_only=True)
    city = serializers.CharField(source='city.name', read_only=True)
    interval = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(CustomerReportScheduleListSerializer, self).__init__(*args, **kwargs)
        fields = self.context.get('fields', None)
        exclude_fields = self.context.get('exclude', None)
        if fields:
            self.Meta.fields = fields
        elif exclude_fields:
            self.Meta.exclude = exclude_fields
        else:
            self.Meta.fields = '__all__'

    class Meta:
        model = CustomerReportSchedule

    def get_interval(self, obj):
        schedule = obj.service_schedule.rrules[0]
        return schedule.frequencies[schedule.freq]
