from blowhorn.customer.constants import (
    SHIPMENT_ORDER_MINIMAL_EXPORT, EVENT_COLUMNS_EXPORT, SHIPMENT_ORDERLINE_EXPORT, \
    SHIPMENT_ORDER_MIS_SUMMARY_EXPORT, \
    SHIPMENT_ORDER_MIS_EXPORT, \
    SHIPMENT_ORDER_NDR_EXPORT)
from blowhorn.report.services.wms_reports import WMSReport

from blowhorn.order.services.shipment_export import ShipmentExport

def report_export_type_minimal(queryset, email_data):
    field_names = SHIPMENT_ORDER_MINIMAL_EXPORT.keys()
    column_names = SHIPMENT_ORDER_MINIMAL_EXPORT.values()

    ShipmentExport(
        query_params=queryset,
        field_names=field_names,
        column_names=column_names,
        email_data = email_data
    ).export_minimal_details()

def report_export_type_pod(queryset, email_data):
    ShipmentExport(
        query_params=queryset,
        field_names=[],
        column_names=[],
        email_data = email_data
    ).export_pod_documents()

def report_export_type_with_events(queryset, email_data):
    field_names = EVENT_COLUMNS_EXPORT.keys()
    column_names = EVENT_COLUMNS_EXPORT.values()

    ShipmentExport(
        query_params=queryset,
        field_names=field_names,
        column_names=column_names,
        email_data = email_data
    ).export_orders_with_events()

def report_export_type_orderline(queryset, email_data):
    field_names = SHIPMENT_ORDERLINE_EXPORT.keys()
    column_names = SHIPMENT_ORDERLINE_EXPORT.values()

    ShipmentExport(
        filename_prefix='shipments_orderlines',
        query_params=queryset,
        field_names=field_names,
        column_names=column_names,
        email_data = email_data
    ).export_orderline_details()

def report_export_type_mis_summary(queryset, email_data):
    field_names = SHIPMENT_ORDER_MIS_SUMMARY_EXPORT.keys()
    column_names = SHIPMENT_ORDER_MIS_SUMMARY_EXPORT.values()

    ShipmentExport(
        filename_prefix='mis_summary',
        query_params=queryset,
        field_names=field_names,
        column_names=column_names,
        email_data = email_data
    ).export_mis_summary()

def report_export_type_mis(queryset, email_data):
    field_names = SHIPMENT_ORDER_MIS_EXPORT.keys()
    column_names = SHIPMENT_ORDER_MIS_EXPORT.values()

    ShipmentExport(
        filename_prefix='mis_report',
        query_params=queryset,
        field_names=field_names,
        column_names=column_names,
        email_data = email_data
    ).export_order_mis()

def report_export_type_ndr(queryset, email_data):
    field_names = SHIPMENT_ORDER_NDR_EXPORT.keys()
    column_names = SHIPMENT_ORDER_NDR_EXPORT.values()

    ShipmentExport(
        filename_prefix='ndr_report',
        query_params=queryset,
        field_names=field_names,
        column_names=column_names,
        email_data = email_data
    ).export_order_ndr()

def report_export_type_inventory(queryset, email_data):
    WMSReport(**email_data, email_data=email_data).export_inventory_data()

def report_export_type_inventory_txn(queryset, email_data):
    WMSReport(**email_data,email_data=email_data).export_inventory_transaction_data()

# def report_export_type_hub_summary(queryset, email_data):
#     WMSReport(**email_data).export_hub_summary()

# def report_export_type_wmsmis(queryset, email_data):
#     WMSReport(**email_data).export_mis_summary()