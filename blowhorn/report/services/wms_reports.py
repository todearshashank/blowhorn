import csv
import uuid
import os
import pytz
import zipfile
import boto3
import os
import operator
import pandas as pd
from functools import reduce
from io import StringIO
from django.core.files.base import ContentFile
from django.conf import settings
from django.core.files import File
from django.db.models import F, Q
from django.utils import timezone
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _

from blowhorn.oscar.core.loading import get_model
from blowhorn.utils.functions import utc_to_ist
from blowhorn.utils.datetime_function import DateTime
from blowhorn.wms.submodels.inventory import (Inventory, InventoryTransaction,)
from blowhorn.report.constants import (INVENTORY_TXN_DATA_TO_BE_EXPORTED,
    INVENTORY_DATA_TO_BE_EXPORTED)

UserReport = get_model('customer', 'UserReport')
tz_diff = DateTime.get_timezone_offset()

s3 = boto3.client('s3', aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

class WMSReport(object):
    """
     WMS Scheduled Reports
    """

    def __init__(self, customer, start_date, end_date, report_type, city, period, email_data=None):
        """
        :param filename_prefix: str
        :param query_params: filter parameters (dict/instance of Q)
        :param field_names: database columns (list)
        :param column_names: column headers for excel (list)
        """
        self.filename_prefix = None
        self.timestamp_format = 'DD/MM/YYYY HH24:MI:SS'
        self.email_data = email_data
        self.column_to_be_imported = None
        self.start_date = start_date
        self.end_date = end_date
        self.customer = customer
        self.header_list = None
        self.report_type = report_type
        self.city = city
        self.period = period

    def export_inventory_data(self):
        self.filename_prefix = 'inventory_data'
        _inventory_bufferio = StringIO()
        self.column_to_be_imported = INVENTORY_DATA_TO_BE_EXPORTED
        
        query = [Q(client__customer=self.customer)]
        if self.start_date:
            query.append(Q(created_date__date__gte=self.start_date))

        final_query = reduce(operator.and_, query)
        cs = [e['label'] for e in self.column_to_be_imported]
        _data = Inventory.objects.filter(final_query).annotate(
                        client_name = F('client__name'),
                        sku_name = F('sku__name'),
                        supplier_name = F('supplier__name'),
                        packconfig_name = F('pack_config__name'),
                        site_name = F('site__name'),
                    ).order_by('-created_date')
        
        if _data:
            df = pd.DataFrame.from_records(_data.values(), columns=cs)
            df.to_csv(_inventory_bufferio)
            self.write_to_excel_file(_inventory_bufferio, 'inventory')

    def export_inventory_transaction_data(self):
        self.filename_prefix = 'inventory_transaction'
        _invtxn_bufferio = StringIO()
        self.column_to_be_imported = INVENTORY_TXN_DATA_TO_BE_EXPORTED
        
        query = [Q(client__customer=self.customer)]
        if self.start_date:
            query.append(Q(created_date__date__gte=self.start_date))

        final_query = reduce(operator.and_, query)
        cs = [e['label'] for e in self.column_to_be_imported]
        _data = InventoryTransaction.objects.filter(final_query).annotate(
                        client_name = F('client__name'),
                        sku_name = F('sku__name'),
                        supplier_name = F('supplier__name'),
                        packconfig_name = F('pack_config__name'),
                        site_name = F('site__name'),
                        from_loc_name = F('from_loc__name'),
                        to_loc_name = F('to_loc__name'),
                    ).order_by('-created_date')
        
        if _data:
            df = pd.DataFrame.from_records(_data.values(), columns=cs)
            df.to_csv(_invtxn_bufferio)
            self.write_to_excel_file(_invtxn_bufferio, 'inventory_transaction')

    # def export_hub_summary(self):
    #     self.filename_prefix = 'hub_summary'
    #     self.column_to_be_imported = HUB_INV_SUMMARY_DATA_TO_BE_EXPORTED
    #     query = {'hub__customer' : self.customer}

    #     if self.start_date:
    #         query['modified_date__date__gte'] = self.start_date

    #     _data = Inventory.objects.filter(**query
    #         ).select_related(
    #             'hub',
    #             'sku',
    #             'sku__product_group'
    #         ).values(
    #             'hub__name',
    #             'sku__product_group__name'
    #         ).annotate(
    #             hub_name=F('hub__name'),
    #             product_group=F('sku__product_group__name'),
    #             total_inv=Count('qty'),
    #             qc_passed=Count('qc_status', filter=Q(qc_status='Passed'))
    #     ).order_by('hub_name')

    #     cs = [e['label'] for e in self.column_to_be_imported]
    #     if _data:
    #         hub_summary = pd.DataFrame.from_records(list(_data), columns=cs)
    #         hub_summary.to_csv(self.filename_prefix)
    #         self.write_to_excel_file()

    # def export_mis_summary(self):
    #     self.filename_prefix = 'mis_report.xlsx'
        
    #     query = {'hub__customer': self.customer}
    #     if self.start_date:
    #         query['created_date__date__gte'] = self.start_date

    #     _cycle = Inventory.objects.filter(**query
    #                 ).values('created_date__date', 'hub', 'sku__product_group__name', 'transportation_status').annotate(
    #                     event_date=Func(
    #                         F('created_date'),
    #                         Value("DD Mon YY"),
    #                         function='to_char',
    #                         output_field=CharField()
    #                     ),
    #                     hub_name=F('hub__name'),
    #                     product=F('sku__product_group__name'),
    #                     quantity=Sum('qty'),
    #                     status=Case(
    #                         When(transportation_status=TRANSPORTATION_STATUS_IN_TRANSIT, then=Value(OUTWARD)),
    #                         When(transportation_status=TRANSPORTATION_STATUS_RECEIVED, then=Value(INWARD)),
    #                         default=Value(PENDING),
    #                         output_field=CharField()
    #                     )
    #                 )

    #     if not _cycle:
    #         return

    #     cs  = ['event_date', 'hub_name', 'product','quantity', 'status']
    #     cycle_df = pd.DataFrame.from_records(_cycle.values(), columns=cs)
    #     if cycle_df.empty:
    #         return

    #     dfinal1 = pd.pivot_table(cycle_df,
    #             index=['event_date'],columns=['status'],values=['quantity'],
    #             fill_value=0, aggfunc=np.sum, margins=True, margins_name='Total'
    #             ).reset_index(level=0,col_level=1).rename(columns={
    #                 'event_date' : 'Day',
    #                 'inward': 'In-Ward Scan',
    #                 'outward': 'Out-Ward Scan',
    #                 'pending': 'Pending Scan',
    #                 'Total': 'Total',
    #                 }
    #             )

    #     dfinal1.columns = dfinal1.columns.droplevel(0)
    #     dfinal1.rename_axis(None, axis=1, inplace=True)
    #     # print(dfinal1)

    #     dfinal2 = pd.pivot_table(cycle_df,
    #             index=['hub_name'],columns=['status'],values=['quantity'],
    #             fill_value=0, aggfunc=np.sum, margins=True, margins_name='Total'
    #             ).reset_index(level=0,col_level=1).rename(columns={
    #                 'inward': 'In-Ward Scan',
    #                 'outward': 'Out-Ward Scan',
    #                 'pending': 'Pending Scan',
    #                 'Total': 'Total',
    #                 'hub_name': 'Hospital/DHO'
    #                 }
    #             )
    #     dfinal2.columns = dfinal2.columns.droplevel(0)
    #     dfinal2.rename_axis(None, axis=1, inplace=True)
    #     # print(dfinal2)

    #     dfinal3 = pd.pivot_table(cycle_df,
    #             index=['product'],columns=['status'],values=['quantity'],
    #             fill_value=0, aggfunc=np.sum, margins=True, margins_name='Total'
    #             ).reset_index(col_level=1).rename(columns={
    #                 'inward': 'In-Ward Scan',
    #                 'outward': 'Out-Ward Scan',
    #                 'pending': 'Pending Scan',
    #                 'Total': 'Total',
    #                 'hub_name': 'Hospital/DHO',
    #                 'product': 'Category'
    #                 }
    #             )
    #     dfinal3.columns = dfinal3.columns.droplevel(0)
    #     dfinal3.rename_axis(None, axis=1, inplace=True)
    #     # print(dfinal3)

    #     # df4 = df[df.status == PENDING]
    #     # if not df4.empty:
    #     #     dfinal4 = pd.pivot_table(df[df.status == PENDING],
    #     #             index=['hub_name'],columns=['product'],values=['quantity'],
    #     #             fill_value=0, aggfunc=np.sum, margins=True, margins_name='Total'
    #     #             ).reset_index(level=0,col_level=1).rename(columns={
    #     #                 'hub_name': 'Location',
    #     #                 'Total': 'Total',
    #     #                 }
    #     #             )
    #     #     dfinal4.columns = dfinal4.columns.droplevel(0)
    #     #     dfinal4.rename_axis(None, axis=1, inplace=True)
    #     # print(dfinal4)

    #     df5 = cycle_df[cycle_df.status == OUTWARD]
    #     if not df5.empty:
    #         dfinal5 = pd.pivot_table(df5,
    #                 index=['hub_name'],columns=['product'],values=['quantity'],
    #                 fill_value=0, aggfunc=np.sum, margins=True, margins_name='Total'
    #                 ).reset_index(level=0,col_level=1).rename(columns={
    #                     'hub_name': 'Hospital/DHO',
    #                     'Total': 'Outward Scan Total',
    #                     }
    #                 )
    #         dfinal5.columns = dfinal5.columns.droplevel(0)
    #         dfinal5.rename_axis(None, axis=1, inplace=True)

    #     writer = pd.ExcelWriter(self.filename_prefix, engine='xlsxwriter')
    #     dfinal1.to_excel(writer, sheet_name='DayWise Summay')
    #     dfinal3.to_excel(writer, sheet_name='Warehouse Inventory')
        
    #     if not df5.empty:
    #         dfinal5.to_excel(writer, sheet_name='LocationWise-PostScan')
        
    #     dfinal2.to_excel(writer, sheet_name='Warehouse Summary')

    #     writer.save()
    #     self.write_to_xlsx_file()

    def get_filename(self, event, ext):
        """
        :return: Filename and path with given prefix and timestamp
        """
        _time = utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT)
        name = '%s_%s.%s' % (os.path.splitext(self.filename_prefix)[0],
                    uuid.uuid4().hex, ext)
        _path = f"media/exports/shipments/{event}/{_time}/{name}"
        return name, _path

    def save_exported_file(self, bufferio, export_filename):
        if not self.email_data:
            return

        _user_email = None
        if 'user_email' in self.email_data:
            _user_email = self.email_data.get('user_email')
            del self.email_data['user_email']
        
        report = ContentFile(bufferio.getvalue().encode('utf-8'))
        user_report = UserReport(**self.email_data)
        user_report.file.save(export_filename, report)
        user_report.save()

        period = self.email_data.get('period', None)
        if period == 'Adhoc':
            from blowhorn.report.services.scheduled_reports import ReportService
            ReportService().send_export_request_reports(user_report, _user_email)

    def write_to_xlsx_file(self):
        """
        Writes data to excel file or saves the data in file for reporting
        :return: excel file in HttpResponse
        """
        export_filename = '%s_%s.xlsx' % (
            os.path.splitext(self.filename_prefix)[0],
            utc_to_ist(timezone.now()).strftime(settings.FILE_TIMESTAMP_FORMAT))

        if self.email_data and os.path.exists(self.filename_prefix):
            with open(self.filename_prefix, 'rb') as excel:
                user_report = UserReport(**self.email_data)
                user_report.file.save(export_filename,File(excel))
                user_report.save()

        if os.path.exists(self.filename_prefix):
            os.remove(self.filename_prefix)

    def write_to_excel_file(self, bufferio, event_name, ext='csv'):
        """
        Writes data to excel file or saves the data in file for reporting
        :return: excel file in HttpResponse
        """
        export_filename, export_path = self.get_filename(event_name, ext)
        bufferio.seek(0)
        s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                        Body=bufferio.getvalue(), Key=export_path)

        if self.email_data:
            self.save_exported_file(bufferio, export_filename)

        else:
            response = HttpResponse(
                        bufferio, content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = \
                'inline; filename=%s' % os.path.basename(export_filename)
            return 200, response
