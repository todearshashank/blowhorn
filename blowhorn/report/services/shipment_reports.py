import pandas as pd
import os
import copy
from datetime import datetime, timedelta
from django.utils import timezone
from django.conf import settings
from django.db.models import (Q, Max, Exists,
    ExpressionWrapper, F, Sum, CharField, Subquery, IntegerField,
    Value, OuterRef, Func, Count, Case, When, DurationField, FloatField)
from django.db.models.functions import (TruncDate,
        ExtractMonth, ExtractWeek, ExtractHour)

from blowhorn.oscar.core.loading import get_model

from config.settings import status_pipelines as StatusPipeline
from blowhorn.report.constants import (time_slot_0_6,
    time_slot_6_12, time_slot_12_18, time_slot_18_24, delivered_order_status,
    unable_to_deliver_status, returned_status, attempted_status_list,
    rejected_status_list, out_for_delivery_status_list, out_for_pickup_status_list,
)

Order = get_model('order', 'Order')
Event = get_model('order', 'Event')
Slots = get_model('address', 'Slots')
OrderDispatch = get_model('order', 'OrderDispatch')
ShipmentAlertConfiguration = get_model('customer','ShipmentAlertConfiguration')

class ShipmentReport(object):

    def __init__(self, customer, data):
        # Fetch the required params
        self.customer = customer
        self.city_id = data.get('city_id', None)
        self.hub_id = data.get('hub_id', None)
        self.start = data.get('start', None)
        self.end = data.get('end', None)
        self.quick_filter = data.get('quick_filter', 0)
        self.order_type = data.get('order_type', settings.SHIPMENT_ORDER_TYPE)
        self.has_pickup = data.get('has_pickup', True)
        self.status = data.get('status', None)

    def get_query_set(self):
        qs = {
            'customer' : self.customer,
            'order_type' : settings.SHIPMENT_ORDER_TYPE
        }

        if self.city_id:
            qs['city__id'] = self.city_id

        if self.hub_id:
            qs['hub__id'] = self.hub_id

        if self.start and self.end:
            datetime_start = datetime.strptime(self.start, '%Y-%m-%d')
            datetime_end = datetime.strptime(self.end, '%Y-%m-%d')

            if datetime_start == datetime_end:
                datetime_end += timedelta(days=1)

        else:
            datetime_end = timezone.now().date()
            datetime_start = datetime_end - timedelta(days=int(self.quick_filter))

        qs['date_placed__date__gte'] = datetime_start
        qs['date_placed__date__lt'] = datetime_end

        self.start_date = datetime_start
        self.end_date = datetime_end
        self.get_order_trend()

        return qs

    def get_order_qs(self, status=None):
        qs = self.get_query_set()
        if status:
            qs['status'] = status

        if 'RDS_READ_REP_1' in os.environ:
            order_qs = Order.copy_data.using('readrep1').filter(**qs).select_related(
                        'customer', 'customer__user', 'city', 'hub'
                    ).prefetch_related(
                        'event_set'
                    ).exclude(
                        status=StatusPipeline.DUMMY_STATUS
                    )
        else:
            order_qs = Order.copy_data.filter(**qs).select_related(
                        'customer', 'customer__user', 'city', 'hub'
                    ).prefetch_related(
                        'event_set'
                    ).exclude(
                        status=StatusPipeline.DUMMY_STATUS
                    )

        return order_qs

    def get_order_trend(self):
        self.scale = ExtractHour
        self.trend_name = 'Hours'
        frequency = 'H'
        func_name = pd.date_range
        range_start = self.start_date
        range_end = self.end_date
        difference = self.end_date - self.start_date

        if difference.days > 31:
            self.scale = ExtractMonth
            frequency = 'M'
            self.trend_name = 'Months'
            start = self.start_date - timedelta(days=30)
            end = self.end_date + timedelta(days=30)
            rstart = datetime.strftime(start, '%Y-%m-%d')
            rend = datetime.strftime(end, '%Y-%m-%d')
            range_start = datetime.strptime(rstart, '%Y-%m-%d')
            range_end = datetime.strptime(rend, '%Y-%m-%d')

        elif difference.days > 7:
            self.scale = ExtractWeek
            self.trend_name = 'Weeks'
            frequency = 'W'
            func_name = pd.interval_range
            start = self.start_date - timedelta(weeks=1)
            end = self.end_date + timedelta(weeks=1)
            rstart = datetime.strftime(start, '%Y-%m-%d')
            rend = datetime.strftime(end, '%Y-%m-%d')
            range_start = datetime.strptime(rstart, '%Y-%m-%d')
            range_end = datetime.strptime(rend, '%Y-%m-%d')

        elif difference.days > 1:
            self.scale = TruncDate
            self.trend_name = 'Days'
            frequency = 'D'

        self.daterange = func_name(start=range_start, end=range_end,
                                    freq=frequency)

    def order_status(self):
        '''
            Display: Pie Chart
            Group By: Order Status
        '''
        qs = self.get_order_qs()
        total_count = 0
        data = []
        new_count, delivered_count, unable_to_deliver_count, returned_count = 0,0,0,0
        rejected_count, delivery_out_count, pickup_out_count = 0,0,0

        orders = qs.values('status').annotate(order_count=Count('id'))

        for order in orders:
            status = order.get('status')
            count = order.get('order_count')

            total_count += count
            if status in delivered_order_status:
                delivered_count += count
            elif status in unable_to_deliver_status:
                unable_to_deliver_count += count
            elif status in returned_status:
                returned_count += count
            elif status in rejected_status_list:
                rejected_count += count
            elif status in out_for_delivery_status_list:
                delivery_out_count += count
            elif status in out_for_pickup_status_list:
                pickup_out_count += count
            else:
                new_count += count

        if total_count:
            data = [
                {'status' : 'New Order' , 'count' : new_count},
                {'status' : 'Delivered' , 'count' : delivered_count},
                {'status' : 'Unable To Deliver' , 'count' : unable_to_deliver_count},
                {'status' : 'Returned' , 'count' : returned_count},
                {'status' : 'Rejected' , 'count' : rejected_count},
                {'status' : 'Out for Delivery' , 'count' : delivery_out_count},
                {'status' : 'Out for Pickup' , 'count' : pickup_out_count},
            ]

        result = {
            'header' : [{'status' : 'Status', 'count' : 'Count'}],
            'data' : data,
            'footer' : [],
            'trend' : self.trend_name
        }


        return result

    def default_order_delivery_performance(self):
        '''
            Display: Table
            Group By: Order Placed Day Slot - 0-6,6-12,12-18,18-24
        '''
        qs = self.get_order_qs(status=StatusPipeline.ORDER_DELIVERED)
        order_pks = qs.values_list('pk', flat=True)

        if 'RDS_READ_REP_1' in os.environ:
            orders = Event.objects.using('readrep1').filter(
                            order_id__in=order_pks,
                            status=StatusPipeline.ORDER_DELIVERED).annotate(
                            trend=self.scale('time'), hour_category=Case(
                                When(time__hour__in=range(18,24), then=Value(time_slot_18_24)),
                                When(time__hour__in=range(12,18), then=Value(time_slot_12_18)),
                                When(time__hour__in=range(6,12), then=Value(time_slot_6_12)),
                                default=Value(time_slot_0_6),
                                output_field=CharField())
                            ).values('hour_category','trend'
                            ).annotate(count=Count('id'))
        else:
            orders = Event.objects.filter(order_id__in=order_pks,
                            status=StatusPipeline.ORDER_DELIVERED).annotate(
                            trend=self.scale('time'), hour_category=Case(
                                When(time__hour__in=range(18,24), then=Value(time_slot_18_24)),
                                When(time__hour__in=range(12,18), then=Value(time_slot_12_18)),
                                When(time__hour__in=range(6,12), then=Value(time_slot_6_12)),
                                default=Value(time_slot_0_6),
                                output_field=CharField())
                            ).values('hour_category','trend'
                            ).annotate(count=Count('id'))


        header,footer = {}, {}
        if orders:
            header = {'trend' : self.trend_name,
                        'ts_0_6_hr' : '0-6 hours',
                        'ts_6_12_hr' : '6-12 hours',
                        'ts_12_18_hr' : '12-18 hours',
                        'ts_18_24_hr' : '18-24 hours'}

        data_dict = {}
        ts_0_6_count,ts_6_12_count,ts_12_18_count,ts_18_24_count=0,0,0,0


        for event in orders:
            trend = event['trend']
            hour_category = event['hour_category']
            count = event['count']

            if trend not in data_dict:
                temp = {
                    'trend' : trend,
                    'ts_0_6_hr' : 0,
                    'ts_6_12_hr' : 0,
                    'ts_12_18_hr' : 0,
                    'ts_18_24_hr' : 0
                }
                data_dict[trend] = temp

            temp_dict = data_dict[trend]

            if hour_category == time_slot_0_6:
                temp_dict['ts_0_6_hr'] = count
                ts_0_6_count += count

            elif hour_category == time_slot_6_12:
                temp_dict['ts_6_12_hr'] = count
                ts_6_12_count += count

            elif hour_category == time_slot_12_18:
                temp_dict['ts_12_18_hr'] = count
                ts_12_18_count += count

            else:
                temp_dict['ts_18_24_hr'] = count
                ts_18_24_count += count

            data_dict[trend] = temp_dict

        data = []
        if orders:
            ts_0_6_count,ts_6_12_count,ts_12_18_count,ts_18_24_count=0,0,0,0

            for elem in self.daterange:
                if self.trend_name == 'Hours':
                    time_format1 = datetime.strftime(elem,'%H')
                    time_format2 = int(time_format1)

                elif self.trend_name == 'Days':
                    time_format1 = datetime.strftime(elem,'%d %b %y')
                    time_format2 = elem.date()

                elif self.trend_name == 'Weeks':
                    time_format1 = datetime.strftime(elem.left,'%d %b') + \
                                    '-' + datetime.strftime(elem.right,'%d %b')

                    time_format2 = int(datetime.strftime(elem.left,'%U'))

                elif self.trend_name == 'Months':
                    time_format1 = datetime.strftime(elem,'%b-%y')
                    time_format2 = int(datetime.strftime(elem,'%m'))

                temp = data_dict.get(time_format2,{})
                ts_0_6_count += temp.get('ts_0_6_hr',0)
                ts_6_12_count += temp.get('ts_6_12_hr',0)
                ts_12_18_count += temp.get('ts_12_18_hr',0)
                ts_18_24_count += temp.get('ts_18_24_hr',0)

                r = {
                    'trend' : time_format1,
                    'ts_0_6_hr' : temp.get('ts_0_6_hr',0),
                    'ts_6_12_hr' : temp.get('ts_6_12_hr',0),
                    'ts_12_18_hr' : temp.get('ts_12_18_hr',0),
                    'ts_18_24_hr' : temp.get('ts_18_24_hr',0)
                }
                data.append(r)

            footer = {
                'trend' : 'GRAND TOTAL',
                'ts_0_6_hr' : ts_0_6_count,
                'ts_6_12_hr' : ts_6_12_count,
                'ts_12_18_hr' : ts_12_18_count,
                'ts_18_24_hr' : ts_18_24_count
            }

        result = {
            'header' : header,
            'data' : data,
            'footer' : footer,
            'trend' : self.trend_name
        }
        return result

    def config_order_delivery_performance(self, alert_config):
        '''
            Display: Table
            Group By: Order Placed Day Slot - 0-6,6-12,12-18,18-24
        '''
        qs = self.get_order_qs(status=StatusPipeline.ORDER_DELIVERED)
        order_pks = qs.values_list('pk', flat=True)

        whens = []
        intervals = {}
        t = 0
        for i,limit in enumerate(alert_config):
            val = limit['time_since_creation']
            if i == 0:
                key = (0,val)
                value = When(Q(delta__gte=timedelta(minutes=0)) & Q(delta__lt=timedelta(minutes=val)), then=Value(i))
            else:
                key = (t,val)
                value = When(Q(delta__gte=timedelta(minutes=t)) & Q (delta__lt=timedelta(minutes=val)), then=Value(i))
            t = val
            intervals[i] = key
            whens.append(value)

        if alert_config:
            key = (t,"max")
            value = value = When(delta__gte=timedelta(minutes=t), then=Value(i))
            i += 1
            intervals[i] = key
            whens.append(value)

        if 'RDS_READ_REP_1' in os.environ:
            orders = Event.objects.using('readrep1').filter(
                            order_id__in=order_pks,
                            status=StatusPipeline.ORDER_DELIVERED).annotate(
                            trend=self.scale('time'), delta=ExpressionWrapper(Func(F('time'),
                            F('order__date_placed'),function='age'),output_field=DurationField()),
                            ).annotate(category=Case(
                                *whens, output_field=IntegerField())).values('trend','category'
                            ).annotate(count=Count('pk'))
        else:
            orders = Event.objects.filter(
                            order_id__in=order_pks,
                            status=StatusPipeline.ORDER_DELIVERED).annotate(
                            trend=self.scale('time'), delta=ExpressionWrapper(Func(F('time'),
                            F('order__date_placed'),function='age'),output_field=DurationField()),
                            ).annotate(category=Case(
                                *whens, output_field=IntegerField())).values('trend','category'
                            ).annotate(count=Count('pk'))


        header,footer = {},{}
        length = len(alert_config)
        if orders:
            header['trend'] = self.trend_name
            for i in range(length+1):
                key = "ts_{0}_{1}_min".format(*intervals[i])
                header[key] = self.label_time_format(*intervals[i])

        data_dict = {}
        initial_dict = {}
        for i in range(length+1):
            key = "ts_{0}_{1}_min".format(*intervals[i])
            initial_dict[key] = 0

        footer_dict = copy.deepcopy(initial_dict)
        for event in orders:
            trend = event['trend']
            category = event['category']
            count = event['count']
            if category == None:
                continue

            if trend not in data_dict:
                temp = copy.deepcopy(initial_dict)
                temp['trend'] = trend
                data_dict[trend] = temp

            temp_dict = data_dict[trend]
            key = "ts_{0}_{1}_min".format(*intervals[category])
            temp_dict[key] = count
            data_dict[trend] = temp_dict


        data = []
        if orders:
            for elem in self.daterange:
                if self.trend_name == 'Hours':
                    time_format1 = datetime.strftime(elem,'%H')
                    time_format2 = int(time_format1)

                elif self.trend_name == 'Days':
                    time_format1 = datetime.strftime(elem,'%d %b %y')
                    time_format2 = elem.date()

                elif self.trend_name == 'Weeks':
                    time_format1 = datetime.strftime(elem.left,'%d %b') + \
                                    '-' + datetime.strftime(elem.right,'%d %b')

                    time_format2 = int(datetime.strftime(elem.left,'%U'))

                elif self.trend_name == 'Months':
                    time_format1 = datetime.strftime(elem,'%b-%y')
                    time_format2 = int(datetime.strftime(elem,'%m'))

                temp = data_dict.get(time_format2,{})
                if not temp:
                    continue

                temp['trend'] = time_format1
                data.append(temp)

                for k,v in temp.items():
                    if k != 'trend':
                        footer_dict[k] += int(v)

            if footer_dict:
                footer_dict['trend'] = 'GRAND TOTAL'

        result = {
            'header' : header,
            'data' : data,
            'footer' : footer_dict,
            'trend' : self.trend_name
        }

        return result

    def order_delivery_performance(self):
        alert_config = ShipmentAlertConfiguration.objects.filter(
                            customer=self.customer).order_by(
                            'time_since_creation').values('time_since_creation')

        if alert_config:
            result = self.config_order_delivery_performance(alert_config)

        else:
            result = self.default_order_delivery_performance()

        return result

    def order_dateplaced_trend(self):
        '''
            Display: Bar Chart
            Group By: Order Placed Trend - Days, Week, Month
        '''
        qs = self.get_order_qs()
        orders = qs.annotate(trend=self.scale('date_placed')).values(
                        'trend').annotate(count=Count('id'))

        order_date_placed_label = []
        order_date_placed_data = [0] * len(self.daterange)

        _index = {}
        i = 0
        for elem in self.daterange:
            if self.trend_name == 'Hours':
                time_format1 = datetime.strftime(elem,'%H')
                time_format2 = int(time_format1)

            elif self.trend_name == 'Days':
                time_format1 = datetime.strftime(elem,'%d %b %y')
                time_format2 = elem.date()

            elif self.trend_name == 'Weeks':
                time_format1 = datetime.strftime(elem.left,'%d %b') + \
                                    '-' + datetime.strftime(elem.right,'%d %b')
                time_format2 = int(datetime.strftime(elem.left,'%U'))

            elif self.trend_name == 'Months':
                time_format1 = datetime.strftime(elem,'%b-%y')
                time_format2 = int(datetime.strftime(elem,'%m'))

            _index[time_format2] = i
            order_date_placed_label.append(time_format1)
            i += 1

        data = []
        for order in orders:
            trend = order['trend']

            if trend in _index:
                idx = _index.get(trend)
                order_date_placed_data[idx] = order['count']
                data.append({'trend' : order_date_placed_label[idx], 'count' : order['count']})

        result = {
            'header' : [{'trend' : self.trend_name, 'count' : 'Count'}],
            'data' : data,
            'footer' : [],
            'trend' : self.trend_name
        }

        return result

    def order_time_placed(self):
        '''
            Display: Area Chart
            Group By: Order Placed Time
        '''
        qs = self.get_order_qs()
        c1, c2, c3, c4, c5, c6 = 0, 0, 0, 0, 0, 0
        orders = qs.values('date_placed__hour').annotate(count=Count('pk'))

        for order in orders:
            order_hours = order['date_placed__hour']
            count = order['count']

            if order_hours in range(0,4):
                c1 += count
            elif order_hours in range(4,8):
                c2 += count
            elif order_hours in range(8,12):
                c3 += count
            elif order_hours in range(12,16):
                c4 += count
            elif order_hours in range(16,20):
                c5 += count
            elif order_hours in range(20,24):
                c6 += count

        data = []
        if orders:
            data = [
                {'trend' : '12AM - 4AM', 'count': c1},
                {'trend' : '4AM - 8AM', 'count': c2},
                {'trend' : '8AM - 12PM', 'count': c3},
                {'trend' : '12PM - 4PM', 'count':  c4},
                {'trend' : '4PM - 8PM', 'count': c5},
                {'trend' : '8PM - 12AM', 'count' : c6},
            ]

        result = {
            'header' : [{'trend' : self.trend_name, 'count' : 'Count'}],
            'data' : data,
            'footer' : [],
            'trend' : self.trend_name
        }

        return result

    def order_payment_mode(self):
        '''
            Display: Bar Chart
            Group By: Payment Mode
        '''
        qs = self.get_order_qs()
        _orders = qs.filter(cash_on_delivery__gt=0)
        _total_cod_order_count = _orders.count()
        _cod = _orders.aggregate(total_money=Sum('cash_on_delivery'))
        
        result = {
            'header' : [{'payment_mode' : 'Payment Mode', 'count' : 'Count', 'total_money' : 'Total Amount'}],
            'total_orders' : qs.count(),
            'total_cod' : _cod.get('total_money', 0) or 0,
            'total_cod_orders' : _total_cod_order_count,
            'footer' : [],
            'trend' : self.trend_name
        }

        return result

    def order_attempts(self):
        '''
            Display: Bar Chart
            Group By: Number of Attempts
        '''
        qs = self.get_order_qs()
        order_pks = qs.values_list('pk', flat=True)

        if 'RDS_READ_REP_1' in os.environ:
            attempted_events = Event.objects.using('readrep1').filter(
                                order_id__in=order_pks,
                                status__in=attempted_status_list).values(
                                'order').annotate(attempts=Count('id'))
        else:
            attempted_events = Event.objects.filter(order_id__in=order_pks,
                                status__in=attempted_status_list).values(
                                'order').annotate(attempts=Count('id'))

        attempt_1_count, attempt_2_count, attempt_3_count = 0,0,0
        for event in attempted_events:
            attempt_count = event['attempts']
            if attempt_count == 1:
                attempt_1_count += 1
            elif attempt_count == 2:
                attempt_2_count += 1
            else:
                attempt_3_count += 1

        data = []
        if attempt_1_count != 0 or attempt_2_count != 0 or attempt_3_count != 0:
            data = [{'attempt' : 'Attempt 1', 'count' : attempt_1_count},
                    {'attempt' : 'Attempt 2', 'count' : attempt_2_count},
                    {'attempt' : 'Attempt 3', 'count' : attempt_3_count}
                ]

        result = {
            'header' : [{'attempt' : 'Attempt', 'count' : 'Count'}],
            'data' : data,
            'footer' : [],
            'trend' : self.trend_name
        }

        return result

    def label_time_format(self, start, end):
        limit_format = "({0}-{1} {2})"
        fstart = start
        fend = end
        unit = 'mins'

        if start > 60 or (end != 'max' and end > 60):
            unit = 'hours'
            if start not in [0,'max']:
                fstart = int(int(start)/60)
            if end not in [0,'max']:
                fend = int(int(end)/60)

        limit_string = limit_format.format(fstart, fend, unit)
        return limit_string

    def order_sla(self):
        '''
            Display: Chart
            Percentage of orders delivery on sameday
        '''
        data = []

        qs = self.get_order_qs()
        alert_config = ShipmentAlertConfiguration.objects.filter(
                            customer=self.customer).order_by('time_since_creation'
                            ).values('time_since_creation','critical_status')

        del_pks = qs.filter(status=StatusPipeline.ORDER_DELIVERED).values_list('pk', flat=True)
        undel_pks = qs.filter(~Q(status=StatusPipeline.ORDER_DELIVERED)).values_list('pk', flat=True)

        if 'RDS_READ_REP_1' in os.environ:
            new_events = Event.objects.using('readrep1').filter(status='New-Order',order_id__in=undel_pks)
            delivered_events = Event.objects.using('readrep1').filter(status='Delivered',order_id__in=del_pks)

        else:
            new_events = Event.objects.filter(status='New-Order',order_id__in=undel_pks)
            delivered_events = Event.objects.filter(status='Delivered',order_id__in=del_pks)

        delivered_orders = delivered_events.annotate(
                            delta=ExpressionWrapper(Func(F('time'),
                            F('order__date_placed'),function='age'),
                            output_field=DurationField())
                        ).values_list('delta', flat=True)

        now = timezone.now()
        undelivered_orders = new_events.annotate(
                            delta=ExpressionWrapper(Func(now,F('order__date_placed'),
                            function='age'),
                            output_field=DurationField())
                        ).values_list('delta', flat=True)

        orders_events = delivered_orders.union(undelivered_orders)

        i,t = 0, 0

        if orders_events:
            for config in alert_config:
                temp = {}
                limit = config.get('time_since_creation')

                if i == 0:
                    dd = [e for e in orders_events if e < timedelta(minutes=limit)]
                    limit_string = self.label_time_format(0,limit)
                else:
                    dd = [e for e in orders_events if e >= timedelta(minutes=t)
                                        and e < timedelta(minutes=limit)]
                    limit_string = self.label_time_format(t,limit)

                temp['limit'] = limit_string
                temp['status'] = config.get('critical_status').title()
                temp['count'] = len(dd)
                data.append(temp)
                i += 1
                t = limit


            # if alert_config:
            #     temp = {}
            #     dd = [e for e in orders_events if e >= timedelta(minutes=t)]
            #     limit_string = limit_format.format(t, "max", "mins")
            #     temp['limit'] = limit_string
            #     temp['status'] = config.get('critical_status')
            #     temp['count'] = len(dd)
            #     data.append(temp)

            limit_format = "({0}-{1} {2})"
            if not data:
                temp = {}
                dd = [e for e in orders_events if e < timedelta(hours=24)]
                temp['status'] = 'On Schedule'
                temp['limit'] = limit_format.format(0, 24, "hours")
                temp['count'] = len(dd)
                data.append(temp)

                temp= {}
                dd = [e for e in orders_events if e >= timedelta(hours=24)]
                temp['limit'] = limit_format.format(24, "max", "hours")
                temp['status'] = 'Failure'
                temp['count'] = len(dd)
                data.append(temp)

        result = {
            'header' : [{'status' : 'Status', 'count' : 'Count'}],
            'data' : data,
            'footer' : [],
            'trend' : self.trend_name
        }

        return result

    def order_dispatch_summary(self):
        '''
            Display: Table
            Group By: Order Dispatch Reports
        '''
        data = []
        header = []
        footer = []

        result = {
            'header' : header,
            'data' : data,
            'footer' : footer
        }

        if 'RDS_READ_REP_1' in os.environ:
            slots = Slots.objects.using('readrep1').filter(customer=self.customer).values('contract_id','hub_id')
        else:
            slots = Slots.objects.filter(customer=self.customer).values('contract_id','hub_id')

        if not slots:
            return result

        # contracts = [e['contract_id'] for e in slots]
        # hubs = [e['hub_id'] for e in slots]

        qs = self.get_query_set()
        qs['status'] = StatusPipeline.ORDER_DELIVERED
        qs['source'] = 'api'

        if 'RDS_READ_REP_1' in os.environ:
            accepted_sq = OrderDispatch.objects.using('readrep1').filter(
                                order_id=OuterRef('pk'),status='Accepted')

            orders = Order.copy_data.using('readrep1').filter(**qs).annotate(
                                driver_auto_assigned = Exists(accepted_sq)
                                ).values('hub_id', 'driver_auto_assigned'
                                ).annotate(count=Count('id')).values('hub_id','hub__name','city__name',
                                            'driver_auto_assigned','count')

        else:
            accepted_sq = OrderDispatch.objects.filter(
                                order_id=OuterRef('pk'),status='Accepted')

            orders = Order.copy_data.filter(**qs).annotate(
                                driver_auto_assigned = Exists(accepted_sq)
                                ).values('hub_id', 'driver_auto_assigned'
                                ).annotate(count=Count('id')).values('hub_id','hub__name','city__name',
                                            'driver_auto_assigned','count')

        temp = {}
        for order in orders:
            hub_id = order['hub_id']
            if hub_id not in temp:
                temp[hub_id] = {
                                'hub_name' : order['hub__name'],
                                'city_name' : order['city__name'],
                                'auto_dispatch_success' : 0,
                                'auto_dispatch_failure' : 0,
                                'total_count' : 0,
                                }

            count = int(order['count'])
            dispatch_status = order['driver_auto_assigned']
            if dispatch_status:
                temp[hub_id]['auto_dispatch_success'] += count
            else:
                temp[hub_id]['auto_dispatch_failure'] += count

            temp[hub_id]['total_count'] += count

        if temp:
            data = list(temp.values())
            header = {
                'hub_name': 'Name of Hub',
                'city_name': 'City Name',
                'auto_dispatch_success': 'Auto Dispatched (AD)',
                'auto_dispatch_success_per': 'AD %',
                'auto_dispatch_failure': 'Manual Dispatched (MD)',
                'auto_dispatch_failure_per': 'MD %',
                'total_count': 'Total Order Count',
            }

            footer = {
                'hub_name': 'GRAND TOTAL',
                'city_name': '',
                'auto_dispatch_success': 0,
                'auto_dispatch_success_per': 0,
                'auto_dispatch_failure': 0,
                'auto_dispatch_failure_per': 0,
                'total_count': 0,
            }

        result = {
            'header' : header,
            'data' : data,
            'footer' : footer
        }

        return result

    def get_sla_qs(self, is_pickup_order_flag=None):
        qs = {
            'order__customer' : self.customer,
            'order__order_type' : settings.SHIPMENT_ORDER_TYPE,
            'status' : StatusPipeline.ORDER_DELIVERED
        }

        if is_pickup_order_flag != None:
            qs['order__has_pickup'] = is_pickup_order_flag
        else:
            qs['order__has_pickup'] = None

        if self.city_id:
            qs['order__city__id'] = self.city_id

        if self.hub_id:
            qs['order__hub__id'] = self.hub_id

        if self.start and self.end:
            datetime_start = datetime.strptime(self.start, '%Y-%m-%d')
            datetime_end = datetime.strptime(self.end, '%Y-%m-%d')

            if datetime_start == datetime_end:
                datetime_end += timedelta(days=1)

        else:
            datetime_end = timezone.now().date()
            datetime_start = datetime_end - timedelta(days=int(self.quick_filter))

        qs['time__date__gte'] = datetime_start
        qs['time__date__lt'] = datetime_end

        self.start_date, self.end_date = datetime_start, datetime_end
        self.get_order_trend()
        # print(qs)
        if 'RDS_READ_REP_1' in os.environ:
            order_qs = Event.objects.using('readrep1').filter(**qs).select_related('order')
        else:
            order_qs = Event.objects.filter(**qs).select_related('order')

        return order_qs

    def order_sameday_delivery(self):
        '''
            Display: Bar Chart
            Percentage of orders delivery on sla
        '''
        data = []
        header = []
        breach = {'hours' : 24}
        sla_string = ''

        max_minutes = ShipmentAlertConfiguration.objects.filter(
                            customer=self.customer).aggregate(Max('time_since_creation'))
        if max_minutes:
            max_min = max_minutes.get('time_since_creation__max',0)
            if max_min and max_min > 0:
                breach = {'minutes' : max_min}

        # for orders not having pickup
        qs = self.get_sla_qs(False)
        non_pickup_orders = qs.annotate(
                                delta=ExpressionWrapper(Func(F('time__date'),
                                F('order__date_placed__date'),function='age'),
                                output_field=DurationField()),
                                trend=self.scale('order__date_placed__date')
                                ).values('trend').annotate(
                                total_delivered_count=Count('pk'),
                                sameday_delivered_count=Count('pk',
                                filter=Q(delta__lte=timedelta(**breach)))
                            )

        # for orders having null pickup
        qs = self.get_sla_qs()
        null_pickup_orders = qs.annotate(
                                delta=ExpressionWrapper(Func(F('time__date'),
                                F('order__date_placed__date'),function='age'),
                                output_field=DurationField()),
                                trend=self.scale('order__date_placed__date')
                                ).values('trend').annotate(
                                total_delivered_count=Count('pk'),
                                sameday_delivered_count=Count('pk',
                                filter=Q(delta__lte=timedelta(**breach)))
                            )

        # for orders not having pickup
        qs = self.get_sla_qs(True)
        if 'RDS_READ_REP_1' in os.environ:
            sq = Event.objects.using('readrep1').filter(order=OuterRef('pk'),
                            status=StatusPipeline.ORDER_PICKED).order_by('-time')
        else:
            sq = Event.objects.filter(order=OuterRef('pk'),
                            status=StatusPipeline.ORDER_PICKED).order_by('-time')

        pickup_orders = qs.annotate(picked_time=Subquery(sq.values('time')[:1])).annotate(
                            delta=ExpressionWrapper(Func(F('time__date'),
                            F('picked_time__date'),function='age'),
                            output_field=DurationField()),
                            trend=self.scale('time__date')
                            ).values('trend').annotate(
                            total_delivered_count=Count('pk'),
                            sameday_delivered_count=Count('pk',
                            filter=Q(delta__lte=timedelta(**breach)))
                        )

        orders_present = False
        if non_pickup_orders or pickup_orders or null_pickup_orders:
            orders_present = True

        if orders_present:
            sla_string = str(list(breach.values())[0]) + " " +  list(breach.keys())[0]
            header = {  'trend' : self.trend_name,
                        'sdd' : 'SDD',
                        'ndd' : 'NDD',
                    }

            _index = {}
            i = 0
            for elem in self.daterange:
                if self.trend_name == 'Hours':
                    time_format1 = datetime.strftime(elem,'%H')
                    time_format2 = int(time_format1)

                elif self.trend_name == 'Days':
                    time_format1 = datetime.strftime(elem,'%d %b %y')
                    time_format2 = elem.date()

                elif self.trend_name == 'Weeks':
                    time_format1 = datetime.strftime(elem.left,'%d %b') + \
                                    '-' + datetime.strftime(elem.right,'%d %b')

                    time_format2 = int(datetime.strftime(elem.left,'%U'))

                elif self.trend_name == 'Months':
                    time_format1 = datetime.strftime(elem,'%b-%y')
                    time_format2 = int(datetime.strftime(elem,'%m'))

                _index[time_format2] = i
                r = {
                            'trend' : time_format1,
                            'total_delivered_count' : 0,
                            'sameday_delivered_count' : 0
                        }
                i += 1
                data.append(r)

            order_types = [pickup_orders, non_pickup_orders, null_pickup_orders]
            for orders in order_types:
                for order in orders:
                    trend = order['trend']
                    if trend in _index:
                        i = _index[trend]
                        temp = data[i]
                        temp['total_delivered_count'] += order['total_delivered_count']
                        temp['sameday_delivered_count'] += order['sameday_delivered_count']
                        data[i] = temp

        result = {
            'header' : [header,],
            'data' : data,
            'footer' : [],
            'trend' : self.trend_name,
            'sla' : sla_string
        }
        return result

    def get_order_location_data(self):
        qs = {
            'customer': self.customer,
            'order_type': self.order_type
        }
        qs['has_pickup'] = self.has_pickup

        if self.city_id:
            qs['city__id'] = self.city_id

        if self.hub_id:
            qs['pickup_hub_id'] = self.hub_id

        if self.status:
            qs['status'] = self.status

        if self.start and self.end:
            datetime_start = datetime.strptime(self.start, '%Y-%m-%d')
            datetime_end = datetime.strptime(self.end, '%Y-%m-%d')
            if datetime_start == datetime_end:
                datetime_end += timedelta(days=1)

        else:
            datetime_end = timezone.now().date()
            datetime_start = datetime_end - timedelta(days=1)

        qs['pickup_datetime__date__gte'] = datetime_start
        qs['pickup_datetime__date__lt'] = datetime_end

        if 'RDS_READ_REP_1' in os.environ:
            order_qs = Order.copy_data.using('readrep1').filter(**qs)
        else:
            order_qs = Order.copy_data.filter(**qs)

        order_qs = order_qs.select_related(
                'customer', 'customer__user', 'city', 'hub'
            ).prefetch_related(
                'event_set'
            ).exclude(
                status=StatusPipeline.DUMMY_STATUS
            )

        orders = order_qs.annotate(
            pickup_lat=ExpressionWrapper(
                Func('pickup_address__geopoint', function='st_y'),
                output_field=FloatField()),
            pickup_lng=ExpressionWrapper(
                Func('pickup_address__geopoint', function='st_x'),
                output_field=FloatField()),
            dropoff_lat=ExpressionWrapper(
                Func('shipping_address__geopoint', function='st_y'),
                output_field=FloatField()),
            dropoff_lng=ExpressionWrapper(
                Func('shipping_address__geopoint', function='st_x'),
                output_field=FloatField())
        ).values(
            'pickup_lat',
            'pickup_lng',
            'dropoff_lat',
            'dropoff_lng',
            'status',
            'city_id',
            'city__name',
            'pickup_hub_id',
            'pickup_hub__name',
        )
        return orders
