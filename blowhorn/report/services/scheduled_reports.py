import logging
import json
import openpyxl
import io
import requests
import pytz
import copy
import operator
from functools import reduce
from datetime import datetime, timedelta
from email import encoders
from email.mime.base import MIMEBase
import pandas as pd

from django.utils.timezone import make_aware
from django.utils import timezone
from django.conf import settings
from django.db.models import Q
from django.template import loader
from django.core.mail.message import EmailMessage

from blowhorn.oscar.core.loading import get_model
from blowhorn.utils.datetime_function import DateTime

from config.settings import status_pipelines as StatusPipeline
from blowhorn.utils.mail import Email
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_SHIPMENT, CONTRACT_STATUS_ACTIVE
from blowhorn.customer.constants import EXPORT_TYPE_ADHOC_REPORT_SUBJECT, EXPORT_TYPE_POD, \
    EXPORT_TYPE_MINIMAL, EXPORT_TYPE_WITH_EVENTS, \
    EXPORT_TYPE_ORDERLINE, EXPORT_TYPE_REPORT_SUBJECT, \
    EXPORT_TYPE_DAY_REPORT_SUBJECT, EXPORT_TYPE_MIS, \
    EXPORT_TYPE_MIS_SUMMARY, EXPORT_TYPE_NDR, AGGREGATION_LEVEL
from blowhorn.report.constants import REPORT_JSON_SETTING
from blowhorn.report.constants import (REPORT_TYPE_WMS_INVENTORY,
    REPORT_TYPE_WMS_INVENTORY_TXN, REPORT_TYPE_WMS_MIS)
from blowhorn.report.services import report_mapper as rmap


CustomerReportSchedule = get_model('customer','CustomerReportSchedule')
ReportType = get_model('customer','Report')
City = get_model('address','City')
Order = get_model('order', 'Order')
Contract = get_model('contract', 'Contract')
UserReport = get_model('customer','UserReport')
Customer = get_model('customer','Customer')


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

MAPPER_FUNCTION = {
    EXPORT_TYPE_MINIMAL: rmap.report_export_type_minimal,
    EXPORT_TYPE_POD : rmap.report_export_type_pod,
    EXPORT_TYPE_WITH_EVENTS : rmap.report_export_type_with_events,
    EXPORT_TYPE_ORDERLINE : rmap.report_export_type_orderline,
    EXPORT_TYPE_MIS_SUMMARY : rmap.report_export_type_mis_summary,
    EXPORT_TYPE_MIS : rmap.report_export_type_mis,
    EXPORT_TYPE_NDR : rmap.report_export_type_ndr,
    REPORT_TYPE_WMS_INVENTORY : rmap.report_export_type_inventory,
    REPORT_TYPE_WMS_INVENTORY_TXN : rmap.report_export_type_inventory_txn,
    # REPORT_TYPE_WMS_HUB_SUMMARY : rmap.report_export_type_hub_summary,
    # REPORT_TYPE_WMS_MIS : rmap.report_export_type_wmsmis
}

class ReportService(object):

    def get_start_end_date(self, delta, end_date):
        end_date = end_date
        start_date = None
        if delta == "Daily":
            start_date = end_date - timedelta(days=1)

        elif delta == "Weekly":
            start_date = end_date - timedelta(days=end_date.weekday())

        else:
            start_date = end_date - timedelta(days=end_date.day - 1)

        if (start_date == end_date):
            end_date = start_date + timedelta(hours=24)

        # add the timezone offset
        timezone_offset = DateTime.get_timezone_offset()
        if timezone_offset:
            start_date = start_date - timedelta(hours=timezone_offset)
            end_date = end_date - timedelta(hours=timezone_offset)

        return start_date, end_date

    def get_export_query(self, customer_id, params):
        quick_filter_date_delta = params.get('quick_filter', None) or 0
        _status = params.get('status', None)
        city_id = params.get('city_id', None)
        hub_id = params.get('hub_id', None)
        driver_id = params.get('driver_id', None)
        order_no = params.get('order_no', None)
        order_ref_nos = params.get('order_ref_nos', None)
        _start = params.get('start', None)
        _end = params.get('end', None)
        reference_number = params.get('reference_number', None)
        customer_reference_number = params.get(
            'customer_reference_number', None)

        if not _start and not _end:
            self.end = timezone.now().date()
            self.start = timezone.now().date() - timedelta(
                days=int(quick_filter_date_delta))
        else:
            self.start = datetime.strptime(_start,'%Y-%m-%d').date()
            self.end = datetime.strptime(_end,'%Y-%m-%d').date()

        self.customer = Customer.objects.filter(id=customer_id).first()
        query = [Q(customer=self.customer),
                 Q(order_type=settings.SHIPMENT_ORDER_TYPE)]
        if _status:
            statuses = _status.split(",")
            status_query = []
            if statuses:
                status_query.append(Q(status__in=statuses))
            query.append(reduce(operator.or_, status_query))
        if reference_number:
            query.append(Q(reference_number=reference_number))
        if customer_reference_number:
            query.append(
                Q(customer_reference_number=customer_reference_number))
        if city_id:
            query.append(Q(city_id=city_id))
        if hub_id:
            query.append(Q(hub_id=hub_id))
        if driver_id:
            query.append(Q(driver__id=driver_id))
        if order_no:
            query.append(Q(number=order_no))
        if self.start and self.end:
            query.append(
                Q(updated_time__date__gte=self.start)
                & Q(updated_time__date__lte=self.end)
                | (Q(created_date__date__gte=self.start)
                   & Q(created_date__date__lte=self.end))
                | (Q(expected_delivery_time__date__gte=self.start)
                   & Q(expected_delivery_time__date__lte=self.end))
            )
        if order_ref_nos:
            query.append(Q(number=order_ref_nos) |
                         Q(reference_number=order_ref_nos) |
                         Q(customer_reference_number=order_ref_nos))
        return reduce(operator.and_, query)

    def export_batch_request(self, customer_id, parms, user_email):
        _parms = json.loads(parms)
        _qs = self.get_export_query(customer_id, _parms)
        export_type = _parms.get('export_type', None)
        if not export_type:
            return

        report_type = ReportType.objects.filter(code=export_type).first()
        if not report_type:
            return

        print('Going for batch report generation')
        email_data = {
            'start_date' : self.start,
            'end_date' : self.end,
            'customer' : self.customer,
            'report_type' : report_type,
            'city' : None,
            'period' : "Adhoc",
            'user_email' : user_email
        }

        report_function = MAPPER_FUNCTION.get(report_type.code)
        report_function(_qs, email_data)

    def generate_subscribed_reports(self, ids=[]):
        if ids:
            schedule = CustomerReportSchedule.objects.filter(id__in=ids)
        else:
            schedule = CustomerReportSchedule.objects.all()

        localtz = pytz.timezone(settings.ACT_TIME_ZONE)
        td = timezone.now().astimezone(localtz)
        job_start = make_aware(datetime(td.year, td.month, td.day, 0, 0, 0))
        print('Job start', job_start)

        for rs in schedule:
            next_occurence = rs.service_schedule.after(job_start)
            if td.date() != next_occurence.date():
                print('exiting since current date not equal to next_occurence')
                continue

            print("---------------",rs.customer,rs.period,"---------------")
            datetime_start = job_start
            start, end = self.get_start_end_date(rs.period, datetime_start)
            print(start,end)

            queryset = self.get_eligible_orders(rs.customer, rs.city, rs.report_type.code,
                                                start, end)

            self.save_customer_report(rs, queryset, start, end)

    def get_eligible_orders(self, customer, city, report_type, start_time, end_time):
        queryset = None

        type_list_1 = [
                EXPORT_TYPE_MINIMAL, EXPORT_TYPE_WITH_EVENTS,
                EXPORT_TYPE_ORDERLINE, EXPORT_TYPE_POD
            ]

        type_list_2 = [
                EXPORT_TYPE_MIS_SUMMARY, EXPORT_TYPE_MIS,
            ]

        type_list_3 = [
                EXPORT_TYPE_NDR
            ]

        if report_type in type_list_1:
            qs_a = Q(customer=customer,
                        order_type=settings.SHIPMENT_ORDER_TYPE,
                        updated_time__gte=start_time,
                        updated_time__lt=end_time,
                        status__in=StatusPipeline.SHIPMENT_ORDER_STATUSES)

            qs_b = Q(customer=customer,
                        order_type=settings.SHIPMENT_ORDER_TYPE,
                        date_placed__gte=start_time,
                        date_placed__lt=end_time,
                        status__in=StatusPipeline.SHIPMENT_ORDER_STATUSES)

            if city:
                qs_a = qs_a & Q(city=city)
                qs_b = qs_b & Q(city=city)

            queryset = qs_a | qs_b

        elif report_type in type_list_2:
            qs_a = Q(customer=customer,
                            order_type=settings.SHIPMENT_ORDER_TYPE,
                            date_placed__gte=start_time,
                            date_placed__lt=end_time,
                            status__in=StatusPipeline.SHIPMENT_ORDER_STATUSES
                        )

            qs_b = Q(customer=customer,
                            order_type=settings.SHIPMENT_ORDER_TYPE,
                            updated_time__gte=start_time,
                            updated_time__lt=end_time,
                            status__in=StatusPipeline.SHIPMENT_ORDER_STATUSES
                        )

            if city:
                qs_a = qs_a & Q(city=city)
                qs_b = qs_b & Q(city=city)

            queryset = qs_a | qs_b

        elif report_type in type_list_3:
            valid_status = copy.deepcopy(StatusPipeline.SHIPMENT_ORDER_STATUSES)
            if StatusPipeline.ORDER_DELIVERED in valid_status:
                valid_status.pop(StatusPipeline.ORDER_DELIVERED)

            qs_a = Q(customer=customer,
                            order_type=settings.SHIPMENT_ORDER_TYPE,
                            date_placed__gte=start_time,
                            date_placed__lt=end_time,
                            status__in=valid_status
                        )

            qs_b = Q(customer=customer,
                            order_type=settings.SHIPMENT_ORDER_TYPE,
                            updated_time__gte=start_time,
                            updated_time__lt=end_time,
                            status__in=valid_status
                        )
            if city:
                qs_a = qs_a & Q(city=city)
                qs_b = qs_b & Q(city=city)

            queryset = qs_a | qs_b

        return queryset

    def save_customer_report(self, schedule, queryset, period_start, period_end):
        if schedule.report_type.system.code != 'WMS':
            if not queryset:
                return

            orders = Order.objects.filter(queryset)
            if not orders.exists():
                print('order data doesnt exist')
                return

            print(orders.count())
        
        print('Going for report generation')
        period_start = period_start + timedelta(days=1)
        period_end = period_end + timedelta(days=1)

        email_data = {
                    'start_date' : period_start.date(),
                    'end_date' : period_end.date(),
                    'customer' : schedule.customer,
                    'report_type' : schedule.report_type,
                    'city' : schedule.city,
                    'period' : schedule.period
        }

        report_function = MAPPER_FUNCTION.get(schedule.report_type.code)
        report_function(queryset, email_data)

    def download_file_and_send_mail(self, report, subject, recipients,
                                        table_data, table_style):

        email_template_name = 'emailtemplates_v2/scheduled_report.html'
        if not report.report_type.display_inline:
            _name = report.report_type.name.split()
            _mime = "octet-stream"
            if report.report_type.code == REPORT_TYPE_WMS_MIS:
                _mime = "vnd.ms-excel"
                _name.append('.xlsx')
            elif report.report_type.code == EXPORT_TYPE_POD:
                _mime = "application/zip"
                _name.append('.zip')
            else:
                _name.append('.csv')

            _filename = '_'.join(_name)
            report_file = requests.get(report.file.url)
            part = MIMEBase('application', _mime)
            part.set_payload(report_file.content)
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment', filename=_filename)

        if settings.ENABLE_SLACK_NOTIFICATIONS:
            recipients = Email()._Email__clean_recipients(recipients)
        else:
            recipients = settings.DEFAULT_TESTMAIL

        subjects = subject.split('|')
        name = subjects[0]
        period = subjects[1].split(' for ')[1]
        context = {
            "name" : name,
            "period" : period,
            "brand_name" : settings.BRAND_NAME,
            "file_url" : report.file.url
        }

        if report.report_type.display_inline:
            context["table_width"] = table_style
            context["tables"] = table_data

        html_content = loader.render_to_string(email_template_name, context)
        mail = EmailMessage(
            subject,
            html_content,
            to=[settings.DEFAULT_FROM_EMAIL,],
            bcc=recipients,
            from_email=settings.DEFAULT_FROM_EMAIL
        )

        mail.content_subtype = 'html'
        # if not report.report_type.display_inline:
        #     mail.attach(part)
        mail.send()

    def get_supervisor_spoc_list(self, customers):
        email_dict = {}
        contracts = Contract.objects.prefetch_related('spocs', 'supervisors') .filter(
                                customer__in=customers,
                                contract_type__in=[CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL],
                                status=CONTRACT_STATUS_ACTIVE)

        for contract in contracts:
            supervisors = list(contract.supervisors.values_list('email',flat=True))
            spocs = list(contract.spocs.values_list('email',flat=True))
            email_list = supervisors + spocs

            if contract.customer not in email_dict:
                email_dict[contract.customer] = email_list
            else:
                email_dict[contract.customer].extend(email_list)

        return email_dict
    
    def get_xlsx_table(self, report):
        _tables = []
        report_file = requests.get(report.file.url)
        _wb = io.BytesIO(report_file.content)
        workbook = openpyxl.load_workbook(_wb)
        for sheet in workbook.sheetnames:
            _temp = workbook[sheet]
            columns = next(_temp.values)[0:]
            rdf = pd.DataFrame(_temp.values, columns=columns)
            report_df = rdf.iloc[1:,1:]
            _tables.append({
                "name": sheet,
                "data": json.loads(report_df.to_json(**REPORT_JSON_SETTING))
            })

        return _tables

    def get_csv_table(self, report):
        report_df = pd.read_csv(report.file.url)
        _json = report_df.to_json(**REPORT_JSON_SETTING)
        _tables = [{
                "name": report.report_type.name,
                "data": json.loads(_json)
            }]
        return _tables
    
    def get_table_data_list(self, report):
        _tables = []
        table_style = "{width}%".format(width=report.report_type.width)
        if report.report_type.code == REPORT_TYPE_WMS_MIS: 
            _tables = self.get_xlsx_table(report)
        elif report.report_type.code == EXPORT_TYPE_POD:
            _tables = ['POD']
        else:
            _tables = self.get_csv_table(report)
        
        return _tables, table_style

    def send_export_request_reports(self, user_report, email):
        date_start = user_report.start_date.strftime('%-d %B %Y')
        date_end = user_report.end_date.strftime('%-d %B %Y')
        subject = EXPORT_TYPE_ADHOC_REPORT_SUBJECT.format(
                    report_name=user_report.report_type.description,
                    period_start=date_start,
                    period_end=date_end
                )

        table_data, table_style = self.get_table_data_list(user_report)
        if table_data:
            self.download_file_and_send_mail(user_report, subject, [email,], table_data, table_style)
            user_report.send_status = True
            user_report.save()

    def send_customer_reports(self, ids=[]):
        send_status = False
        if ids:
            reports = UserReport.objects.filter(id__in=ids, send_status=send_status)
        else:
            reports = UserReport.objects.filter(end_date=timezone.now().date(), send_status=send_status)
        
        if not reports:
            return

        customers = reports.values_list('customer', flat=True)
        schedules = CustomerReportSchedule.objects.filter(customer__in = customers)
        if not schedules:
            return

        email_dict = self.get_supervisor_spoc_list(customers)

        for report in reports:
            try:
                schedule = schedules.filter(customer=report.customer,
                                                city=report.city, period=report.period,
                                                report_type = report.report_type).first()

                recipients = list(schedule.subscribed_users.values_list('user__email',flat=True))
                if not report.file:
                    continue

                if not recipients:
                    recipients = [report.customer.user.email,]

                supervisor_spoc_list = email_dict[report.customer]
                if supervisor_spoc_list:
                    recipients.extend(supervisor_spoc_list)

                date_start = report.start_date.strftime('%-d %B %Y')
                date_end = report.end_date.strftime('%-d %B %Y')
                difference = report.end_date - report.start_date

                if difference == timedelta(days = 1):
                    subject = EXPORT_TYPE_DAY_REPORT_SUBJECT.format(
                                        aggregation_level=AGGREGATION_LEVEL[report.period],
                                        report_name=report.report_type.description,
                                        period_start=date_end)
                else:
                    subject = EXPORT_TYPE_REPORT_SUBJECT.format(
                                        aggregation_level=AGGREGATION_LEVEL[report.period],
                                        report_name=report.report_type.description,
                                        period_start=date_start,
                                        period_end=date_end)

                table_data, table_style = self.get_table_data_list(report)
                if table_data:
                    self.download_file_and_send_mail(report, subject, recipients, table_data, table_style)
                    report.send_status = True
                    report.save()

            except Exception as e:
                em = 'Report sending failed for user report id {} - {}'.format(report.id, str(e))
                logger.error(em)
    
