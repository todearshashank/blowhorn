import pandas as pd
from datetime import datetime, timedelta
import os
from django.utils import timezone
from django.conf import settings
from django.db.models import (Q, Avg, Sum, Count)
from django.db.models.functions import (TruncDate,
        ExtractMonth, ExtractWeek, ExtractHour)

from blowhorn.oscar.core.loading import get_model
from django.core.mail import EmailMultiAlternatives
from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.constants import CONTRACT_TYPE_HYPERLOCAL, CONTRACT_TYPE_SPOT, \
                                        CONTRACT_TYPE_FIXED, CONTRACT_TYPE_SHIPMENT
from blowhorn.report.constants import *


Trip = get_model('trip', 'Trip')
Contract = get_model('contract', 'Contract')

class TripReport(object):

    def __init__(self, customer, data):
        # Fetch the required params
        self.customer = customer
        self.city_id = data.get('city_id', None)
        self.hub_id = data.get('hub_id', None)
        self.start = data.get('start', None)
        self.end = data.get('end', None)
        self.quick_filter = data.get('quick_filter', 0)

    def get_query_set(self):
        contract_qs = {
            'customer' : self.customer,
            'contract_type__in' : [CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED,
                                    CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL]
        }

        if self.city_id:
            contract_qs['city'] = self.city_id

        self.contracts = Contract.objects.filter(**contract_qs).values('id')
        contract_id_list = [element['id'] for element in self.contracts]

        qs = {'customer_contract_id__in' : contract_id_list,
                'status__in' : StatusPipeline.TRIP_STATUS}

        if self.hub_id:
            qs['hub'] = self.hub_id

        if self.start and self.end:
            datetime_start = datetime.strptime(self.start, '%Y-%m-%d')
            datetime_end = datetime.strptime(self.end, '%Y-%m-%d')

            if datetime_start == datetime_end:
                datetime_end += timedelta(days=1)

        else:
            datetime_end = timezone.now().date()
            datetime_start = datetime_end - timedelta(days=int(self.quick_filter))

        qs['actual_start_time__date__gte'] = datetime_start
        qs['actual_start_time__date__lt'] = datetime_end

        self.start_date = datetime_start
        self.end_date = datetime_end
        self.get_trip_trend()

        return qs

    def get_trip_qs(self, filters):
        if 'RDS_READ_REP_1' in os.environ:
            trips = Trip.objects.using('readrep1').filter(**filters).select_related(
                        'customer_contract__customer', 'hub')
        else:
            trips = Trip.objects.filter(**filters).select_related(
                        'customer_contract__customer', 'hub')

        return trips

    def get_trip_trend(self):
        self.scale = ExtractHour
        self.trend_name = 'Hours'
        frequency = 'H'
        func_name = pd.date_range
        range_start = self.start_date
        range_end = self.end_date
        difference = self.end_date - self.start_date

        if difference.days > 31:
            self.scale = ExtractMonth
            frequency = 'M'
            self.trend_name = 'Months'
            start = self.start_date - timedelta(days=30)
            end = self.end_date + timedelta(days=30)
            rstart = datetime.strftime(start, '%Y-%m-%d')
            rend = datetime.strftime(end, '%Y-%m-%d')
            range_start = datetime.strptime(rstart, '%Y-%m-%d')
            range_end = datetime.strptime(rend, '%Y-%m-%d')

        elif difference.days > 7:
            self.scale = ExtractWeek
            self.trend_name = 'Weeks'
            frequency = 'W'
            func_name = pd.interval_range
            start = self.start_date - timedelta(weeks=1)
            end = self.end_date + timedelta(weeks=1)
            rstart = datetime.strftime(start, '%Y-%m-%d')
            rend = datetime.strftime(end, '%Y-%m-%d')
            range_start = datetime.strptime(rstart, '%Y-%m-%d')
            range_end = datetime.strptime(rend, '%Y-%m-%d')

        elif difference.days > 1:
            self.scale = TruncDate
            self.trend_name = 'Days'
            frequency = 'D'

        self.daterange = func_name(start=range_start, end=range_end, freq=frequency)

    def get_completed_trip_stats(self):
        header, footer = [], []
        qs = self.get_query_set()
        tripqs = self.get_trip_qs(qs)
        data = tripqs.annotate(trend=self.scale('actual_start_time')).values(
                    'trend'
                ).annotate(
                    _distance=Sum('total_distance'),
                    _time=Sum('total_time'),
                    _cash=Sum('total_cash_collected'),
                    _toll=Sum('toll_charges'),
                    _parking=Sum('parking_charges'),
                    _assigned=Sum('assigned'),
                    _delivered=Sum('delivered'),
                    _attempted=Sum('attempted'),
                    _rejected=Sum('rejected'),
                    _weight=Sum('weight'),
                    _volume=Sum('volume'),
                    _load_time=Sum('total_trip_wait_time_driver')
                ).order_by('trend')

        _index = {}
        i = 0
        trip_date_label = []
        trip_date_data = [0] * len(self.daterange)

        for elem in self.daterange:
            if self.trend_name == 'Hours':
                time_format1 = datetime.strftime(elem,'%H')
                time_format2 = int(time_format1)

            elif self.trend_name == 'Days':
                time_format1 = datetime.strftime(elem,'%d %b %y')
                time_format2 = elem.date()

            elif self.trend_name == 'Weeks':
                time_format1 = datetime.strftime(elem.left,'%d %b') + \
                                    '-' + datetime.strftime(elem.right,'%d %b')
                time_format2 = int(datetime.strftime(elem.left,'%U'))

            elif self.trend_name == 'Months':
                time_format1 = datetime.strftime(elem,'%b-%y')
                time_format2 = int(datetime.strftime(elem,'%m'))

            _index[time_format2] = i
            trip_date_label.append(time_format1)
            i += 1

        for trip in data:
            trend = trip['trend']

            if trend in _index:
                idx = _index.get(trend)
                trip['trend'] = trip_date_label[idx]

        result = {
            'header' : header,
            'data' : data,
            'footer' : footer
        }

        return result

    def get_all_trip_stats(self):
        qs = self.get_query_set()
        trips = self.get_trip_qs(qs)
        data = trips.aggregate(
                    _count=Count('id'),
                    _acount=Count('pk',filter=Q(status=StatusPipeline.TRIP_IN_PROGRESS)),
                    _distance=Avg('total_distance'),
                    _time=Avg('total_time'),
                    _toll=Sum('toll_charges'),
                    _parking=Sum('parking_charges'),
                    _delivered=Sum('delivered'),
                    _rejected=Sum('rejected')
                )

        result = {
            'data' : data
        }

        return result

    def get_trip_category_stats(self, parm):
        qs = self.get_query_set()

        if parm == 'contract':
            qs['customer_contract__contract_type__in'] = [CONTRACT_TYPE_SHIPMENT, CONTRACT_TYPE_HYPERLOCAL,
                                                            CONTRACT_TYPE_SPOT, CONTRACT_TYPE_FIXED]
            trips = self.get_trip_qs(qs)
            data = trips.values('customer_contract__contract_type'
                    ).annotate(count = Count('id'))

        elif parm == 'status':
            qs['status__in'] = StatusPipeline.TRIP_STATUS
            trips = self.get_trip_qs(qs)
            data = trips.values('status').annotate(count = Count('id'))

        result = {'data' : data}
        return result

    def submit_feedback(self, data, user):
        feedback = data.get('feedback', None)
        recipients = ['tech@blowhorn.com']
        subject = 'Report Dashboard Feedback'

        if feedback:
            message = user.email + ': ' + feedback
            mail = EmailMultiAlternatives(
                subject,
                message,
                to=recipients,
                from_email=settings.DEFAULT_FROM_EMAIL
            )

            mail.send()

    def get_trips_heatmap(self):
        pass
        # qs = self.get_query_set()
        # qs['status'] = StatusPipeline.TRIP_COMPLETED
        # qs['actual_start_time__isnull'] = False
        # trips = self.get_trip_qs(qs)
        # data = trips.aggregate(
        #             _count=Count('id'),
        #             _distance=Sum('total_distance'),
        #             _time=Avg('total_time'),
        #             _cash=Sum('total_cash_collected'),
        #             _toll=Sum('toll_charges'),
        #             _parking=Sum('parking_charges'),
        #         )

        # result = {
        #     'data' : data
        # }

        # return result
        # datas = gdp.read_file('shapefiles/polbnda_ind.shp')
        # df = pd.DataFrame(
        #         { 'Latitude': [12.97, 18.96, 13.01],
        #           'Longitude': [77.66, 72.82, 80.2],
        #             'orders' : [100,300,200]
        #         }
        #     )
        # gdf = gdp.GeoDataFrame(df, geometry=gdp.points_from_xy(df.Longitude, df.Latitude))
        # cities_with_country = gdp.sjoin(gdf, datas, how="inner", op='intersects')
        # for_plotting = datas.merge(cities_with_country[["nam","orders"]], left_on = 'nam', right_on = 'nam',how='left').fillna(0)
        # fig, ax = plt.subplots(1, figsize=(10, 10))
        # for_plotting.plot(column='orders', cmap='Blues', linewidth=0.8, ax=ax, edgecolor="None",legend=True)
        # response = HttpResponse(content_type="image/jpeg")
        # for_plotting.savefig(response, format="jpeg")
        # return response





