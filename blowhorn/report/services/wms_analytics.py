
import pandas as pd
import os
from datetime import datetime, timedelta
from django.utils import timezone
from django.db.models import (F, Sum, CharField, Value, Func)
from django.db.models.functions import (TruncDate,
        ExtractMonth, ExtractWeek, ExtractHour)

from blowhorn.report.constants import INVENTORY_STATUS_MAPPING
from blowhorn.wms.submodels.inventory import Inventory, InventoryTransaction
from blowhorn.wms.constants import WMS_RECEIVED, ORDER_LINE_SHIPPED, LOC_UNLOCKED, WMS_INV_QTY_UPDATED, \
    WMS_STOCK_CHECKED, WMS_INVENTORY_DELETED


class WMSAnalytics(object):

    def __init__(self, customer, data):
        # Fetch the required params
        self.customer = customer
        self.start = data.get('start', None)
        self.end = data.get('end', None)
        self.quick_filter = data.get('quick_filter', 0)
        self.timestamp_format = 'DD/MM/YYYY HH24:MI'
        self.limit = int(data.get('limit', 10)) if data.get('limit', 10) else 0

    def get_inventory_qs(self, prefetch_required=True):
        qs = {'client__customer' : self.customer}

        if self.start and self.end:
            datetime_start = datetime.strptime(self.start, '%Y-%m-%d')
            datetime_end = datetime.strptime(self.end, '%Y-%m-%d')

            if datetime_start == datetime_end:
                datetime_end += timedelta(days=1)

        else:
            datetime_end = timezone.now().date()
            datetime_start = datetime_end - timedelta(days=int(self.quick_filter))

        qs['created_date__date__gte'] = datetime_start
        qs['created_date__date__lt'] = datetime_end

        self.start_date = datetime_start
        self.end_date = datetime_end

        if prefetch_required:
            if 'RDS_READ_REP_1' in os.environ:
                data = Inventory.objects.using('readrep1').filter(**qs).select_related(
                            'sku', 'location', 'site', 'pack_config',
                            'client', 'container', 'dimensions', 'supplier')
            else:
                data = Inventory.objects.filter(**qs).select_related(
                            'sku', 'location', 'site', 'pack_config',
                            'client', 'container', 'dimensions', 'supplier')
        else:
            if 'RDS_READ_REP_1' in os.environ:
                data = Inventory.objects.using('readrep1').filter(**qs)
            else:
                data = Inventory.objects.filter(**qs)

        return data

    def get_inventory_txn_qs(self, prefetch_required=True):
        qs = {'client__customer' : self.customer}

        if self.start and self.end:
            datetime_start = datetime.strptime(self.start, '%Y-%m-%d')
            datetime_end = datetime.strptime(self.end, '%Y-%m-%d')

            if datetime_start == datetime_end:
                datetime_end += timedelta(days=1)

        else:
            datetime_end = timezone.now().date()
            datetime_start = datetime_end - timedelta(days=int(self.quick_filter))

        qs['created_date__date__gte'] = datetime_start
        qs['created_date__lt'] = datetime_end

        self.start_date = datetime_start
        self.end_date = datetime_end

        if prefetch_required:
            if 'RDS_READ_REP_1' in os.environ:
                data = InventoryTransaction.objects.using('readrep1').filter(**qs).select_related(
                            'sku', 'location', 'site', 'pack_config',
                            'client', 'container', 'dimensions', 'supplier')
            else:
                data = InventoryTransaction.objects.filter(**qs).select_related(
                            'sku', 'location', 'site', 'pack_config',
                            'client', 'container', 'dimensions', 'supplier')
        else:
            if 'RDS_READ_REP_1' in os.environ:
                data = InventoryTransaction.objects.using('readrep1').filter(**qs)
            else:
                data = InventoryTransaction.objects.filter(**qs)

        return data

    def get_trend(self):
        self.scale = ExtractHour
        self.trend_name = 'Hours'
        frequency = 'H'
        func_name = pd.date_range
        range_start = self.start_date
        range_end = self.end_date
        difference = self.end_date - self.start_date

        if difference.days > 31:
            self.scale = ExtractMonth
            frequency = 'M'
            self.trend_name = 'Months'
            start = self.start_date - timedelta(days=30)
            end = self.end_date + timedelta(days=30)
            rstart = datetime.strftime(start, '%Y-%m-%d')
            rend = datetime.strftime(end, '%Y-%m-%d')
            range_start = datetime.strptime(rstart, '%Y-%m-%d')
            range_end = datetime.strptime(rend, '%Y-%m-%d')

        elif difference.days > 7:
            self.scale = ExtractWeek
            self.trend_name = 'Weeks'
            frequency = 'W'
            func_name = pd.interval_range
            start = self.start_date - timedelta(weeks=1)
            end = self.end_date + timedelta(weeks=1)
            rstart = datetime.strftime(start, '%Y-%m-%d')
            rend = datetime.strftime(end, '%Y-%m-%d')
            range_start = datetime.strptime(rstart, '%Y-%m-%d')
            range_end = datetime.strptime(rend, '%Y-%m-%d')

        elif difference.days > 1:
            self.scale = TruncDate
            self.trend_name = 'Days'
            frequency = 'D'

        self.daterange = func_name(start=range_start, end=range_end,
                                    freq=frequency)

    def inventory_overview_tiles(self):
        '''
            Display: Top Tiles
        '''
        result = {'unlocked_inventory' : 0,
            'total_inventory' : 0,
            'shipped_count' : 0,
            'received_count' : 0
        }

        qs = self.get_inventory_qs(prefetch_required=False)
        _unlocked_inventory = qs.filter(status=LOC_UNLOCKED).aggregate(unlocked_inventory=Sum('qty'))
        _count = _unlocked_inventory['unlocked_inventory']
        if _count:
            result['unlocked_inventory'] = _count if _count > 0 else 0


        _total_inventory = qs.aggregate(total_inventory=Sum('qty'))
        _count = _total_inventory['total_inventory']
        if _count:
            result['total_inventory'] = _count if _count > 0 else 0

        qs = self.get_inventory_txn_qs(prefetch_required=False)
        inventory_data = qs.filter(status__in=[ORDER_LINE_SHIPPED, WMS_RECEIVED]
                            ).values('status').annotate(count=Sum('qty'))

        for e in inventory_data:
            if e['status'] == ORDER_LINE_SHIPPED:
                result['shipped_count'] = e['count']
            if e['status'] == WMS_RECEIVED:
                result['received_count'] = e['count']
        return result

    def inventory_by_timeperiod(self):
        qs = self.get_inventory_qs()
        self.get_trend()

        inventory_ = qs.annotate(trend=self.scale('created_date')).values(
                        'trend').annotate(count=Sum('qty'))

        inventory_label = []
        inventory_data = [0] * len(self.daterange)

        _index = {}
        i = 0
        for elem in self.daterange:
            if self.trend_name == 'Hours':
                time_format1 = datetime.strftime(elem,'%H')
                time_format2 = int(time_format1)

            elif self.trend_name == 'Days':
                time_format1 = datetime.strftime(elem,'%d %b %y')
                time_format2 = elem.date()

            elif self.trend_name == 'Weeks':
                time_format1 = datetime.strftime(elem.left,'%d %b') + \
                                    '-' + datetime.strftime(elem.right,'%d %b')
                time_format2 = int(datetime.strftime(elem.left,'%U'))

            elif self.trend_name == 'Months':
                time_format1 = datetime.strftime(elem,'%b-%y')
                time_format2 = int(datetime.strftime(elem,'%m'))

            _index[time_format2] = i
            inventory_label.append(time_format1)
            i += 1

        # data = []
        for _data in inventory_:
            trend = _data['trend']

            if trend in _index:
                idx = _index.get(trend)
                inventory_data[idx] = _data['count']
                # data.append({'trend' : inventory_label[idx], 'count' : _data['count']})

        data = {inventory_label[i]: inventory_data[i] for i in range(len(inventory_label))}
        result = {
            'data': data,
            'trend': self.trend_name
        }

        return result

    def inventory_by_status(self):
        qs = self.get_inventory_txn_qs(prefetch_required=False).exclude(status__in=[WMS_INV_QTY_UPDATED,
                                                                        WMS_STOCK_CHECKED, WMS_INVENTORY_DELETED])
        inventory_data = qs.values('status').annotate(count=Sum('qty'))
        for item in inventory_data:
            status = item.get('status')
            item['status'] = INVENTORY_STATUS_MAPPING.get(status) or status

        result = {
            'data': inventory_data
        }

        return result

    def hub_summary(self):
        qs = self.get_inventory_qs()

        hub_data = qs.values('location__name').annotate(
                            location_name = F('location__name'),
                            total_count=Sum('qty'),
                            total_allocated_count=Sum('allocated_qty'),
                        )

        result = {
            'data' : hub_data
        }

        return result

    def top_expiry_products(self):
        qs = self.get_inventory_qs(prefetch_required=False)

        _data = qs.filter(expiry_date__isnull=False
            ).order_by('-expiry_date')[:self.limit].annotate(
            sku_name = F('sku__name'),
            tag_name = F('tag'),
            date_of_expiry = Func(
                F('expiry_date'),
                Value(self.timestamp_format),
                function='to_char',
                output_field=CharField()
            ),
            ).values('sku_name', 'tag_name', 'date_of_expiry')

        result = {
            'data' : _data
        }

        return result
