from blowhorn.customer.constants import AGGREGATION_CATEGORIES
import operator
from functools import reduce
from django.db.models import  Q

from rest_framework import status, views, generics, filters
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated

from blowhorn.common.pagination import StandardResultsSetPagination
from blowhorn.customer.mixins import CustomerMixin
from blowhorn.customer.models import Report
from blowhorn.customer.permissions import IsOrgAdmin
from blowhorn.report.serializers import CustomerReportScheduleListSerializer
from blowhorn.report.services.shipment_reports import ShipmentReport
from blowhorn.report.services.trip_reports import TripReport
from blowhorn.report.services.wms_analytics import WMSAnalytics

# TMS Analytics

class ShipmentStatus(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data= ShipmentReport(customer, request.GET).order_status()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentTimePlaced(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_time_placed()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentPaymentMode(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_payment_mode()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentAttempts(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_attempts()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentPlacedTrend(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_dateplaced_trend()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentDeliveryPerformance(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_delivery_performance()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentSameDayDelivery(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_sameday_delivery()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentSLA(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_sla()
        return Response(status=status.HTTP_200_OK, data=response_data)

class ShipmentAutoDispatch(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = ShipmentReport(customer,request.GET).order_dispatch_summary()
        return Response(status=status.HTTP_200_OK, data=response_data)

class TripCompletedStats(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = TripReport(customer,request.GET).get_completed_trip_stats()
        return Response(status=status.HTTP_200_OK, data=response_data)

class TripAllStats(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = TripReport(customer,request.GET).get_all_trip_stats()
        return Response(status=status.HTTP_200_OK, data=response_data)

class TripContractCategory(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = TripReport(customer,request.GET).get_trip_category_stats('contract')
        return Response(status=status.HTTP_200_OK, data=response_data)

class TripStatusCategory(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = TripReport(customer,request.GET).get_trip_category_stats('status')
        return Response(status=status.HTTP_200_OK, data=response_data)

class CustomerOrderHeatmapView(views.APIView, CustomerMixin):

    def get(self, request):
        user = request.user
        params = request.query_params
        customer = self.get_customer(user)
        order_qs = ShipmentReport(customer, params).get_order_location_data()
        return Response(data=list(order_qs), status=status.HTTP_200_OK)

# WMS Analytics
class WmsInventoryTiles(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = WMSAnalytics(customer,request.GET).inventory_overview_tiles()
        return Response(status=status.HTTP_200_OK, data=response_data)

class WmsInventoryCreationTrend(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = WMSAnalytics(customer,request.GET).inventory_by_timeperiod()
        return Response(status=status.HTTP_200_OK, data=response_data)

class WmsInventoryByStatus(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = WMSAnalytics(customer,request.GET).inventory_by_status()
        return Response(status=status.HTTP_200_OK, data=response_data)

class WmsHubSummary(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = WMSAnalytics(customer,request.GET).hub_summary()
        return Response(status=status.HTTP_200_OK, data=response_data)

class WmsExpiringInventory(views.APIView):

    def get(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = WMSAnalytics(customer,request.GET).top_expiry_products()
        return Response(status=status.HTTP_200_OK, data=response_data)

# Feedback API
class SubmitFeedback(views.APIView):

    def post(self, request):
        customer = CustomerMixin().get_customer(request.user)
        response_data = TripReport(customer,request.GET).submit_feedback(request.data, request.user)
        return Response(status=status.HTTP_200_OK)

# Report Creation API
class ReportCreation(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        api_key = request.META.get('HTTP_API_KEY', None)
        print(api_key)
        if api_key != "P1a03N19d91A":
            return Response(status=status.HTTP_400_BAD_REQUEST,  data='API Key mismatch')

        from blowhorn.report.services.scheduled_reports import ReportService
        ReportService().generate_subscribed_reports()
        return Response(status=status.HTTP_200_OK)

# Report Scheduler API
class ReportScheduleSupportDataView(views.APIView):

    def get(self, request):
        data = request.GET
        result = {}
        reports = []
        category = set()
        system_id = data.get('system', 1)
        qs = Report.objects.filter(is_active=True, system_id=system_id)
        for record in qs:
            report = {
                'id': record.id,
                'code': record.code,
                'name': record.name,
                'category' : record.category
            }
            category.add(record.category)
            reports.append(report)

        category_list = [{'name' : cat} for cat in category]
        result = {
            'reports': reports,
            'category' : category_list,
            'aggregation_categories': AGGREGATION_CATEGORIES
        }
        return Response(status=status.HTTP_200_OK, data=result)


class CustomerReportScheduleListView(generics.ListCreateAPIView, CustomerMixin):
    queryset = CustomerReportScheduleListSerializer.queryset
    serializer_class = CustomerReportScheduleListSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id', 'period', 'report_type__name')
    permission_classes = (IsAuthenticated, IsOrgAdmin)

    def get_queryset(self):
        data = self.request.data or self.request.GET
        user = self.request.user
        report_type_id = data.get('report_type_id', None)
        category = data.get('category', None)
        system_id = data.get('system', 1)
        customer = self.get_customer(user)
        query = [Q(customer=customer) & Q(report_type__system_id=system_id)]
        if report_type_id:
            query.append(Q(report_type_id=report_type_id))

        if category:
            query.append(Q(report_type__category=category))

        final_query = reduce(operator.and_, query)
        return self.serializer_class.Meta.model.objects.filter(final_query).select_related('report_type')
