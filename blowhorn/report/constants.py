from config.settings import status_pipelines as StatusPipeline

# Labels
cash_on_delivery = 'Cash'

time_slot_0_6 = '0-6'
time_slot_6_12 = '6-12'
time_slot_12_18 = '12-18'
time_slot_18_24 = '18-24'

delivered_order_status = [
    StatusPipeline.ORDER_DELIVERED,
    StatusPipeline.DELIVERY_ACK,
    StatusPipeline.ORDER_PAYMENT_DONE
]
unable_to_deliver_status= [
    StatusPipeline.UNABLE_TO_DELIVER
]
returned_status = [
    StatusPipeline.ORDER_RETURNED,
    StatusPipeline.ORDER_RETURNED_RTO,
    StatusPipeline.ORDER_OUT_FOR_RTO,
    StatusPipeline.ORDER_RETURNED_TO_HUB,
    StatusPipeline.RETURNED_ACK
]
attempted_status_list = [
    StatusPipeline.ORDER_DELIVERED,
    StatusPipeline.ORDER_RETURNED,
    StatusPipeline.UNABLE_TO_DELIVER
]
rejected_status_list = [
    StatusPipeline.ORDER_DELIVERED,
]
out_for_delivery_status_list = [
    StatusPipeline.OUT_FOR_DELIVERY,
    StatusPipeline.DRIVER_ACCEPTED,
    StatusPipeline.DRIVER_ASSIGNED
]
out_for_pickup_status_list = [
    StatusPipeline.ORDER_OUT_FOR_PICKUP,
]

order_status_label = [
    'New Order', 'Delivered', 'Unable To Deliver', 'Returned',
    'Rejected', 'Out for Delivery', 'Out for Pickup'
]

order_time_placed_label = [
    '12AM - 4AM', '4AM - 8AM', '8AM - 12PM',
    '12PM - 4PM', '4PM - 8PM', '8PM - 12AM',
]

# order_payment_mode_label = [cash_on_delivery, prepaid]
order_attempt_label = ['Attempt 1', 'Attempt 2', 'Attempt 3']
TRANSPORTATION_STATUS_RECEIVED = 'Received'
TRANSPORTATION_STATUS_IN_TRANSIT = 'In-Transit'

INWARD = 'inward'
OUTWARD = 'outward'
PENDING = 'pending'

REPORT_JSON_SETTING = {
    'orient' : "records",
    'date_format' : "epoch",
    'double_precision' : 10,
    'force_ascii' : True,
    'date_unit' : "ms",
    'default_handler' : None
}

REPORT_TYPE_WMS_INVENTORY = 'wms_inventory_report'
REPORT_TYPE_WMS_INVENTORY_TXN = 'wms_inventory_transaction'

REPORT_TYPE_WMS_MOVEMENT_SUMMARY = 'wms_movement_summary_report'
REPORT_TYPE_WMS_HUB_SUMMARY = 'wms_hub_summary_report'
REPORT_TYPE_WMS_MIS = 'wms_mis_summary'

INVENTORY_DATA_TO_BE_EXPORTED = [
    {'name': 'ID', 'label': 'id', 'type': 'string'},
    {'name': 'Tag', 'label': 'tag', 'type': 'string'},
    {'name': 'SKU', 'label': 'sku_name', 'type': 'string'},
    {'name': 'Site', 'label': 'site_name', 'type': 'string'},
    {'name': 'Client', 'label': 'client_name', 'type': 'string'},
    {'name': 'Pack Config Name', 'label': 'packconfig_name', 'type': 'string'},
    {'name': 'Status', 'label': 'status', 'type': 'string'},
    {'name': 'Quantity', 'label': 'qty', 'type': 'string'},
    {'name': 'Allocated Quantity', 'label': 'allocated_qty', 'type': 'string'},
    {'name': 'Manufacturer', 'label': 'supplier_name', 'type': 'string'},
    {'name': 'Tracking Level', 'label': 'tracking_level', 'type': 'string'},
    {'name': 'Remarks', 'label': 'remarks', 'type': 'string'},
    {'name': 'Created By', 'label': 'created_by', 'type': 'string'},
    {'name': 'Created Date', 'label': 'created_date', 'type': 'datetime'},
    {'name': 'Modified By', 'label': 'modified_by', 'type': 'string'},
    {'name': 'Modified Date', 'label': 'modified_date', 'type': 'datetime'},
]

INVENTORY_TXN_DATA_TO_BE_EXPORTED = [
    {'name': 'ID', 'label': 'id', 'type': 'string'},
    {'name': 'Reference', 'label': 'reference', 'type': 'string'},
    {'name': 'Reference Number', 'label': 'reference_number', 'type': 'string'},
    {'name': 'Tag', 'label': 'tag', 'type': 'string'},
    {'name': 'SKU', 'label': 'sku_name', 'type': 'string'},
    {'name': 'Source Location', 'label': 'from_loc_name', 'type': 'string'},
    {'name': 'Destination Location', 'label': 'to_loc_name', 'type': 'string'},
    {'name': 'Site', 'label': 'site_name', 'type': 'string'},
    {'name': 'Client', 'label': 'client_name', 'type': 'string'},
    {'name': 'Pack Config Name', 'label': 'packconfig_name', 'type': 'string'},
    {'name': 'Status', 'label': 'status', 'type': 'string'},
    {'name': 'Quantity', 'label': 'qty', 'type': 'string'},
    {'name': 'Allocated Quantity', 'label': 'allocated_qty', 'type': 'string'},
    {'name': 'Manufacturer', 'label': 'supplier_name', 'type': 'string'},
    {'name': 'Tracking Level', 'label': 'tracking_level', 'type': 'string'},
    {'name': 'Remarks', 'label': 'remarks', 'type': 'string'},
    {'name': 'Created By', 'label': 'created_by', 'type': 'string'},
    {'name': 'Created Date', 'label': 'created_date', 'type': 'datetime'},
    {'name': 'Modified By', 'label': 'modified_by', 'type': 'string'},
    {'name': 'Modified Date', 'label': 'modified_date', 'type': 'datetime'},

]

HUB_INV_SUMMARY_DATA_TO_BE_EXPORTED = [
    {'name': 'Hub Name', 'label': 'hub_name', 'type': 'string'},
    {'name': 'Product Group', 'label': 'product_group', 'type': 'string'},
    {'name': 'Inventory Count', 'label': 'total_inv', 'type': 'string'},
    {'name': 'Working Count', 'label': 'qc_passed', 'type': 'string'}
]

INVENTORY_STATUS_MAPPING = {'WMS Manifested': 'Intransit', 'WMS Shipped': 'Shipped'}
