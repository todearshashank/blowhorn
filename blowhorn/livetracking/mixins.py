from config.settings import status_pipelines as StatusPipeline
from blowhorn.legacy.adapter import LegacyAdapter
from blowhorn.driver.models import DriverActivity

EXCLUDE_STATUSES = [
    StatusPipeline.ORDER_DELIVERED,
    StatusPipeline.ORDER_RETURNED,
    StatusPipeline.UNABLE_TO_DELIVER
]


class LiveTrackingMixin(object):

    def remove_latest_location(self, driver):
        activity = DriverActivity.objects.filter(driver=driver)
        if activity.exists():
            print('Deleting driver activity: ', activity)
            return activity.delete()

    def get_order_details_for_driver(self, trips):
        _out = []
        order_ids = []
        for trip in trips:
            for stop in trip.stops.all():
                order = stop.waypoint.order if stop.waypoint else stop.order
                if order.id not in order_ids and \
                    order.status not in EXCLUDE_STATUSES:
                    dict_ = LegacyAdapter().get_booking_for_customer(order, trip)
                    _out.append(dict_)
                    order_ids.append(order.id)
        return _out
