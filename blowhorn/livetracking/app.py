from django.conf.urls import url
from .views import LiveTrackingParams, DriverLocations, \
    DriverAction, DriverProfile, OccupiedDriverTripDetails, \
    ResourceAllocationView, DriverDeviceDetails


urlpatterns = [
            url(r'^livetracking/driverlocations$', DriverLocations.as_view(),
                name='driver-locations'),
            url(r'^livetracking/action$', DriverAction.as_view(), name='actions'),
            url(r'^livetracking/params$', LiveTrackingParams.as_view(),
                name='livetracking-params'),
            url(r'^livetracking/trip-details$',
                OccupiedDriverTripDetails.as_view(),
                name='livetracking-trip-details'),
            url(r'^livetracking/resource-allocation-details$',
                ResourceAllocationView.as_view(),
                name='livetracking-resource-allocation-details'),
            url(r'^livetracking/profile/(?P<pk>[0-9]+)$',
                DriverProfile.as_view(), name='profile'),
            url(r'^driver/(?P<driver_pk>[0-9]+)/device-details$',
                DriverDeviceDetails.as_view(), name='driver-device-details'),
        ]
