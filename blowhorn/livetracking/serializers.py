# System && Django
import logging
from django.contrib.auth import get_user_model

# 3rd Party Libraries
from rest_framework import serializers, exceptions
from blowhorn.oscar.core.loading import get_model

# Blowhorn Modules
from blowhorn.common.notification import Notification
from blowhorn.livetracking.constants import ACTIONS, CLEAR, LOGOUT, \
    UPDATE_LOCATION_TIME, REMOVE_IDLE
from blowhorn.livetracking.mixins import LiveTrackingMixin
from blowhorn.driver.serializers import DriverSerializer, DeviceDetailSerializer
from blowhorn.vehicle.serializers import VehicleSerializer
from blowhorn.order.serializers.OrderSerializer import OrderSerializer
from blowhorn.utils.functions import utc_to_ist, get_current_delta

DriverActivity = get_model('driver', 'DriverActivity')
Driver = get_model('driver', 'Driver')
User = get_user_model()

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DriverActionSerializer(serializers.Serializer, LiveTrackingMixin):
    queryset = Driver.objects.all()
    driver_id = serializers.IntegerField(required=False)
    action = serializers.CharField(required=False)
    order_number = serializers.CharField(required=False)
    alarm = serializers.IntegerField(required=False)
    runnable = serializers.IntegerField(required=False)
    intent = serializers.IntegerField(required=False)
    intent_dis = serializers.IntegerField(required=False)

    def validate(self, attrs):
        driver_id = attrs.get('driver_id')
        if not driver_id:
            raise exceptions.ValidationError('Driver id is required')

        driver = self.get_queryset(driver_id=driver_id)
        if not driver:
            raise exceptions.ValidationError('Driver not found')

        action = attrs.get('action')
        if action not in ACTIONS:
            raise exceptions.ParseError('Invalid action request')

        attrs['user'] = driver.user
        attrs['driver'] = driver
        attrs['intent'] = int(attrs.get('intent', 0)) * 1000
        attrs['alarm'] = int(attrs.get('alarm', 0)) * 1000
        attrs['runnable'] = int(attrs.get('runnable', 0)) * 1000
        attrs['intent_dis'] = attrs.get('intent_dis', '')
        logger.info('Driver Action: %s, %s' % (driver_id, action))
        return attrs

    def save(self, **kwargs):
        action = self.validated_data['action']
        if action == REMOVE_IDLE:
            # Clear driver's location from database
            self.remove_latest_location(self.validated_data['driver'])
        else:
            if action in [CLEAR, LOGOUT]:
                # Clear driver's location from database
                self.remove_latest_location(self.validated_data['driver'])

            if action == CLEAR:
                dict_ = {
                    'username': self.validated_data['driver_id'],
                    'action': action,
                }
                # If booking id/key is present, then we need to pass that
                # as it means a conditional clear if diver is on that
                # booking only
                if self.validated_data.get('order_number'):
                    dict_['booking_id'] = self.validated_data['order_number']

            elif action == UPDATE_LOCATION_TIME:
                dict_ = {
                    'username': self.validated_data['driver_id'],
                    'action': action,
                    'alarm': self.validated_data['alarm'],
                    'runnable': self.validated_data['runnable'],
                    'intent': self.validated_data['intent'],
                    'intent_dis': self.validated_data['intent_dis']
                }
            else:
                dict_ = {
                    'username': self.validated_data['driver_id'],
                    'action': action
                }
            notification_status = \
                Notification.send_action_to_driver(self.validated_data['user'],
                                                   dict_)
            if notification_status:
                return True
        return False

    def get_queryset(self, driver_id=None):
        qs = Driver.objects.select_related('user')
        try:
            return qs.get(pk=driver_id)
        except Driver.DoesNotExist:
            return None
