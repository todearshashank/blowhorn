import json
import logging
from datetime import timedelta
from django.conf import settings
from django.db.models import Prefetch
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.views.generic import TemplateView
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from blowhorn.oscar.core.loading import get_model
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from config.settings import status_pipelines as StatusPipeline
from blowhorn.contract.models import Resource
from blowhorn.driver.mixins import DriverMixin
from blowhorn.driver.models import Driver, DriverVehicleMap, DriverActivity
from blowhorn.driver.redis_models import Driver as RedisDriver
from blowhorn.driver.serializers import DriverActivitySerializer, DeviceDetailSerializer
from blowhorn.livetracking.constants import DRIVER_CATEGORIES, DRIVER_STATUSES,\
    ACTIVITY_UPDATE_DELTA_MINUTES
from blowhorn.livetracking.mixins import LiveTrackingMixin
from blowhorn.livetracking.serializers import DriverActionSerializer
from blowhorn.order.models import Order
from blowhorn.trip.models import Trip, Stop
from blowhorn.trip.serializers import TripSerializer
from blowhorn.users.permission import IsBlowhornStaff
from blowhorn.vehicle.utils import VehicleUtils

City = get_model('address', 'City')
User = get_model('users', 'User')

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class LiveTrackingParams(generics.RetrieveAPIView):
    """
        GET api/livetracking/params
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)


    def __get_city_names(self):
        """
        :return: list of cities with dict containing pk, name
        NOTE: `address_id` is required since get_queryset()
            has been overridden in model with `select_related`
        """
        city_qs = City.objects.only('pk', 'name', 'address_id')
        return [{'id': city.pk, 'name': city.name} for city in city_qs]

    def get(self, request):
        logger.info('Fetching all the params...')
        vehicle_icons = VehicleUtils().get_vehicle_icons()
        _out = {
            'vehicle_icons': vehicle_icons,
            'driver_statuses': DRIVER_STATUSES,
            'driver_category': DRIVER_CATEGORIES,
            'order_statuses': StatusPipeline.ORDER_STATUSES,
            'cities': self.__get_city_names()
        }
        return Response(_out)


class DriverLocations(generics.RetrieveAPIView):
    """
        GET api/livetracking/driverlocations
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def get(self, request):
        qs = self.get_queryset()
        ret_dict = {}
        for driver_activity in qs:
            driver = driver_activity.driver
            ret_dict[driver.id] = \
                DriverMixin().get_driver_location_details(
                driver_activity, driver)

        return Response(status=status.HTTP_200_OK, data=ret_dict)

    def get_queryset(self):
        update_time = timezone.now() - timedelta(minutes=ACTIVITY_UPDATE_DELTA_MINUTES)
        return DriverActivity.objects.filter(
            updated_time__gte=update_time
        ).select_related(
            'driver', 'driver__user', 'driver__device_details',
            'driver__operating_city', 'trip'
        ).prefetch_related(Prefetch(
                'driver__vehicles',
                queryset=DriverVehicleMap.objects.select_related(
                    'vehicle__vehicle_model__vehicle_class')
            )
        )

class DriverDeviceDetails(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)
    serializer_class = DeviceDetailSerializer

    def get(self, request, driver_pk=None):
        device_details = RedisDriver(driver_pk)['device_details']
        if not device_details:
            try:
                driver = Driver.objects.only('device_details').get(id=driver_pk)
            except Driver.DoesNotExist:
                return Response(status=404, data=_('Driver not found'))

            device_details = DeviceDetailSerializer(driver.device_details).data
            RedisDriver(driver_pk)['device_details'] = json.dumps(device_details)

        return Response(status=200, data=device_details)


class OccupiedDriverTripDetails(generics.ListAPIView, DriverMixin):
    """
        POST api/livetracking/trip-details
    """
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)
    serializer_class = TripSerializer

    def get(self, request):
        orders = self.get_queryset()
        data = {order.get('driver_id'):{
                'customer_name': order.get('customer__name'),
                'customer_pk': order.get('customer_id'),
                'booking_id': order.get('number'),
                'booking_type': order.get('order_type')
            } for order in orders if order.get('driver_id')}

        return Response(status=status.HTTP_200_OK,
                        data=data)

    def get_queryset(self):
        update_time = timezone.now() - timedelta(minutes=ACTIVITY_UPDATE_DELTA_MINUTES)
        order_pks = DriverActivity.objects.filter(
            order_id__isnull=False,
            updated_time__gte=update_time
        ).values('order_id')
        return Order.objects.filter(
            id__in=order_pks
        ).select_related('customer').values(
            'customer_id', 'customer__name',
            'order_type', 'number', 'driver_id'
        )

class DriverAction(generics.ListCreateAPIView, LiveTrackingMixin):
    """
        POST api/livetracking/action
        DATA:
            action (str)
            driver_id (pk of user)
    """
    serializer_class = DriverActionSerializer
    queryset = serializer_class.queryset
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)


    def post(self, request):
        serializer = self.serializer_class(
            data=request.data,
            context={'request': request}
        )
        if serializer.is_valid():
            response = serializer.save()
            if response:
                return Response(status=status.HTTP_200_OK,
                                data=_('Action successfully sent to driver'))

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=serializer.errors)

    def get_queryset(self, driver_id=None):
        qs = Driver.objects.select_related('user')
        return qs.get(pk=driver_id)


class DriverProfile(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)
    # permission_classes = (AllowAny, )

    def get(self, request, pk=None):
        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Blocked API')

    def get_queryset(self):
        driver_id = self.kwargs.pop('pk')
        qs = DriverActivity._default_manager.filter(driver_id=driver_id)
        return qs.select_related('driver').first()


class ResourceAllocationView(generics.ListCreateAPIView, LiveTrackingMixin):
    """
        GET livetracking/resource-allocation-details
        returns: list
            driver_id
    """
    serializer_class = DriverActivitySerializer
    queryset = serializer_class.queryset
    permission_classes = (IsAuthenticated, IsBlowhornStaff,)

    def get(self, request):
        resources = self.get_queryset()
        return Response(status=status.HTTP_200_OK, data=resources)

    def get_queryset(self):
        update_time = timezone.now() - timedelta(minutes=ACTIVITY_UPDATE_DELTA_MINUTES)
        driver_pks = DriverActivity.objects.filter(
            updated_time__gte=update_time
        ).values_list('driver_id', flat=True)
        return Resource.objects.filter(
            driver_id__in=driver_pks
        ).values_list('driver_id', flat=True)
