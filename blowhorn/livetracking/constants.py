REMOVE_IDLE = 'remove_idle'
CLEAR = 'clear'
LOGOUT = 'logout'
UPDATE_LOCATION_TIME = 'update_location_time'

ACTIONS = [
    CLEAR,
    LOGOUT,
    UPDATE_LOCATION_TIME,
    REMOVE_IDLE,
    'request_location',
    'restore',
    'duty_on',
    'duty_off'
]

# DRIVER_STATUSES = [
#     'idle',
#     'c2c/sme',
#     'b2b',
#     'shipment'
# ]

DRIVER_STATUSES = [
    'idle',
    'booking',
    'subscription',
    'spot',
    'shipment'
]

DRIVER_CATEGORIES = [
    'marketplace',
    'fixed'
]

ACTIVITY_UPDATE_DELTA_MINUTES = 5
