import graphene
import blowhorn.order.schema
import blowhorn.driver.schema
import blowhorn.trip.schema
import blowhorn.customer.schema
from graphene_django.debug import DjangoDebug


class Query(
    blowhorn.order.schema.Query,
    blowhorn.driver.schema.Query,
    blowhorn.customer.schema.Query,
    graphene.ObjectType):
    debug = graphene.Field(DjangoDebug, name='__debug')
    pass


class Mutation(
    blowhorn.order.schema.Mutation,
    blowhorn.trip.schema.Mutation,
    blowhorn.customer.schema.Mutation,
    graphene.ObjectType):
    debug = graphene.Field(DjangoDebug, name='__debug')
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
