from django.db import models
from django.utils.translation import ugettext_lazy as _
from blowhorn.customer.models import Partner
from blowhorn.driver.models import Driver
from blowhorn.common.models import BaseModel

class PartnerLeadStatus(BaseModel):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE, blank=True,
                               null=True)

    partner = models.ForeignKey(Partner, on_delete=models.CASCADE, blank=True,
                                null=True)

    reference_id = models.CharField(max_length=250)

    loan_status = models.CharField(max_length=250)

    loan_amount = models.DecimalField(_('Loan amount'), max_digits=10, decimal_places=2, default=0)

    installment_amount = models.DecimalField(_('Installment amount'), max_digits=10, decimal_places=2, default=0)

    no_of_installments = models.PositiveIntegerField(_("Total Installments"),
                                                     null=True, blank=True, db_index=True)

    upcoming_installment_number = models.PositiveIntegerField(_("upcoming Installment No"),
                                                              null=True, blank=True, db_index=True)

    outstanding_amount = models.DecimalField(_('Outstanding amount'), max_digits=10, decimal_places=2, default=0)


    class Meta:
        verbose_name = _("Leads")
        verbose_name_plural = _("Leads")

    def __str__(self):
        return self.reference_id

class PartnerLeadHistory(BaseModel):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE, blank=True,
                               null=True)

    partner = models.ForeignKey(Partner, on_delete=models.CASCADE, blank=True,
                                null=True)

    reference_id = models.CharField(max_length=250)

    loan_status = models.CharField(max_length=250)

    loan_amount = models.DecimalField(_('Loan amount'), max_digits=10, decimal_places=2, default=0)

    installment_amount = models.DecimalField(_('Installment amount'), max_digits=10, decimal_places=2, default=0)

    no_of_installments = models.PositiveIntegerField(_("Total Installments"),
                                                     null=True, blank=True, db_index=True)

    upcoming_installment_number = models.PositiveIntegerField(_("upcoming Installment No"),
                                                              null=True, blank=True, db_index=True)

    outstanding_amount = models.DecimalField(_('Outstanding amount'), max_digits=10, decimal_places=2, default=0)

    partner_lead = models.ForeignKey(PartnerLeadStatus, on_delete=models.CASCADE, related_name='history')
