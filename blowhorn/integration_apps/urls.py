from blowhorn.integration_apps.decentro.views import DocumentValidate, BankVerification, AccountAcitivity
from django.conf.urls import url

urlpatterns = [
    url(r'^decentro/validate$', DocumentValidate.as_view(), name='decentro-validation'),
    url(r'^decentro/bank/validate$', BankVerification.as_view(), name='decentro-bank-validation'),
    url(r'^decentro/account/activity$', AccountAcitivity.as_view(), name='account-acitvity')
]
