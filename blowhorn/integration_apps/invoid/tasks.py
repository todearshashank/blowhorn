import logging
from celery import shared_task
from django.conf import settings

from blowhorn import celery_app
from blowhorn.common.base_task import TransactionAwareTask
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT
from blowhorn.driver.models import Driver, DriverDocument, DriverDocumentPage
from blowhorn.vehicle.models import VehicleDocument, Vendor, VehicleDocumentPage
from blowhorn.integration_apps.invoid.invoid_api import Invoid

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

@celery_app.task(base=TransactionAwareTask, bind=True,
             max_retries=settings.CELERY_EVENT_MAX_RETRIES, ignore_result=False)
def send_document_for_verification_to_invoid(self, pid, identifier, doc_id, is_driver):
    if settings.DEBUG or settings.WEBSITE_BUILD == 'mzansigo':
        return

    if is_driver:
        ob = Driver.objects.get(id=pid)
    else:
        ob = Vendor.objects.get(id=pid)

    pages = None
    if identifier == DRIVER_DOCUMENT:
        doc = DriverDocument.objects.get(id=doc_id)
        pages = DriverDocumentPage.objects.filter(driver_document=doc
                    ).order_by('-uploaded_at','page_number')

    elif identifier == VEHICLE_DOCUMENT:
        doc = VehicleDocument.objects.get(id=doc_id)
        pages = VehicleDocumentPage.objects.filter(vehicle_document=doc
                    ).order_by('-uploaded_at','page_number')

    if pages and pages.exists():
        front = pages[0].file
        back = None
        if pages.count() > 1:
            back = pages[1].file

        if is_driver:
            invoid = Invoid(ob,identifier)
            result = invoid.id_documentation_verification(doc, front, back)
            logger.info(result)
    # result = invoid.official_database_verification(request, doc, doc_number)
