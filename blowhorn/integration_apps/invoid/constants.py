''' This file consists of all the constants 
    for the invoid api'''


ID_DOC_VERIFICATION_URL = 'https://ic.invoid.co'
OFFICIAL_DB_VERIFICATION_URL = 'https://gc.invoid.co'
CONSISTENCY_CHECK_URL = 'https://consistency.invoid.co'
BANK_ACCOUNT_VERIFICATION_URL = 'https://gc.invoid.co'

DOC_TYPE_DL = 'dl'
DOC_TYPE_PAN = 'pan'
DOC_TYPE_AADHAAR = 'aadhaar'
DOC_TYPE_RC = 'rc'
DOC_TYPE_BANKACCOUNT = 'bank-account'

DRIVER_INVOID_MANDATORY_DOCS = ['DL','PAN','Aadhaar','Photo']
VEHICLE_INVOID_MANDATORY_DOCS = ['RC']
INVOID_TEXT_ONLY_VALIDATION = ['RC','Bank_Passbook']

DOCUMENT_PREFERENCE = {
    'Photo' : 1,
    'PAN' : 2,
    'Aadhaar' : 3,    
    'DL' : 4,
    'RC' : 5,
    'COVIDPass' : 6,
    'Bank_Passbook' : 7
}

INVOID_DOCTYPE_MAPPING = {
    'DL' : DOC_TYPE_DL,
    'PAN' : DOC_TYPE_PAN,
    'Aadhaar' : DOC_TYPE_AADHAAR,
    'RC' : DOC_TYPE_RC,
    'Bank_Passbook' : DOC_TYPE_BANKACCOUNT
}

AADHAAR_VALID_FIELDS = ['gender','address','documentNumber','name']
PAN_VALID_FIELDS = ['parentName','panNumber','documentNumber','dateOfBirth']
DL_VALID_FIELDS = ['nonTransportValidity', 'transportValidity', 'licenseState', 'name', 
                        'documentNumber', 'issueDate', 'validTill']
RC_VALID_FIELDS = ['fitnessUpTo', 'insuranceUpTo', 'pollutionNorms', 
                    'registNumber', 'registrationDate','vehicleClass']
BANKACCOUNT_VALID_FIELDS = ['bank_name','name','account_number','ifsc_code']


AADHAAR_VALID_MAPPING = [
        {
            'gender' : 'gender',
            'name' : 'name',
            'address' : 'permanent_address',
            # 'dob' : 'date_of_birth'
        },
        {
            'documentNumber' : 'number'
        }

    ]

PAN_VALID_MAPPING = [
        {
            'parentName' : 'father_name',
            'panNumber' : 'pan',
            # 'name' : 'name',
            'dateOfBirth' : 'date_of_birth'
        },
        {
            'panNumber' : 'number'
        }
    ]

DL_VALID_MAPPING = [
        {
            'documentNumber' : 'driving_license_number',    
        },
        {
            'documentNumber' : 'number',
            'licenseState' : 'state',
            'nonTransportValidity' : 'non_transport_validity',
            'transportValidity' : 'transport_validity',
            'issueDate' : 'issue_date',
            'validTill' : 'expiry_date'
        }
    ]

RC_VALID_MAPPING = [
        {

        },
        {
            'registNumber' : 'number'
        }


    ]

BANKACCOUNT_VALID_MAPPING = [
        {

        },
        {
            'bank_name' : 'bank_name',
            'name' : 'account_name',
            'account_number' : 'account_number',
            'ifsc_code' : 'ifsc_code'
        }


    ]

VALID_DOC_FIELDS = {
    DOC_TYPE_AADHAAR : (AADHAAR_VALID_FIELDS,AADHAAR_VALID_MAPPING),
    DOC_TYPE_PAN : (PAN_VALID_FIELDS,PAN_VALID_MAPPING),
    DOC_TYPE_DL : (DL_VALID_FIELDS,DL_VALID_MAPPING),
    DOC_TYPE_BANKACCOUNT : (BANKACCOUNT_VALID_FIELDS,BANKACCOUNT_VALID_MAPPING),
    DOC_TYPE_RC : (RC_VALID_FIELDS, RC_VALID_MAPPING)
}

DISPLAY_MAPPING = {
    'gender' : 'Gender',
    'name' : 'Name',
    'permanent_address' : 'Permanent Address',
    'date_of_birth' : 'Date of birth',
    'father_name' : 'Father Name',
    'number' : 'Document No',
    'issue_date' : 'Date of issue',
    'state' : 'License State',
    'non_transport_validity' : 'Non Transport Validity',
    'transport_validity' : 'Transport Validity'
}



FIELD_VALUE_PRESENT = '1'
BANK_VERIFIED = 'VERIFIED'
VALID_DL = 'Licence Valid'




PAN_ADHAAR_DOC_CONFIDENCE_THRESHOLD = 80
PAN_ADHAAR_DOC_DOB_THRESHOLD = 0
PAN_ADHAAR_DOC_NAME_THRESHOLD = 75
PAN_ADHAAR_DOC_FACE_THRESHOLD = 0

GOVTCHECK_STATUS_SUCCESS = 'Success'
GOVTCHECK_VALID_STATUS = [2111,2110,2101,2100,2011,2010]
GOVTCHECK_AADHAAR_VALID_STATUS = [2111,2110,2101,2100,2011,2010]
RETAKE_CODE = [4002,2001,2000]
