from datetime import datetime
import re
import logging
import json
from django.db.utils import IntegrityError

from django.conf import settings
from config.settings.status_pipelines import ONBOARDING, INACTIVE
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT, ACTIVE, REJECT, VERIFICATION_PENDING
from blowhorn.document.models import DocumentType, DocumentEvent
from blowhorn.vehicle.models import Vendor, VehicleDocument
from blowhorn.driver.models import Driver, DriverDocument, BankAccount
from blowhorn.address.models import State
from blowhorn.common import sms_templates

from blowhorn.integration_apps.invoid.service import TapiWrapper
from blowhorn.integration_apps.invoid.constants import *
from blowhorn.integration_apps.invoid.driver_mixin import DriverAddUpdateMixin
from blowhorn.integration_apps.models import InvoidConsistency

class Invoid:

    def __init__(self, reference_object, identifier):
        '''
            Initialise using the reference object i.i driver/vendor
            and identifier i.e. driver/vehicle doucment
            Create a TapiWrapper instance for invoid 
        '''
        self.reference_id = reference_object.id
        self.reference_object = reference_object
        self.identifier = identifier
        self.taw = TapiWrapper('invoid', self.reference_id, identifier)
        self.taw.set_header_required_parameter(('authkey',))
        key = {'authkey' : settings.INVOID_TOKEN}
        self.taw.set_header(**key)

    def __get_document_type(self, document_type):
        '''
            This method takes the blowhorn document type
            and return the invoid document type as per
            the mapping in invoid_constancts file
        '''
        result = None
        
        if document_type:
            temp = document_type.code

            if temp in INVOID_DOCTYPE_MAPPING:
                result = INVOID_DOCTYPE_MAPPING[temp]

        return result

    def id_documentation_verification(self, doc, front, back = None):
        '''
            This method calls the invoid's file based id documentation verification,
            processes the invoid response and returns the response in required
            format
        '''
        result = {'success' : False, 'error_message' : None,
                    'exception' : None, 'retake' : False}

        try:
            doc_type = doc.document_type

            # Get the invoid document type mapped with blowhorn one
            invoid_doc_type = self.__get_document_type(doc_type)

            # dictionary having to be extracted fields intialized to spaces
            final_models = self._base_data_template(invoid_doc_type)
            # Change the format suitable for return in case of some error
            result['data'] = self._update_init_field_mapping(final_models)
            
            # Checks starting to validate the incoming document type as per the document
            if not invoid_doc_type:
                result['error_message'] = 'Please pass the proper document type'

            # elif invoid_doc_type == DOC_TYPE_RC:
            #     result['error_message'] = 'RC not allowed for image verification'

            elif invoid_doc_type == DOC_TYPE_DL and not(front and back):
                result['error_message'] = 'Front and back both image in case of driving license'

            else:
                # Initialize the payload for sending to invoid
                data = {
                    'front' : front,
                    'userId' : self.reference_id,
                    'doctype' : invoid_doc_type,
                    'handle' : 1,
                    'gc' : 1
                }

                # Add the back if present
                if back:
                    data['back'] = back

                # Set the payload and if success make the post call 
                set_payload_status = self.taw.set_payload(**data)
                if set_payload_status['success']:
                    post_call_status = self.taw.post(ID_DOC_VERIFICATION_URL, doc_type)

                    # In case of retake, set retake flag to True
                    # and update document status to reject
                    if post_call_status.get('retake',None):
                        result['retake'] = True
                        # DriverDocument.objects.filter(pk=doc.id).update(status = REJECT)

                    # If data received from invoid, proceed further
                    data = post_call_status.get('data',{})
                    if data and len(data) != 0:

                        try:
                            # Setup the final model dictionary to be updated and
                            # remove empty values
                            final_models = self.format_response_data(data, invoid_doc_type)
                            to_be_updated_models = self._check_if_update_required(final_models)

                            # Update the driver model data
                            driver_data = to_be_updated_models[0]
                            if len(driver_data) != 0:
                                if self.identifier == DRIVER_DOCUMENT:
                                    if isinstance(self.reference_object,Driver):
                                        if invoid_doc_type == DOC_TYPE_PAN:
                                            q = Driver.objects.filter(pan=driver_data['pan'])

                                            if q.exists():
                                                raise ValueError

                                        # driver_data['status'] = ONBOARDING
                                        Driver.objects.filter(pk=self.reference_id).update(
                                                **driver_data)

                            # Update the vehicle/driver document data &
                            # make document active in case of success and
                            # set success for response
                            document_data = to_be_updated_models[1]
                            if post_call_status['success']:
                                result['success']= True
                                document_data['status'] = ACTIVE

                            else:
                                result['error_message'] = post_call_status['exception']
                            
                            if (len(document_data) != 0) and post_call_status['success']:
                            
                                if self.identifier == DRIVER_DOCUMENT:
                                    DriverDocument.objects.filter(pk=doc.id).update(
                                            **document_data)

                                    DocumentEvent.objects.filter(driver_document__driver = 
                                                self.reference_object, driver_document__document_type = 
                                                doc_type, status = VERIFICATION_PENDING
                                                ).update(status = ACTIVE)

                                elif self.identifier == VEHICLE_DOCUMENT:
                                    VehicleDocument.objects.filter(pk=doc.id).update(
                                                    **document_data)

                                    DocumentEvent.objects.filter(vehicle_document__vehicle__driver = 
                                                self.reference_object, vehicle_document__document_type = 
                                                doc_type, status = VERIFICATION_PENDING
                                                ).update(status = ACTIVE)


                            # Invoke the final consitency check in case of success
                            if post_call_status['success']:
                                self.final_consistency_check()

                            # Update the data for sending in response
                            result['data'] = self._update_field_mapping(final_models)

                        except ValueError:
                            result['retake'] = True
                            # DriverDocument.objects.filter(pk=doc.id).update(status = REJECT)
                            result['error_message'] = 'User already exists with same PAN'
                            result['exception'] = 'User already exists with same PAN'

                    else:
                        # Set error message from invoid response
                        result['error_message'] = post_call_status['exception']

                else:
                    result = set_payload_status

        # Catch in case of some exception and set proper message
        except Exception as e:
            result['error_message'] = 'Please check the error in the id documentation verification method'
            result['exception'] = str(e)
            logging.error(str(e))

        return result

    # def official_database_verification(self, doc, id_number):
    #     '''
    #         This method calls the invoid official database verification
    #         as per the document type.
    #     '''
    #     result = {'success' : False, 'error_message' : None, 
    #                 'exception' : None, 'retake' : False}
        
    #     try:
    #         doc_type = doc.document_type

    #         # Get the invoid document type mapped with blowhorn one
    #         invoid_doc_type = self.__get_document_type(doc_type)

    #         # dictionary having to be extracted fields intialized to spaces
    #         final_models = self._base_data_template(invoid_doc_type)
    #         # Change the format suitable for return in case of some error
    #         result['data'] = self._update_init_field_mapping(final_models)

    #         # Checks starting to validate the incoming document type as per the document
    #         if not invoid_doc_type:
    #             result['error_message'] = 'Please pass the proper document type'

    #         elif not id_number:
    #             result['error_message'] = 'Please pass the document identification number'

    #         elif invoid_doc_type == DOC_TYPE_DL and self.reference_object.date_of_birth:
    #             result['error_message'] = 'Date of birth required in case of driving license'

    #         else:
    #             # Initialize the payload for sending to invoid
    #             data = {
    #                 'doctype' : invoid_doc_type,
    #                 'docNumber' : id_number
    #             }

    #             # Fetch the date of birth from driver model for dl enquiry
    #             dob_format_error = False
    #             if invoid_doc_type == DOC_TYPE_DL:
    #                 try:
    #                     date_of_birth = datetime.strptime(self.reference_object.date_of_birth, "%d-%m-%Y")
                    
    #                 except ValueError as e:
    #                     dob_format_error = True
    #                     result['error_message'] = 'Please pass the date of birth in dd-mm-yyyy format'
    #                     result['exception'] = str(e)
                    
    #                 if not dob_format_error:
    #                     data['dob'] = self.reference_object.date_of_birth

    #             if not dob_format_error:
    #                 # Set the invoid payload
    #                 set_payload_status = self.taw.set_payload(**data)
 
    #                 if set_payload_status['success']:
    #                     post_call_status = self.taw.post(OFFICIAL_DB_VERIFICATION_URL, doc_type)

    #                     # Incase of retake flag yes, update the driver/vehicle document rejected
    #                     if post_call_status.get('retake',None):
    #                         result['retake'] = True
    #                         if self.identifier == DRIVER_DOCUMENT:
    #                                 DriverDocument.objects.filter(pk=doc.id).update(status = REJECT)

    #                         elif self.identifier == VEHICLE_DOCUMENT:
    #                             VehicleDocument.objects.filter(pk=doc.id).update(status = REJECT)

    #                     data = post_call_status.get('data',{})
    #                     if data and len(data) != 0:

    #                         try:
    #                             # Setup the final model dictionary to be updated and 
    #                             # remove empty values
    #                             final_models = self.format_response_data(data, invoid_doc_type)
    #                             to_be_updated_models = self._check_if_update_required(final_models)

    #                             # Update the driver model data
    #                             driver_data = to_be_updated_models[0]
    #                             if len(driver_data) != 0:
    #                                 if self.identifier == DRIVER_DOCUMENT:
    #                                     if isinstance(self.reference_object,Driver):
    #                                         if invoid_doc_type == DOC_TYPE_PAN:
    #                                             q = Driver.objects.filter(pan=driver_data['pan'])

    #                                             if q.exists():
    #                                                 raise ValueError

    #                                         driver_data['status'] = ONBOARDING
    #                                         Driver.objects.filter(pk=self.reference_id).update(
    #                                                 **driver_data)

    #                             # Update the vehicle/driver document data &
    #                             # make document active in case of success and
    #                             # set success for response
    #                             document_data = to_be_updated_models[1]
    #                             if post_call_status['success']:
    #                                 result['success']= True
    #                                 document_data['status'] = ACTIVE

    #                             else:
    #                                 result['exception'] = post_call_status['exception']
    #                                 result['error_message'] = post_call_status['exception']

    #                             if (len(document_data) != 0) and post_call_status['success']:
    #                                 if self.identifier == DRIVER_DOCUMENT:
    #                                     DriverDocument.objects.filter(pk=doc.id).update(
    #                                                 **document_data)

    #                                 elif self.identifier == VEHICLE_DOCUMENT:
    #                                     VehicleDocument.objects.filter(pk=doc.id).update(
    #                                                 **document_data)

    #                             # Invoke the final consistency check in case
    #                             # of post call success
    #                             if post_call_status['success']:
    #                                 self.final_consistency_check()

    #                             result['data'] = self._update_field_mapping(final_models)

    #                         except ValueError:
    #                             result['retake'] = True
    #                             DriverDocument.objects.filter(pk=doc.id).update(status = REJECT)
    #                             result['error_message'] = 'User already exists with same PAN'
    #                             result['exception'] = 'User already exists with same PAN'

    #                     else:
    #                         # Set error message from invoid response
    #                         result['error_message'] = post_call_status['exception']

    #                 else:
    #                     result = set_payload_status

    #     # Catch in case of some exception and set proper message
    #     except Exception as e:
    #         result['error_message'] = 'Please check the error in the official database verification method'
    #         result['exception'] = str(e)

    #     return result


    def final_consistency_check(self):
        '''
            This method invokes the final consistency check
            for aadhaar,pan and selfie in case all the required
            five documents i.e. dal,rc,aadhaar,pan & selfie
            are active
        '''
        active_status = None

        # Check if the driver documents are active or not
        if isinstance(self.reference_object,Driver):
            reference_type = 'Driver'
            active_status, driver_documents = DriverAddUpdateMixin(
                            ).check_if_driver_documents_active(self.reference_object)

        # Proceed for the consistency check in case docs active
        if active_status:
            final_check = self.adhaar_pan_consistency(reference_type, 
                            driver_documents['photo'],
                            driver_documents['aadhaar'],
                            driver_documents['pan'])
            
            # Make driver active if check passes else change status to onboarding
            if final_check['success']:
                if isinstance(self.reference_object,Driver):
                    DriverAddUpdateMixin().make_driver_active(self.reference_object,
                                        result['name'])
                    message = sms_templates.template_driver_active
                    self.reference_object.user.send_sms(message['text'], message)
            else:
                self.reference_object.update(status=ONBOARDING)


    def adhaar_pan_consistency(self, reftype, selfie, aadhaar, pan):
        '''
            This method checks the aadhaar, pan and selfie consistency &
            validates the name, year of birth and face similarity percentage along
            with document confidence against the set percentage and gives
            a flag for final pass.
        '''

        # Initialization of fields
        result = {'success' : False, 'error_message' : None,
                    'exception' : None, 'name' : None}
        icm = {'reference_id' : self.reference_id,
                'reference_type' : reftype,
                'consistency_result' : False}

        try:
            # Proceed further if all document present
            if not (selfie or aadhaar or pan):
                result['error_message'] = 'Please pass all the required docs i.e. selfie, adhaar and pan image'
            else:
                data = {
                    'userId' : self.reference_id,
                    'card0_front' : aadhaar,
                    'card1_front' : pan,
                    'selfie' : selfie,
                    'card0_doctype' : DOC_TYPE_AADHAAR,
                    'card1_doctype' : DOC_TYPE_PAN,
                    'url_stat' : 0
                }

                # Set the result for 'Photo' document type
                doctype = DocumentType.objects.get(code = 'Photo')
                set_payload_status = self.taw.set_payload(**data)

                # Make a post call for the check
                if set_payload_status['success']:
                    post_call_status = self.taw.post(CONSISTENCY_CHECK_URL,doctype)
                    
                    # Start extracting the check data for processing in case of success
                    if post_call_status['success']:
                        consistency_data = post_call_status['data']
                        confidence_result = None
                        similarity_result = None

                        # Go for document confidence validation in case card details present
                        if 'card0' in consistency_data and 'card1' in consistency_data:
                            confidence_result,confresult,name = self.__pan_adhaar_document_confidence(
                                                                consistency_data)
                            if name:
                                result['name'] = name

                            icm.update(confresult)
                            icm['overall_document_confidence'] = confidence_result

                        # Go for similarity validation
                        similarity_result,simresult = self.__pan_adhaar_similarity_confidence(
                                                            consistency_data)
                        icm.update(simresult)
                        icm['overall_similarity_confidence'] = similarity_result

                        if confidence_result and similarity_result:
                            # Set the final status True if both check passed
                            icm['consistency_result'] = True
                            result['success'] = True

                        ic = InvoidConsistency(**icm)
                        ic.save()
                            # In case of failed check, update the document status
                            # to verification pending/rejecta as per document type
                            # if isinstance(self.reference_object,Driver):
                            #     DriverAddUpdateMixin(
                            #         ).change_document_status_for_consistency_fail(self.reference_object)

                    else:
                        # Set error message from invoid response
                        result['error_message'] = post_call_status['exception']
                else:
                    result = set_payload_status

        # Catch in case of some exception and set proper message
        except Exception as e:
            result['error_message'] = 'Please check the error in the adhaar pan consistency method'
            result['exception'] = str(e)

        return result

    def __pan_adhaar_document_confidence(self, input_json):
        '''
            This method validates the document confidence
            from input json.
            Returns True/False along with the document
            confidence in dictionary.
        '''
        confidence_dict = {}
        name = None
        name_flag = False
        result = False,confidence_dict,name
        
        if type(input_json) == str:
            t = json.loads(input_json)
            input_json = t

        # Proceed further in case the document details are present
        if 'data' in input_json['card0'] and 'data' in input_json['card1']:
            c0 = input_json['card0']['data']
            c1 = input_json['card1']['data']

            if len(c0) !=0 and len(c1) != 0:
                if 'name' in c0:
                    tempdict = c0['name']
                    if str(tempdict['status']) == FIELD_VALUE_PRESENT:
                        name = tempdict['value']
                        name_flag = True

                if not name_flag and 'name' in c1:
                    tempdict = c1['name']
                    if str(tempdict['status']) == FIELD_VALUE_PRESENT:
                        name = tempdict['value']
                        name_flag = True

                if 'documentType' in c0 and 'documentType' in c1:
                    doc_confidence0 = float(c0['documentType']['docConfidence'])
                    doc_confidence1 = float(c1['documentType']['docConfidence'])

                    confidence_dict = {
                            'document_confidence_1' : doc_confidence0,
                            'document_confidence_2' : doc_confidence1
                        }

                    # Set True if passed
                    if doc_confidence0 >= PAN_ADHAAR_DOC_CONFIDENCE_THRESHOLD and \
                        doc_confidence1 >= PAN_ADHAAR_DOC_CONFIDENCE_THRESHOLD:
                        result = True,confidence_dict,name
        
        return result

    def __pan_adhaar_similarity_confidence(self, input_json):
        '''
            This method validates the name, yob and face 
            similarity from input json.
            Returns True/False along with the document
            similarity result in dictionary.
        '''
        result = False
        yob_similarity = 0.0
        name_similarity = 0.0
        c0_face_similarity = 0.0
        c1_face_similarity = 0.0
        similarity_dict = {}

        if type(input_json) == str:
            t = json.loads(input_json)
            input_json = t

        # Extract the similarity details from input
        if 'yobSimilarity' in input_json:
            yob_similarity = float(input_json['yobSimilarity'][0].get('similarity',0.0))

        if 'nameSimilarity' in input_json:
            name_similarity = float(input_json['nameSimilarity'][0].get('similarity',0.0))

        if 'faceSimilarity' in input_json:
            face_similarity = input_json['faceSimilarity']
            if len(face_similarity) == 2:
                c0_face_similarity = float(face_similarity[0]['similarity'])
                c1_face_similarity = float(face_similarity[1]['similarity'])

        # Create the similarity dictionary
        similarity_dict = {
            'name_similarity' : name_similarity,
            'face_similarity_1' : c0_face_similarity,
            'face_similarity_2' : c1_face_similarity,
            'yob_similarity' : yob_similarity
        }

        # Set the validation result of each check
        r1,r2,r3 = False,False,False
        if yob_similarity >= PAN_ADHAAR_DOC_DOB_THRESHOLD:
            r1 = True

        if name_similarity >= PAN_ADHAAR_DOC_NAME_THRESHOLD:
            r2 = True
        
        if c0_face_similarity >= PAN_ADHAAR_DOC_FACE_THRESHOLD and \
            c1_face_similarity >= PAN_ADHAAR_DOC_FACE_THRESHOLD:
            r3 = True

        # Set the final result
        result = r1 and r2 and r3
        
        return result,similarity_dict

    def bank_account_verification(self, account_number, ifsc):
        '''
            This method verifies the bank account number and ifsc code.
        '''
        result = {'success' : False, 'error_message' : None, 'exception' : None}

        try:
            # Proceed further if account number and ifsc code is present
            if not account_number or not ifsc:
                result['error_message'] = 'Please pass the account number and ifsc code'

            else:
                # Get the invoid doucment type
                doctype = DocumentType.objects.get(identifier=DRIVER_DOCUMENT, code='Bank_Passbook')
                invoid_doc_type = self.__get_document_type(doctype)
                
                # Validate the account number and ifsc code
                ifsc_pattern_check = re.match('[a-zA-Z]{4}0[0-9]{6}',ifsc)
                bank_account_check = re.match('[0-9]{9,18}',str(account_number))

                if not ifsc_pattern_check:
                    result['error_message'] = 'Please pass a valid ifsc code'

                elif not bank_account_check:
                    result['error_message'] = 'Please pass a valid account number'

                else:
                    # If validation passes, set the invoid payload
                    data = {
                        'docType' : invoid_doc_type,
                        'docNumber' : account_number,
                        'ifsc' : ifsc
                    }

                    set_payload_status = self.taw.set_payload(**data)

                    if set_payload_status['success']:
                        post_call_status = self.taw.post(BANK_ACCOUNT_VERIFICATION_URL, doctype)
                        data = post_call_status.get('data',{})

                        if isinstance(data,str):
                            data = json.loads(data)
                        
                        data.update({
                                'bank_name' : {'status' : 1, 'value' : ifsc[:4].upper()},
                                'account_number' : {'status' : 1, 'value' : str(account_number)},
                                'ifsc_code' : {'status' : 1, 'value' : ifsc}
                            })
                        
                        # If data present, update the details
                        if data and len(data) != 0:
                            # Setup the final model dictionary to be updated and 
                            # remove empty values
                            final_models = self.format_response_data(data, invoid_doc_type)
                            to_be_updated_models = self._check_if_update_required(final_models)

                            bank_data = to_be_updated_models[1]
                            
                            # Update BankAccount table and create mapping with the driver
                            if len(bank_data) != 0 and post_call_status['success']:
                                ba, created = BankAccount.objects.get_or_create(
                                        account_name = bank_data['account_name'],
                                        account_number = bank_data['account_number'],
                                        ifsc_code = bank_data['ifsc_code'])
                                action = 'Mapped-FO'

                                if isinstance(self.reference_object,Driver):
                                    ba.add_bank_account_history(ba, self.reference_object.user, 
                                                action, driver = self.reference_object)

                                elif isinstance(self.reference_object,Vendor):
                                    ba.add_bank_account_history(ba, self.reference_object.user, 
                                                action, vendor = self.reference_object)

                                # Update the driver/vendor model with the
                                # bank account
                                driver_data = {'bank_account' : ba}
                                if isinstance(self.reference_object,Driver):
                                    Driver.objects.filter(pk=self.reference_id).update(
                                                **driver_data)
                                
                                    doctype = DocumentType.objects.get(code = 'Bank_Passbook')
                                    DriverDocument.objects.create(document_type = doctype,
                                            driver = self.reference_object,
                                            number = str(account_number),
                                            status = ACTIVE)

                                    result['success']= True
                            
                            else:
                                result = post_call_status

                        else:
                            result = post_call_status
                    
                    else:
                        result = set_payload_status

        # Catch in case of some exception and set proper message
        except Exception as e:
            result['error_message'] = 'Please check the error in bank account verification method ' + str(e) 
            result['exception'] = str(e)
            logging.error(str(e))

        return result

    def format_response_data(self,input_data,doc_type):
        '''
            This method processes the invoid response and 
            extracts the required fields and validates &
            set the values as per the document type

            Output: [{1},{2}] 
            where 1 refers to appropriate driver/vendor fields & values
            2 refers to appropriate driver/vehicle document fields & values
        '''

        # Fetch the to be extracted fields and mappings as per
        # the document type and initialize
        (field_list,field_mapping) = VALID_DOC_FIELDS[doc_type]
        final_model = []
        for mapping in field_mapping:
            temp = {y:'' for x,y in mapping.items()}
            final_model.append(temp)

        # If input type is still string, change to dictionary
        if type(input_data) == str:
            input_data = json.loads(input_data)

        # Loop through each field to be extracted
        for eachfield in field_list:
            final_value = None

            if eachfield in input_data:
                value_dict = input_data[eachfield]

                # For documents other than rc since invoid response is diff for rc only
                if isinstance(value_dict,dict) and len(value_dict) == 2:
                    status = value_dict.get('status', None)
                    value = value_dict.get('value', None)
                    number = value_dict.get('number', None)
                    message = value_dict.get('message', None)

                    # Set the final value 
                    final_value = value or number or message
                    
                    # If status flag is active (1)
                    if final_value and status and (str(status) == FIELD_VALUE_PRESENT):
                        # Field mapping - [{Driver model fields},{Driver/Vehicle document fields}]
                        for index in range(2):
                            mapping = field_mapping[index]
                            
                            # If field not in mapping, go for next
                            if eachfield not in mapping:
                                continue

                            new_field = mapping[eachfield]
                            
                            # Add/Save the fields to other models if required 
                            if new_field == 'permanent_address':
                                final_value = DriverAddUpdateMixin._add_address(final_value)

                            if new_field == 'state':
                                state_str_lst = [s.capitalize() for s in final_value.split()]
                                search_value = ' '.join(state_str_lst)
                                final_value = State.objects.get(name = search_value)

                            if new_field == 'date_of_birth':
                                try:
                                    date = datetime.strptime(final_value,'%d-%m-%Y').date()
                                    pattern = '%Y-%m-%d'
                                    date_format = date.strftime(pattern)
                                    final_value = datetime.strptime(date_format,
                                                    pattern).date()
                                except ValueError as e:
                                    pass

                            # Format the date fields as per blowhorn models
                            flds = ['non_transport_validity','transport_validity',
                                        'issue_date', 'expiry_date']
                            if new_field in flds:
                                if isinstance(final_value,list):
                                    final_value = final_value[0]
                                
                                split_range = final_value.split(' to ')
                                i = len(split_range)
                                try:
                                    temp = datetime.strptime(split_range[i-1],'%d-%m-%Y').date()
                                except Exception as e:
                                    temp = temp = datetime.strptime(split_range[i-1],'%d-%b-%Y').date()
                                final_value = temp
                            
                            # Set the final value in case field to be extracted
                            if new_field in final_model[index]:
                                final_model[index][new_field] = final_value

                # In case of rc, set the fields for vehicle model
                elif (doc_type == DOC_TYPE_RC):
                    final_model = [{},{}]
                    if len(input_data) != 0:
                        final_model[1]['invoid_payload'] = input_data
                        final_model[1]['number'] = input_data['registrationNumber']
                    break

        return final_model

    # def data_update(self, data, doc_type, is_number_updated, status_update):
    #     invoid_doc_type = self.__get_document_type(doc_type)
    #     final_models = self._base_data_template(invoid_doc_type)

    #     for model in final_models:
    #         for key,val in model.items():

    #             if key in model:
    #                 model[key] = val

    #         self._remove_empty_fields(model)

    #     reference_data = final_models[0]
    #     document_data = final_models[1]

    #     if is_number_updated:
    #         if invoid_doc_type == DOC_TYPE_PAN:
    #             reference_data['pan'] = data.get('number')

    #         elif invoid_doc_type == DOC_TYPE_DL:
    #             reference_data['driving_license_number'] = data.get('number')

    #         else:
    #             pass

    #     if status_update:
    #         document_data['status'] = ACTIVE

    #     if self.identifier == DRIVER_DOCUMENT:

    #         if len(reference_data) and isinstance(self.reference_object,Driver):
    #             Driver.objects.filter(pk=self.reference_id).update(**reference_data)

    #         if len(document_data) != 0 and isinstance(self.reference_object,Driver):
    #             DriverDocument.objects.filter(driver_id=self.reference_id).update(
    #                                             **document_data)

    def _base_data_template(self, doc_type):
        '''
            This method gets the valid fields and mapping
            as per the document type and initializes the fields.
            (The mapping defines the fields to extracted from the response)
        '''
        (field_list,field_mapping) = VALID_DOC_FIELDS[doc_type]

        final_model = {}
        for mapping in field_mapping:
            temp = {y:'' for x,y in mapping.items()}
            final_model.update(temp)

        return final_model


    def _check_if_update_required(self, final_model_list):
        '''
            This method removes the keys having no value from
            the input list [driver model, driver/vehicle document]
        '''
        result = []

        for model_dict in final_model_list:
            temp = {}
            for key,value in model_dict.items():
                if value != '':
                    temp[key] = value

            result.append(temp)

        return result

    def _remove_empty_fields(self, data):
        '''
            This method removes the keys having no value from
            the input dictionary
        '''
        result = {}
        for key,value in data.items():
            if value != '':
                result[key] = value

        return result

    def _update_field_mapping(self, model_list):
        '''
            This method formats the data for final response
            required by front end.

            From : {key1 : value1, key2 : value2}
            To : {
                {'key' : key1, 'value' : value1, 'display' : 'To be displayed name'},
                {'key' : key2, 'value' : value2, 'display' : 'To be displayed name'}
            }
        '''
        result = []
        dict_data = {}
        dict_data.update(model_list[0])
        dict_data.update(model_list[1])

        # Update the field's value for sending in response
        self._update_field_values(dict_data)
        for k,v in dict_data.items():
            display = DISPLAY_MAPPING.get(k,None)

            if display:
                t = {}
                t['key'] = k
                t['display'] = display
                t['value'] = v
                result.append(t)

        return result

    def _update_init_field_mapping(self, model_dict):
        '''
            This method formats the initial data for final response
            required by front end in case of some error.

            From : {key1 : value1, key2 : value2}
            To : {
                {'key' : key1, 'value' : value1, 'display' : 'To be displayed name'},
                {'key' : key2, 'value' : value2, 'display' : 'To be displayed name'}
            }
        '''
        result = []
        for k,v in model_dict.items():
            display = DISPLAY_MAPPING.get(k,None)

            if display:
                t = {}
                t['key'] = k
                t['display'] = display
                t['value'] = v
                result.append(t)

        return result
   
    def _update_field_values(self, data):
        '''
            This method updates the fields' value 
            for sending in response
        '''
        if 'permanent_address' in data:
            address  = data['permanent_address']

            if address != '':
                address_str = address.summary

                if ",," in address_str:
                    address_str = address_str.replace(",,",",")
                
                data['permanent_address'] = address_str

        if 'state' in data:
            state = data['state']
            data['state'] = state.name

        if 'invoid_payload' in data:
            data.pop('invoid_payload')
