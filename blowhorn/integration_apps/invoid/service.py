import requests
import json
import secrets 
import string
import logging
from datetime import datetime

from rest_framework import status

from blowhorn.integration_apps.models import ApiCallSaver
from blowhorn.integration_apps.invoid.constants import (GOVTCHECK_STATUS_SUCCESS, 
        GOVTCHECK_VALID_STATUS, BANK_VERIFIED, RETAKE_CODE,
        GOVTCHECK_AADHAAR_VALID_STATUS, VALID_DL)

class TapiWrapper(object):
    """This class is wrapper for the third party api integrations"""
    
    def __init__(self, name, reference_id, identifier):
        self.header = {}
        self.payload = {}
        self.required_header_parm = set()
        self.required_payload_parm = set()
        self.name = name
        self.reference_id = reference_id
        self.identifier = identifier

    def get_required_parameters(self):
        result = {'header' : self.required_header_parm, 
                    'payload' : self.required_payload_parm
                }
        return result

    def get_header(self):
        return self.header

    def get_payload(self):
        return self.payload

    def set_header_required_parameter(self, *args):
        result = {'success' : False, 'error_message' : None, 'exception' : None}

        try:
            if len(args) == 0:
                result['error_message'] = 'Please provide some required parameters for header'

            else:
                self.required_header_parm.extend(set(args))
                result['success'] = True

        except Exception as e:
            result['error_message'] = 'Please check the set_header_required_parameter method/input of wrapper'
            result['exception'] = str(e)

        return result

    def set_payload_required_parameter(self, *args):
        result = {'success' : False, 'error_message' : None, 'exception' : None}

        try:
            if len(kwargs) == 0:
                result['error_message'] = 'Please provide some required parameters for the payload'

            else:
                self.required_payload_parm.extend(set(args))
                result['success'] = True

        except Exception as e:
            result['error_message'] = 'Please check the set_payload_required_parameter method/input of wrapper'
            result['exception'] = str(e)

        return result


    def set_header(self, **kwargs): 
        result = {'success' : False, 'error_message' : None, 'exception' : None}

        try:
            if len(kwargs) == 0:
                result['error_message'] = 'Please provide some required parameters for the header'

            elif len(self.required_header_parm) == 0:
                self.header.update(kwargs)
                result['success'] = True

            else:
                input_parm = set(kwargs.keys())
                required_parm_check_success = self.required_header_parm.issubset(input_parm)

                if required_parm_check_success:
                    self.header.update(kwargs)
                    result['success'] = True

                else:
                    missing_parm = self.required_header_parm - input_parm
                    msg = 'Please provide all the required parameters for the header {}'.format(*missing_parm)
                    result['error_message'] = msg
                
        except Exception as e:
            result['error_message'] = 'Please check the set_header method/input of wrapper'
            result['exception'] = str(e)

        return result

    def set_payload(self, **kwargs):
        result = {'success' : False, 'error_message' : None, 'exception' : None}

        try:
            if len(kwargs) == 0:
                result['error_message'] = 'Please provide some required parameters for the payload'

            elif len(self.required_payload_parm) == 0:
                self.payload.update(kwargs)
                result['success'] = True

            else:
                input_parm = set(kwargs.keys())
                required_parm_check_success = self.required_payload_parm.issubset(input_parm)

                if required_parm_check_success:
                    self.payload.update(kwargs)
                    result['success'] = True

                else:
                    missing_parm = self.required_payload_parm - input_parm
                    msg = 'Please provide all the required parameters for the payload {}'.format(*missing_parm)
                    result['error_message'] = msg
                
        except Exception as e:
            result['error_message'] = 'Please check the set_payload method/input of wrapper'
            result['exception'] = str(e)

        return result   

    def post(self, url, doctype = None):
        result = {'success' : False, 'error_message' : None, 'exception' : None}
        
        try:
            result = self.__post_call(url, doctype)

        except Exception as e:
            result['error_message'] = 'Please check the exception in api post call'
            result['exception'] = str(e)

        return result

    def __post_call(self, url, doctype=None):
        result = {'success' : False, 'error_message' : None, 'exception' : None,
                    'data' : '' , 'retake' : False}   
        
        files = {}
        
        if 'front' in self.payload:
            files['front'] = self.payload.get('front').open('rb')
            self.payload.pop('front')
            
        if 'back' in self.payload:
            files['back'] = self.payload.get('back').open('rb')
            self.payload.pop('back')

        if 'selfie' in self.payload:
            files['selfie'] = self.payload.get('selfie').open('rb')
            self.payload.pop('selfie')

        if 'card0_front' in self.payload:
            files['card0_front'] = self.payload.get('card0_front').open('rb')
            self.payload.pop('card0_front')

        if 'card1_front' in self.payload:
            files['card1_front'] = self.payload.get('card1_front').open('rb')
            self.payload.pop('card1_front')

        response = requests.request('POST', url, data=self.payload,
                        headers=self.header, files = files)

        result['success'],result['data'],result['exception'],result['retake'] = \
                self.response_processing(url,doctype,response)

        return result

    def response_processing(self, url, doctype, response):
        success = False
        exception = None
        call_saver = {}
        ocr_data = {}
        govt_check_passed = False
        valid_200_call = False
        valid_status = False
        status_code = 0
        retake_flag = False
        call_saver['api_name'] = self.name
        call_saver['reference_id'] = self.reference_id
        call_saver['identifier'] = self.identifier
        call_saver['doctype'] = doctype
        call_saver['api_url'] = url
        call_saver['request_rc'] = response.status_code
        call_saver['govtcheck_status'] = False
        call_saver['api_rc'] = response.status_code
        call_saver['in_govt_status'] = False
        call_saver['transaction_id'] = self.__generate_random_txnid(doctype)
        inittext = {'response_html' : str(response.text)}
        call_saver['response_text'] = inittext


        if int(response.status_code) == status.HTTP_200_OK:
            valid_200_call = True
        
        if valid_200_call and len(response.text) !=0:
            jsondata = response.json()

            if type(jsondata) == list:
                rd = jsondata[0]
            else:
                rd = jsondata

            ocr_data = {}
            
            if 'data' in rd:
                ocr_data = rd['data']

            if 'result' in ocr_data:
                t = ocr_data['result']
                ocr_data = t

            if 'govtDetails' in ocr_data:
                temp = ocr_data.pop('govtDetails')
                
                if 'govtCheckStatus' in temp:
                    if temp['govtCheckStatus'] == GOVTCHECK_STATUS_SUCCESS:
                        call_saver['in_govt_status'] = True
                        govt_check_passed = True

                if 'data' in temp:
                    interim = temp['data']

                    if 'result' in interim:
                        ocr_data.update(interim['result'])

            if 'govtCheckStatus' in rd:
                if rd['govtCheckStatus'] == GOVTCHECK_STATUS_SUCCESS:
                    call_saver['in_govt_status'] = True
                    govt_check_passed = True

            if 'status' in rd:
                status_code = int(rd['status'])
                call_saver['api_rc'] = status_code

                if status_code in GOVTCHECK_VALID_STATUS:
                    valid_status = True

            if 'message' in rd:
                exception = rd['message']  

            if govt_check_passed and valid_status:
                success = True
                call_saver['govtcheck_status'] = True     

            if 'transactionId' in rd:
                call_saver['transaction_id'] = rd['transactionId']

            if 'image' in ocr_data:
                ocr_data['image'] = ''
            
            call_saver['response_text'] = rd
            if len(ocr_data) != 0:
                call_saver['response_text'] = ocr_data

            if doctype.code == 'Bank_Passbook' and 'status' in call_saver['response_text']:
                t = call_saver['response_text']['status']
                
                if isinstance(t,dict):
                    bank_status = t.get('value',None)
                else:
                    bank_status = t
                            
                if bank_status == BANK_VERIFIED:
                    success = True
                    call_saver['govtcheck_status'] = True

            if doctype.code == 'RC' and govt_check_passed:
                success = True
                call_saver['govtcheck_status'] = True

            if doctype.code == 'Aadhaar' and status_code in GOVTCHECK_AADHAAR_VALID_STATUS:
                success = True
                call_saver['govtcheck_status'] = True

            if doctype.code == 'DL' and not success:
                validation_message = call_saver['response_text'].get('validation',None)
                if validation_message:
                    message = validation_message.get('message',None)

                    if message and (message == VALID_DL):
                        success = True
                        call_saver['govtcheck_status'] = True

            if status_code in RETAKE_CODE:
                success = False
                call_saver['response_text'] = {}
                call_saver['govtcheck_status'] = False
                retake_flag = True

            if doctype.code == 'Photo':
                success = True
                call_saver['govtcheck_status'] = True

        self.__save_call(call_saver)

        return success,call_saver['response_text'],exception,retake_flag

    def __save_call(self,call_saver):
        acs = ApiCallSaver(**call_saver)
        t = acs.save()

    def __generate_random_txnid(self, doc_type):
        random_stirng = res = ''.join(secrets.choice(string.ascii_uppercase + string.digits) 
                                                  for i in range(10))
        
        timestamp = int(datetime.now().timestamp())
        
        rl = [random_stirng,str(self.reference_id),str(doc_type),str(timestamp)]
        
        return '-'.join(rl)