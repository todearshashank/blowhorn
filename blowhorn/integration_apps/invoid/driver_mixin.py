import re
from datetime import datetime
from django.db import transaction

from blowhorn.driver.models import Driver, DriverDocument, DriverVehicleMap, DriverHistory
from blowhorn.address import utils as address_utils
from blowhorn.vehicle.models import VehicleDocument
from blowhorn.apps.operation_app.v3.helper import *
from rest_framework.response import Response
from rest_framework import status
from blowhorn.integration_apps.invoid.constants import DRIVER_INVOID_MANDATORY_DOCS,VEHICLE_INVOID_MANDATORY_DOCS

class DriverAddUpdateMixin(object):

    def check_if_driver_documents_active(self, driver):
        '''
            This method checks if the drver's documents i.e.
            pan, aadhaar, dl, rc and photo are active or not
            along with the doclist of pan, aadhaar and photo
            to start the consistency check.
        '''
        result = False
        doclist = {}
        active_doc = 0

        # Proceed further if driver not already active, inactive or blacklisted
        if driver.status not in [StatusPipeline.ACTIVE,
                                 StatusPipeline.INACTIVE, StatusPipeline.BLACKLISTED]:

            # Get the driver's driver and vehicle documents
            driver_documents = DriverDocument.objects.filter(driver=driver.pk, 
                                    status = StatusPipeline.ACTIVE)
            vehicle_documents = VehicleDocument.objects.filter(vehicle__driver=driver,
                                    status = StatusPipeline.ACTIVE)

            if not driver_documents.exists() or not vehicle_documents.exists():
                return result,doclist

            # For each doucment check if its active
            for code in DRIVER_INVOID_MANDATORY_DOCS:
                query = driver_documents.filter(document_type__code = code)

                if query.exists():
                    document = query.latest('date_uploaded')
                    active_doc += 1

                    if code == 'Aadhaar':
                        doclist['aadhaar'] = document.file

                    if code == 'PAN':
                        doclist['pan'] = document.file

                    if code == 'Photo':
                        doclist['photo'] = document.file

            for code in VEHICLE_INVOID_MANDATORY_DOCS:
                query = vehicle_documents.filter(document_type__code = code)

                if query.exists():
                    document = query.latest('date_uploaded')
                    active_doc += 1

            # If active documents are >= 5 which is the basic required
            count_of_docs = len(DRIVER_INVOID_MANDATORY_DOCS) + len(VEHICLE_INVOID_MANDATORY_DOCS)
            if active_doc >= count_of_docs:
                result = True

        return result,doclist

    def change_document_status_for_consistency_fail(self, driver):
        '''
            This method changes the status of pan, aadhaar and photo 
            to verification pending, verification pending and rejected 
            respectively in case the consistency check fails and
            creates an event for manual verification via cosmos.
        '''

        from blowhorn.driver.util import create_document_event
        dd = DriverDocument.objects.filter(driver=driver)
        event_data = {'created_by' : driver.user, 'driver' : driver}
        document_type_code = ['Photo','Aadhaar','PAN']

        # For each of the document type code
        for code in document_type_code:
            docs = dd.filter(document_type__code = code)

            if docs.exists():
                # Get the lastes document if it exists
                doc = docs.latest('date_uploaded')
                event_data['driver_document'] = doc

                if doc.document_type.code == 'Photo':
                    # Change the photo status to rejected
                    doc.status = REJECT
                else:
                    # Change the status of other documents to verification pending
                    doc.status = VERIFICATION_PENDING
                
                # Save the document status and create the document event
                doc.save()
                create_document_event(**event_data)

    def make_driver_active(self, driver, name=None):
        '''
            This method makes the driver active.
        '''

        driver.status = StatusPipeline.ACTIVE
        if name:
            driver.name = name
        driver.save()

    def __post_creation_action(self, params, driver, vehicle):
        """
            Tasks which are needed to be performed once the driver is
            created/updated successfully
        """

        # Update the vehicle details(* if any changes are made) in the driver
        # vehicle map
        if vehicle:
            if not driver.driver_vehicle or (
                    driver.driver_vehicle != vehicle.registration_certificate_number):
                DriverVehicleMap.objects.create(driver=driver, vehicle=vehicle)

        # Update the preferred location if needed
        if params.get('address_model', []):
            add_preferred_location(params['address_model'], driver)

        # Update the driver referral if needed
        if driver.referred_by and driver.status == StatusPipeline.ACTIVE:
            DriverReferrals.objects.filter(driver=driver).update(
                activation_timestamp=datetime.datetime.now())

        response_dict = {'driver_id': driver.id,
                         'vehicle_id': vehicle.id if vehicle else '-1',
                         'status': driver.status}

        return response_dict

    def _generate_driver_history(self, driver, driver_new_data, vendor, user):
        """
            Make entries in the driver history table for the modified data
        """

        driver_list = []
        DRIVER_FIELD_ALLOWED_FOR_UPDATE = []

        for field in driver._meta.fields:
            field_name = field.__dict__.get('name')
            if field_name not in DRIVER_FIELD_ALLOWED_FOR_UPDATE:
                old_value = str(getattr(driver, field_name))
                current_value = str(driver_new_data.get(field_name, None))
                if field_name == 'owner_details':
                    old_value = field.value_from_object(driver)
                    current_value = vendor

                if old_value != current_value:
                    if field_name == 'owner_details':
                        old_value = "%s | %s" % (
                            driver.owner_details.name,
                            driver.owner_details.phone_number) if \
                            driver.owner_details else None
                        current_value = "%s | %s" % (
                            vendor.name, vendor.phone_number) if \
                            vendor else None

                    driver_list.append(DriverHistory(field=field_name,
                                                     old_value=old_value,
                                                     new_value=current_value,
                                                     user=user, driver=driver))

        return driver_list

    def _add_address(address_str):
        '''
            Formats the address string as per the address model
            and returns in dictionary format
        '''
        state_list = address_utils.get_state_list()
        address_data = {'line1' : address_str}
        
        for state in state_list:
            if state.lower() in address_str.lower():
                address_data['state'] = state
                temp = re.split(state, address_str, 
                        maxsplit=1, flags=re.IGNORECASE)
                address_data['line1'] = temp[0]
                break

        pincode  = address_utils.AddressUtil(
                        ).get_postcode_from_address_str(address_str)
        if pincode:
            address_data['postcode'] = pincode
        
        address = add_address_details_for_driver(address_data)

        return address

    def delete_drivervehicle_mapping(self, vehicle, action_owner):
        mapping = DriverVehicleMap.objects.filter(vehicle=vehicle).first()

        if mapping:
            if mapping.driver:
                mapping.driver.driver_vehicle = None
                mapping.driver.modified_by = action_owner
                mapping.driver.modified_date = timezone.now()
                mapping.driver.save()
            mapping.delete()

    def _add_vehicle(user_id, vehicle_dict):
        '''
            Format the vehicle data as per the model
            and return in dictionary format
        '''    
        if not isinstance(vehicle_dict,dict):
            return success

        vehicle_number = vehicle_dict.get('registration_certificate_number')
        vehicles = Vehicle.objects.filter(registration_certificate_number=vehicle_number)

        with transaction.atomic():
            if vehicles.exists():
                vehicle = vehicles.first()
            
            else:
                vehicle_data = {
                    'registration_certificate_number': vehicle_number,
                    'vehicle_model': vehicle_dict.get('vehicle_model'),
                    'body_type': vehicle_dict.get('body_type'),
                    'model_year': vehicle_dict.get('model_year')
                }
                
                try:
                    vehicle = add_vehicle(
                        vehicle_params=vehicle_data,
                        is_update=False
                    )
                except BaseException as be:
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                        data='Failed to map vehicle to driver')

            try:
                vehicle_map = DriverVehicleMap.objects.create(**{
                    'driver_id': user_id,
                    'vehicle': vehicle
                })

            except BaseException as be:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                    data=('Failed to map vehicle to driver'))

            is_owner = vehicle_dict.get('is_owner',True)
            driver_data = {
                'driver_vehicle': vehicle_number,
                'modified_date': timezone.now(),
                'own_vehicle': is_owner
            }

            try:
                if not is_owner:
                    vendor_details = {
                        'name': data.get('owner_name'),
                        'phone_number': data.get('owner_phonenumber'),
                        'pan': data.get('owner_pan')
                    }
                    
                    vendor = add_vendor(vendor_details)
                    driver_data['owner_details'] = vendor

                updated_driver = Driver.objects.filter(pk=user_id).update(**driver_data)
                logger.info('Vehicle has been mapped to driver: %s', updated_driver)

            except BaseException as be:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                    data=('Failed to create vendor details'))

        return vehicle