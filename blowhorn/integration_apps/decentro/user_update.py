import json
from datetime import datetime
from blowhorn.integration_apps.decentro.constants import SUCCESS

from blowhorn.oscar.core.loading import get_model
from blowhorn.integration_apps.models import ApiCallSaver
from blowhorn.document.constants import (DOCUMENT_RC,
        DOCUMENT_PAN, DOCUMENT_DL, DOCUMENT_AADHAR)

Driver = get_model('driver', 'Driver')
Vendor = get_model('vehicle', 'Vendor')


class UserUpdate:

    def __init__(self, driver, fleet, identifier, doctype, data):
        self.driver = driver
        self.fleet = fleet
        self.identifier = identifier
        self.doctype = doctype
        self.data = data
        self.txn_id = None
        self.status = False
        self.response_code = None

    def validate_dob(self, date_text):
        date = None
        try:
            if date_text:
                _date = datetime.strptime(date_text, '%d/%m/%Y')
                date = _date.strftime("%Y-%m-%d")
        except ValueError:
            pass
        return date
    
    def get_document_number(self):
        _out = json.loads(self.data)
        _data = _out.get('ocrResult', None)
        _kycdata = _out.get('kycResult', None)
        doc_number = None

        if _data:
            doc_number = _data.get('cardNo', None)
        if _kycdata:
            doc_number = _kycdata.get('idNumber', doc_number)
        if not doc_number and _kycdata:
            doc_number = _kycdata.get('drivingLicenseNumber', doc_number)
        if not doc_number and _kycdata:
            doc_number = _kycdata.get('registrationNumber', None)
        
        return doc_number
    
    def _process_document(self):
        number = None

        if self.doctype.code == DOCUMENT_AADHAR:
            number = self._process_aadhaar()
        
        if self.doctype.code == DOCUMENT_PAN:
            number = self._process_pan()
        
        if self.doctype.code == DOCUMENT_RC:
            number = self._process_registration_certificate()
        
        if self.doctype.code == DOCUMENT_DL:
            number = self.process_driving_license()
   
        return self.status, number

    def _update_pan(self):
        if self.driver:
            self.driver.pan = self.data
            self.driver.save()

        elif self.fleet:
            self.fleet.pan = self.data
            self.fleet.save()

    def _process_pan(self):
        """
            {
            "ocrStatus": "SUCCESS",
            "referenceId": "0000-0000-0000-2005",
            "kycStatus": "SUCCESS",
            "status": "SUCCESS",
            "message": "KYC Details for PAN retrieved successfully",
            "kycResult": {
                "idNumber": "XXXXXXXXXX",
                "idStatus": "VALID",
                "panStatus": "VALID",
                "lastName": "PRIVATE LIMITED",
                "firstName": "AMAZON SELLER SERVICES",
                "fullName": "AMAZON SELLER SERVICES PRIVATE LIMITED",
                "idHolderTitle": "M/S",
                "idLastUpdated": "20/04/2021",
                "aadhaarSeedingStatus": "Y"
            },
            "ocrResult": {
                "cardNo": "<pan number>",
                "dateInfo": "03/04/1982",
                "dateType": "DOB",
                "fatherName": "<fathers name on pan>",
                "name": "<name on pan>"
            },
            "decentroTxnId": "DECXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            }
        """
        _out = json.loads(self.data)
        self.txn_id = _out.get('decentroTxnId')
        _data = _out.get('ocrResult', None)
        _kycdata = _out.get('kycResult', None)
        self.response_code = _out.get('responseCode', None)
        doc_number = None

        if self.driver:
            uob = self.driver
        elif self.fleet:
            uob = self.fleet
        if not _data:
            return doc_number

        # setup user data for update
        _user_data = {}
        doc_number = _data.get('cardNo', None)
        if _kycdata:
            doc_number = _kycdata.get('idNumber', doc_number)
        if doc_number:
            _user_data['pan'] = doc_number

        father_name = _data.get("fatherName", None)
        if not uob.father_name and father_name:
            _user_data['father_name'] = father_name

        date_of_birth = self.validate_dob(_data.get("dateInfo", None))
        if not uob.date_of_birth and date_of_birth:
            _user_data['date_of_birth'] = date_of_birth

        is_kyc_success = _out.get('kycStatus', False)
        is_ocr_success = _out.get('ocrStatus', False)
        is_success = False
        if (is_kyc_success == SUCCESS) or (is_ocr_success == SUCCESS):
            is_success = True
        if doc_number and is_success:
            self.status = True

        if _user_data:
            if self.driver:
                uob = Driver.objects.filter(id=self.driver.id)
            elif self.fleet:
                uob = Vendor.objects.filter(id=self.fleet)
            uob.update(**_user_data)

        self.response_saving()
        return doc_number

    def _process_aadhaar(self):
        """
            Input::
            {
                "ocrStatus": "SUCCESS",
                "status": "SUCCESS",
                "message": "Scan completed successfully.",
                "ocrResult": {
                    "cardNo": "235518011123",
                    "dateInfo": "01/06/1990",
                    "dateType": "DOB",
                    "fatherName": "",
                    "gender": "Male",
                    "name": "XXXXXX",
                    "vid": ""
                },
                "responseCode": "S00000",
                "decentroTxnId": "CEF29C9A08634FDE9B389A6856C8A166"
                }
        """
        _out = json.loads(self.data)
        self.txn_id = _out.get('decentroTxnId')
        _data = _out.get('ocrResult', None)
        self.response_code = _out.get('responseCode', None)
        doc_number = None
        if self.driver:
            uob = self.driver
        elif self.fleet:
            uob = self.fleet
        
        if not (uob and _data):
            return doc_number

        # setup user data for update
        _user_data = {}
        doc_number = _data.get('cardNo', None)
        father_name = _data.get("fatherName", None)
        if not uob.father_name and father_name:
            _user_data['father_name'] = father_name
        
        gender = _data.get("gender", "male").lower()
        if not uob.gender and gender:
            _user_data['gender'] = gender
        
        date_of_birth = self.validate_dob(_data.get("dateType", None))
        if not uob.date_of_birth and date_of_birth:
            _user_data['date_of_birth'] = date_of_birth
        
        is_ocr_success = _out.get('ocrStatus', False)
        if doc_number and (is_ocr_success == SUCCESS):
            self.status = True
        if _user_data:
            if self.driver:
                uob = Driver.objects.filter(id=self.driver.id)
            elif self.fleet:
                uob = Vendor.objects.filter(id=self.fleet.id)
            uob.update(**_user_data)
        
        self.response_saving()
        return doc_number
    
    def _process_registration_certificate(self):
        """
            {
                "kycStatus": "SUCCESS",
                "status": "SUCCESS",
                "message": "Valid Authentication",
                "kycResult": {
                    "blacklistStatus": "NA",
                    "status": "ACTIVE",
                    "registrationDate": "02-Jan-2015",
                    "registrationAddress": "XXX 614201",
                    "registrationNumber": "TN68M9654",
                    "ownerName": "XYZ",
                    "ownerFatherName": "ABC",
                    "ownerPermanentAddress": "QWERTY 614201",
                    "ownerPresentAddress": "QWERTY ABC 614201",
                    "ownerSerialNumber": "1",
                    "chassisNumber": "XX2X11XX0XXX21779",
                    "bodyType": "SOLO WITH PILLION",
                    "class": "M-Cycle/Scooter(2WN)",
                    "category": "2WN",
                    "color": "WHITE",
                    "engineCubicCapacity": "149.0",
                    "numberCylinders": "1",
                    "unladenWeight": "143",
                    "grossWeight": "143",
                    "wheelbase": "999999",
                    "engineNumber": "XXXXXX04928",
                    "manufacturedMonthYear": "7/2014",
                    "makerDescription": "BAJAJ AUTO LTD",
                    "maker": "PULSAR 150 DTSI",
                    "fuelType": "PETROL",
                    "nocDetails": "NA",
                    "normsDescription": "Not Available",
                    "financier": "NA",
                    "fitUpto": "01-Jan-2030",
                    "insuranceUpto": "23-Jun-2020",
                    "insuranceDetails": "Reliance General Insurance Co. Ltd.",
                    "insuranceValidity": "23-Jun-2020",
                    "npCertificateIssuedBy": "KUMBAKONAM RTO, Tamil Nadu",
                    "permitIssueDate": "",
                    "permitNumber": "",
                    "permitType": "",
                    "permitValidFrom": "",
                    "permitValidUpto": "",
                    "pollutionControlNumber": "NA",
                    "pollutionControlValidity": "NA",
                    "seatingCapacity": "2",
                    "sleepingCapacity": "0",
                    "standingCapacity": "0",
                    "statusAsOn": "11-Jun-2021",
                    "taxUpto": "LTT"
                },
                "responseCode": "S00000",
                "requestTimestamp": "2021-06-11 19:16:01.319075 IST (GMT +0530)",
                "responseTimestamp": "2021-06-11 19:16:06.635544 IST (GMT +0530)",
                "decentroTxnId": "4B230EB517B04C7EA77DD4A46BE128BA"
                }

        """
        _out = json.loads(self.data)
        self.txn_id = _out.get('decentroTxnId')
        _data = _out.get('ocrResult', None)
        _kycdata = _out.get('kycResult', None)
        self.response_code = _out.get('responseCode', None)
        doc_number = None

        if _data:
            doc_number = _data.get('cardId', None)
        if _kycdata:
            doc_number = _kycdata.get('registrationNumber', None)

        is_kyc_success = _out.get('kycStatus', False)
        if doc_number and (is_kyc_success == SUCCESS):
            self.status = True
        
        
        self.response_saving()
        return doc_number

    def process_driving_license(self):
        """
        {
            "ocrStatus": "SUCCESS",
            "kycStatus": "SUCCESS",
            "status": "SUCCESS",
            "message": "Scan completed successfully.",
            "ocrResult": {
                "cardId": "XX0420111149646",
                "dob": "09/02/1976",
                "name": "XXXX"
            },
            "kycResult": {
                "bloodGroup": "U",
                "dateOfBirth": "09-02-1976",
                "drivingLicenseNumber": "XX0420111149646",
            },
            "referenceId": "eefa-abs1-0001-89128",
            "requestTimestamp": "2021-01-15 16:07:45.684584 IST (GMT +0530)",
            "responseTimestamp": "2021-01-15 16:07:47.311000 IST (GMT +0530)",
            "decentroTxnId": "<decentroTxnId>"
            }
        """
        doc_number = None
        _out = json.loads(self.data)
        self.txn_id = _out.get('decentroTxnId')
        self.response_code = _out.get('responseCode', None)
        _data = _out.get('ocrResult', None)
        _kycdata = _out.get('kycResult', None)

        is_kyc_success = _out.get('kycStatus', False)
        is_ocr_success = _out.get('ocrStatus', False)
        is_success = False

        if _data:
            doc_number = _data.get('cardId', None)
        if _kycdata:
            doc_number = _kycdata.get('drivingLicenseNumber', doc_number)
        if (is_kyc_success == SUCCESS) or (is_ocr_success == SUCCESS):
            is_success = True
        if doc_number and is_success:
            self.status = True
 
        if self.driver and doc_number:
            Driver.objects.filter(id=self.driver.id).update(driving_license_number=doc_number)

        self.response_saving()
        return doc_number

    def response_saving(self):
        _parm = {}
        acs = ApiCallSaver.objects.filter(transaction_id=self.txn_id)
        if acs:
            if self.driver:
                _parm['reference_id'] = self.driver.id
            else:
                _parm['reference_id'] = self.fleet.id

            _parm['identifier'] = self.identifier
            _parm['govtcheck_status'] = self.status
            _parm['in_govt_status'] = self.status
            acs.update(**_parm)    
