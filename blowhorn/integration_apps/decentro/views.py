import json
import logging
from django.core.exceptions import ValidationError
from blowhorn.customer.permissions import IsValidPartner
from blowhorn.integration_apps.models import ApiCallSaver

from rest_framework.response import Response
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from rest_framework import views, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response

from blowhorn.users.permission import IsActive
from blowhorn.driver.mixins import BankAccountUtils
from blowhorn.integration_apps.decentro.service import DecentroService



logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class DocumentValidate(views.APIView):
    """
        Document Validate via Decentro
    """
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):
        try:
            data = request.data.dict()
            is_ocr = data.get('is_ocr', '')
            is_ocr = json.loads(is_ocr)

            if is_ocr:
                response = DecentroService().ocr_verification(data)
            else:
                response = DecentroService().non_ocr_verification(data)

            if response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
                _message = response.text
                if 'help@decentro.tech'in _message:
                    _message = _message.replace('help@decentro.tech', 'shoutout@blowhorn.com')
                message_json = json.loads(_message)
                return Response(status=status.HTTP_400_BAD_REQUEST, data=message_json)

            if hasattr(response, 'text'):
                _data = json.loads(response.text)
            else:
                _data = json.loads(response.data)

            return Response(status=response.status_code, data=_data)

        except ValidationError as ve:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=ve.message)
        
        except Exception as e:
            logger.info('Decentro Call failed - %s' % str(e))
            return Response(status=status.HTTP_400_BAD_REQUEST, data=
                'Please try again after sometime')

class BankVerification(views.APIView):

    permission_classes = (IsAuthenticated, IsActive)

    def get(self, request):
        _status, _data = BankAccountUtils().get_driver_vendor_bank_details(request.user)
        _status_code = status.HTTP_200_OK if _status else status.HTTP_400_BAD_REQUEST
        return Response(status=_status_code, data=_data)

    def post(self, request):
        is_success, message = BankAccountUtils().add_driver_vendor_bank(request.user, request.data)
        status_code = status.HTTP_200_OK if is_success else status.HTTP_400_BAD_REQUEST
        return Response(status=status_code, data=message)


class AccountAcitivity(views.APIView):

    permission_classes = (IsValidPartner,)

    def post(self, request):
        DecentroService()._save_request(request.data)
        _msg = {
            "response_code": "CB_S00000"
            }
        return Response(status=status.HTTP_200_OK, data=_msg)