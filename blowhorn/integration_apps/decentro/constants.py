from django.conf import settings
from blowhorn.document.constants import DOCUMENT_RC, DOCUMENT_PAN, DOCUMENT_DL, DOCUMENT_AADHAR

DECENTRO_OCR_URL = '{}/kyc/scan_extract/ocr'.format(settings.DECENTRO_BASE_URL)
DECENTRO_NON_OCR_URL = '{}/kyc/public_registry/validate'.format(settings.DECENTRO_BASE_URL)

DOCUMENT_TYPE_MAPPING = {
    DOCUMENT_RC : "RC-Detailed",
    DOCUMENT_PAN : "PAN",
    DOCUMENT_DL : "DRIVING_LICENSE",
    DOCUMENT_AADHAR : "AADHAAR"
}

BANK_ACCOUNT_VERIFICATION_URL = '{}/core_banking/money_transfer/validate_account'.format(settings.DECENTRO_BASE_URL)

HEADERS = {
    'client_id': settings.DECENTRO_CLIENT_ID,
    'client_secret': settings.DECENTRO_CLIENT_SECRET,
    'module_secret': settings.DECENTRO_MODULE_SECRET
}

BANK_HEADERS = {
    'client_id': settings.DECENTRO_CLIENT_ID,
    'client_secret': settings.DECENTRO_CLIENT_SECRET,
    'module_secret': settings.DECENTRO_ACCOUNT_MODULE_SECRET,
    'provider_secret': settings.DECENTRO_BANK_PROVIDER_SECRET
}

SUCCESS = 'SUCCESS'

VERIFICATION_FAILED = 'failed'
VERIFICATION_SUCCESS = 'success'
VERIFICATION_PENDING = 'pending'
