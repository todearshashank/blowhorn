import random
import json
import string

import requests
from rest_framework import status
from rest_framework.response import Response
from django.core.exceptions import ValidationError
from datetime import datetime
from django.conf import settings
from blowhorn.document.constants import BANK_ACCOUNT
from blowhorn.integration_apps.decentro.constants import BANK_ACCOUNT_VERIFICATION_URL, BANK_HEADERS, DECENTRO_NON_OCR_URL, \
    DECENTRO_OCR_URL, DOCUMENT_TYPE_MAPPING, HEADERS, VERIFICATION_SUCCESS
from blowhorn.integration_apps.models import ApiCallSaver

class DecentroService:
    
    def generate_unique_txn_id(self, document_type):
        _timestamp = datetime.now().strftime("%d%m%Y%H%M%S")
        _randomstr = ''.join(random.choices(string.ascii_letters+string.digits, k=10))
        unique_txn_id = f'{document_type}-{_timestamp}-{_randomstr}'
        return unique_txn_id

    def _create_payload(self, is_ocr, kwargs):
        file = kwargs.get('file', None)
        document_number = kwargs.get('document_number', None)
        document_type = kwargs.get('document_type', None)
        dob = kwargs.get('dob', None)

        if not document_type:
            raise ValidationError("Please provide the type of document")

        decentro_document_type = DOCUMENT_TYPE_MAPPING.get(document_type, None)
        if not decentro_document_type:
            raise ValidationError("Document is not valid for validation")

        if is_ocr and not file:
            raise ValidationError("Please upload the file for verification")

        if not is_ocr and not document_number:
            raise ValidationError("Please provide the document number for verification")

        _payload = {
            "reference_id" : self.generate_unique_txn_id(document_type),
            "document_type" : decentro_document_type,
            "consent" : "Y",
            "consent_purpose" : "for bank account purpose only"
        }

        if decentro_document_type == 'AADHAAR':
            _payload['kyc_validate'] = 0

        if dob:
            _payload['dob'] = dob

        if not is_ocr:
            _payload['id_number'] = document_number

        return _payload

    def ocr_verification(self, input):
        document_type = input.get('document_type', None)
        payload = self._create_payload(True, input)
        file = input.get('file', None)
        files=[('document',(file.name, file.read(), 'image/jpeg'))]

        response = requests.post(DECENTRO_OCR_URL, data=payload,
                        headers=HEADERS, files=files)
        self._save_response(document_type, response)
        return response

    def non_ocr_verification(self, input):
        document_number = input.get('document_number', None)
        document_type = input.get('document_type', None)
        acs_qs = ApiCallSaver.objects.filter(document_number=document_number,
                    request_rc=status.HTTP_200_OK)

        if acs_qs:
            acs = acs_qs.latest('timestamp')
            if acs.response_text:
                _data = json.loads(acs.response_text)
                if type(_data) == str:
                    _data = json.loads(_data)
                return Response(status=status.HTTP_200_OK, data=_data)

        payload = self._create_payload(False, input)
        response = requests.post(DECENTRO_NON_OCR_URL, json=payload, headers=HEADERS)
        self._save_response(document_type, response)
        return response
    
    def bank_verification(self, user, bank_account_number, ifsc_code):
        document_number = '%s:%s' % (ifsc_code, bank_account_number)
        acs_qs = ApiCallSaver.objects.filter(document_number=document_number,
                    request_rc=status.HTTP_200_OK)

        if acs_qs:
            acs = acs_qs.latest('timestamp')
            if acs.response_text:
                _data = json.loads(acs.response_text)
                if type(_data) == str:
                    _data = json.loads(_data)
                _status = _data.get('status')
                _message = _data.get('message')
                return _status==VERIFICATION_SUCCESS, _message, _data, False

        payload = {
            "reference_id": self.generate_unique_txn_id(BANK_ACCOUNT),
            "purpose_message": "This is a penny drop transaction",
            "transfer_amount": "1",
            "beneficiary_details": {
                "name": user.name.title() if user.name else '',
                "mobile_number": str(user.phone_number.national_number),
                "email_address": "%s@blowhorn.net" % user.phone_number.national_number,
                "account_number": bank_account_number,
                "ifsc": ifsc_code
            }
        }

        if settings.ENABLE_SLACK_NOTIFICATIONS:
            response = requests.post(BANK_ACCOUNT_VERIFICATION_URL, json=payload, headers=BANK_HEADERS)
        else:
            _data = {
                "status": "success",
                "message": "The account has been successfully verified.",
                "accountStatus": "valid",
                "decentroTxnId": self.generate_unique_txn_id(BANK_ACCOUNT),
                "beneficiaryName": "Test Case",
                "beneficiaryCode": "101010",
                "bankReferenceNumber": "REF01010101",
                "responseCode": "S000009"
            }
            response = Response(status=status.HTTP_200_OK, data=_data)

        _status, _err, resp = self._save_response(BANK_ACCOUNT, response, ifsc_code=ifsc_code, bank_account_number=bank_account_number)
        return _status, _err, resp, True

    def _save_response(self, document_type, response, ifsc_code=None, bank_account_number=None):
        doc_number = None
        call_saver = {}
        if settings.ENABLE_SLACK_NOTIFICATIONS:
            _out = json.loads(response.text)
        else:
            _out = response.data

        call_saver['response_text'] = _out
        if response.status_code == status.HTTP_200_OK:
            if document_type == BANK_ACCOUNT:
                doc_number = '%s:%s' % (ifsc_code, bank_account_number)
            else:
                _data = _out.get('ocrResult', None)
                _kycdata = _out.get('kycResult', None)

                if _data:
                    doc_number = _data.get('cardNo', None)
                if _kycdata:
                    doc_number = _kycdata.get('idNumber', doc_number)
                if not doc_number and _kycdata:
                    doc_number = _kycdata.get('drivingLicenseNumber', doc_number)
                if not doc_number and _kycdata:
                    doc_number = _kycdata.get('registrationNumber', None)

        call_saver['api_name'] = "Decentro"
        call_saver['doctype'] = document_type
        call_saver['document_number'] = doc_number
        call_saver['transaction_id'] = _out.get('decentroTxnId', None)
        call_saver['identifier'] = 'Decentro Call'
        call_saver['request_rc'] = response.status_code
        call_saver['api_rc'] = response.status_code
        acs = ApiCallSaver(**call_saver)
        acs.save()

        if document_type == BANK_ACCOUNT:
            _status = _out.get('status')
            _message = _out.get('message')
            return _status==VERIFICATION_SUCCESS, _message, _out

    def _save_request(self, request):
        call_saver = {}
        call_saver['response_text'] = request
        call_saver['api_name'] = "Decentro Callback"
        call_saver['doctype'] = BANK_ACCOUNT
        call_saver['transaction_id'] = request.get('decentroTxnId', None)
        call_saver['identifier'] = 'Decentro Call'
        call_saver['request_rc'] = status.HTTP_200_OK
        acs = ApiCallSaver(**call_saver)
        acs.save()