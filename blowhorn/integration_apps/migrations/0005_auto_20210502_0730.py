# Generated by Django 2.2.20 on 2021-05-02 07:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('integration_apps', '0004_auto_20200324_1539'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apicallsaver',
            name='identifier',
            field=models.CharField(choices=[('Driver Document', 'Driver Document'), ('Vehicle Document', 'Vehicle Document'), ('Customer Data Request', 'Customer Data Request'), ('Customer Redact Request', 'Customer Redact Request'), ('Store Redact Request', 'Store Redact Request')], help_text='Driver/Vehicle/RequestType Document', max_length=100, verbose_name='Document Identifier'),
        ),
        migrations.AlterField(
            model_name='apicallsaver',
            name='reference_id',
            field=models.IntegerField(help_text='Object id of the driver/vendor/customer', verbose_name='Reference Id'),
        ),
    ]
