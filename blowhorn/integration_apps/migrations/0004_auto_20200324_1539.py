# Generated by Django 2.1.1 on 2020-03-24 15:39

import audit_log.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customer', '0111_auto_20200303_1203'),
        ('driver', '0134_driver_sanitizer_recvd'),
        ('integration_apps', '0003_apicallsaver_invoidconsistency'),
    ]

    operations = [
        migrations.CreateModel(
            name='PartnerLeadHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('reference_id', models.CharField(max_length=250)),
                ('loan_status', models.CharField(max_length=250)),
                ('loan_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Loan amount')),
                ('installment_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Installment amount')),
                ('no_of_installments', models.PositiveIntegerField(blank=True, db_index=True, null=True, verbose_name='Total Installments')),
                ('upcoming_installment_number', models.PositiveIntegerField(blank=True, db_index=True, null=True, verbose_name='upcoming Installment No')),
                ('outstanding_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Outstanding amount')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='partnerleadhistory_created_by', to=settings.AUTH_USER_MODEL)),
                ('driver', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='driver.Driver')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='partnerleadhistory_modified_by', to=settings.AUTH_USER_MODEL)),
                ('partner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='customer.Partner')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PartnerLeadStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('reference_id', models.CharField(max_length=250)),
                ('loan_status', models.CharField(max_length=250)),
                ('loan_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Loan amount')),
                ('installment_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Installment amount')),
                ('no_of_installments', models.PositiveIntegerField(blank=True, db_index=True, null=True, verbose_name='Total Installments')),
                ('upcoming_installment_number', models.PositiveIntegerField(blank=True, db_index=True, null=True, verbose_name='upcoming Installment No')),
                ('outstanding_amount', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Outstanding amount')),
                ('created_by', audit_log.models.fields.CreatingUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='partnerleadstatus_created_by', to=settings.AUTH_USER_MODEL)),
                ('driver', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='driver.Driver')),
                ('modified_by', audit_log.models.fields.LastUserField(editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='partnerleadstatus_modified_by', to=settings.AUTH_USER_MODEL)),
                ('partner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='customer.Partner')),
            ],
            options={
                'verbose_name': 'Leads',
                'verbose_name_plural': 'Leads',
            },
        ),
        migrations.AddField(
            model_name='partnerleadhistory',
            name='partner_lead',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='history', to='integration_apps.PartnerLeadStatus'),
        ),
    ]
