from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField

from blowhorn.driver.models import DriverDocument, Driver
from blowhorn.document.models import DocumentType
from blowhorn.vehicle.models import VehicleDocument
from blowhorn.common.models import BaseModel
from blowhorn.document.constants import DRIVER_DOCUMENT, VEHICLE_DOCUMENT, VENDOR_DOCUMENT
from blowhorn.integration_apps.constants import SHOPIFY_CUSTOMER_DATA, SHOPIFY_CUSTOMER_REDACT, SHOPIFY_STORE_REDACT

class SymboLead(BaseModel):

    # symbo lead reference id
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE, blank=True,
                               null=True)

    symbo_id = models.CharField(_('Unique Lead Reference'), max_length=250)

    referenceId = models.CharField(max_length=250)

    response = JSONField(null=True)

    last_modified = models.DateTimeField(_("Last Modified on Symbo's System"))

    class Meta:
        verbose_name = _("Symbo Leads")
        verbose_name_plural = _("Symbo Leads")

    def __str__(self):
        return self.symbo_id


class SymboBlowhornMap(models.Model):

    lead = models.ForeignKey(SymboLead, on_delete=models.CASCADE, related_name='leads')

    vehicle_document = models.ForeignKey(VehicleDocument, on_delete=models.CASCADE, null=True)

    driver_document = models.ForeignKey(DriverDocument, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = _("Symbo Blowhorn Map")
        verbose_name_plural = _("Symbo Blowhorn Map")

    def __str__(self):
        return str(self.vehicle_document) or str(self.driver_document)


class SymboLeadStatus(BaseModel):

    lead = models.ForeignKey(SymboLead, on_delete=models.CASCADE, related_name='lead_status')

    response = JSONField(null=True)

    disposition = models.CharField(_('Lead Status'), max_length=250)

    sub_Disposition = models.CharField(_('Lead Sub Status'), max_length=250)

    class Meta:
        verbose_name = _("Symbo Lead Status")
        verbose_name_plural = _("Symbo Lead Status")

    def __str__(self):
        return str(self.lead)

class ApiCallSaver(models.Model):
    '''
        This model is used to save the api calls
        made for the third party.
    '''

    IDENTIFIERS = (
        (VENDOR_DOCUMENT, _("Vendor Document")),
        (DRIVER_DOCUMENT, _("Driver Document")),
        (VEHICLE_DOCUMENT, _("Vehicle Document")),
        (SHOPIFY_CUSTOMER_DATA, _("Customer Data Request")),
        (SHOPIFY_CUSTOMER_REDACT, _("Customer Redact Request")),
        (SHOPIFY_STORE_REDACT, _("Store Redact Request")),
    )

    transaction_id = models.CharField(_('Transaction ID'), max_length=250,
        help_text = _('Unique Transaction id for the api call'))
    
    document_number = models.CharField(_('Document Number'), max_length=250,
        null=True, blank=True, help_text = _('Document Number if any'))
    
    reference_id = models.IntegerField(_('Reference Id'), null=True, blank=True,
        help_text = _('Object id of the driver/vendor/customer'))

    identifier = models.CharField(_("Document Identifier"), null=True, blank=True,
                    max_length=100, help_text = _('Driver/Vehicle/RequestType Document'))
    
    doctype = models.CharField(_("Document"), max_length=100,
        null=True, blank=True, help_text = _('Document Type Code'))
    
    api_name = models.CharField(_('Third Pary'), max_length = 50,
        help_text = _('Name of Third party'))
    
    api_url = models.CharField(_('Api Url'), max_length = 256, 
        null=True, blank=True, help_text = _('Url accessed'))
    
    response_text = JSONField(_('Response Text'), null=True,
        help_text = _('Response text of the api'))
    
    govtcheck_status = models.BooleanField(_('Status'),
        null=True, blank=True, help_text = _('Final response of the api call'))

    in_govt_status = models.BooleanField(_('Government Status'),
        null=True, blank=True, help_text = _('Government Check Status in response'))
    
    request_rc = models.IntegerField(_('Response Code'),
        help_text = _('Http standard response status 200,400 etc'))
    
    api_rc = models.CharField(_('Api Response Code'), max_length=10,
        null=True, blank=True, help_text = _('Third Party predefined status code xxxxx'))
    
    timestamp = models.DateTimeField(_('Timestamp'), auto_now_add = True,
        help_text = _('Timestamp of call'))

    class Meta:
        verbose_name = _("API Call Log")
        verbose_name_plural = _("API Call Log")

    def __str__(self):
        return self.transaction_id

class InvoidConsistency(models.Model):
    '''
        This model is used to store the invoid 
        consistancy check result.
    '''

    reference_id = models.IntegerField(_('Reference Id'),
        help_text = _('Object id of the driver/vendor'))

    reference_type = models.CharField(_('Reference'), max_length=50,
        help_text = _('Refers to Driver/Vendor'))

    name_similarity = models.FloatField(_('Name Similarity'), default = 0.0,
        help_text = _('Percentage of the name similarity'))

    face_similarity_1 = models.FloatField(_('Face Similarity 1'), default = 0.0,
        help_text = _('Percentage of the selfie aadhaar face similarity'))

    face_similarity_2 = models.FloatField(_('Face Similarity 2'), default = 0.0,
        help_text = _('Percentage of the selfie pan face similarity'))

    yob_similarity = models.FloatField(_('Year of Birth Similarity'), default = 0.0,
        help_text = _('Percentage of the year of birth similarity'))

    document_confidence_1 = models.FloatField(_('Document Confidence 1'), default = 0.0,
        help_text = _('Percentage of aadhaar document confidence'))

    document_confidence_2 = models.FloatField(_('Document Confidence 2'), default = 0.0,
        help_text = _('Percentage of pan document confidence'))

    consistency_result = models.BooleanField(_('Final Consistency Result'), default = False,
        help_text = _('Final consistency result of the document'))

    overall_document_confidence = models.BooleanField(_('Confidence Result'), default = False,
        help_text = _('Document Confidence passed or failed'))

    overall_similarity_confidence = models.BooleanField(_('Similarity Confidence Result'), default = False,
        help_text = _('Similarity Confidence passed or failed - name, face and yob'))

    class Meta:
        verbose_name = _("Invoid Consistency")
        verbose_name_plural = _("Invoid Consistency")

    def __str__(self):
        result = str(self.reference_id) + ':' + \
                    (self.reference_type) + ':' +\
                    str(self.consistency_result)
        return result








