from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from blowhorn.integration_apps.sub_models.sub_models import PartnerLeadStatus, PartnerLeadHistory
from .models import SymboLead, SymboBlowhornMap, ApiCallSaver, InvoidConsistency
from blowhorn.common.admin import BaseAdmin
from ..utils.datetimerangefilter import DateRangeFilter


class SymboBlowhornInline(admin.TabularInline):
    model = SymboBlowhornMap
    max_num = 1
    extra = 0

    fields = ('id', 'get_vehicle_document', 'get_driver_document', 'get_expiry_date',)

    def get_expiry_date(self, obj):
        if obj:
            if obj.vehicle_document_id:
                return obj.vehicle_document.expiry_date
            if obj.driver_document_id:
                return obj.driver_document.expiry_date
        return ''

    get_expiry_date.short_description = 'Expiry Date'

    def get_vehicle_document(self, obj):
        if obj and obj.vehicle_document_id:
            url = reverse('admin:vehicle_vehicledocument_change', args=[obj.vehicle_document_id])
            return format_html("<a href='{}'>{}</a>", url, obj.vehicle_document_id)
        return ''

    get_vehicle_document.short_description = 'Vehicle Document'

    def get_driver_document(self, obj):
        if obj and obj.driver_document_id:
            url = reverse('admin:driver_driverdocument_change', args=[obj.driver_document_id])
            return format_html("<a href='{}'>{}</a>", url, obj.driver_document_id)
        return ''

    get_driver_document.short_description = 'Driver Document'

    def get_readonly_fields(self, request, obj=None):
        return ['id', 'get_vehicle_document', 'get_driver_document', 'get_expiry_date',]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class SymboLeadAdmin(BaseAdmin):
    fieldsets = (
        ('General', {
            'fields': (
                'id', 'symbo_id', 'referenceId', 'last_modified'
            )
        }),
        ('Audit Log', {
            'fields': ('created_date', 'created_by', 'modified_date',
                       'modified_by'),
        }),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_list_display(self, request):
        return [f.name for f in self.model._meta.fields if f.name not in ['response']]

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    search_fields = ['id', 'symbo_id', 'referenceId']

    inlines = [SymboBlowhornInline]

class SymboBlowhornMapAdmin(BaseAdmin):
    list_display  = ('id', 'get_vehicle_document', 'get_driver_document', 'get_expiry_date',)
    fieldsets = (
        ('General', {
            'fields': (
                'id', 'get_vehicle_document', 'get_driver_document', 'get_expiry_date',
            )
        }),
    )

    def get_expiry_date(self, obj):
        if obj:
            if obj.vehicle_document_id:
                return obj.vehicle_document.expiry_date
            if obj.driver_document_id:
                return obj.driver_document.expiry_date
        return ''

    get_expiry_date.short_description = 'Expiry Date'

    def get_vehicle_document(self, obj):
        if obj and obj.vehicle_document_id:
            url = reverse('admin:vehicle_vehicledocument_change', args=[obj.vehicle_document_id])
            return format_html("<a href='{}'>{}</a>", url, obj.vehicle_document_id)
        return ''

    get_vehicle_document.short_description = 'Vehicle Document'

    def get_driver_document(self, obj):
        if obj and obj.driver_document_id:
            url = reverse('admin:driver_driverdocument_change', args=[obj.driver_document_id])
            return format_html("<a href='{}'>{}</a>", url, obj.driver_document_id)
        return ''

    get_driver_document.short_description = 'Driver Document'

    def get_readonly_fields(self, request, obj=None):
        return ['id', 'get_vehicle_document', 'get_driver_document', 'get_expiry_date',]

class ApiCallSaverAdmin(BaseAdmin):
    list_display = ('transaction_id', 'reference_id',
                        'doctype', 'in_govt_status',
                        'document_number', 'timestamp')
    readonly_fields = ['transaction_id', 'reference_id',
                        'api_name', 'document_number',
                        'govtcheck_status', 'timestamp',
                        'identifier', 'in_govt_status',
                        'api_url', 'request_rc', 'api_rc',
                        'response_text','doctype']

    fieldsets = (
        ('General', {
            'fields': (
                'transaction_id', 'reference_id',
                'api_name', 'api_url', 'timestamp'
            )
        }),
        ('Document Details', {
            'fields': (
                'document_number', 'doctype', 'identifier',
                'govtcheck_status', 'in_govt_status'
            )
        }),
        ('Response', {
            'fields': (
                'response_text', 'request_rc', 'api_rc'
            )
        }),
    )

    list_filter = ('doctype', 'request_rc', 'api_name')
    search_fields = ('reference_id', 'document_number', 'transaction_id')

class InvoidConsistencyAdmin(BaseAdmin):
    list_display = ('reference_id', 'reference_type', 'consistency_result',
                        'overall_document_confidence', 'overall_similarity_confidence')

    readonly_fields = ['reference_id', 'reference_type', 'consistency_result',
                        'overall_document_confidence', 'overall_similarity_confidence',
                        'name_similarity','face_similarity_1', 'face_similarity_2',
                        'yob_similarity', 'document_confidence_1', 'document_confidence_2'
                    ]

    fieldsets = (
            ('General', {
            'fields': (
                'reference_id', 'reference_type', 'consistency_result',
                'overall_document_confidence', 'overall_similarity_confidence'
            )
        }),
        ('Document Confidence', {
            'fields': (
                'document_confidence_1', 'document_confidence_2'
            )
        }),
        ('Similarity Confidence', {
            'fields': (
                'name_similarity','face_similarity_1', 'face_similarity_2',
                'yob_similarity'
            )
        }),


    )

    search_fields = ['reference_id', 'reference_type']



class LeadHistoryInline(admin.TabularInline):
    model = PartnerLeadHistory
    fk_name = 'partner_lead'
    extra = 0
    can_delete = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return [f.name for f in self.model._meta.fields]
        return ''

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver', 'partner')
        return qs

class PartnerLeadAdmin(BaseAdmin):
    list_display = ("id", 'driver', 'partner', 'reference_id', 'loan_status', 'loan_amount',
                    'installment_amount', 'no_of_installments' , 'upcoming_installment_number', 'outstanding_amount')

    inlines = [LeadHistoryInline]

    search_fields = ['driver__user__phone_number', 'reference_id',
                     'driver__vehicles__registration_certificate_number']

    list_filter = [('created_date', DateRangeFilter), 'loan_status']

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return [f.name for f in self.model._meta.fields]
        return ''

    extra = 0
    can_delete = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('driver', 'partner')
        return qs

admin.site.register(PartnerLeadStatus, PartnerLeadAdmin)
admin.site.register(ApiCallSaver, ApiCallSaverAdmin)
admin.site.register(SymboBlowhornMap, SymboBlowhornMapAdmin)
admin.site.register(SymboLead, SymboLeadAdmin)
admin.site.register(InvoidConsistency, InvoidConsistencyAdmin)
