import json

#django imports
from django.contrib import admin
from django.contrib.postgres.fields import JSONField
from django.forms import widgets
from django.contrib.gis.db.models import PointField

#3rd Party Imports
from blowhorn.oscar.core.loading import get_model
from prettyjson import PrettyJSONWidget
from mapwidgets.widgets import GooglePointFieldWidget



#bh imports
from blowhorn.route_optimiser.models import RoutesBatch, OptimalRoute, OptimalRouteStop

class RoutesBatchAdmin(admin.ModelAdmin):

    formfield_overrides = {
        JSONField: {'widget': PrettyJSONWidget}
    }

    list_display = (
        'batch_id', 'source', 'is_success',
        'created_by', 'created_date',
    )

    list_filter = ['source', 'is_success', 'created_by']

    search_fields = ('batch_id',)
    multi_search_fields = ('batch_id',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, delete, obj=None):
        return False

    def has_change_permission(self, delete, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields':
                    ('batch_id', 'source', 'is_success',
                        'input_body',)
            }),
            ('Response Body', {
                'fields':
                    ('response_body',)
            }),
            ('Audit Log', {
                'fields':
                    ('created_date', 'created_by',)
            }),
        )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = [
            "batch_id", "created_date", "created_by",
            "source", "input_body", "response_body",
            "is_success",
        ]
        return readonly_fields

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_delete'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    class Meta:
        fields = '__all__'


class RouteStopInlineAdmin(admin.TabularInline):

    model = OptimalRouteStop

    formfield_overrides = {
        PointField: {
            "widget": GooglePointFieldWidget()
        },
    }

    fields = ('route', 'stop_order', 'stop_type', 'sequence',
            'stop_address', 'capacity_demand', 'stop_lat', 'stop_lng',
            'geopoint')

    readonly_fields = fields
    can_delete = False

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        return qs.order_by('sequence')

    class Meta:
        fields = '__all__'


class OptimalRouteAdmin(admin.ModelAdmin):

    list_display = (
        'route_id', 'batch', 'estimated_distance', 'estimated_duration',
        'load_carried', 'created_by', 'created_date', 'is_trip_created', 'source', 'trip'
    )
    list_filter = ['batch', 'created_by', 'source', 'is_trip_created']

    search_fields = ('route_id',)
    multi_search_fields = ('route_id',)

    inlines = [
        RouteStopInlineAdmin,
    ]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('General', {
                'fields':
                    ('route_id', 'estimated_distance', 'estimated_duration','load_carried',
                    'batch', 'is_trip_created', 'is_saved', 'source' ,'trip',)
            }),
            ('Audit Log', {
                'fields':
                    ('created_date', 'created_by',)
            }),
        )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = [
            "route_id", "estimated_distance", "load_carried", 'estimate_duration',
            "batch", "is_trip_created", "is_saved", 'trip', 'source'
        ]
        return readonly_fields

    def change_view(self, request, object_id, extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save'] = False
        extra_context['show_save_and_continue'] = False
        extra_context['show_delete'] = False
        return super().change_view(request, object_id,
                                   extra_context=extra_context)

    class Meta:
        fields = '__all__'


admin.site.register(RoutesBatch, RoutesBatchAdmin)
admin.site.register(OptimalRoute, OptimalRouteAdmin)
