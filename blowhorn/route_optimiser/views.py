# Lib Imports
import uuid
import os, math, json
import pandas as pd


# Django Imports
from django.shortcuts import render
from django.db.models import Q

# DRF & Oscar Imports
from rest_framework import generics, status, views, exceptions
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from blowhorn.oscar.core.loading import get_model

# BH Imports
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.route_optimiser.models import RoutesBatch, OptimalRoute, OptimalRouteStop
from blowhorn.oscar.core.loading import get_model

# App Imports
from .utils import parse_input, list_address, get_distance_matrix, hub_checker, check_order_stops, check_new_order_pickup, save_batch_routes_stops
from .helpers.delivery_vrp_constraints import run_route_finder
from blowhorn.route_optimiser.utils import parse_data, plan_route

Order = get_model('order', 'Order')
Trip = get_model('trip', 'Trip')


class RoutesHubToDeliveryRaw(views.APIView):
    permission_classes = (AllowAny, )
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def post(self, request):
        data = request.data
        number_of_drivers = int(data['vehicles'])
        selected_constraint = data['constraint']
        vehicle_capacity = int(data['capacity'])
        order_records = data['rows']
        capacity_unit = data['capacity_unit']
        batch_id = str(uuid.uuid1())
        try:
            df, number_of_clusters, non_operational_orders = parse_input(order_records, number_of_drivers)
        except:
            print("All orders are fulfilled.")
            response = {
                "message": "All the Orders are either in End Status or already being fulfilled! Please Check!",
                "type": "ORDERS_FULFILLED"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Admin Web App",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        order_number = df.iloc[0]['order_id']
        order_numbers = df['order_id']
        query_params = Q(number__in=order_numbers)
        order_objects = Order.objects.filter(query_params)
        if len(order_objects) < 1:
            response = {
                "message": "Selected Orders do not exist on the server, Please Check!",
                "type": "ORDERS_NOT_FOUND"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Admin Web App",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        selected_hub = str(order_objects[0].hub.address)
        single_hub_check = hub_checker(order_objects)
        if selected_hub is None:
            response = {
                "message": "Order has no hub!",
                "type": "ORDER_HUB_NOT_FOUND"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Admin Web App",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        elif not single_hub_check:
            response = {
                "message": "Orders should be at the same hub to begin! Please filter the orders by hub!",
                "type": "ORDER_NOT_HUBWISE"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Admin Web App",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        #order_in_existing_trip = check_order_stops(order_objects, non_operational_orders)
        new_order_has_pickup = check_new_order_pickup(order_objects)
        #if order_in_existing_trip:
        #    response = {
        #        "message": "Some orders are already being fulfilled in other trips or are in End Statuses! Please check!",
        #        "type": "ORDERS_FULFILLED_ALREADY"
        #    }
        #    rb = RoutesBatch(
        #        batch_id=batch_id,
        #        source="Admin Web App",
        #        input_body=data,
        #        response_body=response
        #    )
        #    rb.save()
        #    return Response(response, status.HTTP_400_BAD_REQUEST)
        if new_order_has_pickup:
            response = {
                "message": "Some New Orders have a pickup yet to be done! Please check!",
                "type": "NEW_ORDER_PICKUP_PENDING"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Admin Web App",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        address_list_cluster_wise, order_id_list_cluster_wise, latlng_list_cluster_wise, capacity_demand_list_cluster_wise = list_address(df, number_of_clusters, selected_hub, capacity_unit)
        number_of_drivers_per_cluster = max(int(math.floor((int(number_of_drivers)*1.0)/(1.0*number_of_clusters))),1)

        solved_route_schema_cluster_wise = []

        for i in range(len(latlng_list_cluster_wise)):
            address_list = latlng_list_cluster_wise[i]
            order_volume_list = capacity_demand_list_cluster_wise[i]
            dist_matrix = get_distance_matrix(address_list, selected_constraint)
            solved_route_schema = run_route_finder(
                dist_matrix,
                number_of_drivers_per_cluster,
                data[selected_constraint],
                order_volume_list,
                vehicle_capacity)
            solved_route_schema_cluster_wise.append(solved_route_schema)

        try:
            for i in range(len(solved_route_schema_cluster_wise)):
                for route in solved_route_schema_cluster_wise[i]['route_list']:
                    for j in range(len(route)):
                        stop_name = {
                            "order_id": order_id_list_cluster_wise[i][route[j]][0:-4],
                            "type": order_id_list_cluster_wise[i][route[j]][-4:],
                            "stop": address_list_cluster_wise[i][route[j]],
                            "capacity_demand": capacity_demand_list_cluster_wise[i][route[j]],
                            "stop_sequence": j+1,
                            "stop_coordinates": {
                                "latitude": latlng_list_cluster_wise[i][route[j]]['lat'],
                                "longitude": latlng_list_cluster_wise[i][route[j]]['lng']
                            }
                        }
                        route[j] = stop_name
        except:
            print("The constraints are not enough! Please Increase Constraints and try again")
            response = {
                "message": "Please increase the number of drivers/limiting distance and try again!",
                "type": "CONSTRAINTS_NOT_SUFFICIENT"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Admin Web App",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_417_EXPECTATION_FAILED)
        response = {
            "batch_id": batch_id,
            "non_operational_orders": non_operational_orders
        }
        cluster_wise_info = []
        for items in solved_route_schema_cluster_wise:
            cluster_info = {
                "cluster_id": solved_route_schema_cluster_wise.index(items),
                "distance_travelled_current_cluster": items['total_distance']
            }
            route_wise_info=[]
            for i in range(len(items['route_distances'])):
                if items['route_distances'][i]!=0:
                    route_info = {
                        "abs_id": batch_id+str(solved_route_schema_cluster_wise.index(items))+str(i),
                        "route_id": i,
                        "route_distance": items['route_distances'][i],
                        "route_load": items['route_loads'][i],
                        "source": "Admin Web App"
                    }
                    route_list = []
                    for item in items['route_list'][i]:
                        route_order_corr = item
                        if isinstance(route_order_corr, dict):
                            route_list.append(route_order_corr)
                    route_info["elements"] = route_list
                    route_wise_info.append(route_info)
            cluster_info["routes_information"] = route_wise_info
            cluster_wise_info.append(cluster_info)
        response["rows"] = cluster_wise_info
        save_batch_routes_stops(data, response, "Admin Web App")
        return Response(response, status.HTTP_200_OK)


class FleetRouteOptimiserHubWise(views.APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        data = request.data
        order_data = data['order_numbers']
        number_of_drivers = int(data['vehicles'])
        selected_constraint = data['constraint']
        vehicle_capacity = int(data['capacity'])
        capacity_unit = data['capacity_unit']
        query_params = Q(number__in=order_data)
        batch_id = str(uuid.uuid1())
        order_objects = Order.objects.filter(query_params)
        order_json_list = []
        selected_hub = str(order_objects[0].hub.address)
        single_hub_check = hub_checker(order_objects)
        print(selected_hub)
        print(single_hub_check)
        if selected_hub is None:
            response = {
                "message": "Order has no hub!",
                "type": "HUB_NOT_SELECTED"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Customer Dashboard",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        elif not single_hub_check:
            response = {
                "message": "Orders need to be from the same hub",
                "type": "ORDER_NOT_HUBWISE"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Customer Dashboard",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        #order_in_existing_trip = check_order_stops(order_objects)
        new_order_has_pickup = check_new_order_pickup(order_objects)
        #if order_in_existing_trip:
        #    response = {
        #        "message": "Some orders are already being fulfilled in other trips or are in End Statuses! Please check!",
        #        "type": "ORDERS_FULFILLED_ALREADY"
        #    }
        #    rb = RoutesBatch(
        #        batch_id=batch_id,
        #        source="Customer Dashboard",
        #        input_body=data,
        #        response_body=response
        #    )
        #    rb.save()
        #    return Response(response, status.HTTP_400_BAD_REQUEST)
        if new_order_has_pickup:
            response = {
                "message": "Some New Orders have a pickup yet to be done! Please check!",
                "type": "NEW_ORDER_PICKUP_PENDING"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Customer Dashboard",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_400_BAD_REQUEST)
        for order in order_objects:
            order_json = {
                "Address": str(order.shipping_address),
                "Number": str(order.number),
                "Latitude": 0.0,
                "Longitude": 0.0,
                "Weight": float(order.weight),
                "Volume": float(order.volume),
            }
            order_json_list.append(order_json)
        try:
            df, number_of_clusters, non_operational_orders = parse_input(order_json_list, number_of_drivers)
        except:
            print("Network issue")
            response = {
                "message": "Please check the network connection and try again!",
                "type": "NETWORK_NOT_AVAILABLE"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Customer Dashboard",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_417_EXPECTATION_FAILED)
        address_list_cluster_wise, order_id_list_cluster_wise, latlng_list_cluster_wise, capacity_demand_list_cluster_wise = list_address(df, number_of_clusters, selected_hub, capacity_unit)
        number_of_drivers_per_cluster = max(int(math.floor((int(number_of_drivers)*1.0)/(1.0*number_of_clusters))),1)
        solved_route_schema_cluster_wise = []

        for i in range(len(latlng_list_cluster_wise)):
            address_list = latlng_list_cluster_wise[i]
            order_volume_list = capacity_demand_list_cluster_wise[i]
            dist_matrix = get_distance_matrix(address_list, selected_constraint)
            solved_route_schema = run_route_finder(
                dist_matrix,
                number_of_drivers_per_cluster,
                data[selected_constraint],
                order_volume_list,
                vehicle_capacity)
            solved_route_schema_cluster_wise.append(solved_route_schema)

        try:
            for i in range(len(solved_route_schema_cluster_wise)):
                for route in solved_route_schema_cluster_wise[i]['route_list']:
                    for j in range(len(route)):
                        stop_name = {
                            "order_id": order_id_list_cluster_wise[i][route[j]][0:-4],
                            "type": order_id_list_cluster_wise[i][route[j]][-4:],
                            "stop": address_list_cluster_wise[i][route[j]],
                            "capacity_demand": capacity_demand_list_cluster_wise[i][route[j]],
                            "stop_sequence": j+1,
                            "stop_coordinates": {
                                "latitude": latlng_list_cluster_wise[i][route[j]]['lat'],
                                "longitude": latlng_list_cluster_wise[i][route[j]]['lng']
                            }
                        }
                        route[j] = stop_name
        except:
            print("The constraints are not enough! Please Increase Constraints and try again")
            response = {
                "message": "Please increase the number of drivers/limiting distance and try again!",
                "type": "CONSTRAINTS_NOT_SUFFICIENT"
            }
            rb = RoutesBatch(
                batch_id=batch_id,
                source="Customer Dashboard",
                input_body=data,
                response_body=response
            )
            rb.save()
            return Response(response, status.HTTP_417_EXPECTATION_FAILED)
        response = {
            "batch_id": batch_id,
            "non_operational_orders": non_operational_orders
        }
        cluster_wise_info = []
        for items in solved_route_schema_cluster_wise:
            cluster_info = {
                "cluster_id": solved_route_schema_cluster_wise.index(items),
                "distance_travelled_current_cluster": items['total_distance']
            }
            route_wise_info=[]
            for i in range(len(items['route_distances'])):
                if items['route_distances'][i]!=0:
                    route_info = {
                        "abs_id": batch_id+str(solved_route_schema_cluster_wise.index(items))+str(i),
                        "route_id": i,
                        "route_distance": items['route_distances'][i],
                        "route_load": items['route_loads'][i],
                        "source": "Customer Dashboard"
                    }
                    route_list = []
                    for item in items['route_list'][i]:
                        route_order_corr = item
                        if isinstance(route_order_corr, dict):
                            route_list.append(route_order_corr)
                    route_info["elements"] = route_list
                    route_wise_info.append(route_info)
            cluster_info["routes_information"] = route_wise_info
            cluster_wise_info.append(cluster_info)
        response["rows"] = cluster_wise_info
        if capacity_unit=="weight":
            response["capacity_unit"] = "Weight(Kg)"
        else:
            response["capacity_unit"] = "Volume(Cubic cm)"
        if selected_constraint=="distance":
            response["constraint_is_distance"] = 1
        else:
            response["constraint_is_distance"] = 0
        save_batch_routes_stops(data, response, "Customer Dashboard")
        return Response(response, status.HTTP_200_OK)


class OptimalRoutePlanner(views.APIView):
    """
        Description     : Post a request to plan the route for the given stops in a trip.
                          This API is used in driver app for organising the stops in the to-do list.
        Request type    : POST
        Body            : Current Location and Trip ID are required
        Response        : Status of the request and message
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):

        data = request.data.dict()
        print(request.data)
        location = data.get('location_details', None)
        if location is not None:
            if isinstance(location, str):
                location = json.loads(location)
            current_location = {
                "lat":location.get('mLatitude'),
                "lng":location.get('mLongitude')
            }
        source= "Driver App"
        trip_id= int(data.get('trip_id'))
        trip = Trip.objects.get(id=trip_id)
        if not trip.is_route_optimal:
            flag = plan_route(trip=trip,
                        source="Driver App",
                        current_location=current_location)
        else:
            flag=True

        if flag:
            response = {
                "status": True,
                "message": "Trip has been modified with optimal route!"
            }
            return Response(response, status.HTTP_200_OK)
        else:
            response = {
                "status": False,
                "message": "Trip could not be optimized! Please check again later!"
            }
            return Response(response, status.HTTP_417_EXPECTATION_FAILED)
