import pandas as pd
import io, requests, re, os, time, json
import numpy as np
from multiprocessing.dummy import Pool
from requests.adapters import HTTPAdapter
from urllib3.util import Retry

# Settings Imported
from django.conf import settings
from django.db import transaction
from django.db.models import Q
from blowhorn.oscar.core.loading import get_model

# BH Imports
from blowhorn.route_optimiser.models import RoutesBatch, OptimalRoute, OptimalRouteStop
from blowhorn.address.utils import get_city_from_latlong
from blowhorn.oscar.core.loading import get_model
from blowhorn.route_optimiser.helpers.optimal_clustering import optimal_clustering
from blowhorn.route_optimiser.helpers.delivery_vrp_constraints import run_route_finder
from config.settings import status_pipelines as StatusPipeline
from math import floor
import uuid

constraint = 'distance'

Order = get_model('order', 'Order')
Trip = get_model('trip', 'Trip')
Stop = get_model('trip', 'Stop')
StopHistory = get_model('trip', 'StopHistory')


def list_address(clustered_df, number_of_clusters, depot, unit):

    if depot is not None:
        # Google maps api key
        google_maps_api_key = "&key="+settings.GOOGLE_GEOCODING_DISTANCEMATRIX_API_KEY
        # Geocode api base url
        request_url = "https://maps.googleapis.com/maps/api/geocode/"
        # Output Format can be "json?" or "xml?"
        output_format = "json?"
        # Format for inputs
        address_format = "address="
        #format the locations
        loc = re.sub(r"[^a-zA-Z0-9]+", '+', depot)
        # Format for the address
        add_address = loc
        # Request url
        request_url = request_url \
                    + output_format \
                    + address_format \
                    + add_address \
                    + google_maps_api_key
        resp = requests.get(request_url, timeout=5)
        json_resp = resp.json()
        depot_lat_lng = {
            'lat': json_resp['results'][0]['geometry']['location']['lat'],
            'lng': json_resp['results'][0]['geometry']['location']['lng']
        }
        address_list = [[depot] for i in range(number_of_clusters)]
        order_id_list = [["Start/EndHUBS"] for i in range(number_of_clusters)]
        latlng_list = [[depot_lat_lng] for i in range(number_of_clusters)]
        capacity_demand_list = [[0] for i in range(number_of_clusters)]
    else:
        address_list = [[] for i in range(number_of_clusters)]
        order_id_list = [[] for i in range(number_of_clusters)]
        latlng_list = [[] for i in range(number_of_clusters)]
        capacity_demand_list = [[] for i in range(number_of_clusters)]
    for index, row in clustered_df.iterrows():
        address_list[int(row['cluster_no'])].append(row['deliveryAddress'])
        latlng_pair = {
            "lat": row['delivery_lat'],
            "lng": row['delivery_lon']
        }
        latlng_list[int(row['cluster_no'])].append(latlng_pair)
        capacity_demand_list[int(row['cluster_no'])].append(max(int(float(row[unit])), 1))
        order_id_list[int(row['cluster_no'])].append(row['order_id']+"DROP")
    return address_list, order_id_list, latlng_list, capacity_demand_list



def parse_input(json_input, no_drivers):

    USE_LAT_LNG_FIRST = False

    df = pd.DataFrame.from_records(json_input)
    df.drop(df.columns.difference(['Address','Number', 'Latitude', 'Longitude', 'Weight', 'Volume']), 1, inplace=True)
    df = df.rename(columns={"Address":"deliveryAddress",
        "Number": "order_id",
        "Latitude": "del_lat_prime",
        "Longitude": "del_lon_prime",
        "Weight": "weight",
        "Volume": "volume"
    })
    df = geo_coding(df)

    if(USE_LAT_LNG_FIRST):
        df.del_lat_prime.fillna(df.delivery_lat, inplace=True)
        df.del_lon_prime.fillna(df.delivery_lon, inplace=True)
        del df['delivery_lat']
        del df['delivery_lon']
        df = df.rename(columns={"del_lat_prime": "delivery_lat", "del_lon_prime": "delivery_lon"})
    else:
        del df['del_lat_prime']
        del df['del_lon_prime']
    df = df.reindex(sorted(df.columns), axis=1)
    non_operational_orders = check_coverage(df)
    order_numbers = df['order_id']
    query_params = Q(number__in=order_numbers)
    order_objects = Order.objects.filter(query_params)
    non_operational_orders = check_order_stops(order_objects, non_operational_orders)
    no_order_list = []
    for item in non_operational_orders:
        no_order_list.append(item["order_number"])
    df = df[~df["order_id"].isin(no_order_list)]
    geo_point_matrix = df.as_matrix(columns=df.columns[1:3])
    print("asdfasdfasdfasdf")
    xtoc, number_of_clusters = optimal_clustering(geo_point_matrix, no_drivers)
    print("hwoeoww")
    df['cluster_no']=xtoc
    df = df.sort_values(by=['cluster_no'])
    return df, number_of_clusters, non_operational_orders


def check_coverage(df):

    non_operational_orders = []
    for _, row in df.iterrows():
        delivery_city = get_city_from_latlong( latitude = row['delivery_lat'],
            longitude = row['delivery_lon'],
            field = "shipment_coverage"
        )
        if delivery_city is None:
            non_operational_orders.append({
                "order_number": row['order_id'],
                "coverage": "shipment_coverage"
            })

    return non_operational_orders


def geo_coding(loc_df):
    loc_list = []
    for loc in range(len(loc_df)):
        loc_list.append(loc_df.iloc[loc]["deliveryAddress"])
    pool = Pool(15)
    latlng_list = pool.map(get_latlng_from_address, loc_list)
    lat_list = []
    lng_list = []
    for item in latlng_list:
        if isinstance(item[1],dict):
            lat_list.append(item[1]['lat'])
            lng_list.append(item[1]['lng'])
        else:
            lat_list.append(0.0)
            lng_list.append(0.0)
    loc_df['delivery_lat'] = lat_list
    loc_df['delivery_lon'] = lng_list
    return loc_df

def get_latlng_from_address(address):
    try:
        address = str(address)
        url = "https://maps.googleapis.com/maps/api/geocode/json?address={}&key={}".format(re.sub(r"[^a-zA-Z0-9]+", '+', address), settings.GOOGLE_GEOCODING_DISTANCEMATRIX_API_KEY)
        res = requests.get(url).json()['results']
        is_correct = True
        if len(res) < 1: # Address not matching any coordinates - Non-operational with wrong_address
            print("Incorrect Address Error")
            is_correct = False
        else:
            res = {
               'lat': res[0]['geometry']['location']['lat'],
               'lng': res[0]['geometry']['location']['lng']
            }
    except Exception as e:
        print("Network Error! Please check your connection.")
        print(e)
        is_correct = False
    return (address, res, is_correct)


def get_distance_matrix_from_list(latlng_list, constraint_given):
    global constraint
    constraint = constraint_given
    dist_matrix = []
    pool = Pool(15)
    for loc1 in latlng_list:
        temp_list = []
        for loc2 in latlng_list:
            url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={}%2C{}&destinations={}%2C{}&mode=driving&language=en-EN&key={}".format(loc1['lat'], loc1['lng'], loc2['lat'], loc2['lng'], settings.GOOGLE_GEOCODING_DISTANCEMATRIX_API_KEY)
            temp_list.append(url)
        dist_list = pool.map(get_distance_from_url, temp_list)
        dist_matrix.append(dist_list)
    return dist_matrix


def get_distance_matrix(latlng_list, constraint_given):
    global constraint
    constraint = constraint_given
    distance_matrix = []
    slice_limit = int(floor(len(latlng_list)/10.0))
    for row_index in range(slice_limit+1):
        temp_matrix = [[] for _ in range(10)]
        for column_index in range(slice_limit+1):
            sources = latlng_list[(row_index*10):min([((row_index+1)*10), len(latlng_list)])]
            destinations = latlng_list[(column_index*10):min([((column_index+1)*10), len(latlng_list)])]
            response_matrix = get_matrix_subset(sources, destinations, constraint_given)
            for index in range(len(response_matrix)):
                temp_matrix[index].extend(response_matrix[index])
        for item in temp_matrix:
            if item:
                distance_matrix.append(item)
    return distance_matrix


def get_matrix_subset(sources, destinations, constraint_given):
    origins_text = "?origins="
    for origin in sources:
        origins_text = origins_text + "{}%2C{}|".format(origin["lat"], origin["lng"])
    origins_text = origins_text[:-1]
    destinations_text = "&destinations="
    for destination in destinations:
        destinations_text = destinations_text + "{}%2C{}|".format(destination["lat"], destination["lng"])
    destinations_text = destinations_text[:-1]
    key_text = "&mode=driving&language=en-EN&key={}".format(settings.GOOGLE_GEOCODING_DISTANCEMATRIX_API_KEY)
    url = "https://maps.googleapis.com/maps/api/distancematrix/json"+origins_text+destinations_text+key_text
    session = requests.Session()
    retry = Retry(connect=5, backoff_factor=0.7)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    res = session.get(url, timeout=2).json()
    subset_matrix = resolve_response(res, constraint_given)
    return subset_matrix

def resolve_response(response, constraint_given):
    response = response["rows"]
    subset_matrix = []
    for row in response:
        temp = []
        for element in row["elements"]:
            temp.append(element[constraint_given]['value'])
        subset_matrix.append(temp)
    return subset_matrix



def get_distance_from_url(url):
    global constraint
    url = str(url)
    session = requests.Session()
    retry = Retry(connect=5, backoff_factor=0.7)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    res = session.get(url, timeout=2).json()
    if res['status'] == "OK":
        res = res['rows']
    dist = res[0]['elements'][0][constraint]['value']
    return dist


def hub_checker(order_objects):
    selected_hub = order_objects[0].hub
    for order in order_objects:
        if order.hub != selected_hub:
            return False
    return True

def check_order_stops(order_objects, non_operational_orders):
    for order in order_objects:
        ongoing_stop = order.stops.filter(trip__status__in=[StatusPipeline.TRIP_NEW,
                                            StatusPipeline.TRIP_IN_PROGRESS,
                                            StatusPipeline.DRIVER_ACCEPTED]).first()
        if order.status in StatusPipeline.SHIPMENT_TRIP_ORDER_STATUSES:
            non_operational_orders.append({
                "order_number": str(order.number),
                "coverage": "end-status_or_already_fulfilled"
            })
        elif ongoing_stop and ongoing_stop.status in [
            StatusPipeline.TRIP_NEW,
            StatusPipeline.TRIP_IN_PROGRESS]:
            non_operational_orders.append({
                "order_number": str(order.number),
                "coverage": "end-status_or_already_fulfilled"
            })
    return non_operational_orders

def check_new_order_pickup(order_objects):
    for order in order_objects:
        if order.status is StatusPipeline.ORDER_NEW:
            if order.has_pickup:
                return True
    return False

def save_batch_routes_stops(data, response, source):
    routes_batch = RoutesBatch(
        batch_id=response["batch_id"],
        input_body=data,
        response_body=response,
        source=source,
        is_success=True
    )
    routes_batch.save()
    for clusters in response["rows"]:
        for routes in clusters["routes_information"]:
            if data["constraint"] == "distance":
                route_object = OptimalRoute(
                    route_id=response["batch_id"]+str(clusters["cluster_id"])+str(routes["route_id"]),
                    estimated_distance=routes["route_distance"]/1000.00,
                    load_carried=routes["route_load"],
                    batch=routes_batch,
                    source=routes["source"]
                )
            else:
                route_object = OptimalRoute(
                    route_id=response["batch_id"]+str(clusters["cluster_id"])+str(routes["route_id"]),
                    estimated_duration=routes["route_distance"]/3600.00,
                    load_carried=routes["route_load"],
                    batch=routes_batch,
                    source=routes["source"]
                )
            route_object.save()
            stop_objects = []
            for stops in routes["elements"]:
                stop_objects.append(
                    OptimalRouteStop(**{
                        "route": route_object,
                        "stop_type": stops["type"],
                        "stop_order": stops["order_id"],
                        "stop_address": stops["stop"],
                        "sequence": stops["stop_sequence"],
                        "capacity_demand": stops["capacity_demand"],
                        "stop_lat": stops["stop_coordinates"]["latitude"],
                        "stop_lng": stops["stop_coordinates"]["longitude"]
                    })
                )
            OptimalRouteStop.objects.bulk_create(stop_objects)
    return

def parse_data(data, current_location, source):

    #log the api call at entry, mark success at exit
    transaction_id = str(uuid.uuid4())
    routes_batch = RoutesBatch(
        batch_id=transaction_id,
        input_body=data,
        response_body={},
        source=source
    )
    routes_batch.save()
    stop_data = data ["stops"]
    order_list = ["Current Location"]
    lat_lng_list = [current_location]
    sequence_list = [999999]
    for stop in stop_data:
        order_list.append(stop["order"]["number"])
        lat_lng_list.append({
            "lng": stop["order"]["shipping_address"]["geopoint"]["coordinates"][0],
            "lat": stop["order"]["shipping_address"]["geopoint"]["coordinates"][1]
        })
        sequence_list.append(stop["sequence"])

    return transaction_id, order_list, lat_lng_list, sequence_list

def format_stop_data(stops):
    data = {}
    stop_data = []
    default = {
            "lng":77.5946,
            "lat":12.9716
        }
    for stop in stops:
        temp = {
            'order' : {}
        }
        temp['sequence'] = int(stop.sequence)
        temp['order']['number'] = stop.order.number
        temp['order']['shipping_address'] = {}
        if stop.stop_address is not None:
            temp['order']['shipping_address']['line1'] = stop.stop_address.line1
            if stop.stop_address.geopoint:
                coordinates = {
                    "lng":stop.stop_address.geopoint.x,
                    "lat":stop.stop_address.geopoint.y
                }
            else:
                coordinates = get_latlng_from_address(temp['order']['shipping_address']['line1'])
                coordinates = coordinates[1] if coordinates[2] else default
        else:
            temp['order']['shipping_address']['line1'] = stop.order.shipping_address.line1
            if stop.order.shipping_address.geopoint:
                coordinates = {
                    "lng":stop.order.shipping_address.geopoint.x,
                    "lat":stop.order.shipping_address.geopoint.y
                }
            else:
                coordinates = get_latlng_from_address(temp['order']['shipping_address']['line1'])
                coordinates = coordinates[1] if coordinates[2] else default
        temp['order']['shipping_address']['geopoint'] = {}
        temp['order']['shipping_address']['geopoint']['coordinates'] = [ coordinates["lng"], coordinates["lat"] ]
        stop_data.append(temp)
    data['stops'] = stop_data
    return data

def plan_route(trip, source="Trip Create/Change", current_location=None):

    stops = Stop.objects.filter(trip=trip,
                                    status__in= [StatusPipeline.TRIP_IN_PROGRESS, StatusPipeline.TRIP_NEW]
                                ).order_by('sequence')
    if len(stops)<3:
        print("Route already optimised!")
        trip.is_route_optimal = True
        trip.save()
        return True
    # if trip.status == StatusPipeline.TRIP_NEW or trip.status == StatusPipeline.DRIVER_ACCEPTED:
    #     current_location = get_latlng_from_address(str(stops[0].order.hub.address))[1]
    # elif trip.status == StatusPipeline.TRIP_IN_PROGRESS:
    #     if current_location is None:
    #         current_location = get_latlng_from_address(str(stops[0].order.hub.address))[1]
    # else:
    #     print("Trip is already completed!")
    #     return False

    if not current_location:
        current_location = \
        get_latlng_from_address(str(stops[0].order.hub.address))[1]

    data = format_stop_data(stops)
    transaction_id, order_list, lat_lng_list, sequence_list = parse_data(data, current_location, source)
    distance_matrix = get_distance_matrix(lat_lng_list, "distance")
    solved_route_schema = run_route_finder(
                            distance_matrix,
                            1,
                            99999999999,
                            [1]*int(len(order_list)),
                            99999999999
                        )
    solved_route_schema["status"] = "Success"
    # solved_route_schema["prev_distance"] = sum(distance_matrix[0])
    sequence_list.sort()
    stops_revised_sequence = solved_route_schema["route_list"][0]
    try:
        stops_revised_sequence = stops_revised_sequence[1:-1]
    except:
        print("Constraints violated please check overall input!")
        return False

    routes_batch = RoutesBatch.objects.filter(batch_id=transaction_id).first()

    sequence_list = sequence_list[:-1]
    stop_history_list = []
    with transaction.atomic():
        for i in range(len(sequence_list)):
            # if i==0 and trip.status==StatusPipeline.TRIP_IN_PROGRESS:
            #     stops[stops_revised_sequence[i]-1].status = StatusPipeline.TRIP_IN_PROGRESS

            stop_history_list.append(StopHistory(routes_batch=routes_batch,
                                       stop=stops[
                                           stops_revised_sequence[i] - 1],
                                       old_sequence=stops[
                                           stops_revised_sequence[
                                               i] - 1].sequence,
                                       new_sequence=sequence_list[i]))

            stops[stops_revised_sequence[i]-1].sequence=sequence_list[i]
            stops[stops_revised_sequence[i]-1].save()
    StopHistory.objects.bulk_create(stop_history_list)
    print(stops_revised_sequence)
    print(sequence_list)
    print(distance_matrix)
    trip.is_route_optimal = True
    trip.save()
    RoutesBatch.objects.filter(batch_id=transaction_id).update(response_body=solved_route_schema, is_success=True)
    return True
