from django.db import models
from django.conf import settings
from django.db.models import Q, Prefetch, F
from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import ArrayField, JSONField
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.db.models import PointField

#BH imports
from blowhorn.common.models import BaseModel

#3rd party libraries
from blowhorn.oscar.core.loading import get_model

Trip = get_model('trip', 'Trip')

class RoutesBatch(BaseModel):
    """
    Batch transactions from Route Optimiser
    """

    batch_id = models.CharField(
        _("Batch ID"), max_length=100, db_index=True, unique=True)

    source = models.CharField(
        _("Source of Batch"),  max_length=100, null=True)

    input_body = JSONField()

    response_body = JSONField()

    is_success = models.BooleanField(
        _("Is Route Optimiser Transaction Successful"), default=False, null=True, blank=True)

    def __str__(self):
        return self.batch_id

class OptimalRoute(BaseModel):
    """
    Optimised Route Model
    """
    route_id = models.CharField(
        _("Route ID"), max_length=128, unique=True)

    estimated_distance = models.DecimalField(
        _("Estimated Distance (km)"), max_digits=15, decimal_places=2, null=True, blank=True, default=0.0)

    estimated_duration = models.DecimalField(
        _("Estimated Duration (hours)"), max_digits=5, decimal_places=2, null=True, blank=True, default=0.0)

    load_carried = models.IntegerField(
        _("Estimated load carried in Route (Weight(kg)/Volume(cm3))"), null=True, blank=True, default=0)

    batch = models.ForeignKey(
        RoutesBatch, on_delete=models.PROTECT, blank=True, null=True)

    is_trip_created = models.BooleanField(
        _("Is Trip Created for the Route"), default=False, db_index=True)

    is_saved = models.BooleanField(
        _("Is Route saved for Later Use"), default=False, db_index=True)

    source = models.CharField(
        _("Source of Batch"),  max_length=100, null=True, blank=True)

    trip = models.ForeignKey(Trip, related_name='trip',
                        null=True, blank=True, on_delete=models.PROTECT)


class OptimalRouteStop(models.Model):
    """
    Stop in the Optimal Route
    """
    route = models.ForeignKey(OptimalRoute,related_name='stops',
                            null=True, on_delete=models.PROTECT)

    stop_type = models.CharField(
        _("Stop Type"), blank=True, max_length=50)

    stop_order = models.CharField(
        _("Order ID of the Stop Order"), max_length=64 ,null=True, blank=True)

    stop_address = models.TextField(
        _("Stop Address"), null=True, blank=True)

    sequence = models.PositiveIntegerField(_("Sequence ID"),
                                        null=True, blank=True)

    capacity_demand = models.PositiveIntegerField(
        _("Weight/Volume for the Stop Order"), null=True, blank=True)

    stop_lat = models.DecimalField(
        _("Stop Latitude"), max_digits=9, decimal_places=7, null=True, blank=True, default=0.0)

    stop_lng = models.DecimalField(
        _("Stop Longitude"), max_digits=9, decimal_places=7, null=True, blank=True, default=0.0)

    geopoint = PointField(null=True, blank=True, srid=4326, help_text=_(
        "Represented as (longitude, latitude)"))

    def save(self, *args, **kwargs):
        try:
            self.geopoint = PointField(self.stop_lng, self.stop_lat)
        except:
            self.geopoint = PointField(0.0, 0.0)
            print("Hello")
        super(OptimalRouteStop, self).save(*args, **kwargs)




