from django.conf.urls import url
from django.apps import AppConfig


from .views import (
    RoutesHubToDeliveryRaw,
    FleetRouteOptimiserHubWise,
    OptimalRoutePlanner,
)


urlpatterns = [
    url(r'^routeoptimiser/hubtodelivery/$',
        RoutesHubToDeliveryRaw.as_view(), name='pickup-delivery-v1'),
    url(r'^routeoptimiser/fleetdashboardroutes/$',
        FleetRouteOptimiserHubWise.as_view(), name='fleet-routes'),
    url(r'^drivers/v3/routeoptimiser/plan',
        OptimalRoutePlanner.as_view(), name='driver-routes'),


]
