from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

def set_pd_points(numberOfDestinations):
    pickupdelivery = []
    for i in range(1, numberOfDestinations, 2):
        pickupdelivery.append([i, i+1])
    return pickupdelivery

def create_data_model(distanceMatrix, numberOfVehicles):
    data = {}
    data['distance_matrix'] = distanceMatrix
    data['num_vehicles'] = numberOfVehicles
    data['depot'] = 0
    data['pickups_deliveries'] = set_pd_points(len(distanceMatrix))
    return data

def frame_solution(data, manager, routing, assignment):
    total_distance = 0
    distances = []
    plan_output = []
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        # plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        plan_output_driver_wise = []
        route_distance = 0
        while not routing.IsEnd(index):
            #plan_output += ' {} -> '.format(manager.IndexToNode(index))
            plan_output_driver_wise.append(manager.IndexToNode(index))
            previous_index = index
            index = assignment.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id)
        plan_output_driver_wise.append(manager.IndexToNode(index))
        distances.append(route_distance)
        plan_output.append(plan_output_driver_wise)
        total_distance += route_distance
    solution_data = {}
    solution_data['total_distance'] = total_distance
    solution_data['route_distances'] = distances
    solution_data['route_list'] = plan_output
    print(solution_data)
    return solution_data


def run_route_finder(distanceMatrix, numberOfVehicles, limitingDistance):
    # Instantiate the data problem.
    # [START data]
    data = create_data_model(distanceMatrix, numberOfVehicles)
    # [END data]

    # Create the routing index manager.
    # [START index_manager]
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'],
                                           data['depot'])
    # [END index_manager]

    # Create Routing Model.
    # [START routing_model]
    routing = pywrapcp.RoutingModel(manager)

    # [END routing_model]

    # Define cost of each arc.
    # [START arc_cost]
    def distance_callback(from_index, to_index):
        """Returns the manhattan distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)
    # [END arc_cost]

    # Add Distance constraint.
    # [START distance_constraint]
    dimension_name = 'Distance'
    routing.AddDimension(
        transit_callback_index,
        0,  # no slack
        limitingDistance,  # vehicle maximum travel distance
        True,  # start cumul to zero
        dimension_name)
    distance_dimension = routing.GetDimensionOrDie(dimension_name)
    distance_dimension.SetGlobalSpanCostCoefficient(100)
    # [END distance_constraint]

    # Define Transportation Requests.
    # [START pickup_delivery_constraint]
    for request in data['pickups_deliveries']:
        pickup_index = manager.NodeToIndex(request[0])
        delivery_index = manager.NodeToIndex(request[1])
        routing.AddPickupAndDelivery(pickup_index, delivery_index)
        routing.solver().Add(
            routing.VehicleVar(pickup_index) == routing.VehicleVar(
                delivery_index))
        routing.solver().Add(
            distance_dimension.CumulVar(pickup_index) <=
            distance_dimension.CumulVar(delivery_index))
    # [END pickup_delivery_constraint]

    # Setting first solution heuristic.
    # [START parameters]
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PARALLEL_CHEAPEST_INSERTION)
    # [END parameters]

    # Solve the problem.
    # [START solve]
    assignment = routing.SolveWithParameters(search_parameters)
    # [END solve]

    # Print solution on console.
    # [START frame_solution]
    if assignment:
        return frame_solution(data, manager, routing, assignment)
    else:
        return "Not possible within given Constraints. Please Increase distance or drivers."
    # [END frame_solution]
