from __future__ import division
from haversine import haversine
import random
import numpy as np
from scipy.spatial.distance import cdist
from scipy.sparse import issparse

def randomsample( X, n ):
    """ random.sample of the rows of X
        X may be sparse -- best csr
    """
    sampleix = random.sample(range( X.shape[0] ), int(n) )
    return X[sampleix]

def kmeans(X, number_centres, delta=0.001, maxiter=20, metric="euclidean", p=2, \
    verbose=0, restrict_comp_to_close=False):

    centres = randomsample(X, number_centres)

    if not issparse(X):
        X = np.asanyarray(X)
    if issparse(centres):
        centres = centres.todense()
    else:
        centres.copy()

    N, dim = X.shape
    k, cdim = centres.shape
    if dim != cdim:
        raise ValueError("kmeans -> X {} and centres {} must have the same \
        number of columns".format(X.shape, centres.shape))
    if verbose:
        print("kmeans -> X={} centres={} delta={} maxiter={} metric={}" \
            .format(X.shape, centres.shape, delta, maxiter, metric))

    allx = np.arange(N)
    prevdist = 0
    for jiter in range(1, maxiter+1):
        if isinstance(metric, str) and metric.startswith('mydist'):
            mydis = lambda x,y: haversine(x,y)
            D = cdist(X, centres)
            close = np.argsort(D, axis=1)[:,:10]
            xtoc = np.array([close[i][np.argmin(cdist(np.atleast_2d(X[i]),
                            centres[close[i]], mydis))]
                            for i in range(N)])
        else:
            if restrict_comp_to_close:
                D = cdist(X, centres)
                if verbose>=2:
                    print("restrict attention to close centroids...")
                close = np.argsort(D, axis=1)[:,:10]
                D = np.inf*np.ones((N,k))
                if verbose>=2:
                    print("computing distance to close centroids...")
                for i in range(N):
                    D[i, close[i]] = cdist(np.atleast_2d(X[i]),
                                        centres[close[i]],
                                        metric)
                print("done... Assigning points to the closest centroid...")
                xtoc = D.argmin(axis=1)
            else:
                D = cdist_sparse(X, centres, metric=metric, p=p)
                xtoc = D.argmin(axis=1)
        distances = D[allx, xtoc]
        avdist = distances.mean()
        if verbose>=2:
            print("kmeans -> av |X-nearest centre| = {}".format(avdist))
        if (1 - delta)*prevdist <= avdist <= prevdist \
        or jiter == maxiter:
            break
        prevdist = avdist
        for jc in range(k):
            c = np.where(xtoc == jc)[0]
            if len(c) > 0:
                centres[jc] = X[c].mean(axis=0)
    if verbose:
        print("kmeans -> {} iterations; cluster sizes: {}".format(jiter,
            np.bincount(xtoc)))
    if verbose>=2:
        r50 = np.zeros(k)
        r90 = np.zeros(k)
        for j in range(k):
            dist = distances[ xtoc==j ]
            if len(dist)>0:
                r50[j], r90[j] = np.percentile(dist, (50, 90))
        print("kmeans -> cluster 50 {} radius".format(r50.astype(int)))
        print("kmeans -> cluster 90 {} radius".format(r90.astype(int)))

    return centres, xtoc, distances, X

def kmeanssample( X, k, nsample=0, **kwargs ):
    """ 2-pass kmeans, fast for large N:
        1) kmeans a random sample of nsample ~ sqrt(N) from X
        2) full kmeans, starting from those centres
    """
    N, dim = X.shape
    if nsample == 0:
        nsample = max( 2*np.sqrt(N), 10*k )
    Xsample = randomsample( X, int(nsample) )
    pass1centres = randomsample( X, int(k) )
    samplecentres = kmeans( Xsample, pass1centres, **kwargs )[0]
    return kmeans( X, samplecentres, **kwargs )


def cdist_sparse( X, Y, **kwargs ):
    """ -> |X| x |Y| cdist array, any cdist metric
        X or Y may be sparse -- best csr
    """
    sxy = 2*issparse(X) + issparse(Y)
    if sxy == 0:
        return cdist( X, Y, **kwargs )
    d = np.empty( (X.shape[0], Y.shape[0]), np.float64 )
    if sxy == 2:
        for j, x in enumerate(X):
            d[j] = cdist( x.todense(), Y, **kwargs ) [0]
    elif sxy == 1:
        for k, y in enumerate(Y):
            d[:,k] = cdist( X, y.todense(), **kwargs ) [0]
    else:
        for j, x in enumerate(X):
            for k, y in enumerate(Y):
                d[j,k] = cdist( x.todense(), y.todense(), **kwargs ) [0]
    return d


def nearestcentres( X, centres, metric="euclidean", p=2 ):
    """ each X -> nearest centre, any metric
            euclidean2 (~ withinss) is more sensitive to outliers,
            cityblock (manhattan, L1) less sensitive
    """
    D = cdist( X, centres, metric=metric, p=p )  # |X| x |centres|
    return D.argmin(axis=1)

def Lqmetric( x, y=None, q=.5 ):
    # yes a metric, may increase weight of near matches; see ...
    return (np.abs(x - y) ** q) .mean() if y is not None \
        else (np.abs(x) ** q) .mean()

class Kmeans:
    """ km = Kmeans( X, k= or centres=, ... )
        in: either initial centres= for kmeans
            or k= [nsample=] for kmeanssample
        out: km.centres, km.Xtocentre, km.distances
        iterator:
            for jcentre, J in km:
                clustercentre = centres[jcentre]
                J indexes e.g. X[J], classes[J]
    """
    def __init__( self, X, k=0, centres=None, nsample=0, **kwargs ):
        self.X = X
        if centres is None:
            self.centres, self.Xtocentre, self.distances = kmeanssample(
                X, k=k, nsample=nsample, **kwargs )
        else:
            self.centres, self.Xtocentre, self.distances = kmeans(
                X, centres, **kwargs )

    def __iter__(self):
        for jc in range(len(self.centres)):
            yield jc, (self.Xtocentre == jc)\


