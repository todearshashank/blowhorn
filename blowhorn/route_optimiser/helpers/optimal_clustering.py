import pandas as pd
import numpy as np
from .kmeans import kmeans
from scipy.spatial.distance import cdist
from haversine import haversine
from math import sqrt


MAX_KNOWN_CLUSTER_LIMIT_PER_ORDERS = 160
UNIT_NUMBER_OF_ORDERS_PER_CLUSTER = 20.0

def find_optimal_number(geo_point_matrix):
    K = range(1, len(geo_point_matrix))
    km = [kmeans(geo_point_matrix, i, metric="mydist:haversine") for i in K]
    centroids = [centres for (centres, xtoc, distances, X) in km]
    var = [xtoc for (centres, xtoc, distances, X) in km]
    mydis = lambda x,y: haversine(x,y)
    D_k = [cdist(geo_point_matrix, centroid, mydis) for centroid in centroids]
    cIdx = [np.argmin(D, axis=1) for D in D_k]
    dist = [np.min(D, axis=1) for D in D_k]
    avgWithinSS = [sum(d)/geo_point_matrix.shape[0] for d in dist]
    error_threshold = max(1/len(geo_point_matrix)**(0.5), 0.01)
    for i in range(len(avgWithinSS)-1):
        if(avgWithinSS[i]<error_threshold*avgWithinSS[0]):
            break
    kIdx = i
    return K[kIdx]


def optimal_known(geo_point_matrix_len):
    return max(round(geo_point_matrix_len/UNIT_NUMBER_OF_ORDERS_PER_CLUSTER), 1)

def optimal_clustering(geo_point_matrix, no_drivers):
    if (len(geo_point_matrix)<=MAX_KNOWN_CLUSTER_LIMIT_PER_ORDERS):
        optimal_number = min(optimal_known(len(geo_point_matrix)), no_drivers)
    else:
        optimal_number = min(find_optimal_number(geo_point_matrix), no_drivers)

    centroids, xtoc, distances, X = kmeans(geo_point_matrix,
                                        optimal_number,
                                        metric="mydist:haversine")
    return xtoc, optimal_number
