import logging

from rest_framework import serializers

import blowhorn.oscar.models.fields

logger = logging.getLogger(__name__)


def expand_field_mapping(extra_fields):
    # This doesn't make a copy
    field_mapping = serializers.ModelSerializer.serializer_field_mapping
    field_mapping.update(extra_fields)
    return field_mapping


class OscarSerializer(object):
    field_mapping = expand_field_mapping(
        {
            blowhorn.oscar.models.fields.NullCharField: serializers.CharField,
            # models.ImageField: ImageUrlField,
        }
    )

    def __init__(self, *args, **kwargs):
        """
        Allow the serializer to be initiated with only a subset of the
        specified fields
        """
        fields = kwargs.pop("fields", None)
        super(OscarSerializer, self).__init__(*args, **kwargs)
        if fields:
            allowed = set(fields)
            existing = set(self.fields.keys())  # pylint: disable=no-member
            for field_name in existing - allowed:
                self.fields.pop(field_name)  # pylint: disable=no-member

    def update_relation(self, name, manager, values):
        if values is None:
            return

        serializer = self.fields[name]  # pylint: disable=no-member

        # use the serializer to update the attribute_values
        updated_values = serializer.update(manager, values)

        if self.partial:  # pylint: disable=no-member
            manager.add(*updated_values)
        elif hasattr(manager, "field") and not manager.field.null:
            # add the updated_attribute_values to the instance
            manager.add(*updated_values)
            # remove all the obsolete attribute values, this could be caused by
            # the product class changing for example, lots of attributes would become
            # obsolete.
            current_pks = [p.pk for p in updated_values]
            manager.exclude(pk__in=current_pks).delete()
        else:
            manager.set(updated_values)


class OscarModelSerializer(OscarSerializer, serializers.ModelSerializer):
    """
    Correctly map oscar fields to serializer fields.
    """


class OscarHyperlinkedModelSerializer(
    OscarSerializer, serializers.HyperlinkedModelSerializer
):
    """
    Correctly map oscar fields to serializer fields.
    """
