from rest_framework import serializers

from blowhorn.order.models import Order


class BillingAddressSerializer(serializers.ModelSerializer):
    class Meta:
        # from blowhorn.order.serializers.shipment import BillingAddress
        model = Order
        fields = '__all__'
