# import datetime
import requests
import re
from django.conf import settings
from urllib.parse import urljoin
from django.core.exceptions import ValidationError

from blowhorn.driver.models import Driver
from blowhorn.customer.models import Customer
from blowhorn.exotel.constants import *


class Exotel(object):

    def __init__(self):
        self.sid = settings.EXOTEL_SID
        self.token = settings.EXOTEL_TOKEN
        self.baseurl = 'https://{sid_one}:{token}@api.exotel.com/v1/Accounts/{sid}/'.format(
            sid_one=self.sid, token=self.token, sid=self.sid)
        # self.caller_id = CALLER_ID
        self.timelimit = CALL_TIMELIMIT
        self.timeout = CALL_TIMEOUT
        self.status_callback = settings.HOST_URL + STATUS_CALLBACK_URL
        self.custom_data = None
        self.flow_id = FLOW_ID
        self.call_type = CALL_TYPE["TRANSACTIONAL"]

    def validate_call_data(self, data):
        if not data.get('From', None):
            raise ValidationError('From number is mandatorily required')

        if not data.get('To', None):
            raise ValidationError("To number is mandatorily required")

        if not data.get('CallerId', None):
            raise ValidationError("Caller ID is mandatorily required")

        return True

    def validate_sms_data(self, data):
        if not data.get('From', None):
            raise ValidationError('From number is mandatorily required')

        if not data.get('To', None):
            raise ValidationError("To number is mandatorily required")

        if not data.get('Body', None):
            raise ValidationError("Body of message is mandatorily required")

        return True

    def validate_call_flow_data(self, data):
        if not data.get('From', None):
            raise ValidationError('From number is mandatorily required')

        if not data.get('CallerId', None):
            raise ValidationError("Caller ID is mandatorily required")

        if not data.get('Url', None):
            raise ValidationError("Url is needed to connect to the appflow")

        return True

    def validate_whitelisting_data(self, data):
        if not data.get('CallerId', None):
            raise ValidationError("Caller ID is mandatorily required")

        if not data.get('Numbers', []):
            raise ValidationError("No number provided for the whitelisting")

        return True

    def validate_exophone_data(self, data):
        if not data.get('PhoneNumber', None):
            raise ValidationError("Mention the Exophone that you wish to get")

        return True

    def generate_request(self, http_method, url_endpoint, data):
        if http_method == "POST":
            print("This is the data === ", data)
            return requests.post(urljoin(self.baseurl, url_endpoint),
                                 auth=(self.sid, self.token),
                                 data=data)
        if http_method == "GET":
            return requests.get(urljoin(self.baseurl, url_endpoint),
                                auth=(self.sid, self.token))

    def get_input_data(self, input_data, request, call_flow=False):
        obj_id = input_data.get("obj_id")
        # order_number = input_data.get("order_number")
        phone_number = input_data.get("phone_number")

        if not phone_number:
            if not obj_id:
                raise ValidationError(
                    "No phone number for driver/customer was provided")

            if input_data.get("is_driver"):
                obj = Driver.objects.filter(id=obj_id).first()
                if not obj:
                    raise ValidationError("No driver exists with that id")
            else:
                obj = Customer.objects.filter(id=obj_id).first()
                if not obj:
                    raise ValidationError("No customer exists with that id")

            to_phone_number = obj.user.national_number

            if not to_phone_number:
                raise ValidationError("No phone number exists")
        else:
            to_phone_number = re.compile(
                r'(\d{10})$').search(phone_number).group(1)

        from_phone_number = request.user.phone_number
        if not from_phone_number:
            raise ValidationError(
                "Requested user doesn't have the phone number")

        from_phone_number = from_phone_number.national_number
        # custom_data = {
        #     'order_number': order_number
        # }
        # from_phone_number = input_data.get('from')
        # to_phone_number = input_data.get('to')

        if input_data.get('city', None) and input_data['city'] in CALLER_ID_CITY_MAPPING:
            caller_id = CALLER_ID_CITY_MAPPING[input_data['city']]
        else:
            caller_id = CALLER_ID_CITY_MAPPING["Bengaluru"]

        data = {
            'From': from_phone_number,
            'To': to_phone_number,
            'CallerId': caller_id,
            'TimeLimit': self.timelimit,
            'CallType': self.call_type,
            'TimeOut': self.timeout,
            'StatusCallback': self.status_callback
        }

        if call_flow:
            data['Url'] = "http://my.exotel.in/exoml/start/{flow_id}".format(
                flow_id=self.flow_id)

        return data

    def get_callflow_data(self, input_data, request):
        # if not input_data.get('from_phone_number'):

        if not (input_data.get('from_phone_number', None) or
                input_data.get('caller_id', None) or
                input_data.get('app_id')):
            raise ValidationError(
                "Need phone number, caller id of the city and app id")

        data = {
            'From': input_data['from_phone_number'],
            # 'To': to_phone_number,
            'CallerId': input_data['caller_id'],
            'TimeLimit': self.timelimit,
            'CallType': self.call_type,
            'TimeOut': self.timeout,
            'StatusCallback': self.status_callback,
            'Url': "http://my.exotel.in/exoml/start_voice/{flow_id}".format(
                flow_id=input_data['app_id'])
        }

        return data
