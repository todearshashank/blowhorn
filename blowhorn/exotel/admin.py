from django.contrib import admin

from blowhorn.exotel.models import CallDetail
from blowhorn.exotel.constants import OUTGOING
from blowhorn.exotel.forms import ExotelCallDetailForm


class CallDetailAdmin(admin.ModelAdmin):
    """Admin view to display the call details of the calls made via exotel"""

    form = ExotelCallDetailForm

    list_display = ('id', 'order', 'sid', 'agent_number',
                    'client_number', 'created_timestamp', 'status')

    list_filter = ['agent_number', 'client_number', 'recipient']

    def get_fieldsets(self, request, obj=None):
        if obj and obj.order:
            fieldsets = (
                ('Call Details', {
                    'fields': ('sid', 'agent_number', 'client_number', 'order',
                               'created_timestamp', 'recipient', 'status',
                               'call_type')
                }),
                ('Audit Log', {
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                }),
            )
        else:
            fieldsets = (
                ('Call Details', {
                    'fields': ('sid', 'agent_number', 'client_number', 'order',
                               'created_timestamp', 'recipient', 'status',
                               'call_type', 'existing_customer',
                               'order_number')
                }),
                ('Audit Log', {
                    'fields': ('created_date', 'created_by', 'modified_date',
                               'modified_by'),
                }),
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ('created_date', 'created_by', 'modified_date',
                           'modified_by', 'sid', 'agent_number',
                           'client_number', 'created_timestamp',
                           'recipient', 'status', 'call_type')

        if obj and (obj.call_type == OUTGOING or obj.order):
            readonly_fields += ('order',)

        return readonly_fields

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('order', 'created_by', 'modified_by')
        return qs

    class Media:

        js = ('/static/website/js/lib/jquery.min.js',
              '/static/website/js/lib/bootstrap.min.js',
              '/static/admin/js/exotel_admin.js',
              )


admin.site.register(CallDetail, CallDetailAdmin)
