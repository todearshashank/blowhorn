# Django and System libraries
import json
import logging
import re
import requests
import datetime

from django.conf import settings
from django.core.exceptions import ValidationError

# 3rd party libraries
from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import BasicAuthentication

# blowhorn imports
from blowhorn.exotel.calls import ExotelCall
from blowhorn.utils.functions import ist_to_utc
from blowhorn.common.middleware import CsrfExemptSessionAuthentication
from blowhorn.users.models import User
from blowhorn.driver.models import Driver
from blowhorn.order.models import Order, CallMaskingLog
from blowhorn.exotel.constants import *
from blowhorn.exotel.models import CallDetail


class ExotelCallAPIView(views.APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    # permission_classes = (AllowAny,)

    def post(self, request):
        input_data = request.data
        response = {"show_whis_btn": False, "message": ""}
        is_whitelisted = True
        logging.info(
            'This is the input data for calling via '
            'exotel : %s' % (json.dumps(input_data), ))

        obj_id = input_data["obj_id"] if input_data.get("obj_id") else None
        phone_number = input_data.get("phone_number", None)

        if phone_number and not input_data.get("is_driver"):
            user = User.objects.filter(phone_number=phone_number)
            if not user:
                response[
                    "message"] = "No user exist with particular phone number"
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=response)
            elif not user.first().is_mobile_whitelisted:
                is_whitelisted = False
        else:
            driver = Driver.objects.filter(id=obj_id)
            if not driver:
                response[
                    "message"] = "No driver with found"
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=response)

            else:
                driver = driver.first()
                if not driver.user.is_mobile_whitelisted:
                    phone_number = driver.user.phone_number
                    is_whitelisted = False

        if not is_whitelisted:
            try:
                number = re.compile(r'(\d{10})$').search(
                    str(phone_number)).group(1)
                number_metadata = ExotelCall().number_metadata(number)
                if number_metadata.get("Numbers", {}).get("DND", "") == "Yes":
                    response["show_whis_btn"] = True
                    response["message"] = "This number is on DND. " \
                        "Please White list the number First"
                    return Response(status=status.HTTP_400_BAD_REQUEST,
                                    data=response)
                User.objects.filter(phone_number=phone_number).update(
                    is_mobile_whitelisted=True)
            except ValidationError as e:
                response["message"] = e
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=response)

        try:
            response = ExotelCall().call_number(input_data=input_data,
                                                request=request)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data=e.args[0])

        logging.info('Below is the response %s' % (response.content, ))

        if response and response.status_code == 200:

            response_content = response.json()
            call_response = response_content.get("Call", {})
            try:
                created_timestamp = ist_to_utc(datetime.datetime.strptime(
                    call_response.get("StartTime"), "%Y-%m-%d %H:%M:%S"))
            except:
                created_timestamp = datetime.datetime.now()

            try:
                order = Order.objects.get(
                    number=input_data.get("order_number"))
            except:
                order = None
            recipient = DRIVER if input_data.get(
                "is_driver", False) else CUSTOMER

            call_data = {
                "sid": call_response.get("Sid"),
                "created_timestamp": created_timestamp,
                "client_number": re.compile(r'(\d{10})$').search(
                    call_response.get("To")).group(1),
                "agent_number": re.compile(r'(\d{10})$').search(
                    call_response.get("From")).group(1),
                "order": order,
                "recipient": recipient,
                "call_type": OUTGOING
            }
            CallDetail.objects.create(**call_data)
            return Response(status=status.HTTP_200_OK, data="OK")

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=response.json())


class ExotelWhitelistNumberAPIView(views.APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,
                              BasicAuthentication)

    def post(self, request):
        input_data = request.data
        logging.info(
            'This is the input data for whitelisting number via '
            'exotel : %s' % (json.dumps(input_data), ))

        obj_id = input_data.get("obj_id")
        phone_number = input_data.get("phone_number", "")
        response = {"message": ""}

        response = {}

        if not phone_number:
            driver = Driver.objects.filter(id=obj_id)
            if not driver:
                response[
                    'message'] = "No driver with particular phone number exist"
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data=response)
            else:
                driver = driver.first()
                phone_number = driver.user.phone_number

        try:
            resp = ExotelCall().number_whitelisting(phone_number)
        except ValidationError as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=e)

        if resp.get('status', False):
            return Response(status=status.HTTP_200_OK, data=resp['message'])

        return Response(status=status.HTTP_400_BAD_REQUEST,
                        data=resp.get('message', 'Something went wrong'))


class ExotelConnectStatusCallback(views.APIView):

    permission_classes = (AllowAny, )

    def post(self, request):
        input_data = request.data
        logging.info(
            'This is the input data for status callback via '
            'exotel : %s' % (json.dumps(input_data), ))

        if not input_data.get("CallSid", None):
            logging.error("No call sid found in the request")
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="No sid found")

        call_detail = CallDetail.objects.filter(sid=input_data.get("CallSid"))

        if not call_detail:
            logging.error("No call detail found for the sid")
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="No object corresponding to the sid found")

        call_detail.update(status=input_data.get("Status"))

        return Response(status=status.HTTP_200_OK)


class ExotelCustomerStatusCallback(views.APIView):

    permission_classes = (AllowAny, )

    def get(self, request):
        input_data = request.GET
        logging.info(
            'This is the input data for status callback via exotel for '
            'incoming calls : %s' % (json.dumps(input_data), ))

        if not input_data.get("CallSid", None):
            logging.error("No call sid found in the request")
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data="No sid found")

        try:
            created_timestamp = ist_to_utc(datetime.datetime.strptime(
                input_data.get("StartTime"), "%Y-%m-%d %H:%M:%S"))
        except:
            created_timestamp = datetime.datetime.now()

        call_detail_data = {
            "sid": input_data.get("CallSid"),
            "created_timestamp": created_timestamp,
            "client_number": re.compile(r'(\d{10})$').search(
                input_data.get("CallFrom")).group(1),
            "agent_number": re.compile(r'(\d{10})$').search(
                input_data.get("DialWhomNumber")).group(1),
            "status": input_data.get("DialCallStatus"),
            "recipient": AGENT,
            "call_type": INCOMING
        }

        CallDetail.objects.create(**call_detail_data)
        return Response(status=status.HTTP_200_OK, data="OK")

        return Response(status=status.HTTP_200_OK)


class ExotelStatusUpdate(views.APIView):

    permission_classes = (AllowAny, )

    def post(self, request):
        data = request.data
        sid = data.get('CallSid')
        url = settings.EXOTEL_CALL_DETAILS % (settings.EXOTEL_API_KEY, settings.EXOTEL_API_TOKEN, settings.EXOTEL_API_SID, sid)
        call_detail = CallMaskingLog.objects.filter(sid=sid).first()
        if call_detail:
            res = requests.get(url)
            if res.status_code == status.HTTP_200_OK:
                response_data =  json.loads(res.content)
                call_info =  response_data['Call']
                call_detail.duration = call_info.get('Duration', 0) or 0
                call_detail.price = call_info.get('Price', 0) or 0
                call_detail.response=response_data
                call_detail.save()
                return Response(status=status.HTTP_200_OK)
            return Response(status=status.HTTP_400_BAD_REQUEST, data='request failed')
        return Response(status=status.HTTP_400_BAD_REQUEST)
