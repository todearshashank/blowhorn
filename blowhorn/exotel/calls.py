import json
import re
import phonenumbers
from django.conf import settings
from django.core.exceptions import ValidationError
from blowhorn.exotel.mixins import Exotel
from blowhorn.exotel.constants import WHITELISTING_STATUS, CALLER_ID
from blowhorn.users.models import User
from blowhorn.exotel.constants import URL_ENDPOINTS


class ExotelCall(object):

    def call_flow(self, input_data, request):
        """
        call_flow -creates a call to from_number and flow_id with the exophone(caller_id)
        """

        data = Exotel().get_callflow_data(input_data=input_data,
                                          request=request)

        url_endpoint = URL_ENDPOINTS["CALL_CONNECT"]

        return Exotel().generate_request("POST", url_endpoint, data)

    def call_number(self, input_data, request):
        """
        call_number -creates a call to from_number and then to with the exophone(caller_id)
        """
        data = Exotel().get_input_data(input_data, request)

        url_endpoint = URL_ENDPOINTS["CALL_CONNECT"]

        return Exotel().generate_request("POST", url_endpoint, data)

    def call_details(self, call_sid):
        """
         call_details - returns call details object as a response object
        """
        data = {}
        url_endpoint = URL_ENDPOINTS["CALL_DETAIL"].format(call_sid)

        return Exotel().generate_request("GET", url_endpoint, data)

    def number_metadata(self, number):
        data = {}
        number_metadata_url_endpoint = URL_ENDPOINTS[
            "NUMBER_METADATA"].format(number)
        number_metadata_response = Exotel().generate_request(
            "GET", number_metadata_url_endpoint, data)
        number_metadata = json.loads(number_metadata_response.content)

        if number_metadata_response.status_code == 200:
            return number_metadata
        else:
            raise ValidationError(number_metadata.get(
                'Message', "Something went wrong while connection"))

    def get_number_whitelisted_status(self, number):
        data = {}
        whitelist_url_endpoint = URL_ENDPOINTS[
            "WHITELISTING_STATUS"].format(number)
        whitelist_status_response = Exotel().generate_request(
            "GET", whitelist_url_endpoint, data)
        whitelist_status = json.loads(whitelist_status_response.content)
        if whitelist_status_response.status_code == 200:
            return whitelist_status
        else:
            raise ValidationError(whitelist_status.get(
                'Message', "Something went wrong while connection"))
        return whitelist_status

    def number_whitelisting(self, phone):
        number = re.compile(
            r'(\d{10})$').search(str(phone)).group(1)
        number_metadata = self.number_metadata(number)
        response = {"status": True, "message": ""}

        if number_metadata.get("Numbers", {}).get("DND", "") == "Yes":
            whitelist_status = self.get_number_whitelisted_status(number)
            if whitelist_status.get("Result", {}).get("Status", "") == WHITELISTING_STATUS["NEUTRAL"]:
                data = {
                    'VirtualNumber': CALLER_ID,
                    'Number': number,
                    'Language': 'en'
                }
                whitelist_adding_endpoint = URL_ENDPOINTS[
                    "ADDING_TO_WHITELIST"]
                whitelist_adding_response = Exotel().generate_request(
                    "POST", whitelist_adding_endpoint, data)

                if whitelist_adding_response.status_code == 200:
                    whitelist_adding = json.loads(
                        whitelist_adding_response.content)
                    if not whitelist_adding.get("Result", {}).get("Failed", ""):
                        User.objects.filter(phone_number=phone).update(
                            is_mobile_whitelisted=True)
                    else:
                        response["status"] = False
                        response["message"] = whitelist_adding.get(
                            "Result", {}).get("Message")
                else:
                    response["status"] = False
                    response[
                        "message"] = "Something went wrong while connecting to call provider"

            elif whitelist_status.get("Result", {}).get("status", "") == WHITELISTING_STATUS["BLACKLIST"]:
                response["status"] = False
                response[
                    "message"] = "Number has explicitly unsubscribed from getting our calls."
            else:
                User.objects.filter(phone_number=phone).update(
                    is_mobile_whitelisted=True)
                response["message"] = "Number Whitelisted"
        else:
            User.objects.filter(phone_number=phone).update(
                is_mobile_whitelisted=True)
            response["message"] = "Number Whitelisted"
        return response
