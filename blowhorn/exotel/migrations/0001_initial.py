# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-07-04 20:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('order', '0056_order_invoice_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='CallDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sid', models.CharField(blank=True, max_length=50, null=True, verbose_name='Sequence ID')),
                ('created_timestamp', models.DateTimeField(blank=True, null=True, verbose_name='Call initiated at')),
                ('to_number', models.CharField(blank=True, max_length=10, null=True, verbose_name="Recipient's Phone Number")),
                ('from_number', models.CharField(blank=True, max_length=10, null=True, verbose_name="Agent's Phone Number")),
                ('recipient', models.CharField(choices=[('driver', 'Driver'), ('customer', 'Customer')], default='driver', max_length=15, verbose_name='Recipient')),
                ('status', models.CharField(blank=True, choices=[('queued', 'Queued'), ('in-progress', 'In Progress'), ('completed', 'Completed'), ('failed', 'Failed'), ('busy', 'Busy'), ('no-answer', 'No Answer')], default='queued', max_length=30, null=True, verbose_name='Call Status')),
                ('order', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='order.Order')),
            ],
            options={
                'verbose_name': 'Call Detail',
                'verbose_name_plural': 'Call Details',
            },
        ),
    ]
