from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from blowhorn.order.models import Order


class ExotelCallDetailForm(forms.ModelForm):

    existing_customer = forms.BooleanField(
        label=_("Existing Customer called via different Number?"),
        initial=False,
        required=False)

    order_number = forms.CharField(
        label="Existing order number", max_length=20, required=False)

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            call_detail = kwargs['instance']
        super(ExotelCallDetailForm, self).__init__(*args, **kwargs)

        if self.fields.get('order', None):
            client_number = self.fields.get('client_number') \
                if self.fields.get(
                'client_number') else call_detail.client_number

            self.fields['order'].autocomplete = False
            self.fields['order'].initial = None
            self.fields['order'].queryset = Order.objects.filter(
                user__phone_number=('%s%s' % (
                    settings.ACTIVE_COUNTRY_CODE,
                    client_number))) if client_number else []

    def clean(self, *args, **kwargs):
        super(forms.ModelForm, self).clean()
        is_existing_customer = self.cleaned_data.get(
            'existing_customer', False)

        if self.cleaned_data.get('order') and is_existing_customer:
            raise ValidationError("Unselect the below option if order "
                                  "number is already present")

        if is_existing_customer:
            order_number = self.cleaned_data.get('order_number')
            if not order_number:
                raise ValidationError("Order Number is needed")

            order_detail = Order.objects.filter(
                number=order_number)

            if not order_detail:
                raise ValidationError("Please enter the valid order number")

            self.cleaned_data['order'] = order_detail[0]

        return self.cleaned_data
