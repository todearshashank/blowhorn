from django.db import models
from django.utils.translation import ugettext_lazy as _

from blowhorn.common.models import BaseModel
from blowhorn.exotel.constants import DRIVER, RECIPIENT_TYPE, \
    QUEUED, CALL_STATUS, CALL_TYPE_CHOICES, OUTGOING


class CallDetail(BaseModel):
    sid = models.CharField(
        _("Sequence ID"), null=True, blank=True, max_length=50)

    created_timestamp = models.DateTimeField(_('Call initiated at'),
                                             null=True, blank=True)

    client_number = models.CharField(
        _("Client's Phone Number"), null=True, blank=True, max_length=10)

    agent_number = models.CharField(
        _("Agent's Phone Number"), null=True, blank=True, max_length=10)

    order = models.ForeignKey('order.Order', null=True, blank=True,
                              on_delete=models.PROTECT)

    recipient = models.CharField(('Recipient'), max_length=15,
                                 choices=RECIPIENT_TYPE, default=DRIVER)

    status = models.CharField(_("Call Status"), max_length=30,
                              choices=CALL_STATUS,
                              default=QUEUED, null=True, blank=True)

    call_type = models.CharField(_("Call Type"), max_length=30,
                                 choices=CALL_TYPE_CHOICES,
                                 default=OUTGOING)

    def __str__(self):
        return self.sid

    class Meta:
        verbose_name = _("Call Detail")
        verbose_name_plural = _("Call Details")
