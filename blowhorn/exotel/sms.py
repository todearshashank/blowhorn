from blowhorn.exotel.mixins import Exotel


class ExotelSMS(object):

    def sms(self, from_number, to, body, encoding_type="plain", priority="normal", status_callback=None):
        """
        sms - sends sms using exotel api
        """

        data = {
            'From': from_number,
            'To': to,
            'Body': body,
            'EncodingType': encoding_type,
            'Priority': priority,
            'StatusCallback': status_callback
        }

        url_endpoint = 'Sms/send.json'

        return Exotel().generate_request("POST", url_endpoint, data)

    def sms_details(self, sms_sid):
        """
         sms_details - returns sms details object as a response object provided the sms sid
        """
        data = {}
        url_endpoint = 'Sms/Messages/{}.json'.format(sms_sid)

        return Exotel().generate_request("GET", url_endpoint, data)
