WHITELISTING_STATUS = {
    "WHITELIST": "Whitelist",
    "BLACKLIST": "Blacklist",
    "NEUTRAL": "Neutral"
}

CALLER_ID = "09513886363"

CALLER_ID_CITY_MAPPING = {
    "Bengaluru": "08045683500",
    "Delhi": "01140849988",
    "Mumbai": "02248931300",
    "Hyderabad": "04045208000",
    "Chennai": "04448134000"
}

DRIVER = 'driver'
CUSTOMER = 'customer'
AGENT = 'agent'

RECIPIENT_TYPE = (
    (DRIVER, 'Driver'),
    (CUSTOMER, 'Customer'),
    (AGENT, 'Agent')
)

OUTGOING = 'outgoing'
INCOMING = 'incoming'

CALL_TYPE_CHOICES = (
    (OUTGOING, 'Outgoing'),
    (INCOMING, 'Incoming')
)

QUEUED = "queued"
IN_PROGRESS = "in-progress"
COMPLETED = "completed"
FAILED = "failed"
BUSY = "busy"
NO_ANSWER = "no-answer"

CALL_STATUS = (
    (QUEUED, 'Queued'),
    (IN_PROGRESS, 'In Progress'),
    (COMPLETED, 'Completed'),
    (FAILED, 'Failed'),
    (BUSY, 'Busy'),
    (NO_ANSWER, 'No Answer')
)

STATUS_CALLBACK_URL = "/api/exotel/status-callback"
CALL_TIMELIMIT = 14400
CALL_TIMEOUT = 30
FLOW_ID = 17319


CALL_TYPE = {
    "TRANSACTIONAL": "trans"
}

URL_ENDPOINTS = {
    "CALL_CONNECT": "Calls/connect.json",
    "CALL_DETAIL": "Calls/{}.json",
    "NUMBER_METADATA": "Numbers/{}.json",
    "WHITELISTING_STATUS": "CustomerWhitelist/{}.json",
    "ADDING_TO_WHITELIST": "CustomerWhitelist.json"
}
