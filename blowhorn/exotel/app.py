# from django.conf.urls import url
# from django.apps import AppConfig
#
# from blowhorn.exotel.views import *
#
#
# class ExotelApplication(AppConfig):
#
#     def get_urls(self):
#         urlpatterns = [
#             url(r'^exotel/call$', ExotelCallAPIView.as_view(), name='exotel-call'),
#             url(r'^exotel/whitelist$', ExotelWhitelistNumberAPIView.as_view(),
#                 name='exotel-whitelist'),
#             url(r'^exotel/status-callback$', ExotelConnectStatusCallback.as_view(),
#                 name='exotel-status-callback'),
#             url(r'^exotel/incoming-call-status$', ExotelCustomerStatusCallback.as_view(),
#                 name='exotel-incoming-call-status'),
#             url(r'^exotel/call/update$', ExotelStatusUpdate.as_view(),
#                 name='exotel-call-status-update')
#         ]
#         return self.post_process_urls(urlpatterns)
