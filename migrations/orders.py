import json
import traceback
import re
import pprint
from collections import defaultdict

from django.db import transaction
from django.db.models import Q
from django.contrib.gis.geos import LineString, MultiLineString
#from django.db.utils import IntegrityError
#from blowhorn.users.models import 
from blowhorn.address.models import City, Country
from blowhorn.customer.models import Customer
from blowhorn.driver.models import Driver
from blowhorn.vehicle.models import VehicleClass, Vehicle, VehicleModel, BodyType
from blowhorn.common.helper import CommonHelper
from blowhorn.contract.models import Contract
from blowhorn.contract.constants import CONTRACT_TYPE_C2C

from blowhorn.order.models import Order, ShippingAddress, WayPoint, Labour
from blowhorn.trip.models import Trip, Stop
from blowhorn.order.const import ORDER_TYPE_NOW, ORDER_TYPE_LATER, PAYMENT_MODE_RAZORPAY, PAYMENT_MODE_CASH, PAYMENT_STATUS_UNPAID, PAYMENT_STATUS_PAID
from config.settings import status_pipelines as StatusPipeline

from config.settings.status_pipelines import (
    ORDER_NEW,
    OUT_FOR_DELIVERY,
#    ORDER_RETURNED,
    ORDER_DELIVERED,
#    UNABLE_TO_DELIVER
)

from migrations import common

pp = pprint.PrettyPrinter(indent=1)

MIGRATE_ONLY_COMPLETED = False

pp.pprint('-- Vehicle classes --')
vehicle_classes = VehicleClass.objects.all()
vehicle_class_dict = dict([(vm.commercial_classification, vm) for vm in vehicle_classes])
default_vehicle_class = vehicle_class_dict['Mini-Truck']
pp.pprint(vehicle_class_dict)
print('default :', default_vehicle_class)

print()
pp.pprint('-- Vehicle Models --')
default_vehicle_model = VehicleModel.objects.get(model_name='Tata Ace')
default_body_type = BodyType.objects.get(body_type='Container')
print('default :', default_vehicle_model)

print()
pp.pprint('-- Vehicle Class Mapping --')
vehicle_class_mapping = {
    'Mini-Truck' : 'Mini-Truck',
    'Pickup' : 'Pickup-Truck',
    'Pickup-Truck' : 'Pickup-Truck',
    'Light-Truck' : 'Light-Truck',
    'Truck' : 'Light-Truck',
    'Cargo-Van' : 'Light-Truck',
}
pp.pprint(vehicle_class_mapping)

print()
pp.pprint('-- Cities --')
cities = City.objects.all()
cities_dict = dict([(str(c), c) for c in cities])
default_country = Country.objects.first()
pp.pprint(cities_dict)

print()
pp.pprint('-- Contracts --')
contracts = Contract.objects.filter(
    contract_type=CONTRACT_TYPE_C2C, customer__isnull=True
).prefetch_related('vehicle_classes')
contracts_dict=defaultdict(dict)
for c in contracts:
    for vc in c.vehicle_classes.all():
        contracts_dict[str(c.city)][vc.commercial_classification] = c

pp.pprint(contracts_dict)

prior_orders = list(Order.objects.filter(order_type='booking') \
        .values_list('number', flat=True))

print()
print('Prior Orders: %d' % len(prior_orders))

print('Loading customer data')
cleaned_customers = json.load(open('migrations/cleaned_customers.json'))
map_all_ids_to_primary_id = {}
for c in cleaned_customers:
    map_all_ids_to_primary_id[c['id']] = c['id']
    for oid in c['other_ids']:
        map_all_ids_to_primary_id[oid] = c['id']

def clean_rc(s):
    s = s.upper()
    return re.sub(r'[^0-9A-Z]', '', s)

def get_contract(city, vc):
    mapped_vc = vehicle_class_mapping[vc.commercial_classification]
    return contracts_dict[str(city)][mapped_vc]

def get_vehicle_class(vm):
    if vm == 'Pickup':
        vm = 'Pickup-Truck'
    vmo = vehicle_class_dict.get(vm)
    return vmo

def get_city(c):
    map_ = {
        'Delhi' : 'Delhi NCR',
    }
    mapped = map_.get(c, c)
    city = cities_dict[mapped]
    if city is None:
        assert False, 'Unable to find city: %s' % c

    return city

def get_new_status(status, status_code):
    if status == 'completed':
        return ORDER_DELIVERED
    elif status == 'booking_accepted':
        return ORDER_NEW
    elif status_code > 0:
        return OUT_FOR_DELIVERY
    else:
        return None

def get_event_time(data, event):
    all_events = data.get('events', {})
    for e, t in reversed(list(zip(all_events.get('event', []), all_events.get('time', [])))):
        if e == event:
            return common.get_timestamp(t)

    t = data.get('time_%s' % event)
    if t:
        return common.get_timestamp(t)
    return t

def delete_orders():
    Stop.objects.filter(waypoint__order__order_type='booking').delete()
    WayPoint.objects.filter(order__order_type='booking').delete()
    Trip.objects.filter(order__order_type='booking').delete()
    Order.objects.filter(order_type='booking').delete()

def get_payment_mode_and_status(payment_mode, cash_card, s_user, s_remote):
    combined_mode = cash_card or payment_mode
    new_mode = PAYMENT_MODE_RAZORPAY if combined_mode == 'card' else  PAYMENT_MODE_CASH
    combined_status = s_remote or s_user
    new_status = PAYMENT_STATUS_PAID if combined_status == 'paid' else PAYMENT_STATUS_UNPAID
    return new_mode, new_status

def create_order(data):
    current_status = data.get('current_status')
    current_status_code = int(data.get('current_status_code'))
    status = get_new_status(current_status, current_status_code)
    id_ = data.get('id')
    friendly_id = data.get('friendly_id')

    # Check flag and Ignore non-completed orders
    if MIGRATE_ONLY_COMPLETED and current_status != 'completed':
        return

    payment_mode, payment_status = get_payment_mode_and_status(
        data.get('payment_mode'),
        data.get('cash_card'),
        data.get('payment_status_user'),
        data.get('payment_status_remote'),
    )

#        data.get('payment_vendor'), ',',
#        data.get('payment_txn_id'), ','
#    )
#    return

    driver = data.get('driver')
    vehicle = clean_rc(data.get('vehicle') or '')

    order_number = friendly_id or id_

    if order_number in prior_orders:
        return

    customer_id = map_all_ids_to_primary_id[data.get('customer')]
#    print('Orig :', customer_id, 'Mapped :', customer_id)

#    print('Creating order', order_number, customer_id, driver, vehicle)

    if common.mobile_regex.match(customer_id):
        customer = Customer.objects.filter(user__phone_number__icontains=customer_id).first() \
            or Customer.objects.get(user__email__icontains=customer_id)
    else:
        customer = Customer.objects.get(user__email__icontains=customer_id)

    if customer is None:
        raise Exception('Cannot find customer : %s ' % customer_id)

    booking_time = common.get_timestamp(data.get('booking_time'))
#    last_modified = common.get_timestamp(data.get('last_modified'))
    scheduled_time = common.get_timestamp(data.get('requested_pickup_time'))

    device_type = data.get('device_type')
    pickup_address = data.get('pickup_address')
    pickup_geopt = data.get('pickup_geopt')

    pickup = ShippingAddress(
        line1=pickup_address.get('line')[:255],
        line3=pickup_address.get('area'),
        state=pickup_address.get('state'),
        postcode=pickup_address.get('postal_code'),
        country=default_country,
        geopoint = CommonHelper.get_geopoint_from_latlong(
            pickup_geopt.get('lat'),
            pickup_geopt.get('long'),
        ),
    )
    pickup.save()


    dropoff_address = data.get('dropoff_address')
    dropoff_geopt = data.get('dropoff_geopt')

    dropoff = ShippingAddress(
        line1=dropoff_address.get('line')[:255],
        line3=dropoff_address.get('area'),
        state=dropoff_address.get('state'),
        postcode=dropoff_address.get('postal_code'),
        country=default_country,
        geopoint = CommonHelper.get_geopoint_from_latlong(
            dropoff_geopt.get('lat'),
            dropoff_geopt.get('long'),
        ),
    )
    dropoff.save()

    vehicle_class = get_vehicle_class(data.get('vehicle_class'))
    city = get_city(data.get('region'))

    items = data.get('items', [])
    expanded_items = []
    for item in items:
        expanded_items += item.split(',')

    # Restrict each item to 128 char limit
    new_items = [x[:128] for x in expanded_items]

    driver_obj = None
    if driver:
        try:
            driver_obj = Driver.objects.get(user__email__icontains=driver)
        except:
            try:
                driver_obj = Driver.objects.get(user__email__icontains=driver.lstrip('0'))
            except:
                driver_obj = Driver.objects.get(user__phone_number__icontains=driver)

    estimated_cost = data.get('estimated_cost', 0)
#    actual_cost = data.get('actual_cost', 0)
    fare_breakdown = data.get('fare_breakdown', {})

    booking_type = data.get('booking_type')
    if booking_type == 'current':
        booking_type=ORDER_TYPE_NOW
    else:
        booking_type=ORDER_TYPE_LATER

    actual_cost_pd = data.get('actual_cost_post_discount', 0)
    labour = data.get('labour', {})
    lcount = int(labour.get('count', 0))
    lcph = labour.get('cph', 0)
    actual_cost_pd += lcount * lcph

#    contract = None
#    if current_status != 'completed':
#        # New or ongoing trip. Update the contract
    contract = get_contract(city, vehicle_class)

    order = Order(
        customer=customer,
        number=order_number,
        date_placed=booking_time,
        booking_type=booking_type,
        status=status,
#        updated_time=last_modified,
        device_type=device_type,
        pickup_address=pickup,
        pickup_datetime=scheduled_time,
        shipping_address=dropoff,
        vehicle_class_preference=vehicle_class,
        items=new_items,
        city=city,
        customer_contract=contract,
        driver=driver_obj,
        estimated_cost=estimated_cost,
        actual_cost=actual_cost_pd,
        total_excl_tax=actual_cost_pd,
        total_incl_tax=actual_cost_pd,
        revised_cost=actual_cost_pd,
        cost_components=fare_breakdown,
        payment_mode=payment_mode,
        payment_status=payment_status,
    )
    order.save()

    if lcount != 0 and lcph != 0:
        Labour(
            labour_cost=lcph,
            no_of_labours=lcount,
            order=order,
        ).save()


    waypoints = data.get('waypoints', {})
    wlocation = waypoints.get('location', [])
    waddress = waypoints.get('address', {})
    wcity = waddress.get('city', [])
    warea = waddress.get('area', [])
    wcountry = waddress.get('country', [])
    wstate = waddress.get('state', [])
    wpostal_code = waddress.get('postal_code', [])
    wline = waddress.get('line', [])

    waypoint_objects = []
    for i, waypoint in enumerate(zip(wlocation, wcity, warea, wcountry, wstate,
        wpostal_code, wline)):
#        print(i, waypoint)
        location = waypoint[0]
        city = waypoint[1]
        area = waypoint[2]
#        country = waypoint[3]
        state = waypoint[4]
        postal_code = waypoint[5]
        line = waypoint[6]
        if area not in line:
            line = '%s, %s' % (line, area)

        waypoint_address = ShippingAddress(
            line1=line[:255],
            line3=area,
            state=state,
            postcode=postal_code,
            country=default_country,
            geopoint = CommonHelper.get_geopoint_from_latlong(
                location.get('lat'),
                location.get('long'),
            ),
        )
        waypoint_address.save()

        waypoint_object = WayPoint(
            order=order,
            sequence_id=i,
            shipping_address=waypoint_address
        )
        waypoint_object.save()
        waypoint_objects.append(waypoint_object)

    drop_waypoint = WayPoint(
        order=order,
        sequence_id=len(waypoints),
        shipping_address=dropoff
    )
    drop_waypoint.save()

    waypoint_objects.append(drop_waypoint)

#    if current_status_code > 0 or current_status == 'completed':
    if current_status == 'completed':
        # Create trip now
        if not driver_obj:
            raise Exception('Driver not found for In Progress or completed trip')
        if not vehicle:
            vehicle = driver_obj.driver_vehicle

        if not vehicle:
            raise Exception('Vehicle not found for In Progress or completed trip')

        try:
            vehicle_obj = Vehicle.objects.get(registration_certificate_number=vehicle)
        except:
            vehicle_obj, created = Vehicle.objects.get_or_create(
                registration_certificate_number=vehicle,
                vehicle_model=default_vehicle_model,
                body_type=default_body_type,
            )

        estimated_distance_km = data.get('estimated_distnce_km')
        actual_distance_km = data.get('actual_distance_km')
        time_driver_accepted = get_event_time(data, 'driver_accepted')
        time_arrived_at_pickup = get_event_time(data, 'arrived_at_pickup')
#        time_finished_loading = get_event_time(data, 'finished_loading')
#        time_arrived_at_dropoff = get_event_time(data, 'arrived_at_dropoff')
        time_completed = get_event_time(data, 'completed')

        total_time = None
        try:
            total_time = time_completed - time_arrived_at_pickup
        except:
            pass

        trip_status = StatusPipeline.TRIP_IN_PROGRESS
        route_trace = None
        route_info = data.get('route_info')
        if current_status_code == 0:
            trip_status = StatusPipeline.TRIP_COMPLETED
            if len(route_info) > 1:
                line_string = LineString([CommonHelper.get_geopoint_from_latlong(
                        x.get('lat'), x.get('long')) for x in route_info])
                route_trace = MultiLineString([line_string])

        trip = Trip(
            order=order,
            driver=driver_obj,
            vehicle=vehicle_obj,
            planned_start_time=scheduled_time,
            trip_acceptence_time=time_driver_accepted,
            actual_start_time=time_arrived_at_pickup,
            actual_end_time=time_completed,
#            estimated_drop_time=estimated_drop_time,

            estimated_distance_km=estimated_distance_km,
            total_distance=actual_distance_km,
            gps_distance=actual_distance_km,
            total_time=total_time,
            route_trace=route_trace,
            status=trip_status,
        )
        trip.save()

        stops = []
        for i, wp in enumerate(waypoint_objects):
            linestring = None
            distance = 0
            if i == len(waypoint_objects) - 1:
                event = 'completed'
                start_time = get_event_time(data, 'arrived_at_dropoff')
                end_time = get_event_time(data, 'completed')
                distance = actual_distance_km
                if len(route_info) > 1:
                    line_string = LineString([CommonHelper.get_geopoint_from_latlong(
                            x.get('lat'), x.get('long')) for x in route_info])
 
            else:
                event = 'Waypoint-%d' % (i + 1)
                start_time = get_event_time(data, event)
                end_time = start_time

            stops.append(Stop(
                trip=trip,
                waypoint=wp,
                route_trace=linestring,
                distance=distance,
                status=StatusPipeline.TRIP_COMPLETED,
                start_time=start_time,
                end_time=end_time,
            ))

        Stop.objects.bulk_create(stops)

    prior_orders.append(order_number)
    return order


print('\nLoading Order data')
orders = json.load(open('migrations/cleaned_orders.json'))

def migrate(start=None, end=None):
    common.migrate(orders, create_order, start, end)
