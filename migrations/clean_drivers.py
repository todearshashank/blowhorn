import json


# Clean up the driver data so that we are only migrating the 
# minimum set of drivers
# Conditions to satisfy
# - Used in past complted C2C order
# - or is in active state

print('Cleaning Drivers...')
f = open('../drivers.json')
drivers = json.loads(f.read())

orders = json.loads(open('../orders.json').read())
drivers_from_past_orders = set()
for order in orders:
    if order.get('current_status') == 'completed':
        driver = order.get('driver_id')
        if driver:
            driver_id = driver.get('name')
#            print(driver_id)
            drivers_from_past_orders.add(driver_id)

cleaned_drivers = []

for d in drivers:
    did = d.get('__key__').get('name')
    used_in_c2c =  did in drivers_from_past_orders
    if d.get('status') != 'active' and not used_in_c2c:
        continue

    d['used_in_c2c'] = used_in_c2c
    cleaned_drivers.append(d)


print('Original count : %d, Cleaned count : %d\n' % (len(drivers), len(cleaned_drivers)))

nf = open('cleaned_drivers.json', 'w')
json.dump(cleaned_drivers, nf, indent=2)
nf.close()
