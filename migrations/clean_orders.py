import json

# Migrate all customers. At the same time
# merge linked customers into one account.

print('Cleaning Orders...')

drivers = json.load(open('cleaned_drivers.json'))
drivers_tuple = [(x['__key__']['name'], x) for x in drivers]
drivers_dict = dict(drivers_tuple)

orders = json.load(open('../orders.json'))

cleaned_orders = []
START_TIME = '2018-02-12 00:00:00'

customers = set()

for o in orders:
    key = o['__key__']
    id_ = key['id']
    current_status = o.get('current_status')
    status_code = int(o.get('current_status_code'))
    friendly_id = o.get('friendly_id')
    if o.get('requested_pickup_time') is None:
        continue

    rpt = o.get('requested_pickup_time')
#    if status_code >= 0: # or booking_time > START_TIME:

    driver_id = o.get('driver_id', {}).get('name')

#    print(id_, current_status, driver_id, rpt)

    # Only completed orders with driver data and new orders
    # for scheduled date after START_TIME
    if (current_status == 'completed' and driver_id) or \
        (status_code == 0 and rpt >= START_TIME):
        o['id'] = id_

        customer_id = o['customer_id']['name']
        vehicle_rc = o.get('vehicle_key', {}).get('name') \
            or (drivers_dict.get(driver_id, {}).get('vehicle_key', {}).get('name'))

        o['driver'] = driver_id
        o['vehicle'] = vehicle_rc
        o['customer'] = customer_id

        customers.add(customer_id)

        keys_to_del = ['history', '__error__', 'error_info', 'missed_drivers', 'rejected_drivers',
            '__key__', 'driver_id', 'customer_id', 'vehicle_key', 'cost_history',
            'manual_discount_reason', 'task_status_history',
        ]

        for k in keys_to_del:
            if k in o:
                del o[k]

        cleaned_orders.append(o)


print('Original count : %d, Cleaned count : %d\n' % (len(orders), len(cleaned_orders)))

nf = open('cleaned_orders.json', 'w')
json.dump(cleaned_orders, nf, indent=1)
nf.close()

nf = open('customers_used_in_orders.json', 'w')
json.dump(list(customers), nf, indent=1)
nf.close()
