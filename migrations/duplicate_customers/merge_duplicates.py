import csv
from itertools import groupby
from blowhorn.customer.models import *
from blowhorn.address.models import *
from blowhorn.order.models import *
from blowhorn.contract.models import *

dcs = list(csv.reader(open('migrations/duplicate_customers/duplicate_customers.csv')))

grouped = groupby(dcs[1:], lambda x: x[1].upper())
new_data = []
for key, values in grouped:
    new_data.append((key, list(values)))

for key, values in new_data:
    all_ids = [int(v[0]) for v in values]
    primary_id = all_ids[0]
    print('%s, %s, Primary ID: %s' % (key, all_ids, primary_id))
    other_ids = all_ids[1:]
    routes = Route.objects.filter(customer__in=other_ids).update(customer=primary_id)
    hubs = Hub.objects.filter(customer__in=other_ids).update(customer=primary_id)
    orders = Order.objects.filter(customer__in=other_ids).update(customer=primary_id)
    taxes = CustomerTaxInformation.objects.filter(customer__in=other_ids).update(customer=primary_id)
    invoices = CustomerInvoice.objects.filter(customer__in=other_ids).update(customer=primary_id)
    contracts = Contract.objects.filter(customer__in=other_ids).update(customer=primary_id)
    print('\t---Merged routes(%d), hubs(%d), orders(%d), taxes(%d), contracts(%d) invoices(%d)' % (routes, hubs, orders, taxes, contracts, invoices))
    print('\t---Deleting Ids : %s' % other_ids)
    deleted_customers = Customer.objects.filter(id__in=other_ids).delete()
    print('Deleted : %s' % list(deleted_customers))
    print('')

