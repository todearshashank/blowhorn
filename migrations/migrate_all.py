from . import cities
cities.migrate()

from . import drivers
drivers.migrate()

from . import customers
customers.migrate()

from . import orders
orders.migrate()
