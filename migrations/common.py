import re
import traceback
from datetime import datetime, timedelta #, tzinfo
from dateutil import tz

email_regex = re.compile(r'^\S+@\S+$')
mobile_regex = re.compile(r'^[0-9]+$')
TIMESTAMP_FORMAT1 = '%Y-%m-%d %H:%M:%S.%f %Z'
timestamp_regex = re.compile(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d+ UTC')
TIMESTAMP_FORMAT2 = '%Y-%m-%d %H:%M:%S %Z'

UTC = tz.tzoffset('UTC', 0) #timedelta(seconds=0))
#print('UTC', UTC)

def get_timestamp(time_str):

    if timestamp_regex.match(time_str):
        to = datetime.strptime(time_str, TIMESTAMP_FORMAT1)
    else:
        to = datetime.strptime(time_str, TIMESTAMP_FORMAT2)

    to = to.replace(tzinfo=UTC)
    return to

def migrate(data_list, func, start=None, end=None):
    from django.db import transaction
    print('Migrating .....')
    count = len(data_list)
    print('Initial count : %d' % count)

    failed_migrations = 0
    skipped_migrations = 0
    successful_migrations = 0
    for i, c in enumerate(data_list[start:end]):
        try:
            with transaction.atomic():
                obj = func(c)
                if obj:
                    successful_migrations += 1
                else:
                    skipped_migrations += 1
        except Exception as e:
            print(e)
#            print(traceback.format_exc())
            failed_migrations += 1

        print('\rMigrated %d/%d (%.2f%%) ...Successful=%d ...Skipped=%d ...Failed=%d...' % (
            i+1, count, ((i+1)*100.0)/count, 
            successful_migrations, skipped_migrations, failed_migrations),
            end='', flush=True)

    print('')

