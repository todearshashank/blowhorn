from django.db.models import Q
from blowhorn.driver.models import BankAccount, Driver

ebas = BankAccount.objects.filter(
    Q(account_number='') | Q(ifsc_code='') | Q(account_name='')
)
print('Empty bank accounts : %d' % ebas.count())

drivers_with_ebas = Driver.objects.filter(bank_account__in=ebas)
print('Drivers with Empty bank accounts : %d' % drivers_with_ebas.count())

drivers_with_ebas.update(bank_account=None)
ebas.delete()
