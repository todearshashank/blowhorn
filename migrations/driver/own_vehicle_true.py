from blowhorn.driver.models import Driver

queryset = Driver.objects.filter(status='active', user__is_active=True, own_vehicle=False)
print('Own Vehicle False : %d' % queryset.count())
queryset.update(own_vehicle=True)
