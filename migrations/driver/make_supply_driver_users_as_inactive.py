from blowhorn.users.models import User

queryset = User.objects.filter(driver__status='supply').filter(is_active=True)
print('Drivers with Supply Status and user status active : %d' % queryset.count())
updated_count = queryset.update(is_active=False)
print('Updated. User is_active is now set as False')
