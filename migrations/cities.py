import json
from blowhorn.address.models import City
from django.contrib.gis.geos import GEOSGeometry


def migrate():
    cities = json.load(open('migrations/cities.json'))
    for city in cities:
        name=city['name']
        print(name)
        city_obj = City.objects.get(name=city['name'])
        polygon = city['polygon']
        city_obj.coverage = ("POLYGON ((" + ", ".join(['%s %s' % (x['lon'], x['lat']) for x in polygon+[polygon[0]]]) + "))")
#        print(city_obj.coverage)
        city_obj.save()
