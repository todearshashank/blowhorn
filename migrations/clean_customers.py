import json
import re

email_regex = re.compile(r'^\S+@\S+$')
# Migrate all customers. At the same time
# merge linked customers into one account.

print('Cleaning Customers...')
f = open('../customers.json')
customers = json.loads(f.read())

f = open('customers_used_in_orders.json')
customers_used_in_orders = json.loads(f.read())

cleaned_customers = {}
all_mapping = {}

def update_profile(master, new):
    if master['name'].lower() == 'guest':
        master.update({'name': new['name']})
    if not email_regex.match(master['id']) and email_regex.match(new['id']):
        master.update({'id': new['id']})
    if master['mobile'] == master['id'] and master['mobile'] != new['mobile']:
        master.update({'mobile': new['mobile']})

    master['other_ids'] = list(set(master['other_ids'] + new['other_ids']))

for i, c in enumerate(customers):
    linked_account = c.get('linked_account')
    linked_id = linked_account and linked_account.get('name')
    key = c['__key__']
    id_ = key['name']

    if id_ not in customers_used_in_orders:
        continue

    name = c['name']
    mobile = c.get('mobile', '')
    real_is_email = email_regex.match(id_)
    linked_is_email = False

    if linked_id:
        linked_is_email = email_regex.match(linked_id)

    if linked_is_email:
        preferred_id = linked_id
    else:
        preferred_id = id_

    other_ids = set([id_, mobile])
    if linked_id:
        other_ids.add(linked_id)

    profile = {
        'name' : name,
        'id' : id_,
        'mobile_status' : c.get('mobile_status'),
        'email_status' : c.get('status'),
        'category' : c.get('category', {}),
        'creation_time' : c.get('creation_time'),
        'is_real' : bool(real_is_email),
        'mobile' : mobile,
        'other_ids' : list(other_ids),
    }

    updated = False
#    print(i, id_, preferred_id, other_ids)
    for oid in other_ids:
        if oid in all_mapping:
            mapped_id = all_mapping[oid]
#            print(oid, mapped_id)
            cleaned_customer = cleaned_customers.get(mapped_id)
            if cleaned_customer:
                update_profile(cleaned_customer, profile)
                updated = True
        else:
            all_mapping[oid] = preferred_id

    if not updated:
        cleaned_customers[preferred_id] = profile



#    if preferred_id in cleaned_customers:
#        preferred_profile = cleaned_customers[preferred_id]
#        preferred_profile['other_ids'].extend([linked_id, id_])
#        if preferred_profile['mobile'] != mobile and preferred_profile['is_real']:
#            preferred_profile['mobile'] = mobile
#    else:
#        cleaned_customers[preferred_id] = profile


#    if preferred_id not in cleaned_customers or c.get('id_type') != 'mobile':
#        already_present = cleaned_customers.get(preferred_id)
#        if already_present:
#            already_present_mobile = already_present.get('mobile')
#            if already_present_mobile != id_:
#                c['mobile'] =
#
#        cleaned_customers[preferred_id] = c


print('Original count : %d, Cleaned count : %d\n' % (len(customers), len(cleaned_customers)))

nf = open('cleaned_customers.json', 'w')
json.dump(list(cleaned_customers.values()), nf, indent=2)
nf.close()

