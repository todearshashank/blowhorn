import json
import traceback
import re
from dateutil import tz
from datetime import datetime, timedelta

import phonenumbers
from django.db import transaction
from django.db.utils import IntegrityError
from blowhorn.users.models import *
from blowhorn.customer.models import *

from migrations import common

email_regex = re.compile(r'^\S+@\S+$')
mobile_regex = re.compile(r'^[0-9]+$')
DEFAULT_PHONE = '0000000000'

TIMESTAMP_FORMAT1 = '%Y-%m-%d %H:%M:%S.%f %Z'

UTC = tz.tzoffset('UTC', timedelta())
print('UTC', UTC)

customer_ids = [
        re.sub(r'@customer.blowhorn.net$', '', x) for x in \
        list(Customer.objects.values_list('user__email', flat=True).distinct())]


def create_customer(data):
    id_ = data.get('id')

    if id_ in customer_ids:
        return

    mobile = data.get('mobile') or DEFAULT_PHONE
    name = data.get('name')
    mobile_status = data.get('mobile_status') == 'verified'
    email_status = data.get('status') == 'verified'
    creation_time = datetime.strptime(data.get('creation_time'), TIMESTAMP_FORMAT1)
    category = data.get('category', {})
    pricing = category.get('pricing')

    if pricing == 'sme':
        customer_category = CUSTOMER_CATEGORY_NON_ENTERPRISE
    else:
        customer_category = CUSTOMER_CATEGORY_INDIVIDUAL

    if email_regex.match(id_):
        email = id_
    else:
        email = '%s@customer.blowhorn.net' % id_

#    print('Creating Customer', email, mobile, name)
#    user = Customer.objects.upsert_user(
    phone_number = phonenumbers.parse(mobile, settings.DEFAULT_COUNTRY.get('country_code'))
    date_joined = creation_time.replace(tzinfo=UTC)

    user = None
    if id_.endswith('@blowhorn.net') or id_.endswith('@blowhorn.com'):
        try:
            user = User.objects.get(email=email)
        except:
            pass

    if user is None:
        user = User(
            name=name,
            email=email,
            phone_number=phone_number,
            is_mobile_verified=mobile_status,
            is_email_verified=email_status,
            date_joined=date_joined,
            is_active=True,
        )
        user.save()

    customer = Customer(
        user=user,
        name=name,
        customer_category=customer_category,
    )
    customer.save()

    return customer


print('\nLoading Customer data')
customers = json.loads(open('migrations/cleaned_customers.json').read())

def migrate(start=None, end=None):
    common.migrate(customers, create_customer, start, end)
