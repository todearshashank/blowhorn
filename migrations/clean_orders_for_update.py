import json

# Migrate all customers. At the same time
# merge linked customers into one account.

print('Cleaning Orders...')

orders = json.load(open('../orders.json'))

cleaned_orders = []
START_TIME = '2014-05-01 00:00:00'

customers = set()

for o in orders:
    key = o['__key__']
    id_ = key['id']
    current_status = o.get('current_status')
    payment_status = o.get('payment_status_remote')
    status_code = int(o.get('current_status_code'))
    friendly_id = o.get('friendly_id')
    rpt = o.get('requested_pickup_time')

    if rpt is None or rpt < START_TIME or current_status != 'completed' or payment_status == 'paid' :
        continue

#    if status_code >= 0: # or booking_time > START_TIME:
    keys_to_del = ['history', '__error__', 'error_info', 'missed_drivers', 'rejected_drivers',
        '__key__', 'driver_id', 'customer_id', 'vehicle_key', 'cost_history',
        'manual_discount_reason', 'task_status_history',
    ]

    for k in keys_to_del:
        if k in o:
            del o[k]

    o['id'] = id_

    cleaned_orders.append(o)


print('Original count : %d, Cleaned count : %d\n' % (len(orders), len(cleaned_orders)))

nf = open('cleaned_orders_for_update.json', 'w')
json.dump(cleaned_orders, nf, indent=1)
nf.close()

