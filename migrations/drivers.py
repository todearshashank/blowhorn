import json
import re
from config.settings import status_pipelines as StatusPipeline
from blowhorn.driver.models import Driver, DriverAddress, BankAccount, DriverVehicleMap
from blowhorn.vehicle.models import Vehicle, VehicleModel, BodyType
from blowhorn.address.models import City, Country

from migrations import common

def clean_name(name):
    # Allow only alphabets, space and dot
    cleaned = name.replace('.', ' ')
    cleaned = re.sub(r'[^a-zA-Z ]', '', cleaned)
    cleaned = re.sub(r'\s+', ' ', cleaned)
    return cleaned.strip()

def _reduce(s):
    s = s.lower()
    s = s.replace(' ', '')
    return s

def clean_rc(s):
    s = s.upper()
    return re.sub(r'[^0-9A-Z]', '', s)

cities = City.objects.all()
cities_dict = dict([(str(c), c) for c in cities])
country = Country.objects.first()

dls = list(Driver.objects.values_list('driving_license_number', flat=True).distinct())
vehicles = list(Vehicle.objects.values_list('registration_certificate_number', flat=True).distinct())
pans = list(Driver.objects.values_list('pan', flat=True).distinct())
driver_ids = [x.rstrip('@driver.blowhorn.net') for x in \
        list(Driver.objects.values_list('user__email', flat=True).distinct())]

#print('\n'.join(driver_ids))
#print('Cities :', cities_dict)

def get_city(c):
    if c == 'Delhi':
        c = 'Delhi NCR'
    city = cities_dict.get(c)
    if city is None:
        assert False, 'Unable to find city: %s' % c

    return city

vehicle_models = VehicleModel.objects.all()
vehicle_models_dict = dict([(vm.model_name, vm) for vm in vehicle_models])
default_vehicle_model = vehicle_models_dict['Tata Ace']
#print('Vehicle Models :', vehicle_models_dict)
print('Vehicle Model default :', default_vehicle_model)


map_ = {
    'Mahindra Champion' : 'Mahindra Champion',
    'Maruti Ecco' : 'MARUTI EECO',
    'Canter 22FT' : 'Canter 22 ft',
    'Canter 19FT' : 'Canter 19ft',
    'Canter 17FT' : 'Canter 17ft',
    'Canter 14FT' : 'Canter 14ft',
    'Canter 12FT' : 'Canter 12ft',
    'Tata Ace Zip' : 'Tata Ace Zip',
    'Mah Maxximo Mini' : 'Mahindra Maxximo Mini',
    'Al Dost' : 'AL Dost',
    'Tata 407' : 'Tata 407',
    'Omni' : 'Omni',
    'Auto Rickshaw' : 'Auto Rickshaw',
    'Bolero' : 'Bolero',
    'Mah Maxximo' : 'Mahindra Maxximo',
    'Tata Super Ace' : 'Tata Super Ace',
    'Tata Ace' : 'Tata Ace',
    'Tata Ace container' : 'Tata Ace',
    'bolero' : 'Bolero',
    'Tata Ace Container' : 'Tata Ace',
    'Maruthi Omni cargo 2009' : 'Omni',
    'Omni' : 'Omni',
    'TATA Ace' : 'Tata Ace',
    'TATA ACE' : 'Tata Ace',
    'ASHOKLEYLAND DOST' : 'AL Dost',
    'TATA SUPER ACE' : 'Tata Super Ace',
    'Maruthi Omni cargo' : 'Omni',
    'TATa ace 2008' : 'Tata Ace',
    'MAHINDRA MAXIMO' : 'Mahindra Maxximo',
    'Tata Ace High Deck' : 'Tata Ace',
    'Tata Ace Cargo' : 'Tata Ace',
    'TAta Ace ' : 'Tata Ace',
    'Tata Ace BS4' : 'Tata Ace',
    'Tata Ace HT' : 'Tata Ace',
    'OMNI ' : 'Omni',
    'MAXIMO' : 'Mahindra Maxximo',
    'tvs king' : 'Auto Rickshaw',
    'MARUTI OMNI' : 'Omni',
    'tata ace ht' : 'Tata Ace',
    'Tata Ace Ex BS4' : 'Tata Ace',
}

def get_vehicle_model(vm):
    mapped = map_.get(vm.strip(), 'Tata Ace')
    vmo = vehicle_models_dict.get(mapped)
    if vmo:
        return vmo

    return default_vehicle_model

body_types = BodyType.objects.all()
body_types_dict = dict([(_reduce(str(bt)), bt) for bt in body_types])
default_body_type = body_types[0]
#print('Body Types :', body_types_dict)
print('Body Type default :', default_body_type)

def get_body_type(bt):
    bt_ = _reduce(bt or '')
    bto = body_types_dict.get(bt_)
    if bto:
        return bto

    for bt, bto in body_types_dict.items():
        if bt.startswith(bt_):
            return bto

    return default_body_type
#    assert False, 'Body Type not found: %s' % bt

def get_dl(data, id_, dl):
    if data.get('used_in_c2c') and (dl is None or not re.match('^(KA|MH|TN|UP|TS|AP)', dl)):
        dl = id_
    return dl

def create_driver(data):
    vehicle_info = data.get('vehicle')

    key = data.get('__key__')
    id_ = key.get('name')
    if id_ in driver_ids or id_.lstrip('0') in driver_ids:
        return

#    if id_ != '7208250513': #, '7676372247':
#        return

    name = clean_name(data.get('name'))
    user = Driver.objects.upsert_user(
        email=data.get('email'),
        name=name,
        phone_number=id_,
        password='1234',
#        user_id=existing_driver.user.id if existing_driver else None,
        is_active=True if data.get('status') == StatusPipeline.ACTIVE else False
    )

    address = data.get('address')
    current_address = DriverAddress(
        line1=address.get('line'),
        state=address.get('state'),
        postcode=address.get('postal_code'),
        country=country,
    )
    current_address.save()

    permanent_address = data.get('permanent_address')
    pa = DriverAddress(
        line1=permanent_address.get('line'),
        state=permanent_address.get('state'),
        postcode=permanent_address.get('postal_code'),
        country=country,
    )
    pa.save()

    ba = data.get('bank_account')
    bank_account = None
    if ba and ba.get('number'):
#        raise Exception('No bank account')
        bank_account = BankAccount(
            account_name=ba.get('name'),
            ifsc_code=ba.get('bank_code', '').strip(),
            account_number=ba.get('number'),
        )
        bank_account.save()


    dl = (data.get('licence_number') or data.get('driver_license_number') or '').strip()
    pan = data.get('pan_card', '').strip()[:10]

    if pan in pans:
        pan = None

    # If this dl is already present in system,
    # populate his id as a dummy DL
    if dl in dls:
        dl = id_

    dl = dl[:50]

    if dl in dls:
        return

#    dl = get_dl(data, id_, dl)

    cleaned_rc = clean_rc(vehicle_info.get('registration_number'))
    vehicle_string = ''
    if cleaned_rc in vehicles:
        vehicle = Vehicle.objects.get(registration_certificate_number=clean_rc(vehicle_info.get('registration_number')))
        vehicle_string = str(vehicle)
    else:
        vehicle = Vehicle(
            registration_certificate_number=clean_rc(vehicle_info.get('registration_number')),
            vehicle_model=get_vehicle_model(vehicle_info.get('vehicle_model')),
            body_type=get_body_type(vehicle_info.get('body_type')),
        )
        vehicle.save()
        vehicle_string = str(vehicle)

    driver = Driver(
        user=user,
        status=StatusPipeline.SUPPLY,
        pan=pan,
        driving_license_number=dl,
        operating_city=get_city(data.get('operating_city')),
        own_vehicle=data.get('own_vehicle'),
        owner_contact_no=data.get('owner_contact_no'),
        name=name,
        marital_status=data.get('marital_status'),
        current_address=current_address,
        permanent_address=pa,
        bank_account=bank_account,
        driver_vehicle=vehicle_string,
    )

    driver.save()

    try:
        DriverVehicleMap(
            driver=driver,
            vehicle=vehicle,
        ).save()
    except Exception as e:
        print(e)

    pans.append(pan)
    dls.append(dl)
    driver_ids.append(id_)

    return driver


print('\nLoading Driver data')
drivers = json.loads(open('migrations/cleaned_drivers.json').read())

def migrate(start=None, end=None):
    common.migrate(drivers, create_driver, start, end)

