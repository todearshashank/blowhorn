from blowhorn.trip.models import Trip

null_contract_c2c_trips = Trip.objects.filter(
    customer_contract__isnull=True
).filter(
    order__customer_contract__contract_type='C2C'
)

contract_ids = null_contract_c2c_trips.values_list('order__customer_contract', flat=True).distinct()

for ci in contract_ids:
    print(null_contract_c2c_trips.filter(order__customer_contract=ci).update(customer_contract=ci))
