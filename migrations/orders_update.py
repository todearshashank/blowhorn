import json
import pprint

from django.db import transaction
from django.db.models import Q
from blowhorn.order.models import Order, OrderAdjustment, OrderAdjustmentCategory

from migrations import common

pp = pprint.PrettyPrinter(indent=1)

MIGRATE_ONLY_COMPLETED = True



#prior_orders = list(Order.objects.filter(order_type='booking') \
#        .values_list('number', flat=True))
#
#print()
#print('Prior Orders: %d' % len(prior_orders))
#
#map_all_ids_to_primary_id = {}

def update_order(data):
    current_status = data.get('current_status')
    current_status_code = int(data.get('current_status_code'))
#    status = get_new_status(current_status, current_status_code)
    id_ = data.get('id')
    friendly_id = data.get('friendly_id')

    # Check flag and Ignore non-completed orders
    if MIGRATE_ONLY_COMPLETED and current_status != 'completed':
        return

#    payment_mode, payment_status = get_payment_mode_and_status(
#        data.get('payment_mode'),
#        data.get('cash_card'),
#        data.get('payment_status_user'),
#        data.get('payment_status_remote'),
#    )

#        data.get('payment_vendor'), ',',
#        data.get('payment_txn_id'), ','
#    )
#    return


    order_number = friendly_id or id_

#    estimated_cost = data.get('estimated_cost', 0)
#    actual_cost = data.get('actual_cost', 0)
#    fare_breakdown = data.get('fare_breakdown', {})

    actual_cost_pd = data.get('actual_cost_post_discount', 0)
    extra_charges_dict = data.get('extra_charges', {})
    amounts = extra_charges_dict.get('amount', [])
    purposes = extra_charges_dict.get('purpose', [])
    extra_charges = sum(amounts)
    total_charges = actual_cost_pd + extra_charges

    orders_to_update = Order.objects.filter(number=order_number, payment_status='Un-Paid')

    if not extra_charges:
        orders_to_update = orders_to_update.exclude(revised_cost=actual_cost_pd)

    if not orders_to_update:
        return

    o = orders_to_update[0]
    print(order_number, orders_to_update, o.revised_cost, actual_cost_pd, extra_charges, purposes)

    if OrderAdjustment.objects.filter(order=o):
        print('Deleting Adjustments :', OrderAdjustment.objects.filter(order=o).delete())

    for a, p in zip(amounts, purposes):
        cat = OrderAdjustmentCategory.objects.filter(name__icontains=p).first()
        print('Adding Adjustment :', OrderAdjustment.objects.create(
            category=cat,
            order=o,
            comments=p,
            total_amount=a,
        ))
    o.update_invoice_information(actual_cost_pd)
    updated = orders_to_update.update(
        revised_cost=o.revised_cost,
        total_excl_tax=o.total_excl_tax,
        tax=o.tax,
        rcm=o.rcm,
        total_incl_tax=o.total_incl_tax,
        total_payable=o.total_payable,
        invoice_details=o.invoice_details
    )
#
#    print(order_number, 'Updated : ', updated)
    print(order_number, updated, o.revised_cost, actual_cost_pd, extra_charges, purposes)

#    print('Order=%s, Old : fare=%.0f, labour=%.0f, New : revised_cost=%.1f, labour=%.0f total=%0.1f' % (
#        order_number, actual_cost_pd, (lcount * lcph), o.revised_cost, o.get_labour_cost(), o.total_incl_tax))


#print('\nLoading Order data')
#import json
orders = json.load(open('migrations/cleaned_orders_for_update.json'))

#def do():
for order in orders:
    update_order(order)

#def migrate(start=None, end=None):
#    common.migrate(orders, update_order, start, end)
