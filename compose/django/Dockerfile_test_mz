FROM python:3.6

ENV PYTHONUNBUFFERED 1
ENV DJANGO_READ_DOT_ENV_FILE=True
ENV DJANGO_SETTINGS_MODULE=config.settings.test_mz
ENV DEST_PATH=dist_prod

# Install spatial libraries to work with Postgis and Geodjango
RUN apt-get update && \
        apt-get install -y --no-install-recommends \
        binutils \
        libproj-dev \
        gdal-bin \
        python-gdal \
        gettext

RUN groupadd -r django \
    && useradd -r -g django django

# Download and install wkhtmltopdf
RUN apt-get install -y wget
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN tar xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN mv wkhtmltox/bin/wkhtmlto* /usr/bin/

# Requirements have to be pulled and installed here, otherwise caching won't work
COPY ./requirements /requirements
RUN pip install --no-cache-dir -r /requirements/production.txt \
    && rm -rf /requirements

COPY ./compose/django/gunicorn.sh ./compose/django/entrypoint.sh /
RUN sed -i 's/\r//' /entrypoint.sh \
    && sed -i 's/\r//' /gunicorn.sh \
    && chmod +x /entrypoint.sh \
    && chown django /entrypoint.sh \
    && chmod +x /gunicorn.sh \
    && chown django /gunicorn.sh

COPY ./compose/django/run-celeryworker.sh /run-celeryworker.sh
RUN sed -i 's/\r//' /run-celeryworker.sh
RUN chmod +x /run-celeryworker.sh

COPY ./compose/django/run-activityworker.sh /run-activityworker.sh
RUN sed -i 's/\r//' /run-activityworker.sh
RUN chmod +x /run-activityworker.sh

COPY ./compose/django/run-celerybeat.sh /run-celerybeat.sh
RUN sed -i 's/\r//' /run-celerybeat.sh
RUN chmod +x /run-celerybeat.sh

COPY ./compose/django/run-mqtt-server.sh /run-mqtt-server.sh
RUN sed -i 's/\r//' /run-mqtt-server.sh
RUN chmod +x /run-mqtt-server.sh

# create unprivileged user for running celery workers
RUN adduser --disabled-password --gecos '' celeryuser


# INSTALL TOOLS
RUN apt-get update \
    && apt-get -y install unzip \
    && apt-get -y install libaio-dev \
    && mkdir -p /opt/data/api

ADD ./compose/django/oracle-instantclient/ /opt/data
ADD ./compose/django/install-instantclient.sh /opt/data

RUN chmod +x /opt/data/install-instantclient.sh && chown django  /opt/data/install-instantclient.sh

WORKDIR /opt/data

ENV ORACLE_HOME=/opt/oracle/instantclient
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME

ENV OCI_HOME=/opt/oracle/instantclient
ENV OCI_LIB_DIR=/opt/oracle/instantclient
ENV OCI_INCLUDE_DIR=/opt/oracle/instantclient/sdk/include

# INSTALL INSTANTCLIENT AND DEPENDENCIES
RUN /opt/data/install-instantclient.sh

COPY . /app

#RUN chown -R django /app

#USER django

EXPOSE 5000

WORKDIR /app

ENTRYPOINT ["/entrypoint.sh"]
