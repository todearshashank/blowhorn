#!/bin/bash
set -e # Configure shell so that if one command fails, it exits
cmd="$@"
# the official postgres image uses 'postgres' as default user if not set explicitly.
if [ -z "$POSTGRES_USER" ]; then
    export POSTGRES_USER=postgres
fi

# using postgres as default host (in dev env) if not set explicitly.
if [ -z "$POSTGRES_HOST" ]; then
    export POSTGRES_HOST=postgres
fi

export DATABASE_URL="postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:5432/$POSTGRES_DB"

function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$POSTGRES_DB", user="$POSTGRES_USER", password="$POSTGRES_PASSWORD", host="$POSTGRES_HOST")
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing..."
case "$cmd" in
    *shell* ) 
        echo "Running Shell"
        exec $cmd
        ;;
    *pytest* )
        echo "Running pytest"
        exec $cmd
        ;;
    * ) 
        echo ""
        ;;
esac
echo "Starting Tests"
coverage erase  # Remove any coverage data from previous runs
coverage run manage.py test  # Run the full test suite
coverage report  # Report on which files are covered
