# Python build stage
ARG PYTHON_VERSION=3.9.7

# define an alias for the specfic python version used in this file.
FROM python:${PYTHON_VERSION}

ARG DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED 1
ENV DJANGO_READ_DOT_ENV_FILE=False
ENV DJANGO_SETTINGS_MODULE=config.settings.local
ENV DEST_PATH=dist_dev
ENV PIP_DISABLE_PIP_VERSION_CHECK=on

# Install spatial libraries to work with Postgis and Geodjango
RUN apt-get update && \
		apt-get install -y --no-install-recommends \
		binutils \
		libproj-dev \
		gdal-bin \
		libgdal-dev \
		gettext

# Download and install wkhtmltopdf
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN tar xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN mv wkhtmltox/bin/wkhtmlto* /usr/bin/

# Requirements have to be pulled and installed here, otherwise caching won't work
COPY ./requirements /requirements 

# Create Python Dependency and Sub-Dependency Wheels.
RUN pip install -r /requirements/local.txt

# copy all scripts
COPY ./compose/django/entrypoint.sh /entrypoint.sh
RUN sed -i 's/\r//' /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY ./compose/django/start-dev.sh /start-dev.sh
RUN sed -i 's/\r//' /start-dev.sh
RUN chmod +x /start-dev.sh

COPY ./compose/django/gunicorn.sh /gunicorn.sh
RUN sed -i 's/\r//' /gunicorn.sh
RUN chmod +x /gunicorn.sh

COPY ./compose/django/run-celeryworker.sh /run-celeryworker.sh
RUN sed -i 's/\r//' /run-celeryworker.sh
RUN chmod +x /run-celeryworker.sh

COPY ./compose/django/run-celerybeat.sh /run-celerybeat.sh
RUN sed -i 's/\r//' /run-celerybeat.sh
RUN chmod +x /run-celerybeat.sh

COPY ./compose/django/run-mqtt-server.sh /run-mqtt-server.sh
RUN sed -i 's/\r//' /run-mqtt-server.sh
RUN chmod +x /run-mqtt-server.sh

# create unprivileged user for running celery workers
RUN adduser --disabled-password --gecos '' celeryuser

WORKDIR /app

ENTRYPOINT ["/entrypoint.sh"]
