#!/bin/bash
set -e
cmd="$@"

cp ./mytrips_src/$DEST_PATH/inline.bundle.js ./website/static/mytrips/
cp ./mytrips_src/$DEST_PATH/main.bundle.js ./website/static/mytrips/
cp ./mytrips_src/$DEST_PATH/polyfills.bundle.js ./website/static/mytrips/
cp ./mytrips_src/$DEST_PATH/styles.bundle.js ./website/static/mytrips/
cp ./mytrips_src/$DEST_PATH/vendor.bundle.js ./website/static/mytrips/

echo "Copying mytrips package from ./mytrips_src/$DEST_PATH/"

# This entrypoint is used to play nicely with the current cookiecutter configuration.
# Since docker-compose relies heavily on environment variables itself for configuration, we'd have to define multiple
# environment variables just to support cookiecutter out of the box. That makes no sense, so this little entrypoint
# does all this for us.

# if grep -q "start-dev.sh" $file_name
# then
#     chmod 775 /app/celerybeat.pid
# fi

# if /app/celerybeat.pid; then
# 	chmod 775 /app/celerybeat.pid
# fi

# the official postgres image uses 'postgres' as default user if not set explicitly.
if [ -z "$POSTGRES_USER" ]; then
    export POSTGRES_USER=postgres
fi

# using postgres as default host (in dev env) if not set explicitly.
if [ -z "$POSTGRES_HOST" ]; then
    export POSTGRES_HOST=postgres
fi

export DATABASE_URL=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:5432/$POSTGRES_DB


function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$POSTGRES_DB", user="$POSTGRES_USER", password="$POSTGRES_PASSWORD", host="$POSTGRES_HOST")
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing..."
exec $cmd
