#!/bin/sh

# wait for RabbitMQ server to start
sleep 10

su -m celeryuser -c "celery -A blowhorn worker -Q activity -l INFO -n activity_worker --concurrency=3 --pidfile="
