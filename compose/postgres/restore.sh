#!/bin/bash

DUMPFILE=/backups/$1
#DATAFILE=/backups/$2
if [ $# -ne 1 ]; then
  echo "Usage docker exec <blowhorn_postgres_1> restore <schema_file/data_file>"
  exit 1
fi

if [ ! -f $DUMPFILE ]; then
  echo "Please provide restoring pg_dump at /backups"
  echo "Copy the backup file to docker: docker cp $1 <blowhorn_postgres_1>:/backups"
  exit 1
fi

# turn off vacuum for large restore
sed -i -e '/autovacuum = on/c autovacuum = off' \
    /var/lib/postgresql/data/postgresql.conf

pg_restore -c -d $DB_NAME -U $DB_USER --role $DB_USER --verbose $DUMPFILE --no-owner --jobs 1

# turn on autovacuum post restore
sed -i -e '/autovacuum = off/c autovacuum = on' \
    /var/lib/postgresql/data/postgresql.conf
