user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log info;
pid        /var/run/nginx.pid;

events {
  worker_connections  1024;
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;
  ignore_invalid_headers off;

  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
  '$status $body_bytes_sent "$http_referer" '
  '"$http_user_agent" "$http_x_forwarded_for"';

  access_log  /var/log/nginx/access.log  main;

  sendfile        on;
  #tcp_nopush     on;

  keepalive_timeout  65;
  proxy_read_timeout 180s;

  #gzip  on;

  upstream app {
    server django:5000;
  }

  # set client body size to 5M #
  client_max_body_size 5M;

  server {
    listen 80;
    listen 443 ssl;

#    server_name blowhorn.com; # rename to appropriate server

    ssl_certificate /etc/nginx/fullchain.pem;
    ssl_certificate_key /etc/nginx/privkey.pem;
    add_header Strict-Transport-Security "max-age=31536000";

#    if ($http_x_forwarded_proto = 'http') {
    if ($scheme != "https"){
      return 301 https://$host$request_uri;
    }

    location /website/ {
      alias /app/staticfiles/website/;
    }

#    location ^~ /blog {
#    rewrite ^/blog(.*) http://blog.blowhorn.com$1 permanent;

    # location /blog/ {
    #     proxy_set_header Host blowhorn.com;
    #     proxy_set_header X-Real-IP 13.126.156.13;
    #     proxy_set_header X-Forwarded-For 13.126.156.13;
    #     proxy_set_header X-Forwarded-Proto http;
    #     proxy_redirect off;
    #     proxy_pass https://blog.blowhorn.com/;
    # }

# Redirect shipment order from admin site to tertiary server

    location ~ /admin/(shipment|order/shipmentorder|order/newshipmentorder)/ {
        proxy_pass http://3.133.77.230:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /admin/(trip/trip|driver/driverpayment/)/ {
        proxy_pass http://3.133.77.230:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /api/drivers/v[0-9]+/(orders/scan|trips/[0-9]+/end) {
        proxy_pass http://3.133.77.230:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /admin/ {
        proxy_pass http://18.219.255.208:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }



# Location update url should be at top.

    location ~ /driver/v7/location {
        proxy_pass http://18.188.34.81:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /api/driver/[0-9]+/activity {
        proxy_pass http://18.188.34.81:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

# Other App end points
    location ~ /api/trips/ {
        proxy_pass http://18.188.34.81:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /api/trip/ {
        proxy_pass http://18.188.34.81:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /api/drivers/v[0-9]+/ {
        proxy_pass http://18.188.34.81:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /driver/v7/ {
        proxy_pass http://18.188.34.81:5000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location ~ /api/orders/ {
        if ($query_string = "mode=test") {
        rewrite ^(.*)/$ http://api.blowhorn.com:8000$1? permanent;
        }
      proxy_pass http://app;
      proxy_redirect off;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location / {
      # checks for static file, if not found proxy to app
      try_files $uri @proxy_to_app;
    }

    # cookiecutter-django app
    location @proxy_to_app {
      proxy_pass http://app;
      proxy_redirect off;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    # redirect server error pages to the static page /50x.html
    error_page 500 502 503 504  /50x.html;
    location = /50x.html {
      root html;
    }
  }
}

