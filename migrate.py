import sys
import os
import fnmatch
import logging


#Source - App Engine
#Change to point to GAE
sys.path.append('/usr/local/google_appengine/')
sys.path.append('/usr/local/google_appengine/lib/yaml/lib/')
if 'google' in sys.modules:
    del sys.modules['google']

from google.appengine.api.files import records
from google.appengine.datastore import entity_pb
from google.appengine.api import datastore
from google.appengine.ext import db, ndb
sys.path.append("/Users/santosh/Development/acegae")
app_id='s~blowhorntemplate'
os.environ['APPLICATION_ID'] = app_id
datastore_file = '/dev/null'
from google.appengine.api import apiproxy_stub_map,datastore_file_stub
apiproxy_stub_map.apiproxy = apiproxy_stub_map.APIProxyStubMap()
stub = datastore_file_stub.DatastoreFileStub(app_id, datastore_file, '/')
apiproxy_stub_map.apiproxy.RegisterStub('datastore_v3', stub)

from db.database import DBUser
from db.database import DBBusinessUser
from db.database import DBDriver
from db.database import DBBooking

#Target - Django/Postgres
sys.path.append('/usr/local/lib/python2.7/site-packages')
import django
from django.utils import timezone
sys.path.append("/Users/santosh/Development/blowhorn")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")
django.setup()
from django.db import models
from django.contrib.auth import get_user_model
from blowhorn.oscar.core.loading import get_class, get_model
from django.contrib.gis.db.models import PointField


backup_dir='/Users/santosh/Development/backups/datastore_backup_datastore_backup_2017_06_12_DBUser'

User = get_user_model()
Order = get_model('order', 'Order')
Driver = get_model('driver', 'Driver')
Vehicle = get_model('vehicle', 'Vehicle')
VehicleClass = get_model('vehicle', 'VehicleClass')
VehicleModel = get_model('vehicle', 'VehicleModel')
BodyType = get_model('vehicle', 'BodyType')
BankAccount = get_model('driver', 'BankAccount')
DriverRating = get_model('driver', 'DriverRating')
DriverAddress = get_model('driver', 'DriverAddress')
# Country = get_model('address', 'Country')
Customer = get_model('customer', 'Customer')
CustomerAddress = get_model('customer', 'CustomerAddress')
CustomerCategory = get_model('customer', 'CustomerCategory')
CustomerRating = get_model('customer', 'CustomerRating')

import datetime
dummy_date=datetime.date(1900,1,1)

def mapUsers(e):
    try:
        print(e)
        cc, created = CustomerCategory.objects.get_or_create(
            category = 'SME' if e.category.pricing == 'sme' else 'Individual')

        if e.home.address.line is not None:
            ca, created = CustomerAddress.objects.get_or_create(
                first_name=e.name,
                last_name='DUMMY',
                line1=e.home.address.line,
                line2=e.home.address.area,
                line3=e.home.address.landmark,
                line4=e.home.address.city,
                state=e.home.address.state,
                postcode=e.home.address.postal_code,
                country=cntry)

        cr, created = CustomerRating.objects.get_or_create(
            cumulative_rating=e.rating.value,
            num_of_feedbacks=e.rating.count)

        u, created = Customer.objects.get_or_create(
	    customername=e.name,
	    date_joined=timezone.make_aware(e.creation_time, timezone.get_current_timezone()),
            phone_number = '+91' + e.key.id() if e.id_type == 'mobile' else "",
	    email = e.key.id() if e.id_type == 'email' else "",
            current_address = ca,
            customer_category = cc if cc else None,
            customer_rating = cr)

    except django.db.utils.DataError as e:
        logger.error(str(e))

def mapBusinessUsers(e):
    try:
        u, created = User.objects.get_or_create(
	    username=e.key.id(),
	    date_joined=timezone.make_aware(e.creation_time, timezone.get_current_timezone()),
	    first_name=e.name)
        print(u)
        sys.exit(0)
    except django.db.utils.DataError as e:
        logger.error(str(e))

def mapDrivers(e):
    try:
        print(e)
        vc, created = VehicleClass.objects.get_or_create(
            commercial_classification='DUMMY' if e.vehicle.vehicle_model.vehicle_class is None
            else e.vehicle.vehicle_model.vehicle_class,
            legal_classification='DUMMY')

        vm, created = VehicleModel.objects.get_or_create(
            model_name=e.vehicle.vehicle_model,
            model_number='DUMMY',
            capacity='DUMMY',
            vehicle_class=vc)

        b, created = BodyType.objects.get_or_create(
            body_type='Not Specified' if e.vehicle.body_type is None else e.vehicle.body_type)

        v, created = Vehicle.objects.get_or_create(
            registration_certificate_number=e.vehicle.registration_number,
            body_type=b,
            vehicle_model=vm)

        if e.bank_account is None:
            ba, created = BankAccount.objects.get_or_create(
                account_name='DUMMY',
                account_number='DUMMY',
                ifsc_code='DUMMY')
        else:
            ba, created = BankAccount.objects.get_or_create(
                account_name=e.bank_account.name,
                account_number=e.bank_account.number,
                ifsc_code=e.bank_account.bank_code)

        dr, created = DriverRating.objects.get_or_create(
            cumulative_rating=e.rating.value,
            num_of_feedbacks=e.rating.count)


        da1, created = DriverAddress.objects.get_or_create(
            title='Mr',
            first_name=e.name,
            last_name='DUMMY',
            line1=e.permanent_address.line,
            line2=e.permanent_address.area,
            line3=e.permanent_address.landmark,
            line4=e.permanent_address.city,
            state=e.permanent_address.state,
            postcode=e.permanent_address.postal_code,
            country=cntry)

        da2, created = DriverAddress.objects.get_or_create(
            title='Mr',
            first_name=e.name,
            last_name='DUMMY',
            line1=e.address.line,
            line2=e.address.area,
            line3=e.address.landmark,
            line4=e.address.city,
            state=e.address.state,
            postcode=e.address.postal_code,
            country=cntry)

        d, created = Driver.objects.get_or_create(
            drivername=e.name,
            phone_number=e.key.id(),
            date_joined=dummy_date if e.date_of_joining is None else e.date_of_joining,
            date_of_birth=dummy_date if e.date_of_birth is None else e.date_of_birth,
            bank_account=ba,
            driver_rating=dr,
            permanent_address=da1,
            current_address=da2)

    except django.db.utils.DataError as e:
        logger.error(str(e))

def mapOrders(e):
    try:
        print(e)
    except django.db.utils.DataError as e:
        logger.error(str(e))

def map_to_target(e):
    if isinstance(e, DBUser):
        mapUsers(e)
    elif isinstance(e, DBBusinessUser):
        mapBusinessUsers(e)
    elif isinstance(e, DBDriver):
        mapDrivers(e)
    elif isinstance(e, DBBooking):
        mapOrders(e)

def readyMigration():
    n = Customer.objects.all().delete()
    print(n)
    Country.objects.all().delete()

    cntry, created = Country.objects.get_or_create(
        iso_3166_1_a2='IN',
        iso_3166_1_a3='IND',
        iso_3166_1_numeric='356',
        printable_name='India',
        name='India',
        display_order=0,
        is_shipping_country=True)

#if __name__ == "__main__":
logger = logging.getLogger();
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)
cntry = Country.objects.get(iso_3166_1_a2='IN')

readyMigration()
for root, dirs, files in os.walk(backup_dir):
	for file in files:
	    if fnmatch.fnmatch(file, 'output-*'):
                raw = open(os.path.join(root, file), 'rb')
                reader = records.RecordsReader(raw)
                for record in reader:
                    entity_proto = entity_pb.EntityProto(contents=record)
                    entity = datastore.Entity.FromPb(entity_proto)
                    map_to_target(ndb.ModelAdapter().pb_to_entity(entity_pb.EntityProto(record)))

logger.info('Created %d users', User.objects.count())
