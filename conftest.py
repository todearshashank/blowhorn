# conftest.py
import pytest

from rest_framework.test import APIClient

from blowhorn.customer.models import Customer
from blowhorn.driver.models import Driver
from blowhorn.address.models import City


# Inspiration for parameterisation
# https://www.linkedin.com/pulse/how-write-parameterized-unit-tests-django-rest-framework--1e?trk=organization-update-content_share-article
@pytest.fixture
def test_password():
    return 'strong-test-pass'

@pytest.fixture
def create_user(db, django_user_model, test_password):
    def make_user(**kwargs):
        kwargs['password'] = test_password
        return django_user_model.objects.create_user(**kwargs)
    return make_user

@pytest.fixture()
def admin_client(client, admin_user):
    client.force_login(admin_user)
    return client

@pytest.fixture
def test_city(db):
    city = City.objects.create(name="Bengaluru")
    return city

@pytest.fixture
def api_client(test_user):
    client = APIClient()
    client.force_authenticate(test_user)
    return client

@pytest.fixture
def test_customer(db, test_user):
    customer, _ = Customer.objects.get_or_create(
            user=test_user
    )
    return customer

@pytest.fixture
def test_driver(db, test_user):
    driver, _ = Driver.objects.get_or_create(
            user=test_user
    )
