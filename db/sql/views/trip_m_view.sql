create materialized view trip_tripMView TABLESPACE mview as
SELECT t.id, t.app_version, t.status, t.trip_number, t.trip_base_pay , h.name as hub_name, d.name as driver_name,
bh.name as blowhorn_contract_name, vc.commercial_classification, cc.contract_type, c.customer_category,
c.name as customer_name, c.legalname, ct.name as city_name,
CASE WHEN t.is_contingency = True THEN 'Yes' ELSE 'No' END AS "conti", cd.name as division_name,
to_char(t.gps_distance, '999D9') AS "precise_gps_distance", to_char(t._gps_distance, '999D9') AS "mongo_gps_distance",
to_char(t.total_time,'999D9') AS "precise_total_time",
to_char(t.app_distance,'999D9') AS "formatted_app_distance", t.app_points_count, cu.name as "created_by",
mu.name as "modified_by", vh.registration_certificate_number, cc.name as "contract_name",
o.number, to_char((t.planned_start_time + INTERVAL '330min'), 'DD/MM/YYYY') AS "scheduled_date",
to_char((t.planned_start_time + INTERVAL '330min'), 'HH24:MI') AS "scheduled_time",
to_char((t.actual_start_time + INTERVAL '330min'), 'DD/MM/YYYY') AS "actual_st_date",
to_char((t.actual_start_time + INTERVAL '330min'), 'HH24:MI') AS "actual_st_time",
to_char((t.actual_end_time + INTERVAL '330min'), 'DD/MM/YYYY') AS "end_date",
to_char((t.actual_end_time + INTERVAL '330min'), 'HH24:MI') AS "end_time",
to_char(t.total_distance, '999D9') AS "precise_total_distance", t.meter_reading_start, t.meter_reading_end, t.assigned,
t.delivered, t.attempted, t.rejected, t.pickups, t.returned, t.delivered_cash, t.delivered_mpos,
SUM(t.total_distance) OVER (ORDER BY t.id) AS "cum_distance", SUM(t.total_time) OVER (ORDER BY t.id) AS "cum_time",
to_char(t.toll_charges, '999D9') AS "toll_amount", to_char(t.margin, '999D9') AS "operating_margin",
to_char(t.customer_amount, '999D9') AS "revenue_side", to_char(t.driver_amount,'999D9') AS "cost_side"

from trip_trip t

LEFT OUTER JOIN driver_driver d on t.driver_id = d.id
LEFT OUTER JOIN address_hub h on t.hub_id = h.id
LEFT OUTER JOIN contract_contract bh on t.blowhorn_contract_id = bh.id
LEFT OUTER JOIN vehicle_vehicle vh on t.vehicle_id = vh.id
LEFT OUTER JOIN vehicle_vehiclemodel vm on vh.vehicle_model_id = vm.id
LEFT OUTER JOIN vehicle_vehicleclass vc on vm.vehicle_class_id = vc.id
LEFT OUTER JOIN contract_contract cc on t.customer_contract_id = cc.id
LEFT OUTER JOIN customer_customer c on cc.customer_id = c.id
LEFT OUTER JOIN address_city ct on cc.city_id = ct.id
LEFT OUTER JOIN customer_customerdivision cd on cc.division_id = cd.id
LEFT OUTER JOIN users_user cu on t.created_by_id = cu.id
LEFT OUTER JOIN users_user mu on t.modified_by_id = mu.id
LEFT OUTER JOIN order_order o on t.order_id = o.id

WHERE planned_start_time  > current_date - interval '180' day;
