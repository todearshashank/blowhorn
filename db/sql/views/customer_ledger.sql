-- View: public.customer_ledger

-- DROP VIEW public.customer_ledger;

CREATE OR REPLACE VIEW public.customer_ledger AS
 WITH data AS (
         SELECT customer_customerinvoice.customer_id AS "Customer",
            customer_customerinvoice.invoice_date + interval '5.5 hour' AS "Date",
            customer_customerinvoice.taxable_amount AS "Debit",
            0 AS "Credit",
            customer_customerinvoice.invoice_number AS "Description"
           FROM customer_customerinvoice
          WHERE (customer_customerinvoice.status::text = ANY (ARRAY['CUSTOMER ACKNOWLEDGED'::character varying::text, 'CUSTOMER CONFIRMED'::character varying::text, 'PAYMENT DONE'::character varying::text])) AND NOT (EXISTS ( SELECT 1
                   FROM customer_openingbalance
                  WHERE customer_openingbalance.customer_id = customer_customerinvoice.customer_id))
        UNION
         SELECT customer_customerinvoice.customer_id AS "Customer",
            customer_customerinvoice.invoice_date + interval '5.5 hour' AS "Date",
            customer_customerinvoice.taxable_amount AS "Debit",
            0 AS "Credit",
            customer_customerinvoice.invoice_number AS "Description"
           FROM customer_customerinvoice
          WHERE (customer_customerinvoice.status::text = ANY (ARRAY['CUSTOMER ACKNOWLEDGED'::character varying::text, 'CUSTOMER CONFIRMED'::character varying::text, 'PAYMENT DONE'::character varying::text])) AND customer_customerinvoice.invoice_date >= (( SELECT max(customer_openingbalance.date) AS max
                   FROM customer_openingbalance
                  WHERE customer_openingbalance.customer_id = customer_customerinvoice.customer_id))
        UNION
         SELECT order_order.customer_id AS "Customer",
          order_order.pickup_datetime + interval '5.5 hour' AS "Date",
          order_order.total_payable AS "Debit",
          0 AS "Credit",
          concat(coalesce(payment_mode, null, 'None'), ' | ', order_order.number) AS "Description"
          FROM order_order
          WHERE order_order.order_type = 'booking'
          and (order_order.payment_mode not in ('Online', 'PayTm') or payment_mode is null)
          and order_order.status = 'Delivered'
          AND (NOT EXISTS ( SELECT 1
                   FROM customer_openingbalance
                  WHERE customer_openingbalance.customer_id = order_order.customer_id)
               OR (order_order.pickup_datetime > (SELECT max(customer_openingbalance.date) AS max
                   FROM customer_openingbalance
                  WHERE customer_openingbalance.customer_id = order_order.customer_id)))
        UNION
         SELECT ob.customer_id AS "Customer",
            ob.date AS "Date",
            ob.amount AS "Debit",
            0 AS "Credit",
            concat('Opening Balance ', ob.comments) AS "Description"
           FROM customer_openingbalance ob
          WHERE ob.date = (( SELECT max(cob.date) AS max
                   FROM customer_openingbalance cob
                  WHERE cob.customer_id = ob.customer_id))
        UNION
         SELECT ci.customer_id AS "Customer",
            ip.paid_on + interval '5.5 hour' AS "Date",
            0 AS "Debit",
            ip.actual_amount AS "Credit",
            'Receipt - Credited to Bank' AS "Description"
           FROM customer_invoicepayment ip, customer_customerinvoice ci
          WHERE ip.invoice_id = ci.id
        UNION
         SELECT ci.customer_id AS "Customer",
            ip.paid_on + interval '5.5 hour' AS "Date",
            0 AS "Debit",
            ip.tds AS "Credit",
            'Receipt - TDS Deducted' AS "Description"
           FROM customer_invoicepayment ip, customer_customerinvoice ci
          WHERE ip.invoice_id = ci.id
        )
 SELECT row_number() OVER () AS id,
    row_number() OVER (PARTITION BY data."Customer" ORDER BY data."Date" ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS record_number,
    data."Customer" AS customer_id,
    date_trunc('day', data."Date") AS transaction_date,
    data."Description" AS description,
    data."Debit" AS debit,
    data."Credit" AS credit,
    sum(data."Debit"::double precision - data."Credit") OVER (PARTITION BY data."Customer" ORDER BY data."Date" ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS balance
   FROM data;

ALTER TABLE public.customer_ledger
    OWNER TO bh_user;

GRANT ALL ON TABLE public.customer_ledger TO bh_user;
GRANT SELECT ON TABLE public.customer_ledger TO PUBLIC;
GRANT ALL ON TABLE public.customer_ledger TO rdsadmin;

