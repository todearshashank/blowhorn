create materialized view order_shipmentorderMView as
SELECT
c.name as company_name,
phub.name as pickup_hub,
dhub.name as delivery_hub,
o.customer_reference_number,
o.reference_number,
o.status,
to_char((o.date_placed + INTERVAL '330min'), 'DD/MM/YYYY') as order_placed_on,
s.first_name as customer_name,
s.line1 as shipping_address,
s.postcode as pincode,
st_x(s.geopoint) as Latitude,
st_y(s.geopoint) as Longitude,
s.phone_number as customer_phone_number,
pa.first_name as pickup_name,
pa.line1 as pickup_address,
pa.postcode as pickup_pincode,
pa.phone_number as Pick_up_Phone_Number,
o.cash_on_delivery,
o.number,
u.name as driver_name,
u.phone_number as driver_phone_number,
to_char((o.expected_delivery_time + INTERVAL '330min'), 'DD/MM/YYYY HH24: MI') AS expected_delivery_time,
o.Remarks,

(SELECT address_state.name
  FROM address_state
 INNER JOIN address_state_cities
    ON address_state.id = address_state_cities.state_id
 WHERE address_state_cities.city_id IN (cc.id)),

cc.name as city,
to_char((o.bill_date + INTERVAL '330min'), 'DD/MM/YYYY') AS bill_date,
o.bill_number,
to_char(((SELECT time FROM order_event WHERE (order_id = o.id AND status = 'Packed')  LIMIT 1) +  INTERVAL '330min'), 'DD/MM/YYYY HH24: MI"') AS last_packed_at,
to_char(((SELECT time FROM order_event WHERE (order_id = o.id AND status = 'Out-For-Delivery')  LIMIT 1) +  INTERVAL '330min'), 'DD/MM/YYYY HH24: MI"') AS out_on_road_at,
to_char(((SELECT time FROM order_event WHERE (order_id = o.id AND status = 'Delivered')  LIMIT 1) +  INTERVAL '330min'), 'DD/MM/YYYY HH24: MI"') AS delivered_at,
to_char(((SELECT time FROM order_event WHERE (order_id = o.id AND status in ('Delivered','Returned','Unable-To-Deliver') )  LIMIT 1) +  INTERVAL '330min'), 'DD/MM/YYYY HH24: MI"') AS first_attempted_at,
to_char(((SELECT time FROM order_event WHERE (order_id = o.id AND status in ('Delivered','Returned','Unable-To-Deliver') ) ORDER BY order_id DESC LIMIT 1) +  INTERVAL '330min'), 'DD/MM/YYYY HH24: MI"') AS last_attempted_at,
(SELECT string_agg(customer_sku.name, ' | ' ) from customer_sku WHERE customer_sku.id in (select sku_id from order_orderline ol WHERE ol.order_id=o.id)) AS sku,
(SELECT sum(quantity) from order_orderline WHERE order_id =o.id) as total_sku_qty,
o.weight,
o.volume,
(SELECT string_agg(file,' | ') from order_orderdocument WHERE order_id = o.id) as orderdocument,
o.order_type

from order_order o

LEFT OUTER JOIN customer_customer c on o.customer_id = c.id
LEFT OUTER JOIN address_hub phub on o.pickup_hub_id = phub.id
LEFT OUTER JOIN address_hub dhub on o.hub_id = dhub.id
LEFT OUTER JOIN order_shippingaddress s on o.shipping_address_id = s.id
LEFT OUTER JOIN users_user u on o.driver_id = u.id
LEFT OUTER JOIN address_city cc on o.city_id = cc.id
LEFT OUTER JOIN order_shippingaddress pa on o.pickup_address_id =pa.id



WHERE order_type='shipment' and date_placed  > current_date - interval '180' day;
