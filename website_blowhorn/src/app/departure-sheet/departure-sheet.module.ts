import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartureSheetMainComponent } from './components/departure-sheet-main/departure-sheet-main.component';
import { DepartureSheetHeaderComponent } from './components/departure-sheet-header/departure-sheet-header.component';
import { DepartureSheetContentComponent } from './components/departure-sheet-content/departure-sheet-content.component';
import { NgxBarcode6Module } from 'ngx-barcode6';

@NgModule({
  imports: [
    CommonModule,
    NgxBarcode6Module
  ],
  declarations: [DepartureSheetMainComponent, DepartureSheetHeaderComponent, DepartureSheetContentComponent]
})
export class DepartureSheetModule { }
