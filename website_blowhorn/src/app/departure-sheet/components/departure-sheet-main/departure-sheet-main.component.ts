import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DepartureSheet } from '../../../shared/models/departure-sheet.model';
import { Shipment } from '../../../shared/models/shipment.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-departure-sheet-main',
  templateUrl: './departure-sheet-main.component.html',
  styleUrls: ['./departure-sheet-main.component.scss']
})
export class DepartureSheetMainComponent implements OnInit {

  driver_name: string;
  date_time: string;
  shipments: Shipment[];
  status: string;
  message: string;
  vehicle_number: string;

  constructor(
    private route: ActivatedRoute,
    public blowhorn_service: BlowhornService
  ) {
    
    route.data
      .subscribe(data => {
        const departure_sheet = data.departure_sheet;
        this.driver_name = departure_sheet.driver_name;
        this.date_time = departure_sheet.date_time;
        this.shipments = departure_sheet.shipments;
        this.status = departure_sheet.status;
        this.message = departure_sheet.message;
        this.vehicle_number = departure_sheet.vehicle_number;
      },
    err => {
      console.error(err);
    });
  }

  ngOnInit() {
  }

}
