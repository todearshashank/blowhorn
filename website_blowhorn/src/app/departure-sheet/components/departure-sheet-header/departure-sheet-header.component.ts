import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-departure-sheet-header',
  templateUrl: './departure-sheet-header.component.html',
  styleUrls: ['./departure-sheet-header.component.scss']
})
export class DepartureSheetHeaderComponent implements OnInit {

  @Input('driver_name')
  driver_name: string;
  @Input('date_time')
  date_time: string;
  @Input('vehicle_number')
  vehicle_number: string;
  @Input('count')
  count: number;

  constructor() { }

  ngOnInit() {
  }

  print() {
    print();
  }

}
