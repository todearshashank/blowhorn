import { Component, OnInit, Input } from '@angular/core';
import { Shipment } from '../../../shared/models/shipment.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-departure-sheet-content',
  templateUrl: './departure-sheet-content.component.html',
  styleUrls: ['./departure-sheet-content.component.scss']
})
export class DepartureSheetContentComponent implements OnInit {

  @Input('shipments')
  shipments: Shipment[];

  barcode_config = {
    format: 'CODE128',
    value: '',
    show_value: true,
    height: 40,
    width:1
  }

  constructor(
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
  }

}
