import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  ElementRef,
  EventEmitter
} from "@angular/core";
import { Trip } from "../../../shared/models/trip.model";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PaymentService } from "../../../shared/services/payment.service";
import { TripsService } from "../../../shared/services/trips.service";
import { KeysPipe } from "../../../shared/pipes/keys.pipe";
import { BlowhornService } from "../../../shared/services/blowhorn.service";
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: "app-list-trips",
  templateUrl: "./list-trips.component.html",
  styleUrls: ["./list-trips.component.scss"],
  providers: [PaymentService]
})
export class ListTripsComponent implements OnInit {
  @Input() trips: Trip[] = [];
  @Input() showMap = false;
  @Input() done = false;
  @Input() bookingMode = false;
  @ViewChild("overlay_panel_image", {static: false}) overlay_panel: ElementRef;
  @Output() reschedule_trip: EventEmitter<{ trip: Trip }>;
  selectedTrip: Trip;
  datetime: any;
  trackingURL = "";
  bookingId = "";
  selectedCancellationReason = "";
  selected_reason_name = '';
  selected_reason_id = null;
  cancellationReasons = this.blowhorn_service.cancellationReasons;
  cancellationReasonKeyValues = [];
  show_image_viewer = false;
  document_links: {}[] = [];
  show_share_invoice_modal = false;
  show_payment_options = false;
  constants = Constants;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    public paymentService: PaymentService,
    public tripService: TripsService,
    public blowhorn_service: BlowhornService
  ) {
    this.reschedule_trip = new EventEmitter<{ trip: Trip }>(null);
    this.cancellationReasons.forEach((item: any, index: number) => {
      this.cancellationReasonKeyValues.push({
        id: index, name: item
      });
   })
  }

  ngOnInit() {
    this.blowhorn_service.loadRazorpayLibrary();
  }

  public moveTostartPage() {
    this.router.navigate(["dashboard", "mytrips"]);
  }

  public moveToDetails(orderId: any) {
    this.router.navigate(["track", orderId]);
  }

  public moveToInvoice(orderId: string) {
    // this.router.navigate(['invoice', orderId]);
    window.open("/invoice/" + orderId, "_blank");
  }

  public openShareInvoice(trip: Trip) {
    this.show_share_invoice_modal = true;
    this.selectedTrip = trip;
  }

  public cancelBooking(cancelBooking: any, trip: Trip) {
    this.selectedTrip = trip;
    this.selectedCancellationReason = "";
    this.modalService.open(cancelBooking).result.then(
      result => {
        this.blowhorn_service.show_loader();
        this.tripService
          .remove_trip(trip.order_real_id, this.selected_reason_name)
          .subscribe((resp: any) => {
            this.tripService.refresh_trips();
            this.blowhorn_service.hide_loader();
          });
      },
      reason => {}
    );
  }

  public openRebooking(cancelBooking, rebookPopup, trip: Trip) {
    this.selectedTrip = trip;
    this.modalService.open(cancelBooking).result.then(
      result => {
        this.tripService
          .rebook_trip(this.datetime, this.selectedTrip.order_real_id)
          .subscribe((resp: any) => {
            this.bookingId = resp.bookingId;
            this.trackingURL = resp.trackingURL;
            this.tripRebookCreatedOpen(rebookPopup);
            this.tripService.refresh_trips();
          });
      },
      reason => {}
    );
  }

  public tripRebookCreatedOpen(content: any) {
    this.modalService.open(content).result.then(result => {}, reason => {});
  }

  public showPaymentOptions(trip: Trip) {
    this.selectedTrip = trip;
    this.show_payment_options = true;
  }

  rescheduleBooking(trip: Trip): void {
    this.reschedule_trip.emit({ trip: trip });
  }

  show_documents(document_links: string[]) {
    this.document_links = document_links.map(item => {
        return {url: item};
    });
    this.show_image_viewer = true;
  }

  close_fired(event: any) {
    if (event instanceof KeyboardEvent && event.keyCode !== 27) {
      return;
    }
    this.document_links = [];
    this.show_image_viewer = false;
  }

  focus_panel(event: any) {
    this.overlay_panel.nativeElement.focus();
  }

  reason_selected(event: any) {
    this.selected_reason_id = event.id;
    this.selected_reason_name = event.name;
  }

  get document_viewer_styles() {
    return {
      wrapper: {
        width: "90vw",
        height: "75vh"
      }
    };
  }

  get cancellation_reason_dropdown_styles(): {} {
    return {
      'dropdown-styles': {
        'width': '320px',
        'z-index': 2,
        boxShadow: 'none',
        'margin-top': '12px',
      },
      'dropdown-menu-styles': {
        'width': '100%',
      }
    };
  }
}
