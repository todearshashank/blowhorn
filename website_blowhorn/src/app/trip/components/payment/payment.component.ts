import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import { PaymentService } from '../../../shared/services/payment.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
  ]
})
export class PaymentComponent implements OnInit {

  @Input() trip: any;
  @Output() close_fired: EventEmitter<boolean>;
  show = {
    paytm: false,
    razorpay: false,
    payfast: false,
    wallet: true
  }
  walletMessages = {
    lowBalance: '',
    login: ''
  }
  message = '';
  success = false;

  constructor(
      public modalService: NgbModal,
      public paymentService: PaymentService,
      public blowhornService: BlowhornService,
  ) {
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
    let payment_methods = this.blowhornService.payment_methods;
    this.show.paytm = payment_methods.indexOf('paytm') != -1;
    this.show.razorpay = payment_methods.indexOf('razorpay') != -1;
    this.show.payfast = payment_methods.indexOf('payfast') != -1;
    if (this.blowhornService.is_customer) {
      let balanceInWallet = this.blowhornService.user_profile.wallet_available_balance;
      if (this.trip.total_payable > balanceInWallet) {
        this.walletMessages.lowBalance = `Insufficient balance in wallet. Current Balance: ${balanceInWallet}`
      }
    } else if (this.blowhornService.is_staff) {
      this.walletMessages.login = 'Login as customer to pay via wallet';
    } else {
      this.walletMessages.login = 'Login to pay via wallet';
    }
    console.log('trip: ', this.trip);
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
    this.close_fired.emit(true);
  }

  public showPaymentOptions() {
    this.modalService.open(PaymentComponent).result.then(
      (result) => {
      },
      (reason) => {}
    );
  }

  payViaWallet()  {
    if (this.walletMessages.login || this.walletMessages.lowBalance) {
      return;
    }
    this.blowhornService.show_loader();
    this.paymentService.initiate_wallet_payment(this.trip)
    .subscribe((resp: any) => {
      this.blowhornService.hide_loader();
      this.message = 'Payment completed successfully';
      this.success = true;
      setTimeout(() => {
        this.close_fired.emit(true);
      }, 3000);
    },
    err => {
      console.log('err', err);
      this.message = 'Not able to withdraw from wallet';
      this.success = false;
      this.blowhornService.hide_loader();
    });
  }
}
