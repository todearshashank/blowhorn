import { Component, OnInit, Input } from '@angular/core';
import { Stop } from '../../../shared/models/stop.model';

@Component({
  selector: 'app-info-window',
  templateUrl: './info-window.component.html',
  styleUrls: ['./info-window.component.scss']
})
export class InfoWindowComponent implements OnInit {

  @Input()
  stop: Stop;

  constructor() { }

  ngOnInit() {
  }

}
