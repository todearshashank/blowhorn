import { Component, Input, EventEmitter, Output, OnInit, ViewContainerRef, OnChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Trip } from '../../../shared/models/trip.model';
import { DataService } from '../../../shared/services/data.service';
import { TripsService } from '../../../shared/services/trips.service';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { SimpleChanges } from '@angular/core';
import { LocationStop } from '../../../shared/models/location.model';
import { LocationService } from '../../../shared/services/location.service';
import { Constants } from '../../../shared/utils/constants';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { MapService } from '../../../shared/services/map.service';

declare var google: any;

const AUTO_ZOOMOUT_SECONDS = 60;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {

  dashboardConfig = this.bhService.dashboard_config;
  isConfigPresent = !this.bhService.isObjectEmpty(this.dashboardConfig);

  // MAP CONFIGURATION
  lat: number;
  lng: number;

  bounds: any;
  truck_lat = 0;
  truck_lng = 0;
  truck_marker: any;
  styles = Constants.map_styles;
  map: any;
  zoom = 12;

  // MARKER ICON CONFIGURATION
  truckIcon = null;
  doneMarkerImg = null;
  pendingMarkerImg = null;

  pickupMarkerImg = null;
  pickupMarkerDoneImg = null;

  dropoffMarkerImg = null;
  dropoffMarkerDoneImg = null;

  stopMarkerImg = null;
  stopMarkerDoneImg = null;

  markerIterator: any = [];
  leftSVG = `<svg width='35px' height='35px' viewBox='0 0 40 40' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'> \ <title>RedMarkerWithNumber@1x</title> \ <desc>Created with Sketch.</desc> \ <defs></defs> \ <g id='Assets' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'> \ <g id='Assets-Common' transform='translate(-336.000000, -417.000000)'> \ <g id='RedMarkerWithNumber' transform='translate(336.000000, 417.000000)'> \ <g id='Group' transform='translate(4.000000, 1.000000)'> \ <g id='Shadow' transform='translate(6.600000, 28.826087)' fill='#C9D2D6'> \ <path d='M19.8,4.844634 C19.8,7.51914323 15.3682617,9.68926799 9.90141408,9.68926799 C4.43173832,9.68926799 0,7.51914323 0,4.844634 C0,2.1680055 4.43173832,0 9.90141408,0 C15.3682617,0 19.8,2.1680055 19.8,4.844634' id='Fill-1'></path> \ </g> \ <path d='M10.7047569,32.8376585 L16.5,37.3043478 L22.2952431,32.8376585 C28.5491089,30.4263894 33,24.2256175 33,16.9565217 C33,7.59169337 25.6126984,0 16.5,0 C7.38730163,0 0,7.59169337 0,16.9565217 C0,24.2256175 4.4508911,30.4263894 10.7047569,32.8376585 Z' id='Combined-Shape' fill='#141515'></path> \ <rect id='Rectangle' fill='#D8D8D8' x='6.6' y='6.7826087' width='6.6' height='6.7826087'></rect> \ <ellipse id='Oval-2' fill='#FC3446' cx='16.5' cy='16.9565217' rx='14.85' ry='15.2608696'></ellipse> \ </g> \ <text id='2' font-family='${this.bhService.font_name}' font-size='24' font-weight='bold' letter-spacing='-0.300000012' fill='#FFFFFF'> \ <tspan x='13.2476563' y='26'>`;
  rightSVG = `</tspan> \ </text> \ <rect id='bounds' stroke='#979797' stroke-width='0.01' x='0.005' y='0.005' width='39.99' height='39.99'></rect> \ </g> \ </g> \ </g> \ </svg>`;
  fullSVG: string;
  svgICO: string;
  svgIconArray: any = [];
  getMapData: any;
  distanceData: any;
  markers: any = [];
  trace: any = [];
  routes: any = [];
  allowModification = false;
  disableSaveBtn = false;
  displaySaveButton = false;
  strokeWeight = 4;
  strokeColor = '#056DB4';

  @Input() trip: Trip;
  @Input() scrollwheel = true;
  @Output() distanceUpdate = new EventEmitter<number>();
  @Output() driverPosition = new EventEmitter<LocationStop>();

  firebaseObject: AngularFireObject<any>;
  firebaseDb: AngularFireDatabase;
  lastLocation: any = null;
  lastFitZoomTime: any = null;

  constructor(
    private _dataService: DataService,
    firebaseDb: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    private tripsService: TripsService,
    public toastr: ToastrService,
    private locationService: LocationService,
    private bhService: BlowhornService,
    private map_service: MapService,
  ) {
    this.firebaseDb = firebaseDb;
  }

  ngOnInit() {
    if (this.trip) {
      this.myInit();
      this.allowModification = this.bhService.is_staff && this.trip.status !== 'done';
      this.trace = this.trip.route_info;
      this.startRealTimeUpdates();
    }

    this.truckIcon = this.isConfigPresent ? {
            url: this.dashboardConfig.markers.truck_marker,
            scaledSize: new google.maps.Size(30, 30),
        } : Constants.images.tracking_truck;

    this.doneMarkerImg = this.isConfigPresent ? this.dashboardConfig.markers.stop.icon : Constants.markers.stop_done;
    this.pendingMarkerImg = this.isConfigPresent ? this.dashboardConfig.markers.stop.icon : Constants.markers.stop_pending;

    this.pickupMarkerImg = this.isConfigPresent ? this.dashboardConfig.markers.pickup.icon : Constants.markers.pickup;
    this.pickupMarkerDoneImg = this.isConfigPresent ? this.dashboardConfig.markers.pickup.icon : Constants.markers.pickup_done_big_2x;

    this.dropoffMarkerImg = this.isConfigPresent ? this.dashboardConfig.markers.dropoff.icon : Constants.markers.dropoff;
    this.dropoffMarkerDoneImg = this.isConfigPresent ? this.dashboardConfig.markers.dropoff.icon : Constants.markers.dropoff_done_big_2x;

    this.stopMarkerImg = this.isConfigPresent ? this.dashboardConfig.markers.stop.icon : Constants.markers.stop_big;
    this.stopMarkerDoneImg = this.isConfigPresent ? this.dashboardConfig.markers.stop.icon : Constants.markers.stop_done_big_2x;
  }

  myInit() {
    // Setting up map center location (pickup position)
    this.lat = this.trip.pickup.location.lat;
    this.lng = this.trip.pickup.location.lng;
    this.trip.pickup.icon = this.isConfigPresent ? this.dashboardConfig.markers.pickup.icon : this.pickupMarkerImg;
    this.trip.dropoff.icon = this.isConfigPresent ? this.dashboardConfig.markers.dropoff.icon : this.dropoffMarkerImg;
    this.markers = [];
    this.trip.stops.forEach((obj, index) => {
      this.markerIterator = index + 1;
      this.fullSVG = this.leftSVG + this.markerIterator + this.rightSVG;
      obj.icon = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(this.fullSVG);
      this.markers.push(obj);
    });

    this.markers.forEach((obj: any, index: any) => {
      if (obj.status === 'pending') {
        this.markerIterator = index + 1;
        this.fullSVG = this.leftSVG + this.markerIterator + this.rightSVG;
        this.svgICO = 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(this.fullSVG);
        this.svgIconArray.push(this.svgICO);
      }
    });

    // Making an array with origin and destination coordinates
    for (let i = 0; i < this.markers.length - 1; i++) {
      this.routes.push(
        [[this.markers[i].location.lat], [this.markers[i].location.lng],
          [this.markers[i + 1].location.lat], [this.markers[i + 1].location.lng]]
      );
    }
  }

  getBounds () {
    const bounds: any = new google.maps.LatLngBounds();
    if (this.trip.pickup.location.lat && this.trip.pickup.location.lng) {
      let pickupLoc = new google.maps.LatLng(this.trip.pickup.location.lat, this.trip.pickup.location.lng)
      bounds.extend(pickupLoc);
    }
    if (this.trip.dropoff.location.lat && this.trip.dropoff.location.lng) {
      let dropoffLoc = new google.maps.LatLng(this.trip.dropoff.location.lat, this.trip.dropoff.location.lng)
      bounds.extend(dropoffLoc);
    }
    this.trip.stops.forEach((obj, index) => {
      if (obj.location.lat && obj.location.lng) {
        let stopLoc = new google.maps.LatLng(obj.location.lat, obj.location.lng)
        bounds.extend(stopLoc);
      }
    });
    return bounds;
  }

  fitBounds() {
    this.map.fitBounds(this.getBounds());
  }

  fitBoundsWithDriver(driverLocation: any) {
    const now: any = new Date();
    if (this.lastFitZoomTime == null) {
      const bounds: any = this.getBounds();
      bounds.extend(new google.maps.LatLng(driverLocation.lat, driverLocation.lng));
      this.map.fitBounds(bounds);
      this.lastFitZoomTime = now;
    }
  }
  onMapReady(map: any) {
    this.map = map;
    if (this.trip.status === 'ongoing') {
      if (this.trip.route_info.length > 0) {
        this.truck_marker = new google.maps.Marker({
          position: {
            lat: this.truck_lat,
            lng: this.truck_lng
          },
          map: this.map,
          icon: this.truckIcon
        });
        this.truck_marker.setPosition(this.trip.route_info[this.trip.route_info.length - 1]);
        this.fitBoundsWithDriver(this.trip.route_info[this.trip.route_info.length - 1]);
      } else {
        this.fitBounds();
      }
    } else {
      this.fitBounds();
    }
  }

  startRealTimeUpdates() {
    if (this.trip.status !== 'ongoing') {
      return;
    }

    const city_id = this.trip.city_id;
    const driver_id = this.trip.driver_id;
    this.tripsService.get_firebase_token().subscribe((resp: string) => {});
    const firebase_path = `${Constants.urls.FIREBASE_OCCUPIED_DRIVERS}/${city_id}/${driver_id}`;
    this.firebaseObject = this.firebaseDb.object(firebase_path);
    this.firebaseObject.valueChanges().subscribe(snapshot => {
      console.log('firebase-update: ', snapshot);
      if (snapshot) {
        const data_bundle: any = snapshot;//JSON.parse(snapshot);
        if(!data_bundle) {
          console.log('No data: ', data_bundle);
          return;
        }
        let location = data_bundle.location;
        let isLocationNew = this.locationService.isNewLocationNewer(this.lastLocation, location);
        if (data_bundle != null && isLocationNew) {
          let positionObject: LocationStop = new LocationStop();
          positionObject = LocationStop.get_location_from_data({
            lat: parseFloat(location.mLatitude),
            lng: parseFloat(location.mLongitude),
          });
        //   if (!this.map_service.is_location_in_city(
        //     this.trip.city,
        //     new google.maps.LatLng(positionObject.lat, positionObject.lng),
        //     'dropoff'
        //   ) && !this.map_service.is_location_in_city(
        //     this.trip.city,
        //     new google.maps.LatLng(positionObject.lat, positionObject.lng),
        //     'pickup'
        //   )) {
        //     console.log('Driver location is out of coverage..!', positionObject);
        //     return;
        //   }
          if (!this.truck_marker) {
            this.truck_marker = new google.maps.Marker({
              position: {
                lat: positionObject.lat,
                lng: positionObject.lng
              },
              map: this.map,
              icon: this.truckIcon
            });
            let gLoc = new google.maps.LatLng(positionObject.lat, positionObject.lng)
            this.truck_marker.setPosition(gLoc);
          } else {
            let gLoc = new google.maps.LatLng(positionObject.lat, positionObject.lng)
            this.truck_marker.setPosition(gLoc);
          }
          this.lastLocation = location;
          this.fitBoundsWithDriver(positionObject);
          this.distanceUpdate.emit(isLocationNew[1] / 1000);
          if (this.trip.status === 'done') {
            console.log('Not proceeding. Trip is completed');
            return;
          }
          this.driverPosition.emit(LocationStop.get_location_from_data(positionObject));
          // Push point to polyline only if it is ongoing trip
          if (this.trip.status === 'ongoing' && this.trip.pickup.time !== '') {
            this.trace.push(new google.maps.LatLng(positionObject.lat, positionObject.lng));
          }
        }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.trip = changes.trip.currentValue;
    if (this.trip) {
      this.myInit();
    }
  }

  public markerDragEnd(stop, event) {
    this.displaySaveButton = true;
    stop.location = LocationStop.get_location_from_data(event.coords);
  }

  public updateOrder(id: string, pickupLocation, dropoffLocation, stops: any = []) {
    const data = {
      stops: stops,
      pickup: pickupLocation,
      dropoff: dropoffLocation
    };
    this.disableSaveBtn = true;
    this.tripsService.update_order(id, data).subscribe((res: any) => {
      this.disableSaveBtn = false;
      this.displaySaveButton = false;
      this.toastr.success('Marker locations saved successfully');
    });
  }

}
