import { Component, Input, OnInit } from '@angular/core';
import { Trip } from '../../../shared/models/trip.model';
import { Stop } from '../../../shared/models/stop.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-stops',
  templateUrl: './stops.component.html',
  styleUrls: ['./stops.component.scss']
})
export class StopsComponent implements OnInit {

  @Input() trip: Trip;
  @Input() vertical = false;
  private stops: Stop[];
  public calcs: any;
  constructor () {}

  ngOnInit() {
    this.calcs = this.trip.stops_calculates;
    this.stops = this.trip.stops;
  }

}
