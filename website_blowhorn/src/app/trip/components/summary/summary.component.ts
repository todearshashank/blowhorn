import { Component, Input, OnInit } from '@angular/core';
import { Trip } from '../../../shared/models/trip.model';
import { TripsSummary } from '../../../shared/models/trips-summary.model';
import { TripsService } from '../../../shared/services/trips.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  @Input() trips: Trip[] = [];
  public summary: TripsSummary;

  private firstParamVal = 'trips';
  private firstParamLabel = 'No. of trips';

  private secondParamVal = 'month';
  private secondParamLabel = 'This month';

  private firstOptions: any[] = [
    { value: 'trips', label: 'No. of trips'},
    { value: 'distance', label: 'Distance travelled'},
    { value: 'cost', label: 'Cost'},
  ];
  private secondOptions: any[] = [
    { value: 'today', label: 'Today'},
    { value: 'week', label: 'This week'},
    { value: 'month', label: 'This month'},
    { value: 'year', label: 'This year'},
  ];

  constructor(
    private _tripsService: TripsService,
    private blowhornService: BlowhornService
  ) {
    this._tripsService.get_summary_trips().subscribe((res: any) => {
      this.summary = TripsSummary.get_summary_trips_from_data(res);
    });
  }

  ngOnInit() {}

  selectSecondParam(option: any) {
    this.secondParamVal = option.value;
    this.secondParamLabel = option.label;
  }

  selectFirstParam(option: any) {
    this.firstParamVal = option.value;
    this.firstParamLabel = option.label;
  }

  roundNumber(num: number) {
    return Math.round(100 * num) / 100;
  }
}
