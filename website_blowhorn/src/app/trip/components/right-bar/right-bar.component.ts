import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from '../../../shared/utils/constants';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-right-bar',
  templateUrl: './right-bar.component.html',
  styleUrls: ['./right-bar.component.scss']
})
export class RightBarComponent implements OnInit {

  @Input() truckType: string;
  @Input() truckURL: string;
  @Input() driverName: string;
  @Input() driverMobile: string;
  @Input() horizontal = false;
  @Input() orderId: string;
  @Input() trackingUrl: string;
  @Input() vehicleNumber: string;
  constants = Constants;
  constructor(
    private router: Router,
    public bhService: BlowhornService,
  ) {}

  ngOnInit() {}

  moveToDetails() {
    this.router.navigate(['track', this.orderId]);
  }
}
