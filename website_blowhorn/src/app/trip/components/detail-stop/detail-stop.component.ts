import { Component, Input, OnInit } from '@angular/core';
import { Stop } from '../../../shared/models/stop.model';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Constants } from '../../../shared/utils/constants';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { TripsService } from '../../../shared/services/trips.service';

@Component({
  selector: 'app-detail-stop',
  templateUrl: './detail-stop.component.html',
  styleUrls: ['./detail-stop.component.scss',
      '../../../my-trips/components/current-detail/current-detail.component.scss']
})
export class DetailStopComponent implements OnInit {

  @Input() stop: Stop;
  @Input() lastStop = false;
  @Input() firstStop = false;
  @Input() name: string;
  @Input() index: number;
  @Input() isStop = false;
  public selectedStop: any;
  pickup_done_big = Constants.markers.pickup_done_big_2x;
  stop_done_big = Constants.markers.stop_done_big_2x;
  drop_done_big = Constants.markers.dropoff_done_big_2x;
  pickup_marker = Constants.markers.pickup;
  stop_marker = Constants.markers.stop;
  dropoff_marker = Constants.markers.dropoff;
  contact = Constants.images.contact;
  etaCompleted = false;

  constructor(
    private modalService: NgbModal,
    public bh_service: BlowhornService,
    public tripService: TripsService
  ) {
    this.tripService.is_eta_completed.subscribe(data => {
      if (data) {
        this.etaCompleted = true;
      }
    });
  }

  ngOnInit() {
  }

  public showExtraDetails(extraInformationModal: any, stop: Stop) {
    if (stop.extras && !stop.extras.time) {
      return;
    }
    this.selectedStop = stop;
    this.modalService.open(extraInformationModal).result.then((result) => {
    }, (reason) => {});
  }
}
