import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EnterpriseInvoice } from '../../shared/models/enterprise-invoice.model';
import { Constants } from '../../shared/utils/constants';
import { EnterpriseInvoiceService } from '../../shared/services/enterprise-invoice.service';
import { BlowhornService } from '../../shared/services/blowhorn.service';

@Component({
    selector: 'app-invoice-list-view-grid-row',
    templateUrl: './invoice-list-view-grid-row.component.html',
    styleUrls: ['./invoice-list-view-grid-row.component.scss']
})
export class EnterpriseInvoiceListViewGridRowComponent implements OnInit {

    @Input()
    invoice: EnterpriseInvoice;
    @Input()
    show_actions = false;
    @Input()
    show_auditlog = false;
    @Input()
    styles = {};
    @Output()
    action_clicked: EventEmitter<{invoice: EnterpriseInvoice, type: string}>;
    button = {
        accept: {
            label: 'Approve',
            type: 'proceed'
        },
        reject: {
            label: 'Dispute',
            type: 'delete'
        },
        download: {
            label: 'download',
            type: 'edit'
        }
    };
    show_download_option = false;

    constructor(
        private invoice_service: EnterpriseInvoiceService,
        public blowhornService: BlowhornService,
    ) {
        this.action_clicked = new EventEmitter<{invoice: EnterpriseInvoice, type: string}>();
    }

    ngOnInit() {
    }

    proceed_with_action(action_type: string): void {
        this.action_clicked.emit({invoice: this.invoice, type: action_type});
    }

    download_documents(event: string): void {
        this.invoice_service.download_documents(event, this.invoice.invoice_number);
    }

}
