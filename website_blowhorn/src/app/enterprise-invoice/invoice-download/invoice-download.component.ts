import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
    selector: 'app-invoice-download',
    templateUrl: './invoice-download.component.html',
    styleUrls: ['./invoice-download.component.scss']
})
export class InvoiceDownloadComponent implements OnInit {

    download = {
        invoice: {
            label: 'Download Invoice',
            type: 'edit'
        },
        mis: {
            label: 'Download MIS',
            type: 'edit'
        }
    };
    @Input()
    invoice_number: string;
    @Output()
    click_fired: EventEmitter<string>;
    @Output()
    download_closed: EventEmitter<boolean>;

    constructor() {
        this.click_fired = new EventEmitter<string>();
        this.download_closed = new EventEmitter<boolean>();
    }

    ngOnInit() {
    }

}
