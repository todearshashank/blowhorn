import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IdName } from '../../shared/models/id-name.model';

@Component({
    selector: 'app-invoice-action',
    templateUrl: './invoice-action.component.html',
    styleUrls: ['./invoice-action.component.scss']
})
export class InvoiceActionComponent implements OnInit {
    @Input()
    heading: string;
    @Input()
    details: string;
    @Input()
    show_warning: boolean;
    @Input()
    show_footer = true;
    @Input()
    loader: {
        show: false,
        icon: ''
    };
    @Input()
    action_buttons = {
        decline: {
            show: true,
            type: '',
            label: 'No'
        },
        confirm: {
            show: true,
            type: '',
            label: 'Yes'
        },
    };
    @Input()
    styles = {
        header: {},
        details: {},
        footer: {}
    };
    @Input()
    header_icon_url = '';
    @Input()
    show_loader = false;
    @Input()
    dispute_reasons = [];
    @Input()
    show_reason = false;
    @Input()
    show_comments = false;
    @Output()
    click_fired: EventEmitter<{proceed: boolean, reason: string, comments: string}>;

    selected_reason_name = '';
    selected_reason_id = null;
    other_reason = '';
    show_other_reason = false;
    comments = ''

    constructor() {
        this.click_fired = new EventEmitter<{proceed: boolean, reason: string, comments: string}>();
    }

    ngOnInit() {
        this.other_reason = '';
        this.comments = '';
    }

    get reason_styles(): {} {
        return {
            'dropdown-styles': {
                'box-shadow': 'none',
                'height': '36px',
                'width': '100%',
                'border': '1px solid #ccc',
                'padding-left': '10px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px'
            },
            'dropdown-menu-styles': {
                'width': '100%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
                'text-transform': 'unset',
            }
        };
    }

    reason_changed(event: IdName): void {
        this.selected_reason_name = event.name;
        this.selected_reason_id = event.id;
        this.show_other_reason = false;
        console.log(this.selected_reason_id);
        if (this.selected_reason_id === -1) {
            this.show_other_reason = true;
        }
    }

    submitForm(proceed: boolean): void {
        let reason = this.selected_reason_name;
        if (this.selected_reason_id === -1) {
            reason = this.other_reason;
        }
        this.click_fired.emit({proceed: proceed, reason: reason, comments: this.comments});
    }
}
