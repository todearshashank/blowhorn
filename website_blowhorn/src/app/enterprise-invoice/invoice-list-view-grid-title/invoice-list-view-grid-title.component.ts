import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-invoice-list-view-grid-title',
    templateUrl: './invoice-list-view-grid-title.component.html',
    styleUrls: ['./invoice-list-view-grid-title.component.scss']
})
export class EnterpriseInvoiceListViewGridTitleComponent implements OnInit {

    @Input()
    show_actions = false;
    @Input()
    show_auditlog = false;
    @Input()
    styles = {};
    @Input()
    created_by_title = '';
    @Input()
    created_time_title = '';

    constructor() { }

    ngOnInit() {
    }

}
