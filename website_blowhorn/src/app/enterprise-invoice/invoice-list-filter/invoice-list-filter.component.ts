import { Component, OnInit, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WatcherService } from '../../shared/services/watcher.service';
import { BlowhornService } from '../../shared/services/blowhorn.service';
import { IdName } from '../../shared/models/id-name.model';
import { BsDatepickerConfig, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'app-invoice-list-filter',
    templateUrl: './invoice-list-filter.component.html',
    styleUrls: ['./invoice-list-filter.component.scss']
})
export class EnterpriseInvoiceListFilterComponent implements OnInit {

    @Output('filter_changed')
    filter_changed: EventEmitter<{url: string, searchBox: boolean}>;

    @ViewChild('search_input', {read: ElementRef, static: false})
    searchElem: ElementRef;

    search_value = '';
    date_range: Date = null;
    search_activated = false;
    date_activated = false;
    states: IdName[] = [];
    state_store: IdName[] = [];
    selected_state_id: number = null;
    selected_state_name = 'States';
    state_disabled = false;
    bsConfig?: Partial<BsDatepickerConfig> = {
        dateInputFormat: 'MMM/YYYY',
        rangeInputFormat: 'MMM/YYYY',
        minMode: 'month'
    };
    minMode: BsDatepickerViewMode = 'month';

    constructor(
        private watcher_service: WatcherService,
        public _blowhorn_service: BlowhornService,
    ) {
        this.filter_changed = new EventEmitter<{url: string, searchBox: boolean}>();
        this.watcher_service
            .state_store
            .subscribe(state_store => {
            this.state_store = state_store;
            this.states = this._blowhorn_service.copy_array(this.state_store);
      });
    }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        let search = fromEvent(this.searchElem.nativeElement, 'keyup');
        search.pipe(debounceTime(700)).subscribe(c => this.fire_filter());
    }

    activate_date_clear(): void {
        this.date_activated = true;
    }

    deactivate_date_clear(): void {
        this.date_activated = false;
    }

    reset_date(): void {
        this.date_range = null;
        this.fire_filter()
    }

    get state_styles(): {} {
        return {
          'dropdown-styles': {
            'box-shadow': 'none',
            'border': 'none',
            'height': '30px',
            'width': '200px',
            'z-index': 2
          },
          'dropdown-toggle-styles': {
            'padding-left': '0px',
          },
          'dropdown-menu-styles': {
            'width': '250px',
            boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
          }
        };
      }

    filter_dropdowns(): void {
        this.states = [];

        if (this.selected_state_id) {
            this.states = this.state_store
                .filter(item => {
                    return (this.selected_state_id ? item.id === this.selected_state_id : true);
                });
          } else {
            this.states = [];
            this.states  = this._blowhorn_service.copy_array(this.state_store);
          }
      }

    state_changed(event: IdName): void {
        if (this.selected_state_id !== event.id) {
          this.selected_state_name = event.name;
          this.selected_state_id = event.id;
          this.filter_dropdowns();
        }
        const url = this.get_url();
        this.filter_changed.emit({url: url, searchBox: false});
    }

    fire_filter(): void {
        const url = this.get_url();
        this.filter_changed.emit({url: url, searchBox: false});
    }

    reset_search(): void {
        this.search_value = '';
    }

    reset_disabled(): void {
        this.state_disabled = false;
    }

    reset_state(): void {
        this.selected_state_id = null;
        this.selected_state_name = 'All States';
    }

    activate_search_clear(): void {
        this.search_activated = true;
    }

    deactivate_search_clear(): void {
        this.search_activated = false;
    }

    get_url(): string {
        let url = '';
        url += this.search_value ? `&search=${this.search_value}` : '';
        if (this.date_range) {
            let year = this.date_range.getFullYear();
            let month = this.date_range.getMonth();
            url += `&start=${this.date_range.toISOString().substring(0, 10)}`
            url += `&end=${year}-${month + 1}-${this.lastDay(year, month)}`
        }
        url += this.selected_state_id ? `&state=${this.selected_state_id}` : '';
        return url;
    }

    lastDay(y: number, m: number) {
        return new Date(y, m +1, 0).getDate();
    }

}
