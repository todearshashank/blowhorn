import { Component, OnInit, enableProdMode } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WatcherService } from '../../shared/services/watcher.service';
import { environment } from '../../../environments/environment.prod';
import { IdName } from '../../shared/models/id-name.model';
import { BlowhornService } from '../../shared/services/blowhorn.service';

@Component({
    selector: 'app-invoice-main',
    templateUrl: './invoice-main.component.html',
    styleUrls: ['./invoice-main.component.scss']
})
export class EnterpriseInvoiceMainComponent implements OnInit {

    state_store: IdName[] = [];
    links = [];
    link_styles = {};
    navbar_styles = {};
    constructor(
        private route: ActivatedRoute,
        private watcher_service: WatcherService,
        private bhService: BlowhornService,
    ) {
        this.setNavbarOptions()
        this.route.data
        .subscribe( data => {
            this.state_store = data.trip_filter.states;
            this.watcher_service.update_state_store(this.state_store);
        },
        err => {
            console.error(err);
        });
     }

    ngOnInit() {
    }

    setNavbarOptions() {
        if (this.bhService.hasPermission('invoices', 'can_view_pending_invoices')) {
            this.links.push({ link: 'pending', text: 'Pending' })
        }
        if (this.bhService.hasPermission('invoices', 'can_view_accepted_invoices')) {
            this.links.push({ link: 'accepted', text: 'Accepted' })
        }
        if (this.bhService.hasPermission('invoices', 'can_view_disputed_invoices')) {
            this.links.push({ link: 'disputed', text: 'Disputed' })
        }
        if (this.bhService.hasPermission('invoices', 'can_view_partial_paid_invoices')) {
            this.links.push({ link: 'partially_paid', text: 'Partially Paid' })
        }
        if (this.bhService.hasPermission('invoices', 'can_view_paid_invoices')) {
            this.links.push({ link: 'paid', text: 'Paid' })
        }
    }

}
