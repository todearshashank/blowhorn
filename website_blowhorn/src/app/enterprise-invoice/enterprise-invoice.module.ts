import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { EnterpriseInvoiceMainComponent } from './invoice-main/invoice-main.component';
import { EnterpriseInvoiceListViewComponent } from './invoice-list-view/invoice-list-view.component';
import { EnterpriseInvoiceListFilterComponent } from './invoice-list-filter/invoice-list-filter.component';
import { EnterpriseInvoiceListViewGridTitleComponent } from './invoice-list-view-grid-title/invoice-list-view-grid-title.component';
import { EnterpriseInvoiceListViewGridRowComponent } from './invoice-list-view-grid-row/invoice-list-view-grid-row.component';
import { SharedModule } from '../shared/shared.module';
import { InvoiceListViewGridComponent } from './invoice-list-view-grid/invoice-list-view-grid.component';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';
import { InvoiceActionComponent } from './invoice-action/invoice-action.component';
import { InvoiceDownloadComponent } from './invoice-download/invoice-download.component';


@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    BsDatepickerModule,
    SharedModule,
  ],
  declarations: [EnterpriseInvoiceMainComponent, EnterpriseInvoiceListViewComponent, EnterpriseInvoiceListFilterComponent, EnterpriseInvoiceListViewGridTitleComponent, EnterpriseInvoiceListViewGridRowComponent, InvoiceListViewGridComponent, InvoiceDetailsComponent, InvoiceActionComponent, InvoiceDownloadComponent]
})
export class EnterpriseInvoiceModule { }
