import { Component, OnInit } from '@angular/core';
import { EnterpriseInvoice } from '../../shared/models/enterprise-invoice.model';
import { ActivatedRoute, Router } from '@angular/router';
import { BlowhornService } from '../../shared/services/blowhorn.service';
import { EnterpriseInvoiceService } from '../../shared/services/enterprise-invoice.service';
import { Constants } from '../../shared/utils/constants';

@Component({
  selector: 'app-invoice-list-view-grid',
  templateUrl: './invoice-list-view-grid.component.html',
  styleUrls: ['./invoice-list-view-grid.component.scss']
})
export class InvoiceListViewGridComponent implements OnInit {

    DEFAULT_STATUS = 'pending';
    STATUS_ACCEPTED = 'accepted';
    STATUS_DISPUTED = 'disputed';
    STATUS_PARTIALLY_PAID = 'partially_paid';
    STATUS_PAID = 'paid';

    invoice_list: EnterpriseInvoice[] = [];
    count: number;
    next: string;
    previous: string;
    error_msg = 'No invoices found.';
    current_status = '';
    show_actions = false;
    selected_invoice: EnterpriseInvoice;
    next_available = true;
    prev_available = true;
    current_invoice_index = 0;
    total_invoice_in_page = 0;
    dispute_reasons = [];
    prompt = {
        show: false,
        heading: '',
        details: '',
        show_warning: false,
        show_footer: true,
        show_reason: false,
        show_comments:false,
        action_buttons: {},
        loader: {
            show: false,
            icon: ''
        },
        styles: {}
    };
    styles = {
        header: {
            background: '#fff',
            'text-align': 'center',
            'font-size': '16px',
            'font-weight': 500,
            'line-height': '24px',
        },
        details: {
            'font-size': '20px',
            'font-weight': 300,
            'line-height': '29px',
            'text-align': 'center',
        },
        footer: {
            'height': '45px',
        }
    };
    action_buttons = {
        confirm: {
            show: true,
            label: 'confirm',
            type: 'proceed'
        },
        decline: {
            show: true,
            label: 'cancel',
            type: 'delete'
        }
    };
    grid_styles = {};
    show_details = false;
    show_auditlog = false;
    created_by_title = '';
    created_time_title = '';
    action_type = '';

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        public _blowhorn_service: BlowhornService,
        private invoice_service: EnterpriseInvoiceService,
    ) {
        const route_url = this.route.snapshot.url;
        this.current_status = route_url.length ? route_url[0].path : this.DEFAULT_STATUS;
        this.show_actions = (this.current_status === this.DEFAULT_STATUS);
        this.apply_grid_styles();
        this.set_row_header();
        route.data
        .subscribe(data => {
            this.update_invoice_list(data.invoice);
            this._blowhorn_service.hide_loader();
            this.get_support_data();
        },
        err => {
            console.error(err);
        });
    }

    ngOnInit() {
    }

    set_row_header_title(required: boolean, created_by: string, created_time: string): void {
        this.show_auditlog = required;
        this.created_by_title = created_by;
        this.created_time_title = created_time;
    }

    set_row_header(): void {
        switch (this.current_status) {
            case this.STATUS_DISPUTED:
                this.set_row_header_title(true, 'Disputed By', 'Disputed On');
                break;

            case this.STATUS_ACCEPTED:
                this.set_row_header_title(true, 'Accepted By', 'Accepted On');
                break;

            default:
                this.set_row_header_title(false, '', '');
        }
    }

    apply_grid_styles(): void {
        switch (this.current_status) {
            case this.DEFAULT_STATUS:
                this.grid_styles = {
                    'grid-template-columns': '135px repeat(3, minmax(110px, 120px)) 80px 120px 150px 200px 100px 100px'
                };
                break;

            case this.STATUS_PARTIALLY_PAID:
            case this.STATUS_PAID:
                this.grid_styles = {
                    'grid-template-columns': '135px repeat(auto-fit, minmax(90px, 1fr))'
                };
                break;

            default:
                this.grid_styles = {
                    'grid-template-columns': '135px repeat(3, minmax(90px, 120px)) 80px 120px 150px 200px 100px 100px'
                };
        }
    }

    get_support_data(): void {
        this.invoice_service.get_support_data().subscribe((resp: any) => {
            this.dispute_reasons = resp.reasons_for_dispute;
        },
        err => {
            console.error('Error', err);
        });
    }

    update_invoice_list(data: {count: number, next: string, previous: string, results: any[]}) {
        this.count = data.count;
        this.next = data.next;
        this.previous = data.previous;
        this.invoice_list = data.results;
        this.total_invoice_in_page = this.invoice_list.length;
        this.current_invoice_index = 0;
    }

    filter_changed(event: { url: string }): void {
        this.error_msg = 'Loading Contracts..';
        this.update_invoice_list({
            count: 0,
            next: '',
            previous: '',
            results: []
        });
        this._blowhorn_service.show_loader();
        const url = `${Constants.urls.API_INVOICES}&status=${this.current_status}${event.url}`;
        this.invoice_service.get_invoices(url).subscribe(data => {
            this._blowhorn_service.hide_loader();
            this.update_invoice_list(data);
            if (!data.length) {
                this.error_msg = 'No Invoices found.';
            }
        },
        err => {
            this._blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

    open_details(): void {
        this.show_details = true;
    }

    set_prompt_values(
        heading: string,
        details: string,
        styles: any,
        action_buttons: any
    ): void {
        this.prompt.heading = heading;
        this.prompt.details = details;
        this.prompt.styles = styles;
        this.prompt.action_buttons = action_buttons;
    }

    show_prompt(action_type: string, show_reason: boolean, show_comments: boolean, title: string): void {
        console.log(show_reason, show_comments)
        this.action_type = action_type;
        this.prompt.show_reason = show_reason;
        this.prompt.show_comments = show_comments;
        this.prompt.show_footer = true;
        this.action_buttons.confirm.show = true;
        this.action_buttons.decline.type = 'delete';
        this.action_buttons.decline.label = 'Cancel';
        this.prompt.show = true;
        this.prompt.loader.show = false;
        this.prompt.loader.icon = '';
        this.set_prompt_values(
            title,
            'Invoice #: ' + this.selected_invoice.invoice_number,
            this.styles,
            this.action_buttons
        );
    }

    submit_action(event: {proceed: boolean, reason: number, comments: string}): void {
        if (!event.proceed) {
            this.prompt.show = false;
            return;
        }
        this.prompt.show_reason = false;
        this.prompt.show_comments = false;
        this.prompt.show_footer = false;
        this.prompt.loader.show = true;
        this.prompt.loader.icon = '';
        this.action_buttons.decline.label = 'dismiss';
        let data  = {
            action: this.action_type,
            reason: event.reason || null,
            comments: event.comments || null
        };
        this.invoice_service.update_invoice(this.selected_invoice.id, data).subscribe((resp: any) => {
            this.prompt.loader.icon = 'success';
            this.prompt.show_footer = false;
            this.action_buttons.confirm.show = false;
            this.set_prompt_values(
                resp,
                'Invoice # ' + this.selected_invoice.invoice_number,
                this.styles,
                this.action_buttons
            );
            this.remove_current_invoice();
            this.dismiss_alert();
        },
        err => {
            if (err) {
                this.prompt.loader.icon = 'failure';
                this.action_buttons.confirm.show = false;
                this.prompt.show_footer = true;
                this.set_prompt_values(
                    'Failed',
                    'Invoice # ' + this.selected_invoice.invoice_number,
                    this.styles,
                    this.action_buttons
                );
                this.dismiss_alert();
            }
            console.error('Error', err);
        });
    }

    dismiss_alert(): void {
        setTimeout(() => {
            this.prompt.show = false;
       }, 2000);
    }

    remove_current_invoice(): void {
        this.invoice_list.splice(this.current_invoice_index, 1);
        this.total_invoice_in_page = this.invoice_list.length;
        if (this.total_invoice_in_page === 0) {
            this.show_details = false;
        } else if (this.current_invoice_index >= this.total_invoice_in_page) {
            this.selected_invoice = this.invoice_list[--this.current_invoice_index];
        } else {
            this.selected_invoice = this.invoice_list[this.current_invoice_index];
        }
        this.set_next_available();
        this.set_prev_available();
    }

    get_current_invoice_index(): number {
        return this.invoice_list.findIndex(item => item.id === this.selected_invoice.id);
    }

    set_next_available(): void {
        this.next_available = !(this.current_invoice_index + 1 >= this.total_invoice_in_page);
    }

    set_prev_available(): void {
        this.prev_available = !(this.current_invoice_index <= 0);
    }

    apply_pagination(event: any): void {
        switch (event) {
            case 'prev':
                if (this.current_invoice_index > 0) {
                    this.selected_invoice = this.invoice_list[--this.current_invoice_index];
                    this.set_next_available();
                    this.set_prev_available();
                }
                break;

            case 'next':
                if (this.invoice_list.length > this.current_invoice_index + 1) {
                    this.selected_invoice = this.invoice_list[++this.current_invoice_index];
                    this.set_next_available();
                    this.set_prev_available();
                }
                break;

            default:
                console.log('Invalid action..!');
        }
    }

    perform_action(event: {invoice: EnterpriseInvoice, type: string}): void {
        this.selected_invoice = event.invoice;
        this.current_invoice_index = this.get_current_invoice_index();
        this.set_next_available();
        this.set_prev_available();
        switch (event.type) {
            case 'details':
                this.open_details();
                break;

            case 'accept':
                this.show_prompt(
                    'accept',
                    false,
                    false,
                    'Confirmation pending'
                );
                break;

            case 'dispute':
                this.show_prompt(
                    'dispute',
                    true,
                    true,
                    'Select reason for dispute'
                );
                break;

            default:
                console.error('Invalid Action..!');
        }
    }

}
