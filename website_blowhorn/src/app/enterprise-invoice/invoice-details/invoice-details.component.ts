import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EnterpriseInvoice } from '../../shared/models/enterprise-invoice.model';
import { EnterpriseInvoiceService } from '../../shared/services/enterprise-invoice.service';
import { BlowhornService } from '../../shared/services/blowhorn.service';
import { Constants } from '../../shared/utils/constants';

@Component({
    selector: 'app-invoice-details',
    templateUrl: './invoice-details.component.html',
    styleUrls: ['./invoice-details.component.scss']
})
export class InvoiceDetailsComponent implements OnInit {

    @Input()
    invoice: EnterpriseInvoice;
    @Input()
    show_actions = true;
    @Input()
    next_available = true;
    @Input()
    prev_available = true;
    @Output()
    details_closed: EventEmitter<boolean>;
    @Output()
    action_clicked: EventEmitter<{invoice: EnterpriseInvoice, type: string}>;
    @Output()
    paginator_clicked: EventEmitter<string>;

    DOWNLOAD_ICON = Constants.images.download;
    button = {
        accept: {
            label: 'Approve',
            type: 'proceed'
        },
        reject: {
            label: 'Dispute',
            type: 'delete'
        },
        download_invoice: {
            label: 'Download Invoice',
            type: 'info',
            icon: {
                src: this.DOWNLOAD_ICON
            }
        },
        download_mis: {
            label: 'Download MIS',
            type: 'info',
            icon: {
                src: this.DOWNLOAD_ICON
            },
            icon_styles: {
                icon_holder: {
                    'margin-right': '8px'
                }
            }
        },
    };
    paginator = {
        next: {
            label: 'NEXT',
            type: 'info',
            flat_btn: true,
            icon: {
                class: 'fa fa-angle-right'
            },
            icon_styles: {
                wrapper: {
                    'display': 'flex',
                    'flex-direction': 'row-reverse',
                },
                icon_holder: {
                    'background-color': '#0088A8',
                    'color': '#fff',
                    'height': '36px',
                    'width': '36px',
                    'border-radius': '50%',
                    'text-align': 'center',
                    'line-height': '36px',
                    'vertical-align': 'middle',
                    'margin-left': '10px',
                },
                icon: {
                    'font-size': '36px'
                }
            }
        },
        prev: {
            label: 'PREVIOUS',
            type: 'info',
            flat_btn: true,
            icon: {
                class: 'fa fa-angle-left'
            },
            icon_styles: {
                icon_holder: {
                    'background-color': '#0088A8',
                    'color': '#fff',
                    'height': '36px',
                    'width': '36px',
                    'border-radius': '50%',
                    'text-align': 'center',
                    'line-height': '36px',
                    'vertical-align': 'middle',
                    'margin-right': '10px',
                },
                icon: {
                    'font-size': '36px'
                }
            }
        },
    };

    constructor(
        private invoice_service: EnterpriseInvoiceService,
        public blowhorn_service: BlowhornService
    ) {
        this.action_clicked = new EventEmitter<{invoice: EnterpriseInvoice, type: string}>();
        this.details_closed = new EventEmitter<boolean>();
        this.paginator_clicked = new EventEmitter<string>();
    }

    ngOnInit() {
    }

    proceed_with_action(action_type: string): void {
        this.action_clicked.emit({invoice: this.invoice, type: action_type});
    }

    download_documents(event: string): void {
        this.invoice_service.download_documents(event, this.invoice.invoice_number);
    }

}
