import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveLocationComponent } from './components/save-location/save-location.component';
import {HttpClientModule } from '@angular/common/http';
import { LocationInteractionComponent } from './components/location-interaction/location-interaction.component';
import { LocationsListComponent } from './components/locations-list/locations-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
  ],
  declarations: [
    SaveLocationComponent,
    LocationInteractionComponent,
    LocationsListComponent,
  ],
  exports: [
    SaveLocationComponent,
    LocationInteractionComponent,
    LocationsListComponent
  ]
})
export class LocationModule { }
