import { Component, OnInit } from '@angular/core';
import { UserLocation } from '../../../shared/models/user-location.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TripsService } from '../../../shared/services/trips.service';
import { SidebarService } from '../../../shared/services/sidebar.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-location-interaction',
  templateUrl: './location-interaction.component.html',
  styleUrls: ['./location-interaction.component.scss']
})
export class LocationInteractionComponent implements OnInit {

  public location: UserLocation = new UserLocation();
  public id: string;
  public locationGroup: FormGroup;
  public showMap: boolean = null;
  dich_icon = `static/${this.blowhorn_service.current_build}/assets/Assets/dich.jpg`;
  dich_icon_on_map = `static/${this.blowhorn_service.current_build}/assets/Assets/dich2.jpg`;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _sidebar_service: SidebarService,
    private _trips_service: TripsService,
    private fb: FormBuilder,
    public blowhorn_service: BlowhornService,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    // Component handle both creation and edit of saved location
    if (this.id) {

      this.route.data.subscribe(trip => {
        if (trip.details.length > 0) {
          // this.location = <UserLocation>trip.details;
          this.location = <UserLocation>trip.details.filter((location: { id: number; }) => location.id === +this.id)[0];
          if (!this.location) { this.goToLocations(); }

          this._sidebar_service.show_sidebar = false;
          this.initForm();
        } else {
          this.goToLocations();
        }
      });

    } else {
      this.location = new UserLocation();
      this._sidebar_service.show_sidebar = false;
      this.initForm();
    }
  }

  toggleMap() {
    this.showMap = !this.showMap;
  }

  initForm() {
    this.locationGroup = this.fb.group({
      name: [this.location.name, [Validators.required]],

      // TODO: backend doesn't have flat number / building name need to add
      flat: ['', [Validators.required]],
      line: [this.location.address.line, [Validators.required]],
      city: [this.location.address.city, [Validators.required]],
      code: [this.location.address.postal_code, [Validators.required]],
      contactName: [this.location.contact.name, [Validators.required]],
      contactNumber: [this.location.contact.mobile, [Validators.required]],
    });
  }

  // need to call when user click on map to new address
  // (create new component for this with map),
  // create new UserLocation object there pass it to emit output which call this method
  updateLocationValue(location: UserLocation) {
    this.location = location;
    this.locationGroup.reset();
    this.initForm();
  }

  goToLocations() {
    this._sidebar_service.show_sidebar = true;
    this.router.navigate(['/locations']);
  }

  onSubmit() {
    const value = this.locationGroup.value;
    this.location.name = value['name'];
    // Uncomment it when will be on backend
    // this.location.address.flat = value['flat'];
    this.location.address.line = value['line'];
    this.location.address.city = value['city'];
    this.location.address.postal_code = value['code'];
    this.location.contact.name = value['contactName'];
    this.location.contact.mobile = value['contactNumber'];

    // TODO: backend is not ready yet for this
    // remove debuggers when backend will be done
    if (this.id) {
      this._trips_service.update_location(this.location).subscribe(resp => {
        this.goToLocations();
      });
    } else {
      this._trips_service.create_location(this.location).subscribe(resp => {
        this.goToLocations();
      });
    }
  }
}
