import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../../../shared/utils/constants';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { SavedLocation } from '../../../shared/models/saved-location.model';

@Component({
  selector: 'app-save-location',
  templateUrl: './save-location.component.html',
  styleUrls: ['./save-location.component.scss']
})
export class SaveLocationComponent implements OnInit {
  contact_title = 'Contact person for this location';
  @Input()
  heading: string;
  @Input()
  contact_name: string;
  @Input()
  contact_mobile: number;
  @Input()
  location_name: string;
  @Input()
  latitude: number;
  @Input()
  longitude: number;
  @Input()
  full_address: string;
  @Input()
  area: string;
  @Input()
  city: string;
  @Input()
  postal_code: string|number;
  @Input()
  is_saved_location: boolean;
  @Output()
  form_close: EventEmitter<boolean>;
  @Output()
  form_save: EventEmitter<SavedLocation>;
  regex = Constants.regex;
  isdCodeStyles = {
    wrapper: {
      margin: '10px 0px',
      height: 'unset',
      width: '40px',
      padding: '5px 4px 0px 4px'
    },
    text: {
      'margin-bottom': '3px'
    }
  };
  

  constructor(
    private _watcher_service: WatcherService,
    private _http_client: HttpClient,
    public _blowhorn_service: BlowhornService,
  ) {
    this.form_close = new EventEmitter<boolean>();
    this.form_save = new EventEmitter<SavedLocation>();
  }

  ngOnInit() {
    this.contact_mobile = this._blowhorn_service.get_national_number(this.contact_mobile);
  }

  close_form() {
    this.clear_form();
    this.form_close.emit(true);
  }

  save_address() {
    const http_header = {
      headers: {
        'X-CSRFToken': this._blowhorn_service.getCookieValue('csrftoken')
      }
    };
    this._http_client
      .post(
        Constants.urls.API_SAVE_LOCATION,
        {
          'name': this.location_name,
          'contact_name': this.contact_name,
          'contact_mobile': this.contact_mobile,
          'lat': this.latitude,
          'lon': this.longitude,
          'full_address': this.full_address,
          'area': this.area,
          'city': this.city,
          'postal_code': this.postal_code
        },
        http_header
      ).subscribe((locations: SavedLocation[]) => {
        this._blowhorn_service.update_saved_locations(locations);
        this.form_save.emit(locations[locations.length - 1]);
      });
  }

  clear_form() {
    this.heading = '';
    this.contact_name = '';
    this.contact_mobile = null;
    this.location_name = '';
    this.latitude = null;
    this.longitude = null;
    this.full_address = '';
    this.area = '';
    this.city = '';
    this.postal_code = null;
    this._watcher_service
      .update_show_save_address(false);
  }

}
