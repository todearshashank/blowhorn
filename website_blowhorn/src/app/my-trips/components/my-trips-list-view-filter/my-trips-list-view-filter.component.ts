import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { IdName } from '../../../shared/models/id-name.model';
import { City } from '../../../shared/models/city.model';
import { Driver } from '../../../shared/models/driver.model';
import { Constants } from '../../../shared/utils/constants';
import { TripService } from '../../../shared/services/trip.service';
import { Trip } from '../../../shared/models/trips-grid.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { MatDialog } from '@angular/material/dialog';
import { OndemandOrderRequestDailog } from '../ondemand-order-request/ondemand-order-request.component';

declare var trip_id: number;

@Component({
  selector: 'app-my-trips-list-view-filter',
  templateUrl: './my-trips-list-view-filter.component.html',
  styleUrls: ['./my-trips-list-view-filter.component.scss']
})
export class MyTripsListViewFilterComponent implements OnInit {

  @Output('filter_changed')
  filter_changed: EventEmitter<{ url: string, searchBox: boolean }>;
  @Output('search_fired')
  search_fired: EventEmitter<void>;
  @Output('export_fired')
  export_fired: EventEmitter<void>;
  @Output('download_template_fired')
  download_template_fired: EventEmitter<void>;
  @Output()
  new_trips_created: EventEmitter<{ trips: Trip[]}>;
  previous: string;
  date_range: [Date, Date] = [null, null];
  order_trip_number = '';
  drivers: Driver[] = [];
  cities: City[] = [];
  hubs: IdName[] = [];
  vehicle_types: IdName[] = [];
  driver_store: Driver[] = [];
  city_store: City[] = [];
  hub_store: IdName[] = [];
  contract_type_store: any[] = [];
  contract_types: IdName[] = [];
  vehicle_type_store: IdName[] = [];
  selected_city_id: number = null;
  selected_city_name = 'All Cities';
  selected_hub_id: number = null;
  selected_hub_name = 'All Hubs';
  selected_vehicle_type_id: number = null;
  selected_vehicle_type_name = 'All Vehicle Types';
  selected_driver_id: number = null;
  selected_driver_name = 'All Drivers';
  selected_status_id: number = null;
  selected_status_name = 'All Status';
  selected_contract_type_id: number = null;
  selected_contract_type_name = 'All Trip Types';
  city_disabled = false;
  hub_disabled = false;
  vehicle_type_disabled = false;
  driver_disabled = false;
  status_disabled = false;
  date_activated = false;
  order_activated = false;
  FILTER_TODAY = 0;
  FILTER_LAST_7_DAYS = 7;
  FILTER_LAST_30_DAYS = 30;
  quickFilters = [
    { label: 'Today', val: this.FILTER_TODAY },
    { label: 'Last 7 Days', val: this.FILTER_LAST_7_DAYS },
    { label: 'Last 30 Days', val: this.FILTER_LAST_30_DAYS },
  ];
  status_list = [
                  {'id' : 1 , 'name' : 'current'},
                  {'id' : 2 , 'name' : 'upcoming'},
                  {'id' : 3, 'name' : 'completed'},
                ];
  selected_quick_filter = this.FILTER_TODAY;
  show_request_order_modal = false;

  constructor(
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    public dialog: MatDialog,
  ) {
    this.filter_changed = new EventEmitter<{ url: string, searchBox: false }>();
    this.search_fired = new EventEmitter<void>();
    this.export_fired = new EventEmitter<void>();
    this.new_trips_created = new EventEmitter<{ trips: Trip[]}>();
    this.watcher_service
      .city_store
      .subscribe(city_store => {
        this.city_store = city_store;
        this.cities = this._blowhorn_service.copy_array(this.city_store);
      });
    this.watcher_service
      .hub_store
      .subscribe(hub_store => {
        this.hub_store = hub_store;
        this.hubs = this._blowhorn_service.copy_array(this.hub_store);
      });
    this.watcher_service
      .vehicle_type_store
      .subscribe(vehicle_type_store => {
        this.vehicle_type_store = vehicle_type_store;
        this.vehicle_types = this._blowhorn_service.copy_array(this.vehicle_type_store);
      });
    this.watcher_service
      .driver_store
      .subscribe(driver_store => {
        this.driver_store = driver_store;
        this.drivers = this._blowhorn_service.copy_array(this.driver_store);
      });
    this.watcher_service
      .contract_types
      .subscribe(val => {
        this.contract_type_store = val;
        console.log('val', val);
        for (let i = 0; i < val.length; i++) {
            let p = new IdName();
            p.id = i + 1;
            p.name = val[i][0];
            this.contract_types.push(p);
        }
      });
  }

  ngOnInit() {
  }

  activate_date_clear(event): void {
    this.date_activated = true;
  }

  deactivate_date_clear(event): void {
    this.date_activated = false;
  }

  activate_order_clear(event): void {
    this.order_activated = true;
  }

  deactivate_order_clear(event): void {
    this.order_activated = false;
    this.order_changed(event);
  }

  reset_order(event): void {
    this.order_trip_number = '';
    this.order_changed('reset');
  }

  reset_date(event): void {
    this.date_range = [null, null];
    this.date_changed(event);
  }

  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '150px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get hub_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '200px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get vehicle_type_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '150px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get driver_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '200px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get status_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '150px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        'width': '100px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get contract_type_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '120px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        'width': '150px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  city_changed(event: { name: string, id: number }): void {
    if (this.selected_city_id !== event.id) {
      this.selected_city_name = event.name;
      this.selected_city_id = event.id;
      this.reset_hub();
      this.reset_vehicle_type();
      this.reset_driver();
      this.filter_dropdowns();
    }
    const url = this.get_url();
    this.filter_changed.emit({ url: url, searchBox: false });
  }

  hub_changed(event: { name: string, id: number }): void {
    if (this.selected_hub_id !== event.id) {
      this.selected_hub_name = event.name;
      this.selected_hub_id = event.id;
      this.reset_vehicle_type();
      this.reset_driver();
      this.filter_dropdowns();
    }
    const url = this.get_url();
    this.filter_changed.emit({ url: url, searchBox: false });
  }

  vehicle_type_changed(event: { name: string, id: number }): void {
    if (this.selected_vehicle_type_id !== event.id) {
      this.selected_vehicle_type_name = event.name;
      this.selected_vehicle_type_id = event.id;
      this.reset_driver();
      this.filter_dropdowns();
    }
    const url = this.get_url();
    this.filter_changed.emit({ url: url, searchBox: false });
  }

  driver_changed(event: { name: string, id: number }): void {
    if (this.selected_driver_id !== event.id) {
      this.selected_driver_name = event.name;
      this.selected_driver_id = event.id;
      this.filter_dropdowns();
    }
    const url = this.get_url();
    this.filter_changed.emit({ url: url, searchBox: false });
  }

  status_changed(event: { name: string, id: number }): void {
    if (this.selected_status_id !== event.id) {
      this.selected_status_name = event.name;
      this.selected_status_id = event.id;
    }
    const url = this.get_url();
    this.filter_changed.emit({ url: url, searchBox: false });
  }

  contract_type_changed(event: { name: string, id: number }): void {
    if (this.selected_contract_type_id !== event.id) {
        this.selected_contract_type_name = event.name;
        this.selected_contract_type_id = event.id;
      }
      const url = this.get_url();
      this.filter_changed.emit({ url: url, searchBox: false });
  }

  date_changed(event): void {
    const url = this.get_url();
    this.selected_quick_filter = null;
    this.filter_changed.emit({ url: url, searchBox: false });
  }

  order_changed(event) {
    const url = this.get_url();
    this.filter_changed.emit({ url: url, searchBox: event != 'reset' });
  }

  filter_dropdowns(): void {
    this.hubs = [];
    this.drivers = [];
    if (this.selected_city_id) {
      const city = this.city_store
        .filter(item => item.id === this.selected_city_id)[0];
      for (const hub of city.hubs) {
        this.hubs.push(hub);
      }
    } else {
      this.hubs = [];
      this.hubs = this._blowhorn_service.copy_array(this.hub_store);
    }

    this.drivers = this.driver_store
      .filter(item => {
        return (this.selected_city_id ? item.city === this.selected_city_id : true) &&
          (this.selected_hub_id ? item.hub === this.selected_hub_id : true) &&
          (this.selected_vehicle_type_id ? item.vehicle_class === this.selected_vehicle_type_id : true);
      });
  }

  reset_city(): void {
    this.selected_city_id = null;
    this.selected_city_name = 'All Cities';
  }

  reset_hub(): void {
    this.selected_hub_id = null;
    this.selected_hub_name = 'All Hubs';
  }

  reset_vehicle_type(): void {
    this.selected_vehicle_type_id = null;
    this.selected_vehicle_type_name = 'All Vehicle Types';
  }

  reset_driver(): void {
    this.selected_driver_id = null;
    this.selected_driver_name = 'All Drivers';
  }

  reset_status(): void {
    this.selected_status_id = null;
    this.selected_status_name = '';
  }

  reset_disabled(): void {
    this.city_disabled = false;
    this.hub_disabled = false;
    this.vehicle_type_disabled = false;
    this.driver_disabled = false;
    this.status_disabled = false;
  }

  applyQuickFilter(val: number): void {
    if (val === this.selected_quick_filter) {
      return;
    }
    this.selected_quick_filter = val;
    this.date_range = [null, null];
    const url = this.get_url();
    this.filter_changed.emit({ url: url, searchBox: false });
  }

  search() {
    this.search_fired.emit();
  }

  export() {
    this.export_fired.emit();
  }

  download_template() {
    window.open(
      `${Constants.urls.API_TRIP_STOP_TEMPLATE_EXPORT}`,
      '_blank'
    );
  }

  get_url(): string {
    let url = '';
    url += this.selected_city_id ? `&city_id=${this.selected_city_id}` : '';
    url += this.selected_hub_id ? `&hub_id=${this.selected_hub_id}` : '';
    url += this.selected_vehicle_type_id ? `&vehicle_class=${this.selected_vehicle_type_id}` : '';
    url += this.selected_driver_id ? `&driver_id=${this.selected_driver_id}` : '';
    // url += this.selected_status_name ? `&status=${this.selected_status_name}` : '';
    url += this.date_range[0] ? `&start=${this.date_range[0].toISOString().substring(0, 10)}` : '';
    url += this.date_range[1] ? `&end=${this.date_range[1].toISOString().substring(0, 10)}` : '';
    url += this.order_trip_number ? `&order_trip_no=${this.order_trip_number}` : '';
    url += this.selected_contract_type_id ? `&trip_type=${this.selected_contract_type_name}` : '';
    if (this.selected_status_id) {
      url += this.selected_status_name ? `&status=${this.selected_status_name}` : '';
    };
    url += !this.date_range[0] && this.selected_quick_filter !== null ? `&quick_filter=${this.selected_quick_filter}` : '';
    return url;
  }

  open_request_order() {
    const formDialogRef = this.dialog.open(OndemandOrderRequestDailog, {
        width: '30%',
        data: {
            vehicle_classes: this.vehicle_types,
            cities: this.cities,
            city_store: this.city_store,
            hubs: this.hub_store
        }
    });

    formDialogRef
        .afterClosed()
        .subscribe(result => {
            if (result) {
                this.new_trips_created.emit({
                    trips: result
                });
            } else{
                console.log("Cancelled");
            }
        }
    )
  }

}
