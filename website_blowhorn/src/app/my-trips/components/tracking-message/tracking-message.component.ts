import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-tracking-message',
  templateUrl: './tracking-message.component.html',
  styleUrls: ['./tracking-message.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrackingMessageComponent implements OnInit {

  @Input()
  order:any

  showAbleToTrackMessage = true;

  pendingMessage = ''
  datetime = ''
  end_customer = ''

  message = ''
  messageMap = {
    'Out-For-Pickup': '<div class="message">Your order <div class="order-number">ORDER_NUMBER</div> is out for pickup.</div>',
    'Picked': '<div class="message">Your order <div class="order-number">ORDER_NUMBER</div> is picked up and will soon be out for delivery.</div>',
    'Unable-To-Deliver': 'Delivery was attempted on DATETIME and we were unable to deliver due to REASON. We will attempt to deliver soon and you can track it once it is out for delivery',
    'Lost': 'This package is lost in transition. Please contact our customer care.',
    'Returned': 'Your ORDER_NUMBER has been returned to CUSTOMER on DATETIME.',
    'Rejected': 'Your ORDER_NUMBER has been rejected by CUSTOMER due to REASON on DATETIME.'
  }

  constructor() { }

  ngOnInit() {
    this.showAbleToTrackMessage = Constants.shipmenStatusMapping.pending.includes(this.order.detail_status)

    switch(this.order.detail_status) {
      case 'booking_accepted':
      case 'driver_accepted':
      case 'Accepted':
      case 'Assigned':
      case 'At-DropOff':
      case 'At-Hub':
      case 'At-Hub-RTO':
      case 'Moving-To-Hub':
      case 'New-Order':
      case 'Order Pending':
      case 'Picked':
      case 'Pickup-Failed':
        this.message = `<div class="message">Your order <div class="order-number">${this.order.order_id}</div> is awaited from <div class="customer-name">${this.order.customer_name}</div></div>`
        break;

      case 'Dummy':
      case 'Suspended':
      case 'Expired':
        this.message = `<div class="message">Order number ${this.order.order_id} not found</div>`
        break;
      
      case 'Lost':
        this.message = '<div class="message">This package is lost in transition. Please contact our customer care.</div>'
        break;

      case 'Moving-To-Hub-RTO':
      case 'Returned-To-Hub':
      case 'Returned-To-Origin-Failed':
        this.message = `Your order <div class="order-number">${this.order.order_id}</div> has been picked from you and the return is initiated.`
        break;

      case 'Returned-To-Origin':
        this.message = `Your order <div class="order-number">${this.order.order_id}</div> has been returned to <div class="order-number">${this.order.customer_name}</div> on ${this.order.event_date}.`
        break;

      case 'Rejected':
      case 'Returned':
        let end_customer = this.order.dropoff && this.order.dropoff.contact ? this.order.dropoff.contact.name : ''
        this.message = `Your order <div class="order-number">${this.order.order_id}</div> has been rejected by <div class="order-number">${end_customer}</div> on ${this.order.event_date}.`
        break;

      case 'Cancelled':
        this.message = `This order has been cancelled by <div class="highlight-text">${this.order.customer_name}</div>. Request you to contact <div class="customer-name">${this.order.customer_name}</div> customer care.`
        break;

      case 'Unable-To-Deliver':
        this.message = `Delivery was attempted on <div class="highlight-text">${this.order.event_date}</div> and we were unable to deliver due to <div class="highlight-text">${this.order.event_remarks}</div>. <br/> We will attempt to deliver soon and you can track it once it is out for delivery`
        break

      default:
        console.log('LP', this.order, this.order.detail_status)
        this.message = this.messageMap[this.order.detail_status].replace('ORDER_NUMBER', this.order.order_id).replace('CUSTOMER', this.order.customer_name)
    }
  }

}
