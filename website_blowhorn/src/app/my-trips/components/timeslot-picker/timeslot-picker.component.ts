import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { Trip } from '../../../shared/models/trip.model';
import { TripsService } from '../../../shared/services/trips.service';

// declare var moment;

@Component({
  selector: 'app-timeslot-picker',
  templateUrl: './timeslot-picker.component.html',
  styleUrls: ['./timeslot-picker.component.scss']
})
export class TimeslotPickerComponent implements OnInit {

  @Input() trip: Trip;
  @Output() details_closed: EventEmitter<boolean>;
  TIMEFRAME_NOW = 'now';
  TIMEFRAME_LATER = 'later';
  MAX_ADVANCE_BOOKING_DAYS = 14;
  MAX_CALENDER_DAYS_TO_SHOW = 21;
  show_loader = false;
  error_message = '';
  reschedule_error = '';
  reschedule_message = '';
  min_loader_styles = {};
  selected_timeframe = '';
  selected_datetime = null;
  slot_data_available = true;
  max_advance_booking_days = 0;
  selected_day = null;
  timeslot_json = {};
  start_minute = 0;
  end_minute = 0;
  days_array = [];
  timeslots = [];
  showSlots = false;
  noSlots = false;
  nowNotAvailable = false;
  modelTimeSlot = null;
  today = null;
  selected_time = null;
  selected_hour = null;
  selected_minute = null;
  availability_end = null;
  disableSubmit = true;
  timepicker_view = 'hours';
  timepickerButtonStyles = {
    wrapper: {
      border: '1px solid var(--primary-color)',
      'background-color': 'var(--primary-color)',
    }
  }
  pickerConfig = {
    start: '',
    end: '',
    startStr: '',
    endStr: '',
    defaultTime: '',
    minutesGap: 5,
    format: 24
  }
  constructor(
    public trip_service: TripsService,
  ) {
    this.details_closed = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.fetch_timeslots();
  }

  fetch_timeslots() {
    let location = this.trip.pickup.location;
    this.show_loader = true;
    this.trip_service.get_timeslot_details(location.lat, location.lng)
    .subscribe((resp: any) => {
      this.timeslot_json = resp.slots;
      this.max_advance_booking_days = resp.max_advance_booking_days;
      this.start_minute = resp.start_minute;
      this.end_minute = resp.end_minute;
      this.selectTimeframe(this.TIMEFRAME_NOW);
      this.today = new Date();
      this.show_loader = false;
    }, err => {
      this.show_loader = false;
      this.error_message = err.error;
      console.error(err);
    });
  }

  dayIsAvailable(day: any) {
    return day.isBetween(this.today, this.availability_end, 'day', '[]');
  }

  isTimeFrameSelected(timeframe: any) {
    return this.selected_timeframe === timeframe;
  }

  isSlotActive(time: any) {
    return this.selected_time === time;
  };

  isValidForm() {
    return (this.selected_timeframe === this.TIMEFRAME_NOW && !this.nowNotAvailable) ||
      (this.selected_timeframe === this.TIMEFRAME_LATER && this.selected_day && this.selected_time);
  }

  getTimeBox(day: any) {
    let delta = day.diff(this.today, 'days');
    var now = new Date();
    var this_hour = now.getHours();
    if (now.getMinutes() > 59) {
      this_hour = this_hour + 1;
    }
    var coming_hour = this_hour + 1;
    var this_slots = this.timeslot_json[delta];
    var atleast_one = false;
    this.timeslots = [];
    if (this_slots.length) {
      this_slots.forEach((value: any, index: number) => {
        if (delta === 0 && value.t <= this_hour) {
          value.status = 'disabled';
        } else {
          if (value.status !== 'disabled' && value.status !== '') {
            atleast_one = true;
            this.timeslots.push(value.t);
          }
        }
      });
    }
    if (atleast_one) {
      this.noSlots = false;
      this.showSlots = true;
    } else {
      this.noSlots = true;
      this.showSlots = false;
    }
    if (this.timeslots.length) {
      this.pickerConfig.start = `${this.timeslots[0]}:${this.start_minute}`;
      this.pickerConfig.end = `${this.timeslots[this.timeslots.length-1]}:${this.end_minute}`;
      this.pickerConfig.startStr = moment().hour(
        this.timeslots[0]
      ).minute(
        this.start_minute
      ).format('HH:mm');
      this.pickerConfig.endStr = moment().hour(
        this.timeslots[this.timeslots.length-1]
      ).minute(
        this.end_minute
      ).format('HH:mm');
      this.pickerConfig.defaultTime = null;
    }
  }

  selectTimeframe(timeframe: string) {
    if (this.selected_timeframe === timeframe) {
      return;
    }
    this.selected_timeframe = timeframe;
    this.resetDatetime();
    let now = new Date();
    switch (timeframe) {
      case this.TIMEFRAME_NOW:
        this.nowNotAvailable = true;
        this.noSlots = false;
        this.showSlots = false;
        let this_hour_orig = now.getHours();
        let this_hour = this_hour_orig;
        if (now.getMinutes() > 59) {
          this_hour = (this_hour + 1) % 24;
        }
        var coming_hour = (this_hour + 1) % 24;
        var today_slots = this.timeslot_json[0];
        if (today_slots) {
          if (this_hour === 23 || this_hour_orig === 23) {
            // For post 10:30 PM load tomorrow's slots
            today_slots = this.timeslot_json[1];
          }
          today_slots.map((value: any) => {
            if (value.t === coming_hour && value.status === 'enabled') {
              this.nowNotAvailable = false;
              this.disableSubmit = !this.isValidForm();
              return false;
            }
          });
        }
        break;

      case this.TIMEFRAME_LATER:
        this.nowNotAvailable = false;
        this.showSlots = true;
        this.selected_time = null;
        this.today = moment().hour(0).minute(0).second(0).millisecond(0);
        this.availability_end = this.today.clone().add(this.max_advance_booking_days - 1, 'days');
        var show_start = this.today.clone().subtract(1, 'days').startOf('week').add(1, 'days');
        var show_end = show_start.clone().add(this.MAX_CALENDER_DAYS_TO_SHOW, 'days');
        this.days_array = [];
        for (var i = show_start; i < show_end; i = i.clone().add(1, 'days')) {
          this.days_array.push(i);
        }
        if (this.selected_day && !this.timeslots.length) {
          this.noSlots = true;
          this.showSlots = false;
        } else {
          this.noSlots = false;
          this.showSlots = true;
        }
        break;
    }
  }

  selectDay(day: any) {
    this.resetTime();
    if (this.dayIsAvailable(day)) {
      this.selected_day = day;
      this.getTimeBox(day);
    }
  }

  setTimeslot(event: any) {
    this.selected_time = event;
    this.disableSubmit = !this.isValidForm();
  }

  selectPickerView() {
    this.timepicker_view = this.timepicker_view === 'hours' ? 'minutes' : 'hours';
  }

  resetDatetime() {
    this.selected_day = null;
    this.resetTime();
  }

  resetTime() {
    this.selected_time = null;
    this.selected_hour = null;
    this.selected_minute = null;
  }

  close_modal(event: any) {
    this.details_closed.emit(true);
  }

  submit() {
    let data = {
      order_number: this.trip.order_id,
      reschedule_details: {
        now_or_later: this.selected_timeframe,
        later_value: this.selected_day && this.selected_time ?
        `${this.selected_day.format('DD MMM YYYY')} ${this.selected_time}` : ''
      }
    }
    this.show_loader = true;
    this.reschedule_error = '';
    this.trip_service.reschedule_booking(data)
    .subscribe((resp: any) => {
      this.show_loader = false;
      this.reschedule_message = resp;
      this.trip_service.refresh_trips();
    }, err => {
      this.show_loader = false;
      this.reschedule_error = err.error.detail || err.error;
      console.error(err);
    });
  }

  closeMessage(event: any) {
    if (this.reschedule_error) {
      this.reschedule_error = null;
    } else {
      this.reschedule_message = null;
      this.close_modal(event);
    }
  }
}
