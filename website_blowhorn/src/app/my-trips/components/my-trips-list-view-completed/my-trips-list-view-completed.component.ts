import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Trip } from '../../../shared/models/trips-grid.model';
import { TripService } from '../../../shared/services/trip.service';
import { WatcherService } from '../../../shared/services/watcher.service';
import { Constants } from '../../../shared/utils/constants';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { BlowhornPaginationComponent } from '../../../shared/components/blowhorn-pagination/blowhorn-pagination.component';
import { ShipmentService } from '../../../shared/services/shipment.service';
import { AdditionalEntityDetails } from '../../../shared/models/assets.model';

@Component({
  selector: 'app-my-trips-list-view-completed',
  templateUrl: './my-trips-list-view-completed.component.html',
  styleUrls: ['./my-trips-list-view-completed.component.scss']
})
export class MyTripsListViewCompletedComponent implements OnInit {

  @ViewChild('pagination', {static: false})
  pagination: BlowhornPaginationComponent;
  @ViewChild('overlay_panel_details', {static: false})
  panel_details: ElementRef;
  trips: Trip[];
  url = Constants.urls.API_COMPLETED_TRIPS;
  export_url = Constants.urls.API_EXPORT_COMPLETED_TRIPS;
  filter_url = '';
  error_msg = 'No records found';
  count: number;
  next: string;
  previous: string;
  show = {
    details: false
  };
  selected_trip: Trip;
  show_documents = false;
  document_details = [];

  constructor(
    private route: ActivatedRoute,
    private trip_service: TripService,
    private _shipment_service: ShipmentService,
    public _blowhorn_service: BlowhornService,
  ) {
    route.data
      .subscribe(data => {
        const completed_trips_data = data.completed_trips;
        this.update_trips(completed_trips_data);
        this._blowhorn_service.hide_loader();
        this.error_msg = 'No records found';
      },
      err => {
        console.error(err);
        this._blowhorn_service.hide_loader();
        this.error_msg = 'Something went wrong.';
      });
  }

  ngOnInit() {
  }

  filter_changed(event: {url: string, searchBox: boolean}) {
    this.filter_url = event.url;
    this.url = Constants.urls.API_COMPLETED_TRIPS;
    this.export_url = `${Constants.urls.API_EXPORT_COMPLETED_TRIPS}${this.filter_url}`;
    if (!event.searchBox) {
      this.search();
    }
  }

  update_trips(data: {count: number, next: string, previous: string, results: any[]}) {
    this.count = data.count;
    this.next = data.next;
    this.previous = data.previous;
    this.trips = data.results;
  }

  search() {
    this.trips = [];
    this.update_trips({
      count: 0,
      next: '',
      previous: '',
      results: []
    });
    this._blowhorn_service.show_loader();
    this.url += this.filter_url ? this.filter_url : '';
    this.filter_url = '';
    this.trip_service
      .get_completed_trips(this.url)
      .subscribe(data => {
        this.update_trips(data);
        this._blowhorn_service.hide_loader();
        this.error_msg = 'No records found';
      },
      err => {
        console.error(err);
        this._blowhorn_service.hide_loader();
        this.error_msg = 'Something went wrong.';
      });
  }

  export() {
     window.open(this.export_url);
  }

  open_details(event: Trip): void {
    this.selected_trip = event;
    this._blowhorn_service.show_loader();
    this._shipment_service
        .get_orderlines('trip_number', event.trip_number)
        .subscribe(data => {
            this._shipment_service.update_selected_entity_details(
                new AdditionalEntityDetails(
                    event.status,
                    null,
                    0.00,
                    0.00,
                    0.00,
                    data.results,
                    event.otp ? event.otp.toString() : '--NA--'
                )
            );
            this._blowhorn_service.hide_loader();
            this.show.details = true;
          },
          err => {
            console.error(err);
        });
  }

  focus_details_panel(event: any) {
    this.panel_details.nativeElement.focus();
  }

  close_details_fired(event: any) {
    if (event instanceof KeyboardEvent
        && event.keyCode !== 27) {
      return;
    }
    this.show.details = false;
  }

  show_document_gallery(event: any) {
    this._blowhorn_service.show_loader();
    this.trip_service
      .get_pod_details(event.id).subscribe(data => {
        this.document_details = data;
        this.show_documents = true;
        this._blowhorn_service.hide_loader();
      },err => {
        console.log(err);
        this.show_documents = true;
      });
  }
}
