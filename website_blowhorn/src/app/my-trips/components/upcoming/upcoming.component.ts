import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Trip } from '../../../shared/models/trip.model';
import { ActivatedRoute } from '@angular/router';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornPaginationComponent } from '../../../shared/components/blowhorn-pagination/blowhorn-pagination.component';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { TripsService } from '../../../shared/services/trips.service';
import { TripService } from '../../../shared/services/trip.service';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.scss']
})
export class UpcomingComponent implements OnInit {

  @ViewChild('pagination', {static: false})
  pagination: BlowhornPaginationComponent;
  trips: Trip[];
  count: number;
  next: string;
  previous: string;
  show_details = false;
  selected_trip: Trip;
  constructor(
    private route: ActivatedRoute,
    private _watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private _trips_service: TripsService,
    private _trip_service: TripService,
  ) {
    this.route.data.subscribe(data => {
      const trips_data = data.trips;
      this.count = trips_data.count;
      this.next = trips_data.next;
      this.previous = trips_data.previous;
      this.trips = trips_data.results.map(item => Trip.get_trip_from_data(item));
      this._watcher_service.update_enable_trip_export(this.trips.length > 0);
      this._blowhorn_service.hide_loader();
    });
  }

  ngOnInit() {}

  show_reschedule(event: any) {
    this.show_details = true;
    this.selected_trip = event.trip;
  }

  reschedule_window_closed(event: any) {
    this.show_details = false;
    this._blowhorn_service.show_loader();
    this._trip_service.get_upcoming_mytrips(
      Constants.urls.API_UPCOMING_MY_TRIPS
    ).subscribe(data => {
      const trips_data = data;
      this.count = trips_data.count;
      this.next = trips_data.next;
      this.previous = trips_data.previous;
      this.trips = trips_data.results.map(item => Trip.get_trip_from_data(item));
      this._watcher_service.update_enable_trip_export(this.trips.length > 0);
      this._blowhorn_service.hide_loader();
    });
  }

  update_trips(data: {count: number, next: string, previous: string, results: any[]}) {
    this.count = data.count;
    this.next = data.next;
    this.previous = data.previous;
    this.trips = data.results.map(item => Trip.get_trip_from_data(item));
    this._watcher_service.update_enable_trip_export(this.trips.length > 0);
  }

}
