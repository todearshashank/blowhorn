import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-not-found',
  templateUrl: './order-not-found.component.html',
  styleUrls: ['./order-not-found.component.scss']
})
export class OrderNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
