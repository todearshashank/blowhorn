import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { SidebarService } from '../../../shared/services/sidebar.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  private show_menu = true;
  private cancelIcon = `/static/${this.blowhorn_service.current_build}/assets/Assets-Common/BlackCancel@2x.png`;
  constructor(
    public _sidebar_service: SidebarService,
    public blowhorn_service: BlowhornService
  ) {}

  ngOnInit() {}
}
