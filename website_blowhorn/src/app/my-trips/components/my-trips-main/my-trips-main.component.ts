import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Driver } from '../../../shared/models/driver.model';
import { City } from '../../../shared/models/city.model';
import { IdName } from '../../../shared/models/id-name.model';
import { WatcherService } from '../../../shared/services/watcher.service';

@Component({
  selector: 'app-my-trips-main',
  templateUrl: './my-trips-main.component.html',
  styleUrls: ['./my-trips-main.component.scss']
})
export class MyTripsMainComponent implements OnInit {

  driver_store: Driver[] = [];
  city_store: City[] = [];
  hub_store: IdName[] = [];
  vehicle_type_store: IdName[] = [];
  contract_types: any[] = [];

  constructor(
    private route: ActivatedRoute,
    private watcher_service: WatcherService,
  ) {
    route.data
      .subscribe( data => {
        this.driver_store = data.trip_filter.drivers;
        this.city_store = data.trip_filter.cities;
        this.vehicle_type_store = data.trip_filter.vehicle_classes;
        this.contract_types = data.trip_filter.contract_types;
        for (const city of data.trip_filter.cities) {
          for (const hub of city.hubs) {
            this.hub_store.push(hub);
          }
        }
        this.watcher_service
          .update_city_store(this.city_store);
        this.watcher_service
          .update_hub_store(this.hub_store);
        this.watcher_service
          .update_vehicle_type_store(this.vehicle_type_store);
        this.watcher_service
          .update_driver_store(this.driver_store);
        this.watcher_service
            .update_contract_types(this.contract_types)
      },
      err => {
        console.error(err);
      });
  }

  ngOnInit() {
  }

}
