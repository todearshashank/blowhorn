import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { TripService } from '../../../shared/services/trip.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-proof-of-delivery',
  templateUrl: './proof-of-delivery.component.html',
  styleUrls: ['./proof-of-delivery.component.scss']
})
export class ProofOfDeliveryComponent implements OnInit {

  @Input() image_urls = [];
  @Output() details_closed: EventEmitter<boolean>;
  show_details = false;
  grouped_urls = {};

  constructor() {
    this.details_closed = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.grouped_urls = {};
    let total_urls = this.image_urls.length;
    console.log('image_urls--', this.image_urls)
    for (let i = 0; i < total_urls; i++) {
      let obj = this.image_urls[i];
      let key = obj.stop_id || 9999;
      obj.caption = obj.document_type.replace(/_/g, ' ');
      if (this.grouped_urls.hasOwnProperty(key)) {
        this.grouped_urls[key].push(obj)
      } else {
        this.grouped_urls[key] = [obj];
      }
    }
  }
}
