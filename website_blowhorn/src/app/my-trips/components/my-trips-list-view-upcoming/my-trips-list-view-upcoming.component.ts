import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Trip } from '../../../shared/models/trips-grid.model';
import { TripService } from '../../../shared/services/trip.service';
import { WatcherService } from '../../../shared/services/watcher.service';
import { Constants } from '../../../shared/utils/constants';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { BlowhornPaginationComponent } from '../../../shared/components/blowhorn-pagination/blowhorn-pagination.component';

@Component({
  selector: 'app-my-trips-list-view-upcoming',
  templateUrl: './my-trips-list-view-upcoming.component.html',
  styleUrls: ['./my-trips-list-view-upcoming.component.scss']
})
export class MyTripsListViewUpcomingComponent implements OnInit {

  @ViewChild('pagination', {static: false})
  pagination: BlowhornPaginationComponent;
  trips: Trip[];
  url = Constants.urls.API_UPCOMING_TRIPS;
  export_url = Constants.urls.API_EXPORT_UPCOMING_TRIPS;
  filter_url = '';
  error_msg = 'No records found';
  count: number;
  next: string;
  previous: string;
  is_assign_route = false;
  moreActions = [];

  constructor(
    private route: ActivatedRoute,
    private trip_service: TripService,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {
    route.data
      .subscribe(data => {
        const upcoming_trips_data = data.upcoming_trips;
        this.update_trips(upcoming_trips_data);
        this._blowhorn_service.hide_loader();
        this.error_msg = 'No records found';
      },
        err => {
          console.error(err);
          this._blowhorn_service.hide_loader();
          this.error_msg = 'Something went wrong.';
        });
  }

  ngOnInit() {
  }

  filter_changed(event: { url: string, searchBox: boolean}) {
    this.filter_url = event.url;
    this.url = Constants.urls.API_UPCOMING_TRIPS;
    this.export_url = `${Constants.urls.API_EXPORT_UPCOMING_TRIPS}${this.filter_url}`;
    if (!event.searchBox) {
      this.search();
    }
  }

  update_trips(data: { count: number, next: string, previous: string, results: any[] }) {
    this.count = data.count;
    this.next = data.next;
    this.previous = data.previous;
    this.trips = data.results;
  }

  search() {
    this.trips = [];
    this.update_trips({
      count: 0,
      next: '',
      previous: '',
      results: []
    });
    this._blowhorn_service.show_loader();
    this.is_assign_route = false;
    this.url += this.filter_url ? this.filter_url : '';
    this.filter_url = '';
    this.trip_service
      .get_upcoming_trips(this.url)
      .subscribe(data => {
        this.update_trips(data);
        this._blowhorn_service.hide_loader();
        this.error_msg = 'No records found';
      },
        err => {
          console.error(err);
          this._blowhorn_service.hide_loader();
          this.error_msg = 'Something went wrong.';
        });
  }

  export() {
    window.open(this.export_url);
  }

  enable_assign_route(val: boolean) {
    this.is_assign_route = val;
  }

}
