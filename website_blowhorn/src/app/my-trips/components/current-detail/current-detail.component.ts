import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Trip } from '../../../shared/models/trip.model';
import { ActivatedRoute, Router } from '@angular/router';
import { SidebarService } from '../../../shared/services/sidebar.service';
import { TripsService } from '../../../shared/services/trips.service';
import { Trips } from '../../../shared/utils/enums';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaymentService } from '../../../shared/services/payment.service';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';
import { LocationService } from '../../../shared/services/location.service';
import { LocationStop } from '../../../shared/models/location.model';

const FIREBASE_ORDER_UPDATES_PATH = '/orders/v1/';
const ETA_CALCULATION_INTERVAL = 2;

@Component({
  selector: 'app-current-detail',
  templateUrl: './current-detail.component.html',
  styleUrls: ['./current-detail.component.scss'],
  providers: [PaymentService],
})


export class CurrentDetailComponent implements OnInit, OnDestroy {

  public trip: Trip;
  public isPast = false;
  public isUpcoming = false;
  public selectedTrip: Trip;
  public showMap: boolean = false;
  showOnlyMap: boolean = false;
  showTracking = true;
  public noBookingFound = false;
  public nextStop: any;
  public showCompleteTrip = false;
  firebaseObject: AngularFireObject<any>;
  firebaseDb: AngularFireDatabase;
  show_share_invoice_modal = false;
  show_payment_options = false;
  dich_url = `static/${this.blowhornService.current_build}/assets/Assets/dich.jpg`;
  dich_url_on_map = `static/${this.blowhornService.current_build}/assets/Assets/dich2.jpg`;
  constants = Constants;
  driverLastPosition: LocationStop = null;
  dashboardConfig: any;
  showAdditionalInfo = true;
  showCallBtn = false;
  callMessage = '';
  callMessageType = 'success';
  formattedEta: number;
  etaCalculationIntervalInMins = this.blowhornService.eta_recalculation_interval || ETA_CALCULATION_INTERVAL;
  etaIntervalInstance = null;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private sidebarService: SidebarService,
    private tripsService: TripsService,
    private modalService: NgbModal,
    public paymentService: PaymentService,
    firebaseDb: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    public blowhornService: BlowhornService,
    private locationService: LocationService
  ) {
    this.firebaseDb = firebaseDb;
    this.route.data.subscribe(trip => {
      this.trip = Trip.get_trip_from_data(trip.details);
      if (!this.trip.order_id) {
        this.noBookingFound = true;
        this.sidebarService.show_sidebar = false;
        return;
      }
     
      this.showTracking = this.trip.order_type !== 'shipment' || (
        this.trip.order_type === 'shipment' &&
        Constants.shipmenStatusMapping.noMapTracking.indexOf(trip.details.detail_status) === -1
      )

      if (!this.showTracking) {
        return
      }
      this.isPast = trip.past;
      this.isUpcoming = trip.upcoming;
      this.showCompleteTrip = this.trip.status === 'done';
      if (this.trip.status !== 'done') {
        let vm = this;
        this.updateETA(this.trip.route_info.length ? this.trip.route_info[vm.trip.route_info.length - 1] : null);
      } else {
        this.tripsService.update_is_eta_completed(true);
      }
      if (this.tripsService.current_tab === Trips.PastTrips) {
        this.isPast = true;
      } else if (this.tripsService.current_tab === Trips.Upcoming) {
        this.isUpcoming = true;
      }

      this.sidebarService.show_sidebar = false;
      this.titleService.setTitle('Track - ' + this.trip.order_id);
      this.initFirebaseUpdate();
    });
  }

  ngOnInit() {
    this.blowhornService.loadRazorpayLibrary();
    this.dashboardConfig = this.blowhornService.dashboard_config;
    this.showOnlyMap = !this.blowhornService.isObjectEmpty(this.dashboardConfig) &&
        this.dashboardConfig.show_only_map &&
            window.innerWidth < 678;
    this.showMap = this.showOnlyMap;
    this.showAdditionalInfo = this.blowhornService.isObjectEmpty(this.dashboardConfig)
        || this.dashboardConfig.show_additional_info;
    this.showCallBtn = this.dashboardConfig && this.dashboardConfig.show_call_button;
  }

  ngOnDestroy() {
    this.sidebarService.show_sidebar = true;
  }

  initFirebaseUpdate() {
    this.nextStop = this.trip.next_stop;
    if (this.trip.status !== 'done') {
      this.tripsService.get_firebase_token().subscribe((resp: string) => {});
      const firebase_path = FIREBASE_ORDER_UPDATES_PATH + this.trip.order_id;
      this.firebaseObject = this.firebaseDb.object<Trip>(firebase_path);
      this.firebaseObject.valueChanges().subscribe(snapshot => {
        if (snapshot) {
          const data_bundle: any = JSON.parse(snapshot);
          if (data_bundle == null) {
            return;
          }
          const trip = Trip.get_trip_from_data(data_bundle);
          if (trip.last_modified > this.trip.last_modified) {
            this.trip = trip;
            this.updateETA(this.driverLastPosition);
            this.showCompleteTrip = (this.trip.status === 'done');
          }
        }
      });
    }
  }

  toggleMap() {
    this.showMap = !this.showMap;
  }

  moveTostartPage() {
    if (!Object.keys(this.blowhornService.user_details).length) {
      window.open('/', '_self');
    } else {
      if (this.blowhornService.is_staff) {
        window.open('/admin', '_self');
      } else {
        this.router.navigate(['dashboard', 'mytrips']);
      }
    }
  }

  moveToInvoice(orderId: string) {
    // this.router.navigate(['invoice', orderId]);
    window.open('/invoice/' + orderId, '_blank');
  }

  public openShareInvoice(trip: Trip) {
    this.selectedTrip = trip;
    this.show_share_invoice_modal = true;
  }

  tripDoneOpen(content: any, trip: Trip) {
    this.selectedTrip  = trip;
    if (!this.isPast && !this.isUpcoming) {
      this.modalService.open(content).result.then(
        (result) => {},
        (reason) => {}
      );
    }
  }

  public cancelBooking(cancelBooking, trip: Trip) {
    this.selectedTrip = trip;
    this.modalService.open(cancelBooking).result.then((result) => {
    }, (reason) => {

    });
  }

  public openPaymentOptions(trip: Trip) {}

  public toggleShowCompleteTrip() {
    this.showCompleteTrip = false;
    this.trip.display_payment = false;
  }

  updateFinishedDistance(distanceUpdate) {
    // Update distance only if trip is ongoing
    // TODO: update the remaining distance based on current location
    // of driver and the remaining stops
    if (this.trip.status === 'ongoing') {
      if (this.trip.pickup.status === 'done') {
        this.trip.finished_distance += distanceUpdate;
      } else {
        this.trip.pickup_distance_done += distanceUpdate;
      }
    }
  }

  updateETA(driverLocation: any) {
    this.driverLastPosition = driverLocation;
    let all_stops = [this.trip.pickup];
    all_stops = all_stops.concat(this.trip.stops);
    all_stops.push(this.trip.dropoff);
    let vm = this;
    this.locationService.annotateTimeAndDistance(
      all_stops,
      driverLocation,
      this.trip.pickup_datetime,
      this.trip.status
    ).then(data => {
      this.tripsService.update_is_eta_completed(true);
      console.log('data.nextStopEta', data.nextStopEta)
      if (data.nextStopEta) {
        let eta = vm.blowhornService.convertMinutesToStr(data.nextStopEta);
        vm.formattedEta = eta;
        console.log(vm.formattedEta)
      }
    });
    // let remaining_distance_list = stops.map((i: any) => i.remaining_distance);
    // let remaining_distance_stops = remaining_distance_list.reduce((a: number, b: number) => a + b, 0);

    // let estimated_distance_list = stops.map((i: any) => i.estimated_distance);
    // let estimated_distance_stops = estimated_distance_list.reduce((a: number, b: number) => a + b, 0);
  }

  public getDate(str: string) {
    return str.replace(/-/g, ' ');
  }

  public showPaymentOptions(trip: any) {
    this.selectedTrip = trip;
    this.show_payment_options = true;
  }

  callDriver() {
   this.callMessage = 'Requesting..';
   this.tripsService.makeCall({order_id: parseInt(this.trip.order_real_id)}).subscribe(data => {
       console.log('DATA: ', data);
       this.callMessage = data['message'];
       this.callMessageType = 'success';
       setTimeout(() => this.callMessage = '', 4000);
    }, err => {
       this.callMessageType = 'error';
       this.callMessage = err.error;
       setTimeout(() => this.callMessage = '', 4000);
    });
  }

  resetCallError() {
    this.callMessage = '';
  }

}
