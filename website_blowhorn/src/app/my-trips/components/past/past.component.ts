import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Trip } from '../../../shared/models/trip.model';
import { ActivatedRoute } from '@angular/router';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornPaginationComponent } from '../../../shared/components/blowhorn-pagination/blowhorn-pagination.component';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-past',
  templateUrl: './past.component.html',
  styleUrls: ['./past.component.scss']
})
export class PastComponent implements OnInit {

  @ViewChild('pagination', {static: false})
  pagination: BlowhornPaginationComponent;
  trips: Trip[];
  count: number;
  next: string;
  previous: string;
  constructor(
    private route: ActivatedRoute,
    private _watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {
    this.route.data.subscribe(data => {
      const trips_data = data.trips;
      this.count = trips_data.count;
      this.next = trips_data.next;
      this.previous = trips_data.previous;
      this.trips = trips_data.results.map(item => Trip.get_trip_from_data(item));
      this._watcher_service.update_enable_trip_export(this.trips.length > 0);
      this._blowhorn_service.hide_loader();
    });
  }
  ngOnInit() {}

  update_trips(data: {count: number, next: string, previous: string, results: any[]}) {
    this.count = data.count;
    this.next = data.next;
    this.previous = data.previous;
    this.trips = data.results.map(item => Trip.get_trip_from_data(item));
    this._watcher_service.update_enable_trip_export(this.trips.length > 0);
  }
}
