import {
  Component,
  OnInit,
  ViewChild,
  QueryList,
  ChangeDetectorRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { BlowhornMapSearchComponent } from '../../../shared/components/blowhorn-map-search/blowhorn-map-search.component';
import { NgForm } from '@angular/forms';
import { SaveLocationComponent } from '../../../location/components/save-location/save-location.component';
import { SaveContactComponent } from '../../../contact/components/save-contact/save-contact.component';
import { MapService } from '../../../shared/services/map.service';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { SavedLocation } from '../../../shared/models/saved-location.model';
import { RouteService } from '../../../shared/services/route.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { WatcherService } from '../../../shared/services/watcher.service';
import { Route } from '../../../shared/models/route.model';
import { Address } from '../../../shared/models/address.model';
import { Contact } from '../../../shared/models/contact.model';
import { GeoPoint } from '../../../shared/models/geopoint.model';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../../../shared/utils/constants';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-edit-trip-stops',
  templateUrl: './edit-trip-stops.component.html',
  styleUrls: ['./edit-trip-stops.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditTripStopsComponent implements OnInit, OnDestroy {
  @ViewChild('stops_form', {static: false})
  route_form: NgForm;
  @ViewChildren(BlowhornMapSearchComponent)
  map_search_comps: QueryList<BlowhornMapSearchComponent>;
  @ViewChild('address_form', {static: false})
  private _address_form: SaveLocationComponent;
  @ViewChild('contact_form', {static: false})
  private _contact_form: SaveContactComponent;
  selected_city_name = '';
  selected_city_id: number = null;
  first_stop_selected: google.maps.LatLng = MapService.dummy_geopoint;
  last_stop_selected: google.maps.LatLng = MapService.dummy_geopoint;
  remaining_stops_selected: { location: google.maps.LatLng }[] = [];
  stops_selected: RouteStop[] = [];
  stops: RouteStop[] = [];
  deleted_stops: number[] = [];
  inserted_index: number;
  stops_drag_list: number[] = null;
  map_latitude = 12.9716;
  map_longitude = 77.5946;
  map_bounds: google.maps.LatLngBounds;
  show_save_address = false;
  show_save_contact = false;
  stop_under_edit_index: number = null;
  saved_locations: SavedLocation[] = [];
  current_location: google.maps.LatLng;
  is_current_location_in_city = true;
  _show_direction = false;
  private $saved_locations: any;
  private $save_address: any;
  private $save_contact: any;
  _blowhorn_service: BlowhornService;
  _map_service: MapService;
  directions: google.maps.DirectionsResult;
  origin = { lat: 24.799448, lng: 120.979021 };
  destination = { lat: 24.799524, lng: 120.975017 };
  is_error = false;
  error_msg = '';
  order_number: string;
  change_detector: ChangeDetectorRef;
  last_done_stop_index = -1;
  initial_total_stops = 0;
  initial_waypoints: number[] = [];
  show_message = false;
  message_type = '';
  message_header = '';
  message = '';
  submittinForm = false;
  constructor(
    private _watcher_service: WatcherService,
    public blowhorn_service: BlowhornService,
    private map_service: MapService,
    private _change_detector: ChangeDetectorRef,
    private _http_client: HttpClient
  ) {
    this._blowhorn_service = blowhorn_service;
    this._map_service = map_service;
    this.change_detector = _change_detector;
  }

  ngOnDestroy() { }

  ngOnInit() {
    this.stops.forEach((item, index) => {
      if (!item.editable) {
        this.last_done_stop_index = index;
      }
      if (index > 0 && item.id) {
        this.initial_waypoints.push(item.id);
      }
    });
    this.initial_total_stops = this.stops.length;
    this.rearrange_stops();
    // this.show_hide_current_location();
    this.$saved_locations = this._blowhorn_service.saved_locations.subscribe(
      saved_locations => {
        this.saved_locations = saved_locations;
      }
    );
    this.$save_address = this._watcher_service.show_save_address.subscribe(
      show_save_address => {
        this.show_save_address = show_save_address;
      }
    );
    this.$save_contact = this._watcher_service.show_save_contact.subscribe(
      show_save_contact => {
        this.show_save_contact = show_save_contact;
      }
    );
  }

  show_hide_current_location() {
    if (
      this._blowhorn_service.is_navigator_geolocation &&
      this.selected_city_id
    ) {
      this.is_current_location_in_city = this._map_service.is_current_location_in_city(
        this.selected_city_id,
        'dropoff'
      );
    }
  }

  rearrange_stops() {
    this.stops.forEach((stop, index) => (stop.seq_no = index));
    this.stops_selected = this.stops.filter(item => {
      return item.position && item.position[0] && item.position[1]
        ? true
        : false;
    });
    this.stops_drag_list = this.stops.map((item, index) => {
      return index + 1;
    });

    this._show_direction = false;
    this.remaining_stops_selected = [];
    this.first_stop_selected = this.last_stop_selected =
      MapService.zero_geopoint;
    if (this.stops_selected.length === 1) {
      this.first_stop_selected = MapService.map_position_to_geopoint(
        this.stops_selected[0].position
      );
      this.last_stop_selected = this.first_stop_selected;
      this._show_direction = true;
    } else if (this.stops_selected.length === 2) {
      this.first_stop_selected = MapService.map_position_to_geopoint(
        this.stops_selected[0].position
      );
      this.last_stop_selected = MapService.map_position_to_geopoint(
        this.stops_selected[1].position
      );
      this._show_direction = true;
    } else if (this.stops_selected.length > 2) {
      this.first_stop_selected = MapService.map_position_to_geopoint(
        this.stops_selected[0].position
      );
      this.last_stop_selected = MapService.map_position_to_geopoint(
        this.stops_selected[this.stops_selected.length - 1].position
      );
      this.remaining_stops_selected = this.stops_selected
        .slice(1, this.stops_selected.length - 1)
        .map(item => {
          return {
            location: MapService.map_position_to_geopoint(
              item.position
            )
          };
        });
      this._show_direction = true;
    }
    this.set_map_bounds('dropoff');
    this._change_detector.detectChanges();
  }

  update_stop(event: RouteStop, stop: RouteStop) {
    const id = stop.address.id;
    stop = event;
    stop.address.id = id;
    this.rearrange_stops();
    console.log(event, stop, this.stops);
  }

  set_map_bounds(location_type: string) {
    this.map_bounds = MapService.dummy_bounds;
    if (this.stops_selected.length) {
      this.stops_selected.forEach(stop => {
        this.map_bounds.extend(stop.geopoint);
      });
    }
    // else {
      // this._blowhorn_service.get_city_coverage(this.selected_city_id, location_type)
      //   .forEach(item => {
      //     this.map_bounds.extend(item);
      //   });
    // }
  }

  stop_inserted(event) {
    this.inserted_index = event.index;
    const index_copied = event.item - 1;
    this.stops.splice(this.inserted_index, 0, this.stops[index_copied]);
    this.rearrange_stops();
  }

  add_stop() {
    const new_stop = new RouteStop();
    this.stops.push(new_stop);
    this.rearrange_stops();
  }

  edit_trip() {
    this.submittinForm = true;
    const retained_stops: number[] = this.stops
      .filter((item, index) => index > 0 && item.id)
      .map(item => item.id);
    this.deleted_stops = this.initial_waypoints.filter(
      item => !retained_stops.includes(item)
    );
    const data = {
      deleted_ids: this.deleted_stops,
      completed_stops: this.stops
        .filter(item => item.id && !item.editable)
        .map(item => item.id),
      stops: []
    };
    this.stops.forEach(item => {
      const stop = {
        id: item.id,
        sequence_id: item.seq_no,
        completed: !item.editable,
        shipping_address: {
          id: item.address.id,
          first_name: item.contact.name,
          phone_number: item.contact.mobile,
          line1: item.address.full_address,
          line2: item.address.street_address,
          line3: item.address.area,
          line4: item.address.city,
          postcode: item.address.postal_code,
          state: item.address.state,
          country: item.address.country,
          latlng: item.position
        }
      };
      data.stops.push(stop);
    });

    this._http_client
      .put(
        `${Constants.urls.API_ORDER_STOPS}/${this.order_number}/stops/`,
        data
      )
      .subscribe(
        (res: any) => {
          this.show_message = true;
          this.message_type = 'success';
          this.message_header = 'Trip update successful!';
          this.message = res.toString();
          this.change_detector.detectChanges();
          this.submittinForm = false;
        },
        (err: any) => {
            console.log(err)
          this.show_message = true;
          this.message_type = 'error';
          this.message_header = 'Trip update failed!';
          this.message = err.error;
          this.change_detector.detectChanges();
          this.submittinForm = false;
        }
      );
  }
  close_message() {
    this.show_message = false;
    this._blowhorn_service.hide_modal();
  }

  set_directions(directions) {
    this.directions = directions;
  }

  cancel() {
    this._blowhorn_service.hide_modal();
  }

  place_changed(place: google.maps.places.PlaceResult, stop: RouteStop) {
    let location_type = stop.seq_no === 0 ? 'pickup' : 'dropoff';
    const comp = this.map_search_comps.find(item => {
      return item.stop.seq_no === stop.seq_no;
    });
    // if (
    //   this._map_service.is_location_in_city(
    //     this.selected_city_id,
    //     place.geometry.location,
    //     location_type
    //   )
    // )
    {
      comp.placeholder = this.get_stop_placeholder(stop.seq_no);
      stop.geopoint = place.geometry.location;
      const new_address = this._map_service.getAddressComponentsObject(
        place.address_components
      );
      new_address.id = stop.address.id;
      stop.address = new_address;
      stop.position = MapService.map_geopoint_to_position(
        place.geometry.location
      );
      stop.address.name = '';
      stop.saved_location = new SavedLocation();
      this.pan_to(stop.geopoint);
      this.rearrange_stops();
    }
    // else {
    //   comp.placeholder = 'Out of coverage';
    //   this.stops.splice(
    //     stop.seq_no,
    //     1,
    //     new RouteStop(
    //       stop.id,
    //       stop.seq_no,
    //       new google.maps.LatLng(null, null),
    //       [null, null],
    //       new Address(stop.address.id),
    //       stop.contact
    //     )
    //   );
    //   this.rearrange_stops();
    // }
  }

  pan_to(event) {
    this.map_latitude = event.lat();
    this.map_longitude = event.lng();
  }

  delete_stop(event) {
    const index = this.stops.findIndex(x => x.seq_no === event.seq_no);
    const deleted_items = this.stops.splice(index, 1);
    this.rearrange_stops();
  }

  get_stop_placeholder(index: number): string {
    if (index) {
      return index < this.stops.length - 1
        ? 'Choose a stop point'
        : 'Choose an end point';
    } else {
      return 'Choose a start point';
    }
  }

  get_stop_header(index: number): string {
    return `Stop ${index + 1}`;
  }

  get_stop_type(index: number, length: number): string {
    if (index === 0) {
      return 'pickup';
    } else if (index === length - 1) {
      return 'dropoff';
    }
    return 'stop';
  }

  get_icon_url(index: number): string {
    if (index) {
      return index < this.stops.length - 1
        ? Constants.markers.stop
        : Constants.markers.dropoff;
    } else {
      return Constants.markers.pickup;
    }
  }

  get_contact_form_header(index) {
    return `Edit contact for Stop ${index + 1}`;
  }

  marker_dragged(event, stop) {
    stop.position = [event.coords.lat, event.coords.lng];
    stop.geopoint = MapService.map_position_to_geopoint(stop.position);
    stop.saved_location = new SavedLocation();
    this._map_service
      .get_geocode(stop.geopoint)
      .then(results => {
        const result = results[0];
        const new_address = this._map_service.getAddressComponentsObject(
          result.address_components
        );
        new_address.id = stop.address.id;
        stop.address = new_address;
        stop.address.name = '';
        this.rearrange_stops();
      })
      .catch(status => {
        console.info('Geocoding Failed', status);
      });
  }

  open_contact_form(event: boolean, stop: RouteStop, index: number) {
    this.stop_under_edit_index = index;
    this.show_save_contact = true;
  }

  stop_moved(index: number, list: any[]): void {
    if (this.inserted_index <= this.last_done_stop_index) {
      this.stops.splice(this.inserted_index, 1);
    } else if (this.inserted_index > index) {
      this.stops.splice(index, 1);
    } else {
      this.stops.splice(index + 1, 1);
    }
    this.rearrange_stops();
    this.inserted_index = null;
  }

  reset_error() {
    this.is_error = false;
    this.error_msg = '';
  }

  update_stop_address(event) {
    this.stops[this.stop_under_edit_index].contact.mobile =
      event.contact_number;
    this.stops[this.stop_under_edit_index].contact.name =
      event.contact_name;
  }

  update_stop_contact(event) {
    this.stops[this.stop_under_edit_index].contact.mobile =
      event.contact_number;
    this.stops[this.stop_under_edit_index].contact.name =
      event.contact_name;
    this._contact_form.clear_form();
  }

  clear_stop_under_edit_index() {
    this.stop_under_edit_index = null;
  }
}
