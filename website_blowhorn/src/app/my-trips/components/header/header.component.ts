import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Trips } from '../../../shared/utils/enums';
import { TripsService } from '../../../shared/services/trips.service';
import { UserLocation } from '../../../shared/models/user-location.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Constants } from '../../../shared/utils/constants';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

declare var google: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  links = [
    {link: 'current', text: 'Current'},
    {link: 'completed', text: 'Completed'},
    {link: 'upcoming', text: 'Upcoming'}
  ];
  link_styles = { margin: '0px'};
  navbar_styles = {
    padding: '0px',
    boxShadow: '0px 0px 0px 0px transparent',
    border: 'none'
  };
  public quickBook = false;
  private quickBookStatus = '';
  private quickBookMessage = '';
  private locations: UserLocation[] = [];
  private filtered_locations: UserLocation[] = [];
  private fromLocation: UserLocation;
  private toLocation: UserLocation;
  private truckTypeOptions: any[];
  private availableTruckTypes: any[];
  private truckTypeParamVal: string; // = this.truckTypeOptions[0].value;
  private truckTypeParamLabel: string; // = this.truckTypeOptions[0].label;
  public bookingId = '';
  public trackingURL = '';
  private coverage: any;
  private coverage_polygon: any = {};
  constants = Constants;
  _enable_export: boolean;
  @Output() tabChanged: EventEmitter<Trips> = new EventEmitter<Trips>();

  show_booking_form = false;
  iframeSrc = null;
  canViewBookTripButton = true;

  constructor(
    private _tripsService: TripsService,
    private router: Router,
    private modalService: NgbModal,
    private _watcher_service: WatcherService,
    public blowhorn_service: BlowhornService
  ) {
    this._watcher_service.enable_trip_export.subscribe(val => this._enable_export = val);
    this._tripsService.get_locations().subscribe((res: UserLocation[]) => {
      this.locations = [];
      res.forEach(item => {
        this.locations.push(UserLocation.get_location_from_data(item));
      });
    });
  }

  moveTostartPage() {
    this.router.navigate(['dashboard', 'mytrips']);
  }

  private checkValid() {
    return this.locations.indexOf(this.fromLocation) > -1 && this.locations.indexOf(this.toLocation) > -1;
  }

  private resetForm() {
    this.quickBook = false;
    this.fromLocation = null;
    this.toLocation = null;
    this.truckTypeParamVal = '';
    this.truckTypeParamLabel = '';
    this.filtered_locations = [];
  }

  public toggleQuckBook() {
    this.quickBook = !this.quickBook;
    if (!this.quickBook) {
      this.resetForm();
    }
  }

  private valueFormatter(data: UserLocation): string {
    return `${data.name}`;
  }

  private listFormatter(data: UserLocation): string {
    return `${data.name}`;
  }

  private createQuickBook(tripCreated: any) {
    this._tripsService.create_trip(
      this.fromLocation,
      this.toLocation,
      this.truckTypeParamVal
    ).subscribe((res: any) => {
      this.quickBookStatus = res.status;
      this.quickBookMessage = res.message;
      this.bookingId = res.message.friendly_id;
      this.trackingURL = res.message.tracking_url;
      this.tripCreatedOpen(tripCreated);
      this.resetForm();
    },
    err => {
        this.quickBookStatus = err.status;
        this.quickBookMessage = err.message;
        this.tripCreatedOpen(tripCreated);
    });
  }

  public tripCreatedOpen(content: any) {
    this.modalService.open(content).result.then(
      (result) => {},
      (reason) => {}
    );
  }

  private selectSecondParam(option: any) {
    this.truckTypeParamVal = option.value;
    this.truckTypeParamLabel = option.label;
  }

  ngOnInit() {
    this.canViewBookTripButton = this.blowhorn_service.is_org_admin
      || this.blowhorn_service.hasPermission('booking_trips', 'can_book_new_trip')
    this.iframeSrc = '/booking?embed=true&type=booking&hide_scroll=yes';
    this._tripsService.get_params().subscribe((res: any) => {
        this.truckTypeOptions = [];
        this.availableTruckTypes = res.vehicle_classes;
        this.coverage = res.city_coverage;
        Object.keys(this.coverage).forEach(city => {
            let coverage_pickup = this.coverage[city]['coverage']['pickup'];
            let coverage_dropoff = this.coverage[city]['coverage']['dropoff'];
            this.coverage_polygon[city] = {
                pickup: new google.maps.Polygon({
                  paths: coverage_pickup,
                  visible: false,
                }),
                dropoff: new google.maps.Polygon({
                  paths: coverage_dropoff,
                  visible: false,
                })
            };
        });
      });
  }

  export_trips() {
    const trip_type = this.router.url.split('?').shift().split('/').pop();
    let url;
    if (trip_type === 'current') {
      url = Constants.urls.API_EXPORT_CURRENT_MY_TRIPS;
    } else if (trip_type === 'completed') {
      url = Constants.urls.API_EXPORT_COMPLETED_MY_TRIPS;
    } else {
      url = Constants.urls.API_EXPORT_UPCOMING_MY_TRIPS;
    }
    window.open(url);
  }

  private onFromLocationChange(location: UserLocation) {
    this.fromLocation = location;
    this.filtered_locations = [];
    if (!location || !location.geopoint) {
      return;
    }
    let selected_city;
    let geopoint;
    let selected_city_polygon;
    selected_city = Object.keys(this.coverage_polygon).find(_city => {
      const _coverage = this.coverage_polygon[_city].pickup;
      geopoint = new google.maps.LatLng(location.geopoint.lat, location.geopoint.lng);
       return google.maps.geometry.poly.containsLocation(geopoint, _coverage);
    });
    this.truckTypeOptions = this.availableTruckTypes[selected_city];
    selected_city_polygon = this.coverage_polygon[selected_city].pickup;
    this.locations.forEach((obj, index) => {
      geopoint = new google.maps.LatLng(obj.geopoint.lat, obj.geopoint.lng);
      if (google.maps.geometry.poly.containsLocation(geopoint, selected_city_polygon)) {
        this.filtered_locations.push(obj);
      }
    });
  }

  set_to_location(location: UserLocation) {
    this.toLocation = location;
  }

  openBooking() {
    if (this.blowhorn_service.current_build !== Constants.default_build) {
      this.show_booking_form = true;
    } else {
      window.open('/solutions/on-demand-transportation', '_blank');
    }
  }
}
