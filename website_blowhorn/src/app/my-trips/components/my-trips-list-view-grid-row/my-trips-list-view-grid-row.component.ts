import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { Trip } from '../../../shared/models/trips-grid.model';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { SaveRouteComponent } from '../../../route/components/save-route/save-route.component';
import { RouteService } from '../../../shared/services/route.service';
import { WatcherService } from '../../../shared/services/watcher.service';
import { EditTripStopsComponent } from '../edit-trip-stops/edit-trip-stops.component';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../../../shared/utils/constants';
import { MapService } from '../../../shared/services/map.service';
import { Address } from '../../../shared/models/address.model';
import { Contact } from '../../../shared/models/contact.model';
import { TripStop } from '../../../shared/models/trip-stop.model';
import { TripService } from '../../../shared/services/trip.service';
import { BlowhornUploadComponent } from '../../../shared/components/blowhorn-upload/blowhorn-upload.component';

@Component({
    selector: 'app-my-trips-list-view-grid-row',
    templateUrl: './my-trips-list-view-grid-row.component.html',
    styleUrls: ['./my-trips-list-view-grid-row.component.scss']
})
export class MyTripsListViewGridRowComponent implements OnInit {

    @Input()
    trip: Trip;
    @Output()
    is_assign_route_fired: EventEmitter<boolean>;
    @Output()
    trip_clicked: EventEmitter<Trip>;
    @Output() btn_doc_clicked = new EventEmitter<Trip>();

    @ViewChild('file_upload', { static: false }) file_upload: BlowhornUploadComponent;
    @ViewChild('overlay_panel', { static: false }) overlay_panel: ElementRef;
    @ViewChild('overlay_panel', { static: false }) overlay_panel_download: ElementRef;

    _is_assign_route = false;
    routes: { name: string, id: string, pickup: string, dropoff: string, stop_count: number }[] = [];
    city_name: string;
    no_route_message: string;
    filter_text = '';
    show_confirm = false;
    selected_route: { name: string, id: string, pickup: string, dropoff: string, stop_count: number } = null;
    confirm_message: string;
    show_details = false;
    id: any;
    moreActions = [];
    can_assign_route = false;
    can_edit_trip = false;
    can_generate_departure_sheet = false;
    can_track_trip = false;
    can_view_consignment_notes = false;
    can_view_proofs = false;
    error = false;
    type = 'success';
    heading = '';
    message_ = '';
    modal_title = '';
    show = {
        modal: false,
        message: false,
        name: '',
    };
    message = {
        show: false,
        type: '',
        heading: '',
        text: ''
    };
    current_status: string = '';
    status_colors = {
        'upcoming': 'rgb(74, 74, 74)',
        'current': 'rgb(0, 111, 137)',
        'completed': '#007435'
    };
    statusChipColorStyles = {};


    constructor(
        private _apollo: Apollo,
        public _blowhorn_service: BlowhornService,
        private _http_client: HttpClient,
        private tripService: TripService
    ) {
        this.is_assign_route_fired = new EventEmitter<boolean>(false);
        this.trip_clicked = new EventEmitter<Trip>();
        this.btn_doc_clicked = new EventEmitter<Trip>();
    }

    ngOnInit() {
        this.can_assign_route = this._blowhorn_service.hasPermission('enterprise_trips', 'can_assign_route');
        this.can_edit_trip = this._blowhorn_service.hasPermission('enterprise_trips', 'can_edit_trip');
        this.can_generate_departure_sheet = this._blowhorn_service.hasPermission('enterprise_trips', 'can_generate_departure_sheet');
        this.can_track_trip = this._blowhorn_service.hasPermission('enterprise_trips', 'can_track_trip');
        this.can_view_consignment_notes = this._blowhorn_service.hasPermission('enterprise_trips', 'can_view_consignment_notes');
        this.can_view_proofs = this._blowhorn_service.hasPermission('enterprise_trips', 'can_view_proofs');

        this.statusChipColorStyles = this.set_color(this.trip.short_status);
        this.current_status = this.trip.short_status;
        if (this.trip.city_id) {
            this.city_name = this._blowhorn_service.cities.find(item => item.id === this.trip.city_id).name;
            this.no_route_message = `No route configured for city : ${this.city_name}`;
        }
        this.setMoreActions()
    }

    setMoreActions() {
        switch (this.current_status) {
            case 'upcoming':
                if (this.can_assign_route) {
                    this.moreActions.push({ label: 'Assign Route', val: 'assign_route' })
                }
                if (this.can_edit_trip) {
                    this.moreActions.push({ label: 'Edit Trip', val: 'edit_trip' })
                }

                if (this.trip.trip_type === 'Shipment' && this.can_generate_departure_sheet) {
                    this.moreActions.push({ label: 'Departure Sheet', val: 'departure_sheet' });
                };
                break;

            case 'current':
                if (this.can_assign_route) {
                    this.moreActions.push({ label: 'Assign Route', val: 'assign_route' })
                }
                if (this.can_edit_trip) {
                    this.moreActions.push({ label: 'Edit Trip', val: 'edit_trip' })
                }
                if (this.can_view_proofs && this._blowhorn_service.user_profile.show_pod_in_dashboard) {
                    this.moreActions.push({ label: 'Documents', val: 'docs' })
                }
                if (this.can_track_trip) {
                    this.moreActions.push({ label: 'Track', val: 'track' })
                }
                if (this.trip.trip_type === 'Shipment' && this.can_generate_departure_sheet) {
                    this.moreActions.push({ label: 'Departure Sheet', val: 'departure_sheet' });
                };
                break;

            case 'completed':
                if (this.can_view_proofs && this._blowhorn_service.user_profile.show_pod_in_dashboard) {
                    this.moreActions.push({ label: 'Documents', val: 'docs' })
                }
                if (this.can_track_trip) {
                    this.moreActions.push({ label: 'Trace', val: 'track' })
                }
                if (this.can_view_consignment_notes) {
                    this.moreActions.push({ label: 'Consignment Note', val: 'consignment_note' })
                }
                break;

            default:
                console.error('Invalid trip status');

        }
    }

    moreActionSelected(val: string) {
        switch (val) {
            case 'docs':
                this.btn_doc_clicked.emit(this.trip);
                break;

            case 'track':
                this.track_order_trip();
                break;

            case 'edit_trip':
                this.edit_trip();
                break;

            case 'upload_stops':
                this.upload_stops();
                break;

            case 'departure_sheet':
                this.departure_sheet();
                break;

            case 'consignment_note':
                this.consignment_note();
                break;

            case 'assign_route':
                this.assign_route();
                break;
        }
    }

    public getId() {
        this.id = this.trip.trip_number;
        console.log(this.id);
    }

    sendMessage(): void {
        // send message to subscribers via observable subject
        this.tripService.sendMessage(this.trip.trip_number);
    }

    get_seconds_in_hours(seconds: string): string {
        const mins = (parseInt(seconds, 10) / 60);
        return this.get_mins_in_hours(mins);
    }

    get_mins_in_hours(mins: number): string {
        if (mins >= 60) {
            const hrs = mins / 60;
            mins = mins % 60;
            return `${Math.floor(hrs)} hrs ${Math.floor(mins)} mins`;
        } else {
            return `${Math.floor(mins)} mins`;
        }
    }

    track_order_trip(): void {
        const url = '/track/' + this.trip.trip_number;
        window.open(url);
    }

    assign_route() {
        const customer_name = this._blowhorn_service.username;
        let profile = this._blowhorn_service.user_profile;
        this._apollo.watchQuery<any>({
            query: gql`
      query {
        customerRoutes(city: "${this.city_name}", customer_Name: "${profile.organization_name}") {
          edges {
            node {
              id
              name
              stops {
                id
                seqNo
                line1
                line3
              }
            }
          }
        }
      }
      `,
            fetchPolicy: 'network-only'
        }).valueChanges.subscribe(result => {
            this.routes = [];
            result.data.customerRoutes.edges.forEach(edge => {
                const node = edge.node;
                const pickup_stop = node.stops.find(stop => stop.seqNo === 0);
                const dropoff_stop = node.stops.find(stop => stop.seqNo === node.stops.length - 1);
                this.routes.push({
                    id: node.id,
                    name: node.name,
                    pickup: pickup_stop.line1.split(pickup_stop.line3)[0],
                    dropoff: dropoff_stop.line1.split(dropoff_stop.line3)[0],
                    stop_count: node.stops.length - 2
                });
            });
            this._is_assign_route = true;
            this.is_assign_route_fired.next(true);
        });
    }

    select_route(route: { name: string, id: string, pickup: string, dropoff: string, stop_count: number }) {
        this.selected_route = route;
        this.show_confirm = true;
        if (this.current_status === 'upcoming') {
            this.confirm_message = `Any existing stops will be replaced by new set of stops. Are you sure?`;
        } else if (this.current_status === 'current') {
            this.confirm_message = `Since trip is in progress, new set of stops will be appended to any existing stops. Are you sure?`;
        }
    }

    close_routes() {
        this.is_assign_route_fired.next(false);
        this._is_assign_route = !this._is_assign_route;
        this.selected_route = null;
        this.confirm_message = null;
        this.filter_text = '';
    }

    proceed(event: any) {
        if (event) {
            const replace = this.current_status === 'upcoming';
            const trip_id = this.trip.id;
            const order_no = this.trip.order_no;
            const route_id = this.selected_route.id;
            this.show_confirm = false;
            this._blowhorn_service.show_loader();
            const mutation = gql`
                mutation {
                    updateOrderWaypoints(tripId: ${trip_id}, orderNumber: "${order_no}", routeId: "${route_id}", replace: ${replace}) {
                        message,
                        status
                    }
                }
            `;
            this._apollo.mutate({
                mutation: mutation
            }).subscribe(result => {
                this.close_routes();
                this._blowhorn_service.hide_loader();
                this.setMessage(true, 'success', 'Success', 'Route assigned successfully');
            }, err => {
                console.log('err', err);
                this._blowhorn_service.hide_loader();
                this.setMessage(true, 'error', 'Failed', err.errors ? err.errors[0] : err.message || 'Failed to assign route..!');
            });
        } else {
            this.show_confirm = false;
        }
    }

    setMessage(show: boolean, type: string, heading: string, content: any): void {
        this.message.show = show;
        this.message.type = type;
        this.message.heading = heading;
        this.message.text = content;
    }

    edit_trip() {
        this._blowhorn_service.show_loader();
        const trip_stops: RouteStop[] = [];
        this._http_client
            .get<{ pickup: TripStop, waypoints: TripStop[] }>(`${Constants.urls.API_ORDER_STOPS}/${this.trip.order_no}/stops/`)
            .subscribe(data => {
                const stops = [data.pickup, ...data.waypoints.sort((item1, item2) => item1.sequence_id - item2.sequence_id)];
                if (stops.length < 2) {
                    const dropoff = Object.assign({}, data.pickup);
                    const address = Object.assign({}, data.pickup.shipping_address);
                    address.id = null;
                    dropoff.completed = false;
                    dropoff.shipping_address = address;
                    stops.push(dropoff);
                }
                stops.forEach(item => {
                    console.log(item);
                    const address = item.shipping_address;
                    trip_stops.push(new RouteStop(
                        item.id ? item.id : null,
                        item.sequence_id,
                        MapService.map_position_to_geopoint([address.geopoint.coordinates[1], address.geopoint.coordinates[0]]),
                        [address.geopoint.coordinates[1], address.geopoint.coordinates[0]],
                        new Address(
                            address.id, null, address.line1, address.line2, address.line3, address.line4,
                            address.state, address.country, address.postcode,
                            null, null, null
                        ),
                        new Contact(
                            address.phone_number && address.phone_number.startsWith('+91') ? address.phone_number.substring(3) : address.phone_number,
                            address.first_name,
                            null
                        ),
                        null,
                        !item.completed
                    ));
                });
                this._blowhorn_service.hide_loader();
                const comp = this._blowhorn_service
                    .show_modal(EditTripStopsComponent);
                this.set_details(comp, trip_stops);
            }, err => {
                this._blowhorn_service.hide_loader();
            });
    }

    set_details(comp, trip_stops) {
        const instance = <EditTripStopsComponent>comp.instance;
        instance.stops = trip_stops;
        instance.selected_city_name = this._blowhorn_service.get_city_name(this.trip.city_id);
        instance.selected_city_id = this.trip.city_id;
        instance.order_number = this.trip.order_no;
    }

    open_details(): void {
        this.trip_clicked.emit(this.trip);
    }

    departure_sheet(): void {
        if (this.trip.trip_number) {
            window.open(`/departuresheet/${this.trip.trip_number.toUpperCase()}`, '_blank');
        } else {
            this.error = true;
        }
    }

    consignment_note(): void {
        if (this.trip.trip_number) {
            window.open(`api/trip/consignment_note/${this.trip.trip_number.toUpperCase()}`, '_blank');
        }
    }

    focus_panel(event: any) {
        this.overlay_panel.nativeElement.focus();
    }

    close(event: any) {
        if (event instanceof KeyboardEvent && event.keyCode !== 27) {
            return;
        }
        this.show.modal = false;
        this.show.message = false;
        this.type = 'success';
        this.heading = '';
        this.message_ = '';
    }

    suppress_close(event: any) {
        event.stopPropagation();
    }

    upload_stops() {
        this.modal_title = 'upload stops';
        this.show.modal = true;
        this.show.name = 'stops';
    }

    upload_file(files: any) {
        this.file_upload.disable();
        this._blowhorn_service.show_loader();
        const file = files[0];
        const form_data = new FormData();
        form_data.append('stops', file, file.name);
        this.call_stops_upload(form_data);
    }
    call_stops_upload(form_data: any): void {
        this.tripService
            .upload_stops(form_data, this.trip.trip_number)
            .subscribe(data => {
                this.show.message = true;
                this.heading = 'Stops Created';
                this.message_ = `${data['number_of_stops']} stops created for trip ${data['trip_number']}`;
                this.type = 'success';
                this._blowhorn_service.hide_loader();
            }, err => {
                this.show.message = true;
                this.heading = 'Stops Creation Failed!';
                this.message_ = err['error']['errors'];
                this.type = 'error';
                this._blowhorn_service.hide_loader();
            });
    }

    reset() {
        this.file_upload.reset();
        this.show.message = false;
    }

    get messageStyles() {
        return {
            wrapper: {
                background: '#fff',
                padding: '15px',
                width: '320px',
                color: '#000'
            },
            heading: {
                color: 'var(--primary-color)'
            }
        }
    }

    closeMessageModal() {
        this.setMessage(false, '', '', '');
    }

    set_color(status: string) {
        return {
            "background-color": this.status_colors[status] || "#006F89",
            "border-radius": "4px",
            "font-weight": 500,
            "font-size": "12px",
            padding: "2px 12px",
            color: "#fff"
        };
    }
}
