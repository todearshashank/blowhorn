import { Component, OnInit, Input } from '@angular/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-my-trips-list-view-grid-title',
  templateUrl: './my-trips-list-view-grid-title.component.html',
  styleUrls: ['./my-trips-list-view-grid-title.component.scss']
})
export class MyTripsListViewGridTitleComponent implements OnInit {

  constructor(
    public _blowhorn_service: BlowhornService,
  ) { }

  ngOnInit() {
  }

}
