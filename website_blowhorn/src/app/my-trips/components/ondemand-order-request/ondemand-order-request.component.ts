import { BlowhornService } from './../../../shared/services/blowhorn.service';
import { TripService } from './../../../shared/services/trip.service';
import { IdName } from './../../../shared/models/id-name.model';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { animate, group, state, style, transition, trigger } from '@angular/animations';
import { SortPipe } from '../../../shared/pipes/sort.pipe';
import * as moment from 'moment';

@Component({
  selector: 'app-ondemand-order-request',
  templateUrl: './ondemand-order-request.component.html',
  styleUrls: ['./ondemand-order-request.component.scss'],
  providers: [SortPipe],
  animations: [
    trigger('slideInOut', [
      state('in', style({ height: '*', opacity: 0 })),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        group([
          animate(300, style({ height: 0 })),
          animate('200ms ease-in-out', style({ 'opacity': '0' }))
        ])
      ]),
      transition(':enter', [
        style({ height: '0', opacity: 0 }),
        group([
          animate(300, style({ height: '*' })),
          animate('400ms ease-in-out', style({ 'opacity': '1' }))
        ])
      ])
    ])
  ]
})
export class OndemandOrderRequestDailog {

  city_store: any[] = [];
  hub_store: any[] = [];
  vehicle_class_store = [];
  vehicle_classes = [];
  cities: IdName[] = [];
  hubs: any[] = [];
  availableVehicleClasses: IdName[] = [];
  selected_city_id: number = null;
  selected_city_name = 'Select A City';
  selected_vclass_id: number = null;
  selected_vclass_name = 'Select A Vehicle Class';
  selected_hub_id: number = null;
  numberOfVehicle: number = 1;
  pickupDate = null;
  pickupTime = null;
  minDate: Date = new Date();
  maxDate: Date = new Date();
  minTime: Date = new Date();
  maxTime: Date = new Date();
  minuteStep: number = 1;
  submit_button = {
    type: 'save',
    label: 'Save',
    disabled: false
  };
  dismiss_button = {
    label: 'Cancel',
    type: 'delete',
    disabled: false
  };
  alert = {
    type: '',
    messages: []
  }
  loaderStyles = {
    wrapper: {
      'text-align': 'center'
    },
    spinner: {
      'font-size': '2rem',
      color: 'var(--primary-color)'
    }
  }
  no_contracts = false;
  submitting = false;
  errorMessage: string = '';
  message: any[] = [];
  tripInstances = null;

  constructor(
    public dialogFormRef: MatDialogRef<OndemandOrderRequestDailog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private tripService: TripService,
    private bhService: BlowhornService,
    private sortBy: SortPipe,
  ) {
    this.city_store = data.city_store;
    this.hub_store = data.hubs;
  }

  ngOnInit() {
    this.submitting = true;
    this.tripService.get_ondemand_contract_details().subscribe((data: any) => {
        this.submitting = false;
        this.cities = [];
        let city_names = Object.keys(data).sort();
        for (let i = 0; i < this.city_store.length; i++) {
            if (city_names.indexOf(this.city_store[i].name) !== -1) {
                let city = new IdName();
                city.id = this.city_store[i].id;
                city.name = this.city_store[i].name;
                this.cities.push(city);
            }
        }
        this.sortBy.transform(this.cities, ['name']);
        this.vehicle_class_store = data;
    }, err => {
        this.submitting = false;
        this.no_contracts = true;
        this.errorMessage = typeof (err.error) === 'string' ? err.error : err.error.detail;
        this.alert.messages = [this.errorMessage];
        this.alert.type = 'danger';
    });
    let now = new Date();
    this.minDate = now;
    this.maxDate.setDate(now.getDate() + 3);
    this.minTime.setHours(this.minDate.getHours() + 1);
    this.minTime.setMinutes(this.minDate.getMinutes());
    this.maxTime.setHours(23);
    this.maxTime.setMinutes(55);
    this.pickupTime = this.minTime;
    this.pickupDate = this.minDate;
  }

  city_changed(event: { name: string, id: number }): void {
    if (this.selected_city_id !== event.id) {
      this.selected_city_name = event.name;
      this.selected_city_id = event.id;
      this.filter_dropdowns();
    }
  }

  filter_dropdowns(): void {
    this.hubs = [];
    this.vehicle_classes = [];
    if (this.selected_city_id) {
      const city = this.city_store
        .filter(item => item.id === this.selected_city_id)[0];
      for (const hub of city.hubs) {
        this.hubs.push(hub);
      }
      let vclasses = this.vehicle_class_store[city.name];
      if (vclasses) {
        this.vehicle_classes = vclasses.map((item: any) => {
            return {
                id: item.vehicle_classes__id,
                name: item.vehicle_classes__commercial_classification
            }
        });
      }
    }
    this.sortBy.transform(this.hubs, ['name']);
    this.sortBy.transform(this.vehicle_classes, ['name']);
  }

  vehicle_class_changed(event: { name: string, id: number }): void {
    if (this.selected_vclass_id !== event.id) {
      this.selected_vclass_name = event.name;
      this.selected_vclass_id = event.id;
    }
  }

  date_changed(): void {
    this.pickupTime = null;
    if (this.pickupDate.getDate() === new Date().getDate()) {
      this.minTime.setHours(this.pickupDate.getHours());
      this.minTime.setMinutes(this.pickupDate.getMinutes());
    } else {
      this.minTime.setHours(0);
      this.minTime.setMinutes(0);
    }
  }

  onSubmit(): void {
    let requested_time = `${moment(this.pickupDate).format('DD-MM-YYYY')} ${moment(this.pickupTime).format('HH:mm')}`;
    let data = {
      city: this.selected_city_id,
      hub: this.selected_hub_id,
      number_of_vehicles: this.numberOfVehicle,
      requested_time: requested_time,
      preferred_vehicle_class: this.selected_vclass_id,
    };
    this.submitting = true;
    this.tripService
      .raise_order_request(data)
      .subscribe((resp: any) => {
        this.alert.messages = resp.error_messages && resp.error_messages.length ?
          resp.error_messages :
          ['Request raised successfully'];
        this.alert.type = this.alert.messages.length ? 'warning' : 'success';
        this.submitting = false;
        if (!resp.error_messages.length) {
          this.close_fired_modal(resp.trips);
        }
      }, (err: any) => {
        console.log(err);
        this.submitting = false;
        this.errorMessage = typeof (err.error) === 'string' ? err.error : err.error.detail;
        this.alert.messages = [this.errorMessage];
        this.alert.type = 'danger';
      });
  }

  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '100%',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
    this.dialogFormRef.close(event);
  }

  close_alert(): void {
    this.alert.type = '';
    this.alert.messages = [];
  }
}
