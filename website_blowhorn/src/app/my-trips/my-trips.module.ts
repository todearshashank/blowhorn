import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';


import { AppRoutingModule } from '../app-routing.module';
import { MyTripsMainComponent } from './components/my-trips-main/my-trips-main.component';
import { MyTripsMapViewComponent } from './components/my-trips-map-view/my-trips-map-view.component';
import { MyTripsListViewComponent } from './components/my-trips-list-view/my-trips-list-view.component';
import { MyTripsListViewCurrentComponent } from './components/my-trips-list-view-current/my-trips-list-view-current.component';
import { MyTripsListViewCompletedComponent } from './components/my-trips-list-view-completed/my-trips-list-view-completed.component';
import { MyTripsListViewUpcomingComponent } from './components/my-trips-list-view-upcoming/my-trips-list-view-upcoming.component';
import { MyTripsListViewFilterComponent } from './components/my-trips-list-view-filter/my-trips-list-view-filter.component';
import { MyTripsListViewGridTitleComponent } from './components/my-trips-list-view-grid-title/my-trips-list-view-grid-title.component';
import { MyTripsListViewGridRowComponent } from './components/my-trips-list-view-grid-row/my-trips-list-view-grid-row.component';
import { CurrentComponent } from './components/current/current.component';
import { CurrentDetailComponent } from './components/current-detail/current-detail.component';
import { HeaderComponent } from './components/header/header.component';
import { MytripsMainComponent } from './components/mytrips-main/mytrips-main.component';
import { PastComponent } from './components/past/past.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UpcomingComponent } from './components/upcoming/upcoming.component';
import { TripModule } from '../trip/trip.module';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditTripStopsComponent } from './components/edit-trip-stops/edit-trip-stops.component';
import { DndListModule } from 'ng6-dnd-lists';
import { RouteModule } from '../route/route.module';
import { AgmDirectionModule } from 'agm-direction';
import { ContactModule } from '../contact/contact.module';
import { LocationModule } from '../location/location.module';
import { TimeslotPickerComponent } from './components/timeslot-picker/timeslot-picker.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ProofOfDeliveryComponent } from './components/proof-of-delivery/proof-of-delivery.component';
import { OndemandOrderRequestDailog } from './components/ondemand-order-request/ondemand-order-request.component';
import { OrderNotFoundComponent } from './components/order-not-found/order-not-found.component';
import { TrackingMessageComponent } from './components/tracking-message/tracking-message.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AgmCoreModule,
    AgmDirectionModule,
    AppRoutingModule,
    BsDatepickerModule,
    TimepickerModule.forRoot(),
    NgxMaterialTimepickerModule,
    FormsModule,
    TripModule,
    NgxQRCodeModule,
    NgbModule,
    DndListModule,
    RouteModule,
    LocationModule,
    ContactModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
  ],
  declarations: [
    MyTripsMainComponent,
    MyTripsMapViewComponent,
    MyTripsListViewComponent,
    MyTripsListViewCurrentComponent,
    MyTripsListViewCompletedComponent,
    MyTripsListViewUpcomingComponent,
    MyTripsListViewFilterComponent,
    MyTripsListViewGridTitleComponent,
    MyTripsListViewGridRowComponent,
    CurrentComponent,
    CurrentDetailComponent,
    HeaderComponent,
    MytripsMainComponent,
    PastComponent,
    SidebarComponent,
    UpcomingComponent,
    EditTripStopsComponent,
    TimeslotPickerComponent,
    ProofOfDeliveryComponent,
    OndemandOrderRequestDailog,
    OrderNotFoundComponent,
    TrackingMessageComponent,
    ],
    entryComponents: [
        OndemandOrderRequestDailog
    ]
})
export class MyTripsModule { }
