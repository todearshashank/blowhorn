import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { query } from '@angular/animations';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { CustomerResourceAllocation } from '../../../shared/models/customer-resource-allocation.model';
import { all } from 'async';
import * as moment from 'moment';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  is_assign_route = false;
  allocations: CustomerResourceAllocation[] = [];
  cities: string[] = [];
  filter_word = '';

  constructor(
    private _apollo: Apollo,
    public _blowhorn_service: BlowhornService,
  ) { }

  ngOnInit() {
    this.fetch_resource_allocations();
  }

  fetch_resource_allocations() {
    this._blowhorn_service.show_loader();
    let profile = this._blowhorn_service.user_profile;
    if (!profile) {
      setTimeout(() => {
        this.fetch_resource_allocations();
      }, 2000);
    } else {
      this.get_allocations(profile);
    }
  }

  get_allocations(profile: any) {
    this._apollo.watchQuery<any>({
      query: gql`
      {
        customerResourceAllocation( contract_Customer_Name_Iexact: "${profile.organization_name}") {
          edges {
            node {
              id
              shiftStartTime
              hub {
                name
                city {
                  name
                }
              }
              resourceSet {
                id
                route {
                  name
                }
                driver {
                  user {
                    name
                    phoneNumber
                  }
                  vehicles {
                    registrationCertificateNumber
                    vehicleModel {
                      modelName
                    }
                  }
                  tripSet (status_In: "In-Progress"){
                    edges {
                      node {
                        id
                        tripNumber
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      `,
      fetchPolicy: 'network-only'
    }).valueChanges.subscribe(result => {
      let node: any, resources: any;
      result.data.customerResourceAllocation.edges.forEach(edge => {
        node = edge.node;
        if (!this.cities.includes(node.hub.city.name)) {
          this.cities.push(node.hub.city.name);
        }
        resources = node.resourceSet;
        resources.forEach(item => {
          const allocation = {
            id: null,
            shift_time: moment(node.shiftStartTime).format('HH:mm A'),
            city: node.hub.city.name,
            hub: node.hub.name,
            driver_name: '',
            driver_number: '',
            on_trip: false,
            route_name: '',
            vehicle_model: '',
            vehicle_number: ''
          };
          allocation.id = item.id;
          const driver  = item.driver;
          allocation.on_trip = driver.tripSet.edges.length > 0 ? true : false;
          allocation.driver_name = driver.user.name;
          allocation.driver_number =
            (driver.user.phoneNumber && driver.user.phoneNumber.startsWith('+91'))
            ? driver.user.phoneNumber.substring(3) : driver.user.phoneNumber;
          allocation.vehicle_model = driver.vehicles.length ? driver.vehicles[0].vehicleModel.modelName : '';
          allocation.vehicle_number = driver.vehicles.length ? driver.vehicles[0].registrationCertificateNumber : '';
          allocation.route_name = item.route ? item.route.name : '';
          this.allocations.push(allocation);
        });
      });
      this._blowhorn_service.hide_loader();
    });
  }

  enable_assign_route(val: boolean) {
    this.is_assign_route = val;
  }

}
