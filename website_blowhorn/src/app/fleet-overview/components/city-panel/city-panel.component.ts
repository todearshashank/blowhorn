import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CustomerResourceAllocation } from '../../../shared/models/customer-resource-allocation.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Component({
  selector: 'app-city-panel',
  templateUrl: './city-panel.component.html',
  styleUrls: ['./city-panel.component.scss']
})
export class CityPanelComponent implements OnInit {

  @Input()
  records: CustomerResourceAllocation[] = [];
  @Input()
  disable_assign_route = false;
  @Output()
  is_assign_route_fired: EventEmitter<boolean>;
  @Input()
  city: string;
  is_assign_route = false;
  no_route_message: string;
  routes: { name: string, id: string, pickup: string, dropoff: string, stop_count: number }[] = [];
  filter_text = '';
  clicked_index = null;
  collapsed = false;
  show_confirm = false;
  selected_record: CustomerResourceAllocation = null;
  selected_route: { name: string, id: string, pickup: string, dropoff: string, stop_count: number } = null;
  confirm_message: string;

  constructor(
    public _blowhorn_service: BlowhornService,
    private _apollo: Apollo,
  ) {
    this.is_assign_route_fired = new EventEmitter<boolean>(false);
  }

  ngOnInit() {
    this.no_route_message = `No route configured for city : ${this.city}`;
  }

  assign_route(index: number) {
    const customerName = this._blowhorn_service.orgnization_name;
    this._apollo.watchQuery<any>({
      query: gql`
      query {
        customerRoutes(city: "${this.city}", customer_Name: "${customerName}") {
          edges {
            node {
              id
              name
              stops {
                id
                seqNo
                line1
                line3
              }
            }
          }
        }
      }
      `,
      fetchPolicy: 'network-only'
    }).valueChanges.subscribe(result => {
      this.routes = [];
      result.data.customerRoutes.edges.forEach(edge => {
        const node = edge.node;
        const pickup_stop = node.stops.find(stop => stop.seqNo === 0);
        const dropoff_stop = node.stops.find(stop => stop.seqNo === node.stops.length - 1);
        this.routes.push({
          id: node.id,
          name: node.name,
          pickup: pickup_stop.line1.split(pickup_stop.line3)[0],
          dropoff: dropoff_stop.line1.split(dropoff_stop.line3)[0],
          stop_count: node.stops.length - 2
        });
      });
      this.is_assign_route = true;
      this.is_assign_route_fired.next(true);
      this.clicked_index = index;
    });
  }

  select_route(
    record: CustomerResourceAllocation,
    route: { name: string, id: string, pickup: string, dropoff: string, stop_count: number }) {
      this.selected_route = route;
      this.selected_record = record;
      this.show_confirm = true;
      this.confirm_message = this.selected_record.route_name ?
        `Existing route: ${this.selected_record.route_name} will be replaced by route: ${this.selected_route.name}. Are you sure?` :
        `New route: ${this.selected_route.name} will be assigned. Are you sure?`;
  }

  remove_route(
    record: CustomerResourceAllocation,
    ) {
      this.selected_record = record;
      this.show_confirm = true;
      this.confirm_message = 'Route will be removed. Are you sure?';
  }

  close_routes() {
    this.is_assign_route_fired.next(false);
    this.is_assign_route = !this.is_assign_route;
    this.clicked_index = null;
    this.selected_route = null;
    this.selected_record = null;
    this.confirm_message = null;
    this.filter_text = '';
  }

  proceed(event) {
    const route_id = this.selected_route ? this.selected_route.id : '';
    if (event) {
      const mutation = gql`
        mutation {
          updateResourceRoute(resourceId: "${this.selected_record.id}", routeId: "${route_id}") {
            message,
            status
          }
        }
      `;
      this._apollo.mutate({
        mutation: mutation
      }).subscribe(result => {
        if (result.data.updateResourceRoute.status === 200) {
          this.selected_record.route_name = this.selected_route ? this.selected_route.name : '';
        }
        this.close_routes();
        this.show_confirm = false;
      },
      err => {
        this.show_confirm = false;
      });
    } else {
      this.show_confirm = false;
    }
  }

}
