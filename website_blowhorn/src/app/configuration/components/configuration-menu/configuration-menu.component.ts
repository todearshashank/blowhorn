import { Component, OnInit } from '@angular/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configuration-menu',
  templateUrl: './configuration-menu.component.html',
  styleUrls: ['./configuration-menu.component.scss']
})
export class ConfigurationMenuComponent implements OnInit {

  c2c_sme_view = false;

  constructor(
    public _blowhorn_service: BlowhornService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.c2c_sme_view = this._router.url.startsWith('/dashboard/mytrips')
        || !this._blowhorn_service.is_business_user;
  }

}
