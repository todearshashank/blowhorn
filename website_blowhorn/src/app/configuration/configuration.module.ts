import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationMainComponent } from './components/configuration-main/configuration-main.component';
import { SharedModule } from '../shared/shared.module';
import { RouteModule } from '../route/route.module';
import { ConfigurationMenuComponent } from './components/configuration-menu/configuration-menu.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    RouteModule
  ],
  declarations: [
    ConfigurationMainComponent,
    ConfigurationMenuComponent,
  ],
})
export class ConfigurationModule { }
