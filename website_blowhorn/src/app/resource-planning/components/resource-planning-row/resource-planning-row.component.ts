import { ResourcePlanning } from './../../../shared/models/resource-planning.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-resource-planning-row',
  templateUrl: './resource-planning-row.component.html',
  styleUrls: ['./resource-planning-row.component.scss']
})
export class ResourcePlanningRowComponent implements OnInit {

  @Input() row: ResourcePlanning;
  @Input() show_city = true;
  @Output() rowClicked: EventEmitter<any>;
  @Output() actionClicked: EventEmitter<{modal: string, row: ResourcePlanning}>;
  startTime: any;
  endTime: any;
  actions = {
    coverage: 'coverage',
    pincodes: 'pincodes'
  }

  constructor() {
    this.rowClicked = new EventEmitter<any>();
    this.actionClicked = new EventEmitter<{modal: string, row: ResourcePlanning}>();
  }

  ngOnInit() {
    let now = moment(new Date()).format('DD-MM-YYYY');
    this.startTime = moment(`${now} ${this.row.start_time}`, 'DD-MM-YYYY HH:mm:ss').format('HH:mm');
    this.endTime = moment(`${now} ${this.row.end_time}`, 'DD-MM-YYYY HH:mm:ss').format('HH:mm');
  }

  showDriverDetails() {
    this.rowClicked.emit(this.row);
  }

  sendAction(modal: string) {
    this.actionClicked.emit({modal: modal, row: this.row});
  }

}
