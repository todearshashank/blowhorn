import { ResourcePlanningService } from './../../../shared/services/resource-planning.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { IdName } from '../../../shared/models/id-name.model';

@Component({
  selector: 'app-resource-planning-filter',
  templateUrl: './resource-planning-filter.component.html',
  styleUrls: ['./resource-planning-filter.component.scss']
})
export class ResourcePlanningFilterComponent implements OnInit {

  cities: IdName[] = [];
  hubs: IdName[] = [];
  vehicle_classes: IdName[] = [];
  selected_city_id: number = null;
  selected_city_name = 'Select City';

  selected_hub_id: number = null;
  selected_hub_name = 'Select Hub';

  selected_vclass_id: number = null;
  selected_vclass_name = 'Select Vehicle Type';
  currentView: string = 'group';

  @Output() filter_changed: EventEmitter<{ url: string, data: any }>;
  @Output() view_changed: EventEmitter<string>;

  constructor(
    private rpService: ResourcePlanningService,
  ) {
    this.filter_changed = new EventEmitter<{ url: string, data: any }>();
    this.view_changed = new EventEmitter<string>();
    this.rpService.city_store.subscribe((data: any) => {
      if (data) {
        this.cities = data;
      }
    });

    this.rpService.hub_store.subscribe((data: any) => {
      if (data) {
        this.hubs = data;
      }
    });
    this.rpService.vehicle_class_store.subscribe((data: any) => {
      if (data) {
        this.vehicle_classes = data.map((i: any) => {
          return {
            name: i.commercial_classification,
            id: i.id
          }
        });
      }
    });
  }

  ngOnInit() {
    this.changeView('group');
  }

  city_changed(event: { name: string, id: number }): void {
    if (this.selected_city_id !== event.id) {
      this.selected_city_name = event.name;
      this.selected_city_id = event.id;
      const url = this.get_url();
      this.filter_changed.emit({ url: url, data: this.get_data() });
    }
  }

  hub_changed(event: { name: string, id: number }): void {
    if (this.selected_hub_id !== event.id) {
      this.selected_hub_name = event.name;
      this.selected_hub_id = event.id;
      const url = this.get_url();
      this.filter_changed.emit({url: url, data: this.get_data()});
    }
  }

  vclass_changed(event: { name: string, id: number }): void {
    if (this.selected_vclass_id !== event.id) {
      this.selected_vclass_name = event.name;
      this.selected_vclass_id = event.id;
      const url = this.get_url();
      this.filter_changed.emit({url: url, data: this.get_data()});
    }
  }

  reset_city(): void {
    this.selected_city_id = null;
    this.selected_city_name = 'All Cities';
  }

  reset_hub(): void {
    this.selected_hub_id = null;
    this.selected_hub_name = 'All Hubs';
  }

  get_url(): string {
    let url = '';
    url += this.selected_city_id ? `&city_id=${this.selected_city_id}` : '';
    url += this.selected_hub_id ? `&hub_id=${this.selected_hub_id}` : '';
    url += this.selected_vclass_id ? `&vehicle_class_id=${this.selected_vclass_id}` : '';
    return url;
  }

  get_data(): any {
    return {
        city: this.selected_city_id ? this.selected_city_name : '',
        hub: this.selected_hub_id ? this.selected_hub_name : '',
        vehicel_class: this.selected_vclass_id ? this.selected_vclass_name : ''
    }
  }

  changeView(view: string) {
    if (this.currentView !== view) {
        this.currentView = view;
        this.view_changed.emit(view);
    }
  }

  //  Styles
  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        border: 'none',
        height: '30px',
        width: '200px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        width: '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get hub_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        border: 'none',
        height: '30px',
        width: '250px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        width: '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

}
