import { ResourcePlanning } from './../../../shared/models/resource-planning.model';
import { IdName } from './../../../shared/models/id-name.model';
import { ResourcePlanningService } from './../../../shared/services/resource-planning.service';
import { BlowhornService } from './../../../shared/services/blowhorn.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../shared/utils/constants';
import { BlowhornPaginationComponent } from '../../../shared/components/blowhorn-pagination/blowhorn-pagination.component';

@Component({
  selector: 'app-resource-planning-list',
  templateUrl: './resource-planning-list.component.html',
  styleUrls: ['./resource-planning-list.component.scss']
})
export class ResourcePlanningListComponent implements OnInit {

    @ViewChild('pagination', {static: false})
    pagination: BlowhornPaginationComponent;
  resourcePlannings: any[] = [];
  count: number;
  next: string;
  previous: string;
  baseUrl = Constants.urls.API_RESOURCE_PLANNING;
  showDetails = false;
  selectedRow = {};
  currentView = 'group';
  masterCityStore: string[] = [];
  cities: string[] = [];
  filterData: any = {};
  loadingData = false;
  modals = {
    coverage: false,
    pincodes: false
  };

  constructor(
    private rpService: ResourcePlanningService,
  ) {
    this.rpService.city_store.subscribe((data: any) => {
      if (data) {
        this.cities = data.map((item: IdName) => item.name);
      }
    });
    this.filterChanged({
      url: '',
      data: {
        city: '',
        hub: '',
        vehicle_class: ''
      }
    });
  }

  ngOnInit() {
  }

  updateList(data: any): void {
    data.forEach((item: any) => {
      this.resourcePlannings.push(ResourcePlanning.fromJson(item));
    });
  }

  update_records(data: {count: number, next: string, previous: string, results: []}) {
    this.count = data.count;
    this.next = data.next;
    this.previous = data.previous;
    this.updateList(data.results);
}

  filterChanged(event: { url: string, data: any }): void {
    this.resourcePlannings = [];
    this.filterData = event.data;
    this.loadingData = true;
    this.update_records({
        count: 0,
        next: '',
        previous: '',
        results: []
    });
    this.rpService.get_slots(`${this.baseUrl}?${event.url}`).subscribe(data => {
        this.update_records(data);
        this.loadingData = false;
    },
    err => {
        this.loadingData = false;
        console.error('Error', err);
    });
  }

  viewChanged(event: string) {
    this.currentView = event;
  }

  rowClicked(event: any) {
    this.showDetails = true;
    this.selectedRow = event;
  }

  closeDetails(event: any) {
    this.showDetails = false;
    this.selectedRow = null;
  }

  actionClicked(event: {modal: string, row: ResourcePlanning}) {
    this.selectedRow = event.row;
    this.modals[event.modal] = true;
  }

  closeClicked(event: string) {
    this.modals[event] = false;
  }

}
