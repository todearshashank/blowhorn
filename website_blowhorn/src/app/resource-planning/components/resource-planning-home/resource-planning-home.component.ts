import { ResourcePlanningService } from './../../../shared/services/resource-planning.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resource-planning-home',
  templateUrl: './resource-planning-home.component.html',
  styleUrls: ['./resource-planning-home.component.scss']
})
export class ResourcePlanningHomeComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public rpService: ResourcePlanningService,
  ) {
    route.data.subscribe(data => {
        console.log(data);
        if (data) {
            let resp = data.resource_planning;
            this.rpService.update_city_store(resp.cities);
            this.rpService.update_hub_store(resp.hubs);
            this.rpService.update_vehicle_class_store(resp.vehicle_classes);
        }
    },
    err => {
        console.error(err);
    });
  }

  ngOnInit() {
  }

}
