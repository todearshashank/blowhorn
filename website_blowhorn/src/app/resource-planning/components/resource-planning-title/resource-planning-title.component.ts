import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-resource-planning-title',
  templateUrl: './resource-planning-title.component.html',
  styleUrls: ['./resource-planning-title.component.scss']
})
export class ResourcePlanningTitleComponent implements OnInit {

  @Input() show_city = true;
  constructor() { }

  ngOnInit() {
  }

}
