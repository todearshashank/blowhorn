import { ResourcePlanningService } from './../../../shared/services/resource-planning.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-utilization-details',
  templateUrl: './utilization-details.component.html',
  styleUrls: ['./utilization-details.component.scss']
})
export class UtilizationDetailsComponent implements OnInit {

  @Input() row: any;
  @Output() closed: EventEmitter<boolean>;
  loadingData = false;
  results = [];
  minLoaderStyles = {

  };
  constructor(
    private rpService: ResourcePlanningService,
  ) {
    this.closed = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.results = [];
    this.loadingData = true;
    this.rpService.get_driver_details(this.row.pk).subscribe((data: any) => {
        this.loadingData = false;
        this.results = data;
    }, err => {
        this.loadingData = false;
    });
  }

  closeModal() {
    this.closed.emit(true);
  }

  openTracking(tripNumber: string) {
    window.open(`/track/${tripNumber}`);
  }
}
