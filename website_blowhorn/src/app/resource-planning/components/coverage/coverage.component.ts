import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MapService } from '../../../shared/services/map.service';

@Component({
    selector: 'app-coverage',
    templateUrl: './coverage.component.html',
    styleUrls: ['./coverage.component.scss']
})
export class CoverageComponent implements OnInit {

    @Input() row: any;
    @Output() closed: EventEmitter<boolean>;
    polygon = {
        coverage: {
            path: [],
            config: {
                strokeColor: '#1de405',
                fillColor: '#9bf290',
                fillOpacity: '0.2'
            }
        },
        extended_coverage: {
            path: [],
            config: {
                strokeColor: '#57d3f6',
                fillColor: '#57d3f6',
                fillOpacity: '0.2'
            }
        }
    }
    errorMessage: string = '';
    showCoverage = true;
    showExtendedCoverage = true;
    allCoverage = true;

    mapLatitude = 12.9716;
    mapLongitude = 77.5946;
    mapBounds: google.maps.LatLngBounds;

    constructor() {
        this.closed = new EventEmitter<boolean>();
    }

    ngOnInit() {
        this.mapBounds = MapService.dummy_bounds;
        this.setCoverage('coverage');
        this.setCoverage('extended_coverage');
        if (!this.row.coverage && this.row.extended_coverage) {
            this.errorMessage = 'No service area configured.'
        }
    }

    setCoverage(field: string) {
        let points = this.row[field];
        if (points && points.length) {
            this.polygon[field].path = points.map((item: any) => {
                return {
                    lat: item[1],
                    lng: item[0],
                }
            })
        }
        this.setMapBounds(field);
    }

    setMapBounds(field: string) {
        this.polygon[field].path.forEach((item: google.maps.LatLng | google.maps.LatLngLiteral) => {
            this.mapBounds.extend(item);
        });
    }

    showPolygon(field: string) {
        if (!field) {
            this.setCoverage('coverage');
            this.setCoverage('extended_coverage');
        } else {
            this.setCoverage(field);
            if (!this.allCoverage) {
                let clear_field = field === 'coverage' ? 'extended_coverage' : 'coverage';
                this.polygon[clear_field].path = []
            }
        }
    }

    updateShowAllCoverage() {
        this.allCoverage = this.showCoverage && this.showExtendedCoverage;
        if (this.showCoverage) {
            this.showPolygon('coverage');
        }
        if (this.showExtendedCoverage) {
            this.showPolygon('extended_coverage');
        }
    }

    showSomeCoverage(): boolean {
        return [this.showCoverage, this.showExtendedCoverage].some(el => el) && !this.allCoverage;
    }

    showAllCoverage(show: boolean) {
        this.allCoverage = show;
        this.showCoverage = show;
        this.showExtendedCoverage = show;
    }

    closeModal() {
        this.closed.emit(true);
    }

}
