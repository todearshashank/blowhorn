import { ResourcePlanning } from './../../../shared/models/resource-planning.model';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-detail-modal',
  templateUrl: './detail-modal.component.html',
  styleUrls: ['./detail-modal.component.scss']
})
export class DetailModalComponent implements OnInit {

    @Input() row: ResourcePlanning;
    @Output() closed: EventEmitter<boolean>;

  constructor() {
    this.closed = new EventEmitter<boolean>();
  }

  ngOnInit() {
    console.log(this.row);

  }

  closeModal() {
    this.closed.emit(true);
}

}
