import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ResourcePlanning } from '../../../shared/models/resource-planning.model';

@Component({
  selector: 'app-resource-planning-accordion',
  templateUrl: './resource-planning-accordion.component.html',
  styleUrls: ['./resource-planning-accordion.component.scss']
})
export class ResourcePlanningAccordionComponent implements OnInit {

  @Input()
  records: any[] = [];

  @Input()
  city: string;

  @Output() selectedRow: EventEmitter<any>;
  @Output() actionEmit: EventEmitter<{modal: string, row: ResourcePlanning}>;

  filter_text = '';
  clicked_index = null;
  collapsed = false;

  constructor() {
    this.selectedRow = new EventEmitter<any>();
    this.actionEmit = new EventEmitter<{modal: string, row: ResourcePlanning}>();
  }

  ngOnInit() {
  }

  rowClicked(event: any) {
    this.selectedRow.emit(event);
  }

  actionClicked(event: any) {
    this.actionEmit.emit(event);
  }

  toggleGroup() {
    this.collapsed = !this.collapsed;
  }

}
