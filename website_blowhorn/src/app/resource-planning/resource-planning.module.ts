import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { ResourcePlanningHomeComponent } from './components/resource-planning-home/resource-planning-home.component';
import { ResourcePlanningFilterComponent } from './components/resource-planning-filter/resource-planning-filter.component';
import { ResourcePlanningListComponent } from './components/resource-planning-list/resource-planning-list.component';
import { ResourcePlanningTitleComponent } from './components/resource-planning-title/resource-planning-title.component';
import { ResourcePlanningRowComponent } from './components/resource-planning-row/resource-planning-row.component';
import { AppRoutingModule } from '../app-routing.module';
import { UtilizationDetailsComponent } from './components/utilization-details/utilization-details.component';
import { ResourcePlanningAccordionComponent } from './components/resource-planning-accordion/resource-planning-accordion.component';
import { CoverageComponent } from './components/coverage/coverage.component';
import { DetailModalComponent } from './components/detail-modal/detail-modal.component';


@NgModule({
  declarations: [ResourcePlanningHomeComponent, ResourcePlanningFilterComponent, ResourcePlanningListComponent, ResourcePlanningTitleComponent, ResourcePlanningRowComponent, UtilizationDetailsComponent, ResourcePlanningAccordionComponent, CoverageComponent, DetailModalComponent],
  imports: [
    CommonModule,
    NgbModule,
    AppRoutingModule,
    SharedModule,
    AgmCoreModule,
    FormsModule,
    MatCheckboxModule
  ]
})
export class ResourcePlanningModule { }
