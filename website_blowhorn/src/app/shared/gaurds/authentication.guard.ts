import { BlowhornService } from './../services/blowhorn.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

declare var is_authenticated: any;


@Injectable({
    providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
    constructor(
        private bhService: BlowhornService,
        private router: Router,
    ) { }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        console.log('Checking...', is_authenticated, next.routeConfig.path);
        if (is_authenticated) {
            return true
        } else {
            // delete token
            this.bhService.delete_token();
            setTimeout(() => window.open('/accounts/logout', '_self'), 400)
            return false;
        }
    }

}
