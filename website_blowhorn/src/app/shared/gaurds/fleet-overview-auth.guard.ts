import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BlowhornService } from '../services/blowhorn.service';


@Injectable({
  providedIn: 'root'
})
export class FleetOverviewAuthGuard implements CanActivate {

  constructor(
    public _blowhorn_service: BlowhornService,
    private _router: Router,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this._router.url.startsWith('/dashboard/mytrips')
      || !this._blowhorn_service.is_business_user) {
        this._router.navigate(['dashboard', 'configuration', 'routes']);
        return false;
      } else {
        return true;
      }
  }
}
