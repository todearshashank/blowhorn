import * as moment from 'moment';

const API_URL = '/api';
const default_build = 'blowhorn';
const FILTER_TODAY = 0;
const FILTER_LAST_7_DAYS = 7;
const FILTER_LAST_30_DAYS = 30;
declare var current_build: string;

export const Constants = {
    default_build: default_build,
    urls: {
        API_ROUTES_OPT: `${API_URL}/routeoptimiser/hubtodelivery/`,
        API_FLEET_ROUTES_OPT: `${API_URL}/routeoptimiser/fleetdashboardroutes/`,
        API_LIST_ACTIVE_DRIVERS: `${API_URL}/drivers/allactive`,
        API_URL_LOC: 'assets',
        API_USER_PROFILE: `${API_URL}/profile/`,
        API_USER_PASSWORD: '/accounts/password/change/',
        API_CUSTOMER_PASSWORD: `${API_URL}/customer/change-password`,
        API_CUSTOMER_BANK_ACCOUNTS: `${API_URL}/customer/bank-accounts`,
        API_DELETE_CUSTOMER_DATA: `${API_URL}/customer/data`,
        API_RAZORPAY_IFSC: 'https://ifsc.razorpay.com/',

        API_BOOKING_TIMESLOTS: `${API_URL}/slots/`,
        API_ALL_TRIPS: `${API_URL}/orders`,
        API_BOOKING_RESCHEDULE: `${API_URL}/customers/v2/reschedule/`,
        API_SUMMARY_TRIPS: `${API_URL}/summary`,
        API_DETAIL_TRIP: `${API_URL}/order`,
        API_QUICKBOOK: `${API_URL}/quickbook`,
        API_REBOOK: `${API_URL}/rebook`,
        API_RAZORPAY: `${API_URL}/razorpay`,
        API_WALLET_PAYMENT: `${API_URL}/customers/v5/order/bwallet/withdraw/`,
        API_MASK_CALL: `${API_URL}/customer/mask-call`,

        API_LOCATIONS: `${API_URL}/location`,
        API_PARAMS: `/parameters`,
        API_SHARE_INVOICE: `${API_URL}/invoice`,
        API_FIREBASE_TOKEN: `${API_URL}/firebasetoken`,
        API_PAYTM_PAYMENT: `${API_URL}/paytm`,
        API_PAYFAST_PAYMENT: `${API_URL}/payfast/checkout/`,
        API_ALT_ALL_TRIPS: `${API_URL}/trips`,
        API_ALT_DETAIL_TRIP: `${API_URL}/trip`,
        API_FEEDBACK: `${API_URL}/customers/v2/drivers/rating/`,
        API_ORDER_STOPS: `${API_URL}/order`,
        API_ALT_TRIP_SUMMARY:  `${API_URL}/trip/summary`,
        API_TRIP_FILTER: `${API_URL}/fleet/search/load-choices`,
        API_TRIP_BASE: `${API_URL}/fleet/trips`,
        API_TRIPS: `${API_URL}/fleet/trips/?ordering=-planned_start_time`,
        API_TRIP_ONDEMAND_CONTRACTS: `${API_URL}/fleet/order/request/contracts`,
        API_TRIP_RAISE_REQUEST: `${API_URL}/fleet/order/request/`,
        API_COMPLETED_TRIPS: `${API_URL}/fleet/trips/?ordering=-planned_start_time&status=Completed`,
        API_UPCOMING_TRIPS: `${API_URL}/fleet/trips/?ordering=planned_start_time&status=New`,
        API_EXPORT_CURRENT_TRIPS: `${API_URL}/fleet/trips?output=xlsx&ordering=planned_start_time&status=In-Progress`,
        API_EXPORT_COMPLETED_TRIPS: `${API_URL}/fleet/trips?output=xlsx&ordering=-planned_start_time&status=Completed`,
        API_EXPORT_UPCOMING_TRIPS: `${API_URL}/fleet/trips?output=xlsx&ordering=planned_start_time&status=New`,
        API_CURRENT_MY_TRIPS: `${API_URL}/mytrips/trips/?ordering=pickup_datetime&status=current`,
        API_COMPLETED_MY_TRIPS: `${API_URL}/mytrips/trips/?ordering=-pickup_datetime&status=completed`,
        API_UPCOMING_MY_TRIPS: `${API_URL}/mytrips/trips/?ordering=pickup_datetime&status=upcoming`,
        API_EXPORT_CURRENT_MY_TRIPS: `${API_URL}/mytrips/trips/export?output=xlsx&ordering=pickup_datetime&status=current`,
        API_EXPORT_COMPLETED_MY_TRIPS: `${API_URL}/mytrips/trips/export?output=xlsx&ordering=-pickup_datetime&status=completed`,
        API_EXPORT_UPCOMING_MY_TRIPS: `${API_URL}/mytrips/trips/export?output=xlsx&ordering=pickup_datetime&status=upcoming`,
        API_TRIP_STOP_TEMPLATE_EXPORT: `${API_URL}/trip/stops/export`,
        API_TRIP_STOP_TEMPLATE_IMPORT: `${API_URL}/trip/stops/import`,
        API_SHIPMENTS: `${API_URL}/fleet/shipments?ordering=-date_placed`,
        API_SHIPMENT_FILTER: `${API_URL}/fleet/search/shipments-choices`,
        API_SHIPMENT_ASSETS: `${API_URL}/fleet/shipment/assets`,
        API_SHIPMENT_ORDERLINES: `${API_URL}/fleet/shipment/items`,
        API_SHIPMENT_HUB: `${API_URL}/customer/hubdetails`,
        API_SHIPMENT_HOMEDELIVERY: `${API_URL}/orders/shipment`,
        API_SHIPMENT_ORDERDETAILS: `${API_URL}/customer/orderline/information`,
        API_SHIPMENT_SLOT_DRIVERS: `${API_URL}/fleet/shipment/slots/drivers`,
        API_SHIPMENT_ASSIGN_DRIVER: `${API_URL}/fleet/shipment/driver/assign`,
        API_SHIPMENT_COD_DETAILS: `${API_URL}/fleet/cod-payments`,
        API_EXPORT_SHIPMENTS: `${API_URL}/fleet/shipments/export?`,
        API_ORDER_HEATMAP: `${API_URL}/order/heatmap`,
        API_ORDER_HEATMAP_FILTERS: `${API_URL}/order/heatmap-filters`,
        // Shipment Order Line Crud
        API_SHIPMENT_ORDERLINES_CRUD: `${API_URL}/orders/dashboard/orderline/`,
        API_SHIPPING_LABEL: `${API_URL}/order/shippinglabel/`,

        // Template
        API_ALL_CUSTOMERS: `${API_URL}/customers/shipment`,
        API_SHIPMENT_ORDER_TEMPLATE_IMPORT: `${API_URL}/order/import`,
        API_SHIPMENT_ORDER_TEMPLATE_EXPORT: `${API_URL}/order/export`,
        API_SHIPMENT_ORDERLINE_TEMPLATE_IMPORT: `${API_URL}/order/lineitems/import`,
        // Members
        API_MEMBERS: `${API_URL}/fleet/members?page_size=50&ordering=-pk`,
        API_GROUPS: `${API_URL}/fleet/member/groups`,
        API_INVITE_MEMBER: `${API_URL}/fleet/member/invite`,
        API_MEMBER_SUPPORTDATA: `${API_URL}/fleet/member/supportdata`,
        API_MEMBER_PERMISSION_LEVEL: `${API_URL}/fleet/member/permissionlevel`,
        API_MEMBER_USER_PERMISSIONS: `${API_URL}/fleet/member/permissions`,

        API_CREATE_BLANKET_TRIP: `${API_URL}/orders/shipmentorder/createblankettrip/`,
        // Enterprise Invoice
        API_INVOICE_SUPPORT_DATA: `${API_URL}/customer/invoice/enterprise/supportdata`,
        API_INVOICES: `${API_URL}/customer/invoice/enterprise/list/?`,
        API_INVOICE_ACTION: `${API_URL}/customer/invoice/enterprise/action`,
        API_INVOICE_DOWNLOAD: `api/customer/invoice/enterprise/download`,
        // Admin Live Tracking
        API_TRACKING_PARAMS : `${API_URL}/livetracking/params`,
        API_DRIVER_LOCATIONS: `${API_URL}/livetracking/driverlocations`,
        API_DRIVER_TRIP_DETAILS: `${API_URL}/livetracking/trip-details`,
        API_DRIVER_PROFILE: `${API_URL}/livetracking/profile`,
        API_DRIVER_ACTION: `${API_URL}/livetracking/action`,
        API_LIVETRACKING_RESOURCE_DETAILS: `${API_URL}/livetracking/resource-allocation-details`,

        API_SAVE_LOCATION: `${API_URL}/location`,
        API_CUSTOMER_ROUTE: `${API_URL}/customer/route`,
        API_UNASSIGNED_SHIPMENT_ORDRS: `${API_URL}/orders/unassigned-shipment`,
        API_GRAPHQL: `graphql`,
        API_KIOSK_CONTRACT_PACKAGE: `${API_URL}/kiosk/profile`,
        API_KIOSK_ORDER: `${API_URL}/kiosk/order/`,

        // FLASH SALE
        API_FLASHSALE_CREATE_ORDER: `${API_URL}/order/flash-sale/`,

        // Admin Resource Allocation
        API_RESOURCE_SUPPORT_DATA: `${API_URL}/resourceallocation/data`,
        API_RESOURCE_CONTRACTS: `${API_URL}/resourceallocation/contract?ordering=name`,
        API_RESOURCE_RESOURCES: `${API_URL}/resourceallocation/resources`,
        API_RESOURCE_RESOURCE_DELETE: `${API_URL}/resourceallocation/resources/remove/`,
        API_RESOURCE_ALLOCATION_HISTORY: `${API_URL}/resourceallocation/contract/resources/history`,
        API_ORDER_REPORTS: `${API_URL}/fleet/report/orderreports`,
        API_STATUSWISE_COUNTS: `${API_URL}/fleet/report/statuswisecount`,
        API_CODORDER_STATUS: `${API_URL}/fleet/report/codorderstatus`,
        API_SHIPMENT_DELIVERY: `${API_URL}/fleet/report/shipmentdelivery`,
        API_HUBWISE_DATA: `${API_URL}/fleet/report/hubdata`,
        API_ORDERCREATED_TIME: `${API_URL}/fleet/report/ordercreatedtime`,

        // Resource Planning
        API_RESOURCE_PLANNING_SUPPORT_DATA: `${API_URL}/fleet/slots/load-filter`,
        API_RESOURCE_PLANNING: `${API_URL}/fleet/slots`,

        // Error Reporting
        API_REPORT_ERROR: `${API_URL}/angular/error-report`,
        FIREBASE_OCCUPIED_DRIVERS: '/occupied_drivers',

        // Report Scheduler Service
        API_REPORT_SCHEDULE: `${API_URL}/report/schedules`,
        API_REPORT_SCHEDULE_ADD: `${API_URL}/customer/report/schedule/add/`,
        API_REPORT_SCHEDULE_DELETE: `${API_URL}/customer/report/schedule/delete/`,
        API_REPORT_TYPES : `${API_URL}/report/schedules/supportdata`,
        API_REPORT_SCHEDULE_USER_SUBS_UPDATE: `${API_URL}/customer/report/schedule/update/`,

        // Channel Integration Service
        API_CHANNEL_SUBSCRIPTION: `${API_URL}/customer/channel/`,
        API_CHANNEL_KEY: `${API_URL}/customer/key/`,
        API_CHANNEL_SYNCORDERS: `${API_URL}/customer/syncorder/`,

        // Awb Generator Service
        API_AWBGENERATOR_AWBS: `${API_URL}/orders/advanceawb/`,
        API_AWBGENERATOR_AWBLIST: `${API_URL}/orders/advanceawblist/`,
        API_HUBLIST: `${API_URL}/hubs/`,


        // Shipment Analytics Service
        API_SHIPMENTREPORT_STATUS: `${API_URL}/report/order/status`,
        API_SHIPMENTREPORT_TIMEPLACED: `${API_URL}/report/order/timeplaced`,
        API_SHIPMENTREPORT_PAYMENTMODE: `${API_URL}/report/order/paymentmode`,
        API_SHIPMENTREPORT_ATTEMPTS: `${API_URL}/report/order/attempts`,
        API_SHIPMENTREPORT_PLACED: `${API_URL}/report/order/placed`,
        API_SHIPMENTREPORT_DELIVERYPERFORMANCE: `${API_URL}/report/order/deliveryperformance`,
        API_SHIPMENTREPORT_SAMEDAYDELIVERY: `${API_URL}/report/order/samedaydelivery`,
        API_SHIPMENTREPORT_SLA: `${API_URL}/report/order/sla`,
        API_SHIPMENTREPORT_AUTODISPATCH: `${API_URL}/report/order/autodispatch`,
        API_SHIPMENTREPORT_LOCATION: `${API_URL}/report/heatmap`,
        API_TRIPREPORT_STATS: `${API_URL}/report/trip/stats`,
        API_TRIPREPORT_ALLSTATS: `${API_URL}/report/trip/allstats`,
        API_TRIPREPORT_CONTRACT: `${API_URL}/report/trip/contract`,
        API_TRIPREPORT_STATUS: `${API_URL}/report/trip/status`,
        API_TRIPREPORT_FEEDBACK: `${API_URL}/report/trip/feedback`,
    },
    map_styles: [
        { 'elementType': 'geometry', 'stylers': [{ 'color': '#ebe3cd' }] },
        { 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#523735' }] },
        { 'elementType': 'labels.text.stroke', 'stylers': [{ 'color': '#f5f1e6' }] },
        { 'featureType': 'administrative', 'elementType': 'geometry.stroke', 'stylers': [{ 'color': '#c9b2a6' }] },
        { 'featureType': 'administrative.land_parcel', 'elementType': 'geometry.stroke', 'stylers': [{ 'color': '#dcd2be' }] },
        { 'featureType': 'administrative.land_parcel', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#ae9e90' }] },
        { 'featureType': 'landscape.natural', 'elementType': 'geometry', 'stylers': [{ 'color': '#dfd2ae' }] },
        { 'featureType': 'poi', 'elementType': 'geometry', 'stylers': [{ 'color': '#dfd2ae' }] },
        { 'featureType': 'poi', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#93817c' }] },
        { 'featureType': 'poi.park', 'elementType': 'geometry.fill', 'stylers': [{ 'color': '#a5b076' }] },
        { 'featureType': 'poi.park', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#447530' }] },
        { 'featureType': 'road', 'elementType': 'geometry', 'stylers': [{ 'color': '#f5f1e6' }] },
        { 'featureType': 'road.arterial', 'elementType': 'geometry', 'stylers': [{ 'color': '#fdfcf8' }] },
        { 'featureType': 'road.highway', 'elementType': 'geometry', 'stylers': [{ 'color': '#f8c967' }] },
        { 'featureType': 'road.highway', 'elementType': 'geometry.stroke', 'stylers': [{ 'color': '#e9bc62' }] },
        { 'featureType': 'road.highway.controlled_access', 'elementType': 'geometry', 'stylers': [{ 'color': '#e98d58' }] },
        { 'featureType': 'road.highway.controlled_access', 'elementType': 'geometry.stroke', 'stylers': [{ 'color': '#db8555' }] },
        { 'featureType': 'road.local', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#806b63' }] },
        { 'featureType': 'transit.line', 'elementType': 'geometry', 'stylers': [{ 'color': '#dfd2ae' }] },
        { 'featureType': 'transit.line', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#8f7d77' }] },
        { 'featureType': 'transit.line', 'elementType': 'labels.text.stroke', 'stylers': [{ 'color': '#ebe3cd' }] },
        { 'featureType': 'transit.station', 'elementType': 'geometry', 'stylers': [{ 'color': '#dfd2ae' }] },
        { 'featureType': 'water', 'elementType': 'geometry.fill', 'stylers': [{ 'color': '#b9d3c2' }] },
        { 'featureType': 'water', 'elementType': 'labels.text.fill', 'stylers': [{ 'color': '#92998d' }] }
    ],
    battery_percent_icons: {
        'empty': {
            'icon': 'battery-empty',
            'style_class': 'battery-empty',
        },
        'quarter': {
            'icon': 'battery-quarter',
            'style_class': 'battery-quarter',
        },
        'half': {
            'icon': 'battery-half',
            'style_class': 'battery-half',
        },
        'three-querter': {
            'icon': 'battery-three-quarters',
            'style_class': 'battery-three-quarters',
        },
        'full': {
            'icon': 'battery-full',
            'style_class': 'battery-full',
        }
    },
    driver_icons: {
        'idle': `static/${current_build}/assets/Assets-Myfleet/BlueTracker@1x.svg`,
        'occupied': `static/${current_build}/assets/Assets-Myfleet/RedTracker@1x.svg`,
        'selected': `static/${current_build}/assets/Assets-Myfleet/SelectedTracker@1x.svg`
    },
    markers: {
        pickup: `static/${current_build}/assets/markers/Blue-Marker@2x.png`,
        pickup_done_big: `static/${current_build}/assets/markers/Pickup-Done-Big.png`,
        pickup_done_big_2x: `static/${current_build}/assets/markers/Pickup-Done-Big@2x.png`,

        dropoff: `static/${current_build}/assets/markers/Purple-Marker@2x.png`,
        dropoff_done_big: `static/${current_build}/assets/markers/Drop-Done-Big.png`,
        dropoff_done_big_2x: `static/${current_build}/assets/markers/Drop-Done-Big@2x.png`,

        stop: `static/${current_build}/assets/markers/Stop@2x.png`,
        stop_big: `static/${current_build}/assets/markers/Stop-Big.png`,
        stop_pending: `static/${current_build}/assets/markers/StopBig@1x.svg`,
        stop_done: `static/${current_build}/assets/markers/Done-Marker@1x.svg`,
        stop_done_big: `static/${current_build}/assets/markers/StopDone-Big.png`,
        stop_done_big_2x: `static/${current_build}/assets/markers/StopDone-Big@2x.png`,
    },
    images: {
        avatar: `static/${current_build}/assets/Assets/user.svg`,
        default_truck: `static/${current_build}/assets/truck/Ace.png`,
        tracking_truck: `static/${current_build}/assets/Assets-Common/truck_live_tracking.png`,
        empty_truck: `static/${current_build}/assets/EmptyTruck.png`,
        confirm_done: `static/${current_build}/assets/Assets-Common/ConfirmDone@2x.png`,
        confirm_blue: `static/${current_build}/assets/Assets-Menu/ConfirmBlue@2x.png`,
        contact: `static/${current_build}/assets/Assets-Where-Section/Contact@2x.png`,
        make_call: `static/${current_build}/assets/Assets-Where-Section/MakeCall@2x.png`,
        super: `static/${current_build}/assets/Assets-Common/super.svg`,
        error: `static/${current_build}/assets/Assets-Common/Error@2x.png`,
        download: `/static/${current_build}/assets/Assets-Common/icon-download.png`,
        seal: `static/blowhorn/assets/Assets-Common/Blowhorn_seal_blue.png`,
        search: `static/blowhorn/assets/Assets-Common/search.png`,
        warning: `static/blowhorn/assets/Assets-Common/RedError@2x.png`,
        notification: `static/${current_build}/assets/Assets-Common/Notifications.png`,
        dropdown: `static/${current_build}/assets/Assets/DropDown.png`,
        cancel: `static/${current_build}/assets/Assets/Cancel.png`,
        ok: `static/${current_build}/assets/Assets/green-okay.png`,
        home: `static/${current_build}/assets/home.svg`,
        call_us: `static/${current_build}/assets/Assets-Common/CallUs.png`,
        email: `static/${current_build}/assets/Assets-Common/EMail.png`,
        add_stop: `static/${current_build}/assets/Assets-Where-Section/AddStop.png`
    },
    regex: {
        name: /^[a-zA-Z .]+$/,
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        integer: /^\d+$/,
        phone: current_build === default_build ? /^\d{10}$/ : /^\d{9,10}$/,
        altPhone: /[0-9]{8,10}/,
        otp: /^\d{4,}$/,
        coupon: /^[a-zA-Z0-9]{2,}$/,
        latlng: /([+-]?\d+\.\d+)(\s*,\s*|\s+)([+-]?\d+\.\d+)/,
        w3wPattern: /^[a-zA-Z]*\.[a-zA-Z]*\.[a-zA-Z]*$/,
        gstin: /^\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}$/
    },
    quick_date_filter_delta: {
        FILTER_TODAY: FILTER_TODAY,
        FILTER_LAST_7_DAYS: FILTER_LAST_7_DAYS,
        FILTER_LAST_30_DAYS: FILTER_LAST_30_DAYS
    },
    quickDateFilters: [
        {label: 'Today', val: FILTER_TODAY},
        {label: 'Last 7 Days', val: FILTER_LAST_7_DAYS},
        {label: 'Last 30 Days', val: FILTER_LAST_30_DAYS},
    ],
    shipmenStatusMapping: {
        noMapTracking: [
            'booking_accepted',
            'driver_accepted',
            'Out-For-Pickup',
            'Picked',
            'Accepted',
            'Assigned',
            'At-DropOff',
            'At-Hub',
            'At-Hub-RTO',
            'Moving-To-Hub',
            'New-Order',
            'Order Pending',
            'Out-For-Pickup',
            'Picked',
            'Pickup-Failed',
            'Cancelled',
            'Dummy',
            'Suspended',
            'Expired',
            'Lost',
            'Moving-To-Hub-RTO',
            'Returned-To-Hub',
            'Returned-To-Origin',
            'Returned-To-Origin-Failed',
            'Returned',
            'Rejected',
            'Unable-To-Deliver',
        ],
        pending: [
            'booking_accepted',
            'driver_accepted',
            'Out-For-Pickup',
            'Picked',
            'Accepted',
            'Assigned',
            'At-DropOff',
            'At-Hub',
            'At-Hub-RTO',
            'Moving-To-Hub',
            'New-Order',
            'Order Pending',
            'Out-For-Pickup',
            'Picked',
            'Pickup-Failed',
        ],
        dummy: [
            'Dummy',
            'Suspended'
        ],
        return: [
            'Moving-To-Hub-RTO',
            'Returned-To-Hub',
            'Returned-To-Origin',
            'Returned-To-Origin-Failed',
        ]
    },
    version_info: {
        livetracking: {
            app_title: 'Admin Live Tracking',
            app_code: 'bh_alt',
            version: 'v2.4.3',
            whats_new: {
                features: [],
                bug_fixes: ['Height of navbar is being extended']
            }
        },
        resource_allocation: {
            app_title: 'Resource Allocation',
            app_code: 'bh_ra',
            version: 'v2.0.2',
            whats_new: {
                features: [],
                bug_fixes: [
                    'Fixed; Allocation history not displaying all data.',
                ]
            }
        },
        admin_heatmap: {
            app_title: 'Admin Heatmap',
            app_code: 'bh_ah',
            version: 'v1.1.0',
            whats_new: {
                features: ['Added filter; cancellation reasons.',],
                bug_fixes: []
            }
        },
        dashboard: {
            app_title: 'Announcement',
            app_code: 'bh_announcement',
            version: moment(new Date()).format('MMM DD, YYYY'),
            whats_new: {
                announcement: `<div>
                <div style="display: flex;
                overflow: hidden;
                height: 25vh;
                padding: 15px 0;">
                    <svg style="height: 100%;" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="951.57086" height="665.12779" viewBox="0 0 951.57086 665.12779"><path d="M166.5995,372.68625c.90983,56.17389,21.9053,62.25537,47.68851,56.39652,29.45433-6.693,53.6295-27.67107,65.54457-55.4269l8.21721-19.14177a52.21662,52.21662,0,0,0-27.38436-68.5802l-.0001,0a52.21661,52.21661,0,0,0-68.58022,27.38436C165.64317,323.70531,160.42089,345.20938,166.5995,372.68625Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><path d="M332.38876,259.62619l7.91365-13.39131a12.57983,12.57983,0,0,0-7.10867-18.41687h0a12.57983,12.57983,0,0,0-16.2947,12.423l.46505,14.39458-37.19064,101.9211,20.28071,4.23409Z" transform="translate(-124.21457 -117.43611)" fill="#9f616a"/><path d="M136.73039,510.87935l-9.74073,12.12728a12.57982,12.57982,0,0,0,4.41236,19.24175l0,0a12.57983,12.57983,0,0,0,17.89835-9.97482l1.59045-14.314,51.33163-95.583-19.47062-7.08022Z" transform="translate(-124.21457 -117.43611)" fill="#9f616a"/><path d="M196.91231,611.77256l1.43374,17.22261.33632,4.01806,95.3352-9.16888a21.02015,21.02015,0,0,0,16.83326-29.47148L280.105,528.36751l-10.62034-22.79833-46.02147-10.62034c-37.95,29.04658-3.80566,61.10227,39.15364,93.52972q2.52237,1.91174,5.09777,3.82338Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><path d="M164.78854,671.41121h0a6.84954,6.84954,0,0,0,6.2494-2.41836l31.18452-37.74967-1.77006-17.70057-10.633-6.37978a6.987,6.987,0,0,0-10.09383,3.42593l-20.33954,51.52682A6.84955,6.84955,0,0,0,164.78854,671.41121Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><path d="M191.60214,608.23245l1.77006,21.24067,4.97385-.47795,90.36135-8.69093a21.02015,21.02015,0,0,0,16.83326-29.47148L280.105,533.89008v-5.52257l-10.62034-22.79833-46.02147-10.62034c-37.95,29.04658-3.80566,61.10227,39.15364,93.52972l-.2124.28327Z" transform="translate(-124.21457 -117.43611)" opacity="0.2"/><path d="M155.93826,664.331h0a6.84951,6.84951,0,0,0,6.2494-2.41835L193.3722,624.163l-1.77006-17.70056-10.633-6.37978a6.987,6.987,0,0,0-10.09384,3.42593L150.5358,655.03536A6.84954,6.84954,0,0,0,155.93826,664.331Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><path d="M188.062,604.69233l1.77,21.24068,95.33529-9.17139A21.0234,21.0234,0,0,0,302.008,587.29413L276.56484,530.35V498.489l-61.952-10.62033c-39.44328,30.18689-1.01208,63.62728,44.25141,97.35309Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><circle cx="124.02937" cy="211.12744" r="26.55084" fill="#9f616a"/><path d="M212.84282,372.815,250.014,397.59574l31.861-7.08022L264.17445,372.815V346.26411l-28.3209-1.77005C235.25892,355.09662,229.18353,364.79887,212.84282,372.815Z" transform="translate(-124.21457 -117.43611)" fill="#9f616a"/><path d="M207.53265,496.7189l69.03219,8.85028,15.93051-74.34237c7.40077-19.73352,3.15453-37.16842-10.54741-52.737a11.8239,11.8239,0,0,0-11.23618-9.49965l-6.53731.28464c.7395,23.47551-16.55636,17.82135-42.48135-1.77l-3.13044.93913a17.58322,17.58322,0,0,0-12.34088,19.41846l2.8545,19.26788a131.83032,131.83032,0,0,0,5.61911,22.46529C221.77291,450.74766,219.24374,473.14036,207.53265,496.7189Z" transform="translate(-124.21457 -117.43611)" fill="#00d2f6"/><path d="M175.67163,417.06636l31.861,10.62034,14.16045-51.33163-4.79727-2.18058a17.20447,17.20447,0,0,0-22.28465,7.538Z" transform="translate(-124.21457 -117.43611)" fill="#00d2f6"/><polygon points="145.27 251.839 162.971 271.309 184.211 237.678 157.66 223.518 145.27 251.839" fill="#00d2f6"/><path d="M217.268,320.5983c16.68448,6.2914,34.49038,5.63913,53.10169,0V299.35762H217.268Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><polygon points="921.132 567.779 902.827 575.406 780.798 409.141 692.327 578.457 669.447 573.881 736.563 340.5 814.356 340.5 921.132 567.779" fill="#2f2e41"/><path d="M1069.90341,724.00146h0a7.976,7.976,0,0,1-6.07834-.78277l-38.60279-22.22585a7.83329,7.83329,0,0,1-1.63043-12.32748l3.45008-3.45006,18.30436-1.52536,27.98213,26.86284A7.976,7.976,0,0,1,1069.90341,724.00146Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><path d="M770.62955,730.10291h0a7.976,7.976,0,0,0,6.07834-.78278l38.60279-22.22585a7.83329,7.83329,0,0,0,1.63043-12.32748l-3.45008-3.45006-18.30436-1.52536-27.98213,26.86284A7.976,7.976,0,0,0,770.62955,730.10291Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><path d="M808.915,215.40339l-7.62682,4.57609-15.80509-25.68327a11.5509,11.5509,0,0,1,7.45781-17.35692h0a11.55089,11.55089,0,0,1,13.89894,10.44984Z" transform="translate(-124.21457 -117.43611)" fill="#ffb9b9"/><path d="M1020.18781,415.36054l3.16427-8.31241,28.30256,10.41136a11.55089,11.55089,0,0,1,4.87162,18.25236l0,0a11.55089,11.55089,0,0,1-17.38487.38192Z" transform="translate(-124.21457 -117.43611)" fill="#ffb9b9"/><circle cx="776.22219" cy="166.60864" r="22.88045" fill="#ffb9b9"/><polygon points="796.052 215.42 764.019 215.42 764.019 180.337 791.476 177.286 796.052 215.42" fill="#ffb9b9"/><path d="M943.14693,467.08834H856.20122c3.06581-48.51189,5.4936-95.39321,0-122.02906l5.05559-24.26683a13.6723,13.6723,0,0,1,15.08076-10.7782l11.89628,1.487c8.05639,14.16347,17.79287,14.02685,28.98191,1.52537h14.32935a14.36528,14.36528,0,0,1,14.36246,14.64968v0a181.4649,181.4649,0,0,1,3.68663,52.08078Z" transform="translate(-124.21457 -117.43611)" fill="#3f3d56"/><polygon points="672.497 97.967 684.7 91.866 750.291 200.167 731.987 227.623 672.497 97.967" fill="#3f3d56"/><path d="M1020.361,419.7006l10.42855-8.79682-55.413-77.64818a49.111,49.111,0,0,0-48.84414-19.77546l-1.74815.321,20.95313,36.66813,27.59739,20.89445Z" transform="translate(-124.21457 -117.43611)" fill="#3f3d56"/><path d="M926.898,270.538s4.27546-9.406-6.84076-13.6815c0,0-9.406-15.39169-28.21809-5.13056,0,0-12.82641,5.13056-12.82641,18.81206s-4.27547,16.24678-4.27547,16.24678,6.36692-6.53187,8.07711-11.66243,15.01043-6.29454,20.141-1.164,17.957,4.27546,17.957,4.27546l-.8551,18.81207s4.27547-11.11622,5.98566-11.11622S929.46329,270.538,926.898,270.538Z" transform="translate(-124.21457 -117.43611)" fill="#2f2e41"/><ellipse cx="800.57086" cy="657.83612" rx="140" ry="7.29167" fill="#e6e6e6"/><ellipse cx="101.57086" cy="657.83612" rx="74" ry="3.85417" fill="#e6e6e6"/><path d="M791.16889,245.72478,341.30038,286.04364a8.37412,8.37412,0,0,1-9.07753-7.58424L322.22146,166.86625a8.37413,8.37413,0,0,1,7.58424-9.07753l449.86852-40.31887a8.37412,8.37412,0,0,1,9.07752,7.58424l10.00139,111.59316A8.37412,8.37412,0,0,1,791.16889,245.72478Z" transform="translate(-124.21457 -117.43611)" fill="#00d2f6"/><circle cx="269.65462" cy="99.25313" r="35.12995" fill="#fff"/><path d="M472.58815,186.12032a5.855,5.855,0,0,0,1.04531,11.66323L748.5526,173.14429a5.855,5.855,0,1,0-1.0453-11.66324Z" transform="translate(-124.21457 -117.43611)" fill="#fff"/><path d="M475.72406,221.11a5.855,5.855,0,1,0,1.0453,11.66323L751.68851,208.134a5.855,5.855,0,1,0-1.0453-11.66324Z" transform="translate(-124.21457 -117.43611)" fill="#fff"/></svg>
                </div>
                <div style="margin-bottom: 6px !important;">Dear Customer,</div>
                <div style="margin-bottom: 6px">We are pleased to announce the <b>all-new members</b> module in the Blowhorn customer portal. You can now add permissions at much granular level and create role based access across your organization</div>
                <div style="margin-bottom: 6px">There are two permission modules to choose from,</div>
                <ul>
                  <li >Global Level (Hierarchical) - where you can group permissions for one / multiple city, division, hub</li>
                <li>Module Level - For each section (like shipments / trips / analytics, etc.) you could assign specific sub component (module) level permissions (like uploading orders or edit route)</li>
                  </ul>
                <div style="margin-bottom: 6px">Additionally you can group the permissions to a role and new users can be assigned a role to cut short the configuration / user onboarding. Hope this helps you in scaling your team and providing relevant access as the organization grows.</div>
                <div style="margin-bottom: 12px">Looking forward to hearing from you on the feature feedback or any other questions</div>
                <div>Product Team,</div>
                <div style="font-weight: 600;">Blowhorn</div>
                </div>`,
                features: [],
                bug_fixes: []
            }
        },
    }
};
