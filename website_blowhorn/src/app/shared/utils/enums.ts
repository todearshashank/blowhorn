export enum FleetTrips {
    InProgress = 'In-Progress',
    Completed = 'Completed',
    Upcoming = 'New'
}

export enum Trips {
    Current,
    PastTrips,
    Upcoming
}

interface Scripts {
    name: string;
    src: string;
}

export const ScriptStore: Scripts[] = [{
    name: 'razorpay', src: 'https://checkout.razorpay.com/v1/checkout.js'
}];