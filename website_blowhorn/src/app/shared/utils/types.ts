export type Geopoint = {
  type: string;
  coordinates: [number, number];
};

export type Address = {
  line1: string;
  geopoint: Geopoint;
};

export type Un_Assigned_Order = {
  number: number;
  pickupAddress: Address;
  shippingAddress: Address;
  status: string;
};

export type Un_Assigned_Orders = {
  orders: Un_Assigned_Order[];
};
export type slotwiseReports = {
  status: string;
  key: string;
  value: number;
};

export type slotwise_report = {
  slot_report: slotwiseReports[];
};

export type statuswiseReports = {
  status: string;
  count: number;
}

export type statuswise_report = {
  status_report: statuswiseReports[];
}
