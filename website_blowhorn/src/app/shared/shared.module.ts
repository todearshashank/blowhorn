import { ListFilterPipe } from './pipes/list-filter.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApolloModule } from 'apollo-angular';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { DndListModule } from 'ng6-dnd-lists';
import { NguiMapModule } from '@ngui/map';
import { CookieService } from 'ngx-cookie-service';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule, MatInputModule, MatPaginatorModule, MatIconModule, MatSelectModule } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';

// Modules
import { AppRoutingModule } from '../app-routing.module';

import { DropdownSelectComponent } from './components/dropdown-select/dropdown-select.component';
import { DropdownUserComponent } from './components/dropdown-user/dropdown-user.component';
import { NavLinkComponent } from './components/nav-link/nav-link.component';
import { NavButtonComponent } from './components/nav-button/nav-button.component';
import { NotFound404Component } from './components/not-found-404/not-found-404.component';
import { BlowhornAddressComponent } from './components/blowhorn-address/blowhorn-address.component';
import { BlowhornGstInfoComponent } from './components/blowhorn-gst-info/blowhorn-gst-info.component';
import { BlowhornMapComponent } from './components/blowhorn-map/blowhorn-map.component';
import { BlowhornSearchComponent } from './components/blowhorn-search/blowhorn-search.component';
import { BlowhornLoaderComponent } from './components/blowhorn-loader/blowhorn-loader.component';
import { BlowhornMapSearchComponent } from './components/blowhorn-map-search/blowhorn-map-search.component';
import { BlowhornImageViewerComponent } from './components/blowhorn-image-viewer/blowhorn-image-viewer.component';
import { BlowhornCheckboxComponent } from './components/blowhorn-checkbox/blowhorn-checkbox.component';
import { WhatsNewComponent } from './components/whats-new/whats-new.component';
import { BlowhornConfirmComponent } from './components/blowhorn-confirm/blowhorn-confirm.component';
import { BlowhornHeatmapComponent } from './components/blowhorn-heatmap/blowhorn-heatmap.component';
import { ShipmentTemplateComponent } from './components/shipment-template/shipment-template.component';
import { BlowhornUploadComponent } from './components/blowhorn-upload/blowhorn-upload.component';
import { BlowhornMessageComponent } from './components/blowhorn-message/blowhorn-message.component';
import { BlowhornButtonComponent } from './components/blowhorn-button/blowhorn-button.component';
import { BlowhornPaginationComponent } from './components/blowhorn-pagination/blowhorn-pagination.component';
import { BlowhornNavbarComponent } from './components/blowhorn-navbar/blowhorn-navbar.component';
import { BlowhornAutocompleteComponent } from './components/blowhorn-autocomplete/blowhorn-autocomplete.component';
import { BlowhornInputComponent } from './components/blowhorn-input/blowhorn-input.component';
import { BlowhornMultiselectComponent } from './components/blowhorn-multiselect/blowhorn-multiselect.component';
import { BlowhornYesnoComponent } from './components/blowhorn-yesno/blowhorn-yesno.component';
import { MultiselectBoxComponent } from './components/multiselect-box/multiselect-box.component';
import { MultiselectBoxPanelComponent } from './components/multiselect-box-panel/multiselect-box-panel.component';
import { MiniLoaderComponent } from './components/mini-loader/mini-loader.component';
import { ShipmentAssetsComponent } from './components/shipment-assets/shipment-assets.component';
import { ShipmentSkuComponent } from './components/shipment-sku/shipment-sku.component';
import { BlowhornTitleContentComponent } from './components/blowhorn-title-content/blowhorn-title-content.component';
import { BarcodeComponent } from './components/barcode/barcode.component';
import { BlowhornRadioComponent } from './components/blowhorn-radio/blowhorn-radio.component';
import { ShareInvoiceComponent } from './components/share-invoice/share-invoice.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { BlowhornDropdownComponent } from './components/dropdown/dropdown.component';
import { IsdCodeComponent } from './components/isd-code/isd-code.component';

// Pipes
import { SavedLocationFilterPipe } from './pipes/saved-location-filter.pipe';
import { NameValuePipe } from './pipes/name-value.pipe';
import { VehicleNumberFilterPipe } from './pipes/vehicle-number-filter.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { CurrencySymbolPipe } from './pipes/currency.pipe';
import { ReplaceStringPipe } from './pipes/replace-string.pipe';
import { SafePipe } from './pipes/safe.pipe';
import { UnassignedOrderFilterPipe } from './pipes/unassigned-order-filter.pipe';
import { NameFilterPipe } from './pipes/name-filter.pipe';
import { CityFilterPipe } from './pipes/city-filter.pipe';
import { KeysPipe } from './pipes/keys.pipe';
import { StringFilterPipe } from './pipes/string-filter.pipe';
import { DropdownMultiselectComponent } from './components/dropdown-multiselect/dropdown-multiselect.component';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { BrowserModule } from '@angular/platform-browser';
import { ShipmentEditableOrderlinesComponent } from './components/shipment-editable-orderlines/shipment-editable-orderlines.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule,
    BsDropdownModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    DndListModule,
    HttpClientModule,
    NguiMapModule,
    ApolloModule,
    NgxBarcode6Module,
    BrowserModule,
    MatTableModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatPaginatorModule,
    MatIconModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatSelectModule
  ],
  providers: [
    CookieService
  ],
  declarations: [
    DropdownSelectComponent,
    DropdownUserComponent,
    NavLinkComponent,
    NavButtonComponent,
    NotFound404Component,
    BlowhornAddressComponent,
    BlowhornGstInfoComponent,
    BlowhornMapComponent,
    BlowhornSearchComponent,
    BlowhornLoaderComponent,
    BlowhornMapSearchComponent,
    BlowhornImageViewerComponent,
    BlowhornCheckboxComponent,
    WhatsNewComponent,
    BlowhornConfirmComponent,
    BlowhornHeatmapComponent,
    ShipmentTemplateComponent,
    BlowhornUploadComponent,
    BlowhornMessageComponent,
    BlowhornButtonComponent,
    BlowhornPaginationComponent,
    BlowhornNavbarComponent,
    BlowhornAutocompleteComponent,
    BlowhornInputComponent,
    BlowhornMultiselectComponent,
    BlowhornYesnoComponent,
    MultiselectBoxComponent,
    MultiselectBoxPanelComponent,
    MiniLoaderComponent,
    ShipmentAssetsComponent,
    ShipmentSkuComponent,
    BlowhornTitleContentComponent,
    BarcodeComponent,
    BlowhornRadioComponent,
    ShareInvoiceComponent,
    UserListComponent,
    GalleryComponent,
    BlowhornDropdownComponent,
    IsdCodeComponent,
    DropdownMultiselectComponent,
    ShipmentEditableOrderlinesComponent,

    // Pipe
    KeysPipe,
    NameValuePipe,
    NameFilterPipe,
    CityFilterPipe,
    SortPipe,
    CapitalizePipe,
    ReplaceStringPipe,
    SafePipe,
    CurrencySymbolPipe,
    VehicleNumberFilterPipe,
    SavedLocationFilterPipe,
    UnassignedOrderFilterPipe,
    StringFilterPipe,
    ListFilterPipe,
    SafeHtmlPipe,

    // Directives
    ClickOutsideDirective

  ],
  exports: [
    DropdownSelectComponent,
    DropdownUserComponent,
    NavLinkComponent,
    NavButtonComponent,
    NotFound404Component,
    BlowhornAddressComponent,
    BlowhornGstInfoComponent,
    BlowhornMapComponent,
    BlowhornSearchComponent,
    BlowhornLoaderComponent,
    BlowhornMapSearchComponent,
    BlowhornImageViewerComponent,
    BlowhornCheckboxComponent,
    WhatsNewComponent,
    BlowhornConfirmComponent,
    BlowhornHeatmapComponent,
    BlowhornUploadComponent,
    BlowhornMessageComponent,
    BlowhornButtonComponent,
    BlowhornPaginationComponent,
    BlowhornNavbarComponent,
    BlowhornAutocompleteComponent,
    BlowhornInputComponent,
    BlowhornMultiselectComponent,
    BlowhornYesnoComponent,
    MultiselectBoxComponent,
    MultiselectBoxPanelComponent,
    MiniLoaderComponent,
    ShipmentAssetsComponent,
    ShipmentSkuComponent,
    BarcodeComponent,
    BlowhornTitleContentComponent,
    UserListComponent,
    ShareInvoiceComponent,
    GalleryComponent,
    BlowhornDropdownComponent,
    IsdCodeComponent,
    DropdownMultiselectComponent,
    ShipmentEditableOrderlinesComponent,

    // Pipe
    NameValuePipe,
    KeysPipe,
    VehicleNumberFilterPipe,
    SortPipe,
    ReplaceStringPipe,
    CurrencySymbolPipe,
    SafePipe,
    NameFilterPipe,
    CityFilterPipe,
    UnassignedOrderFilterPipe,
    SavedLocationFilterPipe,
    StringFilterPipe,
    ListFilterPipe,
    SafeHtmlPipe,
]
})
export class SharedModule { }
