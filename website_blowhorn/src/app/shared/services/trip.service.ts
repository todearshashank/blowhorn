import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Constants } from '../utils/constants';
import { Trip } from '../models/trip.model';
import { DepartureSheet } from '../models/departure-sheet.model';
import { TripsGrid } from '../models/trips-grid.model';
import { TripFilter } from '../models/trip-filter.model';
import { BlowhornService } from './blowhorn.service';
import { MyTripsGrid } from '../models/my-trips-grid.model';

@Injectable()
export class TripService {

  private subject = new Subject<any>();

  constructor(
    private http_client: HttpClient,
    public _blowhorn_service: BlowhornService,
  ) { }

  get_trip_details(id: number): Observable<Trip> {
    const url = this._blowhorn_service.is_business_user ? Constants.urls.API_DETAIL_TRIP : Constants.urls.API_ALT_DETAIL_TRIP;
    return this.http_client
       .get<Trip>(`${url}/${id}`);
  }

  get_trip_new_details(id: string): Observable<Trip> {
      let url = Constants.urls.API_DETAIL_TRIP;
    return this.http_client
       .get<Trip>(`${url}/${id}`);
  }

  get_departure_sheet(id: number): Observable<DepartureSheet> {
    const url = `/api/trip/${id}/departure-sheet`;
    return this.http_client
      .get<DepartureSheet>(url);
  }

  get_enterprise_trips(url: string): Observable<TripsGrid> {
    return this.http_client.get<TripsGrid>(url);
  }

  get_completed_trips(url): Observable<TripsGrid> {
    return this.http_client
      .get<TripsGrid>(url);
  }

  get_upcoming_trips(url): Observable<TripsGrid> {
    return this.http_client
      .get<TripsGrid>(url);
  }

  get_current_mytrips(url): Observable<MyTripsGrid> {
    return this.http_client
      .get<MyTripsGrid>(url);
  }

  get_completed_mytrips(url): Observable<MyTripsGrid> {
    return this.http_client
      .get<MyTripsGrid>(url);
  }

  get_upcoming_mytrips(url): Observable<MyTripsGrid> {
    return this.http_client
      .get<MyTripsGrid>(url);
  }

  get_trip_filter(): Observable<TripFilter> {
    return this.http_client
      .get<TripFilter>(Constants.urls.API_TRIP_FILTER);
  }

  get_pod_details(id: number): Observable<any> {
    let url = `${Constants.urls.API_TRIP_BASE}/${id}/pod`;
    return this.http_client.get<any>(url);
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  sendMessage(message: any) {
    this.subject.next(message);
  }

  upload_stops(data: any, trip_number: any) {
    return this.http_client
      .post(`${Constants.urls.API_TRIP_STOP_TEMPLATE_IMPORT}/${trip_number}`, data, BlowhornService.http_xlsx_headers);
  }

  get_ondemand_contract_details(): Observable<any> {
    return this.http_client.get<any>(Constants.urls.API_TRIP_ONDEMAND_CONTRACTS);
  }


  raise_order_request(data: any): any {
    return this.http_client
        .post(`${Constants.urls.API_TRIP_RAISE_REQUEST}`, data, BlowhornService.http_headers);
  }

}
