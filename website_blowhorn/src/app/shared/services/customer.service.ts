import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable } from 'rxjs';
import { IdName } from '../models/id-name.model';

@Injectable()
export class CustomerService {

  constructor(private _http_client: HttpClient) { }

  get_all_customers(params = {}): Observable<IdName[]> {
    return this._http_client
      .get<IdName[]>(Constants.urls.API_ALL_CUSTOMERS, {
        params: params
      });
  }

}
