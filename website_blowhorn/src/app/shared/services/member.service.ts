import { NgModule, Injectable } from '@angular/core';
import { HttpClientModule, HttpClient,  HttpClientXsrfModule } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { BehaviorSubject, Observable } from 'rxjs';
import { BlowhornService } from './blowhorn.service';
import { MemberGrid, Member, SupportData, Group } from './../models/member.model';

@NgModule({
    imports: [HttpClientModule, HttpClientXsrfModule],
})

@Injectable({
  providedIn: 'root'
})
export class MemberService {

    constructor(
        private _http_client: HttpClient,
        private bhService: BlowhornService,
    ) { }

    private _support_data_store = new BehaviorSubject<SupportData>(new SupportData());

    support_data_store = this._support_data_store.asObservable();

    update_support_data_store(val: any) {
        this._support_data_store.next(val);
    }

    get has_invite_permission(): boolean {
        return this.bhService.user_profile.is_org_admin;
    }

    loadSupportData() {
        this.get_member_supportdata().subscribe((resp: any) => {
            this.update_support_data_store(resp)
        },
        err => {
            let errors = err.error.message || err.error.detail;
            if (!errors) {
                errors = err.error.email.join('\n');
            }
        });
    }

    get_members(url: string = Constants.urls.API_MEMBERS): Observable<MemberGrid> {
        return this._http_client.get<MemberGrid>(url);
    }

    invite_member(data: any): Observable<any> {
        return this._http_client.post<any>(Constants.urls.API_INVITE_MEMBER,
            data, BlowhornService.http_headers);
    }

    update_member(data: any): Observable<Member> {
        return this._http_client.put<Member>(Constants.urls.API_MEMBERS,
            data, BlowhornService.http_headers);
    }

    reinvite_member(data: any): Observable<any> {
        return this._http_client.put<any>(Constants.urls.API_INVITE_MEMBER,
            data, BlowhornService.http_headers);
    }

    get_member_supportdata(): Observable<any> {
        return this._http_client.get<any>(Constants.urls.API_MEMBER_SUPPORTDATA);
    }

    get_member_permission_level(data: any): Observable<any> {
        return this._http_client.get<any>('api/fleet/member/permissionlevel', {
            params: data
        });
    }

    get_member_permissions(userId: any): Observable<any> {
        return this._http_client.get<any>(`api/fleet/member/${userId}/permissions`);
    }

    update_user_permissions(userId: number, data: any): Observable<any> {
        return this._http_client.put<any>(`api/fleet/member/${userId}/permissions`,
            data, BlowhornService.http_headers);
    }

    // Groups
    get_groups(url: string = Constants.urls.API_GROUPS): Observable<any> {
        return this._http_client.get<any>(url);
    }

    get_group_detail(id: number): Observable<Group> {
        return this._http_client.get<Group>(`api/fleet/member/group/${id}`);
    }

    add_group(data: any): Observable<any> {
        return this._http_client.post<any>(Constants.urls.API_GROUPS,
            data, BlowhornService.http_headers);
    }

    update_group(id: number, data: any): Observable<any> {
        return this._http_client.put<any>(`api/fleet/member/group/${id}`,
            data, BlowhornService.http_headers);
    }

}
