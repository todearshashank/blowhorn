import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable, BehaviorSubject } from 'rxjs';
import { BlowhornService } from './blowhorn.service';
import { ApolloQueryResult } from 'apollo-client';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { slotwise_report } from '../utils/types';
import { statuswise_report } from '../utils/types';
import * as moment from 'moment';
import { saveAs as fileSaverSave } from 'file-saver'


@Injectable()
export class ReportService {
    private _order_location_store = new BehaviorSubject<any>({});
    private _orderstatus_store = new BehaviorSubject<any>({});
    private _ordertimeplaced_store = new BehaviorSubject<any>({});
    private _orderpaymentmode_store = new BehaviorSubject<any>({});
    private _orderattempts_store = new BehaviorSubject<any>({});
    private _orderplaced_store = new BehaviorSubject<any>({});
    private _orderdeliveryperformance_store = new BehaviorSubject<any>({});
    private _ordersamedaydelivery_store = new BehaviorSubject<any>({});
    private _order_count_tiles_store = new BehaviorSubject<any>({});
    private _order_sddcount_tiles_store = new BehaviorSubject<any>({});
    private _order_codsum_tiles_store = new BehaviorSubject<any>({});
    private _order_sla_store = new BehaviorSubject<any>({});
    private _orderautodispatch_store = new BehaviorSubject<any>({});
    private _tripstats_store = new BehaviorSubject<any>({});
    private _tripallstats_store = new BehaviorSubject<any>({});
    private _tripcontracttype_store = new BehaviorSubject<any>({});
    private _tripstatus_store = new BehaviorSubject<any>({});

    order_location_store = this._order_location_store.asObservable();
    orderstatus_store = this._orderstatus_store.asObservable();
    ordertimeplaced_store = this._ordertimeplaced_store.asObservable();
    orderpaymentmode_store = this._orderpaymentmode_store.asObservable();
    orderattempts_store = this._orderattempts_store.asObservable();
    orderplaced_store = this._orderplaced_store.asObservable();
    orderdeliveryperformance_store = this._orderdeliveryperformance_store.asObservable();
    ordersamedaydelivery_store = this._ordersamedaydelivery_store.asObservable();
    order_count_tiles_store = this._order_count_tiles_store.asObservable();
    order_sddcount_tiles_store = this._order_sddcount_tiles_store.asObservable();
    order_codsum_tiles_store = this._order_codsum_tiles_store.asObservable();
    order_sla_store = this._order_sla_store.asObservable();
    orderautodispatch_store = this._orderautodispatch_store.asObservable();
    tripstats_store = this._tripstats_store.asObservable();
    tripallstats_store = this._tripallstats_store.asObservable();
    tripcontracttype_store = this._tripcontracttype_store.asObservable();
    tripstatus_store = this._tripstatus_store.asObservable();

    public pickupDate = moment().subtract({ 'days': 1 }).format("YYYY-MM-DD");

    constructor(
        private http_client: HttpClient,
        private _apollo: Apollo
    ) { }


    update_order_location_store(val: any) {
        this._order_location_store.next(val);
    }

    update_orderstatus_store(val: any) {
        this._orderstatus_store.next(val);
    }

    update_ordertimeplaced_store(val: any) {
        this._ordertimeplaced_store.next(val);
    }

    update_orderpaymentmode_store(val: any) {
        this._orderpaymentmode_store.next(val);
    }

    update_orderattempts_store(val: any) {
        this._orderattempts_store.next(val);
    }

    update_orderplaced_store(val: any) {
        this._orderplaced_store.next(val);
    }

    update_orderdeliveryperformance_store(val: any) {
        this._orderdeliveryperformance_store.next(val);
    }

    update_ordersamedaydelivery_store(val: any) {
        this._ordersamedaydelivery_store.next(val);
    }

    update_ordertiles_store(val: any) {
        this._order_count_tiles_store.next(val);
    }

    update_order_sddcount_tiles_store(val: any) {
        this._order_sddcount_tiles_store.next(val);
    }

    update_order_codsum_tiles_store(val: any) {
        this._order_codsum_tiles_store.next(val);
    }

    update_order_sla_store(val: any) {
        this._order_sla_store.next(val);
    }

    update_orderautodispatch_store(val: any) {
        this._orderautodispatch_store.next(val);
    }

    update_tripstats_store(val: any) {
        this._tripstats_store.next(val);
    }

    update_tripallstats_store(val: any) {
        this._tripallstats_store.next(val);
    }

    update_tripcontracttype_store(val: any) {
        this._tripcontracttype_store.next(val);
    }

    update_tripstatus_store(val: any) {
        this._tripstatus_store.next(val);
    }

    get_slotwise_report_data(date: any): Observable<ApolloQueryResult<slotwise_report>> {
        return this._apollo.query<slotwise_report>({
            query: gql`{
        slotwiseReports(pickupDate:"${this.pickupDate}"){
          status
          key
          value
        }
      }`,
            fetchPolicy: 'network-only'
        });
    }

    get_statuswise_report_data(): Observable<ApolloQueryResult<statuswise_report>> {
        return this._apollo.query<statuswise_report>({
            query: gql`{
        statuswiseReports(pickupDate:"${this.pickupDate}"){
          status
          count
        }
      }`,
            fetchPolicy: 'network-only'
        });
    }

    get_order_reports(pickupDate: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_ORDER_REPORTS}?pickupDate=${pickupDate}`);
    }
    get_statuswisecount(pickupDate: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_STATUSWISE_COUNTS}?pickupDate=${pickupDate}`);
    }
    get_codorder_status(pickupDate: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_CODORDER_STATUS}?pickupDate=${pickupDate}`);
    }
    get_shipment_delivery(pickupDate: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENT_DELIVERY}?pickupDate=${pickupDate}`);
    }
    get_hubwise_data(pickupDate: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_HUBWISE_DATA}?pickupDate=${pickupDate}`);
    }
    get_ordercreated_time(pickupDate: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_ORDERCREATED_TIME}?pickupDate=${pickupDate}`);
    }
    get_order_status(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_STATUS}?${query}`);
    }
    get_order_timeplaced(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_TIMEPLACED}?${query}`);
    }
    get_order_paymentmode(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_PAYMENTMODE}?${query}`);
    }
    get_order_attempts(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_ATTEMPTS}?${query}`);
    }
    get_order_placed(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_PLACED}?${query}`);
    }
    get_order_deliveryperformance(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_DELIVERYPERFORMANCE}?${query}`);
    }
    get_order_samedaydelivery(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_SAMEDAYDELIVERY}?${query}`);
    }
    get_order_sla(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_SLA}?${query}`);
    }
    get_order_autodispatch(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_SHIPMENTREPORT_AUTODISPATCH}?${query}`);
    }
    get_order_location_data(params: any): Observable<any> {
        return this.http_client
            .get<any>(Constants.urls.API_SHIPMENTREPORT_LOCATION, {params: params});
    }
    get_trip_stats(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_TRIPREPORT_STATS}?${query}`);
    }
    get_trip_allstats(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_TRIPREPORT_ALLSTATS}?${query}`);
    }
    get_trip_contracttype(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_TRIPREPORT_CONTRACT}?${query}`);
    }
    get_trip_status(query: any): Observable<any> {
        return this.http_client
            .get<any>(`${Constants.urls.API_TRIPREPORT_STATUS}?${query}`);
    }
    submit_feedback(data: any): Observable<any> {
        return this.http_client.post<any>(Constants.urls.API_TRIPREPORT_FEEDBACK,
            data, BlowhornService.http_headers);
    }

    dataURLtoFile(dataurl: string, filename: string, format: string) {
        const arr = dataurl.split(',');
        const mime = arr[0].match(/:(.*?);/)[1];
        const bstr = atob(arr[1]);
        let n = bstr.length;
        const u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        fileSaverSave(new File([u8arr], filename, { type: format }));
    }

}
