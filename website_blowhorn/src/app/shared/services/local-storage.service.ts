import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() {}

  storageAvailable(type :any): boolean {
    // Check if local-storage is available.
    // Sometimes user might have disabled browser storage.
    try {
        let storage = window[type];
        let x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch (e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            Storage.length !== 0;
        }
    };

  get_item(key: string): any {
    const item = localStorage.getItem(key);
    if (item !== null) {
      return item;
    }
    return false;
  }

  set_item(key: string, val: any): void {
    localStorage.setItem(key, val);
  }

  remove_item(key: string): void {
    localStorage.removeItem(key);
  }

}
