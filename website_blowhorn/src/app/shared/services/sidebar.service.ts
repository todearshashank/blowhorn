import { Injectable } from '@angular/core';


@Injectable()
export class SidebarService {

  private _show_sidebar = true;
  constructor() {}

  public get show_sidebar() {
    return this._show_sidebar;
  }

  public set show_sidebar(show_sidebar: boolean) {
    this._show_sidebar = show_sidebar;
  }
}
