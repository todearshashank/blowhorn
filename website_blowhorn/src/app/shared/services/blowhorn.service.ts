import { ViewContainerRef, Injectable, ComponentFactoryResolver, ComponentRef, Type } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BlowhornLoaderComponent } from '../components/blowhorn-loader/blowhorn-loader.component';
import { Constants } from '../utils/constants';
import { HttpClient } from '@angular/common/http';
import { SavedLocation } from '../models/saved-location.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase, ChildEvent } from 'angularfire2/database';
import { ScriptService } from './script.service';
import { LocalStorageService } from './local-storage.service';
import { ToastrService } from 'ngx-toastr';


declare var customer: string;
declare var customer_id: any;
declare var customer_params: {
    cities: {
        city_name: string,
        id: number,
        coverage: {
            pickup: {
                lat: number,
                lng: number
            }[],
            dropoff: {
                lat: number,
                lng: number
            }[]
        },
    }[]
};
declare var is_authenticated: boolean;
declare var is_business_user: boolean;
declare var logged_user: any;
declare var is_non_individual: boolean;
declare var is_staff: boolean;
declare var is_org_admin: boolean;
declare var is_org_owner: boolean;
declare var current_build: any;
declare var payment_methods: string[];
declare var cancellationReasons: any;
declare var default_currency: any;
declare var country_code: string;
declare var isd_dialing_code: string;
declare var termsOfServiceUrl: string;
declare var supportEmail: string;
declare var homepage_url: string;
declare var fontName: string;
declare var brand_name: string;
declare var dashboard_config: any;
declare var eta_recalculation_interval: any;
declare var modules: any;
declare var permissions: any;


@Injectable()
export class BlowhornService {

    private _modal_container: ViewContainerRef;
    private _user_profile: any;
    _user_permissions: any[] = [];
    _modules: any[] = modules;
    _first_page_permission: any;
    private _loader_active = false;
    private _city_coverage_pickup: Map<number, {lat: number, lng: number}[]> = new Map<number, {lat: number, lng: number}[]>();
    private _city_coverage_dropoff: Map<number, {lat: number, lng: number}[]> = new Map<number, {lat: number, lng: number}[]>();
    private _city_coverage_pickup_by_name: Map<string, {lat: number, lng: number}[]> = new Map<string, {lat: number, lng: number}[]>();
    private _city_coverage_dropoff_by_name: Map<string, {lat: number, lng: number}[]> = new Map<string, {lat: number, lng: number}[]>();
    private _cities: {name: string, id: number}[];
    private _is_navigator_geolocation = false;
    private _coordinates = new BehaviorSubject<any>(null);
    private _saved_locations = new BehaviorSubject<SavedLocation[]>(null);
    private _user_profile_obs = new BehaviorSubject<any>(null);
    private _user_permissions_obs = new BehaviorSubject<any>(null);
    private _first_page_permission_obs = new BehaviorSubject<any>(null);

    coordinates = this._coordinates.asObservable();
    saved_locations = this._saved_locations.asObservable();
    user_profile_obs = this._user_profile_obs.asObservable();
    user_permissions_obs = this._user_permissions_obs.asObservable();
    first_page_permission_obs = this._first_page_permission_obs.asObservable();

    constructor(
        private _component_factory_resolver: ComponentFactoryResolver,
        private _http_client: HttpClient,
        public scriptService: ScriptService,
        private cookieService: CookieService,
        private lsService: LocalStorageService,
        private firebase_db: AngularFireDatabase,
        private toastr: ToastrService,
    ) {
        if (!is_staff && is_non_individual) {
            this.processUserPermissions()
        }
    }

    static getCookie(name: string) {
        if (!document.cookie) {
            return null;
        }

        const xsrfCookies = document.cookie.split(';')
            .map(c => c.trim())
            .filter(c => c.startsWith(name + '='));

        if (xsrfCookies.length === 0) {
            return null;
        }

        return decodeURIComponent(xsrfCookies[0].split('=')[1]);
    }

    static get http_headers(): {} {
        const token = this.getCookie('csrftoken');
        return {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Access-Control-Allow-Origin': '*',
                'X-CSRFToken': token
            })
        };
    }

    static get http_headers_for_form(): {} {
        const token = this.getCookie('csrftoken');
        return {
            headers: new HttpHeaders({
                'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'X-CSRFToken': token,
                'X-Requested-With': 'XMLHttpRequest'
            })
        }
    }

    static get http_xlsx_headers(): {} {
        const token = this.getCookie('csrftoken');
        return {
            headers: new HttpHeaders({
                'Access-Control-Allow-Origin': '*',
                'X-CSRFToken': token
            })
        };
    }

    public getErrorMessage(data: any) {
        // Using default error messages from django response.
        var _form = data.form;
        if (!_form) {
          _form = data.responseJSON.form;
        }
        var nonFieldErrors = _form.errors;
        if (nonFieldErrors.length > 0) {
          return nonFieldErrors[0];
        }
        var fields = _form.fields;
        console.log('fields: ', fields);
        for (var field in fields) {
            var fieldErrors = fields[field].errors;
            console.log('fieldErrors: ', fieldErrors);
            if (fieldErrors.length > 0) {
                return fieldErrors[0];
            }
        }
    }

    get is_navigator_geolocation(): boolean {
        return this._is_navigator_geolocation;
    }

    set is_navigator_geolocation(is_navigator_geolocation: boolean) {
        this._is_navigator_geolocation = is_navigator_geolocation;
    }

    get user_profile(): any {
        if (typeof this._user_profile === 'undefined') {
            return {};
        }
        return this.copy_object(this._user_profile);
    }

    set user_profile(data: any) {
        this._user_profile = data;
    }

    get modules(): any {
        return modules;
    }

    get first_page_permission(): any {
        return this._first_page_permission;
    }

    initialize_live_updates(customerId: any): void {
        if (window.location.pathname.indexOf('/track') !== -1) {
            return
        }
        let _config = {tapToDismiss: true, timeOut: 5000,}
        this.firebase_db
            .list(`/dashboard/${customerId}/ordersync`)
            .stateChanges([<ChildEvent>"value"]).subscribe(data => {
                let modified_data: any = data.payload.val();
                if (modified_data != null) {
                    modified_data = typeof(modified_data) === 'string' ? JSON.parse(modified_data) : modified_data;
                }
                if (modified_data) {
                    for (const [key, value] of Object.entries(JSON.parse(modified_data))) {
                        if (key === ''){
                            this.toastr.success(<string>value, key , _config);
                        } else {
                            this.toastr.error(<string>value, key, _config);
                        }
                    }
                }
            });
    }

    // Dynamic Asset link
    get logo_url(): string {
        return `static/${current_build}/assets/logo/logo.png`;
    }

    get rect_logo_url(): string {
        return `static/${current_build}/assets/logo/logo_rect.png`;
    }

    get avatar_url(): string {
        return `static/${current_build}/assets/Assets/avatar.png`;
    }

    get termsOfServiceUrl(): string {
        return termsOfServiceUrl;
    }

    get supportEmail(): string {
        return supportEmail;
    }

    get homepage_url(): string {
        return homepage_url;
    }

    get font_name(): string {
        return fontName;
    }

    get country_code(): string {
        return country_code;
    }

    get isd_dialing_code(): string {
        return isd_dialing_code;
    }

    get brand_name(): string {
        return brand_name;
    }

    get is_authenticated(): boolean {
        return is_authenticated;
    }

    get eta_recalculation_interval(): number {
        return eta_recalculation_interval;
    }

    set modal_container(modal_container: ViewContainerRef) {
        this._modal_container = modal_container;
    }

    get username(): string {
        return customer || logged_user ? logged_user.name : logged_user.email;
    }

    get orgnization_name(): string {
        return (customer && customer != 'None') ? customer : (this._user_profile ? this._user_profile.orgnization_name : '');
    }

    get is_customer(): boolean {
        return (customer && customer != 'None') || (customer_id && customer_id != 'None');
    }

    get user_details(): any {
        return logged_user;
    }

    get is_staff(): boolean {
        return is_staff;
    }

    get is_business_user(): boolean {
        return is_business_user;
    }

    get is_non_individual(): boolean {
        return is_non_individual;
    }

    get is_org_admin(): boolean {
        return is_org_admin;
    }

    get is_org_owner(): boolean {
        return is_org_owner;
    }

    get current_build(): string {
        return current_build;
    }

    get default_build(): boolean {
        return current_build == Constants.default_build;
    }

    get currency(): string {
        return default_currency;
    }

    get payment_methods(): string[] {
        return payment_methods;
    }

    get cancellationReasons(): any {
        return cancellationReasons;
    }

    get customer_params(): {} {
        return this.copy_object(customer_params);
    }

    get city_coverage_pickup(): Map<number, {lat: number, lng: number}[]> {
        return new Map(this._city_coverage_pickup);
    }

    get city_coverage_dropoff(): Map<number, {lat: number, lng: number}[]> {
        return new Map(this._city_coverage_dropoff);
    }

    get cities(): {name: string, id: number}[] {
        return this.copy_array(this._cities);
    }

    get dashboard_config(): any {
        return dashboard_config;
    }

    get stored_permissions(): any {
        return permissions;
    }

    isObjectEmpty(data: any): boolean {
        return data && Object.keys(data).length === 0;
    }

    get_national_number(val: any): number {
        return val && val.toString().startsWith(this.isd_dialing_code) ?
            parseInt(val.toString().substring(3)) : val;
    }

    get_city_name(id: number): string {
        const city = this.cities.find(item => item.id === id);
        return city.name;
    }

    get_city_id(name: string): number {
        const city = this.cities.find(item => item.name === name);
        return city.id;
    }

    array_to_object_array(list: any[]): {}[] {
        return list.map( (item, index) => {
            return {
                name: item,
                id : index + 1
            };
        });
    }

    convertMinutesToStr(num: number) {
        var value = num;
        var units = {
            day: 24*60,
            hour: 60,
            minute: 1,
        }
        var result = <any>[]
        for(var name in units) {
            var p =  Math.floor(value/units[name]);
            if(p == 1) result.push(" " + p + " " + name);
            if(p >= 2) result.push(" " + p + " " + name + "s");
            value %= units[name]
        }
        return result.join('');
    }

    show_loader() {
        if (!this._loader_active) {
            this._loader_active = true;
            this.show_modal(BlowhornLoaderComponent);
        }
    }

    hide_loader() {
       this.hide_modal();
       this._loader_active = false;
    }

    show_modal(component: Type<any>): any {
        const component_factory = this._component_factory_resolver.resolveComponentFactory(component);
        const comp = this._modal_container.createComponent(component_factory);
        return comp;
    }

    hide_modal() {
        this._modal_container.clear();
    }

    load_user_profile() {
        this._http_client
            .get<{status: string, message: {}}>(Constants.urls.API_USER_PROFILE)
            .subscribe(data => {
                this._user_profile = data.message;
                this._user_profile_obs.next(data.message);
                this._saved_locations.next(this._user_profile.saved_locations);
                this.initialize_live_updates(data.message['cid']);
            });
    }

    update_saved_locations(saved_locations: SavedLocation[]) {
        this._user_profile.saved_locations = saved_locations;
        this._saved_locations.next(saved_locations);
    }

    update_user_profile(val: any) {
        this._user_profile_obs.next(val);
    }

    update_profile(data: any): Observable<any> {
        return this._http_client
          .put<any>(Constants.urls.API_USER_PROFILE, data, BlowhornService.http_headers);
    }

    update_password(data: any): Observable<any> {
        let url = is_staff ? Constants.urls.API_USER_PASSWORD : Constants.urls.API_CUSTOMER_PASSWORD
        return this._http_client
          .post<any>(url, data, BlowhornService.http_headers_for_form);
    }

    delete_data_request(): Observable<any> {
        return this._http_client.delete(Constants.urls.API_DELETE_CUSTOMER_DATA,
            BlowhornService.http_headers);
    }

    loadRazorpayLibrary() {
        this.scriptService.load('razorpay').then(data => {
            console.log('script loaded ', data);
        }).catch(error => console.log(error));
    }

    processUserPermissions() {
        if (is_org_admin) {
            return;
        }
        // Merge individual and group permissions
        const moduleIds = this._modules.map(m => m.id)
        let permissions = [];
        if (this.stored_permissions) {
            let storedPerms = this.stored_permissions;
            let tmsPerms = storedPerms.user_permissions.filter((p:any) => this._modules.findIndex(m => m.id === p.module) > -1)
            permissions = permissions.concat(tmsPerms)
            let groups = storedPerms.groups || []
            groups.forEach((grp: { permissions: any; }) => {
                let tmsGroupPerms = grp.permissions.filter((p:any) => this._modules.findIndex(m => m.id === p.module) > -1)
                permissions = permissions.concat(tmsGroupPerms);
            })
        }
        this._user_permissions = permissions;
        this._user_permissions_obs.next(permissions);

        const sortedPermissions = permissions.sort((a, b) => {
            const aKey = a.module;
            const bKey = b.module;
            return moduleIds.indexOf(aKey) - moduleIds.indexOf(bKey);
         });
        let firstPerm = sortedPermissions.length ? sortedPermissions[0] : null
        this._first_page_permission = firstPerm ? this.getModuleById(firstPerm.module) : null
        this._first_page_permission_obs.next(firstPerm)
    }

    getFirstChildModuleUrl(moduleNames: string[]) {
        let modules = this.modules.filter((m: any) => moduleNames.indexOf(m.code) > -1)
        for (let i = 0; i < modules.length; i++) {
            let perm = this._user_permissions.find(p => p.module === modules[i].id)
            if (perm) {
                return perm.url
            }
        }
        return null
    }

    hasPermission(moduleId: string, codename: string) {
        if (is_org_admin) {
            return true;
        }
        let module = this.getModuleByName(moduleId);
        if (!module) {
            return false;
        }
        return this._user_permissions.findIndex(i=> i.module === module.id && i.codename === codename) > -1;
    }

    hasPermissionByCode(permission_names: string[]) {
        if (is_org_admin) {
            return true;
        }
        for (let i = 0; i < permission_names.length; i++) {
            let perm = this._user_permissions.find(p => p.codename === permission_names[i]);
            if (perm) {
                return perm.url;
            }
        }
        return false
    }

    hasPermissionByUrl(url: string) {
        if (is_org_admin) {
            return true;
        }
        const mytripsUrl = '/dashboard/mytrips'
        const mytripsChildPageUrls = ['/dashboard/mytrips/current', '/dashboard/mytrips/completed', '/dashboard/mytrips/upcoming']
        if (mytripsChildPageUrls.includes(url)) {
            url = mytripsUrl
        }
        return this._user_permissions.findIndex(i => i.url === url) > -1;
    }

    hasAtleastOnePermission(moduleCode: string, key: string = 'code') {
        if (is_org_admin) {
            return true;
        }
        let module = this.getModule(moduleCode, key);
        if (!module) {
            return false;
        }
        return this._user_permissions.findIndex(i => i.module === module.id) > -1;
    }

    hasChildPagePerm(modules: string[], key: string = 'code') {
        for (var i = 0; i < modules.length; i++) {
            if (this.hasAtleastOnePermission(modules[i]), key) {
                return true;
            }
        }
        return false;
    }

    getModule(val: string, key: string) {
        return this._modules.find((m: any) => m[key] === val);
    }

    getModuleByName(code: string) {
        return this._modules.find((m: { code: string; }) => m.code === code);
    }

    getModuleById(id: Number) {
        return this._modules.find((m: { id: number; }) => m.id === id);
    }

    get default_city(): {name: string, id: number} {
        return this.cities[0];
    }

    setup_initial_data() {
        if (!customer_params) {
            return;
        }
        this._cities = customer_params.cities.map(item => {
            return {
              name: item.city_name,
              id: item.id
            };
          });
        customer_params.cities.forEach(item => {
            this._city_coverage_pickup.set(item.id, item.coverage.pickup);
            this._city_coverage_dropoff.set(item.id, item.coverage.dropoff);
            this._city_coverage_pickup_by_name.set(item.city_name, item.coverage.pickup);
            this._city_coverage_dropoff_by_name.set(item.city_name, item.coverage.dropoff);
        });
        // NOTE: this needs to be changed
        if (window.location.pathname.startsWith('/vdaysale')
            || window.location.pathname.startsWith('/livetrack')
            || window.location.pathname.startsWith('/track')) {
            return;
        }
        if (this._is_navigator_geolocation = navigator.geolocation ? true : false) {
          navigator.geolocation.watchPosition(pos => {
            this._coordinates.next(pos.coords);
          });
        }
    }

    get_city_coverage(identifier: string | number, location_type: string): {lat: number, lng: number}[] {
        if (typeof identifier === 'string') {
            if (location_type === 'pickup') {
                return this._city_coverage_pickup_by_name.get(identifier);
            } else {
                return this._city_coverage_dropoff_by_name.get(identifier);
            }
        } else {
            if (location_type === 'pickup') {
                return this._city_coverage_pickup.get(identifier);
            } else {
                return this._city_coverage_dropoff.get(identifier);
            }
        }
    }

    get_dropdown_list(list: any[]): {}[] {
        return list.map( (item, index) => {
            return {
                name: item,
                id : index + 1
            };
        });
    }

    copy_object(object: {}): {} {
        return JSON.parse(JSON.stringify(object));
    }

    copy_array(array: any[]): any[] {
        return array.slice(0, array.length);
    }

    getCookieValue(name: string) {
        let cookie_value = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            cookie_value = cookies.find(item => {
                return item.trim().substring(0, name.length + 1) === (name + '=');
            });
        }
        return cookie_value.substring(cookie_value.indexOf('=') + 1);
    }

    get_trip_row_styles(current: boolean, upcoming: boolean, completed: boolean): {} {
        let styles = {};
        if (completed) {
            styles = {
                'grid-template-columns': 'repeat(auto-fit, minmax(100px, 1fr))'
            };
        } else if (upcoming) {
            styles = {
                'grid-template-columns': 'repeat(auto-fit, minmax(120px, 1fr))'
            };
        } else if (current) {
            styles = {
                'grid-template-columns': 'repeat(auto-fit, minmax(100px, 1fr))'
            };
        }
        return styles;
    }

    delete_token() {
        const tokenKey = 'auth._token.local';
        if (this.lsService.storageAvailable('localStorage')) {
            console.log('localStorage-->')
            this.lsService.remove_item(tokenKey)
        }
        const cookieExists: boolean = this.cookieService.check(tokenKey);
        if (cookieExists) {
            console.log('cookieExists-->', cookieExists)
            document.cookie = 'auth._token.local=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
    }

    convertObjectToQueryString(obj: {}) {
        return Object.keys(obj).map(key => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
        }).join('&');
    }

}
