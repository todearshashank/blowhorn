import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable, BehaviorSubject } from 'rxjs';
import { ShipmentsGrid } from '../models/shipments-grid.model';
import { ShipmentFilter } from '../models/shipment-filter.model';
import { BlowhornService } from './blowhorn.service';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Un_Assigned_Orders } from '../utils/types';
import { ApolloQueryResult } from 'apollo-client';
import { ShipmentAlertConfig } from '../models/shipment-alert.model';
import { AdditionalEntityDetails } from '../models/assets.model';

@Injectable()
export class ShipmentService {

    private _additional_details = new BehaviorSubject<AdditionalEntityDetails>(null);
    private _selectAll = new BehaviorSubject<boolean>(false);

  additional_details = this._additional_details.asObservable();
  selectAll = this._selectAll.asObservable();

  update_selected_entity_details(val: AdditionalEntityDetails) {
    this._additional_details.next(val);
  }

  updateSelectAll(val: any) {
    this._selectAll.next(val);
  }

  constructor(
    private http_client: HttpClient,
    private _apollo: Apollo,
  ) { }


  hexToRgbA(hex: string) {
    let color: any;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      color = hex.substring(1).split('');
      if (color.length === 3) {
        color = [
          color[0],
          color[0],
          color[1],
          color[1],
          color[2],
          color[2]
        ];
      }
      color = '0x' + color.join('');
      return 'rgba(' + [
        // tslint:disable-next-line: no-bitwise
        (color >> 16) & 255,
        // tslint:disable-next-line: no-bitwise
        (color >> 8) & 255,
        // tslint:disable-next-line: no-bitwise
        color & 255].join(',') + ',0.5)';
    }
    throw new Error('Bad Hex');
  }

  get_alert_level(
    alert_config: ShipmentAlertConfig[],
    start_time: string,
    end_time: string
  ): string {
    let delta_milliseconds = <any>new Date(end_time) - <any>new Date(start_time);
    let delta_minutes = Math.ceil(delta_milliseconds / 60000);
    let prev = 0;
    for (let i = 0; i < alert_config.length; i++) {
      if (prev <= delta_minutes
        && delta_minutes <= alert_config[i].time_since_creation) {
        prev = alert_config[i].time_since_creation;
        return this.hexToRgbA(alert_config[i].alert_colours);
      } else if (delta_minutes > alert_config[i].time_since_creation) {
        return this.hexToRgbA('#ff0000');
      }
    }
  }

  get_shipment_orders(url: string = Constants.urls.API_SHIPMENTS): Observable<ShipmentsGrid> {
    return this.http_client.get<ShipmentsGrid>(url);
  }

  get_shipment_filter(module: string = 'shipment_orders'): Observable<ShipmentFilter> {
    return this.http_client
      .get<ShipmentFilter>(`${Constants.urls.API_SHIPMENT_FILTER}?module=${module}`);
  }

  upload_shipment_orders(data: any) {
    return this.http_client
      .post(`${Constants.urls.API_SHIPMENT_ORDER_TEMPLATE_IMPORT}`, data, BlowhornService.http_xlsx_headers);
  }

  upload_orderlines(data: any) {
    return this.http_client
      .post(`${Constants.urls.API_SHIPMENT_ORDERLINE_TEMPLATE_IMPORT}`, data, BlowhornService.http_xlsx_headers);
  }

  // order line crud operations
  manage_orderlines(data: any) {
    return this.http_client
      .post(`${Constants.urls.API_SHIPMENT_ORDERLINES_CRUD}`, data, BlowhornService.http_xlsx_headers);
  }

  get_dashboard_orderlines(number: string): Observable<any> {
    let url = `${Constants.urls.API_SHIPMENT_ORDERLINES_CRUD}?order_number=${number}`;
    return this.http_client.get<any>(url);
  }

  get_assets(field: string, number: string): Observable<any> {
    let url = `${Constants.urls.API_SHIPMENT_ASSETS}?${field}=${number}`;
    return this.http_client.get<any>(url);
  }

  get_orderdetails(number: string): Observable<any> {
    let url = `${Constants.urls.API_SHIPMENT_ORDERDETAILS}?reference_number=${number}`;
    return this.http_client.get<any>(url);
  }

  post_order(query: any[]): Observable<any> {
    return this.http_client.post<any>(Constants.urls.API_SHIPMENT_ORDERDETAILS, query, BlowhornService.http_headers);
  }

  get_orderlines(field: string, number: string): Observable<any> {
    let url = `${Constants.urls.API_SHIPMENT_ORDERLINES}?${field}=${number}`;
    return this.http_client.get<any>(url);
  }

  get_hubdetails(): Observable<any> {
    return this.http_client.get<any>(Constants.urls.API_SHIPMENT_HUB);
  }

  createHomeDelivery(query: any[]): Observable<any> {
    return this.http_client.post<any>(Constants.urls.API_SHIPMENT_HOMEDELIVERY, query, BlowhornService.http_headers);
  }

  get_drivers(order_number: string): Observable<any> {
    return this.http_client.get<any>(`${Constants.urls.API_SHIPMENT_SLOT_DRIVERS}`, {
        params: {
            order_number: order_number
        }
    });
  }

  assign_driver(data: {order_number: string, driver_pk: number}): Observable<any> {
    return this.http_client.post<any>(Constants.urls.API_SHIPMENT_ASSIGN_DRIVER, data, BlowhornService.http_headers);
  }

  updateHomeDelivery(id: any, query: any[]): Observable<any> {
    let url = `${Constants.urls.API_SHIPMENT_HOMEDELIVERY}/${id}`;
    return this.http_client.put<any>(url, query);
  }

  downloadShippingLabel(params: string): Observable<any> {
    return this.http_client.get<any>(`${Constants.urls.API_SHIPPING_LABEL}?${params}`);
  }

  get_cod_payment_details(params: any): Observable<any> {
    return this.http_client.get<any>(`${Constants.urls.API_SHIPMENT_COD_DETAILS}?${params}`);
  }

  get_unassigned_shipment_orders(): Observable<ApolloQueryResult<Un_Assigned_Orders>> {
    return this._apollo.query<Un_Assigned_Orders>({
      query: gql`
        {
          unassignedOrders {
            number
            status
            datePlaced
            pickupAddress {
              id
              line1
              geopoint
              postcode
            }
            shippingAddress {
              id
              line1
              geopoint
              postcode
            }
            batch {
              id
              numberOfEntries
              description
            }
            city {
              id
              name
            }
            customer {
              id
              name
            }
          }
        }
      `,
      fetchPolicy: 'network-only'
    });
  }

}
