import { Component, Injectable, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Trip } from '../models/trip.model';
import { Constants } from '../utils/constants';
import { BlowhornService } from './blowhorn.service';


declare const Razorpay: any;

@Injectable()
export class PaymentService {

    razorpay: any;

    constructor(
        private _http_client: HttpClient,
        public bh_service: BlowhornService
    ) { }

    public initiate_razorpay_payment(trip: Trip) {
        let amount: any = trip.balance_payable || trip.total_payable;
        amount = (amount * 100).toString();
        const http_client = this._http_client;
        const options = {
            'key': environment.razorpay_api_key,
            'amount': amount,
            'name': 'blowhorn',
            'description': 'Logistic Services',
            'image': this.bh_service.logo_url,
            'prefill': {
                'name': '', // profile_data.name,
                'email': '', // profile_data.email,
                'contact': '', // profile_data.mobile
            },
            'notes' : {
                'booking_id' : trip.order_id,
                'booking_key' : trip.order_key,
                'type' : 'booking',
            },

            'handler': function (response: any) {
                const data: any = {
                    'booking_key': trip.order_key,
                    'txn_id': response.razorpay_payment_id,
                    'amount': amount,
                    'type': 'booking'
                };
                http_client
                    .post(Constants.urls.API_RAZORPAY, data)
                    .subscribe(res => {
                        trip.payment_status = 'paid';
                    });
            },
        };

        const razorpay = new Razorpay(options);
        razorpay.open();
    }

    public initiate_paytm_payment(trip: Trip) {
        const trip_id = trip.order_real_id;
        const trip_number = trip.order_key;
        let balance_payable = trip.balance_payable;
        window.location.href = `${Constants.urls.API_PAYTM_PAYMENT}/?entity_type=order&entity_id=${trip_id}&balance_payable=${balance_payable}&next=/track/${trip_number}`;
    }

    public initiate_payfast_payment(trip: Trip) {
        console.log(trip);
        const trip_id = trip.order_real_id;
        const trip_number = trip.order_key;
        window.location.href = `${Constants.urls.API_PAYFAST_PAYMENT}?entity_type=order&entity_id=${trip_id}`;
    }

    public initiate_wallet_payment(trip: Trip) {
        return this._http_client
            .post(Constants.urls.API_WALLET_PAYMENT, {'number': trip.order_key}, BlowhornService.http_headers);
    }
}
