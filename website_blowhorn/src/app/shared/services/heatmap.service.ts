import { Injectable } from '@angular/core';
import { Constants } from '../utils/constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApolloQueryResult } from 'apollo-client';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable()
export class HeatmapService {

  constructor(
    private _http_client: HttpClient,
    private _apollo: Apollo,
  ) { }

  get_heatmap_orders(params: {}): Observable<any> {
    return this._http_client
      .get<any>(Constants.urls.API_ORDER_HEATMAP, {params: params});
  }

  get_heapmap_order_filters(): Observable<any> {
    return this._http_client
      .get<any>(Constants.urls.API_ORDER_HEATMAP_FILTERS);
  }

  get_drivers(status: string): Observable<ApolloQueryResult<any>> {
    let status_filter = '';
    if (status) {
      status_filter = `(status: "${status.toLowerCase()}")`;
    }
    return this._apollo.query<any>({
      query: gql`
        {
          drivers ${status_filter} {
            edges {
              node {
                name
                driverVehicle
                status
                operatingCity {
                  id
                  name
                }
                driverpreferredlocationSet {
                  geopoint
                }
              }
            }
          }
        }
      `,
      fetchPolicy: 'network-only'
    });
  }
}
