import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';


@Injectable()
export class WhatsNewService {

  private _showDialog = false;
  constructor(
    private local_storage: LocalStorageService,
  ) {}

  public get displayDialog() {
    return this._showDialog;
  }

  public set displayDialog(showDialog: boolean) {
    this._showDialog = showDialog;
  }

}
