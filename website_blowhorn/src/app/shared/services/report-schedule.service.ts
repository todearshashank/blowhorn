import { NgModule,Injectable } from '@angular/core';
import { HttpClientModule, HttpClient,  HttpClientXsrfModule } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable } from 'rxjs';
import { BlowhornService } from './blowhorn.service';

@NgModule({
  imports: [HttpClientModule, HttpClientXsrfModule],
})

@Injectable({
  providedIn: 'root'
})
export class ReportScheduleService {

  constructor(
      private _http_client: HttpClient
  ) {}

  get_report_types(): Observable<any> {
    return this._http_client.get<any>(Constants.urls.API_REPORT_TYPES);
  }
  
  get_report_schedules(): Observable<any> {
    return this._http_client.get<any>(Constants.urls.API_REPORT_SCHEDULE);
  }

  add_report_schedule(data: any): Observable<any> {
    return this._http_client.post<any>(Constants.urls.API_REPORT_SCHEDULE_ADD,
        data, BlowhornService.http_headers);
  }

  delete_report_schedule(rid: number): Observable<any> {
    return this._http_client.delete<any>(`${Constants.urls.API_REPORT_SCHEDULE_DELETE}${rid}`,
    BlowhornService.http_headers);
  }

  update_report_schedule_users(data: any): Observable<any> {
    return this._http_client.put<any>(Constants.urls.API_REPORT_SCHEDULE_USER_SUBS_UPDATE,
        data, BlowhornService.http_headers);
  }
}
