import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable } from 'rxjs';
import { BankAccount } from '../models/bank.model';
import { BlowhornService } from './blowhorn.service';

@Injectable()
export class BankAccountsService {

  constructor(
    private http_client: HttpClient
  ) { }

    getBankAccounts(): Observable<any> {
        let url = Constants.urls.API_CUSTOMER_BANK_ACCOUNTS;
        // url = 'https://api.npoint.io/1d531195fee36a472a4c'
        return this.http_client.get<any>(url)
    }

    saveBankAccount(data: BankAccount): Observable<BankAccount> {
        return this.http_client.post<BankAccount>(
            Constants.urls.API_CUSTOMER_BANK_ACCOUNTS,
            data,
            BlowhornService.http_headers
        );
    }

    updateBankAccount(data: BankAccount): Observable<BankAccount> {
        return this.http_client.put<BankAccount>(
            `${Constants.urls.API_CUSTOMER_BANK_ACCOUNTS}/${data.id}`,
            data,
            BlowhornService.http_headers
        );
    }

    getBankDetailsFromIFSC(ifsc: string) {
        return this.http_client.get<any>(`${Constants.urls.API_RAZORPAY_IFSC}${ifsc}`);
    }

    deleteBankAccount(pk: number): Observable<any> {
        return this.http_client.delete<any>(`${Constants.urls.API_CUSTOMER_BANK_ACCOUNTS}/${pk}`,
            BlowhornService.http_headers);
    }

}
