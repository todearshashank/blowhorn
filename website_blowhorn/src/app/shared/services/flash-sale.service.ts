import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { BlowhornService } from './blowhorn.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class FlashSaleService {

    constructor(
        private _http_client: HttpClient,
    ) { }

    get_support_data(): any {
        return;
    }

    create_order(data: any): Observable<any> {
        return this._http_client
            .post<any>(
                Constants.urls.API_FLASHSALE_CREATE_ORDER,
                data);
    }

    update_order(data: any): Observable<any> {
        return this._http_client
            .put<any>(
                Constants.urls.API_FLASHSALE_CREATE_ORDER,
                data,
                BlowhornService.http_headers
            );
    }
}
