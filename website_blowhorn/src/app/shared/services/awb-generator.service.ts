import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable } from 'rxjs';
import { BlowhornService } from './blowhorn.service';

// @NgModule({
//   imports: [HttpClientModule, HttpClientXsrfModule],
// })

@Injectable({
  providedIn: 'root'
})
export class AwbGeneratorService {

  constructor(private _http_client: HttpClient) { 
  }


  get_awb_list(data: any): Observable<any> {
    return this._http_client.post<any>(Constants.urls.API_AWBGENERATOR_AWBS,
        data, BlowhornService.http_headers);
  }

  get_hub_list(): Observable<any> {
    return this._http_client.get<any>(Constants.urls.API_HUBLIST);
  }

  get_awb_records(data: any): Observable<any> {
    return this._http_client.post<any>(Constants.urls.API_AWBGENERATOR_AWBLIST,
        data, BlowhornService.http_headers);
  }
}
