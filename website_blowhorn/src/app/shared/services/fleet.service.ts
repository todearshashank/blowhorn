import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FleetInfoWindow } from '../models/fleet-info-window.model';

@Injectable()
export class FleetService {

  constructor(
    private http_client: HttpClient,
  ) { }

  get_fleet_info_window(id: number): Observable<FleetInfoWindow> {
    return this.http_client
      .get<FleetInfoWindow>(`/api/fleet/drivers/${id}/status`);
  }

}
