import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { loadavg } from 'os';
import { logging } from 'protractor';
@Injectable({
    providedIn: 'root',
})
export class LoggingService {

    lastMessage: string;

    constructor(
        private _http_client: HttpClient,
    ) { }

    logError(message: string, stack: string) {
        console.log(message, stack);
        if (message === this.lastMessage) {
            console.log('Error repeated. Not reporting.');
            return;
        }
        // this.lastMessage = message;
        // this._http_client
        //     .post<any>(
        //         Constants.urls.API_REPORT_ERROR,
        //         {
        //             'message': message,
        //             'stack': stack
        //         }).subscribe();
    }
}
