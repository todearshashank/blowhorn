import { Address } from '../models/address.model';
import { Injectable } from '@angular/core';
import { BlowhornService } from './blowhorn.service';
import { MapsAPILoader } from '@agm/core';


declare var country_code: string;

@Injectable()
export class MapService {

    constructor(
        public _blowhorn_service: BlowhornService,
        private _maps_api_loader: MapsAPILoader,
    ) {
        this._blowhorn_service
            .coordinates
            .subscribe(coordinates => {
                if (coordinates) {
                    this._current_position = MapService.map_position_to_geopoint([coordinates.latitude, coordinates.longitude]);
                }
            });
    }

    private _current_position: google.maps.LatLng;
    private _geocoder: google.maps.Geocoder;
    private _gmap_addr = {
        street_number: 'short_name',
        premise: 'short_name',
        subpremise: 'short_name',
        route: 'short_name',
        neighborhood: 'short_name', // Area1.1.1.1
        sublocality_level_3: 'long_name', // Area1.1.1
        sublocality_level_2: 'long_name', // Area1.1
        sublocality_level_1: 'long_name', // Area1
        sublocality: 'long_name', // General Area
        locality: 'long_name', // City
        postal_code: 'short_name', // Pin code
        administrative_area_level_1: 'long_name', // State
        country: 'long_name', // Country
    };
    private _render_options = {
        suppressMarkers: true,
        draggable: false,
        preserveViewport: true,
        polylineOptions: {
            geodesic: true,
            strokeColor: '#f74757',
            strokeOpacity: 1.0,
            strokeWeight: 4
        }
    };

    static map_position_to_geopoint(position: [number, number]): google.maps.LatLng {
        return new google.maps.LatLng(position[0], position[1]);
    }

    covert_latlng_to_geopoint(latitude: number, longitude: number): google.maps.LatLng {
        return new google.maps.LatLng(latitude, longitude);
    }

    static map_geopoint_to_position(geopoint: google.maps.LatLng): [number, number] {
        return [geopoint.lat(), geopoint.lng()];
    }

    static get dummy_geopoint(): google.maps.LatLng {
        return new google.maps.LatLng(null, null);
    }

    static get zero_geopoint(): google.maps.LatLng {
        return new google.maps.LatLng(0, 0);
    }

    static get dummy_position(): [number, number] {
        return [null, null];
    }

    static get_autocomplete(el: HTMLInputElement): google.maps.places.Autocomplete {
        return new google.maps.places.Autocomplete(el, {
            // types: ['address'],
            componentRestrictions: {
                country: country_code
            }
        });
    }

    static get_circle(loc: google.maps.LatLng, radius: number): google.maps.Circle {
        return new google.maps.Circle({
            center: loc,
            radius: radius
        });
    }

    static get dummy_bounds(): google.maps.LatLngBounds {
        return new google.maps.LatLngBounds();
    }

    getAddressComponentsObject(addressComponents): Address {
        const components = this.getComponents(addressComponents);
        const addressDetails = [
            this.getStreetAddressFromComponents(components),
            this.getSubLocalityFromAddressComponents(components),
            this.getCityFromAddressComponents(components),
            this.getPostalCodeFromAddressComponents(components),
            this.getStateFromAddressComponents(components)
        ];
        return new Address(
            null,
            null,
            addressDetails.join(' ').replace(/\s+/g, ' ').trim(),
            addressDetails[0],
            addressDetails[1],
            addressDetails[2],
            addressDetails[4],
            this._blowhorn_service.country_code,
            addressDetails[3]
        );
    }

    getCityFromAddressComponents(components) {
        return components.locality;
    }

    getPostalCodeFromAddressComponents(components) {
        return components.postal_code;
    }

    getStateFromAddressComponents(components) {
        return components.administrative_area_level_1;
    }

    getSubLocalityFromAddressComponents(components) {
        if (components.sublocality_level_1 !== '') {
            return components.sublocality_level_1;
        } else if (components.sublocality_level_2 !== '') {
            return components.sublocality_level_2;
        } else if (components.sublocality_level_3 !== '') {
            return components.sublocality_level_3;
        }
        return components.sublocality;
    }

    getStreetAddressFromComponents(components) {
        let a: any = [
            components.street_number,
            components.premise,
            components.subpremise,
            components.route,
            components.neighborhood,
            components.sublocality_level_3,
            components.sublocality_level_2,
        ];
        const b = [];
        const sublocality = this.getSubLocalityFromAddressComponents(components);
        const s_min = sublocality.replace(/\s/g, '').toLowerCase();

        for (let i = 0; i < a.length; i++) {
            const a_min = a[i].replace(/\s/g, '').toLowerCase();
            if (a_min === s_min) {
                continue;
            }
            if (b.join(' ').replace(/\s/g, '').toLowerCase().search(a_min) >= 0) {
                continue;
            }
            b.push(a[i]);
        }
        a = b.join(' ');
        a = a.replace(this.getSubLocalityFromAddressComponents(components), '');
        a = a.replace(/\s+/g, ' ');
        a = a.trim();
        return a;
    }

    getComponents(address_components): {} {
        const return_data = {};

        for (const k in this._gmap_addr) {
            if (this._gmap_addr.hasOwnProperty(k)) {
                return_data[k] = '';
            }
        }
        for (let i = 0; i < address_components.length; i++) {
            let addressType;
            if (address_components[i].types[0] === 'political') {
                addressType = address_components[i].types[2];
            } else {
                // Assign other component types like locality, country, postal_code
                addressType = address_components[i].types[0];
            }
            if (this._gmap_addr[addressType]) {
                const val = address_components[i][this._gmap_addr[addressType]];
                return_data[addressType] = val;
            }
        }
        return return_data;
    }

    get_geocode(geopoint: google.maps.LatLng): Promise<any> {
        console.log(geopoint);
        return new Promise((resolve, reject) => {
            this._geocoder
                .geocode({ location: geopoint }, (results, status) => {
                    if (status === google.maps.GeocoderStatus.OK) {
                        console.log(status);
                        resolve(results);
                    } else {
                        reject(status);
                    }
                });
        });
    }

    get current_position(): google.maps.LatLng {
        return this._current_position;
    }

    set current_position(current_position: google.maps.LatLng) {
        this._current_position = current_position;
    }

    get_bounds(identifier: string | number, location_type: string): google.maps.LatLngBounds {
        const bounds = new google.maps.LatLngBounds();
        this._blowhorn_service
            .get_city_coverage(identifier, location_type)
            .forEach(item => {
                bounds.extend(item);
            });
        return bounds;
    }

    get_polygon(identifier: string | number, location_type: string): google.maps.Polygon {
        const polygon = new google.maps.Polygon();
        polygon.setPaths(this._blowhorn_service.get_city_coverage(identifier, location_type));
        return polygon;
    }

    is_location_in_city(
        city: number | string,
        location: google.maps.LatLng,
        location_type: string): boolean {
        return google.maps.geometry.poly.containsLocation(location, this.get_polygon(city, location_type));
    }

    is_current_location_in_city(city: number | string, location_type: string): boolean {
        return this._current_position ? this.is_location_in_city(city, this._current_position, location_type) : false;
    }

    get_city_from_location(cities: any): string {
        let total_cities = cities.length;
        let city = '';
        for (let i = 0; i < total_cities; i++) {
            let _city = cities[i];
            if (this.is_current_location_in_city(_city.name, 'dropoff')
                || this.is_current_location_in_city(_city.name, 'pickup')) {
                city = _city;
                break;
            }
        }
        return city;
    }

    get render_options(): {} {
        return this._render_options;
    }

    load_google_maps_api() {
        this._maps_api_loader
            .load()
            .then(() => {
                this._geocoder = new google.maps.Geocoder;
            });
    }

    get_geopoint_from_address(address: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this._geocoder
                .geocode({ address: address }, (results, status) => {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resolve(results);
                    } else {
                        reject(status);
                    }
                });
        });
    }

    distanceBetween2Points(location1: any, location2: any) {
        console.log('location2: ', location1, location2);
        return google.maps.geometry.spherical.computeDistanceBetween(
            new google.maps.LatLng(parseFloat(location1.lat), parseFloat(location1.lng)),
            new google.maps.LatLng(parseFloat(location2.lat), parseFloat(location2.lng)),
        );
    }

    initDirectionsService() {
        return new google.maps.DirectionsService();
    }

    getDirections(directionService: any, requestPayload: any): Promise<any> {
        // refer: https://developers.google.com/maps/documentation/directions/intro#DirectionsRequests
        return new Promise((resolve, reject) => {
            directionService.route(requestPayload, function (results: any, status: string) {
                if (status === google.maps.GeocoderStatus.OK) {
                    resolve(results);
                } else {
                    reject(status);
                }
            });
        });
    }

    getDistanceAndTime(result: any) {
        if (!result) {
            return {
                total_distance_meters: 0,
                total_time_seconds: 0
            }
        }
        let total_distance_meters = result.routes[0].legs.reduce(function (a: number, b: any) {
            return a + b.distance.value;
        }, 0);
        let total_time_seconds = result.routes[0].legs.reduce(function (a: number, b: any) {
            return a + b.duration.value + (b.duration_in_traffic ? b.duration_in_traffic.value : 0);
        }, 0);
        return {
            total_distance_meters: total_distance_meters,
            total_time_seconds: total_time_seconds
        }
    }

    getCentroidFromPolygon(polygon: any) {
        let lowx: number,
            highx: number,
            lowy: number,
            highy: number,
            lats = [],
            lngs = [],
            vertices = polygon.getPath();

        for(var i = 0; i < vertices.length; i++) {
            lngs.push(vertices.getAt(i).lng());
            lats.push(vertices.getAt(i).lat());
        }

        lats.sort();
        lngs.sort();
        lowx = lats[0];
        highx = lats[vertices.length - 1];
        lowy = lngs[0];
        highy = lngs[vertices.length - 1];
        let center_x = lowx + ((highx-lowx) / 2);
        let center_y = lowy + ((highy - lowy) / 2);
        return new google.maps.LatLng(center_x, center_y);
    }
}
