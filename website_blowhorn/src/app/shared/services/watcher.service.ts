import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { City } from '../models/city.model';
import { IdName } from '../models/id-name.model';
import { Driver } from '../models/driver.model';
import { ShipmentAlertConfig } from '../models/shipment-alert.model';
// import { slotwise_report } from '../utils/types';
import { SlotwiseData, StatuswiseData, OrderData, CODOrderData, HubOrderData, ShipmentDelivery,

} from '../models/report.model'


@Injectable()
export class WatcherService {

  private _city_store = new BehaviorSubject<City[]>([]);
  private _state_store = new BehaviorSubject<IdName[]>([]);
  private _hub_store = new BehaviorSubject<IdName[]>([]);
  private _vehicle_type_store = new BehaviorSubject<IdName[]>([]);
  private _driver_store = new BehaviorSubject<Driver[]>([]);
  private _contract_types = new BehaviorSubject<any[]>([]);
  private _status_store = new BehaviorSubject<string[]>([]);
  private _status_legend_store = new BehaviorSubject<any>([]);
  private _show_save_route = new BehaviorSubject<boolean>(false);
  private _show_save_address = new BehaviorSubject<boolean>(false);
  private _show_save_contact = new BehaviorSubject<boolean>(false);
  private _enable_trip_export = new BehaviorSubject<boolean>(false);
  private _order_status_colors = new BehaviorSubject<{}>({});
  private _alert_config = new BehaviorSubject<ShipmentAlertConfig[]>([]);
  private _slotwise_report = new BehaviorSubject<SlotwiseData[]>([]);
  private _statuswise_report = new BehaviorSubject<StatuswiseData[]>([]);
  private _hubwise_data = new BehaviorSubject<any>({});
  private _ordercreated_time = new BehaviorSubject<any>({});
  private _order_report = new BehaviorSubject<OrderData>({ max_order:0,
    time_range:0,
    avg_completion_time:0,
    reattempted_orders:0,
    total_attempts:0,
    first_attempt:0,
    second_attempt:0,
    third_attempt:0});
  private _codorder_report = new BehaviorSubject<CODOrderData>({
    total_orders:0,
    cash_collected:0
  });
  private _shipmentorder_report = new BehaviorSubject<HubOrderData>({
    total_orders :0,
    shipment_lost:0,
    shipmenthub:0
  });
  private _filter_changed_method = new BehaviorSubject<boolean>(false);
  public _shipment_delivery_percent = new BehaviorSubject<ShipmentDelivery>({
    shipment_delivery:0
  });

  hub_store = this._hub_store.asObservable();
  city_store = this._city_store.asObservable();
  state_store = this._state_store.asObservable();
  vehicle_type_store = this._vehicle_type_store.asObservable();
  driver_store = this._driver_store.asObservable();
  contract_types = this._contract_types.asObservable();
  status_store = this._status_store.asObservable();
  status_legend_store = this._status_legend_store.asObservable();
  show_save_route = this._show_save_route.asObservable();
  show_save_address = this._show_save_address.asObservable();
  show_save_contact = this._show_save_contact.asObservable();
  enable_trip_export = this._enable_trip_export.asObservable();
  order_status_colors = this._order_status_colors.asObservable();
  alert_config = this._alert_config.asObservable();
  slotwise_report = this._slotwise_report.asObservable();
  statuswise_report = this._statuswise_report.asObservable();
  order_report = this._order_report.asObservable();
  codorder_report = this._codorder_report.asObservable();
  shipmentorder_report = this._shipmentorder_report.asObservable();
  filter_changed_method = this._filter_changed_method.asObservable();
  shipment_delivery_percent = this._shipment_delivery_percent.asObservable();
  hubwise_data = this._hubwise_data.asObservable();
  ordercreated_time = this._ordercreated_time.asObservable();

  constructor() { }

  update_city_store(city_store: City[]) {
    this._city_store.next(city_store);
  }

  update_state_store(state_store: IdName[]) {
    this._state_store.next(state_store);
  }

  update_hub_store(hub_store: IdName[]) {
    this._hub_store.next(hub_store);
  }

  update_vehicle_type_store(vehicle_type_store: IdName[]) {
    this._vehicle_type_store.next(vehicle_type_store);
  }

  update_driver_store(driver_store: Driver[]) {
    this._driver_store.next(driver_store);
  }

  update_contract_types(val: any[]) {
    this._contract_types.next(val);
  }

  update_status_store(status_store: string[]) {
    this._status_store.next(status_store);
  }

  update_status_legend_store(val: any) {
    this._status_legend_store.next(val);
  }

  update_alert_config(val: ShipmentAlertConfig[]) {
    this._alert_config.next(val);
  }

  update_status_color_store(val: {}) {
    this._order_status_colors.next(val);
  }

  update_show_save_route(show_save_route: boolean) {
    this._show_save_route.next(show_save_route);
  }

  update_show_save_address(show_save_address: boolean) {
    this._show_save_address.next(show_save_address);
  }

  update_show_save_contact(show_save_contact: boolean) {
    this._show_save_contact.next(show_save_contact);
  }

  update_enable_trip_export(enable: boolean) {
    this._enable_trip_export.next(enable);
  }

  update_slotwise_data(val: SlotwiseData[]) {
    this._slotwise_report.next(val);
  }

  update_statuswise_data(val:StatuswiseData[]){
    this._statuswise_report.next(val);
  }

  update_order_data(val:OrderData){
    this._order_report.next(val);
  }

  update_codorder_data(val:CODOrderData){
    this._codorder_report.next(val);
  }

  update_shipmentorder_data(val:HubOrderData){
    this._shipmentorder_report.next(val);
  }

  update_filter_changed_method(filter_changed_method: boolean) {
    this._filter_changed_method.next(filter_changed_method);
  }

  update_shipment_delivery(val:ShipmentDelivery){
    this._shipment_delivery_percent.next(val);
  }

  update_hubwise_data(val:any){
    this._hubwise_data.next(val);
  }

  update_ordercreated_time(val:any){
    this._ordercreated_time.next(val);
  }
}
