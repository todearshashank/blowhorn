import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceAllocationFilter, Contract } from '../models/resource-allocation.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { Constants } from '../utils/constants';
import { IdName } from '../models/id-name.model';
import { BlowhornService } from './blowhorn.service';
import { AllocationHistory } from '../models/resource-allocation.model';

@Injectable({
    providedIn: 'root'
})
export class ResourceAllocationService {

    private _city_store = new BehaviorSubject<IdName[]>([]);
    private _division_store = new BehaviorSubject<IdName[]>([]);
    private _customer_store = new BehaviorSubject<IdName[]>([]);
    private _contract_store = new BehaviorSubject<Contract[]>([]);
    public contract_url = '';

    city_store = this._city_store.asObservable();
    customer_store = this._customer_store.asObservable();
    division_store = this._division_store.asObservable();
    contract_store = this._contract_store.asObservable();

    constructor(
        private http_client: HttpClient,
    ) { }

    update_city_store(city_store: IdName[]) {
        this._city_store.next(city_store);
    }

    update_division_store(division_store: IdName[]) {
        this._division_store.next(division_store);
    }

    update_customer_store(customer_store: IdName[]) {
        this._customer_store.next(customer_store);
    }

    update_contract_store(contract_store: Contract[]) {
        this._contract_store.next(contract_store);
    }

    get_support_data(): Observable<ResourceAllocationFilter> {
        return this.http_client.get<ResourceAllocationFilter>(Constants.urls.API_RESOURCE_SUPPORT_DATA);
    }

    get_contracts(url: string): Observable<any> {
        return this.http_client.get<any>(`${url}`);
    }

    get_resources(contract_pk: number): Observable<any> {
        return this.http_client.get<any>(`${Constants.urls.API_RESOURCE_RESOURCES}/${contract_pk}`);
    }

    update_resource(contract_pk: number, data: any): Observable<any> {
        return this.http_client.post<any>(`${Constants.urls.API_RESOURCE_RESOURCES}/${contract_pk}`,
            data, BlowhornService.http_headers);
    }

    delete_resource(resource_id: number): Observable<any> {
        return this.http_client.post<any>(`${Constants.urls.API_RESOURCE_RESOURCE_DELETE}${resource_id}`, {},
        BlowhornService.http_headers);
    }

    get_allocation_history(contract_pk: number): Observable<AllocationHistory[]> {
        return this.http_client.get<AllocationHistory[]>(`${Constants.urls.API_RESOURCE_ALLOCATION_HISTORY}/${contract_pk}`);
    }
}
