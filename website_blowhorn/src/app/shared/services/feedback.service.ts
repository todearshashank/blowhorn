import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable, BehaviorSubject } from 'rxjs';
import { BlowhornService } from './blowhorn.service';

@Injectable()
export class FeedbackService {

  constructor(
    private http_client: HttpClient
  ) { }

    get_customerdetails(number: string): Observable<any> {
        let url = `${Constants.urls.API_ALT_DETAIL_TRIP}/${number}`;
        return this.http_client.get<any>(url);
    }

    get_remarks(): Observable<any> {
        return this.http_client.get<any>(Constants.urls.API_PARAMS);
    }

    createFeedback(query: any[]): Observable<any> {
        return this.http_client.post<any>(Constants.urls.API_FEEDBACK, query, BlowhornService.http_headers);
    }

}
