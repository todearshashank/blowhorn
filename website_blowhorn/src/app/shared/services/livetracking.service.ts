import { NgModule, Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpClientXsrfModule, HttpXsrfTokenExtractor } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { Observable, BehaviorSubject } from 'rxjs';
import { LivetrackingFilter } from '../models/livetracking-filter.model';
import { DriverTrackingDetail, Extras } from '../models/driver-tracking-detail.model';
import { Driver } from '../models/driver.model';
import { map } from 'rxjs/operators';
import { BlowhornService } from './blowhorn.service';

@NgModule({
    imports: [HttpClientModule, HttpClientXsrfModule],
})

@Injectable()
export class LivetrackingService {

    private _driver_markers = new BehaviorSubject<any>([]);
    private _filter_vehicle_class = new BehaviorSubject<any>([]);
    private _filter_driver_status = new BehaviorSubject<any>([]);
    private _filter_fixed_driver = new BehaviorSubject<any>([]);
    private _filter_covid_pass = new BehaviorSubject<any>('both');

    driver_markers = this._driver_markers.asObservable();
    filter_vehicle_class = this._filter_vehicle_class.asObservable();
    filter_driver_status = this._filter_driver_status.asObservable();
    filter_fixed_driver = this._filter_fixed_driver.asObservable();
    filter_covid_pass = this._filter_covid_pass.asObservable();

    private _http_options: any;

    constructor(
        private http_client: HttpClient,
        public bh_service: BlowhornService,
    ) {
        const token = this.bh_service.getCookieValue('csrftoken');
        this._http_options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'X-CSRFToken': token
            })
        };
    }

    get_gps_timestamp_human_readable_format(mTime: number) {
        let dTime = new Date(mTime);
        return dTime.toLocaleDateString('en-IN').replace(/\//g, '-') + ' ' + dTime.toLocaleTimeString('en-IN');
    }

    update_driver_markers(val: any) {
        this._driver_markers.next(val);
    }

    update_filter_vehicle_class(val: any) {
        this._filter_vehicle_class.next(val);
    }

    update_filter_driver_status(val: any) {
        this._filter_driver_status.next(val);
    }

    update_filter_fixed_driver(val: any) {
        this._filter_fixed_driver.next(val);
    }

    update_filter_covid_pass(val: any) {
        this._filter_covid_pass.next(val);
    }

    get_battery_icon(percent: any) {
        let battery_info = {};

        switch (true) {
            case percent < 10:
                battery_info = Constants.battery_percent_icons['empty'];
                break;

            case percent > 10 && percent <= 30:
                battery_info = Constants.battery_percent_icons['quarter'];
                break;

            case percent > 30 && percent <= 60:
                battery_info = Constants.battery_percent_icons['half'];
                break;

            case percent > 60 && percent <= 95:
                battery_info = Constants.battery_percent_icons['three-querter'];
                break;

            case percent > 95:
                battery_info = Constants.battery_percent_icons['full'];
                break;

            default:
                battery_info = Constants.battery_percent_icons['empty'];
        }
        return battery_info;
    }

    // API calls
    get_support_data(): Observable<LivetrackingFilter> {
        return this.http_client.get<LivetrackingFilter>(Constants.urls.API_TRACKING_PARAMS);
    }

    get_driver_locations(): Observable<DriverTrackingDetail> {
        return this.http_client.get<DriverTrackingDetail>(Constants.urls.API_DRIVER_LOCATIONS);
    }

    get_trip_details(): Observable<any> {
        return this.http_client.get<any>(Constants.urls.API_DRIVER_TRIP_DETAILS);
    }

    get_driver_profile(driver_id: number): Observable<Driver> {
        return this.http_client.get<Driver>(`${Constants.urls.API_DRIVER_PROFILE}/${driver_id}`);
    }

    get_resource_allocation_info(): Observable<any> {
        return this.http_client.get<any>(`${Constants.urls.API_LIVETRACKING_RESOURCE_DETAILS}`);
    }

    get_firebase_auth_token() {
        return this.http_client.get(Constants.urls.API_FIREBASE_TOKEN).pipe(map((resp: any) => {
            const response = JSON.parse(resp._body);
            return response.firebase_auth_token;
        }));
    }

    send_action_to_driver(data: any): Observable<any> {
        return this.http_client.post(Constants.urls.API_DRIVER_ACTION, data, this._http_options).pipe(map((resp: any) => {
            return resp;
        }));
    }

}
