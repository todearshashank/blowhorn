import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { MapService } from './map.service';
import { BlowhornService } from './blowhorn.service';

/**********************************************
 * Parameters to process location updates and
 * filter out noise */
const MIN_DISTANCE_DELTA_METERS = 10;
const MIN_TIMEDELTA_MS = 1000;
const MAX_SPEED_KMPH = 100;
const DISTANCE_MULTIPLIER = 1.3;
const AVERAGE_VEHICLE_SPEED_KMPH = 15.0;
/*********************************************/

const DATE_FORMAT = 'YYYY-MM-DD';
const TIME_FORMAT = 'hh:mm A';

@Injectable()
export class LocationService {

    directionsService = this.mapService.initDirectionsService();

    constructor(
        private mapService: MapService,
        private bh_service: BlowhornService
    ) { }

    /* Sanity check if latest received location is actually up to date. */
    isNewLocationNewer(old_location: any, new_location: any) {
        if (old_location == null) {
            return true;
        } else if (new_location.mAccuracy > 30) {
            return false;
        }
        const old_: any = new Date(old_location.mTime);
        const new_: any = new Date(new_location.mTime);
        const distance_meters = this.mapService.distanceBetween2Points(old_location, new_location);
        const timedelta_ms = new_ - old_;
        const speed_kmph = (distance_meters * 60 * 60) / timedelta_ms;
        const newer = (timedelta_ms > MIN_TIMEDELTA_MS &&
            distance_meters > MIN_DISTANCE_DELTA_METERS &&
            speed_kmph < MAX_SPEED_KMPH
        );
        return [newer, distance_meters];
    }

    async annotateTimeAndDistance(stops: any, driverLocation: any, pickupDatetime: any, status: any) {
        let p1 = stops[0];
        let firstPendingFound = false;
        let firstPendingStopIndex = -1;
        let i = 0;
        let googleCallCount = 0;
        let totalStops = stops.length;
        let nextStop = null;
        /*Since app is not calculating ETA at client, time wont be null for
            upcoming stops as well because it's been already calculated at server*/
        for (let m = 0; m < totalStops; m++) {
            let stop = stops[i];
            if (stop.status === 'pending') {
                stops[i].iso_time = null;
            }
        }
        for await (let stop of stops) {
            let start_location = null;
            if (!firstPendingFound) {
                firstPendingStopIndex++;
                if (stop.status === 'done' && stop.iso_time) {
                    stop.iso_time = stop.iso_time ? new Date(stop.iso_time) : new Date();
                    stop.time = moment(stop.iso_time).format(TIME_FORMAT);
                    stop.date = moment(stop.iso_time).format(DATE_FORMAT);
                    stop.remaining_distance = 0;
                    continue;
                } else {
                    firstPendingFound = true;
                    start_location = driverLocation ?
                        this.mapService.covert_latlng_to_geopoint(
                            driverLocation.lat,
                            driverLocation.lng
                        ) : stop.location;
                }
            }
            let start_time = p1.iso_time && p1.iso_time > new Date() ? p1.iso_time : new Date();
            pickupDatetime = pickupDatetime ? new Date(pickupDatetime) : new Date();
            start_time = !start_time ? new Date() : new Date(start_time);

            if (i == 0 && status === 'upcoming') {
                stop.iso_time = (new Date() > pickupDatetime) ? start_time : pickupDatetime;
            } else {
                if (p1.location && p1.location.lat && stop.location && stop.location.lat) {
                    let departureTimeP1 = new Date();
                    if (p1.iso_time) {
                        if (p1.iso_time > new Date()) {
                            departureTimeP1 = p1.iso_time;
                        }
                    } else if (start_time > new Date()) {
                        departureTimeP1 = start_time;
                    }

                    if (!stop.location.lat || !stop.location.lng) {
                        console.log('Stop location is not present..!');
                        continue;
                    }
                    let data = await this.getDistanceAndTime(p1.location, [], stop.location, departureTimeP1);
                    let total_distance = data.total_distance_meters;
                    let total_time = data.total_time_seconds;

                    let result = await this.getDistanceAndTime(
                        start_location || p1.location,
                        [],
                        stop.location,
                        p1.iso_time > new Date() ? p1.iso_time : new Date()
                    );
                    let remaining_distance = result.total_distance_meters;
                    let remaining_time_mins = result.total_time_seconds / 60;
                    let _datetime = moment(start_time).add(remaining_time_mins, 'minutes');
                    stop.iso_time = new Date(_datetime.toDate());
                    stop.estimated_distance = (total_distance / 1000).toFixed(1);
                    stop.remaining_distance = remaining_distance;
                    stop.time = moment(stop.iso_time).format(TIME_FORMAT);
                    stop.date = moment(stop.iso_time).format(DATE_FORMAT);
                    stop.remaining_time_mins = remaining_time_mins;
                    if (!nextStop) {
                        nextStop = Object.assign({}, stop);
                    }
                    googleCallCount++;
                } else {
                    stop.estimated_distance = 0
                    stop.remaining_distance = 0
                    stop.remaining_time_mins = 0;
                    stop.time = ''
                    stop.date = ''
                }
            }
            p1 = stop;
            i++;
            if (googleCallCount > 2) {
                // Cannot calculate time&distance due to query-limit
                for (var j = i; j < stops.length; j++) {
                    let s = stops[j];
                    if (s.status != 'done') {
                        s.time = '--NA--'
                    }
                }
                break;
            }
        }
        return {
            stops: stops,
            nextStopEta: nextStop ? nextStop.remaining_time_mins : 0
        };
    }

    getDistanceAndTime(origin: any, waypoints: any, destination: any, departureTime: any): Promise<any> {
        let request = {
            origin: {lat: origin.lat, lng: origin.lng},
            waypoints: waypoints,
            destination: {lat: destination.lat, lng: destination.lng},
            travelMode: google.maps.TravelMode.DRIVING,
            /* WARNING: Using `drivingOptions` is under Advanced Direction API.
                        Disabling it to reduce billing
            */
            // drivingOptions: {
            //     departureTime: departureTime || new Date(),
            // },
        };
        return new Promise((resolve, reject) => {
            this.mapService.getDirections(this.directionsService, request).then(results => {
                return resolve(this.mapService.getDistanceAndTime(results));
            });
        });
    }
}
