import { Injectable, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Trip } from '../models/trip.model';
import { Trips } from '../utils/enums';
import { Subject ,  Observable, BehaviorSubject } from 'rxjs';
import { Constants } from '../utils/constants';
import { UserLocation } from '../models/user-location.model';
import { BlowhornService } from './blowhorn.service';

@Injectable()
export class TripsService {

  private _current_tab = Trips.Current;
  private _options: any;
  private trips_subject = new Subject<any>();

  private _is_eta_completed = new BehaviorSubject<any>(false);
  is_eta_completed = this._is_eta_completed.asObservable();

  constructor(
    private _http_client: HttpClient,
    public _blowhorn_service: BlowhornService,
  ) {}

  update_is_eta_completed(val: any) {
    this._is_eta_completed.next(val);
  }

  public get current_tab(): any {
    return this._current_tab;
  }

  public set current_tab(tab: any) {
    this._current_tab = tab;
  }

  refresh_trips() {
    this.trips_subject.next();
  }

  get_trip_triggers(): Observable<any> {
    return this.trips_subject.asObservable();
  }

  public get_trip_details(id: number) {
    const url = Constants.urls.API_ALT_DETAIL_TRIP;
    return this._http_client
      .get<Trip>(`${url}/${id}`);
  }

  public get_summary_trips() {
    const url = Constants.urls.API_ALT_TRIP_SUMMARY;
    return this._http_client
      .get(url);
  }

  public get_locations() {
    return this._http_client
      .get<UserLocation[]>(Constants.urls.API_LOCATIONS);
  }

  public get_location(id: string | number) {
    return this._http_client
      .get<UserLocation>(`${Constants.urls.API_LOCATIONS}/${id}`);
  }

  public create_location(location: UserLocation) {
    return this._http_client
      .post<UserLocation>(Constants.urls.API_LOCATIONS, location);
  }

  public update_location(location: UserLocation) {
    return this._http_client
      .patch<UserLocation>(`${Constants.urls.API_LOCATIONS}/${location.id}`, location);
  }

  public remove_location(locationId: number) {
    return this._http_client
      .delete(`${Constants.urls.API_LOCATIONS}/${locationId}`);
  }

  public get_params() {
    return this._http_client.get(Constants.urls.API_PARAMS);
  }

  public create_trip(from_loc, to_loc, truck_type) {
    const creation_data = {
      'from': from_loc,
      'to': to_loc,
      'truck_type': truck_type
    };
    return this._http_client
      .post<Trip>(Constants.urls.API_QUICKBOOK, creation_data);
  }

  public rebook_trip(datetime: any, id: string) {
    return this._http_client
      .post(`${Constants.urls.API_REBOOK}/${id}`, JSON.stringify({datetime}));
  }

  public share_invoice(id: string | number, emails: string[]) {
    return this._http_client
      .post(`${Constants.urls.API_SHARE_INVOICE}/${id}?output=email`, emails, BlowhornService.http_headers);
  }

  public remove_trip(id: string | number, reason: string) {
    return this._http_client
      .post(`${Constants.urls.API_ALL_TRIPS}/${id}`, {'reason': reason});
  }

  public get_firebase_token() {
    return this._http_client
      .get(`${Constants.urls.API_FIREBASE_TOKEN}/tracking`);
  }

  public update_order(id: string, data: any) {
    return this._http_client
      .post(`${Constants.urls.API_DETAIL_TRIP}/${id}/`, data);
  }

  public reschedule_booking(data: any) {
    return this._http_client.post(Constants.urls.API_BOOKING_RESCHEDULE, data);
  }

  public get_timeslot_details(latitude: number, longitude: number) {
    const url = Constants.urls.API_BOOKING_TIMESLOTS;
    return this._http_client
      .get<any>(`${url}?from-lat=${latitude}&from-lon=${longitude}`);
  }

  public makeCall(data: any) {
    return this._http_client.post(Constants.urls.API_MASK_CALL, data);
  }

}
