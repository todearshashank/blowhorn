import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class DataService {

  constructor(private http: HttpClient) {}

  getFileData (): Observable<Response> {
    return this.http.get<any>('assets/data/data.json');
  }
}
