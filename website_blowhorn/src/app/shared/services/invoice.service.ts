import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Invoice } from '../models/invoice.model';

@Injectable()
export class InvoiceService {

  constructor(
    private _http_client: HttpClient,
  ) { }

  get_order_invoice(id: string, params = {}): Observable<Invoice> {
    return this._http_client
      .get<Invoice>(`/api/order/${id}/invoice`, {params: params});
  }

}
