import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceAllocationFilter, Contract } from '../models/resource-allocation.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { Constants } from '../utils/constants';
import { IdName } from '../models/id-name.model';
import { BlowhornService } from './blowhorn.service';
import { AllocationHistory } from '../models/resource-allocation.model';

@Injectable({
    providedIn: 'root'
})
export class ResourcePlanningService {

    private _city_store = new BehaviorSubject<IdName[]>([]);
    private _hub_store = new BehaviorSubject<IdName[]>([]);
    private _vehicle_class_store = new BehaviorSubject<any[]>([]);

    city_store = this._city_store.asObservable();
    hub_store = this._hub_store.asObservable();
    vehicle_class_store = this._vehicle_class_store.asObservable();

    constructor(
        private http_client: HttpClient,
    ) { }

    update_city_store(val: IdName[]) {
        this._city_store.next(val);
    }

    update_hub_store(val: IdName[]) {
        this._hub_store.next(val);
    }

    update_vehicle_class_store(val: any[]) {
        this._vehicle_class_store.next(val);
    }

    get_support_data(): Observable<ResourceAllocationFilter> {
        return this.http_client.get<ResourceAllocationFilter>(Constants.urls.API_RESOURCE_PLANNING_SUPPORT_DATA);
    }

    get_slots(url: string): Observable<any> {
        return this.http_client.get<any>(url);
    }

    get_driver_details(pk: number): Observable<any> {
        return this.http_client.get<any>(`${Constants.urls.API_RESOURCE_PLANNING}/${pk}/drivers`);
    }
}
