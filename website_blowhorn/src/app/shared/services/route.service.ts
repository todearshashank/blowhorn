import { Injectable, QueryList } from '@angular/core';
import { Route } from '../models/route.model';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils/constants';
import { BehaviorSubject } from 'rxjs';
import { BlowhornService } from './blowhorn.service';
import { Observable } from 'rxjs';
import { Contact } from '../models/contact.model';
import { GeoPoint } from '../models/geopoint.model';
import { RouteStop } from '../models/route-stop.model';
import { MapService } from './map.service';
import { Address } from '../models/address.model';
import { BlowhornMapSearchComponent } from '../components/blowhorn-map-search/blowhorn-map-search.component';
import { MarkerLabel } from '@agm/core/services/google-maps-types';
import * as $ from 'jquery';

@Injectable()
export class RouteService {

  private  _http_header = {
    headers: {
      'X-CSRFToken': this._blowhorn_service.getCookieValue('csrftoken')
    }
  };

  private _routes: BehaviorSubject<Route[]>;
  routes: Observable<Route[]>;
  private _new_route: BehaviorSubject<Route>;
  new_route: Observable<Route>;
  private _selected_route: BehaviorSubject<Route>;
  selected_route: Observable<Route>;
  private _all_routes: Route[];
  private _route: Route;
  private _route_selected: Route;
  private _save_route_mode: string;
  private _route_list_save_address: BehaviorSubject<boolean>;
  route_list_save_address: Observable<boolean>;
  private _route_list_save_contact: BehaviorSubject<boolean>;
  route_list_save_contact: Observable<boolean>;

  constructor(
    private _http_client: HttpClient,
    public _blowhorn_service: BlowhornService,
    private _map_service: MapService,
  ) {
    this._routes = new BehaviorSubject<Route[]>([]);
    this._new_route = new BehaviorSubject<Route>(new Route());
    this._selected_route = new BehaviorSubject<Route>(null);
    this._route_list_save_address = new BehaviorSubject<boolean>(false);
    this._route_list_save_contact = new BehaviorSubject<boolean>(false);
    this.routes = this._routes.asObservable();
    this.new_route = this._new_route.asObservable();
    this.selected_route = this._selected_route.asObservable();
    this.route_list_save_address = this._route_list_save_address.asObservable();
    this.route_list_save_contact = this._route_list_save_contact.asObservable();
  }

  update_routes(routes: Route[]) {
    if (!this._route_selected) {
      this._route_selected = routes[0];
      this.update_selected_route(this._route_selected);
    } else if (routes.find(item => item.id === this._route_selected.id)) {
      this._route_selected = routes.find(item => item.id === this._route_selected.id);
      this.update_selected_route(this._route_selected);
    }
    this._all_routes = routes;
    this._routes.next(routes);
  }

  update_new_route(route: Route) {
    this._route = route;
    this.rearrannge_seq_no(route);
    this._new_route.next(route);
  }

  update_selected_route(route: Route) {
    if (!route && this._all_routes) {
      route = this._all_routes[0];
    }
    this._route_selected = route;
    this._selected_route.next(this._route_selected);
  }

  update_route_list_save_address(val: boolean) {
    this._route_list_save_address.next(val);
  }

  update_route_list_save_contact(val: boolean) {
    this._route_list_save_contact.next(val);
  }

  get_routes(): Observable<any> {
    return this._http_client
      .get<any>(Constants.urls.API_CUSTOMER_ROUTE);
  }

  save_route(data: {}): Observable<any> {
    return this._http_client
      .post<any>(
        Constants.urls.API_CUSTOMER_ROUTE,
        data,
        this._http_header
      );
  }

  edit_route(data: {}): Observable<any> {
    return this._http_client
      .put<any>(
        Constants.urls.API_CUSTOMER_ROUTE,
        data,
        this._http_header
      );
  }

  delete_route(id: number): Observable<any> {
    const headers = Object.assign({}, this._http_header);
    headers['params'] = {
      id: id
    };
    return this._http_client
      .delete<any>(
        Constants.urls.API_CUSTOMER_ROUTE,
        headers
      );
  }

  get_route_data_for_api(route: Route): {} {
    const route_data = {
      id: route.id,
      name: route.name,
      contact_name: route.contact.name,
      contact_mobile: route.contact.mobile,
      city: route.city,
      distance: route.distance,
      stops: []
    };

    route.stops.forEach(stop => {
      route_data.stops.push({
        id: stop.id,
        seq_no: stop.seq_no,
        address_name: stop.address.name,
        line1: stop.address.full_address,
        line2: stop.address.street_address,
        line3: stop.address.area,
        line4: stop.address.city,
        postcode: stop.address.postal_code,
        state: stop.address.state,
        country: this._blowhorn_service.country_code,
        contact_name: stop.contact && stop.contact.name,
        contact_mobile: stop.contact && stop.contact.mobile,
        lat: stop.geopoint.lat(),
        lng: stop.geopoint.lng()
      });
    });
    return route_data;
  }

  get_routes_data_from_api(result) {
    const routes = [];
    result.forEach(item => {
      const route = new Route();
      route.id = item.id;
      route.city = item.city;
      route.distance = item.distance;
      route.name = item.name;
      route.contact = new Contact(this.get_mobile_number(item.contact_mobile), item.contact_name);
      route.stops = [];
      item.stops.forEach(element => {
        const stop = new RouteStop();
        stop.id = element.id;
        stop.seq_no = element.seq_no;
        stop.contact = new Contact(this.get_mobile_number(element.contact_mobile), element.contact_name);
        stop.geopoint = MapService.map_position_to_geopoint
          ([element.geopoint.coordinates[1], element.geopoint.coordinates[0]]);
        stop.address = new Address(
          null,
          element.address_name,
          element.line1,
          element.line2,
          element.line3,
          element.line4,
          element.state,
          element.country,
          element.postcode
        );
        if (element.address_name) {
          stop.saved_location = this._blowhorn_service
                                    .user_profile
                                    .saved_locations
                                    .find(loc => loc.name === element.address_name);
        }
        route.stops.push(stop);
      });
      routes.push(route);
    });
    this.update_routes(routes);
  }

  get_mobile_number(number: string) {
    return number && number.startsWith('+91') ? number.substring(3) : number;
  }

  initialize_new_route() {
    this._route = new Route();
    this.reset_stops();
  }

  reset_stops() {
    this._route.stops = [];
    this._route.stops.push(new RouteStop(null, 0));
    this._route.stops.push(new RouteStop(null, 1));
    this._new_route.next(this._route);
  }

  set_selected_route(route: Route) {
    this._selected_route.next(route);
  }

  rearrannge_seq_no(route) {
    route.stops.forEach((stop, index) => stop.seq_no = index);
  }

  get_icon_url(index: number): string {
    if (index) {
      return index < this._route.stops.length - 1 ?
        Constants.markers.stop_big :
        Constants.markers.dropoff;
    } else {
      return Constants.markers.pickup;
    }
  }

  get_stop_placeholder(index: number): string {
    if (index) {
      return index < this._route.stops.length - 1 ?
        'Choose a stop point' :
        'Choose an end point';
    } else {
      return 'Choose a start point';
    }
  }

  get_stop_header(index: number): string {
    return `Stop ${index + 1}`;
  }

  get_stop_type(index: number, length: number): string {
    if (index === 0) {
      return 'pickup';
    } else if (index === length - 1) {
      return 'dropoff';
    }
    return 'stop';
  }

  get_location_form_header() {
    return `Save Location`;
  }

  get_contact_form_header(index) {
    return `Edit contact for Stop ${index + 1}`;
  }

  edit_selected_route() {
    this._route = this._route_selected;
    this._new_route.next(this._route_selected);
  }

  get save_route_mode(): string {
    return this._save_route_mode;
  }

  set save_route_mode(mode: string) {
    this._save_route_mode = mode;
  }

  get_meter_to_km(meter: number): string {
    return (meter / 1000).toFixed(1);
  }
}
