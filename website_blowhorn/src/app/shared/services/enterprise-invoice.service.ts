import { Injectable, NgModule } from '@angular/core';
import { HttpClientModule, HttpClientXsrfModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { BlowhornService } from './blowhorn.service';
import { Observable } from 'rxjs';
import { EnterpriseInvoice } from '../models/enterprise-invoice.model';
import { Constants } from '../utils/constants';


@NgModule({
    imports: [HttpClientModule, HttpClientXsrfModule],
})

@Injectable({
    providedIn: 'root'
})

export class EnterpriseInvoiceService {

    private _http_options: any;

    constructor(
        private http_client: HttpClient,
        public bh_service: BlowhornService,
    ) {
        const token = this.bh_service.getCookieValue('csrftoken');
        this._http_options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'X-CSRFToken': token
            })
        };
    }

    //  API Calls
    get_support_data(): Observable<any> {
        return this.http_client.get<any>(Constants.urls.API_INVOICE_SUPPORT_DATA);
    }
    get_invoices(url: string): Observable<any> {
        return this.http_client.get<EnterpriseInvoice[]>(`${url}&ordering=-service_period_end`);
    }

    update_invoice(pk: number, data: any): Observable<any> {
        return this.http_client.put<any>(`${Constants.urls.API_INVOICE_ACTION}/${pk}`, data, this._http_options);
    }

    // Common functions
    download_documents(event: string, invoice_number: string): void {
        switch (event) {
            case 'invoice':
                window.open(`${Constants.urls.API_INVOICE_DOWNLOAD}/${invoice_number}/invoice`, '_blank');
                break;

            case 'mis':
                window.open(`${Constants.urls.API_INVOICE_DOWNLOAD}/${invoice_number}/mis`, '_blank');
                break;

            default:
                console.error('Invalid Action..!');
        }
    }
}
