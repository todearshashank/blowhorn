import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Constants } from '../utils/constants';
import { BlowhornService } from './blowhorn.service';


@Injectable({
    providedIn: 'root'
})
export class IntegrationService {

    constructor(
        private http_client: HttpClient
    ) { }

    private _current_partner = new BehaviorSubject<any>('');
    current_partner = this._current_partner.asObservable();
    
    update_current_partner(val: any) {
        this._current_partner.next(val);
    }

    submit_request(data: any): Observable<any> {
        return this.http_client.post<any>(Constants.urls.API_CHANNEL_SUBSCRIPTION,
            data, BlowhornService.http_headers);
    }

    get_api_key(): Observable<any> {
        return this.http_client.get<any>(Constants.urls.API_CHANNEL_KEY);
    }

    sync_customer_orders(): Observable<any> {
        return this.http_client.get<any>(Constants.urls.API_CHANNEL_SYNCORDERS);
    }
}