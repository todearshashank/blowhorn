import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { MemberService } from './../services/member.service';
import { BlowhornService } from '../services/blowhorn.service';


@Injectable({
    providedIn: 'root',
})
export class GroupResolver implements Resolve<any> {

  constructor(
      private member_service: MemberService,
      public blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>  {
    this.blowhorn_service.show_loader();
    return this.member_service.get_groups();
  }
}
