import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TripService } from '../services/trip.service';
import { Trip } from '../models/trip.model';


@Injectable()
export class TripDetailResolver implements Resolve<Trip> {

  constructor(private trip_service: TripService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Trip>  {
    const id = route.params['id'];
    return this.trip_service
      .get_trip_details(id);
  }
}
