import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CustomerService } from '../services/customer.service';
import { Injectable } from '@angular/core';
import { IdName } from '../models/id-name.model';

@Injectable()
export class AllCustomersResolver implements Resolve<IdName[]> {

    constructor(private _customer_service: CustomerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IdName[]> {
        return this._customer_service
            .get_all_customers();
    }

}
