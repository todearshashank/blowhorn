import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TripService } from '../services/trip.service';
import { TripsGrid } from '../models/trips-grid.model';
import { Constants } from '../utils/constants';
import { WatcherService } from '../services/watcher.service';
import { BlowhornService } from '../services/blowhorn.service';

@Injectable()
export class CompletedTripsResolver implements Resolve<TripsGrid> {

    constructor(
        private trip_service: TripService,
        private watcher_service: WatcherService,
        public _blowhorn_service: BlowhornService,
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TripsGrid> {
        this._blowhorn_service.show_loader();
        return this.trip_service
            .get_completed_trips(Constants.urls.API_COMPLETED_TRIPS);
    }
}
