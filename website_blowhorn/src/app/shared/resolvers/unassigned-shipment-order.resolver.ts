import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ShipmentService } from '../services/shipment.service';
import { ApolloQueryResult } from 'apollo-client';
import { Un_Assigned_Orders } from '../utils/types';
import { WatcherService } from '../services/watcher.service';
import { BlowhornService } from '../services/blowhorn.service';


@Injectable()
export class UnassignedShipmentOrderResolver implements Resolve <ApolloQueryResult<Un_Assigned_Orders>> {

  constructor(
    private _shipment_service: ShipmentService,
    private _watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ApolloQueryResult<Un_Assigned_Orders>> {
    this._blowhorn_service.show_loader();
    return this._shipment_service
      .get_unassigned_shipment_orders();
  }
}
