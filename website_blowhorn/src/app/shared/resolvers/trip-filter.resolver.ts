import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TripFilter } from '../models/trip-filter.model';
import { WatcherService } from '../services/watcher.service';
import { TripService } from '../services/trip.service';
import { BlowhornService } from '../services/blowhorn.service';


@Injectable()
export class TripFilterResolver implements Resolve<TripFilter> {

  constructor(
    private trip_service: TripService,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TripFilter> {
    this._blowhorn_service.show_loader();
    return this.trip_service
      .get_trip_filter();
  }
}
