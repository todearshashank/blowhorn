import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { BlowhornService } from '../services/blowhorn.service';
import { FlashSaleService } from '../services/flash-sale.service';


@Injectable()
export class FlashSaleResolver implements Resolve<any> {

  constructor(
    public _blowhorn_service: BlowhornService,
    private _flash_sale_service: FlashSaleService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this._blowhorn_service.show_loader();
    return this._flash_sale_service.get_support_data();
  }
}

