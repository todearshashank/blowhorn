import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { BlowhornService } from '../services/blowhorn.service';
import { ShipmentService } from '../services/shipment.service';

@Injectable()
export class Reports2Resolver implements Resolve<any> {

    constructor(
      public _blowhorn_service: BlowhornService,
      private _shipment_service: ShipmentService

    ) {}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>  {
      return this._shipment_service.get_shipment_filter('analytics')
    }
}