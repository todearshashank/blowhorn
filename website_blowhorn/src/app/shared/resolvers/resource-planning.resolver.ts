import { ResourcePlanningService } from './../services/resource-planning.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { BlowhornService } from '../services/blowhorn.service';
import { ResourcePlanningFilter } from '../models/resource-planning.model';


@Injectable()
export class ResourcePlanningResolver implements Resolve<ResourcePlanningFilter> {

    constructor(
        public _blowhorn_service: BlowhornService,
        private rpService: ResourcePlanningService,
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        return this.rpService.get_support_data();
    }
}
