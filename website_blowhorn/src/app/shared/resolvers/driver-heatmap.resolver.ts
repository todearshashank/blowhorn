import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HeatmapService } from '../services/heatmap.service';
import { WatcherService } from '../services/watcher.service';
import { ApolloQueryResult } from 'apollo-client';
import { BlowhornService } from '../services/blowhorn.service';

@Injectable()
export class DriverHeatmapResolver implements Resolve<any> {

  constructor(
    private _heatmap_service: HeatmapService,
    private _watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ApolloQueryResult<any>> {
    this._blowhorn_service.show_loader();
    return this._heatmap_service
       .get_drivers('');
  }
}
