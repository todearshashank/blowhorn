import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EnterpriseInvoiceService } from '../services/enterprise-invoice.service';
import { EnterpriseInvoice } from '../models/enterprise-invoice.model';
import { BlowhornService } from '../services/blowhorn.service';
import { Constants } from '../utils/constants';


@Injectable()
export class EnterpriseInvoiceResolver implements Resolve<EnterpriseInvoice[]> {

    constructor(
        private invoice_service: EnterpriseInvoiceService,
        public _blowhorn_service: BlowhornService,
        ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<EnterpriseInvoice[]> {
        this._blowhorn_service.show_loader();
        return this.invoice_service.get_invoices(`${Constants.urls.API_INVOICES}&status=${route.url[0].path}`);
    }
}
