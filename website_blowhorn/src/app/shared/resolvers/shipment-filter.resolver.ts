import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ShipmentFilter } from '../models/shipment-filter.model';
import { WatcherService } from '../services/watcher.service';
import { ShipmentService } from '../services/shipment.service';
import { BlowhornService } from '../services/blowhorn.service';


@Injectable()
export class ShipmentFilterResolver implements Resolve<ShipmentFilter> {

  constructor(
    private shipment_service: ShipmentService,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShipmentFilter> {
    this._blowhorn_service.show_loader();
    return this.shipment_service.get_shipment_filter('shipment_orders');
  }
}
