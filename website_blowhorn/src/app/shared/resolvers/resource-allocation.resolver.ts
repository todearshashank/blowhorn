import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { WatcherService } from '../services/watcher.service';
import { BlowhornService } from '../services/blowhorn.service';
import { ResourceAllocationService } from '../services/resource-allocation.service';
import { Constants } from '../utils/constants';


@Injectable()
export class ResourceAllocationResolver implements Resolve<any> {

    constructor(
        private watcher_service: WatcherService,
        public _blowhorn_service: BlowhornService,
        private resource_allocation_service: ResourceAllocationService,
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        this._blowhorn_service.show_loader();
        return this.resource_allocation_service.get_contracts(Constants.urls.API_RESOURCE_CONTRACTS);
    }
}
