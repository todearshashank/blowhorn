import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { BlowhornService } from '../services/blowhorn.service';
import { BankAccount } from '../models/bank.model';
import { BankAccountsService } from '../services/bank-accounts.service';


@Injectable()
export class BankAccountsResolver implements Resolve<BankAccount[]> {

  constructor(
    private bankAccountsService: BankAccountsService,
    public blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<BankAccount[]>  {
    this.blowhorn_service.show_loader();
    return this.bankAccountsService.getBankAccounts()
  }
}
