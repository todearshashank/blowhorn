import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TripService } from '../services/trip.service';
import { Constants } from '../utils/constants';
import { WatcherService } from '../services/watcher.service';
import { MyTripsGrid } from '../models/my-trips-grid.model';
import { BlowhornService } from '../services/blowhorn.service';

@Injectable()
export class CurrentMyTripsResolver implements Resolve<MyTripsGrid> {

    constructor(
        private trip_service: TripService,
        private watcher_service: WatcherService,
        public _blowhorn_service: BlowhornService,
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MyTripsGrid> {
        this._blowhorn_service.show_loader();
        return this.trip_service
            .get_current_mytrips(Constants.urls.API_CURRENT_MY_TRIPS);
    }
}
