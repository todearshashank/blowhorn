import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TripsService } from '../services/trips.service';
import { Trip } from '../models/trip.model';


@Injectable()
export class DetailLocationResolver implements Resolve<Trip> {

  constructor(private _trips_service: TripsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any  {
    return this._trips_service.get_locations();
  }
}
