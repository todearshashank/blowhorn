import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LivetrackingService } from '../services/livetracking.service';
import { WatcherService } from '../services/watcher.service';
import { LivetrackingFilter } from '../models/livetracking-filter.model';
import { BlowhornService } from '../services/blowhorn.service';

@Injectable()
export class LivetrackingResolver implements Resolve<LivetrackingFilter> {

  constructor(
    private livetracking_service: LivetrackingService,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<LivetrackingFilter>  {
    this._blowhorn_service.show_loader();
    return this.livetracking_service.get_support_data();
  }
}
