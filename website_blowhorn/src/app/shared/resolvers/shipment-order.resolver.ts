import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ShipmentService } from '../services/shipment.service';
import { ShipmentsGrid } from '../models/shipments-grid.model';


@Injectable()
export class ShipmentOrderResolver implements Resolve<ShipmentsGrid> {

  constructor(private shipment_service: ShipmentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShipmentsGrid>  {
    return this.shipment_service
      .get_shipment_orders();
  }
}
