import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { MemberService } from './../services/member.service';
import { MemberGrid } from './../models/member.model';
import { BlowhornService } from '../services/blowhorn.service';


@Injectable()
export class MemberResolver implements Resolve<MemberGrid> {

  constructor(
      private member_service: MemberService,
      public blowhorn_service: BlowhornService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MemberGrid>  {
    this.blowhorn_service.show_loader();
    return this.member_service.get_members();
  }
}
