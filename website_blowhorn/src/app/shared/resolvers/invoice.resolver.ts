import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Invoice } from '../models/invoice.model';
import { InvoiceService } from '../services/invoice.service';


@Injectable()
export class InvoiceResolver implements Resolve<Invoice> {

  constructor(private invoice_service: InvoiceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Invoice>  {
    const id = route.params['id'];
    return this.invoice_service
      .get_order_invoice(id);
  }
}
