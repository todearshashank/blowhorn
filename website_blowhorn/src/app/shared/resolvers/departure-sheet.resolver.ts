import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { DepartureSheet } from '../models/departure-sheet.model';
import { TripService } from '../services/trip.service';


@Injectable()
export class DepartureSheetResolver implements Resolve<DepartureSheet> {

  constructor(private trip_service: TripService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<DepartureSheet> {
    const id = route.params['id'];
    return this.trip_service
      .get_departure_sheet(id);
  }
}
