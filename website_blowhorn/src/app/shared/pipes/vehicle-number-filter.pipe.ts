import { Pipe, PipeTransform } from '@angular/core';
import { CustomerResourceAllocation } from '../models/customer-resource-allocation.model';

@Pipe({
    name: 'vehicleNumberFilter'
})

export class VehicleNumberFilterPipe implements PipeTransform {
    transform(items: CustomerResourceAllocation[], search_term: any): CustomerResourceAllocation[] {
        if (search_term.trim()) {
            const filtered_items = items.filter(item => {
                return item.vehicle_number.toLowerCase().includes(search_term.trim().toLowerCase());
            });
            return filtered_items;
        }
        return items;
    }
}
