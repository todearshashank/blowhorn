import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cityFilter'
})
export class CityFilterPipe implements PipeTransform {

  transform(items: any, searchTerm: any, key: any): any {
    if (searchTerm.trim()) {
      const filteredItems = items.filter((item: any) => {
        let city = item.city || item.properties.city;
        return city.toLowerCase().includes(searchTerm.toLowerCase());
      });
      return  filteredItems;
    }
    return items;
  }

}
