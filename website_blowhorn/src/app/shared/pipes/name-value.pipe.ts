import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameValue',
  // pure: false
})
export class NameValuePipe implements PipeTransform {
  transform(options: any): any {
    let keys = [];
    for (let key in options) {
      keys.push({ name: key, id: options[key]});
    }
    return keys;
  }
}
