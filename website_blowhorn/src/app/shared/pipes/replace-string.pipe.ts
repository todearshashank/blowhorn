import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceString'
})
export class ReplaceStringPipe implements PipeTransform {

  transform(input: any, to_replace?: any, replace_with?: any): any {
    if (!to_replace) {
        to_replace = /\-/g;
    }
    if (!replace_with) {
        replace_with = ' ';
    }
    return input.replace(to_replace, replace_with);
  }

}
