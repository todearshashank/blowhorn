import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'currencySymbol' })
export class CurrencySymbolPipe implements PipeTransform {

  transform(value: any) {
    const getNavigatorLanguage = () => navigator.language || (navigator.languages && navigator.languages.length) ? navigator.languages[0] : navigator.language || 'en-IN';
    function getCurrencySymbol (locale, currency) {
        locale = locale || 'en-IN';
        currency = currency || 'INR';
        return (0).toLocaleString(
            locale,
            {
                style: 'currency',
                currency: currency,
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            }
        ).replace(/\d/g, '').trim()
    }
    if (value === 'ZAR') {
        // locale-string not converting to symbol for all currency
        return 'R';
    }
    return getCurrencySymbol(getNavigatorLanguage(), value);
  }
}
