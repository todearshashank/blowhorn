import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keys'
})
export class KeysPipe implements PipeTransform {
  transform(options: any, args:string): any {
    let keys = [];
    for (let key in options) {
      keys.push({key: key, value: options[key]});
    }
    return keys;
  }
}
