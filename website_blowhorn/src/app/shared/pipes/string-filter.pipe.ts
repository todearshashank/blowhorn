import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringFilter'
})
export class StringFilterPipe implements PipeTransform {

  transform(items: any, key: string, term: any): any {
    if (term.trim()) {
      const filteredItems = items.filter((item: any) => {
        return item[key].toLowerCase().includes(term.toLowerCase());
      });
      return filteredItems;
    }
    return items;
  }

}
