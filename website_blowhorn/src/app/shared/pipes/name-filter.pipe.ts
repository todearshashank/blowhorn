import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameFilter'
})
export class NameFilterPipe implements PipeTransform {

  transform(items: any, search_term: any): any {
    if (search_term) {
      const filtered_items = items.filter((item) => {
        return item.name.toLowerCase().includes(search_term.toLowerCase());
      });
      return  filtered_items;
    }
    return items;
  }

}
