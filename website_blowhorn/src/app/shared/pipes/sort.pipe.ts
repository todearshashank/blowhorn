import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'sortBy'
})
export class SortPipe implements PipeTransform {

    transform(source_arr: any[], path: string[], order: number = 1): any[] {
        if (!source_arr || !path || !order) {
            console.log('returning', source_arr, path, order);
            return source_arr;
        }

        return source_arr.sort((a: any, b: any) => {
            path.forEach(property => {
                a = a[property];
                b = b[property];
            });

            // Order * (-1)
            return a > b ? order : order * (- 1);
        });
    }

}
