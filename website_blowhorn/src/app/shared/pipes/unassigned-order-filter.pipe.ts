import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unassignedOrderFilter'
})
export class UnassignedOrderFilterPipe implements PipeTransform {

  transform(items: any, search_term: any): any {
    const filtered_items = items.filter((item) => {
      return !item.is_added && !item.is_ignore;
    });
    return  filtered_items;
  }

}
