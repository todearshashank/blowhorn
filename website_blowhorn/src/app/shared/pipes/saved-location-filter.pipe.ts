import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'savedLocationFilter'
})
export class SavedLocationFilterPipe implements PipeTransform {

  transform(items: any, search_term: any): any {
    if (search_term) {
      const filtered_items = items.filter((item) => {
        return item.address.city.toLowerCase().includes(search_term.toLowerCase());
      });
      return  filtered_items;
    }
    return items;
  }

}
