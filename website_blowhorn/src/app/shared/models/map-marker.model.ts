import { Driver } from './driver.model';

export class MapMarker {

    constructor(
        private _driver: Driver = new Driver(),
        private _visible: boolean = false,
        private _icon_url: string = '',
        private _next_stop: string = '',
        private _infowindow: any = ''
    ) {}

    get driver(): Driver {
        return this._driver;
    }

    set driver(driver: Driver) {
        this._driver = driver;
    }

    get visible(): boolean {
        return this._visible;
    }

    set visible(visible: boolean) {
        this._visible = visible;
    }

    get icon_url(): string {
        return this._icon_url;
    }

    set icon_url(icon_url: string) {
        this._icon_url = icon_url;
    }

    get next_stop(): string {
        return this._next_stop;
    }

    set next_stop(next_stop: string) {
        this._next_stop = next_stop;
    }

    get infowindow(): any {
        return this._infowindow;
    }

    set infowindow(infowindow: any) {
        this._infowindow = infowindow;
    }
}
