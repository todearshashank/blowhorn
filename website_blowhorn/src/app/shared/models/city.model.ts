import { IdName } from './id-name.model';

export class City {

    constructor(
        private _id: number = null,
        private _name: string = '',
        private _hubs: IdName[] = [],
        private _geopoint: [number, number] = [null, null]
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get hubs(): IdName[] {
        return this._hubs;
    }

    set hubs(hubs: IdName[]) {
        this._hubs = hubs;
    }

    get geopoint(): [number, number] {
        return this._geopoint;
    }

    set geopoint(geopoint: [number, number]) {
        this._geopoint = geopoint;
    }
}
