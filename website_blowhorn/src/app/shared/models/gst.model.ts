export class GST {
    constructor(
        private _amount: number = 0,
        private _percentage: number = 0
    ) {}

    public get amount(): number {
        return this._amount;
    }

    public set amount(amount: number) {
        this._amount = amount;
    }

    public get percentage(): number {
        return this._percentage;
    }

    public set percentage(percentage: number) {
        this._percentage = percentage;
    }
}
