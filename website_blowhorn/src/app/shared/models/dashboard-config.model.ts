
export class LocationMarkerConfig {
    constructor(
        private _show: boolean = true,
        private _icon: string = '',
    ) {}

    public get show() {
        return this._show;
    }

    public set show(val: boolean) {
        this._show = val;
    }

    public get icon() {
        return this._icon;
    }

    public set icon(val: string) {
        this._icon = val;
    }
}

export class MarkersConfig {
    constructor(
        private _pickup: LocationMarkerConfig = new LocationMarkerConfig(),
        private _dropoff: LocationMarkerConfig = new LocationMarkerConfig(),
        private _stop: LocationMarkerConfig = new LocationMarkerConfig(),
    ) {}

    public get icon() {
        return this._pickup;
    }

    public set icon(val: LocationMarkerConfig) {
        this._pickup = val;
    }

    public get dropoff() {
        return this._dropoff;
    }

    public set dropoff(val: LocationMarkerConfig) {
        this._dropoff = val;
    }

    public get stop() {
        return this._stop;
    }

    public set stop(val: LocationMarkerConfig) {
        this._stop = val;
    }
}

export class DashboardConfig {

    constructor(
      private _markers: MarkersConfig[] = [],
      private _showAdditionalInfo: boolean = true,
      private _showOnlyMap: boolean = false,
    ) {}

    public get markers() {
      return this._markers;
    }

    public set markers(val: MarkersConfig[]) {
      this._markers = val;
    }

    public get showAdditionalInfo() {
        return this._showAdditionalInfo;
    }

    public set showAdditionalInfo(val: boolean) {
        this._showAdditionalInfo = val;
    }

    public get showOnlyMap() {
        return this._showOnlyMap;
    }

    public set showOnlyMap(val: boolean) {
        this._showOnlyMap = val;
    }
}
