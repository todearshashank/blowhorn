export class Customer {
    constructor(
        private _gstin: string = '',
        private _is_business_user: boolean = false,
        private _legal_name: string = '',
        private _name: string = '',
        private _mobile: number = null
    ) {}

    get gstin(): string {
        return this._gstin;
    }

    set gstin(gstin: string) {
        this._gstin = gstin;
    }

    get is_business_user(): boolean {
        return this._is_business_user;
    }

    set is_business_user(is_business_user: boolean) {
        this._is_business_user = is_business_user;
    }

    get legal_name(): string {
        return this._legal_name;
    }

    set legal_name(legal_name: string) {
        this._legal_name = legal_name;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get mobile(): number {
        return this._mobile;
    }

    set mobile(mobile: number) {
        this._mobile = mobile;
    }
}
