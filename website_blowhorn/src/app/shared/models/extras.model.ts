export class Extras {

  static get_extras_from_data(data): Extras {
    const extras = new Extras();
    Object.keys(data).forEach(key => {
        stop[key] = data[key];
    });
    return extras;
  }

  constructor(
    private _sign_file_url: string = '',
    private _pod_file_url: string = '',
    private _order_status: string = '',
    private _cod: string = '',
    private _cash_collected: string = '',
    private _time: string = '',
  ) {}

  public get sign_file_url() {
    return this._sign_file_url;
  }

  public set sign_file_url(sign_file_url: string) {
    this._sign_file_url = sign_file_url;
  }

  public get pod_file_url() {
    return this._pod_file_url;
  }

  public set pod_file_url(pod_file_url: string) {
    this._pod_file_url = pod_file_url;
  }

  public get order_status() {
    return this._order_status;
  }

  public set order_status(order_status: string) {
    this._order_status = order_status;
  }

  public get cod() {
    return this._cod;
  }

  public set cod(cod: string) {
    this._cod = cod;
  }

  public get cash_collected() {
    return this._cash_collected;
  }

  public set cash_collected(cash_collected: string) {
    this._cash_collected = cash_collected;
  }

  public get time() {
    return this._time;
  }

  public set time(time: string) {
    this._time = time;
  }
}
