export class BatteryInfo {
    constructor(
        private _LastChargingPluggedDateTime: string = '',
        private _LastChargingUnPluggedDateTime: string = '',
        private _batteryLevel: number = 0,
        private _chargingMode: string = '',
        private _chargingStatus: string = '',
        private _icon: string = '',
        private _style_class: string = '',
    ) {}

    public get LastChargingPluggedDateTime(): string {
        return this._LastChargingPluggedDateTime;
    }

    public set LastChargingPluggedDateTime(LastChargingPluggedDateTime: string) {
        this._LastChargingPluggedDateTime = LastChargingPluggedDateTime;
    }

    public get LastChargingUnPluggedDateTime(): string {
        return this._LastChargingUnPluggedDateTime;
    }

    public set LastChargingUnPluggedDateTime(LastChargingUnPluggedDateTime: string) {
        this._LastChargingUnPluggedDateTime = LastChargingUnPluggedDateTime;
    }

    public get batteryLevel(): number {
        return this._batteryLevel;
    }

    public set batteryLevel(batteryLevel: number) {
        this._batteryLevel = batteryLevel;
    }

    public get chargingMode(): string {
        return this._chargingMode;
    }

    public set chargingMode(chargingMode: string) {
        this._chargingMode = chargingMode;
    }

    public get chargingStatus(): string {
        return this._chargingStatus;
    }

    public set chargingStatus(chargingStatus: string) {
        this._chargingStatus = chargingStatus;
    }

    public get icon(): string {
        return this._icon;
    }

    public set icon(icon: string) {
        this._icon = icon;
    }

    public get style_class(): string {
        return this._style_class;
    }

    public set style_class(style_class: string) {
        this._style_class = style_class;
    }
}


export class DeviceDetails {

    constructor(
        private _app_version_name: string = '',
        private _device_name: string = '',
        private _imei_number: string = '',
        private _os_version: string = '',
        private _sim_network: string = '',
        private _sim_number: string = '',
        private _sim_sr_number: string = '',
        private _battery_info: BatteryInfo = new BatteryInfo(),
    ) {}

    public get app_version_name(): string {
        return this._app_version_name;
    }

    public set app_version_name(app_version_name: string) {
        this._app_version_name = app_version_name;
    }

    public get device_name(): string {
        return this._device_name;
    }

    public set device_name(device_name: string) {
        this._device_name = device_name;
    }

    public get imei_number(): string {
        return this._imei_number;
    }

    public set imei_number(imei_number: string) {
        this._imei_number = imei_number;
    }

    public get sim_network(): string {
        return this._sim_network;
    }

    public set sim_network(sim_network: string) {
        this._sim_network = sim_network;
    }

    public get sim_number(): string {
        return this._sim_number;
    }

    public set sim_number(sim_number: string) {
        this._sim_number = sim_number;
    }

    public get sim_sr_number(): string {
        return this._sim_sr_number;
    }

    public set sim_sr_number(sim_sr_number: string) {
        this._sim_sr_number = sim_sr_number;
    }

    public get os_version(): string {
        return this._os_version;
    }

    public set os_verion(os_version: string) {
        this._os_version = os_version;
    }

    public get battery_info(): BatteryInfo {
        return this._battery_info;
    }

    public set battery_info(battery_info: BatteryInfo) {
        this._battery_info = battery_info;
    }
}
