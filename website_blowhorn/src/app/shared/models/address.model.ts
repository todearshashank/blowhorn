declare var country_code: string;
export class Address {

    private _fullAddress = '';
    private _postalCode: number|string = '';

    public static get_address_from_data(data): Address {
        const address = new Address();
        address.area = data.address.area;
        address.city = data.address.city;
        address.country = data.address.country;
        address.full_address = data.address.full_address;
        address.landmark = data.address.landmark;
        address.line = data.address.line;
        address.postal_code = data.address.postal_code;
        address.state = data.address.state;
        address.what3words = data.address.what3words;
        return address;
    }

    constructor(
        private _id: number = null,
        private _name: string = '',
        private _full_address: string = '',
        private _street_address: string = '',
        private _area: string = '',
        private _city: string = '',
        private _state: string = '',
        private _country: string = country_code,
        private _postal_code: string|number = '',
        private _landmark: string = '',
        private _line: string = '',
        private _what3words: string = '',
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get full_address(): string {
        return this._full_address || this._line;
    }

    set full_address(full_address: string) {
        this._full_address = full_address;
        this._fullAddress = full_address;
    }

    get fullAddress(): string {
        return this._fullAddress;
    }

    set fullAddress(full_address: string) {
        this._fullAddress = full_address;
        this._full_address = full_address;
    }

    get street_address(): string {
        return this._street_address;
    }

    set street_address(street_address: string) {
        this._street_address = street_address;
    }

    get city(): string {
        return this._city;
    }

    set city(city: string) {
        this._city = city;
    }

    get area(): string {
        return this._area;
    }

    set area(area: string) {
        this._area = area;
    }

    get state(): string {
        return this._state;
    }

    set state(state: string) {
        this._state = state;
    }

    get country(): string {
        return this._country;
    }

    set country(country: string) {
        this._country = country;
    }

    get postal_code(): number|string {
        return this._postal_code;
    }

    set postal_code(postal_code: number|string) {
        this._postal_code = postal_code;
        this._postalCode = postal_code;
    }

    get postalCode(): number|string {
        return this._postal_code;
    }

    set postalCode(postal_code: number|string) {
        this._postal_code = postal_code;
        this._postalCode = postal_code;
    }

    get landmark(): string {
        return this._landmark;
    }

    set landmark(landmark: string) {
        this._landmark = landmark;
    }

    get line(): string {
        return this._line;
    }

    set line(line: string) {
        this._line = line;
    }

    get what3words(): string {
        return this._what3words;
    }

    set what3words(what3words: string) {
        this._what3words = what3words;
    }
}
