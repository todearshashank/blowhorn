import { Base } from './base.model';
import { LocationStop } from './location.model';
import { Contact } from './contact.model';
import { Extras } from './extras.model';

export class Stop extends Base {

  static get_stop_from_data(data): Stop {
    const stop = new Stop();
    Object.keys(data).forEach(key => {
      switch (key) {
        case 'location':
          stop[key] = LocationStop.get_location_from_data(data[key]);
          break;
        case 'contact':
          stop[key] = Contact.get_contact_from_data(data[key]);
          break;
        case 'extras':
          stop[key] = Extras.get_extras_from_data(data[key]);
          break;
        default:
          stop[key] = data[key];
      }
    });
    return stop;
  }

  constructor(
    _status: string = 'done',
    _icon: string = '',
    _estimated_distance: number = 0,
    _distance: number = 0,
    _travel_time: string = '',
    _name: string = '',
    _address: string = '',
    _wait_duration: string = '',
    _location: LocationStop = new LocationStop(),
    _time: string = '',
    _date: string = '',
    _contact: Contact = new Contact(),
    _extras: Extras = new Extras()
  ) {
    super(_status, _icon, _estimated_distance, _distance, _travel_time, _name,
      _address, _wait_duration, _location, _time, _date, _contact, _extras);
  }
}
