import { Address } from './address.model';
import { Contact } from './contact.model';
import { MapService } from '../services/map.service';
import { SavedLocation } from './saved-location.model';

export class RouteStop {
    constructor(
        private _id: number = null,
        private _seq_no: number = null,
        private _geopoint: google.maps.LatLng = MapService.dummy_geopoint,
        private _position: [number, number] = [null, null],
        private _address: Address = new Address(),
        private _contact: Contact = new Contact(),
        private _saved_location: SavedLocation = new SavedLocation(),
        private _editable: boolean = true,
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get seq_no(): number {
        return this._seq_no;
    }

    set seq_no(seq_no: number) {
        this._seq_no = seq_no;
    }

    get geopoint(): google.maps.LatLng {
        return this._geopoint;
    }

    set geopoint(geopoint: google.maps.LatLng) {
        this._geopoint = geopoint;
        this.position = MapService.map_geopoint_to_position(geopoint);
    }

    get position(): [number, number] {
        return this._position;
    }

    set position(position: [number, number]) {
        this._position = position;
        this._geopoint = MapService.map_position_to_geopoint(position);
    }

    get address(): Address {
        return this._address;
    }

    set address(address: Address) {
        this._address = address;
    }

    get contact(): Contact {
        return this._contact;
    }

    set contact(contact: Contact) {
        this._contact = contact;
    }

    get saved_location(): SavedLocation {
        return this._saved_location;
    }

    set saved_location(saved_location: SavedLocation) {
        this._saved_location = saved_location;
    }

    get editable(): boolean {
        return this._editable;
    }

    set editable(editable: boolean) {
        this._editable = editable;
    }
}
