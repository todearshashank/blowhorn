export class InvoiceHistory {
    constructor(
        private _created_by: string = '',
        private _created_time: string = '',
    ) {}

    public get created_by() {
        return this._created_by;
    }

    public set created_by(val: string) {
        this._created_by = val;
    }

    public get created_time() {
        return this._created_time;
    }

    public set created_time(val: string) {
        this._created_time = val;
    }

}

export class EnterpriseInvoice {
    constructor(
        private _id: number = 0,
        private _invoice_number: string = '',
        private _invoice_state: string = '',
        private _invoice_type: string = '',
        private _invoice_date: string = '',
        private _service_period_start: string = '',
        private _service_period_end: string = '',
        private _status: string = '',
        private _charges_basic: number = 0,
        private _charges_service: number = 0,
        private _taxable_adjustment: number = 0,
        private _non_taxable_adjustment: number = 0,
        private _total_service_charge: number = 0,
        private _taxable_amount: number = 0,
        private _cgst_percentage: number = 0,
        private _sgst_percentage: number = 0,
        private _igst_percentage: number = 0,
        private _cgst_amount: number = 0,
        private _sgst_amount: number = 0,
        private _igst_amount: number = 0,
        private _total_amount: number = 0,
        private _total_rcm_amount: number = 0,
        private _total_with_tax: number = 0,
        private _total_parking_charges = 0,
        private _total_toll_charges = 0,
        private _file: string = '',
        private _invoice_history: InvoiceHistory = new InvoiceHistory(),
    ) { }


    public get id() {
        return this._id;
    }

    public set id(val: number) {
        this._id = val;
    }

    public get invoice_number() {
        return this._invoice_number;
    }

    public set invoice_number(val: string) {
        this._invoice_number = val;
    }

    public get invoice_state() {
        return this._invoice_state;
    }

    public set invoice_state(val: string) {
        this._invoice_state = val;
    }

    public get invoice_type() {
        return this._invoice_type;
    }

    public set invoice_type(val: string) {
        this._invoice_type = val;
    }

    public get invoice_date() {
        return this._invoice_date;
    }

    public set invoice_date(val: string) {
        this._invoice_date = val;
    }

    public get service_period_start() {
        return this._service_period_start;
    }

    public set service_period_start(val: string) {
        this._service_period_start = val;
    }

    public get service_period_end() {
        return this._service_period_end;
    }

    public set service_period_end(val: string) {
        this._service_period_end = val;
    }

    public get status() {
        return this._status;
    }

    public set status(val: string) {
        this._status = val;
    }

    public get total_amount() {
        return this._total_amount;
    }

    public set total_amount(val: number) {
        this._total_amount = val;
    }

    public get charges_basic() {
        return this._charges_basic;
    }

    public set charges_basic(val: number) {
        this._charges_basic = val;
    }

    public get charges_service() {
        return this._charges_service;
    }

    public set charges_service(val: number) {
        this._charges_service = val;
    }

    public get total_rcm_amount() {
        return this._total_rcm_amount;
    }

    public set total_rcm_amount(val: number) {
        this._total_rcm_amount = val;
    }

    public get taxable_adjustment() {
        return this._taxable_adjustment;
    }

    public set taxable_adjustment(val: number) {
        this._taxable_adjustment = val;
    }

    public get non_taxable_adjustment() {
        return this._non_taxable_adjustment;
    }

    public set non_taxable_adjustment(val: number) {
        this._non_taxable_adjustment = val;
    }

    public get total_service_charge() {
        return this._total_service_charge;
    }

    public set total_service_charge(val: number) {
        this._taxable_amount = val;
    }

    public get taxable_amount() {
        return this._taxable_amount;
    }

    public set taxable_amount(val: number) {
        this._taxable_amount = val;
    }

    public get cgst_percentage() {
        return this._cgst_percentage;
    }

    public set cgst_percentage(val: number) {
        this._cgst_percentage = val;
    }

    public get sgst_percentage() {
        return this._sgst_percentage;
    }

    public set sgst_percentage(val: number) {
        this._sgst_percentage = val;
    }

    public get igst_percentage() {
        return this._igst_percentage;
    }

    public set igst_percentage(val: number) {
        this._igst_percentage = val;
    }

    public get cgst_amount() {
        return this._cgst_amount;
    }

    public set cgst_amount(val: number) {
        this.cgst_amount = val;
    }

    public get sgst_amount() {
        return this._sgst_amount;
    }

    public set sgst_amount(val: number) {
        this._sgst_amount = val;
    }

    public get igst_amount() {
        return this._igst_amount;
    }

    public set igst_amount(val: number) {
        this._igst_amount = val;
    }

    public get total_toll_charges() {
        return this._total_toll_charges;
    }

    public set total_toll_charges(val: number) {
        this._total_toll_charges = val;
    }

    public get total_parking_charges() {
        return this._total_parking_charges;
    }

    public set total_parking_charges(val: number) {
        this._total_parking_charges = val;
    }

    public get total_with_tax() {
        return this._total_with_tax;
    }

    public set total_with_tax(val: number) {
        this._total_with_tax = val;
    }

    public get file() {
        return this._file;
    }

    public set file(val: string) {
        this._file = val;
    }

    public get invoice_history() {
        return this._invoice_history;
    }

    public set invoice_history(val: InvoiceHistory) {
        this._invoice_history = val;
    }
}
