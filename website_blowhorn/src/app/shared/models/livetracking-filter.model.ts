export class LivetrackingFilter {

    constructor(
        private _cities: string[] = [],
        private _vehicle_classes: string[] = [],
        private _driver_statuses: string[] = [],
        private _driver_category: string[] = [],
        private _order_statuses: string[] = [],
    ) {}

    get cities(): string[] {
        return this._cities;
    }

    set cities(cities: string[]) {
        this._cities = cities;
    }

    get driver_statuses(): string[] {
        return this._driver_statuses;
    }

    set driver_statuses(driver_statuses: string[]) {
        this._cities = driver_statuses;
    }

    get driver_category(): string[] {
        return this._driver_category;
    }

    set driver_category(driver_category: string[]) {
        this._driver_category = driver_category;
    }

    get order_statuses(): string[] {
        return this._order_statuses;
    }

    set order_statuses(order_statuses: string[]) {
        this._order_statuses = order_statuses;
    }

    get vehicle_classes(): string[] {
        return this._vehicle_classes;
    }

    set vehicle_classes(vehicle_classes: string[]) {
        this._vehicle_classes = vehicle_classes;
    }

}