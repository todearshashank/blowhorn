import { Base } from './base.model';
import { LocationStop } from './location.model';
import { Contact } from './contact.model';

export class DropOff extends Base {

  static get_dropoff_from_data(data): DropOff {
    const dropoff = new DropOff();
    Object.keys(data).forEach(key => {
      switch (key) {
        case 'location':
          dropoff[key] = LocationStop.get_location_from_data(data[key]);
          break;
        case 'contact':
          dropoff[key] = Contact.get_contact_from_data(data[key]);
          break;
        default:
          dropoff[key] = data[key];
      }
    });
    return dropoff;
  }

  constructor(
    _status: string = 'done',
    _icon: string = '',
    _distance: number = 0,
    _estimated_distance: number = 0,
    _travel_time: string = '',
    _name: string = '',
    _address: string = '',
    _wait_duration: string = '',
    _location: LocationStop = new LocationStop(),
    _time: string = '',
    _date: string = '',
    _contact: Contact = new Contact()
  ) {
    super(_status, _icon, _estimated_distance, _distance, _travel_time, _name, _address, _wait_duration, _location, _time, _date, _contact);
  }
}
