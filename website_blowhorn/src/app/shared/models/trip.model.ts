import { FareBreakup } from './fare-breakup.model';
import { DropOff } from './dropoff.model';
import { Pickup } from './pickup.model';
import { Stop } from './stop.model';
import { NameValue } from './name-value.model';

export class Trip {

  public static get_trip_from_data(data): Trip {
    const trip = new Trip();
    Object.keys(data).forEach(key => {
      switch (key) {
        case 'fare_breakup':
          trip[key] = FareBreakup.get_fare_breakup_from_data(data[key]);
          break;
        case 'fare_breakup_details':
          trip[key] = [];
          data[key].forEach(item => {
            trip[key].push(NameValue.get_name_value_from_data(item));
          });
          break;
        case 'dropoff':
          trip[key] = DropOff.get_dropoff_from_data(data[key]);
          break;
        case 'pickup':
          trip[key] = Pickup.get_pickup_from_data(data[key]);
          break;
        case 'stops':
          trip[key] = [];
          data[key].forEach(item => {
            trip[key].push(Stop.get_stop_from_data(item));
          });
          break;
        case 'amount_payable':
          trip[key] = [];
          data[key].forEach(item => {
            trip[key].push(NameValue.get_name_value_from_data(item));
          });
          break;
        default:
          trip[key] = data[key];
      }
    });
    return trip;
  }

  public get stops_calculates(): {next_index: number, stops_done: number, stops_left: number} {
    let stops_done = 0;
    stops_done = this._stops.filter(item => item.status === 'done').length;
    const stops_left: number = this._stops.length - stops_done;

    return {
      next_index: stops_done === this._stops.length ? null : stops_done,
      stops_done: stops_done,
      stops_left: stops_done === this._stops.length ? 0 : stops_left
    };
  }

  public get next_stop(): {} {
    const next_stop = {};
    let all_stops = [];

    // Push all stops to one array
    all_stops.push(this._pickup);
    all_stops = all_stops.concat(this._stops);
    all_stops.push(this._dropoff);

    const stop_index = all_stops.findIndex(item => item.status !== 'done');
    if ( stop_index >= 0 ) {
      next_stop['stop'] = all_stops[stop_index];
      next_stop['stop_index'] = stop_index;
      next_stop['is_pickup'] = stop_index ? false : true;
      next_stop['is_dropoff'] = stop_index === all_stops.length - 1 ? true : false;
      next_stop['is_stop'] = (stop_index && stop_index < all_stops.length - 1) ? true : false;
    }
    return next_stop;
  }

  constructor(
    private _booked_from: string = '',
    private _booking_datetime: string = '',
    private _pickup_datetime: string = '',
    private _status: string = 'done',
    private _detail_status: string = '',
    private _city: string = '',
    private _city_id: number = 0,
    private _estimated_distance: number = 0,
    private _total_duration: string = '',
    private _total_distance: number = 0,
    private _order_id: string = '',
    private _order_key: string = '',
    private _order_real_id: string = '',
    private _estimate: number = 0,
    private _price: number = 0,
    private _total_payable: number = 0,
    private _balance_payable: number= 0,
    private _fare_breakup: FareBreakup = new FareBreakup(),
    private _fare_breakup_details: NameValue[] = [],
    private _dropoff: DropOff = new DropOff(),
    private _truck_type: string = '',
    private _truck_url: string = '',
    private _truck_url_detail: string = '',
    private _header_to: string = '',
    private _pickup: Pickup = new Pickup(),
    private _driver_name: string = '',
    private _header_from: string = '',
    private _stops: Stop[] = [],
    private _date: any = '',
    private _date_sortable: string = '',
    private _driver_id: string = '',
    private _driver_mobile: string = '',
    private _tracking_url: string = '',
    private _finished_distance: number = 0,
    private _route_info: any[] = [],
    private _payment_status: string = 'unpaid',
    private _payment_mode: string = '',
    private _display_payment: boolean = true,
    private _time: string = '',
    private _vehicle_model: any = null,
    private _vehicle_number: string = '',
    private _pickup_distance: number = 0,
    private _pickup_distance_done: number = 0,
    private _pickup_time_duration: string = '',
    private _last_modified: string = '',
    private _HSN_CODE: string = '',
    private _amount_payable: NameValue[] = [],
    private _rcm: boolean = false,
    private _rcm_message: string = '',
    private _gst_category: string = '',
    private _invoice_number: string = '',
    private _pod_list: string[] = [],
    private _order_type: string = '',
    private _call_masking_required: boolean = false,
    private _customer_name: string = '',
    private _event_date: string = '',
    private _event_remarks: string = '',
) {}

  public get pickup_datetime() {
    return this._pickup_datetime;
  }

  public set pickup_datetime(val: string) {
    this._pickup_datetime = val;
  }

  public get status() {
    return this._status;
  }

  public set status(status: string) {
    this._status = status;
  }

  public get detail_status() {
    return this._detail_status;
  }

  public set detail_status(val: string) {
    this._detail_status = val;
  }

  public get time() {
    return this._time;
  }

  public set time(time: string) {
    this._time = time;
  }

  public get city() {
    return this._city;
  }

  public set city(city: string) {
    this._city = city;
  }

  public get city_id() {
    return this._city_id;
  }

  public set city_id(val: number) {
    this._city_id = val;
  }

  public get estimated_distance() {
    return this._estimated_distance;
  }

  public set estimated_distance(estimated_distance: number) {
    this._estimated_distance = estimated_distance;
  }

  public get total_duration() {
    return this._total_duration;
  }

  public set total_duration(total_duration: string) {
    this._total_duration = total_duration;
  }

  public get total_distance() {
    return this._total_distance;
  }

  public set total_distance(total_distance: number) {
    this._total_distance = total_distance;
  }

  public get order_id() {
    return this._order_id;
  }

  public set order_id(order_id: string) {
    this._order_id = order_id;
  }

  public get order_real_id() {
    return this._order_real_id;
  }

  public set order_real_id(order_real_id: string) {
    this._order_real_id = order_real_id;
  }

  public get order_key() {
    return this._order_key;
  }

  public set order_key(order_key: string) {
    this._order_key = order_key;
  }

  public get estimate() {
    return this._estimate;
  }

  public set estimate(estimate: number) {
    this._estimate = estimate;
  }

  public get price() {
    return this._price;
  }

  public set price(price: number) {
    this._price = price;
  }

  public get total_payable() {
    return this._total_payable;
  }

  public set total_payable(total_payable: number) {
    this._total_payable = total_payable;
  }

  public get balance_payable() {
    return this._balance_payable;
  }

  public set balance_payable(val: number) {
    this._balance_payable = val;
  }

  public get fare_breakup() {
    return this._fare_breakup;
  }

  public set fare_breakup(fare_breakup: FareBreakup) {
    this._fare_breakup = fare_breakup;
  }

  public get dropoff() {
    return this._dropoff;
  }

  public set dropoff(dropoff: DropOff) {
    this._dropoff = dropoff;
  }

  public get truck_type() {
    return this._truck_type;
  }

  public set truck_type(truck_type: string) {
    this._truck_type = truck_type;
  }

  public get truck_url() {
    return this._truck_url;
  }

  public set truck_url(truck_url: string) {
    this._truck_url = truck_url;
  }

  public get truck_url_detail() {
    return this._truck_url_detail;
  }

  public set truck_url_detail(truck_url_detail: string) {
    this._truck_url_detail = truck_url_detail;
  }

  public get header_to() {
    return this._header_to;
  }

  public set header_to(header_to: string) {
    this._header_to = header_to;
  }

  public get pickup() {
    return this._pickup;
  }

  public set pickup(pickup: Pickup) {
    this._pickup = pickup;
  }

  public get driver_name() {
    return this._driver_name;
  }

  public set driver_name(driver_name: string) {
    this._driver_name = driver_name;
  }

  public get header_from() {
    return this._header_from;
  }

  public set header_from(header_from: string) {
    this._header_from = header_from;
  }

  public get stops() {
    return this._stops;
  }

  public set stops(stops: Stop[]) {
    this._stops = stops;
  }

  public get date() {
    return this._date;
  }

  public set date(date: any) {
    this._date = date;
  }

  public get date_sortable() {
    return this._date_sortable;
  }

  public set date_sortable(date_sortable: string) {
    this._date_sortable = date_sortable;
  }

  public get driver_id() {
    return this._driver_id;
  }

  public set driver_id(driver_id) {
    this._driver_id = driver_id;
  }

  public get driver_mobile() {
    return this._driver_mobile;
  }

  public set driver_mobile(driver_mobile: string) {
    this._driver_mobile = driver_mobile;
  }

  public get tracking_url() {
    return this._tracking_url;
  }

  public set tracking_url(tracking_url: string) {
    this._tracking_url = tracking_url;
  }

  public get route_info() {
    return this._route_info;
  }

  public set route_info(route_info: any[]) {
    this._route_info = route_info;
  }

  public get finished_distance() {
    return this._finished_distance;
  }

  public set finished_distance(finished_distance: number) {
    this._finished_distance = finished_distance;
  }

  public get payment_status() {
    return this._payment_status;
  }

  public set payment_status(payment_status: string) {
    this._payment_status = payment_status;
  }

  public get booked_from() {
    return this._booked_from;
  }

  public set booked_from(booked_from: string) {
    this._booked_from = booked_from;
  }

  public get booking_datetime() {
    return this._booking_datetime;
  }

  public set booking_datetime(booking_datetime: string) {
    this._booking_datetime = booking_datetime;
  }

  public get fare_breakup_details(): NameValue[] {
    return this._fare_breakup_details;
  }

  public set fare_breakup_details(fare_breakup_details: NameValue[]) {
    this._fare_breakup_details = fare_breakup_details;
  }

  public get amount_payable(): NameValue[] {
    return this._amount_payable;
  }

  public set amount_payable(amount_payable: NameValue[]) {
    this._amount_payable = amount_payable;
  }

  public get HSN_CODE(): string {
    return this._HSN_CODE;
  }

  public set HSN_CODE(HSN_CODE: string) {
    this._HSN_CODE = HSN_CODE;
  }

  public get rcm(): boolean {
    return this._rcm;
  }

  public set rcm(rcm: boolean) {
    this._rcm = rcm;
  }

  public get rcm_message(): string {
    return this._rcm_message;
  }

  public set rcm_message(rcm_message: string) {
    this._rcm_message = rcm_message;
  }

  public get gst_category(): string {
    return this._gst_category;
  }

  public set gst_category(gst_category: string) {
    this._gst_category = gst_category;
  }

  public get vehicle_model(): string {
    return this._vehicle_model;
  }

  public set vehicle_model(vehicle_model: string) {
    this._vehicle_model = vehicle_model;
  }

  public get vehicle_number(): string {
    return this._vehicle_number;
  }

  public set vehicle_number(vehicle_number: string) {
    this._vehicle_number = vehicle_number;
  }

  public get pickup_distance(): number {
    return this._pickup_distance;
  }

  public set pickup_distance(pickup_distance: number) {
    this._pickup_distance = pickup_distance;
  }

  public get pickup_distance_done(): number {
    return this._pickup_distance_done;
  }

  public set pickup_distance_done(pickup_distance_done: number) {
    this._pickup_distance_done = pickup_distance_done;
  }

  public get pickup_time_duration(): string {
    return this._pickup_time_duration;
  }

  public set pickup_time_duration(pickup_time_duration: string) {
    this._pickup_time_duration = pickup_time_duration;
  }

  public get last_modified(): string {
    return this._last_modified;
  }

  public set last_modified(last_modified: string) {
    this._last_modified = last_modified;
  }

  public get payment_mode() {
    return this._payment_mode;
  }

  public set payment_mode(payment_mode: string) {
    this._payment_mode = payment_mode;
  }

  public get display_payment(): boolean {
    return this._display_payment;
  }

  public set display_payment(display_payment: boolean) {
    this._display_payment = display_payment;
  }

  public get invoice_number(): string {
    return this._invoice_number;
  }

  public set invoice_number(invoice_number: string) {
    this._invoice_number = invoice_number;
  }

  public get pod_list(): string[] {
    return this._pod_list;
  }

  public set pod_list(val: string[]) {
    this._pod_list = val;
  }

  public get order_type(): string {
    return this._order_type;
  }

  public set order_type(val: string) {
    this._order_type = val;
  }

  public get call_masking_required(): boolean {
    return this._call_masking_required;
  }

  public set call_masking_required(val: boolean) {
    this._call_masking_required = val;
  }

  public get customer_name(): string {
    return this._customer_name;
  }

  public set customer_name(val: string) {
    this._customer_name = val;
  }

  public get event_date(): string {
    return this._event_date;
  }

  public set event_date(val: string) {
    this._event_date = val;
  }

  public get event_remarks(): string {
    return this._event_remarks;
  }

  public set event_remarks(val: string) {
    this._event_remarks = val;
  }

}
