import { OrderLine } from './shipment-sku.model';

export class Driver {
    constructor(
        private _name: string = '',
        private _phone: string = '',
    ) {}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get phone(): string {
        return this._phone;
    }

    set phone(phone: string) {
        this._phone = phone;
    }
}

export class ShipmentsGrid {
    constructor(
        private _count: number = null,
        private _next: string = '',
        private _previous: string = '',
        private _results: Shipment[] = null,
    ) {}

    get count(): number {
        return this._count;
    }

    set count(count: number) {
        this._count = count;
    }

    get next(): string {
        return this._next;
    }

    set next(next: string) {
        this._next = next;
    }

    get previous(): string {
        return this._previous;
    }

    set previous(previous: string) {
        this._previous = previous;
    }

    get results(): Shipment[] {
        return this._results;
    }

    set results(results: Shipment[]) {
        this._results = results;
    }
}

export class Shipment {
    constructor(
        private _id: number = null,
        private _number: string = null,
        private _date_placed: string = '',
        private _delivered_time: string = '',
        private _status: string = '',
        private _driver: Driver = new Driver(),
        private _cash_on_delivery: string = '',
        private _customer: Customer = null,
        private _reference_number: string = '',
        private _trip_number: string = '',
        private _customer_reference_number: string = '',
        private _picked_time: string = '',
        private _expected_delivery_time_start: string = '',
        private _expected_delivery_time_end: string = '',
        private _expected_pickup_time: string = '',
        private _pod_files: string[] = [],
        private _batch: string = '',
        private _num_of_attempts: number = 0,
        private _payment_mode: string = '',
        private _amount_paid_online: number = 0,
        private _cash_collected: number = 0,
        private _orderlines:  OrderLine[] = [],
        private _flash: boolean = false,
        private _otp: string = '',
        public _driver_rating: any = [],
        private _is_offline_order: boolean = false,
        private _return_order: boolean = false,
        private _allow_assign_driver: boolean = false,
        private _is_valid_lineedit_status: boolean = false,
        private _checked: boolean = false,
    ) {}


    get id(): number {
        return this._id;
    }

    set id(val: number) {
        this._id = val;
    }

    get number(): string {
        return this._number;
    }

    set number(number: string) {
        this._number = number;
    }

    get pod_files(): string[] {
        return this._pod_files;
    }

    set pod_files(pod_files: string[]) {
        this._pod_files = pod_files;
    }

    get trip_number(): string {
        return this._trip_number;
    }

    set trip_number(trip_number: string) {
        this._trip_number = trip_number;
    }

    get reference_number(): string {
        return this._reference_number;
    }

    set reference_number(reference_number: string) {
        this._reference_number = reference_number;
    }

    get date_placed(): string {
        return this._date_placed;
    }

    set date_placed(date_placed: string) {
        this._date_placed = date_placed;
    }

    get picked_time(): string {
        return this._picked_time;
    }

    set picked_time(val: string) {
        this._picked_time = val;
    }

    get delivered_time(): string {
        return this._delivered_time;
    }

    set delivered_time(delivered_time: string) {
        this._delivered_time = delivered_time;
    }

    get status(): string {
        return this._status;
    }

    set status(status: string) {
        this._status = status;
    }

    get cash_on_delivery(): string {
        return this._cash_on_delivery;
    }

    set cash_on_delivery(cash_on_delivery: string) {
        this._cash_on_delivery = cash_on_delivery;
    }

    get driver(): Driver {
        return this._driver;
    }

    set driver(driver: Driver) {
        this._driver = driver;
    }

    get customer(): Customer {
        return this._customer;
    }

    set vehicle(customer: Customer) {
        this._customer = customer;
    }

    get customer_reference_number(): string {
        return this._customer_reference_number;
    }

    set customer_reference_number(customer_reference_number: string) {
        this._customer_reference_number = customer_reference_number;
    }

    get expected_delivery_time_start(): string {
        return this._expected_delivery_time_start;
    }

    set expected_delivery_time_start(val: string) {
        this._expected_delivery_time_end = val;
    }

    get expected_delivery_time_end(): string {
        return this._expected_delivery_time_end;
    }

    set expected_delivery_time_end(val: string) {
        this._expected_delivery_time_end = val;
    }

    get expected_pickup_time(): string {
        return this._expected_pickup_time;
    }

    set expected_pickup_time(val: string) {
        this._expected_pickup_time = val;
    }

    get batch(): string {
        return this._batch;
    }

    set batch(batch: string) {
        this._batch = batch;
    }

    get num_of_attempts(): number {
        return this._num_of_attempts;
    }

    set num_of_attempts(num_of_attempts: number) {
        this._num_of_attempts = num_of_attempts;
    }

    get payment_mode(): string {
        return this._payment_mode;
    }

    set payment_mode(payment_mode: string) {
        this._payment_mode = payment_mode;
    }

    get amount_paid_online(): number {
        return this._amount_paid_online;
    }

    set amount_paid_online(val: number) {
        this._amount_paid_online = val;
    }

    get cash_collected(): number {
        return this._cash_collected;
    }

    set cash_collected(val: number) {
        this._cash_collected = val;
    }

    get flash(): boolean {
        return this._flash;
    }

    set flash(val: boolean) {
        this._flash = val;
    }

    get orderlines(): OrderLine[] {
        return this._orderlines;
    }

    set orderlines(val: OrderLine[]) {
        this._orderlines = val;
    }

    get otp(): string {
        return this._otp;
    }

    set otp(otp: string) {
        this._otp = otp;
    }

    get is_offline_order(): boolean {
        return this._is_offline_order;
    }

    set is_offline_order(val: boolean) {
        this._is_offline_order = val;
    }

    get return_order(): boolean {
        return this._return_order;
    }

    set return_order(val: boolean) {
        this._return_order = val;
    }

    get allow_assign_driver(): boolean {
        return this._allow_assign_driver;
    }

    set allow_assign_driver(val: boolean) {
        this._allow_assign_driver = val;
    }

    get is_valid_lineedit_status (): boolean {
        return this._is_valid_lineedit_status;
    }

    set is_valid_lineedit_status(val: boolean) {
        this._is_valid_lineedit_status = val;
    }

    get checked(): boolean {
        return this._checked;
    }

    set checked(val: boolean) {
        this._checked = val;
    }

}

export class Customer {
    constructor(
        private _name: string = '',
        private _phone: string = '',
        private _address: string = '',
    ) {}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get phone(): string {
        return this._phone;
    }

    set phone(phone: string) {
        this._phone = phone;
    }

    get address(): string {
        return this._address;
    }

    set address(address: string) {
        this._address = address;
    }
}
