export class Office {

    constructor(
        private _company: { name: string } = { name: ''},
        private _address: string = '',
        private _gst_identification_no: string = '',
        private _cin: string = '',
        private _pan: string = '',
        private _phone_number: string = '',
        private _support_email: string = '',
    ) {}

    get company(): {name: string} {
        return this._company;
    }

    set company( company: {name: string}) {
        this._company = company;
    }

    get address(): string {
        return this._address;
    }

    set address(address: string) {
        this._address = address;
    }

    get gst_identification_no(): string {
        return this._gst_identification_no;
    }

    set gst_identification_no(gst_identification_no: string) {
        this._gst_identification_no = gst_identification_no;
    }

    get cin(): string {
        return this._cin;
    }

    set cin(cin: string) {
        this._cin = cin;
    }

    get pan(): string {
        return this._pan;
    }

    set pan(pan: string) {
        this._pan = pan;
    }

    get phone_number(): string {
        return this._phone_number;
    }

    set phone_number(val: string) {
        this._phone_number = val;
    }

    get support_email(): string {
        return this._support_email;
    }

    set support_email(val: string) {
        this._support_email = val;
    }

}
