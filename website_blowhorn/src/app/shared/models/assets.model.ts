import { OrderLine } from './shipment-sku.model';

export class Asset {

    constructor(
        private _number: string = '',
        private _type: string = '',
    ) {}

    get number(): string {
        return this._number;
    }

    set number(val: string) {
        this._number = val;
    }

    get type(): string {
        return this._type;
    }

    set type(val: string) {
        this._type = val;
    }
}

export class AdditionalEntityDetails {
    constructor(
        private _status: string = '',
        private _payment_mode: string = '',
        private _amount_paid_online: number = 0,
        private _total_cost: number = 0,
        private _cash_collected: number = 0,
        private _orderlines: OrderLine[],
        private _otp: string = '',
        private _number: string = '',
        private _is_valid_lineedit_status: boolean = false,
    ) {}

    get status(): string {
        return this._status;
    }

    set status(val: string) {
        this._status = val;
    }

    get payment_mode(): string {
        return this._payment_mode;
    }

    set payment_mode(val: string) {
        this._payment_mode = val;
    }

    get amount_paid_online(): number {
        return this._amount_paid_online;
    }

    set amount_paid_online(val: number) {
        this._amount_paid_online = val;
    }

    get total_cost(): number {
        return this._total_cost;
    }

    set total_cost(val: number) {
        this._total_cost = val;
    }

    get cash_collected(): number {
        return this._cash_collected;
    }

    set cash_collected(val: number) {
        this._cash_collected = val;
    }

    get orderlines(): OrderLine[] {
        return this._orderlines;
    }

    set orderlines(val: OrderLine[]) {
        this._orderlines = val;
    }

    get otp(): string {
        return this._otp;
    }

    set otp(val: string) {
        this._otp = val;
    }

    get number(): string {
        return this._number;
    }

    set number(val: string) {
        this._number = val;
    }

    get is_valid_lineedit_status (): boolean {
        return this._is_valid_lineedit_status;
    }

    set is_valid_lineedit_status(val: boolean) {
        this._is_valid_lineedit_status = val;
    }
}
