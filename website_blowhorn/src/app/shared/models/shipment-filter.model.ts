import { City } from './city.model';
import { Driver } from './driver.model';
export class ShipmentFilter {

    constructor(
        private _cities: City[] = [],
        private _drivers: Driver[] = [],
        private _statuses: string[] = [],
        private _alert_colors = {},
    ) {}

    get cities(): City[] {
        return this._cities;
    }

    set cities(cities: City[]) {
        this._cities = cities;
    }

    get statuses(): string[] {
        return this._statuses;
    }

    set statuses(statuses: string[]) {
        this._statuses = statuses;
    }

    get alert_colors(): {} {
        return this._alert_colors;
    }

    set alert_colors(val: {}) {
        this._alert_colors = val;
    }

    get drivers(): Driver[] {
        return this._drivers;
    }

    set drivers(drivers: Driver[]) {
        this._drivers = drivers;
    }
}
