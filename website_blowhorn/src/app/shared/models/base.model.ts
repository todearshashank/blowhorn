import { LocationStop } from './location.model';
import { Contact } from './contact.model';
import { Extras } from './extras.model';

export class Base {

  constructor(
    private _status: string = '',
    private _icon: string = '',
    private _estimated_distance: number = 0,
    private _distance: number = 0,
    private _travel_time: string = '',
    private _name: string = '',
    private _address: string = '',
    private _wait_duration: string = '',
    private _location: LocationStop = new LocationStop(),
    private _time: string = '',
    private _date: string = '',
    private _contact: Contact = new Contact(),
    private _extras: Extras = new Extras()
  ) {}

  public get status() {
    return this._status;
  }

  public set status(status: string) {
    this._status = status;
  }

  public get icon() {
    return this._icon;
  }

  public set icon(icon: string) {
    this._icon = icon;
  }

  public get travel_time() {
    return this._travel_time;
  }

  public set travel_time(travel_time: string) {
    this._travel_time = travel_time;
  }

  public get distance() {
    return this._distance;
  }

  public set distance(distance: number) {
    this._distance = distance;
  }

  public get estimated_distance() {
    return this._estimated_distance;
  }

  public set estimated_distance(estimated_distance: number) {
    this._estimated_distance = estimated_distance;
  }

  public get name() {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }

  public get address() {
    return this._address;
  }

  public set address(adress: string) {
    this._address = adress;
  }

  public get wait_duration() {
    return this._wait_duration;
  }

  public set wait_duration(wait_duration: string) {
    this._wait_duration = wait_duration;
  }

  public get location() {
    return this._location;
  }

  public set location(location: LocationStop) {
    this._location = location;
  }

  public get time() {
    return this._time;
  }

  public set time(time: string) {
    this._time = time;
  }

  public get date() {
    return this._date;
  }

  public set date(date: string) {
    this._date = date;
  }

  public get contact(): Contact {
    return this._contact;
  }

  public set contact(contact: Contact) {
    this._contact = contact;
  }

  public get extras(): Extras {
    return this._extras;
  }

  public set extras(extras: Extras) {
    this._extras = extras;
  }
}
