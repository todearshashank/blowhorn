export class ShipmentUnassignedOrder {
    constructor(
        private _lat: number = null,
        private _lng: number = null,
        private _is_modified: boolean = false,
        private _icon_url: string = '',
        private _order_no: string = '',
        private _address: string = '',
        private _is_hidden: boolean = false,
        private _batch_id: number = null,
        private _is_info_window_open = false,
        private _is_ignore: boolean = false,
        private _is_added: boolean = false,
        private _date_placed: string = '',
        private _no_of_lines: number = 0,
        private _batch_description: string = '',
        private _city_id: number = null,
        private _city_name: string = '',
        private _customer_id: number = null,
        private _customer_name: string = '',
        private _address_id: number = null,
        private _postcode: string = '',
    ) {}

    get lat(): number {
        return this._lat;
    }

    set lat(lat: number) {
        this._lat = lat;
    }

    get lng(): number {
        return this._lng;
    }

    set lng(lng: number) {
        this._lng = lng;
    }

    get is_modified(): boolean {
        return this._is_modified;
    }

    set is_modified(modified: boolean) {
        this._is_modified = modified;
    }

    get icon_url(): string {
        return this._icon_url;
    }

    set icon_url(icon_url: string) {
        this._icon_url = icon_url;
    }

    get order_no(): string {
        return this._order_no;
    }

    set order_no(order_no: string) {
        this._order_no = order_no;
    }

    get address(): string {
        return this._address;
    }

    set address(address: string) {
        this._address = address;
    }

    get is_hidden(): boolean {
        return this._is_hidden;
    }

    set is_hidden(hidden: boolean) {
        this._is_hidden = hidden;
    }

    get batch_id(): number {
        return this._batch_id;
    }

    set batch_id(batch_id: number) {
        this._batch_id = batch_id;
    }

    get is_info_window_open(): boolean {
        return this._is_info_window_open;
    }

    set is_info_window_open(is_info_window_open: boolean) {
        this._is_info_window_open = is_info_window_open;
    }

    get is_ignore(): boolean {
        return this._is_ignore;
    }

    set is_ignore(ignore: boolean) {
        this._is_ignore = ignore;
    }

    get is_added(): boolean {
        return this._is_added;
    }

    set is_added(added: boolean) {
        this._is_added = added;
    }

    get no_of_lines(): number {
        return this._no_of_lines;
    }

    set no_of_lines(no_of_lines: number) {
        this._no_of_lines = no_of_lines;
    }

    get batch_description(): string {
        return this._batch_description;
    }

    set batch_description(batch_description: string) {
        this._batch_description = batch_description;
    }

    get city_id(): number {
        return this._city_id;
    }

    set city_id(city_id: number) {
        this._city_id = city_id;
    }

    get city_name(): string {
        return this._city_name;
    }

    set city_name(city_name: string) {
        this._city_name = city_name;
    }

    get customer_id(): number {
        return this._customer_id;
    }

    set customer_id(customer_id: number) {
        this._customer_id = customer_id;
    }

    get customer_name(): string {
        return this._customer_name;
    }

    set customer_name(customer_name: string) {
        this._customer_name = customer_name;
    }

    get date_placed(): string {
        return this._date_placed;
    }

    set date_placed(date_placed: string) {
        this._date_placed = date_placed;
    }

    get address_id(): number {
        return this._address_id;
    }

    set address_id(address_id: number) {
        this._address_id = address_id;
    }

    get postcode(): string {
        return this._postcode;
    }

    set postcode(postcode: string) {
        this._postcode = postcode;
    }
}
