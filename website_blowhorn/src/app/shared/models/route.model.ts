import { Contact } from './contact.model';
import { RouteStop } from './route-stop.model';
import { MapService } from '../services/map.service';

export class Route {
    constructor(
        private _id: number = null,
        private _name: string = '',
        private _contact: Contact = new Contact(),
        private _city: number|string = null,
        private _distance: string = '',
        private _stops: RouteStop[] = [],
        private _geopoint: google.maps.LatLng = MapService.dummy_geopoint,
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get contact(): Contact {
        return this._contact;
    }

    set contact(contact: Contact) {
        this._contact = contact;
    }

    get geopoint(): google.maps.LatLng {
        return this._geopoint;
    }

    set geopoint(geopoint: google.maps.LatLng) {
        this._geopoint = geopoint;
    }

    get city(): number|string {
        return this._city;
    }

    set city(city: number|string) {
        this._city = city;
    }

    get distance(): string {
        return this._distance;
    }

    set distance(distance: string) {
        this._distance = distance;
    }

    get stops(): RouteStop[] {
        return this._stops;
    }

    set stops(stops: RouteStop[]) {
        this._stops = stops;
    }
}
