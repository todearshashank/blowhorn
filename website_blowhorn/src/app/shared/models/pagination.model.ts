export class Pagination {
    constructor(
        private _count: number = null,
        private _next: string = '',
        private _previous: string = '',
        private _results: any[] = null,
    ) {}

    get count(): number {
        return this._count;
    }

    set count(count: number) {
        this._count = count;
    }

    get next(): string {
        return this._next;
    }

    set next(next: string) {
        this._next = next;
    }

    get previous(): string {
        return this._previous;
    }

    set previous(previous: string) {
        this._previous = previous;
    }

    get results(): any[] {
        return this._results;
    }

    set results(results: any[]) {
        this._results = results;
    }
}
