export class LocationStop {

  static get_location_from_data(data: {}): LocationStop {
    const loc = new LocationStop();
    if (!data) {
        return loc;
    }
    loc.lat = data['lat'] || null;
    loc.lng = data['lng'] || null;
    return loc;
  }

  constructor(
    private _lat: number = null,
    private _lng: number = null,
  ) {}

  public get lat() {
    return this._lat;
  }

  public set lat(lat: number) {
    this._lat = lat;
  }

  public get lng() {
    return this._lng;
  }

  public set lng(lng: number) {
    this._lng = lng;
  }

}

export class DriverLocationDetail {
    createByJson (json: JSON) {
        this._better_location = json['better_location'] ? json['better_location'] : true;
        this._is_mock_location = json['is_mock_location'] ? json['is_mock_location'] : false;
        this._city = json['city'] ? json['city'] : '';
        this._distance_km = json['distance_km'] ? json['distance_km'] : 0;
        this._currentTime = json['currentTime'] ? json['currentTime'] : '';
        this._mTime = json['mTime'] ? json['mTime'] : 0;
        this._mSpeed = json['mSpeed'] ? +json['mSpeed'] : 0;
        this._mLatitude = json['mLatitude'] ? parseFloat(json['mLatitude']) : 0;
        this._mLongitude = json['mLongitude'] ? parseFloat(json['mLongitude']) : 0;
        this._mAccuracy = json['mAccuracy'] ? json['mAccuracy'] : 0;
        this._mProvider = json['mProvider'] ? json['mProvider'] : 0;
    }

    constructor(
        private _better_location: boolean = true,
        private _is_mock_location: boolean = false,
        private _is_late_update: boolean = false,
        private _city: string = '',
        private _distance_km: number = 0,
        private _currentTime: string = '',
        private _mTime: number = 0,
        private _mSpeed: number = 0,
        private _mLatitude: number = 0,
        private _mLongitude: number = 0,
        private _mAccuracy: number = 0,
        private _mProvider: string = '',
        // Below params need to be used for better location prediction algo.
        // private _mAltitude: number = 0,
        // private _mBearingAccuracyDegrees: number = 0,
        // private _mElapsedRealtimeNanos: number = 0,
        // private _mSpeedAccuracyMetersPerSecond: number = 0,
        // private _mVerticalAccuracyMeters: number = 0,
    ) {}

    public get better_location(): boolean {
        return this._better_location;
    }

    public set better_location(better_location: boolean) {
        this._better_location = better_location;
    }

    public get is_late_update(): boolean {
        return this._is_late_update;
    }

    public set is_late_update(is_late_update: boolean) {
        this._is_late_update = is_late_update;
    }

    public get is_mock_location(): boolean {
        return this._is_mock_location;
    }

    public set is_mock_location(is_mock_location: boolean) {
        this._is_mock_location = is_mock_location;
    }

    public get city(): string {
        return this._city;
    }

    public set city(city: string) {
        this._city = city;
    }

    public get distance_km(): number {
        return this._distance_km;
    }

    public set distance_km(distance_km: number) {
        this._distance_km = distance_km;
    }

    public get mTime(): number {
        return this._mTime;
    }

    public set mTime(mTime: number) {
        this._mTime = mTime;
    }

    public get currentTime(): string {
        return this._currentTime;
    }

    public set currentTime(currentTime: string) {
        this._currentTime = currentTime;
    }

    public get mSpeed(): number {
        return this._mSpeed;
    }

    public set mSpeed(mSpeed: number) {
        this._mSpeed = mSpeed;
    }

    public get mLatitude(): number {
        return this._mLatitude;
    }

    public set mLatitude(mLatitude: number) {
        this._mLatitude = mLatitude;
    }

    public get mLongitude(): number {
        return this._mLongitude;
    }

    public set mLongitude(mLongitude: number) {
        this._mLongitude = mLongitude;
    }

    public get mAccuracy(): number {
        return this._mAccuracy;
    }

    public set mAccuracy(mAccuracy: number) {
        this._mAccuracy = mAccuracy;
    }

    public get mProvider(): string {
        return this._mProvider;
    }

    public set mProvider(_mProvider: string) {
        this._mProvider = _mProvider;
    }
}
