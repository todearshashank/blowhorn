export class FleetInfoWindow {

    constructor(
        private _message: string = '',
        private _data: Data = new Data()
    ) {}

    get message(): string {
        return this._message;
    }

    set message(message: string) {
        this._message = message;
    }

    get data(): Data {
        return this._data;
    }

    set data(data: Data) {
        this._data = data;
    }
}

class Data {

    constructor(
        private _trip: Trip = new Trip()
    ) {}

    get trip(): Trip {
        return this._trip;
    }

    set trip(trip: Trip) {
        this._trip = trip;
    }
}

class Trip {

    constructor(
        private _id: number = null,
        private _next_stop: string = '',
        private _order_number: string = ''
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get next_stop(): string {
        return this._next_stop;
    }

    set next_stop(next_stop: string) {
        this._next_stop = next_stop;
    }

    get order_number(): string {
        return this._order_number;
    }

    set order_number(order_number: string) {
        this._order_number = order_number;
    }
}
