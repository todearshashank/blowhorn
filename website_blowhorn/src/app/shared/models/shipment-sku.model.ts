export class OrderLine {
    
    constructor(
        private _id: number = null,
        private _sku: string = '',
        private _order_number: string = '',
        private _reference_number: string = '',
        private _order: number = null,
        private _quantity: number = 0,
        private _delivered: number = 0,
        private _each_item_price: number = 0,
        private _qty_delivered: number = 0,
        private _qty_removed: number = 0,
        private _total_item_price: string = '',
        private _discount_type: string = '',
        private _discounts: number = 0,
        private _hsn_code: string = '',
        private _cgst: number = 0,
        private _sgst: number = 0,
        private _igst: number = 0,
        private _length: number = 0,
        private _breadth: number = 0,
        private _height: number = 0,
        private _volume: number = 0,
        private _weight: number = 0,
        private _uom: string = ''
    ) {}

    get id(): number {
        return this._id;
    }

    set id(val: number) {
        this._id = val;
    }

    get order_number(): string {
        return this._order_number;
    }

    set order_number(val: string) {
        this._order_number = val;
    }

    get reference_number(): string {
        return this._reference_number;
    }

    set reference_number(val: string) {
        this._reference_number = val;
    }

    get order(): number {
        return this._order;
    }

    set order(val: number) {
        this._order = val;
    }

    get quantity(): number {
        return this._quantity;
    }

    set quantity(val: number) {
        this._quantity = val;
    }

    get delivered(): number {
        return this._delivered;
    }

    set delivered(val: number) {
        this._delivered = val;
    }

    get each_item_price(): number {
        return this._each_item_price;
    }

    set each_item_price(val: number) {
        this._each_item_price = val;
    }

    get qty_removed(): number {
        return this._qty_removed;
    }

    set qty_removed(val: number) {
        this._qty_removed = val;
    }

    get qty_delivered(): number {
        return this._qty_delivered;
    }

    set qty_delivered(val: number) {
        this._qty_delivered = val;
    }

    get total_item_price(): string {
        return this._total_item_price;
    }

    set total_item_price(val: string) {
        this._total_item_price = val;
    }

    get discount_type(): string {
        return this._discount_type;
    }

    set discount_type(val: string) {
        this._discount_type = val;
    }

    get discounts(): number {
        return this._discounts;
    }

    set discounts(val: number) {
        this._discounts = val;
    }

    get sku(): string {
        return this._sku;
    }

    set sku(val: string) {
        this._sku = val;
    }

    get hsn_code(): string {
        return this._hsn_code;
    }

    set hsn_code(val: string) {
        this._hsn_code = val;
    }

    get cgst(): number {
        return this._cgst;
    }

    set cgst(val: number) {
        this._cgst = val;
    }

    get sgst(): number {
        return this._sgst;
    }

    set sgst(val: number) {
        this._sgst = val;
    }

    get igst(): number {
        return this._igst;
    }

    set igst(val: number) {
        this._igst = val;
    }

    get length(): number {
        return this._length;
    }

    set length(val: number) {
        this._length = val;
    }

    get breadth(): number {
        return this._breadth;
    }

    set breadth(val: number) {
        this._breadth = val;
    }

    get height(): number {
        return this._height;
    }

    set height(val: number) {
        this._height = val;
    }

    get volume(): number {
        return this._volume;
    }

    set volume(val: number) {
        this._volume = val;
    }

    get weight(): number {
        return this._weight;
    }

    set weight(val: number) {
        this._weight = val;
    }

    get uom(): string {
        return this._uom;
    }

    set uom(val: string) {
        this._uom = val;
    }
    


}
