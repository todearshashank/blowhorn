import { LocationStop } from './location.model';

export class GeoPoint extends LocationStop {
  constructor(
    lat: number = null,
    lng: number = null
  ) {
    super(lat, lng);
  }
}
