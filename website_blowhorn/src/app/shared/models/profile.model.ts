export class Profile {

    fromJson(data: any): Profile {
        let profile = new Profile();
        profile.cid = data['cid'];
        profile.name = data['name'];
        profile.email = data['email'];
        profile.mobile = data['mobile'];
        profile.organization_name = data['organization_name'];
        profile.legal_name = data['legal_name'];
        profile.gstin = data['gstin'];
        profile.mobile_status = data['mobile_status'];
        profile.email_status = data['email_status'];
        profile.is_business_user = data['is_business_user'];
        profile.is_org_admin = data['is_org_admin'];
        profile.is_staff = data['is_staff'];
        profile.pending_amount = data['pending_amount'];
        profile.wallet_available_balance = data['wallet_available_balance'];
        profile.state = data['state'];
        profile.recent_locations = data['recent_locations'] || [];
        profile.saved_locations = data['saved_locations'] || [];
        return profile;
    }

    constructor(
        private _cid: number = null,
        private _name: string = '',
        private _email: string = '',
        private _mobile: number = null,
        private _organization_name: string = '',
        private _legal_name: string = '',
        private _gstin: string = '',
        private _mobile_status: boolean = false,
        private _email_status: boolean = false,
        private _is_business_user: boolean = false,
        private _is_org_admin: boolean = false,
        private _is_staff: boolean = false,
        private _pending_amount: number = 0,
        private _wallet_available_balance: number = 0,
        private _state: number = null,
        private _recent_locations: any[] = [],
        private _saved_locations: any[] = [],
    ) {}

    get cid(): number {
        return this._cid;
    }

    set cid(val: number) {
        this._cid = val;
    }

    get name(): string {
        return this._name;
    }

    set name(val: string) {
        this._name = val;
    }

    get email(): string {
        return this._email;
    }

    set email(val: string) {
        this._email = val;
    }

    get mobile(): number {
        return this._mobile;
    }

    set mobile(val: number) {
        this._mobile = val;
    }

    get organization_name(): string {
        return this._organization_name;
    }

    set organization_name(val: string) {
        this._organization_name = val;
    }

    get legal_name(): string {
        return this._legal_name;
    }

    set legal_name(val: string) {
        this._legal_name = val;
    }

    get gstin(): string {
        return this._gstin;
    }

    set gstin(val: string) {
        this._gstin = val;
    }

    get mobile_status(): boolean {
        return this._mobile_status;
    }

    set mobile_status(val: boolean) {
        this._mobile_status = val;
    }

    get email_status(): boolean {
        return this._email_status;
    }

    set email_status(val: boolean) {
        this._email_status = val;
    }

    get is_business_user(): boolean {
        return this._is_business_user;
    }

    set is_business_user(val: boolean) {
        this._is_business_user = val;
    }

    get is_org_admin(): boolean {
        return this._is_org_admin;
    }

    set is_org_admin(val: boolean) {
        this._is_org_admin = val;
    }

    get is_staff(): boolean {
        return this._is_staff;
    }

    set is_staff(val: boolean) {
        this._is_staff = val;
    }

    get pending_amount(): number {
        return this._pending_amount;
    }

    set pending_amount(val: number) {
        this._pending_amount = val;
    }

    get wallet_available_balance(): number {
        return this._wallet_available_balance;
    }

    set wallet_available_balance(val: number) {
        this._wallet_available_balance = val;
    }

    get state(): number {
        return this._state;
    }

    set state(val: number) {
        this._state = val;
    }

    get recent_locations(): any[] {
        return this._recent_locations;
    }

    set recent_locations(val: any[]) {
        this._recent_locations = val;
    }

    get saved_locations(): any[] {
        return this._saved_locations;
    }

    set saved_locations(val: any[]) {
        this._saved_locations = val;
    }
}
