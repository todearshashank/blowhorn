export class TripsSummary {

  public static get_summary_trips_from_data (data: {}): TripsSummary {
    const instance = new TripsSummary();
    instance.total_distance = data['total_distance'] ? +data['total_distance'] : 0;
    instance.total_cost = data['total_cost'] ? +data['total_cost'] : 0;
    instance.num_trips = data['number_trips'] ? +data['number_trips'] : 0;
    instance.data_to_chart = data['data'] ? data['data'] : [];

    instance.num_x = instance.data_to_chart.length;

    if (instance.num_x > 0) {
      instance.data_to_chart.forEach(item => {
        if (+item.trips > instance.max_trips) {
          instance.max_trips = +item.trips;
        }
        if (+item.distance > instance.max_distance) {
          instance.max_distance = +item.distance;
        }
        if (+item.cost > instance.max_cost) {
          instance.max_cost = +item.cost;
        }
      });
    }
    return instance;
  }

  constructor(
    private _num_x: number = 0,
    private _max_trips: number = 0,
    private _max_distance: number = 0,
    private _max_cost: number = 0,
    private _total_distance: number = 0,
    private _total_cost: number = 0,
    private _num_trips: number = 0,
    private _data_to_chart: {[key: string ]: string}[] = []
  ) {}

  public get num_x(): number {
    return this._num_x;
  }

  public set num_x(num_x: number) {
    this._num_x = num_x;
  }

  public get max_trips(): number {
    return this._max_trips;
  }

  public set max_trips(max_trips: number) {
    this._max_trips = max_trips;
  }

  public get max_distance(): number {
    return this._max_distance;
  }

  public set max_distance(max_distance: number) {
    this._max_distance = max_distance;
  }

  public get max_cost(): number {
    return this._max_cost;
  }

  public set max_cost(max_cost: number) {
    this._max_cost = max_cost;
  }

  public get total_distance(): number {
    return this._total_distance;
  }

  public set total_distance(total_distance: number) {
    this._total_distance = total_distance;
  }

  public get total_cost(): number {
    return this._total_cost;
  }

  public set total_cost(total_cost: number) {
    this._total_cost = total_cost;
  }

  public get num_trips(): number {
    return this._num_trips;
  }

  public set num_trips(num_trips: number) {
    this._num_trips = num_trips;
  }

  public get data_to_chart(): {[key: string ]: string}[]{
    return this._data_to_chart;
  }

  public set data_to_chart(data_to_chart: {[key: string ]: string}[]) {
    this._data_to_chart = data_to_chart;
  }

}
