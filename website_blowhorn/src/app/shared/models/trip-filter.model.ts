import { City } from './city.model';
import { IdName } from './id-name.model';
import { Driver } from './driver.model';
export class TripFilter {

    constructor(
        private _cities: City[] = [],
        private _vehicle_classes: IdName[] = [],
        private _drivers: Driver[] = [],
        private _contract_types: any[] = []
    ) {}

    get cities(): City[] {
        return this._cities;
    }

    set cities(cities: City[]) {
        this._cities = cities;
    }

    get vehicle_classes(): IdName[] {
        return this._vehicle_classes;
    }

    set vehicle_classes(vehicle_classes: IdName[]) {
        this._vehicle_classes = vehicle_classes;
    }

    get drivers(): Driver[] {
        return this._drivers;
    }

    set drivers(drivers: Driver[]) {
        this._drivers = drivers;
    }

    get contract_types(): any[] {
        return this._contract_types;
    }

    set contract_types(val: any[]) {
        this._contract_types = val;
    }
}
