
export class TripStop {
    constructor(
        private _id: number = null,
        private _completed: boolean = false,
        private _sequence_id: number = null,
        private _shipping_address: ShippingAddress = new ShippingAddress(),
    ) {}

    get completed(): boolean {
        return this._completed;
    }

    set completed(completed: boolean) {
        this._completed = completed;
    }

    get sequence_id(): number {
        return this._sequence_id;
    }

    set id(id: number) {
        this._id = id;
    }

    get id(): number {
        return this._id;
    }

    set sequence_id(sequence_id: number) {
        this._sequence_id = sequence_id;
    }

    get shipping_address(): ShippingAddress {
        return this._shipping_address;
    }

    set shipping_address(shipping_address: ShippingAddress) {
        this._shipping_address = shipping_address;
    }
}

export class ShippingAddress {
    constructor(
        private _id: number = null,
        private _first_name: string = '',
        private _last_name: string = '',
        private _line1: string = '',
        private _line2: string = '',
        private _line3: string = '',
        private _line4: string = '',
        private _state: string = '',
        private _country: string = '',
        private _postcode: number = null,
        private _phone_number: string = '',
        private _geopoint: Geopoint = new Geopoint(),
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get first_name(): string {
        return this._first_name;
    }

    set first_name(first_name: string) {
        this._first_name = first_name;
    }

    get last_name(): string {
        return this._last_name;
    }

    set last_name(last_name: string) {
        this._last_name = last_name;
    }

    get line1(): string {
        return this._line1;
    }

    set line1(line1: string) {
        this._line1 = line1;
    }

    get line2(): string {
        return this._line2;
    }

    set line2(line2: string) {
        this._line2 = line2;
    }

    get line3(): string {
        return this._line3;
    }

    set line3(line3: string) {
        this._line3 = line3;
    }

    get line4(): string {
        return this._line4;
    }

    set line4(line4: string) {
        this._line4 = line4;
    }

    get state(): string {
        return this._state;
    }

    set state(state: string) {
        this._state = state;
    }

    get country(): string {
        return this._country;
    }

    set country(country: string) {
        this._country = country;
    }

    get postcode(): number {
        return this._postcode;
    }

    set postcode(postcode: number) {
        this._postcode = postcode;
    }

    get phone_number(): string {
        return this._phone_number;
    }

    set phone_number(phone_number: string) {
        this._phone_number = phone_number;
    }

    get geopoint(): Geopoint {
        return this._geopoint;
    }

    set geopoint(geopoint: Geopoint) {
        this._geopoint = geopoint;
    }
}

export class Geopoint {
    constructor(
        private _type: string = '',
        private _coordinates: [number, number] = [null, null]
    ) {}

    get type(): string {
        return this._type;
    }

    set type(type: string) {
        this._type = type;
    }

    get coordinates(): [number, number] {
        return this._coordinates;
    }

    set coordinates(coordinates: [number, number]) {
        this._coordinates = coordinates;
    }
}

