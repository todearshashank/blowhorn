import { TaxDistribution } from './tax-distribution.model';

export class FareBreakup {

  static get_fare_breakup_from_data(data): FareBreakup {
    const fare_breakup = new FareBreakup();
    Object.keys(data).forEach(key => {
      switch (key) {
        case 'tax_distribution':
          fare_breakup[key] = TaxDistribution.get_tax_distribution_from_data(data[key]);
          break;
        default:
          fare_breakup[key] = data[key];
      }
    });
    return fare_breakup;
  }

  constructor(
    private _time: number = 0,
    private _distance: number = 0,
    private _labour: number = 0,
    private _GST: number = 0,
    private _fixed: number = 0,
    private _adjustment: number = 0,
    private _tax_distribution: TaxDistribution = new TaxDistribution()
  ) {}

  public get tax_distribution(): TaxDistribution {
    return this._tax_distribution;
  }

  public set tax_distribution(tax_distribution: TaxDistribution) {
    this._tax_distribution = tax_distribution;
  }

  public get time() {
    return this._time;
  }

  public set time(time: number) {
    this._time = time;
  }

  public get distance() {
    return this._distance;
  }

  public set distance(distance: number) {
    this._distance = distance;
  }

  public get GST() {
    return this._GST;
  }

  public set GST(GST: number) {
    this._GST = GST;
  }

  public get fixed() {
    return this._fixed;
  }

  public set fixed(fixed: number) {
    this._fixed = fixed;
  }

  public get adjustment() {
    return this._adjustment;
  }

  public set adjustment(adjustment: number) {
    this._adjustment = adjustment;
  }

  public get labour() {
    return this._labour;
  }

  public set labour(labour: number) {
    this._labour = labour;
  }
}
