import { Driver } from './driver.model';
import { DeviceDetails } from './device-details.model';
import { MapMarker } from './map-marker.model';
import { DriverLocationDetail } from './location.model';

export class Extras {
    constructor(
        private _trip_number: string = '',
        private _customer_name: string = '',
        private _customer_pk: number = null,
        private _booking_id: string = '',
        private _booking_type: string = '',
        private _event: string = '',
        private _has_covid_pass = false
    ) {}

    public get trip_number(): string {
        return this._trip_number;
    }

    public set trip_number(trip_number: string) {
        this._trip_number = trip_number;
    }

    public get customer_name(): string {
        return this._customer_name;
    }

    public set customer_name(customer_name: string) {
        this._customer_name = customer_name;
    }

    public get customer_pk(): number {
        return this._customer_pk;
    }

    public set customer_pk(customer_pk: number) {
        this._customer_pk = customer_pk;
    }

    public get booking_id(): string {
        return this._booking_id;
    }

    public set booking_id(booking_id: string) {
        this._booking_id = booking_id;
    }

    public get booking_type(): string {
        return this._booking_type;
    }

    public set booking_type(booking_type: string) {
        this._booking_type = booking_type;
    }

    public get event(): string {
        return this._event;
    }

    public set event(event: string) {
        this._event = event;
    }

    public get has_covid_pass(): boolean {
        return this._has_covid_pass;
    }

    public set has_covid_pass(val: boolean) {
        this._has_covid_pass = val;
    }

}

export class DriverTrackingDetail {

    constructor(
        private _driver_id: number = null,
        private _profile: Driver = new Driver(),
        private _device_details: DeviceDetails = new DeviceDetails(),
        private _location: DriverLocationDetail = new DriverLocationDetail(),
        private _extras: Extras = new Extras(),
        private _marker: MapMarker = new MapMarker(),
    ) {}

    public get driver_id(): number {
        return this._driver_id;
    }

    public set driver_id(driver_id: number) {
        this._driver_id = driver_id;
    }

    public get profile(): Driver {
        return this._profile;
    }

    public set profile(profile: Driver) {
        this._profile = profile;
    }

    public get device_details(): DeviceDetails {
        return this._device_details;
    }

    public set device_details(device_details: DeviceDetails) {
        this._device_details = device_details;
    }

    public get location(): DriverLocationDetail {
        return this._location;
    }

    public set location(location: DriverLocationDetail) {
        this._location = location;
    }

    public get extras(): Extras {
        return this._extras;
    }

    public set extras(extras: Extras) {
        this._extras = extras;
    }

    public get marker(): MapMarker {
        return this._marker;
    }

    public set marker(marker: MapMarker) {
        this._marker = marker;
    }

}
