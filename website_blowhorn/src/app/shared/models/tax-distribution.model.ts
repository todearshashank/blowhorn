import { GST } from './gst.model';

export class TaxDistribution {

    static get_tax_distribution_from_data(data): TaxDistribution {
        return new TaxDistribution(data['SGST'], data['CGST']);
    }

    constructor(
        private _SGST: GST = new GST(),
        private _CGST: GST = new GST(),
    ) {}

    public get SGST(): GST {
        return this._SGST;
    }

    public set SGST(SGST: GST) {
        this._SGST = SGST;
    }

    public get CGST(): GST {
        return this._CGST;
    }

    public set CGST(CGST: GST) {
        this._CGST = CGST;
    }
}
