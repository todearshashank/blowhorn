export class BankAccount {

    constructor(
        private _id: number = null,
        private _account_name: string = '',
        private _account_number: number = null,
        private _confirm_account_number: number = null,
        private _bank_name: string = '',
        private _branch_name: string = '',
        private _ifsc_code: string = '',
        private _address: string = '',
        private _city: string = '',
        private _district: string = '',
        private _state: string = '',
        private _rtgs: boolean = false,
        private _is_active: boolean = null,
    ) {}

    get id(): number {
        return this._id;
    }

    set id(val: number) {
        this._id = val;
    }

    get account_number(): number {
        return this._account_number;
    }

    set account_number(val: number) {
        this._account_number = val;
    }

    get confirm_account_number(): number {
        return this._confirm_account_number;
    }

    set confirm_account_number(val: number) {
        this._confirm_account_number = val;
    }

    get account_name(): string {
        return this._account_name;
    }

    set account_name(val: string) {
        this._account_name = val;
    }

    get ifsc_code(): string {
        return this._ifsc_code;
    }

    set ifsc_code(val: string) {
        this._ifsc_code = val;
    }

    get bank_name(): string {
        return this._bank_name;
    }

    set bank_name(val: string) {
        this._bank_name = val;
    }

    get branch_name(): string {
        return this._branch_name;
    }

    set branch_name(val: string) {
        this._branch_name = val;
    }

    get address(): string {
        return this._address;
    }

    set address(val: string) {
        this._address = val;
    }

    get city(): string {
        return this._city;
    }

    set city(val: string) {
        this._city = val;
    }

    get district(): string {
        return this._district;
    }

    set district(val: string) {
        this._district = val;
    }

    get state(): string {
        return this._state;
    }

    set state(val: string) {
        this._state = val;
    }

    get rtgs(): boolean {
        return this._rtgs;
    }

    set rtgs(val: boolean) {
        this._rtgs = val;
    }

    get is_active(): boolean {
        return this._is_active;
    }

    set is_active(val: boolean) {
        this._is_active = val;
    }
}
