export class TripsGrid {
    constructor(
        private _count: number = null,
        private _next: string = '',
        private _previous: string = '',
        private _results: Trip[] = null,
    ) {}

    get count(): number {
        return this._count;
    }

    set count(count: number) {
        this._count = count;
    }

    get next(): string {
        return this._next;
    }

    set next(next: string) {
        this._next = next;
    }

    get previous(): string {
        return this._previous;
    }

    set previous(previous: string) {
        this._previous = previous;
    }

    get results(): Trip[] {
        return this._results;
    }

    set results(results: Trip[]) {
        this._results = results;
    }
}

export class Trip {
    constructor(
        private _distance_elapsed: number = null,
        private _id: number = null,
        private _order_no: string = '',
        private _time_elapsed: string = '',
        private _time_scheduled: string = '',
        private _time_started_actual: string = '',
        private _trip_number: string = '',
        private _driver: Driver = new Driver(),
        private _vehicle: Vehicle = new Vehicle(),
        private _time_ended_actual: string = '',
        private _total_distance: number = null,
        private _total_time: number = null,
        private _wait_time: number = null,
        private _city_id: number = null,
        private _trip_type: string = '',
        private _otp: string = '',
        private _status: string = '',
        private _short_status: string = '',
        private _stops_count: number = 0,
    ) {}

    get distance_elapsed(): number {
        return this._distance_elapsed;
    }

    set distance_elapsed(distance_elapsed: number) {
        this._distance_elapsed = distance_elapsed;
    }

    get total_distance(): number {
        return this._total_distance;
    }

    set total_distance(total_distance: number) {
        this._total_distance = total_distance;
    }

    get total_time(): number {
        return this._total_time;
    }

    set total_time(total_time: number) {
        this._total_time = total_time;
    }

    get wait_time(): number {
        return this._wait_time;
    }

    set wait_time(val: number) {
        this._wait_time = val;
    }

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get order_no(): string {
        return this._order_no;
    }

    set order_no(order_no: string) {
        this._order_no = order_no;
    }

    get time_elapsed(): string {
        return this._time_elapsed;
    }

    set time_elapsed(time_elapsed: string) {
        this._time_elapsed = time_elapsed;
    }

    get time_scheduled(): string {
        return this._time_scheduled;
    }

    set time_scheduled(time_scheduled: string) {
        this._time_scheduled = time_scheduled;
    }

    get time_started_actual(): string {
        return this._time_started_actual;
    }

    set time_started_actual(time_started_actual: string) {
        this._time_started_actual = time_started_actual;
    }

    get time_ended_actual(): string {
        return this._time_ended_actual;
    }

    set time_ended_actual(time_ended_actual: string) {
        this._time_ended_actual = time_ended_actual;
    }

    get trip_number(): string {
        return this._trip_number;
    }

    set trip_number(trip_number: string) {
        this._trip_number = trip_number;
    }

    get driver(): Driver {
        return this._driver;
    }

    set driver(driver: Driver) {
        this._driver = driver;
    }

    get vehicle(): Vehicle {
        return this._vehicle;
    }

    set vehicle(vehicle: Vehicle) {
        this._vehicle = vehicle;
    }

    get city_id(): number {
      return this._city_id;
    }

    set city_id(city_id: number) {
      this._city_id = city_id;
    }

    get trip_type(): string {
        return this._trip_type;
    }

    set trip_type(val: string) {
        this._trip_type = val;
    }

    get otp(): string {
        return this._otp;
    }

    set otp(val: string) {
        this._otp = val;
    }

    get status(): string {
        return this._status;
    }

    set status(val: string) {
        this._status = val;
    }

    get short_status(): string {
        return this._short_status;
    }

    set short_status(val: string) {
        this._short_status = val;
    }

    get stops_count(): number {
        return this._stops_count;
    }

    set stops_count(val: number) {
        this._stops_count = val;
    }

}

export class Driver {
    constructor(
        private _name: string = '',
        private _phone: string = '',
    ) {}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get phone(): string {
        return this._phone;
    }

    set phone(phone: string) {
        this._phone = phone;
    }
}

export class Vehicle {
    constructor(
        private _name: string = '',
        private _type: string = '',
    ) {}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get type(): string {
        return this._type;
    }

    set type(type: string) {
        this._type = type;
    }
}
