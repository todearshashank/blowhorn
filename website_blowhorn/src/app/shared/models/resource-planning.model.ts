import { IdName } from './id-name.model';

export class ResourcePlanningFilter {
    constructor(
        private _cities: IdName[] = [],
        private _hubs: IdName[] = [],
    ) {}

    get cities(): IdName[] {
        return this._cities;
    }

    set cities(cities: IdName[]) {
        this._cities = cities;
    }

    get hubs(): IdName[] {
        return this._hubs;
    }

    set hubs(val: IdName[]) {
        this._hubs = val;
    }

}

export class ResourcePlanning {

    public static fromJson(data: any): ResourcePlanning {
        let rp = new ResourcePlanning();
        rp.pk = data.id,
        rp.city = data.city;
        rp.hub = data.hub;
        rp.vehicle_class = data.commercial_classification;
        rp.start_time = data.start_time;
        rp.end_time = data.end_time;
        rp.total_drivers = data.autodispatch_drivers.length;
        rp.occupied_drivers = 0;
        rp.coverage = data.coverage && data.coverage.coordinates && data.coverage.coordinates.length ?
            data.coverage.coordinates[0] : [];
        rp.extended_coverage = data.extended_coverage && data.extended_coverage.coordinates && data.extended_coverage.coordinates.length ?
            data.extended_coverage.coordinates[0] : [];
        rp.radiusEnabled = data.radius_based_order;
        rp.radius = data.radius;
        rp.pincodes = data.pincodes.sort();
        return rp;
    }
    constructor(
        private _pk: number = null,
        private _city: string = '',
        private _hub: string = '',
        private _vehicle_class: string = '',
        private _start_time: string = '',
        private _end_time: string = '',
        private _occupied_drivers: number = 0,
        private _total_drivers: number = 0,
        private _coverage: any = [],
        private _extended_coverage: any = [],
        private _radiusEnabled: boolean = false,
        private _radius: number = null,
        private _pincodes: any[] = [],
    ) {}

    get pk(): number {
        return this._pk;
    }

    set pk(val: number) {
        this._pk = val;
    }

    get city(): string {
        return this._city;
    }

    set city(val: string) {
        this._city = val;
    }

    get hub(): string {
        return this._hub;
    }

    set hub(val: string) {
        this._hub = val;
    }

    get vehicle_class(): string {
        return this._vehicle_class;
    }

    set vehicle_class(val: string) {
        this._vehicle_class = val;
    }

    get start_time(): string {
        return this._start_time;
    }

    set start_time(val: string) {
        this._start_time = val;
    }

    get end_time(): string {
        return this._end_time;
    }

    set end_time(val: string) {
        this._end_time = val;
    }

    get occupied_drivers(): number {
        return this._occupied_drivers;
    }

    set occupied_drivers(val: number) {
        this._occupied_drivers = val;
    }

    get total_drivers(): number {
        return this._total_drivers;
    }

    set total_drivers(val: number) {
        this._total_drivers = val;
    }

    get coverage(): [] {
        return this._coverage;
    }

    set coverage(val: []) {
        this._coverage = val;
    }

    get extended_coverage(): [] {
        return this._extended_coverage;
    }

    set extended_coverage(val: []) {
        this._extended_coverage = val;
    }

    get radiusEnabled(): boolean {
        return this._radiusEnabled;
    }

    set radiusEnabled(val: boolean) {
        this._radiusEnabled = val;
    }

    get radius(): number {
        return this._radius;
    }

    set radius(val: number) {
        this._radius = val;
    }

    get pincodes(): any[] {
        return this._pincodes;
    }

    set pincodes(val: any[]) {
        this._pincodes = val;
    }
}
