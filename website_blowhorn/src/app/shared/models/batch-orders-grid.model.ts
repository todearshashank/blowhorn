export class BatchOrdersGrid {
    constructor(
        private _count: number = null,
        private _next: string = '',
        private _previous: string = '',
        private _results: BatchOrder[] = null,
    ) {}

    get count(): number {
        return this._count;
    }

    set count(count: number) {
        this._count = count;
    }

    get next(): string {
        return this._next;
    }

    set next(next: string) {
        this._next = next;
    }

    get previous(): string {
        return this._previous;
    }

    set previous(previous: string) {
        this._previous = previous;
    }

    get results(): BatchOrder[] {
        return this._results;
    }

    set results(results: BatchOrder[]) {
        this._results = results;
    }
}

export class BatchOrder {
    constructor(
        private _id: number = null,
        private _description: string = '',
        private _number_of_entries: number = null
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get description(): string {
        return this._description;
    }

    set description(description: string) {
        this._description = description;
    }

    get number_of_entries(): number {
        return this._number_of_entries;
    }

    set number_of_entries(number_of_entries: number) {
        this._number_of_entries = number_of_entries;
    }
}
