import { Contact } from './contact.model';
import { Address } from './address.model';
import { GeoPoint } from './geopoint.model';

export class SavedLocation {
    constructor(
        private _name: string = '',
        private _geopoint: GeoPoint = new GeoPoint(),
        private _contact: Contact = new Contact(),
        private _address: Address = new Address(),
    ) {}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get geopoint(): GeoPoint {
        return this._geopoint;
    }

    set geopoint(geopoint: GeoPoint) {
        this._geopoint = geopoint;
    }

    get contact(): Contact {
        return this._contact;
    }

    set contact(contact: Contact) {
        this._contact = contact;
    }

    get address(): Address {
        return this._address;
    }

    set address(address: Address) {
        this._address = address;
    }
}
