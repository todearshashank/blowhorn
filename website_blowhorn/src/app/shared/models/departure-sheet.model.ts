import { Shipment } from './shipment.model';

export class DepartureSheet {
    constructor(
        private _shipments: Shipment[] = [],
        private _date_time: string = '',
        private _driver_name: string = '',
    ) {}

    get shipments(): Shipment[] {
        return this._shipments;
    }

    set shipment(shipments: Shipment[]) {
        this._shipments = shipments;
    }

    get date_time(): string {
        return this._date_time;
    }

    set date_time(date_time: string) {
        this._date_time = date_time;
    }

    get driver_name(): string {
        return this._driver_name;
    }

    set driver_name(driver_name: string) {
        this._driver_name = driver_name;
    }
}
