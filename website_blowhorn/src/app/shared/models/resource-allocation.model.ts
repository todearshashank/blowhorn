import { IdName } from './id-name.model';
import { Driver } from './driver.model';

export class ResourceAllocationFilter {
    constructor(
        private _cities: IdName[] = [],
        private _customers: IdName[] = [],
        private _divisions: IdName[] = [],
        private _contract_url: string = '',
    ) {}

    get cities(): IdName[] {
        return this._cities;
    }

    set cities(cities: IdName[]) {
        this._cities = cities;
    }

    get customers(): IdName[] {
        return this._customers;
    }

    set customers(customers: IdName[]) {
        this._customers = customers;
    }

    get divisions(): IdName[] {
        return this._divisions;
    }

    set divisions(divisions: IdName[]) {
        this._divisions = divisions;
    }

    get contract_url(): string {
        return this._contract_url;
    }

    set contract_url(val: string) {
        this._contract_url = val;
    }
}

export class Contract {
    constructor(
        private _id: number = 0,
        private _name: string = '',
        private _description: string = '',
        private _city_id: number = 0,
        private _active_drivers: number = 0,
        private _division: IdName,
        private _customer: IdName,
        private _customer_url: string,
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get description(): string {
        return this._description;
    }

    set description(description: string) {
        this._description = description;
    }

    get city_id(): number {
        return this._city_id;
    }

    set city_id(city_id: number) {
        this._city_id = city_id;
    }

    get active_drivers(): number {
        return this._active_drivers;
    }

    set active_drivers(active_drivers: number) {
        this._active_drivers = active_drivers;
    }

    get division(): IdName {
        return this._division;
    }

    set division(division: IdName) {
        this._division = division;
    }

    get customer(): IdName {
        return this._customer;
    }

    set customer(customer: IdName) {
        this._customer = customer;
    }

    get customer_url(): string {
        return this._customer_url;
    }

    set customer_url(val: string) {
        this._customer_url = val;
    }
}

export class Resource {
    constructor(
        private _id: number = 0,
        private _hub: string = '',
        private _unplanned_additional_drivers: number = 0,
        private _shift_start_time: string,
        private _drivers: Driver[],
        private _exotel_app_id: string = '',
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get hub(): string {
        return this._hub;
    }

    set hub(hub: string) {
        this._hub = hub;
    }

    get unplanned_additional_drivers(): number {
        return this._unplanned_additional_drivers;
    }

    set unplanned_additional_drivers(unplanned_additional_drivers: number) {
        this._unplanned_additional_drivers = unplanned_additional_drivers;
    }

    get shift_start_time(): string {
        return this._shift_start_time;
    }

    set shift_start_time(shift_start_time: string) {
        this._shift_start_time = shift_start_time;
    }

    get drivers(): Driver[] {
        return this._drivers;
    }

    set drivers(drivers: Driver[]) {
        this._drivers = drivers;
    }

    get exotel_app_id(): string {
        return this._exotel_app_id;
    }

    set exotel_app_id(val: string) {
        this._exotel_app_id = val;
    }

}

export class AllocationHistory {
    constructor(
        private _id: number = 0,
        private _event_time: string = '',
        private _modified_by: string = '',
        private _driver: string,
        private _action: string,
    ) {}

    get id(): number {
        return this._id;
    }

    set id(val: number) {
        this._id = val;
    }

    get event_time(): string {
        return this._event_time;
    }

    set event_time(val: string) {
        this._event_time = val;
    }

    get modified_by(): string {
        return this._modified_by;
    }

    set modified_by(val: string) {
        this._modified_by = val;
    }

    get driver(): string {
        return this._driver;
    }

    set driver(val: string) {
        this._driver = val;
    }

    get action(): string {
        return this._action;
    }

    set action(val: string) {
        this._action = val;
    }
}
