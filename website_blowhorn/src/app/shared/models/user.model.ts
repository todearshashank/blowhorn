export class User {

    constructor(
        private _first_name: string = '',
        private _last_name: string = '',
        private _line1: string = '',
        private _line2: string = '',
        private _line3: string = '',
        private _line4: string = '',
        private _phone_number: number = null,
        private _postcode: string = '',
        private _state: string = '',
        private _title: string = ''
    ) {}

    get first_name(): string {
        return this._first_name;
    }

    set first_name(first_name: string) {
        this._first_name = first_name;
    }

    get last_name(): string {
        return this._last_name;
    }

    set last_name(last_name: string) {
        this._last_name = last_name;
    }

    get line1(): string {
        return this._line1;
    }

    set line1(line1: string) {
        this._line1 = line1;
    }

    get line2(): string {
        return this._line2;
    }

    set line2(line2: string) {
        this._line2 = line2;
    }

    get line3(): string {
        return this._line3;
    }

    set line3(line3: string) {
        this._line3 = line3;
    }

    get line4(): string {
        return this._line4;
    }

    set line4(line4: string) {
        this._line4 = line4;
    }


    get phone_number(): number {
        return this._phone_number;
    }

    set phone_number(phone_number: number) {
        this._phone_number = phone_number;
    }

    get postcode(): string {
        return this._postcode;
    }

    set postcode(postcode: string) {
        this._postcode = postcode;
    }

    get state(): string {
        return this._state;
    }

    set state(state: string) {
        this._state = state;
    }

    get title(): string {
        return this._title;
    }

    set title(title: string) {
        this._title = title;
    }
}
