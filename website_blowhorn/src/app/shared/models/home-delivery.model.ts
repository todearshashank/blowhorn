export class HomeDelivery  {
    customer_name: string;
    customer_mobile: number;
    customer_email: string;
    delivery_address: string;
    delivery_postal_code: number;
    reference_number: number;
    delivery_lat: number;
    delivery_lon: number;
    pickup_address: string;
    pickup_postal_code: number;
    pickup_lat: number;
    pickup_lon: number;
    order_creation_time: Date;
    expected_delivery_time: Date;
    is_cod: boolean;
    cash_on_delivery: string;
    is_offline: boolean;
    item_details: ItemDetails[];
}

export class ItemDetails {
    item_code: any;
    item_sequence_no: any;
    uom: string;
    item_quantity: number;
    item_name: string;
    item_price_per_each: number;
    total_item_price: number;
}
