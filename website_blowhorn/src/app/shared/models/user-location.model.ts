import { Address } from './address.model';
import { Contact } from './contact.model';
import { GeoPoint } from './geopoint.model';

export class UserLocation {

  public static get_location_from_data(data): UserLocation {
    const location = new UserLocation();
    location.id = data.id || null;
    location.name = data.name;
    location.geopoint = new GeoPoint(data.geopoint.lat, data.geopoint.lng);
    location.contact = new Contact(data.contact.mobile, data.contact.name);
    location.address = Address.get_address_from_data(data);
    return location;
  }

  constructor(
    private _id: number = 0,
    private _address: Address = new Address(),
    private _contact: Contact = new Contact(),
    private _geopoint: GeoPoint = new GeoPoint(),
    private _name: string = '',
  ) {}

  public get id(): number {
    return this._id;
  }

  public set id(id: number) {
    this._id = id;
  }

  public get address(): Address {
    return this._address;
  }

  public set address(address: Address) {
    this._address = address;
  }

  public get contact(): Contact {
    return this._contact;
  }

  public set contact(contact: Contact) {
    this._contact = contact;
  }

  public get geopoint(): GeoPoint {
    return this._geopoint;
  }

  public set geopoint(geopoint: GeoPoint) {
    this._geopoint = geopoint;
  }

  public get name(): string {
    return this._name;
  }

  public set name(name: string) {
    this._name = name;
  }
}
