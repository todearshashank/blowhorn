export class NameValue {

    static get_name_value_from_data(data): NameValue {
        return new NameValue(data.name, data.value);
    }

    constructor(
        private _name: string = '',
        private _value: number = 0
    ) {}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get value(): number {
        return this._value;
    }

    set value(value: number) {
        this._value = value;
    }
}
