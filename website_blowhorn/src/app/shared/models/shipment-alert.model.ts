export class ShipmentAlertConfig {

    constructor(
        private _id: number = null,
        private _customer_id: number = null,
        private _critical_status: string = '',
        private _alert_colours: string = '',
        private _time_since_creation: number = 0,
    ) {}

    get id(): number {
        return this._id;
    }

    set id(val: number) {
        this._id = val;
    }

    get customer_id(): number {
        return this._customer_id;
    }

    set customer_id(val: number) {
        this._customer_id = val;
    }

    get critical_status(): string {
        return this._critical_status;
    }

    set critical_status(val: string) {
        this._critical_status = val;
    }

    get alert_colours(): string {
        return this._alert_colours;
    }

    set alert_colours(val: string) {
        this._alert_colours = val;
    }

    get time_since_creation(): number {
        return this._time_since_creation;
    }

    set time_since_creation(val: number) {
        this._time_since_creation = val;
    }
}
