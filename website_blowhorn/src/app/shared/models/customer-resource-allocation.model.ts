export interface CustomerResourceAllocation {
    id: string;
    hub: string;
    city: string;
    on_trip: boolean;
    route_name: string;
    driver_name: string;
    driver_number: string;
    vehicle_number: string;
    vehicle_model: string;
    shift_time: string;
}
