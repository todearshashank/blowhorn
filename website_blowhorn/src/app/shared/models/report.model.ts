export class SlotwiseData {
    constructor(
        private _status: string =null,
        private _key: string = null,
        private _value: number = null,
    ) {}

    get status(): string{
        return this._status;
    }
    set status(val:string){
        this._status= val;
    }
    get key(): string {
        return this._key;
    }

    set key(val: string) {
        this._key = val;
    }

    get value(): number {
        return this._value;
    }

    set value(val: number) {
        this._value = val;
    }
}
export class SlotwiseDataDelivered{
    private status:string;
    private key: string;
    private value: string;
}
export class SlotwiseDataInprogress{
    private status:string;
    private key: string;
    private value: string;
}

export class StatuswiseData{
    private status:string;
    private count: number;
}

export class filterDate{
    // constructor(
    //     private _status: string =null,
    //     private _key: string = null,
    //     private _value: number = null,
    // ) {}
}

export class OrderData{
    public max_order: any;
    public avg_completion_time: any;
    public time_range: any;
    public reattempted_orders: any;
    public  total_attempts:number;
    public first_attempt : number;
    public second_attempt: number;
    public third_attempt: number;
}

export class CODOrderData{
    public total_orders: any;
    public cash_collected: any;
}

export class HubOrderData {
    public total_orders:number;
    public shipment_lost: number;
    public shipmenthub: number;
}

export class ShipmentDelivery {
    public shipment_delivery: number;
}

// export class HubwiseData{
//     public hub_name: string;
//     public time_range: any;
//     public count: number;
// }