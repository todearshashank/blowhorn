import { Trip } from './trip.model';
import { Office } from './office.model';
import { User } from './user.model';
import { Customer } from './customer.model';

export class Invoice {
    constructor(
        private _trip: Trip = new Trip(),
        private _shipments: string[] = [],
        private _office: Office = new Office(),
        private _consignor: User = new User(),
        private _consignee: User = new User(),
        private _customer: Customer = new Customer(),
    ) {}

    public get trip(): Trip {
        return this._trip;
    }

    public set trip(trip: Trip) {
        this._trip = trip;
    }

    public get shipments(): string[] {
        return this._shipments;
    }

    public set shipments(shipments: string[]) {
        this._shipments = shipments;
    }

    public get office(): Office {
        return this._office;
    }

    public set office(office: Office) {
        this._office = office;
    }

    public get consignor(): User {
        return this._consignor;
    }

    public set consignor(consignor: User) {
        this._consignor = consignor;
    }

    public get consignee(): User {
        return this._consignee;
    }

    public set consignee(consignee: User) {
        this._consignee = consignee;
    }

    public get customer(): Customer {
        return this._customer;
    }

    public set customer(customer: Customer) {
        this._customer = customer;
    }
}
