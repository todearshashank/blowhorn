import { User } from './user.model';
import { OrderLine } from './shipment-sku.model';

export class Shipment {
    constructor(
        private _customer: User = new User(),
        private _tracking_id: string = '',
        private _payment_mode: string = '',
        private _company: string = '',
        private _status: string = '',
        private _message: string = '',
        private _vehicle_number: string = '',
        private _order_no: string = '',
        private _sku_details: any[] = [],
        private _delivery_instructions: string = '',
    ) {}

    get customer(): User {
        return this._customer;
    }

    set customer(customer: User) {
        this._customer = customer;
    }

    get tracking_id(): string {
        return this._tracking_id;
    }

    set tracking_id(tracking_id: string) {
        this._tracking_id = tracking_id;
    }

    get company(): string {
        return this._company;
    }

    set company(company: string) {
        this._company = company;
    }

    get payment_mode(): string {
        return this._payment_mode;
    }

    set payment_mode(payment_mode: string) {
        this._payment_mode = payment_mode;
    }

    get status(): string {
        return this._status;
    }

    set status(status: string) {
        this._status = status;
    }

    get message(): string {
        return this._message;
    }

    set message(message: string) {
        this._message = message;
    }

    get vehicle_number(): string {
        return this._vehicle_number;
    }

    set vehicle_number(val: string) {
        this._vehicle_number = val;
    }

    get order_no(): string {
        return this._order_no;
    }

    set order_no(order_no: string) {
        this._order_no = order_no;
    }

    get sku_details(): any[] {
        return this._sku_details;
    }

    set sku_details(val: any[]) {
        this._sku_details = val;
    }

    get delivery_instructions(): string {
        return this._delivery_instructions;
    }

    set delivery_instructions(val: string) {
        this._delivery_instructions = val;
    }
}
