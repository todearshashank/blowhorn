export class MemberGrid {
    constructor(
        private _count: number = null,
        private _next: string = '',
        private _previous: string = '',
        private _results: Member[] = null,
    ) {}

    get count(): number {
        return this._count;
    }

    set count(count: number) {
        this._count = count;
    }

    get next(): string {
        return this._next;
    }

    set next(next: string) {
        this._next = next;
    }

    get previous(): string {
        return this._previous;
    }

    set previous(previous: string) {
        this._previous = previous;
    }

    get results(): Member[] {
        return this._results;
    }

    set results(results: Member[]) {
        this._results = results;
    }
}

export class Member {
    constructor(
        private _id: number = null,
        private _name: string = '',
        private _email: string = '',
        private _phone_number: string = '',
        private _is_active: boolean = false,
        private _is_admin: boolean = false,
        private _status: string = 'invited',
        private _created: string = '',
        private _modified: string = '',
        private _is_invite_link_expired: boolean = false,
        private _roles: any[] = []
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get email(): string {
        return this._email;
    }

    set email(email: string) {
        this._email = email;
    }

    get phone_number(): string {
        return this._phone_number;
    }

    set phone_number(phone_number: string) {
        this._phone_number = phone_number;
    }

    get is_active(): boolean {
        return this._is_active;
    }

    set is_active(is_active: boolean) {
        this._is_active = is_active;
    }

    get is_admin(): boolean {
        return this._is_admin;
    }

    set is_admin(is_admin: boolean) {
        this._is_admin = is_admin;
    }

    get is_invite_link_expired(): boolean {
        return this._is_invite_link_expired;
    }

    set is_invite_link_expired(val: boolean) {
        this._is_invite_link_expired = val;
    }

    get status(): string {
        return this._status;
    }

    set status(status: string) {
        this._status = status;
    }

    get created(): string {
        return this._created;
    }

    set created(created: string) {
        this._created = created;
    }

    get modified(): string {
        return this._modified;
    }

    set modified(modified: string) {
        this._modified = modified;
    }

    get roles(): any[] {
        return this._roles;
    }

    set roles(val: any[]) {
        this._roles = val;
    }
}

export class Permission {
    constructor(
        private _id: number = 0,
        private _codename: string = '',
        private _name: string = '',
        private _module: number = 0,
        private _active: boolean = false,
    ) {}

    get id(): number {
        return this._id;
    }

    set id(val: number) {
        this._id = val;
    }

    get codename(): string {
        return this._codename;
    }

    set codename(val: string) {
        this._codename = val;
    }

    get name(): string {
        return this._name;
    }

    set name(val: string) {
        this._name = val;
    }

    get module(): number {
        return this._module;
    }

    set module(val: number) {
        this._module = val;
    }

    get active(): boolean {
        return this._active;
    }

    set active(val: boolean) {
        this._active = val;
    }

}

export class Group {
    constructor(
        private _id: number = 0,
        private _name: string = '',
        private _permissions: Permission[] = [],
        private _permission_levels: any[] = [],
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(val: string) {
        this._name = val;
    }

    get permissions(): Permission[] {
        return this._permissions;
    }

    set permissions(val: Permission[]) {
        this._permissions = val;
    }

    get permission_levels(): any[] {
        return this._permission_levels;
    }

    set permission_levels(val: any[]) {
        this._permission_levels = val;
    }

}

export class System {
    constructor(
        private _id: number = 0,
        private _name: string = '',
        private _code: string = '',
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(val: string) {
        this._name = val;
    }

    get code(): string {
        return this._code;
    }

    set code(val: string) {
        this._code = val;
    }
}

export class Module {
    constructor(
        private _id: number = 0,
        private _name: string = '',
        private _code: string = '',
        private _system: number = 0,
        private _all: boolean = false,
        private _permissions: Permission[] = [],
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get name(): string {
        return this._name;
    }

    set name(val: string) {
        this._name = val;
    }

    get code(): string {
        return this._code;
    }

    set code(val: string) {
        this._code = val;
    }

    get system(): number {
        return this._system;
    }

    set system(val: number) {
        this._system = val;
    }

    get all(): boolean {
        return this._all;
    }

    set all(val: boolean) {
        this._all = val;
    }

    get permissions(): Permission[] {
        return this._permissions;
    }

    set permissions(val: Permission[]) {
        this._permissions = val;
    }
}

export class SupportData {
    constructor(
        private _systems: System[] = [],
        private _modules: Module[] = [],
        private _groups: Group[] = [],
        private _permissions: Permission[] = [],
    ) {}

    get systems(): System[] {
        return this._systems;
    }

    set systems(val: System[]) {
        this._systems = val;
    }

    get modules(): Module[] {
        return this._modules;
    }

    set modules(val: Module[]) {
        this._modules = val;
    }

    get groups(): Group[] {
        return this._groups;
    }

    set groups(val: Group[]) {
        this._groups = val;
    }

    get permissions(): Permission[] {
        return this._permissions;
    }

    set permissions(val: Permission[]) {
        this._permissions = val;
    }
}
