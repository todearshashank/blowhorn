import { Base } from './base.model';
import { LocationStop } from './location.model';
import { Contact } from './contact.model';

export class Pickup extends Base {

  static get_pickup_from_data(data): Pickup {
    const pickup = new Pickup();
    Object.keys(data).forEach(key => {
      switch (key) {
        case 'location':
          pickup[key] = LocationStop.get_location_from_data(data[key]);
          break;
        case 'contact':
          pickup[key] = Contact.get_contact_from_data(data[key]);
          break;
        default:
          pickup[key] = data[key];
      }
    });
    return pickup;
  }

  constructor(
    _status: string = 'done',
    _icon: string = '',
    _estimated_distance: number = 0,
    _distance: number = 0,
    _travel_time: string = '',
    _name: string = '',
    _address: string = '',
    _wait_duration: string = '',
    _location: LocationStop = new LocationStop(),
    _time: string = '',
    _date: string = '',
    _contact: Contact = new Contact(),
  ) {
    super(_status, _icon, _estimated_distance, _distance, _travel_time, _name, _address, _wait_duration, _location, _time, _date, _contact);
  }
}
