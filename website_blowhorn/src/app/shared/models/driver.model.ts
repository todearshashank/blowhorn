export class Driver {

    constructor(
        private _id: number = null,
        private _driver_id: number = null,
        private _name: string = '',
        private _city: number = null,
        private _hub: number = null,
        private _vehicle_class = null,
        private _status: string = '',
        private _timestamp_arrived_at_pickup: string = '',
        private _last_updated_location: number[] = [],
        private _mobile: number = null,
        private _order_number: string = '',
        private _trip_number: string = '',
        private _vehicle_number: string = '',
        private _vehicle_model: string = '',
        private _licence_number: string = '',
        private _login_time: string = '',
        private _avatar_url: string = '',
        private _truck_icon: string = '',
    ) {}

    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }

    get driver_id(): number {
        return this._driver_id;
    }

    set driver_id(driver_id: number) {
        this._driver_id = driver_id;
    }

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get city(): number {
        return this._city;
    }

    set city(city: number) {
        this._city = city;
    }

    get hub(): number {
        return this._hub;
    }

    set hub(hub: number) {
        this._hub = hub;
    }

    get vehicle_class(): number {
        return this._vehicle_class;
    }

    set vehicle_class(vehicle_class: number) {
        this._vehicle_class = vehicle_class;
    }

    get status(): string {
        return this._status;
    }

    set status(status: string) {
        this._status = status;
    }

    get timestamp_arrived_at_pickup(): string {
        return this._timestamp_arrived_at_pickup;
    }

    set timestamp_arrived_at_pickup(timestamp_arrived_at_pickup: string) {
        this._timestamp_arrived_at_pickup = timestamp_arrived_at_pickup;
    }

    get last_updated_location(): number[] {
        return this._last_updated_location;
    }

    set last_updated_location(last_updated_location: number[]) {
        this._last_updated_location = last_updated_location;
    }

    get mobile(): number {
        return this._mobile;
    }

    set mobile(mobile: number) {
        this._mobile = mobile;
    }

    get order_number(): string {
        return this._order_number;
    }

    set order_number(order_number: string) {
        this._order_number = order_number;
    }

    get trip_number(): string {
        return this._trip_number;
    }

    set trip_number(trip_number: string) {
        this._trip_number = trip_number;
    }

    get vehicle_number(): string {
        return this._vehicle_number;
    }

    set vehicle_number(vehicle_number: string) {
        this._vehicle_number = vehicle_number;
    }

    get vehicle_model(): string {
        return this._vehicle_model;
    }

    set vehicle_model(vehicle_model: string) {
        this._vehicle_model = vehicle_model;
    }

    get licence_number(): string {
        return this._licence_number;
    }

    set licence_number(licence_number: string) {
        this._licence_number = licence_number;
    }

    get login_time(): string {
        return this._login_time;
    }

    set login_time(login_time: string) {
        this._login_time = login_time;
    }

    get avatar_url(): string {
        return this._avatar_url;
    }

    set avatar_url(avatar_url: string) {
        this._avatar_url = avatar_url;
    }

    get truck_icon(): string {
        return this._truck_icon;
    }

    set truck_icon(truck_icon: string) {
        this._truck_icon = truck_icon;
    }
}
