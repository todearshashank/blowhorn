import { Component, OnInit, Input } from '@angular/core';
import { BlowhornService } from '../../services/blowhorn.service';

@Component({
  selector: 'app-isd-code',
  templateUrl: './isd-code.component.html',
  styleUrls: ['./isd-code.component.scss']
})
export class IsdCodeComponent implements OnInit {

  isdDialingCode: string = '';
  @Input() styles = {
    wrapper: {},
    text: {}
  };

  constructor(
    private bhService: BlowhornService,
  ) { }

  ngOnInit() {
    this.isdDialingCode = this.bhService.isd_dialing_code;
  }

}
