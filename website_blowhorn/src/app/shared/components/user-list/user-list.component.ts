import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { BlowhornService } from '../../services/blowhorn.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
  ]
})
export class UserListComponent implements OnInit {

  @Output() setUsersList = new EventEmitter<string[]>();
  public users: string[] = [];
  private user: string;
  public showAddForm = false;
  error_message = '';
  cancel_src = Constants.images.cancel;
  add_stop_src = Constants.images.add_stop;
  constructor(
    public bh_service: BlowhornService
  ) {}

  ngOnInit() {}

  removeUser(i) {
    this.users.splice(i, 1);
  }

  addUser() {
    if (!this.user) {
      this.error_message = 'Please, enter an email id';
      return;
    }
    if (this.users.indexOf(this.user) !== -1) {
      this.error_message = `${this.user} already present`;
      return;
    }
    this.users.push(this.user);
    this.setUsersList.emit(this.users);
    this.showAddForm = false;
    this.user = "";
  }

}
