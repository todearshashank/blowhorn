import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-nav-link',
  templateUrl: './nav-link.component.html',
  styleUrls: ['./nav-link.component.scss']
})
export class NavLinkComponent implements OnInit {

  @Input('router_link')
  router_link = 'Link';
  @Input('link_text')
  link_text = 'Link';
  @Output() active_link_text: EventEmitter<string>;
  constructor() {
    this.active_link_text = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  route_clicked(event) {
    this.active_link_text.next(this.link_text);
  }

}
