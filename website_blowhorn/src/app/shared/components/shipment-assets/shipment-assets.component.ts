import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ShipmentService } from '../../services/shipment.service';

@Component({
  selector: 'app-shipment-assets',
  templateUrl: './shipment-assets.component.html',
  styleUrls: ['./shipment-assets.component.scss']
})
export class ShipmentAssetsComponent implements OnInit {

  @Input()
  number = '';
  @Input()
  field = '';
  @Input()
  heading = '';
  @Input()
  styles = {
    wrapper: {}
  };
  @Output()
  close_fired: EventEmitter<boolean>;
  show = {
    assets: false,
    loader: false
  };
  assets = [];

  constructor(
      private shipment_service: ShipmentService,
  ) {
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.assets = [];
    this.show.loader = true;
    this.show.assets = true;
    this.shipment_service.get_assets(this.field, this.number)
    .subscribe(data => {
        this.assets = data;
        this.show.loader = false;
    },
    err => {
        console.error(err);
        this.show.loader = false;
    });
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
      this.close_fired.emit(true);
  }

}
