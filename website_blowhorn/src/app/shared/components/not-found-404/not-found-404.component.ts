import { Component, OnInit } from '@angular/core';
import { BlowhornService } from '../../services/blowhorn.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-not-found-404',
  templateUrl: './not-found-404.component.html',
  styleUrls: ['./not-found-404.component.scss']
})
export class NotFound404Component implements OnInit {

  home_src = Constants.images.home;
  constructor(
    public bh_service: BlowhornService
  ) { }

  ngOnInit() {
  }

}
