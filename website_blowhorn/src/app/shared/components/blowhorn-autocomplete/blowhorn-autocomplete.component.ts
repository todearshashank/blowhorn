import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-autocomplete',
  templateUrl: './blowhorn-autocomplete.component.html',
  styleUrls: ['./blowhorn-autocomplete.component.scss']
})
export class BlowhornAutocompleteComponent implements OnInit {

  @Input()
  placeholder = 'Placeholder';
  @Input()
  items = [];
  @Input()
  display_property = '';
  @Output()
  item_selected: EventEmitter<{}>;
  is_focus = false;
  input_model: string;
  filtered_items = [];
  previous_input = '';

  constructor() {
    this.item_selected = new EventEmitter<{}>();
  }

  ngOnInit() {
    this.filtered_items = this.items;
  }

  reset_input() {
    this.input_model = this.previous_input;
  }

  set_input(event, item) {
    this.previous_input = this.input_model;
    this.input_model = this.display_property ? item[this.display_property] : item;
  }

  set_input_and_emit_item(event, item) {
    this.set_input(event, item);
    this.input_model = this.display_property ? item[this.display_property] : item;
    this.item_selected.emit(item);
  }

  filter_items() {
    if (this.input_model) {
      this.filtered_items = this.items.filter(item => {
        const filter = this.display_property ? item[this.display_property] : item;
        return filter.includes(this.input_model);
      });
    } else {
      this.filtered_items = this.items;
    }
  }

  check_item() {
    this.is_focus = false;
    this.filtered_items = this.items.filter(item => {
      const filter = this.display_property ? item[this.display_property] : item;
      return filter.includes(this.input_model);
    });
    if (this.filtered_items.length === 1) {
      this.item_selected.emit(this.filtered_items[0]);
    } else {
      this.item_selected.emit(null);
    }
  }

  show_list_and_filter() {
    this.is_focus = true;
    this.filter_items();
  }

}
