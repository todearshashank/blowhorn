import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mini-loader',
  templateUrl: './mini-loader.component.html',
  styleUrls: ['./mini-loader.component.scss']
})
export class MiniLoaderComponent implements OnInit {

    @Input()
    styles = {
      wrapper: {},
      ball1: {},
      ball2: {},
      ball3: {},
      spinner: {}
    };
    @Input()
    font_awesome_spinner = '';

  constructor() { }

  ngOnInit() {
  }

}
