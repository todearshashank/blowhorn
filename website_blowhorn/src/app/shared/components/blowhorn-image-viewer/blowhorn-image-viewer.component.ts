import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-image-viewer',
  templateUrl: './blowhorn-image-viewer.component.html',
  styleUrls: ['./blowhorn-image-viewer.component.scss']
})
export class BlowhornImageViewerComponent implements OnInit {

  @ViewChild('image', {static: false})
  image_ref: ElementRef;
  @Input()
  document_links: any[];
  @Input()
  styles = {
    wrapper: {}
  };
  @Output()
  close_fired: EventEmitter<boolean>;
  current_index = 0;
  currentItem: any;
  footer_buttons: {
    print: {
      label: 'print',
      type: 'save'
    },
    download: {
      label: 'download',
      type: 'save'
    },
};

  constructor() {
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.currentItem = this.document_links[this.current_index];
  }

  next(event: any) {
    this.current_index++;
    this.currentItem = this.document_links[this.current_index];
    this.current_index = this.current_index % this.document_links.length;
    this.supress_event(event);
  }

  supress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  previous(event: any) {
    this.current_index -= 1;
    if (this.current_index < 0) {
      this.current_index = this.document_links.length - 1;
    }
    this.currentItem = this.document_links[this.current_index];
    this.current_index = this.current_index % this.document_links.length;
    this.supress_event(event);
  }

  download_image(event: any, url: string) {
    window.open(url, '_blank');
  }

  close_image_viewer(event: any) {
    if (event instanceof KeyboardEvent && event.keyCode !== 27) {
      return;
    }
    this.close_fired.emit(true);
  }

  download_document(): void {
    window.open(this.currentItem.url, '_blank');
  }

  print_document(): void {
  }

  setPlaceholder(item: any) {
    item.message = `Click download to view file.`;
  }

}
