import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Shipment } from '../../models/shipments-grid.model';
import { ShipmentService } from '../../services/shipment.service';
import { AdditionalEntityDetails } from '../../models/assets.model';
import { BlowhornService } from '../../services/blowhorn.service';

@Component({
  selector: 'app-shipment-sku',
  templateUrl: './shipment-sku.component.html',
  styleUrls: ['./shipment-sku.component.scss']
})
export class ShipmentSkuComponent implements OnInit {

  @Input()
  heading = '';
  @Input()
  styles = {
    wrapper: {}
  };
  @Input()
  show_order_details = true;
  @Input()
  show_order_number = false;
  @Output()
  close_fired: EventEmitter<boolean>;
  status_new = 'New-Order';
  total_sku_cost = 0;
  details: AdditionalEntityDetails;

  constructor(
      private shipment_service: ShipmentService,
      public bh_service: BlowhornService,
  ) {
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.shipment_service.additional_details.subscribe(val => {
        this.details = val;
    });
  }

  update_total_sku_cost(val: Shipment): void {
    this.total_sku_cost = 0;
    if (val.orderlines != null && val.orderlines.length > 0) {
        val.orderlines.forEach(x => this.total_sku_cost += parseFloat(x.total_item_price));
    }
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
    this.close_fired.emit(true);
  }

}
