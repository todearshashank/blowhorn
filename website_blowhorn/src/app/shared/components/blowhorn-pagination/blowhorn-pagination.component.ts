import { Component, OnInit, ElementRef, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WatcherService } from '../../services/watcher.service';
import { BlowhornService } from '../../services/blowhorn.service';
import { Router, UrlSegmentGroup } from '@angular/router';

@Component({
  selector: 'app-blowhorn-pagination',
  templateUrl: './blowhorn-pagination.component.html',
  styleUrls: ['./blowhorn-pagination.component.scss']
})
export class BlowhornPaginationComponent implements OnInit {

  @ViewChild('search_page', {static: false})
  search_input: ElementRef;
  current_page = 1;
  @Input()
  count = 0;
  @Input()
  page_size = 50;
  @Input()
  previous = '';
  @Input()
  next = '';
  show_page_search = false;
  @Output()
  data_fetched: EventEmitter<{}>;

  constructor(
    private _http_client: HttpClient,
    private _watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private _router: Router,
  ) {
    this.data_fetched = new EventEmitter<{}>();
  }

  ngOnInit() {
  }

  fetch_first() {
    this.current_page = 1;
    this.fetch_data(this.get_page_url(1));
  }

  fetch_previous() {
    this.current_page--;
    this.fetch_data(this.previous);
  }

  show_current_page_search(event) {
    const el = this.search_input.nativeElement;
    this.show_page_search = true;
    el.focus();
  }

  hide_current_page_search(event) {
    const el = this.search_input.nativeElement;
    el.value = this.current_page;
    this.show_page_search = false;
  }

  fetch_current(event) {
    const value = event.target.value;
    const key = event.key;
    const code = event.code;
    const key_code = event.keyCode;
    if (key_code === 13 ) {
      if (parseInt(value, 10) >= 1 || parseInt(value, 10) <= this.last_page) {
        this.current_page = parseInt(event.target.value, 10);
        this.fetch_data(this.get_page_url(this.current_page));
      }
      this.search_input.nativeElement.blur();
      this.show_page_search = false;
      return;
    }
    if ((key_code < 37 && key_code !== 8 && key_code !== 9) || (key_code > 40 && key_code < 48 && key_code !== 46) || key_code > 57 ||
      ((key_code >= 48 && key_code <= 57) && isNaN(value + key) || parseInt(value + key, 10) < 1 ||
      parseInt(value + key, 10) > this.last_page)) {
      event.preventDefault();
      return;
    }
  }

  fetch_next() {
    this.current_page++;
    this.fetch_data(this.next);
  }

  fetch_last() {
    this.current_page = this.last_page;
    this.fetch_data(this.get_page_url(this.last_page));
  }

  get last_page(): number {
    return this.count ? Math.ceil(this.count / this.page_size) : 1;
  }

  fetch_data(url: string) {
    const url_frags = url.split('://')[1].split('/');
    url_frags.splice(0, 1);
    url =  url_frags.join('/');
    this._blowhorn_service.show_loader();
    this._http_client
      .get(url)
      .subscribe((data: {count: number, next: string, previous: string, results: any[]}) => {
        this.count = data.count;
        this.next = data.next;
        this.previous = data.previous;
        this.data_fetched.emit(data);
        this._blowhorn_service.hide_loader();
      },
      err => {
        console.error(err);
        this._blowhorn_service.hide_loader();
      });
  }

  get_page_url(page_no: number): string {
    const url = this.next || this.previous;
    const url_frags = url.split('?');
    let url_params = url_frags.pop().split('&');
    url_params = url_params.map(item => {
      if (item.startsWith('page=')) {
        return `page=${page_no}`;
      }
      return item;
    });
    return url_frags.shift() + '?' + url_params.join('&');
  }

}
