import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-button',
  templateUrl: './blowhorn-button.component.html',
  styleUrls: ['./blowhorn-button.component.scss']
})
export class BlowhornButtonComponent implements OnInit {

  @Input()
  label: string;
  @Input()
  disabled = false;
  @Input()
  type: string;
  @Input()
  footer_btn = false;
  @Input()
  flat_btn = false;
  @Input()
  styles = {
    wrapper: {},
    icon_holder: {},
    icon: {},
    label: {},
  };
  @Input()
  icon = {
      src: '',
      class: ''
  };
  @Output()
  click_fired: EventEmitter<boolean>;

  constructor() {
    this.click_fired  = new EventEmitter<boolean>(false);
  }

  ngOnInit() {
  }

  clicked(event: any) {
    if (!this.disabled) {
      this.click_fired.next(true);
    }
  }

}
