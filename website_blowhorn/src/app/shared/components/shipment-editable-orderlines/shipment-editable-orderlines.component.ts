import { Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import { Shipment } from '../../models/shipments-grid.model';
import { ShipmentService } from '../../services/shipment.service';
import { AdditionalEntityDetails } from '../../models/assets.model';
import { OrderLine } from '../../models/shipment-sku.model';
import { BlowhornService } from '../../services/blowhorn.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shipment-editable-orderlines',
  templateUrl: './shipment-editable-orderlines.component.html',
  styleUrls: ['./shipment-editable-orderlines.component.scss']
})

export class ShipmentEditableOrderlinesComponent implements OnInit {

  displayedColumns: string[] = [
    'select',
    'sku',
    'hsn_code',
    'quantity', 
    'each_item_price',
    'cgst',
    'sgst',
    'igst',
    'discount_type',
    'discounts',
    'volume',
    'weight',
    'uom'
  ];
  
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @Input()
  heading = '';
  @Input()
  styles = {
    wrapper: {}
  };
  @Input()
  show_order_details = true;
  @Input()
  show_order_number = false;
  @Output()
  close_fired: EventEmitter<boolean>;
  status_new = 'New-Order';
  total_sku_cost = 0;
  validationError = null;
  validationSuccess = null;
  details: AdditionalEntityDetails;
  validations_form: any;
  dataSource: any;
  selectedIds = [];
  uomList = [];

  selection = new SelectionModel<OrderLine>(true, []);
  order_number: string = null;
  panelOpenState = false;
  show_all_buttons = false;
  discountTypes = [{
    label: '--',
    value: ''
  }, {
    label: 'Fixed',
    value: 'fixed'
  }, {
    label: 'Percentage',
    value: 'percentage'
  }]

  newOrderLine = {
    cgst: 0,
    sgst: 0,
    igst: 0,
    item_name: '',
    item_quantity: 0,
    hsn_code: '',
    item_price_per_each: 0,
    total_item_price: '',
    discount_type: '',
    discounts: 0,
    length: 0,
    breadth: 0,
    height: 0,
    volume: 0,
    weight: 0,
    uom: 'kg'
  }

  constructor(
    private shipment_service: ShipmentService,
    public bh_service: BlowhornService,
    private toastr: ToastrService,
  ) { 
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.selectedIds = [];
    this.dataSource = new MatTableDataSource<OrderLine>([]);;
    this.shipment_service.additional_details.subscribe(val => {
      this.details = val;
      this.order_number = this.details.number;
      this.show_all_buttons = this.details.is_valid_lineedit_status;
      this.shipment_service
        .get_dashboard_orderlines(this.order_number)
        .subscribe(resp => {
          this.dataSource = new MatTableDataSource<OrderLine>(resp.item_details);
          this.uomList = resp.uoms;
          this.dataSource.paginator = this.paginator;        
      }, err => {
          console.error(err);
      });
    });
  }

  updateData() {
    let inputdata = []
    let is_price_valid = true;
    for (let line of this.dataSource.data) {
      if ((line['quantity'] <= 0) || (line['each_item_price'] <= 0)){
        is_price_valid = false;
        break;
      }
      line['item_quantity'] = line['quantity']
      line['item_name'] = line['sku']
      line['item_price_per_each'] = line['each_item_price']
      line['total_item_price'] = line['quantity'] * line['each_item_price'];
      inputdata.push(line)
    }

    if (!is_price_valid) {
      this.toastr.error('Item price & quantity should be a non zero value');
      return
    }
    let data = {
      option : 'update',
      order_number : this.order_number,
      item_details : inputdata
    }
    this.shipment_service.manage_orderlines(data).subscribe(response => {
      let itemarray = response['message'].item_details
      let itemlist: OrderLine[] = Object.values(itemarray)
      this.dataSource = new MatTableDataSource<OrderLine>(itemlist);
      this.dataSource.paginator = this.paginator;
      this.toastr.success('Items have been updated successfully');
    }, err => {
      console.log(err);
      this.toastr.error(err.error.message);
    });
  }

  deleteData() {
    if (this.selection.selected.length === 0) {
      this.toastr.error('No data selected for deletion');
      return;
    }
    let data = {
      option : 'delete',
      order_number : this.order_number,
      item_details : this.selection.selected.map(i => i.id)
    }
    this.shipment_service.manage_orderlines(data).subscribe(response => {
      this.selection.selected.forEach(item => {
        let index: number = this.dataSource.data.findIndex((d: OrderLine) => d === item);
        this.dataSource.data.splice(index, 1)
        this.dataSource = new MatTableDataSource<OrderLine>(this.dataSource.data);
        this.dataSource.paginator = this.paginator;
      });
      this.selection = new SelectionModel<OrderLine>(true, []);
      this.toastr.success('Items have been deleted successfully');
    }, err => {
      console.log(err);
      this.toastr.error(err.error.message);
    });
  }

  addRow() {
    this.validationError = null;
    this.validationSuccess = null;
    if (!this.newOrderLine.item_name || !this.newOrderLine.item_quantity || !this.newOrderLine.hsn_code || !this.newOrderLine.item_price_per_each) {
      this.validationError = 'Please enter all the required data';
      return false;
    }

    this.newOrderLine.total_item_price = (this.newOrderLine.item_quantity * this.newOrderLine.item_price_per_each).toFixed(2)
    const data = {
      option : 'add',
      order_number : this.order_number,
      item_details : [this.newOrderLine,]
    }
      
    this.shipment_service.manage_orderlines(data).subscribe(response => {
      let details = response['message'].item_details;
      let new_data = Object.values(details);
      this.dataSource.data = this.dataSource.data.concat(new_data)
      
      // make the fields blank
      this.newOrderLine.cgst = 0
      this.newOrderLine.sgst = 0
      this.newOrderLine.igst = 0
      this.newOrderLine.item_name = ''
      this.newOrderLine.item_quantity = 0
      this.newOrderLine.hsn_code = ''
      this.newOrderLine.item_price_per_each = 0
      this.newOrderLine.total_item_price = ''
      this.newOrderLine.uom = 'kg'
      this.newOrderLine.weight = 0;
      this.newOrderLine.volume = 0;
      this.newOrderLine.discount_type = '';
      this.newOrderLine.discounts = 0;
      this.dataSource = new MatTableDataSource<OrderLine>(this.dataSource.data);
      this.dataSource.paginator = this.paginator;
      this.toastr.success('Item has been added successfully');
    }, err =>  {
      console.log(err);
      this.toastr.error(err.error.message);
    });
  }

  
  editElement(element: any) {
    if (this.show_all_buttons) {
      element.disabled = false;
      for (let item of this.dataSource.data) {
        item.disabled = item.id === element.id
      }
    }
  }

  update_total_sku_cost(val: Shipment): void {
    this.total_sku_cost = 0;
    if (val.orderlines != null && val.orderlines.length > 0) {
      val.orderlines.forEach(x => this.total_sku_cost += parseFloat(x.total_item_price));
    }
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
    this.close_fired.emit(true);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.selectedIds = [];
      this.dataSource.data.forEach((row: OrderLine) => {
        this.selection.select(row);
        this.selectedIds.push(row.id);
      });
  }

}
