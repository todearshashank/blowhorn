import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import htmlToImage from 'html-to-image';


@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.component.html',
  styleUrls: ['./barcode.component.scss']
})
export class BarcodeComponent implements OnInit {

    @Input()
    format = 'CODE128';
    @Input()
    value = '';
    @Input()
    show_value = true;
    @Input()
    heading = '';
    @Input()
    height = 80;
    @Input()
    filename = this.value;
    @Output()
    close_fired: EventEmitter<boolean>;
    disable = {
      download: false,
      print: false
    };

  constructor() {
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
      this.close_fired.emit(true);
  }

  download_barcode(): void {
    this.disable.download = true;
    let elem = document.getElementById('barcode-container');
    let filename = this.filename;
    htmlToImage.toPng(elem, { quality: 1.0 })
    .then(function (dataUrl) {
        let link = document.createElement('a');
        link.download = `barcode_${filename}`;
        link.href = dataUrl;
        link.click();
        this.disable.download = false;
    });
  }

}
