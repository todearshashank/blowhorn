import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Constants } from '../../utils/constants';

@Component({
    selector: 'app-blowhorn-confirm',
    templateUrl: './blowhorn-confirm.component.html',
    styleUrls: ['./blowhorn-confirm.component.scss']
})
export class BlowhornConfirmComponent implements OnInit {

    @Input()
    heading: string;
    @Input()
    details: string;
    @Input()
    show_warning: boolean;
    @Input()
    show_footer = true;
    @Input()
    action_buttons = {
        decline: {
            show: true,
            type: '',
            label: 'No'
        },
        confirm: {
            show: true,
            type: '',
            label: 'Yes'
        },
    };
    @Input()
    styles = {
        header_icon: {
            img: ''
        },
        header: {},
        details: {},
        footer: {}
    };
    @Input()
    header_icon_url = '';
    @Input()
    show_loader = false;
    @Output()
    proceed: EventEmitter<boolean>;
    constants = Constants;
    constructor() {
        this.proceed = new EventEmitter<boolean>();
    }

    ngOnInit() {
    }

}
