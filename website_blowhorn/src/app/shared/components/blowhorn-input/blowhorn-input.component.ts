import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-input',
  templateUrl: './blowhorn-input.component.html',
  styleUrls: ['./blowhorn-input.component.scss']
})
export class BlowhornInputComponent implements OnInit {

    @Input('input_model') input_model = null;
    @Input('form_control') form_control = null;
    @Input('type') type = 'text';
    @Input('name') name = '';
    @Input('placeholder') placeholder = '';
    @Input('style') style = '';
    @Input('disabled') disabled = false;
    @Input('readonly') readonly = false;
    @Input('required') required = false;
    @Input('enable_focus_listener') enable_focus_listener = false;
    @Input('enable_blur_listener') enable_blur_listener = false;
    @Input('enable_keyup_listener') enable_keyup_listener = false;
    @Output() on_focus: EventEmitter<any>;
    @Output() on_blur: EventEmitter<any>;
    @Output() on_keyup: EventEmitter<any>;

    constructor() {
        this.on_focus = new EventEmitter<any>();
        this.on_blur = new EventEmitter<any>();
        this.on_keyup = new EventEmitter<any>();
    }

    ngOnInit() {
    }

    onFocus(): void {
        if (!this.enable_focus_listener) {return; }
        this.on_focus.emit(this.input_model);
    }

    onBlur(): void {
        if (!this.enable_blur_listener) {return; }
        this.on_blur.emit(this.input_model);
    }

    onKeyup(): void {
        if (!this.enable_keyup_listener) {return; }
        this.on_keyup.emit(this.input_model);
    }

}
