import { Profile } from './../../models/profile.model';
import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { BlowhornService } from '../../services/blowhorn.service';

@Component({
  selector: 'app-dropdown-user',
  templateUrl: './dropdown-user.component.html',
  styleUrls: ['./dropdown-user.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
  ]
})
export class DropdownUserComponent implements OnInit {
  @Input()
  styles: {} = {
    'dropdown_styles': {},
    'dropdown-toggle-styles': {},
    'dropdown-toggle-display-styles': {},
    'dropdown-menu-styles': {},
    'dropdown-item-styles': {}
  };
  @Input()
  username: String = 'Blowhorn User';
  min_loader_styles = {};
  userProfile: Profile = new Profile();
  showProfile = false;
  profileUpdateRequired = false
  submittingProfile = false;
  successMessage = null;
  errorMessage = null;
  error = null;
  isUpdateSuccess = false;
  showChangePassword = false;
  pwdErrorMessage = null;
  pwdUpdateSuccess = false;
  pwdSubmitting = false;
  data = [];
  pwdData = {
    oldpassword: '',
    password1: '',
    password2: '',
  };
  isdCodeStyles = {
    wrapper: {
      height: '38px',
      padding: 0
    },
    text: {
      'line-height': '36px',
    }
  }
  deleteBtnStyles = {
      wrapper: {
        background: '#c90303',
        color: '#fff'
    }
  }

  constructor(
    public blowhorn_service: BlowhornService
  ) {
    console.log('this.userProfile: ', this.userProfile);
    this.blowhorn_service.user_profile_obs.subscribe(
      val => {
        if (val) {
          this.userProfile = val;
          if (!this.userProfile.name || !this.userProfile.mobile || !this.userProfile.email) {
            this.profileUpdateRequired = true;
            this.error = 'Please update your profile';
            setTimeout(() => {
              this.error = null;
            }, 2000);
            this.showProfile = true;
            this.isUpdateSuccess = false;
          }
        }
      },
      err => {
        console.error(err);
      }
    );
  }

  ngOnInit() {}

  openProfile() {
    this.errorMessage = null;
    this.userProfile = this.blowhorn_service.user_profile;
    this.showProfile = true;
    this.isUpdateSuccess = false;
  }

  openChangePassword() {
    this.pwdData = {
      oldpassword: '',
      password1: '',
      password2: '',
    };
    this.pwdErrorMessage = null;
    this.pwdUpdateSuccess = false;
    this.showChangePassword = true;
  }

  updateProfile() {
    this.submittingProfile = true;
    this.blowhorn_service.update_profile(this.userProfile)
    .subscribe(data => {
      this.blowhorn_service.user_profile = this.userProfile;
      this.submittingProfile = false;
      this.isUpdateSuccess = true;
      this.profileUpdateRequired = false;
      this.successMessage = 'Profile updated successfully'
      this.error = null;
      setTimeout(() => {
        this.isUpdateSuccess = false;
      }, 2000);
    }, err => {
      console.error(err);
      this.submittingProfile = false;
      this.errorMessage = err.error;
      setTimeout(() => {
        this.errorMessage = null;
      }, 2000);
    });
  }

  updatePassword() {
    this.pwdSubmitting = true;
    let data = `oldpassword=${this.pwdData.oldpassword}&password1=${this.pwdData.password1}&password2=${this.pwdData.password2}`;
    this.blowhorn_service.update_password(data)
        .subscribe(data => {
            this.pwdSubmitting = false;
            this.pwdUpdateSuccess = true;
            setTimeout(() => {
                this.pwdUpdateSuccess = false;
            }, 2000);
        }, err => {
            console.error('error', err);
            this.pwdSubmitting = false;
            this.pwdErrorMessage = err.error.message;
        });
  }

  deleteRequest() {
    this.submittingProfile = true;
    this.blowhorn_service.delete_data_request()
        .subscribe(data => {
            this.submittingProfile = false;
            this.isUpdateSuccess = true;
            this.successMessage = 'Your request for deleting your data has been received.'
            setTimeout(() => {
                this.isUpdateSuccess = false;
            }, 2000);
        }, err => {
            console.error('error', err);
            this.submittingProfile = false;
            this.errorMessage = err.error.message;
        });
  }

  logoutUser() {
    this.blowhorn_service.delete_token();
    setTimeout(() => window.open('/accounts/logout', '_self'), 400)
  }

}
