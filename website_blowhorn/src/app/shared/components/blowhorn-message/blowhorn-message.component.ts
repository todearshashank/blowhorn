import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-message',
  templateUrl: './blowhorn-message.component.html',
  styleUrls: ['./blowhorn-message.component.scss']
})
export class BlowhornMessageComponent implements OnInit {

  @Input()
  heading: string;
  @Input()
  message: string;
  @Input()
  type = 'success';
  @Input()
  error_list = [];
  @Input()
  styles = {
    wrapper: {},
    heading: {},
    message: {}
  };
  @Output()
  close_fired: EventEmitter<boolean>;
  constructor() {
    this.close_fired = new EventEmitter<boolean>(false);
  }

  ngOnInit() {
      console.log('error_list', this.error_list);
  }

  close() {
    this.close_fired.next(true);
  }

}
