import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-yesno',
  templateUrl: './blowhorn-yesno.component.html',
  styleUrls: ['./blowhorn-yesno.component.scss']
})
export class BlowhornYesnoComponent implements OnInit {

  @Input()
  label = 'label';
  @Input()
  active = false;
  @Input()
  justify: string;
  @Output()
  is_active: EventEmitter<boolean>;

  constructor() {
    this.is_active = new EventEmitter<boolean>(false);
  }

  ngOnInit() {
  }

  active_change() {
    this.active = !this.active;
    this.is_active.emit(this.active);
  }

}
