import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-blowhorn-multiselect',
  templateUrl: './blowhorn-multiselect.component.html',
  styleUrls: ['./blowhorn-multiselect.component.scss']
})
export class BlowhornMultiselectComponent implements OnInit, OnChanges {

  show_panel = false;
  @Input()
  styles: {} = {
    'dropdown-styles': {},
    'dropdown-toggle-styles': {},
    'dropdown-toggle-display-styles': {},
    'dropdown-menu-styles': {}
  };
  @Output()
  selected_items: EventEmitter<{id: number, selected: boolean, name: string}[]>;
  items_selected: {id: number, selected: boolean, name: string}[] = [];
  @Input()
  items: {id: number, selected: boolean, name: string}[] = [];
  filter_word: string;
  @Input()
  label = 'All Items';
  @Input()
  disabled = false;
  default_label: string;
  @Input()
  max_items_in_label = 3;


  constructor() {
    this.selected_items = new EventEmitter<{id: number, selected: boolean, name: string}[]>();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const prop in changes) {
      if (prop === 'items') {
        this.items_selected = [];
      }
    }
  }

  ngOnInit() {
    this.default_label = this.label;
  }

  hide_show_panel() {
    this.show_panel = !this.show_panel;
  }

  reset_and_suppress_click(event) {
    this.items_selected.forEach(item => item.selected = false);
    this.items_selected = [];
    this.label = this.default_label;
    this.selected_items.emit(this.items_selected);
    event.stopPropagation();
    event.preventDefault();
    this.show_panel = false;
    return false;
  }

  on_filter_field_click(event) {

  }

  on_item_select(item) {
    item.selected = !item.selected;
  }

  fire_selection() {
    this.filter_word = '';
    this.items_selected = this.items.filter(item => item.selected);
    let total_items_selected = this.items_selected.length;
    if (total_items_selected <= this.max_items_in_label) {
        let label_strings = [];
        for (let i = 0; i < total_items_selected; i++) {
            label_strings.push(this.items_selected[i].name);
        }
        this.label = label_strings.join(', ');
    } else {
        this.label = total_items_selected + ' selected';
    }
    this.selected_items.emit(this.items_selected);
    this.show_panel = false;
  }

}
