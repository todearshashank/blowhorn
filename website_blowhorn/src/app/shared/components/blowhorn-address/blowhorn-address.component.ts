import { Component, OnInit, Input } from '@angular/core';
import { BlowhornService } from '../../services/blowhorn.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-blowhorn-address',
  templateUrl: './blowhorn-address.component.html',
  styleUrls: ['./blowhorn-address.component.scss']
})
export class BlowhornAddressComponent implements OnInit {

  @Input()
  address: string;
  @Input()
  legal_name: string;
  @Input()
  phone_number = '';
  @Input()
  support_email = '';
  constants = Constants;
  constructor(
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
  }

}
