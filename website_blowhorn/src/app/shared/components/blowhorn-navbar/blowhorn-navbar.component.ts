import { Component, OnInit, Input } from '@angular/core';
import { BlowhornService } from '../../services/blowhorn.service';

@Component({
  selector: 'app-blowhorn-navbar',
  templateUrl: './blowhorn-navbar.component.html',
  styleUrls: ['./blowhorn-navbar.component.scss']
})
export class BlowhornNavbarComponent implements OnInit {

  @Input()
  link_styles = {};
  @Input()
  navbar_styles = {};
  @Input()
  show_logo = true;
  @Input()
  show_user = true;
  username: string;
  username_styles = {
    dropdownMenuStyles: {
      width: '100%',
      boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
    }
  };
  @Input()
  links: {link: string, text: string}[] = [];

  constructor(
    public _blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
    this.username = this._blowhorn_service.username;
  }

  link_clicked(link) {
  }

}
