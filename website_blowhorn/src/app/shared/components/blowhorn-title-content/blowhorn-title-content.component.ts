import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-blowhorn-title-content',
  templateUrl: './blowhorn-title-content.component.html',
  styleUrls: ['./blowhorn-title-content.component.scss']
})
export class BlowhornTitleContentComponent implements OnInit {

  @Input()
   title: string;

   @Input()
   content: string;

  constructor() { }

  ngOnInit() {
  
  }

}
