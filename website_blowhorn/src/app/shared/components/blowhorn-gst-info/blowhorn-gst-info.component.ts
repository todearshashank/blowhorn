import { Component, OnInit, Input } from '@angular/core';
import { BlowhornService } from '../../services/blowhorn.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-blowhorn-gst-info',
  templateUrl: './blowhorn-gst-info.component.html',
  styleUrls: ['./blowhorn-gst-info.component.scss']
})
export class BlowhornGstInfoComponent implements OnInit {

  @Input('gst_category')
  gst_category: string;
  @Input('gst_in')
  gst_in: string;
  @Input('hsn_code')
  hsn_code: string;
  @Input('cin')
  cin: string;
  @Input('pan')
  pan: string;
  @Input('rcm')
  rcm: boolean;
  @Input('rcm_message')
  rcm_message: string;
  show = {
    gst_category: this.bh_service.current_build == Constants.default_build,
    gst_in: true,
    hsn_code: this.bh_service.current_build == Constants.default_build,
    cin: this.bh_service.current_build == Constants.default_build,
    pan: this.bh_service.current_build == Constants.default_build,
    rcm_message: this.bh_service.current_build == Constants.default_build
  };
  taxationLabels = {
    IN: 'GSTIN',
    ZA: 'Reg. No.'
  };
  taxationLabel = this.taxationLabels[this.bh_service.country_code];

  constructor(
    public bh_service: BlowhornService
  ) { }

  ngOnInit() {
  }

}
