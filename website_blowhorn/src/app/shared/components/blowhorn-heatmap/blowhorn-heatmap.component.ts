import { Component, OnInit, ViewChild, Input, SimpleChanges, Output, EventEmitter, OnChanges } from '@angular/core';
import { HeatmapLayer } from '@ngui/map';
import { BlowhornService } from '../../services/blowhorn.service';

declare var google: any;
declare var MarkerClusterer: any;

@Component({
  selector: 'app-blowhorn-heatmap',
  templateUrl: './blowhorn-heatmap.component.html',
  styleUrls: ['./blowhorn-heatmap.component.scss']
})
export class BlowhornHeatmapComponent implements OnInit, OnChanges {

  @ViewChild(HeatmapLayer, {static: false})
  heat_map_layer: HeatmapLayer;
  @Input()
  radius = 15;
  @Input()
  opacity = 1;
  @Input()
  data_set: google.maps.LatLng[] = [];
  @Input()
  zoom = 12;
  @Input()
  center: google.maps.LatLng|google.maps.LatLngLiteral;
  bounds: google.maps.LatLngBounds;
  @Output()
  map_is_ready: EventEmitter<boolean>;
  heatmap: google.maps.visualization.HeatmapLayer;
  map: google.maps.Map;
  markers: google.maps.Marker[];
  marker_clusterer: any;
  @Input()
  show_marker_cluster = false;
  @Input()
  show_heatmap = false;
  @Input()
  show_limits = false;
  @Input()
  enable_marker_click = false;
  @Input()
  city: number | string;
  @Input()
  coverage_type: string = 'dropoff';
  @Input()
  coverage: {lat: number, lng: number}[];
  city_coverage: {lat: number, lng: number}[];

  constructor(
    public _blowhorn_service: BlowhornService
  ) {
    this.map_is_ready = new EventEmitter(false);
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'data_set') {
        this.show_hide_marker_cluster();
      } else if (propName === 'radius') {
        this.set_radius(this.radius);
      } else if (propName === 'show_marker_cluster') {
        this.show_hide_marker_cluster();
      } else if (propName === 'city') {
        this.city_coverage = this._blowhorn_service.get_city_coverage(this.city, this.coverage_type);
      }
    }
  }

  heatmap_initialized(heatmap: google.maps.visualization.HeatmapLayer) {
    this.heatmap = heatmap;
    this.set_radius(this.radius);
  }

  map_ready(map: any) {
    this.map = map;
    this.set_bounds();
    const options = {
      imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
      gridSize: this.radius
    };
    this.marker_clusterer = new MarkerClusterer(this.map, [], options);
    this.map_is_ready.emit(true);
  }

  set_radius(radius: number) {
    if (this.heatmap) {
      this.heatmap.set('radius', radius);
    }
    if (this.marker_clusterer) {
      this.redraw_marker_cluster();
    }
  }

  redraw_marker_cluster() {
      this.marker_clusterer.setGridSize(this.radius);
      if (this.show_marker_cluster) {
        this.display_marker_cluster();
      }
  }

  set_bounds() {
    if (this.map) {
      this.bounds = new google.maps.LatLngBounds();
      this.data_set.forEach(item => {
        this.bounds.extend(item);
      });
      this.map.fitBounds(this.bounds);
    }
  }

  show_hide_marker_cluster() {
    if (this.show_marker_cluster) {
      this.get_markers();
      this.display_marker_cluster();
    } else if (this.marker_clusterer) {
      this.clear_marker_cluster();
    }
    if (this.data_set.length > 0) {
      this.set_bounds();
    }
  }

  display_marker_cluster() {
    this.clear_marker_cluster();
    this.marker_clusterer.addMarkers(this.markers);
  }

  get_markers() {
    this.markers = [];
    const icon_url = `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/RedTracker.png`;
    this.markers = this.data_set.filter(item => item.lat && item.lng).map(item => {
      const marker = new google.maps.Marker({
        position: item,
        icon: icon_url
      });
      return marker;
    });
  }

  clear_marker_cluster() {
    this.marker_clusterer.clearMarkers();
  }

}
