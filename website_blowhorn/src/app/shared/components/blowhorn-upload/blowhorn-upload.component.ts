import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-upload',
  templateUrl: './blowhorn-upload.component.html',
  styleUrls: ['./blowhorn-upload.component.scss']
})
export class BlowhornUploadComponent implements OnInit {

  @Input()
  id = 'file_chooser';
  @Input()
  disabled = false;
  @ViewChild('file', {static: false})
  file: ElementRef;
  files_selected = false;
  file_chooser_label = 'Select a file';
  file_name = '';
  @Output()
  file_changed: EventEmitter<{}>;
  @Output()
  upload_file: EventEmitter<{}>;

  constructor() {
    this.file_changed = new EventEmitter<{}>();
    this.upload_file = new EventEmitter<{}>();
  }

  ngOnInit() {
  }

  file_change_fired(event) {
    const files = this.file.nativeElement.files;
    this.files_selected = files.length > 0 ? true : false;
    if (this.files_selected) {
      this.file_name = files[0].name;
    } else {
      this.file_name = '';
    }
    this.file_changed.next(files);
  }

  upload_file_fired(event) {
    this.upload_file.next(this.file.nativeElement.files);
    event.stopPropagation();
    event.preventDefault();
  }

  remove_file(event) {
    event.preventDefault();
    event.stopPropagation();
    this.reset();
    return false;
  }

  stop_propagation(event) {
    event.stopPropagation();
  }

  reset() {
    this.disabled = false;
    this.file.nativeElement.value = '';
    this.file_change_fired(event);
  }

  disable() {
    this.disabled = true;
  }

  enable() {
    this.disabled = false;
  }

}
