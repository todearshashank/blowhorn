import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { select } from 'async';

@Component({
    selector: 'app-multiselect-box-panel',
    templateUrl: './multiselect-box-panel.component.html',
    styleUrls: ['./multiselect-box-panel.component.scss']
})
export class MultiselectBoxPanelComponent implements OnInit, OnChanges {

    @Input()
    items = [];
    @Input()
    max_items_to_enable_search = 5;
    @Input()
    label = '';
    @Input()
    select_all_label = 'Select All';
    @Input()
    show_select_all = false;

    @Output()
    dbl_click: EventEmitter<{ id: number, chosen: boolean, name: string, selected: boolean }>;
    @Output()
    selected_items: EventEmitter<{ id: number, chosen: boolean, name: string, selected: boolean }[]>;

    search_text = '';
    search_activated = false;
    select_all = false;
    dbl_click_item: { id: number, chosen: boolean, name: string, selected: boolean };
    items_selected: { id: number, chosen: boolean, name: string, selected: boolean }[] = [];

    constructor(
    ) {
        this.dbl_click = new EventEmitter<{ id: number, chosen: boolean, name: string, selected: boolean }>();
        this.selected_items = new EventEmitter<{ id: number, chosen: boolean, name: string, selected: boolean }[]>();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        for (const prop in changes) {
            if (prop === 'items') {
                this.items_selected = [];
            }
        }
    }

    activate_search_clear(event): void {
        this.search_activated = true;
    }

    deactivate_search_clear(event): void {
        this.search_activated = false;
    }

    reset_search(event): void {
        this.search_text = '';
    }

    on_item_select(item) {
        item.chosen = !item.chosen;
        this.items_selected = this.items.filter(_item => _item.chosen);
        this.selected_items.emit(this.items_selected);
    }

    move_item(item) {
        item.chosen = false;
        this.dbl_click.emit(item);
    }

    trigger_all_select(): void {
        this.select_all = !this.select_all;
        this.items.forEach(item => item.chosen = this.select_all);
        if (!this.select_all) {
            this.items_selected = [];
        } else {
            this.items_selected = this.items;
        }
        this.selected_items.emit(this.items_selected);
        this.select_all = false;
    }

}
