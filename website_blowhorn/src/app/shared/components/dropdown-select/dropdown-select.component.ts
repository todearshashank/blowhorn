import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import * as $ from 'jquery';
import { IdName } from '../../models/id-name.model';
import { CapitalizePipe } from '../../pipes/capitalize.pipe';

@Component({
  selector: 'app-dropdown-select',
  templateUrl: './dropdown-select.component.html',
  styleUrls: ['./dropdown-select.component.scss']
})
export class DropdownSelectComponent implements OnInit, OnChanges {

  @Input()
  selected_name = 'Select';
  @Input()
  selected_id: number;
  @Input()
  items: {}[] = [];
  @Input()
  styles: {} = {
    'dropdown-styles': {},
    'dropdown-toggle-styles': {},
    'dropdown-toggle-display-styles': {},
    'dropdown-menu-styles': {}
  };
  @Input()
  filterable = false;
  filter_word = '';
  @Output()
  value_changed: EventEmitter<IdName>;
  @Input()
  is_reset = false;
  is_activated = false;
  @Input()
  disabled = false;
  default_display: string;
  @Input()
  show_cross = true;
  @Input()
  use_titlecase = true;

  constructor() {
    this.value_changed = new EventEmitter<IdName>();
  }

  ngOnInit() {
    this.default_display = this.selected_name;
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'selected_id' && !changes[propName].currentValue) {
        this.is_reset = false;
      }
    }
  }

  on_dropdown_select(event: any, dropdown: any): void {
    this.selected_name = event.target.dataset.itemName;
    this.selected_id = parseInt(event.target.dataset.itemId, 10);
    this.value_changed.emit(new IdName(this.selected_id, this.selected_name));
    if (this.filterable) {
      this.filter_word = '';
      $('#filter-field').val('');
      dropdown.hide();
    }
    this.is_reset = true;
  }

  on_filter_field_click(event: any): any {
    $('#filter-field').focus();
    event.stopPropagation();
    return false;
  }

  reset_dropdown(event): boolean {
    if (this.selected_id) {
      this.selected_name = this.default_display;
      this.selected_id = null;
      this.value_changed.emit(new IdName(this.selected_id, this.selected_name));
    }
    this.is_reset = false;
    event.stopPropagation();
    return false;
  }

  set_items(items: {}[]): void {
    this.items = items;
  }

  activate_clear(event): void {
    this.is_activated = true;
  }

  deactivate_clear(event): void {
    this.is_activated = false;
  }

  check_filterable(): void {
    this.filter_word = '';
    this.filterable = this.items.length > 10;
  }
}
