import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { LocalStorageService } from '../../services/local-storage.service';
import { WhatsNewService } from '../../services/whats-new.service';

@Component({
    selector: 'app-whats-new',
    templateUrl: './whats-new.component.html',
    styleUrls: ['./whats-new.component.scss'],
    providers: [LocalStorageService, WhatsNewService]
})
export class WhatsNewComponent implements OnInit {

    @Input('app_title') public app_title: string = 'My App';
    @Input('app_code') public app_code: string = '';
    @Input('version') public version: string = '';
    @Input('whats_new') public whats_new = {
        features: [],
        bug_fixes: [],
        announcement: ''
    };

    showDialog = false;
    dialog_subscription: any;

    constructor(
        private local_storage: LocalStorageService,
        public whats_new_service: WhatsNewService
    ) { }

    ngOnInit() {
        const app_version = this.local_storage.get_item(this.app_code);
        if (!app_version || app_version !== this.version) {
            this.whats_new_service.displayDialog = true;
        }
    }

    OnDestroy() {
    }

    dismiss(): void {
        this.whats_new_service.displayDialog = false;
        this.local_storage.set_item(this.app_code, this.version);
    }

}
