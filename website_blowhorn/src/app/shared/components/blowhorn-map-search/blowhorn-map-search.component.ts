import {
  Component, OnInit, ViewChild, ElementRef, NgZone, Input, Output, EventEmitter, OnChanges, SimpleChanges, AfterViewInit
} from '@angular/core';
import { ReactiveFormsModule, NgModel } from '@angular/forms';
import { MapService } from '../../services/map.service';
import { RouteStop } from '../../models/route-stop.model';
import { BsDropdownDirective } from 'ngx-bootstrap/dropdown';
import { SavedLocation } from '../../models/saved-location.model';
import { BlowhornService } from '../../services/blowhorn.service';
import { resolve } from 'path';


@Component({
  selector: 'app-blowhorn-map-search',
  templateUrl: './blowhorn-map-search.component.html',
  styleUrls: ['./blowhorn-map-search.component.scss']
})
export class BlowhornMapSearchComponent implements OnInit, OnChanges, AfterViewInit {

  @ViewChild('search_model', {static: false})
  search_model: NgModel;
  @ViewChild('search', {static: false})
  search_place_ref: ElementRef;
  @ViewChild('dropdown', {static: false})
  dropdown: BsDropdownDirective;
  @Input()
  selected_city: number | string;
  @Input()
  placeholder: string;
  @Input()
  has_saved_locations = true;
  @Input()
  stop: RouteStop;
  @Input()
  stop_header: string;
  @Input()
  stop_type: string;
  @Input()
  enable_delete = false;
  @Input()
  sequence: number;
  @Input()
  enable_swap = false;
  @Input()
  saved_locations: SavedLocation[] = [];
  @Input()
  show_current_loc = false;
  @Output()
  stop_deleted: EventEmitter<RouteStop>;
  @Output()
  stop_updated: EventEmitter<RouteStop>;
  @Output()
  pan_to_location: EventEmitter<google.maps.LatLng>;
  @Output()
  focused: EventEmitter<boolean>;
  @Output()
  blurred: EventEmitter<boolean>;
  @Output()
  place_changed: EventEmitter<google.maps.places.PlaceResult>;
  current_loc: google.maps.LatLng;
  autocomplete: google.maps.places.Autocomplete;
  placeholder_backup: string;
  @Input()
  favourites = true;
  @Input()
  draggable = true;
  @Input()
  default_current_location = false;
  @Input()
  show_delete_placeholder = false;
  @Input()
  disabled = false;

  constructor(
    private _ngZone: NgZone,
    private _map_service: MapService,

  ) {
    this.stop_deleted = new EventEmitter<RouteStop>();
    this.stop_updated = new EventEmitter<RouteStop>();
    this.place_changed = new EventEmitter<google.maps.places.PlaceResult>();
    this.pan_to_location = new EventEmitter<google.maps.LatLng>();
    this.focused = new EventEmitter<boolean>();
    this.blurred = new EventEmitter<boolean>();
  }

  ngOnChanges(changes: SimpleChanges) {
      if (!this.search_place_ref) {
        return;
      }

    if (!this.stop.address.full_address) {
      this.search_place_ref.nativeElement.value = '';
    } else {
      this.search_place_ref.nativeElement.value = this.stop.address.full_address;
    }
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.autocomplete = MapService.get_autocomplete(this.search_place_ref.nativeElement);
    this.auto_complete_set_bounds();
    this.autocomplete.addListener('place_changed', () => {
        this._ngZone.run(() => {
            const place = this.autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }
            this.place_changed.emit(place);
        });
    });
  this.placeholder_backup = this.placeholder;
    navigator.geolocation.getCurrentPosition(pos => {
      this.current_loc = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      if (this.default_current_location) {
        this.set_current_loc();
      }
    });
  }

  fire_delete() {
    this.stop_deleted.emit(this.stop);
  }

  reset_search_text() {
    this.search_place_ref.nativeElement.value = this.stop.address.full_address;
    this.blurred.emit(true);
  }

  pan_to() {
    if (this.stop.geopoint.lat() && this.stop.geopoint.lng()) {
      this.pan_to_location.emit(this.stop.geopoint);
    }
  }

  inputFocused() {
    this.focused.emit(true);
  }

  set_current_loc() {
    if (this.current_loc) {
      this.stop.geopoint = this.current_loc;
        this.stop.position = MapService.map_geopoint_to_position(this.current_loc);
        this._map_service
            .get_geocode(this.current_loc)
            .then(results => {
              const result = results[0];
              const address = this._map_service
                .getAddressComponentsObject(result.address_components);
                this.stop.address.name = '';
                address.id = this.stop.address.id;
                this.stop.address = address;
                this.stop.saved_location = new SavedLocation();
                this.dropdown.hide();
                this.stop_updated.emit(this.stop);
                this.pan_to_location.emit(this.stop.geopoint);
                this.placeholder = this.placeholder_backup;
          })
          .catch(status => {
              console.info('Geocoding Failed', status);
              this.dropdown.hide();
          });
    } else {
      navigator.geolocation.getCurrentPosition(pos => {
        this.current_loc = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        this.set_current_loc();
      });
    }
  }

  set_stop_loc(location: SavedLocation) {
    this.stop.geopoint = MapService.map_position_to_geopoint([location.geopoint.lat, location.geopoint.lng]);
    this.stop.position = [location.geopoint.lat, location.geopoint.lng];
    location.address.id = this.stop.address.id;
    this.stop.address = location.address;
    this.stop.address.name = location.name;
    this.stop.contact = location.contact;
    this.stop.saved_location = JSON.parse(JSON.stringify(location));
    this.dropdown.hide();
    this.stop_updated.emit(this.stop);
    this.pan_to_location.emit(this.stop.geopoint);
    this.placeholder = this.placeholder_backup;
  }

  auto_complete_set_bounds() {
    if (this.selected_city) {
      this.autocomplete.setBounds(this._map_service.get_bounds(this.selected_city, 'pickup'));
    }
  }

  reset_errors() {
    if (this.default_current_location) {
      this.set_current_loc();
    }  else {
      this.search_model.reset();
    }
  }

  check_value(event: any){
    this.search_place_ref.nativeElement.value;
    let trimmed = this.search_place_ref.nativeElement.value.trim();
    let match = /([+-]?\d+\.\d+)(\s*,\s*|\s+)([+-]?\d+\.\d+)/.exec(trimmed);
    if (match) {
      let lat = parseFloat(match[1]),
          lon = parseFloat(match[3]);
      let latlng_object = new google.maps.LatLng(lat, lon);
    this.getCurrentLocation(latlng_object);
  }
  return null;
}
getCurrentLocation(location) {
    this._map_service.get_geocode(location).then(results =>{
    let result = results[0];
    const address = this._map_service
                .getAddressComponentsObject(result.address_components)
                address.id = this.stop.address.id;
                this.stop.address = address;
                this.stop.saved_location = new SavedLocation();
                this.stop_updated.emit(this.stop);
                this.pan_to_location.emit(this.stop.geopoint);
                this.placeholder = this.placeholder_backup;
                if (address != null) {
                  this.place_changed.emit(results[0]);
                } else {
                  alert("No address available!");
                }
              }).catch(status => {
                  console.info('Geocoding Failed', status);
                  this.dropdown.hide();
              });
            }
 }

