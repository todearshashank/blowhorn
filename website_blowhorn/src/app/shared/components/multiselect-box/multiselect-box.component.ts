import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BlowhornService } from '../../services/blowhorn.service';

@Component({
    selector: 'app-multiselect-box',
    templateUrl: './multiselect-box.component.html',
    styleUrls: ['./multiselect-box.component.scss']
})
export class MultiselectBoxComponent implements OnInit {

    @Input()
    items = [];
    @Input()
    available_panel_label = 'Available';
    @Input()
    chosen_panel_label = 'Selected';
    @Input()
    select_all_label_available = 'Add All';
    @Input()
    select_all_label_chosen = 'Remove All';
    @Input()
    show_select_all = false;
    @Input()
    show_remove_all = false;

    @Output()
    selected_options: EventEmitter<{ id: number, selected: boolean, name: string, chosen: boolean}[]>;

    selected_available_items = [];
    selected_chosen_items = [];

    available_items = [];
    chosen_items = [];

    @Input()
    action_buttons = {
        select: {
            label: '⇨',
            type: '',
            disabled: false,
            title: 'Add'
        },
        deselect: {
            label: '⇦',
            type: '',
            disabled: false,
            title: 'Remove'
        }
    };

    constructor(
        public blowhorn_service: BlowhornService,
    ) {
        this.selected_options = new EventEmitter<{ id: number, selected: boolean, name: string, chosen: boolean }[]>();
    }

    ngOnInit() {
        this.available_items = this.items.filter(item => !item.selected);
        this.chosen_items = this.items.filter(item => item.selected);
    }

    set_selected_items(event: { id: number, chosen: boolean, name: string, selected: boolean }[]): void {
        this.selected_available_items = event;
        if (event.length === this.available_items.length) {
            this.selectValues();
        }
    }

    add_item(event: { id: number, chosen: boolean, name: string, selected: boolean }): void {
        const addItemIndex = this.available_items.findIndex(_item => _item.id === event.id);
        this.available_items.splice(addItemIndex, 1);
        const itemIndexInSelected = this.selected_available_items.findIndex(_item => _item.id === event.id);
        if (itemIndexInSelected !== -1) {
            this.selected_available_items.splice(itemIndexInSelected, 1);
        }
        this.chosen_items.push(event);
        this.selected_options.emit(this.chosen_items);
    }

    remove_item(event: { id: number, chosen: boolean, name: string, selected: boolean }): void {
        const addItemIndex = this.chosen_items.findIndex(_item => _item.id === event.id);
        this.chosen_items.splice(addItemIndex, 1);
        const itemIndexInSelected = this.selected_chosen_items.findIndex(_item => _item.id === event.id);
        if (itemIndexInSelected !== -1) {
            this.selected_chosen_items.splice(itemIndexInSelected, 1);
        }
        this.available_items.push(event);
        this.selected_options.emit(this.chosen_items);
    }

    set_chosen_items(event: { id: number, chosen: boolean, name: string, selected: boolean }[]): void {
        this.selected_chosen_items = event;
        if (event.length === this.chosen_items.length) {
            this.deSelectValues();
        }
    }

    comparer(arr): any {
        return function (currentItem) {
            return arr.filter(function (item) {
                return item.chosen === currentItem.chosen;
            }).length === 0;
        };
    }

    selectValues(): void {
        this.available_items = this.available_items.filter(this.comparer(this.selected_available_items));
        this.selected_available_items.forEach(item => {
            item.chosen = false;
            item.selected = true;
            this.chosen_items.push(item);
        });
        this.selected_available_items = [];
        this.selected_options.emit(this.chosen_items);
    }

    deSelectValues(): void {
        this.chosen_items = this.chosen_items.filter(this.comparer(this.selected_chosen_items));
        this.selected_chosen_items.forEach(item => {
            item.chosen = false;
            item.selected = false;
            this.available_items.push(item);
        });
        this.selected_chosen_items = [];
        this.selected_options.emit(this.chosen_items);
    }

}
