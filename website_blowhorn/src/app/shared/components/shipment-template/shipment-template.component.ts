import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShipmentService } from '../../services/shipment.service';
import { IdName } from '../../models/id-name.model';
import { Constants } from '../../utils/constants';
import { BlowhornUploadComponent } from '../blowhorn-upload/blowhorn-upload.component';
import { WatcherService } from '../../services/watcher.service';
import { BlowhornService } from '../../services/blowhorn.service';

@Component({
  selector: 'app-shipment-template',
  templateUrl: './shipment-template.component.html',
  styleUrls: ['./shipment-template.component.scss']
})
export class ShipmentTemplateComponent implements OnInit {

  @ViewChild('file_upload', {static: false})
  file_upload: BlowhornUploadComponent;

  ROWWISE_SKU = 'Rowwise SKU';
  COLUMNWISE_SKU = 'Columnwise SKU';
  customer_list: IdName[];
  selected_id: number;
  selected_sku_orientation = null;
  selected_sku_orientation_upload = null;
  show_message = false;
  type = 'success';
  heading = '';
  message = '';

  sku_orientations = [{
    name: this.ROWWISE_SKU,
    id: 1
  },{
    name: this.COLUMNWISE_SKU,
    id: 2
  }]

  constructor(
    private _route: ActivatedRoute,
    private _shipment_service: ShipmentService,
    private _watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
  ) {
    this._route.data.subscribe(data => {
      this.customer_list = data.customers;
    });
  }

  ngOnInit() {
  }

  get_template() {
    if (!this.selected_sku_orientation) {
      alert('Please, select SKU orientation');
      return;
    }
    window.open(
      `${Constants.urls.API_SHIPMENT_ORDER_TEMPLATE_EXPORT}?customer_id=${this.selected_id}&is_rowwise=${this.selected_sku_orientation == this.ROWWISE_SKU}`,
      '_blank'
    );
  }

  customer_selected(id_name: IdName) {
    this.selected_id = id_name.id;
  }

  file_changed(file: File) {

  }

  sku_orientation_selected(event: IdName) {
    this.selected_sku_orientation = event.name;
  }

  sku_orientation_selected_upload(event: IdName) {
    this.selected_sku_orientation_upload = event.name;
  }

  reset() {
    this.file_upload.reset();
    this.show_message = false;
  }

  upload_file(files: any) {
    if (!this.sku_orientation_selected_upload) {
      alert('Please, select SKU orientation');
      return;
    }
    this.file_upload.disable();
    this._blowhorn_service.show_loader();
    const file = files[0];
    const form_data = new FormData();
    form_data.append('shipment_orders', file, file.name);
    form_data.set('is_rowwise', `${this.selected_sku_orientation_upload == this.ROWWISE_SKU}`);
    this._shipment_service
      .upload_shipment_orders(form_data)
      .subscribe(data => {
        this.show_message = true;
        this.heading = 'Orders Created';
        this.message = `Batch id is ${data['batch']}. Number of orders created is ${data['number_of_orders']}`;
        this.type = 'success';
        this._blowhorn_service.hide_loader();
      }, err => {
        this.show_message = true;
        this.heading = 'Order Creation Failed!';
        console.log('err: ', err);
        let errorMsg = err['error']['errors'];
        if (!errorMsg && err['error'].length > 0) {
            errorMsg = err['error'][0];
        }
        this.message = errorMsg;
        this.type = 'error';
        this._blowhorn_service.hide_loader();
      });
  }

  get customer_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'relative',
        'width': '300px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

}
