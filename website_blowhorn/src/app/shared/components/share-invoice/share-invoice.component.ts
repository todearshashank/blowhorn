import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { TripsService } from '../../services/trips.service';

@Component({
  selector: 'app-share-invoice',
  templateUrl: './share-invoice.component.html',
  styleUrls: ['./share-invoice.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),
  ]
})
export class ShareInvoiceComponent implements OnInit {

  @Input() trip: any;
  @Output() close_fired: EventEmitter<boolean>;
  usersShare: string[] = [];
  isSubmitting = false;
  success = false;
  message = '';
  min_loader_styles = {};

  constructor(
    public tripService: TripsService,
  ) {
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
    this.close_fired.emit(true);
  }

  public setUsersList(users: string[]) {
    this.usersShare = users;
  }

  public shareInvoice() {
    this.isSubmitting = true;
    this.tripService
      .share_invoice(this.trip.order_real_id, this.usersShare)
      .subscribe((resp: any) => {
        this.message = 'Invoice has been sent successfully';
        this.isSubmitting = false;
        this.success = true;
        setTimeout(() => {
          this.success = false;
          this.message = '';
        }, 2000);
      },
      err => {
        console.error(err);
        this.isSubmitting = false;
        this.message = 'Failed to send invoice';
      });
  }

}
