import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class BlowhornDropdownComponent implements OnInit {

  @Input()
  headerText: string = '';
  @Input()
  options: string = '';
  @Output()
  selectedOption: EventEmitter<string>;
  @Input()
  styles: {} = {
    'dropdown-styles': {},
    'dropdown-toggle-styles': {},
    'dropdown-toggle-display-styles': {},
    'dropdown-menu-styles': {}
  };

  constructor() {
    this.selectedOption = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  performClick(val: string, dropdown: any) {
    this.selectedOption.next(val);
  }

}
