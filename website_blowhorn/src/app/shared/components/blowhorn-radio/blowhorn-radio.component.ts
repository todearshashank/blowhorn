import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blowhorn-radio',
  templateUrl: './blowhorn-radio.component.html',
  styleUrls: ['./blowhorn-radio.component.scss']
})
export class BlowhornRadioComponent implements OnInit {

  @Input() id = null;
  @Input() name = '';
  @Input() value = '';
  @Input() label = '';
  @Input() default_value = '';
  @Input() selected = false;
  @Output() selected_radio: EventEmitter<string>;

  constructor() {
    this.selected_radio = new EventEmitter<string>();
  }

  ngOnInit() {
  }

  radioClicked() {
    this.selected_radio.emit(this.value);
  }

}
