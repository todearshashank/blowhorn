import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, ViewChild } from '@angular/core';

@Component({
  selector: 'app-blowhorn-checkbox',
  templateUrl: './blowhorn-checkbox.component.html',
  styleUrls: ['./blowhorn-checkbox.component.scss']
})
export class BlowhornCheckboxComponent implements OnInit {

  @ViewChild('checkbox', {static: false})
  checkbox: HTMLInputElement;
  @Input() label = '';
  @Input('group_name') public group_name = '';
  @Input('id') public id = '';
  @Input('styles')
  styles: {} = {
    'checkbox-group-styles': {},
    'checkbox-input-styles': {},
    'checkbox-label-styles': {},
  };
  @Input('checked') public checked = false;
  @Input('disabled') public disabled = false;
  @Input('hide_label') hide_label = false;
  @Output('value_changed') value_changed: EventEmitter<any>;

  constructor() {
    this.value_changed = new EventEmitter<any>();
  }

  ngOnInit() {
  }

  public selectValue(event: any) {
    const target = event.target;
    const value = {
        'group_name': target.name,
        'name': target.id,
        'checked': target.checked
    };
    this.checked = target.checked;
    this.value_changed.emit(value);
  }

  public reset() {
    this.checked = false;
    this.checkbox.checked =  false;
    this.value_changed.emit({
        'group_name': this.group_name,
        'name': this.id,
        'checked': false
    });
  }

}
