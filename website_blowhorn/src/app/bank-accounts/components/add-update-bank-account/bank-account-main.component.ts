import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
import { BankAccount } from "../../../shared/models/bank.model";
import { BankAccountsService } from "../../../shared/services/bank-accounts.service";
import { BlowhornService } from "../../../shared/services/blowhorn.service";

@Component({
    selector: "app-bank-account-main",
    templateUrl: "./bank-account-main.component.html",
    styleUrls: ["./bank-account-main.component.scss"],
})
export class AddUpdateBankAccount implements OnInit {

    @Output() emitService: EventEmitter<any>;

    header_text = ''
    bankForm: FormGroup;
    bank: BankAccount = null;
    isActive = true;
    editMode = true;
    bankAccountBackup: BankAccount = null;
    showIfscDependancyField = false;
    localSearchHistory = [];
    ifscDepedantFields = {
        bank_name: "BANK",
        branch_name: "BRANCH",
        address: "ADDRESS",
        city: "CITY",
        district: "DISTRICT",
        state: "STATE",
        rtgs: "RTGS",
    };
    loading = false;

    constructor(
        private route: ActivatedRoute,
        public activeModal: NgbActiveModal,
        private toastr: ToastrService,
        private bankAccountService: BankAccountsService
    ) {
        this.emitService = new EventEmitter<any>();
    }

    ngOnInit() {
        this.header_text = this.bank ? 'Account Details' : 'Add Account'
        this.editMode = !this.bank;
        this.isActive = this.bank && this.bank.is_active
        this.bankForm = new FormGroup({
            account_name: new FormControl(
                {
                    value: this.bank ? this.bank.account_name : "",
                    disabled: !this.editMode,
                },
                [Validators.required, Validators.pattern("[a-zA-Z .]{2,60}")]
            ),
            account_number: new FormControl(
                {
                    value: this.bank ? this.bank.account_number : "",
                    disabled: !this.editMode,
                },
                [
                    Validators.required,
                    Validators.maxLength(18),
                    Validators.minLength(9),
                    Validators.pattern("[0-9]{9,18}"),
                ]
            ),
            confirm_account_number: new FormControl(
                {
                    value: this.bank ? this.bank.account_number : "",
                    disabled: !this.editMode,
                },
                [
                    Validators.required,
                    Validators.maxLength(18),
                    Validators.minLength(9),
                    Validators.pattern("[0-9]{9,18}"),
                ]
            ),
            ifsc_code: new FormControl(
                {
                    value: this.bank ? this.bank.ifsc_code : "",
                    disabled: !this.editMode,
                },
                [
                    Validators.required,
                    Validators.pattern("[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}"),
                ]
            ),
            bank_name: new FormControl(
                {
                    value: this.bank ? this.bank.bank_name : "",
                    disabled: !this.editMode,
                },
                [Validators.required, Validators.maxLength(255)]
            ),
            branch_name: new FormControl(
                {
                    value: this.bank ? this.bank.branch_name : "",
                    disabled: !this.editMode,
                },
                [Validators.required, Validators.maxLength(255)]
            ),
            address: new FormControl({
                value: this.bank ? this.bank.address : "",
                disabled: true,
            }),
            city: new FormControl({
                value: this.bank ? this.bank.city : "",
                disabled: true,
            }),
            district: new FormControl({
                value: this.bank ? this.bank.district : "",
                disabled: true,
            }),
            state: new FormControl({
                value: this.bank ? this.bank.state : "",
                disabled: true,
            }),
            rtgs: new FormControl({
                value: this.bank ? this.bank.rtgs : "",
                disabled: true,
            }),
        });
    }

    hasError = (controlName: string, errorName: string) => {
        return this.bankForm.controls[controlName].hasError(errorName);
    };

    clearDependantFeilds() {
        this.setBankDetails({
            BANK: "",
            BRANCH: "",
            ADDRESS: "",
            DISTRICT: "",
            CITY: "",
            STATE: "",
            RTGS: false,
        });
    }

    resetForm() {
        this.bankForm.reset();
    }

    restoreForm() {
        let bankAc = new BankAccount()
        bankAc = this.bankAccountBackup
        this.bank = bankAc;
        delete bankAc.id;
        this.bankForm.setValue({
            ...bankAc,
            address: '',
            city: '',
            district: '',
            state: '',
            rtgs: false
        })
        this.editMode = false;
    }

    enableEditMode() {
        this.editMode = true
        this.resetForm()
    }

    searchIfscFromLocalHistory() {
        return this.localSearchHistory.find(
            (record) => record.IFSC === this.bankForm.value.ifsc_code
        );
    }

    setBankDetails(data: any) {
        for (const [key, value] of Object.entries(this.ifscDepedantFields)) {
            this.bankForm.controls[key].setValue(data[value]);
        }
    }

    getBankDetailsFromIFSC() {
        if (this.bankForm.controls.ifsc_code.valid) {
            this.showIfscDependancyField = true;
            let bankDetails = this.searchIfscFromLocalHistory();
            if (!bankDetails) {
                this.bankAccountService
                    .getBankDetailsFromIFSC(this.bankForm.value.ifsc_code)
                    .subscribe((data) => {
                        console.log({ data });
                        this.localSearchHistory.push(data);
                        this.setBankDetails(data);
                    });
            } else {
                this.setBankDetails(bankDetails);
            }
        }
    }

    confirmedValidator(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
            if (
                matchingControl.errors &&
                !matchingControl.errors.confirmedValidator
            ) {
                return;
            }
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ confirmedValidator: true });
            } else {
                matchingControl.setErrors(null);
            }
        };
    }

    onSubmit() {
        if (this.bankForm.invalid) {
            this.toastr.warning("Please correct error and try again.");
            return;
        }
        let payload = this.bankForm.value
        payload.id = this.bank ? this.bank.id : null
        payload.is_active = this.isActive
        this.loading = true;
        this.bankAccountService.saveBankAccount(payload).subscribe(
            (data) => {
                this.loading = false;
                this.toastr.success("Bank details updated successfully");
                this.emitService.next(true);
            },
            (err) => {
                this.loading = false;
                this.toastr.error(err.message);
            }
        );
    }

    deleteRecord() {
        this.loading = true;
        this.bankAccountService.deleteBankAccount(this.bank.id).subscribe(
            (data) => {
                this.loading = false;
                this.toastr.success("Bank details deleted successfully");
                this.emitService.next(true);
            },
            (err) => {
                this.loading = false;
                this.toastr.error(err.message);
            }
        );
    }
}
