import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { BankAccount } from '../../../shared/models/bank.model';
import { BankAccountsService } from '../../../shared/services/bank-accounts.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { AddUpdateBankAccount } from '../add-update-bank-account/bank-account-main.component';

@Component({
  selector: 'app-bank-account-list',
  templateUrl: './bank-account-list.component.html',
  styleUrls: ['./bank-account-list.component.scss']
})
export class BankAccountListComponent implements OnInit {

    bankAccounts: BankAccount[] = []
    selectedAccount: BankAccount = null;
    delete_button = {
        label: 'Delete',
        type: 'delete'
    };
    status_button = {
        label: 'Activate',
        type: 'save'
    }
    confirmation_category = '';
    prompt = {
        show: false,
        heading: '',
        details: '',
        styles: {}
    };
    loading = false;
    confirmation = {
        heading: '',
        details: '',
        show_warning: false,
        show_alert: false,
        action_buttons: {
            confirm: {
                show: false,
                label: 'Yes',
                type: 'save'
            },
            decline: {
                show: false,
                label: 'No',
                type: ''
            }
        },
        styles: {
            header: {}
        }
    };

  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modalService: NgbModal,
    private _blowhorn_service: BlowhornService,
    private bankAccountService: BankAccountsService
  ) {
    route.data.subscribe(
        (data) => {
            this._blowhorn_service.hide_loader();
            this.bankAccounts = data.bankAccount || []
        }, (err) => {
            console.error(err);
            this._blowhorn_service.hide_loader();
        }
    );
  }

  ngOnInit() {
  }

  show_confirmation(row: BankAccount, category: string): void {
    this.confirmation_category = category;
    this.selectedAccount = row;
    this.confirmation.show_alert = true;
    this.confirmation.heading = 'Confirm';
    let details = ''
    if (category === 'delete') {
        details = `${this.selectedAccount.account_number} will be deleted permanently.
            Do you really want to delete?`;
    } else {
        let action = row.is_active ? 'deactivate' : 'activate'
        details = `${this.selectedAccount.account_number} will be ${action}d.
            Do you really want to ${action}?`;
    }
    this.confirmation.details = details
    this.confirmation.show_warning = category === 'delete';
    this.confirmation.action_buttons.confirm.show = true;
    this.confirmation.action_buttons.decline.show = true;
    this.confirmation.action_buttons.decline.label = 'No';
    this.confirmation.action_buttons.decline.type = '';
    this.confirmation.styles.header = {};
  };

  confirmAction(proceed: boolean) {
    if (!proceed) {
        this.selectedAccount = null;
        this.confirmation.show_alert = false;
        return;
    }
    this.confirmation.show_alert = false;
    if (this.confirmation_category === 'status') {
        this.updateAccount()
    } else {
        this.deleteAccount()
    }
  }

  updateAccount() {
    this.loading = true;
    let payload = Object.assign({}, this.selectedAccount)
    payload.is_active = !payload.is_active
    this.bankAccountService.updateBankAccount(payload).subscribe(
        (data) => {
            this.loading = false;
            this.toastr.success("Bank details updated successfully");
            this.fetchBankAccounts()
        },
        (err) => {
            this.loading = false;
            this.toastr.error(err.error);
        }
    );
  }

  deleteAccount() {
    this.loading = true;
    this.bankAccountService.deleteBankAccount(this.selectedAccount.id).subscribe(
        (data) => {
            this.loading = false;
            let deletedIndex = this.bankAccounts.findIndex(i => i.id === this.selectedAccount.id)
            this.bankAccounts.splice(deletedIndex, 1)
            this.selectedAccount = null;
            this.confirmation.show_alert = false;
            this.toastr.success('Account deleted successfully');
        },
        (err) => {
            this.loading = false;
            this.toastr.error(err.message);
        }
    );
  }

  fetchBankAccounts() {
    this.bankAccounts = []
    this.bankAccountService.getBankAccounts().subscribe((resp) => {
        this.bankAccounts = resp;
    }, err => {
        console.error('Failed to fetch records')
    })
  }

  addAccount() {
    const modalRef = this.modalService.open(AddUpdateBankAccount, {
        backdropClass: 'backdrop-color',
        centered: true
    });
    modalRef.componentInstance.bank = null;
    modalRef.componentInstance.emitService.subscribe(result => {
        let index = this.bankAccounts.findIndex(item => item.id === result.id);
        this.bankAccounts.splice(index > -1 ? index : 0, index > -1 ? 1 : 0, result);
        setTimeout(() => this.modalService.dismissAll(false), 500)
        this.fetchBankAccounts()
    }, (err: any) => {
        console.error('Error', err);
    });
  }

  viewBankAccount(account: BankAccount) {
    const modalRef = this.modalService.open(AddUpdateBankAccount, {
        backdropClass: 'backdrop-color',
        centered: true
    });
    modalRef.componentInstance.bank = account;
    modalRef.componentInstance.emitService.subscribe(result => {
        let index = this.bankAccounts.findIndex(item => item.id === result.id);
        this.bankAccounts.splice(index > -1 ? index : 0, index > -1 ? 1 : 0, result);
        setTimeout(() => this.modalService.dismissAll(false), 500)
        this.fetchBankAccounts()
    }, (err: any) => {
        console.error('Error', err);
    });
  }

}
