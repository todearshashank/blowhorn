import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule, MatDividerModule, MatInputModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';


import { SharedModule } from '../shared/shared.module';
import { RouteModule } from '../route/route.module';
import { AddUpdateBankAccount } from './components/add-update-bank-account/bank-account-main.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    RouteModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule,
    MatIconModule,
    MatSlideToggleModule,
  ],
  declarations: [
    AddUpdateBankAccount
  ],
  entryComponents: [
    AddUpdateBankAccount
  ]
})
export class BankAccountsModule { }
