import { ResourcePlanningResolver } from './shared/resolvers/resource-planning.resolver';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER, ErrorHandler } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { DndListModule } from 'ng6-dnd-lists';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NguiMapModule} from '@ngui/map';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule, MatDividerModule, MatInputModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';


import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { MyFleetModule } from './my-fleet/my-fleet.module';
import { MyTripsModule } from './my-trips/my-trips.module';
import { DepartureSheetModule } from './departure-sheet/departure-sheet.module';
import { AdminModule } from './admin/admin.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { FleetService } from './shared/services/fleet.service';
import { TripService } from './shared/services/trip.service';
import { InvoiceService } from './shared/services/invoice.service';
import { TripDetailResolver } from './shared/resolvers/trip-detail.resolver';
import { TripFilterResolver } from './shared/resolvers/trip-filter.resolver';
import { ShipmentFilterResolver } from './shared/resolvers/shipment-filter.resolver';
import { InvoiceResolver } from './shared/resolvers/invoice.resolver';
import { DepartureSheetResolver } from './shared/resolvers/departure-sheet.resolver';
import { CompletedTripsResolver } from './shared/resolvers/completed-trips.resolver';
import { TripsResolver } from './shared/resolvers/trips.resolver';
import { UpcomingTripsResolver } from './shared/resolvers/upcoming-trips.resolver';
import { DetailResolver } from './shared/resolvers/details.resolver';
import { OrderHeatmapResolver } from './shared/resolvers/order-heatmap.resolver';
import { WatcherService } from './shared/services/watcher.service';
import { ShipmentService } from './shared/services/shipment.service';
import { ShipmentOrderResolver } from './shared/resolvers/shipment-order.resolver';
import { ShipmentOrderModule } from './shipment-order/shipment-order.module';
import { MapService } from './shared/services/map.service';
import { BlowhornService } from './shared/services/blowhorn.service';
import { AgmDirectionModule } from 'agm-direction';
import { LivetrackingResolver } from './shared/resolvers/livetracking.resolver';
import { LivetrackingService } from './shared/services/livetracking.service';
import { WhatsNewService } from './shared/services/whats-new.service';
import { LocalStorageService } from './shared/services/local-storage.service';
import { NameValuePipe } from './shared/pipes/name-value.pipe';
import { BlowhornLoaderComponent } from './shared/components/blowhorn-loader/blowhorn-loader.component';
import { SaveRouteComponent } from './route/components/save-route/save-route.component';
import { RouteService } from './shared/services/route.service';
import { PaymentService } from './shared/services/payment.service';
import { SidebarService } from './shared/services/sidebar.service';
import { TripsService } from './shared/services/trips.service';
import { DetailLocationResolver } from './shared/resolvers/details-location.resolver';
import { DataService } from './shared/services/data.service';
import { ToastrModule } from 'ngx-toastr';
import { TripModule } from './trip/trip.module';
import { HeatmapService } from './shared/services/heatmap.service';
import { AllCustomersResolver } from './shared/resolvers/all-customers.resolver';
import { CustomerService } from './shared/services/customer.service';
import { CompletedMyTripsResolver } from './shared/resolvers/completed-my-trips.resolver';
import { CurrentMyTripsResolver } from './shared/resolvers/current-my-trips.resolver';
import { UpcomingMyTripsResolver } from './shared/resolvers/upcoming-my-trips.resolver';
import { UnassignedShipmentOrderResolver } from './shared/resolvers/unassigned-shipment-order.resolver';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { Constants } from './shared/utils/constants';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { DriverHeatmapResolver } from './shared/resolvers/driver-heatmap.resolver';
import { MembersModule } from './members/members.module';
import { MemberResolver } from './shared/resolvers/members.resolver';
import { ResourceAllocationResolver } from './shared/resolvers/resource-allocation.resolver';
import { FleetOverviewAuthGuard } from './shared/gaurds/fleet-overview-auth.guard';
import { FleetOverviewModule } from './fleet-overview/fleet-overview.module';
import { EditTripStopsComponent } from './my-trips/components/edit-trip-stops/edit-trip-stops.component';
import { EnterpriseInvoiceModule } from './enterprise-invoice/enterprise-invoice.module';
import { EnterpriseInvoiceService } from './shared/services/enterprise-invoice.service';
import { EnterpriseInvoiceResolver } from './shared/resolvers/enterprise-invoice.resolver';
import { FlashSaleModule } from './flash-sale/flash-sale.module';
import { FlashSaleResolver } from './shared/resolvers/flash-sale.resolver';
import { FlashSaleService } from './shared/services/flash-sale.service';
import { FlashSaleMessageResolver } from './shared/resolvers/flash-sale-message.resolver';
import { GlobalErrorHandler } from './global-error-handler';
import { ReportService } from './shared/services/report.service';
import { Reports2Module} from './reports2/reports2.module';
import { Reports2Resolver } from './shared/resolvers/reports2.resolver';
import { FeedbackComponent } from './feedback/feedback.component';
import { FeedbackService } from './shared/services/feedback.service';
import { ResourcePlanningModule } from './resource-planning/resource-planning.module';
import { AwbGeneratorComponent } from './awb-generator/awb-generator.component';
import { AwbFileComponent } from './awb-generator/awb-file/awb-file.component';
import { ThirdPartyIntegrationComponent } from './third-party-integration/third-party-integration.component';
import { TpiProcessFlowComponent } from './third-party-integration/tpi-process-flow/tpi-process-flow.component';
import { TpiFormComponent } from './third-party-integration/tpi-form/tpi-form.component';
import { TpiApiKeyComponent } from './third-party-integration/tpi-api-key/tpi-api-key.component';
import { TpiAddServiceComponent } from './third-party-integration/tpi-add-service/tpi-add-service.component';
import { TpiSuggestionFormComponent } from './third-party-integration/tpi-suggestion-form/tpi-suggestion-form.component';
import { BankAccountsResolver } from './shared/resolvers/bank-accounts.resolver';
import { BankAccountsService } from './shared/services/bank-accounts.service';
import { BankAccountListComponent } from './bank-accounts/components/bank-account-list/bank-account-list.component';
import { BankAccountsModule } from './bank-accounts/bank-account.module';


@NgModule({
  declarations: [
    AppComponent,
    FeedbackComponent,
    AwbGeneratorComponent,
    AwbFileComponent,
    ThirdPartyIntegrationComponent,
    TpiProcessFlowComponent,
    TpiFormComponent,
    TpiApiKeyComponent,
    TpiAddServiceComponent,
    TpiSuggestionFormComponent,
    BankAccountListComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    // Material
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDividerModule,

    AppRoutingModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: environment.google_maps.apiKey,
      libraries: ['geometry', 'places', 'visualization', 'drawing']
    }),
    AgmJsMarkerClustererModule,
    NguiMapModule.forRoot({
      apiUrl: `https://maps.google.com/maps/api/js?libraries=geometry,places,visualization,drawing&key=${environment.google_maps.apiKey}`,
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    SharedModule,
    MyFleetModule,
    MyTripsModule,
    ConfigurationModule,
    AdminModule,
    DepartureSheetModule,
    ShipmentOrderModule,
    DndListModule,
    AgmDirectionModule,
    NgbModule,
    TripModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
    }),
    ApolloModule,
    HttpLinkModule,
    MembersModule,
    FleetOverviewModule,
    EnterpriseInvoiceModule,
    FlashSaleModule,
    Reports2Module,
    ResourcePlanningModule,
    BankAccountsModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      multi: true,
      deps: [MapsAPILoader]
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    BlowhornService,
    FleetService,
    TripService,
    InvoiceService,
    TripDetailResolver,
    TripFilterResolver,
    ShipmentFilterResolver,
    DepartureSheetResolver,
    InvoiceResolver,
    CompletedTripsResolver,
    TripsResolver,
    UpcomingTripsResolver,
    CompletedMyTripsResolver,
    CurrentMyTripsResolver,
    UpcomingMyTripsResolver,
    WatcherService,
    ShipmentService,
    ShipmentOrderResolver,
    MapService,
    BlowhornService,
    LivetrackingResolver,
    LivetrackingService,
    NameValuePipe,
    WhatsNewService,
    LocalStorageService,
    RouteService,
    PaymentService,
    SidebarService,
    TripsService,
    DataService,
    DetailResolver,
    DetailLocationResolver,
    HeatmapService,
    OrderHeatmapResolver,
    DriverHeatmapResolver,
    AllCustomersResolver,
    CustomerService,
    UnassignedShipmentOrderResolver,
    MemberResolver,
    ResourceAllocationResolver,
    FleetOverviewAuthGuard,
    EnterpriseInvoiceService,
    EnterpriseInvoiceResolver,
    FlashSaleResolver,
    FlashSaleMessageResolver,
    FlashSaleService,
    ReportService,
    Reports2Resolver,
    FeedbackService,
    ResourcePlanningResolver,
    BankAccountsResolver,
    BankAccountsService,
  ],
  entryComponents: [
    BlowhornLoaderComponent,
    SaveRouteComponent,
    EditTripStopsComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(
    apollo: Apollo,
    http_link: HttpLink,
    maps_api_loader: MapsAPILoader,
  ) {
    const http = http_link.create({
      uri: Constants.urls.API_GRAPHQL
    });
    const auth = setContext((_) => {
      return BlowhornService.http_headers;
    });
    apollo.create({
      link: auth.concat(http),
      cache: new InMemoryCache()
    });
  }
}

export function init_app (maps_api_loader: MapsAPILoader) {
  return () => maps_api_loader.load().then(() => {
    console.log('map api service initialized from app init');
  });
}
