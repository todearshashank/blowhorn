import { Component, OnInit } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { HeatmapService } from '../../../shared/services/heatmap.service';
import { ActivatedRoute } from '@angular/router';
import { IdName } from '../../../shared/models/id-name.model';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-driver-heatmap',
  templateUrl: './driver-heatmap.component.html',
  styleUrls: ['./driver-heatmap.component.scss']
})
export class DriverHeatmapComponent implements OnInit {

  data_set: google.maps.LatLng [] = [];
  center: google.maps.LatLngLiteral = { lat: 12.9716, lng: 77.5946 };
  total_drivers = 0;
  radius = 20;
  show_heatmap = true;
  show_marker_cluster = false;
  show_limits = false;
  enable_limits = false;
  selected_city_id: number = null;
  selected_city_name = 'All Cities';
  selected_status_id: number = null;
  selected_status_name = 'All Statuses';
  cities: IdName[] = [];
  statuses: {}[] = [];
  all_drivers: {geopoints: google.maps.LatLngLiteral[], status: string, city: string}[];
  filtered_drivers: {geopoints: google.maps.LatLngLiteral[], status: string, city: string}[];

  constructor(
    private _watcher_service: WatcherService,
    private _heatmap_service: HeatmapService,
    private _route: ActivatedRoute,
    private _apollo: Apollo,
    public _blowhorn_service: BlowhornService,
  ) {
    this._apollo.watchQuery<any>({
      query: gql`
        {
          driverStatuses
        }
      `,
      fetchPolicy: 'network-only'
    }).valueChanges.subscribe(result => {
      this.statuses = this._blowhorn_service.get_dropdown_list(result.data.driverStatuses);
    });
  }

  ngOnInit() {
  }

  initialize(map_loaded: boolean) {
    this._route
      .data
      .subscribe(data => {
        const city_map = new Map();
        this.all_drivers = data.drivers.data.drivers.edges.map(edge => {
          if (edge.node.operatingCity) {
            city_map.set(edge.node.operatingCity.id, edge.node.operatingCity.name);
          }
          let geopoints: google.maps.LatLngLiteral[] = [];
          if (edge.node.driverpreferredlocationSet.length) {
            geopoints = edge.node.driverpreferredlocationSet.map(item => {
              return {
                lat: item.geopoint.coordinates[1],
                lng: item.geopoint.coordinates[0]
              };
            });
          } else {
            geopoints.push({lat: null, lng: null});
          }
          return {
            city: edge.node.operatingCity ? edge.node.operatingCity.name : '',
            status: edge.node.status,
            geopoints: geopoints
          };
        });
        city_map.forEach((val, key) => {
          this.cities.push(new IdName(key, val));
        });
        this.filtered_drivers = this.all_drivers;
        this.set_data_set();
        this._blowhorn_service.hide_loader();
      },
      err => {
        this._blowhorn_service.hide_loader();
      }
    );
  }

  set_data_set() {
    this.data_set = [];
    this.filtered_drivers.forEach(driver => {
      driver.geopoints.forEach(geopoint => {
        if (geopoint.lat && geopoint.lng) {
          this.data_set.push(new google.maps.LatLng(geopoint.lat, geopoint.lng));
        }
      });
    });
  }

  city_changed(city: IdName) {
    this.selected_city_id = city.id;
    this.selected_city_name = city.name;
    if (this.selected_city_id) {
      this.enable_limits = true;
    } else {
      this.enable_limits = false;
    }
    this.filter_drivers();
  }

  status_changed(status: IdName) {
    this.selected_status_id = status.id;
    this.selected_status_name = status.name;
    this.filter_drivers();
  }

  filter_drivers() {
    if (this.selected_city_id && this.selected_status_id) {
      this.filtered_drivers = this.all_drivers.filter(driver => {
        return driver.city === this.selected_city_name && driver.status.toLowerCase() === this.selected_status_name.toLowerCase();
      });
    } else if (this.selected_city_id) {
      this.filtered_drivers = this.all_drivers.filter(driver => {
        return driver.city === this.selected_city_name;
      });
    } else if (this.selected_status_id) {
      this.filtered_drivers = this.all_drivers.filter(driver => {
        return driver.status.toLowerCase() === this.selected_status_name.toLowerCase();
      });
    } else {
      this.filtered_drivers = this.all_drivers;
    }
    this.set_data_set();
  }
  show_hide_limits(event) {
    this.show_limits = event['checked'];
  }

  switch_view(event) {
    if (event.target.value === 'heatmap') {
      this.show_marker_cluster = false;
      this.show_heatmap = true;
    } else {
      this.show_marker_cluster = true;
      this.show_heatmap = false;
    }
  }

  check_radius(event) {
    if (!((event.keyCode > 95 && event.keyCode < 106)
      || (event.keyCode > 47 && event.keyCode < 58)
      || event.keyCode === 8)) {
        event.stopPropagation();
        event.preventDefault();
        return false;
    }
  }

  download_file() {
    const city = this.selected_city_id ? '?city=' + this.selected_city_id : '';
    const url = `/api/driver/drivers/export${city}`;
    window.open(url);
  }

  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '40px',
        'width': '150px',
        'padding-left': '5px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        marginTop: '3px',
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get status_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '40px',
        'width': '150px',
        'padding-left': '5px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        marginTop: '3px',
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

}
