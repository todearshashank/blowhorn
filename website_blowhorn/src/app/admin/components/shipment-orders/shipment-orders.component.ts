import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shipment-orders',
  templateUrl: './shipment-orders.component.html',
  styleUrls: ['./shipment-orders.component.scss']
})
export class ShipmentOrdersComponent implements OnInit {

  links = [{link: 'template', text: 'Import/Export'}, {link: 'assign-driver', text: 'Assign Driver'}, {link: 'findroutes', text: 'Find Optimized Routes [BETA]'}];

  constructor() { }

  ngOnInit() {
  }

}
