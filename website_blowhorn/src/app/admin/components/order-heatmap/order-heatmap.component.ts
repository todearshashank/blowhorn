import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IdName } from '../../../shared/models/id-name.model';
import { City } from '../../../shared/models/city.model';
import { WatcherService } from '../../../shared/services/watcher.service';
import { HeatmapService } from '../../../shared/services/heatmap.service';
import { ActivatedRoute } from '@angular/router';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';

declare var google: any;


@Component({
  selector: 'app-order-heatmap',
  templateUrl: './order-heatmap.component.html',
  styleUrls: ['./order-heatmap.component.scss']
})
export class OrderHeatmapComponent implements OnInit, AfterViewInit {

    public app_title = Constants.version_info.admin_heatmap.app_title;
    public app_code: string = Constants.version_info.admin_heatmap.app_code;
    public version: string = Constants.version_info.admin_heatmap.version;
    public whats_new: any = Constants.version_info.admin_heatmap.whats_new;

  master_data = [];
  data_set: google.maps.LatLng[] = [];
  data_set_pickup: google.maps.LatLng[] = [];
  data_set_dropoff: google.maps.LatLng[] = [];
  center: google.maps.LatLng | google.maps.LatLngLiteral = { lat: 12.9716, lng: 77.5946 };
  total_orders = 0;
  start_date: string;
  end_date: string;
  http_params = {};
  date_range: [Date, Date] = [null, null];
  radius = 20;
  customer_types: IdName[] = [new IdName(1, 'Individual'), new IdName(2, 'Non Enterprise')];
  cities: City[] = [];
  statuses: IdName[] = [];
  cancellation_reasons: IdName[] = [];
  hubs: IdName[] = [];
  hubs_backup: IdName[] = [];
  vehicle_types: IdName[] = [];
  selected_city_id: number = null;
  selected_city_name = 'All Cities';
  selected_hub_id: number = null;
  selected_hub_name = 'All Hubs';
  selected_status_id: number = null;
  selected_status_name = 'All Statuses';
  selected_vehicle_type_id: number = null;
  selected_vehicle_type_name = 'All Vehicle Types';
  selected_customer_type_id: number = null;
  selected_customer_type_name = 'All Customer Types';
  selected_reason_name = 'Select Cancellation Reason';
  selected_reason_id: number = null;
  show_pickup = true;
  show_dropoff  = true;
  show_heatmap = true;
  show_marker_cluster = false;
  show_limits = false;
  enable_limits = false;
  ORDER_CANCELLED = 'Cancelled';

  constructor(
    private _http_client: HttpClient,
    private _watcher_service: WatcherService,
    private _heatmap_service: HeatmapService,
    private _route: ActivatedRoute,
    public _blowhorn_service: BlowhornService,
  ) { }

  ngOnInit() {
  }

  initialize(map_loaded: boolean) {
    this._route
      .data
      .subscribe(data => {
          this.master_data = data['orders'];
          this.load_data(this.master_data);
      },
      err => {
        this._blowhorn_service.hide_loader();
      });
  }

  ngAfterViewInit() {
    this.fetch_filters();
  }

  get_latlng(arr: any[]): google.maps.LatLng {
    return new google.maps.LatLng(arr[0], arr[1]);
  }

  reset_date(event: any) {
    this.date_range = [null, null];
    delete this.http_params['end_date'];
    delete this.http_params['start_date'];
    this.fetch_orders();
  }

  date_changed(event: any) {
    this.start_date = this.date_range[0].toISOString().substring(0, 10);
    this.end_date = this.date_range[1].toISOString().substring(0, 10);
    this.http_params['end_date'] = this.end_date;
    this.http_params['start_date'] = this.start_date;
    this.fetch_orders();
  }

  cancellation_reason_changed(event: any) {
    if (!event.id || this.selected_status_name !== this.ORDER_CANCELLED) {
        this.load_data(this.master_data);
        return;
    }
    let filtered_data = [];
    this.master_data.forEach(item => {
        if (item.status === this.ORDER_CANCELLED &&
            item.reason_for_cancellation === event.name) {
            filtered_data.push(item);
        }
    });
    this.load_data(filtered_data);
  }

  city_changed(city: {}) {
    this.hubs = [];
    const city_from_cities = this.cities.find(item => item.id === city['id']);
    this.selected_city_id = city['id'];
    if (city_from_cities) {
      this.center = {lat: city_from_cities.geopoint[1], lng: city_from_cities.geopoint[0]};
      this.hubs.push(...city_from_cities.hubs);
      this.http_params['city_id'] = city['id'];
      this.enable_limits = true;
    } else {
      this.hubs.push(...this.hubs_backup);
      delete this.http_params['city_id'];
      this.enable_limits = false;
      this.show_limits = false;
    }
    this.fetch_orders();
  }

  hub_changed(hub: {}) {
    if (hub && hub['id']) {
      this.http_params['hub_id'] = hub['id'];
    } else {
      delete this.http_params['hub_id'];
    }
    this.fetch_orders();
  }

  vehicle_type_changed(vehicle_type: {}) {
    if (vehicle_type && vehicle_type['id']) {
      this.http_params['vehicle_class_id'] = vehicle_type['id'];
    } else {
      delete this.http_params['vehicle_class_id'];
    }
    this.fetch_orders();
  }

  customer_type_changed(customer_type: {}) {
    if (customer_type && customer_type['id']) {
      this.http_params['customer_category'] = customer_type['name'];
    } else {
      delete this.http_params['customer_category'];
    }
    this.fetch_orders();
  }

  status_changed(status: {}) {
      this.selected_status_name = status && status['id'] ? status['name'] : null;
    if (status && status['id']) {
      this.http_params['status'] = status['name'];
    } else {
      delete this.http_params['status'];
    }
    this.fetch_orders();
  }

  fetch_orders() {
    this._blowhorn_service.show_loader();
    this._heatmap_service
      .get_heatmap_orders(this.http_params)
        .subscribe((data: any[]) => {
            this.master_data = data;
            this.load_data(data);
      },
      err => {
        this._blowhorn_service.hide_loader();
      });
  }

  fetch_filters() {
    this._heatmap_service
      .get_heapmap_order_filters()
      .subscribe(data => {
        this.cities = data['cities'];
        this.vehicle_types = data['vehicle_classes'];
        this.cancellation_reasons = data['cancellation_reasons'];
        this.hubs = [];
        this.hubs_backup = [];
        this.cities.forEach(item => {
          this.hubs.push(...item.hubs);
          this.hubs_backup.push(...item.hubs);
        });
        data['statuses'].forEach((item: string, index: number) => {
          this.statuses.push(new IdName(index + 1, item));
        });
      });
  }

  load_data(data: any[]) {
    this.data_set_dropoff = [];
    this.data_set_pickup = [];
    data.forEach(item => {
      this.data_set_pickup.push(new google.maps.LatLng(item['pickup_lat'], item['pickup_lng']));
      this.data_set_dropoff.push(new google.maps.LatLng(item['dropoff_lat'], item['dropoff_lng']));
    });
    this.merge_pickup_dropoff();
    this.total_orders = data.length;
    this._blowhorn_service.hide_loader();
  }

  merge_pickup_dropoff() {
    this.data_set = [];
    if (this.show_pickup && this.show_dropoff) {
      this.data_set = [...this.data_set_pickup, ...this.data_set_dropoff];
    } else if (this.show_pickup) {
      this.data_set = [...this.data_set_pickup];
    } else if (this.show_dropoff) {
      this.data_set = [...this.data_set_dropoff];
    }
  }

  show_hide_limits(event: { [x: string]: boolean; }) {
    this.show_limits = event['checked'];
  }

  dropoff_changed(event: { checked: boolean; }) {
    this.show_dropoff = event.checked;
    this.merge_pickup_dropoff();
  }

  pickup_changed(event: { checked: boolean; }) {
    this.show_pickup = event.checked;
    this.merge_pickup_dropoff();
  }

  switch_view(event: { target: { value: string; }; }) {
    if (event.target.value === 'heatmap') {
      this.show_marker_cluster = false;
      this.show_heatmap = true;
    } else {
      this.show_marker_cluster = true;
      this.show_heatmap = false;
    }
  }

  check_radius(event: { keyCode: number; stopPropagation: () => void; preventDefault: () => void; }) {
    if (!((event.keyCode > 95 && event.keyCode < 106)
      || (event.keyCode > 47 && event.keyCode < 58)
      || event.keyCode === 8)) {
        event.stopPropagation();
        event.preventDefault();
        return false;
    }
  }

  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '40px',
        'width': '150px',
        'padding-left': '5px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        marginTop: '3px',
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get cancellation_reason_styles(): {} {
    return {
        'dropdown-styles': {
          'box-shadow': 'none',
          'border': 'none',
          'height': '40px',
          'width': '190px',
          'padding-left': '5px',
          'z-index': 2
        },
        'dropdown-toggle-styles': {
          'padding-left': '0px'
        },
        'dropdown-menu-styles': {
          marginTop: '3px',
          'width': '200px',
          boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
        }
      };
  }

  get hub_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '40px',
        'width': '200px',
        'padding-left': '5px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        marginTop: '3px',
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get status_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '40px',
        'width': '200px',
        'padding-left': '5px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        marginTop: '3px',
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get vehicle_type_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '40px',
        'width': '150px',
        'padding-left': '5px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        marginTop: '3px',
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get customer_type_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '40px',
        'width': '150px',
        'padding-left': '5px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        marginTop: '3px',
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

}
