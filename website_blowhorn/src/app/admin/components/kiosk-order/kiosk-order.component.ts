import {
    Component,
    OnInit,
    ViewChildren,
    QueryList,
} from '@angular/core';
import { MapService } from '../../../shared/services/map.service';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { BlowhornMapSearchComponent } from '../../../shared/components/blowhorn-map-search/blowhorn-map-search.component';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';
import { HttpClient } from '@angular/common/http';
import { IdName } from '../../../shared/models/id-name.model';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-kiosk-order',
    templateUrl: './kiosk-order.component.html',
    styleUrls: ['./kiosk-order.component.scss']
})
export class KioskOrderComponent implements OnInit {

    GROUND = 'Ground';
    ELEVATOR = 'Elevator';
    FLOOR_PRICE_EXEMPTIONS = [this.GROUND, this.ELEVATOR];
    PRICE_FIXED = 'fixed';
    PRICE_VARIABLE = 'variable';
    customer_name: string;
    customer_email: string;
    customer_number: string;
    map_latitude = 12.9716;
    map_longitude = 77.5946;
    map_bounds: google.maps.LatLngBounds;
    selected_city_name: string;
    selected_city_id: number;
    selected_contract_name: string;
    selected_contract_id: number;
    selected_package_name: string;
    selected_package_id: number;
    selected_payment_mode_name: string;
    selected_payment_mode_id: number;
    selected_floor_name: string;
    selected_floor_id: number;
    selected_item_name: string;
    selected_item_id: number;
    calculated_distance = 0;
    pre_paid = false;
    order_cost: String;
    cost_category: String = this.PRICE_VARIABLE;
    is_variable_price = true;
    stops: [RouteStop, RouteStop] = [
        new RouteStop(null, 0),
        new RouteStop(null, 1)
    ];
    @ViewChildren(BlowhornMapSearchComponent)
    map_search_comps: QueryList<BlowhornMapSearchComponent>;
    is_current_location_in_city = false;
    _blowhorn_service: BlowhornService;
    _map_service: MapService;
    directions: google.maps.DirectionsResult;
    currentPolygon = {
        path: [],
        config: {
            strokeColor: '',
            fillColor: '',
            fillOpacity: ''
        }
    }
    polygonConfigs = {
        pickup: {
            strokeColor: '#57d3f6',
            fillColor: '#57d3f6',
            fillOpacity: '0.2'
        },
        dropoff: {
            strokeColor: '#9d76f4',
            fillColor: '#9d76f4',
            fillOpacity: '0.2'
        }
    }
    pickup_position: google.maps.LatLng = MapService.dummy_geopoint;
    dropoff_position: google.maps.LatLng = MapService.dummy_geopoint;
    show_direction = false;
    packages: IdName[] = [];
    contracts: IdName[] = [];
    available_floor_slabs: IdName[] = [];
    num_of_item_slabs: IdName[] = [];
    floor_slabs = [];
    item_slabs = [];
    contract_package_map: {} = {};
    base_pay = 0;
    package_pay = 0;
    total_cost = 0;
    floor_number = 0;
    number_of_items = 0;
    message_type = '';
    message_header = '';
    message = '';
    display_info = {
        distance: 0,
        time: 0
    };
    show_message = false;
    payment_modes = [new IdName(1, 'Cash'), new IdName(2, 'PayTm')];
    email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    show_mini_loader = false;
    min_loader_styles = {
        wrapper: {
            width: '50px'
        },
        ball1: {
            height: '10px',
            width: '10px',
        },
        ball2: {
            height: '10px',
            width: '10px',
        },
        ball3: {
            height: '10px',
            width: '10px',
        },
    };


    constructor(
        private map_service: MapService,
        public blowhorn_service: BlowhornService,
        private _http_client: HttpClient
    ) {
        this._blowhorn_service = blowhorn_service;
        this._map_service = map_service;
    }

    ngOnInit() {
        this.map_bounds = MapService.dummy_bounds;
        this.selected_city_name = this._blowhorn_service.default_city.name;
        this.selected_city_id = this._blowhorn_service.default_city.id;
        this.set_map_bounds('pickup');
        this.focused(null, 'pickup');
        this.show_hide_current_location();
        this.fetch_contract_package();
    }

    set_map_bounds(location_type: string) {
        this.map_bounds = MapService.dummy_bounds;
        if (
            this.stops[0].position[0] &&
            this.stops[0].position[1] &&
            this.stops[1].position[0] &&
            this.stops[1].position[1]
        ) {
            this.stops.forEach(stop => {
                this.map_bounds.extend(stop.geopoint);
            });
        } else if (this.stops[0].position[0] && this.stops[0].position[1]) {
            this.map_bounds.extend(this.stops[0].geopoint);
        } else if (this.stops[1].position[0] && this.stops[1].position[1]) {
            this.map_bounds.extend(this.stops[1].geopoint);
        } else {
            this._blowhorn_service.get_city_coverage(this.selected_city_id, location_type)
                .forEach(item => {
                    this.map_bounds.extend(item);
                });
        }
    }

    marker_dragged(event: any, stop: any) {
        stop.position = [event.coords.lat, event.coords.lng];
        stop.geopoint = MapService.map_position_to_geopoint(stop.position);
        this._map_service
            .get_geocode(stop.geopoint)
            .then(results => {
                const result = results[0];
                stop.address = this._map_service.getAddressComponentsObject(
                    result.address_components
                );
                stop.address.name = '';
            })
            .catch(status => {
                console.info('Geocoding Failed', status);
            });
        this.set_position(stop);
    }

    payment_mode_changed(event: any) {
        if (event.id) {
            this.selected_payment_mode_name = event.name;
            this.selected_payment_mode_id = event.id;
        } else {
            this.selected_payment_mode_name = 'select payment mode';
            this.selected_payment_mode_id = null;
        }
    }

    create_order(form: NgForm) {
        this._blowhorn_service.show_loader();
        let selected_floor_name = [this.GROUND, this.ELEVATOR].indexOf(this.selected_floor_name) !== -1 ? 0 : this.selected_floor_id;
        const data = {
            'customer_name': this.customer_name,
            'customer_email': this.customer_email,
            'customer_number': this.customer_number,
            'city_id': this.selected_city_id,
            'cost_category': this.cost_category,
            'contract_id': this.selected_contract_id,
            'cost': this.order_cost,
            'pre_paid': this.pre_paid,
            'labour_id': this.selected_package_id,
            'pickup_address': {
                'line2': this.stops[0].address.street_address,
                'line3': this.stops[0].address.area,
                'line4': this.stops[0].address.city,
                'postcode': this.stops[0].address.postal_code,
                'line1': this.stops[0].address.full_address,
                'phone_number': '',
                'geopoint': {
                    'latitude': this.stops[0].position[0],
                    'longitude': this.stops[0].position[1],
                }
            },
            'shipping_address': {
                'line2': this.stops[1].address.street_address,
                'line3': this.stops[1].address.area,
                'line4': this.stops[1].address.city,
                'postcode': this.stops[1].address.postal_code,
                'line1': this.stops[1].address.full_address,
                'phone_number': '',
                'geopoint': {
                    'latitude': this.stops[1].position[0],
                    'longitude': this.stops[1].position[1],
                }
            },
            'base_pay': this.base_pay,
            'labour_pay': this.package_pay,
            'payment_mode': this.selected_payment_mode_id ? this.selected_payment_mode_name : null,
            'floor_number': selected_floor_name,
            'number_of_items': this.selected_item_name,
            'estimated_distance': this.calculated_distance / 1000,
        };
        this._http_client
            .post(
                Constants.urls.API_KIOSK_ORDER,
                data,
                BlowhornService.http_headers
            )
            .subscribe(
                res => {
                    this._blowhorn_service.hide_loader();
                    this.message_type = 'success';
                    this.message_header = 'Order creation successful';
                    this.message = `Order number: ${res['order_number']}`;
                    this.show_message = true;
                    this.clear_fields(form);
                },
                err => {
                    console.log('ERR: ', err);
                    this._blowhorn_service.hide_loader();
                    this.message_type = 'error';
                    this.message_header = 'Order creation failed';
                    this.message = err['error']['message'] || err.error;
                    this.show_message = true;
                }
            );
    }

    reset_error() {
        this.message_type = '';
        this.message_header = '';
        this.message = '';
        this.show_message = false;
    }

    clear_fields(form: NgForm) {
        this.city_changed({});
        this.customer_name = '';
        this.customer_number = '';
        this.selected_floor_id = null;
        this.selected_floor_name = '';
        this.selected_item_id = null;
        this.selected_item_name = '';
        this.order_cost = null;
        form.resetForm();
        this.cost_category = this.PRICE_VARIABLE;
        this.cost_category_changed({});
        this.map_search_comps.forEach(comp => comp.reset_errors());
    }

    reset_search_text(component) {
        component.nativeElement.value = '';
    }

    set_position(stop: RouteStop) {
        let location_type = 'dropoff';
        if (stop.seq_no) {
            this.dropoff_position = MapService.map_position_to_geopoint(
                stop.position
            );
        } else {
            this.pickup_position = MapService.map_position_to_geopoint(
                stop.position
            );
            location_type = 'pickup';
        }
        this.set_show_direction();
        this.set_map_bounds(location_type);
    }

    fetch_contract_package() {
        this.show_mini_loader = true;
        this._http_client
            .get(Constants.urls.API_KIOSK_CONTRACT_PACKAGE, {
                params: {
                    city_id: `${this.selected_city_id}`
                }
            })
            .subscribe((data: any[]) => {
                this.contracts = [];
                this.packages = [];
                this.contract_package_map = {};
                this.show_mini_loader = false;
                if (data.length) {
                    data.forEach(res => {
                        this.contracts.push(
                            new IdName(res.contract.id, res.contract.name)
                        );
                        this.contract_package_map[`${res.contract.id}`] = {
                            contract: res.contract,
                            labours: res.labours,
                            vas_slabs: res.vas_slabs
                        };
                    });
                }
            });
    }

    set_show_direction() {
        if (
            this.stops[0].position[0] &&
            this.stops[0].position[1] &&
            this.stops[1].position[0] &&
            this.stops[1].position[1]
        ) {
            this.show_direction = true;
        } else {
            if (this.stops[0].position[0] && this.stops[0].position[1]) {
                this.dropoff_position = this.pickup_position;
                this.show_direction = true;
            } else if (this.stops[1].position[0] && this.stops[1].position[1]) {
                this.pickup_position = this.dropoff_position;
                this.show_direction = true;
            } else {
                this.show_direction = false;
                this.dropoff_position = this.pickup_position = MapService.dummy_geopoint;
            }
        }
    }

    payment_status_change(status: any) {
        this.pre_paid = status;
        if (!this.pre_paid) {
            this.payment_mode_changed({});
        }
    }

    floor_changed(event: any) {
        if (event && event.id) {
            this.selected_floor_name = event.name;
            this.selected_floor_id = event.id;
        } else {
            this.selected_floor_name = '';
            this.selected_floor_id = null;
        }
        this.calculate_total_cost();
    }

    item_changed(event: any) {
        if (event && event.id) {
            this.selected_item_name = event.name;
            this.selected_item_id = event.id;
        } else {
            this.selected_item_name = '';
            this.selected_item_id = null;
        }
        this.calculate_total_cost();
    }

    calculate_total_cost() {
        let slab_cost = 0;
        if (this.selected_floor_id &&
                this.FLOOR_PRICE_EXEMPTIONS.indexOf(this.selected_floor_name) === -1) {
            let cost = this.floor_slabs.map(item => {
                if (item.start <= this.selected_floor_id <= item.end) {
                    return parseFloat(item.rate) * this.selected_floor_id;
                }
                return 0;
            });
            slab_cost += cost.reduce((a, b) => a + b, 0);
        }

        if (this.selected_item_id) {
            let cost = this.item_slabs.map(item => {
                if (item.start <= this.selected_item_id <= item.end) {
                    return parseFloat(item.rate) * this.selected_item_id;
                }
                return 0;
            });
            slab_cost += cost.reduce((a, b) => a + b, 0);
        }
        this.total_cost = this.base_pay + this.package_pay + slab_cost;
        console.log('total_cost', this.total_cost);
    }

    retrieve_options(slabs: any) {
        let available_slabs = slabs.map((item: { end: any; }) => item.end);
        let maxSlabVal = Math.max(...available_slabs);
        return Array.from(new Array(maxSlabVal), (val, index) => index);
    }

    set_floor_and_item_slabs(vas_slabs) {
        this.floor_slabs = [];
        this.item_slabs = [];
        for (let slab of vas_slabs) {
            let data = {
                start: slab.start,
                end: slab.end,
                rate: slab.rate
            };
            if (slab.vas_attribute === 'Floor Number') {
                this.floor_slabs.push(data);
            } else if (slab.vas_attribute === 'Number of Items') {
                this.item_slabs.push(data);
            }
        }
        let fs = this.retrieve_options(this.floor_slabs);
        this.available_floor_slabs = fs.map(item => {
            if (item === 0) {
                return new IdName(99999, this.GROUND);
            } else {
                return new IdName(item, item.toString());
            }
        });
        this.available_floor_slabs.push(new IdName(-1, this.ELEVATOR));
        let nis = this.retrieve_options(this.item_slabs);
        this.num_of_item_slabs = nis.map(item => new IdName(item + 1, (item + 1).toString()));
    }

    cost_category_changed(event: any) {
        if (this.cost_category === this.PRICE_FIXED) {
            this.selected_contract_id = null;
            this.selected_contract_name = '';
            this.selected_package_id = null;
            this.selected_package_name = '';
            this.is_variable_price = false;
        } else {
            this.order_cost = null;
            this.is_variable_price = true;
        }
    }

    city_changed(event: any) {
        if (event.id) {
            this.selected_city_name = event.name;
            this.selected_city_id = event.id;
        } else {
            this.selected_city_name = this._blowhorn_service.default_city.name;
            this.selected_city_id = this._blowhorn_service.default_city.id;
        }
        this.show_hide_current_location();
        this.stops = [new RouteStop(null, 0), new RouteStop(null, 1)];
        this.show_direction = false;
        this.pickup_position = this.dropoff_position =
            MapService.dummy_geopoint;
        this.set_map_bounds('pickup');
        this.fetch_contract_package();
        this.contract_changed({});
    }

    package_changed(event: { id?: any; name?: any; }) {
        if (event.id) {
            this.selected_package_name = event.name;
            this.selected_package_id = event.id;
            let selected_contract = this.contract_package_map[`${this.selected_contract_id}`];
            this.package_pay = selected_contract.labours.filter(
                (rec: { id: number; }) => rec.id === this.selected_package_id
            )[0].labour_cost;
        } else {
            this.selected_package_name = 'select package';
            this.selected_package_id = null;
            this.package_pay = 0;
        }
        this.calculate_total_cost();
    }

    contract_changed(event: any) {
        if (event.id) {
            this.selected_contract_name = event.name;
            this.selected_contract_id = event.id;
            let current_contract = this.contract_package_map[`${this.selected_contract_id}`];
            this.packages = current_contract.labours.map(record => {
                return new IdName(record.id, record.labour_category);
            });
            this.base_pay = current_contract.contract.base_pay;
            this.set_floor_and_item_slabs(current_contract.contract.vas_slabs);
        } else {
            this.selected_contract_name = 'select contract';
            this.selected_contract_id = null;
            this.packages = [];
            this.base_pay = 0;
            this.num_of_item_slabs = [];
            this.available_floor_slabs = [];
        }
        this.package_changed({});
    }

    show_hide_current_location() {
        if (
            this._blowhorn_service.is_navigator_geolocation &&
            this.selected_city_id
        ) {
            if (this._map_service.current_position) {
                this.is_current_location_in_city = this._map_service.is_current_location_in_city(
                    this.selected_city_id,
                    'dropoff'
                );
            } else {
                navigator.geolocation.getCurrentPosition(pos => {
                     const current_loc = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                     this.is_current_location_in_city = this._map_service.is_location_in_city(
                        this.selected_city_id, current_loc, 'dropoff');
                });
            }
        }
    }

    pan_to(event: google.maps.LatLng) {
        this.map_latitude = event.lat();
        this.map_longitude = event.lng();
    }

    update_stop(event: RouteStop, stop: RouteStop) {
        stop = event;
        this.set_position(stop);
    }

    focused(event: any, stopType: any) {
        console.log({stopType});
        this.currentPolygon.config = this.polygonConfigs[stopType];
        this.currentPolygon.path = [];
        this._blowhorn_service
            .get_city_coverage(this.selected_city_id, stopType)
            .forEach(item => {
                this.currentPolygon.path.push(item);
            });
    }

    blurred(event: any) {
        this.currentPolygon.path = [];
    }

    place_changed(
        place: google.maps.places.PlaceResult,
        stop: RouteStop,
        placeholder: string,
        location_type: string
    ) {
        const comp = this.map_search_comps.find(item => {
            return item.stop.seq_no === stop.seq_no;
        });
        if (
            this._map_service.is_location_in_city(
                this.selected_city_id,
                place.geometry.location,
                location_type,
            )
        ) {
            comp.placeholder = placeholder;
            stop.geopoint = place.geometry.location;
            stop.address = this._map_service.getAddressComponentsObject(
                place.address_components
            );
            stop.position = MapService.map_geopoint_to_position(
                place.geometry.location
            );
            stop.address.name = '';
            this.pan_to(stop.geopoint);
        } else {
            comp.placeholder = 'Out of coverage';
            this.stops.splice(stop.seq_no, 1, new RouteStop(null, stop.seq_no));
        }
        this.set_position(stop);
    }

    get_origin_destination(stop: RouteStop) {
        if (this.stops[0].position && this.stops[1].position) {
            return MapService.map_position_to_geopoint(stop.position);
        } else {
            return MapService.dummy_geopoint;
        }
    }

    set_directions(directions: any) {
        this.directions = directions;
        console.log('directions: ', directions);
        if (directions.routes && directions.routes.length) {
            let direction = directions.routes[0].legs[0]
            this.calculated_distance = direction.distance.value;
            this.display_info.distance = direction.distance.text;
            this.display_info.time = direction.duration.text;
        } else {
            this.calculated_distance = 0;
            this.display_info = {
                distance: 0,
                time: 0
            };
        }
    }

    get_icon_url(index: number): string {
        return index ? Constants.markers.dropoff : Constants.markers.pickup;
    }

    get_stop_header(index: number): string {
        return index ? 'Dropoff' : 'Pickup';
    }

    get dropdown_styles() {
        return {
            'dropdown-styles': {
                boxShadow: 'none',
                border: 'none',
                backgroundColor: '#f7fbfc',
                color: '#757E7F',
                fontSize: '14px',
                fontWeight: 900,
                letterSpacing: '2px',
                lineHeight: '21px',
                textTransform: 'uppercase'
            },
            'dropdown-menu-styles': {
                width: '100%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    is_price_invalid(): boolean {
        return this.is_variable_price ? !this.selected_contract_id : !this.order_cost;
    }

    is_prepaid_invalid(): boolean {
        return this.pre_paid && !this.selected_payment_mode_id;
    }
}
