import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MapService } from '../../../shared/services/map.service';
import { ShipmentUnassignedOrder } from '../../../shared/models/shipment-unassigned-order.model';
import { IdName } from '../../../shared/models/id-name.model';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { WatcherService } from '../../../shared/services/watcher.service';
import { LatLngLiteral } from '@agm/core';
import { BlowhornCheckboxComponent } from '../../../shared/components/blowhorn-checkbox/blowhorn-checkbox.component';
import { onErrorResumeNext } from 'rxjs';
import { validate } from 'graphql';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

declare var google: any;

@Component({
  selector: 'app-shipment-order-assign',
  templateUrl: './shipment-order-assign.component.html',
  styleUrls: ['./shipment-order-assign.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class ShipmentOrderAssignComponent implements OnInit {

  @ViewChild('multi_selection_checkbox', {static: false})
  multi_selection_checkbox: BlowhornCheckboxComponent;
  zoom = 12;
  scrollwheel = true;
  streetViewControl = false;
  zoomControl = true;
  mapDraggable = true;
  lat = 12.9716;
  lng = 77.5946;
  orders: ShipmentUnassignedOrder[] = [];
  all_orders: ShipmentUnassignedOrder[] = [];
  selected_orders: ShipmentUnassignedOrder[] = [];
  orders_without_geopoint: ShipmentUnassignedOrder[] = [];
  saved_icon_url = `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/SelectedTracker.png`;
  unsaved_icon_url = `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/RedTracker.png`;
  ignore_icon_url = `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/BlackTracker.png`;
  selected_icon_url = `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/BlueTracker.png`;
  map_bounds: google.maps.LatLngBounds;
  selected_city_name: string;
  selected_city_id: number;
  selected_customer_name: string;
  selected_customer_id: number;
  selected_batch_name: string;
  selected_batch_id: number;
  selected_driver_name: string;
  selected_driver_id: string;
  selected_driver_active_trips: string;
  selected_pincodes: {id: number, selected: boolean, name: string}[] = [];
  cities: IdName[] = [];
  customers: IdName[] = [];
  batches: IdName[] = [];
  date_activated = false;
  date_range: [Date, Date] = [null, null];
  show_driver_lookup = false;
  show_message = false;
  messages: string[];
  driver_lookup_message = 'Search for results';
  driver_name_search: string;
  vehicle_number_search: string;
  start_cursor = '';
  end_cursor = '';
  has_next_page = false;
  has_previous_page = false;
  driver_list: {}[] = [];
  is_paginated = false;
  address_changed = false;
  message_type = '';
  show_confirm = false;
  confirm_message = '';
  show_cluster = true;
  show_restore = false;
  reset_cluster = true;
  total_unsaved = 0;
  total_fetched = 0;
  show_only_modified = false;
  selected_points: LatLngLiteral[] = [];
  strokeWeight = 2;
  strokeColor = '#056DB4';
  multi_selection = false;
  show_multi_select_actions = false;
  map: google.maps.Map;
  polygon: google.maps.Polygon;
  pincodes: {id: number, selected: boolean, name: string}[];
  pincode_label = 'All Pincodes';

  constructor(
    private _route: ActivatedRoute,
    private _map_service: MapService,
    private _apollo: Apollo,
    private _watcher_service: WatcherService,
    private _change_detector: ChangeDetectorRef,
    public _blowhorn_service: BlowhornService,
  ) {
  }

  fetch_geopoints(orders: ShipmentUnassignedOrder[]) {
    orders.forEach(order => {
      const address = order.address;
      this.get_geopoint_from_address(order, address);
    });
  }

  get_geopoint_from_address(order: ShipmentUnassignedOrder, address: string) {
    this._map_service.get_geopoint_from_address(address)
        .then((results) => {
          this.total_fetched ++;
          if (results.length > 0) {
            const location = results[0].geometry.location;
            order.lat = location.lat();
            order.lng = location.lng();
            order.is_modified = true;
            order.icon_url = this.unsaved_icon_url;
          }
          this.check_total_loaded();
        })
        .catch(reason => {
          if (reason === 'OVER_QUERY_LIMIT') {
            setTimeout(() => this.get_geopoint_from_address(order, address), 1000);
          } else if (reason === 'ZERO_RESULTS') {
            this.set_default_location(order);
          } else {
            console.error('reason:', reason, address);
            this.set_default_location(order);
          }
        });
  }

  set_default_location(order: ShipmentUnassignedOrder) {
    this.total_fetched ++;
    order.lat = 0;
    order.lng = 0;
    order.is_modified = true;
    order.icon_url = this.unsaved_icon_url;
    this.check_total_loaded();
  }

  check_total_loaded() {
    if (this.total_fetched === this.total_unsaved) {
      this.total_fetched = 0;
      this.total_unsaved = 0;
      this._blowhorn_service.hide_loader();
      this.set_map_bounds(true);
      this._change_detector.detectChanges();
    }
  }

  ngOnInit() {
    this._route.data.subscribe(data => {
      const unassigned_orders = data.unassigned_orders.data.unassignedOrders;
      this.all_orders = unassigned_orders.map(order => {
        const order_new = new ShipmentUnassignedOrder(
          null, null, false, this.saved_icon_url, order.number, order.shippingAddress.line1, false,
          order.batch ? order.batch.id : '', false, false, false, order.datePlaced, order.batch ? order.batch.numberOfEntries : ''
          , order.batch ? order.batch.description : '', order.city.id, order.city.name, order.customer.id, order.customer.name,
          order.shippingAddress.id, order.shippingAddress.postcode
        );
        if (!order.shippingAddress.geopoint) {
          this.orders_without_geopoint.push(order_new);
        } else {
          order_new.lat = order.shippingAddress.geopoint.coordinates[1];
          order_new.lng = order.shippingAddress.geopoint.coordinates[0];
        }
        return order_new;
      });
      this.filter_orders_onload();
    });
    if (this.orders_without_geopoint.length) {
      this.total_unsaved = this.orders_without_geopoint.length;
      this.address_changed = true;
      this.fetch_geopoints(this.orders_without_geopoint);
    } else {
      this._blowhorn_service.hide_loader();
      this.set_map_bounds(true);
    }
  }

  on_map_ready(event) {
    this.map = event;
    this.polygon = new google.maps.Polygon({
      map: this.map,
      path: this.selected_points,
      strokeColor: this.strokeColor,
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: this.strokeColor,
      fillOpacity: 0.35,
      draggable: true,
      visible: true,
      editable: true,
      zIndex: 50
    });
    google.maps.event.addListener(this.polygon, 'click', () => this.show_multi_select_actions = true);
  }

  draw_polygon(event) {
    if (this.multi_selection) {
      this.selected_points.push(event.coords);
      this.polygon.setPath(this.selected_points);
    }
  }

  add_selection_to_list() {
    this.multi_selection_checkbox.reset();
    this.orders.filter(order => {
      return !order.is_added && !order.is_ignore;
    }).forEach(order => {
      if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(order.lat, order.lng), this.polygon)) {
        order.icon_url = this.selected_icon_url;
        order.is_added = true;
        this.selected_orders.push(order);
      }
    });
    this.remove_selection();
    this.set_map_bounds(false);
  }

  close_action_panel() {
    this.show_multi_select_actions = false;
  }

  remove_selection() {
    this.selected_points = [];
    this.polygon.setPath(this.selected_points);
    this.close_action_panel();
  }

  undo_last() {
    this.selected_points = this.selected_points.slice(0, this.selected_points.length - 1);
    if (!this.selected_points.length) {
      this.close_action_panel();
    }
    this.polygon.setPath(this.selected_points);
  }

  update_order(event, order: ShipmentUnassignedOrder) {
    order.lat = event.coords.lat;
    order.lng = event.coords.lng;
    order.is_modified = true;
    order.icon_url = order.is_added ? this.selected_icon_url : this.unsaved_icon_url;
    this.address_changed = true;
  }

  hide_order(order: ShipmentUnassignedOrder) {
    order.is_hidden = true;
  }

  ignore_order(order: ShipmentUnassignedOrder) {
    order.is_ignore = true;
    order.icon_url = this.ignore_icon_url;
    this.show_restore = true;
    this.close_info_window(order);
  }

  restore_addresses() {
    this.orders.filter(order => order.is_ignore).forEach(order => {
      order.is_ignore = false;
      order.icon_url = !order.is_modified ? this.saved_icon_url : this.unsaved_icon_url;
    });
    this.show_restore = false;
    this.set_map_bounds(false);
  }

  restore_order(order: ShipmentUnassignedOrder) {
    order.is_ignore = false;
    order.icon_url = order.is_modified ? this.unsaved_icon_url : this.saved_icon_url;
    this.close_info_window(order);
    this.set_map_bounds(false);
  }

  add_remove_restore_order(order: ShipmentUnassignedOrder) {
    if (order.is_added) {
      this.remove_order(order);
    } else if (order.is_ignore) {
      this.restore_order(order);
    } else {
      this.add_order(order);
    }
  }

  add_order(order: ShipmentUnassignedOrder) {
    if (this.selected_city_id) {
      order.icon_url = this.selected_icon_url;
      order.is_added = true;
      this.selected_orders.push(order);
      this.close_info_window(order);
      this._change_detector.detectChanges();
    } else {
      this.open_message(['Select a city to proceed.'], 'warning');
    }
  }

  remove_order(order: ShipmentUnassignedOrder) {
    const index = this.selected_orders.indexOf(order);
    this.selected_orders.splice(index, 1);
    order.icon_url = order.is_modified ? this.unsaved_icon_url : this.saved_icon_url;
    order.is_added = false;
    this.close_info_window(order);
    this.set_map_bounds(false);
  }

  clear_all_orders() {
    this.selected_orders.forEach(order => {
      order.icon_url = order.is_modified ? this.unsaved_icon_url : this.saved_icon_url;
      order.is_added = false;
    });
    this.selected_orders = [];
    this.remove_driver();
    this.set_map_bounds(false);
  }

  show_info_window(order: ShipmentUnassignedOrder) {
    const open_loc = this.orders.find(loc => loc.is_info_window_open);
    if (open_loc) {
      open_loc.is_info_window_open = false;
    }
    order.is_info_window_open = true;
  }

  hide_info_window(order: ShipmentUnassignedOrder) {
    // order.is_info_window_open = false;
  }

  close_info_window(order: ShipmentUnassignedOrder) {
    order.is_info_window_open = false;
  }

  city_changed(city: IdName) {
    this.selected_city_id = city.id;
    this.selected_city_name = city.name;
    this.reset_customer();
    this.reset_customers_batches_pincodes();
    this.clear_all_orders();
  }

  customer_changed(customer: IdName) {
    this.selected_customer_id = customer.id;
    this.selected_customer_name = customer.name;
    this.reset_date(null);
    this.reset_batches_pincodes();
  }

  date_changed(date) {
    this.reset_batch();
    this.reset_batches_pincodes();
  }

  batch_changed(batch: IdName) {
    this.selected_batch_id = batch.id;
    this.selected_batch_name = batch.name;
    this.reset_pincode();
  }

  reset_cities_customers_batches () {
    const city_map = new Map();
    const customer_map = new Map();
    const batch_map = new Map();
    const pincode_map = new Map();
    this.cities = [];
    this.customers = [];
    this.batches = [];
    this.pincodes = [];
    this.orders.forEach(order => {
      city_map.set(order.city_id, order.city_name);
      customer_map.set(order.customer_id, order.customer_name);
      if (order.batch_id) {
        batch_map.set(order.batch_id, order.batch_id);
      }
      if (order.batch_id) {
        batch_map.set(order.batch_id, order.batch_id);
      }
      if (order.postcode) {
        pincode_map.set(order.postcode, order.postcode);
      }
    });
    city_map.forEach((val, key) => {
      this.cities.push(new IdName(key, val));
    });
    customer_map.forEach((val, key) => {
      this.customers.push(new IdName(key, val));
    });
    batch_map.forEach((val, key) => {
      this.batches.push(new IdName(key, val));
    });
    pincode_map.forEach((val, key) => {
      this.pincodes.push({
        id: val,
        selected: false,
        name: val
      });
    });
  }

  reset_customers_batches_pincodes () {
    const customer_map = new Map();
    const batch_map = new Map();
    const pincode_map = new Map();
    this.customers = [];
    this.batches = [];
    this.pincodes = [];
    this.orders.forEach(order => {
      customer_map.set(order.customer_id, order.customer_name);
      if (order.batch_id) {
        batch_map.set(order.batch_id, order.batch_id);
      }
      if (order.postcode) {
        pincode_map.set(order.postcode, order.postcode);
      }
    });
    customer_map.forEach((val, key) => {
      this.customers.push(new IdName(key, val));
    });
    batch_map.forEach((val, key) => {
      this.batches.push(new IdName(key, val));
    });
    pincode_map.forEach((val, key) => {
      this.pincodes.push({
        id: val,
        selected: false,
        name: val
      });
    });
  }

  reset_batches_pincodes () {
    const batch_map = new Map();
    this.batches = [];
    const pincode_map = new Map();
    this.pincodes = [];
    this.orders.forEach(order => {
      if (order.batch_id) {
        batch_map.set(order.batch_id, order.batch_id);
      }
      if (order.postcode) {
        pincode_map.set(order.postcode, order.postcode);
      }
    });
    batch_map.forEach((val, key) => {
      this.batches.push(new IdName(key, val));
    });
    pincode_map.forEach((val, key) => {
      this.pincodes.push({
        id: val,
        selected: false,
        name: val
      });
    });
  }

  reset_pincodes () {
    const pincode_map = new Map();
    this.pincodes = [];
    this.orders.forEach(order => {
      if (order.postcode) {
        pincode_map.set(order.postcode, order.postcode);
      }
    });
    pincode_map.forEach((val, key) => {
      this.pincodes.push({
        id: val,
        selected: false,
        name: val
      });
    });
  }

  filter_orders() {
    this.orders = this.all_orders.filter(order => {
      let city = true,
          customer = true,
          batch = true,
          date = true,
          postcode = true;
      if (this.selected_city_id) {
        city = parseInt(order.city_id + '', 10) === this.selected_city_id;
      }
      if (this.selected_customer_id) {
        customer = parseInt(order.customer_id + '', 10) === this.selected_customer_id;
      }
      if (this.selected_batch_id) {
        batch = parseInt(order.batch_id + '', 10) === this.selected_batch_id;
      }
      if (this.date_range[0] && this.date_range[1]) {
        const ordered_time = new Date(order.date_placed).getTime();
        const start_time = new Date(this.date_range[0].setHours(0, 0 , 0)).getTime();
        const end_time = new Date(this.date_range[1].setHours(23, 59, 59)).getTime();
        date = start_time <= ordered_time && ordered_time <= end_time;
      }
      if (this.selected_pincodes.length) {
        const orders = this.selected_pincodes.map(item => item.name);
        postcode = orders.includes(order.postcode);
      }
      if (this.show_only_modified) {
        return city && customer && batch && date && postcode && order.is_modified;
      } else {
        return city && customer && batch && date && postcode;
      }
    });
    if (this.orders.find(order => order.is_ignore)) {
      this.show_restore = true;
    } else {
      this.show_restore = false;
    }
    if (this.orders.find(order => order.is_modified)) {
      this.address_changed = true;
    } else {
      this.address_changed = false;
    }
    this.set_map_bounds(true);
    this.remove_selection();
  }

  filter_orders_onload() {
      this.orders = this.all_orders;
      this.reset_cities_customers_batches();
  }

  set_map_bounds(reset = false) {
    this.reset_cluster = true;
    this._change_detector.detectChanges();
    if (reset) {
      this.map_bounds = MapService.dummy_bounds;
      const orders = this.orders.filter(order => !order.is_added && !order.is_ignore);
      if (orders.length) {
        orders.forEach( order => {
          this.map_bounds.extend({lat: order.lat, lng: order.lng});
        });
      } else {
        this.map_bounds.extend( {lat: 0, lng: 0});
        this.open_message(['No orders found. Reset search criteria.'], 'info');
      }
    }
    this.reset_cluster = false;
    this._change_detector.detectChanges();
  }

  reset_customer() {
    this.selected_customer_id = null;
    this.selected_customer_name = 'All Customers';
    this.reset_date(null);
  }

  reset_date(event): void {
    this.date_range = [null, null];
    this.reset_batch();
  }

  reset_batch() {
    this.selected_batch_id = null;
    this.selected_batch_name = 'All Batches';
    this.reset_pincode();
  }

  reset_pincode() {
    this.pincode_label = 'All Pincodes';
    this.selected_pincodes = [];
    this.filter_orders();
    this.reset_pincodes();
  }

  activate_date_clear(event): void {
    this.date_activated = true;
  }

  deactivate_date_clear(event): void {
    this.date_activated = false;
  }

  reset_result_info() {
    this.driver_list = [];
    this.has_next_page = false;
    this.has_previous_page = false;
    this.start_cursor = '';
    this.end_cursor = '';
    this.driver_lookup_message = 'Search for results';
  }

  fetch_drivers(page: string) {
    this._blowhorn_service.show_loader();
    let after_before = '';
    if (this.is_paginated) {
      if (page === 'next') {
        after_before = `first: 50, after: "${this.end_cursor}",`;
      } else if (page === 'previous') {
        after_before = `last: 50, before: "${this.start_cursor}",`;
      } else {
        after_before = 'first: 50,';
      }
    }
    this.reset_result_info();
    this._apollo.watchQuery<any>({
      query: gql`
      {
        activeDriversWithTrips( ${after_before} operatingCity_Name:"${this.selected_city_name}"
         ${this.driver_name_search ? ', name_Icontains:' + '"' + this.driver_name_search + '"' : ''}
         ${this.vehicle_number_search ? ', driverVehicle_Icontains:' + '"' + this.vehicle_number_search + '"' : ''}) {
          pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
          }
          edges {
            node {
              id
              name
              driverVehicle
              tripSet (status_In: "New, In-Progress"){
                edges {
                  node {
                    id
                    status
                    tripNumber
                    order {
                      id
                    }
                  }
                }
              }
            }
          }
        }
      }
      `,
      fetchPolicy: 'network-only'
    }).valueChanges.subscribe(result => {
      const active_drivers = result.data.activeDriversWithTrips;
      if (active_drivers.pageInfo) {
        const page_info = active_drivers.pageInfo;
        this.has_next_page = page_info.hasNextPage;
        this.has_previous_page = page_info.hasPreviousPage;
        this.start_cursor = page_info.startCursor;
        this.end_cursor = page_info.endCursor;
      }
      this.driver_list =  active_drivers.edges.filter(edge => {
            if (edge.node.tripSet.edges.length > 1 || (edge.node.tripSet.edges.length === 1 && edge.node.tripSet.edges[0].node.order)) {
              return false;
            } else {
              return true;
            }
          })
        .map(edge => {
        const trips = edge.node.tripSet.edges.map(_edge => _edge.node.tripNumber);
        return {
          id: edge.node.id,
          name: edge.node.name,
          vehicle: edge.node.driverVehicle,
          trips: trips.length ? trips.join(' | ') : null
        };
      });
      if (!active_drivers.edges.length) {
        this.driver_lookup_message = 'No records found';
      }
      this._blowhorn_service.hide_loader();
      this._change_detector.detectChanges();
    });

  }

  remove_driver() {
    this.selected_driver_id = null;
    this.selected_driver_name = '';
    this.selected_driver_active_trips = null;
  }

  assign_driver(driver: {id: string, name: string, trips: string}) {
    this.selected_driver_id = driver.id;
    this.selected_driver_name = driver.name;
    this.selected_driver_active_trips = driver.trips;
    this.show_confirm = true;
    this.confirm_message = `You are about to assign orders to ${this.selected_driver_name}. ${this.address_changed ? 'Unsaved addresses will be saved. ' : ''}Are you sure?`;
  }

  open_driver_lookup() {
    this.show_driver_lookup = true;
  }

  close_driver_lookup() {
    this.show_driver_lookup = false;
    this.reset_result_info();
    this.driver_name_search = '';
    this.vehicle_number_search = '';
    this.remove_driver();
  }

  open_message(messages: string[], message_type: string) {
    this.message_type = message_type;
    this.messages = messages;
    this.show_message = true;
  }

  hide_message() {
    this.show_message = false;
    this.messages = [];
    this.message_type = '';
  }

  cancel_assign() {
    this.remove_driver();
    this.show_confirm = false;
    this.confirm_message = '';
  }

  proceed() {
    this.show_confirm = false;
    this.confirm_message = '';
    this.save_addresses(this.selected_orders, false);
    const orders = this.selected_orders.map(order => '"' + order.order_no + '"');
    this._blowhorn_service.show_loader();
    const trip_number = this.selected_driver_active_trips ? `tripNumber: "${this.selected_driver_active_trips}", ` : '';
    const mutation = gql`
      mutation {
        createUpdateTrip
        (orderNumbers:[${orders}], ${trip_number} driverId: "${this.selected_driver_id}") {
          messages
          status
        }
    }
    `;
    this._apollo.mutate({
      mutation: mutation
    }).subscribe(result => {
      this.show_message = true;
      this.messages = result.data.createUpdateTrip.messages;
      this.message_type = result.data.createUpdateTrip.status;
      this.close_driver_lookup();
      this.all_orders = this.all_orders.filter(order => {
        return !this.selected_orders.includes(order);
      });
      this.orders = this.orders.filter(order => {
        return !this.selected_orders.includes(order);
      });
      this.selected_orders = [];
      this.set_map_bounds(true);
      this._blowhorn_service.hide_loader();
    });
  }

  save_filtered_addresses() {
    this.save_addresses(this.orders, true);
  }

  save_addresses(orders: ShipmentUnassignedOrder[], show_loading: boolean) {
    const modified_orders = orders.filter(order => order.is_modified);
    if (modified_orders.length) {
      let addresses = '[';
      modified_orders.forEach( (order, index) => {
        if (index > 0) {
          addresses += ', ';
        }
        addresses += `{id: ${order.address_id}, lat:${order.lat}, lng: ${order.lng}}`;
      });
      addresses += ']';
      if (show_loading) {
        this._blowhorn_service.show_loader();
      }
      const mutation = gql`
        mutation {
          updateAddresses(addresses: ${addresses}){
            message
          }
        }
      `;
        this._apollo.mutate({
          mutation: mutation
        }).subscribe(result => {
          this.address_changed = false;
          modified_orders.forEach(order => {
            order.is_modified = false;
            order.icon_url = this.saved_icon_url;
            this.show_message = true;
            this.messages = [result.data.updateAddresses.message];
            this.message_type = 'success';
          });
          this._change_detector.detectChanges();
          if (show_loading) {
            this._blowhorn_service.hide_loader();
          }
        });
    }
  }

  show_hide_modified_orders(event) {
    if (event['checked']) {
      this.show_only_modified = true;
      this.orders = this.orders.filter(order => order.is_modified);
    } else {
      this.show_only_modified = false;
      this.filter_orders();
    }
  }

  enable_multi_selection(event) {
    this.multi_selection = event['checked'];
  }

  pincode_changed(event: {id: number, selected: boolean, name: string}[]) {
    if (event.length) {
      this.pincode_label = event.map(item => item.name).join(', ');
    } else {
      this.pincode_label = 'All Pincodes';
    }
    this.selected_pincodes = event;
    this.filter_orders();
  }

  onRightClick() {
    console.info("Rightclicked!");
  }

  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '15px',
        'left': '15px',
        'width': '150px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
        'z-index': 2
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get customer_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '15px',
        'left': '170px',
        'width': '150px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
        'z-index': 2
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get batch_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '15px',
        'left': '531px',
        'width': '120px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
        'z-index': 2
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get order_nos_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '15px',
        'left': '655px',
        'width': '180px',
        'height': '40px',
        'z-index': 10,
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }
}
