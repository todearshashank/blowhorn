import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departure-sheet-search',
  templateUrl: './departure-sheet-search.component.html',
  styleUrls: ['./departure-sheet-search.component.scss']
})
export class DepartureSheetSearchComponent implements OnInit {

  enabled = false;
  error = false;

  constructor() { }

  ngOnInit() {
  }

  generate_departure_sheet(id: string) {
    if (id) {
      window.open(`/departuresheet/${id.toUpperCase()}`, '_blank');
    } else {
      this.error = true;
    }
  }

  enable_generate(event) {
    this.enabled = event.target.value.trim() ? true : false;
  }

  remove_error() {
    this.error = false;
  }
}
