import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { BlowhornUploadComponent } from '../../../shared/components/blowhorn-upload/blowhorn-upload.component';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MapMarker } from '../../../shared/models/map-marker.model';
import { Constants } from '../../../shared/utils/constants';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { Papa } from 'ngx-papaparse'
import { resolve } from 'url';


@Component({
  selector: 'app-route-optimiser',
  templateUrl: './route-optimiser.component.html',
  styleUrls: ['./route-optimiser.component.scss']
})

export class RouteOptimiserComponent implements OnInit {

  @ViewChild('file_upload', {static: false})
  file_upload: BlowhornUploadComponent;

  map: any;
  latitude_center = 12.9715987;
  longitude_center = 77.5945627;
  zoom_level = 12;
  public upload_tab: boolean = true;
  public map_tab: boolean = !this.upload_tab;
  displayedColumns: string[] = ['order_id', 'stop', 'type', 'stop_sequence'];
  number_vehicles = 3;
  limiting_distance = 60000;
  limiting_duration = 10800;
  JSONData: any;
  headers: any[] = [];
  result: any[] = [];
  text: any;
  api_response: any;
  zoomPosition: google.maps.ControlPosition = google.maps.ControlPosition.TOP_RIGHT;
  trip_api: any;
  public show_message: boolean = true;
  public heading = "Success Message";
  public type = "success";
  public message = "Routes Created Successfully for all Orders Given";
  public error_lists: any[] = [];
  route_directory: any[] = [];
  public capacity = 100000;
  public selectedUnit = "weight";
  public constraints = [
    {
      "name": "Distance",
      "param": "distance"
    },
    {
      "name": "Time",
      "param": "duration"
    }
  ];
  public capacity_const = [
    {
      "name": "Weight",
      "param": "weight"
    },
    {
      "name": "Volume",
      "param": "volume"
    }
  ]
  public selectedConstraint = "distance";
  public constraint_is_distance = true;
  public unit_is_weight = true;

  public renderOptions = {
    suppressMarkers: true,
  };

  markerOptions = {
    origin: {
      icon: Constants.markers.stop,
    },
    destination: {
      icon: Constants.markers.stop,
    },
    waypoints: {
      icon: Constants.markers.stop,
    },
  };

  constructor(
    private _blowhorn_service: BlowhornService,
    private _http_client: HttpClient,
    public _router: Router,
    public _location: Location,
    public dialog: MatDialog,
    private parser: Papa,
  ) { }

  static getCookie(name) {
    if (!document.cookie) {
        return null;
    }

    const xsrfCookies = document.cookie.split(';')
        .map(c => c.trim())
        .filter(c => c.startsWith(name + '='));

    if (xsrfCookies.length === 0) {
        return null;
    }

    return decodeURIComponent(xsrfCookies[0].split('=')[1]);
  }

  static get http_headers(): {} {
      const token = this.getCookie('csrftoken');
      return {
          headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Access-Control-Allow-Origin': '*',
              'X-CSRFToken': token
          })
      };
  }

  http_options = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  ngOnInit() {

  }

  file_changed(file: File) {

  }

  onKeyVehicleCapacity(capacity: any){
    this.capacity = capacity.target.value;
  }

  onKeyNumberOfVehicles(vehicles: any){
    this.number_vehicles = vehicles.target.value;
  }


  onKeyLimitingDistance(distance: any){
    this.limiting_distance = distance.target.value*1000;
  }

  onKeyLimitingDuration(duration: any){
    this.limiting_duration = duration.target.value*3600;
  }

  changeConstraint(value: any){
    if(this.selectedConstraint === "duration"){
      this.constraint_is_distance = false;
    }
    else{
      this.constraint_is_distance = true;
    }
  }

  changeCapacityUnit(value: any){
    if(this.selectedUnit === "volume"){
      this.unit_is_weight = false;
    }
    else{
      this.unit_is_weight = true;
    }
  }

  on_map_ready(map: any) {
    this.map = map;
  }

  select_marker(stop: any){
  }

  get_icon_url(stop_sequence: number, route: any): string {
    if (stop_sequence) {
      return stop_sequence < route.elements.length ?
        Constants.markers.stop :
        Constants.markers.pickup;
    } else {
      return Constants.markers.pickup;
    }
  }

  close(){
    this.show_message = !this.show_message;
  }

  openDialog(route: any){
    this._blowhorn_service.show_loader()
    this._blowhorn_service.hide_loader()
    const dialogRef = this.dialog.open(DialogPickDriver, {
      height: '30%',
      width: '50%',
      data: {
        route: route,
        selected_driver: {}
      }
    });

    dialogRef.afterClosed()
    .subscribe(
      result => {
        if(result) {
          let trip_details = {
            "route": route,
            "driver": result
          };
          console.log(trip_details);
          this.generateTrip(trip_details);
        }
        else {
          let trip_details = {
            "route": route
          };
          console.log(trip_details);
        }

      }
    );
  }

  generateRoute(route: any){
    console.log(route)
    this._blowhorn_service.show_loader();
    let waypoints: any[] = [];
    for (let i=1; i<route.elements.length-1; i++){
      let temp = {
        location:
          {
            lat: route.elements[i].stop_coordinates.latitude,
            lng: route.elements[i].stop_coordinates.longitude }
        };
      waypoints.push(temp);
    }
    if(waypoints.length<=23){
      let temp_object = {
        origin: { lat: route.elements[0].stop_coordinates.latitude, lng:route.elements[0].stop_coordinates.longitude },
        destination: { lat: route.elements[route.elements.length-1].stop_coordinates.latitude, lng:route.elements[route.elements.length-1].stop_coordinates.longitude },
        waypoints: waypoints,
        visible: true
      }
      this.route_directory.push(temp_object);
    }
    else{
      let limit = Math.floor((waypoints.length+2)/25);
      for(let i=0; i<limit; i++){
        let temp_object = {
          origin: { lat: route.elements[25*i].stop_coordinates.latitude, lng:route.elements[25*i].stop_coordinates.longitude },
          destination: {lat: route.elements[(25*(i+1))].stop_coordinates.latitude, lng:route.elements[(25*(i+1))].stop_coordinates.longitude},
          waypoints: waypoints.slice((25*i), (25*(i+1))),
          visible: true
        }
        this.route_directory.push(temp_object);
      }
      let temp_object = {
        origin: { lat: route.elements[25*limit].stop_coordinates.latitude, lng:route.elements[25*limit].stop_coordinates.longitude },
        destination: { lat: route.elements[Math.min((25*limit) + 24, route.elements.length-1)].stop_coordinates.latitude, lng:route.elements[Math.min((25*limit) + 24, route.elements.length-1)].stop_coordinates.longitude},
        waypoints: waypoints.slice((25*limit), Math.min((25*(limit+1)), route.elements.length-1)),
        visible: true
      }
      this.route_directory.push(temp_object);
    }
    console.log(this.route_directory);
    this._blowhorn_service.hide_loader();
  }

  generateTrip(trip_details: any){
    this._blowhorn_service.show_loader();
    this._http_client.post(Constants.urls.API_CREATE_BLANKET_TRIP, trip_details, RouteOptimiserComponent.http_headers)
      .toPromise()
      .then(
        res => {
          this.trip_api = res;
          alert("Note: " + this.trip_api.trip_id);
          this._blowhorn_service.hide_loader();
        },
        err => {
          alert("Trip could not be created due to internal server error! Please Try Again later!")
          this._blowhorn_service.hide_loader();
        }

      )
  }

  clearRoutes(){
    for(let i=0; i<this.route_directory.length; i++){
      this.route_directory[i].visible = false;
    }
  }

  refreshRoutes(){
    this._router.navigateByUrl("/template", {skipLocationChange: true}).then(() =>{
      this._router.navigate([decodeURI(this._location.path())]);
    });
  }


  async upload_file(files: any) {
    this.file_upload.disable()
    this._blowhorn_service.show_loader();

    await new Promise(
      (resolve, reject)=> {
        this.parser.parse(files[0], {
          header: true,
          dynamicTyping: true,
          skipEmptyLines: 'greedy',
          worker: false,
          complete: (result,file) => {
            this.JSONData = result.data;
            resolve('Done')
          }
        });
      }
    );
    let params_json = {
      "vehicles": this.number_vehicles,
      "distance": this.limiting_distance,
      "duration": this.limiting_duration,
      "constraint": this.selectedConstraint,
      "capacity": this.capacity,
      "capacity_unit": this.selectedUnit,
      "rows": this.JSONData
    };
    this._http_client.post(Constants.urls.API_ROUTES_OPT, params_json, this.http_options)
    .toPromise()
    .then(
      (res) => {
        this.upload_tab = false;
        this.map_tab = !this.upload_tab;
        this.api_response = res;
        this.error_lists = this.api_response["non_operational_orders"];
        if(this.error_lists.length){
          this.heading = "Error Message"
          this.type = "error"
          this.message = "Some Routes Could not be Created. Please Check the mentioned orders for operational area coverage in admin panel."
        }
        this._blowhorn_service.hide_loader();
        console.log(this.api_response);

      },
      (err) => {
        this._blowhorn_service.hide_loader();
        if(err.error.type) {
          alert(err.error.type+": "+err.error.message);
        }
        else {
          alert("Unexpected Error! Try reducing the number of orders or try again later!")
        }
        console.log(err);
        this.file_upload.enable();
      }
    );
  }
}

export interface Driver {
  id: number;
  name: string;
  driver_vehicle: string;
  is_blowhorn_driver: boolean;
  is_marketplace_driver: boolean;
  contract: number;
}

@Component({
  selector: 'pick-driver-dialog',
  templateUrl: './dialog-pickdriver.component.html',
  styleUrls: ['./dialog-pickdriver.component.scss']
})
export class DialogPickDriver {

  constructor(

    public dialogRef: MatDialogRef<DialogPickDriver>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _http_client: HttpClient,
  ) { }

  driverControl: FormControl = new FormControl();

  drivers: Driver[] = this.data.driver_list;

  isLoading = false;

  filteredDrivers: Observable<Driver[]>;

  search_term: any;

  ngOnInit() {
    this.filteredDrivers = this.driverControl.valueChanges
    .pipe(
      debounceTime(500),
      tap(() => {
        this.isLoading = true;
      }),
      switchMap(value => this._http_client.get<Driver[]>(Constants.urls.API_LIST_ACTIVE_DRIVERS, { headers: new HttpHeaders({'Content-Type':  'application/json'}), params: { 'data' : value } })
        .pipe(
          finalize(() => {
            this.isLoading = false
          }),
        )
      )
    )
  }

  filter(val: string): Driver[] {
    return this.drivers.filter(driver =>
      driver.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  checkDriver() {
    if(!this.data.selected_driver || !(this.data.selected_driver instanceof Object) || !(this.driverControl.value instanceof Object)){
      this.driverControl.setValue("");
      this.data.selected_driver = {};
    }
  }

  displayFn(driver: Driver): string {
    return driver && driver.name ? driver.name + " || ID: " + driver.id + " || Vehicle Number: " + driver.driver_vehicle : "";
  }

  selectDriver(driver: any){
    this.data.selected_driver = driver;
  }

  onNoClick(): void {
    this.data.selected_driver = {};
    this.dialogRef.close();
  }

}
