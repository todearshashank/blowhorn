import { Component, OnInit, OnDestroy } from '@angular/core';
import { LivetrackingService } from '../../../../shared/services/livetracking.service';
import { WhatsNewService } from '../../../../shared/services/whats-new.service';
import { Constants } from '../../../../shared/utils/constants';
import { Subscriber, Subscription } from 'rxjs';

@Component({
    selector: 'app-filter-sidebar',
    templateUrl: './filter-sidebar.component.html',
    styleUrls: ['./filter-sidebar.component.scss']
})
export class FilterSidebarComponent implements OnInit, OnDestroy {

    // @Input('driver_locations')
    // driver_locations: any = [];

    version = Constants.version_info.livetracking.version;

    vehicle_class_group_name = 'vehicle_class';
    driver_status_group_name = 'driver_status';
    fixed_driver_group_name = 'fixed_driver';

    driver_marker_subscription: Subscription;
    vehicle_class_subscription: Subscription;
    driver_status_subscription: Subscription;
    fixed_driver_subscription: Subscription;

    collapsedFilter = false;
    showStatusFilter = true;
    showVehicleClassFilter = true;
    showFixedDriverBoolFilter = true;
    showCovidPassFilter = true;

    updated_markers: any = {};
    driver_statuses: any = [];
    vehicle_classes: any = [];
    show_only_fixed_drivers = false;
    selected_covid_filter = 'both';

    selected_vehicle_class_count: any = {};
    selected_driver_status_count: any = {};
    fixed_driver_count = 0;
    non_fixed_drivers_count = 0;
    total_vehicles = 0;

    fixedDriverFilterInfo = {
        'name': 'Non-fixed drivers', checked: false
    };

    covidPassFilterInfo = [
        {label: 'Yes', val: 'yes', count: 0},
        {label: 'No', val: 'no', count: 0},
        {label: 'Both', val: 'both', count: 0},
    ];
    covid_filter_length = this.covidPassFilterInfo.length;

    constructor(
        private livetracking_service: LivetrackingService,
        private whats_new_service: WhatsNewService,
    ) { }

    ngOnInit() {

        this.driver_marker_subscription = this.livetracking_service
            .driver_markers
            .subscribe(markers => {
                this.updated_markers = markers;
                this.update_filter_count();
            });

        this.driver_status_subscription = this.livetracking_service
            .filter_driver_status
            .subscribe(status => {
                if (!this.driver_statuses.length) {
                    this.driver_statuses = status;
                    this.init_driver_status_count();
                } else {
                    this.update_filter_data(this.driver_statuses, status);
                }
            });

        this.livetracking_service
            .filter_vehicle_class
            .subscribe(vclass => {
                if (!this.vehicle_classes.length) {
                    this.vehicle_classes = vclass;
                    this.init_vehicle_class_count();
                } else {
                    this.update_filter_data(this.vehicle_classes, vclass);
                }
            });
    }

    get checkbox_styles(): {} {
        return {
            'checkbox-label-styles': {
                color: '#8b8d8e',
                'font-size': '14px',
                'line-height': '21px'
            },
            'checkbox-group-styles': {},
            'checkbox-input-styles': {}
        };
    }

    update_filter_data(source_array: any, data: any) {
        let elem_found = false;
        for (let i of source_array) {
            if (i.name === data.name) {
                i.checked = data.checked;
                elem_found = true;
            }
        }
        if (!elem_found) {
            source_array.push(data);
        }
    }

    init_vehicle_class_count() {
        this.vehicle_classes.forEach(element => {
            this.selected_vehicle_class_count[element.name] = 0;
        });
    }

    init_driver_status_count() {
        this.driver_statuses.forEach(element => {
            this.selected_driver_status_count[element.name] = 0;
        });
    }

    public vehicle_class_changed(vclass: any) {
        this.livetracking_service.update_filter_vehicle_class(vclass);
        this.update_filter_count();
    }

    public fixed_driver_filter_changed(checked: any) {
        this.livetracking_service.update_filter_fixed_driver(checked);
        this.update_filter_count();
    }

    public covid_pass_filter_changed(val: string) {
        this.selected_covid_filter = val;
        this.livetracking_service.update_filter_covid_pass(val);
        this.update_filter_count();
    }

    public driver_status_changed(status: any) {
        this.livetracking_service.update_filter_driver_status(status);
        this.update_filter_count();
    }

    update_filter_count() {
        this.init_driver_status_count();
        this.init_vehicle_class_count();
        this.total_vehicles = 0;
        this.fixed_driver_count = 0;
        this.non_fixed_drivers_count = 0;
        this.reset_covid_pass_count();
        for (let i in this.updated_markers) {
            let item = this.updated_markers[i];
            if (item.marker.visible) {
                this.selected_vehicle_class_count[item.profile.vehicle_class] += 1;
                this.total_vehicles +=1;
                let extras = item.extras;
                if (extras.booking_type) {
                    this.selected_driver_status_count[extras.booking_type] += 1;
                } else {
                    this.selected_driver_status_count['idle'] += 1;
                }
                if (extras) {
                    if (!extras.is_fixed_driver) {
                        this.non_fixed_drivers_count += 1;
                    }
                    if (extras.has_covid_pass) {
                        this.covidPassFilterInfo[0].count += 1;
                    } else {
                        this.covidPassFilterInfo[1].count += 1;
                    }
                    this.covidPassFilterInfo[2].count += 1;
                }
            }
        }
        this.fixed_driver_count = this.total_vehicles - this.non_fixed_drivers_count;
    }

    showVersionLog(): void {
        this.whats_new_service.displayDialog = true;
    }

    reset_covid_pass_count() {
        for (let i = 0; i < this.covid_filter_length; i++) {
            this.covidPassFilterInfo[i].count = 0;
        }
    }

    ngOnDestroy() {
        this.driver_marker_subscription.unsubscribe();
        this.vehicle_class_subscription.unsubscribe();
        this.driver_status_subscription.unsubscribe();
        this.fixed_driver_subscription.unsubscribe();
    }
}
