import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DriverProfileComponent } from '../driver-profile/driver-profile.component';
import { DriverTrackingDetail } from '../../../../shared/models/driver-tracking-detail.model';
import { LivetrackingService } from '../../../../shared/services/livetracking.service';
import { BlowhornService } from '../../../../shared/services/blowhorn.service';

@Component({
    selector: 'app-info-window',
    templateUrl: './info-window.component.html',
    styleUrls: ['./info-window.component.scss'],
})
export class InfoWindowComponent implements OnInit, OnChanges {

    @Input('locDetail')
    locDetail: DriverTrackingDetail;

    @Input('vehicle_icons')
    vehicle_icons: any = [];

    public truck_icon = '';
    gps_human_readable = '';

    constructor(
        private modalService: NgbModal,
        private livetracking_service: LivetrackingService,
        public blowhorn_service: BlowhornService
    ) { }

    ngOnInit() {
    }

    ngOnChanges() {
        this.set_gps_timestamp_color();
        let mTime = this.locDetail.location.mTime;
        if (mTime) {
            this.gps_human_readable = this.livetracking_service.get_gps_timestamp_human_readable_format(mTime);
        }
        this.locDetail.profile.truck_icon = this.vehicle_icons[this.locDetail.profile.vehicle_class] ||
            `/static/${this.blowhorn_service.current_build}/assets/truck/Ace.png`;
    }

    set_gps_timestamp_color(): void {
        let timestamp: any = new Date(this.locDetail.location.mTime);
        let now: any = new Date();
        this.locDetail.location.is_late_update = ((now - timestamp) / 1000 > 240);
    }

    open_driver_profile(locDetail: DriverTrackingDetail) {
        const modalRef = this.modalService.open(DriverProfileComponent,
            { size: 'lg', windowClass: 'lg-dialog' });
        modalRef.componentInstance.locDetail = this.locDetail;
    }
}
