import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-order-statistics-rightbar',
  templateUrl: './order-statistics-rightbar.component.html',
  styleUrls: ['./order-statistics-rightbar.component.scss']
})
export class OrderStatisticsRightbarComponent implements OnInit {

  @Input('order_statuses') public order_statuses: any = [];
  public total_orders = 0;
  showOrderCount = false;

  constructor() { }

  ngOnInit() {
    this.order_statuses = [
      {'status': 'Order-New', 'count': 900},
      {'status': 'In-Progress', 'count': 5001},
      {'status': 'Delivered', 'count': 700},
      {'status': 'Cancelled', 'count': 100},
    ];
    this.total_orders = this.order_statuses.reduce((sum, item) => sum + item.count, 0);
  }

  getStatusCardBackground(status): string {
    switch (status) {
      case 'Order-New':
        return 'skyblue';
      case 'In-Progress':
        return 'blue';
      case 'Delivered':
        return 'green';
      case 'Cancelled':
        return 'red';
    }
  }

}
