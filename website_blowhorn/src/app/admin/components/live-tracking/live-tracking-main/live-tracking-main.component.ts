import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { ToastrService } from 'ngx-toastr';
import { LivetrackingService } from '../../../../shared/services/livetracking.service';
import { MapMarker } from '../../../../shared/models/map-marker.model';
import { Constants } from '../../../../shared/utils/constants';
import { NameValuePipe } from '../../../../shared/pipes/name-value.pipe';
import { BlowhornService } from '../../../../shared/services/blowhorn.service';
import { DriverLocationDetail } from '../../../../shared/models/location.model';
import { DriverTrackingDetail } from '../../../../shared/models/driver-tracking-detail.model';
import { SortPipe } from '../../../../shared/pipes/sort.pipe';
import { MapService } from '../../../../shared/services/map.service';

declare var google: any;

/**********************************************
 * Parameters to process location updates and
 * filter out noise */
const MIN_DISTANCE_DELTA_METERS = 10;
const MIN_TIMEDELTA_MS = 1000;
const MAX_SPEED_KMPH = 100;
const MAX_ACCURACY_METER = 40;
const AUTO_ZOOMOUT_SECONDS = 60;
/*********************************************/

@Component({
    selector: 'app-live-tracking-main',
    templateUrl: './live-tracking-main.component.html',
    styleUrls: ['./live-tracking-main.component.scss'],
    providers: [LivetrackingService, SortPipe],
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LiveTrackingMainComponent implements OnInit {

    public app_title = Constants.version_info.livetracking.app_title;
    public app_code: string = Constants.version_info.livetracking.app_code;
    public version: string = Constants.version_info.livetracking.version;
    public whats_new: any = Constants.version_info.livetracking.whats_new;
    navbar_styles = {
        padding: '0px',
        boxShadow: '0px 0px 0px 0px transparent',
        border: 'none',
        height: '50px',
        top: '0px',
    };
    link_styles = {
        'justify-content': 'center'
    };
    links = [{link: '', text: 'Live Tracking'}];

    public selected_city_id = 0;
    public selected_city_name = '';

    public selected_customer_id = 0;
    public selected_customer_name = '';

    public selected_driver_id = 0;
    public selected_driver_name = '';

    public selected_vehicle_classes: any = [];
    public selected_driver_status: any = [];
    public show_non_only_fixed_drivers = false;
    public covid_pass_drivers = 'both';

    public cities: any = [];
    public order_statuses: any = [];
    public driver_statuses: any = [];
    public driver_categories: any = [];
    public vehicle_classes: any = [];
    public vehicle_icons: any = [];
    public customers: any = [];
    public customer_list: any = [];
    public drivers: any = [];
    public profile_not_exists: any = [];

    public driver_locations: any = {};
    updatedLocations: any = [];
    trip_numbers: string[] = [];

    map: any;
    latitude_center = 12.9715987;
    longitude_center = 77.5945627;
    zoom_level = 14;
    battery_info: any = {};

    constructor(
        private route: ActivatedRoute,
        private firebase_db: AngularFireDatabase,
        // private cd: ChangeDetectorRef,
        private livetracking_service: LivetrackingService,
        private nameValuePipe: NameValuePipe,
        private sortBy: SortPipe,
        public _blowhorn_service: BlowhornService,
        private toastr: ToastrService,
        private _map_service: MapService
    ) {
        route.data.subscribe(data => {
            this.initialize_live_updates();
            data = data.live_tracking;
            this.cities = data.cities;
            this.order_statuses = data.order_statuses;
            this.driver_statuses = data.driver_statuses.map(item => {
                return { name: item, checked: true };
            });
            this.selected_driver_status = data.driver_statuses;
            this.livetracking_service.update_filter_driver_status(this.driver_statuses);

            // this.driver_categories = data.driver_category;
            this.vehicle_icons = data.vehicle_icons;
            this.vehicle_classes = Object.keys(this.vehicle_icons).map(item => {
                return { name: item, checked: true };
            });
            this.selected_vehicle_classes = this.vehicle_classes.map(item => {
                return item.name;
            });
            this.livetracking_service.update_filter_vehicle_class(this.vehicle_classes);
        },
        err => {
            console.error(err);
        });
    }

    ngOnInit() {
        this.livetracking_service.get_driver_locations().subscribe(data => {
            this.driver_locations = data;
            this.get_trip_details();
        }, err => {
            console.error(err);
        });

        this.livetracking_service
            .filter_driver_status
            .subscribe(status => {
                if (status.checked) {
                    this.selected_driver_status.push(status.name);
                } else {
                    const index = this.selected_driver_status.indexOf(status.name);
                    if (index === -1) {
                        return;
                    }
                    this.selected_driver_status.splice(index, 1);
                }

                if (status) {
                    this.updatedLocations = this.updatedLocations.map(item => {
                        if (item.marker) {
                            item.marker.visible = this.is_marker_visible(item);
                        }
                        return item;
                    });
                    this.livetracking_service.update_driver_markers(this.updatedLocations);
                }
            });

        this.livetracking_service
            .filter_vehicle_class
            .subscribe(vclass => {
                if (vclass.checked) {
                    this.selected_vehicle_classes.push(vclass.name);
                } else {
                    const index = this.selected_vehicle_classes.indexOf(vclass.name);
                    if (index === -1) {
                        return;
                    }
                    this.selected_vehicle_classes.splice(index, 1);
                }

                if (vclass) {
                    this.updatedLocations = this.updatedLocations.map(item => {
                        if (item.marker) {
                            item.marker.visible = this.is_marker_visible(item);
                        }
                        return item;
                    });
                    this.livetracking_service.update_driver_markers(this.updatedLocations);
                }
            });

        this.livetracking_service
            .filter_fixed_driver
            .subscribe(filter_val => {
                this.show_non_only_fixed_drivers = filter_val.checked;
                if (filter_val) {
                    this.updatedLocations = this.updatedLocations.map(item => {
                        if (item.marker) {
                            item.marker.visible = this.is_marker_visible(item);
                        }
                        return item;
                    });
                    this.livetracking_service.update_driver_markers(this.updatedLocations);
                }
            });

        this.livetracking_service
            .filter_covid_pass
            .subscribe(filter_val => {
                this.covid_pass_drivers = filter_val;
                if (filter_val) {
                    this.updatedLocations = this.updatedLocations.map(item => {
                        if (item.marker) {
                            item.marker.visible = this.is_marker_visible(item);
                        }
                        return item;
                    });
                    this.livetracking_service.update_driver_markers(this.updatedLocations);
                }
            });
    }

    // Filters
    get city_styles(): {} {
        return {
            'dropdown-styles': {
                position: 'absolute',
                top: '20px',
                left: '25px',
                width: '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
                'z-index': 2
            },
            'dropdown-menu-styles': {
                width: '100%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get customer_styles(): {} {
        return {
            'dropdown-styles': {
                position: 'absolute',
                top: '20px',
                left: '235px',
                width: '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
                'z-index': 2
            },
            'dropdown-menu-styles': {
                width: '100%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get driver_styles(): {} {
        return {
            'dropdown-styles': {
                position: 'absolute',
                top: '20px',
                left: '445px',
                width: '300px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
                'z-index': 2
            },
            'dropdown-menu-styles': {
                width: '100%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    city_changed(event: { name: string, id: number }): void {
        if (this.selected_city_id !== event.id) {
            this.selected_city_name = event.name;
            this.selected_city_id = event.id;
            this.reset_filter_values();
            this.reset_customer();
            this.reset_driver();
            this.apply_filters();
        }
    }

    customer_changed(event: { name: string, id: number }): void {
        if (this.selected_customer_id !== event.id) {
            this.selected_customer_name = event.name;
            this.selected_customer_id = event.id;
            this.reset_filter_values();
            this.reset_driver();
            this.apply_filters();
        }
    }

    driver_changed(event: { name: string, id: number }): void {
        if (this.selected_driver_id !== event.id) {
            this.selected_driver_name = event.name;
            this.selected_driver_id = event.id;
            if (this.selected_driver_id) {
                this.zoom_selected_driver(this.selected_driver_id);
            } else {
                this.fit_bound();
            }
            // this.apply_filters();
        }
    }

    show_info_window(locDetail: DriverTrackingDetail) {
        const open_loc = this.updatedLocations.find(loc => loc.marker.infowindow);
        if (open_loc) {
            open_loc.marker.infowindow = false;
        }
        this.select_marker(locDetail);
    }

    setZoom(lat: any, lng: any) {
        const location = new google.maps.LatLng(lat, lng);
        this.map.setCenter(location);
        this.map.setZoom(20);
    }

    zoom_selected_driver(driver_id: number): void {
        const locDetail = this.updatedLocations.find(_loc => _loc.driver_id === driver_id);
        this.setZoom(locDetail.location.mLatitude, locDetail.location.mLongitude);
        this.show_info_window(locDetail);
    }

    get_driver_option(item: any): {} {
        return {
            name: `${item.profile.name} - ${item.profile.vehicle_number}`,
            id: item.driver_id
        };
    }

    reset_filter_values(): void {
        this.drivers = [];
        for (let k in this.driver_locations) {
            let item = this.driver_locations[k];
            if ((this.selected_city_id ? item.location.city === this.selected_city_name : true) &&
                (this.selected_customer_id ? item.extras.customer_pk === this.selected_customer_id : true)) {
                this.drivers.push(this.get_driver_option(item));
            }
        }
        this.sortBy.transform(this.drivers, ['name']);
    }

    reset_city(): void {
        this.selected_city_id = null;
        this.selected_city_name = 'All Cities';
    }

    reset_customer(): void {
        this.selected_customer_id = null;
        this.selected_customer_name = 'All Customers';
    }

    reset_driver(): void {
        this.selected_driver_id = null;
        this.selected_driver_name = 'All Drivers';
    }

    apply_filters(): void {
        this.updatedLocations = this.updatedLocations.map(item => {
            item.marker.visible = this.is_marker_visible(item);
            return item;
        });
        this.livetracking_service.update_driver_markers(this.updatedLocations);
        this.fit_bound();
    }

    get_driver_status(booking_type: string): boolean {
        return (this.selected_driver_status.indexOf(booking_type) !== -1) ||
            (!booking_type && this.selected_driver_status.indexOf('idle') !== -1);
    }

    get_trip_details(): void {
        this.livetracking_service.get_trip_details().subscribe(data => {
            this.get_resource_allocation_details(data);
            this._blowhorn_service.hide_loader();
            this.toastr.success('Successfully fetched the ongoing trip details');
        },
        err => {
            console.error(err);
            this._blowhorn_service.hide_loader();
            let errorMessage = typeof (err.error) === 'string' ? err.error : err.error.detail;
            this.toastr.error(errorMessage);
        });
    }

    get_resource_allocation_details(trip_data: any): void {
        this.livetracking_service.get_resource_allocation_info().subscribe(data => {
            for (let k in this.driver_locations) {
                let res_details = data.find(item => item.driver_id === parseInt(k));
                let item = this.driver_locations[k];
                let extras = item.extras;
                item.extras = Object.assign({}, trip_data[k], res_details);
                item.extras['is_fixed_driver'] = res_details ? true : false;
                item.extras['has_covid_pass'] = extras['has_covid_pass'];
            }
            this.create_markers();
        },
        err => {
            console.error(err);
            let errorMessage = typeof (err.error) === 'string' ? err.error : err.error.detail;
            this.toastr.error(errorMessage);
        });
    }

    get_show_only_fixed_driver_flag(item: any): boolean {
        return this.show_non_only_fixed_drivers ? (item.extras && !item.extras.is_fixed_driver) : true;
    }

    get_covid_pass_status(has_covid_pass: boolean): boolean {
        return (this.covid_pass_drivers == 'both') ||
            (has_covid_pass && this.covid_pass_drivers === 'yes') ||
            (!has_covid_pass && this.covid_pass_drivers == 'no');
    }

    is_marker_visible(item: any): boolean {

        return (this.selected_city_id ? item.location.city === this.selected_city_name : true) &&
            (this.selected_customer_id ? item.extras.customer_pk === this.selected_customer_id : true) &&
            (this.selected_driver_id ? item.driver_id === this.selected_driver_id : true) &&
            (this.selected_vehicle_classes.indexOf(item.profile.vehicle_class) !== -1) &&
            this.get_show_only_fixed_driver_flag(item) &&
            this.get_covid_pass_status(item.extras.has_covid_pass) &&
            this.get_driver_status(item.extras.booking_type);
    }

    create_marker(item: any): MapMarker {
        const icon_url = this.get_marker_icon(item.extras.trip_number);
        this.add_customer(item);
        this.drivers.push(this.get_driver_option(item));
        return new MapMarker(item.profile, true, icon_url);
    }

    create_markers(): void {
        for (const k in this.driver_locations) {
            const item = this.driver_locations[k];
            item['marker'] = this.create_marker(item);
            this.updatedLocations.push(item);
        }
        this.fit_bound();
        this.livetracking_service.update_driver_markers(this.updatedLocations);
        this.sortBy.transform(this.drivers, ['name']);
        this.sortBy.transform(this.customers, ['name']);
    }

    fit_bound() {
        const bounds: any = new google.maps.LatLngBounds();
        for (const i in this.updatedLocations) {
            const loc = this.updatedLocations[i];
            if (loc.marker && loc.marker.visible) {
                bounds.extend({
                    lat: parseFloat(loc.location.mLatitude),
                    lng: parseFloat(loc.location.mLongitude)
                });
            }
        }
        this.map.fitBounds(bounds);
        google.maps.event.trigger(this.map, 'resize');
    }

    on_map_ready(map: any) {
        this.map = map;
    }

    select_marker(driverLoc: DriverTrackingDetail): void {
        driverLoc.marker.infowindow = true;
        this.set_selected_marker_icon(driverLoc);
    }

    set_selected_marker_icon(driverLoc: DriverTrackingDetail) {
        let battery_info = driverLoc.device_details.battery_info;
        const battery_color_details = this.livetracking_service.get_battery_icon(battery_info.batteryLevel);
        battery_info.style_class = battery_color_details['style_class'];
        battery_info.icon = battery_color_details['icon'];
        driverLoc.marker.icon_url = Constants.driver_icons.selected;
    }

    get_marker_icon(trip_number: string) {
        return !trip_number ? Constants.driver_icons.idle : Constants.driver_icons.occupied;
    }

    reset_selected_marker_icon(locDetail: DriverTrackingDetail) {
        locDetail.marker.icon_url = this.get_marker_icon(locDetail.extras.trip_number);
        locDetail.marker.infowindow = false;
    }

    add_customer(item: any) {
        if (item.extras && item.extras.customer_name && item.extras.customer_pk &&
            this.customer_list.indexOf(item.extras.customer_pk) === -1) {
            this.customer_list.push(item.extras.customer_pk);
            this.customers.push({ name: item.extras.customer_name, id: item.extras.customer_pk });
        }
    }

    initialize_live_updates(): void {
        this.firebase_db
            .list('/active_drivers')
            .stateChanges(['child_added', 'child_changed', 'child_removed']).subscribe(data => {
                let modified_data: any = data.payload.val();
                if (modified_data != null) {
                    modified_data = typeof(modified_data) === 'string' ? JSON.parse(modified_data) : modified_data;
                    switch (data.type) {
                        case 'child_removed':
                            this.remove_driver(modified_data);
                            break;

                        case 'child_added':
                            this.add_driver(modified_data);
                            break;

                        case 'child_changed':
                            this.update_driver(modified_data);
                            break;

                        default:
                            console.log('Stale update');
                    }
                }
            });
    }

    add_driver(driverLocation: any): void {
        console.log('driverLocation', driverLocation);
    }

    update_driver(modified_data: any): void {
        const driver_id = parseInt(modified_data.driver_id, 0);
        const locDetailIndex = this.updatedLocations.findIndex(item =>
            item.driver_id === driver_id);

        if (locDetailIndex === -1) {
            return;
        }
        let locDetail = this.updatedLocations[locDetailIndex];
        const newLoc = Object.assign({}, locDetail);
        let marker = newLoc ? newLoc.marker : null;
        if (marker) {
            marker.visible = this.is_marker_visible(newLoc);
            if (this.isNewLocationNewer(newLoc.location, modified_data)) {
                let newLocation = new DriverLocationDetail();
                newLocation.createByJson(modified_data);
                newLoc.location = newLocation;
                if (modified_data.battery_info) {
                    newLoc.device_details.battery_info = modified_data.battery_info;
                }
                if (modified_data.location && modified_data.location.event) {
                    newLoc.extras.event = modified_data.location.event;
                }
                this.driver_locations[driver_id].location = modified_data;
            }
        }
        newLoc.mTime = modified_data['mTime'];
        this.updatedLocations.splice(locDetailIndex, 1, newLoc);
    }

    remove_driver(loc: any): void {
        const driver_id = parseInt(loc.driver_id, 10);
        delete this.driver_locations[driver_id];
        const mIndex = this.updatedLocations.findIndex(item =>
            item.driver_id === driver_id);
        this.updatedLocations.splice(mIndex, 1);
    }

    /* Sanity check if latest received location is actually up to date.*/
    private isNewLocationNewer(old_location: any, new_location: any) {
        if (new_location == null) {
            return false;
        }

        if (old_location == null) {
            return true;
        } else if (new_location && new_location.mAccuracy > MAX_ACCURACY_METER) {
            return false;
        }

        const distance_meters = google.maps.geometry.spherical.computeDistanceBetween(
            new google.maps.LatLng(parseFloat(old_location.mLatitude), parseFloat(old_location.mLongitude)),
            new google.maps.LatLng(parseFloat(new_location.mLatitude), parseFloat(new_location.mLongitude)),
        );
        const timedelta_ms: number = new_location.mTime - old_location.mTime;
        const speed_kmph: number = (distance_meters * 60 * 60) / timedelta_ms;
        const newer: boolean = (timedelta_ms > MIN_TIMEDELTA_MS &&
            distance_meters > MIN_DISTANCE_DELTA_METERS &&
            speed_kmph < MAX_SPEED_KMPH
        );
        return newer;
    }
}
