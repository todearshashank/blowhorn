import { Constants } from './../../../../shared/utils/constants';
import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MapMarker } from '../../../../shared/models/map-marker.model';
import { LivetrackingService } from '../../../../shared/services/livetracking.service';
import { DriverTrackingDetail } from '../../../../shared/models/driver-tracking-detail.model';
import { isInteger } from '@ng-bootstrap/ng-bootstrap/util/util';

@Component({
    selector: 'app-driver-profile',
    templateUrl: './driver-profile.component.html',
    styleUrls: ['./driver-profile.component.scss']
})
export class DriverProfileComponent implements OnInit {

    locDetail: DriverTrackingDetail = null;
    message = '';
    showAlert = false;
    disableActions = false;
    gps_human_readable = '';
    avatarUrl = '';
    buttonList = {
        left: [
            // {label: 'Duty On', action: 'duty_on'},
            { label: 'Request Location', action: 'request_location' },
            { label: 'Logout App', action: 'logout' },
            // {label: 'Remove Idle Status', action: 'remove_idle'},
        ],
        right: [
            // {label: 'Duty Off', action: 'duty_off'},
            { label: 'Restore', action: 'restore' },
            { label: 'Clear App', action: 'clear' },
        ]
    };

    constructor(
        public modalService: NgbModal,
        public activeModal: NgbActiveModal,
        private livetracking_service: LivetrackingService,
    ) { }

    ngOnInit() {
        let mTime = this.locDetail.location.mTime;
        if (mTime) {
            this.gps_human_readable = this.livetracking_service.get_gps_timestamp_human_readable_format(mTime);
        }
        if (typeof(this.locDetail.profile.login_time) === 'number') {
            this.locDetail.profile.login_time = this.livetracking_service.get_gps_timestamp_human_readable_format(this.locDetail.profile.login_time);
        }
        this.avatarUrl = this.locDetail.profile.avatar_url || Constants.images.avatar;
    }

    show_alert(message: string): void {
        this.showAlert = true;
        this.message = message;
        this.disableActions = false;
        setTimeout(() => {
            this.showAlert = false;
        }, 5000);
    }

    sendAction(action: any): void {
        const data = {
            'driver_id': this.locDetail.driver_id,
            'action': action
        };
        this.disableActions = true;
        this.livetracking_service.send_action_to_driver(data).subscribe(_data => {
            this.show_alert(_data);
        }, err => {
            console.error(err);
            let errorMessage = !err.error || Object.keys(err.error).length === 0 ? 'Not able to reach device.' : err.error;
            this.show_alert(errorMessage);
        });
    }

}
