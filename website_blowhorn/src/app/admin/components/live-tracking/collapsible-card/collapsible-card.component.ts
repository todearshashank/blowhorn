import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { LivetrackingService } from '../../../../shared/services/livetracking.service';

@Component({
  selector: 'app-collapsible-card',
  templateUrl: './collapsible-card.component.html',
  styleUrls: ['./collapsible-card.component.scss']
})
export class CollapsibleCardComponent implements OnInit {

  headerText = '';
  expandCard = false;
  items = [];

  constructor(
    private livetrackingService: LivetrackingService
  ) { }

  ngOnInit() {
  }

  selectionChanged(event): void {
  }

}
