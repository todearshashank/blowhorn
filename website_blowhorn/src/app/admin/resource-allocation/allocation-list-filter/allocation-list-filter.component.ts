import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IdName } from '../../../shared/models/id-name.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ResourceAllocationService } from '../../../shared/services/resource-allocation.service';
import { Contract } from '../../../shared/models/resource-allocation.model';

@Component({
    selector: 'app-allocation-list-filter',
    templateUrl: './allocation-list-filter.component.html',
    styleUrls: ['./allocation-list-filter.component.scss']
})
export class ResourceAllocationListFilterComponent implements OnInit {

    @Output('filter_changed')
    filter_changed: EventEmitter<{ url: string }>;

    cities: IdName[] = [];
    city_store: IdName[] = [];
    customer_store: IdName[] = [];
    customers: IdName[] = [];
    divisions: IdName[] = [];
    division_store: IdName[] = [];
    search_text = '';

    selected_city_id: number = null;
    selected_city_name = 'Select City';
    selected_customer_id: number = null;
    selected_customer_name = 'Select Customer';
    selected_division_id: number = null;
    selected_division_name = 'Select Division';

    city_disabled = false;
    customer_disabled = false;
    division_disabled = false;
    search_activated = false;
    filter_val_changed = false;

    constructor(
        public _blowhorn_service: BlowhornService,
        private ra_service: ResourceAllocationService,
    ) {
        this.filter_changed = new EventEmitter<{ url: string }>();
        this.ra_service
            .city_store
            .subscribe(city_store => {
                this.city_store = city_store;
                this.cities = this._blowhorn_service.copy_array(this.city_store);
            });

        this.ra_service
        .customer_store
        .subscribe(customer_store => {
            this.customer_store = customer_store;
            this.customers = this._blowhorn_service.copy_array(this.customer_store);
        });

        this.ra_service
            .division_store
            .subscribe(division_store => {
                this.division_store = division_store;
                this.divisions = this._blowhorn_service.copy_array(this.division_store);
            });
    }

    ngOnInit() {
    }

    get city_styles(): {} {
        return {
            'dropdown-styles': {
                'box-shadow': 'none',
                'border': 'none',
                'height': '30px',
                'width': '130px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px'
            },
            'dropdown-menu-styles': {
                'width': '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get customer_styles(): {} {
        return {
            'dropdown-styles': {
                'box-shadow': 'none',
                'border': 'none',
                'height': '30px',
                'width': '130px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px'
            },
            'dropdown-menu-styles': {
                'width': '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get division_styles(): {} {
        return {
            'dropdown-styles': {
                'box-shadow': 'none',
                'border': 'none',
                'height': '30px',
                'width': '130px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px'
            },
            'dropdown-menu-styles': {
                'width': '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    city_changed(event: { name: string, id: number }): void {
        if (this.selected_city_id !== event.id) {
            this.selected_city_name = event.name;
            this.selected_city_id = event.id;
        }
        this.filter_val_changed = true;
        // this.reset_customer();
        // this.reset_division();
    }

    customer_changed(event: { name: string, id: number }): void {
        if (this.selected_customer_id !== event.id) {
            this.selected_customer_name = event.name;
            this.selected_customer_id = event.id;
        }
        const url = this.get_url();
        this.filter_changed.emit({url: url});
    }

    division_changed(event: { name: string, id: number }): void {
        if (this.selected_division_id !== event.id) {
            this.selected_division_name = event.name;
            this.selected_division_id = event.id;
        }
        const url = this.get_url();
        this.filter_changed.emit({url: url});
    }

    fire_filter(): void {
        this.filter_val_changed = false;
        const url = this.get_url();
        this.filter_changed.emit({url: url});
    }

    reset_search(event): void {
        this.search_text = '';
        this.filter_val_changed = true;
    }

    reset_city(): void {
        this.selected_city_id = null;
        this.selected_city_name = 'All Cities';
        this.filter_val_changed = true;
    }

    reset_customer(): void {
        this.selected_customer_id = null;
        this.selected_customer_name = 'All Customer';
    }

    reset_division(): void {
        this.selected_division_id = null;
        this.selected_division_name = 'All Division';
    }

    activate_search_clear(event): void {
        this.search_activated = true;
    }

    deactivate_search_clear(event): void {
        this.search_activated = false;
    }

    get_url(): string {
        let url = '';
        url += this.selected_city_id ? `&city_id=${this.selected_city_id}` : '';
        url += this.selected_customer_id ? `&customer_id=${this.selected_customer_id}` : '';
        url += this.selected_division_id ? `&division_id=${this.selected_division_id}` : '';
        url += this.search_text ? `&search=${this.search_text}` : '';
        return url;
    }

}
