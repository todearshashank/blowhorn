import { Component, OnInit } from '@angular/core';
import { Contract } from '../../../shared/models/resource-allocation.model';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ResourceAllocationService } from '../../../shared/services/resource-allocation.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';

@Component({
    selector: 'app-allocation-list-view',
    templateUrl: './allocation-list-view.component.html',
    styleUrls: ['./allocation-list-view.component.scss']
})
export class ResourceAllocationListViewComponent implements OnInit {

    public app_title = Constants.version_info.resource_allocation.app_title;
    public app_code: string = Constants.version_info.resource_allocation.app_code;
    public version: string = Constants.version_info.resource_allocation.version;
    public whats_new: any = Constants.version_info.resource_allocation.whats_new;

    master_data: Contract[] = [];
    contract_list: Contract[] = [];
    count: number;
    next: string;
    previous: string;
    url = Constants.urls.API_RESOURCE_CONTRACTS;
    error_msg = 'Loading Contracts..';
    navbar_styles = {
        padding: '0px',
        boxShadow: '0px 0px 0px 0px transparent',
        border: 'none',
        height: '50px'
    };
    link_styles = {
        'justify-content': 'center'
    };
    links = [{link: '', text: 'Resource Allocation'}];

    constructor(
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private ra_service: ResourceAllocationService,
        public _blowhorn_service: BlowhornService,
    ) {
        route.data
        .subscribe(data => {
            console.log(data);
            this.update_contract_list(data.resourceallocation);
            this._blowhorn_service.hide_loader();
            this.get_support_data();
        },
        err => {
            console.error(err);
        });
    }

    ngOnInit() {
    }

    get_support_data(): void {
        this.ra_service.get_support_data().subscribe(data => {
            this.ra_service.update_city_store(data.cities);
            // this.ra_service.update_customer_store(data.customers);
            // this.ra_service.update_division_store(data.divisions);
            this.ra_service.contract_url = data.contract_url;
            this._blowhorn_service.hide_loader();
        },
        err => {
            this._blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

    update_contract_list(data: {count: number, next: string, previous: string, results: any[]}) {
        this.count = data.count;
        this.next = data.next;
        this.previous = data.previous;
        this.contract_list = data.results;
        this.master_data = data.results;
    }

    filter_changed(event: { url: string }): void {
        this.error_msg = 'Loading Contracts..';
        this.update_contract_list({
            count: 0,
            next: '',
            previous: '',
            results: []
        });
        this._blowhorn_service.show_loader();
        this.ra_service.get_contracts(`${Constants.urls.API_RESOURCE_CONTRACTS}?${event.url}`).subscribe(data => {
            this._blowhorn_service.hide_loader();
            this.update_contract_list(data);
            if (!this.master_data.length) {
                this.error_msg = 'No contract found';
            }
        },
        err => {
            this._blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

}
