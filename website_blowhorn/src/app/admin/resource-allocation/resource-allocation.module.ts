import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../../app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgbPaginationModule, NgbPopoverModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { ResourceAllocationListViewComponent } from './allocation-list-view/allocation-list-view.component';
import { ResourceAllocationListFilterComponent } from './allocation-list-filter/allocation-list-filter.component';
import { ResourceAllocationListViewGridRowComponent } from './allocation-list-view-grid-row/allocation-list-view-grid-row.component';
import { ResourceAllocationListViewGridTitleComponent } from './allocation-list-view-grid-title/allocation-list-view-grid-title.component';
import { ResourceAllocationMainComponent } from './allocation-main/allocation-main.component';
import { AddUpdateAllocationComponent } from './add-update-allocation/add-update-allocation.component';
import { SharedModule } from '../../shared/shared.module';
import { AllocationHistoryComponent } from './allocation-history/allocation-history.component';

@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        BrowserAnimationsModule,
        SharedModule,
        NgbPaginationModule,
        NgbPopoverModule,
        NgbTimepickerModule,
    ],
    declarations: [
        ResourceAllocationListViewComponent,
        ResourceAllocationListFilterComponent,
        ResourceAllocationListViewGridRowComponent,
        ResourceAllocationListViewGridTitleComponent,
        ResourceAllocationMainComponent,
        AddUpdateAllocationComponent,
        AllocationHistoryComponent
    ]
})
export class ResourceAllocationModule { }
