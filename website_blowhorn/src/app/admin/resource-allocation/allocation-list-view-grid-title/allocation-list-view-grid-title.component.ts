import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-allocation-list-view-grid-title',
  templateUrl: './allocation-list-view-grid-title.component.html',
  styleUrls: ['./allocation-list-view-grid-title.component.scss']
})
export class ResourceAllocationListViewGridTitleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
