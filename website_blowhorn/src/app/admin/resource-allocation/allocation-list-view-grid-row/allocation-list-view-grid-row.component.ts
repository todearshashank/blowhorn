import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contract } from '../../../shared/models/resource-allocation.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddUpdateAllocationComponent } from '../add-update-allocation/add-update-allocation.component';
import { AllocationHistoryComponent } from '../allocation-history/allocation-history.component';
import { ResourceAllocationService } from '../../../shared/services/resource-allocation.service';


@Component({
    selector: 'app-allocation-list-view-grid-row',
    templateUrl: './allocation-list-view-grid-row.component.html',
    styleUrls: ['./allocation-list-view-grid-row.component.scss']
})
export class ResourceAllocationListViewGridRowComponent implements OnInit {

    @Input()
    contract: Contract;

    constructor(
        private modalService: NgbModal,
        private ra_service: ResourceAllocationService,
    ) { }

    ngOnInit() {
    }

    open_resource_modal(): void {
        const modalRef = this.modalService.open(AddUpdateAllocationComponent,
            {backdropClass: 'backdrop-color', centered: true});
        modalRef.componentInstance.contract = this.contract;
        modalRef.componentInstance.emitService.subscribe((result) => {
            this.contract = result;
        },
        err => {
            console.error('Error', err);
        });
    }

    open_link(url: string): void {
        window.open(url, '_blank');
    }

    open_contract(contract: Contract): void {
        let url = `${this.ra_service.contract_url}/${contract.id}/change`;
        window.open(url, '_blank');
    }

    show_allocation_history(contract: Contract): void {
        const modalRef = this.modalService.open(
            AllocationHistoryComponent,
            {backdropClass: 'backdrop-color', centered: true});
        modalRef.componentInstance.contract = this.contract;
    }

}
