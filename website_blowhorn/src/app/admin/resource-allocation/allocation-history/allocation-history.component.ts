import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AllocationHistory, Contract } from '../../../shared/models/resource-allocation.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ResourceAllocationService } from '../../../shared/services/resource-allocation.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-allocation-history',
    templateUrl: './allocation-history.component.html',
    styleUrls: ['./allocation-history.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AllocationHistoryComponent implements OnInit {

    contract: Contract;
    allocation_history: AllocationHistory[] = [];
    error_message = 'No resources found';
    page_num = 1;
    currentPage = 1;
    itemsPerPage = 12;
    currentPageData = [];
    startItem = 0;
    endItem = 0;

    constructor(
        public activeModal: NgbActiveModal,
        public blowhorn_service: BlowhornService,
        private ra_service: ResourceAllocationService,
    ) { }

    ngOnInit() {
        this.get_history();
    }

    get_history(): void {
        this.blowhorn_service.show_loader();
        this.error_message = 'Loading history..';
        this.ra_service.get_allocation_history(this.contract.id).subscribe((resp: any) => {
            this.allocation_history = resp;
            console.log('resp', resp);
            this.error_message = 'No resources found';
            this.setPage(1);
            this.blowhorn_service.hide_loader();
        },
        err => {
            if (err) {
                this.error_message = err.error.message || err.error.detail;
            }
            this.blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

    setPage(pageNum: number): void {
        this.page_num = pageNum;
        this.startItem = (pageNum - 1) * this.itemsPerPage;
        this.endItem = pageNum * this.itemsPerPage;
        let copyResources = this.blowhorn_service.copy_array(this.allocation_history);
        this.currentPageData = copyResources.slice(this.startItem, this.endItem);
    }

    pageChanged(event: number): void {
        this.setPage(event);
    }

}
