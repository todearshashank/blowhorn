import { Component, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Contract, Resource } from '../../../shared/models/resource-allocation.model';
import { ResourceAllocationService } from '../../../shared/services/resource-allocation.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
    selector: 'app-add-update-allocation',
    templateUrl: './add-update-allocation.component.html',
    styleUrls: ['./add-update-allocation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AddUpdateAllocationComponent implements OnInit {

    @Output() emitService: EventEmitter<any>;

    contract: Contract;
    current_contract: Contract;
    resources: any = [];
    total_available_drivers = [];
    available_drivers = [];
    available_hubs = [];
    header_text = '';
    error_message = 'Loading resource details';
    currentPage = 1;
    itemsPerPage = 8;
    currentPageData = [];
    show_form = false;
    instance: any;
    driver_label = 'No drivers selected';
    selected_hub_name = 'Select a hub';
    selected_hub_id = null;
    disable_button = false;
    disabled_submit = false;
    page_num = 1;
    current_resource = null;
    remind_driver = {
        'name': 'Remind Driver', checked: false
    };
    confirmation = {
        heading: '',
        details: '',
        show_warning: false,
        show_alert: false,
        action_buttons: {
            confirm: {
                show: false,
                label: 'Yes',
                type: 'save'
            },
            decline: {
                show: false,
                label: 'No',
                type: ''
            }
        },
        styles: {
            header: {}
        }
    };
    action_buttons = {
        add: {
            label: 'Add Resource Allocation',
            type: '',
            disabled: false
        },
        edit: {
            label: 'Edit »',
            type: 'edit',
            disabled: false
        },
        delete: {
            label: 'Delete',
            type: 'delete',
            disabled: false
        },
        submit: {
            label: 'Save',
            type: 'save',
            disabled: false,
        },
        return: {
            label: '« Back',
            type: 'delete',
            disabled: false
        },
        dismiss: {
            label: 'Close Window',
            type: 'delete',
            disabled: false
        }
    };
    multiselect_action_buttons = {
        select: {
            label: '⇨',
            type: '',
            disabled: false,
            title: 'Add selected drivers to resource'
        },
        deselect: {
            label: '⇦',
            type: '',
            disabled: false,
            title: 'Remove selected drivers from resource'
        }
    };

    alert = {
        type: '',
        message: '',
        show_message: false
    };

    constructor(
        public activeModal: NgbActiveModal,
        public blowhorn_service: BlowhornService,
        public ra_service: ResourceAllocationService,
    ) {
        this.emitService = new EventEmitter<any>();
        this.header_text = '';
    }

    ngOnInit() {
        this.current_contract = Object.assign({}, this.contract);
        this.header_text = this.contract.name;
        this.get_resources();
        this.disable_button = false;
        this.page_num = 1;
        this.confirmation.show_alert = false;
    }

    get hub_dropdown_styles(): {} {
        return {
            'dropdown-styles': {
                'height': '40px',
                'width': '300px',
                boxShadow: 'none'
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                'width': '300px',
                'z-index': '3',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get remind_driver_styles(): {} {
        return {
            'checkbox-group-styles': {
                'margin-top': '50px'
            }
        };
    }

    get driver_dropdown_styles(): {} {
        return {
            'dropdown-styles': {
                height: '40px',
                'width': '300px',
            },
            'dropdown-menu-styles': {
                'width': '300px',
                'z-index': '3',
                'top': '30%',
                'left': '27.5%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get_resources(): void {
        this.blowhorn_service.show_loader();
        this.ra_service.get_resources(this.current_contract.id).subscribe((resp: any) => {
            // this.show_alert('success', resp.message);
            this.resources = resp.resource_allocations;
            this.available_hubs = resp.hubs;
            this.total_available_drivers = resp.drivers;
            this.error_message = 'No resources found';
            this.setPage(1);
            this.blowhorn_service.hide_loader();
        },
        err => {
            if (err) {
                this.error_message = err.error.message || err.error.detail;
            }
            this.blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

    actionClicked(resource: Resource): void {
        console.log('action clicked');
    }

    setPage(pageNum: number): void {
        this.page_num = pageNum;
        const startItem = (pageNum - 1) * this.itemsPerPage;
        const endItem = pageNum * this.itemsPerPage;
        let copy_resources = this.blowhorn_service.copy_array(this.resources);
        this.currentPageData = copy_resources.slice(startItem, endItem);
    }

    pageChanged(event: number): void {
        this.setPage(event);
    }

    open_resource(resource: Resource): void {
        this.header_text = this.current_contract.name;
        this.show_form = true;
        this.instance = Object.assign({}, resource);
        this.instance.shift_start_time = {
            hour: new Date(this.instance.shift_start_time).getHours(),
            minute: new Date(this.instance.shift_start_time).getMinutes()
        };
        this.available_drivers = [];
        this.total_available_drivers.forEach(item => {
            this.available_drivers.push({
                id: item.id,
                selected: false,
                name: item.name
            });
        });
        this.selected_hub_name = this.instance.hub.name;
        this.selected_hub_id = this.instance.hub.id;

        for (let i of this.instance.drivers) {
            this.available_drivers.forEach(item => {
                if (item.id === i.id) {
                    item.selected = true;
                }
            });
        }
        this.driver_label = this.instance.drivers.length + ' selected';
    }

    drivers_changed(event: { id: number, selected: boolean, name: string }[]) {
        if (!event.length) {
            this.driver_label = 'No drivers selected';
        }
        this.instance.drivers = event;
    }

    remind_driver_changed(event: any) {
        this.remind_driver.checked = !this.remind_driver.checked;
        this.disabled_submit = (this.remind_driver.checked
            && !this.instance.exotel_app_id);
    }

    exotel_app_id_changed(event: any) {
        this.disabled_submit = (this.remind_driver.checked
            && !this.instance.exotel_app_id);
    }

    open_add_modal(): void {
        this.show_form = true;
        this.header_text = this.current_contract.name;
        this.selected_hub_name = 'Select a hub';
        this.selected_hub_id = null;
        this.available_drivers = [];
        this.remind_driver.checked = false;
        this.total_available_drivers.forEach(item => {
            this.available_drivers.push({
                id: item.id,
                selected: false,
                name: item.name
            });
        });
        this.instance = {
            hub: {
                id: '',
                name: ''
            },
            drivers: [],
            unplanned_additional_drivers: 0,
            shift_start_time: ''
        };
        this.driver_label = 'No Driver Selected';
    }

    hubChanged(event: {id: number, name: string}) {
        this.selected_hub_name = 'Select a hub';
        this.selected_hub_id = null;
        if (event.id) {
            this.selected_hub_name = event.name;
            this.selected_hub_id = event.id;
        }
    }

    open_confirmation_dialog(resource: Resource): void {
        this.current_resource = resource;
        this.confirmation.show_alert = true;
        this.confirmation.heading = 'Delete Resource Allocation';
        this.confirmation.details = `${this.current_resource.hub.name} has ${this.current_resource.drivers.length}
            driver(s). Do you really want to delete?`;
        this.confirmation.show_warning = true;
        this.confirmation.action_buttons.confirm.show = true;
        this.confirmation.action_buttons.decline.show = true;
        this.confirmation.action_buttons.decline.label = 'No';
        this.confirmation.action_buttons.decline.type = '';
        this.confirmation.styles.header = {};
    }

    remove_resource_from_list(id: number): void {
        let delIndex = this.resources.findIndex(item => item.id === id);
        this.resources.splice(delIndex, 1);
        this.setPage(this.page_num);
    }

    delete_resource(proceed: boolean): void {
        this.confirmation.show_alert = false;
        if (!proceed) {
            this.current_resource = null;
            return;
        }
        this.disable_button = true;
        this.blowhorn_service.show_loader();
        this.ra_service.delete_resource(this.current_resource.id).subscribe((resp: any) => {
            this.show_alert('Success', resp);
            this.remove_resource_from_list(this.current_resource.id);
            this.blowhorn_service.hide_loader();
            this.current_contract.active_drivers = 0;
            this.current_contract.active_drivers += this.resources.map(item => {
                return item.drivers.length;
            }).reduce((a, b) => a + b, 0);
            this.emitService.next(this.current_contract);
        },
        err => {
            if (err) {
                this.show_alert('Error', err.error.message || err.error.detail || err.error);
            }
            this.blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

    onSubmit(resource: any): void {
        this.disable_button = true;
        resource['hub']['id'] = this.selected_hub_id;
        resource['remind_driver'] = this.remind_driver.checked;
        this.blowhorn_service.show_loader();
        this.ra_service.update_resource(this.current_contract.id, resource).subscribe((resp: any) => {
            this.show_alert('Success', resp.message);
            if (resp.created) {
                this.resources.splice(0, 0, resp.data);
            } else {
                let updateIndex = this.resources.findIndex(item => item.id === resp.data.id);
                this.resources.splice(updateIndex, 1, resp.data);
            }
            this.setPage(this.page_num);
            this.current_contract.active_drivers = 0;
            this.current_contract.active_drivers += this.resources.map(item => {
                return item.drivers.length;
            }).reduce((a, b) => a + b, 0);
            this.emitService.next(this.current_contract);
            this.blowhorn_service.hide_loader();
        },
        err => {
            if (err) {
                this.show_alert('Failed', err.error.message || err.error.detail || err.error);
            }
            this.blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

    show_alert(type: string, message: string): void {
        this.confirmation.heading = type;
        this.confirmation.details = message;
        this.confirmation.show_warning = false;
        this.confirmation.action_buttons.confirm.show = false;
        this.confirmation.action_buttons.decline.show = true;
        this.confirmation.action_buttons.decline.label = 'Ok';
        this.confirmation.action_buttons.decline.type = '';
        this.disable_button = false;
        this.confirmation.styles.header = {
            'padding': '5px 10px',
            'color': '#fff'
        };
        this.confirmation.show_alert = true;
        if (type === 'Failed') {
            this.confirmation.styles.header['background'] = '#cf342e';
            return;
        }
        this.confirmation.styles.header['background'] = '#05c75c';
        setTimeout(() => {
            this.confirmation.show_alert = false;
            this.show_form = false;
        }, 3000);
    }
}
