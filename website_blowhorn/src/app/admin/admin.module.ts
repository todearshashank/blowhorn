import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { DepartureSheetSearchComponent } from './components/departure-sheet-search/departure-sheet-search.component';
import { DepartureSheetModule } from '../departure-sheet/departure-sheet.module';
import { LiveTrackingMainComponent } from './components/live-tracking/live-tracking-main/live-tracking-main.component';
import { FilterSidebarComponent } from './components/live-tracking/filter-sidebar/filter-sidebar.component';
import { CollapsibleCardComponent } from './components/live-tracking/collapsible-card/collapsible-card.component';
import { OrderStatisticsRightbarComponent } from './components/live-tracking/order-statistics-rightbar/order-statistics-rightbar.component';
import { InfoWindowComponent } from './components/live-tracking/info-window/info-window.component';
import { DriverProfileComponent } from './components/live-tracking/driver-profile/driver-profile.component';
import { AdminHeatmapComponent } from './components/admin-heatmap/admin-heatmap.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { ShipmentOrdersComponent } from './components/shipment-orders/shipment-orders.component';
import { AppRoutingModule } from '../app-routing.module';
import { ShipmentOrderAssignComponent } from './components/shipment-order-assign/shipment-order-assign.component';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { ApolloModule } from 'apollo-angular';
import { OrderHeatmapComponent } from './components/order-heatmap/order-heatmap.component';
import { DriverHeatmapComponent } from './components/driver-heatmap/driver-heatmap.component';
import { KioskOrderComponent } from './components/kiosk-order/kiosk-order.component';
import { AgmDirectionModule } from 'agm-direction';
import { ResourceAllocationModule } from './resource-allocation/resource-allocation.module';
import { AddUpdateAllocationComponent } from './resource-allocation/add-update-allocation/add-update-allocation.component';
import { AllocationHistoryComponent } from './resource-allocation/allocation-history/allocation-history.component';
import { RouteOptimiserComponent, DialogPickDriver } from './components/route-optimiser/route-optimiser.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule, MatAutocomplete } from '@angular/material/autocomplete';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AgmCoreModule,
    DepartureSheetModule,
    BsDatepickerModule,
    FormsModule,
    AppRoutingModule,
    AgmJsMarkerClustererModule,
    ApolloModule,
    AgmDirectionModule,
    ResourceAllocationModule,
    MatExpansionModule,
    MatListModule,
    MatTableModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [
    DepartureSheetSearchComponent,
    LiveTrackingMainComponent,
    FilterSidebarComponent,
    CollapsibleCardComponent,
    OrderStatisticsRightbarComponent,
    InfoWindowComponent,
    DriverProfileComponent,
    AdminHeatmapComponent,
    ShipmentOrdersComponent,
    ShipmentOrderAssignComponent,
    OrderHeatmapComponent,
    DriverHeatmapComponent,
    KioskOrderComponent,
    RouteOptimiserComponent,
    DialogPickDriver,
  ],
  entryComponents: [
    DriverProfileComponent,
    AddUpdateAllocationComponent,
    AllocationHistoryComponent,
    DialogPickDriver
  ]
})
export class AdminModule { }
