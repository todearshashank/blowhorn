import { Component, OnInit } from '@angular/core';
import { IntegrationService } from '../../shared/services/integration.service';

@Component({
  selector: 'app-tpi-api-key',
  templateUrl: './tpi-api-key.component.html',
  styleUrls: ['./tpi-api-key.component.scss']
})
export class TpiApiKeyComponent implements OnInit {

  apikey: string = 'Pending';
  formError = "** API Key will be displayed once contract is active";

  constructor(public integrationService : IntegrationService) { 
    this.integrationService.get_api_key().subscribe(
      response=>{
          if (response.is_active) {
            this.apikey = response.key;
            this.formError = null;
          }
      },
      err=>{
          console.log(err);
      }
);
  }

  ngOnInit() {
  }

/* To copy Text from Textbox */
  copyInputMessage() {
    console.log('here');
    var data = document.getElementById("api-key-area") as HTMLInputElement;
    data.select();
    document.execCommand('copy');
  }

}