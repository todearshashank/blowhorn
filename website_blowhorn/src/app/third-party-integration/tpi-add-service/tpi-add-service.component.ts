import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { IntegrationService } from '../../shared/services/integration.service';

@Component({
  selector: 'app-tpi-add-service',
  templateUrl: './tpi-add-service.component.html',
  styleUrls: ['./tpi-add-service.component.scss']
})
export class TpiAddServiceComponent implements OnInit {

  formError = null;
  formSuccess = null;
  option_disabled = false;
  selected_option_id = null;
  selected_option_name = '----------';

  options = [
    {name : 'Shopify', id : 1},
    {name : 'Unicommerce', id : 2},
  ]
  constructor(public integrationService : IntegrationService) { }

  ngOnInit() {
    this.selected_option_id=1;
    this.selected_option_name='Shopify';
  }

  option_changed(event: any): void {
    this.formError = null;
    this.formSuccess = null;
    this.selected_option_id = event.id;
    this.selected_option_name = event.name;
    this.integrationService.update_current_partner(event.name);
  }

  save_subscription(): void {
    this.formSuccess = null;
    this.formError = null;

    let data = {
      partner : this.selected_option_name
    };
    
    this.integrationService.submit_request(data).subscribe(
          response=>{
              this.formSuccess = response   
          },
          err=>{
              this.formError = err.error;
          }
    );

  }

  get filter_options_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '130px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '10px'
      },
      'dropdown-menu-styles': {
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

}
