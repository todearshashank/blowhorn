import { Component, OnInit } from '@angular/core';
import { IntegrationService } from '../../shared/services/integration.service';

interface Flow {
  summary: String;
  detail: String;
}

@Component({
  selector: 'app-tpi-process-flow',
  templateUrl: './tpi-process-flow.component.html',
  styleUrls: ['./tpi-process-flow.component.scss']
})
export class TpiProcessFlowComponent implements OnInit {

  selectedPartner = 'Shopify';
  shopifyFlowData = [
    {
        "summary" : "App Installation",
        "detail" : "Install the Blowhorn App from shopify app store once the shopify setup is completed. You will be redirected to https://blowhorn.com after successfull installation."
    },
    {
        "summary" : "Blowhorn Account Setup",
        "detail" : "For new users, business account will be created and you will receive an email with the credentials you can use to login to blowhorn dashboard. Our Sales team will contact you for setup completion and to discuss terms. For existing users, you can directly move to next step. Please make sure the email id is used for blowhorn account creation as used for shopify account."
    },
    {
      "summary" : "Sales Team Confirmation",
      "detail" : "Please wait for confirmation from Sales Team regarding contract activation. Also the API key will be visible once the contract is successfully activated."
    },
    {
        "summary" : "Partner Options",
        "detail" : "Please select Shopify from the the dropdown on right and mention the shopify domain. Click on sumbit."
    },
    {
        "summary" : "Activation Successfull",
        "detail" : "A message will be displayed confirming integration is successfull."
    },
    {
      "summary" : "Sync Order",
      "detail" : "You can now go to Shipments tab and sync up the orders via 'SYNC ORDER' button."
    },
  ]

  unicommerceFlowData = [
    {
        "summary" : "Blowhorn Account Setup",
        "detail" : "Please get in touch with the Sales team for account setup completion."
    },
    {
      "summary" : "Sales Team Confirmation",
      "detail" : "Please wait for confirmation from Sales Team regarding contract activation. Also the API key will be visible once the contract is successfully activated."
    },
    {
        "summary" : "Unicommerce Setup",
        "detail" : "Contact the unicommerce team and setup account using the same email and password as used for blowhorn account"
    },
    {
        "summary" : "Partner Options",
        "detail" : "Please select Unicommerce from the the dropdown on right and click on sumbit."
    },
    {
        "summary" : "Activation Successfull",
        "detail" : "A message will be displayed confirming integration is successfull."
    }
  ]

  flowData: Flow[] = this.shopifyFlowData;

  constructor(public integrationService : IntegrationService) {   
  }

  ngOnInit() {
    this.integrationService.current_partner.subscribe((data: any) => {
      if (data) {
        this.selectedPartner = data;
        if (this.selectedPartner == 'Shopify') {
          this.flowData = this.shopifyFlowData
        }
        if (this.selectedPartner == 'Unicommerce') {
          this.flowData = this.unicommerceFlowData
        }
      }
    });
  }

}
