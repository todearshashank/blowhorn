import { Component, OnInit } from '@angular/core';
import { ReportService } from '../../shared/services/report.service';

@Component({
  selector: 'app-tpi-suggestion-form',
  templateUrl: './tpi-suggestion-form.component.html',
  styleUrls: ['./tpi-suggestion-form.component.scss']
})
export class TpiSuggestionFormComponent implements OnInit {

  partner_email='';
  constructor(private report_service: ReportService) { }

  ngOnInit() {
  }

  submit_partner_email() {
    let data = {
      feedback : this.partner_email
    };
    this.report_service.submit_feedback(data).subscribe(
          response=>{
              console.log('success')
          },
          err=>{
              console.log(err);
          }
    );
}

}
