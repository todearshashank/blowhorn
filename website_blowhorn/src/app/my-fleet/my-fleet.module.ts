import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyFleetMainComponent } from './components/my-fleet-main/my-fleet-main.component';
import { SharedModule } from '../shared/shared.module';
import { MyFleetNavBarComponent } from './components/my-fleet-nav-bar/my-fleet-nav-bar.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppRoutingModule } from '../app-routing.module';
import { AgmCoreModule } from '@agm/core';
import { InvoiceModule } from '../invoice/invoice.module';
import { MyFleetHomeComponent } from './components/my-fleet-home/my-fleet-home.component';
import { MyFleetInfoWindowComponent } from './components/my-fleet-info-window/my-fleet-info-window.component';
import { MyFleetMapCardComponent } from './components/my-fleet-map-card/my-fleet-map-card.component';
import { MyFleetMapMiniCardComponent } from './components/my-fleet-map-mini-card/my-fleet-map-mini-card.component';
import { RouteModule } from '../route/route.module';
import { ContactModule } from '../contact/contact.module';
import { LocationModule } from '../location/location.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BsDropdownModule,
    AppRoutingModule,
    AgmCoreModule,
    InvoiceModule,
    RouteModule,
    ContactModule,
    LocationModule,
  ],
  declarations: [
    MyFleetMainComponent,
    MyFleetNavBarComponent,
    MyFleetHomeComponent,
    MyFleetInfoWindowComponent,
    MyFleetMapCardComponent,
    MyFleetMapMiniCardComponent
  ],
  exports: [
  ],
  providers: [
  ]
})
export class MyFleetModule { }
