import { BlowhornService } from './../../../shared/services/blowhorn.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-my-fleet-map-mini-card',
  templateUrl: './my-fleet-map-mini-card.component.html',
  styleUrls: ['./my-fleet-map-mini-card.component.scss']
})
export class MyFleetMapMiniCardComponent implements OnInit {

  @Input('reported_count')
  reported_count: number;
  @Input('total_count')
  total_count: number;
  @Input('ongoing_count')
  ongoing_count: number;
  @Input('styles')
  styles: {} = {
    'trip-card-styles': {}
  };
  can_view_enterprise_trips = false;

  constructor(
    private _blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
    this.can_view_enterprise_trips = this._blowhorn_service.hasPermission('enterprise_trips', 'can_view_enterprise_trips');
  }

}
