import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';

@Component({
  selector: 'app-my-fleet-home',
  templateUrl: './my-fleet-home.component.html',
  styleUrls: ['./my-fleet-home.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyFleetHomeComponent implements OnInit {

  show_save_route = false;
  show_save_address = false;
  show_save_contact = false;

  constructor(
    private watcher_service: WatcherService
  ) { }

  ngOnInit() {
    this.watcher_service
      .show_save_route
      .subscribe(show_save_route => {
        this.show_save_route = show_save_route;
      });
    this.watcher_service
      .show_save_address
      .subscribe(show_save_address => {
        this.show_save_address = show_save_address;
      });
    this.watcher_service
      .show_save_contact
      .subscribe(show_save_contact => {
        this.show_save_contact = show_save_contact;
      });
  }

}
