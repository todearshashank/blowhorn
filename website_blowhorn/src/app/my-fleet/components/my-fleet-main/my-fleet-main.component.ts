import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FleetService } from '../../../shared/services/fleet.service';
import { Driver } from '../../../shared/models/driver.model';
import { MapMarker } from '../../../shared/models/map-marker.model';
import { City } from '../../../shared/models/city.model';
import { IdName } from '../../../shared/models/id-name.model';
import { AngularFireDatabase } from 'angularfire2/database';
import { ActivatedRoute } from '@angular/router';
import { FleetTrips } from '../../../shared/utils/enums';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { MapService } from '../../../shared/services/map.service';

@Component({
  selector: 'app-my-fleet-main',
  templateUrl: './my-fleet-main.component.html',
  styleUrls: ['./my-fleet-main.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})

export class MyFleetMainComponent implements OnInit {
  drivers: Driver[] = [];
  cities: City[] = [];
  hubs: IdName[] = [];
  vehicle_types: IdName[] = [];
  driver_store: Driver[] = [];
  city_store: City[] = [];
  hub_store: IdName[] = [];
  vehicle_type_store: IdName[] = [];
  driver: string;
  reported_trips_count = 0;
  total_trips_count = 0;
  ongoing_trips_count = 0;
  selected_city_id: number = null;
  selected_city_name = '';
  selected_hub_id: number = null;
  selected_hub_name = '';
  selected_vehicle_type_id: number = null;
  selected_vehicle_type_name = '';
  selected_driver_id: number = null;
  selected_driver_name = '';
  cenroid = this.mapService.get_bounds(1, 'pickup').getCenter();
  longitude_center = this.cenroid.lng();
  latitude_center = this.cenroid.lat();
  zoom_level = 10;
  markers: MapMarker[];
  city_disabled = false;
  hub_disabled = false;
  vehicle_type_disabled = false;
  driver_disabled = false;

  constructor(
    private fleet_service: FleetService,
    private firebase_db: AngularFireDatabase,
    private route: ActivatedRoute,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    public mapService: MapService,
  ) {
    this.route.data
      .subscribe( data => {
        this.driver_store = data.trip_filter.drivers;
        this.city_store = data.trip_filter.cities;
        this.vehicle_type_store = data.trip_filter.vehicle_classes;
        for (const city of data.trip_filter.cities) {
          for (const hub of city.hubs) {
            this.hub_store.push(hub);
          }
        }
        this.cities = this._blowhorn_service.copy_array(this.city_store);
        this.hubs  = this._blowhorn_service.copy_array(this.hub_store);
        this.vehicle_types  = this._blowhorn_service.copy_array(this.vehicle_type_store);
        this.drivers  = this._blowhorn_service.copy_array(this.driver_store);
        this.add_markers();
        this.display_counts();
        this.start_live_location_update();
        this.check_disabled();
        this._blowhorn_service.hide_loader();
      },
      err => {
        this._blowhorn_service.hide_loader();
      });
  }

  ngOnInit() {
  }

  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '75px',
        'left': '25px',
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
        'z-index': 2
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get hub_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '75px',
        'left': '250px',
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
        'z-index': 2
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get vehicle_type_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '75px',
        'left': '525px',
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
        'z-index': 2
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get driver_styles(): {} {
    return {
      'dropdown-styles': {
        'position': 'absolute',
        'top': '75px',
        'left': '750px',
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
        'z-index': 2
      },
      'dropdown-menu-styles': {
        'width': '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get trip_card_styles(): {} {
    return {
      'trip-card-styles': {
        'left': '25px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get reported_trips_styles(): {} {
    return {
      'trip-card-styles': {
        'max-width': '180px',
        'left': '25px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get ongoing_trips_styles(): {} {
    return {
      'trip-card-styles': {
        'max-width': '130px',
        'left': '225px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  city_changed(event: {name: string, id: number}): void {
    if (this.selected_city_id !== event.id) {
      this.selected_city_name = event.name;
      this.selected_city_id = event.id;
      this.reset_hub();
      this.reset_vehicle_type();
      this.reset_driver();
      this.filter_dropdowns();
      this.reset_markers();
      this.display_counts();
      this.recenter_selected_city();
      this.check_disabled('city');
    }
  }

  hub_changed(event): void {
    if (this.selected_hub_id !== event.id) {
      this.selected_hub_name = event.name;
      this.selected_hub_id = event.id;
      this.reset_vehicle_type();
      this.reset_driver();
      this.filter_dropdowns();
      this.reset_markers();
      this.display_counts();
      this.recenter_selected_city();
      this.check_disabled('hub');
    }
  }

  vehicle_type_changed(event): void {
    if (this.selected_vehicle_type_id !== event.id) {
      this.selected_vehicle_type_name = event.name;
      this.selected_vehicle_type_id = event.id;
      this.reset_driver();
      this.filter_dropdowns();
      this.reset_markers();
      this.display_counts();
      this.recenter_selected_city();
      this.check_disabled('vehicle_type');
    }
  }

  driver_changed(event): void {
    if (this.selected_driver_id !== event.id) {
      this.selected_driver_name = event.name;
      this.selected_driver_id = event.id;
      this.filter_dropdowns();
      this.reset_markers();
      const marker = this.markers.filter(item => {
        return item.driver.id === this.selected_driver_id;
      }).pop();
      if (this.selected_driver_id) {
        this.recenter_map(marker.driver.last_updated_location[0], marker.driver.last_updated_location[1]);
      } else {
        this.recenter_selected_city();
      }
    }
  }

  filter_dropdowns(): void {
    this.hubs = [];
    this.drivers = [];
    if (this.selected_city_id) {
      const city = this.city_store
        .filter(item => item.id === this.selected_city_id)[0];
      for (const hub of city.hubs) {
        this.hubs.push(hub);
      }
    } else {
      this.hubs = [];
      this.hubs  = this._blowhorn_service.copy_array(this.hub_store);
    }

    this.drivers = this.driver_store
      .filter(item => {
        return (this.selected_city_id ? item.city === this.selected_city_id : true) &&
        (this.selected_hub_id ? item.hub === this.selected_hub_id : true) &&
        (this.selected_vehicle_type_id ? item.vehicle_class === this.selected_vehicle_type_id : true);
      });
  }

  reset_city(): void {
    this.selected_city_id = null;
    this.selected_city_name = 'All Cities';
  }

  reset_hub(): void {
    this.selected_hub_id = null;
    this.selected_hub_name = 'All Hubs';
  }

  reset_vehicle_type(): void {
    this.selected_vehicle_type_id = null;
    this.selected_vehicle_type_name = 'All Vehicle Types';
  }

  reset_driver(): void {
    this.selected_driver_id = null;
    this.selected_driver_name = 'All Drivers';
  }

  add_markers(): void {
    this.markers = this.driver_store.map(item => {

      const icon_url = item.status === 'In-Progress' ?
      `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/RedTracker@2x.png` :
      `static/$${this._blowhorn_service.current_build}/assets/Assets-Myfleet/BlueTracker@2x.png`;

      const is_visible = (item.status !== 'Not-Reported') &&
        (this.selected_city_id ? item.city === this.selected_city_id : true) &&
        (this.selected_hub_id ? item.hub === this.selected_hub_id : true) &&
        (this.selected_vehicle_type_id ? item.vehicle_class === this.selected_vehicle_type_id : true) &&
        (this.selected_driver_id ? item.id === this.selected_driver_id : true);

      const geopoint = item.last_updated_location ? item.last_updated_location : [null, null];

      return new MapMarker(item, is_visible, icon_url);
    });
  }

  reset_markers(): void {
    this.markers = this.markers.map(item => {
      this.reset_marker(item);
      return item;
    });
  }

  reset_marker(item): void {
    const is_visible = (this.selected_city_id ? item.driver.city === this.selected_city_id : true) &&
        (this.selected_hub_id ? item.driver.hub === this.selected_hub_id : true) &&
        (this.selected_vehicle_type_id ? item.driver.vehicle_class === this.selected_vehicle_type_id : true) &&
        (this.selected_driver_id ? item.driver.id === this.selected_driver_id : true);

    item.visible = is_visible;
  }

  display_counts(): void {
    const filtered_drivers: Driver[] = this.drivers.filter(item =>
      this.selected_driver_id ? this.selected_driver_id === item.id : true);
    const reported_drivers: Driver[] = filtered_drivers.filter(item => item.status === 'New');
    const ongoing_drivers: Driver[] = filtered_drivers.filter(item => item.status === 'In-Progress');

    this.reported_trips_count = reported_drivers.length;
    this.ongoing_trips_count = ongoing_drivers.length;
    this.total_trips_count = filtered_drivers.length - ongoing_drivers.length;
  }

  fetch_trip_data(marker: MapMarker): void {
    this.fleet_service
      .get_fleet_info_window(marker.driver.id)
      .subscribe(fleet_info_window => {
          const trip = fleet_info_window.data.trip;
          marker.next_stop = trip.next_stop;
        },
        err => {
          console.error(err);
        }
      );
  }

  select_marker_and_fetch_trip(marker: MapMarker): void {
    this.set_selected_marker_icon(marker);
    this.fetch_trip_data(marker);
  }

  set_selected_marker_icon(marker: MapMarker) {
    marker.icon_url = `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/SelectedTracker@2x.png`;
  }

  reset_selected_marker_icon(marker: MapMarker) {
    marker.icon_url = marker.driver.status === FleetTrips.InProgress ?
      `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/RedTracker@2x.png` :
      `static/${this._blowhorn_service.current_build}/assets/Assets-Myfleet/BlueTracker@2x.png`
  }

  start_live_location_update(): void {
    this.markers.forEach(item => {
      let url = `/active_drivers/${item.driver.mobile}`;
      this.firebase_db
        .object(url)
        .valueChanges()
        .subscribe(data => {
          let modified_data: any = data;
          modified_data = JSON.parse(modified_data);
          if (modified_data && modified_data.hire_status === 'occupied') {
            const lat = modified_data.location.lat;
            const lng = modified_data.location.lon;
            item.driver.last_updated_location = [parseFloat(lat), parseFloat(lng)];
          }
        });
        url = `/active_drivers/${item.driver.id}`;
        this.firebase_db
        .object(url)
        .valueChanges()
        .subscribe(data => {
          let modified_data: any = data;
          modified_data = JSON.parse(modified_data);
          if (modified_data && modified_data.hire_status === 'occupied') {
            const lat = modified_data.location.lat;
            const lng = modified_data.location.lon;
            item.driver.last_updated_location = [parseFloat(lat), parseFloat(lng)];
          }
        });
    });
  }

  recenter_map(lat: number, lng: number) {
      this.latitude_center = lat;
      this.longitude_center = lng;
      this.zoom_level = 14;
  }

  recenter_selected_city(): void {
    const selected_city = this.city_store.filter(item => item.id === this.selected_city_id).pop();
    if (selected_city) {
      this.recenter_map(selected_city.geopoint[1], selected_city.geopoint[0]);
    } else {
      this.recenter_map(12.9715987, 77.5945627);
    }
  }

  check_disabled(called_from = 'none'): void {
    this.reset_disabled();
    if (!this.drivers.length) {
      switch (called_from) {
        case 'none':
          this.city_disabled = true;
          this.hub_disabled = true;
          this.vehicle_type_disabled = true;
          this.driver_disabled = true;
          break;
        case 'city':
          this.hub_disabled = true;
          this.vehicle_type_disabled = true;
          this.driver_disabled = true;
          break;
        case 'hub':
          this.vehicle_type_disabled = true;
          this.driver_disabled = true;
          break;
        case 'vehicle_type':
          this.driver_disabled = true;
      }
    }
  }

  reset_disabled(): void {
    this.city_disabled = false;
    this.hub_disabled = false;
    this.vehicle_type_disabled = false;
    this.driver_disabled = false;
  }
}
