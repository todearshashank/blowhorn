import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DropdownUserComponent } from '../../../shared/components/dropdown-user/dropdown-user.component';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';
import { ShipmentService } from '../../../shared/services/shipment.service';
import { BlowhornUploadComponent } from '../../../shared/components/blowhorn-upload/blowhorn-upload.component';
import { WatcherService } from '../../../shared/services/watcher.service';
import { Router } from '@angular/router';
import { IdName } from '../../../shared/models/id-name.model';

declare var customer_id: number;

@Component({
  selector: 'app-my-fleet-nav-bar',
  templateUrl: './my-fleet-nav-bar.component.html',
  styleUrls: ['./my-fleet-nav-bar.component.scss']
})
export class MyFleetNavBarComponent implements OnInit {
  @Input()
  username: string;
  @ViewChild('file_upload', {static: false}) file_upload: BlowhornUploadComponent;
  @ViewChild('overlay_panel', {static: false}) overlay_panel: ElementRef;
  @ViewChild('overlay_panel_download', {static: false}) overlay_panel_download: ElementRef;

  ROWWISE_SKU = 'Rowwise SKU';
  COLUMNWISE_SKU = 'Columnwise SKU';
  ORDER_TEMPLATE = 'Order';
  ORDERLINE_TEMPLATE = 'Orderline'
  items: string[] = [];
  c2c_sme_view: boolean;
  is_non_individual: boolean;
  is_business_user = false;
  is_shipment_tab = false;
  selected_sku_orientation = this.ROWWISE_SKU;
  selected_sku_orientation_upload = this.ROWWISE_SKU;
  selected_type_category = this.ORDER_TEMPLATE;
  show_download_template_modal = false;
  show = {
    modal: false,
    message: false,
    name: '',
    orientation: true
  };
  type = 'success';
  heading = '';
  message = '';
  errors = '';
  modal_title = '';
  sku_orientations = [{
    name: this.ROWWISE_SKU,
    id: 1
  },{
    name: this.COLUMNWISE_SKU,
    id: 2
  }];
  template_options = [{
    name: this.ORDER_TEMPLATE,
    id: 1
  },{
    name: this.ORDERLINE_TEMPLATE,
    id: 2
  }];
  moreActions = [];
  showUploadOption = false;
  can_upload_orders = false;
  can_upload_orderlines = false;
  can_download_template = false;
  can_view_overview = false;
  can_view_enterprise_trips = false;
  can_view_booking_trips = false;
  can_view_shipment_orders = false;
  can_view_invoices = false;
  can_view_analytics = false;
  showOrientation = true;

  showConfigLink = false;
  configurationUrl = 'configuration';

  showInvoiceLink = false;
  invoiceUrl = 'enterpriseinvoice'

  showAnalyticsLink = false;
  analyticsUrl = 'analytics'

  onHidden(): void { }
  onShown(): void { }
  isOpenChange(): void { }
  constructor(
    public _blowhorn_service: BlowhornService,
    private _shipment_service: ShipmentService,
    private _watcher_service: WatcherService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.username = this._blowhorn_service.username;
    this.is_non_individual = this._blowhorn_service.is_non_individual;
    this.is_business_user = this._blowhorn_service.is_business_user;
    this.is_shipment_tab = this._router.url.startsWith('/dashboard/shipments');
    this.c2c_sme_view = this._router.url.startsWith('/dashboard/mytrips') || !this.is_business_user;
    this.setOrderActionOptions()
    this.setNavbarOptions()
  }

  setNavbarOptions() {
    let configPageChildren = ['fleet_overview', 'resource_planning', 'routes', 'awb_generator', 'third_party_integrations']
    // this.showConfigLink = this._blowhorn_service.hasChildPagePerm(configPageChildren)
    let firstUrl = this._blowhorn_service.getFirstChildModuleUrl(configPageChildren)
    this.showConfigLink = this._blowhorn_service.is_org_admin || firstUrl !== null;
    if (firstUrl) {
      this.configurationUrl = firstUrl || this.configurationUrl
    }

    let invoicePageChildren = ['can_view_invoices', 'can_view_pending_invoices', 'can_view_accepted_invoices', 'can_view_disputed_invoices',
      'can_view_partial_paid_invoices', 'can_view_paid_invoices']
    let invoiceFirstUrl = this._blowhorn_service.hasPermissionByCode(invoicePageChildren)
    if (!this._blowhorn_service.is_org_admin && invoiceFirstUrl) {
      this.invoiceUrl = invoiceFirstUrl
    }

    let analyticsPageChildren = ['can_view_analytics', 'can_view_order_reports', 'can_view_trip_reports',
      'can_view_heatmaps', 'can_view_schedule_reports']
    let analyticsFirstUrl = this._blowhorn_service.hasPermissionByCode(analyticsPageChildren)
    if (!this._blowhorn_service.is_org_admin && analyticsFirstUrl) {
      this.analyticsUrl = analyticsFirstUrl
    }

    this.can_view_overview = this._blowhorn_service.hasAtleastOnePermission('overview');
    this.can_view_enterprise_trips = this._blowhorn_service.hasAtleastOnePermission('enterprise_trips');
    this.can_view_booking_trips = this._blowhorn_service.hasAtleastOnePermission('booking_trips');
    this.can_view_shipment_orders = this._blowhorn_service.hasAtleastOnePermission('shipment_orders');
    this.can_view_invoices = this._blowhorn_service.hasAtleastOnePermission('invoices');
    this.can_view_analytics = this._blowhorn_service.hasAtleastOnePermission('analytics');
  }

  setOrderActionOptions() {
    this.can_upload_orders = this._blowhorn_service.hasPermission('shipment_orders', 'can_upload_orders')
    this.can_upload_orderlines = this._blowhorn_service.hasPermission('shipment_orders', 'can_upload_orderlines')
    this.can_download_template = this._blowhorn_service.hasPermission('shipment_orders', 'can_download_template')
    if (this.can_download_template) {
      this.moreActions.push({label: 'Download Template', val: 'template_order'})
    }
    if (this.can_upload_orders) {
      this.moreActions.push({label: 'Upload Orders', val: 'upload_order'})
    }
    if (this.can_upload_orderlines) {
      this.moreActions.push({label: 'Upload Orderlines', val: 'upload_orderline'})
    }
  }

  moreActionSelected(val: string): void {
    switch (val) {
      case 'template_order':
        this.show_download_template();
        break;
      case 'upload_order':
        this.upload_orders();
        break;
      case 'upload_orderline':
        this.update_orders();
        break;
      default:
        console.log('Invalid action');
    }
  }

  create_new_order() {
    window.open('/', '_blank');
  }

  get username_styles(): {} {
    return {
      'dropdown-menu-styles': {
        width: '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  route_clicked(route: string) {
    this.is_shipment_tab = route === 'shipments';
  }

  download_template() {
    if (!this.selected_sku_orientation) {
      alert('Please select SKU orientation');
      return;
    }
    if (!this.selected_type_category) {
      alert('Please select the category');
      return;
    }
    this.show_download_template_modal = false;
    window.open(
      `${Constants.urls.API_SHIPMENT_ORDER_TEMPLATE_EXPORT}?type=${this.selected_type_category}&customer_id=${customer_id}&is_rowwise=${this.selected_sku_orientation == this.ROWWISE_SKU}`,
      '_blank'
    );
  }

  show_download_template() {
    this.show_download_template_modal = !this.show_download_template_modal;
  }

  sku_orientation_selected_upload(event: string) {
    this.selected_sku_orientation_upload = event;
  }

  type_category_selected(event: string) {
    this.selected_type_category = event;
    this.showOrientation = (event === this.ORDER_TEMPLATE)
  }

  sku_orientation_selected(event: string) {
    this.selected_sku_orientation = event;
  }

  upload_orders() {
    this.modal_title = 'upload shipment orders';
    this.show.modal = true;
    this.show.name = 'orders';
    this.show.orientation = true
  }

  update_orders() {
    this.modal_title = 'upload orderlines';
    this.show.modal = true;
    this.show.name = 'orderline';
    this.show.orientation = false;
  }

  focus_panel(event: any) {
    this.overlay_panel.nativeElement.focus();
  }

  focus_panel_download_panel(event: any) {
    this.overlay_panel_download.nativeElement.focus();
  }

  close(event: any) {
    if (event instanceof KeyboardEvent && event.keyCode !== 27) {
      return;
    }
    this.show.modal = false;
    this.show.message = false;
    this.type = 'success';
    this.heading = '';
    this.message = '';
  }

  close_template_download(event: any) {
    if (event instanceof KeyboardEvent && event.keyCode !== 27) {
      return;
    }
    this.show_download_template_modal = false;
  }

  suppress_close(event: any) {
    event.stopPropagation();
  }

  reset() {
    this.file_upload.reset();
    this.show.message = false;
  }

  upload_file(files: any) {
    if (!this.selected_sku_orientation_upload) {
      alert('Please, select SKU orientation');
      return;
    }
    this.file_upload.disable();
    this._blowhorn_service.show_loader();
    const file = files[0];
    const form_data = new FormData();
    form_data.append('shipment_orders', file, file.name);
    form_data.set('is_rowwise', `${this.selected_sku_orientation_upload == this.ROWWISE_SKU}`);
    if (this.show.name === 'orders') {
        this.call_order_upload(form_data);
    } else {
        this.call_orderline_upload(form_data);
    }
  }

  call_order_upload(form_data: any): void {
    this._shipment_service
    .upload_shipment_orders(form_data)
    .subscribe(data => {
      this.show.message = true;
      this.heading = 'Orders Created';
      this.message = `Batch id is ${data['batch']}. Number of orders created is ${data['number_of_orders']}`;
      this.type = 'success';
      this._blowhorn_service.hide_loader();
    }, err => {
      this.show.message = true;
      this.heading = 'Order Creation Failed!';
      this.message = err['error']['errors'];
      this.type = 'error';
      this._blowhorn_service.hide_loader();
    });
  }

  call_orderline_upload(form_data: any): void {
    this._shipment_service
    .upload_orderlines(form_data)
    .subscribe(data => {
      let message = '';
      let invalid_orders = data['invalid_order_numbers'];
      if (invalid_orders.length) {
          message = 'Invalid reference numbers:' + invalid_orders.join(',');
      }
      this.errors = data['validation_errors'];
      this.message = message;
      this.type = !this.errors.length && !this.message ? 'success' : 'error';
      this.heading = this.errors.length || this.message ?
        'Please, fill the missing details'
        : 'Orderlines updated successfully';
      this.show.message = true;
      this._blowhorn_service.hide_loader();
    }, err => {
      this.show.message = true;
      this.heading = 'Failed to upload orderlines!';
      this.message = err['error']['errors'];
      this.type = 'error';
      this._blowhorn_service.hide_loader();
    });
  }

  update_file(files: any) {
    this.file_upload.disable();
    this._blowhorn_service.show_loader();
    const file = files[0];
    const form_data = new FormData();
    form_data.append('shipment_orders', file, file.name);
  }

  get sku_styles(): {} {
    return {
      'dropdown-styles': {
        position: 'relative',
        width: '300px',
        margin: '10px 0 10px 10px',
        boxShadow: 'none',
      },
      'dropdown-menu-styles': {
        width: '100%',
      }
    };
  }

}
