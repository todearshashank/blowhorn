import { Component, OnInit, Input, TemplateRef, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-my-fleet-map-card',
  templateUrl: './my-fleet-map-card.component.html',
  styleUrls: ['./my-fleet-map-card.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyFleetMapCardComponent implements OnInit {
  @Input()
  info: string;
  @Input()
  link: string;
  @Input('router_link')
  router_link: string[];
  @Input()
  styles: {} = {
    'trip-card-styles': {},
    'trip-card-count-styles': {},
    'trip-card-info-styles': {},
    'trip-card-link-styles': {}
  };
  @Input()
  count_template: TemplateRef<any>;
  @Input('show_link')
  show_link = true;
  @Input('show_card')
  show_card: boolean;
  @Input('first_card')
  first_card = true;

  constructor() { }

  ngOnInit() {
  }

}
