import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FleetService } from '../../../shared/services/fleet.service';
import { MapMarker } from '../../../shared/models/map-marker.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-my-fleet-info-window',
  templateUrl: './my-fleet-info-window.component.html',
  styleUrls: ['./my-fleet-info-window.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})

export class MyFleetInfoWindowComponent implements OnInit {

  @Input('trip_header')
  trip_header = 'on trip';
  @Input('marker')
  marker: MapMarker;
  truck_img = `static/${this.blowhorn_service.current_build}/assets/truck/Ace.png`;

  constructor(
    private fleet_service: FleetService,
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {}

  track_order_trip(): void {
    const url = '/track/' + (this.marker.driver.order_number || this.marker.driver.trip_number);
    window.open(url);
  }
}
