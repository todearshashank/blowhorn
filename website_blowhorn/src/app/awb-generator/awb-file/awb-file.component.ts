import { Component, Input, OnInit } from '@angular/core';
import { AwbGeneratorService } from '../../shared/services/awb-generator.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { BlowhornService } from '../../shared/services/blowhorn.service';
import { IdName } from '../../shared/models/id-name.model';

@Component({
    selector: 'app-awb-file',
    templateUrl: './awb-file.component.html',
    styleUrls: ['./awb-file.component.scss']
})
export class AwbFileComponent implements OnInit {

    @Input()
    awb_number;
    show_error = false;
    error_message = '';
    hubs: IdName[] = [];
    selected_hub_id: number = null;
    selected_hub_name = '';
    city_disabled = false;
    hub_disabled = false;
    submit_button = {
        type: 'save',
        label: 'Save',
        disabled: false
    };
    hub_store: IdName[];

    constructor(
        private _awbservice: AwbGeneratorService,
        public _blowhorn_service: BlowhornService,
    ) {
        this._awbservice.get_hub_list().subscribe(data => {
            this.hubs = [new IdName(null, 'No Store')];
            data.data.forEach((item: { hub_code: any; hub_id: number; hub_name: string; }) => {
                if (item.hub_code !== null) {
                    this.hubs.push(new IdName(item.hub_id, item.hub_name));
                }
              });
        });
    }

    ngOnInit() {

    }

    get hub_styles(): {} {
        return {
          'dropdown-styles': {
            'margin-left': '20px',
            'margin-block-start': '50px',
            'background-color': '#e4faf8',
            'color': '#00d2f6',
            'width': '370px',
            'font-weight': '400',
            boxShadow: '0px 0px 4px 0px rgba(0, 0 , 0, .2)',
            'z-index': 2,
            'padding-left': '10px'
          },
          'dropdown-menu-styles': {
            'width': '100%',
            boxShadow: '0px 0px 4px 0px rgba(0, 0 , 0, .2)'
          },
        };
      }

    hub_changed(event: { _id: number; _name: string; }): void {
        this.selected_hub_id = event._id;
        this.selected_hub_name = event._name;
    }

    download_template() {
        var awb_count = Number(this.awb_number);
        this.show_error = false;

        if (awb_count > 0) {
            this.list_fetch(awb_count, this.selected_hub_id);
        }
        else {
            this.show_error = true;
            this.error_message = 'Please enter a valid non zero number';
            this.awb_number = '';
        }
    }

    list_fetch(count: Number, hubID: Number) {
        let data = {
            awb_count: count,
            hub_id: hubID
        };
        this.submit_button.disabled = true;
        this._blowhorn_service.show_loader();
        this._awbservice.get_awb_list(data).subscribe(
            response => {
                let input_data = [];
                response.forEach((element: any) => {
                    input_data.push({ 'awb': element })
                });

                if (input_data) {
                    var options = {
                        headers: ["AWB Number",]
                    }
                    new ngxCsv(input_data, 'awb_list', options)
                };
                this.submit_button.disabled = false;
                this.awb_number = '';
                this.selected_hub_id = null;
                this.selected_hub_name = 'Select the store';
                this._blowhorn_service.hide_loader();
            },
            err => {
                this.submit_button.disabled = false;
                this._blowhorn_service.hide_loader();
                this.selected_hub_id = null;
                this.selected_hub_name = 'Select the store';
                this.show_error = true;
                this.awb_number = '';
                this.error_message = 'Please try again in sometime';
                if (err.error) {
                    this.error_message = err.error;
                }
            }
        );
    }

}
