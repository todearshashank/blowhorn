import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutesMainComponent } from './components/routes-main/routes-main.component';
import { RouteSummaryCardComponent } from './components/route-summary-card/route-summary-card.component';
import { RoutesSummaryComponent } from './components/routes-summary/routes-summary.component';
import { RouteDetailListComponent } from './components/route-detail-list/route-detail-list.component';
import { RouteDetailFooterComponent } from './components/route-detail-footer/route-detail-footer.component';
import { RouteDetailListHeaderComponent } from './components/route-detail-list-header/route-detail-list-header.component';
import { RouteDetailMapComponent } from './components/route-detail-map/route-detail-map.component';
import { RouteDetailMapHeaderCardComponent } from './components/route-detail-map-header-card/route-detail-map-header-card.component';
import { RouteDetailListRowComponent } from './components/route-detail-list-row/route-detail-list-row.component';
import { SaveRouteComponent } from './components/save-route/save-route.component';
import { AgmCoreModule } from '@agm/core';
import { DndListModule } from 'ng6-dnd-lists';
import { SharedModule } from '../shared/shared.module';
import { RouteInfoWindowComponent } from './components/route-info-window/route-info-window.component';
import { LocationModule } from '../location/location.module';
import { ContactModule } from '../contact/contact.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgmDirectionModule } from 'agm-direction';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DndListModule,
    AgmCoreModule,
    AgmDirectionModule,
    LocationModule,
    ContactModule,
    FormsModule,
    HttpClientModule,
  ],
  declarations: [
    RoutesMainComponent,
    RouteSummaryCardComponent,
    RoutesSummaryComponent,
    RouteDetailListComponent,
    RouteDetailFooterComponent,
    RouteDetailListHeaderComponent,
    RouteDetailMapComponent,
    RouteDetailMapHeaderCardComponent,
    RouteDetailListRowComponent,
    SaveRouteComponent,
    RouteInfoWindowComponent,
  ],
  exports: [
    SaveRouteComponent,
    RouteInfoWindowComponent,
  ]
})
export class RouteModule { }
