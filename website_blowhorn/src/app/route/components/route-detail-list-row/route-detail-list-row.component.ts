import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { RouteService } from '../../../shared/services/route.service';

@Component({
  selector: 'app-route-detail-list-row',
  templateUrl: './route-detail-list-row.component.html',
  styleUrls: ['./route-detail-list-row.component.scss']
})
export class RouteDetailListRowComponent implements OnInit {

  @Input()
  stop: RouteStop;
  @Input()
  stop_type: string;
  @Output()
  fire_stop_edit: EventEmitter<RouteStop>;
  constructor(
    private _watcher_service: WatcherService,
    private _route_service: RouteService,
  ) {
    this.fire_stop_edit = new EventEmitter<RouteStop>();
  }

  ngOnInit() {
  }

  show_save_addess() {
    this.fire_stop_edit.emit(this.stop);
    this._route_service
      .update_route_list_save_address(true);
  }

  show_save_contact() {
    this.fire_stop_edit.emit(this.stop);
    this._route_service
      .update_route_list_save_contact(true);
  }

}
