import {
    Component,
    OnInit,
    ViewChild,
    ViewChildren,
    QueryList,
    OnDestroy,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { MapService } from '../../../shared/services/map.service';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { SavedLocation } from '../../../shared/models/saved-location.model';
import { Route } from '../../../shared/models/route.model';
import { NgForm } from '@angular/forms';
import { BlowhornMapSearchComponent } from '../../../shared/components/blowhorn-map-search/blowhorn-map-search.component';
import { RouteService } from '../../../shared/services/route.service';
import { SaveLocationComponent } from '../../../location/components/save-location/save-location.component';
import { SaveContactComponent } from '../../../contact/components/save-contact/save-contact.component';
import { Contact } from '../../../shared/models/contact.model';
import { Address } from '../../../shared/models/address.model';
import { GeoPoint } from '../../../shared/models/geopoint.model';
import { GeoCoder } from '@ngui/map';
import { Constants } from '../../../shared/utils/constants';

@Component({
    selector: 'app-save-route',
    templateUrl: './save-route.component.html',
    styleUrls: ['./save-route.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SaveRouteComponent implements OnInit, OnDestroy {
    @ViewChild('route_form', {static: false}) route_form: NgForm;
    @ViewChildren(BlowhornMapSearchComponent)
    map_search_comps: QueryList<BlowhornMapSearchComponent>;
    @ViewChild('address_form', {static: false}) private _address_form: SaveLocationComponent;
    @ViewChild('contact_form', {static: false}) private _contact_form: SaveContactComponent;
    selected_city_name = '';
    selected_city_id: number = null;
    first_stop_selected: google.maps.LatLng = MapService.dummy_geopoint;
    last_stop_selected: google.maps.LatLng = MapService.dummy_geopoint;
    remaining_stops_selected: { location: google.maps.LatLng }[] = [];
    stops_selected: RouteStop[] = [];
    inserted_index: number;
    stops_drag_list: number[] = null;
    map_latitude = 12.9716;
    map_longitude = 77.5946;
    map_bounds: google.maps.LatLngBounds;
    show_save_address = false;
    show_save_contact = false;
    stop_under_edit_index: number = null;
    saved_locations: SavedLocation[] = [];
    route: Route;
    current_location: google.maps.LatLng;
    is_current_location_in_city = false;
    _show_direction = false;
    submittingForm = false;
    private $route: any;
    private $saved_locations: any;
    private $save_address: any;
    private $save_contact: any;
    _first_time = true;
    _route_service: RouteService;
    _blowhorn_service: BlowhornService;
    _map_service: MapService;
    directions: google.maps.DirectionsResult;
    origin = { lat: 24.799448, lng: 120.979021 };
    destination = { lat: 24.799524, lng: 120.975017 };
    is_error = false;
    error_msg = '';
    check_place: any;
    check_place_with_location: any;
    regex = Constants.regex;
    isdCodeStyles = {
        wrapper: {
            'margin-bottom': '5px',
            background: '#fff',
            height: 'unset',
            'border-bottom': '1px solid #F0F5F7',
            padding: 0
        }
    }

    constructor(
        private _watcher_service: WatcherService,
        public blowhorn_service: BlowhornService,
        private map_service: MapService,
        private route_service: RouteService,
        private _change_detector: ChangeDetectorRef,
    ) {
        this._route_service = route_service;
        this._blowhorn_service = blowhorn_service;
        this._map_service = map_service;
    }

    ngOnDestroy() {
        this.$route.unsubscribe();
        this.$save_address.unsubscribe();
        this.$save_contact.unsubscribe();
        this.$saved_locations.unsubscribe();
    }

    ngOnInit() {
        this.$route = this._route_service.new_route.subscribe(route => {
            this.route = new Route();
            if (route && this._first_time) {
                this._first_time = false;
                this.route.city = route.city;
                let contact = this.blowhorn_service.get_national_number(route.contact.mobile);
                route.contact.mobile = contact ? contact.toString() : '';
                this.route.contact = Object.assign(
                    new Contact(),
                    route.contact
                );
                this.route.distance = route.distance;
                this.route.geopoint = Object.assign(
                    new google.maps.LatLng(0, 0),
                    route.geopoint
                );
                this.route.id = route.id;
                this.route.name = route.name;
                route.stops.forEach(stop => {
                    const new_stop = new RouteStop();
                    new_stop.address = Object.assign(
                        new Address(),
                        stop.address
                    );
                    new_stop.contact = Object.assign(
                        new Contact(),
                        stop.contact
                    );
                    new_stop.geopoint = Object.assign(
                        new google.maps.LatLng(0, 0),
                        stop.geopoint
                    );
                    new_stop.id = stop.id;
                    new_stop.position = [stop.position[0], stop.position[1]];
                    new_stop.saved_location = new SavedLocation();
                    new_stop.saved_location.address = Object.assign(
                        new Address(),
                        stop.saved_location ? stop.saved_location.address : {}
                    );
                    new_stop.saved_location.contact = Object.assign(
                        new Contact(),
                        stop.saved_location ? stop.saved_location.contact : {}
                    );
                    new_stop.saved_location.geopoint = Object.assign(
                        new GeoPoint(),
                        stop.saved_location ? stop.saved_location.geopoint : {}
                    );
                    new_stop.saved_location.name = stop.saved_location ? stop.saved_location.name : '';
                    new_stop.seq_no = stop.seq_no;
                    this.route.stops.push(new_stop);
                });
            } else if (route) {
                this.route = route;
            }
            this.stops_selected = this.route.stops.filter(item => {
                return item.position && item.position[0] && item.position[1]
                    ? true
                    : false;
            });
            this.stops_drag_list = this.route.stops.map((item, index) => {
                return index + 1;
            });
            if (route && route.city) {
                const city = this._blowhorn_service.cities.find(
                    item => {
                        console.log('item.name', item.name);
                        return item.name === route.city
                    }
                );
                console.log('route.city: ', route.city, city);
                this.selected_city_name = city.name;
                this.selected_city_id = city.id;
            } else {
                this.selected_city_name = this._blowhorn_service.default_city.name;
                this.selected_city_id = this._blowhorn_service.default_city.id;
            }
            this.show_hide_current_location();
            this._show_direction = false;
            this.remaining_stops_selected = [];
            this.first_stop_selected = this.last_stop_selected =
                MapService.zero_geopoint;
            if (this.stops_selected.length === 1) {
                this.first_stop_selected = MapService.map_position_to_geopoint(
                    this.stops_selected[0].position
                );
                this.last_stop_selected = this.first_stop_selected;
                this._show_direction = true;
            } else if (this.stops_selected.length === 2) {
                this.first_stop_selected = MapService.map_position_to_geopoint(
                    this.stops_selected[0].position
                );
                this.last_stop_selected = MapService.map_position_to_geopoint(
                    this.stops_selected[1].position
                );
                this._show_direction = true;
            } else if (this.stops_selected.length > 2) {
                this.first_stop_selected = MapService.map_position_to_geopoint(
                    this.stops_selected[0].position
                );
                this.last_stop_selected = MapService.map_position_to_geopoint(
                    this.stops_selected[this.stops_selected.length - 1].position
                );
                this.remaining_stops_selected = this.stops_selected
                    .slice(1, this.stops_selected.length - 1)
                    .map(item => {
                        return {
                            location: MapService.map_position_to_geopoint(
                                item.position
                            )
                        };
                    });
                this._show_direction = true;
            }
            this.map_bounds = MapService.dummy_bounds;
            if (this.stops_selected.length) {
                this.stops_selected.forEach(stop => {
                    this.map_bounds.extend(stop.geopoint);
                });
            } else {
                this._blowhorn_service.get_city_coverage(this.selected_city_id, 'dropoff')
                    .forEach(item => {
                        this.map_bounds.extend(item);
                    });
            }
        });
        this.$saved_locations = this._blowhorn_service.saved_locations.subscribe(
            saved_locations => {
                this.saved_locations = saved_locations;
            }
        );
        this.$save_address = this._watcher_service.show_save_address.subscribe(
            show_save_address => {
                console.log('show_save_address: ', show_save_address);
                this.show_save_address = show_save_address;
            }
        );
        this.$save_contact = this._watcher_service.show_save_contact.subscribe(
            show_save_contact => {
                this.show_save_contact = show_save_contact;
            }
        );
    }

    cancel() {
        this._blowhorn_service.hide_modal();
    }

    get dropdown_styles() {
        return {
            'dropdown-styles': {
                boxShadow: 'none',
                border: 'none',
                backgroundColor: '#f7fbfc',
                color: '#757E7F',
                fontFamily: 'Heebo',
                fontSize: '14px',
                fontWeight: 900,
                letterSpacing: '2px',
                lineHeight: '21px',
                textTransform: 'uppercase'
            },
            'dropdown-menu-styles': {
                width: '100%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    add_stop() {
        const new_stop = new RouteStop();
        this.route.stops.push(new_stop);
        this._route_service.update_new_route(this.route);
    }

    delete_stop(event) {
        const index = this.route.stops.findIndex(
            x => x.seq_no === event.seq_no
        );
        this.route.stops.splice(index, 1);
        this._route_service.update_new_route(this.route);
    }

    stop_moved(index: number, list: any[]): void {
        if (this.inserted_index > index) {
            this.route.stops.splice(index, 1);
        } else {
            this.route.stops.splice(index + 1, 1);
        }
        this.inserted_index = null;
        this._route_service.update_new_route(this.route);
    }

    update_stop(event: RouteStop, stop: RouteStop) {
        stop = event;
        this._route_service.update_new_route(this.route);
    }

    marker_dragged(event, stop) {
        stop.position = [event.coords.lat, event.coords.lng];
        stop.geopoint = MapService.map_position_to_geopoint(stop.position);
        stop.saved_location = new SavedLocation();
        this._map_service
            .get_geocode(stop.geopoint)
            .then(results => {
                const result = results[0];
                stop.address = this._map_service.getAddressComponentsObject(
                    result.address_components
                );
                stop.address.name = '';
                this._route_service.update_new_route(this.route);
            })
            .catch(status => {
                console.info('Geocoding Failed', status);
            });
    }

    stop_inserted(event) {
        this.inserted_index = event.index;
        const index_copied = event.item - 1;
        this.route.stops.splice(
            this.inserted_index,
            0,
            this.route.stops[index_copied]
        );
        this._route_service.update_new_route(this.route);
    }

    pan_to(event) {
        this.map_latitude = event.lat();
        this.map_longitude = event.lng();
    }

    calculate_distance(): number {
        let distance = 0;
        const legs = this.directions.routes[0].legs;
        for (const leg of legs) {
            distance += leg.distance.value;
        }
        return distance;
    }

    city_changed(event) {
        this.selected_city_name = event.id ? event.name : 'choose city';
        this.selected_city_id = event.id;
        this.route.city =
            this.selected_city_name && this.selected_city_name !== 'choose city'
                ? this.selected_city_name
                : '';
        this.route.stops = [new RouteStop(null, 0), new RouteStop(null, 1)];
        this._route_service.update_new_route(this.route);
        this.show_hide_current_location();
        this.map_search_comps.forEach(
            (item, index) =>
                (item.placeholder = this.get_stop_placeholder(index))
        );
        this._first_time = true;
        this._show_direction = false;
    }

    open_contact_form(event: boolean, stop: RouteStop, index: number) {
        this.stop_under_edit_index = index;
        this.show_save_contact = true;
    }

    open_address_form(event, stop: RouteStop, index: number) {
        this.stop_under_edit_index = index;
        this.show_save_address = true;
    }

    reset_error() {
        this.is_error = false;
        this.error_msg = '';
    }

    save_route() {
        this.route.city = this.selected_city_name;
        this.route.distance = this.calculate_distance() + '';
        const route_data = {
            route: this._route_service.get_route_data_for_api(this.route)
        };
        this._route_service.save_route(route_data).subscribe(result => {
            this._route_service.get_routes_data_from_api(result);
            this._blowhorn_service.hide_modal();
        }, err => {
            console.info(err);
            this.is_error = true;
            this.error_msg = err.error || err;
            this._change_detector.detectChanges();
        });
    }

    edit_route() {
        this.route.city = this.selected_city_name;
        this.route.distance = this.calculate_distance() + '';
        const route_data = {
            route: this._route_service.get_route_data_for_api(this.route)
        };
        this.submittingForm = true;
        this._route_service.edit_route(route_data).subscribe(result => {
            this._route_service.get_routes_data_from_api(result);
            this._blowhorn_service.hide_modal();
            this.submittingForm = false;
        }, err => {
            console.info(err);
            this.is_error = true;
            this.error_msg = err.error || err;
            this.submittingForm = false;
            this._change_detector.detectChanges();
        });
    }

    clear_stop_under_edit_index() {
        this.stop_under_edit_index = null;
    }

    update_stop_address(event) {
        this.route.stops[this.stop_under_edit_index].contact.mobile =
            event.contact_number;
        this.route.stops[this.stop_under_edit_index].contact.name =
            event.contact_name;
        this._route_service.update_new_route(this.route);
    }

    update_stop_contact(event) {
        this.route.stops[this.stop_under_edit_index].contact.mobile =
            event.contact_number;
        this.route.stops[this.stop_under_edit_index].contact.name =
            event.contact_name;
        this._route_service.update_new_route(this.route);
        this._contact_form.clear_form();
    }

    show_hide_current_location() {
        if (
            this._blowhorn_service.is_navigator_geolocation &&
            this.selected_city_id
        ) {
            this.is_current_location_in_city = this._map_service.is_current_location_in_city(
                this.selected_city_id,
                'dropoff'
            ) || this._map_service.is_current_location_in_city(
                this.selected_city_id,
                'pickup'
            );;
        }
    }

    place_changed(place: google.maps.places.PlaceResult, stop: RouteStop) {
        let location_type = stop.seq_no === 0 ? 'pickup' : 'dropoff';
        const comp = this.map_search_comps.find(item => {
            return item.stop.seq_no === stop.seq_no;
        });

       if (
            this._map_service.is_location_in_city(
                this.selected_city_id,
                place.geometry.location,
                location_type
            )
        ) {
            comp.placeholder = this._route_service.get_stop_placeholder(stop.seq_no);
            stop.geopoint = place.geometry.location;
            stop.address = this._map_service.getAddressComponentsObject(
                place.address_components
            );
            stop.position = MapService.map_geopoint_to_position(
                place.geometry.location
            );
            stop.address.name = '';
            stop.saved_location = new SavedLocation();
            this.pan_to(stop.geopoint);
        } else {
            comp.placeholder = 'Out of coverage';
            this.route.stops.splice(
            stop.seq_no,
            1,
            new RouteStop(null, stop.seq_no)
            );
        }
        this._route_service.update_new_route(this.route);
    }

    update_stop_saved_location(saved_location: SavedLocation, stop: RouteStop) {
        stop.saved_location = saved_location;
        if (!stop.contact.name) {
            stop.contact.name = saved_location.contact.name;
        }
        if (!stop.contact.mobile) {
            stop.contact.mobile = saved_location.contact.mobile;
        }
        stop.address.name = saved_location.name;
        this._address_form.clear_form();
    }

    get_stop_placeholder(index): string {
        return this._route_service.get_stop_placeholder(index);
    }

    get_icon_url(index): string {
        return this._route_service.get_icon_url(index);
    }

    get_location_form_header(): string {
        return this._route_service.get_location_form_header();
    }

    set_directions(directions) {
        this.directions = directions;
    }
}
