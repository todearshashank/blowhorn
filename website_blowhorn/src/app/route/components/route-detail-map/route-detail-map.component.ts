import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { Route } from '../../../shared/models/route.model';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { MapService } from '../../../shared/services/map.service';
import { RouteService } from '../../../shared/services/route.service';
import { SaveLocationComponent } from '../../../location/components/save-location/save-location.component';
import { SaveContactComponent } from '../../../contact/components/save-contact/save-contact.component';
import { SavedLocation } from '../../../shared/models/saved-location.model';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-route-detail-map',
  templateUrl: './route-detail-map.component.html',
  styleUrls: ['./route-detail-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteDetailMapComponent implements OnInit, OnChanges {

  @ViewChild('address_form', {static: false})
  private _address_form: SaveLocationComponent;
  @ViewChild('contact_form', {static: false})
  private _contact_form: SaveContactComponent;
  @Input()
  route: Route;
  renderOptions = {
      suppressMarkers: true,
      draggable: false,
      preserveViewport: true,
      polylineOptions: {
        geodesic: true,
        strokeColor: '#f74757',
        strokeOpacity: 1.0,
        strokeWeight: 4
      }
  };
  show_save_contact = false;
  show_save_address = false;
  latitude = 12.9716;
  longitude = 77.5946;
  bounds: google.maps.LatLngBounds;
  waypoints: {location: google.maps.LatLng}[];
  stop_under_edit_index: number = null;
  is_error = {
    contact: false,
    address: false
  };
  error_heading = '';
  error_msg = '';
  submitting = false;

  constructor(
    private _map_service: MapService,
    private _route_service: RouteService,
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const route_city = this.route.city;
    this.bounds = MapService.dummy_bounds;
    if (this.route.stops.length) {
      this.route.stops.forEach( stop => {
        this.bounds.extend(stop.geopoint);
      });
    }
    this.waypoints = this.route.stops.slice(1, this.route.stops.length - 1).map(item => {
      return { location: MapService.map_position_to_geopoint(item.position) };
    });
  }

  get_icon_url(index: number): string {
    if (index) {
      return index < this.route.stops.length - 1 ?
        Constants.markers.stop :
        Constants.markers.dropoff;
    } else {
      return Constants.markers.pickup;
    }
  }

  get_stop_header(index: number): string {
    return `Stop ${index + 1}`;
  }

  open_contact_form(event: boolean, stop: RouteStop, index: number) {
    this.stop_under_edit_index = index;
    this.show_save_contact = true;
  }

  open_address_form(event, stop: RouteStop, index: number) {
    this.stop_under_edit_index = index;
    this.show_save_address = true;
  }

  clear_stop_under_edit_index() {
    this.stop_under_edit_index = null;
  }

  update_stop_saved_location(saved_location: SavedLocation, stop: RouteStop) {
    stop.saved_location = saved_location;
    if (!stop.contact.name) {
      stop.contact.name = saved_location.contact.name;
    }
    if (!stop.contact.mobile) {
      stop.contact.mobile = saved_location.contact.mobile;
    }
    stop.address.name = saved_location.name;
    this.submitting = true;
    this._route_service
      .edit_route({route: this._route_service.get_route_data_for_api(this.route)})
      .subscribe(result => {
        this._route_service.get_routes_data_from_api(result);
        this.close_address_form();
        this.submitting = false;
      }, err => {
        console.info(err);
        this.is_error.address = true;
        this.error_heading = 'Failed to save address';
        this.error_msg = err.error || err;
        this.submitting = false;
        this.show_save_address = false;
      });
  }

  update_stop_contact(event: {contact_number: number, contact_name: string}, stop: RouteStop) {
    stop.contact.mobile = event.contact_number + '';
    stop.contact.name = event.contact_name;
    this.submitting = true;
    this._route_service
      .edit_route({route: this._route_service.get_route_data_for_api(this.route)})
      .subscribe(result => {
        this._route_service.get_routes_data_from_api(result);
        this.close_contact_form(true);
        this.submitting = false;
      }, err => {
        console.info(err);
        this.is_error.contact = true;
        this.error_heading = 'Failed to save contact';
        this.error_msg = err.error || err;
        this.submitting = false;
        this.show_save_contact = false;
      });
  }

  close_address_form() {
    this._address_form.clear_form();
    this.show_save_address = false;
    this.clear_stop_under_edit_index();
  }

  close_contact_form(reset: boolean) {
    if (reset) {
      this._contact_form.clear_form();
      this.clear_stop_under_edit_index();
    }
    this.show_save_contact = false;
  }

  reset_error() {
    this.error_msg = '';
    this.error_heading = '';
    if (this.is_error.contact) {
      this.show_save_contact = true;
      this.is_error.contact = false;
    } else if (this.is_error.address) {
      this.show_save_address = true;
      this.is_error.address = false;
    }
  }

}
