import { Component, OnInit, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Route } from '../../../shared/models/route.model';
import { RouteService } from '../../../shared/services/route.service';
import { SaveContactComponent } from '../../../contact/components/save-contact/save-contact.component';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-route-detail-list-header',
  templateUrl: './route-detail-list-header.component.html',
  styleUrls: ['./route-detail-list-header.component.scss']
})
export class RouteDetailListHeaderComponent implements OnInit {

  map_active = false;
  @Input()
  route: Route;
  @Output('view_changed')
  view_changed: EventEmitter<boolean>;
  @ViewChild('contact_form', {static: false})
  private _contact_form: SaveContactComponent;
  show_save_contact = false;

  constructor(
    private _route_service: RouteService,
    private bhService: BlowhornService,
  ) {
    this.view_changed = new EventEmitter<boolean>(false);
  }

  ngOnInit() {
    this.route.contact.mobile =  this.bhService.get_national_number(this.route.contact.mobile).toString();
  }

  fire_view_changed(map_view: boolean) {
    this.view_changed.emit(map_view);
  }

  update_route_contact(event: {contact_name: string, contact_number: number}) {
    this.route.contact.name = event.contact_name;
    this.route.contact.mobile = event.contact_number + '';
    this._route_service
      .edit_route({route: this._route_service.get_route_data_for_api(this.route)})
      .subscribe(result => {
        this._route_service.get_routes_data_from_api(result);
        this.close_contact_form();
      });
  }

  close_contact_form() {
    this.show_save_contact = false;
    this._contact_form.clear_form();
  }

  open_contact_form() {
    this.show_save_contact = true;
  }
}
