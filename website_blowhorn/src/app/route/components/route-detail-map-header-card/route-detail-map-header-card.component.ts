import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Route } from '../../../shared/models/route.model';
import { RouteService } from '../../../shared/services/route.service';
import { SaveContactComponent } from '../../../contact/components/save-contact/save-contact.component';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-route-detail-map-header-card',
  templateUrl: './route-detail-map-header-card.component.html',
  styleUrls: ['./route-detail-map-header-card.component.scss']
})
export class RouteDetailMapHeaderCardComponent implements OnInit {


  @Input()
  route: Route;
  @ViewChild('contact_form', {static: false})
  private _contact_form: SaveContactComponent;
  show_save_contact = false;
  constants = Constants;
  constructor(
    private _route_service: RouteService,
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
  }

  update_route_contact(event: {contact_name: string, contact_number: number}) {
    this.route.contact.name = event.contact_name;
    this.route.contact.mobile = event.contact_number + '';
    this._route_service
      .edit_route({route: this._route_service.get_route_data_for_api(this.route)})
      .subscribe(result => {
        this._route_service.get_routes_data_from_api(result);
        this.close_contact_form();
      });
  }

  close_contact_form() {
    this.show_save_contact = false;
    this._contact_form.clear_form();
  }

  open_contact_form() {
    this.show_save_contact = true;
  }

}
