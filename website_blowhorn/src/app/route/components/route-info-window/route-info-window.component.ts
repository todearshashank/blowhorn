import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { WatcherService } from '../../../shared/services/watcher.service';

@Component({
  selector: 'app-route-info-window',
  templateUrl: './route-info-window.component.html',
  styleUrls: ['./route-info-window.component.scss']
})
export class RouteInfoWindowComponent implements OnInit {

  @Input()
  stop: RouteStop;
  @Input()
  stop_header: string;
  @Output()
  open_contact_form: EventEmitter<{stop: RouteStop, stop_type: string}>;
  @Output()
  open_address_form: EventEmitter<{stop: RouteStop}>;
  @Input()
  enable_location = true;

  constructor(
    private _watcher_service: WatcherService,
  ) {
    this.open_contact_form = new EventEmitter<{stop: RouteStop, stop_type: string}>();
    this.open_address_form = new EventEmitter<{stop: RouteStop}>();
  }

  ngOnInit() {
  }

  edit_save_address(stop: RouteStop) {
    this.open_address_form.emit({stop: this.stop});
  }

  add_edit_contact(stop: RouteStop) {
    this.open_contact_form.emit({stop: this.stop, stop_type: this.stop_header});
  }

}
