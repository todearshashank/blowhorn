import { Component, OnInit, Input } from '@angular/core';
import { Route } from '../../../shared/models/route.model';
import { RouteService } from '../../../shared/services/route.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { WatcherService } from '../../../shared/services/watcher.service';
import { SaveRouteComponent } from '../save-route/save-route.component';

@Component({
  selector: 'app-route-detail-footer',
  templateUrl: './route-detail-footer.component.html',
  styleUrls: ['./route-detail-footer.component.scss']
})
export class RouteDetailFooterComponent implements OnInit {

  @Input('map_view')
  map_view: boolean;
  @Input()
  route: Route;
  show_confirm = false;

  constructor(
    private _route_service: RouteService,
    public _blowhorn_service: BlowhornService,
    private _watcher_service:  WatcherService,
  ) { }

  ngOnInit() {
  }

  delete_route() {
    this.show_confirm = true;
  }

  edit_route() {
    this._blowhorn_service
      .show_modal(SaveRouteComponent);
    this._watcher_service
      .update_show_save_route(true);
    this._route_service
      .edit_selected_route();
    this._route_service
      .save_route_mode = 'edit';
  }

  proceed(event) {
    if (event) {
      this._route_service
        .delete_route(this.route.id)
        .subscribe(result => {
          this._route_service.get_routes_data_from_api(result);
          this._route_service.update_selected_route(null);
          this.show_confirm = false;
        });
    } else {
      this.show_confirm = false;
    }
  }

}
