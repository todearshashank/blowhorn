import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Route } from '../../../shared/models/route.model';
import { RouteStop } from '../../../shared/models/route-stop.model';

@Component({
  selector: 'app-route-summary-card',
  templateUrl: './route-summary-card.component.html',
  styleUrls: ['./route-summary-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteSummaryCardComponent implements OnInit {

  @Input()
  route: Route;
  total_stops: number;
  constructor() { }

  ngOnInit() {
    this.total_stops = this.route.stops.length;
  }

  get from_address(): string {
      const from_stop: RouteStop = this.route.stops.filter(stop => {
        return +stop.seq_no === 0;
      })[0];
      return from_stop.address.street_address ? from_stop.address.street_address : from_stop.address.full_address;
  }

  get to_address(): string {
      const to_stop: RouteStop = this.route.stops.filter(stop => {
        return +stop.seq_no === this.total_stops - 1;
      })[0];
      return to_stop.address.street_address ? to_stop.address.street_address : to_stop.address.full_address;
  }

}
