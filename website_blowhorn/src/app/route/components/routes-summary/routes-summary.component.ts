import { Component, OnInit, Input, AfterViewInit, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { Route } from '../../../shared/models/route.model';
import { FormsModule } from '@angular/forms'
import { RouteStop } from '../../../shared/models/route-stop.model';
import { CityFilterPipe } from '../../../shared/pipes/city-filter.pipe';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { SaveRouteComponent } from '../save-route/save-route.component';
import { RouteService } from '../../../shared/services/route.service';

@Component({
  selector: 'app-routes-summary',
  templateUrl: './routes-summary.component.html',
  styleUrls: ['./routes-summary.component.scss']
})
export class RoutesSummaryComponent implements OnInit, AfterViewInit, OnChanges {

  @Input()
  routes: Route[] = [];
  @Input()
  selected_route: Route;
  filter_keyword: string;
  _city_routes = new Map<string | number, Route[]>();
  _city_panel_visibility_list: boolean[] = [];
  _search_placeholder: string;
  _city_routes_arr: {name: string | number, routes: Route[]}[];
  _city_visible = new Map<string | number, boolean>();


  constructor(
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private _route_service: RouteService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    this._set_place_holder();
    this._set_city_routes();
    this._set_city_visible();
  }

  ngOnInit() { }

  ngAfterViewInit() { }

  add_new_route() {
    this._blowhorn_service
      .show_modal(SaveRouteComponent);
    this.watcher_service
      .update_show_save_route(true);
    this._route_service
      .initialize_new_route();
    this._route_service
      .save_route_mode = 'add';
  }

  get city_count_map(): IterableIterator<[any, any]> {
    const city_count_map = new Map();
    this.routes.forEach(item => {
      if (city_count_map.has(item.name)) {
        city_count_map.set(item.name, city_count_map.get(item.name) + 1);
      } else {
        city_count_map.set(item.name, 1);
      }
    });
    return city_count_map.entries();
  }

  select_route(route: Route) {
      this._route_service
          .update_selected_route(route);
  }

  _filter_cities() {
    this._set_city_routes();
    this._set_city_visible();
  }

  private _set_place_holder() {
    let route_str = 'route';
    if (this.routes.length > 1) {
      route_str += 's';
    }
    this._search_placeholder =  `${this.routes.length || 'No'} ${route_str} configured`;
  }

  private _set_city_routes() {
    this._city_routes.clear();
    this.routes.forEach(item => {
      if (!this.filter_keyword || item.name.toLowerCase().indexOf(this.filter_keyword.toLowerCase()) >= 0) {
        if (this._city_routes.get(item.city)) {
           this._city_routes.set(item.city, [...this._city_routes.get(item.city), item]);
        } else {
          this._city_routes.set(item.city, [item]);
        }
      }
    });
    this._city_routes_arr = [];
    this._city_routes.forEach((routes, city) => {
      this._city_routes_arr.push({
        name: city,
        routes: routes
      });
    });
  }

  private _set_city_visible() {
    this._city_routes_arr
      .forEach(item => {
        if (!this._city_visible.get(item.name)) {
          this._city_visible.set(item.name, true);
        }
      });
    this._city_visible.forEach((val, key) => {
      if (!this._city_routes_arr.find(item => item.name === key)) {
        this._city_visible.delete(key);
      }
    });
  }

  hide_show(key: string | number) {
    this._city_visible.set(key, !this._city_visible.get(key));
  }

  load_route(route: Route) {
    this._route_service.update_selected_route(route);
  }

}
