import { Component, OnInit } from '@angular/core';
import { Route } from '../../../shared/models/route.model';
import { RouteService } from '../../../shared/services/route.service';
import { MapsAPILoader } from '@agm/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-routes-main',
  templateUrl: './routes-main.component.html',
  styleUrls: ['./routes-main.component.scss']
})
export class RoutesMainComponent implements OnInit {

  map_view = true;
  routes: Route[] = [];
  selected_route: Route;
  constructor(
    private _route_service: RouteService,
    public bhService: BlowhornService,
  ) {

    this.bhService.user_profile_obs.subscribe(
        val => {
            console.log('prfile: ', val);
            if (val) {
                this._route_service
                    .get_routes()
                    .subscribe(result => {
                        this._route_service.get_routes_data_from_api(result);
                    });
            }
        }
    );
    this._route_service
      .routes
      .subscribe((routes: Route[]) => {
        this.routes = routes;
      });
    this._route_service
      .selected_route
      .subscribe(route => {
        this.selected_route = route;
      });
  }

  ngOnInit() {
  }

  switch_to_map() {
    this.map_view = true;
  }

  switch_to_list() {
    this.map_view = false;
  }

  update_route(route: Route) {
  }

}
