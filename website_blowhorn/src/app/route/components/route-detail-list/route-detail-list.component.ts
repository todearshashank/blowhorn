import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { Route } from '../../../shared/models/route.model';
import { RouteService } from '../../../shared/services/route.service';
import { SavedLocation } from '../../../shared/models/saved-location.model';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { SaveLocationComponent } from '../../../location/components/save-location/save-location.component';
import { SaveContactComponent } from '../../../contact/components/save-contact/save-contact.component';

@Component({
  selector: 'app-route-detail-list',
  templateUrl: './route-detail-list.component.html',
  styleUrls: ['./route-detail-list.component.scss']
})
export class RouteDetailListComponent implements OnInit, OnDestroy, OnChanges {

  @ViewChild('address_form', {static: false})
  private _address_form: SaveLocationComponent;
  @ViewChild('contact_form', {static: false})
  private _contact_form: SaveContactComponent;
  @Input()
  route: Route;
  show_save_address = false;
  show_save_contact = false;
  private $save_address: any;
  private $save_contact: any;
  private _stop_under_edit;
  is_error = {
    contact: false,
    address: false
  };
  error_heading = '';
  error_msg = '';
  submitting = false;

  constructor(
    private _route_service: RouteService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy() {
    this.$save_address.unsubscribe();
    this.$save_contact.unsubscribe();
  }

  ngOnInit() {
    this.$save_address = this._route_service
                             .route_list_save_address
                             .subscribe(val => this.show_save_address = val);
    this.$save_contact = this._route_service
                             .route_list_save_contact
                             .subscribe(val => this.show_save_contact = val);
  }

  get_stop_type(index: number): string {
    if (index === 0) {
      return 'pickup';
    } else if (index === this.route.stops.length - 1) {
      return 'dropoff';
    } else {
      return 'stop';
    }
  }

  set_stop_under_edit(stop) {
    this._stop_under_edit = stop;
  }

  close_address_form() {
    this._route_service.update_route_list_save_address(false);
    this.clear_stop_under_edit();
    this._address_form.clear_form();
  }

  close_contact_form() {
    this._route_service.update_route_list_save_contact(false);
    this.clear_stop_under_edit();
    this._contact_form.clear_form();
  }

  clear_stop_under_edit() {
  }

  update_stop_saved_location(saved_location: SavedLocation, stop: RouteStop) {
    stop.saved_location = saved_location;
    if (!stop.contact.name) {
      stop.contact.name = saved_location.contact.name;
    }
    if (!stop.contact.mobile) {
      stop.contact.mobile = saved_location.contact.mobile;
    }
    stop.address.name = saved_location.name;
    this._route_service
      .edit_route({route: this._route_service.get_route_data_for_api(this.route)})
      .subscribe(result => {
        this._route_service.get_routes_data_from_api(result);
        this.close_address_form();
      }, err => {
        console.info(err);
        this.is_error.address = true;
        this.error_heading = 'Failed to save address';
        this.error_msg = err.error || err;
        this.submitting = false;
        this.show_save_address = false;
      });
  }

  update_stop_contact(event: {contact_name: string, contact_number: number}, stop: RouteStop) {
    stop.contact.name = event.contact_name;
    stop.contact.mobile = event.contact_number + '';
    this._route_service
      .edit_route({route: this._route_service.get_route_data_for_api(this.route)})
      .subscribe(result => {
        this._route_service.get_routes_data_from_api(result);
        this.close_contact_form();
      }, err => {
        console.info(err);
        this.is_error.contact = true;
        this.error_heading = 'Failed to save contact';
        this.error_msg = err.error || err;
        this.submitting = false;
        this.show_save_contact = false;
      });
  }

  sorted_stops(stops: RouteStop[]): RouteStop[] {
    return stops.sort((item1, item2) => item1.seq_no - item2.seq_no);
  }

  reset_error() {
    this.error_msg = '';
    this.error_heading = '';
    if (this.is_error.contact) {
      this.show_save_contact = true;
      this.is_error.contact = false;
    } else if (this.is_error.address) {
      this.show_save_address = true;
      this.is_error.address = false;
    }
  }

}
