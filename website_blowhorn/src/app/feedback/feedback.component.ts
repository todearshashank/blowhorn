import { Component, OnInit } from "@angular/core";
import { FeedbackService } from "../../app/shared/services/feedback.service";
import { BlowhornService } from '../shared/services/blowhorn.service';

@Component({
  selector: "app-feedback",
  templateUrl: "./feedback.component.html",
  styleUrls: ["./feedback.component.scss"]
})
export class FeedbackComponent implements OnInit {
  url: any;
  ratingInNumber: any;
  orderId: any;
  comments: any;
  saveData: any;
  remarks: any = [];
  remarksData: any;
  remarksTotalData: any;
  customerDetails: any;
  originlRemarksTotalData: any;
  Message: any = 1;
  constructor(
    private feedbackservice: FeedbackService,
    public bh_service: BlowhornService
  ) {}
  ngOnInit() {
    this.getURL();
    this.getTotalRemarks();
  }
  getURL() {
    this.url = window.location.href;
    this.orderId = this.url.substring(this.url.lastIndexOf("/") + 1);
    this.feedbackservice.get_customerdetails(this.orderId).subscribe(result => {
      this.customerDetails = result;
      if (this.customerDetails.driver_rating){
        this.Message =3;
      }
      if (this.customerDetails.status === "FAIL"){
        this.Message =4;
      }

    });
  }
  getTotalRemarks() {
    this.feedbackservice.get_remarks().subscribe(data => {
      this.originlRemarksTotalData = data.driver_rating_config;
      this.remarksTotalData = JSON.parse(JSON.stringify(this.originlRemarksTotalData));
      this.remarksTotalData.sort((a, b) => (a.rating > b.rating) ? 1 : -1)
      for (let i = 0; i < this.remarksTotalData.length; i++) {
        for (let j = 0; j < this.remarksTotalData[i].remarks.length; j++) {
          this.remarksTotalData[i].remarks[j].checked = false;
        }
      }
    });
  }
  onClick(rating: number): void {
    this.ratingInNumber = rating;
    this.remarks = [];
    if (this.ratingInNumber === 1) {
      this.remarksData = this.remarksTotalData[0];
    } else if (this.ratingInNumber === 2) {
      this.remarksData = this.remarksTotalData[1];
    } else if (this.ratingInNumber === 3) {
      this.remarksData = this.remarksTotalData[2];
    } else if (this.ratingInNumber === 4) {
      this.remarksData = this.remarksTotalData[3];
    } else if (this.ratingInNumber === 5) {
      this.remarksData = this.remarksTotalData[4];
    }
  }
  public sendCheckedReviews(): void {
    const Reviews = this.remarksData.remarks.filter(review => review.checked);
    for (let i = 0; i < Reviews.length; i++) {
      this.remarks.push(Reviews[i].id);
    }
  }
  saveFormData() {

    this.saveData = {
      order_id: parseInt(this.customerDetails.order_real_id),
      rating: this.ratingInNumber,
      remarks: this.remarks,
      customer_remark: this.comments === "Add a comment" ? '' : this.comments
    };
    this.feedbackservice.createFeedback(this.saveData).subscribe(
      _result => {
        this.Message = 2;
        console.log(_result);
      },
      _error => {
        alert(_error.error);
      });
  }
}

