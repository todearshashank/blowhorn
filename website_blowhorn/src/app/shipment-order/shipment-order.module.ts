import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShipmentOrderMainComponent } from './components/shipment-order-main/shipment-order-main.component';
import { ShipmentOrderListViewComponent } from './components/shipment-order-list-view/shipment-order-list-view.component';
import { ShipmentOrderListViewFilterComponent } from './components/shipment-order-list-view-filter/shipment-order-list-view-filter.component';
import { ShipmentOrderListViewGridTitleComponent } from './components/shipment-order-list-view-grid-title/shipment-order-list-view-grid-title.component';
import { ShipmentOrderListViewGridRowComponent } from './components/shipment-order-list-view-grid-row/shipment-order-list-view-grid-row.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BatchIdLookupComponent } from './components/batch-id-lookup/batch-id-lookup.component';
import { AgmCoreModule } from '@agm/core';
import { ApolloModule } from 'apollo-angular';
import { ShipmentOfflineComponent } from './components/shipment-offline/shipment-offline.component';
import { AgmDirectionModule } from 'agm-direction';
import { OfflineInitialComponent } from './components/shipment-offline/offline-initial/offline-initial.component';
import { FleetRouteOptimiserComponent } from './components/fleet-route-optimiser/fleet-route-optimiser.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule, MatAutocomplete } from '@angular/material/autocomplete';
import { ConstraintFormDialog } from './components/fleet-route-optimiser/constraint-form-dialog.component';
import { AssignDriverComponent } from './components/assign-driver/assign-driver.component';
import { CodDetailsComponent } from './components/cod-details/cod-details.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    BsDatepickerModule,
    FormsModule,
    AgmCoreModule,
    ApolloModule,
    ReactiveFormsModule,
    AgmDirectionModule,
    MatExpansionModule,
    MatListModule,
    MatTableModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule
  ],
  declarations: [
    ShipmentOrderMainComponent,
    ShipmentOrderListViewComponent,
    ShipmentOrderListViewFilterComponent,
    ShipmentOrderListViewGridTitleComponent,
    ShipmentOrderListViewGridRowComponent,
    BatchIdLookupComponent,
    ShipmentOfflineComponent,
    OfflineInitialComponent,
    FleetRouteOptimiserComponent,
    ConstraintFormDialog,
    AssignDriverComponent,
    CodDetailsComponent,
  ],
  entryComponents: [
    ConstraintFormDialog,
    FleetRouteOptimiserComponent,
  ]
})
export class ShipmentOrderModule { }
