import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BatchOrder, BatchOrdersGrid } from '../../../shared/models/batch-orders-grid.model';

@Component({
  selector: 'app-batch-id-lookup',
  templateUrl: './batch-id-lookup.component.html',
  styleUrls: ['./batch-id-lookup.component.scss']
})
export class BatchIdLookupComponent implements OnInit {

  @ViewChild('search_page', {static: false})
  search_input: ElementRef;
  public _batch_activated: boolean;
  private _date_activated: boolean;
  @Output()
  batch_id_changed: EventEmitter<number | string>;
  batch_id = '';
  show_search = false;
  date_range: [Date, Date] = [null, null];
  private _batches: BatchOrder[];
  private _error_msg: string;
  count: number;
  next: string;
  previous: string;
  page_size = 50;
  current_page = 1;
  show_page_search = false;
  url = 'api/fleet/shipments/order-batches?';
  base_url = 'api/fleet/shipments/order-batches?';
  @ViewChild('overlay_panel_batch_id', {static: false})
  overlay_panel: ElementRef;


  constructor(private _http_client: HttpClient) {
    this.batch_id_changed = new EventEmitter<number | string>();
  }

  ngOnInit() {
    this._batch_activated = false;
    this._date_activated = false;
    this._batches = [];
    this._error_msg = 'Search for results';
  }

  activate_date_clear(event): void {
    this._date_activated = true;
  }

  deactivate_date_clear(event): void {
    this._date_activated = false;
  }

  reset_date(event): void {
    this.date_range = [null, null];
    this.date_changed(event);
  }

  date_changed(event): void {
    this._batches = [];
    this._error_msg = 'Search for results';
    this.url = this.base_url;
    this.url += this.date_range[0] ? 'start_time=' + this.date_range[0].toISOString().substring(0, 10) : '';
    this.url += this.date_range[1] ? '&end_time=' + this.date_range[1].toISOString().substring(0, 10) : '';
  }

  activate_batch_clear(event): void {
    this._batch_activated = true;
  }

  focus_panel(event) {
    this.overlay_panel.nativeElement.focus();
  }

  deactivate_batch_clear(event): void {
    this._batch_activated = false;
  }

  reset_batch(event): void {
    this.batch_id = '';
    this.batch_changed(event);
  }

  batch_changed(event) {
    this.batch_id_changed.emit(this.batch_id);
  }

  open_search() {
    this.show_search = true;
  }

  supress_click(event) {
    event.stopPropagation();
    return false;
  }

  close_search(event) {
    if (event instanceof KeyboardEvent && event.keyCode !== 27) {
      return;
    }
    this.date_range = [null, null];
    this._batches = [];
    this.show_search = false;
  }

  fetch_batches() {
    this._http_client
    .get<BatchOrdersGrid>(this.url)
    .subscribe(batches => {
      this._batches = batches.results;
      this.count = batches.count;
      this.next = batches.next;
      this.previous = batches.previous;
      if (!this._batches.length) {
        this._error_msg = 'No records found';
      }
    });
  }

  select_batch(index: number) {
    this.batch_id = this._batches[index].id + '';
    this.batch_id_changed.emit(this.batch_id);
    this.close_search(null);
  }

  fetch_next() {
    this.url = this.next;
    this.current_page++;
    this.fetch_batches();
  }

  fetch_previous() {
    this.url = this.previous;
    this.current_page--;
    this.fetch_batches();
  }

  fetch_first() {
    this.url = this.get_page_url(1);
    this.current_page = 1;
    this.fetch_batches();
  }

  fetch_last() {
    this.url = this.get_page_url(this.last_page);
    this.current_page = this.last_page;
    this.fetch_batches();
  }

  fetch_current(event) {
    const value = event.target.value;
    const key = event.key;
    const code = event.code;
    const key_code = event.keyCode;
    if (key_code === 13 ) {
      if (parseInt(value, 10) >= 1 || parseInt(value, 10) <= this.last_page) {
        this.current_page = event.target.value;
        this.url = this.get_page_url(this.current_page);
        this.fetch_batches();
      }
      this.search_input.nativeElement.blur();
      this.show_page_search = false;
      return;
    }
    if ((key_code < 37 && key_code !== 8 && key_code !== 9) || (key_code > 40 && key_code < 48 && key_code !== 46) || key_code > 57 ||
      ((key_code >= 48 && key_code <= 57) && isNaN(value + key) || parseInt(value + key, 10) < 1 ||
      parseInt(value + key, 10) > this.last_page)) {
      event.preventDefault();
      return;
    }
  }

  get_page_url(page_no: number): string {
    const url = this.next || this.previous;
    let url_frags = url.split('&');
    url_frags = url_frags.map(item => {
      if (item.startsWith('page=')) {
        return `page=${page_no}`;
      }
      return item;
    });
    return url_frags.join('&');
  }

  show_current_page_search(event) {
    const el = this.search_input.nativeElement;
    this.show_page_search = true;
    el.focus();
  }

  hide_current_page_search(event) {
    const el = this.search_input.nativeElement;
    el.value = this.current_page;
    this.show_page_search = false;
  }

  get last_page(): number {
    return Math.ceil(this.count / this.page_size);
  }

}
