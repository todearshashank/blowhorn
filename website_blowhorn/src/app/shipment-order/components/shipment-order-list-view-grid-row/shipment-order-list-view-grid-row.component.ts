import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Shipment } from "../../../shared/models/shipments-grid.model";
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import { ShipmentAlertConfig } from "../../../shared/models/shipment-alert.model";
import { ShipmentService } from "../../../shared/services/shipment.service";
import { BlowhornService } from "../../../shared/services/blowhorn.service";
import { WatcherService } from '../../../shared/services/watcher.service';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-shipment-order-list-view-grid-row",
  templateUrl: "./shipment-order-list-view-grid-row.component.html",
  styleUrls: ["./shipment-order-list-view-grid-row.component.scss"]
})
export class ShipmentOrderListViewGridRowComponent implements OnInit {
  @Input()
  shipment: Shipment;
  @Input()
  status_colors = {};
  @Input()
  alert_config: ShipmentAlertConfig[] = [];
  @Input()
  status_legends: [] = [];
  @Output()
  pod_clicked: EventEmitter<string[]>;
  @Output()
  driver_clicked: EventEmitter<Shipment>;
  @Output()
  order_number_clicked: EventEmitter<Shipment>;
  @Output()
  barcode_clicked: EventEmitter<Shipment>;
  @Output()
  assign_driver_clicked: EventEmitter<Shipment>;
  @Output()
  selected: EventEmitter<boolean>;
  @Output()
  cod_clicked: EventEmitter<Shipment>;

  show_shipment_statuses = false;
  show_status_mapping_modal = false;
  show_feedback = false;
  rating: any;
  customerRemarks = [];
  comments: any;
  _error_msg = "Order not yet assigned";
  _status_history: {
    status: string;
    createdDate: Date;
    remarks: string;
  }[];
  btnStatusMap = {
    label: '',
    type: 'info',
    flat_btn: true,
    icon: {
      class: 'fa fa-info-circle'
    },
    icon_styles: {
      wrapper: {
        'padding-top': 0,
      },
      icon_holder: {
        'background-color': '#0088A8',
        'color': '#fff',
        'height': '22px',
        'width': '22px',
        'border-radius': '50%',
        'text-align': 'center',
        'line-height': '28px',
        'vertical-align': 'middle',
        'margin-right': '10px',
      },
      icon: {
        'font-size': '22px'
      }
    }
  };
  alert_level = {};
  moreActions = [
      {label: 'Shipping Manifest (Without SKUs)', val: 'shipping_manifest'},
      {label: 'Shipping Label (With SKUs)', val: 'shipping_label'},
    ];
  constructor(
    private _apollo: Apollo,
    private toastr: ToastrService,
    public _blowhorn_service: BlowhornService,
    private shipment_service: ShipmentService,
    public watcher_service: WatcherService,
  ) {
    this.pod_clicked = new EventEmitter<string[]>();
    this.driver_clicked = new EventEmitter<Shipment>();
    this.order_number_clicked = new EventEmitter<Shipment>();
    this.barcode_clicked = new EventEmitter<Shipment>();
    this.assign_driver_clicked = new EventEmitter<Shipment>();
    this.selected = new EventEmitter<boolean>();
    this.cod_clicked = new EventEmitter<Shipment>();
  }

  ngOnInit() {
    this.alert_level = this.set_alert_level();
    const shipmentData = JSON.parse(JSON.stringify(this.shipment));
    for (let i = 0; i < shipmentData.driver_rating.length; i++) {
      this.rating = Number(shipmentData.driver_rating[i].rating);
      this.customerRemarks.push(shipmentData.driver_rating[i].remark);
      this.comments = shipmentData.driver_rating[i].customer_remark;
    }
    this.setMoreActions();
  }

  setMoreActions() {
    let can_view_proof = this._blowhorn_service.hasPermission('shipment_orders', 'can_view_proofs');
    let can_download_challan = this._blowhorn_service.hasPermission('shipment_orders', 'can_download_challan');
    let can_track_order = this._blowhorn_service.hasPermission('shipment_orders', 'can_track_order');
    let can_view_timeline = this._blowhorn_service.hasPermission('shipment_orders', 'can_view_timeline');
    if (this.shipment.pod_files.length && can_view_proof && this._blowhorn_service.user_profile.show_pod_in_dashboard) {
      this.moreActions.push({label: 'Documents', val: 'docs'});
    }
    if (this.shipment.number) {
      if (can_track_order) {
        this.moreActions.push({label: 'Track', val: 'track'});
      }
      if (can_download_challan) {
        this.moreActions.push({label: 'Download Challan', val: 'challan'});
      }
    }
  }

  moreActionSelected(val: string): void {
    switch (val) {
      case 'docs':
        this.pod_clicked.emit(this.shipment.pod_files);
        break;

      case 'track':
        let url = this.shipment.status === 'Out-For-Delivery' ? 'track' : 'track-parcel'
        window.open(`/${url}/${this.shipment.number}`);
        break;

      case 'challan':
        window.open(`/api/order/${this.shipment.number}/challan`, 'blank');
        break;

      case 'shipment':
        window.open(`/track-parcel/${this.shipment.number}`);
        break;

      case 'shipping_label':
        this.downloadShippingLabel(true)
        break;

      case 'shipping_manifest':
        this.downloadShippingLabel(false)
        break;

      default:
        console.log('Invalid action');

    }
  }

  downloadShippingLabel(isWithSku: boolean) {
    this.shipment_service.downloadShippingLabel(`ids[]=${this.shipment.id}&is_with_sku=${isWithSku}`)
        .subscribe((resp) => {
            window.open(resp.url, '_blank');
        }, err => {
            console.error('Error', err)
            this.toastr.error('Failed to generate file');
        })
  }

  selectOrder(event: any) {
    this.selected.emit(event['checked'])
  }

  openPod(val: string) {
    window.open(val, '_blank');
  }

  open_assets() {
    this.driver_clicked.emit(this.shipment);
  }

  open_details() {
    this.order_number_clicked.emit(this.shipment);
  }

  open_barcode() {
    this.barcode_clicked.emit(this.shipment);
  }

  show_status_mapping() {
    this.show_status_mapping_modal = true;
  }

  show_status_history(shipment: Shipment) {
    this._blowhorn_service.show_loader();
    this._apollo
      .watchQuery<any>({
        query: gql`
        {
          shipmentOrderStatuses(order_Number: "${shipment.number}") {
            edges{
              node {
                status
                createdDate
                remarks
              }
            }
          }
        }
        `,
        fetchPolicy: "network-only"
      }).valueChanges.subscribe(result => {
        this._status_history = result.data.shipmentOrderStatuses.edges;
        this.show_shipment_statuses = true;
        this._blowhorn_service.hide_loader();
      });
  }

  show_remarks(shipment: Shipment) {
    this._blowhorn_service.show_loader();
    this.show_feedback = true;
    this._blowhorn_service.hide_loader();
  }

  close_remarks(event: any) {
    this.show_feedback = false;
  }

  close_search(event: any) {
    if (event instanceof KeyboardEvent && event.keyCode !== 27) {
      return;
    }
    this.show_shipment_statuses = false;
  }

  close_status_mapping(event: any) {
    if (event instanceof KeyboardEvent && event.keyCode !== 27) {
      return;
    }
    this.show_status_mapping_modal = false;
  }

  suppress_click(event: any) {
    event.stopPropagation();
    return false;
  }

  set_color(status: string) {
    return {
      "background-color": this.status_colors[status] || "#006F89",
      "border-radius": "4px",
      "font-weight": 500,
      "font-size": "12px",
      padding: "2px 12px",
      color: "#fff"
    };
  }

  set_alert_level() {
    let color = "#fff";
    if (this.shipment.picked_time) {
      color = this.shipment_service.get_alert_level(
        this.alert_config,
        this.shipment.picked_time,
        this.shipment.delivered_time || new Date().toISOString()
      );
    }
    return {
      "background-image": `linear-gradient(to bottom right, ${color}, #fff)`
    };
  }

  assign_driver() {
    this.assign_driver_clicked.emit(this.shipment);
  }

  open_cod_details() {
    this.cod_clicked.emit(this.shipment);
  }
}
