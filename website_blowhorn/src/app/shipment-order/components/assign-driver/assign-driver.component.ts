import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { IdName } from './../../../shared/models/id-name.model';
import { ShipmentService } from './../../../shared/services/shipment.service';
import { Shipment } from './../../../shared/models/shipments-grid.model';

@Component({
  selector: 'app-assign-driver',
  templateUrl: './assign-driver.component.html',
  styleUrls: ['./assign-driver.component.scss'],
  animations: [
    trigger('shrinkOut', [
      state('in', style({height: '*'})),
      transition('* => void', [
        style({height: '*'}),
        animate(250, style({height: 0}))
      ])
    ])
  ]
})
export class AssignDriverComponent implements OnInit {

  @Input() shipment: Shipment;
  @Output() close_fired: EventEmitter<boolean>;
  @Output() trip_created: EventEmitter<any>;

  heading: string = '';
  availableDrivers: IdName[] = [];
  availableDriverStore: any[] = []
  loading = false;
  message = {
    status: null,
    text: ''
  };
  selected_driver_name: string = '';
  selected_driver_id: number = null;
  submitBtn = {
    label: 'Submit',
    type: 'save',
  }
  dismissBtn = {
    label: this.message.status !== 200 ? 'Cancel' : 'Close',
    type: 'delete',
  }
  constructor(
    private shipmentService: ShipmentService,
  ) {
    this.close_fired = new EventEmitter<boolean>();
    this.trip_created = new EventEmitter<any>();
  }

  ngOnInit() {
    this.heading = `Assign Driver - ${this.shipment.number}`;
    this.loading = true;
    this.shipmentService.get_drivers(this.shipment.number).subscribe((data: any) => {
        this.availableDriverStore = data;
        this.availableDrivers = data.map((item: any) =>  {
            let idName = new IdName();
            idName.id = item.autodispatch_drivers__pk;
            idName.name = `${item.autodispatch_drivers__name} | ${item.autodispatch_drivers__driver_vehicle}`;
            return idName;
        });
        this.loading = false;
    }, (err: any) => {
        this.loading = false;
        this.message.status = err.status;
        this.message.text = typeof (err.error) === 'string' ? err.error : err.error.detail;
    });
  }

  driver_changed(event: IdName) {
    this.selected_driver_id = event.id;
    this.selected_driver_name = event.name;
  }

  on_submit() {
    this.loading = true;
    this.shipmentService.assign_driver({
        order_number: this.shipment.number,
        driver_pk: this.selected_driver_id
    }).subscribe((data: any) => {
        this.loading = false;
        this.message.status = 200;
        this.message.text = `Driver has been assigned to trip ${data.trip_number}`;
        this.trip_created.emit(data.order);
    }, (err: any) => {
        this.loading = false;
        this.message.status = err.status;
        this.message.text = typeof (err.error) === 'string' ? err.error : err.error.detail;
    });
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
    this.close_fired.emit(true);
  }

  get dropdown_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        border: 'none',
        height: '34px',
        width: '100%',
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        width: '100%',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

}
