import { Component, OnInit } from '@angular/core';
import { Driver } from '../../../shared/models/driver.model';
import { City } from '../../../shared/models/city.model';
import { IdName } from '../../../shared/models/id-name.model';
import { ActivatedRoute } from '@angular/router';
import { WatcherService } from '../../../shared/services/watcher.service';

@Component({
  selector: 'app-shipment-order-main',
  templateUrl: './shipment-order-main.component.html',
  styleUrls: ['./shipment-order-main.component.scss']
})
export class ShipmentOrderMainComponent implements OnInit {

  city_store: City[] = [];
  hub_store: IdName[] = [];
  status_store: string[] = [];
  status_legend: string[] = [];

  constructor(
    private route: ActivatedRoute,
    private watcher_service: WatcherService,
  ) {
    route.data
      .subscribe( data => {
        this.city_store = data.shipment_filter.cities;
        this.status_store = data.shipment_filter.statuses;
        this.status_legend = data.shipment_filter.status_legend;
        for (const city of data.shipment_filter.cities) {
          for (const hub of city.hubs) {
            this.hub_store.push(hub);
          }
        }
        this.watcher_service
          .update_city_store(this.city_store);
        this.watcher_service
          .update_hub_store(this.hub_store);
        this.watcher_service
          .update_status_store(this.status_store);
        this.watcher_service
          .update_status_legend_store(this.status_legend);
        this.watcher_service
            .update_status_color_store(data.shipment_filter.status_colors);
        this.watcher_service
            .update_alert_config(data.shipment_filter.alert_config);
      },
      err => {
        console.error(err);
      });
  }

  ngOnInit() {
  }

}
