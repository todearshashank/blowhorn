import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef, NgZone, Input } from '@angular/core';
import { ShipmentService } from '../../../shared/services/shipment.service';
import { Router } from '@angular/router';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { MapService } from '../../../shared/services/map.service';
import * as moment from 'moment';

@Component({
  selector: 'app-shipment-offline',
  templateUrl: './shipment-offline.component.html',
  styleUrls: ['./shipment-offline.component.scss']
})
export class ShipmentOfflineComponent implements OnInit {

  @Input() reference_number: string ;
  @ViewChild('search', {static: false}) public searchElementRef: ElementRef;
  @Output() messageEvent = new EventEmitter<boolean>();
  // reference_number = '';
  customer_name = '';
  customer_mobile = '91';
  customer_email = '';
  hub_address = '';
  delivery_address = '';
  delivery_postal_code = '';
  delivery_lat = '';
  delivery_lon = '';
  pickup_address = '';
  pickup_postal_code = '';
  pickup_lat = '';
  pickup_lon = '';
  expected_delivery_time: any;
  is_cod = false;
  cash_on_delivery = '0';
  land_mark: string;
  item_details: any;
  saveData: any;
  address: string;
  masterOrderLines: any[] = [];
  displayOrderLines: any[] = [];
  removedOrderLines: any [] = [];
  data: string;
  equality: any;
  nameValid: boolean;
  mobileValid: boolean;
  mobileDigitValid: boolean;
  emailValid: boolean;
  addressValid: boolean;
  disable = false;
  billValid: boolean;
  hubValid: boolean;
  pinValid: boolean;
  remarks: any;
  itemDisable: boolean = false;
  removedItems = [];
  dist: number;
  loading: boolean;
  d = new Date();
  currentDate = moment().format("DD");
  month = this.d. getMonth() + 1;
  year = this.d. getFullYear();
  fullDate = this.year.toString() + this.month.toString() + this.currentDate;
  timeFrame:any;
  deliveryTime: any;
  @Output() details_closed: EventEmitter<boolean>;
  billNumber: any;
  results: any;
  number: any;
  errorMessage: any;
  date: any = new Date();
  currentHour = this.date.getHours();
  todayDeliveryDate: any = this.date.toISOString();
  tomorrowDeliveryDate: any = new Date(this.date.setDate(this.date.getDate() + 1)).toISOString();
  res: any;
  hubItem: any;
  direction:any;
  zoom = 11;
  dlat = 12.930016787581579;
  dlon = 77.60087100467683;
  latitude =  12.9716;
  longitude = 77.5946;
  selectedMarker: any;
  mapType = 'satellite';
  private geoCoder;
  message: boolean = true;

  labelOptions = {
    color: '#CC0000',
    }

  constructor(
      public blowhorn_service: BlowhornService,
      private shipment: ShipmentService,
      private map_service: MapService,
      private router: Router,
      private mapsAPILoader: MapsAPILoader,
      private ngZone: NgZone
      ) {
      this.details_closed = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.reference_number += this.fullDate;
    this.number = this.reference_number; //Number(this.reference_number);
    this.getHub();
    this.setCurrentLocation();
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address'],
        componentRestrictions: { country: "in" }
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.delivery_postal_code = this.map_service.getPostalCodeFromAddressComponents(place.address_components);
          this.zoom = 12;
        });
      });
    });
  }

disableAddButton(item: any) {
  let obj = this.masterOrderLines.find(i => i.item_code === item.item_code);
  item.disable_add = item.item_quantity >= obj.item_quantity;
}

calculateTotalPrice(item: any) {
  item.total_item_price = item.item_quantity * item.item_price_per_each;
}

addQuantity(item: any) {
  if (item.disable_add) {
    return;
  }
  item.item_quantity++;
  this.calculateTotalPrice(item);
  this.disableAddButton(item);
}

subQuantity(item: any) {
  if (item.item_quantity <= 0) {
    return;
  }
  item.item_quantity--;
  this.calculateTotalPrice(item);
  this.disableAddButton(item);
}

removeItem(item: any, i: number) {
  this.displayOrderLines.splice(i, 1)
  this.removedOrderLines.push(item);
  console.log(this.removedOrderLines);
}

addBackItem(item: any, i: number) {
  this.removedOrderLines.splice(i, 1)
  this.displayOrderLines.push(item);
}

initOrderlines() {
  this.displayOrderLines = JSON.parse(JSON.stringify(this.masterOrderLines));
  this.displayOrderLines.forEach(item => this.disableAddButton(item));
}

restoreInitialOrderlines() {
  this.initOrderlines();
  this.removedOrderLines = [];
}

loadHub() {
    this.hubItem = this.res.find(item => item.id === Number(this.hub_address));
    this.pickup_address = this.hubItem.address.line1,
    this.pickup_postal_code = this.hubItem.address.postcode,
    this.pickup_lon =  this.hubItem.address.geopoint.coordinates[0],
    this.pickup_lat = this.hubItem.address.geopoint.coordinates[1],
    this.dlat = parseFloat(this.hubItem.address.geopoint.coordinates[1]),
    this.dlon =  parseFloat(this.hubItem.address.geopoint.coordinates[0]);
  }

  getData() {
    this.loading = true;
    this.shipment
      .get_orderdetails(this.number).subscribe(data => {
        this.results = data;
        console.log(this.results);
        this.masterOrderLines = JSON.parse(JSON.stringify(this.results.data));
        this.initOrderlines();
        console.log(this.masterOrderLines);
      },
        err => {
          alert('Unable to fetch the record from POS, Please scan again by clearing the field')
          this.errorMessage = false;
        });

  }

  landmark() {
    this.address = this.address.concat(',' + this.land_mark)
  }

  getHub() {
    this.shipment
      .get_hubdetails().subscribe(data => {
        this.res = data;
      });
  }
  onChange(value){
    this.timeFrame = value;
  }

saveFormData() {
  this.saveData = {
    'customer_name' : this.customer_name,
    'customer_mobile': this.customer_mobile,
    'customer_email': this.customer_email,
    'delivery_address': this.address,
    'delivery_postal_code': this.delivery_postal_code,
    'reference_number': this.reference_number,
    'delivery_lat': this.latitude.toString(),
    'delivery_lon': this.longitude.toString(),
    'pickup_address': this.hubItem.address.line1,
    'pickup_postal_code': this.pickup_postal_code,
    'pickup_lat': this.pickup_lat.toString(),
    'pickup_lon': this.pickup_lon.toString(),
    'order_creation_time': null,
    'expected_delivery_time': this.timeFrame == 'today' ? this.todayDeliveryDate : this.tomorrowDeliveryDate,
    'is_cod': false,
    'cash_on_delivery': '0',
    'is_offline_order': true,
    'remarks': this.remarks,
    'item_details': this.displayOrderLines
  };
  this.Validation();
  if (!this.results.order_number) {
    this.shipment.createHomeDelivery(this.saveData).subscribe(
      _result => {
        alert(`Total number of items to be delivered are ${this.displayOrderLines.length}.`);
        this.messageEvent.emit(this.message);
        this.details_closed.emit(true);
      },
      _error => {
        alert(_error.Message);
      }
    );
  } else {
    delete this.saveData.cash_on_delivery;
    this.shipment.updateHomeDelivery(this.results.order_number, this.saveData).subscribe(
      _result => {
        alert(`Total Number of items to be delivered are ${this.displayOrderLines.length}.`);
        this.messageEvent.emit(this.message);
        this.details_closed.emit(true);
      },
      _error => {
        alert(_error.Message);
      }
    );
  }
}

validPhone(){
  var phone = document.forms["myForm"]["Mobile"];
  phone.value = phone.value.replace(/[^0-9]/g, '');

  if(phone.value.length != 12) {
    this.mobileDigitValid = true;
    phone.focus();
  } else {
    this.mobileDigitValid = false;
}
}

  Validation() {
    var hub = document.forms["myForm"]["HubName"];
    var billnumber = document.forms["myForm"]["ReferenceNumber"];
    var name = document.forms["myForm"]["Name"];
    var phone = document.forms["myForm"]["Mobile"];
    var address = document.forms["myForm"]["first"];
    var pin = document.forms["myForm"]["PinCode"];

    if (hub.value == "") {
      this.hubValid = true;
      name.focus();
      return false;
    }
    if (billnumber.value == "") {
      this.billValid = true;
      name.focus();
      return false;
    }
    if (name.value == "") {
      this.nameValid = true;
      name.focus();
      return false;
    }
    if (phone.value == "") {
      this.mobileValid = true;
      phone.focus();
      return false;
    }
    if (address.value == "") {
      this.addressValid = true;
      address.focus();
      return false;
    }
    if (pin.value == "") {
      this.pinValid = true;
      phone.focus();
      return false;
    }
    return true;
}

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

markerDragEnd($event: MouseEvent) {
  this.latitude = $event.coords.lat;
  this.longitude = $event.coords.lng;
  this.getAddress(this.latitude, this.longitude);
  this.getDirection();
}

  public getDirection() {
    this.direction = {
      origin: { lat: this.dlat, lng: this.dlon },
      destination: { lat: this.latitude, lng: this.longitude }
    }

    this.distance(this.dlat, this.dlon, this.latitude, this.longitude)
  }

 distance(lat1, lon1, lat2, lon2) {
  var radlat1 = Math.PI * lat1/180;
  var radlat2 = Math.PI * lat2/180
  var theta = lon1-lon2
  var radtheta = Math.PI * theta/180
  var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = dist * 180/Math.PI;
  dist = dist * 60 * 1.1515
  dist = dist * 1.609344;
  this.dist = Math.round( dist * 10) / 10;
}

  getAddress(latitude: number, longitude: number) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results.length) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          this.delivery_postal_code = this.map_service.getPostalCodeFromAddressComponents(results[0].address_components);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
