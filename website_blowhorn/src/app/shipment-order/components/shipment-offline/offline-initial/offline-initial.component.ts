import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import { ShipmentService } from '../../../../shared/services/shipment.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-offline-initial',
    templateUrl: './offline-initial.component.html',
    styleUrls: ['./offline-initial.component.scss']
 })
 
 export class OfflineInitialComponent implements OnInit{

  @Output() details_closed: EventEmitter<boolean>;
  reference_number = '';
  saveData: any;
  show_details = false;
  myData: any;
  errorData: any;
  message:boolean= false;
  isReferenceNumberValid: boolean;

  constructor(
    private shipment: ShipmentService,
    private router: Router
  ) {
    this.details_closed = new EventEmitter<boolean>();
  }

  receiveMessage($event) {
    this.message = $event;
    this.details_closed.emit(true);
  }
  validReference() {
    var referenceNumber = document.forms["initForm"]["ReferenceNumber"];
    referenceNumber.value = referenceNumber.value.replace(/[^0-9]/g, '');

    if (referenceNumber.value.length === 12) {
      this.isReferenceNumberValid = true;
    } else {
      this.isReferenceNumberValid = false;
      referenceNumber.focus();
    }
  }

  ngOnInit() { }
  saveFormData() {
    this.saveData = {
      'reference_number': this.reference_number
    }
    this.shipment.post_order(this.saveData).subscribe(
      result => {
        alert(result);
        this.myData = result.data;
      },
      _error => {
        alert(_error.error);
      }
    );
  }
  public show_edit() {
    this.show_details = !this.show_details;
  }
}