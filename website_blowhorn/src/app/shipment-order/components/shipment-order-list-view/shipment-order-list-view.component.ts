import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShipmentService } from '../../../shared/services/shipment.service';
import { WatcherService } from '../../../shared/services/watcher.service';
import { Driver, Shipment } from '../../../shared/models/shipments-grid.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { BlowhornPaginationComponent } from '../../../shared/components/blowhorn-pagination/blowhorn-pagination.component';
import { ShipmentAlertConfig } from '../../../shared/models/shipment-alert.model';
import { TripsService } from '../../../shared/services/trips.service';
import { Constants } from '../../../shared/utils/constants';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AdditionalEntityDetails } from '../../../shared/models/assets.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ConstraintFormDialog } from '../fleet-route-optimiser/constraint-form-dialog.component'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FleetRouteOptimiserComponent } from '../fleet-route-optimiser/fleet-route-optimiser.component'
import { ToastrService } from 'ngx-toastr';

declare var customer_id: number;

@Component({
  selector: 'app-shipment-order-list-view',
  templateUrl: './shipment-order-list-view.component.html',
  styleUrls: ['./shipment-order-list-view.component.scss']
})
export class ShipmentOrderListViewComponent implements OnInit {

  @ViewChild('pagination', {static: false})
  pagination: BlowhornPaginationComponent;
  @ViewChild('overlay_panel_image', {static: false})
  overlay_panel: ElementRef;
  @ViewChild('overlay_panel_assets', {static: false})
  panel_assets: ElementRef;
  @ViewChild('overlay_panel_details', {static: false})
  panel_details: ElementRef;
  @ViewChild('overlay_panel_barcode', {static: false})
  panel_barcode: ElementRef;
  @ViewChild('overlay_panel_assign_driver', {static: false})
  panel_assign_driver: ElementRef;
  @ViewChild('overlay_panel_cod', {static: false})
  overlay_panel_cod: ElementRef;
  shipments: Shipment[];
  url = Constants.urls.API_SHIPMENTS;
  export_url = Constants.urls.API_EXPORT_SHIPMENTS;
  filter_url = '';
  error_msg = 'No records found';
  show_image_viewer = false;
  show_details = false;
  show_assign_driver = false;
  document_links: {}[] = [];
  count: number;
  next: string;
  previous: string;
  shipment: Shipment;
  show = {
    assets: false,
    details: false,
    barcode: false,
    assign_driver: false,
    cod: false,
  };
  models = {
    document: 'document',
    assets: 'assets',
    barcode: 'barcode',
    details: 'details',
    assign_driver: 'assign_driver',
    cod: 'cod',
  };
  order_lines = [];
  status_colors = {};
  data_loaded_at: Date = null;
  alert_config: ShipmentAlertConfig[] = [];
  firebaseObject: AngularFireObject<any>;
  status_legends_master_data = {};
  allOrdersSelected = false;
  constructor(
    private route: ActivatedRoute,
    private watcher_service: WatcherService,
    private shipment_service: ShipmentService,
    public _blowhorn_service: BlowhornService,
    private trip_service: TripsService,
    private firebase_db: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    public _http_client: HttpClient,
    public dialog: MatDialog,
    private toastr: ToastrService,
  ) {
    route.data
      .subscribe(data => {
        if (data.shipment_orders) {
          const shipment_orders_data = data.shipment_orders;
          this.update_shipments(shipment_orders_data);
          this._blowhorn_service.hide_loader();
          this.error_msg = 'No records found';
        }
      },
        err => {
          console.error(err);
          this._blowhorn_service.hide_loader();
          this.error_msg = 'Something went wrong.';
        });

    this.watcher_service
      .order_status_colors
      .subscribe(colors => {
        this.status_colors = colors;
      });

    this.watcher_service
      .alert_config
      .subscribe(config => {
        this.alert_config = config;
      });
      this.watcher_service
      .status_legend_store
      .subscribe(val => {
        this.status_legends_master_data = val;
    });

    this.shipment_service.selectAll
      .subscribe(resp => {
        this.shipments.forEach(item => item.checked = resp)
      });
    this.initialize_live_updates();
  }

  static getCookie(name: string) {
    if (!document.cookie) {
        return null;
    }

    const xsrfCookies = document.cookie.split(';')
        .map(c => c.trim())
        .filter(c => c.startsWith(name + '='));

    if (xsrfCookies.length === 0) {
        return null;
    }

    return decodeURIComponent(xsrfCookies[0].split('=')[1]);
  }

  static get http_headers(): {} {
    const token = this.getCookie('csrftoken');
    return {
        headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Access-Control-Allow-Origin': '*',
            'X-CSRFToken': token
        })
    };
  }

  ngOnInit() {
  }

  filter_changed(event: { url: string }) {
    this.filter_url = event.url;
    this.shipments = [];
    this.error_msg = 'Filter changed. Search for results.';
    this.update_shipments({
      count: 0,
      next: '',
      previous: '',
      results: []
    });
    this.url = Constants.urls.API_SHIPMENTS;
    this.export_url = `${Constants.urls.API_EXPORT_SHIPMENTS}${this.filter_url}`;
  }

  update_shipments(data: { count: number, next: string, previous: string, results: any[] }) {
    this.count = data.count;
    this.next = data.next;
    this.previous = data.previous;
    this.shipments = data.results;
    this.data_loaded_at = new Date();
  }

  search() {
    this._blowhorn_service.show_loader();
    this.url += this.filter_url ? this.filter_url : '';
    this.filter_url = '';
    this.shipment_service
      .get_shipment_orders(this.url)
      .subscribe(data => {
        this.update_shipments(data);
        this._blowhorn_service.hide_loader();
        this.error_msg = 'No records found. Change filters to get data.';
    }, err => {
          console.error(err);
          this._blowhorn_service.hide_loader();
          this.error_msg = 'Something went wrong.';
    });
  }

  export(event: any) {
    window.open(`${this.export_url}&export_type=${event}`);
  }

  get_routes(event: any){
    let orderNumbers = this.shipments.map(x => x.number)
    if (orderNumbers.length < 2) {
      this.toastr.error("The Route Optimiser must be used for atleast two orders! Make Sure to filter the orders by Hubs & Status!");
      return
    }
    this.open_form_dialog(orderNumbers);
  }

  open_form_dialog(orders_data: any) {
    const formDialogRef = this.dialog.open(ConstraintFormDialog, {
      height: '60%',
      width: '40%',
      data: {
        order_numbers: orders_data,
      }
    });

    formDialogRef.afterClosed()
    .subscribe(
      result => {
        if (result) {
          let json_data = result
          console.log(json_data);
          this.call_routes_api(json_data);
        }
      }
    )
  }

  call_routes_api(json_data: any) {
    this._blowhorn_service.show_loader();
    this._http_client.post(Constants.urls.API_FLEET_ROUTES_OPT, json_data, ShipmentOrderListViewComponent.http_headers)
    .toPromise()
    .then(res => {
        let routes_result = res;
        this._blowhorn_service.hide_loader();
        this.open_show_routes_dialog(routes_result);
      }, err => {
        this._blowhorn_service.hide_loader();
        if (err.error.type) {
            this.toastr.error(err.error.type+": "+err.error.message);
        } else {
            this.toastr.error("Unexpected Error! Please try again later!")
        }
      }
    );
  }

  open_show_routes_dialog(route_output: any) {
    console.log(route_output)
    const dialogRoutesDisplay = this.dialog.open(FleetRouteOptimiserComponent, {
      height: '80%',
      width: '95%',
      data: {
        api_response: route_output
      }
    });
    dialogRoutesDisplay.afterClosed().subscribe(result => {})
  }

  show_documents(document_links: any[]) {
    this.document_links = document_links.map(item => {
      return {
        url: item.file,
        caption: item.event ? `Status: ${item.event.status} ${item.event.remarks ? " | Remarks: " + item.event.remarks : ''}` : ''
      };
    });
    this.show_image_viewer = true;
  }

  open_assets(event: any) {
    this.show.assets = true;
    this.shipment = event;
  }

  open_barcode(event: any) {
    this.show.barcode = true;
    this.shipment = event;
  }

  open_cod(event: any) {
    this.show.cod = true;
    this.shipment = event;
  }

  open_details(event: Shipment) {
    this.show.details = true;
    this.shipment = event;
    this.shipment_service.update_selected_entity_details(new AdditionalEntityDetails(
      event.status,
      event.payment_mode,
      event.amount_paid_online,
      event.cash_on_delivery ? parseFloat(event.cash_on_delivery) : 0.00,
      event.cash_collected || 0.00,
      event.orderlines,
      event.otp ? event.otp.toString() : '--NA--',
      event.number,
      event.is_valid_lineedit_status
    ));
  }

  open_assign_driver(event: Shipment) {
    this.show.assign_driver = true;
    this.shipment = event;
  }

  close_fired(event: any, modal: string) {
    if (event instanceof KeyboardEvent
      && event.keyCode !== 27) {
      return;
    }
    switch (modal) {
      case this.models.document:
        this.document_links = [];
        this.show_image_viewer = false;
        break;

      case this.models.assets:
        this.show.assets = false;
        break;

      case this.models.details:
        this.show.details = false;
        break;

      case this.models.barcode:
        this.show.barcode = false;
        break;

      case this.models.assign_driver:
        this.show.assign_driver = false;
        break;

      case this.models.cod:
        this.show.cod = false;
        break;

      default:
        console.log('Modal name is missing');
    }
  }

  focus_panel(event: any, modal: string) {
    switch (modal) {
      case this.models.document:
        this.overlay_panel.nativeElement.focus();
        break;

      case this.models.assets:
        this.panel_assets.nativeElement.focus();
        break;

      case this.models.details:
        this.panel_details.nativeElement.focus();
        break;

      case this.models.barcode:
        this.panel_barcode.nativeElement.focus();
        break;

      case this.models.assign_driver:
        this.panel_assign_driver.nativeElement.focus();
        break;

      case this.models.cod:
          this.overlay_panel_cod.nativeElement.focus();
          break;

      default:
        console.log('Modal name is missing');
    }
  }

  initialize_live_updates(): void {
    this.trip_service.get_firebase_token().subscribe((resp: string) => {
        console.log('Retrieved token')
    });
    const url = `dashboard/${customer_id}/orders/shipment`;
    this.firebase_db
      .object(url)
      .valueChanges().subscribe(snapshot => {
        if (this.url.indexOf('order_ref_nos') !== -1 || this.url.indexOf('batch_id') !== -1) {
            console.error('Order search applied. Not proceeding with firebase updates', this.url)
            return;
        }
        let modified_data: any = snapshot;
        if (modified_data != null) {
          modified_data = typeof modified_data === 'string' ? JSON.parse(modified_data) : modified_data[customer_id];
          if (!modified_data['broadcast_time'] || new Date(modified_data['broadcast_time']) < this.data_loaded_at) {
            // Do not allow to proceed if broadcast time is past time
            return;
          }
          this.update_order(modified_data);
        }
      });
  }

  get_index(data: any): any {
    return this.shipments.findIndex(item => item.number === data.number);
  }

  update_order(data: any): void {
    let index = this.get_index(data);
    if (index === -1) {
      this.shipments.splice(0, 0, data);
    } else {
      this.shipments.splice(index, 1, data);
      if (this.shipment && this.shipment.number === data.number) {
        this.shipment_service.update_selected_entity_details(
          new AdditionalEntityDetails(
            data.status,
            data.payment_mode,
            data.amount_paid_online,
            data.cash_on_delivery ? parseFloat(data.cash_on_delivery) : 0.00,
            data.cash_collected || 0.00,
            data.orderlines,
            data.otp ? data.otp.toString() : '--NA--'
          )
        );
      }
    }
  }

  trip_created(event: any): void {
    let index = this.get_index(event);
    this.shipments.splice(index, 1, event);
  }

  order_selected(event: any, shipment: Shipment) {
    shipment.checked = event
    this.allOrdersSelected = this.shipments.every(item => item.checked)
  }

  print_label(event: any) {
    let isWithSku = event === 'shipping_label'
    let orderIds = this.shipments.filter(item => item.checked).map(item => item.id)
    if (!orderIds.length) {
        this.toastr.error('Select one or more orders.')
        return;
    }
    if (orderIds.length > 20) {
        this.toastr.error('Maximum of 20 orders are allowed.')
        return;
    }
    this.toastr.success('Generating the file. It will be downloaded automatically.')
    this.shipment_service.downloadShippingLabel(`ids[]=${orderIds}&is_with_sku=${isWithSku}`)
        .subscribe((resp) => {
            window.open(resp.url, '_blank');
        }, err => {
            console.error('Error', err)
            this.toastr.error('Failed to generate file');
        })
  }
}
