import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { BlowhornCheckboxComponent } from '../../../shared/components/blowhorn-checkbox/blowhorn-checkbox.component';
import { ShipmentService } from '../../../shared/services/shipment.service';

@Component({
  selector: 'app-shipment-order-list-view-grid-title',
  templateUrl: './shipment-order-list-view-grid-title.component.html',
  styleUrls: ['./shipment-order-list-view-grid-title.component.scss']
})
export class ShipmentOrderListViewGridTitleComponent implements OnInit {

    @ViewChild('select_all', {static: false})
    select_all: BlowhornCheckboxComponent;

    @Input() checked: boolean = false;

    selected = false

    constructor(
        private shipment_service: ShipmentService,
    ) {
    }

    ngOnInit() {
    }

    selectAll(event: any) {
        this.shipment_service.updateSelectAll(event['checked'])
    }

}
