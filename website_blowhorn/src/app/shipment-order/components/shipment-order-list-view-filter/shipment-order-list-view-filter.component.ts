import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { City } from '../../../shared/models/city.model';
import { IdName } from '../../../shared/models/id-name.model';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { IntegrationService } from '../../../shared/services/integration.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shipment-order-list-view-filter',
  templateUrl: './shipment-order-list-view-filter.component.html',
  styleUrls: ['./shipment-order-list-view-filter.component.scss']
})
export class ShipmentOrderListViewFilterComponent implements OnInit {

  show_details = false;

  @Output()
  filter_changed: EventEmitter<{url: string}>;
  @Output()
  search_fired: EventEmitter<void>;
  @Output()
  export_fired: EventEmitter<string>;
  @Output()
  print_fired: EventEmitter<string>;
  @Output()
  routes_fired: EventEmitter<string>;
  previous: string;
  date_range: [Date, Date] = [null, null];
  order_number = '';
  batch_id = '';
  status = '';
  FILTER_TODAY = 0;
  FILTER_LAST_7_DAYS = 7;
  FILTER_LAST_30_DAYS = 30;
  quickFilters = [
    {label: 'Today', val: this.FILTER_TODAY},
    {label: 'Last 7 Days', val: this.FILTER_LAST_7_DAYS},
    {label: 'Last 30 Days', val: this.FILTER_LAST_30_DAYS},
  ];
  export_options = [
    {id: 1, name: 'Minimal', value: 'minimal'},
    {id: 2, name: 'With Order Events', value: 'with_events'},
    {id: 3, name: 'Orderline Details', value: 'orderline'},
    {id: 4, name: 'Proof of Delivery', value: 'pod_docs'},
  ];
  print_options = [
    {id: 2, name: 'Shipping Manifest (Without SKUs)', value: 'shipping_manifest'},
    {id: 1, name: 'Shipping Label (With SKUs)', value: 'shipping_label'},
  ]
  cities: City[] = [];
  delivery_hubs: IdName[] = [];
  pickup_hubs: IdName[] = [];
  city_store: City[] = [];
  delivery_hub_store: IdName[] = [];
  pickup_hub_store: IdName[] = [];
  status_store: string[] = [];
  statuses: string[] = [];
  selected_city_id: number = null;
  selected_city_name = 'All Cities';
  selected_delivery_hub_id: number = null;
  selected_pickup_hub_id: number = null;
  selected_delivery_hub_name = 'All Delivery Hubs';
  selected_pickup_hub_name = 'All Pickup Hubs';
  selected_status = 'All Statuses';
  selected_status_id: number|string = null;
  selected_export_name = 'Export';
  selected_export_id = null;
  selected_shipment_label_name = 'Print';
  selected_shipment_label_id = null;
  selected_quick_filter = this.FILTER_TODAY;
  city_disabled = false;
  pickup_hub_disabled = false;
  delivery_hub_disabled = false;
  date_activated = false;
  order_activated = false;
  batch_activated = false;

  constructor(
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    public integrationService : IntegrationService,
    private toastr: ToastrService,
  ) {
    this.filter_changed = new EventEmitter<{url: string}>();
    this.search_fired = new EventEmitter<void>();
    this.export_fired = new EventEmitter<string>();
    this.print_fired = new EventEmitter<string>();
    this.routes_fired = new EventEmitter<string>();
    this.watcher_service
      .city_store
      .subscribe(city_store => {
        this.city_store = city_store;
        this.cities = this._blowhorn_service.copy_array(this.city_store);
      });
    this.watcher_service
      .hub_store
      .subscribe(hub_store => {
        this.delivery_hub_store = hub_store;
        this.delivery_hubs  = this._blowhorn_service.copy_array(this.delivery_hub_store);
        this.pickup_hub_store = hub_store;
        this.pickup_hubs  = this._blowhorn_service.copy_array(this.pickup_hub_store);
      });
    this.watcher_service
      .status_store
      .subscribe(status_store => {
        this.status_store = status_store;
        this.statuses  = this._blowhorn_service.copy_array(this._blowhorn_service.array_to_object_array(this.status_store));
      });
  }

  ngOnInit() {}

  public show_edit() {
    this.show_details = !this.show_details;
  }

  sync_order() {
    this.integrationService.sync_customer_orders().subscribe(
      response=>{
        this.toastr.success('Order sync in progress');
      },
      err=>{
          console.log(err);
      }
    );
  }

  suppress_click(event: any) {
    event.stopPropagation();
    return false;
  }

  activate_date_clear(event: any): void {
    this.date_activated = true;
  }

  deactivate_date_clear(event: any): void {
    this.date_activated = false;
  }

  activate_order_clear(event: any): void {
    this.order_activated = true;
  }

  deactivate_order_clear(event: any): void {
    this.order_activated = false;
  }

  activate_batch_clear(event: any): void {
    this.batch_activated = true;
  }

  deactivate_batch_clear(event: any): void {
    this.batch_activated = false;
  }

  reset_order(event: any): void {
    this.order_number = '';
    this.order_changed(event);
  }

  reset_batch(event: any): void {
    this.batch_id = '';
    this.batch_changed(event);
  }

  reset_date(event: any): void {
    this.date_range = [null, null];
    this.date_changed(event);
  }

  applyQuickFilter(val: number): void {
    if (val === this.selected_quick_filter) {
        return;
    }
    this.selected_quick_filter = val;
    this.date_range = [null, null];
    this.filter_changed.emit({url: this.get_url()});
    this.search_fired.emit();
  }

  get city_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '130px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px'
      },
      'dropdown-menu-styles': {
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get hub_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '160px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get status_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '160px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
      },
      'dropdown-menu-styles': {
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
      }
    };
  }

  get export_styles(): {} {
    return {
        'dropdown-styles': {
          'box-shadow': 'none',
          'border': 'none',
          'height': '30px',
          'width': '100px',
          'z-index': 2
        },
        'dropdown-toggle-styles': {
          'padding-left': '0px',
          'text-align': 'right',
        },
        'dropdown-menu-styles': {
            width: '160px',
            boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
            'letter-spacing': '1.5px',
            'line-height': '18px',
            'font-family': 'Heebo',
            'font-size': '12px',
        }
    };
  }

  city_changed(event: {name: string, id: number}): void {
    if (this.selected_city_id !== event.id) {
      this.selected_city_name = event.name;
      this.selected_city_id = event.id;
      this.reset_hub();
      this.filter_dropdowns();
    }
    const url = this.get_url();
    this.filter_changed.emit({url: url});
    this.search_fired.emit();
  }

  delivery_hub_changed(event: any): void {
    if (this.selected_delivery_hub_id !== event.id) {
      this.selected_delivery_hub_name = event.name;
      this.selected_delivery_hub_id = event.id;
    }
    const url = this.get_url();
    this.filter_changed.emit({url: url});
    this.search_fired.emit();
  }

  pickup_hub_changed(event: any): void {
    if (this.selected_pickup_hub_id !== event.id) {
      this.selected_pickup_hub_name = event.name;
      this.selected_pickup_hub_id = event.id;
    }
    const url = this.get_url();
    this.filter_changed.emit({url: url});
    this.search_fired.emit();
  }

  status_changed(event: any): void {
    if (this.selected_status_id !== event.id) {
      this.selected_status = event.name;
      this.selected_status_id = event.id;
    }
    const url = this.get_url();
    this.filter_changed.emit({url: url});
    this.search_fired.emit();
  }

  date_changed(event: any): void {
    const url = this.get_url();
    this.selected_quick_filter = null;
    this.filter_changed.emit({url: url});
    this.search_fired.emit();
  }

  order_changed(event: any) {
    const url = this.get_url();
    this.filter_changed.emit({url: url});
    if (!this.order_number) {
        this.search_fired.emit();
    }
  }

  batch_changed(event: any) {
    this.batch_id = event;
    const url = this.get_url();
    this.filter_changed.emit({url: url});
    if (!this.batch_id) {
        this.search_fired.emit();
    }
  }

  filter_dropdowns(): void {
    this.delivery_hubs = [];
    this.pickup_hubs = [];
    if (this.selected_city_id) {
      const city = this.city_store
        .filter(item => item.id === this.selected_city_id)[0];
      for (const hub of city.hubs) {
        this.pickup_hubs.push(hub);
        this.delivery_hubs.push(hub);
      }
    } else {
      this.delivery_hubs = [];
      this.delivery_hubs  = this._blowhorn_service.copy_array(this.delivery_hub_store);
      this.pickup_hubs = [];
      this.pickup_hubs  = this._blowhorn_service.copy_array(this.pickup_hub_store);
    }
  }

  reset_city(): void {
    this.selected_city_id = null;
    this.selected_city_name = 'All Cities';
  }

  reset_hub(): void {
    this.selected_delivery_hub_id = null;
    this.selected_delivery_hub_name = 'All Delivery Hubs';
    this.selected_pickup_hub_id = null;
    this.selected_pickup_hub_name = 'All Pickup Hubs';
  }

  reset_export(): void {
    this.selected_export_id = null;
    this.selected_export_name = 'Export';
  }

  reset_disabled(): void {
    this.city_disabled = false;
    this.delivery_hub_disabled = false;
    this.pickup_hub_disabled = false;
  }

  search() {
    this.search_fired.emit();
  }

  export_changed(event: any) {
    this.reset_export();
    let selected_option = this.export_options.find(
        item => item.id === event.id);
    this.export_fired.emit(selected_option.value);
  }

  print_changed(event: any) {
    this.reset_print();
    let selected_option = this.print_options.find(
        item => item.id === event.id);
    this.print_fired.emit(selected_option.value);
  }

  reset_print(): void {
    this.selected_export_id = null;
    this.selected_export_name = 'Export';
  }

  route_optimiser_selected(event: any) {
    this.routes_fired.emit("Hello");
  }

  get_url(): string {
    let url = '';
    url += this.selected_city_id ? `&city_id=${this.selected_city_id}` : '';
    url += this.selected_delivery_hub_id ? `&delivery_hub_id=${this.selected_delivery_hub_id}` : '';
    url += this.selected_pickup_hub_id ? `&pickup_hub_id=${this.selected_pickup_hub_id}` : '';
    url += this.date_range[0] ? `&start=${this.date_range[0].toISOString().substring(0, 10)}` : '';
    url += this.date_range[1] ? `&end=${this.date_range[1].toISOString().substring(0, 10)}` : '';
    url += this.order_number ? `&order_ref_nos=${this.order_number}` : '';
    url += this.selected_status_id ? `&status=${this.selected_status}` : '';
    url += this.batch_id ? `&batch_id=${this.batch_id}` : '';
    url += !this.date_range[0] && this.selected_quick_filter !== null ? `&quick_filter=${this.selected_quick_filter}` : '';
    return url;
  }
}
