import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';
import { DialogPickDriver } from '../../../admin/components/route-optimiser/route-optimiser.component';


@Component({
  selector: 'app-fleet-route-optimiser',
  templateUrl: './fleet-route-optimiser.component.html',
  styleUrls: ['./fleet-route-optimiser.component.scss']
})
export class FleetRouteOptimiserComponent implements OnInit {


  map: any;
  latitude_center = 12.9715987;
  longitude_center = 77.5945627;
  zoom_level = 10;
  zoomPosition: google.maps.ControlPosition = google.maps.ControlPosition.TOP_RIGHT;
  trip_api: any;
  public show_message: boolean = true;
  public heading = "Success Message";
  public type = "success";
  public message = "Routes Created Successfully for all Orders Given";
  public error_lists: any[] = [];
  public route_directory: any[] = [];
  
  public renderOptions = {
    suppressMarkers: true,
  };

  markerOptions = {
    origin: {
      icon: Constants.markers.stop,
    },
    destination: {
      icon: Constants.markers.stop,
    },
    waypoints: {
      icon: Constants.markers.stop,
    },
  };

  constructor(
    public dialogRef: MatDialogRef<FleetRouteOptimiserComponent>,
    @Inject(MAT_DIALOG_DATA) public api_response: any,
    private _http_client: HttpClient,
    private _blowhorn_service: BlowhornService,
    public dialog: MatDialog
  ) { }

  static getCookie(name) {
    if (!document.cookie) {
        return null;
    }

    const xsrfCookies = document.cookie.split(';')
        .map(c => c.trim())
        .filter(c => c.startsWith(name + '='));

    if (xsrfCookies.length === 0) {
        return null;
    }

    return decodeURIComponent(xsrfCookies[0].split('=')[1]);
  }

  static get http_headers(): {} {
      const token = this.getCookie('csrftoken');
      return {
          headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Access-Control-Allow-Origin': '*',
              'X-CSRFToken': token
          })
      };
  }

  ngOnInit() {
    this.api_response = this.api_response.api_response;
    this.error_lists = this.api_response["non_operational_orders"];
    if(this.error_lists.length){
      this.heading = "Error Message"
      this.type = "error"
      this.message = "Some Routes Could not be Created. Please Check the mentioned orders for operational area coverage in admin panel."
    }
  }

  on_map_ready(map: any) {
    this.map = map;
  }

  select_marker(stop: any){
  }

  close(){
    this.show_message = !this.show_message;
  }

  get_icon_url(stop_sequence: number, route: any): string {
    if (stop_sequence) {
      return stop_sequence < route.elements.length ?
        Constants.markers.stop :
        Constants.markers.pickup;
    } else {
      return Constants.markers.pickup;
    }
  }

  generateRoute(route: any){
    console.log(route)
    this._blowhorn_service.show_loader();
    let waypoints: any[] = [];
    for (let i=1; i<route.elements.length-1; i++){
      let temp = {
        location:  
          { 
            lat: route.elements[i].stop_coordinates.latitude, 
            lng: route.elements[i].stop_coordinates.longitude }
        };
      waypoints.push(temp);
    }
    if(waypoints.length<=23){
      let temp_object = {
        origin: { lat: route.elements[0].stop_coordinates.latitude, lng:route.elements[0].stop_coordinates.longitude },
        destination: { lat: route.elements[route.elements.length-1].stop_coordinates.latitude, lng:route.elements[route.elements.length-1].stop_coordinates.longitude },
        waypoints: waypoints,
        visible: true
      }
      this.route_directory.push(temp_object);
    }
    else{
      let limit = Math.floor((waypoints.length+2)/25);
      for(let i=0; i<limit; i++){
        let temp_object = {
          origin: { lat: route.elements[25*i].stop_coordinates.latitude, lng:route.elements[25*i].stop_coordinates.longitude },
          destination: {lat: route.elements[(25*(i+1))].stop_coordinates.latitude, lng:route.elements[(25*(i+1))].stop_coordinates.longitude},
          waypoints: waypoints.slice((25*i), (25*(i+1))),
          visible: true
        }
        this.route_directory.push(temp_object);
      }
      let temp_object = {
        origin: { lat: route.elements[25*limit].stop_coordinates.latitude, lng:route.elements[25*limit].stop_coordinates.longitude },
        destination: { lat: route.elements[Math.min((25*limit) + 24, route.elements.length-1)].stop_coordinates.latitude, lng:route.elements[Math.min((25*limit) + 24, route.elements.length-1)].stop_coordinates.longitude},
        waypoints: waypoints.slice((25*limit), Math.min((25*(limit+1)), route.elements.length-1)),
        visible: true
      }
      this.route_directory.push(temp_object);
    }
    console.log(this.route_directory);
    this._blowhorn_service.hide_loader();
  }

  generateTrip(trip_details: any){
    this._blowhorn_service.show_loader();
    this._http_client.post(Constants.urls.API_CREATE_BLANKET_TRIP, trip_details, FleetRouteOptimiserComponent.http_headers)
      .toPromise()
      .then(
        res => {
          this.trip_api = res;
          alert("Note: " + this.trip_api.trip_id);
          this._blowhorn_service.hide_loader();
        },
        err => {
          alert("Trip could not be created due to internal server error! Please Try Again later!")
          this._blowhorn_service.hide_loader();
        }

      )
  }

  allTripsHelper(trip_details: any): any{
    this._http_client.post(Constants.urls.API_CREATE_BLANKET_TRIP, trip_details, FleetRouteOptimiserComponent.http_headers)
      .toPromise()
      .then(
        res => {
          this.trip_api = res;
          return this.trip_api.trip_id;
        },
        err => {
          return "Trip could not be created due to internal server error! Please Try Again later!";
        }
      )
  }

  clearRoutes(){
    for(let i=0; i<this.route_directory.length; i++){
      this.route_directory[i].visible = false;
    }
  }
  
  createAllTrips(){
    let out_list = [];
    for(let cluster of this.api_response.rows){
      for(let route of cluster.routes_information){
        out_list.push(this.allTripsHelper({
          "route": route
        }));
      }
    }
    console.log(out_list);
    alert("Trips have been created! Please check them in the Enterprise Trips sections! Driver will be assigned soon from Blowhorn Team!");
  }

  openDialog(route: any){
    this._blowhorn_service.show_loader()
    this._blowhorn_service.hide_loader()
    const dialogRefDriver = this.dialog.open(DialogPickDriver, {
      width: '750px',
      data: {
        route: route,  
        selected_driver: {}
      }
    });

    dialogRefDriver.afterClosed()
    .subscribe(
      result => {
        if(result) {  
          let trip_details = {
            "route": route,
            "driver": result
          }
          console.log(trip_details);
          this.generateTrip(trip_details);
        }
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async createTripWithoutOpenDialog(route:any ){
      this._blowhorn_service.show_loader();
      let trip_details = {
        "route": route
      };
      await this.generateTrip(trip_details);
      this._blowhorn_service.hide_loader();
  }

}
