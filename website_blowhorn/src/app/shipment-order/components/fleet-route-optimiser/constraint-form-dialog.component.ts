import { Component, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
    selector: 'constraint-form-dialog',
    templateUrl: './constraint-form-dialog.component.html',
    styleUrls: ['./constraint-form-dialog.component.scss']
})
export class ConstraintFormDialog {
    public capacity=1000000;
    public number_vehicles = 3;
    public limiting_distance = 60000;
    public limiting_duration = 10800;
    public selectedUnit = "weight";
    public constraints = [
        {
            "name": "Distance",
            "param": "distance"
        },
        {
            "name": "Time",
            "param": "duration"
        }
    ];
    public capacity_const = [
        {
            "name": "Weight",
            "param": "weight"
        },
        {
            "name": "Volume",
            "param": "volume"
        }
    ]
    public selectedConstraint = "distance";
    public constraint_is_distance = true;
    public unit_is_weight = true;
    public params_json = {
        "vehicles": this.number_vehicles,
        "distance": this.limiting_distance,
        "duration": this.limiting_duration,
        "constraint": this.selectedConstraint,
        "capacity": this.capacity,
        "capacity_unit": this.selectedUnit,
        "order_numbers": this.data.order_numbers
    };
    constructor(
        public dialogFormRef: MatDialogRef<ConstraintFormDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

    ngOnInit() {
    }
    update_params_json() {
        this.params_json.vehicles = this.number_vehicles;
        this.params_json.distance = this.limiting_distance;
        this.params_json.duration = this.limiting_duration;
        this.params_json.constraint = this.selectedConstraint;
        this.params_json.capacity = this.capacity;
        this.params_json.capacity_unit = this.selectedUnit;
    }

    onKeyVehicleCapacity(capacity: any){
        this.capacity = capacity.target.value;
        this.update_params_json();
    }

    onKeyNumberOfVehicles(vehicles: any){
        this.number_vehicles = vehicles.target.value;
        this.update_params_json();
    }
    

    onKeyLimitingDistance(distance: any){
        this.limiting_distance = distance.target.value*1000;
        this.update_params_json();
    }

    onKeyLimitingDuration(duration: any){
        this.limiting_duration = duration.target.value*3600;
        this.update_params_json();
    }

    changeConstraint(value: any){
        if(this.selectedConstraint === "duration"){
            this.constraint_is_distance = false;
        }
        else{
            this.constraint_is_distance = true;
        }
        this.update_params_json();
    }

    changeCapacityUnit(value: any){
        if(this.selectedUnit === "volume"){
            this.unit_is_weight = false;
        }
        else{
            this.unit_is_weight = true;
        }
        this.update_params_json();
    }

    onNoClick(): void {
        this.data.selected_driver = {};
        this.dialogFormRef.close();
    }
}