import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Shipment } from '../../../shared/models/shipments-grid.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ShipmentService } from '../../../shared/services/shipment.service';

@Component({
  selector: 'app-cod-details',
  templateUrl: './cod-details.component.html',
  styleUrls: ['./cod-details.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CodDetailsComponent implements OnInit {

  @Input()
  shipment: Shipment;
  @Input()
  heading: string = 'COD Details';
  @Output()
  close_fired: EventEmitter<boolean>;

  loading = true;
  results: any = []
  columnsToDisplay = ['id', 'trip_number', 'amount', 'payment_bank_txn', 'payment_mode', 'status', 'created_date', 'delivery_hub', 'more']
  headerNames = {
    id: 'id',
    trip: 'trip',
    amount: 'amount',
    payment_bank_txn: 'txn id',
    payment_mode: 'payment mode',
    status: 'status',
    created_date: 'txn date',
    delivery_hub: 'hub'
  }
  expandedElement: any | null;

  constructor(
    private shipment_service: ShipmentService,
    private bh_service: BlowhornService,
  ) {
    this.close_fired = new EventEmitter<boolean>();
  }

  ngOnInit() {
    const params = this.bh_service.convertObjectToQueryString({
      page_size: 1000,
      page: 1,
      ordering: '-created_date',
      start: this.get_formatted_date(new Date(this.shipment.date_placed)),
      end: this.get_formatted_date(new Date()),
      search: this.shipment.number,
    })
    this.shipment_service
      .get_cod_payment_details(params).subscribe(resp => {
        this.results = resp.results;
        this.loading = false;
      }, err => {
        console.error(err);
        this.loading = false;
      });
  }

  get_formatted_date(date: Date) {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  suppress_event(event: any) {
    event.stopPropagation();
    return false;
  }

  close_fired_modal(event: any) {
    this.close_fired.emit(true);
  }
  

}
