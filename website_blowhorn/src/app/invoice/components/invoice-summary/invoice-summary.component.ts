import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Trip } from '../../../shared/models/trip.model';
import { Office } from '../../../shared/models/office.model';
import { User } from '../../../shared/models/user.model';
import { Customer } from '../../../shared/models/customer.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-invoice-summary',
  templateUrl: './invoice-summary.component.html',
  styleUrls: ['./invoice-summary.component.scss']
})
export class InvoiceSummaryComponent implements OnInit {

  @Input('trip')
  trip: Trip;
  @Input('office')
  office: Office;
  @Input('consignor')
  consignor: User;
  @Input('consignee')
  consignee: User;
  @Input('shipments')
  shipments: string[];
  @Input('customer')
  customer: Customer;
  @Output()
  download_pressed: EventEmitter<boolean>;

  constructor(
    public blowhorn_service: BlowhornService
  ) {
    this.download_pressed = new EventEmitter(false);
  }

  ngOnInit() {
  }

  download_invoice(download) {
    if (download) {
      this.download_pressed.emit(true);
    }
  }

}
