import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as $ from 'jquery';
import { Trip } from '../../../shared/models/trip.model';

@Component({
  selector: 'app-invoice-print-panel',
  templateUrl: './invoice-print-panel.component.html',
  styleUrls: ['./invoice-print-panel.component.scss']
})
export class InvoicePrintPanelComponent implements OnInit {

  @Input()
  trip: Trip;
  trip_id: string;
  trip_date: string;
  distance: number;
  time_taken: string;
  invoice_no: string;
  additional_stops: number;
  @Output()
  download_pressed: EventEmitter<boolean>;

  constructor() {
    this.download_pressed = new EventEmitter(false);
  }

  ngOnInit() {
    const trip = this.trip;
    this.trip_id = trip.order_id;
    this.trip_date = trip.date;
    this.distance = trip.total_distance;
    this.time_taken = trip.total_duration;
    this.additional_stops = trip.stops.length;
    this.invoice_no = trip.invoice_number;
  }

  print(): void {
   print();
  }

  download(): void {
    this.download_pressed.emit(true);
  }

}
