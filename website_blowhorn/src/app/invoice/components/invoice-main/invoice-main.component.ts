import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Trip } from '../../../shared/models/trip.model';
import { Office } from '../../../shared/models/office.model';
import { User } from '../../../shared/models/user.model';
import { Customer } from '../../../shared/models/customer.model';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-invoice-main',
  templateUrl: './invoice-main.component.html',
  styleUrls: ['./invoice-main.component.scss']
})
export class InvoiceMainComponent implements OnInit {

  trip: Trip;
  office: Office;
  consignor: User;
  consignee: User;
  shipments: string[];
  customer: Customer;
  @ViewChild('invoice', {static: false})
  invoice: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private title_service: Title,
  ) {
    route.data
      .subscribe(data => {
        const invoice = data.invoice;
        this.trip = invoice.trip;
        this.office = invoice.office;
        this.consignor = invoice.consignor;
        this.consignee = invoice.consignee;
        this.shipments = invoice.shipments;
        this.customer = invoice.customer;
        this.title_service
          .setTitle(`Invoice - ${this.trip.order_id}`);
      },
    err => {
      console.error(err);
      this.title_service
          .setTitle(`Invoice`);
    });
  }

  ngOnInit() {
  }

  download_invoice(download) {
    window.open(`api/order/${this.trip.order_id}/invoice?output=pdf&download=true`, '_blank');
  }

}
