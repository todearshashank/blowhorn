import { Component, OnInit, Input } from '@angular/core';
import { BlowhornAddressComponent } from '../../../shared/components/blowhorn-address/blowhorn-address.component';
import { Office } from '../../../shared/models/office.model';
import { Trip } from '../../../shared/models/trip.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-invoice-footer',
  templateUrl: './invoice-footer.component.html',
  styleUrls: ['./invoice-footer.component.scss']
})
export class InvoiceFooterComponent implements OnInit {

  @Input('office')
  office: Office;
  @Input('trip')
  trip: Trip;
  show_seal = this.blowhorn_service.current_build === Constants.default_build;
  seal_src = Constants.images.seal;
  constructor(
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
  }

}
