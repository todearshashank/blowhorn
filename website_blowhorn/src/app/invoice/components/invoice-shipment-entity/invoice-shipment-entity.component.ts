import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-invoice-shipment-entity',
  templateUrl: './invoice-shipment-entity.component.html',
  styleUrls: ['./invoice-shipment-entity.component.scss']
})
export class InvoiceShipmentEntityComponent implements OnInit {
  @Input('title')
  title: string;
  @Input('detail_content')
  detail_content: string;
  @Input('main_content')
  main_content: string;
  @Input('sub_content')
  sub_content: string;
  footer_content: string;

  constructor() { }

  ngOnInit() {
  }

}
