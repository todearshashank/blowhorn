import { Component, OnInit, Input } from '@angular/core';
import { Stop } from '../../../shared/models/stop.model';

@Component({
  selector: 'app-invoice-stop',
  templateUrl: './invoice-stop.component.html',
  styleUrls: ['./invoice-stop.component.scss']
})
export class InvoiceStopComponent implements OnInit {

  @Input('stop')
  stop: Stop;
  @Input('img_url')
  img_url: string;
  @Input('title')
  title: string;
  address_headding: string;
  address_details: string;
  constructor() { }

  ngOnInit() {
    const stop = this.stop;
    this.address_headding = stop.name;
    this.address_details = stop.address;
  }

}
