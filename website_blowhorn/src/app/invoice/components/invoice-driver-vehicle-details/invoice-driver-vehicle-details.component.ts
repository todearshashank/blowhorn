import { Component, OnInit, Input } from '@angular/core';
import { Trip } from '../../../shared/models/trip.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-invoice-driver-vehicle-details',
  templateUrl: './invoice-driver-vehicle-details.component.html',
  styleUrls: ['./invoice-driver-vehicle-details.component.scss']
})
export class InvoiceDriverVehicleDetailsComponent implements OnInit {

  @Input('trip')
  trip: Trip;
  truck_type: string;
  driver_name: string;
  truck_url: string;
  driver_avatar = Constants.images.contact;
  constructor(
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
    const trip = this.trip;
    this.truck_type = trip.truck_type;
    this.driver_name = trip.driver_name;
    this.truck_url = trip.truck_url;
  }

}
