import { Component, OnInit, Input } from '@angular/core';
import { Trip } from '../../../shared/models/trip.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { Constants } from '../../../shared/utils/constants';

@Component({
  selector: 'app-invoice-pickup-dropoff-fare',
  templateUrl: './invoice-pickup-dropoff-fare.component.html',
  styleUrls: ['./invoice-pickup-dropoff-fare.component.scss']
})
export class InvoicePickupDropoffFareComponent implements OnInit {
  @Input('trip')
  trip: Trip;
  pickup_location: string;
  dropoff_location: string;
  total_amount: number;
  pickup_time: string;
  dropoff_time: string;
  address_config = {
    pickup: {
      title: 'Pickup',
      img_url: Constants.markers.pickup
    },
    dropoff: {
      title: 'Dropoff',
      img_url: Constants.markers.dropoff
    }
  }

  constructor(
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {
    const trip = this.trip;
    this.pickup_location = trip.header_from;
    this.dropoff_location = trip.header_to;
    this.total_amount = trip.price;
    this.pickup_time = trip.pickup.time;
    this.dropoff_time = trip.dropoff.time;
  }

  show_breakup(): boolean {
    const fare_breakup = this.trip.fare_breakup_details;
    return fare_breakup.filter(item => item.value > 0).length > 0;
  }

}
