import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../shared/models/user.model';
import { Customer } from '../../../shared/models/customer.model';

@Component({
  selector: 'app-invoice-shipment-details',
  templateUrl: './invoice-shipment-details.component.html',
  styleUrls: ['./invoice-shipment-details.component.scss']
})
export class InvoiceShipmentDetailsComponent implements OnInit {

  @Input('shipments')
  shipments: string[];
  @Input('consignor')
  consignor: User;
  @Input('consignee')
  consignee: User;
  @Input('customer')
  customer: Customer;
  consignor_gst: string = null;
  constructor() { }

  ngOnInit() {
  }

  get_full_name_or_customer_name(user: User) {
    const user_full_name = `${user.first_name} ${user.last_name}`.trim();
    return user_full_name ? user_full_name : this.get_customer_name();
  }

  get_shipments_list(): string {
    return this.shipments.join(', ');
  }

  get_customer_name(): string {
    const name = this.customer.legal_name || this.customer.name;
    if (name.toLowerCase() === 'guest') {
      return `${this.customer.mobile}  - ${name} User`;
    } else {
      return name;
    }
  }

  get_company_name(): string {
    if (this.customer.is_business_user
        && (this.consignor.first_name
        || this.consignor.last_name)) {
      return this.customer.legal_name || this.customer.name;
    } else {
      return '';
    }
  }

}
