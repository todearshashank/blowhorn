import { Component, OnInit, Input } from '@angular/core';
import { NameValue } from '../../../shared/models/name-value.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-invoice-fare-breakup',
  templateUrl: './invoice-fare-breakup.component.html',
  styleUrls: ['./invoice-fare-breakup.component.scss']
})
export class InvoiceFareBreakupComponent implements OnInit {

  @Input('fare_breakup_details')
  fare_breakup_details: NameValue[];
  @Input('amount_payable')
  amount_payable: NameValue[];

  constructor(
    public blowhorn_service: BlowhornService
  ) { }

  ngOnInit() {}

}
