import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceSummaryComponent } from './components/invoice-summary/invoice-summary.component';
import { InvoiceTripDetailsComponent } from './components/invoice-trip-details/invoice-trip-details.component';
import { InvoiceMainComponent } from './components/invoice-main/invoice-main.component';
import { InvoicePrintPanelComponent } from './components/invoice-print-panel/invoice-print-panel.component';
import { InvoicePickupDropoffFareComponent } from './components/invoice-pickup-dropoff-fare/invoice-pickup-dropoff-fare.component';
import { InvoiceFareBreakupComponent } from './components/invoice-fare-breakup/invoice-fare-breakup.component';
import { InvoiceStopComponent } from './components/invoice-stop/invoice-stop.component';
import { InvoiceDriverVehicleDetailsComponent } from './components/invoice-driver-vehicle-details/invoice-driver-vehicle-details.component';
import { InvoiceShipmentDetailsComponent } from './components/invoice-shipment-details/invoice-shipment-details.component';
import { InvoiceShipmentEntityComponent } from './components/invoice-shipment-entity/invoice-shipment-entity.component';
import { InvoiceFooterComponent } from './components/invoice-footer/invoice-footer.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    InvoiceSummaryComponent,
    InvoiceTripDetailsComponent,
    InvoiceMainComponent,
    InvoicePrintPanelComponent,
    InvoicePickupDropoffFareComponent,
    InvoiceFareBreakupComponent,
    InvoiceStopComponent,
    InvoiceDriverVehicleDetailsComponent,
    InvoiceShipmentDetailsComponent,
    InvoiceShipmentEntityComponent,
    InvoiceFooterComponent,
  ]
})
export class InvoiceModule { }
