import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Constants } from './../../shared/utils/constants';
import { Member } from './../../shared/models/member.model';
import { BlowhornPaginationComponent } from './../../shared/components/blowhorn-pagination/blowhorn-pagination.component';
import { BlowhornService } from '../../shared/services/blowhorn.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { AddUpdateBasicDetailsComponent } from '../add-update-basic-details/add-update-basic-details.component';
import { MemberService } from '../../shared/services/member.service';

@Component({
  selector: 'app-member-list-view',
  templateUrl: './member-list-view.component.html',
  styleUrls: ['./member-list-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  styles: [`
    @media screen {
        .modal-dialog.modal-lg {
            width: 45vw !important;
            max-width: calc(80%);
        }
    }`]
})
export class MemberListViewComponent implements OnInit {

    @ViewChild('pagination', {static: false})
    pagination: BlowhornPaginationComponent;

    views = {
        list: 'list',
        grid: 'grid'
    }
    master_data: Member[] = [];
    members: Member[] = [];
    url = Constants.urls.API_MEMBERS;
    count: number;
    next: string;
    previous: string;
    error_msg = 'No members found';
    currentView = this.views.list;
    confirmation = {
        heading: '',
        details: '',
        show_warning: false,
        show_alert: false,
        disable_button: false,
        action_buttons: {
            confirm: {
                show: false,
                label: 'Yes',
                type: 'save'
            },
            decline: {
                show: false,
                label: 'No',
                type: ''
            }
        },
        styles: {
            header: {}
        }
    };

    constructor(
        private route: ActivatedRoute,
        private _bottomSheet: MatBottomSheet,
        public blowhorn_service: BlowhornService,
        private memberService: MemberService
    ) {
        this.memberService.loadSupportData()
        route.data.subscribe(data => {
            if (data.members) {
                this.update_members(data.members);
            }
            this.blowhorn_service.hide_loader();
        },
        err => {
            console.error(err);
            this.error_msg = 'Something went wrong.';
            this.blowhorn_service.hide_loader();
        });
    }

    ngOnInit() {
        this.confirmation.show_alert = false;
    }

    update_members(data: {count: number, next: string, previous: string, results: any[]}) {
        this.count = data.count;
        this.next = data.next;
        this.previous = data.previous;
        this.members = data.results;
        this.master_data = data.results;
    }

    filter_changed(event: {term: string, role: string, status: string}) {
        this.members = [];
        const url = this.getUrl(event)
        this.update_members({
            count: 0,
            next: '',
            previous: '',
            results: []
        });
        this.blowhorn_service.show_loader();
        this.memberService.get_members(url).subscribe(data => {
            this.blowhorn_service.hide_loader();
            this.update_members(data);
        },
        err => {
            this.blowhorn_service.hide_loader();
            console.error('Error', err);
        });
    }

    getUrl(filterParams: any): string {
        let url = '';
        if (filterParams) {
            url += filterParams.term ? `&search=${filterParams.term.toLowerCase().trim()}` : '';
            url += filterParams.role ? `&group_id=${filterParams.role}` : '';
            url += filterParams.status ? `&status=${filterParams.status}` : '';
        }
        return `${Constants.urls.API_MEMBERS}?${url}`;
    }

    open_member_modal(): void {
        const bottomSheetRef = this._bottomSheet.open(AddUpdateBasicDetailsComponent, {
            data: {
                operation: 'Invite'
            }
        });
        bottomSheetRef.afterDismissed().subscribe((result) => {
            console.log('Bottom sheet has been dismissed.', result);
            if (result) {
                this.master_data.splice(0, 0, result);
            }
        });
    }


    viewChanged(event: string) {
        this.currentView = event;
    }

    show_alert(event: {type: string, message: string}): void {
        this.confirmation.heading = event.type;
        this.confirmation.details = event.message;
        this.confirmation.show_warning = false;
        this.confirmation.action_buttons.confirm.show = false;
        this.confirmation.action_buttons.decline.show = true;
        this.confirmation.action_buttons.decline.label = 'Ok';
        this.confirmation.action_buttons.decline.type = '';
        this.confirmation.disable_button = false;
        this.confirmation.styles.header = {
            'padding': '5px 10px',
            'color': '#fff'
        };
        this.confirmation.show_alert = true;
        if (event.type === 'Failed') {
            this.confirmation.styles.header['background'] = '#cf342e';
            return;
        }
        this.confirmation.styles.header['background'] = '#05c75c';
        setTimeout(() => {
            this.confirmation.show_alert = false;
        }, 3000);
    }

    close_alert(event: any): void {
        this.confirmation.show_alert = false;
    }

    get membersPanel(): {} {
        let gridRows = {}
        switch (this.currentView) {
            case this.views.grid:
                gridRows = {
                    'grid-template-rows': '50px minmax(auto, calc(100vh - 198px)) 55px;'
                }
                break;
            
            default:
                gridRows = {
                    'grid-template-rows': '50px 30px minmax(auto, calc(100vh - 198px)) 55px;'
                }
                break;
        }
        return gridRows
    }

}
