import { BlowhornService } from './../../shared/services/blowhorn.service';
import { MemberService } from './../../shared/services/member.service';
import { Member } from './../../shared/models/member.model';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-add-update-basic-details',
    templateUrl: './add-update-basic-details.component.html',
    styleUrls: ['./add-update-basic-details.component.scss']
})
export class AddUpdateBasicDetailsComponent implements OnInit {

    headerText: String = '';
    member: Member;
    memberForm: FormGroup;
    user_details: any;
    submit_button = {
        type: 'save',
        label: 'Save',
        disabled: false
    };
    dismiss_button = {
        label: 'Cancel',
        type: 'delete',
        disabled: false
    };
    alert = {
        type: '',
        message: '',
        show_message: false
    };
    allowToChangeAdminStatus = false;
    allowToEdit = false;
    statusPipeline = {
        invited: [{
            label: 'Activate user',
            value: 'active',
            type: 'save'
        },
        {
            label: 'Deactivate user',
            value: 'inactive',
            type: 'delete'
        }],
        active: [{
            label: 'Deactivate user',
            value: 'inactive',
            type: 'delete'
        }],
        inactive: [{
            label: 'Activate user',
            value: 'active',
            type: 'save'
        },]
    }
    toastrConfig = {
        tapToDismiss: true,
        timeOut: 5000,
        positionClass: 'toast-top-center',
        closeButton: true,
        progressBar: true,
        preventDuplicates: true,
    }

    @Output() closeModal: EventEmitter<boolean>;

    constructor(
        @Inject(MAT_BOTTOM_SHEET_DATA) public data: { member: Member, operation: String },
        private _bottomSheetRef: MatBottomSheetRef<AddUpdateBasicDetailsComponent>,
        public _member_service: MemberService,
        private _bh_service: BlowhornService,
        private toastr: ToastrService,
    ) {
        this.closeModal = new EventEmitter<boolean>();
        this.member = this.data.member;
    }

    ngOnInit() {
        let vm = this;
        console.log(this.member)
        this.user_details = this._bh_service.user_details;
        this.submit_button.disabled = !this._member_service.has_invite_permission;
        this.allowToChangeAdminStatus = this.member && this.member.email !== this.user_details.email && this._member_service.has_invite_permission;
        this.allowToEdit = this._member_service.has_invite_permission;
        this.headerText = `${!this.member ? 'Invite' : 'Edit'} Member`;
        this.memberForm = new FormGroup({
            name: new FormControl({value: vm.member ? vm.member.name : '', disabled: !this.allowToEdit}, [Validators.required, Validators.maxLength(60)]),
            email: new FormControl({
                value: vm.member ? vm.member.email : '', disabled: !this.allowToEdit},
                [Validators.email, Validators.required]
            ),
            phone_number: new FormControl({value: vm.member ? vm.member.phone_number : '', disabled: !this.allowToEdit}, [
                Validators.required,
                Validators.pattern('[0-9]{10}'),
                Validators.maxLength(10)
            ]),
            is_admin: new FormControl({value: vm.member ? vm.member.is_admin : false, disabled: !this.allowToEdit})
        });
    }

    hasError = (controlName: string, errorName: string) => {
        return this.memberForm.controls[controlName].hasError(errorName);
    }

    onSubmit(_ownerFormValue: any): void {
        console.log('this.memberForm', this.memberForm);
        if (!this.memberForm.valid) {
            this.toastr.error('Please check the errors', 'Error', this.toastrConfig)
            return;
        }
        this.submit_button.disabled = true;
        if (this.data.operation === 'Invite') {
            this.invite_member();
        } else {
            this.update_member();
        }
    }

    invite_member(): any {
        this._member_service.invite_member(this.memberForm.value)
        .subscribe((resp: any) => {
            console.log(resp.data, resp);
            this._bottomSheetRef.dismiss(resp.data);
            this.submit_button.disabled = false;
            this.toastr.success(resp.message, 'Success', this.toastrConfig);
        },
        err => {
            this.submit_button.disabled = false;
            if (err) {
                this.toastr.error(err.error.message || err.error.detail, 'Failed', this.toastrConfig);
            }
            console.error('Error', err);
        });
    }

    update_member(): any {
        let memberBkp = this._bh_service.copy_object(this.member)
        let requestPayload = Object.assign(memberBkp, this.memberForm.value);
        this._member_service
            .update_member(requestPayload)
            .subscribe((resp: any) => {
                this.toastr.success(resp.message, 'Success', this.toastrConfig);
                this.submit_button.disabled = false;
                this._bottomSheetRef.dismiss(Object.assign({}, resp.data));
            }, (err: any) => {
                this.submit_button.disabled = false;
                let errors = err.error.message || err.error.detail;
                if (!errors) {
                    errors = err.error.email.join('\n');
                }
                this.toastr.error(errors || 'Something went wrong', 'Failed', this.toastrConfig);
                console.error('Error', err);
            });
    }

    updateStatus(item: any) {
        this.member.status = item.value;
        this.update_member();
    }

    show_alert(type: string, message: string): void {
        this.toastr.success(message);
    }

    dismiss(event: any) {
        this._bottomSheetRef.dismiss();
    }

}
