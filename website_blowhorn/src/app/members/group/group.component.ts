import { BlowhornService } from './../../shared/services/blowhorn.service';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Group, Member, Module, Permission, System } from '../../shared/models/member.model';
import { MemberService } from '../../shared/services/member.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-group',
    templateUrl: './group.component.html',
    styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

    group: Group;

    modules: Module[] = [];
    permission_levels = [];
    permissions: Permission[] = [];
    systems: System[] = [];
    groups: Group[] = []
    formData: any = {
        name: '',
        permissions: ''
    };
    dismissButton = {
        label: 'Cancel',
        type: 'delete',
        disabled: false
    };
    submitButton = {
        type: 'save',
        label: 'Save',
        disabled: false
    };

    toastrConfig = {
        tapToDismiss: true,
        timeOut: 5000,
        positionClass: 'toast-top-center',
        closeButton: true,
        progressBar: true,
        preventDuplicates: true,
    }

    constructor(
        private router: Router,
        private _bh_service: BlowhornService,
        private _member_service: MemberService,
        private toastr: ToastrService,
    ) {
        this._member_service.loadSupportData()
    }

    ngOnInit() {
        let groupId = this.router.url.split('?').shift().split('/').pop()
        this._member_service.support_data_store.subscribe((resp: any) => {
            if (!resp.systems.length) {
                return
            }
            this.systems = resp.systems.sort();
            this.modules = resp.modules;
            this.permission_levels = resp.permission_levels ?  resp.permission_levels.sort((a: { position: number; }, b: { position: number; }) => a.position - b.position) : [];
            this.permissions = resp.permissions;
            this.groups = resp.groups;
            if (groupId) {
                this.group = resp.groups.find((i: Group) => i.id === parseInt(groupId));
                if (this.group) {
                    this.formData = this._bh_service.copy_object(this.group);
                    this.permissions.forEach(perm => {
                        perm.active = this.group.permissions.findIndex(p => p.id === perm.id) > 1
                    })
                }
            }
        });
    }

    selectedPermissions(event: any) {
        this.formData.modules = event;
    }

    saveForm(event: any) {
        if (!this.formData.name) {
            this.toastr.error('Name is mandatory to create a role', 'Error', this.toastrConfig)
            return;
        }
        let selectedPermissions = [];
        let selectedPermissionLevels = [];
        let modules = this.formData.modules || [];
        for (let i = 0; i < modules.length; i++) {
            let module = modules[i];
            selectedPermissions = !module.all
                ? selectedPermissions.concat(module.permissions.filter((perm: Permission) => perm.active))
                : selectedPermissions.concat(module.permissions);

            let permissionLevels = module.permissionLevels;
            let tmpPermLevel = []
            for (let j = 0; j < permissionLevels.length; j++) {
                let permLevel = permissionLevels[j];
                if (permLevel.selectedOptions.length) {
                    tmpPermLevel.push({
                        permission_level_id: permLevel.id,
                        module_id: module.id,
                        value: permLevel.selectedOptions.map((level: { id: number; }) => level.id)
                    });
                }
            }
            selectedPermissionLevels = selectedPermissionLevels.concat(tmpPermLevel)
        }

        if (this.group && this.group.id) {
            this._member_service.update_group(this.group.id, {
                name: this.formData.name,
                permissions: selectedPermissions.map((i: Permission) => i.id),
                levels: selectedPermissionLevels,
            }).subscribe((resp) => {
                this.dismiss(null);
            }, err => {
                console.error(err);
                let errorMessage = typeof (err.error) === 'string' ? err.error : err.error.detail;
            })
        } else {
            this._member_service.add_group({
                name: this.formData.name,
                permissions: selectedPermissions.map((i: Permission) => i.id),
                levels: selectedPermissionLevels,
            }).subscribe((resp) => {
                this.dismiss(null);
            }, err => {
                console.error(err);
                let errorMessage = typeof (err.error) === 'string' ? err.error : err.error.detail;
            })
        }
    }

    dismiss(event: any) {
        this.router.navigate(["dashboard", "members", "roles"]);
    }

}
