import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MemberService } from '../../shared/services/member.service';

@Component({
  selector: 'app-members-home',
  templateUrl: './members-home.component.html',
  styleUrls: ['./members-home.component.scss']
})
export class MembersHomeComponent implements OnInit {

    links = [
        { link: 'listview', text: 'Users' },
        { link: 'roles', text: 'Roles' },
    ];
    link_styles = {};
    navbar_styles = {};

    constructor(
        private route: ActivatedRoute,
        private _member_service: MemberService,
    ) { }

    ngOnInit() {
        
    }

}
