import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MemberService } from '../../shared/services/member.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-group-list-view-filter',
    templateUrl: './group-list-view-filter.component.html',
    styleUrls: ['./group-list-view-filter.component.scss']
})
export class GroupListViewFilterComponent implements OnInit {

    @Output() filter_changed: EventEmitter<{ term: string }>;
    @Output() add_group_clicked: EventEmitter<void>;
    filter_value = '';
    add_button = {
        label: 'Add Role'
    };

    constructor(
        public memberService: MemberService,
        private router: Router,
    ) {
        this.filter_changed = new EventEmitter<{ term: string }>();
    }

    ngOnInit() {
    }

    filter_value_changed(event: any) {
        const emitVal = {
            term: this.filter_value,
        };
        this.filter_changed.emit(emitVal);
    }

    add_group() {
        this.router.navigate(['dashboard', 'members', 'roles', 'add']);
    }

}
