import { AddUpdateBasicDetailsComponent } from './../add-update-basic-details/add-update-basic-details.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Member } from './../../shared/models/member.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MemberService } from '../../shared/services/member.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { PermissionDailogComponent } from '../permission-dailog/permission-dailog.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-member-list-view-grid-row',
  templateUrl: './member-list-view-grid-row.component.html',
  styleUrls: ['./member-list-view-grid-row.component.scss']
})
export class MemberListViewGridRowComponent implements OnInit {

    @Input() member: Member;
    @Output() show_message: EventEmitter<{type: string, message: string}>;
    resp_msg = '';
    show = {
        invite_loader: false,
    };
    min_loader_styles = {
        wrapper: {
            width: '50px'
        },
        ball1: {
            height: '10px',
            width: '10px',
        },
        ball2: {
            height: '10px',
            width: '10px',
        },
        ball3: {
            height: '10px',
            width: '10px',
        },
    };

    constructor(
        private _bottomSheet: MatBottomSheet,
        private router: Router,
        public member_service: MemberService,
    ) {
        this.show_message = new EventEmitter<{type: string, message: string}>();
    }

    ngOnInit() {
    }

    open_member_modal(): void {
        const bottomSheetRef = this._bottomSheet.open(AddUpdateBasicDetailsComponent, {
            data: {
                member: this.member,
                operation: 'Update'
            }
        })
        bottomSheetRef.afterDismissed().subscribe((result) => {
            console.log('Bottom sheet has been dismissed.', result);
            if (result) {
                this.member = result;
            }
        });
    }

    open_permission_modal(): void {
        this.router.navigate(['dashboard', 'members', 'listview', this.member.id, 'permissions']);
    }

    openGroup(id: number) {
        this.router.navigate(['dashboard', 'members', 'roles', id]);
    }

    resend_invite_link(): void {
        this.show.invite_loader = true;
        this.member_service.reinvite_member({
            email: this.member.email
        }).subscribe(resp => {
            this.show_message.emit({type: 'Success', message: resp.message});
            this.show.invite_loader = false;
            this.member.is_invite_link_expired = false;
        }, resp => {
            console.error(resp);
            this.show_message.emit({type: 'Failed', message: resp.error.message});
            this.show.invite_loader = false;
        });
    }
}
