import { EditGroupComponent } from './edit-group/edit-group.component';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';


import { AppRoutingModule } from '../app-routing.module';
import { MemberListViewComponent } from './member-list-view/member-list-view.component';
import { MemberListViewGridRowComponent } from './member-list-view-grid-row/member-list-view-grid-row.component';
import { MemberListViewGridTitleComponent } from './member-list-view-grid-title/member-list-view-grid-title.component';
import { MemberMainComponent } from './member-main/member-main.component';
import { SharedModule } from '../shared/shared.module';
import { MemberListViewFilterComponent } from './member-list-view-filter/member-list-view-filter.component';
import { AddUpdateMemberComponent } from './add-update-member/add-update-member.component';
import { CardComponent } from './card/card.component';
import { AddUpdateBasicDetailsComponent } from './add-update-basic-details/add-update-basic-details.component';
import { PermissionDailogComponent } from './permission-dailog/permission-dailog.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { GroupComponent } from './group/group.component';
import { GroupListViewComponent } from './group-list-view/group-list-view.component';
import { GroupListViewFilterComponent } from './group-list-view-filter/group-list-view-filter.component';
import { GroupListViewGridRowComponent } from './group-list-view-grid-row/group-list-view-grid-row.component';
import { GroupListViewGridTitleComponent } from './group-list-view-grid-title/group-list-view-grid-title.component';
import { GroupsMainComponent } from './groups-main/groups-main.component';
import { ReadonlyPermissionViewComponent } from './readonly-permission-view/readonly-permission-view.component';
import { MembersHomeComponent } from './members-home/members-home.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule,
    SharedModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // Material
    MatBottomSheetModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatDividerModule,
    MatExpansionModule,
    MatListModule,
    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatChipsModule
  ],
  declarations: [
      MemberListViewComponent,
      MemberListViewGridRowComponent,
      MemberListViewGridTitleComponent,
      MemberMainComponent,
      MemberListViewFilterComponent,
      AddUpdateMemberComponent,
      CardComponent,
      AddUpdateBasicDetailsComponent,
      PermissionDailogComponent,
      PermissionsComponent,
      GroupComponent,
      GroupListViewComponent,
      GroupListViewFilterComponent,
      GroupListViewGridRowComponent,
      GroupListViewGridTitleComponent,
      GroupsMainComponent,
      EditGroupComponent,
      ReadonlyPermissionViewComponent,
      MembersHomeComponent,
    ],
    entryComponents: [
        AddUpdateMemberComponent,
        AddUpdateBasicDetailsComponent,
        PermissionDailogComponent
    ]
})
export class MembersModule { }
