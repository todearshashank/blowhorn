import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MemberService } from '../../shared/services/member.service';
import { WatcherService } from './../../shared/services/watcher.service';

@Component({
    selector: 'app-member-main',
    templateUrl: './member-main.component.html',
    styleUrls: ['./member-main.component.scss']
})
export class MemberMainComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private _member_service: MemberService,
    ) { }

    ngOnInit() {
    }

}
