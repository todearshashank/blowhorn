import { Group } from './../../shared/models/member.model';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-group-list-view-grid-row',
  templateUrl: './group-list-view-grid-row.component.html',
  styleUrls: ['./group-list-view-grid-row.component.scss']
})
export class GroupListViewGridRowComponent implements OnInit {

    @Input()
    group: Group;

    @Output() edit_group_clicked: EventEmitter<void>;


  constructor() {
    this.edit_group_clicked = new EventEmitter<void>();
  }

  ngOnInit() {
  }

  showGroupEdit() {
    this.edit_group_clicked.emit();
  }

}
