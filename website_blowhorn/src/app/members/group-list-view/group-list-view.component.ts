import { BlowhornService } from './../../shared/services/blowhorn.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Group, SupportData } from '../../shared/models/member.model';
import { MemberService } from '../../shared/services/member.service';

@Component({
    selector: 'app-group-list-view',
    templateUrl: './group-list-view.component.html',
    styleUrls: ['./group-list-view.component.scss']
})
export class GroupListViewComponent implements OnInit {

    groups: Group[] = []
    master_data: Group[] = [];
    supportData: SupportData;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private blowhorn_service: BlowhornService,
        private _member_service: MemberService,
    ) {
        this._member_service.loadSupportData()
        route.data.subscribe(data => {
            if (data.groups) {
                this.update_groups(data.groups);
            }
            this.blowhorn_service.hide_loader();
            console.log('data', data.groups);
        },
        err => {
            console.error(err);
            this.blowhorn_service.hide_loader();
        });
    }

    ngOnInit() {
        this._member_service.support_data_store.subscribe((resp: any) => {
            this.supportData = resp
        })
    }

    update_groups(data: Group[]) {
        this.groups = data;
        this.master_data = data;
        if (this.supportData) {
            this.supportData.groups = data
            this._member_service.update_support_data_store(this.supportData)
        }
    }

    match_filter(item: any, event: any) {
        return event.term ? item.name.toLowerCase().includes(event.term.toLowerCase()) : true
    }

    filter_changed(event: { term: string, role: string, status: string }) {
        this.groups = [];
        this.master_data.forEach((item: any) => {
            if (event) {
                if (this.match_filter(item, event)) {
                    this.groups.push(item);
                }
            } else {
                this.groups = this.master_data;
            }
        });
    }

    showEditGroup(group: Group) {
        this.router.navigate(['dashboard', 'members', 'roles', group.id]);
    }

}
