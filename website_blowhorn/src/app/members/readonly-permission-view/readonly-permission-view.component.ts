import { Group } from './../../shared/models/member.model';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Module, System } from '../../shared/models/member.model';
import { MatAccordion } from '@angular/material';

@Component({
  selector: 'app-readonly-permission-view',
  templateUrl: './readonly-permission-view.component.html',
  styleUrls: ['./readonly-permission-view.component.scss']
})
export class ReadonlyPermissionViewComponent implements OnInit {

    @ViewChild('accordion', {static: false}) accordion: MatAccordion;
    @Input() systems: System[] = [];
    @Input() modules: Module[] = [];
    @Input() permissions = [];
    @Input() permission_levels = [];
    @Input() groups: Group[] = [];
    @Input() master_data = {
        groups: [],
        permissions: []
    }

    displayData = [];

  constructor() { }

  ngOnInit() {
    this.displayData = [];
    this.systems.forEach(system => {
        let systemData = {
            name: system.name,
            modules: []
        };
        for (let i = 0; i < this.modules.length; i++) {
            let module = this.modules[i];
            if (module.system === system.id) {
                let moduleData = {
                    name: module.name,
                    permission_levels: this.permission_levels.filter(item => item.module_id === module.id),
                    permissions: this.permissions.filter(perm => perm.module === module.id)
                };
                systemData.modules.push(moduleData)
            }
        }
        this.displayData.push(systemData)
    })
  }

  ngAfterViewInit() {
    this.accordion.openAll();
  }

}
