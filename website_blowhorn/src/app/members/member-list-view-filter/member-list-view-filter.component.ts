import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { MemberService } from '../../shared/services/member.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-member-list-view-filter',
  templateUrl: './member-list-view-filter.component.html',
  styleUrls: ['./member-list-view-filter.component.scss']
})
export class MemberListViewFilterComponent implements OnInit {

    @Input() total_count = 0;
    @Output() filter_changed: EventEmitter<{term: string, role: string, status: string}>;
    @Output() add_member_clicked: EventEmitter<void>;
    @Output() view_changed: EventEmitter<string>;
    private searchDecouncer$: Subject<string> = new Subject();

    currentView: string = 'list';
    filter_value = '';
    add_button = {
        label: 'Add Member'
    };
    roles = [];
    statuses = [{
        id: 1,
        name: 'invited'
    }, {
        id: 2,
        name: 'active'
    }, {
        id: 3,
        name: 'inactive'
    }];
    default_role_value = 'All Roles'
    selected_role_name = '';
    selected_role_id = '';

    default_status_value = 'All Statuses'
    selected_status_name = '';
    selected_status_id = '';

    constructor(
        public memberService: MemberService
    ) {
        this.filter_changed = new EventEmitter<{term: string, role: string, status: string}>();
        this.add_member_clicked = new EventEmitter<void>();
        this.view_changed = new EventEmitter<string>();
        this.memberService.support_data_store.subscribe((resp: any) => {
            if (!resp.groups.length) {
                return
            }
            this.roles = resp.groups;
        })
    }

    ngOnInit() {
        this.changeView('list');
        this.setupSearchDebouncer();
    }

    filter_value_changed(event: any) {
        const emitVal = {
            term: this.filter_value,
            role: this.selected_role_id,
            status: this.selected_status_name
        };
        console.log({emitVal})
        this.filter_changed.emit(emitVal);
    }

    private setupSearchDebouncer(): void {
        this.searchDecouncer$.pipe(
          debounceTime(700),
          distinctUntilChanged(),
        ).subscribe((term: string) => {
            console.log({term});
            this.filter_value_changed(term);
        });
    }

    onSearchInputChange(term: string): void {
        this.searchDecouncer$.next(term);
    }

    add_member() {
        this.add_member_clicked.emit();
    }

    role_changed(event: any): void {
        if (this.selected_role_id !== event.id) {
            this.selected_role_name = event.name !== this.default_role_value ? event.name : null ;
            this.selected_role_id = event.id;
        }
        this.filter_value_changed(null);
    }

    status_changed(event: any): void {
        if (this.selected_status_id !== event.id) {
            this.selected_status_name = event.name !== this.default_status_value ? event.name : null ;
            this.selected_status_id = event.id;
        }
        this.filter_value_changed(null);
    }

    changeView(view: string) {
        if (this.currentView !== view) {
            this.currentView = view;
            this.view_changed.emit(view);
        }
    }

    get dropdown_styles() {
        return {
            'dropdown-styles': {
                'box-shadow': 'none',
                'border-radius': '4px',
                border: '1px solid #d8d8d8',
                height: '40px',
                width: '200px',
                padding: '0 8px'
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px',
            },
            'dropdown-menu-styles': {
                width: '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

}
