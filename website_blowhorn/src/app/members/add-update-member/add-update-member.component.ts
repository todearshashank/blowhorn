import { Component, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Member } from '../../shared/models/member.model';
import { MemberService } from './../../shared/services/member.service';
import { BlowhornService } from '../../shared/services/blowhorn.service';
import { Constants } from '../../shared/utils/constants';


@Component({
  selector: 'app-add-update-member',
  templateUrl: './add-update-member.component.html',
  styleUrls: ['./add-update-member.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AddUpdateMemberComponent implements OnInit {

    @Output()
    emitService: EventEmitter<any>;

    member: Member;
    operation = null;
    header_text = '';
    user_details = {
        'name': '',
        'email': ''
    };
    current_member = {
        name: '',
        email: '',
        phone_number: '',
        is_admin: false,
        is_active: false
    };
    submit_button = {
        type: 'save',
        label: '',
        disabled: !this.member_service.has_invite_permission
    };
    dismiss_button = {
        label: 'Cancel',
        type: 'delete',
        disabled: false
    };
    alert = {
        type: '',
        message: '',
        show_message: false
    };
    allowToChangeAdminStatus = false;
    inputRegex = Constants.regex;

    constructor(
        public modalService: NgbModal,
        public activeModal: NgbActiveModal,
        public member_service: MemberService,
        public blowhorn_service: BlowhornService,
    ) {
        this.emitService  = new EventEmitter<any>();
        this.header_text = '';
        this.user_details = this.blowhorn_service.user_details;
    }

    ngOnInit() {
        this.current_member = Object.assign({}, this.member);
        this.submit_button.label = this.operation;
        this.header_text = (this.operation === 'Invite') ? 'invite member' : 'update member details';
        this.allowToChangeAdminStatus = this.current_member.email !== this.user_details.email && this.member_service.has_invite_permission;
    }

    show_alert(type: string, message: string): void {
        this.alert.type = type;
        this.alert.message = message;
        this.alert.show_message = true;
    }

    invite_member(): any {
        this.member_service.invite_member(this.current_member)
        .subscribe((resp: any) => {
            this.show_alert('success', resp.message);
            this.emitService.next(resp);
            this.submit_button.disabled = false;
        },
        err => {
            this.submit_button.disabled = false;
            if (err) {
                this.show_alert('error', err.error.message || err.error.detail);
            }
            console.error('Error', err);
        });
    }

    update_member(): any {
        this.member_service.update_member(this.current_member)
        .subscribe((resp: any) => {
            this.show_alert('success', resp.message);
            this.emitService.next(Object.assign({}, this.current_member));
            this.submit_button.disabled = false;
        },
        err => {
            this.submit_button.disabled = false;
            let errors = err.error.message || err.error.detail;
            if (!errors) {
                errors = err.error.email.join('\n');
            }
            if (err) {
                this.show_alert('error', errors);
            }
            console.error('Error', err);
        });
    }

    onSubmit(): void {
        this.submit_button.disabled = true;
        if (this.operation === 'Invite') {
            this.invite_member();
        } else {
            this.update_member();
        }
    }

}
