import { PermissionDailogComponent } from './../permission-dailog/permission-dailog.component';
import { Member } from './../../shared/models/member.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MemberService } from '../../shared/services/member.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddUpdateMemberComponent } from '../add-update-member/add-update-member.component';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { AddUpdateBasicDetailsComponent } from '../add-update-basic-details/add-update-basic-details.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

    @Input() member: Member;
    @Output() show_message: EventEmitter<{ type: string, message: string }>;

    resp_msg = '';
    show = {
        invite_loader: false,
    };
    min_loader_styles = {
        wrapper: {
            width: '50px',
            position: 'absolute',
            left: '25%',
            top: '70%'
        },
        ball1: {
            height: '10px',
            width: '10px',
        },
        ball2: {
            height: '10px',
            width: '10px',
        },
        ball3: {
            height: '10px',
            width: '10px',
        },
    };
    avatarUrl = '';
    avatarColors = ['var(--teal)', '#8a2be2', '#9acd32', '#40e0d0', '#ff6347', '#6495ed', '#008b8b', '#09015f', '#312c51', '#2c061f'];
    avatarColor = '#8a2be2';
    statusColors = {
        invited: 'gray',
        active: 'green',
        inactive: 'red'
    }
    avatarStore = {
        male: ['https://i.imgur.com/mKjcX04.png', 'https://i.imgur.com/H98v1M0.jpg', 'https://i.imgur.com/5PXYSj9.png'],
        female: ['https://i.imgur.com/tDaWKIr.jpg', 'https://i.imgur.com/lwNZCEh.png']
    }

    constructor(
        private _bottomSheet: MatBottomSheet,
        private router: Router,
        public member_service: MemberService,
    ) {
        this.show_message = new EventEmitter<{ type: string, message: string }>();
    }

    ngOnInit() {
        this.avatarUrl = this.member.name.split(' ').reduce((accumulator, item) => accumulator + item[0], '')
        this.avatarColor = this.avatarColors[Math.floor(Math.random() * this.avatarColors.length)];
    }

    open_member_modal(): void {
        const bottomSheetRef = this._bottomSheet.open(AddUpdateBasicDetailsComponent, {
            data: {
                member: this.member,
                operation: 'Update'
            }
        });
        bottomSheetRef.afterDismissed().subscribe((result) => {
            console.log('Bottom sheet has been dismissed.', result);
            if (result) {
                this.member = result;
            }
        });
    }

    open_permission_modal(): void {
        // const bottomSheetRef = this._bottomSheet.open(PermissionDailogComponent, {
        //     panelClass: 'full-width-bottomsheet',
        //     data: {
        //         member: this.member
        //     }
        // });
        // bottomSheetRef.afterDismissed().subscribe((result) => {
        //     console.log('Bottom sheet has been dismissed.', result);
        //     if (result) {
        //         this.member = result;
        //     }
        // });
        this.router.navigate(['dashboard', 'members', 'listview', this.member.id, 'permissions']);
    }

    resend_invite_link(): void {
        this.show.invite_loader = true;
        this.member_service.reinvite_member({
            email: this.member.email
        }).subscribe(resp => {
            this.show_message.emit({ type: 'Success', message: resp.message });
            this.show.invite_loader = false;
            this.member.is_invite_link_expired = false;
        }, resp => {
            console.error(resp);
            this.show_message.emit({ type: 'Failed', message: resp.error.message });
            this.show.invite_loader = false;
        });
    }

}
