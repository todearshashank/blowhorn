import { MemberService } from './../../shared/services/member.service';
import { BlowhornService } from './../../shared/services/blowhorn.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Member, Module, Permission, System } from '../../shared/models/member.model';
import { transition, trigger, useAnimation } from '@angular/animations';
import { transAnimation } from '../../animations';
@Component({
    selector: 'app-permissions',
    templateUrl: './permissions.component.html',
    styleUrls: ['./permissions.component.scss'],
    animations: [
        trigger('openClose', [
            transition('open => closed', [
                useAnimation(transAnimation, {
                    params: {
                        height: 0,
                        opacity: 1,
                        backgroundColor: 'red',
                        time: '1s'
                    }
                })
            ])
        ])
    ],
})
export class PermissionsComponent implements OnInit {

    @Input() master_permissions: Permission[] = [];
    @Input() modules: Module[] = [];
    @Input() systems: System[] = [];
    @Input() permission_levels = [];
    @Input() view_mode: boolean = false;
    @Input() user_stored_data = {
        permission_levels: []
    };
    @Output() permissions_selected: EventEmitter<any>;

    permissions: Permission[] = [];
    selectedSystem = {} as any;
    selectedModule = {} as any;
    displayModules = [];
    dropdownSettings = {
        singleSelection: false,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'Unselect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
    };
    globalDropdownSettings = {
        singleSelection: false,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'Unselect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        showSelectAll: false
    };

    permissionLevels: any[] = [];
    globalPermissionLevels: any[] = [];
    permissionLevelMasterStore = {};
    loading = {
        permissionLevels: false
    }
    formData = []

    constructor(
        private _bh_service: BlowhornService,
        private _member_service: MemberService
    ) {
        this.permissions_selected = new EventEmitter<any>();
    }

    ngOnInit() {
        this.permissions = this._bh_service.copy_array(this.master_permissions);
        if (this.systems.length) {
            this.selectSystem(this.systems[0])
        }
    }

    selectSystem(val: any) {
        this.selectedSystem = val;
        this.displayModules = this.modules.filter(i => i.system === val.id)
        this.loading.permissionLevels = true;
        this._member_service.get_member_permission_level({
            system_id: val.id
        }).subscribe((resp: any) => {
            for (let i = 0; i < this.displayModules.length; i++) {
                let currentModule = this.displayModules[i]
                currentModule.permissions = this.permissions.filter(item => item.module === this.displayModules[i].id).sort();
                currentModule.all = currentModule.permissions.length != null && currentModule.permissions.every(t => t.active);
                currentModule.selectedPermCount = currentModule.permissions.filter(t => t.active).length
                this.globalPermissionLevels = this.permission_levels.filter(item => item.system === val.id);
                currentModule.permissionLevels = this.globalPermissionLevels.map(item => {
                    let stored_level = this.user_stored_data.permission_levels.find(i =>
                        i.module_id === currentModule.id && i.permission_level_id === item.id)
                    let options = resp[item.name];
                    item.options = options
                    item.selectedOptions = []
                    let selectedOptions = [];
                    if (stored_level && stored_level.value) {
                      selectedOptions = options.filter((el) => {
                        return stored_level.value.findIndex(o => o.id === el.id) > -1;
                      });
                    }
                    return {
                        id: item.id,
                        name: item.name,
                        options: options,
                        selectedOptions: selectedOptions
                    }
                })
            }
            if (this.displayModules.length) {
                this.selectModule(this.displayModules[0], null)
                this.emitData();
            }
            this.loading.permissionLevels = false;
        }, err => {
            console.log({err});
            this.loading.permissionLevels = false;
        })
    }

    selectModule(module: any, val: boolean) {
        this.selectedModule = !this.selectedModule || this.selectedModule !== module.name ? module : null;
    }

    permChanged(module: Module) {
        module.all = module.permissions.length != null && module.permissions.every(t => t.active);
        this.emitData();
    }

    selectAllPermissionsForModule(event: any, module: Module) {
        module.all = event.checked
        module.permissions.forEach(elem => elem.active = event.checked);
        this.emitData();
    }

    emitData() {
        this.formData = this.displayModules;
        this.permissions_selected.emit(this.formData);        
    }

    permissionLevelChanged(event: any) {
        this.emitData();
    }

    someComplete(module: any): boolean {
        if (!module.permissions.length) {
          return false;
        }
        module.selectedPermCount = module.permissions.filter(t => t.active).length
        return module.selectedPermCount > 0 && !module.all;
    }

    removePermRemoved(permLevel: any, item: any) {
        let index = permLevel.selectedOptions.findIndex((i: any) => i.id === item.id)
        permLevel.selectedOptions.splice(index, 1)
        this.globalPermissionLevelChanged(permLevel)
    }

    globalPermissionLevelChanged(event: any) {
        for (let i = 0; i < this.displayModules.length; i++) {
            let item = this.displayModules[i].permissionLevels.find((permLevel: any) => permLevel.id === event.id)
            item.selectedOptions = event.selectedOptions;
        }
        this.emitData();
    }

    get permissionLevelStyles(): {} {
        return {
          'dropdown-styles': {
            border: '1px solid #eee',
            height: '40px',
            width: '200px',
            margin: '0 10px 0 0',
            'box-shadow': 'none',
            'padding-left': '5px',
            'z-index': 2
          },
          'dropdown-toggle-styles': {
            'padding-left': '0px'
          },
          'dropdown-menu-styles': {
            marginTop: '3px',
            width: '200px',
            boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
          }
        };
    }

}
