import { BlowhornService } from './../../shared/services/blowhorn.service';
import { MemberService } from './../../shared/services/member.service';
import { Component, Inject, OnInit } from '@angular/core';
import { Member, Group, Module, Permission, System } from '../../shared/models/member.model';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-permission-dailog',
    templateUrl: './permission-dailog.component.html',
    styleUrls: ['./permission-dailog.component.scss']
})
export class PermissionDailogComponent implements OnInit {

    screens = {
        permission: 'permission',
        group: 'group',
    }
    currentScreen = this.screens.permission;
    headerText = '';
    groupName = '';
    showLoader = false;
    member: Member;
    modules: Module[] = [];
    permission_levels = [];
    permissions: Permission[] = [];
    systems: System[] = [];
    groups = {
        name: 'Groups',
        options: [],
        selectedOptions: []
    }

    // Temp data from form
    formData: any = {};

    selectedSystem = {} as any;
    selectedModule = {} as any;
    displayModules = [];
    displayPermissions = [];
    selectAll = {};
    submitButton = {
        type: 'save',
        label: 'Save',
        disabled: false
    };
    dismissButton = {
        label: 'Cancel',
        type: 'delete',
        disabled: false
    };
    addGroupBtn = {
        label: 'Create Group',
        type: 'save',
        disabled: false
    };
    dropdownSettings = {
        singleSelection: false,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 2,
        allowSearchFilter: true
    };
    submitting = false;
    loading = true;
    view_mode = false;
    userId = null;
    userStoredData = {
        user_permissions: [],
        permission_levels: [],
        groups: [],
    };
    multiselectActionButtons = {
        select: {
            label: '⇨',
            type: '',
            disabled: false,
            title: 'Add selected drivers to resource'
        },
        deselect: {
            label: '⇦',
            type: '',
            disabled: false,
            title: 'Remove selected drivers from resource'
        }
    };
    availableGroups = [];
    selectedGroups = [];
    masterDataCounts = {
        groups: [],
        permissions: [],
    }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _bh_service: BlowhornService,
        private _member_service: MemberService,
        private toastr: ToastrService,
    ) {
        this._bh_service.show_loader()
        this._member_service.loadSupportData()
        this.userId = route.snapshot.params.id;
        route.data
        .subscribe(data => {
            this._bh_service.hide_loader();
            this.userStoredData = data.permissions;
        }, (err: any) => {
            console.error(err);
        });
    }

    ngOnInit(): void {
        this._member_service.support_data_store.subscribe((resp: any) => {
            if (resp.systems.length) {
                this.systems = resp.systems.sort();
                this.modules = resp.modules;
                this.permission_levels = resp.permission_levels.sort((a: { position: number; }, b: { position: number; }) => a.position - b.position);
                this.permissions = resp.permissions;
                this.availableGroups = resp.groups.map((grp: { id: any; name: any; }) => {
                    return {
                        id: grp.id,
                        name: grp.name,
                        selected: this.userStoredData.groups.findIndex(g => g.id === grp.id) > -1
                    }
                });
                this.selectedGroups = this.userStoredData.groups
                this.permissions.forEach(perm => {
                    perm.active = this.userStoredData.user_permissions.findIndex(p => p.id === perm.id && p.module === perm.module) > -1
                });
                this.masterDataCounts.groups = resp.groups;
                this.masterDataCounts.permissions = resp.permissions;   
            }
        });
    }

    toggleViewMode() {
        this.view_mode = !this.view_mode;
    }

    dismiss(event: any) {
        this.router.navigate(["dashboard", "members", "listview"]);
    }

    groupsChanged(event: any) {
        this.selectedGroups = event;
    }

    selectedPermissions(event: any) {
        this.formData.modules = event;
    }

    openRolePage(event: any) {
        this.router.navigate(['dashboard', 'members', 'roles', 'add']);
    }

    saveForm(event: any) {
        this.submitting = true;
        let selectedPermissions = [];
        let selectedPermissionLevels = [];
        let modules = this.formData.modules || [];
        console.log( this.formData.modules);
        
        for (let i = 0; i < modules.length; i++) {
            let module = modules[i];
            selectedPermissions = !module.all
                ? selectedPermissions.concat(module.permissions.filter((perm: Permission) => perm.active === true))
                : selectedPermissions.concat(module.permissions);

            let permissionLevels = module.permissionLevels;
            let tmpPermLevel = []
            for (let j = 0; j < permissionLevels.length; j++) {
                let permLevel = permissionLevels[j];
                if (permLevel.selectedOptions.length) {
                    tmpPermLevel.push({
                        permission_level_id: permLevel.id,
                        module_id: module.id,
                        value: permLevel.selectedOptions.map((level: { id: number; }) => level.id)
                    });
                }
            }
            selectedPermissionLevels = selectedPermissionLevels.concat(tmpPermLevel)
        }
        let formPermissions = selectedPermissions.map((i: Permission) => i.id) || []
        let formPermissionLevels = selectedPermissionLevels || []
        let formGroups = this.selectedGroups.length ? this.selectedGroups.map(i => i.id) : []
        this._member_service.update_user_permissions(this.userId, {
            permissions: formPermissions,
            levels: formPermissionLevels,
            groups: formGroups
        }).subscribe((resp) => {
            this.toastr.success(resp);
            this.submitting = false;
            this.dismiss(null);
        }, err => {
            this.submitting = false;
            console.error(err);
            let errorMessage = typeof (err.error) === 'string' ? err.error : err.error.detail;
            this.toastr.error(errorMessage);
        })
    }

}
