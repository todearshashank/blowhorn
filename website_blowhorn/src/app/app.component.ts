import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { WatcherService } from './shared/services/watcher.service';
import { BlowhornService } from './shared/services/blowhorn.service';
import { MapService } from './shared/services/map.service';
import { Router, NavigationEnd } from '@angular/router';
import { Constants } from './shared/utils/constants';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'app';
    show = false;
    @ViewChild('modal_container', { read: ViewContainerRef, static: false })
    modal_container: ViewContainerRef;
    includeWhatsNew = false;

    public app_title = Constants.version_info.dashboard.app_title;
    public app_code: string = Constants.version_info.dashboard.app_code;
    public version: string = Constants.version_info.dashboard.version;
    public whats_new: any = Constants.version_info.dashboard.whats_new;

    constructor(
        private watcher_service: WatcherService,
        public _blowhorn_service: BlowhornService,
        private _map_service: MapService,
        private _router: Router
    ) {
        this._map_service.load_google_maps_api();
        this._router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                if (!this._blowhorn_service.is_staff
                    && this._blowhorn_service.is_non_individual
                    && !this._blowhorn_service.hasPermissionByUrl(event.urlAfterRedirects)
                ) {
                    console.log(event, event.urlAfterRedirects);
                    this._blowhorn_service.first_page_permission_obs.subscribe(event => {
                        if (event) {
                            this._router.navigateByUrl(event.url);
                        }
                    })
                }

                // Google Analytics
                try {
                    (<any>window).ga('set', 'page', event.urlAfterRedirects);
                    (<any>window).ga('send', 'pageview');
                } catch {
                    console.log('`ga` not available');
                }
            }
        });
    }

    ngOnInit() {
        if (this._blowhorn_service.is_authenticated) {
            this._blowhorn_service.load_user_profile();
        }
        this._blowhorn_service.setup_initial_data();
    }

    ngAfterViewInit() {
        this._blowhorn_service.modal_container = this.modal_container;
    }
}
