import {
    Component, OnInit, ViewChild, ElementRef, NgZone, OnChanges, SimpleChanges
  } from '@angular/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { FlashSaleService } from '../../../shared/services/flash-sale.service';
import { RouteStop } from '../../../shared/models/route-stop.model';
import { NgModel, NgForm } from '@angular/forms';
import { MapService } from '../../../shared/services/map.service';
import { Trip } from '../../../shared/models/trip.model';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../../shared/utils/constants';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

declare const Razorpay: any;
@Component({
    selector: 'app-flash-sale-home',
    templateUrl: './flash-sale-home.component.html',
    styleUrls: ['./flash-sale-home.component.scss']
})
export class FlashSaleHomeComponent implements OnInit, OnChanges {

    @ViewChild('search', {static: false})
    search_place_ref: ElementRef;
    @ViewChild('search_model', {static: false})
    search_model: NgModel;

    HOME_VIEW = 'home_view';
    FORM_VIEW = 'form_view';
    RESPONSE_VIEW = 'response_view';
    DEFAULT_CITY = 'Bengaluru';
    delivery_address_error_message = '';
    currentScreen = this.HOME_VIEW;
    icon_wrapper_styles = {
        'box-shadow': '0 0 16px 0 rgba(0, 0, 0, 0.3)',
        width: '100%',
        height: '60px',
        border: 'none',
    };
    payment_icon_styles = {
        height: '35px'
    };
    button = {
        home: {
            label: 'BOOK YOUR BOUQUET',
            type: 'delete'
        },
        continue: {
            label: 'CONTINUE',
            type: 'delete',
            flat_btn: true,
            icon: {
                class: 'fa fa-angle-right'
            },
            icon_styles: {
                wrapper: {
                    display: 'flex',
                    background: '#fff',
                    color: '#EB7A7A',
                    'flex-direction': 'row-reverse',

                },
                label: {

                },
                icon_holder: {
                    color: '#fff',
                    height: '36px',
                    width: '36px',
                    'background-color': '#EB7A7A',
                    'border-radius': '50%',
                    'text-align': 'center',
                    'line-height': '36px',
                    'vertical-align': 'middle',
                    'margin-left': '10px',
                    'padding-left': '4px',
                },
                icon: {
                    'font-size': '36px'
                }
            }
        },
        back: {
            label: 'BACK',
            type: 'delete',
            flat_btn: true,
            icon: {
                class: 'fa fa-angle-left'
            },
            icon_styles: {
                wrapper: {
                    display: 'flex',
                    background: '#fff',
                    color: '#EB7A7A',
                    'flex-direction': 'row-reverse',

                },
                label: {

                },
                icon_holder: {
                    color: '#fff',
                    height: '36px',
                    width: '36px',
                    'background-color': '#EB7A7A',
                    'border-radius': '50%',
                    'text-align': 'center',
                    'line-height': '36px',
                    'vertical-align': 'middle',
                    'margin-left': '10px',
                    'padding-right': '4px',
                },
                icon: {
                    'font-size': '36px'
                }
            }
        },
        paytm: {
            label: '',
            type: 'default',
            flat_btn: false,
            icon: {
                src: `/static/${this.blowhorn_service.current_build}/assets/Assets/paytm_logo_alt.png`
            },
            icon_styles: {
                wrapper: this.icon_wrapper_styles,
                icon: this.payment_icon_styles,
            },
        },
        razorpay: {
            label: '',
            type: 'default',
            flat_btn: false,
            icon: {
                src: `/static/${this.blowhorn_service.current_build}/assets/Assets/razorpay_brand_logo.svg`
            },
            icon_styles: {
                wrapper: this.icon_wrapper_styles,
                icon: this.payment_icon_styles,
            },
        }
    };
    gallary = [
        `/static/${this.blowhorn_service.current_build}/assets/images/val_img_1.jpg`,
        `/static/${this.blowhorn_service.current_build}/assets/images/val_img_2.jpg`,
        `/static/${this.blowhorn_service.current_build}/assets/images/val_img_3.jpg`,
        `/static/${this.blowhorn_service.current_build}/assets/images/val_img_4.jpg`,
    ];
    selected_image_url = this.gallary[0];
    available_timeslots = [
        // {id: 10, name: '10AM-12PM'},
        // {id: 12, name: '12PM-2PM'},
        {id: 14, name: '2PM-4PM'},
        {id: 16, name: '4PM-6PM'},
    ];
    delivery_timeslot: string;
    delivery_timeslot_id: number;
    slot_msg = '';

    // form fields
    customer_name: string;
    customer_email: string;
    customer_number: string;
    deliver_to: string;
    deliver_number: string;
    shipping_address: string;
    requested_timeslot: string;
    delivery_address: RouteStop = new RouteStop(null, 1);
    autocomplete: google.maps.places.Autocomplete;
    order_id: number;
    order_number: string;
    trip: Trip = new Trip();
    show_payment_window = false;
    message = {
        type: '',
        header: '',
        content: '',
        show: false,
    };

    constructor(
        private route: ActivatedRoute,
        private _ngZone: NgZone,
        private _http_client: HttpClient,
        private _map_service: MapService,
        public blowhorn_service: BlowhornService,
        private flash_sale_service: FlashSaleService,
    ) {
        route.data.subscribe(data => {
            this.blowhorn_service.hide_loader();
        },
        err => {
            console.error(err);
        });
    }

    ngOnInit() {
        this.currentScreen = 'closed';
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    timeslot_changed(event: any) {
        let currentHour = new Date().getHours();
        console.log('Selected timeslot: ', currentHour, event.id);
        if (!event.id) {
            this.slot_msg = '';
            this.delivery_timeslot = 'Select timeslot for delivery';
            this.delivery_timeslot_id = null;
            this.requested_timeslot = null;
        } else if (currentHour > event.id && currentHour >= event.id + 2) {
            this.delivery_timeslot = 'Select timeslot for delivery';
            this.delivery_timeslot_id = null;
            this.requested_timeslot = null;
            this.slot_msg = 'This slot is closed. Please, choose different.';
        } else {
            this.slot_msg = '';
            this.delivery_timeslot_id = event.id;
            this.requested_timeslot = '14-Feb-2018 ' + event.id + ':00';
        }
    }

    openBookingForm() {
        this.currentScreen = this.FORM_VIEW;
    }

    openPaymentOptions() {
        this.blowhorn_service.show_loader();
        const data = {
            'customer_name': this.customer_name,
            'customer_email': this.customer_email,
            'customer_number': this.customer_number,
            'shipping_address': {
                'first_name': this.deliver_to,
                'phone_number': this.deliver_number,
                'line2': '',
                'line3': '',
                'line4': '',
                // 'postcode': ,
                'line1': this.shipping_address,
                'geopoint': {},
            },
            'requested_timeslot': this.requested_timeslot,
        };

        if (this.trip.order_key) {
            data['number'] = this.trip.order_key;
            this.flash_sale_service.update_order(data).subscribe(resp => {
                this.blowhorn_service.hide_loader();
                this.show_payment_window = true;
            },
            err => {
                    console.log(err);
                    this.blowhorn_service.hide_loader();
                    this.showMessageModal(
                        'error',
                        'Failed',
                        err.error
                    );
                }
            );
        } else {
            this.flash_sale_service.create_order(data).subscribe(resp => {
                this.blowhorn_service.hide_loader();
                this.trip.order_real_id = resp['pk'];
                this.trip.order_key = resp['number'];
                this.trip.total_payable = resp['total_payable'];
                this.show_payment_window = true;
            },
            err => {
                    console.error('err', err);
                    this.blowhorn_service.hide_loader();
                    this.showMessageModal(
                        'error',
                        'Failed',
                        err.error
                    );
                }
            );
        }
    }

    proceedToPaytm() {
        window.location.href = `api/paytm/?entity_type=order&entity_id=${this.trip.order_real_id}&next=/vdaysale/message/${this.trip.order_key}`;
    }

    proceedToRazorpay() {
        const amount: any = (this.trip.total_payable * 100).toString();
        const trip = this.trip;
        const _http_client = this._http_client;
        const options = {
            'key': environment.razorpay_api_key,
            'amount': amount,
            'name': 'blowhorn',
            'description': 'Logistic Services',
            'image': this.blowhorn_service.logo_url,
            'prefill': {
                'name': this.customer_name,
                'email': this.customer_email,
                'contact': this.customer_number,
            },
            'notes' : {
                'booking_id' : trip.order_id,
                'booking_key' : trip.order_key,
                'type' : 'booking',
            },

            'handler': function (response: any) {
                const data: any = {
                    'booking_key': trip.order_key,
                    'txn_id': response.razorpay_payment_id,
                    'amount': amount,
                    'type': 'booking'
                };
                _http_client
                    .post(Constants.urls.API_RAZORPAY, data)
                    .subscribe(res => {
                        window.open(`/vdaysale/message/${trip.order_key}`, '_self');
                    });
            },
        };

        const razorpay = new Razorpay(options);
        razorpay.open();
    }

    showMessageModal(messageType: string, messageHeader: string, messageContent: string) {
        this.setMessageModalContent(true, messageType, messageHeader, messageContent);
    }

    closeMessageModal() {
        this.setMessageModalContent(false, '', '', '');
    }

    setMessageModalContent(show: boolean, messageType: string, messageHeader: string, messageContent: string) {
        this.message.show = show;
        this.message.type = messageType;
        this.message.header = messageHeader;
        this.message.content = messageContent;
    }

    get home_button_styles() {
        return {
            wrapper: {
                height: '48px',
                width: '312px',
                color: '#ffffff',
                border: 'none',
                'border-radius': '24px',
                'background-color': '#EB7A7A',
                'box-shadow': '0 2px 8px 0 rgba(0,0,0,0.32)',
                'font-weight': 'bold',
                'line-height': '19px',
            },
            label: {

            },
        };
    }

    get dropdown_styles() {
        return {
            'dropdown-styles': {
                boxShadow: 'none',
                border: 'none',
                backgroundColor: '#f7fbfc',
                color: '#757E7F',
                fontSize: '14px',
                fontWeight: 900,
                letterSpacing: '2px',
                lineHeight: '21px',
                textTransform: 'uppercase'
            },
            'dropdown-menu-styles': {
                width: '100%',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }
}
