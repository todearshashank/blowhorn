import { Component, OnInit } from '@angular/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { TripService } from '../../../shared/services/trip.service';
import { Trip } from '../../../shared/models/trip.model';

@Component({
  selector: 'app-flash-sale-message',
  templateUrl: './flash-sale-message.component.html',
  styleUrls: ['./flash-sale-message.component.scss']
})
export class FlashSaleMessageComponent implements OnInit {

    order_id = '';
    payment_status = '';
    hide_block = true;
    trip: Trip;

  constructor(
      public blowhorn_service: BlowhornService,
      private trip_service: TripService,
  ) { }

  ngOnInit() {
      this.blowhorn_service.show_loader();
      let url_parts = window.location.pathname.split('/');
      this.order_id = url_parts[url_parts.length - 1];
      this.trip_service.get_trip_new_details(this.order_id).subscribe(resp => {
        this.trip = resp;
        this.blowhorn_service.hide_loader();
        this.hide_block = false;
    },
    err => {
        console.log('error', err);
        this.hide_block = false;
        this.blowhorn_service.hide_loader();
    });
  }

}
