import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { FlashSaleHomeComponent } from './components/flash-sale-home/flash-sale-home.component';
import { FlashSaleMessageComponent } from './components/flash-sale-message/flash-sale-message.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [FlashSaleHomeComponent, FlashSaleMessageComponent]
})
export class FlashSaleModule { }
