import { PermissionDailogComponent } from './members/permission-dailog/permission-dailog.component';
import { MembersHomeComponent } from './members/members-home/members-home.component';
import { HeatmapsComponent } from './reports2/components/heatmaps/heatmaps.component';
import { ResourcePlanningListComponent } from './resource-planning/components/resource-planning-list/resource-planning-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyFleetMainComponent } from './my-fleet/components/my-fleet-main/my-fleet-main.component';
import { MyFleetHomeComponent } from './my-fleet/components/my-fleet-home/my-fleet-home.component';
import { MyTripsMainComponent } from './my-trips/components/my-trips-main/my-trips-main.component';
import { MyTripsMapViewComponent } from './my-trips/components/my-trips-map-view/my-trips-map-view.component';
import { MyTripsListViewComponent } from './my-trips/components/my-trips-list-view/my-trips-list-view.component';
import { ConfigurationMainComponent } from './configuration/components/configuration-main/configuration-main.component';
import { NotFound404Component } from './shared/components/not-found-404/not-found-404.component';
import { InvoiceMainComponent } from './invoice/components/invoice-main/invoice-main.component';
import { DepartureSheetMainComponent } from './departure-sheet/components/departure-sheet-main/departure-sheet-main.component';
import { DepartureSheetSearchComponent } from './admin/components/departure-sheet-search/departure-sheet-search.component';
import { TripFilterResolver } from './shared/resolvers/trip-filter.resolver';
import { ShipmentFilterResolver } from './shared/resolvers/shipment-filter.resolver';
import { InvoiceResolver } from './shared/resolvers/invoice.resolver';
import { DepartureSheetResolver } from './shared/resolvers/departure-sheet.resolver';
import { TripsResolver } from './shared/resolvers/trips.resolver';
import { ShipmentOrderMainComponent } from './shipment-order/components/shipment-order-main/shipment-order-main.component';
import { ShipmentOrderResolver } from './shared/resolvers/shipment-order.resolver';
import { ShipmentOrderListViewComponent } from './shipment-order/components/shipment-order-list-view/shipment-order-list-view.component';
import { RoutesMainComponent } from './route/components/routes-main/routes-main.component';
import { RouteOptimiserComponent } from './admin/components/route-optimiser/route-optimiser.component';
import { LiveTrackingMainComponent } from './admin/components/live-tracking/live-tracking-main/live-tracking-main.component';
import { LivetrackingResolver } from './shared/resolvers/livetracking.resolver';
import { CurrentDetailComponent } from './my-trips/components/current-detail/current-detail.component';
import { DetailResolver } from './shared/resolvers/details.resolver';
import { MytripsMainComponent } from './my-trips/components/mytrips-main/mytrips-main.component';
import { LocationsListComponent } from './location/components/locations-list/locations-list.component';
import { LocationInteractionComponent } from './location/components/location-interaction/location-interaction.component';
import { DetailLocationResolver } from './shared/resolvers/details-location.resolver';
import { AdminHeatmapComponent } from './admin/components/admin-heatmap/admin-heatmap.component';
import { OrderHeatmapResolver } from './shared/resolvers/order-heatmap.resolver';
import { AllCustomersResolver } from './shared/resolvers/all-customers.resolver';
import { ShipmentTemplateComponent } from './shared/components/shipment-template/shipment-template.component';
import { CurrentComponent } from './my-trips/components/current/current.component';
import { CurrentMyTripsResolver } from './shared/resolvers/current-my-trips.resolver';
import { PastComponent } from './my-trips/components/past/past.component';
import { CompletedMyTripsResolver } from './shared/resolvers/completed-my-trips.resolver';
import { UpcomingComponent } from './my-trips/components/upcoming/upcoming.component';
import { UpcomingMyTripsResolver } from './shared/resolvers/upcoming-my-trips.resolver';
import { ShipmentOrdersComponent } from './admin/components/shipment-orders/shipment-orders.component';
import { ShipmentOrderAssignComponent } from './admin/components/shipment-order-assign/shipment-order-assign.component';
import { UnassignedShipmentOrderResolver } from './shared/resolvers/unassigned-shipment-order.resolver';
import { OrderHeatmapComponent } from './admin/components/order-heatmap/order-heatmap.component';
import { DriverHeatmapComponent } from './admin/components/driver-heatmap/driver-heatmap.component';
import { DriverHeatmapResolver } from './shared/resolvers/driver-heatmap.resolver';
import { MemberMainComponent } from './members/member-main/member-main.component';
import { MemberListViewComponent } from './members/member-list-view/member-list-view.component';
import { MemberResolver } from './shared/resolvers/members.resolver';
import { KioskOrderComponent } from './admin/components/kiosk-order/kiosk-order.component';
import { OverviewComponent } from './fleet-overview/components/overview/overview.component';
import { ResourceAllocationMainComponent } from './admin/resource-allocation/allocation-main/allocation-main.component';
import { ResourceAllocationResolver } from './shared/resolvers/resource-allocation.resolver';
import { ResourceAllocationListViewComponent } from './admin/resource-allocation/allocation-list-view/allocation-list-view.component';
import { FleetOverviewAuthGuard } from './shared/gaurds/fleet-overview-auth.guard';
import { EnterpriseInvoiceListViewComponent } from './enterprise-invoice/invoice-list-view/invoice-list-view.component';
import { EnterpriseInvoiceResolver } from './shared/resolvers/enterprise-invoice.resolver';
import { EnterpriseInvoiceMainComponent } from './enterprise-invoice/invoice-main/invoice-main.component';
import { FlashSaleHomeComponent } from './flash-sale/components/flash-sale-home/flash-sale-home.component';
import { FlashSaleMessageComponent } from './flash-sale/components/flash-sale-message/flash-sale-message.component';
import { FlashSaleResolver } from './shared/resolvers/flash-sale.resolver';
import { FlashSaleMessageResolver } from './shared/resolvers/flash-sale-message.resolver';
import { Reports2MainComponent } from './reports2/components/reports2-main/reports2-main.component';
import { Reports2ChartsComponent } from './reports2/components/reports2-charts/reports2-charts.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ResourcePlanningResolver } from './shared/resolvers/resource-planning.resolver';
import { ResourcePlanningHomeComponent } from './resource-planning/components/resource-planning-home/resource-planning-home.component';
import { FleetRouteOptimiserComponent } from './shipment-order/components/fleet-route-optimiser/fleet-route-optimiser.component';
import { AwbGeneratorComponent } from './awb-generator/awb-generator.component';
import { TripBasedReportsComponent } from './reports2/components/trip-based-reports/trip-based-reports.component';
import { ReportConfigurationMainComponent } from './reports2/components/report-configuration-main/report-configuration-main.component';
import { AuthenticationGuard } from './shared/gaurds/authentication.guard';
import { ThirdPartyIntegrationComponent } from './third-party-integration/third-party-integration.component';
import { GroupListViewComponent } from './members/group-list-view/group-list-view.component';
import { GroupResolver } from './shared/resolvers/group.resolver';
import { GroupComponent } from './members/group/group.component';
import { GroupsMainComponent } from './members/groups-main/groups-main.component';
import { GroupDetailResolver } from './shared/resolvers/group-detail.resolver';
import { EditGroupComponent } from './members/edit-group/edit-group.component';
import { UserPermissionsResolver } from './shared/resolvers/user-permissions.resolver';
import { BankAccountsResolver } from './shared/resolvers/bank-accounts.resolver';
import { BankAccountListComponent } from './bank-accounts/components/bank-account-list/bank-account-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'feedback/:id',
    component: FeedbackComponent,
    resolve: {
      details: DetailResolver
    }
  },
  {
    path: 'track/:id',
    component: CurrentDetailComponent,
    resolve: {
      details: DetailResolver
    }
  },
  {
    path: 'livetrack/:id',
    component: CurrentDetailComponent,
    resolve: {
      details: DetailResolver
    }
  },
  { path: 'locations', component: LocationsListComponent },
  { path: 'locations/create', component: LocationInteractionComponent },
  {
    path: 'locations/:id', component: LocationInteractionComponent,
    resolve: {
      details: DetailLocationResolver
    }
  },
  {
    path: 'dashboard',
    component: MyFleetHomeComponent,
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: 'overview',
        component: MyFleetMainComponent,
        canActivate: [AuthenticationGuard],
        resolve: {
          trip_filter: TripFilterResolver
        }
      },
      {
        path: 'trips',
        component: MyTripsMainComponent,
        canActivate: [AuthenticationGuard],
        resolve: {
          trip_filter: TripFilterResolver
        },
        children: [
          {
            path: '',
            redirectTo: 'listview',
            pathMatch: 'full'
          },
          {
            path: 'listview',
            component: MyTripsListViewComponent,
            resolve: {
              trips: TripsResolver
            }
          },
          {
            path: 'mapview',
            component: MyTripsMapViewComponent
          }
        ]
      },
      {
        path: 'shipments',
        component: ShipmentOrderMainComponent,
        canActivate: [AuthenticationGuard],
        resolve: {
          shipment_filter: ShipmentFilterResolver
        },
        children: [
          {
            path: '',
            redirectTo: 'listview',
            pathMatch: 'full'
          },
          {
            path: 'listview',
            component: ShipmentOrderListViewComponent,
            resolve: {
              shipment_orders: ShipmentOrderResolver
            }
          },
          {
            path: 'route_optimiser ',
            component: FleetRouteOptimiserComponent,
            resolve: {
              shipment_orders: ShipmentOrderResolver
            }
          }
        ]
      },
      {
        path: 'enterpriseinvoice',
        component: EnterpriseInvoiceMainComponent,
        canActivate: [AuthenticationGuard],
        resolve: {
          trip_filter: TripFilterResolver
        },
        children: [
          {
            path: '',
            redirectTo: 'pending',
            pathMatch: 'full'
          },
          {
            path: 'pending',
            component: EnterpriseInvoiceListViewComponent,
            resolve: {
              invoice: EnterpriseInvoiceResolver
            }
          },
          {
            path: 'accepted',
            component: EnterpriseInvoiceListViewComponent,
            resolve: {
              invoice: EnterpriseInvoiceResolver
            }
          },
          {
            path: 'disputed',
            component: EnterpriseInvoiceListViewComponent,
            resolve: {
              invoice: EnterpriseInvoiceResolver
            }
          },
          {
            path: 'partially_paid',
            component: EnterpriseInvoiceListViewComponent,
            resolve: {
              invoice: EnterpriseInvoiceResolver
            }
          },
          {
            path: 'paid',
            component: EnterpriseInvoiceListViewComponent,
            resolve: {
              invoice: EnterpriseInvoiceResolver
            }
          }
        ]
      },
      {
        path: 'members',
        component: MembersHomeComponent,
        canActivate: [AuthenticationGuard],
        children: [
          {
            path: '',
            redirectTo: 'listview',
            pathMatch: 'full'
          },
          {
            path: 'listview',
            component: MemberMainComponent,
            children: [
              {
                path: '',
                component: MemberListViewComponent,
                resolve: {
                  members: MemberResolver
                },
              },
              {
                path: ':id/permissions',
                component: PermissionDailogComponent,
                resolve: {
                  permissions: UserPermissionsResolver
                }
              },
            ]
          },
          {
            path: 'roles',
            component: GroupsMainComponent,
            children: [
              {
                path: '',
                component: GroupListViewComponent,
                resolve: {
                  groups: GroupResolver
                },
              },
              {
                path: 'add',
                component: GroupComponent,
              },
              {
                path: ':id',
                component: EditGroupComponent,
                resolve: {
                  group: GroupDetailResolver
                }
              },
            ]
          }
        ]
      },
      {
        path: 'analytics',
        component: Reports2MainComponent,
        canActivate: [AuthenticationGuard],
        children: [{
          path: '',
          redirectTo: 'order',
          pathMatch: 'full'
        }, {
          path: 'order',
          component: Reports2ChartsComponent,
        },
        {
          path: 'trip',
          component: TripBasedReportsComponent,
        },
        {
          path: 'heatmaps',
          component: HeatmapsComponent,
        },
        {
          path: 'schedule',
          component: ReportConfigurationMainComponent
        }
        ]
      },
      {
        path: 'mytrips',
        component: MytripsMainComponent,
        canActivate: [AuthenticationGuard],
        children: [
          {
            path: '',
            redirectTo: 'current',
            pathMatch: 'full'
          },
          {
            path: 'current',
            component: CurrentComponent,
            resolve: {
              trips: CurrentMyTripsResolver
            }
          },
          {
            path: 'completed',
            component: PastComponent,
            resolve: {
              trips: CompletedMyTripsResolver
            }
          },
          {
            path: 'upcoming',
            component: UpcomingComponent,
            resolve: {
              trips: UpcomingMyTripsResolver
            }
          }
        ]
      },
      {
        path: 'configuration',
        component: ConfigurationMainComponent,
        children: [
          {
            path: '',
            redirectTo: 'fleet-overview',
            pathMatch: 'full',
          },
          {
            path: 'routes',
            component: RoutesMainComponent
          },
          {
            path: 'fleet-overview',
            component: OverviewComponent,
            canActivate: [FleetOverviewAuthGuard],
          },
          {
            path: 'awb-generator',
            component: AwbGeneratorComponent
          },
          {
            path: 'third-party-integration',
            component: ThirdPartyIntegrationComponent
          },
          {
            path: 'bank-accounts',
            component: BankAccountListComponent,
            resolve: {
                bankAccount: BankAccountsResolver
            }
          },
          {
            path: 'resource-planning',
            component: ResourcePlanningHomeComponent,
            resolve: {
              resource_planning: ResourcePlanningResolver
            },
            children: [{
              path: '',
              component: ResourcePlanningListComponent,
            }]
          },
        ]
      },
      {
        path: '**',
        component: NotFound404Component
      }
    ]
  },
  {
    path: 'invoice/:id',
    component: InvoiceMainComponent,
    resolve: {
      invoice: InvoiceResolver
    }
  },
  {
    path: 'vdaysale',
    component: FlashSaleHomeComponent,
    resolve: {
      flash_sale: FlashSaleResolver
    }
  },
  {
    path: 'vdaysale/message/:id',
    component: FlashSaleMessageComponent,
    resolve: {
      flash_sale_message: FlashSaleMessageResolver
    }
  },
  {
    path: 'admin/departuresheet',
    component: DepartureSheetSearchComponent
  },
  {
    path: 'admin/heatmap',
    component: AdminHeatmapComponent,
    children: [
      {
        path: '',
        redirectTo: 'order',
        pathMatch: 'full'
      },
      {
        path: 'order',
        component: OrderHeatmapComponent,
        resolve: {
          orders: OrderHeatmapResolver
        }
      },
      {
        path: 'driver',
        component: DriverHeatmapComponent,
        resolve: {
          drivers: DriverHeatmapResolver
        }
      }
    ]
  },
  {
    path: 'departuresheet/:id',
    component: DepartureSheetMainComponent,
    resolve: {
      departure_sheet: DepartureSheetResolver
    }
  },
  {
    path: 'admin/livetracking',
    component: LiveTrackingMainComponent,
    resolve: {
      live_tracking: LivetrackingResolver
    }
  },
  {
    path: 'admin/shipment',
    component: ShipmentOrdersComponent,
    children: [
      {
        path: '',
        redirectTo: 'template',
        pathMatch: 'full'
      },
      {
        path: 'template',
        component: ShipmentTemplateComponent,
        resolve: {
          customers: AllCustomersResolver
        }
      },
      {
        path: 'findroutes',
        component: RouteOptimiserComponent,
        data: {
          host: "link"
        }
      },
      {
        path: 'assign-driver',
        component: ShipmentOrderAssignComponent,
        resolve: {
          unassigned_orders: UnassignedShipmentOrderResolver
        }
      }
    ]
  },
  {
    path: 'admin/kiosk',
    component: KioskOrderComponent
  },
  {
    path: 'admin/resourceallocation',
    component: ResourceAllocationMainComponent,
    children: [
      {
        path: '',
        redirectTo: 'listview',
        pathMatch: 'full'
      },
      {
        path: 'listview',
        component: ResourceAllocationListViewComponent,
        resolve: {
          resourceallocation: ResourceAllocationResolver
        }
      }
    ]
  },
  {
    path: '**',
    component: NotFound404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
