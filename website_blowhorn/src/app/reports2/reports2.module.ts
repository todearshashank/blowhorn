import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApolloModule } from 'apollo-angular';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { RouteModule } from '../route/route.module';
import { AppRoutingModule } from '../app-routing.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { Reports2MainComponent } from './components/reports2-main/reports2-main.component';
import { Reports2ChartsComponent } from './components/reports2-charts/reports2-charts.component';
import { Reports2NavbarComponent } from './components/reports2-navbar/reports2-navbar.component';
import { Reports2AnalyticsDataComponent } from './components/reports2-analytics-data/reports2-analytics-data.component';
import { Reports2AlertComponent } from './components/reports2-alert/reports2-alert.component';
import { Reports2BarChartComponent } from './components/reports2-bar-chart/reports2-bar-chart.component';
import { Reports2PieChartComponent } from './components/reports2-pie-chart/reports2-pie-chart.component';
import { Reports2ShipmentDataComponent } from './components/reports2-shipment-data/reports2-shipment-data.component';
import { Reports2TableComponent } from './components/reports2-table/reports2-table.component';
import { Reports2LineChartComponent } from './components/reports2-line-chart/reports2-line-chart.component';
import { ShipmentReportNavbarComponent } from './components/shipment-report-navbar/shipment-report-navbar.component';
import { Report2OrderCreationTrendComponent } from './components/report2-order-creation-trend/report2-order-creation-trend.component';
import { Report2OrderAttemptsComponent } from './components/report2-order-attempts/report2-order-attempts.component';
import { Report2OrderPaymentmodeComponent } from './components/report2-order-paymentmode/report2-order-paymentmode.component';
import { Report2OrderDeliveryperformanceComponent } from './components/report2-order-deliveryperformance/report2-order-deliveryperformance.component';
import { Report2OrderPlacedComponent } from './components/report2-order-placed/report2-order-placed.component';
import { Report2OrderSamedaydeliveryDistributionComponent } from './components/report2-order-samedaydelivery-distribution/report2-order-samedaydelivery-distribution.component';
import { Report2SlaComponent } from './components/report2-sla/report2-sla.component';
import { SearchErrorComponent } from './components/search-error/search-error.component';
import { TripBasedReportsComponent } from './components/trip-based-reports/trip-based-reports.component';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReportFeedbackFormComponent } from './components/report-feedback-form/report-feedback-form.component';
import { TbrTileinfoDisplayComponent } from './components/tbr-tileinfo-display/tbr-tileinfo-display.component';
import { TbrAreaChartComponent } from './components/tbr-area-chart/tbr-area-chart.component';
import { Report2AutoDispatchComponent } from './components/report2-auto-dispatch/report2-auto-dispatch.component';
import { TbrContracttypeStatsComponent } from './components/tbr-contracttype-stats/tbr-contracttype-stats.component';
import { TbrStatusStatsComponent } from './components/tbr-status-stats/tbr-status-stats.component';
import { AddDeleteScheduleComponent } from './components/add-delete-schedule/add-delete-schedule.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { ErrorPromptComponent } from './components/error-prompt/error-prompt.component';
import { ReportConfigurationMainComponent } from './components/report-configuration-main/report-configuration-main.component';
import { UserSubscriptionComponent } from './components/user-subscription/user-subscription.component';
import { TbrTopinfoTileComponent } from './components/tbr-topinfo-tile/tbr-topinfo-tile.component';
import { TripStatusTableComponent } from './components/trip-status-table/trip-status-table.component';
import { StatisticCardComponent } from './components/statistic-card/statistic-card.component';
import { HeatmapsComponent } from './components/heatmaps/heatmaps.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouteModule,
    AppRoutingModule,
    ApolloModule,
    BsDatepickerModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatToolbarModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatSidenavModule,
  ],
  declarations: [
    Reports2MainComponent,
    Reports2ChartsComponent,
    Reports2NavbarComponent,
    Reports2AnalyticsDataComponent,
    Reports2AlertComponent,
    Reports2BarChartComponent,
    Reports2PieChartComponent,
    Reports2ShipmentDataComponent,
    Reports2TableComponent,
    Reports2LineChartComponent,
    ShipmentReportNavbarComponent,
    Report2OrderCreationTrendComponent,
    Report2OrderAttemptsComponent,
    Report2OrderPaymentmodeComponent,
    Report2OrderDeliveryperformanceComponent,
    Report2OrderPlacedComponent,
    Report2OrderSamedaydeliveryDistributionComponent,
    Report2SlaComponent,
    SearchErrorComponent,
    TripBasedReportsComponent,
    ReportFeedbackFormComponent,
    TbrTileinfoDisplayComponent,
    TbrAreaChartComponent,
    Report2AutoDispatchComponent,
    TbrContracttypeStatsComponent,
    TbrStatusStatsComponent,
    ReportConfigurationMainComponent,
    AddDeleteScheduleComponent,
    ConfirmationComponent,
    ErrorPromptComponent,
    UserSubscriptionComponent, TbrTopinfoTileComponent, TripStatusTableComponent, StatisticCardComponent, HeatmapsComponent

  ],
  entryComponents: [
    ReportFeedbackFormComponent,
    AddDeleteScheduleComponent,
    UserSubscriptionComponent
]
})
export class Reports2Module { }
