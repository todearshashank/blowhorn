import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { MemberService } from '../../../shared/services/member.service';
import { ReportScheduleService } from '../../../shared/services/report-schedule.service';
import { AddDeleteScheduleComponent } from '../add-delete-schedule/add-delete-schedule.component';
import { UserSubscriptionComponent } from '../user-subscription/user-subscription.component';



@Component({
  selector: 'app-report-configuration-main',
  templateUrl: './report-configuration-main.component.html',
  styleUrls: ['./report-configuration-main.component.scss']
})
export class ReportConfigurationMainComponent implements OnInit {

  @Output() emitService: EventEmitter<any>;

  report_schedules = [];
  selected_schedule = null;
  available_users = [];
  user_profile = {is_org_admin : false};

  prompt = {
    show: false,
    heading: '',
    details: '',
    styles: {}
  };
  okay = {
    show: false,
    heading: '',
    details: '',
    styles: {}
  };

  add_button = {
    label: 'Add Schedule'
  };

  delete_button = {
    label: 'Delete',
    type: 'delete'
  };

  return_label = {
    label: 'Cancel'
  };

  user_add_button = {
    label: 'Add/Modify Users',
    type: 'proceed'
  };

  constructor(
    private modalService: NgbModal,
    public rs_service: ReportScheduleService,
    public _blowhorn_service: BlowhornService,
    public member_service: MemberService,
  ) {
    this.emitService  = new EventEmitter<any>();
    this._blowhorn_service.show_loader();
    this._blowhorn_service.user_profile_obs.subscribe(
      val => {
        if (val) {
          this.user_profile = val;
        }
      },
      err=>{
        console.error(err);
      }
    );

    rs_service.get_report_schedules().subscribe(response => {
      this.report_schedules = response.results;
      this._blowhorn_service.hide_loader();
    },
    err=>{
      console.error(err);
      this._blowhorn_service.hide_loader();
    });
  }

  ngOnInit() {
    this.member_service.get_members().subscribe(
        response=>{
          this.available_users = [];
          response.results.forEach(item => {
              if (item.is_active) {
                this.available_users.push({
                  id: item.id,
                  selected: false,
                  name: item.name
              });
            };
          });
      },
      err=>{
        console.error(err);
      });

  }

  open_member_modal(): void {
    const modalRef = this.modalService.open(AddDeleteScheduleComponent);
    modalRef.componentInstance.schedule = {};
    modalRef.componentInstance.operation = 'Add';
    modalRef.componentInstance.emitService.subscribe(result => {
      let index = this.report_schedules.findIndex(item => item.id === result.id);
      this.report_schedules.splice(index > -1 ? index : 0, index > -1 ? 1 : 0, result);
      setTimeout(() => this.modalService.dismissAll(false), 1000)
    },
    err => {
        console.error('Error', err);
    });
  }

  show_confirmation(report_id: any): void {
    this.prompt.show = true;
    this.selected_schedule = report_id;
  };

  do_nothing(event): void {
    this.okay.show = false;
  }

  delete_report_schedule(event): void {
    if (!event.proceed) {
      this.prompt.show = false;
      return;
    }
    this.rs_service.delete_report_schedule(this.selected_schedule).subscribe(
      response => {
        let index = this.report_schedules.findIndex(item => item.id === this.selected_schedule);
        this.report_schedules.splice(index, 1);
        this.prompt.show = false
      },
      err => {
        console.log(err);
        this.prompt.show = false;
        this.okay.show = true;
      });
  }

  open_user_subscription_modal(event): void {
    const modalRef = this.modalService.open(UserSubscriptionComponent, {size: 'lg'});
    modalRef.componentInstance.available_users = this.available_users;
    modalRef.componentInstance.selected_schedule_id = event.id;
    modalRef.componentInstance.subscribed_users = event.subscribed_users;
    modalRef.componentInstance.emitService.subscribe((result: any) => {
      let index = this.report_schedules.findIndex((item: any) => item.id === event.id);
      if (index > -1) {
        this.report_schedules[index].subscribed_users = result;
      }
      setTimeout(() => this.modalService.dismissAll(false), 2000)
    },
    (err: any) => {
      console.error('Error', err);
      this.okay.show = true;
    });
  }

}
