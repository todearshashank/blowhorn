import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-report2-order-placed',
  templateUrl: './report2-order-placed.component.html',
  styleUrls: ['./report2-order-placed.component.scss']
})
export class Report2OrderPlacedComponent implements OnInit {

  BarChart;
  input_data = [];
  barChartdata = [];
  barChartLabel = [];
  trend_name = '';
  error_msg = 'No data found';


  constructor(private report_service: ReportService) {

      this.report_service.orderplaced_store.subscribe(data => {
        
        this.input_data = data.data || [];
        this.barChartLabel = [];
        this.barChartdata = [];
        this.trend_name = data.trend;

        // loop the input data to set the values
        this.input_data.forEach(item => {
          this.barChartLabel.push(item.trend);
          this.barChartdata.push(item.count);
        });

        if (this.BarChart) {
          this.BarChart.destroy();
        }
        this.draw_bar_chart();
    });
   }

  ngOnInit() {
  }

  draw_bar_chart() {
    this.BarChart = new Chart('barChart2', {
      type: 'bar',
      data: {
        labels: this.barChartLabel,
        datasets: [{
          label: 'No of orders',
          data: this.barChartdata,
          lineTension: 0.3,
          borderWidth: 1,
          fill: true,
          backgroundColor: [
            "#7189BF", "#DF7599", "#FFC785", "#7CDDDD",
            "#ab93c9", "#d698b9", "#eda1ab", "#ffbea3",
            "#ac8daf", "#fff1ac", "#f9bcdd", "#d5a4cf",
            "#aeddcd", "#DF7599", "#FFC785", "#72D6C9",
            "#ab93c9", "#d698b9", "#eda1ab", "#ffbea3",
            "#ac8daf", "#fff1ac", "#f9bcdd", "#7189BF",
          ]
        }]

      },
      options: {
        plugins: {
          datalabels: {
          color: 'black',
          anchor: 'end',
          align: 'top'
        }
        },
        legend: {
          display: false
        },
        scales : {
          yAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: 'Orders',
                  fontSize: 14
                },
                ticks: {
                  beginAtZero: true,
                  fontSize: 14,
                  maxTicksLimit: 5
    
                }
              }],
              xAxes: [{
                gridLines: {
                    display: false
                },
                scaleLabel: {
                  display: true,
                  labelString: this.trend_name,
                  fontSize: 14
                },
              }]
        },
      }
    })
  }

  download_documents(event: string): void {
    new ngxCsv(this.input_data, 'Order Placed'); 
  }

  download_charts(): void {
    let base64 = this.BarChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'chart.png', 'png');
  }

}

