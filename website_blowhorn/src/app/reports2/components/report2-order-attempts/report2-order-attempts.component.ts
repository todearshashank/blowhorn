import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-report2-order-attempts',
  templateUrl: './report2-order-attempts.component.html',
  styleUrls: ['./report2-order-attempts.component.scss']
})
export class Report2OrderAttemptsComponent implements OnInit {

  BarChart;
  chartLabels: any;
  input_data = [];
  barChartdata = [];
  barChartLabel = [];
  sum = 0;
  error_msg = 'No data found';
  
  constructor(private report_service: ReportService) {

    
        this.chartLabels = ChartDataLabels;
        this.sum = 0;
        this.report_service.orderattempts_store.subscribe(data => {
        this.barChartLabel = [];
        this.barChartdata = [];
        this.input_data = data.data || [];
        this.sum = 0;

        // loop the input data to set the values
        this.input_data.forEach(item => {
          this.barChartLabel.push(item.attempt);
          this.barChartdata.push(item.count);
          this.sum += Number(item.count);
        });
      
      if (this.BarChart) {
        this.BarChart.destroy();
      }

      this.draw_bar_chart(this.sum);
    });

    

   }

  ngOnInit() {
  }


  draw_bar_chart(sum) {
    this.BarChart = new Chart('barChart', {
      type: 'bar',
      data: {
        labels: this.barChartLabel,
        datasets: [{
          data: this.barChartdata,
          lineTension: 0.3,
          borderWidth: 1,
          fill: true,
          backgroundColor: [
            "#ab93c9", "#7CDDDD", "#7189BF"
          ]
        }]

      },
      options: {
        legend: {
          display: false
        },
        animation: {
          duration: 1500,
          onComplete: function (animation) { 
              this.toBase64Image();
      }},
        plugins: {
          datalabels: {
            color: 'black',
            anchor: 'end',
            align: function(context) {
              return context.dataset.data[context.dataIndex] < 5000 ? 'top' : 'bottom';
            },
            font: {
              weight: 'bold',
              size: 12,
            },
            formatter: function(value) {
              if (Number(value) > 0) {
                let result = value;
                let val = Number(value / sum * 100).toFixed(0);
                let percentage = " (" + val + "%)";
                if (value !== 0) {
                  result += percentage;
                };
                return result;
              };
              }
            }
        },
        scales : {
          yAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: 'Orders',
                  fontSize: 14
                },
                ticks: {
                  beginAtZero: true,
                  fontSize: 14,
                  maxTicksLimit: 5
    
                }
              }],
              xAxes: [{
                gridLines: {
                    display: false
                },
              }]
        },
      }
    })
  }

  arrayToCSV(objArray) {
    const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
    let str = `${Object.keys(array[0]).map(value => `"${value}"`).join(",")}` + '\r\n';

    return array.reduce((str, next) => {
        str += `${Object.values(next).map(value => `"${value}"`).join(",")}` + '\r\n';
        return str;
       }, str);
  }
  download_documents(event: string): void {
    new ngxCsv(this.input_data, 'Order Attempts');
    
  }

  download_charts(): void {
    let base64 = this.BarChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'chart.png', 'png');
  }
}
