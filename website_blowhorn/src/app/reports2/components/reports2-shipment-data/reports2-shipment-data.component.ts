import { Component, OnInit, Input } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ActivatedRoute } from '@angular/router';
import { ReportService } from '../../../shared/services/report.service';
import { Shipment } from '../../../shared/models/shipments-grid.model';
import { RSA_NO_PADDING } from 'constants';



@Component({
  selector: 'app-reports2-shipment-data',
  templateUrl: './reports2-shipment-data.component.html',
  styleUrls: ['./reports2-shipment-data.component.scss']
})
export class Reports2ShipmentDataComponent implements OnInit {

  @Input()
  total_orders_cod;
  @Input()
  cash_collected;
  @Input()
  total_orders;
  @Input()
  shipment_lost;
  @Input()
  shipmenthub;

  codorder_report = {
    total_orders: 0,
    cash_collected: 0
  }

  shipmentorder_report = {
    total_orders: 0,
    shipment_lost: 0,
    shipmenthub: 0
  }
  shipment_delivery_percent = {
    shipment_delivery: 0
  }


  billing = {
    home: {
      title: 'Shipment Delivery Percent',
      content: 0
    }
  }

  lost = {
    home: {
      title: 'COD Orders',
      content: 0
    }
  }

  shipment_hub = {
    home: {
      title: 'Amount of Cash Collected',
      content: 0
    }
  }


  constructor(private route: ActivatedRoute,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private report_service: ReportService
  ) {
    this.watcher_service.codorder_report.subscribe(codorder_report => {
      this.codorder_report = codorder_report;

      this.lost = {
        home: {
          title: 'COD Orders',
          content: this.codorder_report.total_orders
        }
      }

      this.shipment_hub = {
        home: {
          title: 'Amount of Cash Collected',
          content: this.codorder_report.cash_collected
        }
      }

    });

    this.watcher_service.shipment_delivery_percent.subscribe(shipment_delivery_percent => {
      this.shipment_delivery_percent = shipment_delivery_percent;
      this.billing = {
        home: {
          title: 'Shipment Delivery Percent',
          content: this.shipment_delivery_percent.shipment_delivery
        }
      }

    })

  }

  ngOnInit() {

  }


}
