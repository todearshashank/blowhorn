import { Component, OnInit } from '@angular/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ReportService } from '../../../shared/services/report.service';
import { WatcherService } from '../../../shared/services/watcher.service';

@Component({
  selector: 'app-trip-based-reports',
  templateUrl: './trip-based-reports.component.html',
  styleUrls: ['./trip-based-reports.component.scss']
})
export class TripBasedReportsComponent implements OnInit {
  calling_api: boolean = false;
  trips_present = false;

  constructor(private report_service: ReportService, 
              private watcher_service: WatcherService,
              public _blowhorn_service: BlowhornService,
            ) { 
    this.watcher_service.filter_changed_method.subscribe(() => {
      if (this.calling_api) {
        return;
      }
      this.calling_api = true;
      this._blowhorn_service.show_loader();
      this.filter_changed({url:'quick_filter=7'});
    });
  }

  ngOnInit() {
  }

  filter_changed(event: any) {
    this._blowhorn_service.show_loader();
    this.report_service.get_trip_stats(event.url).subscribe(data => {
      this.report_service.update_tripstats_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_trip_allstats(event.url).subscribe(data => {
      if (Number(data.data._count) !== 0) {
        this.trips_present = true;
      }
      this.report_service.update_tripallstats_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_trip_contracttype(event.url).subscribe(data => {
      this.report_service.update_tripcontracttype_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_trip_status(event.url).subscribe(data => {
      this.report_service.update_tripstatus_store(data);
      this._blowhorn_service.hide_loader();
    })
  }

}
