import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import html2canvas from 'html2canvas';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-report2-auto-dispatch',
  templateUrl: './report2-auto-dispatch.component.html',
  styleUrls: ['./report2-auto-dispatch.component.scss']
})
export class Report2AutoDispatchComponent implements OnInit {

  @ViewChild('screen', { read: ElementRef, static: false }) pdfForm: ElementRef;
  content = [];
  header = [];
  footer  = [];
  header_keys = [];
  show_error_msg = true;
  error_msg = 'No data found';
  input_data = [];

  constructor(private report_service: ReportService) {
    this.report_service.orderautodispatch_store.subscribe(data => {
      // loop the input data to set the values
      this.content = data.data || [];
      let header = data.header;
      this.footer = data.footer;
      let total_success = 0;
      let total_count = 0;
      let total_failure = 0;

      if (header) {
        this.header = Object.values(header);
        this.header_keys = Object.keys(header);
      }

      this.content.forEach(element => {
        let percentage = 0;

        total_success += Number(element.auto_dispatch_success)
        total_count += Number(element.total_count)
        total_failure += Number(element.auto_dispatch_failure)
        if(Number(element.auto_dispatch_success) > 0) {
            percentage = Number((Number(element.auto_dispatch_success)/element.total_count * 100));
        };

        element.auto_dispatch_success_per = percentage.toFixed(2) + "%";
        element.auto_dispatch_failure_per = (100-percentage).toFixed(2) + "%";

      });

      if (this.content.length > 0) {
        let percentage = Number((Number(total_success)/total_count * 100));
        this.footer['hub_name'] = 'GRAND TOTAL';
        this.footer['auto_dispatch_success'] = total_success;
        this.footer['auto_dispatch_success_per'] = percentage.toFixed(2) + "%";
        this.footer['auto_dispatch_failure'] = total_failure;
        this.footer['auto_dispatch_failure_per'] = (100-percentage).toFixed(2) + "%";
        this.footer['total_count'] = total_count;
      }

      if (this.content.length > 0) {
        this.show_error_msg = false;
      }
  });
  }

  ngOnInit() {
  }

  download_documents(event: string): void {
    var container = document.getElementById("dispatch_chart");
    html2canvas(container, {scrollY: -window.scrollY}).then(function (canvas) {

        var link = document.createElement("a");
        document.body.appendChild(link);
        link.download = "autodispatch_report.jpg";
        link.href = canvas.toDataURL();
        link.target = '_blank';
        link.click();
    });

}

}
