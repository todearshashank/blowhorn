import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-tbr-status-stats',
  templateUrl: './tbr-status-stats.component.html',
  styleUrls: ['./tbr-status-stats.component.scss']
})
export class TbrStatusStatsComponent implements OnInit {

  PieChart;
  statuswiseData: any;
  type_disabled = true;
  selected_chart_id = null;
  selected_chart_name = '----------';
  statuswise_count = [];
  charts = [];
  statusData = [];
  error_msg = 'No records found';
  showInfo = false;
  sum = 0;

  chart_types = [
    {name : 'pie', id : 1},
    {name : 'doughnut', id : 2},
    {name : 'polarArea', id : 2},
  ]

  constructor(
    private report_service: ReportService
  ) {
    this.report_service.tripstatus_store.subscribe(data => {
      this.statuswiseData = [];
      this.statuswise_count = data.data || [];
      this.statusData = [];
      this.type_disabled = true;
      this.sum = 0;
      this.selected_chart_name = 'pie'
      this.selected_chart_id = 1

      // calculate sum for finding the percentage
      this.statuswise_count.forEach(item => {
        this.sum += Number(item.count);
      });

      // loop thru the input data to calculate the pecentage for display
      this.statuswise_count.forEach(item => {
          let count = 0;

          // calculate percentage if value > 0
          if (Number(item.count) > 0) {
            count = Number((Number(item.count)/this.sum * 100).toFixed(2));
            this.statuswiseData.push(count);
            this.statusData.push(item.status);
          };

      });
  
      if (this.PieChart) {
        this.PieChart.destroy();
      }
      setTimeout(() => this.draw_pie_chart(), 2000)
    });
   }

  ngOnInit() {
    
  }

  chart_type_changed(event: any): void {
    this.selected_chart_id = event.id
    this.selected_chart_name = event.name
    if (this.PieChart) {
      this.PieChart.destroy();
    }
    this.draw_pie_chart();
  }

  draw_pie_chart() {
    if (!this.statuswiseData.length) {
      return;
    }
    this.type_disabled = false;
    this.PieChart = new Chart('tripstatuschart', {
      type: this.selected_chart_name,
      data: {
        datasets: [{
          data: this.statuswiseData,
          backgroundColor: [
            "#ab93c9", "#99DCFF", "#DF7599", "#FFC785", 
            "#eda1ab", "#7189BF", "#d698b9"
          ]
        }],
        labels: this.statusData,

      },
      options: {
        plugins: {
            datalabels: {
              formatter: function(value) {
                var temp = Number(value);
                if (Number(temp) > 0) {
                  return temp + '%';
                };
                return "";
                }
            }
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            let lab = data.labels[tooltipItem.index] + 
                        " : " + 
                        data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + 
                        "%";
            return lab;
          },
         }
        },
        animation: {
          duration: 4000
        },
        legend: { 
          display: true,
          position: "right",
          labels: {
            fontSize: 12,
          }          
        }
        
        
      }
    })
  }

  get chart_options_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '130px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '10px'
      },
      'dropdown-menu-styles': {
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  download_documents(event: string): void {
    new ngxCsv(this.statuswise_count, 'Order Status Report');
  }

  download_charts(): void {
    let base64 = this.PieChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'image.png', 'png');
  }

}

