import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportScheduleService } from '../../../shared/services/report-schedule.service';
import { ShipmentService } from '../../../shared/services/shipment.service';

@Component({
  selector: 'app-add-delete-schedule',
  templateUrl: './add-delete-schedule.component.html',
  styleUrls: ['./add-delete-schedule.component.scss']
})
export class AddDeleteScheduleComponent implements OnInit {

  @Output()
  emitService: EventEmitter<any>;
  formError = null;
  selected_weekday_id = null;
  selected_weekday_name = null;
  selected_report_id = null;
  selected_report_name = null;
  selected_city_id = null;
  selected_city_name = null;
  selected_category = null;
  category_options = [];
  schedule_type= null;
  report_type = null;
  master_report_type = null;
  report_options = [];
  selected_period_id = null;
  selected_period_name = null;
  cities: any[];
  
  show = {
      request_loader: false,
  };

  header_text = 'Add Request';
  weekday_options = [
    {'name' : 'Mon', 'id' : 0},
    {'name' : 'Tue', 'id' : 1},
    {'name' : 'Wed', 'id' : 2},
    {'name' : 'Thu', 'id' : 3},
    {'name' : 'Fri', 'id' : 4},
    {'name' : 'Sat', 'id' : 5},
    {'name' : 'Sun', 'id' : 6},
  ]

  periodlist = [
    {'name' : 'Daily', 'id' : 1},
    {'name' : 'Weekly', 'id' : 2},
    {'name' : 'Monthly', 'id' : 3},
  ]
  
  submit_button = {
    type: 'save',
    label: 'Save',
    disabled: false
  };

  dismiss_button = {
      label: 'Cancel',
      type: 'delete',
      disabled: false
  };
  

  constructor(
    public rs_service: ReportScheduleService,
    public activeModal: NgbActiveModal,
    private shipment_service: ShipmentService
  ) { 
    
    this.shipment_service.get_shipment_filter('schedule_reports').subscribe(data => {
      this.cities = data.cities;
    });
    
    this.rs_service.get_report_types().subscribe(
      response=>{
        this.category_options = response.category;
        this.master_report_type = response.reports
        this.submit_button.disabled = false;
    },
    err=>{
      console.error(err);
    }
    );
    
    this.emitService  = new EventEmitter<any>();
  }

  ngOnInit() {
  }

  report_category_changed(event): void {
    this.selected_category = event.name
    this.master_report_type.forEach(item => {
      if (event.name === item.category) {
          this.report_options.push(item);
      }
    });
    this.formError = null;
    this.selected_report_id = null;
  }

  report_type_changed(event): void {
    this.report_options.forEach((item,index) => {
      if (event.name === item.name) {
          this.selected_report_id = item.id;
      }
    });
    this.selected_report_name = event.name;
    this.formError = null;
  }

  weekday_changed(event): void {
    this.selected_weekday_id = event.id;
    this.selected_weekday_name = event.name;
    this.formError = null;
  }

  city_changed(event): void {
    this.selected_city_id = event.id;
    this.selected_city_name = event.name;
    this.formError = null;
  }

  period_changed(event): void {
    this.selected_period_id = event.id;
    this.selected_period_name = event.name;
    this.formError = null;
  }

  save_schedule(): void {
    this.formError = null;
    if (!this.selected_category) {
      this.formError = 'Please select the module category';
    } else if (!this.selected_report_id) {
      this.formError = 'Please select the report type';
    } else if (this.schedule_type === 'weekly' && this.selected_weekday_id === null) {
      this.formError = 'Please select the weekday option for weekly reports';
    } else if (!this.schedule_type) {
      this.formError = 'Please select the schedule type';
    } else if (!this.selected_period_id) {
      this.formError = 'Please select the aggregator period';
    }

    if (this.formError) {
      return;
    }

    let data = {
      period : this.schedule_type,
      weekday : this.selected_weekday_id,
      report_type : this.selected_report_id,
      city : this.selected_city_id,
      aggregator : this.selected_period_name
    };
    this.show.request_loader = true;
    this.submit_button.disabled = true;
    this.rs_service.add_report_schedule(data).subscribe(
          response=>{
              this.emitService.next(response);
              this.submit_button.disabled = false;
              this.show.request_loader = false;
          },
          err=>{
              this.formError = 'Please try again later'
              this.show.request_loader = false;
              this.submit_button.disabled = false;
              console.log(err);
          }
    );

  }

  get weekday_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '160px',
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
        'padding-top': '10px',
      },
      'dropdown-menu-styles': {
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

  get report_type_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '160px',
      },
      'dropdown-toggle-styles': {
        'padding-left': '0px',
        'padding-top': '10px',
      },
      'dropdown-menu-styles': {
        'width': '250px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)',
      }
    };
  }
}
