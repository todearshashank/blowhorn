import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-error',
  templateUrl: './search-error.component.html',
  styleUrls: ['./search-error.component.scss']
})
export class SearchErrorComponent implements OnInit {

  
  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e) {
    var data = document.body;
    var pageX = data.clientWidth;
    var pageY = data.clientHeight;
    var mouseY=0;
    var mouseX=0;

    mouseY = e.pageY;
    var yAxis = (pageY/2-mouseY)/pageY*300; 
    mouseX = e.pageX / -pageX;
    var xAxis = -mouseX * 100 - 100;
    var ghost_eye = document.getElementById('ghosteye');
    ghost_eye.style.transform = 'translate('+ xAxis +'%,-'+ yAxis +'%)';
    }
  
  constructor() { }

  ngOnInit() {
  }

}
