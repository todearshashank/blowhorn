import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-statistic-card',
  templateUrl: './statistic-card.component.html',
  styleUrls: ['./statistic-card.component.scss']
})
export class StatisticCardComponent implements OnInit {

    @Input()
    label = ''
    @Input()
    value = ''
    @Input()
    icon = ''
    @Input()
    styles = {
        wrapper: {},
        body: {},
        avatar: {},
        icon: {}
    }
    srcData: SafeResourceUrl;

  constructor(
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.srcData = this.sanitizer.bypassSecurityTrustResourceUrl(this.icon);
  }

}
