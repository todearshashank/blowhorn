import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { Apollo } from 'apollo-angular';
import { ReportService } from '../../../shared/services/report.service';
import { WatcherService } from '../../../shared/services/watcher.service';
import { SlotwiseDataDelivered, SlotwiseDataInprogress } from '../../../shared/models/report.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-reports2-bar-chart',
    templateUrl: './reports2-bar-chart.component.html',
    styleUrls: ['./reports2-bar-chart.component.scss']
})
export class Reports2BarChartComponent implements OnInit {

    BarChart;
    charts;
    keyForLabels = [];
    dataForDelivered = [];
    dataForInprogress = [];
    status_inprogress;
    status_delivered;
    slotwiseDataDelivered: any;
    slotwiseDataInProgress: any;
    error_msg = 'No records found';
    data = {};
    constructor(
        private _apollo: Apollo,
        private route: ActivatedRoute,
        public _blowhorn_service: BlowhornService,
        private _watcher_service: WatcherService,
        private report_service: ReportService,
    ) {
        this._watcher_service.slotwise_report
            .subscribe(data => {
                if (!data) {
                    return;
                }
                this.dataForDelivered = [];
                this.keyForLabels = [];
                this.dataForInprogress = [];
                this.slotwiseDataDelivered = data.filter((item) => item.status === "Delivered");
                this.slotwiseDataInProgress = data.filter((item) => item.status === "Inprogress");
                this.slotwiseDataDelivered.forEach(item => {
                    this.dataForDelivered.push(item.value);
                    this.keyForLabels.push(item.key);
                });
                this.slotwiseDataInProgress.forEach(item => {
                    this.dataForInprogress.push(item.value);
                });

                if (this.BarChart) {
                    this.BarChart.destroy();
                }
                this.draw_bar_chart();
            });
    }

    ngOnInit() {
    }

    draw_bar_chart() {
        this.BarChart = new Chart('barChart', {
            type: 'bar',
            data: {
                labels: this.keyForLabels,
                datasets: [
                    {
                        label: 'Delivered',
                        data: this.dataForDelivered,
                        backgroundColor: '#2D99FF'
                    },

                    {
                        label: 'Inprogress',
                        data: this.dataForInprogress,
                        backgroundColor: '#2CD9C5'
                    }

                ]
            },
            options: {

                title: {
                    text: "Bar Chart",
                    display: false
                },
                animation: {
                    duration: 4000
                },
                scales: {
                    yAxes: [{
                        stacked: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Number of Deliveries',
                            fontSize: 15
                        },
                        ticks: {
                            beginAtZero: true,
                            fontSize: 15

                        }
                    }],
                    xAxes: [{

                        stacked: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Hours',
                            fontSize: 15
                        }

                    }]
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        fontSize: 14
                    }
                }
            }
        });
    }

}


