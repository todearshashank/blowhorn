import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import html2canvas from 'html2canvas';
import jspdf from 'jspdf';
import { City } from '../../../shared/models/city.model';
import { IdName } from '../../../shared/models/id-name.model';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ShipmentService } from '../../../shared/services/shipment.service';
import { WatcherService } from '../../../shared/services/watcher.service';

@Component({
    selector: 'app-shipment-report-navbar',
    templateUrl: './shipment-report-navbar.component.html',
    styleUrls: ['./shipment-report-navbar.component.scss']
})
export class ShipmentReportNavbarComponent implements OnInit {

    @Output()
    filter_changed: EventEmitter<{ url: string }>;
    @Output()
    search_fired: EventEmitter<void>;

    // variable declaration
    FILTER_TODAY = 0;
    FILTER_LAST_7_DAYS = 7;
    FILTER_LAST_30_DAYS = 30;
    quickFilters = [
        { label: 'Today', val: this.FILTER_TODAY },
        { label: 'Last 7 Days', val: this.FILTER_LAST_7_DAYS },
        { label: 'Last 30 Days', val: this.FILTER_LAST_30_DAYS },
    ];

    date_range: [Date, Date] = [null, null];
    cities: City[] = [];
    hubs: IdName[] = [];
    city_store: City[] = [];
    hub_store: IdName[] = [];
    selected_city_id: number = null;
    selected_city_name = 'All Cities';
    selected_hub_id: number = null;
    selected_hub_name = 'All Hubs';
    selected_quick_filter = this.FILTER_LAST_7_DAYS;
    date_activated = false;
    city_disabled = false;
    hub_disabled = false;

    constructor(
        public _blowhorn_service: BlowhornService,
        private shipment_service: ShipmentService
    ) {
        this.filter_changed = new EventEmitter<{ url: string }>();
        this.search_fired = new EventEmitter<void>();
        this.shipment_service.get_shipment_filter('analytics')
            .subscribe(data => {
                this.cities = data.cities;
                this.city_store = data.cities;
                this.cities.forEach(item => {
                    this.hub_store.concat(item.hubs);
                    this.hubs.concat(item.hubs);
                });
            });
    }

    ngOnInit() {
    }

    activate_date_clear(event: any): void {
        this.date_activated = true;
    }

    deactivate_date_clear(event: any): void {
        this.date_activated = false;
    }

    filter_dropdowns(): void {
        this.hubs = [];
        if (this.selected_city_id) {
            const city = this.city_store
                .filter(item => item.id === this.selected_city_id)[0];
            for (const hub of city.hubs) {
                this.hubs.push(hub);
            }
        } else {
            this.hubs = [];
            this.hubs = this._blowhorn_service.copy_array(this.hub_store);
        }
    }

    reset_city(): void {
        this.selected_city_id = null;
        this.selected_city_name = 'All Cities';
    }

    reset_hub(): void {
        this.selected_hub_id = null;
        this.selected_hub_name = 'All Hubs';
    }

    reset_date(event: any): void {
        this.date_range = [null, null];
        this.date_changed(event);
    }

    applyQuickFilter(val: number): void {
        if (val === this.selected_quick_filter) {
            return;
        }
        this.selected_quick_filter = val;
        this.date_range = [null, null];
        const url = this.get_url();
        this.filter_changed.emit({ url: this.get_url() });
    }

    date_changed(event: any): void {
        this.selected_quick_filter = null;
        const url = this.get_url();
        this.filter_changed.emit({ url: url });
    }

    city_changed(event: { name: string, id: number }): void {
        if (this.selected_city_id !== event.id) {
            this.selected_city_name = event.name;
            this.selected_city_id = event.id;
            this.reset_hub();
            this.filter_dropdowns();
        }
        const url = this.get_url();
        this.filter_changed.emit({ url: url });
    }

    hub_changed(event: any): void {
        if (this.selected_hub_id !== event.id) {
            this.selected_hub_name = event.name;
            this.selected_hub_id = event.id;
        }
        const url = this.get_url();
        this.filter_changed.emit({ url: url });
    }

    get_url(): string {
        let url = '';
        url += this.selected_city_id ? `&city_id=${this.selected_city_id}` : '';
        url += this.selected_hub_id ? `&hub_id=${this.selected_hub_id}` : '';
        url += this.date_range[0] ? `&start=${this.date_range[0].toISOString().substring(0, 10)}` : '';
        url += this.date_range[1] ? `&end=${this.date_range[1].toISOString().substring(0, 10)}` : '';
        url += !this.date_range[0] && this.selected_quick_filter !== null ? `&quick_filter=${this.selected_quick_filter}` : '';
        console.log(url);
        return url;
    }

    download_documents(event: string): void {
        var data = document.body;
        html2canvas(data, {
            scrollY: -window.scrollY,
            allowTaint: true
        }).then((canvas) => {
            var link = document.createElement("a");
            document.body.appendChild(link);
            link.download = "html_image.png";
            link.href = canvas.toDataURL("image/png");
            link.target = '_blank';
            link.click();
        });
    }

    get city_styles(): {} {
        return {
            'dropdown-styles': {
                'box-shadow': 'none',
                'border': 'none',
                'height': '30px',
                'width': '130px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px'
            },
            'dropdown-menu-styles': {
                'width': '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get hub_styles(): {} {
        return {
            'dropdown-styles': {
                'box-shadow': 'none',
                'border': 'none',
                'height': '30px',
                'width': '160px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px',
            },
            'dropdown-menu-styles': {
                'width': '250px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }
}
