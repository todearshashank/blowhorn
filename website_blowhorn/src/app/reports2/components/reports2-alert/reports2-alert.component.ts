import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ActivatedRoute } from '@angular/router';
import { ReportService } from '../../../shared/services/report.service';
import { Shipment } from '../../../shared/models/shipments-grid.model';

@Component({
  selector: 'app-reports2-alert',
  templateUrl: './reports2-alert.component.html',
  styleUrls: ['./reports2-alert.component.scss']
})
export class Reports2AlertComponent implements OnInit {
  @Input()
  attempt: string;
  @Input()
  firstAttemptData: number;
  @Input()
  secondAttemptData: number;
  @Input()
  thirdAttemptData: number;
  @Input()
  data: number;
  @Output()
  close_fired: EventEmitter<boolean>;

  first_attempt: number = 30;
  third_attempt: number = 40;
  second_attempt: number = 50;
  order_report = {
    max_order: 0,
    time_range: 0,
    reattempted_orders: 0,
    total_attempts: 0,
    first_attempt: 0,
    second_attempt: 0,
    third_attempt: 0
  };

  constructor(
    private route: ActivatedRoute,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private report_service: ReportService
  ) {
    this.close_fired = new EventEmitter<boolean>();
    this.watcher_service.order_report.subscribe(order_report => {
      this.order_report = order_report;

    });

  }

  ngOnInit() {


  }


  close_fired_wrapper(event: any) {
    this.close_fired.emit(true);
  }

}