import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ActivatedRoute } from '@angular/router';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-reports2-line-chart',
  templateUrl: './reports2-line-chart.component.html',
  styleUrls: ['./reports2-line-chart.component.scss']
})
export class Reports2LineChartComponent implements OnInit {
  LineChart;
  ordercreated_time;
  lineChartdata = [];
  lineChartLabel = [];

  constructor(
    private route: ActivatedRoute,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private report_service: ReportService
  ) {
    this.watcher_service.ordercreated_time.subscribe(ordercreated_time => {
      this.lineChartLabel = [];
      this.lineChartdata = [];
      this.ordercreated_time = ordercreated_time;
      this.lineChartdata = Object.values(ordercreated_time);
      this.lineChartLabel = Object.keys(ordercreated_time);

      if (this.LineChart) {
        this.LineChart.destroy();
    }
      this.draw_line_chart();

    });

  }

  ngOnInit() {

  }

  draw_line_chart() {
    this.LineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: this.lineChartLabel,
        datasets: [{
          label: 'No of Orders',
          data: this.lineChartdata,
          lineTension: 0.2,
          borderColor: "rgb(23,162,184)",
          borderWidth: 1,
          fill: true,
backgroundColor: "rgba(23,162,184, 0.7)"
        }]

      },
      options: {
        title: {
          text: "Line Chart",
          display: false,
        },
        animation: {
          duration: 4000
        },
        scales: {
          yAxes: [{
            stacked: true,
            scaleLabel: {
              display: true,
              labelString: 'Number of Orders',
              fontSize: 15
            },
            ticks: {
              beginAtZero: true,
              fontSize: 15

            }
          }],
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Hours',
              fontSize: 15
            }
          }]
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            fontSize: 14
          }
        }
      }
    })
  }

}
