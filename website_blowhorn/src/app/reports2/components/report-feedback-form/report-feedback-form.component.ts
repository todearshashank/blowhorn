import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-report-feedback-form',
  templateUrl: './report-feedback-form.component.html',
  styleUrls: ['./report-feedback-form.component.scss']
})
export class ReportFeedbackFormComponent implements OnInit {

  @Output()
  emitService: EventEmitter<any>;

  submit_button = {
    type: 'save',
    label: 'Submit',
    disabled: false
  };

  dismiss_button = {
      label: 'Cancel',
      type: 'delete',
      disabled: false
  };

  request_loader = false;
  user_feedback: any;
  formError = '';
  sent = false;
  
  constructor(private report_service: ReportService,
                public activeModal: NgbActiveModal,) 
  {
    this.emitService  = new EventEmitter<any>();
  }

  ngOnInit() {
    this.user_feedback = '';
  }

  submit_feedback() {
    let data = {
      feedback : this.user_feedback
    };
    this.request_loader = true;
    this.submit_button.disabled = true;
    this.report_service.submit_feedback(data).subscribe(
          response=>{
              this.emitService.next(response);
              this.submit_button.disabled = false;
              this.request_loader = false;
              this.sent = true
          },
          err=>{
              this.formError = 'Please try again later'
              this.request_loader = false;
              this.submit_button.disabled = false;
              console.log(err);
          }
    );
}

}
