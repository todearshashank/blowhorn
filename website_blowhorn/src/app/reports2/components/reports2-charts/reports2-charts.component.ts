import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-reports2-charts',
  templateUrl: './reports2-charts.component.html',
  styleUrls: ['./reports2-charts.component.scss'],
})
export class Reports2ChartsComponent implements OnInit {
  master_data: any = [];
  pickupDate = new Date();
  max_order: number;
  time_range: string;
  reattempted_orders: number;
  total_attempts: number;
  first_attempt: number;
  third_attempt: number;
  second_attempt: number;
  total_orders_cod: number;
  cash_collected: number;
  total_orders: number;
  shipment_lost: number;
  shipmenthub: number;
  calling_api: boolean;

  @Input()
  order_present: boolean;

  constructor(
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private report_service: ReportService,
  ) {
    this.watcher_service.filter_changed_method.subscribe(() => {
      if (this.calling_api) {
        return;
      }
      this.calling_api = true;
      this._blowhorn_service.show_loader();
      this.filter_changed({url:'quick_filter=7'});
    });
  }

  ngOnInit() {

  }

  get_status_report() {
    this.report_service.get_statuswise_report_data().subscribe(data => {
      this.master_data = data.data;
      this.watcher_service.update_statuswise_data(
        this.master_data.statuswiseReports
      );
    });
  }
  get_slotwise_report() {
    this.report_service.get_slotwise_report_data(this.report_service.pickupDate).subscribe(data => {
      this.master_data = data.data;
      this.watcher_service.update_slotwise_data(
        this.master_data.slotwiseReports);
    })
  }

  getOrderReports() {

    this.report_service
      .get_order_reports(this.report_service.pickupDate)
      .subscribe(data => {
        this.watcher_service.update_order_data(data);
      });
  }
  getCODOrderReports() {
    this.report_service
      .get_codorder_status(this.report_service.pickupDate)
      .subscribe(data => {
        this.watcher_service.update_codorder_data(data);
      });
  }

  getStatusWisecount() {
    this.report_service.get_statuswisecount(this.report_service.pickupDate).subscribe(data => {
      this.calling_api = false;
      this.watcher_service.update_shipmentorder_data(data);
    })
  }

  getShipmentDelivery() {
    this.report_service.get_shipment_delivery(this.report_service.pickupDate).subscribe(data => {
      this.calling_api = false;
      this.watcher_service.update_shipment_delivery(data);
    })
  }

  getHubwiseData() {
    this.report_service.get_hubwise_data(this.report_service.pickupDate).subscribe(data => {
      this.calling_api = false;
      this._blowhorn_service.hide_loader();
      this.watcher_service.update_hubwise_data(data);

    })
  }

  getOrdercreatedTime() {
    this.report_service.get_ordercreated_time(this.report_service.pickupDate).subscribe(data => {
      this.watcher_service.update_ordercreated_time(data);
    })
  }

  filter_changed(event: any) {
    var order_data = [];
    this.order_present = false;
    this.report_service.get_order_status(event.url).subscribe(data => {
      order_data = data.data;
      this.report_service.update_orderstatus_store(data);
      this._blowhorn_service.hide_loader();
      if (order_data.length > 0) {
        this.order_present = true;
      }
    })

    this.report_service.get_order_timeplaced(event.url).subscribe(data => {
      this.report_service.update_ordertimeplaced_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_order_paymentmode(event.url).subscribe(data => {
      this.report_service.update_orderpaymentmode_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_order_attempts(event.url).subscribe(data => {
      this.report_service.update_orderattempts_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_order_placed(event.url).subscribe(data => {
      this.report_service.update_orderplaced_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_order_deliveryperformance(event.url).subscribe(data => {
      this.report_service.update_orderdeliveryperformance_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_order_samedaydelivery(event.url).subscribe(data => {
      this.report_service.update_ordersamedaydelivery_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_order_sla(event.url).subscribe(data => {
      this.report_service.update_order_sla_store(data);
      this._blowhorn_service.hide_loader();
    })

    this.report_service.get_order_autodispatch(event.url).subscribe(data => {
      this.report_service.update_orderautodispatch_store(data);
      this._blowhorn_service.hide_loader();
    })
  }
}
