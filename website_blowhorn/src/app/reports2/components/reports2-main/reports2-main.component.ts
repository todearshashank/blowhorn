import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import html2canvas from 'html2canvas';
import { ActivatedRoute } from '@angular/router';
import { ReportFeedbackFormComponent } from '../report-feedback-form/report-feedback-form.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddDeleteScheduleComponent } from '../add-delete-schedule/add-delete-schedule.component';
import { ReportScheduleService } from '../../../shared/services/report-schedule.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
    selector: 'app-reports2-main',
    templateUrl: './reports2-main.component.html',
    styleUrls: ['./reports2-main.component.scss']
})
export class Reports2MainComponent implements OnInit {
    report_schedules: any;
    @Output() emitService: EventEmitter<any>;

    links = [];
    link_styles = { margin: '0px' };
    navbar_styles = {
        padding: '0px',
        boxShadow: '0px 0px 0px 0px transparent',
        border: 'none'
    };
    can_view_schedule_reports = false;

    constructor(
        private route: ActivatedRoute,
        private modalService: NgbModal,
        public rs_service: ReportScheduleService,
        public _blowhorn_service: BlowhornService,
    ) {
        rs_service.get_report_schedules().subscribe(
            response => {
                this.report_schedules = response;
                this._blowhorn_service.hide_loader();
            },
            err => {
                console.error(err);
                this._blowhorn_service.hide_loader();
            });
    }

    ngOnInit() {
        let can_view_order_reports = this._blowhorn_service.hasPermission('analytics', 'can_view_order_reports');
        let can_view_trip_reports = this._blowhorn_service.hasPermission('analytics', 'can_view_trip_reports');
        let can_view_heatmaps = this._blowhorn_service.hasPermission('analytics', 'can_view_heatmaps');
        this.can_view_schedule_reports = this._blowhorn_service.hasPermission('analytics', 'can_view_schedule_reports');
        if (can_view_order_reports) {
            this.links.push({ link: 'order', text: 'Order Based Reports' })
        }
        if (can_view_trip_reports) {
            this.links.push({ link: 'trip', text: 'Trip Based Reports' })
        }
        if (can_view_heatmaps) {
            this.links.push({ link: 'heatmaps', text: 'Heatmaps' })
        }
        if (this.can_view_schedule_reports) {
            this.links.push({ link: 'schedule', text: 'Schedule Reports' })
        }
    }

    open_feedback_modal(): void {
        const modalRef = this.modalService.open(ReportFeedbackFormComponent);
        modalRef.componentInstance.emitService.subscribe((result: any) => {
            setTimeout(() => this.modalService.dismissAll(false), 3000)
        },
        (err: any) => {
            console.error('Error', err);
        });
    }

    download_documents(event: string): void {
        var data = document.body;
        html2canvas(data, { scrollY: -window.scrollY, allowTaint: true }).then(function (canvas) {
            var link = document.createElement("a");
            document.body.appendChild(link);
            link.download = "html_image.png";
            link.href = canvas.toDataURL("image/png");
            link.target = '_blank';
            link.click();

        });
    }

    open_member_modal(): void {
        const modalRef = this.modalService.open(AddDeleteScheduleComponent);
        modalRef.componentInstance.schedule = {};
        modalRef.componentInstance.operation = 'Add';
        modalRef.componentInstance.emitService.subscribe((result: any) => {
            let index = this.report_schedules.findIndex((item: any) => item.name === result.name);
            this.report_schedules.splice(index > -1 ? index : 0, index > -1 ? 1 : 0, result);
            setTimeout(() => this.modalService.dismissAll(false), 1000)
        }, (err: any) => {
            console.error('Error', err);
        });
    }

}
