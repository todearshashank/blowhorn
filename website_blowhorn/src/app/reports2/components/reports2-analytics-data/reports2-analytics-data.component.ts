
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-reports2-analytics-data',
  templateUrl: './reports2-analytics-data.component.html',
  styleUrls: ['./reports2-analytics-data.component.scss']
})
export class Reports2AnalyticsDataComponent implements OnInit {


  @ViewChild('overlay_attempt_details', {static: false})
  maximise: ElementRef;
  @Input()
  delivery_percentage = 0;
  @Input()
  delivered_orders = 0;
  @Input()
  total_orders = 0;
  @Input()
  sdd_count = 0;
  @Input()
  sdd_percentage = 0;
  @Input()
  cod_count = 0;
  @Input()
  cod_percentage = 0;
  statistics = {
    total_orders: {
        label: 'Total Shipment Orders',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(`<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px" viewBox="0 0 24 24" fill="none" stroke="rgb(0, 207, 232)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>`),
        styles: {
            avatar: {
                background: 'rgb(0, 207, 232, 0.12)',
            },
            icon: {
                color: '#00cfe8'
            }
        }
    },
    delivered_orders: {
        label: 'Delivered Orders',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(`<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px" viewBox="0 0 24 24" fill="none" stroke="#28c76f" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>`),
        styles: {
            avatar: {
                background: 'rgba(40, 199, 111, 0.12)',
            },
            icon: {
                color: '#28c76f'
            }
        }
    },
    sla_fullfilled: {
        label: 'SLA Fulfilled Orders',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(`<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px" viewBox="0 0 24 24" fill="none" stroke="#7367f0" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-award"><circle cx="12" cy="8" r="7"></circle><polyline points="8.21 13.89 7 23 12 20 17 23 15.79 13.88"></polyline></svg>`),
        styles: {
            avatar: {
                background: 'rgba(115, 103, 240, 0.12)',
            },
            icon: {
                color: '#28c76f'
            }
        }
    },
    cod_count: {
        label: 'Cash On Delivery Amount',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa('<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px" viewBox="0 0 24 24" fill="none" stroke="rgb(232, 62, 140)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-credit-card"><rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect><line x1="1" y1="10" x2="23" y2="10"></line></svg>'),
        styles: {
            avatar: {
                background: 'rgba(232, 62, 140, 0.12)',
            },
            icon: {
                color: '#28c76f'
            }
        }
    }
  }

  constructor(
    private report_service: ReportService
    ) {

    this.report_service.order_count_tiles_store.subscribe(data => {
      this.delivery_percentage = data.percentage || 0;
      this.delivered_orders = data.delivered || 0;
      this.total_orders = data.total_orders || 0;

      this.statistics.total_orders.value = `${this.total_orders}`;
      this.statistics.delivered_orders.value = `${this.delivered_orders} (${this.delivery_percentage}%)`;
    });

    this.report_service.order_sddcount_tiles_store.subscribe(data => {
      this.sdd_count = data.sdd_total || 0;
      this.sdd_percentage = data.percentage || 0;
      this.statistics.sla_fullfilled.value = `${this.sdd_count} (${this.sdd_percentage}%)`;
    });

    this.cod_percentage = 0;
    this.report_service.order_codsum_tiles_store.subscribe(data => {
      this.cod_count = data.cod || 0;
      this.cod_percentage = data.percentage || 0;
      this.statistics.cod_count.value = `${this.cod_count} (${this.cod_percentage}%)`;
    });
  }

  ngOnInit() {

  }

}
