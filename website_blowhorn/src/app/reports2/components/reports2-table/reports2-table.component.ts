import { Component, OnInit } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ActivatedRoute } from '@angular/router';
import { ReportService } from '../../../shared/services/report.service';


@Component({
  selector: 'app-reports2-table',
  templateUrl: './reports2-table.component.html',
  styleUrls: ['./reports2-table.component.scss']
})
export class Reports2TableComponent implements OnInit {

  hubwise_data;
  slot = [];
  hubname_values;
  time_slot;
  hubname_counts;
  hubname_key: any;
  counts: any[];
  hubwise_name: string[];
  error_msg = 'No records found';

  data = [];
  constructor(
    private route: ActivatedRoute,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private report_service: ReportService
  ) {
    this.watcher_service.hubwise_data.subscribe(hubwise_data => {
      this.hubwise_data = hubwise_data;
      this.hubwise_name = Object.keys(hubwise_data);
      this.data = [];
      for (const key in this.hubwise_data) {
        this.slot = Object.keys(this.hubwise_data[key]);
        let _dict = {
          hub: key
        };
        let slots = [];
        for (let k in this.hubwise_data[key]) {
          slots.push(this.hubwise_data[key][k]);
        }
        _dict['slots'] = slots;
        this.data.push(_dict)
      }
    })
  }

  ngOnInit() {
  }

}