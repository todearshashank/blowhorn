import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { MemberService } from '../../../shared/services/member.service';
import { ReportScheduleService } from '../../../shared/services/report-schedule.service';

@Component({
  selector: 'app-user-subscription',
  templateUrl: './user-subscription.component.html',
  styleUrls: ['./user-subscription.component.scss']
})
export class UserSubscriptionComponent implements OnInit {

  @Output() emitService: EventEmitter<any>;

  available_users = [];
  change_event = false;
  selected_users = [];
  subscribed_users = [];
  formError = null;
  selected_schedule_id = null;
  response_data = {};
  multiselect_action_buttons = {
    select: {
        label: '⇨',
        type: '',
        disabled: false,
        title: 'Select users for subscription'
    },
    deselect: {
        label: '⇦',
        type: '',
        disabled: false,
        title: 'Remove selected users from subscription'
    }
  };

  header_text = 'User Subscription';
  submit_button = {
    type: 'save',
    label: 'Save',
    disabled: false
  };

  dismiss_button = {
      label: 'Cancel',
      type: 'delete',
      disabled: false
  };

  show = {
    request_loader: false,
  };

  constructor(
    public activeModal: NgbActiveModal,
    public member_service: MemberService,
    public rs_service: ReportScheduleService,
    public bh_service: BlowhornService

    ) {
    this.emitService  = new EventEmitter<any>();
  }

  ngOnInit() {
      this.change_event = false;
      this.selected_users = [];
      if (this.available_users) {
        this.available_users.forEach(item => {
          item.selected = false;
        });
      }

      if (this.subscribed_users) {
        this.subscribed_users.forEach(item => {
          let index = this.available_users.findIndex(i=> i.id === item);
          if (index > -1) {
            let user = this.available_users[index];
            user.selected = true;
          }
        });
    }
  }

  users_changed(event): void {
      this.change_event = true;
      event.forEach(item => {
        item.selected = false;
      });
      this.selected_users = event;
      this.formError = null;
  }

  save_users_for_schedule(): void {
    let temp_users = [];
    this.submit_button.disabled = true;
    let user_absence = false;
    this.formError = null;
    this.response_data['schedule_id'] = this.selected_schedule_id;
    if (!this.change_event) {
      this.submit_button.disabled = false;
      this.formError = 'There are no changes in the user subscription list';
      return;
    }

    if (this.selected_users) {
      this.selected_users.forEach(item => {
          temp_users.push(item.id)
          if (this.subscribed_users && !this.subscribed_users.includes(item.id)) {
            user_absence = true;
          }
        });
    }

    let subscriber_length = 0;
    if (this.subscribed_users) {
      subscriber_length = this.subscribed_users.length
    }

    if (temp_users.length === subscriber_length && !user_absence) {
      this.submit_button.disabled = false;
      this.formError = 'There are no changes in the user subscription list';
      return;
    }

    this.show.request_loader = true;
    this.response_data['user_list'] = temp_users;
    this.rs_service.update_report_schedule_users(this.response_data).subscribe(response => {
        this.emitService.next(temp_users);
        this.submit_button.disabled = false;
        this.show.request_loader = false;
    }, err => {
          this.formError = 'Please try again in sometime'
          console.log(err);
          this.submit_button.disabled = false;
          this.show.request_loader = false
    });
  }

}
