import { Component, OnInit } from '@angular/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-tbr-topinfo-tile',
  templateUrl: './tbr-topinfo-tile.component.html',
  styleUrls: ['./tbr-topinfo-tile.component.scss']
})
export class TbrTopinfoTileComponent implements OnInit {

  total_trips = 0;
  active_trips = 0;
  avg_time = 0;
  avg_distance = 0;

  statistics = {
    total_trips: {
        label: 'Total Trips',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(`<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px" viewBox="0 0 24 24" fill="none" stroke="#28c76f" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>`),
        styles: {
            avatar: {
                background: 'rgba(40, 199, 111, 0.12)',
            },
            icon: {
                color: '#00cfe8'
            }
        }
    },
    active_trips: {
        label: 'Active Trips',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(`<svg xmlns="http://www.w3.org/2000/svg" width="21px" height="21px" viewBox="0 0 24 24" fill="none" stroke="rgb(115, 103, 240)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>`),
        styles: {
            avatar: {
                background: 'rgba(115, 103, 240, 0.12)',
            },
            icon: {
                color: '#00cfe8'
            }
        }
    },
    avg_time: {
        label: 'Avg Time',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(`<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="rgb(0, 207, 232)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock"><circle cx="12" cy="12" r="10"></circle><polyline points="12 6 12 12 16 14"></polyline></svg>`),
        styles: {
            avatar: {
                background: 'rgba(0, 207, 232, 0.12)',
            },
            icon: {
                color: '#00cfe8'
            }
        }
    },
    avg_distance: {
        label: 'Avg Distance',
        value: '',
        icon: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(`<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="rgb(232, 62, 140)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-navigation"><polygon points="3 11 22 2 13 21 11 13 3 11"></polygon></svg>`),
        styles: {
            avatar: {
                background: 'rgba(232, 62, 140, 0.12)',
            },
            icon: {
                color: '#00cfe8'
            }
        }
    },
}

  constructor(private report_service: ReportService,
    public _blowhorn_service: BlowhornService,) {
    this.total_trips = 0;
    this.active_trips = 0;
    this.avg_time = 0;
    this.avg_distance = 0;

    this.report_service.tripallstats_store.subscribe(data => {

      if (data.data) {
        if (data.data._count) {
          this.total_trips = data.data._count.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
        if (data.data._time) {
          this.active_trips = data.data._acount.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
        if (data.data._time) {
          this.avg_time = data.data._time.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
        if (data.data._distance) {
          this.avg_distance = data.data._distance.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
      };

    });
  }

  ngOnInit() {
  }

}
