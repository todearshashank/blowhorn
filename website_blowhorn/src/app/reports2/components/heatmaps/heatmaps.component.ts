import { Constants } from './../../../shared/utils/constants';
import { HeatmapService } from './../../../shared/services/heatmap.service';
import { ReportService } from './../../../shared/services/report.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IdName } from '../../../shared/models/id-name.model';
import { City } from '../../../shared/models/city.model';
import { WatcherService } from '../../../shared/services/watcher.service';
import { ActivatedRoute } from '@angular/router';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

declare var google: any;


@Component({
    selector: 'app-heatmaps',
    templateUrl: './heatmaps.component.html',
    styleUrls: ['./heatmaps.component.scss']
})
export class HeatmapsComponent implements OnInit, AfterViewInit {

    master_data = [];
    data_set: google.maps.LatLng[] = [];
    data_set_pickup: google.maps.LatLng[] = [];
    data_set_dropoff: google.maps.LatLng[] = [];
    center: google.maps.LatLng | google.maps.LatLngLiteral = { lat: 12.9716, lng: 77.5946 };
    total_orders = 0;
    start_date: string;
    end_date: string;
    http_params = {};
    date_range: [Date, Date] = [null, null];
    radius = 20;
    customer_types: IdName[] = [new IdName(1, 'Individual'), new IdName(2, 'Non Enterprise')];
    cities: City[] = [];
    order_types: IdName[] = [new IdName(1, 'Shipment Orders'), new IdName(2, 'Subscription Orders')];
    cancellation_reasons: IdName[] = [];
    hubs: IdName[] = [];
    hubs_backup: IdName[] = [];
    vehicle_types: IdName[] = [];
    statuses: IdName[] = [];
    selected_city_id: number = null;
    selected_city_name = 'All Cities';
    selected_hub = null;
    selected_hub_id: number = null;
    selected_hub_name = 'All Hubs';
    selected_ordertype_id: number = null;
    selected_ordertype_name = 'All Orders';
    selected_vehicle_type_id: number = null;
    selected_vehicle_type_name = 'All Vehicle Types';
    selected_customer_type_id: number = null;
    selected_customer_type_name = 'All Customer Types';
    selected_reason_name = 'Select Cancellation Reason';
    selected_reason_id: number = null;
    selected_status_name = 'All Statuses';
    selected_status_id: number = null;
    show_pickup = true;
    show_dropoff = true;
    show_heatmap = true;
    show_marker_cluster = false;
    show_city_limits = false;
    show_hub_limits = false;
    enable_limits = false;
    showSecondaryFilters = false;
    quickDateFilters = Constants.quickDateFilters;
    selected_quick_filter = Constants.quick_date_filter_delta.FILTER_TODAY;
    numOfLocationsWithoutGeopt = {
        pickup: 0,
        dropoff: 0
    }
    coverage: {lat: number, lng: number}[];
    ORDER_CANCELLED = 'Cancelled';

    constructor(
        private _watcher_service: WatcherService,
        private _report_service: ReportService,
        private _heatmap_service: HeatmapService,
        private _route: ActivatedRoute,
        public _blowhorn_service: BlowhornService,
    ) { }

    ngOnInit() {
        this.http_params['quick_filter'] = this.selected_quick_filter;
    }

    initialize(map_loaded: boolean) {
        this.fetch_orders();
    }

    ngAfterViewInit() {
        this.order_type_changed(this.order_types[0]);
        this.fetch_filters();
    }

    get_latlng(arr: any[]): google.maps.LatLng {
        return new google.maps.LatLng(arr[0], arr[1]);
    }

    reset_date(event: any) {
        this.date_range = [null, null];
        delete this.http_params['end_date'];
        delete this.http_params['start_date'];
        this.fetch_orders();
    }

    date_changed(event: any) {
        this.start_date = this.date_range[0].toISOString().substring(0, 10);
        this.end_date = this.date_range[1].toISOString().substring(0, 10);
        this.selected_quick_filter = null;
        this.http_params['end'] = this.end_date;
        this.http_params['start'] = this.start_date;
        delete this.http_params['quick_filter'];
        this.fetch_orders();
    }

    cancellation_reason_changed(event: any) {
        if (!event.id || this.selected_ordertype_name !== this.ORDER_CANCELLED) {
            this.load_data(this.master_data);
            return;
        }
        let filtered_data = [];
        this.master_data.forEach(item => {
            if (item.status === this.ORDER_CANCELLED &&
                item.reason_for_cancellation === event.name) {
                filtered_data.push(item);
            }
        });
        this.load_data(filtered_data);
    }

    city_changed(city: {}) {
        this.hubs = [];
        const city_from_cities = this.cities.find(item => item.id === city['id']);
        this.selected_city_id = city['id'];
        if (city_from_cities) {
            this.center = {
                lat: city_from_cities.geopoint[1],
                lng: city_from_cities.geopoint[0]
            };
            this.hubs.push(...city_from_cities.hubs);
            this.http_params['city_id'] = city['id'];
            this.enable_limits = true;
        } else {
            this.hubs.push(...this.hubs_backup);
            delete this.http_params['city_id'];
            this.enable_limits = false;
            this.show_city_limits = false;
            this.show_hub_limits = false;
        }
        this.fetch_orders();
    }

    hub_changed(hub: any) {
        if (hub && hub['id']) {
            this.selected_hub_id = hub.id;
            this.selected_hub = this.hubs_backup.find(i => i.id === hub.id);
            this.http_params['hub_id'] = hub['id'];
        } else {
            this.selected_hub_id = null;
            delete this.http_params['hub_id'];
        }
        this.fetch_orders();
    }

    vehicle_type_changed(vehicle_type: {}) {
        if (vehicle_type && vehicle_type['id']) {
            this.http_params['vehicle_class_id'] = vehicle_type['id'];
        } else {
            delete this.http_params['vehicle_class_id'];
        }
        this.fetch_orders();
    }

    customer_type_changed(customer_type: {}) {
        if (customer_type && customer_type['id']) {
            this.http_params['customer_category'] = customer_type['name'];
        } else {
            delete this.http_params['customer_category'];
        }
        this.fetch_orders();
    }

    order_type_changed(val: any) {
        if (!val) {
            val = this.order_types[0];
        }
        this.selected_ordertype_name = val && val.id ? val.name : null;
        if (val && val.id) {
            this.http_params['order_type'] = val.id === 1 ? 'shipment' : 'subscription';
        } else {
            delete this.http_params['order_type'];
        }
        this.fetch_orders();
    }

    status_changed(val: any) {
        if (val && val.id) {
            this.http_params['status'] = val.name;
        } else {
            delete this.http_params['status'];
        }
        this.fetch_orders();
    }

    fetch_orders() {
        this._blowhorn_service.show_loader();
        this._report_service
            .get_order_location_data(this.http_params)
            .subscribe((data: any[]) => {
                this.master_data = data;
                this.load_data(data);
            },
            err => {
                this._blowhorn_service.hide_loader();
            });
    }

    fetch_filters() {
        this._heatmap_service
            .get_heapmap_order_filters()
            .subscribe(data => {
                this.cities = data.cities;
                this.hubs = [];
                this.hubs_backup = [];
                this.cities.forEach(item => {
                    this.hubs.push(...item.hubs);
                    this.hubs_backup.push(...item.hubs);
                });
                this.statuses = [];
                data.statuses.forEach((item: any, index: number) => {
                    this.statuses.push(new IdName(index, item));
                });
                this.statuses.sort()
            });
    }

    load_data(data: any[]) {
        this.data_set_dropoff = [];
        this.data_set_pickup = [];
        this.numOfLocationsWithoutGeopt.pickup = 0;
        this.numOfLocationsWithoutGeopt.dropoff = 0;
        data.forEach(item => {
            if (item.pickup_lat && item.pickup_lng) {
                this.data_set_pickup.push(new google.maps.LatLng(item.pickup_lat, item.pickup_lng));
            } else {
                this.numOfLocationsWithoutGeopt.pickup++;
            }
            if (item.dropoff_lat && item.dropoff_lng) {
                this.data_set_dropoff.push(new google.maps.LatLng(item.dropoff_lat, item.dropoff_lng));
            } else {
                this.numOfLocationsWithoutGeopt.dropoff++;
            }
        });
        this.merge_pickup_dropoff();
        this.total_orders = data.length;
        this._blowhorn_service.hide_loader();
    }

    merge_pickup_dropoff() {
        this.data_set = [];
        if (this.show_pickup && this.show_dropoff) {
            this.data_set = [...this.data_set_pickup, ...this.data_set_dropoff];
        } else if (this.show_pickup) {
            this.data_set = [...this.data_set_pickup];
        } else if (this.show_dropoff) {
            this.data_set = [...this.data_set_dropoff];
        }
    }

    show_hide_limits(event: { [x: string]: boolean; }) {
        this.show_city_limits = event['checked'];
        if (this.show_city_limits) {
            this.coverage = this._blowhorn_service.get_city_coverage(this.selected_city_id, 'shipment');
        }
    }

    show_hide_hub_limits(event: { [x: string]: boolean; }) {
        this.show_hub_limits = event['checked'];
        console.log('this.selected_hub', this.selected_hub)
        if (this.show_hub_limits && this.selected_hub.coverage && this.selected_hub.coverage.coordinates.length) {
            this.coverage = this.selected_hub.coverage.coordinates[0].map((gpt: any[]) => {
                return {
                    lat: gpt[1],
                    lng: gpt[0]
                }
            })
            console.log('this.this.coverage', this.coverage);
        }
    }

    dropoff_changed(event: { checked: boolean; }) {
        this.show_dropoff = event.checked;
        this.merge_pickup_dropoff();
    }

    pickup_changed(event: { checked: boolean; }) {
        this.show_pickup = event.checked;
        this.merge_pickup_dropoff();
    }

    applyQuickFilter(val: number): void {
        if (val === this.selected_quick_filter) {
            return;
        }
        this.selected_quick_filter = val;
        this.http_params['quick_filter'] = val;
        this.date_range = [null, null];
        this.fetch_orders();
      }

    switch_view(event: { target: { value: string; }; }) {
        if (event.target.value === 'heatmap') {
            this.show_marker_cluster = false;
            this.show_heatmap = true;
        } else {
            this.show_marker_cluster = true;
            this.show_heatmap = false;
        }
    }

    check_radius(event: { keyCode: number; stopPropagation: () => void; preventDefault: () => void; }) {
        if (!((event.keyCode > 95 && event.keyCode < 106)
            || (event.keyCode > 47 && event.keyCode < 58)
            || event.keyCode === 8)) {
            event.stopPropagation();
            event.preventDefault();
            return false;
        }
    }

    get city_styles(): {} {
        return {
            'dropdown-styles': {
                border: 'none',
                height: '40px',
                width: '150px',
                'box-shadow': 'none',
                'padding-left': '5px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px'
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                width: '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get cancellation_reason_styles(): {} {
        return {
            'dropdown-styles': {
                border: 'none',
                height: '40px',
                width: '190px',
                'box-shadow': 'none',
                'padding-left': '5px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px'
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                width: '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get hub_styles(): {} {
        return {
            'dropdown-styles': {
                border: 'none',
                height: '40px',
                width: '200px',
                'box-shadow': 'none',
                'padding-left': '5px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px',
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                width: '250px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get order_type_styles(): {} {
        return {
            'dropdown-styles': {
                border: 'none',
                height: '40px',
                width: '200px',
                'box-shadow': 'none',
                'padding-left': '5px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px',
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                width: '250px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get status_styles(): {} {
        return {
            'dropdown-styles': {
                border: 'none',
                height: '40px',
                width: '120px',
                'box-shadow': 'none',
                'padding-left': '5px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px',
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                width: '250px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get vehicle_type_styles(): {} {
        return {
            'dropdown-styles': {
                border: 'none',
                height: '40px',
                width: '150px',
                'box-shadow': 'none',
                'padding-left': '5px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px',
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                width: '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

    get customer_type_styles(): {} {
        return {
            'dropdown-styles': {
                border: 'none',
                height: '40px',
                width: '150px',
                'box-shadow': 'none',
                'padding-left': '5px',
                'z-index': 2
            },
            'dropdown-toggle-styles': {
                'padding-left': '0px',
            },
            'dropdown-menu-styles': {
                marginTop: '3px',
                width: '200px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
            }
        };
    }

}
