import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ngxCsv } from 'ngx-csv';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-report2-order-samedaydelivery-distribution',
  templateUrl: './report2-order-samedaydelivery-distribution.component.html',
  styleUrls: ['./report2-order-samedaydelivery-distribution.component.scss']
})
export class Report2OrderSamedaydeliveryDistributionComponent implements OnInit {

  BarChart;
  content = [];
  sdd_arr = [];
  ndd_arr = [];
  trend = 'Trend';
  barChartLabel = [];
  tile_data = {};
  sdd_sum = 0;
  total_sum = 0;
  sla_string = '';
  error_msg = 'No data found';

  constructor(private report_service: ReportService) {

      this.sdd_arr = [], this.ndd_arr = [];

      this.report_service.ordersamedaydelivery_store.subscribe(data => {
      this.sdd_arr = [], this.ndd_arr = [];
      this.sdd_sum = 0, this.total_sum = 0;
      this.barChartLabel = [];
      this.sla_string = data.sla;

      this.content = data.data || [];
      let header = data.trend;
      
      if (header) {
          this.trend = header;
      };

      this.content.forEach(item => {
        let sdd = Number(((item.sameday_delivered_count / item.total_delivered_count) * 100).toFixed(1));
        let ndd = (100-sdd).toFixed(1);
        this.sdd_sum += Number(item.sameday_delivered_count)
        this.total_sum += Number(item.total_delivered_count)
        
        if (Number(item.sameday_delivered_count) !== 0) {
          this.sdd_arr.push(sdd);
          this.ndd_arr.push(ndd);
          this.barChartLabel.push(item.trend);
          };
        });

        this.tile_data = { percentage : 0, sdd_total : this.sdd_sum};
      
        if (this.sdd_sum !== 0) {
          this.tile_data['percentage'] = Number((this.sdd_sum/this.total_sum * 100).toFixed(1));
        };

        this.report_service.update_order_sddcount_tiles_store(this.tile_data);


      if (this.BarChart) {
        this.BarChart.destroy();
      }

      this.draw_bar_chart();

    });

   }

  ngOnInit() {
  }

  draw_bar_chart() {
    this.BarChart = new Chart('barChart3', {
      type: 'bar',
      data: {
        labels: this.barChartLabel,
        datasets: [
            {
              label: 'Fulfilled',
              data: this.sdd_arr,
              backgroundColor: "#7189BF",
              hoverBorderWidth: 0
            },
            {
              label: 'Unfulfilled',
              data: this.ndd_arr,
              backgroundColor: "#DF7599",
              hoverBorderWidth: 0
            },
      ]

      },
      options: {
        plugins: {
          datalabels: {
          color: 'white',
          anchor: 'end',
          align: function(context) {
              return context.dataset.data[context.dataIndex] < 10 ? 'top' : 'bottom';
            },
          formatter: function(value) {
            var temp = Number(value);
            if (temp > 0) {
              return temp + '%';
            };
            return "";
          }
        }
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel + '%';
          },
         }
        },
        legend: {
            display: true,
            position: "top",
            labels: {
              fontSize: 12
            }
        },
        scales : {
          yAxes: [{
                stacked: true,
                scaleLabel: {
                  display: true,
                  fontSize: 13
                },
                ticks: {
                  beginAtZero: true,
                  fontSize: 13,
                  stepSize: 25,
                  callback: function(value, index, values) {
                    return value + '%';
                  }
                }
              }],
              xAxes: [{
                stacked: true,
                gridLines: {
                    display: false
                },
                scaleLabel: {
                  display: true,
                  labelString: this.trend || 'Trend',
                  fontSize: 14
                },
              }]
        },
      }
    })
  }

  download_documents(event: string): void {
    new ngxCsv(this.content, 'Order Sameday Delivery Distribution');
  }

  download_charts(): void {
    let base64 = this.BarChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'chart.png', 'png');
  }
}


