import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportService } from '../../../shared/services/report.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-report2-order-paymentmode',
  templateUrl: './report2-order-paymentmode.component.html',
  styleUrls: ['./report2-order-paymentmode.component.scss']
})
export class Report2OrderPaymentmodeComponent implements OnInit {

  BarChart;
  barChartdata = [];
  barChartLabel = ['COD', 'Prepaid'];
  total_order_count = 0;
  cod_order_count = 0;
  cod_sum = 0;
  cod_per = 0;
  
  prepaid_sum = 0;
  prepaid_order_count = 0;
  error_msg = 'No data found';
  showInfo = false;

  constructor(private report_service: ReportService,
              public blowhorn_service: BlowhornService) {

      this.barChartdata = [];
      this.cod_per = 0;
      this.cod_sum = 0;
      this.total_order_count = 0;
      this.cod_order_count = 0;
      this.prepaid_order_count = 0;

      this.report_service.orderpaymentmode_store.subscribe(data => {
        this.cod_sum = Number(data.total_cod || 0);
        this.total_order_count = Number(data.total_orders || 0);
        this.cod_order_count = Number(data.total_cod_orders || 0);
        this.prepaid_order_count = this.total_order_count - this.cod_order_count;
      
      this.barChartdata = [this.cod_order_count, this.prepaid_order_count];
      if (this.cod_order_count !== 0) {
          this.cod_per = Number((this.cod_order_count / this.total_order_count * 100).toFixed(1));
      };

      if (this.cod_sum !== NaN) {
          this.report_service.update_order_codsum_tiles_store(
              {cod : this.cod_sum, percentage: this.cod_per});
      };

      if (this.BarChart) {
        this.BarChart.destroy();
      }
      this.draw_bar_chart();

    });

    

   }

  ngOnInit() {
  }

  draw_bar_chart() {
    this.BarChart = new Chart('barChart1', {
      type: 'doughnut',
      data: {
        labels: this.barChartLabel,
        datasets: [{
          data: this.barChartdata,
          borderWidth: 0,
          backgroundColor: [
            "#7CDDDD", "#FFC785"
          ]
        }]

      },
      options: {
        plugins: {
          datalabels: {
            formatter: function(value) {
              if (Number(value) > 0) {
                return value + " orders";
              };
              return "";
              },
              font: {
                weight: 'bold',
                size: 14,
              }
          }
        },
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        cutoutPercentage: 60,
        legend: {
          display: true,
          position: "bottom",
        },
      }
    })
  }

  download_charts(): void {
    let base64 = this.BarChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'chart.png', 'png');
  }
}
