import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-tbr-contracttype-stats',
  templateUrl: './tbr-contracttype-stats.component.html',
  styleUrls: ['./tbr-contracttype-stats.component.scss']
})
export class TbrContracttypeStatsComponent implements OnInit {

  PieChart;
  statuswiseData: any;
  statuswise_count = [];
  charts = [];
  statusData = [];
  error_msg = 'No records found';
  showInfo = false;
  info_data = {};
  sum = 0;

  constructor(
    private report_service: ReportService
  ) {
    this.report_service.tripcontracttype_store.subscribe(data => {
      this.statuswiseData = [];
      this.info_data = {};
      this.statuswise_count = data.data || [];
      this.statusData = [];
      this.sum = 0;

      // calculate sum for finding the percentage
      this.statuswise_count.forEach(item => {
        this.sum += Number(item.count);
      });

      // loop thru the input data to calculate the pecentage for display
      this.statuswise_count.forEach(item => {
          let count = 0;
          let info_item = item;
          info_item.percentage = 0;

          // calculate percentage if value > 0
          this.info_data[item.customer_contract__contract_type] = 0;
          if (Number(item.count) > 0) {
            count = Number((Number(item.count)/this.sum * 100).toFixed(2));
            info_item.percentage = count;
            this.statuswiseData.push(count);
            this.statusData.push(item.customer_contract__contract_type);
            this.info_data[item.customer_contract__contract_type] = item.count;

          };

      });
  
      if (this.PieChart) {
        this.PieChart.destroy();
      }
      setTimeout(() => this.draw_pie_chart(), 2000)
    });
   }

  ngOnInit() {
    
  }


  draw_pie_chart() {
    this.PieChart = new Chart('tripcontracttypechart', {
      type: 'doughnut',
      data: {
        datasets: [{
          data: this.statuswiseData,
          backgroundColor: [
            "#FFC857", "#C8C85E", "#E61D8C", "#FFC785", 
            "#eda1ab", "#7189BF", "#d698b9"
          ]
        }],
        labels: this.statusData,

      },
      options: {
        cutoutPercentage: 75,
        plugins: {
            datalabels: {
              formatter: function(value) {
                var temp = Number(value);
                if (Number(temp) > 0) {
                  return temp + '%';
                };
                return "";
                }
            }
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            let lab = data.labels[tooltipItem.index] + 
                        " : " + 
                        data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + 
                        "%";
            return lab;
          },
         }
        },
        animation: {
          duration: 4000
        },
        legend: { 
          display: true,
          position: "bottom",
          labels: {
            fontSize: 12,
          }
        }
      }
    })
  }

  download_documents(event: string): void {
    new ngxCsv(this.statuswise_count, 'Order Status Report');
  }

  download_charts(): void {
    let base64 = this.PieChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'image.png', 'png');
  }

}
