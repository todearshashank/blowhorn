import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-report2-order-deliveryperformance',
  templateUrl: './report2-order-deliveryperformance.component.html',
  styleUrls: ['./report2-order-deliveryperformance.component.scss']
})
export class Report2OrderDeliveryperformanceComponent implements OnInit {

  @ViewChild('screen', { read: ElementRef, static: false }) pdfForm: ElementRef;
  content = [];
  header = [];
  footer  = [];
  header_keys = [];
  show_error_msg = true;
  error_msg = 'No data found';
  input_data = [];

  constructor(private report_service: ReportService) {

    this.report_service.orderdeliveryperformance_store.subscribe(data => {
        // loop the input data to set the values
        this.input_data = [];
        this.content = data.data || [];
        let header = data.header;
        this.footer = data.footer;

        if (header) {
          this.header = Object.values(header);
          this.header_keys = Object.keys(header);
        }

        if (this.content.length > 0) {
          this.show_error_msg = false;
        }
    });
 }

  ngOnInit() {
  }

  download_documents(event: string): void {
        var container = document.getElementById("delchart");
        html2canvas(container, {scrollY: -window.scrollY}).then(function (canvas) {

            var link = document.createElement("a");
            document.body.appendChild(link);
            link.download = "delivery_performance.jpg";
            link.href = canvas.toDataURL();
            link.target = '_blank';
            link.click();
        });

  }

}
