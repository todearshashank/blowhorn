import { Component, OnInit } from '@angular/core';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ReportService } from '../../../shared/services/report.service';

@Component({
  selector: 'app-tbr-tileinfo-display',
  templateUrl: './tbr-tileinfo-display.component.html',
  styleUrls: ['./tbr-tileinfo-display.component.scss']
})
export class TbrTileinfoDisplayComponent implements OnInit {

  delivered_packages = 0;
  rejected_packages = 0;
  toll_charges = 0;
  parking_charges = 0;
  
  constructor(private report_service: ReportService,
    public _blowhorn_service: BlowhornService,) { 
    this.delivered_packages = 0;
    this.rejected_packages = 0;
    this.toll_charges = 0;
    this.parking_charges = 0;
    
    this.report_service.tripallstats_store.subscribe(data => {
      
      if (data.data) {
        if (data.data._count) {
          this.delivered_packages = data.data._delivered.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
        if (data.data._time) {
          this.rejected_packages = data.data._rejected.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
        if (data.data._toll) {
          this.toll_charges = data.data._toll.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
        if (data.data._parking) {
          this.parking_charges = data.data._parking.toFixed(0) || 0;
          this._blowhorn_service.hide_loader();
        }
      };
      
    });
  }

  ngOnInit() {
  }

}
