import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { BlowhornService } from '../../../shared/services/blowhorn.service';
import { ReportService } from '../../../shared/services/report.service';
import { WatcherService } from '../../../shared/services/watcher.service';

@Component({
  selector: 'app-tbr-area-chart',
  templateUrl: './tbr-area-chart.component.html',
  styleUrls: ['./tbr-area-chart.component.scss']
})
export class TbrAreaChartComponent implements OnInit {

  LineChart;
  option_disabled = true;
  selected_option_id = null;
  selected_option_name = '----------';
  type_disabled = true;
  selected_chart_id = null;
  selected_chart_name = '----------';
  chart_data = {};
  datasets = [];
  lineChartdata = [];
  lineChartLabel = [];

  options = [
    {name : 'Charges', id : 1},
    {name : 'Time', id : 2},
    {name : 'Distance', id : 3},
    {name : 'Packages', id : 4},
    {name : 'Weight/Volume', id : 5}
  ]

  chart_types = [
    {name : 'line', id : 1},
    {name : 'bar', id : 2},
  ]
  constructor(private report_service: ReportService,
    public _blowhorn_service: BlowhornService,) {

    this.report_service.tripstats_store.subscribe(data => {
      this.option_disabled = true;
      this.type_disabled = true;
      this.lineChartLabel = [];
      this.lineChartdata = [];

      if (data && data.data && data.data.length !== 0) {
        this.option_disabled = false;
        this.type_disabled = false;
        this.chart_data = data.data;
        this.selected_option_id = 1;
        this.selected_option_name = 'Charges';
        this.selected_chart_name = 'line'
        this.selected_chart_id = 1
        this.set_data_for_chart(data.data);
        this.datasets = this.create_dataset(this.selected_option_name);
      }

      if (this.LineChart) {
        this.LineChart.destroy();
      }
      setTimeout(() => this.draw_line_chart(), 2000)
      this._blowhorn_service.hide_loader()
    });
  }

  ngOnInit() {
  }

  option_changed(event: any): void {
    this.create_dataset(event.name);
    if (this.LineChart) {
      this.LineChart.destroy();
    }
    
    this.draw_line_chart();
  }

  chart_type_changed(event: any): void {
    this.selected_chart_id = event.id
    this.selected_chart_name = event.name
    if (this.LineChart) {
      this.LineChart.destroy();
    }
    this.draw_line_chart();
  }

  draw_line_chart() {
    this.LineChart = new Chart('tripstatschart', {
      type: this.selected_chart_name,
      data: {
        labels: this.lineChartLabel,
        datasets: this.datasets

      },
      options: {
        plugins: {
          datalabels: {
              display: false,
          },
        },
        animation: {
          duration: 4000
        },
        scales: {
          yAxes: [{
            gridLines: {
              display:false
          }
          }],
          xAxes: [{
            gridLines: {
              display:false
          }
          }]
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            fontSize: 12
          }
        }
      }
    })
  }

  set_data_for_chart(data) {
    let time = [];
    let load_time = [];
    let distance = [];
    let cash = [];
    let toll = [];
    let parking = [];
    let assigned = [];
    let delivered = [];
    let attempted = [];
    let rejected = [];
    let weight = [];
    let volume = [];

    data.forEach(element => {
      this.lineChartLabel.push(element.trend)

      // Time related attributes
      if (element._time) {
        time.push(element._time.toFixed(0) || 0); }
      else {
        time.push(0);
      }

      if (element._load_time) {
        load_time.push(element._load_time.toFixed(0) || 0); }
      else {
        load_time.push(0);
      }
      
      // Distance related attributes
      if (element._distance) {
        distance.push(element._distance.toFixed(0) || 0); }
      else {
        distance.push(0);
      }

      // Cash related attributes
      if (element._cash) {
        cash.push(element._cash.toFixed(0) || 0); }
      else {
        cash.push(0);
      }

      if (element._toll) {
        toll.push(element._toll.toFixed(0) || 0); }
      else {
        toll.push(0);
      }

      if (element._parking) {
        parking.push(element._parking.toFixed(0) || 0); }
      else {
        parking.push(0);
      }

      // Shipment Packages related attributes
      if (element._assigned) {
        assigned.push(element._assigned.toFixed(0) || 0); }
      else {
        assigned.push(0);
      }

      if (element._delivered) {
        delivered.push(element._delivered.toFixed(0) || 0); }
      else {
        delivered.push(0);
      }

      if (element._attempted) {
        attempted.push(element._attempted.toFixed(0) || 0); }
      else {
        attempted.push(0);
      }

      if (element._rejected) {
        rejected.push(element._rejected.toFixed(0) || 0); }
      else {
        rejected.push(0);
      }

      if (element._volume) {
        volume.push(element._volume.toFixed(0) || 0); }
      else {
        volume.push(0);
      }

      if (element._weight) {
        weight.push(element._weight.toFixed(0) || 0); }
      else {
        weight.push(0);
      }

    });

    this.chart_data = {
      'time' : time,
      'load_time' : load_time,
      'distance' : distance,
      'cash' : cash,
      'toll' : toll,
      'parking' : parking,
      'assigned' : assigned,
      'attempted' : attempted,
      'rejected' : rejected,
      'delivered' : delivered,
      'weight' : weight,
      'volume' : volume
    }
    
  }

  create_dataset(option) {
    this.datasets = [];
    var c = document.getElementById("tripstatschart") as HTMLCanvasElement;
    var ctx = c.getContext("2d");
    
    var blue_grd = ctx.createLinearGradient(0, 0, 30, 250);
    blue_grd.addColorStop(0, "#7fd1f1");
    blue_grd.addColorStop(0.75, "#ccecf9"); 
    blue_grd.addColorStop(1, "#e5f5fc");

    if (option === 'Charges') {
        let temp2 = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: false,
          backgroundColor: "#C8C85E",
          borderColor: "#C8C85E",
          pointBorderColor: "#C8C85E",
          pointBackgroundColor: "#C8C85E",
          pointHoverBackgroundColor: "#C8C85E",
          pointHoverBorderColor: "#C8C85E",
        };
        temp2['data'] = this.chart_data['parking']
        temp2['label'] = 'Parking Ticket'
        this.datasets.push(temp2);

        let temp1 = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: false,
          backgroundColor: "#E61D8C",
          borderColor: "#E61D8C",
          pointBorderColor: "#E61D8C",
          pointBackgroundColor: "#E61D8C",
          pointHoverBackgroundColor: "#E61D8C",
          pointHoverBorderColor: "#E61D8C",
          
        };
        temp1['data'] = this.chart_data['toll']
        temp1['label'] = 'Toll Amount'
        this.datasets.push(temp1);

        let temp = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: true,
          backgroundColor: "#ccecf9",
          borderColor: "#20A4F3",
          pointBorderColor: "#20A4F3",
          pointBackgroundColor: "white",
          pointHoverBackgroundColor: "#20A4F3",
          pointHoverBorderColor: "#20A4F3",
        };
        temp['data'] = this.chart_data['cash']
        temp['label'] = 'Cash Collected'
        this.datasets.push(temp);
    }

    if (option === 'Time') {
      let temp1 = {
        label: '',
        data: [],
        lineTension: 0.4,
        borderWidth: 2,
        fill: false,
        backgroundColor: "#E61D8C",
        borderColor: "#E61D8C",
        pointBorderColor: "#E61D8C",
        pointBackgroundColor: "#E61D8C",
        pointHoverBackgroundColor: "#E61D8C",
        pointHoverBorderColor: "#E61D8C",
      };
      temp1['data'] = this.chart_data['load_time']
      temp1['label'] = 'Load Time'
      this.datasets.push(temp1);

      let temp = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: true,
          backgroundColor: blue_grd,
          borderColor: "#20A4F3",
          pointBorderColor: "#20A4F3",
          pointBackgroundColor: "white",
          pointHoverBackgroundColor: "#20A4F3",
          pointHoverBorderColor: "#20A4F3",
      };
      temp['data'] = this.chart_data['time']
      temp['label'] = 'Average Time'
      this.datasets.push(temp);
    }

    if (option === 'Distance') {
      let temp = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: true,
          backgroundColor: blue_grd,
          borderColor: "#20A4F3",
          pointBorderColor: "#20A4F3",
          pointBackgroundColor: "white",
          pointHoverBackgroundColor: "#20A4F3",
          pointHoverBorderColor: "#20A4F3",
      };
      temp['data'] = this.chart_data['distance']
      temp['label'] = 'Distance'
      this.datasets.push(temp);
    }

    if (option === 'Packages') {
      let temp1 = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: false,
          backgroundColor: "#FFC857",
          borderColor: "#FFC857",
          pointBorderColor: "#FFC857",
          pointBackgroundColor: "#FFC857",
          pointHoverBackgroundColor: "#FFC857",
          pointHoverBorderColor: "#FFC857",
      };
      temp1['data'] = this.chart_data['assigned']
      temp1['label'] = 'Assigned'
      this.datasets.push(temp1);

      let temp2 = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: false,
          borderColor: "#C8C85E",
          backgroundColor: "#C8C85E",
          pointBorderColor: "#C8C85E",
          pointBackgroundColor: "#C8C85E",
          pointHoverBackgroundColor: "#C8C85E",
          pointHoverBorderColor: "#C8C85E",
      };
      temp2['data'] = this.chart_data['attempted']
      temp2['label'] = 'Attempted'
      this.datasets.push(temp2);

      let temp3 = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: false,
          borderColor: "#E61D8C",
          backgroundColor: "#E61D8C",
          pointBorderColor: "#E61D8C",
          pointBackgroundColor: "#E61D8C",
          pointHoverBackgroundColor: "#E61D8C",
          pointHoverBorderColor: "#E61D8C",
      };
      temp3['data'] = this.chart_data['rejected']
      temp3['label'] = 'Rejected'
      this.datasets.push(temp3);

      let temp = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: true,
          backgroundColor: blue_grd,
          borderColor: "#20A4F3",
          pointBorderColor: "#20A4F3",
          pointBackgroundColor: "white",
          pointHoverBackgroundColor: "#20A4F3",
          pointHoverBorderColor: "#20A4F3",
      };
      temp['data'] = this.chart_data['delivered']
      temp['label'] = 'Delivered'
      this.datasets.push(temp);
    }

    if (option === 'Weight/Volume') {
      let temp1 = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: false,
          backgroundColor: "#E61D8C",
          borderColor: "#E61D8C",
          pointBorderColor: "#E61D8C",
          pointBackgroundColor: "#E61D8C",
          pointHoverBackgroundColor: "#E61D8C",
          pointHoverBorderColor: "#E61D8C",
      };
      temp1['data'] = this.chart_data['weight']
      temp1['label'] = 'Weight'
      this.datasets.push(temp1);

      let temp = {
          label: '',
          data: [],
          lineTension: 0.4,
          borderWidth: 2,
          fill: true,
          backgroundColor: blue_grd,
          borderColor: "#20A4F3",
          pointBorderColor: "#20A4F3",
          pointBackgroundColor: "white",
          pointHoverBackgroundColor: "#20A4F3",
          pointHoverBorderColor: "#20A4F3",
      };
      temp['data'] = this.chart_data['volume']
      temp['label'] = 'Volume'
      this.datasets.push(temp);
    }

    return this.datasets;
    
  }

  get filter_options_styles(): {} {
    return {
      'dropdown-styles': {
        'box-shadow': 'none',
        'border': 'none',
        'height': '30px',
        'width': '130px',
        'z-index': 2
      },
      'dropdown-toggle-styles': {
        'padding-left': '10px'
      },
      'dropdown-menu-styles': {
        'width': '200px',
        boxShadow: '0px 0px 8px 0px rgba(0, 0 , 0, .5)'
      }
    };
  }

}
