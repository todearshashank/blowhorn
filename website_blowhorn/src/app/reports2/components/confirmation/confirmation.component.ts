import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

    action_buttons = {
        decline: {
            show: true,
            type: 'delete',
            label: 'No'
        },
        confirm: {
            show: true,
            type: 'save',
            label: 'Yes'
        },
    };
    @Input()
    styles = {
        header: {},
        details: {},
        footer: {}
    };
  
    @Output()
    click_fired: EventEmitter<{proceed: boolean}>;

    constructor() {
        this.click_fired = new EventEmitter<{proceed: boolean}>();
    }

    ngOnInit() {
        
    }

    submitForm(proceed: boolean): void {
        this.click_fired.emit({proceed: proceed});
    }
}

