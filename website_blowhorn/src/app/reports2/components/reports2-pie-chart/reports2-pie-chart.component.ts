import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-reports2-pie-chart',
  templateUrl: './reports2-pie-chart.component.html',
  styleUrls: ['./reports2-pie-chart.component.scss']
})
export class Reports2PieChartComponent implements OnInit {
  PieChart;
  statuswiseData: any;
  charts = [];
  statusData = [];
  sum = 0;
  tile_data = {};
  error_msg = 'No records found';
  statuswise_count: any;
  display_information = false;
  showInfo = false;
  info_data = [];

  constructor(
    private report_service: ReportService
  ) {
    this.display_information = false;
    this.report_service.orderstatus_store.subscribe(data => {
      this.statuswiseData = [];
      this.info_data = [];
      this.display_information = false;
      this.statuswise_count = data.data || [];
      this.statusData = [];
      this.sum = 0;
      let delivered_percentage = 0
      let delivered_count = 0;

      // calculate sum for finding the percentage
      this.statuswise_count.forEach(item => {
        this.sum += Number(item.count);
      });

      // loop thru the input data to calculate the pecentage for display
      this.statuswise_count.forEach(item => {
          let count = 0;
          let info_item = item;
          info_item.percentage = 0;

          // calculate percentage if value > 0
          if (Number(item.count) > 0) {
            count = Number((Number(item.count)/this.sum * 100).toFixed(2));
            info_item.percentage = count;
            this.statuswiseData.push(count);
            this.statusData.push(item.status);
          };

          if (item.status == 'Delivered') {
            delivered_count = Number(item.count);
            delivered_percentage = Number(count.toFixed(1));
          };

          this.info_data.push(info_item);
      });
          if (this.sum !== 0) {
            this.tile_data = { percentage : delivered_percentage, 
                delivered : delivered_count,
                total_orders : this.sum   
            };
            this.report_service.update_ordertiles_store(this.tile_data);
      };
      if (this.PieChart) {
        this.PieChart.destroy();
      }
      this.draw_pie_chart();
    });
   }

  ngOnInit() {
    
  }


  draw_pie_chart() {
    this.PieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        datasets: [{
          data: this.statuswiseData,
          backgroundColor: [
            "#ab93c9", "#7CDDDD", "#DF7599", "#FFC785", 
            "#eda1ab", "#7189BF", "#d698b9"
          ]
        }],
        labels: this.statusData,

      },
      options: {
        plugins: {
            datalabels: {
              formatter: function(value) {
                var temp = Number(value).toFixed(0);
                if (Number(temp) > 0) {
                  return temp + '%';
                };
                return "";
                }
            }
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            let lab = data.labels[tooltipItem.index] + 
                        " : " + 
                        data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + 
                        "%";
            return lab;
          },
         }
        },
        animation: {
          duration: 4000
        },
        legend: { 
          display: true,
          position: "right",
          labels: {
            fontSize: 12,
          }
        }
      }
    })
  }

  download_documents(event: string): void {
    new ngxCsv(this.statuswise_count, 'Order Status Report');
  }

  download_charts(): void {
    let base64 = this.PieChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'image.png', 'png');
  }

}
