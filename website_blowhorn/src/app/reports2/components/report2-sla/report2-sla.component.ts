import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-report2-sla',
  templateUrl: './report2-sla.component.html',
  styleUrls: ['./report2-sla.component.scss']
})
export class Report2SlaComponent implements OnInit {

  PieChart;
  statuswiseData: any;
  charts = [];
  statusData = [];
  sum = 0;
  tile_data = {};
  delivered_count = 0;
  error_msg = 'No records found';
  statuswise_count: any;

  constructor(
    private report_service: ReportService
  ) {
    this.report_service.order_sla_store.subscribe(data => {
      this.statuswiseData = [];
      this.delivered_count = 0;
      this.statuswise_count = data.data || [];
      this.statusData = [];
      this.sum = 0;
  
      // calculate sum for finding the percentage
      this.statuswise_count.forEach(item => {
        this.sum += Number(item.count);
      });

      // loop thru the input data to calculate the pecentage for display
      this.statuswise_count.forEach(item => {
        let count = 0;
        

        // calculate percentage if value > 0
        if (Number(item.count) > 0) {
          count = Number((Number(item.count)/this.sum * 100).toFixed(0));
          var label = item.status + " " + item.limit
          this.statusData.push(label);
          this.statuswiseData.push(count);
        };

      });
      
      if (this.PieChart) {
        this.PieChart.destroy();
      }
      this.draw_pie_chart();
    });
   }

  ngOnInit() {
    
  }


  draw_pie_chart() {
    this.PieChart = new Chart('pieChart1', {
      type: 'pie',
      data: {
        datasets: [{
          data: this.statuswiseData,
          backgroundColor: [
            "#7CDDDD", "#ab93c9", "#FFC785", "#DF7599", 
            "#eda1ab", "#7189BF", "#d698b9"
          ]
        }],
        labels: this.statusData,

      },
      options: {
        plugins: {
            datalabels: {
              formatter: function(value) {
                if (Number(value) > 0) {
                  return value + '%';
                };
                return "";
                }
            }
        },
        tooltips: {
          mode: 'label',
          callbacks: {
          label: function(tooltipItem, data) { 
            let lab = data.labels[tooltipItem.index] + 
                        " : " + 
                        data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + 
                        "%";
            return lab;
          },
         }
        },
        animation: {
          duration: 4000
        },
        legend: { 
          display: true,
          position: "right",
          labels: {
            fontSize: 12,
          }
        }
      }
    })
  }

  download_documents(event: string): void {
    new ngxCsv(this.statuswise_count, 'Order SLA');
  }

  download_charts(): void {
    let base64 = this.PieChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'chart.png', 'png');
  }


}

