import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Output, EventEmitter } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

import * as moment from 'moment';
import { ReportService } from '../../../shared/services/report.service';


@Component({
  selector: 'app-reports2-navbar',
  templateUrl: './reports2-navbar.component.html',
  styleUrls: ['./reports2-navbar.component.scss']
})
export class Reports2NavbarComponent implements OnInit {

  @Output()
  filter_changed: EventEmitter<string>;
  @Output()
  search_fired: EventEmitter<void>;
  previous: string;
  date_range: [Date, Date] = [null, null];
  order_number = '';
  batch_id = '';
  status = '';
  FILTER_YESTERDAY = 1;
  FILTER_LAST_WEEK = 8;
  quickFilters = [
    { label: 'Yesterday', val: this.FILTER_YESTERDAY },
    { label: 'Last Week', val: this.FILTER_LAST_WEEK },
  ];
  selected_quick_filter = this.FILTER_YESTERDAY;

  pickupDate = new Date();
  delta_yesterday: any;
  delta_lastweek: any;
  call_update: boolean;

  constructor(private route: ActivatedRoute,
    private watcher_service: WatcherService,
    public _blowhorn_service: BlowhornService,
    private report_service: ReportService
  ) {
    this.filter_changed = new EventEmitter<string>();
  }

  ngOnInit() {
    this.applyQuickFilter(this.FILTER_YESTERDAY, true);

  }

  applyQuickFilter(val: number, call_update): void {
    if (call_update && val === this.selected_quick_filter) {
      this.watcher_service.update_filter_changed_method(
        true
      );
    }

    let delta = moment().subtract({ 'days': val }).format('YYYY-MM-DD');
    this.selected_quick_filter = val;
    // this.date_range = [null, null];
    this.report_service.pickupDate = delta;
    this.watcher_service.update_filter_changed_method(
      true
    );

  }

}