import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportService } from '../../../shared/services/report.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-report2-order-creation-trend',
  templateUrl: './report2-order-creation-trend.component.html',
  styleUrls: ['./report2-order-creation-trend.component.scss']
})
export class Report2OrderCreationTrendComponent implements OnInit {

  LineChart;
  input_data = [];
  lineChartdata = [];
  lineChartLabel = [];
  error_msg = 'No data found';

  constructor(private report_service: ReportService) {

      this.report_service.ordertimeplaced_store.subscribe(data => {
        // loop the input data to set the values
        this.input_data = data.data || [];
        this.lineChartdata = [];
        this.lineChartLabel = [];
        this.input_data.forEach(item => {
        this.lineChartLabel.push(item.trend);
        this.lineChartdata.push(item.count);
      });

      if (this.LineChart) {
        this.LineChart.destroy();
      }
      this.draw_line_chart();

    });

    

   }

  ngOnInit() {
  }

  draw_line_chart() {
    this.LineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: this.lineChartLabel,
        datasets: [{
          label: 'No of Orders',
          data: this.lineChartdata,
          lineTension: 0.3,
          borderColor: "rgb(23,162,184)",
          borderWidth: 1,
          fill: true,
    backgroundColor: "#7CDDDD"
        }]

      },
      options: {
        title: {
          text: "Line Chart",
          display: false,
        },
        animation: {
          duration: 4000
        },
        scales: {
          yAxes: [{
            // stacked: true,
            scaleLabel: {
              display: true,
              labelString: 'Number of Orders',
              fontSize: 15
            },
            ticks: {
              beginAtZero: true,
              fontSize: 15,
              callback: function (value, index, ticks) {
                if (Math.floor(Number(value)) === value) {
                  return value;
              }
            }

            }
          }],
          xAxes: [{
            scaleLabel: {
              display: false,
            }
          }]
        },
        legend: {
          display: false,
        }
        ,
        plugins: {
          datalabels: {
          color: 'black',
          anchor: 'end',
          align: 'top'
        }
        },
      }
    })
  }

  download_documents(event: string): void {
    new ngxCsv(this.input_data, 'Order Creation Trend') 
  }

  download_charts(): void {
    let base64 = this.LineChart.toBase64Image();
    this.report_service.dataURLtoFile(base64, 'chart.png', 'png');
  }
}
