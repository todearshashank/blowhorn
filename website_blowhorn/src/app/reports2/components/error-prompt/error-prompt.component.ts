import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-error-prompt',
  templateUrl: './error-prompt.component.html',
  styleUrls: ['./error-prompt.component.scss']
})
export class ErrorPromptComponent implements OnInit {

    @Input()
    styles = {
        header: {},
        details: {},
        footer: {}
    };

    @Output()
    click_fired: EventEmitter<{proceed: boolean}>;

    action_buttons = {
            decline: {
                show: true,
                type: 'delete',
                label: 'Ok'
            },
        };

    
    constructor() {
        this.click_fired = new EventEmitter<{proceed: boolean}>();
    }

    ngOnInit() {
        
    }

    submitForm(proceed: boolean): void {
        this.click_fired.emit({proceed: proceed});
    }

}
