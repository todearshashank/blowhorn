import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveContactComponent } from './components/save-contact/save-contact.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
  ],
  declarations: [SaveContactComponent],
  exports: [
    SaveContactComponent,
  ]
})
export class ContactModule { }
