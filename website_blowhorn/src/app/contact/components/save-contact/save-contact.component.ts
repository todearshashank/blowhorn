import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WatcherService } from '../../../shared/services/watcher.service';
import { Constants } from '../../../shared/utils/constants';
import { BlowhornService } from '../../../shared/services/blowhorn.service';

@Component({
  selector: 'app-save-contact',
  templateUrl: './save-contact.component.html',
  styleUrls: ['./save-contact.component.scss']
})
export class SaveContactComponent implements OnInit {

  @Input()
  heading: string;
  @Input()
  details: string;
  @Input()
  contact_name: string;
  @Input()
  contact_number: number;
  @Output()
  form_close: EventEmitter<boolean>;
  @Output()
  form_save: EventEmitter<{contact_name: string, contact_number: number}>;
  @Input()
  disabled = false;
  regex = Constants.regex;
  isdCodeStyles = {
    wrapper: {
      margin: '10px 0px',
      height: 'unset',
      width: '40px',
      padding: '5px 4px 0px 4px'
    },
    text: {
      'margin-bottom': '5px'
    }
  };

  constructor(
    private _watcher_service: WatcherService,
    private bhService:BlowhornService,
  ) {
    this.form_close = new EventEmitter<boolean>();
    this.form_save = new EventEmitter<{contact_name: string, contact_number: number}>();
  }

  ngOnInit() {
    this.contact_number = this.bhService.get_national_number(this.contact_number);
  }

  close_form() {
    this.clear_form();
    this.form_close.emit(true);
  }

  save_contact() {
    this.form_save.emit({
      contact_name: this.contact_name,
      contact_number: this.contact_number
    });
  }

  clear_form() {
    this.heading = '';
    this.details = '';
    this.contact_name = '';
    this.contact_number = null;
    this._watcher_service
      .update_show_save_contact(false);
  }
}
