# WebsiteAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

<!-- COMMANDS -->
<!-- TO BUILD ALL FLAVOUR AT ONCE -->
./build_all.sh

<!-- TO BUILD A SINGLE FLAVOUR -->
<!-- Template -->
./build.sh <config> <build> <flavor>

<!-- Commands -->
./build.sh blowhorn prod blowhorn
./build.sh mzansigo prod mzansigo


<!-- -------- BUILD.SH -------- -->
<!-- Template -->
ng build --configuration=<configuration> --output-path=dist/dev/<flavor> && rm -f ../website/static/myfleets/flavor/*.js && rm -f ../website/static/myfleets/flavor/*.css && cp dist/dev/flavor/*.js ../website/static/myfleets/<flavor>/ && cp dist/dev/<flavor>/*.css ../website/static/myfleets/<flavor>/

<!-- Dev -->
ng build --configuration=mzansigo-dev --output-path=dist/dev/mzansigo && rm -f ../website/static/myfleets/mzansigo/*.js && rm -f ../website/static/myfleets/mzansigo/*.css && cp dist/dev/mzansigo/*.js ../website/static/myfleets/mzansigo/ && cp dist/dev/mzansigo/*.css ../website/static/myfleets/mzansigo/

ng build --configuration=dev --output-path=dist/dev/blowhorn && rm -f ../website/static/myfleets/blowhorn/*.js && rm -f ../website/static/myfleets/blowhorn/*.css && cp dist/dev/blowhorn/*.js ../website/static/myfleets/blowhorn/ && cp dist/dev/blowhorn/*.css ../website/static/myfleets/blowhorn/

<!-- Prod -->
ng build --configuration=mzansigo --output-path=dist/prod/mzansigo && rm -f ../website/static/myfleets/mzansigo/*.js && rm -f ../website/static/myfleets/mzansigo/*.css && cp dist/prod/mzansigo/*.js ../website/static/myfleets/mzansigo/ && cp dist/prod/mzansigo/*.css ../website/static/myfleets/mzansigo/

ng build --configuration=blowhorn --output-path=dist/prod/blowhorn && rm -f ../website/static/myfleets/blowhorn/*.js && rm -f ../website/static/myfleets/blowhorn/*.css && cp dist/prod/blowhorn/*.js ../website/static/myfleets/blowhorn/ && cp dist/prod/blowhorn/*.css ../website/static/myfleets/blowhorn/
