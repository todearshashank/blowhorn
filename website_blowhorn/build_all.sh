#!/bin/sh

start_time=$(date +"%T")
echo -e "\e[40;38;5;82m Start time \e[30;48;5;82m $start_time \e[0m"
for i in {1..10} ; do echo -en "\e[48;5;${222}m -- \e[0m" ; done ; echo

./build.sh dev dev blowhorn && ./build.sh blowhorn prod blowhorn

end_time=$(date +"%T")
echo -e "\e[40;38;5;82m End time \e[30;48;5;82m $end_time \e[0m"
