#!/bin/sh

: '
    Config:
        blowhorn:
            dev, blowhorn
        mzansigo:
            mzansigo-dev, mzansigo

    Build: dev, prod
    Flavour: blowhorn, mzansigo

    # All Commands
    ./build.sh dev dev blowhorn
    ./build.sh blowhorn prod blowhorn

    ./build.sh mzansigo-dev dev mzansigo
    ./build.sh mzansigo prod mzansigo
'

echo "Building project.."
echo -e "\e[40;38;5;82m Config \e[30;48;5;82m $1 \e[0m \e[40;38;5;82m Build \e[30;48;5;82m $2 \e[0m \e[40;38;5;82m Flavor \e[30;48;5;82m $3 \e[0m"

ng build --configuration=$1 --output-path=dist/$2/$3 && rm -f ../website/static/myfleets/$3/$2/*.js ../website/static/myfleets/$3/$2/*.css && cp dist/$2/$3/*.js dist/$2/$3/*.css ../website/static/myfleets/$3/$2/
