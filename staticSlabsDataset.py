from blowhorn.contract.models import *
from blowhorn.contract.constants import *
# from matplotlib import pyplot as plt
import pandas as pd
from django.utils.translation import ugettext_lazy as _
import os
from statistics import mean


# ASSUMPTIONS
# Assumption ------- Each Contract interval is PER MONTH
# Assumption ------- Each Contract ONLY 1 DRIVER will do Daily Trips
# Assumption ------- Consider pertrip = daily and convert it to per month (x30)
# Assumption ------- Graphs for 3 categories - i. 1 trip ii. 2 trips iii. 3 or >3 trips

class VehicleSlabs:
    def __init__(self):
        self.baseBuy = 0.0
        self.baseSell = 0.0
        self.leaveFactor = 0.0
        self.leave_deduct_amount = 0.0
        self.trip_one = [[5, 30], [10, 60], [15, 90], [20, 120], [25, 150],
                         [30, 180], [35, 210], [40, 240], [45, 270], [50, 300],
                         [55, 330], [60, 360], [65, 390], [70, 420], [75, 450],
                         [80, 480], [85, 510], [90, 540], [95, 570], [100, 600]]
        self.trip_two = [[5, 30], [10, 60], [15, 90], [20, 120], [25, 150],
                         [30, 180], [35, 210], [40, 240], [45, 270], [50, 300],
                         [55, 330], [60, 360], [65, 390], [70, 420], [75, 450],
                         [80, 480], [85, 510], [90, 540], [95, 570], [100, 600]]
        self.trip_three = [[5, 30], [10, 60], [15, 90], [20, 120], [25, 150],
                           [30, 180], [35, 210], [40, 240], [45, 270],
                           [50, 300], [55, 330], [60, 360], [65, 390],
                           [70, 420], [75, 450], [80, 480], [85, 510],
                           [90, 540], [95, 570], [100, 600]]
        self.df_trip_one = pd.DataFrame(self.trip_one, columns=['Dist', 'Time'])
        self.df_trip_two = pd.DataFrame(self.trip_two, columns=['Dist', 'Time'])
        self.df_trip_three = pd.DataFrame(self.trip_three,
                                          columns=['Dist', 'Time'])
        self.margin1trip = []
        self.margin2trip = []
        self.margin3trip = []
        self.flagBuy = False
        self.flagSell = False
        self.sellPerMonthDataset = 0.0
        self.buyPerMonthDataset = 0.0
        self.buySlabDataset1 = 0.0
        self.sellSlabDataset1 = 0.0
        self.buySlabDataset2 = 0.0
        self.sellSlabDataset2 = 0.0
        self.buySlabDataset3 = 0.0
        self.sellSlabDataset3 = 0.0
        self.sell = {}
        self.buy = {}
        self.sellPerDayDataset1 = 0.0
        self.buyPerDayDataset1 = 0.0
        self.sellPerDayDataset2 = 0.0
        self.buyPerDayDataset2 = 0.0
        self.sellPerDayDataset3 = 0.0
        self.buyPerDayDataset3 = 0.0
        self.totSellSlabDataset1 = 0.0
        self.totBuySlabDataset1 = 0.0
        self.totSellSlabDataset2 = 0.0
        self.totBuySlabDataset2 = 0.0
        self.totSellSlabDataset3 = 0.0
        self.totBuySlabDataset3 = 0.0
        self.totSellSlabDatasetList1 = []
        self.totBuySlabDatasetList1 = []
        self.totSellSlabDatasetList2 = []
        self.totBuySlabDatasetList2 = []
        self.totSellSlabDatasetList3 = []
        self.totBuySlabDatasetList3 = []
        self.totalDist = self.df_trip_one['Dist'].sum()
        self.totalTime = self.df_trip_one['Time'].sum()
        self.totalDist2 = self.df_trip_two['Dist'].sum()
        self.totalTime2 = self.df_trip_two['Time'].sum()
        self.totalTime3 = self.df_trip_three['Time'].sum()
        self.totalDist3 = self.df_trip_three['Dist'].sum()
        self.per_day_one_two = [
            [self.trip_one[i][j] + self.trip_two[i][j] for j in
             range(len(self.trip_one[0]))] for i in range(len(self.trip_one))]
        self.df_per_day_one_two = pd.DataFrame(self.per_day_one_two,
                                               columns=['Dist', 'Time'])
        self.per_day_all = [
            [self.per_day_one_two[i][j] + self.trip_three[i][j] for j in
             range(len(self.per_day_one_two[0]))] for i in
            range(len(self.per_day_one_two))]
        self.df_per_day_all = pd.DataFrame(self.per_day_all,
                                           columns=['Dist', 'Time'])
        # print(self.totalDist, self.totalTime)

    def base_rate_calc(self, buy_rate, sell_rate, leave_num):
        baseBuy = float(buy_rate.driver_base_payment_amount or 0)
        baseSell = float(sell_rate.base_pay or 0)
        baseBuyInt = buy_rate.base_payment_interval
        baseSellInt = sell_rate.base_pay_interval

        if baseBuyInt == 'Daily':
            factorB = 30
        elif baseBuyInt == 'Weekly':
            factorB = 4
            # self.leaveFactor = 4.0
        elif baseBuyInt == 'Per-Trip':
            factorB = 30
            self.flagBuy = True
        elif baseBuyInt == 'Month':
            factorB = 1
            # self.leaveFactor = 1.0
        elif baseBuyInt == 'FortNightly':
            factorB = 2
            # self.leaveFactor = 2.0
        if baseSellInt == 'Daily':
            factorS = 30
        elif baseSellInt == 'Weekly':
            factorS = 4
        elif baseSellInt == 'Per-Trip':
            factorS = 30
            self.flagSell = True
        elif baseSellInt == 'Month':
            factorS = 1
        elif baseSellInt == 'FortNightly':
            factorS = 2

        self.baseBuy = baseBuy * factorB
        # self.baseSell= baseSell * factorS
        self.baseSell = baseSell * factorS - (
            baseSell / float(30 / factorS)) * leave_num

    def sell_vehicle_slab(self):
        for i in range(0, len(self.sellVeh)):
            sellVehStart = float(self.sellVeh[i].start)
            if not self.sell.get(self.sellVeh[i].attribute, 0) or self.sell.get(
                self.sellVeh[i].attribute, 0) >= sellVehStart:
                self.sell[self.sellVeh[i].attribute] = sellVehStart

        return ()

    def buy_vehicle_slab(self):
        for i in range(0, len(self.buyVeh)):
            buyVehEnd = float(self.buyVeh[i].end)
            if not self.buy.get(self.buyVeh[i].attribute, 0) or self.buy.get(
                self.buyVeh[i].attribute, 0) <= buyVehEnd:
                self.buy[self.buyVeh[i].attribute] = buyVehEnd
        return ()

    def sell_calc(self, compVariable, sellVehStart, sellVehEnd, sellVehStep,
                  sellVehRate, incrVariable):
        # compVariable - the one for comparing eg-userPerDayKm,userDurMinOne
        # incrVariable - the one for incrementing eg-sellSlabUser1 etc.
        flag = False
        if compVariable <= sellVehStart:
            incrVariable += 0.0
            flag = False
        elif compVariable >= sellVehStart and compVariable < sellVehEnd:
            incrVariable += (
                ((compVariable - sellVehStart) / sellVehStep) * sellVehRate)
            flag = True
        else:
            incrVariable += (
                ((sellVehEnd - sellVehStart) / sellVehStep) * sellVehRate)
            flag = False
        return incrVariable, flag

    def buy_calc(self, compVariable, buyVehStart, buyVehEnd, buyVehStep,
                 buyVehRate, incrVariable):
        if compVariable <= buyVehStart:
            incrVariable += 0.0
        elif compVariable > buyVehStart and compVariable < buyVehEnd:
            incrVariable += (
                ((compVariable - buyVehStart) / buyVehStep) * buyVehRate)
        else:
            incrVariable += (
                ((buyVehEnd - buyVehStart) / buyVehStep) * buyVehRate)
        return incrVariable

    def one_trip_margins(self, leave_num, leave_rate):  # leave_days
        for j in range(0, len(self.df_trip_one)):
            self.sellPerMonthDataset = 0.0
            self.buyPerMonthDataset = 0.0
            self.sellPerDayDataset1 = 0.0
            self.buyPerDayDataset1 = 0.0
            self.sellSlabDataset1 = 0.0
            self.buySlabDataset1 = 0.0
            # self.totSellSlabDataset1 = 0.0 #making 0 to calculate for each dist-time separately
            self.totBuySlabDataset1 = 0.0  # making 0 to calculate for each dist-time separately
            sellVehFix = 0.0
            for i in range(0, len(self.sellVeh)):
                self.totSellSlabDataset1 = 0.0
                # CODE FOR SELL SLAB
                sellVehStart = float(self.sellVeh[i].start)
                sellVehEnd = float(self.sellVeh[i].end)
                sellVehStep = float(self.sellVeh[i].step_size)
                sellVehFix = float(self.sellVeh[i].fixed_amount)
                sellVehAtt = self.sellVeh[i].attribute
                sellVehRate = float(self.sellVeh[i].rate)
                sellVehInt = self.sellVeh[i].interval
                flag = False
                if sellVehInt == 'Per-Month':

                    if sellVehAtt == 'Minutes':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            self.totalTime, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerMonthDataset)
                    if sellVehAtt == 'Hours':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            self.totalTime / 60.0, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerMonthDataset)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            self.totalDist, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerMonthDataset)
                    if flag:
                        self.sellPerMonthDataset += sellVehFix

                if sellVehInt == 'Trip':
                    if sellVehAtt == 'Minutes':
                        self.sellSlabDataset1, flag = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                    if sellVehAtt == 'Hours':
                        self.sellSlabDataset1, flag = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                    if sellVehAtt == 'Kilometres':
                        self.sellSlabDataset1, flag = self.sell_calc(
                            self.df_trip_one.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                    # print('Per-Trip-Sell ',self.sellSlabDataset1)
                    if flag:
                        self.sellSlabDataset1 += sellVehFix

                if sellVehInt == 'Per-Day':
                    if sellVehAtt == 'Minutes':
                        self.sellPerDayDataset1, flag = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset1)
                    if sellVehAtt == 'Hours':
                        self.sellPerDayDataset1, flag = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset1)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerDayDataset1, flag = self.sell_calc(
                            self.df_trip_one.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset1)
                    if flag:
                        self.sellPerDayDataset1 += sellVehFix

                self.totSellSlabDataset1 = self.sellPerMonthDataset / (
                    30.0 - leave_num) + self.sellSlabDataset1 + self.sellPerDayDataset1  # leave_days
            self.totSellSlabDatasetList1.append(self.totSellSlabDataset1)

            for i in range(0, len(self.buyVeh)):
                # CODE FOR BUY SLAB
                buyVehStart = float(self.buyVeh[i].start)
                buyVehEnd = float(self.buyVeh[i].end)
                buyVehStep = float(self.buyVeh[i].step_size)
                buyVehFix = float(self.buyVeh[i].fixed_amount)
                buyVehAtt = self.buyVeh[i].attribute
                buyVehRate = float(self.buyVeh[i].rate)
                buyVehInt = self.buyVeh[i].interval

                if buyVehInt == 'Trip':
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)

                if buyVehInt == 'Per-Month':
                    if buyVehAtt == 'Minutes':
                        self.buyPerMonthDataset = self.buy_calc(self.totalTime,
                                                                buyVehStart,
                                                                buyVehEnd,
                                                                buyVehStep,
                                                                buyVehRate,
                                                                self.buyPerMonthDataset)
                    if buyVehAtt == 'Hours':
                        self.buyPerMonthDataset = self.buy_calc(
                            self.totalTime / 60.0, buyVehStart, buyVehEnd,
                            buyVehStep, buyVehRate, self.buyPerMonthDataset)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerMonthDataset = self.buy_calc(self.totalDist,
                                                                buyVehStart,
                                                                buyVehEnd,
                                                                buyVehStep,
                                                                buyVehRate,
                                                                self.buyPerMonthDataset)

                if buyVehInt == 'Per-Day':
                    if buyVehAtt == 'Minutes':
                        self.buyPerDayDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset1)
                    if buyVehAtt == 'Hours':
                        self.buyPerDayDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset1)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerDayDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset1)

                self.totBuySlabDataset1 = self.buySlabDataset1 + self.buyPerMonthDataset / (
                    30.0 - leave_num) + self.buyPerDayDataset1
            self.totBuySlabDatasetList1.append(self.totBuySlabDataset1)

            baseSellAndSlabDataset = self.baseSell
            if len(self.totSellSlabDatasetList1) > 0:
                baseSellAndSlabDataset += (self.totSellSlabDatasetList1[j]) * (
                    30.0 - leave_num)  # leave_days
            baseBuyAndSlabDataset = (self.totBuySlabDatasetList1[j]) * (
                30.0 - leave_num) + self.baseBuy  # leave_days
            try:
                margin1 = ((
                               baseSellAndSlabDataset - baseBuyAndSlabDataset + self.leave_deduction(
                               leave_num,
                               leave_rate)) / baseSellAndSlabDataset) * 100.0
            except ValueError:
                margin1 = 0
            self.margin1trip.append(round(margin1, 3))
            # self.margin1trip=[max(self.margin1trip),min(self.margin1trip),mean(self.margin1trip)]
        return self.margin1trip

    def two_trip_margins(self, leave_num, leave_rate):

        for j in range(0, len(self.df_trip_two)):
            self.sellPerMonthDataset = 0.0
            self.buyPerMonthDataset = 0.0
            self.sellPerDayDataset2 = 0.0
            self.buyPerDayDataset2 = 0.0
            self.sellSlabDataset1 = 0.0
            self.buySlabDataset1 = 0.0
            self.buySlabDataset2 = 0.0
            self.sellSlabDataset2 = 0.0
            self.totSellSlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            self.totBuySlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            sellVehFix = 0.0
            for i in range(0, len(self.sellVeh)):
                # CODE FOR SELL SLAB
                sellVehStart = float(self.sellVeh[i].start)
                sellVehEnd = float(self.sellVeh[i].end)
                sellVehStep = float(self.sellVeh[i].step_size)
                sellVehFix = float(self.sellVeh[i].fixed_amount)
                sellVehAtt = self.sellVeh[i].attribute
                sellVehRate = float(self.sellVeh[i].rate)
                sellVehInt = self.sellVeh[i].interval
                flag, flag1, flag2 = False, False, False
                if sellVehInt == 'Per-Month':
                    if sellVehAtt == 'Minutes':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            self.totalTime + self.totalTime2, sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Hours':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            (self.totalTime + self.totalTime2) / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            self.totalDist + self.totalDist2, sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if flag:
                        self.sellPerMonthDataset += sellVehFix
                if sellVehInt == 'Trip':
                    if sellVehAtt == 'Minutes':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            self.df_trip_two.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset2)
                    if sellVehAtt == 'Hours':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            self.df_trip_two.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset2)
                    if sellVehAtt == 'Kilometres':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            self.df_trip_one.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            self.df_trip_two.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset2)
                    # print('Per-Trip-Sell ',self.sellSlabDataset1)
                    if flag2:
                        self.sellSlabDataset2 += sellVehFix
                    if flag1:
                        self.sellSlabDataset1 += sellVehFix

                if sellVehInt == 'Per-Day':
                    if sellVehAtt == 'Minutes':
                        self.sellPerDayDataset2, flag = self.sell_calc(
                            self.df_per_day_one_two.loc[j, 'Time'],
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset2)
                    if sellVehAtt == 'Hours':
                        self.sellPerDayDataset2, flag = self.sell_calc(
                            self.df_per_day_one_two.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset2)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerDayDataset2, flag = self.sell_calc(
                            self.df_per_day_one_two.loc[j, 'Dist'],
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset2)
                    if flag:
                        self.sellPerDayDataset2 += sellVehFix

                self.totSellSlabDataset2 = self.sellPerMonthDataset / (
                    30.0 - leave_num) + self.sellSlabDataset1 + self.sellSlabDataset2 + self.sellPerDayDataset2
            self.totSellSlabDatasetList2.append(self.totSellSlabDataset2)

            for i in range(0, len(self.buyVeh)):
                # CODE FOR BUY SLAB
                buyVehStart = float(self.buyVeh[i].start)
                buyVehEnd = float(self.buyVeh[i].end)
                buyVehStep = float(self.buyVeh[i].step_size)
                buyVehFix = float(self.buyVeh[i].fixed_amount)
                buyVehAtt = self.buyVeh[i].attribute
                buyVehRate = float(self.buyVeh[i].rate)
                buyVehInt = self.buyVeh[i].interval

                if buyVehInt == 'Trip':
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                if buyVehInt == 'Second Trip of the day':
                    self.buySlabDataset2 = 0
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)

                if buyVehInt == 'Per-Month':
                    if buyVehAtt == 'Minutes':
                        self.buyPerMonthDataset = self.buy_calc(
                            self.totalTime + self.totalTime2, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Hours':
                        self.buyPerMonthDataset = self.buy_calc(
                            (self.totalTime + self.totalTime2) / 60.0,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerMonthDataset = self.buy_calc(
                            self.totalDist + self.totalDist2, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)

                if buyVehInt == 'Per-Day':
                    if buyVehAtt == 'Minutes':
                        self.buyPerDayDataset2 = self.buy_calc(
                            self.df_per_day_one_two.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset2)
                    if buyVehAtt == 'Hours':
                        self.buyPerDayDataset2 = self.buy_calc(
                            self.df_per_day_one_two.loc[j, 'Time'] / 60.0,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerDayDataset2 = self.buy_calc(
                            self.df_per_day_one_two.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset2)

                self.totBuySlabDataset2 = self.buySlabDataset1 + self.buyPerMonthDataset / (
                    30.0 - leave_num) + self.buySlabDataset2 + self.buyPerDayDataset2

            self.totBuySlabDatasetList2.append(self.totBuySlabDataset2)

            if not self.flagSell:
                baseSellAndSlabDataset = self.totSellSlabDatasetList2[j] * (
                    30.0 - leave_num) + self.baseSell

            else:
                baseSellAndSlabDataset = self.totSellSlabDatasetList2[j] * (
                    30.0 - leave_num) + self.baseSell * 2.0

            if not self.flagBuy:
                baseBuyAndSlabDataset = self.totBuySlabDatasetList2[j] * (
                    30.0 - leave_num) + self.baseBuy
            else:
                baseBuyAndSlabDataset = self.totBuySlabDatasetList2[j] * (
                    30.0 - leave_num) + self.baseBuy * 2.0
            try:
                margin2 = ((
                               baseSellAndSlabDataset - baseBuyAndSlabDataset + self.leave_deduction(
                               leave_num,
                               leave_rate)) / baseSellAndSlabDataset) * 100.0
            except ValueError:
                margin2 = 0

            self.margin2trip.append(round(margin2, 3))

            # self.margin2trip=[max(self.margin2trip),min(self.margin2trip),mean(self.margin2trip)]
        return (self.margin2trip)

    def three_trip_margins(self, leave_num, leave_rate):
        for j in range(0, len(self.df_trip_two)):

            self.sellPerMonthDataset = 0.0
            self.buyPerMonthDataset = 0.0
            self.sellSlabDataset1 = 0.0
            self.buySlabDataset1 = 0.0
            self.buySlabDataset2 = 0.0
            self.sellSlabDataset2 = 0.0
            self.sellPerDayDataset3 = 0.0
            self.buyPerDayDataset3 = 0.0
            self.buySlabDataset3 = 0.0
            self.sellSlabDataset3 = 0.0
            self.totSellSlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            self.totBuySlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            self.totSellSlabDataset3 = 0.0
            self.totBuySlabDataset3 = 0.0
            sellVehFix = 0.0
            for i in range(0, len(self.sellVeh)):
                # CODE FOR SELL SLAB
                sellVehStart = float(self.sellVeh[i].start)
                sellVehEnd = float(self.sellVeh[i].end)
                sellVehStep = float(self.sellVeh[i].step_size)
                sellVehFix = float(self.sellVeh[i].fixed_amount)
                sellVehAtt = self.sellVeh[i].attribute
                sellVehRate = float(self.sellVeh[i].rate)
                sellVehInt = self.sellVeh[i].interval
                flag, flag1, flag2, flag3 = False, False, False, False
                if sellVehInt == 'Per-Month':
                    if sellVehAtt == 'Minutes':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            self.totalTime + self.totalTime2 + self.totalTime3,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Hours':
                        self.sellPerMonthDataset, flag = self.sell_calc((
                                                                            self.totalTime + self.totalTime2 + self.totalTime3) / 60.0,
                                                                        sellVehStart,
                                                                        sellVehEnd,
                                                                        sellVehStep,
                                                                        sellVehRate,
                                                                        self.sellPerMonthDataset)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            self.totalDist + self.totalDist2 + self.totalDist3,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    # print('Per-Month-Sell ',self.sellPerMonthDataset)
                    if flag:
                        self.sellPerMonthDataset += sellVehFix
                if sellVehInt == 'Trip':
                    if sellVehAtt == 'Minutes':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            self.df_trip_two.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset2)
                        self.sellSlabDataset3, flag3 = self.sell_calc(
                            self.df_trip_three.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset3)
                    if sellVehAtt == 'Hours':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            self.df_trip_two.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset2)
                        self.sellSlabDataset3, flag3 = self.sell_calc(
                            self.df_trip_three.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset3)
                    if sellVehAtt == 'Kilometres':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            self.df_trip_one.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            self.df_trip_two.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset2)
                        self.sellSlabDataset3, flag3 = self.sell_calc(
                            self.df_trip_three.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellSlabDataset3)

                    if flag1:
                        self.sellSlabDataset1 += sellVehFix
                    if flag2:
                        self.sellSlabDataset2 += sellVehFix
                    if flag3:
                        self.sellSlabDataset3 += sellVehFix

                if sellVehInt == 'Per-Day':
                    if sellVehAtt == 'Minutes':
                        self.sellPerDayDataset3, flag = self.sell_calc(
                            self.df_per_day_all.loc[j, 'Time'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset3)
                    if sellVehAtt == 'Hours':
                        self.sellPerDayDataset3, flag = self.sell_calc(
                            self.df_per_day_all.loc[j, 'Time'] / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset3)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerDayDataset3, flag = self.sell_calc(
                            self.df_per_day_all.loc[j, 'Dist'], sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset3)
                    if flag:
                        self.sellPerDayDataset3 += sellVehFix

                self.totSellSlabDataset3 = self.sellPerMonthDataset / (
                    30.0 - leave_num) + self.sellSlabDataset1 + self.sellSlabDataset2 + self.sellSlabDataset3 + self.sellPerDayDataset3
            self.totSellSlabDatasetList3.append(self.totSellSlabDataset3)

            for i in range(0, len(self.buyVeh)):
                # CODE FOR BUY SLAB
                buyVehStart = float(self.buyVeh[i].start)
                buyVehEnd = float(self.buyVeh[i].end)
                buyVehStep = float(self.buyVeh[i].step_size)
                buyVehFix = float(self.buyVeh[i].fixed_amount)
                buyVehAtt = self.buyVeh[i].attribute
                buyVehRate = float(self.buyVeh[i].rate)
                buyVehInt = self.buyVeh[i].interval

                if buyVehInt == 'Trip':
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                        self.buySlabDataset3 = self.buy_calc(
                            self.df_trip_three.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset3)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                        self.buySlabDataset3 = self.buy_calc(
                            self.df_trip_three.loc[j, 'Time'] / 60.0,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset3)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset1 = self.buy_calc(
                            self.df_trip_one.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                        self.buySlabDataset3 = self.buy_calc(
                            self.df_trip_three.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset3)

                if buyVehInt == 'Second Trip of the day':
                    self.buySlabDataset2 = 0
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Time'] / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset2 = self.buy_calc(
                            self.df_trip_two.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset2)

                if buyVehInt == 'Third Trip onwards':
                    self.buySlabDataset3 = 0
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset3 = self.buy_calc(
                            self.df_trip_three.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset3)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset3 = self.buy_calc(
                            self.df_trip_three.loc[j, 'Time'] / 60.0,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset3)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset3 = self.buy_calc(
                            self.df_trip_three.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buySlabDataset3)

                if buyVehInt == 'Per-Month':
                    if buyVehAtt == 'Minutes':
                        self.buyPerMonthDataset = self.buy_calc(
                            self.totalTime + self.totalTime2 + self.totalTime3,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Hours':
                        self.buyPerMonthDataset = self.buy_calc((
                                                                    self.totalTime + self.totalTime2 + self.totalTime3) / 60.0,
                                                                buyVehStart,
                                                                buyVehEnd,
                                                                buyVehStep,
                                                                buyVehRate,
                                                                self.buyPerMonthDataset)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerMonthDataset = self.buy_calc(
                            self.totalDist + self.totalDist2 + self.totalDist3,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                if buyVehInt == 'Per-Day':
                    if buyVehAtt == 'Minutes':
                        self.buyPerDayDataset3 = self.buy_calc(
                            self.df_per_day_all.loc[j, 'Time'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset3)
                    if buyVehAtt == 'Hours':
                        self.buyPerDayDataset3 = self.buy_calc(
                            self.df_per_day_all.loc[j, 'Time'] / 60.0,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset3)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerDayDataset3 = self.buy_calc(
                            self.df_per_day_all.loc[j, 'Dist'], buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerDayDataset3)

                self.totBuySlabDataset3 = self.buySlabDataset1 + self.buyPerMonthDataset / (
                    30.0 - leave_num) + self.buySlabDataset2 + self.buySlabDataset3 + self.buyPerDayDataset3
            self.totBuySlabDatasetList3.append(self.totBuySlabDataset3)

            if not self.flagSell:
                baseSellAndSlabDataset = self.totSellSlabDatasetList3[j] * (
                    30.0 - leave_num) + self.baseSell
            else:
                baseSellAndSlabDataset = self.totSellSlabDatasetList3[j] * (
                    30.0 - leave_num) + self.baseSell * 3.0

            if not self.flagBuy:
                baseBuyAndSlabDataset = self.totBuySlabDatasetList3[j] * (
                    30.0 - leave_num) + self.baseBuy
            else:
                baseBuyAndSlabDataset = self.totBuySlabDatasetList3[j] * (
                    30.0 - leave_num) + self.baseBuy * 3.0
            try:
                margin3 = ((
                               baseSellAndSlabDataset - baseBuyAndSlabDataset + self.leave_deduction(
                               leave_num,
                               leave_rate)) / baseSellAndSlabDataset) * 100.0
            except ValueError:
                margin3 = 0

            self.margin3trip.append(round(margin3, 3))

            # self.margin3trip=[max(self.margin3trip),min(self.margin3trip),mean(self.margin3trip)]
        return (self.margin3trip)

    '''
    def Plotter(self,otm,ttm,thtm):
        
        #x=[]
        x=self.df_trip_one['Dist']
        x=x.tolist()
        plt.clf()
        fig=plt.figure()
        a1=plt.scatter(x,otm)
        a2=plt.scatter(x,ttm)
        a3=plt.scatter(x,thtm)
        a1
        a2
        a3
        plt.legend((a1,a2,a3),('1 trip','2 trips','3+ trips'),scatterpoints=1,loc='lower right',ncol=1,fontsize=8)

        plt.xlabel('Distance')
        plt.ylabel('Margins')
        plt.show()
        fig.savefig('datasetGraphs.png')
    '''

    def leave_deduction(self, absent, leave_rate):
        self.leave_deduct_amount = float(absent) * float(leave_rate)

        return self.leave_deduct_amount

    def margincalcuser(self, num_trips, dist1, time1, dist2, time2, dist3,
                       time3, leave_num, contract_id):
        cterm = ContractTerm.objects.filter(contract_id=contract_id).latest(
            'wef')
        buy_rate = cterm.buy_rate
        sell_rate = cterm.sell_rate
        if sell_rate is None:
            raise Exception(_("Sell rate is not defined for this contract"))
        if buy_rate is not None:
            leave_rate = buy_rate.driver_deduction_per_leave
        else:
            raise Exception(_("Buy rate is not defined for this contract"))
        self.sellVeh = sell_rate.sellvehicleslab_set.all()
        self.buyVeh = buy_rate.buyvehicleslab_set.all()

        baseBuy = float(buy_rate.driver_base_payment_amount or 0)
        baseSell = float(sell_rate.base_pay or 0)
        baseBuyInt = buy_rate.base_payment_interval
        baseSellInt = sell_rate.base_pay_interval

        if baseBuyInt == 'Daily':
            factorB = 30
        elif baseBuyInt == 'Weekly':
            factorB = 4

        elif baseBuyInt == 'Per-Trip':
            factorB = 30
            self.flagBuy = True
        elif baseBuyInt == 'Month':
            factorB = 1
        elif baseBuyInt == 'FortNightly':
            factorB = 2
        if baseSellInt == 'Daily':
            factorS = 30
        elif baseSellInt == 'Weekly':
            factorS = 4
        elif baseSellInt == 'Per-Trip':
            factorS = 30
            self.flagSell = True
        elif baseSellInt == 'Month':
            factorS = 1
        elif baseSellInt == 'FortNightly':
            factorS = 2

        if baseBuyInt == 'Per-Trip':
            self.baseBuy = baseBuy * factorB - (
                baseBuy / float(30 / factorB)) * (leave_num)
        else:
            self.baseBuy = baseBuy * factorB  # - (baseBuy/float(30/factorB))*(leave_num)
        self.baseSell = baseSell * factorS - (
            baseSell / float(30 / factorS)) * (leave_num)

        if num_trips == 1:
            self.sellPerMonthDataset = 0.0
            self.buyPerMonthDataset = 0.0
            self.sellPerDayDataset1 = 0.0
            self.buyPerDayDataset1 = 0.0
            self.sellSlabDataset1 = 0.0
            self.buySlabDataset1 = 0.0
            self.totSellSlabDataset1 = 0.0  # making 0 to calculate for each dist-time separately
            self.totBuySlabDataset1 = 0.0  # making 0 to calculate for each dist-time separately
            self.margin1trip = []
            sellVehFix = 0.0
            for i in range(0, len(self.sellVeh)):
                # CODE FOR SELL SLAB
                sellVehStart = float(self.sellVeh[i].start)
                sellVehEnd = float(self.sellVeh[i].end)
                sellVehStep = float(self.sellVeh[i].step_size)
                sellVehFix = float(self.sellVeh[i].fixed_amount)
                sellVehAtt = self.sellVeh[i].attribute
                sellVehRate = float(self.sellVeh[i].rate)
                sellVehInt = self.sellVeh[i].interval
                flag = False
                if sellVehInt == 'Per-Month':
                    if sellVehAtt == 'Minutes':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            time1 * (30.0 - leave_num), sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Hours':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            time1 * (30.0 - leave_num) / 60.0, sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            dist1 * (30.0 - leave_num), sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if flag:
                        self.sellPerMonthDataset += sellVehFix
                if sellVehInt == 'Trip':
                    if sellVehAtt == 'Minutes':
                        self.sellSlabDataset1, flag = self.sell_calc(time1,
                                                                     sellVehStart,
                                                                     sellVehEnd,
                                                                     sellVehStep,
                                                                     sellVehRate,
                                                                     self.sellSlabDataset1)
                    if sellVehAtt == 'Hours':
                        self.sellSlabDataset1, flag = self.sell_calc(
                            time1 / 60.0, sellVehStart, sellVehEnd, sellVehStep,
                            sellVehRate, self.sellSlabDataset1)
                    if sellVehAtt == 'Kilometres':
                        self.sellSlabDataset1, flag = self.sell_calc(dist1,
                                                                     sellVehStart,
                                                                     sellVehEnd,
                                                                     sellVehStep,
                                                                     sellVehRate,
                                                                     self.sellSlabDataset1)
                    if flag:
                        self.sellSlabDataset1 += sellVehFix

                if sellVehInt == 'Per-Day':
                    if sellVehAtt == 'Minutes':
                        self.sellPerDayDataset1, flag = self.sell_calc(time1,
                                                                       sellVehStart,
                                                                       sellVehEnd,
                                                                       sellVehStep,
                                                                       sellVehRate,
                                                                       self.sellPerDayDataset1)
                    if sellVehAtt == 'Hours':
                        self.sellPerDayDataset1, flag = self.sell_calc(
                            time1 / 60.0, sellVehStart, sellVehEnd, sellVehStep,
                            sellVehRate, self.sellPerDayDataset1)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerDayDataset1, flag = self.sell_calc(dist1,
                                                                       sellVehStart,
                                                                       sellVehEnd,
                                                                       sellVehStep,
                                                                       sellVehRate,
                                                                       self.sellPerDayDataset1)
                    if flag:
                        self.sellPerDayDataset1 += sellVehFix

                self.totSellSlabDataset1 = self.sellPerMonthDataset / (
                    30.0 - leave_num) + self.sellSlabDataset1 + self.sellPerDayDataset1

            for i in range(0, len(self.buyVeh)):
                # CODE FOR BUY SLAB
                buyVehStart = float(self.buyVeh[i].start)
                buyVehEnd = float(self.buyVeh[i].end)
                buyVehStep = float(self.buyVeh[i].step_size)
                buyVehFix = float(self.buyVeh[i].fixed_amount)
                buyVehAtt = self.buyVeh[i].attribute
                buyVehRate = float(self.buyVeh[i].rate)
                buyVehInt = self.buyVeh[i].interval

                if buyVehInt == 'Trip':
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset1 = self.buy_calc(time1, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset1 = self.buy_calc(time1 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset1 = self.buy_calc(dist1, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                if buyVehInt == 'Per-Month':
                    if buyVehAtt == 'Minutes':
                        self.buyPerMonthDataset = self.buy_calc(
                            time1 * (30.0 - leave_num), buyVehStart, buyVehEnd,
                            buyVehStep, buyVehRate, self.buyPerMonthDataset)
                    if buyVehAtt == 'Hours':
                        self.buyPerMonthDataset = self.buy_calc(
                            time1 * (30.0 - leave_num) / 60.0, buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerMonthDataset = self.buy_calc(
                            dist1 * (30.0 - leave_num), buyVehStart, buyVehEnd,
                            buyVehStep, buyVehRate, self.buyPerMonthDataset)
                if buyVehInt == 'Per-Day':
                    if buyVehAtt == 'Minutes':
                        self.buyPerDayDataset1 = self.buy_calc(time1,
                                                               buyVehStart,
                                                               buyVehEnd,
                                                               buyVehStep,
                                                               buyVehRate,
                                                               self.buyPerDayDataset1)
                    if buyVehAtt == 'Hours':
                        self.buyPerDayDataset1 = self.buy_calc(time1 / 60.0,
                                                               buyVehStart,
                                                               buyVehEnd,
                                                               buyVehStep,
                                                               buyVehRate,
                                                               self.buyPerDayDataset1)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerDayDataset1 = self.buy_calc(dist1,
                                                               buyVehStart,
                                                               buyVehEnd,
                                                               buyVehStep,
                                                               buyVehRate,
                                                               self.buyPerDayDataset1)

                self.totBuySlabDataset1 = self.buySlabDataset1 + self.buyPerMonthDataset / (
                    30.0 - leave_num) + self.buyPerDayDataset1

            baseSellAndSlabDataset = self.baseSell + (
                self.totSellSlabDataset1 * (30.0 - leave_num))

            baseBuyAndSlabDataset = (self.totBuySlabDataset1) * (
                30.0 - leave_num) + self.baseBuy
            try:
                margin1 = ((
                               baseSellAndSlabDataset - baseBuyAndSlabDataset + self.leave_deduction(
                               leave_num,
                               leave_rate)) / baseSellAndSlabDataset) * 100.0
            except ValueError:
                margin1 = 0

            self.margin1trip.append(round(margin1, 3))
            # print(self.margin1trip)
            # self.margin1trip=[max(self.margin1trip),min(self.margin1trip),mean(self.margin1trip)]
            return (self.margin1trip)


        elif num_trips == 2:

            self.sellPerMonthDataset = 0.0
            self.buyPerMonthDataset = 0.0
            self.sellPerDayDataset2 = 0.0
            self.buyPerDayDataset2 = 0.0
            self.sellSlabDataset1 = 0.0
            self.buySlabDataset1 = 0.0
            self.buySlabDataset2 = 0.0
            self.sellSlabDataset2 = 0.0
            self.totSellSlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            self.totBuySlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            self.margin2trip = []

            sellVehFix = 0.0
            for i in range(0, len(self.sellVeh)):
                # CODE FOR SELL SLAB

                sellVehStart = float(self.sellVeh[i].start)
                sellVehEnd = float(self.sellVeh[i].end)
                sellVehStep = float(self.sellVeh[i].step_size)
                sellVehFix = float(self.sellVeh[i].fixed_amount)
                sellVehAtt = self.sellVeh[i].attribute
                sellVehRate = float(self.sellVeh[i].rate)
                sellVehInt = self.sellVeh[i].interval
                flag, flag1, flag2 = False, False, False
                if sellVehInt == 'Per-Month':
                    if sellVehAtt == 'Minutes':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            (time1 + time2) * (30.0 - leave_num), sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Hours':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            (time1 + time2) * (30.0 - leave_num) / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            (dist1 + dist2) * (30.0 - leave_num), sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if flag:
                        self.sellPerMonthDataset += sellVehFix
                if sellVehInt == 'Trip':
                    if sellVehAtt == 'Minutes':
                        self.sellSlabDataset1, flag1 = self.sell_calc(time1,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(time2,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset2)
                    if sellVehAtt == 'Hours':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            time1 / 60.0, sellVehStart, sellVehEnd, sellVehStep,
                            sellVehRate, self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            time2 / 60.0, sellVehStart, sellVehEnd, sellVehStep,
                            sellVehRate, self.sellSlabDataset2)
                    if sellVehAtt == 'Kilometres':
                        self.sellSlabDataset1, flag1 = self.sell_calc(dist1,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(dist2,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset2)
                    if flag1:
                        self.sellSlabDataset1 += sellVehFix
                    if flag2:
                        self.sellSlabDataset2 += sellVehFix

                if sellVehInt == 'Per-Day':
                    if sellVehAtt == 'Minutes':
                        self.sellPerDayDataset2, flag = self.sell_calc(
                            time1 + time2, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerDayDataset2)
                    if sellVehAtt == 'Hours':
                        self.sellPerDayDataset2, flag = self.sell_calc(
                            (time1 + time2) / 60.0, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerDayDataset2)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerDayDataset2, flag = self.sell_calc(
                            dist1 + dist2, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerDayDataset2)
                    if flag:
                        self.sellPerDayDataset2 += sellVehFix

                self.totSellSlabDataset2 = self.sellPerMonthDataset / (
                    30.0 - leave_num) + self.sellSlabDataset1 + self.sellSlabDataset2 + self.sellPerDayDataset2

            for i in range(0, len(self.buyVeh)):
                # CODE FOR BUY SLAB
                buyVehStart = float(self.buyVeh[i].start)
                buyVehEnd = float(self.buyVeh[i].end)
                buyVehStep = float(self.buyVeh[i].step_size)
                buyVehFix = float(self.buyVeh[i].fixed_amount)
                buyVehAtt = self.buyVeh[i].attribute
                buyVehRate = float(self.buyVeh[i].rate)
                buyVehInt = self.buyVeh[i].interval

                if buyVehInt == 'Trip':
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset1 = self.buy_calc(time1, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(time2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset1 = self.buy_calc(time1 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(time2 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset1 = self.buy_calc(dist1, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(dist2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                if buyVehInt == 'Second Trip of the day':
                    self.buySlabDataset2 = 0
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset2 = self.buy_calc(time2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset2 = self.buy_calc(time2 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset2 = self.buy_calc(dist2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)

                if buyVehInt == 'Per-Month':
                    if buyVehAtt == 'Minutes':
                        self.buyPerMonthDataset = self.buy_calc(
                            (time1 + time2) * (30.0 - leave_num), buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Hours':
                        self.buyPerMonthDataset = self.buy_calc(
                            (time1 + time2) * (30.0 - leave_num) / 60.0,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerMonthDataset = self.buy_calc(
                            (dist1 + dist2) * (30.0 - leave_num), buyVehStart,
                            buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)

                if buyVehInt == 'Per-Day':
                    if buyVehAtt == 'Minutes':
                        self.buyPerDayDataset2 = self.buy_calc(time1 + time2,
                                                               buyVehStart,
                                                               buyVehEnd,
                                                               buyVehStep,
                                                               buyVehRate,
                                                               self.buyPerDayDataset2)
                    if buyVehAtt == 'Hours':
                        self.buyPerDayDataset2 = self.buy_calc(
                            (time1 + time2) / 60.0, buyVehStart, buyVehEnd,
                            buyVehStep, buyVehRate, self.buyPerDayDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerDayDataset2 = self.buy_calc(dist1 + dist2,
                                                               buyVehStart,
                                                               buyVehEnd,
                                                               buyVehStep,
                                                               buyVehRate,
                                                               self.buyPerDayDataset2)

                self.totBuySlabDataset2 = self.buySlabDataset1 + self.buyPerMonthDataset / (
                    30.0 - leave_num) + self.buySlabDataset2 + self.buyPerDayDataset2

            if not self.flagSell:
                baseSellAndSlabDataset = self.totSellSlabDataset2 * (
                    30.0 - leave_num) + self.baseSell

            else:
                baseSellAndSlabDataset = self.totSellSlabDataset2 * (
                    30.0 - leave_num) + self.baseSell * 2.0

            if not self.flagBuy:
                baseBuyAndSlabDataset = self.totBuySlabDataset2 * (
                    30.0 - leave_num) + self.baseBuy

            else:
                baseBuyAndSlabDataset = self.totBuySlabDataset2 * (
                    30.0 - leave_num) + self.baseBuy * 2.0

            try:
                margin2 = ((
                               baseSellAndSlabDataset - baseBuyAndSlabDataset + self.leave_deduction(
                               leave_num,
                               leave_rate)) / baseSellAndSlabDataset) * 100.0
            except ValueError:
                margin2 = 0

            self.margin2trip.append(round(margin2, 3))

            # self.margin2trip=[max(self.margin2trip),min(self.margin2trip),mean(self.margin2trip)]
            return (self.margin2trip)


        elif num_trips == 3:

            self.sellPerMonthDataset = 0.0
            self.buyPerMonthDataset = 0.0
            self.sellSlabDataset1 = 0.0
            self.buySlabDataset1 = 0.0
            self.buySlabDataset2 = 0.0
            self.sellSlabDataset2 = 0.0
            self.sellPerDayDataset3 = 0.0
            self.buyPerDayDataset3 = 0.0
            self.buySlabDataset3 = 0.0
            self.sellSlabDataset3 = 0.0
            self.totSellSlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            self.totBuySlabDataset2 = 0.0  # making 0 to calculate for each dist-time separately
            self.totSellSlabDataset3 = 0.0
            self.totBuySlabDataset3 = 0.0
            self.margin3trip = []
            sellVehFix = 0.0
            for i in range(0, len(self.sellVeh)):
                # CODE FOR SELL SLAB

                sellVehStart = float(self.sellVeh[i].start)
                sellVehEnd = float(self.sellVeh[i].end)
                sellVehStep = float(self.sellVeh[i].step_size)
                sellVehFix = float(self.sellVeh[i].fixed_amount)
                sellVehAtt = self.sellVeh[i].attribute
                sellVehRate = float(self.sellVeh[i].rate)
                sellVehInt = self.sellVeh[i].interval
                flag, flag1, flag2, flag3 = False, False, False, False

                if sellVehInt == 'Per-Month':
                    if sellVehAtt == 'Minutes':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            (time1 + time2 + time3) * (30.0 - leave_num),
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Hours':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            (time1 + time2 + time3) * (30.0 - leave_num) / 60.0,
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerMonthDataset, flag = self.sell_calc(
                            (dist1 + dist2 + dist3) * (30.0 - leave_num),
                            sellVehStart, sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerMonthDataset)
                    # print('Per-Month-Sell ',self.sellPerMonthDataset)
                    if flag:
                        self.sellPerMonthDataset += sellVehFix
                if sellVehInt == 'Trip':
                    if sellVehAtt == 'Minutes':
                        self.sellSlabDataset1, flag1 = self.sell_calc(time1,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(time2,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset2)
                        self.sellSlabDataset3, flag3 = self.sell_calc(time3,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset3)
                    if sellVehAtt == 'Hours':
                        self.sellSlabDataset1, flag1 = self.sell_calc(
                            time1 / 60.0, sellVehStart, sellVehEnd, sellVehStep,
                            sellVehRate, self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(
                            time2 / 60.0, sellVehStart, sellVehEnd, sellVehStep,
                            sellVehRate, self.sellSlabDataset2)
                        self.sellSlabDataset3, flag3 = self.sell_calc(
                            time3 / 60.0, sellVehStart, sellVehEnd, sellVehStep,
                            sellVehRate, self.sellSlabDataset3)
                    if sellVehAtt == 'Kilometres':
                        self.sellSlabDataset1, flag1 = self.sell_calc(dist1,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset1)
                        self.sellSlabDataset2, flag2 = self.sell_calc(dist2,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset2)
                        self.sellSlabDataset3, flag3 = self.sell_calc(dist3,
                                                                      sellVehStart,
                                                                      sellVehEnd,
                                                                      sellVehStep,
                                                                      sellVehRate,
                                                                      self.sellSlabDataset3)
                    # print('Per-Trip-Sell ',self.sellSlabDataset1)
                    if flag1:
                        self.sellSlabDataset1 += sellVehFix
                    if flag2:
                        self.sellSlabDataset2 += sellVehFix
                    if flag3:
                        self.sellSlabDataset3 += sellVehFix

                if sellVehInt == 'Per-Day':
                    if sellVehAtt == 'Minutes':
                        self.sellPerDayDataset3, flag = self.sell_calc(
                            time1 + time2 + time3, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerDayDataset3)
                    if sellVehAtt == 'Hours':
                        self.sellPerDayDataset3, flag = self.sell_calc(
                            (time1 + time2 + time3) / 60.0, sellVehStart,
                            sellVehEnd, sellVehStep, sellVehRate,
                            self.sellPerDayDataset3)
                    if sellVehAtt == 'Kilometres':
                        self.sellPerDayDataset3, flag = self.sell_calc(
                            dist1 + dist2 + dist3, sellVehStart, sellVehEnd,
                            sellVehStep, sellVehRate, self.sellPerDayDataset3)
                    if flag:
                        self.sellPerDayDataset3 += sellVehFix

                self.totSellSlabDataset3 = self.sellPerMonthDataset / (
                    30.0 - leave_num) + self.sellSlabDataset1 + self.sellSlabDataset2 + self.sellSlabDataset3 + self.sellPerDayDataset3

            for i in range(0, len(self.buyVeh)):
                # CODE FOR BUY SLAB
                buyVehStart = float(self.buyVeh[i].start)
                buyVehEnd = float(self.buyVeh[i].end)
                buyVehStep = float(self.buyVeh[i].step_size)
                buyVehFix = float(self.buyVeh[i].fixed_amount)
                buyVehAtt = self.buyVeh[i].attribute
                buyVehRate = float(self.buyVeh[i].rate)
                buyVehInt = self.buyVeh[i].interval

                if buyVehInt == 'Trip':
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset1 = self.buy_calc(time1, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(time2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                        self.buySlabDataset3 = self.buy_calc(time3, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset3)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset1 = self.buy_calc(time1 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(time2 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                        self.buySlabDataset3 = self.buy_calc(time3 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset3)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset1 = self.buy_calc(dist1, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset1)
                        self.buySlabDataset2 = self.buy_calc(dist2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                        self.buySlabDataset3 = self.buy_calc(dist3, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset3)
                if buyVehInt == 'Second Trip of the day':
                    self.buySlabDataset2 = 0
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset2 = self.buy_calc(time2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset2 = self.buy_calc(time2 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset2 = self.buy_calc(dist2, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset2)
                    # print('Trip 2',self.buySlabDataset2)

                if buyVehInt == 'Third Trip onwards':
                    self.buySlabDataset3 = 0
                    if buyVehAtt == 'Minutes':
                        self.buySlabDataset3 = self.buy_calc(time3, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset3)
                    if buyVehAtt == 'Hours':
                        self.buySlabDataset3 = self.buy_calc(time3 / 60.0,
                                                             buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset3)
                    if buyVehAtt == 'Kilometres':
                        self.buySlabDataset3 = self.buy_calc(dist3, buyVehStart,
                                                             buyVehEnd,
                                                             buyVehStep,
                                                             buyVehRate,
                                                             self.buySlabDataset3)
                    # print('pppp',self.buySlabUser3)

                if buyVehInt == 'Per-Month':
                    if buyVehAtt == 'Minutes':
                        self.buyPerMonthDataset = self.buy_calc(
                            (time1 + time2 + time3) * (30.0 - leave_num),
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Hours':
                        self.buyPerMonthDataset = self.buy_calc(
                            (time1 + time2 + time3) * (30.0 - leave_num) / 60.0,
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerMonthDataset = self.buy_calc(
                            (dist1 + dist2 + dist3) * (30.0 - leave_num),
                            buyVehStart, buyVehEnd, buyVehStep, buyVehRate,
                            self.buyPerMonthDataset)
                    # print('Per-Month-Buy ',self.buyPerMonthDataset)
                if buyVehInt == 'Per-Day':
                    if buyVehAtt == 'Minutes':
                        self.buyPerDayDataset3 = self.buy_calc(
                            time1 + time2 + time3, buyVehStart, buyVehEnd,
                            buyVehStep, buyVehRate, self.buyPerDayDataset3)
                    if buyVehAtt == 'Hours':
                        self.buyPerDayDataset3 = self.buy_calc(
                            (time1 + time2 + time3), buyVehStart, buyVehEnd,
                            buyVehStep, buyVehRate, self.buyPerDayDataset3)
                    if buyVehAtt == 'Kilometres':
                        self.buyPerDayDataset3 = self.buy_calc(
                            dist1 + dist2 + dist3, buyVehStart, buyVehEnd,
                            buyVehStep, buyVehRate, self.buyPerDayDataset3)

                self.totBuySlabDataset3 = self.buySlabDataset1 + self.buyPerMonthDataset / (
                    30.0 - leave_num) + self.buySlabDataset2 + self.buySlabDataset3 + self.buyPerDayDataset3

            if not self.flagSell:
                baseSellAndSlabDataset = self.totSellSlabDataset3 * (
                    30.0 - leave_num) + self.baseSell
            else:
                baseSellAndSlabDataset = self.totSellSlabDataset3 * (
                    30.0 - leave_num) + self.baseSell * 3.0

            if not self.flagBuy:
                baseBuyAndSlabDataset = self.totBuySlabDataset3 * (
                    30.0 - leave_num) + self.baseBuy
            else:
                baseBuyAndSlabDataset = self.totBuySlabDataset3 * (
                    30.0 - leave_num) + self.baseBuy * 3.0
            try:
                margin3 = ((
                               baseSellAndSlabDataset - baseBuyAndSlabDataset + self.leave_deduction(
                               leave_num,
                               leave_rate)) / baseSellAndSlabDataset) * 100.0
            except ValueError:
                margin3 = 0
            self.margin3trip.append(round(margin3, 3))
            # self.margin3trip=[max(self.margin3trip),min(self.margin3trip),mean(self.margin3trip)]
            return (self.margin3trip)

        else:
            return ''

    def DisplayAll(self, contract_id, leave_num):
        cterm = ContractTerm.objects.filter(contract_id=contract_id).latest(
            'wef')
        buy_rate = cterm.buy_rate
        sell_rate = cterm.sell_rate
        if buy_rate is None:
            return 'Buy rate is not defined.'
        leave_rate = buy_rate.driver_deduction_per_leave
        # pay_cycle = Contract.objects.get(id=contract_id).payment_cycle
        self.sellVeh = sell_rate.sellvehicleslab_set.all()
        self.buyVeh = buy_rate.buyvehicleslab_set.all()
        brc = self.base_rate_calc(buy_rate, sell_rate, leave_num)
        svs = self.sell_vehicle_slab()
        bvs = self.buy_vehicle_slab()
        otm = self.one_trip_margins(leave_num, leave_rate)
        ttm = self.two_trip_margins(leave_num, leave_rate)
        thtm = self.three_trip_margins(leave_num, leave_rate)
        # mcu = self.margincalcuser()
        return dict(time=(self.df_trip_one['Time']).tolist(), onetripmargin=otm,
                    twotripmargins=ttm, threetripmargins=thtm)
