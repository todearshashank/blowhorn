Blowhorn Logistics
==================

Re-architect blowhorn based on Django, Oscar and others

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django


:License: MIT


Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run manage.py test
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ py.test

Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Moved to `Live reloading and SASS compilation`_.

.. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html





Sentry
^^^^^^

Sentry is an error logging aggregator service. You can sign up for a free account at  https://sentry.io/signup/?code=cookiecutter  or download and host it yourself.
The system is setup with reasonable defaults, including 404 logging and integration with the WSGI application.

You must set the DSN url in production.


Deployment
----------

The following details how to deploy this application.



Docker
^^^^^^

See detailed `cookiecutter-django Docker documentation`_.

.. _`cookiecutter-django Docker documentation`: http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html

Populate country data
^^^^^^^^^^^^^^^^^^^^^
To populate country data, run::

    $ python manage.py oscar_populate_countries --no-shipping


Docker usage in Dev environments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Building docker for the first time and subsequent updates::

    $ docker-compose -f dev.yml build*

- Apply migration scripts::

    $ docker-compose -f dev.yml run django python manage.py makemigrations*
    $ docker-compose -f dev.yml run django python manage.py migrate*

- Starting sandbox::

    $ docker-compose -f dev.yml up

- Stopping sandbox::

    $ docker-compose -f dev.yml down

Debugging in docker - Interactively
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- To view docker processes, run::

    $ docker ps

+-------------------+-----------------------+------------------------+---------------+---------------------+---------------------------+------------------------+
| **CONTAINER ID**  |    **IMAGE**          |  **COMMAND**           | **CREATED**   |     **STATUS**      |        **PORTS**          |     **NAMES**          |
+-------------------+-----------------------+------------------------+---------------+---------------------+---------------------------+------------------------+
| 6e95e22ada2f      |     blowhorn_django   |  "/entrypoint.sh /s..."|   2 hours ago |    Up 4 minutes     |   0.0.0.0:8000->8000/tcp  |  blowhorn_django_1     |
+-------------------+-----------------------+------------------------+---------------+---------------------+---------------------------+------------------------+
| d8165b0917d3      |     blowhorn_postgres |  "docker-entrypoint..."|   2 hours ago |    Up 4 minutes     |       5432/tcp            |  blowhorn_postgres_1   |
+-------------------+-----------------------+------------------------+---------------+---------------------+---------------------------+------------------------+

- To enter bash shell, run::

    $ docker exec -it <CONTAINER ID> bash

- Remove exited containers::

    $ docker rm $(docker ps -a -f status=exited -q)

- Remove dangling images::

    $ docker rmi $(docker images -f dangling=true -q)

Database backups
^^^^^^^^^^^^^^^^
- Create backups, run::

    $ docker-compose -f dev.yml run postgres backup

- List backups, run::

    $ docker-compose -f dev.yml run postgres list-backups

- Restore backups, run::

    $ docker cp <filename.dmp> <container>:/backups/
    $ docker exec <container> restore <filename.dmp>

- Local copy of backups, run::

    $ docker cp <containerId>:/backups /host/path/target

TO run Tests
^^^^^^^^^^^^
- To run unit tests::

  $ docker-compose -f djangotest.yml run django python manage.py unittest

- PyTest is fairly independent and does not depend on manage.py

  $ docker-compose -f djangotest.yml run django pytest

- Good to alias in ~/.zshrc or ~/.bashrc or ~/.profile

  $ alias ut="docker-compose -f djangotest.yml run django python manage.py unittest"
  $ alias pytest="docker-compose -f djangotest.yml run django pytest"

TO change Language of system
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- First, compile the message file like::

    $ docker-compose -f dev.yml run django python manage.py makemessages -l kn

`kn -for kannada`

- Then, compile message as following::

    $  docker-compose -f dev.yml run django python manage.py compilemessages

- In base.py file change the language code to `kn`, change::
    *LANGUAGE_CODE = 'en_us'* ----------> *LANGUAGE_CODE = 'kn'*

Generate SSL certificates on local
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**Ubuntu users:**

**Method 1:**

- Follow these steps to generate ssl certificate in local system using certbot::

    $ wget https://dl.eff.org/certbot-auto
    $ chmod a+x certbot-auto
    $ ./certbot-auto certonly --standalone -d localhost

- Once created, copy files fullchain.pem and privkey.pem files at the root of blowhorn directory. (Not inside blowhorn directory). Do a docker build.

**Method 2:**

- Create CA key and cert::

    $ openssl genrsa -out server_rootCA.key 2048
    $ openssl req -x509 -new -nodes -key server_rootCA.key -sha256 -days 3650 -out server_rootCA.pem

- Create server_rootCA.csr.cnf::

   [req]
   default_bits = 2048
   prompt = no
   default_md = sha256
   distinguished_name = dn

   [dn]
   C=DE
   ST=Berlin
   L=NeuKoelln
   O=Weisestrasse
   OU=local_RootCA
   emailAddress=ikke@server.berlin
   CN = server.berlin

- Create test.ext configuration file::

   authorityKeyIdentifier=keyid,issuer
   basicConstraints=CA:FALSE
   keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
   subjectAltName = @alt_names

   [alt_names]
   DNS.1 = server.berlin

- Create server key::

   $ openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout server.key -config <( cat server_rootCA.csr.cnf )

- Create server certificate::

   $ openssl x509 -req -in server.csr -CA server_rootCA.pem -CAkey server_rootCA.key -CAcreateserial -out server.crt -days 3650 -sha256 -extfile test.ext

- Add cert and key to nginx and ssl.conf::

    ssl_certificate /etc/nginx/server.crt;
    ssl_certificate_key /etc/nginx/server.key;

- Make certificate trusted in chrome::

    Chromium -> Setting -> (Advanced) Manage Certificates -> Import -> 'server_rootCA.pem'

**NOTE**: Don't forget to change `server_name` in nginx.conf
